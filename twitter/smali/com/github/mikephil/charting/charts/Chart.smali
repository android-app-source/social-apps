.class public abstract Lcom/github/mikephil/charting/charts/Chart;
.super Landroid/view/ViewGroup;
.source "Twttr"

# interfaces
.implements Lfu;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/github/mikephil/charting/data/h",
        "<+",
        "Lgc",
        "<+",
        "Lcom/github/mikephil/charting/data/Entry;",
        ">;>;>",
        "Landroid/view/ViewGroup;",
        "Lfu;"
    }
.end annotation


# instance fields
.field protected C:Z

.field protected D:Lcom/github/mikephil/charting/data/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected E:Z

.field protected F:Lfc;

.field protected G:Landroid/graphics/Paint;

.field protected H:Landroid/graphics/Paint;

.field protected I:Lcom/github/mikephil/charting/components/XAxis;

.field protected J:Z

.field protected K:Lcom/github/mikephil/charting/components/c;

.field protected L:Lcom/github/mikephil/charting/components/Legend;

.field protected M:Lcom/github/mikephil/charting/listener/c;

.field protected N:Lcom/github/mikephil/charting/listener/ChartTouchListener;

.field protected O:Lgr;

.field protected P:Lgp;

.field protected Q:Lfl;

.field protected R:Lhp;

.field protected S:Lew;

.field protected T:[Lfj;

.field protected U:F

.field protected V:Z

.field protected W:Lcom/github/mikephil/charting/components/d;

.field private a:Z

.field protected aa:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private b:F

.field private c:Ljava/lang/String;

.field private d:Lcom/github/mikephil/charting/listener/b;

.field private e:F

.field private f:F

.field private g:F

.field private h:F

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 184
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 73
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/Chart;->C:Z

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->D:Lcom/github/mikephil/charting/data/h;

    .line 84
    iput-boolean v3, p0, Lcom/github/mikephil/charting/charts/Chart;->E:Z

    .line 89
    iput-boolean v3, p0, Lcom/github/mikephil/charting/charts/Chart;->a:Z

    .line 97
    const v0, 0x3f666666    # 0.9f

    iput v0, p0, Lcom/github/mikephil/charting/charts/Chart;->b:F

    .line 102
    new-instance v0, Lfc;

    invoke-direct {v0, v2}, Lfc;-><init>(I)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->F:Lfc;

    .line 124
    iput-boolean v3, p0, Lcom/github/mikephil/charting/charts/Chart;->J:Z

    .line 146
    const-string/jumbo v0, "No chart data available."

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->c:Ljava/lang/String;

    .line 165
    new-instance v0, Lhp;

    invoke-direct {v0}, Lhp;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->R:Lhp;

    .line 175
    iput v1, p0, Lcom/github/mikephil/charting/charts/Chart;->e:F

    iput v1, p0, Lcom/github/mikephil/charting/charts/Chart;->f:F

    iput v1, p0, Lcom/github/mikephil/charting/charts/Chart;->g:F

    iput v1, p0, Lcom/github/mikephil/charting/charts/Chart;->h:F

    .line 393
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/Chart;->i:Z

    .line 462
    iput v1, p0, Lcom/github/mikephil/charting/charts/Chart;->U:F

    .line 708
    iput-boolean v3, p0, Lcom/github/mikephil/charting/charts/Chart;->V:Z

    .line 1616
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->aa:Ljava/util/ArrayList;

    .line 1729
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/Chart;->j:Z

    .line 185
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->a()V

    .line 186
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 192
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 73
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/Chart;->C:Z

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->D:Lcom/github/mikephil/charting/data/h;

    .line 84
    iput-boolean v3, p0, Lcom/github/mikephil/charting/charts/Chart;->E:Z

    .line 89
    iput-boolean v3, p0, Lcom/github/mikephil/charting/charts/Chart;->a:Z

    .line 97
    const v0, 0x3f666666    # 0.9f

    iput v0, p0, Lcom/github/mikephil/charting/charts/Chart;->b:F

    .line 102
    new-instance v0, Lfc;

    invoke-direct {v0, v2}, Lfc;-><init>(I)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->F:Lfc;

    .line 124
    iput-boolean v3, p0, Lcom/github/mikephil/charting/charts/Chart;->J:Z

    .line 146
    const-string/jumbo v0, "No chart data available."

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->c:Ljava/lang/String;

    .line 165
    new-instance v0, Lhp;

    invoke-direct {v0}, Lhp;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->R:Lhp;

    .line 175
    iput v1, p0, Lcom/github/mikephil/charting/charts/Chart;->e:F

    iput v1, p0, Lcom/github/mikephil/charting/charts/Chart;->f:F

    iput v1, p0, Lcom/github/mikephil/charting/charts/Chart;->g:F

    iput v1, p0, Lcom/github/mikephil/charting/charts/Chart;->h:F

    .line 393
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/Chart;->i:Z

    .line 462
    iput v1, p0, Lcom/github/mikephil/charting/charts/Chart;->U:F

    .line 708
    iput-boolean v3, p0, Lcom/github/mikephil/charting/charts/Chart;->V:Z

    .line 1616
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->aa:Ljava/util/ArrayList;

    .line 1729
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/Chart;->j:Z

    .line 193
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->a()V

    .line 194
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 200
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 73
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/Chart;->C:Z

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->D:Lcom/github/mikephil/charting/data/h;

    .line 84
    iput-boolean v3, p0, Lcom/github/mikephil/charting/charts/Chart;->E:Z

    .line 89
    iput-boolean v3, p0, Lcom/github/mikephil/charting/charts/Chart;->a:Z

    .line 97
    const v0, 0x3f666666    # 0.9f

    iput v0, p0, Lcom/github/mikephil/charting/charts/Chart;->b:F

    .line 102
    new-instance v0, Lfc;

    invoke-direct {v0, v2}, Lfc;-><init>(I)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->F:Lfc;

    .line 124
    iput-boolean v3, p0, Lcom/github/mikephil/charting/charts/Chart;->J:Z

    .line 146
    const-string/jumbo v0, "No chart data available."

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->c:Ljava/lang/String;

    .line 165
    new-instance v0, Lhp;

    invoke-direct {v0}, Lhp;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->R:Lhp;

    .line 175
    iput v1, p0, Lcom/github/mikephil/charting/charts/Chart;->e:F

    iput v1, p0, Lcom/github/mikephil/charting/charts/Chart;->f:F

    iput v1, p0, Lcom/github/mikephil/charting/charts/Chart;->g:F

    iput v1, p0, Lcom/github/mikephil/charting/charts/Chart;->h:F

    .line 393
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/Chart;->i:Z

    .line 462
    iput v1, p0, Lcom/github/mikephil/charting/charts/Chart;->U:F

    .line 708
    iput-boolean v3, p0, Lcom/github/mikephil/charting/charts/Chart;->V:Z

    .line 1616
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->aa:Ljava/util/ArrayList;

    .line 1729
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/Chart;->j:Z

    .line 201
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->a()V

    .line 202
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1739
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1740
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1742
    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 1743
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    move-object v0, p1

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    move-object v0, p1

    .line 1744
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/github/mikephil/charting/charts/Chart;->a(Landroid/view/View;)V

    .line 1743
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1746
    :cond_1
    check-cast p1, Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1748
    :cond_2
    return-void
.end method


# virtual methods
.method public a(FF)Lfj;
    .locals 2

    .prologue
    .line 674
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->D:Lcom/github/mikephil/charting/data/h;

    if-nez v0, :cond_0

    .line 675
    const-string/jumbo v0, "MPAndroidChart"

    const-string/jumbo v1, "Can\'t select by touch. No data set."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    const/4 v0, 0x0

    .line 678
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->getHighlighter()Lfl;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lfl;->a(FF)Lfj;

    move-result-object v0

    goto :goto_0
.end method

.method protected a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 209
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/github/mikephil/charting/charts/Chart;->setWillNotDraw(Z)V

    .line 212
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_1

    .line 213
    new-instance v0, Lew;

    invoke-direct {v0}, Lew;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->S:Lew;

    .line 225
    :goto_0
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lho;->a(Landroid/content/Context;)V

    .line 226
    const/high16 v0, 0x43fa0000    # 500.0f

    invoke-static {v0}, Lho;->a(F)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/charts/Chart;->U:F

    .line 228
    new-instance v0, Lcom/github/mikephil/charting/components/c;

    invoke-direct {v0}, Lcom/github/mikephil/charting/components/c;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->K:Lcom/github/mikephil/charting/components/c;

    .line 229
    new-instance v0, Lcom/github/mikephil/charting/components/Legend;

    invoke-direct {v0}, Lcom/github/mikephil/charting/components/Legend;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->L:Lcom/github/mikephil/charting/components/Legend;

    .line 231
    new-instance v0, Lgr;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/Chart;->R:Lhp;

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/Chart;->L:Lcom/github/mikephil/charting/components/Legend;

    invoke-direct {v0, v1, v2}, Lgr;-><init>(Lhp;Lcom/github/mikephil/charting/components/Legend;)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->O:Lgr;

    .line 233
    new-instance v0, Lcom/github/mikephil/charting/components/XAxis;

    invoke-direct {v0}, Lcom/github/mikephil/charting/components/XAxis;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->I:Lcom/github/mikephil/charting/components/XAxis;

    .line 235
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->G:Landroid/graphics/Paint;

    .line 237
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->H:Landroid/graphics/Paint;

    .line 238
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->H:Landroid/graphics/Paint;

    const/16 v1, 0xf7

    const/16 v2, 0xbd

    const/16 v3, 0x33

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 239
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->H:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 240
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->H:Landroid/graphics/Paint;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-static {v1}, Lho;->a(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 242
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/Chart;->C:Z

    if-eqz v0, :cond_0

    .line 243
    const-string/jumbo v0, ""

    const-string/jumbo v1, "Chart.init()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    :cond_0
    return-void

    .line 215
    :cond_1
    new-instance v0, Lew;

    new-instance v1, Lcom/github/mikephil/charting/charts/Chart$1;

    invoke-direct {v1, p0}, Lcom/github/mikephil/charting/charts/Chart$1;-><init>(Lcom/github/mikephil/charting/charts/Chart;)V

    invoke-direct {v0, v1}, Lew;-><init>(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->S:Lew;

    goto :goto_0
.end method

.method public a(Lfj;Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 625
    .line 627
    if-nez p1, :cond_1

    .line 628
    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->T:[Lfj;

    .line 647
    :goto_0
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/Chart;->T:[Lfj;

    invoke-virtual {p0, v1}, Lcom/github/mikephil/charting/charts/Chart;->setLastHighlighted([Lfj;)V

    .line 649
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/Chart;->M:Lcom/github/mikephil/charting/listener/c;

    if-eqz v1, :cond_0

    .line 651
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->u()Z

    move-result v1

    if-nez v1, :cond_4

    .line 652
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->M:Lcom/github/mikephil/charting/listener/c;

    invoke-interface {v0}, Lcom/github/mikephil/charting/listener/c;->a()V

    .line 660
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->invalidate()V

    .line 661
    return-void

    .line 631
    :cond_1
    iget-boolean v1, p0, Lcom/github/mikephil/charting/charts/Chart;->C:Z

    if-eqz v1, :cond_2

    .line 632
    const-string/jumbo v1, "MPAndroidChart"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Highlighted: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lfj;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 634
    :cond_2
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/Chart;->D:Lcom/github/mikephil/charting/data/h;

    invoke-virtual {v1, p1}, Lcom/github/mikephil/charting/data/h;->a(Lfj;)Lcom/github/mikephil/charting/data/Entry;

    move-result-object v1

    .line 635
    if-nez v1, :cond_3

    .line 636
    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->T:[Lfj;

    move-object p1, v0

    move-object v0, v1

    .line 637
    goto :goto_0

    .line 641
    :cond_3
    const/4 v0, 0x1

    new-array v0, v0, [Lfj;

    const/4 v2, 0x0

    aput-object p1, v0, v2

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->T:[Lfj;

    move-object v0, v1

    goto :goto_0

    .line 655
    :cond_4
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/Chart;->M:Lcom/github/mikephil/charting/listener/c;

    invoke-interface {v1, v0, p1}, Lcom/github/mikephil/charting/listener/c;->a(Lcom/github/mikephil/charting/data/Entry;Lfj;)V

    goto :goto_1
.end method

.method public a([Lfj;)V
    .locals 0

    .prologue
    .line 546
    iput-object p1, p0, Lcom/github/mikephil/charting/charts/Chart;->T:[Lfj;

    .line 548
    invoke-virtual {p0, p1}, Lcom/github/mikephil/charting/charts/Chart;->setLastHighlighted([Lfj;)V

    .line 551
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->invalidate()V

    .line 552
    return-void
.end method

.method protected a(Lfj;)[F
    .locals 3

    .prologue
    .line 759
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    invoke-virtual {p1}, Lfj;->i()F

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p1}, Lfj;->j()F

    move-result v2

    aput v2, v0, v1

    return-object v0
.end method

.method protected abstract b()V
.end method

.method protected b(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 424
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->K:Lcom/github/mikephil/charting/components/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->K:Lcom/github/mikephil/charting/components/c;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/components/c;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->K:Lcom/github/mikephil/charting/components/c;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/components/c;->b()Lhk;

    move-result-object v0

    .line 428
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/Chart;->G:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/Chart;->K:Lcom/github/mikephil/charting/components/c;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/c;->u()Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 429
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/Chart;->G:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/Chart;->K:Lcom/github/mikephil/charting/components/c;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/c;->v()F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 430
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/Chart;->G:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/Chart;->K:Lcom/github/mikephil/charting/components/c;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/c;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 431
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/Chart;->G:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/Chart;->K:Lcom/github/mikephil/charting/components/c;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/c;->c()Landroid/graphics/Paint$Align;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 436
    if-nez v0, :cond_1

    .line 437
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/Chart;->R:Lhp;

    invoke-virtual {v1}, Lhp;->b()F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/Chart;->K:Lcom/github/mikephil/charting/components/c;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/components/c;->s()F

    move-result v1

    sub-float v1, v0, v1

    .line 438
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/Chart;->R:Lhp;

    invoke-virtual {v2}, Lhp;->d()F

    move-result v2

    sub-float/2addr v0, v2

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/Chart;->K:Lcom/github/mikephil/charting/components/c;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/c;->t()F

    move-result v2

    sub-float/2addr v0, v2

    .line 444
    :goto_0
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/Chart;->K:Lcom/github/mikephil/charting/components/c;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/c;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/Chart;->G:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v1, v0, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 446
    :cond_0
    return-void

    .line 440
    :cond_1
    iget v1, v0, Lhk;->a:F

    .line 441
    iget v0, v0, Lhk;->b:F

    goto :goto_0
.end method

.method protected c(FF)V
    .locals 2

    .prologue
    .line 375
    .line 377
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->D:Lcom/github/mikephil/charting/data/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->D:Lcom/github/mikephil/charting/data/h;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/h;->j()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 379
    :cond_0
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 384
    :goto_0
    invoke-static {v0}, Lho;->b(F)I

    move-result v0

    .line 387
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/Chart;->F:Lfc;

    invoke-virtual {v1, v0}, Lfc;->a(I)V

    .line 388
    return-void

    .line 381
    :cond_1
    sub-float v0, p2, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    goto :goto_0
.end method

.method protected c(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 721
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->W:Lcom/github/mikephil/charting/components/d;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->u()Z

    move-result v0

    if-nez v0, :cond_1

    .line 749
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 724
    :goto_0
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/Chart;->T:[Lfj;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 726
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/Chart;->T:[Lfj;

    aget-object v2, v2, v0

    .line 728
    iget-object v3, p0, Lcom/github/mikephil/charting/charts/Chart;->D:Lcom/github/mikephil/charting/data/h;

    invoke-virtual {v2}, Lfj;->f()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/github/mikephil/charting/data/h;->a(I)Lgc;

    move-result-object v3

    .line 730
    iget-object v4, p0, Lcom/github/mikephil/charting/charts/Chart;->D:Lcom/github/mikephil/charting/data/h;

    iget-object v5, p0, Lcom/github/mikephil/charting/charts/Chart;->T:[Lfj;

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Lcom/github/mikephil/charting/data/h;->a(Lfj;)Lcom/github/mikephil/charting/data/Entry;

    move-result-object v4

    .line 731
    invoke-interface {v3, v4}, Lgc;->e(Lcom/github/mikephil/charting/data/Entry;)I

    move-result v5

    .line 734
    if-eqz v4, :cond_2

    int-to-float v5, v5

    invoke-interface {v3}, Lgc;->s()I

    move-result v3

    int-to-float v3, v3

    iget-object v6, p0, Lcom/github/mikephil/charting/charts/Chart;->S:Lew;

    invoke-virtual {v6}, Lew;->b()F

    move-result v6

    mul-float/2addr v3, v6

    cmpl-float v3, v5, v3

    if-lez v3, :cond_3

    .line 724
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 737
    :cond_3
    invoke-virtual {p0, v2}, Lcom/github/mikephil/charting/charts/Chart;->a(Lfj;)[F

    move-result-object v3

    .line 740
    iget-object v5, p0, Lcom/github/mikephil/charting/charts/Chart;->R:Lhp;

    aget v6, v3, v1

    aget v7, v3, v8

    invoke-virtual {v5, v6, v7}, Lhp;->b(FF)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 744
    iget-object v5, p0, Lcom/github/mikephil/charting/charts/Chart;->W:Lcom/github/mikephil/charting/components/d;

    invoke-interface {v5, v4, v2}, Lcom/github/mikephil/charting/components/d;->a(Lcom/github/mikephil/charting/data/Entry;Lfj;)V

    .line 747
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/Chart;->W:Lcom/github/mikephil/charting/components/d;

    aget v4, v3, v1

    aget v3, v3, v8

    invoke-interface {v2, p1, v4, v3}, Lcom/github/mikephil/charting/components/d;->a(Landroid/graphics/Canvas;FF)V

    goto :goto_1
.end method

.method public getAnimator()Lew;
    .locals 1

    .prologue
    .line 774
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->S:Lew;

    return-object v0
.end method

.method public getCenter()Lhk;
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 1055
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Lhk;->a(FF)Lhk;

    move-result-object v0

    return-object v0
.end method

.method public getCenterOfView()Lhk;
    .locals 1

    .prologue
    .line 1455
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->getCenter()Lhk;

    move-result-object v0

    return-object v0
.end method

.method public getCenterOffsets()Lhk;
    .locals 1

    .prologue
    .line 1067
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->R:Lhp;

    invoke-virtual {v0}, Lhp;->l()Lhk;

    move-result-object v0

    return-object v0
.end method

.method public getChartBitmap()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 1465
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1467
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1469
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1470
    if-eqz v2, :cond_0

    .line 1472
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1478
    :goto_0
    invoke-virtual {p0, v1}, Lcom/github/mikephil/charting/charts/Chart;->draw(Landroid/graphics/Canvas;)V

    .line 1480
    return-object v0

    .line 1476
    :cond_0
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    goto :goto_0
.end method

.method public getContentRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 1275
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->R:Lhp;

    invoke-virtual {v0}, Lhp;->k()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getData()Lcom/github/mikephil/charting/data/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 1401
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->D:Lcom/github/mikephil/charting/data/h;

    return-object v0
.end method

.method public getDefaultValueFormatter()Lff;
    .locals 1

    .prologue
    .line 984
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->F:Lfc;

    return-object v0
.end method

.method public getDescription()Lcom/github/mikephil/charting/components/c;
    .locals 1

    .prologue
    .line 1243
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->K:Lcom/github/mikephil/charting/components/c;

    return-object v0
.end method

.method public getDragDecelerationFrictionCoef()F
    .locals 1

    .prologue
    .line 799
    iget v0, p0, Lcom/github/mikephil/charting/charts/Chart;->b:F

    return v0
.end method

.method public getExtraBottomOffset()F
    .locals 1

    .prologue
    .line 1125
    iget v0, p0, Lcom/github/mikephil/charting/charts/Chart;->g:F

    return v0
.end method

.method public getExtraLeftOffset()F
    .locals 1

    .prologue
    .line 1139
    iget v0, p0, Lcom/github/mikephil/charting/charts/Chart;->h:F

    return v0
.end method

.method public getExtraRightOffset()F
    .locals 1

    .prologue
    .line 1111
    iget v0, p0, Lcom/github/mikephil/charting/charts/Chart;->f:F

    return v0
.end method

.method public getExtraTopOffset()F
    .locals 1

    .prologue
    .line 1097
    iget v0, p0, Lcom/github/mikephil/charting/charts/Chart;->e:F

    return v0
.end method

.method public getHighlighted()[Lfj;
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->T:[Lfj;

    return-object v0
.end method

.method public getHighlighter()Lfl;
    .locals 1

    .prologue
    .line 1435
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->Q:Lfl;

    return-object v0
.end method

.method public getJobs()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1648
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->aa:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLegend()Lcom/github/mikephil/charting/components/Legend;
    .locals 1

    .prologue
    .line 1254
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->L:Lcom/github/mikephil/charting/components/Legend;

    return-object v0
.end method

.method public getLegendRenderer()Lgr;
    .locals 1

    .prologue
    .line 1264
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->O:Lgr;

    return-object v0
.end method

.method public getMarker()Lcom/github/mikephil/charting/components/d;
    .locals 1

    .prologue
    .line 1214
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->W:Lcom/github/mikephil/charting/components/d;

    return-object v0
.end method

.method public getMarkerView()Lcom/github/mikephil/charting/components/d;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1224
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->getMarker()Lcom/github/mikephil/charting/components/d;

    move-result-object v0

    return-object v0
.end method

.method public getMaxHighlightDistance()F
    .locals 1

    .prologue
    .line 466
    iget v0, p0, Lcom/github/mikephil/charting/charts/Chart;->U:F

    return v0
.end method

.method public getOnChartGestureListener()Lcom/github/mikephil/charting/listener/b;
    .locals 1

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->d:Lcom/github/mikephil/charting/listener/b;

    return-object v0
.end method

.method public getOnTouchListener()Lcom/github/mikephil/charting/listener/ChartTouchListener;
    .locals 1

    .prologue
    .line 697
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->N:Lcom/github/mikephil/charting/listener/ChartTouchListener;

    return-object v0
.end method

.method public getRenderer()Lgp;
    .locals 1

    .prologue
    .line 1420
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->P:Lgp;

    return-object v0
.end method

.method public getViewPortHandler()Lhp;
    .locals 1

    .prologue
    .line 1411
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->R:Lhp;

    return-object v0
.end method

.method public getXAxis()Lcom/github/mikephil/charting/components/XAxis;
    .locals 1

    .prologue
    .line 974
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->I:Lcom/github/mikephil/charting/components/XAxis;

    return-object v0
.end method

.method public getXChartMax()F
    .locals 1

    .prologue
    .line 1035
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v0, v0, Lcom/github/mikephil/charting/components/XAxis;->s:F

    return v0
.end method

.method public getXChartMin()F
    .locals 1

    .prologue
    .line 1040
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v0, v0, Lcom/github/mikephil/charting/components/XAxis;->t:F

    return v0
.end method

.method public getXRange()F
    .locals 1

    .prologue
    .line 1045
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v0, v0, Lcom/github/mikephil/charting/components/XAxis;->u:F

    return v0
.end method

.method public getYMax()F
    .locals 1

    .prologue
    .line 1021
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->D:Lcom/github/mikephil/charting/data/h;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/h;->f()F

    move-result v0

    return v0
.end method

.method public getYMin()F
    .locals 1

    .prologue
    .line 1030
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->D:Lcom/github/mikephil/charting/data/h;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/h;->e()F

    move-result v0

    return v0
.end method

.method public abstract h()V
.end method

.method protected abstract j()V
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 1718
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 1722
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/Chart;->j:Z

    if-eqz v0, :cond_0

    .line 1723
    invoke-direct {p0, p0}, Lcom/github/mikephil/charting/charts/Chart;->a(Landroid/view/View;)V

    .line 1724
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 399
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/Chart;->D:Lcom/github/mikephil/charting/data/h;

    if-nez v1, :cond_2

    .line 401
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/Chart;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 403
    :goto_0
    if-eqz v0, :cond_0

    .line 404
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->getCenter()Lhk;

    move-result-object v0

    .line 405
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/Chart;->c:Ljava/lang/String;

    iget v2, v0, Lhk;->a:F

    iget v0, v0, Lhk;->b:F

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/Chart;->H:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 416
    :cond_0
    :goto_1
    return-void

    .line 401
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 411
    :cond_2
    iget-boolean v1, p0, Lcom/github/mikephil/charting/charts/Chart;->i:Z

    if-nez v1, :cond_0

    .line 413
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->j()V

    .line 414
    iput-boolean v0, p0, Lcom/github/mikephil/charting/charts/Chart;->i:Z

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 1654
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1655
    invoke-virtual {p0, v0}, Lcom/github/mikephil/charting/charts/Chart;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p2, p3, p4, p5}, Landroid/view/View;->layout(IIII)V

    .line 1654
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1657
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 1661
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 1662
    const/high16 v0, 0x42480000    # 50.0f

    invoke-static {v0}, Lho;->a(F)F

    move-result v0

    float-to-int v0, v0

    .line 1664
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->getSuggestedMinimumWidth()I

    move-result v1

    .line 1665
    invoke-static {v0, p1}, Lcom/github/mikephil/charting/charts/Chart;->resolveSize(II)I

    move-result v2

    .line 1664
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1667
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->getSuggestedMinimumHeight()I

    move-result v2

    .line 1668
    invoke-static {v0, p2}, Lcom/github/mikephil/charting/charts/Chart;->resolveSize(II)I

    move-result v0

    .line 1667
    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1663
    invoke-virtual {p0, v1, v0}, Lcom/github/mikephil/charting/charts/Chart;->setMeasuredDimension(II)V

    .line 1670
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 3

    .prologue
    const/16 v2, 0x2710

    .line 1674
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/Chart;->C:Z

    if-eqz v0, :cond_0

    .line 1675
    const-string/jumbo v0, "MPAndroidChart"

    const-string/jumbo v1, "OnSizeChanged()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1677
    :cond_0
    if-lez p1, :cond_3

    if-lez p2, :cond_3

    if-ge p1, v2, :cond_3

    if-ge p2, v2, :cond_3

    .line 1679
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->R:Lhp;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {v0, v1, v2}, Lhp;->a(FF)V

    .line 1681
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/Chart;->C:Z

    if-eqz v0, :cond_1

    .line 1682
    const-string/jumbo v0, "MPAndroidChart"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Setting chart dimens, width: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", height: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1684
    :cond_1
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->aa:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 1685
    invoke-virtual {p0, v0}, Lcom/github/mikephil/charting/charts/Chart;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1688
    :cond_2
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->aa:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1691
    :cond_3
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->h()V

    .line 1693
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 1694
    return-void
.end method

.method public setData(Lcom/github/mikephil/charting/data/h;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 288
    iput-object p1, p0, Lcom/github/mikephil/charting/charts/Chart;->D:Lcom/github/mikephil/charting/data/h;

    .line 289
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/github/mikephil/charting/charts/Chart;->i:Z

    .line 291
    if-nez p1, :cond_1

    .line 308
    :cond_0
    :goto_0
    return-void

    .line 296
    :cond_1
    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/h;->e()F

    move-result v0

    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/h;->f()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/github/mikephil/charting/charts/Chart;->c(FF)V

    .line 298
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->D:Lcom/github/mikephil/charting/data/h;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/h;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgc;

    .line 299
    invoke-interface {v0}, Lgc;->h()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-interface {v0}, Lgc;->g()Lff;

    move-result-object v2

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/Chart;->F:Lfc;

    if-ne v2, v3, :cond_2

    .line 300
    :cond_3
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/Chart;->F:Lfc;

    invoke-interface {v0, v2}, Lgc;->a(Lff;)V

    goto :goto_1

    .line 304
    :cond_4
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->h()V

    .line 306
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/Chart;->C:Z

    if-eqz v0, :cond_0

    .line 307
    const-string/jumbo v0, "MPAndroidChart"

    const-string/jumbo v1, "Data is set."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setDescription(Lcom/github/mikephil/charting/components/c;)V
    .locals 0

    .prologue
    .line 1233
    iput-object p1, p0, Lcom/github/mikephil/charting/charts/Chart;->K:Lcom/github/mikephil/charting/components/c;

    .line 1234
    return-void
.end method

.method public setDragDecelerationEnabled(Z)V
    .locals 0

    .prologue
    .line 790
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/Chart;->a:Z

    .line 791
    return-void
.end method

.method public setDragDecelerationFrictionCoef(F)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 812
    cmpg-float v1, p1, v0

    if-gez v1, :cond_1

    .line 815
    :goto_0
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_0

    .line 816
    const v0, 0x3f7fbe77    # 0.999f

    .line 818
    :cond_0
    iput v0, p0, Lcom/github/mikephil/charting/charts/Chart;->b:F

    .line 819
    return-void

    :cond_1
    move v0, p1

    goto :goto_0
.end method

.method public setDrawMarkerViews(Z)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1371
    invoke-virtual {p0, p1}, Lcom/github/mikephil/charting/charts/Chart;->setDrawMarkers(Z)V

    .line 1372
    return-void
.end method

.method public setDrawMarkers(Z)V
    .locals 0

    .prologue
    .line 1392
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/Chart;->V:Z

    .line 1393
    return-void
.end method

.method public setExtraBottomOffset(F)V
    .locals 1

    .prologue
    .line 1118
    invoke-static {p1}, Lho;->a(F)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/charts/Chart;->g:F

    .line 1119
    return-void
.end method

.method public setExtraLeftOffset(F)V
    .locals 1

    .prologue
    .line 1132
    invoke-static {p1}, Lho;->a(F)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/charts/Chart;->h:F

    .line 1133
    return-void
.end method

.method public setExtraRightOffset(F)V
    .locals 1

    .prologue
    .line 1104
    invoke-static {p1}, Lho;->a(F)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/charts/Chart;->f:F

    .line 1105
    return-void
.end method

.method public setExtraTopOffset(F)V
    .locals 1

    .prologue
    .line 1090
    invoke-static {p1}, Lho;->a(F)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/charts/Chart;->e:F

    .line 1091
    return-void
.end method

.method public setHardwareAccelerationEnabled(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1704
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 1706
    if-eqz p1, :cond_0

    .line 1707
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v2}, Lcom/github/mikephil/charting/charts/Chart;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1714
    :goto_0
    return-void

    .line 1709
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, Lcom/github/mikephil/charting/charts/Chart;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0

    .line 1711
    :cond_1
    const-string/jumbo v0, "MPAndroidChart"

    const-string/jumbo v1, "Cannot enable/disable hardware acceleration for devices below API level 11."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setHighlightPerTapEnabled(Z)V
    .locals 0

    .prologue
    .line 505
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/Chart;->E:Z

    .line 506
    return-void
.end method

.method public setHighlighter(Lfh;)V
    .locals 0

    .prologue
    .line 1445
    iput-object p1, p0, Lcom/github/mikephil/charting/charts/Chart;->Q:Lfl;

    .line 1446
    return-void
.end method

.method protected setLastHighlighted([Lfj;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 528
    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    aget-object v0, p1, v1

    if-nez v0, :cond_1

    .line 529
    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->N:Lcom/github/mikephil/charting/listener/ChartTouchListener;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/listener/ChartTouchListener;->a(Lfj;)V

    .line 533
    :goto_0
    return-void

    .line 531
    :cond_1
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->N:Lcom/github/mikephil/charting/listener/ChartTouchListener;

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/listener/ChartTouchListener;->a(Lfj;)V

    goto :goto_0
.end method

.method public setLogEnabled(Z)V
    .locals 0

    .prologue
    .line 1149
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/Chart;->C:Z

    .line 1150
    return-void
.end method

.method public setMarker(Lcom/github/mikephil/charting/components/d;)V
    .locals 0

    .prologue
    .line 1205
    iput-object p1, p0, Lcom/github/mikephil/charting/charts/Chart;->W:Lcom/github/mikephil/charting/components/d;

    .line 1206
    return-void
.end method

.method public setMarkerView(Lcom/github/mikephil/charting/components/d;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1219
    invoke-virtual {p0, p1}, Lcom/github/mikephil/charting/charts/Chart;->setMarker(Lcom/github/mikephil/charting/components/d;)V

    .line 1220
    return-void
.end method

.method public setMaxHighlightDistance(F)V
    .locals 1

    .prologue
    .line 476
    invoke-static {p1}, Lho;->a(F)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/charts/Chart;->U:F

    .line 477
    return-void
.end method

.method public setNoDataText(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1168
    iput-object p1, p0, Lcom/github/mikephil/charting/charts/Chart;->c:Ljava/lang/String;

    .line 1169
    return-void
.end method

.method public setNoDataTextColor(I)V
    .locals 1

    .prologue
    .line 1177
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->H:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1178
    return-void
.end method

.method public setNoDataTextTypeface(Landroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 1186
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/Chart;->H:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1187
    return-void
.end method

.method public setOnChartGestureListener(Lcom/github/mikephil/charting/listener/b;)V
    .locals 0

    .prologue
    .line 1003
    iput-object p1, p0, Lcom/github/mikephil/charting/charts/Chart;->d:Lcom/github/mikephil/charting/listener/b;

    .line 1004
    return-void
.end method

.method public setOnChartValueSelectedListener(Lcom/github/mikephil/charting/listener/c;)V
    .locals 0

    .prologue
    .line 993
    iput-object p1, p0, Lcom/github/mikephil/charting/charts/Chart;->M:Lcom/github/mikephil/charting/listener/c;

    .line 994
    return-void
.end method

.method public setOnTouchListener(Lcom/github/mikephil/charting/listener/ChartTouchListener;)V
    .locals 0

    .prologue
    .line 688
    iput-object p1, p0, Lcom/github/mikephil/charting/charts/Chart;->N:Lcom/github/mikephil/charting/listener/ChartTouchListener;

    .line 689
    return-void
.end method

.method public setRenderer(Lgp;)V
    .locals 0

    .prologue
    .line 1430
    if-eqz p1, :cond_0

    .line 1431
    iput-object p1, p0, Lcom/github/mikephil/charting/charts/Chart;->P:Lgp;

    .line 1432
    :cond_0
    return-void
.end method

.method public setTouchEnabled(Z)V
    .locals 0

    .prologue
    .line 1196
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/Chart;->J:Z

    .line 1197
    return-void
.end method

.method public setUnbindEnabled(Z)V
    .locals 0

    .prologue
    .line 1759
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/Chart;->j:Z

    .line 1760
    return-void
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 495
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/Chart;->E:Z

    return v0
.end method

.method public u()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 516
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/Chart;->T:[Lfj;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/Chart;->T:[Lfj;

    array-length v1, v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/Chart;->T:[Lfj;

    aget-object v1, v1, v0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public v()Z
    .locals 1

    .prologue
    .line 781
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/Chart;->a:Z

    return v0
.end method

.method public w()Z
    .locals 1

    .prologue
    .line 1158
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/Chart;->C:Z

    return v0
.end method

.method public x()V
    .locals 2

    .prologue
    .line 1282
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1283
    if-eqz v0, :cond_0

    .line 1284
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1285
    :cond_0
    return-void
.end method

.method public y()V
    .locals 2

    .prologue
    .line 1291
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/Chart;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1292
    if-eqz v0, :cond_0

    .line 1293
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1294
    :cond_0
    return-void
.end method

.method public z()Z
    .locals 1

    .prologue
    .line 1381
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/Chart;->V:Z

    return v0
.end method
