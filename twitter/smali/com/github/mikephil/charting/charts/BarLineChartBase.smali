.class public abstract Lcom/github/mikephil/charting/charts/BarLineChartBase;
.super Lcom/github/mikephil/charting/charts/Chart;
.source "Twttr"

# interfaces
.implements Lfr;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "RtlHardcoded"
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/github/mikephil/charting/data/b",
        "<+",
        "Lfz",
        "<+",
        "Lcom/github/mikephil/charting/data/Entry;",
        ">;>;>",
        "Lcom/github/mikephil/charting/charts/Chart",
        "<TT;>;",
        "Lfr;"
    }
.end annotation


# instance fields
.field protected A:Lhj;

.field protected B:[F

.field private a:Z

.field private ab:Z

.field private ac:Z

.field private ad:J

.field private ae:J

.field private af:Landroid/graphics/RectF;

.field private ag:Z

.field protected b:I

.field protected c:Z

.field protected d:Z

.field protected e:Z

.field protected f:Z

.field protected g:Landroid/graphics/Paint;

.field protected h:Landroid/graphics/Paint;

.field protected i:Z

.field protected j:Z

.field protected k:F

.field protected l:Z

.field protected m:Lcom/github/mikephil/charting/listener/d;

.field protected n:Lcom/github/mikephil/charting/components/YAxis;

.field protected o:Lcom/github/mikephil/charting/components/YAxis;

.field protected p:Lhc;

.field protected q:Lhc;

.field protected r:Lhm;

.field protected s:Lhm;

.field protected t:Lgz;

.field protected u:Landroid/graphics/Matrix;

.field protected v:Landroid/graphics/Matrix;

.field protected w:Landroid/graphics/Matrix;

.field protected x:Landroid/graphics/Matrix;

.field protected y:[F

.field protected z:Lhj;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v3, 0x2

    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 145
    invoke-direct {p0, p1}, Lcom/github/mikephil/charting/charts/Chart;-><init>(Landroid/content/Context;)V

    .line 53
    const/16 v0, 0x64

    iput v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->b:I

    .line 58
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->c:Z

    .line 65
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->d:Z

    .line 70
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->e:Z

    .line 76
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->f:Z

    .line 81
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->a:Z

    .line 83
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ab:Z

    .line 84
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ac:Z

    .line 96
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->i:Z

    .line 98
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->j:Z

    .line 103
    const/high16 v0, 0x41700000    # 15.0f

    iput v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->k:F

    .line 108
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->l:Z

    .line 180
    iput-wide v6, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ad:J

    .line 181
    iput-wide v6, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ae:J

    .line 439
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->af:Landroid/graphics/RectF;

    .line 569
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->u:Landroid/graphics/Matrix;

    .line 590
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->v:Landroid/graphics/Matrix;

    .line 611
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->w:Landroid/graphics/Matrix;

    .line 695
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->x:Landroid/graphics/Matrix;

    .line 934
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ag:Z

    .line 1009
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->y:[F

    .line 1267
    invoke-static {v4, v5, v4, v5}, Lhj;->a(DD)Lhj;

    move-result-object v0

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->z:Lhj;

    .line 1286
    invoke-static {v4, v5, v4, v5}, Lhj;->a(DD)Lhj;

    move-result-object v0

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->A:Lhj;

    .line 1534
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->B:[F

    .line 146
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v3, 0x2

    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 141
    invoke-direct {p0, p1, p2}, Lcom/github/mikephil/charting/charts/Chart;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    const/16 v0, 0x64

    iput v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->b:I

    .line 58
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->c:Z

    .line 65
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->d:Z

    .line 70
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->e:Z

    .line 76
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->f:Z

    .line 81
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->a:Z

    .line 83
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ab:Z

    .line 84
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ac:Z

    .line 96
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->i:Z

    .line 98
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->j:Z

    .line 103
    const/high16 v0, 0x41700000    # 15.0f

    iput v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->k:F

    .line 108
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->l:Z

    .line 180
    iput-wide v6, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ad:J

    .line 181
    iput-wide v6, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ae:J

    .line 439
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->af:Landroid/graphics/RectF;

    .line 569
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->u:Landroid/graphics/Matrix;

    .line 590
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->v:Landroid/graphics/Matrix;

    .line 611
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->w:Landroid/graphics/Matrix;

    .line 695
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->x:Landroid/graphics/Matrix;

    .line 934
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ag:Z

    .line 1009
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->y:[F

    .line 1267
    invoke-static {v4, v5, v4, v5}, Lhj;->a(DD)Lhj;

    move-result-object v0

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->z:Lhj;

    .line 1286
    invoke-static {v4, v5, v4, v5}, Lhj;->a(DD)Lhj;

    move-result-object v0

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->A:Lhj;

    .line 1534
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->B:[F

    .line 142
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v3, 0x2

    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 137
    invoke-direct {p0, p1, p2, p3}, Lcom/github/mikephil/charting/charts/Chart;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    const/16 v0, 0x64

    iput v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->b:I

    .line 58
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->c:Z

    .line 65
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->d:Z

    .line 70
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->e:Z

    .line 76
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->f:Z

    .line 81
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->a:Z

    .line 83
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ab:Z

    .line 84
    iput-boolean v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ac:Z

    .line 96
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->i:Z

    .line 98
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->j:Z

    .line 103
    const/high16 v0, 0x41700000    # 15.0f

    iput v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->k:F

    .line 108
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->l:Z

    .line 180
    iput-wide v6, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ad:J

    .line 181
    iput-wide v6, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ae:J

    .line 439
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->af:Landroid/graphics/RectF;

    .line 569
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->u:Landroid/graphics/Matrix;

    .line 590
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->v:Landroid/graphics/Matrix;

    .line 611
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->w:Landroid/graphics/Matrix;

    .line 695
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->x:Landroid/graphics/Matrix;

    .line 934
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ag:Z

    .line 1009
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->y:[F

    .line 1267
    invoke-static {v4, v5, v4, v5}, Lhj;->a(DD)Lhj;

    move-result-object v0

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->z:Lhj;

    .line 1286
    invoke-static {v4, v5, v4, v5}, Lhj;->a(DD)Lhj;

    move-result-object v0

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->A:Lhj;

    .line 1534
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->B:[F

    .line 138
    return-void
.end method


# virtual methods
.method public a(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Lhm;
    .locals 1

    .prologue
    .line 534
    sget-object v0, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    if-ne p1, v0, :cond_0

    .line 535
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->r:Lhm;

    .line 537
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->s:Lhm;

    goto :goto_0
.end method

.method protected a()V
    .locals 5

    .prologue
    const/16 v4, 0xf0

    .line 150
    invoke-super {p0}, Lcom/github/mikephil/charting/charts/Chart;->a()V

    .line 152
    new-instance v0, Lcom/github/mikephil/charting/components/YAxis;

    sget-object v1, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    invoke-direct {v0, v1}, Lcom/github/mikephil/charting/components/YAxis;-><init>(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    .line 153
    new-instance v0, Lcom/github/mikephil/charting/components/YAxis;

    sget-object v1, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->b:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    invoke-direct {v0, v1}, Lcom/github/mikephil/charting/components/YAxis;-><init>(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    .line 155
    new-instance v0, Lhm;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    invoke-direct {v0, v1}, Lhm;-><init>(Lhp;)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->r:Lhm;

    .line 156
    new-instance v0, Lhm;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    invoke-direct {v0, v1}, Lhm;-><init>(Lhp;)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->s:Lhm;

    .line 158
    new-instance v0, Lhc;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->r:Lhm;

    invoke-direct {v0, v1, v2, v3}, Lhc;-><init>(Lhp;Lcom/github/mikephil/charting/components/YAxis;Lhm;)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->p:Lhc;

    .line 159
    new-instance v0, Lhc;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->s:Lhm;

    invoke-direct {v0, v1, v2, v3}, Lhc;-><init>(Lhp;Lcom/github/mikephil/charting/components/YAxis;Lhm;)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->q:Lhc;

    .line 161
    new-instance v0, Lgz;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->r:Lhm;

    invoke-direct {v0, v1, v2, v3}, Lgz;-><init>(Lhp;Lcom/github/mikephil/charting/components/XAxis;Lhm;)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->t:Lgz;

    .line 163
    new-instance v0, Lfh;

    invoke-direct {v0, p0}, Lfh;-><init>(Lfr;)V

    invoke-virtual {p0, v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->setHighlighter(Lfh;)V

    .line 165
    new-instance v0, Lcom/github/mikephil/charting/listener/a;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    invoke-virtual {v1}, Lhp;->p()Landroid/graphics/Matrix;

    move-result-object v1

    const/high16 v2, 0x40400000    # 3.0f

    invoke-direct {v0, p0, v1, v2}, Lcom/github/mikephil/charting/listener/a;-><init>(Lcom/github/mikephil/charting/charts/BarLineChartBase;Landroid/graphics/Matrix;F)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->N:Lcom/github/mikephil/charting/listener/ChartTouchListener;

    .line 167
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->g:Landroid/graphics/Paint;

    .line 168
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->g:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 170
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->g:Landroid/graphics/Paint;

    invoke-static {v4, v4, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 173
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->h:Landroid/graphics/Paint;

    .line 174
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 175
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->h:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 176
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->h:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1}, Lho;->a(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 177
    return-void
.end method

.method public a(FFFF)V
    .locals 6

    .prologue
    .line 623
    iget-object v5, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->w:Landroid/graphics/Matrix;

    .line 624
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    neg-float v4, p4

    move v1, p1

    move v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lhp;->a(FFFFLandroid/graphics/Matrix;)V

    .line 625
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    const/4 v1, 0x0

    invoke-virtual {v0, v5, p0, v1}, Lhp;->a(Landroid/graphics/Matrix;Landroid/view/View;Z)Landroid/graphics/Matrix;

    .line 630
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->j()V

    .line 631
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->postInvalidate()V

    .line 632
    return-void
.end method

.method protected a(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 515
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->i:Z

    if-eqz v0, :cond_0

    .line 518
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    invoke-virtual {v0}, Lhp;->k()Landroid/graphics/RectF;

    move-result-object v0

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 521
    :cond_0
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->j:Z

    if-eqz v0, :cond_1

    .line 522
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    invoke-virtual {v0}, Lhp;->k()Landroid/graphics/RectF;

    move-result-object v0

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 524
    :cond_1
    return-void
.end method

.method protected a(Landroid/graphics/RectF;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 359
    iput v0, p1, Landroid/graphics/RectF;->left:F

    .line 360
    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 361
    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 362
    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 365
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/components/Legend;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/components/Legend;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 366
    sget-object v0, Lcom/github/mikephil/charting/charts/BarLineChartBase$2;->c:[I

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/components/Legend;->f()Lcom/github/mikephil/charting/components/Legend$LegendOrientation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/github/mikephil/charting/components/Legend$LegendOrientation;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 437
    :cond_0
    :goto_0
    return-void

    .line 369
    :pswitch_0
    sget-object v0, Lcom/github/mikephil/charting/charts/BarLineChartBase$2;->b:[I

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/components/Legend;->d()Lcom/github/mikephil/charting/components/Legend$LegendHorizontalAlignment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/github/mikephil/charting/components/Legend$LegendHorizontalAlignment;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 371
    :pswitch_1
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    iget v1, v1, Lcom/github/mikephil/charting/components/Legend;->a:F

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    .line 372
    invoke-virtual {v2}, Lhp;->n()F

    move-result v2

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/components/Legend;->q()F

    move-result v3

    mul-float/2addr v2, v3

    .line 371
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    .line 373
    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/Legend;->s()F

    move-result v2

    add-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    goto :goto_0

    .line 377
    :pswitch_2
    iget v0, p1, Landroid/graphics/RectF;->right:F

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    iget v1, v1, Lcom/github/mikephil/charting/components/Legend;->a:F

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    .line 378
    invoke-virtual {v2}, Lhp;->n()F

    move-result v2

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/components/Legend;->q()F

    move-result v3

    mul-float/2addr v2, v3

    .line 377
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    .line 379
    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/Legend;->s()F

    move-result v2

    add-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    goto :goto_0

    .line 384
    :pswitch_3
    sget-object v0, Lcom/github/mikephil/charting/charts/BarLineChartBase$2;->a:[I

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/components/Legend;->e()Lcom/github/mikephil/charting/components/Legend$LegendVerticalAlignment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/github/mikephil/charting/components/Legend$LegendVerticalAlignment;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 386
    :pswitch_4
    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    iget v1, v1, Lcom/github/mikephil/charting/components/Legend;->b:F

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    .line 387
    invoke-virtual {v2}, Lhp;->m()F

    move-result v2

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/components/Legend;->q()F

    move-result v3

    mul-float/2addr v2, v3

    .line 386
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    .line 388
    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/Legend;->t()F

    move-result v2

    add-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 390
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v0

    invoke-virtual {v0}, Lcom/github/mikephil/charting/components/XAxis;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v0

    invoke-virtual {v0}, Lcom/github/mikephil/charting/components/XAxis;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    iget v0, p1, Landroid/graphics/RectF;->top:F

    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v1

    iget v1, v1, Lcom/github/mikephil/charting/components/XAxis;->E:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    goto/16 :goto_0

    .line 395
    :pswitch_5
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    iget v1, v1, Lcom/github/mikephil/charting/components/Legend;->b:F

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    .line 396
    invoke-virtual {v2}, Lhp;->m()F

    move-result v2

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/components/Legend;->q()F

    move-result v3

    mul-float/2addr v2, v3

    .line 395
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    .line 397
    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/Legend;->t()F

    move-result v2

    add-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 399
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v0

    invoke-virtual {v0}, Lcom/github/mikephil/charting/components/XAxis;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v0

    invoke-virtual {v0}, Lcom/github/mikephil/charting/components/XAxis;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 400
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v1

    iget v1, v1, Lcom/github/mikephil/charting/components/XAxis;->E:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 412
    :pswitch_6
    sget-object v0, Lcom/github/mikephil/charting/charts/BarLineChartBase$2;->a:[I

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/components/Legend;->e()Lcom/github/mikephil/charting/components/Legend$LegendVerticalAlignment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/github/mikephil/charting/components/Legend$LegendVerticalAlignment;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto/16 :goto_0

    .line 414
    :pswitch_7
    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    iget v1, v1, Lcom/github/mikephil/charting/components/Legend;->b:F

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    .line 415
    invoke-virtual {v2}, Lhp;->m()F

    move-result v2

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/components/Legend;->q()F

    move-result v3

    mul-float/2addr v2, v3

    .line 414
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    .line 416
    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/Legend;->t()F

    move-result v2

    add-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    .line 418
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v0

    invoke-virtual {v0}, Lcom/github/mikephil/charting/components/XAxis;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v0

    invoke-virtual {v0}, Lcom/github/mikephil/charting/components/XAxis;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419
    iget v0, p1, Landroid/graphics/RectF;->top:F

    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v1

    iget v1, v1, Lcom/github/mikephil/charting/components/XAxis;->E:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    goto/16 :goto_0

    .line 423
    :pswitch_8
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    iget v1, v1, Lcom/github/mikephil/charting/components/Legend;->b:F

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    .line 424
    invoke-virtual {v2}, Lhp;->m()F

    move-result v2

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/components/Legend;->q()F

    move-result v3

    mul-float/2addr v2, v3

    .line 423
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    .line 425
    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/Legend;->t()F

    move-result v2

    add-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 427
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v0

    invoke-virtual {v0}, Lcom/github/mikephil/charting/components/XAxis;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v0

    invoke-virtual {v0}, Lcom/github/mikephil/charting/components/XAxis;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 428
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v1

    iget v1, v1, Lcom/github/mikephil/charting/components/XAxis;->E:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 366
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_6
    .end packed-switch

    .line 369
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 384
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 412
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public b(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Lcom/github/mikephil/charting/components/YAxis;
    .locals 1

    .prologue
    .line 1368
    sget-object v0, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    if-ne p1, v0, :cond_0

    .line 1369
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    .line 1371
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    goto :goto_0
.end method

.method public b(FF)Lfz;
    .locals 2

    .prologue
    .line 1257
    invoke-virtual {p0, p1, p2}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->a(FF)Lfj;

    move-result-object v1

    .line 1258
    if-eqz v1, :cond_0

    .line 1259
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/b;

    invoke-virtual {v1}, Lfj;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/data/b;->a(I)Lgc;

    move-result-object v0

    check-cast v0, Lfz;

    .line 1261
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()V
    .locals 4

    .prologue
    .line 349
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/b;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/b;->g()F

    move-result v2

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/b;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/b;->h()F

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/github/mikephil/charting/components/XAxis;->a(FF)V

    .line 352
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/b;

    sget-object v2, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/data/b;->a(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)F

    move-result v2

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/b;

    sget-object v3, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    invoke-virtual {v0, v3}, Lcom/github/mikephil/charting/data/b;->b(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)F

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/github/mikephil/charting/components/YAxis;->a(FF)V

    .line 353
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/b;

    sget-object v2, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->b:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/data/b;->a(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)F

    move-result v2

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/b;

    sget-object v3, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->b:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    invoke-virtual {v0, v3}, Lcom/github/mikephil/charting/data/b;->b(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)F

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/github/mikephil/charting/components/YAxis;->a(FF)V

    .line 355
    return-void
.end method

.method public b(FFFF)V
    .locals 6

    .prologue
    .line 951
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ag:Z

    .line 952
    new-instance v0, Lcom/github/mikephil/charting/charts/BarLineChartBase$1;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/github/mikephil/charting/charts/BarLineChartBase$1;-><init>(Lcom/github/mikephil/charting/charts/BarLineChartBase;FFFF)V

    invoke-virtual {p0, v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->post(Ljava/lang/Runnable;)Z

    .line 962
    return-void
.end method

.method public c(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Z
    .locals 1

    .prologue
    .line 1376
    invoke-virtual {p0, p1}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->b(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v0

    invoke-virtual {v0}, Lcom/github/mikephil/charting/components/YAxis;->D()Z

    move-result v0

    return v0
.end method

.method public computeScroll()V
    .locals 1

    .prologue
    .line 557
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->N:Lcom/github/mikephil/charting/listener/ChartTouchListener;

    instance-of v0, v0, Lcom/github/mikephil/charting/listener/a;

    if-eqz v0, :cond_0

    .line 558
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->N:Lcom/github/mikephil/charting/listener/ChartTouchListener;

    check-cast v0, Lcom/github/mikephil/charting/listener/a;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/listener/a;->b()V

    .line 559
    :cond_0
    return-void
.end method

.method protected f()V
    .locals 5

    .prologue
    .line 279
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->C:Z

    if-eqz v0, :cond_0

    .line 280
    const-string/jumbo v0, "MPAndroidChart"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Preparing Value-Px Matrix, xmin: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v2, v2, Lcom/github/mikephil/charting/components/XAxis;->t:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", xmax: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v2, v2, Lcom/github/mikephil/charting/components/XAxis;->s:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", xdelta: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v2, v2, Lcom/github/mikephil/charting/components/XAxis;->u:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->s:Lhm;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v1, v1, Lcom/github/mikephil/charting/components/XAxis;->t:F

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v2, v2, Lcom/github/mikephil/charting/components/XAxis;->u:F

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    iget v3, v3, Lcom/github/mikephil/charting/components/YAxis;->u:F

    iget-object v4, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    iget v4, v4, Lcom/github/mikephil/charting/components/YAxis;->t:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lhm;->a(FFFF)V

    .line 287
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->r:Lhm;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v1, v1, Lcom/github/mikephil/charting/components/XAxis;->t:F

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v2, v2, Lcom/github/mikephil/charting/components/XAxis;->u:F

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    iget v3, v3, Lcom/github/mikephil/charting/components/YAxis;->u:F

    iget-object v4, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    iget v4, v4, Lcom/github/mikephil/charting/components/YAxis;->t:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lhm;->a(FFFF)V

    .line 291
    return-void
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 295
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->s:Lhm;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/components/YAxis;->D()Z

    move-result v1

    invoke-virtual {v0, v1}, Lhm;->a(Z)V

    .line 296
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->r:Lhm;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/components/YAxis;->D()Z

    move-result v1

    invoke-virtual {v0, v1}, Lhm;->a(Z)V

    .line 297
    return-void
.end method

.method public getAxisLeft()Lcom/github/mikephil/charting/components/YAxis;
    .locals 1

    .prologue
    .line 1347
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    return-object v0
.end method

.method public getAxisRight()Lcom/github/mikephil/charting/components/YAxis;
    .locals 1

    .prologue
    .line 1357
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    return-object v0
.end method

.method public bridge synthetic getData()Lcom/github/mikephil/charting/data/b;
    .locals 1

    .prologue
    .line 44
    invoke-super {p0}, Lcom/github/mikephil/charting/charts/Chart;->getData()Lcom/github/mikephil/charting/data/h;

    move-result-object v0

    check-cast v0, Lcom/github/mikephil/charting/data/b;

    return-object v0
.end method

.method public getDrawListener()Lcom/github/mikephil/charting/listener/d;
    .locals 1

    .prologue
    .line 1006
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->m:Lcom/github/mikephil/charting/listener/d;

    return-object v0
.end method

.method public getHighestVisibleX()F
    .locals 4

    .prologue
    .line 1296
    sget-object v0, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    invoke-virtual {p0, v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->a(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Lhm;

    move-result-object v0

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    invoke-virtual {v1}, Lhp;->g()F

    move-result v1

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    .line 1297
    invoke-virtual {v2}, Lhp;->h()F

    move-result v2

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->A:Lhj;

    .line 1296
    invoke-virtual {v0, v1, v2, v3}, Lhm;->a(FFLhj;)V

    .line 1298
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v0, v0, Lcom/github/mikephil/charting/components/XAxis;->s:F

    float-to-double v0, v0

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->A:Lhj;

    iget-wide v2, v2, Lhj;->a:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 1299
    return v0
.end method

.method public getLowestVisibleX()F
    .locals 4

    .prologue
    .line 1277
    sget-object v0, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    invoke-virtual {p0, v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->a(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Lhm;

    move-result-object v0

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    invoke-virtual {v1}, Lhp;->f()F

    move-result v1

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    .line 1278
    invoke-virtual {v2}, Lhp;->h()F

    move-result v2

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->z:Lhj;

    .line 1277
    invoke-virtual {v0, v1, v2, v3}, Lhm;->a(FFLhj;)V

    .line 1279
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v0, v0, Lcom/github/mikephil/charting/components/XAxis;->t:F

    float-to-double v0, v0

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->z:Lhj;

    iget-wide v2, v2, Lhj;->a:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 1280
    return v0
.end method

.method public getMaxVisibleCount()I
    .locals 1

    .prologue
    .line 1043
    iget v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->b:I

    return v0
.end method

.method public getMinOffset()F
    .locals 1

    .prologue
    .line 1176
    iget v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->k:F

    return v0
.end method

.method public getRendererLeftYAxis()Lhc;
    .locals 1

    .prologue
    .line 1441
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->p:Lhc;

    return-object v0
.end method

.method public getRendererRightYAxis()Lhc;
    .locals 1

    .prologue
    .line 1454
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->q:Lhc;

    return-object v0
.end method

.method public getRendererXAxis()Lgz;
    .locals 1

    .prologue
    .line 1428
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->t:Lgz;

    return-object v0
.end method

.method public getScaleX()F
    .locals 1

    .prologue
    .line 1315
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    if-nez v0, :cond_0

    .line 1316
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1318
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    invoke-virtual {v0}, Lhp;->q()F

    move-result v0

    goto :goto_0
.end method

.method public getScaleY()F
    .locals 1

    .prologue
    .line 1325
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    if-nez v0, :cond_0

    .line 1326
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1328
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    invoke-virtual {v0}, Lhp;->r()F

    move-result v0

    goto :goto_0
.end method

.method public getVisibleXRange()F
    .locals 2

    .prologue
    .line 1308
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getHighestVisibleX()F

    move-result v0

    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getLowestVisibleX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    return v0
.end method

.method public getYChartMax()F
    .locals 2

    .prologue
    .line 1468
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    iget v0, v0, Lcom/github/mikephil/charting/components/YAxis;->s:F

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    iget v1, v1, Lcom/github/mikephil/charting/components/YAxis;->s:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method public getYChartMin()F
    .locals 2

    .prologue
    .line 1473
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    iget v0, v0, Lcom/github/mikephil/charting/components/YAxis;->t:F

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    iget v1, v1, Lcom/github/mikephil/charting/components/YAxis;->t:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public h()V
    .locals 4

    .prologue
    .line 302
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->D:Lcom/github/mikephil/charting/data/h;

    if-nez v0, :cond_1

    .line 303
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->C:Z

    if-eqz v0, :cond_0

    .line 304
    const-string/jumbo v0, "MPAndroidChart"

    const-string/jumbo v1, "Preparing... DATA NOT SET."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    :cond_0
    :goto_0
    return-void

    .line 307
    :cond_1
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->C:Z

    if-eqz v0, :cond_2

    .line 308
    const-string/jumbo v0, "MPAndroidChart"

    const-string/jumbo v1, "Preparing..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    :cond_2
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->P:Lgp;

    if-eqz v0, :cond_3

    .line 312
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->P:Lgp;

    invoke-virtual {v0}, Lgp;->a()V

    .line 314
    :cond_3
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->b()V

    .line 316
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->p:Lhc;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    iget v1, v1, Lcom/github/mikephil/charting/components/YAxis;->t:F

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    iget v2, v2, Lcom/github/mikephil/charting/components/YAxis;->s:F

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/components/YAxis;->D()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lhc;->a(FFZ)V

    .line 317
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->q:Lhc;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    iget v1, v1, Lcom/github/mikephil/charting/components/YAxis;->t:F

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    iget v2, v2, Lcom/github/mikephil/charting/components/YAxis;->s:F

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/components/YAxis;->D()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lhc;->a(FFZ)V

    .line 318
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->t:Lgz;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v1, v1, Lcom/github/mikephil/charting/components/XAxis;->t:F

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v2, v2, Lcom/github/mikephil/charting/components/XAxis;->s:F

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lgz;->a(FFZ)V

    .line 320
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->L:Lcom/github/mikephil/charting/components/Legend;

    if-eqz v0, :cond_4

    .line 321
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->O:Lgr;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->D:Lcom/github/mikephil/charting/data/h;

    invoke-virtual {v0, v1}, Lgr;->a(Lcom/github/mikephil/charting/data/h;)V

    .line 323
    :cond_4
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->j()V

    goto :goto_0
.end method

.method protected i()V
    .locals 4

    .prologue
    .line 331
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getLowestVisibleX()F

    move-result v1

    .line 332
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getHighestVisibleX()F

    move-result v2

    .line 334
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/b;

    invoke-virtual {v0, v1, v2}, Lcom/github/mikephil/charting/data/b;->a(FF)V

    .line 336
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/b;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/b;->g()F

    move-result v2

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/b;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/b;->h()F

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/github/mikephil/charting/components/XAxis;->a(FF)V

    .line 339
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/b;

    sget-object v2, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/data/b;->a(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)F

    move-result v2

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/b;

    sget-object v3, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    invoke-virtual {v0, v3}, Lcom/github/mikephil/charting/data/b;->b(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)F

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/github/mikephil/charting/components/YAxis;->a(FF)V

    .line 340
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/b;

    sget-object v2, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->b:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/data/b;->a(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)F

    move-result v2

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/b;

    sget-object v3, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->b:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    invoke-virtual {v0, v3}, Lcom/github/mikephil/charting/data/b;->b(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)F

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/github/mikephil/charting/components/YAxis;->a(FF)V

    .line 343
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->j()V

    .line 344
    return-void
.end method

.method public j()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 444
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ag:Z

    if-nez v0, :cond_3

    .line 448
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->af:Landroid/graphics/RectF;

    invoke-virtual {p0, v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->a(Landroid/graphics/RectF;)V

    .line 450
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->af:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v4

    .line 451
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->af:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    add-float v3, v4, v1

    .line 452
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->af:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, v4

    .line 453
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->af:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v2, v4

    .line 456
    iget-object v4, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/components/YAxis;->J()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 457
    iget-object v4, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    iget-object v5, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->p:Lhc;

    .line 458
    invoke-virtual {v5}, Lhc;->a()Landroid/graphics/Paint;

    move-result-object v5

    .line 457
    invoke-virtual {v4, v5}, Lcom/github/mikephil/charting/components/YAxis;->a(Landroid/graphics/Paint;)F

    move-result v4

    add-float/2addr v0, v4

    .line 461
    :cond_0
    iget-object v4, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/components/YAxis;->J()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 462
    iget-object v4, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    iget-object v5, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->q:Lhc;

    .line 463
    invoke-virtual {v5}, Lhc;->a()Landroid/graphics/Paint;

    move-result-object v5

    .line 462
    invoke-virtual {v4, v5}, Lcom/github/mikephil/charting/components/YAxis;->a(Landroid/graphics/Paint;)F

    move-result v4

    add-float/2addr v1, v4

    .line 466
    :cond_1
    iget-object v4, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/components/XAxis;->x()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/components/XAxis;->h()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 468
    iget-object v4, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v4, v4, Lcom/github/mikephil/charting/components/XAxis;->E:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    invoke-virtual {v5}, Lcom/github/mikephil/charting/components/XAxis;->t()F

    move-result v5

    add-float/2addr v4, v5

    .line 471
    iget-object v5, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    invoke-virtual {v5}, Lcom/github/mikephil/charting/components/XAxis;->y()Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;

    move-result-object v5

    sget-object v6, Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;->b:Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;

    if-ne v5, v6, :cond_4

    .line 473
    add-float/2addr v2, v4

    .line 486
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getExtraTopOffset()F

    move-result v4

    add-float/2addr v3, v4

    .line 487
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getExtraRightOffset()F

    move-result v4

    add-float/2addr v1, v4

    .line 488
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getExtraBottomOffset()F

    move-result v4

    add-float/2addr v2, v4

    .line 489
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getExtraLeftOffset()F

    move-result v4

    add-float/2addr v0, v4

    .line 491
    iget v4, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->k:F

    invoke-static {v4}, Lho;->a(F)F

    move-result v4

    .line 493
    iget-object v5, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    .line 494
    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v6

    .line 495
    invoke-static {v4, v3}, Ljava/lang/Math;->max(FF)F

    move-result v7

    .line 496
    invoke-static {v4, v1}, Ljava/lang/Math;->max(FF)F

    move-result v8

    .line 497
    invoke-static {v4, v2}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 493
    invoke-virtual {v5, v6, v7, v8, v4}, Lhp;->a(FFFF)V

    .line 499
    iget-boolean v4, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->C:Z

    if-eqz v4, :cond_3

    .line 500
    const-string/jumbo v4, "MPAndroidChart"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "offsetLeft: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, ", offsetTop: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, ", offsetRight: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", offsetBottom: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    const-string/jumbo v0, "MPAndroidChart"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Content: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    invoke-virtual {v2}, Lhp;->k()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    :cond_3
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->g()V

    .line 507
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->f()V

    .line 508
    return-void

    .line 475
    :cond_4
    iget-object v5, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    invoke-virtual {v5}, Lcom/github/mikephil/charting/components/XAxis;->y()Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;

    move-result-object v5

    sget-object v6, Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;->a:Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;

    if-ne v5, v6, :cond_5

    .line 477
    add-float/2addr v3, v4

    goto/16 :goto_0

    .line 479
    :cond_5
    iget-object v5, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    invoke-virtual {v5}, Lcom/github/mikephil/charting/components/XAxis;->y()Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;

    move-result-object v5

    sget-object v6, Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;->c:Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;

    if-ne v5, v6, :cond_2

    .line 481
    add-float/2addr v2, v4

    .line 482
    add-float/2addr v3, v4

    goto/16 :goto_0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 1057
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->f:Z

    return v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 1086
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->a:Z

    return v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 1109
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ab:Z

    return v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 1113
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ac:Z

    return v0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 1132
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->e:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 185
    invoke-super {p0, p1}, Lcom/github/mikephil/charting/charts/Chart;->onDraw(Landroid/graphics/Canvas;)V

    .line 187
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->D:Lcom/github/mikephil/charting/data/h;

    if-nez v0, :cond_1

    .line 267
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 193
    invoke-virtual {p0, p1}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->a(Landroid/graphics/Canvas;)V

    .line 195
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/YAxis;->x()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 196
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->p:Lhc;

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    iget v3, v3, Lcom/github/mikephil/charting/components/YAxis;->t:F

    iget-object v4, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    iget v4, v4, Lcom/github/mikephil/charting/components/YAxis;->s:F

    iget-object v5, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    invoke-virtual {v5}, Lcom/github/mikephil/charting/components/YAxis;->D()Z

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Lhc;->a(FFZ)V

    .line 197
    :cond_2
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/YAxis;->x()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 198
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->q:Lhc;

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    iget v3, v3, Lcom/github/mikephil/charting/components/YAxis;->t:F

    iget-object v4, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    iget v4, v4, Lcom/github/mikephil/charting/components/YAxis;->s:F

    iget-object v5, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    invoke-virtual {v5}, Lcom/github/mikephil/charting/components/YAxis;->D()Z

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Lhc;->a(FFZ)V

    .line 199
    :cond_3
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/XAxis;->x()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 200
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->t:Lgz;

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v3, v3, Lcom/github/mikephil/charting/components/XAxis;->t:F

    iget-object v4, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v4, v4, Lcom/github/mikephil/charting/components/XAxis;->s:F

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lgz;->a(FFZ)V

    .line 202
    :cond_4
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->t:Lgz;

    invoke-virtual {v2, p1}, Lgz;->b(Landroid/graphics/Canvas;)V

    .line 203
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->p:Lhc;

    invoke-virtual {v2, p1}, Lhc;->b(Landroid/graphics/Canvas;)V

    .line 204
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->q:Lhc;

    invoke-virtual {v2, p1}, Lhc;->b(Landroid/graphics/Canvas;)V

    .line 206
    iget-boolean v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->c:Z

    if-eqz v2, :cond_5

    .line 207
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->i()V

    .line 210
    :cond_5
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->t:Lgz;

    invoke-virtual {v2, p1}, Lgz;->c(Landroid/graphics/Canvas;)V

    .line 211
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->p:Lhc;

    invoke-virtual {v2, p1}, Lhc;->c(Landroid/graphics/Canvas;)V

    .line 212
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->q:Lhc;

    invoke-virtual {v2, p1}, Lhc;->c(Landroid/graphics/Canvas;)V

    .line 214
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/XAxis;->n()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 215
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->t:Lgz;

    invoke-virtual {v2, p1}, Lgz;->d(Landroid/graphics/Canvas;)V

    .line 217
    :cond_6
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/YAxis;->n()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 218
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->p:Lhc;

    invoke-virtual {v2, p1}, Lhc;->e(Landroid/graphics/Canvas;)V

    .line 220
    :cond_7
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/YAxis;->n()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 221
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->q:Lhc;

    invoke-virtual {v2, p1}, Lhc;->e(Landroid/graphics/Canvas;)V

    .line 224
    :cond_8
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v2

    .line 225
    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    invoke-virtual {v3}, Lhp;->k()Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 227
    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->P:Lgp;

    invoke-virtual {v3, p1}, Lgp;->a(Landroid/graphics/Canvas;)V

    .line 230
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->u()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 231
    iget-object v3, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->P:Lgp;

    iget-object v4, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->T:[Lfj;

    invoke-virtual {v3, p1, v4}, Lgp;->a(Landroid/graphics/Canvas;[Lfj;)V

    .line 234
    :cond_9
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 236
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->P:Lgp;

    invoke-virtual {v2, p1}, Lgp;->c(Landroid/graphics/Canvas;)V

    .line 238
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/XAxis;->n()Z

    move-result v2

    if-nez v2, :cond_a

    .line 239
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->t:Lgz;

    invoke-virtual {v2, p1}, Lgz;->d(Landroid/graphics/Canvas;)V

    .line 241
    :cond_a
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/YAxis;->n()Z

    move-result v2

    if-nez v2, :cond_b

    .line 242
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->p:Lhc;

    invoke-virtual {v2, p1}, Lhc;->e(Landroid/graphics/Canvas;)V

    .line 244
    :cond_b
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/components/YAxis;->n()Z

    move-result v2

    if-nez v2, :cond_c

    .line 245
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->q:Lhc;

    invoke-virtual {v2, p1}, Lhc;->e(Landroid/graphics/Canvas;)V

    .line 247
    :cond_c
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->t:Lgz;

    invoke-virtual {v2, p1}, Lgz;->a(Landroid/graphics/Canvas;)V

    .line 248
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->p:Lhc;

    invoke-virtual {v2, p1}, Lhc;->a(Landroid/graphics/Canvas;)V

    .line 249
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->q:Lhc;

    invoke-virtual {v2, p1}, Lhc;->a(Landroid/graphics/Canvas;)V

    .line 251
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->P:Lgp;

    invoke-virtual {v2, p1}, Lgp;->b(Landroid/graphics/Canvas;)V

    .line 253
    iget-object v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->O:Lgr;

    invoke-virtual {v2, p1}, Lgr;->a(Landroid/graphics/Canvas;)V

    .line 255
    invoke-virtual {p0, p1}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->b(Landroid/graphics/Canvas;)V

    .line 257
    invoke-virtual {p0, p1}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->c(Landroid/graphics/Canvas;)V

    .line 259
    iget-boolean v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->C:Z

    if-eqz v2, :cond_0

    .line 260
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 261
    iget-wide v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ad:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ad:J

    .line 262
    iget-wide v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ae:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ae:J

    .line 263
    iget-wide v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ad:J

    iget-wide v4, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ae:J

    div-long/2addr v2, v4

    .line 264
    const-string/jumbo v4, "MPAndroidChart"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Drawtime: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ms, average: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ms, cycles: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ae:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1540
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->B:[F

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->B:[F

    const/4 v2, 0x0

    aput v2, v1, v3

    aput v2, v0, v4

    .line 1542
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->l:Z

    if-eqz v0, :cond_0

    .line 1543
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->B:[F

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    invoke-virtual {v1}, Lhp;->f()F

    move-result v1

    aput v1, v0, v4

    .line 1544
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->B:[F

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    invoke-virtual {v1}, Lhp;->e()F

    move-result v1

    aput v1, v0, v3

    .line 1545
    sget-object v0, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    invoke-virtual {p0, v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->a(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Lhm;

    move-result-object v0

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->B:[F

    invoke-virtual {v0, v1}, Lhm;->b([F)V

    .line 1549
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/github/mikephil/charting/charts/Chart;->onSizeChanged(IIII)V

    .line 1551
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->l:Z

    if-eqz v0, :cond_1

    .line 1554
    sget-object v0, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    invoke-virtual {p0, v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->a(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Lhm;

    move-result-object v0

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->B:[F

    invoke-virtual {v0, v1}, Lhm;->a([F)V

    .line 1555
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->B:[F

    invoke-virtual {v0, v1, p0}, Lhp;->a([FLandroid/view/View;)V

    .line 1559
    :goto_0
    return-void

    .line 1557
    :cond_1
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    invoke-virtual {v1}, Lhp;->p()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1, p0, v3}, Lhp;->a(Landroid/graphics/Matrix;Landroid/view/View;Z)Landroid/graphics/Matrix;

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 542
    invoke-super {p0, p1}, Lcom/github/mikephil/charting/charts/Chart;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 544
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->N:Lcom/github/mikephil/charting/listener/ChartTouchListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->D:Lcom/github/mikephil/charting/data/h;

    if-nez v1, :cond_1

    .line 551
    :cond_0
    :goto_0
    return v0

    .line 548
    :cond_1
    iget-boolean v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->J:Z

    if-eqz v1, :cond_0

    .line 551
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->N:Lcom/github/mikephil/charting/listener/ChartTouchListener;

    invoke-virtual {v0, p0, p1}, Lcom/github/mikephil/charting/listener/ChartTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 1337
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    invoke-virtual {v0}, Lhp;->s()Z

    move-result v0

    return v0
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 1395
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->d:Z

    return v0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 1424
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    invoke-virtual {v0}, Lhp;->v()Z

    move-result v0

    return v0
.end method

.method public s()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1482
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n:Lcom/github/mikephil/charting/components/YAxis;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/components/YAxis;->D()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1486
    :cond_0
    :goto_0
    return v0

    .line 1484
    :cond_1
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o:Lcom/github/mikephil/charting/components/YAxis;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/components/YAxis;->D()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1486
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAutoScaleMinMaxEnabled(Z)V
    .locals 0

    .prologue
    .line 1498
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->c:Z

    .line 1499
    return-void
.end method

.method public setBorderColor(I)V
    .locals 1

    .prologue
    .line 1169
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1170
    return-void
.end method

.method public setBorderWidth(F)V
    .locals 2

    .prologue
    .line 1160
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->h:Landroid/graphics/Paint;

    invoke-static {p1}, Lho;->a(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1161
    return-void
.end method

.method public setDoubleTapToZoomEnabled(Z)V
    .locals 0

    .prologue
    .line 1123
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->e:Z

    .line 1124
    return-void
.end method

.method public setDragEnabled(Z)V
    .locals 0

    .prologue
    .line 1077
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->a:Z

    .line 1078
    return-void
.end method

.method public setDragOffsetX(F)V
    .locals 1

    .prologue
    .line 1405
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    invoke-virtual {v0, p1}, Lhp;->k(F)V

    .line 1406
    return-void
.end method

.method public setDragOffsetY(F)V
    .locals 1

    .prologue
    .line 1415
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    invoke-virtual {v0, p1}, Lhp;->l(F)V

    .line 1416
    return-void
.end method

.method public setDrawBorders(Z)V
    .locals 0

    .prologue
    .line 1151
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->j:Z

    .line 1152
    return-void
.end method

.method public setDrawGridBackground(Z)V
    .locals 0

    .prologue
    .line 1141
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->i:Z

    .line 1142
    return-void
.end method

.method public setGridBackgroundColor(I)V
    .locals 1

    .prologue
    .line 1067
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1068
    return-void
.end method

.method public setHighlightPerDragEnabled(Z)V
    .locals 0

    .prologue
    .line 1053
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->f:Z

    .line 1054
    return-void
.end method

.method public setKeepPositionOnRotation(Z)V
    .locals 0

    .prologue
    .line 1197
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->l:Z

    .line 1198
    return-void
.end method

.method public setMaxVisibleValueCount(I)V
    .locals 0

    .prologue
    .line 1039
    iput p1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->b:I

    .line 1040
    return-void
.end method

.method public setMinOffset(F)V
    .locals 0

    .prologue
    .line 1183
    iput p1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->k:F

    .line 1184
    return-void
.end method

.method public setOnDrawListener(Lcom/github/mikephil/charting/listener/d;)V
    .locals 0

    .prologue
    .line 997
    iput-object p1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->m:Lcom/github/mikephil/charting/listener/d;

    .line 998
    return-void
.end method

.method public setPinchZoom(Z)V
    .locals 0

    .prologue
    .line 1386
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->d:Z

    .line 1387
    return-void
.end method

.method public setRendererLeftYAxis(Lhc;)V
    .locals 0

    .prologue
    .line 1450
    iput-object p1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->p:Lhc;

    .line 1451
    return-void
.end method

.method public setRendererRightYAxis(Lhc;)V
    .locals 0

    .prologue
    .line 1463
    iput-object p1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->q:Lhc;

    .line 1464
    return-void
.end method

.method public setScaleEnabled(Z)V
    .locals 0

    .prologue
    .line 1096
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ab:Z

    .line 1097
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ac:Z

    .line 1098
    return-void
.end method

.method public setScaleXEnabled(Z)V
    .locals 0

    .prologue
    .line 1101
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ab:Z

    .line 1102
    return-void
.end method

.method public setScaleYEnabled(Z)V
    .locals 0

    .prologue
    .line 1105
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->ac:Z

    .line 1106
    return-void
.end method

.method public setVisibleXRangeMaximum(F)V
    .locals 2

    .prologue
    .line 731
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v0, v0, Lcom/github/mikephil/charting/components/XAxis;->u:F

    div-float/2addr v0, p1

    .line 732
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    invoke-virtual {v1, v0}, Lhp;->a(F)V

    .line 733
    return-void
.end method

.method public setVisibleXRangeMinimum(F)V
    .locals 2

    .prologue
    .line 744
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v0, v0, Lcom/github/mikephil/charting/components/XAxis;->u:F

    div-float/2addr v0, p1

    .line 745
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->R:Lhp;

    invoke-virtual {v1, v0}, Lhp;->b(F)V

    .line 746
    return-void
.end method

.method public setXAxisRenderer(Lgz;)V
    .locals 0

    .prologue
    .line 1437
    iput-object p1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase;->t:Lgz;

    .line 1438
    return-void
.end method
