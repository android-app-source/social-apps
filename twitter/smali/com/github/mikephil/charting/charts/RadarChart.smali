.class public Lcom/github/mikephil/charting/charts/RadarChart;
.super Lcom/github/mikephil/charting/charts/PieRadarChartBase;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/github/mikephil/charting/charts/PieRadarChartBase",
        "<",
        "Lcom/github/mikephil/charting/data/n;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:Lhe;

.field protected b:Lhb;

.field private e:F

.field private f:F

.field private g:I

.field private h:I

.field private i:I

.field private j:Z

.field private k:I

.field private l:Lcom/github/mikephil/charting/components/YAxis;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/16 v1, 0x7a

    .line 71
    invoke-direct {p0, p1}, Lcom/github/mikephil/charting/charts/PieRadarChartBase;-><init>(Landroid/content/Context;)V

    .line 30
    const/high16 v0, 0x40200000    # 2.5f

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->e:F

    .line 35
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->f:F

    .line 40
    invoke-static {v1, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->g:I

    .line 45
    invoke-static {v1, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->h:I

    .line 50
    const/16 v0, 0x96

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->i:I

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->j:Z

    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->k:I

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/16 v1, 0x7a

    .line 75
    invoke-direct {p0, p1, p2}, Lcom/github/mikephil/charting/charts/PieRadarChartBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    const/high16 v0, 0x40200000    # 2.5f

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->e:F

    .line 35
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->f:F

    .line 40
    invoke-static {v1, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->g:I

    .line 45
    invoke-static {v1, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->h:I

    .line 50
    const/16 v0, 0x96

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->i:I

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->j:Z

    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->k:I

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/16 v1, 0x7a

    .line 79
    invoke-direct {p0, p1, p2, p3}, Lcom/github/mikephil/charting/charts/PieRadarChartBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    const/high16 v0, 0x40200000    # 2.5f

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->e:F

    .line 35
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->f:F

    .line 40
    invoke-static {v1, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->g:I

    .line 45
    invoke-static {v1, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->h:I

    .line 50
    const/16 v0, 0x96

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->i:I

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->j:Z

    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->k:I

    .line 80
    return-void
.end method


# virtual methods
.method public a(F)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 181
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/RadarChart;->getRotationAngle()F

    move-result v0

    sub-float v0, p1, v0

    invoke-static {v0}, Lho;->c(F)F

    move-result v2

    .line 183
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/RadarChart;->getSliceAngle()F

    move-result v3

    .line 185
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/n;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/n;->k()Lgc;

    move-result-object v0

    check-cast v0, Lgh;

    invoke-interface {v0}, Lgh;->s()I

    move-result v4

    move v0, v1

    .line 189
    :goto_0
    if-ge v0, v4, :cond_1

    .line 191
    add-int/lit8 v5, v0, 0x1

    int-to-float v5, v5

    mul-float/2addr v5, v3

    const/high16 v6, 0x40000000    # 2.0f

    div-float v6, v3, v6

    sub-float/2addr v5, v6

    .line 193
    cmpl-float v5, v5, v2

    if-lez v5, :cond_0

    .line 199
    :goto_1
    return v0

    .line 189
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method protected a()V
    .locals 3

    .prologue
    .line 84
    invoke-super {p0}, Lcom/github/mikephil/charting/charts/PieRadarChartBase;->a()V

    .line 86
    new-instance v0, Lcom/github/mikephil/charting/components/YAxis;

    sget-object v1, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    invoke-direct {v0, v1}, Lcom/github/mikephil/charting/components/YAxis;-><init>(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->l:Lcom/github/mikephil/charting/components/YAxis;

    .line 88
    const/high16 v0, 0x3fc00000    # 1.5f

    invoke-static {v0}, Lho;->a(F)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->e:F

    .line 89
    const/high16 v0, 0x3f400000    # 0.75f

    invoke-static {v0}, Lho;->a(F)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->f:F

    .line 91
    new-instance v0, Lgw;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/RadarChart;->S:Lew;

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/RadarChart;->R:Lhp;

    invoke-direct {v0, p0, v1, v2}, Lgw;-><init>(Lcom/github/mikephil/charting/charts/RadarChart;Lew;Lhp;)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->P:Lgp;

    .line 92
    new-instance v0, Lhe;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/RadarChart;->R:Lhp;

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/RadarChart;->l:Lcom/github/mikephil/charting/components/YAxis;

    invoke-direct {v0, v1, v2, p0}, Lhe;-><init>(Lhp;Lcom/github/mikephil/charting/components/YAxis;Lcom/github/mikephil/charting/charts/RadarChart;)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->a:Lhe;

    .line 93
    new-instance v0, Lhb;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/RadarChart;->R:Lhp;

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/RadarChart;->I:Lcom/github/mikephil/charting/components/XAxis;

    invoke-direct {v0, v1, v2, p0}, Lhb;-><init>(Lhp;Lcom/github/mikephil/charting/components/XAxis;Lcom/github/mikephil/charting/charts/RadarChart;)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->b:Lhb;

    .line 95
    new-instance v0, Lfo;

    invoke-direct {v0, p0}, Lfo;-><init>(Lcom/github/mikephil/charting/charts/RadarChart;)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->Q:Lfl;

    .line 96
    return-void
.end method

.method protected b()V
    .locals 4

    .prologue
    .line 100
    invoke-super {p0}, Lcom/github/mikephil/charting/charts/PieRadarChartBase;->b()V

    .line 102
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/RadarChart;->l:Lcom/github/mikephil/charting/components/YAxis;

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/n;

    sget-object v2, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/data/n;->a(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)F

    move-result v2

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/n;

    sget-object v3, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    invoke-virtual {v0, v3}, Lcom/github/mikephil/charting/data/n;->b(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)F

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/github/mikephil/charting/components/YAxis;->a(FF)V

    .line 103
    iget-object v1, p0, Lcom/github/mikephil/charting/charts/RadarChart;->I:Lcom/github/mikephil/charting/components/XAxis;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/n;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/n;->k()Lgc;

    move-result-object v0

    check-cast v0, Lgh;

    invoke-interface {v0}, Lgh;->s()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v2, v0}, Lcom/github/mikephil/charting/components/XAxis;->a(FF)V

    .line 104
    return-void
.end method

.method public getFactor()F
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 164
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->R:Lhp;

    invoke-virtual {v0}, Lhp;->k()Landroid/graphics/RectF;

    move-result-object v0

    .line 165
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float/2addr v1, v2

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    div-float/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/RadarChart;->l:Lcom/github/mikephil/charting/components/YAxis;

    iget v1, v1, Lcom/github/mikephil/charting/components/YAxis;->u:F

    div-float/2addr v0, v1

    return v0
.end method

.method public getRadius()F
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 331
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->R:Lhp;

    invoke-virtual {v0}, Lhp;->k()Landroid/graphics/RectF;

    move-result-object v0

    .line 332
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float/2addr v1, v2

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    div-float/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method protected getRequiredBaseOffset()F
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->I:Lcom/github/mikephil/charting/components/XAxis;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/components/XAxis;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->I:Lcom/github/mikephil/charting/components/XAxis;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/components/XAxis;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v0, v0, Lcom/github/mikephil/charting/components/XAxis;->D:I

    int-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x41200000    # 10.0f

    .line 326
    invoke-static {v0}, Lho;->a(F)F

    move-result v0

    goto :goto_0
.end method

.method protected getRequiredLegendOffset()F
    .locals 2

    .prologue
    .line 319
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->O:Lgr;

    invoke-virtual {v0}, Lgr;->a()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Paint;->getTextSize()F

    move-result v0

    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v0, v1

    return v0
.end method

.method public getSkipWebLineCount()I
    .locals 1

    .prologue
    .line 314
    iget v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->k:I

    return v0
.end method

.method public getSliceAngle()F
    .locals 2

    .prologue
    .line 174
    const/high16 v1, 0x43b40000    # 360.0f

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/n;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/n;->k()Lgc;

    move-result-object v0

    check-cast v0, Lgh;

    invoke-interface {v0}, Lgh;->s()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v1, v0

    return v0
.end method

.method public getWebAlpha()I
    .locals 1

    .prologue
    .line 254
    iget v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->i:I

    return v0
.end method

.method public getWebColor()I
    .locals 1

    .prologue
    .line 269
    iget v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->g:I

    return v0
.end method

.method public getWebColorInner()I
    .locals 1

    .prologue
    .line 284
    iget v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->h:I

    return v0
.end method

.method public getWebLineWidth()F
    .locals 1

    .prologue
    .line 221
    iget v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->e:F

    return v0
.end method

.method public getWebLineWidthInner()F
    .locals 1

    .prologue
    .line 235
    iget v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->f:F

    return v0
.end method

.method public getYAxis()Lcom/github/mikephil/charting/components/YAxis;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->l:Lcom/github/mikephil/charting/components/YAxis;

    return-object v0
.end method

.method public getYChartMax()F
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->l:Lcom/github/mikephil/charting/components/YAxis;

    iget v0, v0, Lcom/github/mikephil/charting/components/YAxis;->s:F

    return v0
.end method

.method public getYChartMin()F
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->l:Lcom/github/mikephil/charting/components/YAxis;

    iget v0, v0, Lcom/github/mikephil/charting/components/YAxis;->t:F

    return v0
.end method

.method public getYRange()F
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->l:Lcom/github/mikephil/charting/components/YAxis;

    iget v0, v0, Lcom/github/mikephil/charting/components/YAxis;->u:F

    return v0
.end method

.method public h()V
    .locals 4

    .prologue
    .line 108
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->D:Lcom/github/mikephil/charting/data/h;

    if-nez v0, :cond_0

    .line 120
    :goto_0
    return-void

    .line 111
    :cond_0
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/RadarChart;->b()V

    .line 113
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->a:Lhe;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/RadarChart;->l:Lcom/github/mikephil/charting/components/YAxis;

    iget v1, v1, Lcom/github/mikephil/charting/components/YAxis;->t:F

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/RadarChart;->l:Lcom/github/mikephil/charting/components/YAxis;

    iget v2, v2, Lcom/github/mikephil/charting/components/YAxis;->s:F

    iget-object v3, p0, Lcom/github/mikephil/charting/charts/RadarChart;->l:Lcom/github/mikephil/charting/components/YAxis;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/components/YAxis;->D()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lhe;->a(FFZ)V

    .line 114
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->b:Lhb;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/RadarChart;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v1, v1, Lcom/github/mikephil/charting/components/XAxis;->t:F

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/RadarChart;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v2, v2, Lcom/github/mikephil/charting/components/XAxis;->s:F

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lhb;->a(FFZ)V

    .line 116
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->L:Lcom/github/mikephil/charting/components/Legend;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->L:Lcom/github/mikephil/charting/components/Legend;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/components/Legend;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->O:Lgr;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/RadarChart;->D:Lcom/github/mikephil/charting/data/h;

    invoke-virtual {v0, v1}, Lgr;->a(Lcom/github/mikephil/charting/data/h;)V

    .line 119
    :cond_1
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/RadarChart;->j()V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 124
    invoke-super {p0, p1}, Lcom/github/mikephil/charting/charts/PieRadarChartBase;->onDraw(Landroid/graphics/Canvas;)V

    .line 126
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->D:Lcom/github/mikephil/charting/data/h;

    if-nez v0, :cond_0

    .line 156
    :goto_0
    return-void

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->I:Lcom/github/mikephil/charting/components/XAxis;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/components/XAxis;->x()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 133
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->b:Lhb;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/RadarChart;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v1, v1, Lcom/github/mikephil/charting/components/XAxis;->t:F

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/RadarChart;->I:Lcom/github/mikephil/charting/components/XAxis;

    iget v2, v2, Lcom/github/mikephil/charting/components/XAxis;->s:F

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lhb;->a(FFZ)V

    .line 135
    :cond_1
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->b:Lhb;

    invoke-virtual {v0, p1}, Lhb;->a(Landroid/graphics/Canvas;)V

    .line 137
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->j:Z

    if-eqz v0, :cond_2

    .line 138
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->P:Lgp;

    invoke-virtual {v0, p1}, Lgp;->c(Landroid/graphics/Canvas;)V

    .line 140
    :cond_2
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->a:Lhe;

    invoke-virtual {v0, p1}, Lhe;->e(Landroid/graphics/Canvas;)V

    .line 142
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->P:Lgp;

    invoke-virtual {v0, p1}, Lgp;->a(Landroid/graphics/Canvas;)V

    .line 144
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/RadarChart;->u()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 145
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->P:Lgp;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/RadarChart;->T:[Lfj;

    invoke-virtual {v0, p1, v1}, Lgp;->a(Landroid/graphics/Canvas;[Lfj;)V

    .line 147
    :cond_3
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->a:Lhe;

    invoke-virtual {v0, p1}, Lhe;->a(Landroid/graphics/Canvas;)V

    .line 149
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->P:Lgp;

    invoke-virtual {v0, p1}, Lgp;->b(Landroid/graphics/Canvas;)V

    .line 151
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->O:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Landroid/graphics/Canvas;)V

    .line 153
    invoke-virtual {p0, p1}, Lcom/github/mikephil/charting/charts/RadarChart;->b(Landroid/graphics/Canvas;)V

    .line 155
    invoke-virtual {p0, p1}, Lcom/github/mikephil/charting/charts/RadarChart;->c(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public setDrawWeb(Z)V
    .locals 0

    .prologue
    .line 294
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/RadarChart;->j:Z

    .line 295
    return-void
.end method

.method public setSkipWebLineCount(I)V
    .locals 1

    .prologue
    .line 305
    const/4 v0, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->k:I

    .line 306
    return-void
.end method

.method public setWebAlpha(I)V
    .locals 0

    .prologue
    .line 245
    iput p1, p0, Lcom/github/mikephil/charting/charts/RadarChart;->i:I

    .line 246
    return-void
.end method

.method public setWebColor(I)V
    .locals 0

    .prologue
    .line 265
    iput p1, p0, Lcom/github/mikephil/charting/charts/RadarChart;->g:I

    .line 266
    return-void
.end method

.method public setWebColorInner(I)V
    .locals 0

    .prologue
    .line 280
    iput p1, p0, Lcom/github/mikephil/charting/charts/RadarChart;->h:I

    .line 281
    return-void
.end method

.method public setWebLineWidth(F)V
    .locals 1

    .prologue
    .line 217
    invoke-static {p1}, Lho;->a(F)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->e:F

    .line 218
    return-void
.end method

.method public setWebLineWidthInner(F)V
    .locals 1

    .prologue
    .line 231
    invoke-static {p1}, Lho;->a(F)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/charts/RadarChart;->f:F

    .line 232
    return-void
.end method
