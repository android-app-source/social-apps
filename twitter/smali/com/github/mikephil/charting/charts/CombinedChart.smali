.class public Lcom/github/mikephil/charting/charts/CombinedChart;
.super Lcom/github/mikephil/charting/charts/BarLineChartBase;
.source "Twttr"

# interfaces
.implements Lfv;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/github/mikephil/charting/charts/CombinedChart$DrawOrder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/github/mikephil/charting/charts/BarLineChartBase",
        "<",
        "Lcom/github/mikephil/charting/data/i;",
        ">;",
        "Lfv;"
    }
.end annotation


# instance fields
.field protected a:Z

.field protected ab:[Lcom/github/mikephil/charting/charts/CombinedChart$DrawOrder;

.field private ac:Z

.field private ad:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0, p1}, Lcom/github/mikephil/charting/charts/BarLineChartBase;-><init>(Landroid/content/Context;)V

    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->ac:Z

    .line 37
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->a:Z

    .line 43
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->ad:Z

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/github/mikephil/charting/charts/BarLineChartBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->ac:Z

    .line 37
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->a:Z

    .line 43
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->ad:Z

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/github/mikephil/charting/charts/BarLineChartBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->ac:Z

    .line 37
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->a:Z

    .line 43
    iput-boolean v1, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->ad:Z

    .line 65
    return-void
.end method


# virtual methods
.method public a(FF)Lfj;
    .locals 8

    .prologue
    .line 109
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->D:Lcom/github/mikephil/charting/data/h;

    if-nez v0, :cond_0

    .line 110
    const-string/jumbo v0, "MPAndroidChart"

    const-string/jumbo v1, "Can\'t select by touch. No data set."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    const/4 v0, 0x0

    .line 117
    :goto_0
    return-object v0

    .line 113
    :cond_0
    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/CombinedChart;->getHighlighter()Lfl;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lfl;->a(FF)Lfj;

    move-result-object v7

    .line 114
    if-eqz v7, :cond_1

    invoke-virtual {p0}, Lcom/github/mikephil/charting/charts/CombinedChart;->e()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move-object v0, v7

    goto :goto_0

    .line 117
    :cond_2
    new-instance v0, Lfj;

    invoke-virtual {v7}, Lfj;->a()F

    move-result v1

    invoke-virtual {v7}, Lfj;->b()F

    move-result v2

    .line 118
    invoke-virtual {v7}, Lfj;->c()F

    move-result v3

    invoke-virtual {v7}, Lfj;->d()F

    move-result v4

    .line 119
    invoke-virtual {v7}, Lfj;->f()I

    move-result v5

    const/4 v6, -0x1

    invoke-virtual {v7}, Lfj;->h()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lfj;-><init>(FFFFIILcom/github/mikephil/charting/components/YAxis$AxisDependency;)V

    goto :goto_0
.end method

.method protected a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 69
    invoke-super {p0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->a()V

    .line 72
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/github/mikephil/charting/charts/CombinedChart$DrawOrder;

    const/4 v1, 0x0

    sget-object v2, Lcom/github/mikephil/charting/charts/CombinedChart$DrawOrder;->a:Lcom/github/mikephil/charting/charts/CombinedChart$DrawOrder;

    aput-object v2, v0, v1

    sget-object v1, Lcom/github/mikephil/charting/charts/CombinedChart$DrawOrder;->b:Lcom/github/mikephil/charting/charts/CombinedChart$DrawOrder;

    aput-object v1, v0, v3

    const/4 v1, 0x2

    sget-object v2, Lcom/github/mikephil/charting/charts/CombinedChart$DrawOrder;->c:Lcom/github/mikephil/charting/charts/CombinedChart$DrawOrder;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/github/mikephil/charting/charts/CombinedChart$DrawOrder;->d:Lcom/github/mikephil/charting/charts/CombinedChart$DrawOrder;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/github/mikephil/charting/charts/CombinedChart$DrawOrder;->e:Lcom/github/mikephil/charting/charts/CombinedChart$DrawOrder;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->ab:[Lcom/github/mikephil/charting/charts/CombinedChart$DrawOrder;

    .line 76
    new-instance v0, Lfi;

    invoke-direct {v0, p0, p0}, Lfi;-><init>(Lfv;Lfq;)V

    invoke-virtual {p0, v0}, Lcom/github/mikephil/charting/charts/CombinedChart;->setHighlighter(Lfh;)V

    .line 79
    invoke-virtual {p0, v3}, Lcom/github/mikephil/charting/charts/CombinedChart;->setHighlightFullBarEnabled(Z)V

    .line 81
    new-instance v0, Lgo;

    iget-object v1, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->S:Lew;

    iget-object v2, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->R:Lhp;

    invoke-direct {v0, p0, v1, v2}, Lgo;-><init>(Lcom/github/mikephil/charting/charts/CombinedChart;Lew;Lhp;)V

    iput-object v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->P:Lgp;

    .line 82
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 165
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->ac:Z

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->ad:Z

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 204
    iget-boolean v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->a:Z

    return v0
.end method

.method public getBarData()Lcom/github/mikephil/charting/data/a;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->D:Lcom/github/mikephil/charting/data/h;

    if-nez v0, :cond_0

    .line 133
    const/4 v0, 0x0

    .line 134
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/i;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/i;->m()Lcom/github/mikephil/charting/data/a;

    move-result-object v0

    goto :goto_0
.end method

.method public getBubbleData()Lcom/github/mikephil/charting/data/f;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->D:Lcom/github/mikephil/charting/data/h;

    if-nez v0, :cond_0

    .line 154
    const/4 v0, 0x0

    .line 155
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/i;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/i;->a()Lcom/github/mikephil/charting/data/f;

    move-result-object v0

    goto :goto_0
.end method

.method public getCandleData()Lcom/github/mikephil/charting/data/g;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->D:Lcom/github/mikephil/charting/data/h;

    if-nez v0, :cond_0

    .line 147
    const/4 v0, 0x0

    .line 148
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/i;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/i;->o()Lcom/github/mikephil/charting/data/g;

    move-result-object v0

    goto :goto_0
.end method

.method public getCombinedData()Lcom/github/mikephil/charting/data/i;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/i;

    return-object v0
.end method

.method public getDrawOrder()[Lcom/github/mikephil/charting/charts/CombinedChart$DrawOrder;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->ab:[Lcom/github/mikephil/charting/charts/CombinedChart$DrawOrder;

    return-object v0
.end method

.method public getLineData()Lcom/github/mikephil/charting/data/j;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->D:Lcom/github/mikephil/charting/data/h;

    if-nez v0, :cond_0

    .line 126
    const/4 v0, 0x0

    .line 127
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/i;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/i;->l()Lcom/github/mikephil/charting/data/j;

    move-result-object v0

    goto :goto_0
.end method

.method public getScatterData()Lcom/github/mikephil/charting/data/o;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->D:Lcom/github/mikephil/charting/data/h;

    if-nez v0, :cond_0

    .line 140
    const/4 v0, 0x0

    .line 141
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->D:Lcom/github/mikephil/charting/data/h;

    check-cast v0, Lcom/github/mikephil/charting/data/i;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/i;->n()Lcom/github/mikephil/charting/data/o;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic setData(Lcom/github/mikephil/charting/data/h;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Lcom/github/mikephil/charting/data/i;

    invoke-virtual {p0, p1}, Lcom/github/mikephil/charting/charts/CombinedChart;->setData(Lcom/github/mikephil/charting/data/i;)V

    return-void
.end method

.method public setData(Lcom/github/mikephil/charting/data/i;)V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0, p1}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->setData(Lcom/github/mikephil/charting/data/h;)V

    .line 92
    new-instance v0, Lfi;

    invoke-direct {v0, p0, p0}, Lfi;-><init>(Lfv;Lfq;)V

    invoke-virtual {p0, v0}, Lcom/github/mikephil/charting/charts/CombinedChart;->setHighlighter(Lfh;)V

    .line 93
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->P:Lgp;

    check-cast v0, Lgo;

    invoke-virtual {v0}, Lgo;->b()V

    .line 94
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->P:Lgp;

    invoke-virtual {v0}, Lgp;->a()V

    .line 95
    return-void
.end method

.method public setDrawBarShadow(Z)V
    .locals 0

    .prologue
    .line 186
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->ad:Z

    .line 187
    return-void
.end method

.method public setDrawOrder([Lcom/github/mikephil/charting/charts/CombinedChart$DrawOrder;)V
    .locals 1

    .prologue
    .line 225
    if-eqz p1, :cond_0

    array-length v0, p1

    if-gtz v0, :cond_1

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 227
    :cond_1
    iput-object p1, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->ab:[Lcom/github/mikephil/charting/charts/CombinedChart$DrawOrder;

    goto :goto_0
.end method

.method public setDrawValueAboveBar(Z)V
    .locals 0

    .prologue
    .line 175
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->ac:Z

    .line 176
    return-void
.end method

.method public setHighlightFullBarEnabled(Z)V
    .locals 0

    .prologue
    .line 196
    iput-boolean p1, p0, Lcom/github/mikephil/charting/charts/CombinedChart;->a:Z

    .line 197
    return-void
.end method
