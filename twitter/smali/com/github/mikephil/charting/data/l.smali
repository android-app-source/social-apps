.class public abstract Lcom/github/mikephil/charting/data/l;
.super Lcom/github/mikephil/charting/data/c;
.source "Twttr"

# interfaces
.implements Lgf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/github/mikephil/charting/data/Entry;",
        ">",
        "Lcom/github/mikephil/charting/data/c",
        "<TT;>;",
        "Lgf",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected q:Z

.field protected r:Z

.field protected s:F

.field protected t:Landroid/graphics/DashPathEffect;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/high16 v1, 0x3f000000    # 0.5f

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/github/mikephil/charting/data/c;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 15
    iput-boolean v0, p0, Lcom/github/mikephil/charting/data/l;->q:Z

    .line 16
    iput-boolean v0, p0, Lcom/github/mikephil/charting/data/l;->r:Z

    .line 19
    iput v1, p0, Lcom/github/mikephil/charting/data/l;->s:F

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/github/mikephil/charting/data/l;->t:Landroid/graphics/DashPathEffect;

    .line 27
    invoke-static {v1}, Lho;->a(F)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/l;->s:F

    .line 28
    return-void
.end method


# virtual methods
.method public Q()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/github/mikephil/charting/data/l;->q:Z

    return v0
.end method

.method public R()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/github/mikephil/charting/data/l;->r:Z

    return v0
.end method

.method public S()F
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/github/mikephil/charting/data/l;->s:F

    return v0
.end method

.method public T()Landroid/graphics/DashPathEffect;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/github/mikephil/charting/data/l;->t:Landroid/graphics/DashPathEffect;

    return-object v0
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/github/mikephil/charting/data/l;->r:Z

    .line 36
    return-void
.end method

.method public e(Z)V
    .locals 0

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/github/mikephil/charting/data/l;->q:Z

    .line 44
    return-void
.end method
