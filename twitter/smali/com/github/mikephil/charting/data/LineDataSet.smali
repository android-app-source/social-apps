.class public Lcom/github/mikephil/charting/data/LineDataSet;
.super Lcom/github/mikephil/charting/data/k;
.source "Twttr"

# interfaces
.implements Lgd;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/github/mikephil/charting/data/LineDataSet$Mode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/github/mikephil/charting/data/k",
        "<",
        "Lcom/github/mikephil/charting/data/Entry;",
        ">;",
        "Lgd;"
    }
.end annotation


# instance fields
.field private A:Landroid/graphics/DashPathEffect;

.field private B:Lfe;

.field private C:Z

.field private D:Z

.field private u:Lcom/github/mikephil/charting/data/LineDataSet$Mode;

.field private v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private w:I

.field private x:F

.field private y:F

.field private z:F


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/github/mikephil/charting/data/Entry;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 69
    invoke-direct {p0, p1, p2}, Lcom/github/mikephil/charting/data/k;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 23
    sget-object v0, Lcom/github/mikephil/charting/data/LineDataSet$Mode;->a:Lcom/github/mikephil/charting/data/LineDataSet$Mode;

    iput-object v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->u:Lcom/github/mikephil/charting/data/LineDataSet$Mode;

    .line 28
    iput-object v2, p0, Lcom/github/mikephil/charting/data/LineDataSet;->v:Ljava/util/List;

    .line 33
    const/4 v0, -0x1

    iput v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->w:I

    .line 38
    const/high16 v0, 0x41000000    # 8.0f

    iput v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->x:F

    .line 43
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->y:F

    .line 48
    const v0, 0x3e4ccccd    # 0.2f

    iput v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->z:F

    .line 53
    iput-object v2, p0, Lcom/github/mikephil/charting/data/LineDataSet;->A:Landroid/graphics/DashPathEffect;

    .line 58
    new-instance v0, Lfb;

    invoke-direct {v0}, Lfb;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->B:Lfe;

    .line 63
    iput-boolean v1, p0, Lcom/github/mikephil/charting/data/LineDataSet;->C:Z

    .line 65
    iput-boolean v1, p0, Lcom/github/mikephil/charting/data/LineDataSet;->D:Z

    .line 74
    iget-object v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->v:Ljava/util/List;

    if-nez v0, :cond_0

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->v:Ljava/util/List;

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 82
    iget-object v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->v:Ljava/util/List;

    const/16 v1, 0x8c

    const/16 v2, 0xea

    const/16 v3, 0xff

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    return-void
.end method


# virtual methods
.method public A()F
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->z:F

    return v0
.end method

.method public B()F
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->x:F

    return v0
.end method

.method public C()F
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->y:F

    return v0
.end method

.method public D()Z
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->A:Landroid/graphics/DashPathEffect;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public E()Landroid/graphics/DashPathEffect;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->A:Landroid/graphics/DashPathEffect;

    return-object v0
.end method

.method public F()Z
    .locals 1

    .prologue
    .line 254
    iget-boolean v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->C:Z

    return v0
.end method

.method public G()Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 266
    iget-object v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->u:Lcom/github/mikephil/charting/data/LineDataSet$Mode;

    sget-object v1, Lcom/github/mikephil/charting/data/LineDataSet$Mode;->b:Lcom/github/mikephil/charting/data/LineDataSet$Mode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public H()I
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public I()I
    .locals 1

    .prologue
    .line 373
    iget v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->w:I

    return v0
.end method

.method public J()Z
    .locals 1

    .prologue
    .line 387
    iget-boolean v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->D:Z

    return v0
.end method

.method public K()Lfe;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->B:Lfe;

    return-object v0
.end method

.method public a(Lcom/github/mikephil/charting/data/LineDataSet$Mode;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/github/mikephil/charting/data/LineDataSet;->u:Lcom/github/mikephil/charting/data/LineDataSet$Mode;

    .line 125
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 249
    iput-boolean p1, p0, Lcom/github/mikephil/charting/data/LineDataSet;->C:Z

    .line 250
    return-void
.end method

.method public g(I)I
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->v:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public z()Lcom/github/mikephil/charting/data/LineDataSet$Mode;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/github/mikephil/charting/data/LineDataSet;->u:Lcom/github/mikephil/charting/data/LineDataSet$Mode;

    return-object v0
.end method
