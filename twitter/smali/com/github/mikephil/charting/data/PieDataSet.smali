.class public Lcom/github/mikephil/charting/data/PieDataSet;
.super Lcom/github/mikephil/charting/data/DataSet;
.source "Twttr"

# interfaces
.implements Lgg;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/github/mikephil/charting/data/PieDataSet$ValuePosition;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/github/mikephil/charting/data/DataSet",
        "<",
        "Lcom/github/mikephil/charting/data/PieEntry;",
        ">;",
        "Lgg;"
    }
.end annotation


# instance fields
.field private a:F

.field private p:F

.field private q:Lcom/github/mikephil/charting/data/PieDataSet$ValuePosition;

.field private r:Lcom/github/mikephil/charting/data/PieDataSet$ValuePosition;

.field private s:I

.field private t:F

.field private u:F

.field private v:F

.field private w:F

.field private x:Z


# virtual methods
.method public A()Lcom/github/mikephil/charting/data/PieDataSet$ValuePosition;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/github/mikephil/charting/data/PieDataSet;->q:Lcom/github/mikephil/charting/data/PieDataSet$ValuePosition;

    return-object v0
.end method

.method public B()Lcom/github/mikephil/charting/data/PieDataSet$ValuePosition;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/github/mikephil/charting/data/PieDataSet;->r:Lcom/github/mikephil/charting/data/PieDataSet$ValuePosition;

    return-object v0
.end method

.method public C()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/github/mikephil/charting/data/PieDataSet;->s:I

    return v0
.end method

.method public D()F
    .locals 1

    .prologue
    .line 131
    iget v0, p0, Lcom/github/mikephil/charting/data/PieDataSet;->t:F

    return v0
.end method

.method public E()F
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/github/mikephil/charting/data/PieDataSet;->u:F

    return v0
.end method

.method public F()F
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/github/mikephil/charting/data/PieDataSet;->v:F

    return v0
.end method

.method public G()F
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/github/mikephil/charting/data/PieDataSet;->w:F

    return v0
.end method

.method public H()Z
    .locals 1

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/github/mikephil/charting/data/PieDataSet;->x:Z

    return v0
.end method

.method public a()F
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/github/mikephil/charting/data/PieDataSet;->a:F

    return v0
.end method

.method protected bridge synthetic a(Lcom/github/mikephil/charting/data/Entry;)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, Lcom/github/mikephil/charting/data/PieEntry;

    invoke-virtual {p0, p1}, Lcom/github/mikephil/charting/data/PieDataSet;->a(Lcom/github/mikephil/charting/data/PieEntry;)V

    return-void
.end method

.method protected a(Lcom/github/mikephil/charting/data/PieEntry;)V
    .locals 0

    .prologue
    .line 51
    if-nez p1, :cond_0

    .line 55
    :goto_0
    return-void

    .line 54
    :cond_0
    invoke-virtual {p0, p1}, Lcom/github/mikephil/charting/data/PieDataSet;->c(Lcom/github/mikephil/charting/data/Entry;)V

    goto :goto_0
.end method

.method public z()F
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/github/mikephil/charting/data/PieDataSet;->p:F

    return v0
.end method
