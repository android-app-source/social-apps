.class public abstract Lcom/github/mikephil/charting/data/h;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lgc",
        "<+",
        "Lcom/github/mikephil/charting/data/Entry;",
        ">;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected a:F

.field protected b:F

.field protected c:F

.field protected d:F

.field protected e:F

.field protected f:F

.field protected g:F

.field protected h:F

.field protected i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    const v0, -0x800001

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput v0, p0, Lcom/github/mikephil/charting/data/h;->a:F

    .line 31
    iput v1, p0, Lcom/github/mikephil/charting/data/h;->b:F

    .line 36
    iput v0, p0, Lcom/github/mikephil/charting/data/h;->c:F

    .line 41
    iput v1, p0, Lcom/github/mikephil/charting/data/h;->d:F

    .line 44
    iput v0, p0, Lcom/github/mikephil/charting/data/h;->e:F

    .line 46
    iput v1, p0, Lcom/github/mikephil/charting/data/h;->f:F

    .line 48
    iput v0, p0, Lcom/github/mikephil/charting/data/h;->g:F

    .line 50
    iput v1, p0, Lcom/github/mikephil/charting/data/h;->h:F

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    .line 62
    return-void
.end method

.method public varargs constructor <init>([Lgc;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)V"
        }
    .end annotation

    .prologue
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    const v0, -0x800001

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput v0, p0, Lcom/github/mikephil/charting/data/h;->a:F

    .line 31
    iput v1, p0, Lcom/github/mikephil/charting/data/h;->b:F

    .line 36
    iput v0, p0, Lcom/github/mikephil/charting/data/h;->c:F

    .line 41
    iput v1, p0, Lcom/github/mikephil/charting/data/h;->d:F

    .line 44
    iput v0, p0, Lcom/github/mikephil/charting/data/h;->e:F

    .line 46
    iput v1, p0, Lcom/github/mikephil/charting/data/h;->f:F

    .line 48
    iput v0, p0, Lcom/github/mikephil/charting/data/h;->g:F

    .line 50
    iput v1, p0, Lcom/github/mikephil/charting/data/h;->h:F

    .line 70
    invoke-direct {p0, p1}, Lcom/github/mikephil/charting/data/h;->a([Lgc;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    .line 71
    invoke-virtual {p0}, Lcom/github/mikephil/charting/data/h;->b()V

    .line 72
    return-void
.end method

.method private a([Lgc;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 82
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 84
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    .line 85
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 88
    :cond_0
    return-object v1
.end method


# virtual methods
.method public a(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)F
    .locals 2

    .prologue
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    .line 217
    sget-object v0, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    if-ne p1, v0, :cond_1

    .line 219
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->f:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 220
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->h:F

    .line 227
    :goto_0
    return v0

    .line 222
    :cond_0
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->f:F

    goto :goto_0

    .line 224
    :cond_1
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->h:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 225
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->f:F

    goto :goto_0

    .line 227
    :cond_2
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->h:F

    goto :goto_0
.end method

.method public a(Lfj;)Lcom/github/mikephil/charting/data/Entry;
    .locals 3

    .prologue
    .line 338
    invoke-virtual {p1}, Lfj;->f()I

    move-result v0

    iget-object v1, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 339
    const/4 v0, 0x0

    .line 341
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    invoke-virtual {p1}, Lfj;->f()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgc;

    invoke-virtual {p1}, Lfj;->a()F

    move-result v1

    invoke-virtual {p1}, Lfj;->b()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lgc;->b(FF)Lcom/github/mikephil/charting/data/Entry;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)Lgc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 366
    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 367
    :cond_0
    const/4 v0, 0x0

    .line 369
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgc;

    goto :goto_0
.end method

.method protected a(Ljava/util/List;)Lgc;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 636
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgc;

    .line 637
    invoke-interface {v0}, Lgc;->q()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v2

    sget-object v3, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    if-ne v2, v3, :cond_0

    .line 640
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(FF)V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgc;

    .line 120
    invoke-interface {v0, p1, p2}, Lgc;->a(FF)V

    goto :goto_0

    .line 124
    :cond_0
    invoke-virtual {p0}, Lcom/github/mikephil/charting/data/h;->c()V

    .line 125
    return-void
.end method

.method public a(Lcom/github/mikephil/charting/data/Entry;I)V
    .locals 2

    .prologue
    .line 434
    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p2, :cond_1

    if-ltz p2, :cond_1

    .line 436
    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgc;

    .line 438
    invoke-interface {v0, p1}, Lgc;->d(Lcom/github/mikephil/charting/data/Entry;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 446
    :goto_0
    return-void

    .line 441
    :cond_0
    invoke-interface {v0}, Lgc;->q()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/github/mikephil/charting/data/h;->a(Lcom/github/mikephil/charting/data/Entry;Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)V

    goto :goto_0

    .line 444
    :cond_1
    const-string/jumbo v0, "addEntry"

    const-string/jumbo v1, "Cannot add Entry because dataSetIndex too high or too low."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected a(Lcom/github/mikephil/charting/data/Entry;Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)V
    .locals 2

    .prologue
    .line 456
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->a:F

    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->b()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 457
    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->b()F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/h;->a:F

    .line 458
    :cond_0
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->b:F

    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->b()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 459
    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->b()F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/h;->b:F

    .line 461
    :cond_1
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->c:F

    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->h()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 462
    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->h()F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/h;->c:F

    .line 463
    :cond_2
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->d:F

    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->h()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 464
    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->h()F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/h;->d:F

    .line 466
    :cond_3
    sget-object v0, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    if-ne p2, v0, :cond_6

    .line 468
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->e:F

    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->b()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    .line 469
    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->b()F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/h;->e:F

    .line 470
    :cond_4
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->f:F

    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->b()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    .line 471
    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->b()F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/h;->f:F

    .line 478
    :cond_5
    :goto_0
    return-void

    .line 473
    :cond_6
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->g:F

    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->b()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_7

    .line 474
    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->b()F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/h;->g:F

    .line 475
    :cond_7
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->h:F

    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->b()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    .line 476
    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/Entry;->b()F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/h;->h:F

    goto :goto_0
.end method

.method public a(Lgc;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 379
    if-nez p1, :cond_0

    .line 385
    :goto_0
    return-void

    .line 382
    :cond_0
    invoke-virtual {p0, p1}, Lcom/github/mikephil/charting/data/h;->b(Lgc;)V

    .line 384
    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public b(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)F
    .locals 2

    .prologue
    const v1, -0x800001

    .line 247
    sget-object v0, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    if-ne p1, v0, :cond_1

    .line 249
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->e:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 250
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->g:F

    .line 257
    :goto_0
    return v0

    .line 252
    :cond_0
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->e:F

    goto :goto_0

    .line 254
    :cond_1
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->g:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 255
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->e:F

    goto :goto_0

    .line 257
    :cond_2
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->g:F

    goto :goto_0
.end method

.method public b(Ljava/util/List;)Lgc;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 650
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgc;

    .line 651
    invoke-interface {v0}, Lgc;->q()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v2

    sget-object v3, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->b:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    if-ne v2, v3, :cond_0

    .line 654
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/github/mikephil/charting/data/h;->c()V

    .line 108
    return-void
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 679
    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgc;

    .line 680
    invoke-interface {v0, p1}, Lgc;->d(I)V

    goto :goto_0

    .line 682
    :cond_0
    return-void
.end method

.method protected b(Lgc;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 487
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->a:F

    invoke-interface {p1}, Lgc;->w()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 488
    invoke-interface {p1}, Lgc;->w()F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/h;->a:F

    .line 489
    :cond_0
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->b:F

    invoke-interface {p1}, Lgc;->v()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 490
    invoke-interface {p1}, Lgc;->v()F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/h;->b:F

    .line 492
    :cond_1
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->c:F

    invoke-interface {p1}, Lgc;->y()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 493
    invoke-interface {p1}, Lgc;->y()F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/h;->c:F

    .line 494
    :cond_2
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->d:F

    invoke-interface {p1}, Lgc;->x()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 495
    invoke-interface {p1}, Lgc;->x()F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/h;->d:F

    .line 497
    :cond_3
    invoke-interface {p1}, Lgc;->q()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v0

    sget-object v1, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    if-ne v0, v1, :cond_6

    .line 499
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->e:F

    invoke-interface {p1}, Lgc;->w()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    .line 500
    invoke-interface {p1}, Lgc;->w()F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/h;->e:F

    .line 501
    :cond_4
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->f:F

    invoke-interface {p1}, Lgc;->v()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    .line 502
    invoke-interface {p1}, Lgc;->v()F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/h;->f:F

    .line 509
    :cond_5
    :goto_0
    return-void

    .line 504
    :cond_6
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->g:F

    invoke-interface {p1}, Lgc;->w()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_7

    .line 505
    invoke-interface {p1}, Lgc;->w()F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/h;->g:F

    .line 506
    :cond_7
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->h:F

    invoke-interface {p1}, Lgc;->v()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    .line 507
    invoke-interface {p1}, Lgc;->v()F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/h;->h:F

    goto :goto_0
.end method

.method protected c()V
    .locals 4

    .prologue
    const v3, 0x7f7fffff    # Float.MAX_VALUE

    const v2, -0x800001

    .line 132
    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    if-nez v0, :cond_1

    .line 186
    :cond_0
    return-void

    .line 135
    :cond_1
    iput v2, p0, Lcom/github/mikephil/charting/data/h;->a:F

    .line 136
    iput v3, p0, Lcom/github/mikephil/charting/data/h;->b:F

    .line 137
    iput v2, p0, Lcom/github/mikephil/charting/data/h;->c:F

    .line 138
    iput v3, p0, Lcom/github/mikephil/charting/data/h;->d:F

    .line 140
    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgc;

    .line 141
    invoke-virtual {p0, v0}, Lcom/github/mikephil/charting/data/h;->b(Lgc;)V

    goto :goto_0

    .line 144
    :cond_2
    iput v2, p0, Lcom/github/mikephil/charting/data/h;->e:F

    .line 145
    iput v3, p0, Lcom/github/mikephil/charting/data/h;->f:F

    .line 146
    iput v2, p0, Lcom/github/mikephil/charting/data/h;->g:F

    .line 147
    iput v3, p0, Lcom/github/mikephil/charting/data/h;->h:F

    .line 150
    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/github/mikephil/charting/data/h;->a(Ljava/util/List;)Lgc;

    move-result-object v0

    .line 152
    if-eqz v0, :cond_5

    .line 154
    invoke-interface {v0}, Lgc;->w()F

    move-result v1

    iput v1, p0, Lcom/github/mikephil/charting/data/h;->e:F

    .line 155
    invoke-interface {v0}, Lgc;->v()F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/h;->f:F

    .line 157
    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgc;

    .line 158
    invoke-interface {v0}, Lgc;->q()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v2

    sget-object v3, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->a:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    if-ne v2, v3, :cond_3

    .line 159
    invoke-interface {v0}, Lgc;->v()F

    move-result v2

    iget v3, p0, Lcom/github/mikephil/charting/data/h;->f:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_4

    .line 160
    invoke-interface {v0}, Lgc;->v()F

    move-result v2

    iput v2, p0, Lcom/github/mikephil/charting/data/h;->f:F

    .line 162
    :cond_4
    invoke-interface {v0}, Lgc;->w()F

    move-result v2

    iget v3, p0, Lcom/github/mikephil/charting/data/h;->e:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    .line 163
    invoke-interface {v0}, Lgc;->w()F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/h;->e:F

    goto :goto_1

    .line 169
    :cond_5
    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/github/mikephil/charting/data/h;->b(Ljava/util/List;)Lgc;

    move-result-object v0

    .line 171
    if-eqz v0, :cond_0

    .line 173
    invoke-interface {v0}, Lgc;->w()F

    move-result v1

    iput v1, p0, Lcom/github/mikephil/charting/data/h;->g:F

    .line 174
    invoke-interface {v0}, Lgc;->v()F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/h;->h:F

    .line 176
    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgc;

    .line 177
    invoke-interface {v0}, Lgc;->q()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v2

    sget-object v3, Lcom/github/mikephil/charting/components/YAxis$AxisDependency;->b:Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    if-ne v2, v3, :cond_6

    .line 178
    invoke-interface {v0}, Lgc;->v()F

    move-result v2

    iget v3, p0, Lcom/github/mikephil/charting/data/h;->h:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_7

    .line 179
    invoke-interface {v0}, Lgc;->v()F

    move-result v2

    iput v2, p0, Lcom/github/mikephil/charting/data/h;->h:F

    .line 181
    :cond_7
    invoke-interface {v0}, Lgc;->w()F

    move-result v2

    iget v3, p0, Lcom/github/mikephil/charting/data/h;->g:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_6

    .line 182
    invoke-interface {v0}, Lgc;->w()F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/data/h;->g:F

    goto :goto_2
.end method

.method public d()I
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    if-nez v0, :cond_0

    .line 197
    const/4 v0, 0x0

    .line 198
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public e()F
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->b:F

    return v0
.end method

.method public f()F
    .locals 1

    .prologue
    .line 237
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->a:F

    return v0
.end method

.method public g()F
    .locals 1

    .prologue
    .line 267
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->d:F

    return v0
.end method

.method public h()F
    .locals 1

    .prologue
    .line 276
    iget v0, p0, Lcom/github/mikephil/charting/data/h;->c:F

    return v0
.end method

.method public i()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 285
    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    return-object v0
.end method

.method public j()I
    .locals 3

    .prologue
    .line 792
    const/4 v0, 0x0

    .line 794
    iget-object v1, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgc;

    .line 795
    invoke-interface {v0}, Lgc;->s()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 796
    goto :goto_0

    .line 798
    :cond_0
    return v1
.end method

.method public k()Lgc;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 808
    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 809
    :cond_0
    const/4 v1, 0x0

    .line 819
    :cond_1
    return-object v1

    .line 811
    :cond_2
    iget-object v0, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgc;

    .line 813
    iget-object v1, p0, Lcom/github/mikephil/charting/data/h;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgc;

    .line 815
    invoke-interface {v0}, Lgc;->s()I

    move-result v3

    invoke-interface {v1}, Lgc;->s()I

    move-result v4

    if-le v3, v4, :cond_3

    :goto_1
    move-object v1, v0

    .line 817
    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method
