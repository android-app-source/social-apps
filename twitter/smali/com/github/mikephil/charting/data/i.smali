.class public Lcom/github/mikephil/charting/data/i;
.super Lcom/github/mikephil/charting/data/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/github/mikephil/charting/data/b",
        "<",
        "Lfz",
        "<+",
        "Lcom/github/mikephil/charting/data/Entry;",
        ">;>;"
    }
.end annotation


# instance fields
.field private j:Lcom/github/mikephil/charting/data/j;

.field private k:Lcom/github/mikephil/charting/data/a;

.field private l:Lcom/github/mikephil/charting/data/o;

.field private m:Lcom/github/mikephil/charting/data/g;

.field private n:Lcom/github/mikephil/charting/data/f;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/github/mikephil/charting/data/b;-><init>()V

    .line 28
    return-void
.end method


# virtual methods
.method public a(Lfj;)Lcom/github/mikephil/charting/data/Entry;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 180
    invoke-virtual {p0}, Lcom/github/mikephil/charting/data/i;->p()Ljava/util/List;

    move-result-object v0

    .line 182
    invoke-virtual {p1}, Lfj;->e()I

    move-result v2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lt v2, v3, :cond_1

    move-object v0, v1

    .line 200
    :cond_0
    :goto_0
    return-object v0

    .line 185
    :cond_1
    invoke-virtual {p1}, Lfj;->e()I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/github/mikephil/charting/data/h;

    .line 187
    invoke-virtual {p1}, Lfj;->f()I

    move-result v2

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/h;->d()I

    move-result v3

    if-lt v2, v3, :cond_2

    move-object v0, v1

    .line 188
    goto :goto_0

    .line 193
    :cond_2
    invoke-virtual {p1}, Lfj;->f()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/data/h;->a(I)Lgc;

    move-result-object v0

    .line 194
    invoke-virtual {p1}, Lfj;->a()F

    move-result v2

    invoke-interface {v0, v2}, Lgc;->a(F)Ljava/util/List;

    move-result-object v0

    .line 195
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/github/mikephil/charting/data/Entry;

    .line 196
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/Entry;->b()F

    move-result v3

    invoke-virtual {p1}, Lfj;->b()F

    move-result v4

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    .line 197
    invoke-virtual {p1}, Lfj;->b()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 200
    goto :goto_0
.end method

.method public a()Lcom/github/mikephil/charting/data/f;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/github/mikephil/charting/data/i;->n:Lcom/github/mikephil/charting/data/f;

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/github/mikephil/charting/data/i;->j:Lcom/github/mikephil/charting/data/j;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/github/mikephil/charting/data/i;->j:Lcom/github/mikephil/charting/data/j;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/j;->b()V

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/data/i;->k:Lcom/github/mikephil/charting/data/a;

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/github/mikephil/charting/data/i;->k:Lcom/github/mikephil/charting/data/a;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/a;->b()V

    .line 161
    :cond_1
    iget-object v0, p0, Lcom/github/mikephil/charting/data/i;->m:Lcom/github/mikephil/charting/data/g;

    if-eqz v0, :cond_2

    .line 162
    iget-object v0, p0, Lcom/github/mikephil/charting/data/i;->m:Lcom/github/mikephil/charting/data/g;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/g;->b()V

    .line 163
    :cond_2
    iget-object v0, p0, Lcom/github/mikephil/charting/data/i;->l:Lcom/github/mikephil/charting/data/o;

    if-eqz v0, :cond_3

    .line 164
    iget-object v0, p0, Lcom/github/mikephil/charting/data/i;->l:Lcom/github/mikephil/charting/data/o;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/o;->b()V

    .line 165
    :cond_3
    iget-object v0, p0, Lcom/github/mikephil/charting/data/i;->n:Lcom/github/mikephil/charting/data/f;

    if-eqz v0, :cond_4

    .line 166
    iget-object v0, p0, Lcom/github/mikephil/charting/data/i;->n:Lcom/github/mikephil/charting/data/f;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/f;->b()V

    .line 168
    :cond_4
    invoke-virtual {p0}, Lcom/github/mikephil/charting/data/i;->c()V

    .line 169
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    const v2, 0x7f7fffff    # Float.MAX_VALUE

    const v1, -0x800001

    .line 58
    iget-object v0, p0, Lcom/github/mikephil/charting/data/i;->i:Ljava/util/List;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/data/i;->i:Ljava/util/List;

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/data/i;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 63
    iput v1, p0, Lcom/github/mikephil/charting/data/i;->a:F

    .line 64
    iput v2, p0, Lcom/github/mikephil/charting/data/i;->b:F

    .line 65
    iput v1, p0, Lcom/github/mikephil/charting/data/i;->c:F

    .line 66
    iput v2, p0, Lcom/github/mikephil/charting/data/i;->d:F

    .line 68
    iput v1, p0, Lcom/github/mikephil/charting/data/i;->e:F

    .line 69
    iput v2, p0, Lcom/github/mikephil/charting/data/i;->f:F

    .line 70
    iput v1, p0, Lcom/github/mikephil/charting/data/i;->g:F

    .line 71
    iput v2, p0, Lcom/github/mikephil/charting/data/i;->h:F

    .line 73
    invoke-virtual {p0}, Lcom/github/mikephil/charting/data/i;->p()Ljava/util/List;

    move-result-object v0

    .line 75
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/github/mikephil/charting/data/h;

    .line 77
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/h;->c()V

    .line 79
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/h;->i()Ljava/util/List;

    move-result-object v2

    .line 80
    iget-object v3, p0, Lcom/github/mikephil/charting/data/i;->i:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 82
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/h;->f()F

    move-result v2

    iget v3, p0, Lcom/github/mikephil/charting/data/i;->a:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    .line 83
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/h;->f()F

    move-result v2

    iput v2, p0, Lcom/github/mikephil/charting/data/i;->a:F

    .line 85
    :cond_2
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/h;->e()F

    move-result v2

    iget v3, p0, Lcom/github/mikephil/charting/data/i;->b:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3

    .line 86
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/h;->e()F

    move-result v2

    iput v2, p0, Lcom/github/mikephil/charting/data/i;->b:F

    .line 88
    :cond_3
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/h;->h()F

    move-result v2

    iget v3, p0, Lcom/github/mikephil/charting/data/i;->c:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_4

    .line 89
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/h;->h()F

    move-result v2

    iput v2, p0, Lcom/github/mikephil/charting/data/i;->c:F

    .line 91
    :cond_4
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/h;->g()F

    move-result v2

    iget v3, p0, Lcom/github/mikephil/charting/data/i;->d:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_5

    .line 92
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/h;->g()F

    move-result v2

    iput v2, p0, Lcom/github/mikephil/charting/data/i;->d:F

    .line 94
    :cond_5
    iget v2, v0, Lcom/github/mikephil/charting/data/h;->e:F

    iget v3, p0, Lcom/github/mikephil/charting/data/i;->e:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_6

    .line 95
    iget v2, v0, Lcom/github/mikephil/charting/data/h;->e:F

    iput v2, p0, Lcom/github/mikephil/charting/data/i;->e:F

    .line 97
    :cond_6
    iget v2, v0, Lcom/github/mikephil/charting/data/h;->f:F

    iget v3, p0, Lcom/github/mikephil/charting/data/i;->f:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_7

    .line 98
    iget v2, v0, Lcom/github/mikephil/charting/data/h;->f:F

    iput v2, p0, Lcom/github/mikephil/charting/data/i;->f:F

    .line 100
    :cond_7
    iget v2, v0, Lcom/github/mikephil/charting/data/h;->g:F

    iget v3, p0, Lcom/github/mikephil/charting/data/i;->g:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_8

    .line 101
    iget v2, v0, Lcom/github/mikephil/charting/data/h;->g:F

    iput v2, p0, Lcom/github/mikephil/charting/data/i;->g:F

    .line 103
    :cond_8
    iget v2, v0, Lcom/github/mikephil/charting/data/h;->h:F

    iget v3, p0, Lcom/github/mikephil/charting/data/i;->h:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    .line 104
    iget v0, v0, Lcom/github/mikephil/charting/data/h;->h:F

    iput v0, p0, Lcom/github/mikephil/charting/data/i;->h:F

    goto/16 :goto_0

    .line 107
    :cond_9
    return-void
.end method

.method public l()Lcom/github/mikephil/charting/data/j;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/github/mikephil/charting/data/i;->j:Lcom/github/mikephil/charting/data/j;

    return-object v0
.end method

.method public m()Lcom/github/mikephil/charting/data/a;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/github/mikephil/charting/data/i;->k:Lcom/github/mikephil/charting/data/a;

    return-object v0
.end method

.method public n()Lcom/github/mikephil/charting/data/o;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/github/mikephil/charting/data/i;->l:Lcom/github/mikephil/charting/data/o;

    return-object v0
.end method

.method public o()Lcom/github/mikephil/charting/data/g;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/github/mikephil/charting/data/i;->m:Lcom/github/mikephil/charting/data/g;

    return-object v0
.end method

.method public p()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/github/mikephil/charting/data/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 137
    iget-object v1, p0, Lcom/github/mikephil/charting/data/i;->j:Lcom/github/mikephil/charting/data/j;

    if-eqz v1, :cond_0

    .line 138
    iget-object v1, p0, Lcom/github/mikephil/charting/data/i;->j:Lcom/github/mikephil/charting/data/j;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    :cond_0
    iget-object v1, p0, Lcom/github/mikephil/charting/data/i;->k:Lcom/github/mikephil/charting/data/a;

    if-eqz v1, :cond_1

    .line 140
    iget-object v1, p0, Lcom/github/mikephil/charting/data/i;->k:Lcom/github/mikephil/charting/data/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    :cond_1
    iget-object v1, p0, Lcom/github/mikephil/charting/data/i;->l:Lcom/github/mikephil/charting/data/o;

    if-eqz v1, :cond_2

    .line 142
    iget-object v1, p0, Lcom/github/mikephil/charting/data/i;->l:Lcom/github/mikephil/charting/data/o;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    :cond_2
    iget-object v1, p0, Lcom/github/mikephil/charting/data/i;->m:Lcom/github/mikephil/charting/data/g;

    if-eqz v1, :cond_3

    .line 144
    iget-object v1, p0, Lcom/github/mikephil/charting/data/i;->m:Lcom/github/mikephil/charting/data/g;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    :cond_3
    iget-object v1, p0, Lcom/github/mikephil/charting/data/i;->n:Lcom/github/mikephil/charting/data/f;

    if-eqz v1, :cond_4

    .line 146
    iget-object v1, p0, Lcom/github/mikephil/charting/data/i;->n:Lcom/github/mikephil/charting/data/f;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    :cond_4
    return-object v0
.end method
