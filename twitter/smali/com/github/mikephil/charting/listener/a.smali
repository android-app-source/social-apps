.class public Lcom/github/mikephil/charting/listener/a;
.super Lcom/github/mikephil/charting/listener/ChartTouchListener;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/github/mikephil/charting/listener/ChartTouchListener",
        "<",
        "Lcom/github/mikephil/charting/charts/BarLineChartBase",
        "<+",
        "Lcom/github/mikephil/charting/data/b",
        "<+",
        "Lfz",
        "<+",
        "Lcom/github/mikephil/charting/data/Entry;",
        ">;>;>;>;"
    }
.end annotation


# instance fields
.field private f:Landroid/graphics/Matrix;

.field private g:Landroid/graphics/Matrix;

.field private h:Lhk;

.field private i:Lhk;

.field private j:F

.field private k:F

.field private l:F

.field private m:Lgc;

.field private n:Landroid/view/VelocityTracker;

.field private o:J

.field private p:Lhk;

.field private q:Lhk;

.field private r:F

.field private s:F


# direct methods
.method public constructor <init>(Lcom/github/mikephil/charting/charts/BarLineChartBase;Landroid/graphics/Matrix;F)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/github/mikephil/charting/charts/BarLineChartBase",
            "<+",
            "Lcom/github/mikephil/charting/data/b",
            "<+",
            "Lfz",
            "<+",
            "Lcom/github/mikephil/charting/data/Entry;",
            ">;>;>;",
            "Landroid/graphics/Matrix;",
            "F)V"
        }
    .end annotation

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 87
    invoke-direct {p0, p1}, Lcom/github/mikephil/charting/listener/ChartTouchListener;-><init>(Lcom/github/mikephil/charting/charts/Chart;)V

    .line 35
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->f:Landroid/graphics/Matrix;

    .line 40
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->g:Landroid/graphics/Matrix;

    .line 45
    invoke-static {v2, v2}, Lhk;->a(FF)Lhk;

    move-result-object v0

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->h:Lhk;

    .line 50
    invoke-static {v2, v2}, Lhk;->a(FF)Lhk;

    move-result-object v0

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->i:Lhk;

    .line 52
    iput v1, p0, Lcom/github/mikephil/charting/listener/a;->j:F

    .line 53
    iput v1, p0, Lcom/github/mikephil/charting/listener/a;->k:F

    .line 54
    iput v1, p0, Lcom/github/mikephil/charting/listener/a;->l:F

    .line 63
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/github/mikephil/charting/listener/a;->o:J

    .line 64
    invoke-static {v2, v2}, Lhk;->a(FF)Lhk;

    move-result-object v0

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->p:Lhk;

    .line 65
    invoke-static {v2, v2}, Lhk;->a(FF)Lhk;

    move-result-object v0

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->q:Lhk;

    .line 88
    iput-object p2, p0, Lcom/github/mikephil/charting/listener/a;->f:Landroid/graphics/Matrix;

    .line 90
    invoke-static {p3}, Lho;->a(F)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/listener/a;->r:F

    .line 92
    const/high16 v0, 0x40600000    # 3.5f

    invoke-static {v0}, Lho;->a(F)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/listener/a;->s:F

    .line 93
    return-void
.end method

.method private static a(Lhk;Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    .line 452
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    add-float/2addr v0, v1

    .line 453
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    add-float/2addr v1, v2

    .line 454
    div-float/2addr v0, v3

    iput v0, p0, Lhk;->a:F

    .line 455
    div-float v0, v1, v3

    iput v0, p0, Lhk;->b:F

    .line 456
    return-void
.end method

.method private c(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 283
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->g:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/github/mikephil/charting/listener/a;->f:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 284
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->h:Lhk;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, v0, Lhk;->a:F

    .line 285
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->h:Lhk;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, v0, Lhk;->b:F

    .line 287
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->b(FF)Lfz;

    move-result-object v0

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->m:Lgc;

    .line 288
    return-void
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 526
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->m:Lgc;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->s()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->m:Lgc;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    iget-object v1, p0, Lcom/github/mikephil/charting/listener/a;->m:Lgc;

    .line 527
    invoke-interface {v1}, Lgc;->q()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->c(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 526
    :goto_0
    return v0

    .line 527
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    .line 297
    sget-object v0, Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;->b:Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->a:Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;

    .line 299
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->f:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/github/mikephil/charting/listener/a;->g:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 301
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getOnChartGestureListener()Lcom/github/mikephil/charting/listener/b;

    move-result-object v2

    .line 306
    invoke-direct {p0}, Lcom/github/mikephil/charting/listener/a;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 309
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    instance-of v0, v0, Lcom/github/mikephil/charting/charts/HorizontalBarChart;

    if-eqz v0, :cond_1

    .line 310
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/github/mikephil/charting/listener/a;->h:Lhk;

    iget v1, v1, Lhk;->a:F

    sub-float/2addr v0, v1

    neg-float v1, v0

    .line 311
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v3, p0, Lcom/github/mikephil/charting/listener/a;->h:Lhk;

    iget v3, v3, Lhk;->b:F

    sub-float/2addr v0, v3

    .line 321
    :goto_0
    iget-object v3, p0, Lcom/github/mikephil/charting/listener/a;->f:Landroid/graphics/Matrix;

    invoke-virtual {v3, v1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 323
    if-eqz v2, :cond_0

    .line 324
    invoke-interface {v2, p1, v1, v0}, Lcom/github/mikephil/charting/listener/b;->b(Landroid/view/MotionEvent;FF)V

    .line 325
    :cond_0
    return-void

    .line 313
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/github/mikephil/charting/listener/a;->h:Lhk;

    iget v1, v1, Lhk;->a:F

    sub-float v1, v0, v1

    .line 314
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v3, p0, Lcom/github/mikephil/charting/listener/a;->h:Lhk;

    iget v3, v3, Lhk;->b:F

    sub-float/2addr v0, v3

    neg-float v0, v0

    goto :goto_0

    .line 317
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/github/mikephil/charting/listener/a;->h:Lhk;

    iget v1, v1, Lhk;->a:F

    sub-float v1, v0, v1

    .line 318
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v3, p0, Lcom/github/mikephil/charting/listener/a;->h:Lhk;

    iget v3, v3, Lhk;->b:F

    sub-float/2addr v0, v3

    goto :goto_0
.end method

.method private e(Landroid/view/MotionEvent;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 334
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-lt v0, v9, :cond_2

    .line 336
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getOnChartGestureListener()Lcom/github/mikephil/charting/listener/b;

    move-result-object v6

    .line 339
    invoke-static {p1}, Lcom/github/mikephil/charting/listener/a;->g(Landroid/view/MotionEvent;)F

    move-result v3

    .line 341
    iget v0, p0, Lcom/github/mikephil/charting/listener/a;->s:F

    cmpl-float v0, v3, v0

    if-lez v0, :cond_2

    .line 344
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->i:Lhk;

    iget v0, v0, Lhk;->a:F

    iget-object v5, p0, Lcom/github/mikephil/charting/listener/a;->i:Lhk;

    iget v5, v5, Lhk;->b:F

    invoke-virtual {p0, v0, v5}, Lcom/github/mikephil/charting/listener/a;->a(FF)Lhk;

    move-result-object v7

    .line 345
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getViewPortHandler()Lhp;

    move-result-object v8

    .line 348
    iget v0, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    const/4 v5, 0x4

    if-ne v0, v5, :cond_8

    .line 350
    sget-object v0, Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;->e:Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->a:Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;

    .line 352
    iget v0, p0, Lcom/github/mikephil/charting/listener/a;->l:F

    div-float/2addr v3, v0

    .line 354
    cmpg-float v0, v3, v4

    if-gez v0, :cond_3

    .line 356
    :goto_0
    if-eqz v1, :cond_4

    .line 357
    invoke-virtual {v8}, Lhp;->w()Z

    move-result v0

    move v5, v0

    .line 360
    :goto_1
    if-eqz v1, :cond_5

    .line 361
    invoke-virtual {v8}, Lhp;->y()Z

    move-result v0

    move v1, v0

    .line 364
    :goto_2
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->m()Z

    move-result v0

    if-eqz v0, :cond_6

    move v2, v3

    .line 365
    :goto_3
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 367
    :goto_4
    if-nez v1, :cond_0

    if-eqz v5, :cond_1

    .line 369
    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->f:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/github/mikephil/charting/listener/a;->g:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 370
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->f:Landroid/graphics/Matrix;

    iget v1, v7, Lhk;->a:F

    iget v4, v7, Lhk;->b:F

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 372
    if-eqz v6, :cond_1

    .line 373
    invoke-interface {v6, p1, v2, v3}, Lcom/github/mikephil/charting/listener/b;->a(Landroid/view/MotionEvent;FF)V

    .line 419
    :cond_1
    :goto_5
    invoke-static {v7}, Lhk;->a(Lhk;)V

    .line 422
    :cond_2
    return-void

    :cond_3
    move v1, v2

    .line 354
    goto :goto_0

    .line 358
    :cond_4
    invoke-virtual {v8}, Lhp;->x()Z

    move-result v0

    move v5, v0

    goto :goto_1

    .line 362
    :cond_5
    invoke-virtual {v8}, Lhp;->z()Z

    move-result v0

    move v1, v0

    goto :goto_2

    :cond_6
    move v2, v4

    .line 364
    goto :goto_3

    :cond_7
    move v3, v4

    .line 365
    goto :goto_4

    .line 376
    :cond_8
    iget v0, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    if-ne v0, v9, :cond_b

    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->m()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 378
    sget-object v0, Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;->c:Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->a:Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;

    .line 380
    invoke-static {p1}, Lcom/github/mikephil/charting/listener/a;->h(Landroid/view/MotionEvent;)F

    move-result v0

    .line 381
    iget v3, p0, Lcom/github/mikephil/charting/listener/a;->j:F

    div-float v3, v0, v3

    .line 383
    cmpg-float v0, v3, v4

    if-gez v0, :cond_9

    .line 384
    :goto_6
    if-eqz v1, :cond_a

    .line 385
    invoke-virtual {v8}, Lhp;->w()Z

    move-result v0

    .line 388
    :goto_7
    if-eqz v0, :cond_1

    .line 390
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->f:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/github/mikephil/charting/listener/a;->g:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 391
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->f:Landroid/graphics/Matrix;

    iget v1, v7, Lhk;->a:F

    iget v2, v7, Lhk;->b:F

    invoke-virtual {v0, v3, v4, v1, v2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 393
    if-eqz v6, :cond_1

    .line 394
    invoke-interface {v6, p1, v3, v4}, Lcom/github/mikephil/charting/listener/b;->a(Landroid/view/MotionEvent;FF)V

    goto :goto_5

    :cond_9
    move v1, v2

    .line 383
    goto :goto_6

    .line 386
    :cond_a
    invoke-virtual {v8}, Lhp;->x()Z

    move-result v0

    goto :goto_7

    .line 397
    :cond_b
    iget v0, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 399
    sget-object v0, Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;->d:Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->a:Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;

    .line 401
    invoke-static {p1}, Lcom/github/mikephil/charting/listener/a;->i(Landroid/view/MotionEvent;)F

    move-result v0

    .line 402
    iget v3, p0, Lcom/github/mikephil/charting/listener/a;->k:F

    div-float v3, v0, v3

    .line 404
    cmpg-float v0, v3, v4

    if-gez v0, :cond_c

    move v0, v1

    .line 405
    :goto_8
    if-eqz v0, :cond_d

    .line 406
    invoke-virtual {v8}, Lhp;->y()Z

    move-result v0

    .line 409
    :goto_9
    if-eqz v0, :cond_1

    .line 411
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->f:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/github/mikephil/charting/listener/a;->g:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 412
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->f:Landroid/graphics/Matrix;

    iget v1, v7, Lhk;->a:F

    iget v2, v7, Lhk;->b:F

    invoke-virtual {v0, v4, v3, v1, v2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 414
    if-eqz v6, :cond_1

    .line 415
    invoke-interface {v6, p1, v4, v3}, Lcom/github/mikephil/charting/listener/b;->a(Landroid/view/MotionEvent;FF)V

    goto/16 :goto_5

    :cond_c
    move v0, v2

    .line 404
    goto :goto_8

    .line 407
    :cond_d
    invoke-virtual {v8}, Lhp;->z()Z

    move-result v0

    goto :goto_9
.end method

.method private f(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 431
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->a(FF)Lfj;

    move-result-object v1

    .line 433
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->c:Lfj;

    invoke-virtual {v1, v0}, Lfj;->a(Lfj;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 434
    iput-object v1, p0, Lcom/github/mikephil/charting/listener/a;->c:Lfj;

    .line 435
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->a(Lfj;Z)V

    .line 437
    :cond_0
    return-void
.end method

.method private static g(Landroid/view/MotionEvent;)F
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 465
    invoke-virtual {p0, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p0, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    sub-float/2addr v0, v1

    .line 466
    invoke-virtual {p0, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {p0, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    sub-float/2addr v1, v2

    .line 467
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private static h(Landroid/view/MotionEvent;)F
    .locals 2

    .prologue
    .line 478
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 479
    return v0
.end method

.method private static i(Landroid/view/MotionEvent;)F
    .locals 2

    .prologue
    .line 490
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 491
    return v0
.end method


# virtual methods
.method public a(FF)Lhk;
    .locals 3

    .prologue
    .line 505
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getViewPortHandler()Lhp;

    move-result-object v1

    .line 507
    invoke-virtual {v1}, Lhp;->a()F

    move-result v0

    sub-float v2, p1, v0

    .line 511
    invoke-direct {p0}, Lcom/github/mikephil/charting/listener/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 512
    invoke-virtual {v1}, Lhp;->c()F

    move-result v0

    sub-float v0, p2, v0

    neg-float v0, v0

    .line 517
    :goto_0
    invoke-static {v2, v0}, Lhk;->a(FF)Lhk;

    move-result-object v0

    return-object v0

    .line 514
    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, p2

    invoke-virtual {v1}, Lhp;->d()F

    move-result v1

    sub-float/2addr v0, v1

    neg-float v0, v0

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 631
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->q:Lhk;

    iput v1, v0, Lhk;->a:F

    .line 632
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->q:Lhk;

    iput v1, v0, Lhk;->b:F

    .line 633
    return-void
.end method

.method public b()V
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    const-wide v8, 0x3f847ae147ae147bL    # 0.01

    .line 637
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->q:Lhk;

    iget v0, v0, Lhk;->a:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->q:Lhk;

    iget v0, v0, Lhk;->b:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 672
    :goto_0
    return-void

    .line 640
    :cond_0
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    .line 642
    iget-object v3, p0, Lcom/github/mikephil/charting/listener/a;->q:Lhk;

    iget v4, v3, Lhk;->a:F

    iget-object v2, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v2, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getDragDecelerationFrictionCoef()F

    move-result v2

    mul-float/2addr v2, v4

    iput v2, v3, Lhk;->a:F

    .line 643
    iget-object v3, p0, Lcom/github/mikephil/charting/listener/a;->q:Lhk;

    iget v4, v3, Lhk;->b:F

    iget-object v2, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v2, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getDragDecelerationFrictionCoef()F

    move-result v2

    mul-float/2addr v2, v4

    iput v2, v3, Lhk;->b:F

    .line 645
    iget-wide v2, p0, Lcom/github/mikephil/charting/listener/a;->o:J

    sub-long v2, v0, v2

    long-to-float v2, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    .line 647
    iget-object v3, p0, Lcom/github/mikephil/charting/listener/a;->q:Lhk;

    iget v3, v3, Lhk;->a:F

    mul-float/2addr v3, v2

    .line 648
    iget-object v4, p0, Lcom/github/mikephil/charting/listener/a;->q:Lhk;

    iget v4, v4, Lhk;->b:F

    mul-float/2addr v2, v4

    .line 650
    iget-object v4, p0, Lcom/github/mikephil/charting/listener/a;->p:Lhk;

    iget v5, v4, Lhk;->a:F

    add-float/2addr v3, v5

    iput v3, v4, Lhk;->a:F

    .line 651
    iget-object v3, p0, Lcom/github/mikephil/charting/listener/a;->p:Lhk;

    iget v4, v3, Lhk;->b:F

    add-float/2addr v2, v4

    iput v2, v3, Lhk;->b:F

    .line 653
    const/4 v4, 0x2

    iget-object v2, p0, Lcom/github/mikephil/charting/listener/a;->p:Lhk;

    iget v5, v2, Lhk;->a:F

    iget-object v2, p0, Lcom/github/mikephil/charting/listener/a;->p:Lhk;

    iget v6, v2, Lhk;->b:F

    move-wide v2, v0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v2

    .line 655
    invoke-direct {p0, v2}, Lcom/github/mikephil/charting/listener/a;->d(Landroid/view/MotionEvent;)V

    .line 656
    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    .line 657
    iget-object v2, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v2, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getViewPortHandler()Lhp;

    move-result-object v2

    iget-object v3, p0, Lcom/github/mikephil/charting/listener/a;->f:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    invoke-virtual {v2, v3, v4, v7}, Lhp;->a(Landroid/graphics/Matrix;Landroid/view/View;Z)Landroid/graphics/Matrix;

    move-result-object v2

    iput-object v2, p0, Lcom/github/mikephil/charting/listener/a;->f:Landroid/graphics/Matrix;

    .line 659
    iput-wide v0, p0, Lcom/github/mikephil/charting/listener/a;->o:J

    .line 661
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->q:Lhk;

    iget v0, v0, Lhk;->a:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    cmpl-double v0, v0, v8

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->q:Lhk;

    iget v0, v0, Lhk;->b:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    cmpl-double v0, v0, v8

    if-ltz v0, :cond_2

    .line 662
    :cond_1
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    invoke-static {v0}, Lho;->a(Landroid/view/View;)V

    goto/16 :goto_0

    .line 667
    :cond_2
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->j()V

    .line 668
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->postInvalidate()V

    .line 670
    invoke-virtual {p0}, Lcom/github/mikephil/charting/listener/a;->a()V

    goto/16 :goto_0
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const v3, 0x3fb33333    # 1.4f

    const/high16 v4, 0x3f800000    # 1.0f

    .line 557
    sget-object v0, Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;->h:Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->a:Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;

    .line 559
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getOnChartGestureListener()Lcom/github/mikephil/charting/listener/b;

    move-result-object v0

    .line 561
    if-eqz v0, :cond_0

    .line 562
    invoke-interface {v0, p1}, Lcom/github/mikephil/charting/listener/b;->b(Landroid/view/MotionEvent;)V

    .line 566
    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getData()Lcom/github/mikephil/charting/data/h;

    move-result-object v0

    check-cast v0, Lcom/github/mikephil/charting/data/b;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/b;->j()I

    move-result v0

    if-lez v0, :cond_2

    .line 568
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/github/mikephil/charting/listener/a;->a(FF)Lhk;

    move-result-object v5

    .line 570
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    iget-object v1, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v1, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->m()Z

    move-result v1

    if-eqz v1, :cond_3

    move v2, v3

    :goto_0
    iget-object v1, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v1, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n()Z

    move-result v1

    if-eqz v1, :cond_4

    :goto_1
    iget v1, v5, Lhk;->a:F

    iget v4, v5, Lhk;->b:F

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->a(FFFF)V

    .line 572
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->w()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 573
    const-string/jumbo v0, "BarlineChartTouch"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Double-Tap, Zooming In, x: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v5, Lhk;->a:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", y: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v5, Lhk;->b:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 576
    :cond_1
    invoke-static {v5}, Lhk;->a(Lhk;)V

    .line 579
    :cond_2
    invoke-super {p0, p1}, Lcom/github/mikephil/charting/listener/ChartTouchListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    :cond_3
    move v2, v4

    .line 570
    goto :goto_0

    :cond_4
    move v3, v4

    goto :goto_1
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 619
    sget-object v0, Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;->j:Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->a:Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;

    .line 621
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getOnChartGestureListener()Lcom/github/mikephil/charting/listener/b;

    move-result-object v0

    .line 623
    if-eqz v0, :cond_0

    .line 624
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/github/mikephil/charting/listener/b;->a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V

    .line 627
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/github/mikephil/charting/listener/ChartTouchListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 585
    sget-object v0, Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;->i:Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->a:Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;

    .line 587
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getOnChartGestureListener()Lcom/github/mikephil/charting/listener/b;

    move-result-object v0

    .line 589
    if-eqz v0, :cond_0

    .line 591
    invoke-interface {v0, p1}, Lcom/github/mikephil/charting/listener/b;->a(Landroid/view/MotionEvent;)V

    .line 593
    :cond_0
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 598
    sget-object v0, Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;->g:Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->a:Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;

    .line 600
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getOnChartGestureListener()Lcom/github/mikephil/charting/listener/b;

    move-result-object v0

    .line 602
    if-eqz v0, :cond_0

    .line 603
    invoke-interface {v0, p1}, Lcom/github/mikephil/charting/listener/b;->c(Landroid/view/MotionEvent;)V

    .line 606
    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->t()Z

    move-result v0

    if-nez v0, :cond_1

    .line 607
    const/4 v0, 0x0

    .line 613
    :goto_0
    return v0

    .line 610
    :cond_1
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->a(FF)Lfj;

    move-result-object v0

    .line 611
    invoke-virtual {p0, v0, p1}, Lcom/github/mikephil/charting/listener/a;->a(Lfj;Landroid/view/MotionEvent;)V

    .line 613
    invoke-super {p0, p1}, Lcom/github/mikephil/charting/listener/ChartTouchListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x0

    const/4 v2, 0x3

    const/4 v1, 0x2

    const/4 v8, 0x1

    .line 99
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->n:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 100
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->n:Landroid/view/VelocityTracker;

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->n:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 104
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 105
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->n:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->n:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->n:Landroid/view/VelocityTracker;

    .line 111
    :cond_1
    iget v0, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    if-nez v0, :cond_2

    .line 112
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->d:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 115
    :cond_2
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->l()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->m()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n()Z

    move-result v0

    if-nez v0, :cond_3

    .line 268
    :goto_0
    return v8

    .line 119
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 266
    :cond_4
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getViewPortHandler()Lhp;

    move-result-object v0

    iget-object v1, p0, Lcom/github/mikephil/charting/listener/a;->f:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    invoke-virtual {v0, v1, v2, v8}, Lhp;->a(Landroid/graphics/Matrix;Landroid/view/View;Z)Landroid/graphics/Matrix;

    move-result-object v0

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->f:Landroid/graphics/Matrix;

    goto :goto_0

    .line 123
    :pswitch_1
    invoke-virtual {p0, p2}, Lcom/github/mikephil/charting/listener/a;->a(Landroid/view/MotionEvent;)V

    .line 125
    invoke-virtual {p0}, Lcom/github/mikephil/charting/listener/a;->a()V

    .line 127
    invoke-direct {p0, p2}, Lcom/github/mikephil/charting/listener/a;->c(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 132
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-lt v0, v1, :cond_4

    .line 134
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->x()V

    .line 136
    invoke-direct {p0, p2}, Lcom/github/mikephil/charting/listener/a;->c(Landroid/view/MotionEvent;)V

    .line 139
    invoke-static {p2}, Lcom/github/mikephil/charting/listener/a;->h(Landroid/view/MotionEvent;)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/listener/a;->j:F

    .line 142
    invoke-static {p2}, Lcom/github/mikephil/charting/listener/a;->i(Landroid/view/MotionEvent;)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/listener/a;->k:F

    .line 145
    invoke-static {p2}, Lcom/github/mikephil/charting/listener/a;->g(Landroid/view/MotionEvent;)F

    move-result v0

    iput v0, p0, Lcom/github/mikephil/charting/listener/a;->l:F

    .line 147
    iget v0, p0, Lcom/github/mikephil/charting/listener/a;->l:F

    const/high16 v3, 0x41200000    # 10.0f

    cmpl-float v0, v0, v3

    if-lez v0, :cond_5

    .line 149
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->q()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 150
    iput v10, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    .line 161
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->i:Lhk;

    invoke-static {v0, p2}, Lcom/github/mikephil/charting/listener/a;->a(Lhk;Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 152
    :cond_6
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->m()Z

    move-result v3

    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n()Z

    move-result v0

    if-eq v3, v0, :cond_8

    .line 153
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->m()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_3
    iput v0, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3

    .line 155
    :cond_8
    iget v0, p0, Lcom/github/mikephil/charting/listener/a;->j:F

    iget v3, p0, Lcom/github/mikephil/charting/listener/a;->k:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_9

    :goto_4
    iput v1, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    goto :goto_2

    :cond_9
    move v1, v2

    goto :goto_4

    .line 166
    :pswitch_3
    iget v0, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    if-ne v0, v8, :cond_a

    .line 168
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->x()V

    .line 169
    invoke-direct {p0, p2}, Lcom/github/mikephil/charting/listener/a;->d(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    .line 171
    :cond_a
    iget v0, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    if-eq v0, v1, :cond_b

    iget v0, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    if-eq v0, v2, :cond_b

    iget v0, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    if-ne v0, v10, :cond_d

    .line 173
    :cond_b
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->x()V

    .line 175
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->m()Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->n()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 176
    :cond_c
    invoke-direct {p0, p2}, Lcom/github/mikephil/charting/listener/a;->e(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    .line 178
    :cond_d
    iget v0, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    if-nez v0, :cond_4

    .line 179
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/github/mikephil/charting/listener/a;->h:Lhk;

    iget v1, v1, Lhk;->a:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/github/mikephil/charting/listener/a;->h:Lhk;

    iget v3, v3, Lhk;->b:F

    invoke-static {v0, v1, v2, v3}, Lcom/github/mikephil/charting/listener/a;->a(FFFF)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/github/mikephil/charting/listener/a;->r:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    .line 182
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->r()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 184
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->p()Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->l()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 185
    iput v8, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    goto/16 :goto_1

    .line 188
    :cond_e
    sget-object v0, Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;->b:Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->a:Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;

    .line 190
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->k()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 191
    invoke-direct {p0, p2}, Lcom/github/mikephil/charting/listener/a;->f(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    .line 194
    :cond_f
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->l()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 195
    sget-object v0, Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;->b:Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->a:Lcom/github/mikephil/charting/listener/ChartTouchListener$ChartGesture;

    .line 196
    iput v8, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    goto/16 :goto_1

    .line 203
    :pswitch_4
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->n:Landroid/view/VelocityTracker;

    .line 204
    invoke-virtual {p2, v9}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    .line 205
    const/16 v4, 0x3e8

    invoke-static {}, Lho;->c()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v0, v4, v5}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 206
    invoke-virtual {v0, v3}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v4

    .line 207
    invoke-virtual {v0, v3}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v3

    .line 209
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {}, Lho;->b()I

    move-result v5

    int-to-float v5, v5

    cmpl-float v0, v0, v5

    if-gtz v0, :cond_10

    .line 210
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {}, Lho;->b()I

    move-result v5

    int-to-float v5, v5

    cmpl-float v0, v0, v5

    if-lez v0, :cond_11

    .line 212
    :cond_10
    iget v0, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    if-ne v0, v8, :cond_11

    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->v()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 214
    invoke-virtual {p0}, Lcom/github/mikephil/charting/listener/a;->a()V

    .line 216
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/github/mikephil/charting/listener/a;->o:J

    .line 218
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->p:Lhk;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    iput v5, v0, Lhk;->a:F

    .line 219
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->p:Lhk;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iput v5, v0, Lhk;->b:F

    .line 221
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->q:Lhk;

    iput v3, v0, Lhk;->a:F

    .line 222
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->q:Lhk;

    iput v4, v0, Lhk;->b:F

    .line 224
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    invoke-static {v0}, Lho;->a(Landroid/view/View;)V

    .line 229
    :cond_11
    iget v0, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    if-eq v0, v1, :cond_12

    iget v0, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    if-eq v0, v2, :cond_12

    iget v0, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    if-eq v0, v10, :cond_12

    iget v0, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_13

    .line 237
    :cond_12
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->j()V

    .line 238
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->postInvalidate()V

    .line 241
    :cond_13
    iput v9, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    .line 242
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->e:Lcom/github/mikephil/charting/charts/Chart;

    check-cast v0, Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->y()V

    .line 244
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->n:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_14

    .line 245
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->n:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 246
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/github/mikephil/charting/listener/a;->n:Landroid/view/VelocityTracker;

    .line 249
    :cond_14
    invoke-virtual {p0, p2}, Lcom/github/mikephil/charting/listener/a;->b(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    .line 253
    :pswitch_5
    iget-object v0, p0, Lcom/github/mikephil/charting/listener/a;->n:Landroid/view/VelocityTracker;

    invoke-static {p2, v0}, Lho;->a(Landroid/view/MotionEvent;Landroid/view/VelocityTracker;)V

    .line 255
    const/4 v0, 0x5

    iput v0, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    goto/16 :goto_1

    .line 260
    :pswitch_6
    iput v9, p0, Lcom/github/mikephil/charting/listener/a;->b:I

    .line 261
    invoke-virtual {p0, p2}, Lcom/github/mikephil/charting/listener/a;->b(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    .line 119
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method
