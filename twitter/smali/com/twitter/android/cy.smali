.class public Lcom/twitter/android/cy;
.super Lcom/twitter/library/widget/SlidingUpPanelLayout$d;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/twitter/ui/user/BaseUserView$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/cy$b;,
        Lcom/twitter/android/cy$a;,
        Lcom/twitter/android/cy$d;,
        Lcom/twitter/android/cy$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/widget/SlidingUpPanelLayout$d;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/twitter/model/core/TwitterUser;",
        ">;>;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/view/View$OnTouchListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lcom/twitter/ui/user/BaseUserView$a",
        "<",
        "Lcom/twitter/ui/user/UserView;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public a:Z

.field public b:Z

.field private c:Lcom/twitter/model/util/FriendshipCache;

.field private final d:Landroid/app/Activity;

.field private final e:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field private final f:Lcom/twitter/android/UsersAdapter;

.field private final g:Landroid/widget/ListView;

.field private final h:Lcom/twitter/library/client/p;

.field private final i:Lcom/twitter/library/client/Session;

.field private final j:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final k:Lcom/twitter/library/widget/SlidingPanel;

.field private final l:Lcom/twitter/android/cy$d;

.field private final m:I

.field private n:F

.field private o:Lcom/twitter/android/cy$c;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/twitter/library/client/Session;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/widget/SlidingPanel;I)V
    .locals 3
    .param p5    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    const/16 v2, 0x8

    .line 93
    invoke-direct {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout$d;-><init>()V

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/cy;->b:Z

    .line 78
    new-instance v0, Lcom/twitter/android/cy$d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/cy$d;-><init>(Lcom/twitter/android/cy;Lcom/twitter/android/cy$1;)V

    iput-object v0, p0, Lcom/twitter/android/cy;->l:Lcom/twitter/android/cy$d;

    .line 94
    iput-object p1, p0, Lcom/twitter/android/cy;->d:Landroid/app/Activity;

    .line 95
    iput-object p2, p0, Lcom/twitter/android/cy;->i:Lcom/twitter/library/client/Session;

    .line 96
    iput-object p3, p0, Lcom/twitter/android/cy;->j:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 97
    iput p5, p0, Lcom/twitter/android/cy;->e:I

    .line 98
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cy;->h:Lcom/twitter/library/client/p;

    .line 100
    iput-object p4, p0, Lcom/twitter/android/cy;->k:Lcom/twitter/library/widget/SlidingPanel;

    .line 101
    iget-object v0, p0, Lcom/twitter/android/cy;->k:Lcom/twitter/library/widget/SlidingPanel;

    const v1, 0x7f130079

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 102
    iget-object v0, p0, Lcom/twitter/android/cy;->k:Lcom/twitter/library/widget/SlidingPanel;

    const v1, 0x7f130077

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 103
    iget-object v0, p0, Lcom/twitter/android/cy;->k:Lcom/twitter/library/widget/SlidingPanel;

    const v1, 0x7f130164

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/cy;->m:I

    .line 106
    iget-object v0, p0, Lcom/twitter/android/cy;->k:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0, p0}, Lcom/twitter/library/widget/SlidingPanel;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 108
    iget-object v0, p0, Lcom/twitter/android/cy;->k:Lcom/twitter/library/widget/SlidingPanel;

    const v1, 0x7f1304ae

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/twitter/android/cy;->g:Landroid/widget/ListView;

    .line 109
    iget-object v0, p0, Lcom/twitter/android/cy;->g:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 111
    new-instance v0, Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/model/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/cy;->c:Lcom/twitter/model/util/FriendshipCache;

    .line 112
    invoke-virtual {p0}, Lcom/twitter/android/cy;->a()Lcom/twitter/android/UsersAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cy;->f:Lcom/twitter/android/UsersAdapter;

    .line 113
    iget-object v0, p0, Lcom/twitter/android/cy;->g:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/cy;->f:Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 114
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/cy;)Lcom/twitter/library/widget/SlidingPanel;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/cy;->k:Lcom/twitter/library/widget/SlidingPanel;

    return-object v0
.end method

.method private a(JI)V
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/twitter/android/cy;->c:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/model/util/FriendshipCache;->a(JI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/twitter/android/cy;->c:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/model/util/FriendshipCache;->b(JI)V

    .line 332
    iget-object v0, p0, Lcom/twitter/android/cy;->f:Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->notifyDataSetChanged()V

    .line 334
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/cy;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/cy;->g:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/cy;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/cy;->d:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/cy;)Lcom/twitter/model/util/FriendshipCache;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/cy;->c:Lcom/twitter/model/util/FriendshipCache;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/cy;)Lcom/twitter/android/UsersAdapter;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/cy;->f:Lcom/twitter/android/UsersAdapter;

    return-object v0
.end method


# virtual methods
.method protected a()Lcom/twitter/android/UsersAdapter;
    .locals 6

    .prologue
    .line 118
    new-instance v0, Lcom/twitter/android/UsersAdapter;

    iget-object v1, p0, Lcom/twitter/android/cy;->d:Landroid/app/Activity;

    iget v2, p0, Lcom/twitter/android/cy;->e:I

    iget-object v4, p0, Lcom/twitter/android/cy;->c:Lcom/twitter/model/util/FriendshipCache;

    const/4 v5, 0x0

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/UsersAdapter;-><init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/UsersAdapter$CheckboxConfig;)V

    return-object v0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 301
    packed-switch p1, :pswitch_data_0

    .line 317
    :cond_0
    :goto_0
    return-void

    .line 303
    :pswitch_0
    const/4 v0, -0x1

    if-ne v0, p2, :cond_0

    if-eqz p3, :cond_0

    .line 304
    const-string/jumbo v0, "user_id"

    invoke-virtual {p3, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 305
    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    const-string/jumbo v2, "friendship"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 306
    const-string/jumbo v2, "friendship"

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 308
    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/cy;->a(JI)V

    goto :goto_0

    .line 301
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/twitter/android/cy;->c:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/model/util/FriendshipCache;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 215
    const-string/jumbo v0, "friendship_cache"

    iget-object v1, p0, Lcom/twitter/android/cy;->c:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 217
    :cond_0
    return-void
.end method

.method public a(Landroid/support/v4/app/LoaderManager;[J)V
    .locals 2

    .prologue
    .line 183
    if-eqz p1, :cond_0

    .line 184
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 185
    const-string/jumbo v1, "tags"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 186
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 188
    :cond_0
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 198
    iget-object v0, p0, Lcom/twitter/android/cy;->f:Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0, p2}, Lcom/twitter/android/UsersAdapter;->a(Ljava/util/List;)V

    .line 199
    iget-boolean v0, p0, Lcom/twitter/android/cy;->a:Z

    if-eqz v0, :cond_0

    .line 200
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/cy;->a(Z)V

    .line 202
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/android/cy$c;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/twitter/android/cy;->o:Lcom/twitter/android/cy$c;

    .line 154
    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/ui/user/BaseUserView;JII)V
    .locals 6

    .prologue
    .line 46
    move-object v1, p1

    check-cast v1, Lcom/twitter/ui/user/UserView;

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/cy;->a(Lcom/twitter/ui/user/UserView;JII)V

    return-void
.end method

.method public a(Lcom/twitter/ui/user/UserView;JII)V
    .locals 8

    .prologue
    .line 123
    const v0, 0x7f130003

    if-ne p4, v0, :cond_0

    .line 124
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getPromotedContent()Lcgi;

    move-result-object v6

    .line 125
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->j()Z

    move-result v7

    .line 127
    if-eqz v7, :cond_1

    .line 128
    new-instance v1, Lbhs;

    iget-object v2, p0, Lcom/twitter/android/cy;->d:Landroid/app/Activity;

    iget-object v3, p0, Lcom/twitter/android/cy;->i:Lcom/twitter/library/client/Session;

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lbhs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    .line 130
    iget-object v0, p0, Lcom/twitter/android/cy;->h:Lcom/twitter/library/client/p;

    new-instance v2, Lcom/twitter/android/cy$b;

    invoke-direct {v2, p0, p2, p3}, Lcom/twitter/android/cy$b;-><init>(Lcom/twitter/android/cy;J)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 132
    iget-object v0, p0, Lcom/twitter/android/cy;->c:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/model/util/FriendshipCache;->c(J)V

    .line 141
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/cy;->o:Lcom/twitter/android/cy$c;

    if-eqz v0, :cond_0

    .line 142
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/db;

    .line 143
    iget-object v1, p0, Lcom/twitter/android/cy;->o:Lcom/twitter/android/cy$c;

    iget-object v5, v0, Lcom/twitter/android/db;->g:Ljava/lang/String;

    move v2, v7

    move-wide v3, p2

    invoke-interface/range {v1 .. v6}, Lcom/twitter/android/cy$c;->a(ZJLjava/lang/String;Lcgi;)V

    .line 146
    :cond_0
    return-void

    .line 134
    :cond_1
    new-instance v1, Lbhq;

    iget-object v2, p0, Lcom/twitter/android/cy;->d:Landroid/app/Activity;

    iget-object v3, p0, Lcom/twitter/android/cy;->i:Lcom/twitter/library/client/Session;

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lbhq;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    .line 136
    iget-object v0, p0, Lcom/twitter/android/cy;->h:Lcom/twitter/library/client/p;

    new-instance v2, Lcom/twitter/android/cy$a;

    invoke-direct {v2, p0, p2, p3}, Lcom/twitter/android/cy$a;-><init>(Lcom/twitter/android/cy;J)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 138
    iget-object v0, p0, Lcom/twitter/android/cy;->c:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/model/util/FriendshipCache;->b(J)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 157
    iput-boolean p1, p0, Lcom/twitter/android/cy;->a:Z

    .line 159
    iget-object v0, p0, Lcom/twitter/android/cy;->o:Lcom/twitter/android/cy$c;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/twitter/android/cy;->o:Lcom/twitter/android/cy$c;

    invoke-interface {v0, p1}, Lcom/twitter/android/cy$c;->c(Z)V

    .line 163
    :cond_0
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/twitter/android/cy;->f:Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 164
    iget-boolean v0, p0, Lcom/twitter/android/cy;->b:Z

    if-eqz v0, :cond_2

    .line 165
    iget-object v0, p0, Lcom/twitter/android/cy;->k:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/cy;->l:Lcom/twitter/android/cy$d;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 173
    iget-object v0, p0, Lcom/twitter/android/cy;->k:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->a()Z

    .line 180
    :cond_1
    :goto_0
    return-void

    .line 174
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/cy;->k:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getPanelState()I

    move-result v0

    if-nez v0, :cond_1

    .line 175
    iget-object v0, p0, Lcom/twitter/android/cy;->k:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->b()Z

    goto :goto_0

    .line 178
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/cy;->k:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->d()Z

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 220
    const-string/jumbo v0, "friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    const-string/jumbo v0, "friendship_cache"

    .line 222
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/util/FriendshipCache;

    iput-object v0, p0, Lcom/twitter/android/cy;->c:Lcom/twitter/model/util/FriendshipCache;

    .line 224
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/twitter/android/cy;->a:Z

    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/cy;->b:Z

    .line 211
    return-void
.end method

.method public c(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 321
    invoke-virtual {p0}, Lcom/twitter/android/cy;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    iput-boolean v1, p0, Lcom/twitter/android/cy;->a:Z

    .line 323
    iget-object v0, p0, Lcom/twitter/android/cy;->o:Lcom/twitter/android/cy$c;

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/twitter/android/cy;->o:Lcom/twitter/android/cy$c;

    invoke-interface {v0, v1}, Lcom/twitter/android/cy$c;->c(Z)V

    .line 327
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 228
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 237
    :goto_0
    return-void

    .line 230
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/cy;->a(Z)V

    goto :goto_0

    .line 228
    nop

    :pswitch_data_0
    .packed-switch 0x7f130164
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 192
    const-string/jumbo v0, "tags"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    .line 193
    new-instance v1, Lcom/twitter/library/api/aa;

    iget-object v2, p0, Lcom/twitter/android/cy;->d:Landroid/app/Activity;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/api/aa;-><init>(Landroid/content/Context;[J)V

    return-object v1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 278
    instance-of v0, p2, Lcom/twitter/ui/user/UserView;

    if-eqz v0, :cond_1

    .line 279
    iget-object v0, p0, Lcom/twitter/android/cy;->o:Lcom/twitter/android/cy$c;

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/twitter/android/cy;->o:Lcom/twitter/android/cy$c;

    invoke-interface {v0}, Lcom/twitter/android/cy$c;->S_()V

    .line 283
    :cond_0
    check-cast p2, Lcom/twitter/ui/user/UserView;

    .line 284
    invoke-virtual {p2}, Lcom/twitter/ui/user/UserView;->getUserName()Ljava/lang/String;

    move-result-object v4

    .line 286
    iget-object v0, p0, Lcom/twitter/android/cy;->c:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {p2}, Lcom/twitter/ui/user/UserView;->getUserId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->j(J)Ljava/lang/Integer;

    move-result-object v0

    .line 287
    iget-object v1, p0, Lcom/twitter/android/cy;->d:Landroid/app/Activity;

    .line 289
    invoke-virtual {p2}, Lcom/twitter/ui/user/UserView;->getUserId()J

    move-result-wide v2

    .line 291
    invoke-virtual {p2}, Lcom/twitter/ui/user/UserView;->getPromotedContent()Lcgi;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/cy;->j:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-nez v0, :cond_2

    const/4 v7, -0x1

    :goto_0
    move-object v9, v8

    .line 287
    invoke-static/range {v1 .. v9}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ILcom/twitter/library/api/PromotedEvent;Lcom/twitter/model/timeline/r;)Landroid/content/Intent;

    move-result-object v0

    .line 296
    iget-object v1, p0, Lcom/twitter/android/cy;->d:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 298
    :cond_1
    return-void

    .line 293
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 46
    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/cy;->a(Landroid/support/v4/content/Loader;Ljava/util/List;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 206
    iget-object v0, p0, Lcom/twitter/android/cy;->f:Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->k()Lcjt;

    move-result-object v0

    invoke-static {}, Lcbi;->f()Lcbi;

    move-result-object v1

    invoke-interface {v0, v1}, Lcjt;->a(Lcbi;)Lcbi;

    .line 207
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 244
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    .line 245
    invoke-static {p2}, Landroid/support/v4/view/MotionEventCompat;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v1

    .line 247
    packed-switch v1, :pswitch_data_0

    .line 270
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    :goto_1
    return v0

    .line 249
    :pswitch_1
    iput v0, p0, Lcom/twitter/android/cy;->n:F

    goto :goto_0

    .line 253
    :pswitch_2
    iget v1, p0, Lcom/twitter/android/cy;->n:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 261
    iget v1, p0, Lcom/twitter/android/cy;->m:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 262
    const/4 v0, 0x1

    goto :goto_1

    .line 247
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
