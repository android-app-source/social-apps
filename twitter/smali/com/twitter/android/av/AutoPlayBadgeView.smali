.class public Lcom/twitter/android/av/AutoPlayBadgeView;
.super Landroid/widget/LinearLayout;
.source "Twttr"


# instance fields
.field a:Landroid/widget/ImageView;

.field b:Lcom/twitter/android/av/AutoPlayBadgeTextView;

.field c:Z

.field d:Ljava/lang/String;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/ImageView;

.field private g:Lcom/twitter/android/av/AutoPlayEqualizerDrawable;

.field private final h:Lcom/twitter/android/av/i;

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Lcom/twitter/model/av/AVMedia;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/av/AutoPlayBadgeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/av/AutoPlayBadgeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/twitter/android/av/i;

    invoke-direct {v0}, Lcom/twitter/android/av/i;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/android/av/AutoPlayBadgeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/twitter/android/av/i;)V

    .line 50
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/twitter/android/av/i;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->c:Z

    .line 55
    iput-object p4, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->h:Lcom/twitter/android/av/i;

    .line 60
    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    .line 61
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->setAnimateParentHierarchy(Z)V

    .line 62
    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AutoPlayBadgeView;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 63
    return-void
.end method

.method private a(Lcom/twitter/library/av/playback/AVDataSource;)Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->i:Z

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/twitter/library/av/playback/AVDataSource;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->b:Lcom/twitter/android/av/AutoPlayBadgeTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AutoPlayBadgeTextView;->setVisibility(I)V

    .line 179
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 180
    return-void
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->j:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private setAutoPlayDrawableState(I)V
    .locals 2

    .prologue
    .line 220
    iget-boolean v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->c:Z

    if-nez v0, :cond_1

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    if-nez p1, :cond_3

    .line 226
    const/16 v0, 0x8

    .line 230
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->f:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    .line 231
    iget-object v1, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 233
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->g:Lcom/twitter/android/av/AutoPlayEqualizerDrawable;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->g:Lcom/twitter/android/av/AutoPlayEqualizerDrawable;

    invoke-virtual {v0, p1}, Lcom/twitter/android/av/AutoPlayEqualizerDrawable;->a(I)V

    goto :goto_0

    .line 228
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/av/AutoPlayBadgeView;->setAutoPlayDrawableState(I)V

    .line 184
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/aa;)V
    .locals 4

    .prologue
    .line 212
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 213
    invoke-virtual {p1}, Lcom/twitter/library/av/playback/aa;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x0

    .line 214
    :goto_0
    invoke-static {v0, v1}, Lcom/twitter/util/aa;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 215
    iget-object v1, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->b:Lcom/twitter/android/av/AutoPlayBadgeTextView;

    iget-object v2, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/av/AutoPlayBadgeTextView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    :cond_0
    return-void

    .line 213
    :cond_1
    iget-wide v0, p1, Lcom/twitter/library/av/playback/aa;->c:J

    iget-wide v2, p1, Lcom/twitter/library/av/playback/aa;->b:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->k:Lcom/twitter/model/av/AVMedia;

    invoke-static {v0}, Lcom/twitter/model/av/b;->a(Lcom/twitter/model/av/AVMedia;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    invoke-virtual {p0}, Lcom/twitter/android/av/AutoPlayBadgeView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a009c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->d:Ljava/lang/String;

    .line 193
    :goto_0
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/android/av/AutoPlayBadgeView;->setAutoPlayDrawableState(I)V

    .line 194
    return-void

    .line 190
    :cond_0
    const-string/jumbo v0, "%s"

    iput-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 197
    invoke-direct {p0}, Lcom/twitter/android/av/AutoPlayBadgeView;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->b:Lcom/twitter/android/av/AutoPlayBadgeTextView;

    iget-object v1, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AutoPlayBadgeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->b:Lcom/twitter/android/av/AutoPlayBadgeTextView;

    invoke-virtual {v0}, Lcom/twitter/android/av/AutoPlayBadgeTextView;->requestLayout()V

    .line 203
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/av/AutoPlayBadgeView;->setAutoPlayDrawableState(I)V

    .line 204
    return-void
.end method

.method public getEqDrawableState()I
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->g:Lcom/twitter/android/av/AutoPlayEqualizerDrawable;

    invoke-virtual {v0}, Lcom/twitter/android/av/AutoPlayEqualizerDrawable;->a()I

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 87
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 88
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->g:Lcom/twitter/android/av/AutoPlayEqualizerDrawable;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->g:Lcom/twitter/android/av/AutoPlayEqualizerDrawable;

    const/4 v1, 0x0

    .line 90
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AutoPlayEqualizerDrawable;->a(I)V

    .line 92
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 67
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 69
    const v0, 0x7f13019d

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AutoPlayBadgeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/AutoPlayBadgeTextView;

    iput-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->b:Lcom/twitter/android/av/AutoPlayBadgeTextView;

    .line 70
    const v0, 0x7f13019e

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AutoPlayBadgeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->a:Landroid/widget/ImageView;

    .line 72
    const v0, 0x7f1301a0

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AutoPlayBadgeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->f:Landroid/widget/ImageView;

    .line 73
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->f:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->h:Lcom/twitter/android/av/i;

    invoke-virtual {v0}, Lcom/twitter/android/av/i;->a()Lcom/twitter/android/av/AutoPlayEqualizerDrawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->g:Lcom/twitter/android/av/AutoPlayEqualizerDrawable;

    .line 75
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->f:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->g:Lcom/twitter/android/av/AutoPlayEqualizerDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 76
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/av/AutoPlayBadgeView;->setAutoPlayDrawableState(I)V

    .line 78
    :cond_0
    const v0, 0x7f13019f

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AutoPlayBadgeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->e:Landroid/view/View;

    .line 79
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->e:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 83
    :cond_1
    return-void
.end method

.method public setAVDataSource(Lcom/twitter/library/av/playback/AVDataSource;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 113
    invoke-interface {p1}, Lcom/twitter/library/av/playback/AVDataSource;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 163
    :goto_0
    :pswitch_0
    return-void

    .line 115
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->a:Landroid/widget/ImageView;

    const v1, 0x7f0206b7

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 116
    invoke-direct {p0}, Lcom/twitter/android/av/AutoPlayBadgeView;->d()V

    goto :goto_0

    .line 121
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/twitter/android/av/AutoPlayBadgeView;->a(Lcom/twitter/library/av/playback/AVDataSource;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    const v0, 0x7f020654

    .line 126
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 127
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 128
    iput-boolean v3, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->c:Z

    .line 129
    invoke-direct {p0}, Lcom/twitter/android/av/AutoPlayBadgeView;->d()V

    goto :goto_0

    .line 124
    :cond_0
    const v0, 0x7f0203f5

    goto :goto_1

    .line 136
    :pswitch_3
    invoke-interface {p1}, Lcom/twitter/library/av/playback/AVDataSource;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->j:Ljava/lang/String;

    .line 137
    invoke-direct {p0}, Lcom/twitter/android/av/AutoPlayBadgeView;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 138
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->b:Lcom/twitter/android/av/AutoPlayBadgeTextView;

    iget-object v1, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AutoPlayBadgeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->b:Lcom/twitter/android/av/AutoPlayBadgeTextView;

    invoke-virtual {v0, v3}, Lcom/twitter/android/av/AutoPlayBadgeTextView;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 147
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 142
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->b:Lcom/twitter/android/av/AutoPlayBadgeTextView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/av/AutoPlayBadgeTextView;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->e:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 153
    :pswitch_4
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->b:Lcom/twitter/android/av/AutoPlayBadgeTextView;

    invoke-virtual {v0, v3}, Lcom/twitter/android/av/AutoPlayBadgeTextView;->setVisibility(I)V

    .line 154
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 155
    iget-object v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->c:Z

    goto :goto_0

    .line 113
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public setAvMedia(Lcom/twitter/model/av/AVMedia;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->k:Lcom/twitter/model/av/AVMedia;

    .line 167
    return-void
.end method

.method public setDisableSnapreelBadge(Z)V
    .locals 0

    .prologue
    .line 170
    iput-boolean p1, p0, Lcom/twitter/android/av/AutoPlayBadgeView;->i:Z

    .line 171
    return-void
.end method

.method public setTweet(Lcom/twitter/model/core/Tweet;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 102
    new-instance v0, Lcom/twitter/library/av/playback/TweetAVDataSource;

    invoke-direct {v0, p1}, Lcom/twitter/library/av/playback/TweetAVDataSource;-><init>(Lcom/twitter/model/core/Tweet;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/av/AutoPlayBadgeView;->setAVDataSource(Lcom/twitter/library/av/playback/AVDataSource;)V

    .line 103
    return-void
.end method
