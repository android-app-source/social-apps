.class public abstract Lcom/twitter/android/revenue/card/ae;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field protected i:Landroid/view/View;

.field protected j:J

.field protected k:Lcom/twitter/android/card/CardActionHelper;

.field protected l:Lcom/twitter/android/revenue/card/m;


# direct methods
.method protected constructor <init>(Lcom/twitter/android/revenue/card/m;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/twitter/android/revenue/card/ae;->l:Lcom/twitter/android/revenue/card/m;

    .line 29
    invoke-virtual {p1}, Lcom/twitter/android/revenue/card/m;->o()Lcom/twitter/android/card/CardActionHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/ae;->k:Lcom/twitter/android/card/CardActionHelper;

    .line 30
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    .prologue
    .line 36
    return-void
.end method

.method protected abstract a(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V
.end method

.method protected a(Lcom/twitter/library/card/z$a;)V
    .locals 2

    .prologue
    .line 33
    iget-wide v0, p1, Lcom/twitter/library/card/z$a;->b:J

    iput-wide v0, p0, Lcom/twitter/android/revenue/card/ae;->j:J

    .line 34
    return-void
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 38
    return-void
.end method

.method protected f()Landroid/view/View;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ae;->i:Landroid/view/View;

    return-object v0
.end method
