.class public Lcom/twitter/network/apache/message/b;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lcom/twitter/network/apache/message/b;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final b:Lcom/twitter/network/apache/message/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/twitter/network/apache/message/b;

    invoke-direct {v0}, Lcom/twitter/network/apache/message/b;-><init>()V

    sput-object v0, Lcom/twitter/network/apache/message/b;->a:Lcom/twitter/network/apache/message/b;

    .line 55
    new-instance v0, Lcom/twitter/network/apache/message/b;

    invoke-direct {v0}, Lcom/twitter/network/apache/message/b;-><init>()V

    sput-object v0, Lcom/twitter/network/apache/message/b;->b:Lcom/twitter/network/apache/message/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/network/apache/f;)I
    .locals 2

    .prologue
    .line 346
    if-nez p1, :cond_1

    .line 347
    const/4 v0, 0x0

    .line 356
    :cond_0
    :goto_0
    return v0

    .line 350
    :cond_1
    invoke-interface {p1}, Lcom/twitter/network/apache/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 351
    invoke-interface {p1}, Lcom/twitter/network/apache/f;->b()Ljava/lang/String;

    move-result-object v1

    .line 352
    if-eqz v1, :cond_0

    .line 354
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x3

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method protected a([Lcom/twitter/network/apache/f;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 280
    if-eqz p1, :cond_0

    array-length v1, p1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 289
    :cond_0
    return v0

    .line 284
    :cond_1
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    mul-int/lit8 v1, v1, 0x2

    .line 285
    array-length v3, p1

    move v4, v0

    move v0, v1

    move v1, v4

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, p1, v1

    .line 286
    invoke-virtual {p0, v2}, Lcom/twitter/network/apache/message/b;->a(Lcom/twitter/network/apache/f;)I

    move-result v2

    add-int/2addr v2, v0

    .line 285
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0
.end method

.method public a(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/f;Z)Lcom/twitter/network/apache/util/CharArrayBuffer;
    .locals 2

    .prologue
    .line 318
    const-string/jumbo v0, "Name / value pair"

    invoke-static {p2, v0}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 319
    invoke-virtual {p0, p2}, Lcom/twitter/network/apache/message/b;->a(Lcom/twitter/network/apache/f;)I

    move-result v0

    .line 321
    if-nez p1, :cond_1

    .line 322
    new-instance p1, Lcom/twitter/network/apache/util/CharArrayBuffer;

    invoke-direct {p1, v0}, Lcom/twitter/network/apache/util/CharArrayBuffer;-><init>(I)V

    .line 327
    :goto_0
    invoke-interface {p2}, Lcom/twitter/network/apache/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/network/apache/util/CharArrayBuffer;->a(Ljava/lang/String;)V

    .line 328
    invoke-interface {p2}, Lcom/twitter/network/apache/f;->b()Ljava/lang/String;

    move-result-object v0

    .line 329
    if-eqz v0, :cond_0

    .line 330
    const/16 v1, 0x3d

    invoke-virtual {p1, v1}, Lcom/twitter/network/apache/util/CharArrayBuffer;->a(C)V

    .line 331
    invoke-virtual {p0, p1, v0, p3}, Lcom/twitter/network/apache/message/b;->a(Lcom/twitter/network/apache/util/CharArrayBuffer;Ljava/lang/String;Z)V

    .line 334
    :cond_0
    return-object p1

    .line 324
    :cond_1
    invoke-virtual {p1, v0}, Lcom/twitter/network/apache/util/CharArrayBuffer;->a(I)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/network/apache/util/CharArrayBuffer;[Lcom/twitter/network/apache/f;Z)Lcom/twitter/network/apache/util/CharArrayBuffer;
    .locals 2

    .prologue
    .line 252
    const-string/jumbo v0, "Header parameter array"

    invoke-static {p2, v0}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 253
    invoke-virtual {p0, p2}, Lcom/twitter/network/apache/message/b;->a([Lcom/twitter/network/apache/f;)I

    move-result v0

    .line 255
    if-nez p1, :cond_1

    .line 256
    new-instance p1, Lcom/twitter/network/apache/util/CharArrayBuffer;

    invoke-direct {p1, v0}, Lcom/twitter/network/apache/util/CharArrayBuffer;-><init>(I)V

    .line 261
    :goto_0
    const/4 v0, 0x0

    :goto_1
    array-length v1, p2

    if-ge v0, v1, :cond_2

    .line 262
    if-lez v0, :cond_0

    .line 263
    const-string/jumbo v1, "; "

    invoke-virtual {p1, v1}, Lcom/twitter/network/apache/util/CharArrayBuffer;->a(Ljava/lang/String;)V

    .line 265
    :cond_0
    aget-object v1, p2, v0

    invoke-virtual {p0, p1, v1, p3}, Lcom/twitter/network/apache/message/b;->a(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/f;Z)Lcom/twitter/network/apache/util/CharArrayBuffer;

    .line 261
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 258
    :cond_1
    invoke-virtual {p1, v0}, Lcom/twitter/network/apache/util/CharArrayBuffer;->a(I)V

    goto :goto_0

    .line 268
    :cond_2
    return-object p1
.end method

.method protected a(Lcom/twitter/network/apache/util/CharArrayBuffer;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/16 v3, 0x22

    const/4 v1, 0x0

    .line 374
    .line 375
    if-nez p3, :cond_0

    move v0, v1

    .line 376
    :goto_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    if-nez p3, :cond_0

    .line 377
    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, Lcom/twitter/network/apache/message/b;->a(C)Z

    move-result p3

    .line 376
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 381
    :cond_0
    if-eqz p3, :cond_1

    .line 382
    invoke-virtual {p1, v3}, Lcom/twitter/network/apache/util/CharArrayBuffer;->a(C)V

    .line 384
    :cond_1
    :goto_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 385
    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 386
    invoke-virtual {p0, v0}, Lcom/twitter/network/apache/message/b;->b(C)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 387
    const/16 v2, 0x5c

    invoke-virtual {p1, v2}, Lcom/twitter/network/apache/util/CharArrayBuffer;->a(C)V

    .line 389
    :cond_2
    invoke-virtual {p1, v0}, Lcom/twitter/network/apache/util/CharArrayBuffer;->a(C)V

    .line 384
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 391
    :cond_3
    if-eqz p3, :cond_4

    .line 392
    invoke-virtual {p1, v3}, Lcom/twitter/network/apache/util/CharArrayBuffer;->a(C)V

    .line 394
    :cond_4
    return-void
.end method

.method protected a(C)Z
    .locals 1

    .prologue
    .line 406
    const-string/jumbo v0, " ;,:@()<>\\\"/[]?={}\t"

    invoke-virtual {v0, p1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(C)Z
    .locals 1

    .prologue
    .line 419
    const-string/jumbo v0, "\"\\"

    invoke-virtual {v0, p1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
