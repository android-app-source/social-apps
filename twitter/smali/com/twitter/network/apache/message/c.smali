.class public Lcom/twitter/network/apache/message/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/network/apache/message/e;


# static fields
.field public static final a:Lcom/twitter/network/apache/message/c;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final b:Lcom/twitter/network/apache/message/c;

.field private static final c:Ljava/util/BitSet;

.field private static final d:Ljava/util/BitSet;


# instance fields
.field private final e:Lcom/twitter/network/apache/message/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    new-instance v0, Lcom/twitter/network/apache/message/c;

    invoke-direct {v0}, Lcom/twitter/network/apache/message/c;-><init>()V

    sput-object v0, Lcom/twitter/network/apache/message/c;->a:Lcom/twitter/network/apache/message/c;

    .line 60
    new-instance v0, Lcom/twitter/network/apache/message/c;

    invoke-direct {v0}, Lcom/twitter/network/apache/message/c;-><init>()V

    sput-object v0, Lcom/twitter/network/apache/message/c;->b:Lcom/twitter/network/apache/message/c;

    .line 67
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lcom/twitter/network/apache/message/g;->a([I)Ljava/util/BitSet;

    move-result-object v0

    sput-object v0, Lcom/twitter/network/apache/message/c;->c:Ljava/util/BitSet;

    .line 68
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lcom/twitter/network/apache/message/g;->a([I)Ljava/util/BitSet;

    move-result-object v0

    sput-object v0, Lcom/twitter/network/apache/message/c;->d:Ljava/util/BitSet;

    return-void

    .line 67
    nop

    :array_0
    .array-data 4
        0x3d
        0x3b
        0x2c
    .end array-data

    .line 68
    :array_1
    .array-data 4
        0x3b
        0x2c
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    sget-object v0, Lcom/twitter/network/apache/message/g;->a:Lcom/twitter/network/apache/message/g;

    iput-object v0, p0, Lcom/twitter/network/apache/message/c;->e:Lcom/twitter/network/apache/message/g;

    .line 74
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/network/apache/message/e;)[Lcom/twitter/network/apache/d;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/twitter/network/apache/ParseException;
        }
    .end annotation

    .prologue
    .line 88
    const-string/jumbo v0, "Value"

    invoke-static {p0, v0}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 90
    new-instance v0, Lcom/twitter/network/apache/util/CharArrayBuffer;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/twitter/network/apache/util/CharArrayBuffer;-><init>(I)V

    .line 91
    invoke-virtual {v0, p0}, Lcom/twitter/network/apache/util/CharArrayBuffer;->a(Ljava/lang/String;)V

    .line 92
    new-instance v1, Lcom/twitter/network/apache/message/f;

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/twitter/network/apache/message/f;-><init>(II)V

    .line 93
    if-eqz p1, :cond_0

    .line 94
    :goto_0
    invoke-interface {p1, v0, v1}, Lcom/twitter/network/apache/message/e;->a(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;)[Lcom/twitter/network/apache/d;

    move-result-object v0

    .line 93
    return-object v0

    :cond_0
    sget-object p1, Lcom/twitter/network/apache/message/c;->b:Lcom/twitter/network/apache/message/c;

    goto :goto_0
.end method


# virtual methods
.method protected a(Ljava/lang/String;Ljava/lang/String;[Lcom/twitter/network/apache/f;)Lcom/twitter/network/apache/d;
    .locals 1

    .prologue
    .line 164
    new-instance v0, Lcom/twitter/network/apache/message/a;

    invoke-direct {v0, p1, p2, p3}, Lcom/twitter/network/apache/message/a;-><init>(Ljava/lang/String;Ljava/lang/String;[Lcom/twitter/network/apache/f;)V

    return-object v0
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/network/apache/f;
    .locals 1

    .prologue
    .line 297
    new-instance v0, Lcom/twitter/network/apache/message/BasicNameValuePair;

    invoke-direct {v0, p1, p2}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;)[Lcom/twitter/network/apache/d;
    .locals 3

    .prologue
    .line 102
    const-string/jumbo v0, "Char array buffer"

    invoke-static {p1, v0}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 103
    const-string/jumbo v0, "Parser cursor"

    invoke-static {p2, v0}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 105
    :cond_0
    :goto_0
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->c()Z

    move-result v1

    if-nez v1, :cond_2

    .line 106
    invoke-virtual {p0, p1, p2}, Lcom/twitter/network/apache/message/c;->b(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;)Lcom/twitter/network/apache/d;

    move-result-object v1

    .line 107
    invoke-interface {v1}, Lcom/twitter/network/apache/d;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v1}, Lcom/twitter/network/apache/d;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 108
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 111
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/twitter/network/apache/d;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/network/apache/d;

    return-object v0
.end method

.method public b(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;)Lcom/twitter/network/apache/d;
    .locals 4

    .prologue
    .line 140
    const-string/jumbo v0, "Char array buffer"

    invoke-static {p1, v0}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 141
    const-string/jumbo v0, "Parser cursor"

    invoke-static {p2, v0}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 142
    invoke-virtual {p0, p1, p2}, Lcom/twitter/network/apache/message/c;->d(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;)Lcom/twitter/network/apache/f;

    move-result-object v1

    .line 143
    const/4 v0, 0x0

    .line 144
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->c()Z

    move-result v2

    if-nez v2, :cond_0

    .line 145
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p1, v2}, Lcom/twitter/network/apache/util/CharArrayBuffer;->charAt(I)C

    move-result v2

    .line 146
    const/16 v3, 0x2c

    if-eq v2, v3, :cond_0

    .line 147
    invoke-virtual {p0, p1, p2}, Lcom/twitter/network/apache/message/c;->c(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;)[Lcom/twitter/network/apache/f;

    move-result-object v0

    .line 150
    :cond_0
    invoke-interface {v1}, Lcom/twitter/network/apache/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, Lcom/twitter/network/apache/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2, v1, v0}, Lcom/twitter/network/apache/message/c;->a(Ljava/lang/String;Ljava/lang/String;[Lcom/twitter/network/apache/f;)Lcom/twitter/network/apache/d;

    move-result-object v0

    return-object v0
.end method

.method public c(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;)[Lcom/twitter/network/apache/f;
    .locals 3

    .prologue
    .line 194
    const-string/jumbo v0, "Char array buffer"

    invoke-static {p1, v0}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 195
    const-string/jumbo v0, "Parser cursor"

    invoke-static {p2, v0}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 196
    iget-object v0, p0, Lcom/twitter/network/apache/message/c;->e:Lcom/twitter/network/apache/message/g;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/network/apache/message/g;->a(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;)V

    .line 197
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 198
    :cond_0
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 199
    invoke-virtual {p0, p1, p2}, Lcom/twitter/network/apache/message/c;->d(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;)Lcom/twitter/network/apache/f;

    move-result-object v1

    .line 200
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v1}, Lcom/twitter/network/apache/util/CharArrayBuffer;->charAt(I)C

    move-result v1

    .line 202
    const/16 v2, 0x2c

    if-ne v1, v2, :cond_0

    .line 206
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/twitter/network/apache/f;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/network/apache/f;

    return-object v0
.end method

.method public d(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;)Lcom/twitter/network/apache/f;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 234
    const-string/jumbo v0, "Char array buffer"

    invoke-static {p1, v0}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 235
    const-string/jumbo v0, "Parser cursor"

    invoke-static {p2, v0}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 237
    iget-object v0, p0, Lcom/twitter/network/apache/message/c;->e:Lcom/twitter/network/apache/message/g;

    sget-object v1, Lcom/twitter/network/apache/message/c;->c:Ljava/util/BitSet;

    invoke-virtual {v0, p1, p2, v1}, Lcom/twitter/network/apache/message/g;->a(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;Ljava/util/BitSet;)Ljava/lang/String;

    move-result-object v1

    .line 238
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    new-instance v0, Lcom/twitter/network/apache/message/BasicNameValuePair;

    invoke-direct {v0, v1, v3}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    :goto_0
    return-object v0

    .line 241
    :cond_0
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/twitter/network/apache/util/CharArrayBuffer;->charAt(I)C

    move-result v0

    .line 242
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->b()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p2, v2}, Lcom/twitter/network/apache/message/f;->a(I)V

    .line 243
    const/16 v2, 0x3d

    if-eq v0, v2, :cond_1

    .line 244
    invoke-virtual {p0, v1, v3}, Lcom/twitter/network/apache/message/c;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/network/apache/f;

    move-result-object v0

    goto :goto_0

    .line 246
    :cond_1
    iget-object v0, p0, Lcom/twitter/network/apache/message/c;->e:Lcom/twitter/network/apache/message/g;

    sget-object v2, Lcom/twitter/network/apache/message/c;->d:Ljava/util/BitSet;

    invoke-virtual {v0, p1, p2, v2}, Lcom/twitter/network/apache/message/g;->b(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/message/f;Ljava/util/BitSet;)Ljava/lang/String;

    move-result-object v0

    .line 247
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->c()Z

    move-result v2

    if-nez v2, :cond_2

    .line 248
    invoke-virtual {p2}, Lcom/twitter/network/apache/message/f;->b()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p2, v2}, Lcom/twitter/network/apache/message/f;->a(I)V

    .line 250
    :cond_2
    invoke-virtual {p0, v1, v0}, Lcom/twitter/network/apache/message/c;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/network/apache/f;

    move-result-object v0

    goto :goto_0
.end method
