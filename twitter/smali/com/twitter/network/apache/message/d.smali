.class public Lcom/twitter/network/apache/message/d;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lcom/twitter/network/apache/message/d;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final b:Lcom/twitter/network/apache/message/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/twitter/network/apache/message/d;

    invoke-direct {v0}, Lcom/twitter/network/apache/message/d;-><init>()V

    sput-object v0, Lcom/twitter/network/apache/message/d;->a:Lcom/twitter/network/apache/message/d;

    .line 63
    new-instance v0, Lcom/twitter/network/apache/message/d;

    invoke-direct {v0}, Lcom/twitter/network/apache/message/d;-><init>()V

    sput-object v0, Lcom/twitter/network/apache/message/d;->b:Lcom/twitter/network/apache/message/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/network/apache/util/CharArrayBuffer;)Lcom/twitter/network/apache/util/CharArrayBuffer;
    .locals 1

    .prologue
    .line 78
    .line 79
    if-eqz p1, :cond_0

    .line 80
    invoke-virtual {p1}, Lcom/twitter/network/apache/util/CharArrayBuffer;->a()V

    .line 84
    :goto_0
    return-object p1

    .line 82
    :cond_0
    new-instance p1, Lcom/twitter/network/apache/util/CharArrayBuffer;

    const/16 v0, 0x40

    invoke-direct {p1, v0}, Lcom/twitter/network/apache/util/CharArrayBuffer;-><init>(I)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/c;)Lcom/twitter/network/apache/util/CharArrayBuffer;
    .locals 1

    .prologue
    .line 279
    const-string/jumbo v0, "Header"

    invoke-static {p2, v0}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 282
    instance-of v0, p2, Lcom/twitter/network/apache/b;

    if-eqz v0, :cond_0

    .line 284
    check-cast p2, Lcom/twitter/network/apache/b;

    invoke-interface {p2}, Lcom/twitter/network/apache/b;->a()Lcom/twitter/network/apache/util/CharArrayBuffer;

    move-result-object v0

    .line 289
    :goto_0
    return-object v0

    .line 286
    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/network/apache/message/d;->a(Lcom/twitter/network/apache/util/CharArrayBuffer;)Lcom/twitter/network/apache/util/CharArrayBuffer;

    move-result-object v0

    .line 287
    invoke-virtual {p0, v0, p2}, Lcom/twitter/network/apache/message/d;->b(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/c;)V

    goto :goto_0
.end method

.method protected b(Lcom/twitter/network/apache/util/CharArrayBuffer;Lcom/twitter/network/apache/c;)V
    .locals 4

    .prologue
    .line 304
    invoke-interface {p2}, Lcom/twitter/network/apache/c;->b()Ljava/lang/String;

    move-result-object v1

    .line 305
    invoke-interface {p2}, Lcom/twitter/network/apache/c;->c()Ljava/lang/String;

    move-result-object v2

    .line 307
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    .line 308
    if-eqz v2, :cond_0

    .line 309
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v0, v3

    .line 311
    :cond_0
    invoke-virtual {p1, v0}, Lcom/twitter/network/apache/util/CharArrayBuffer;->a(I)V

    .line 313
    invoke-virtual {p1, v1}, Lcom/twitter/network/apache/util/CharArrayBuffer;->a(Ljava/lang/String;)V

    .line 314
    const-string/jumbo v0, ": "

    invoke-virtual {p1, v0}, Lcom/twitter/network/apache/util/CharArrayBuffer;->a(Ljava/lang/String;)V

    .line 315
    if-eqz v2, :cond_1

    .line 316
    invoke-virtual {p1, v2}, Lcom/twitter/network/apache/util/CharArrayBuffer;->a(Ljava/lang/String;)V

    .line 318
    :cond_1
    return-void
.end method
