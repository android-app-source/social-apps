.class public Lcom/twitter/network/apache/entity/b;
.super Lcom/twitter/network/apache/entity/a;
.source "Twttr"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field protected final d:[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final e:[B

.field private final f:I

.field private final g:I


# direct methods
.method public constructor <init>([B)V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/network/apache/entity/b;-><init>([BLcom/twitter/network/apache/entity/ContentType;)V

    .line 90
    return-void
.end method

.method public constructor <init>([BLcom/twitter/network/apache/entity/ContentType;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/twitter/network/apache/entity/a;-><init>()V

    .line 58
    const-string/jumbo v0, "Source byte array"

    invoke-static {p1, v0}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 59
    iput-object p1, p0, Lcom/twitter/network/apache/entity/b;->d:[B

    .line 60
    iput-object p1, p0, Lcom/twitter/network/apache/entity/b;->e:[B

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/network/apache/entity/b;->f:I

    .line 62
    iget-object v0, p0, Lcom/twitter/network/apache/entity/b;->e:[B

    array-length v0, v0

    iput v0, p0, Lcom/twitter/network/apache/entity/b;->g:I

    .line 63
    if-eqz p2, :cond_0

    .line 64
    invoke-virtual {p2}, Lcom/twitter/network/apache/entity/ContentType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/network/apache/entity/b;->a(Ljava/lang/String;)V

    .line 66
    :cond_0
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 103
    iget v0, p0, Lcom/twitter/network/apache/entity/b;->g:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public a(Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    const-string/jumbo v0, "Output stream"

    invoke-static {p1, v0}, Lcom/twitter/network/apache/util/a;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 114
    iget-object v0, p0, Lcom/twitter/network/apache/entity/b;->e:[B

    iget v1, p0, Lcom/twitter/network/apache/entity/b;->f:I

    iget v2, p0, Lcom/twitter/network/apache/entity/b;->g:I

    invoke-virtual {p1, v0, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 115
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 116
    return-void
.end method

.method public b()Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 108
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/twitter/network/apache/entity/b;->e:[B

    iget v2, p0, Lcom/twitter/network/apache/entity/b;->f:I

    iget v3, p0, Lcom/twitter/network/apache/entity/b;->g:I

    invoke-direct {v0, v1, v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    return v0
.end method

.method public clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 131
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
