.class public abstract Lcom/twitter/analytics/model/ScribeLog;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcpk;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/analytics/model/ScribeLog$SearchDetails;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TScribe",
        "Log:Lcom/twitter/analytics/model/ScribeLog",
        "<TTScribe",
        "Log;",
        ">;>",
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Lcpk;"
    }
.end annotation


# static fields
.field private static final a:Lcom/fasterxml/jackson/core/JsonFactory;

.field private static final b:Lcok$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcok$a",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Z

.field private D:Ljava/lang/String;

.field private E:I

.field private F:[B

.field private G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:Ljava/lang/String;

.field private L:Ljava/lang/String;

.field private M:Ljava/lang/String;

.field private N:Z

.field private O:Ljava/lang/String;

.field private P:J

.field private Q:Ljava/lang/String;

.field private R:Ljava/lang/String;

.field private S:Ljava/lang/String;

.field private T:I

.field private U:I

.field private V:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/model/ScribeItem;",
            ">;"
        }
    .end annotation
.end field

.field private W:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/model/ScribeAssociation;",
            ">;"
        }
    .end annotation
.end field

.field private X:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private Y:Z

.field private Z:I

.field private aa:Ljava/lang/String;

.field private ab:Ljava/lang/String;

.field private ac:Ljava/lang/String;

.field private ad:Ljava/lang/String;

.field private ae:Ljava/lang/String;

.field private af:Ljava/lang/String;

.field private ag:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:I

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/model/ScribeItem;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/model/ScribeSection;",
            ">;"
        }
    .end annotation
.end field

.field private final i:J

.field private j:J

.field private k:I

.field private l:I

.field private m:Ljava/lang/String;

.field private n:I

.field private o:I

.field private p:Ljava/lang/String;

.field private q:Lcnz;

.field private r:I

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Lcom/twitter/analytics/model/ScribeLog$SearchDetails;

.field private y:I

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 139
    new-instance v0, Lcom/fasterxml/jackson/core/JsonFactory;

    invoke-direct {v0}, Lcom/fasterxml/jackson/core/JsonFactory;-><init>()V

    sput-object v0, Lcom/twitter/analytics/model/ScribeLog;->a:Lcom/fasterxml/jackson/core/JsonFactory;

    .line 141
    const-string/jumbo v0, "disable_client_event_association_reporting"

    const/4 v1, 0x0

    .line 143
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 142
    invoke-static {v0, v1}, Lcok;->a(Ljava/lang/String;Ljava/lang/Object;)Lcok$a;

    move-result-object v0

    sput-object v0, Lcom/twitter/analytics/model/ScribeLog;->b:Lcok$a;

    .line 141
    return-void
.end method

.method protected constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183
    iput v2, p0, Lcom/twitter/analytics/model/ScribeLog;->d:I

    .line 193
    const v0, 0x7fffffff

    iput v0, p0, Lcom/twitter/analytics/model/ScribeLog;->f:I

    .line 198
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->g:Ljava/util/List;

    .line 200
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->h:Ljava/util/List;

    .line 210
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/analytics/model/ScribeLog;->j:J

    .line 215
    iput v2, p0, Lcom/twitter/analytics/model/ScribeLog;->k:I

    .line 220
    iput v2, p0, Lcom/twitter/analytics/model/ScribeLog;->l:I

    .line 287
    iput v2, p0, Lcom/twitter/analytics/model/ScribeLog;->y:I

    .line 401
    iput v2, p0, Lcom/twitter/analytics/model/ScribeLog;->T:I

    .line 406
    iput v2, p0, Lcom/twitter/analytics/model/ScribeLog;->U:I

    .line 429
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/analytics/model/ScribeLog;->Y:Z

    .line 431
    iput v2, p0, Lcom/twitter/analytics/model/ScribeLog;->Z:I

    .line 442
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/model/ScribeLog;->i:J

    .line 443
    iput v2, p0, Lcom/twitter/analytics/model/ScribeLog;->r:I

    .line 444
    const-string/jumbo v0, "client_event"

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->z:Ljava/lang/String;

    .line 445
    return-void
.end method

.method protected constructor <init>(J)V
    .locals 1

    .prologue
    .line 463
    invoke-direct {p0}, Lcom/twitter/analytics/model/ScribeLog;-><init>()V

    .line 464
    new-instance v0, Lcnz;

    invoke-direct {v0, p1, p2}, Lcnz;-><init>(J)V

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->q:Lcnz;

    .line 465
    return-void
.end method

.method protected constructor <init>(JLcom/twitter/analytics/model/a;)V
    .locals 1

    .prologue
    .line 473
    invoke-direct {p0, p1, p2}, Lcom/twitter/analytics/model/ScribeLog;-><init>(J)V

    .line 474
    invoke-virtual {p0, p3}, Lcom/twitter/analytics/model/ScribeLog;->a(Lcom/twitter/analytics/model/a;)Lcom/twitter/analytics/model/ScribeLog;

    .line 475
    return-void
.end method

.method protected varargs constructor <init>(J[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 468
    invoke-direct {p0, p1, p2}, Lcom/twitter/analytics/model/ScribeLog;-><init>(J)V

    .line 469
    invoke-static {p3}, Lcom/twitter/analytics/model/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->Q:Ljava/lang/String;

    .line 470
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183
    iput v3, p0, Lcom/twitter/analytics/model/ScribeLog;->d:I

    .line 193
    const v0, 0x7fffffff

    iput v0, p0, Lcom/twitter/analytics/model/ScribeLog;->f:I

    .line 198
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->g:Ljava/util/List;

    .line 200
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->h:Ljava/util/List;

    .line 210
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/twitter/analytics/model/ScribeLog;->j:J

    .line 215
    iput v3, p0, Lcom/twitter/analytics/model/ScribeLog;->k:I

    .line 220
    iput v3, p0, Lcom/twitter/analytics/model/ScribeLog;->l:I

    .line 287
    iput v3, p0, Lcom/twitter/analytics/model/ScribeLog;->y:I

    .line 401
    iput v3, p0, Lcom/twitter/analytics/model/ScribeLog;->T:I

    .line 406
    iput v3, p0, Lcom/twitter/analytics/model/ScribeLog;->U:I

    .line 429
    iput-boolean v2, p0, Lcom/twitter/analytics/model/ScribeLog;->Y:Z

    .line 431
    iput v3, p0, Lcom/twitter/analytics/model/ScribeLog;->Z:I

    .line 478
    sget-object v0, Lcnz;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnz;

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->q:Lcnz;

    .line 479
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->z:Ljava/lang/String;

    .line 480
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->Q:Ljava/lang/String;

    .line 481
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->R:Ljava/lang/String;

    .line 482
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/analytics/model/ScribeLog;->i:J

    .line 483
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->p:Ljava/lang/String;

    .line 484
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->e:Ljava/lang/String;

    .line 485
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->S:Ljava/lang/String;

    .line 486
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->m:Ljava/lang/String;

    .line 487
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/model/ScribeLog;->n:I

    .line 488
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/model/ScribeLog;->o:I

    .line 489
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/analytics/model/ScribeLog;->j:J

    .line 490
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/model/ScribeLog;->k:I

    .line 491
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/model/ScribeLog;->l:I

    .line 492
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/model/ScribeLog;->r:I

    .line 493
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->s:Ljava/lang/String;

    .line 494
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->t:Ljava/lang/String;

    .line 495
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->c:Ljava/lang/String;

    .line 496
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->v:Ljava/lang/String;

    .line 497
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->w:Ljava/lang/String;

    .line 498
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/model/ScribeLog;->y:I

    .line 499
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->A:Ljava/lang/String;

    .line 500
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->B:Ljava/lang/String;

    .line 501
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->D:Ljava/lang/String;

    .line 502
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/model/ScribeLog;->E:I

    .line 503
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 504
    if-eq v0, v3, :cond_0

    .line 505
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->F:[B

    .line 506
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->F:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 508
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->G:Ljava/lang/String;

    .line 509
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/model/ScribeLog;->d:I

    .line 510
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/model/ScribeLog;->f:I

    .line 511
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->H:Ljava/lang/String;

    .line 512
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->I:Ljava/lang/String;

    .line 513
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->J:Ljava/lang/String;

    .line 514
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->K:Ljava/lang/String;

    .line 515
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->L:Ljava/lang/String;

    .line 516
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->M:Ljava/lang/String;

    .line 517
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/analytics/model/ScribeLog;->N:Z

    .line 518
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->O:Ljava/lang/String;

    .line 519
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/analytics/model/ScribeLog;->P:J

    .line 520
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->u:Ljava/lang/String;

    .line 521
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->h:Ljava/util/List;

    const-class v3, Lcom/twitter/analytics/model/ScribeSection;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 522
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->g:Ljava/util/List;

    const-class v3, Lcom/twitter/analytics/model/ScribeItem;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 523
    const-class v0, Lcom/twitter/analytics/model/ScribeAssociation;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->W:Ljava/util/List;

    .line 524
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 525
    if-lez v3, :cond_2

    move v0, v2

    .line 526
    :goto_1
    if-ge v0, v3, :cond_2

    .line 527
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/twitter/analytics/model/ScribeLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 526
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v2

    .line 517
    goto :goto_0

    .line 530
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/model/ScribeLog;->Z:I

    .line 531
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->af:Ljava/lang/String;

    .line 532
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->aa:Ljava/lang/String;

    .line 533
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->ab:Ljava/lang/String;

    .line 534
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->ac:Ljava/lang/String;

    .line 535
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->ad:Ljava/lang/String;

    .line 536
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->ae:Ljava/lang/String;

    .line 537
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/model/ScribeLog;->T:I

    .line 538
    const-class v0, Lcom/twitter/analytics/model/ScribeLog$SearchDetails;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog$SearchDetails;

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->x:Lcom/twitter/analytics/model/ScribeLog$SearchDetails;

    .line 539
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/model/ScribeLog;->U:I

    .line 540
    const-class v0, Lcom/twitter/analytics/model/ScribeItem;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->V:Ljava/util/List;

    .line 541
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->ag:Ljava/lang/String;

    .line 542
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    :goto_2
    iput-boolean v1, p0, Lcom/twitter/analytics/model/ScribeLog;->C:Z

    .line 543
    return-void

    :cond_3
    move v1, v2

    .line 542
    goto :goto_2
.end method

.method protected varargs constructor <init>([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 448
    invoke-direct {p0}, Lcom/twitter/analytics/model/ScribeLog;-><init>()V

    .line 449
    invoke-static {p1}, Lcom/twitter/analytics/model/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->Q:Ljava/lang/String;

    .line 450
    return-void
.end method

.method public static varargs a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 566
    if-eqz p1, :cond_0

    array-length v1, p1

    if-nez v1, :cond_1

    .line 567
    :cond_0
    const-string/jumbo v0, ""

    .line 583
    :goto_0
    return-object v0

    .line 568
    :cond_1
    array-length v1, p1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 569
    aget-object v0, p1, v0

    goto :goto_0

    .line 571
    :cond_2
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 572
    array-length v1, p1

    .line 573
    :goto_1
    if-ge v0, v1, :cond_5

    .line 574
    aget-object v2, p1, v0

    if-nez v2, :cond_4

    .line 575
    const-string/jumbo v2, ""

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 579
    :goto_2
    add-int/lit8 v2, v1, -0x1

    if-ge v0, v2, :cond_3

    .line 580
    const/16 v2, 0x3a

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 573
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 577
    :cond_4
    aget-object v2, p1, v0

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 583
    :cond_5
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static varargs a([Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 551
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    .line 552
    :cond_0
    const-string/jumbo v0, ""

    .line 556
    :goto_0
    return-object v0

    .line 553
    :cond_1
    array-length v0, p0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 554
    const/4 v0, 0x0

    aget-object v0, p0, v0

    goto :goto_0

    .line 556
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0, p0}, Lcom/twitter/analytics/model/ScribeLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v6, -0x1

    .line 686
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 687
    const-string/jumbo v0, "_category_"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->z:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    const-string/jumbo v0, "format_version"

    const/4 v1, 0x2

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 689
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 690
    const-string/jumbo v0, "client_version"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    :cond_0
    const-string/jumbo v0, "client_event"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "app_download_client_event"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "asset_prefetching_event"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->z:Ljava/lang/String;

    .line 694
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "live_video_heartbeat_event"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 695
    :cond_1
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->R:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 696
    const-string/jumbo v0, "referring_event"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "android:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/analytics/model/ScribeLog;->R:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    :cond_2
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->Q:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 699
    const-string/jumbo v0, "event_name"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "android:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/analytics/model/ScribeLog;->Q:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    :cond_3
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->S:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 702
    const-string/jumbo v0, "limit_ad_tracking"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->S:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 704
    :cond_4
    const-string/jumbo v0, "ts"

    iget-wide v4, p0, Lcom/twitter/analytics/model/ScribeLog;->i:J

    invoke-virtual {p1, v0, v4, v5}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 705
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->e:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 706
    const-string/jumbo v0, "server"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    :cond_5
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->m:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 709
    const-string/jumbo v0, "protocol"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    :cond_6
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->n:I

    if-ltz v0, :cond_7

    .line 712
    const-string/jumbo v0, "stream_id"

    iget v1, p0, Lcom/twitter/analytics/model/ScribeLog;->n:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 714
    :cond_7
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->o:I

    if-ltz v0, :cond_8

    .line 715
    const-string/jumbo v0, "content_length"

    iget v1, p0, Lcom/twitter/analytics/model/ScribeLog;->o:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 717
    :cond_8
    iget-wide v0, p0, Lcom/twitter/analytics/model/ScribeLog;->j:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_9

    .line 718
    const-string/jumbo v0, "duration_ms"

    iget-wide v4, p0, Lcom/twitter/analytics/model/ScribeLog;->j:J

    invoke-virtual {p1, v0, v4, v5}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 720
    :cond_9
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->k:I

    if-eq v0, v6, :cond_a

    .line 721
    const-string/jumbo v0, "status_code"

    iget v1, p0, Lcom/twitter/analytics/model/ScribeLog;->k:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 723
    :cond_a
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->l:I

    if-eq v0, v6, :cond_b

    .line 724
    const-string/jumbo v0, "failure_type"

    iget v1, p0, Lcom/twitter/analytics/model/ScribeLog;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 726
    :cond_b
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->t:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 727
    const-string/jumbo v0, "message"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    :cond_c
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->c:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 730
    const-string/jumbo v0, "event_info"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    :cond_d
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->v:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 733
    const-string/jumbo v0, "impression_id"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->v:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    :cond_e
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->u:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 736
    const-string/jumbo v0, "event_value"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->u:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 738
    :cond_f
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->p:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 739
    const-string/jumbo v0, "url"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    :cond_10
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->r:I

    if-eq v0, v6, :cond_11

    .line 742
    const-string/jumbo v0, "event_initiator"

    iget v1, p0, Lcom/twitter/analytics/model/ScribeLog;->r:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 744
    :cond_11
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->w:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 745
    const-string/jumbo v0, "query"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->w:Ljava/lang/String;

    const-string/jumbo v3, "UTF-8"

    invoke-static {v1, v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    :cond_12
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->y:I

    if-eq v0, v6, :cond_13

    .line 748
    const-string/jumbo v0, "position"

    iget v1, p0, Lcom/twitter/analytics/model/ScribeLog;->y:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 750
    :cond_13
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->A:Ljava/lang/String;

    if-eqz v0, :cond_14

    .line 751
    const-string/jumbo v0, "context"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->A:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 753
    :cond_14
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->B:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 754
    const-string/jumbo v0, "profile_id"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    :cond_15
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->G:Ljava/lang/String;

    if-eqz v0, :cond_16

    .line 757
    const-string/jumbo v0, "orientation"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->G:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    :cond_16
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->d:I

    if-eq v0, v6, :cond_17

    .line 760
    const-string/jumbo v0, "network_status"

    iget v1, p0, Lcom/twitter/analytics/model/ScribeLog;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 762
    :cond_17
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->f:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_18

    .line 763
    const-string/jumbo v0, "signal_strength"

    iget v1, p0, Lcom/twitter/analytics/model/ScribeLog;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 765
    :cond_18
    const/4 v0, 0x0

    .line 766
    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->H:Ljava/lang/String;

    if-eqz v1, :cond_19

    .line 767
    const-string/jumbo v0, "mobile_network_operator_iso_country_code"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->H:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 770
    :cond_19
    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->I:Ljava/lang/String;

    if-eqz v1, :cond_1a

    .line 771
    const-string/jumbo v0, "mobile_network_operator_code"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->I:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 774
    :cond_1a
    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->J:Ljava/lang/String;

    if-eqz v1, :cond_1b

    .line 775
    const-string/jumbo v0, "mobile_network_operator_name"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->J:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 778
    :cond_1b
    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->K:Ljava/lang/String;

    if-eqz v1, :cond_1c

    .line 779
    const-string/jumbo v0, "mobile_sim_provider_iso_country_code"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->K:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 782
    :cond_1c
    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->L:Ljava/lang/String;

    if-eqz v1, :cond_1d

    .line 783
    const-string/jumbo v0, "mobile_sim_provider_code"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->L:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 786
    :cond_1d
    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->M:Ljava/lang/String;

    if-eqz v1, :cond_1e

    .line 787
    const-string/jumbo v0, "mobile_sim_provider_name"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->M:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 790
    :cond_1e
    if-eqz v0, :cond_1f

    .line 791
    const-string/jumbo v0, "is_roaming"

    iget-boolean v1, p0, Lcom/twitter/analytics/model/ScribeLog;->N:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 793
    :cond_1f
    invoke-direct {p0, p1}, Lcom/twitter/analytics/model/ScribeLog;->c(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 794
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->x:Lcom/twitter/analytics/model/ScribeLog$SearchDetails;

    if-eqz v0, :cond_20

    .line 795
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->x:Lcom/twitter/analytics/model/ScribeLog$SearchDetails;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/model/ScribeLog$SearchDetails;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 797
    :cond_20
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_22

    .line 798
    const-string/jumbo v0, "items"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->d(Ljava/lang/String;)V

    .line 799
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeItem;

    .line 800
    invoke-virtual {v0, p1}, Lcom/twitter/analytics/model/ScribeItem;->b(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    goto :goto_0

    .line 802
    :cond_21
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->b()V

    .line 804
    :cond_22
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->W:Ljava/util/List;

    if-eqz v0, :cond_24

    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->W:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_24

    invoke-static {}, Lcom/twitter/analytics/model/ScribeLog;->k()Z

    move-result v0

    if-eqz v0, :cond_24

    .line 805
    const-string/jumbo v0, "associations"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->e(Ljava/lang/String;)V

    .line 806
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->W:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeAssociation;

    .line 807
    invoke-virtual {v0, p1}, Lcom/twitter/analytics/model/ScribeAssociation;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    goto :goto_1

    .line 809
    :cond_23
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 811
    :cond_24
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->X:Ljava/util/Map;

    if-eqz v0, :cond_26

    .line 812
    const-string/jumbo v0, "external_ids"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->e(Ljava/lang/String;)V

    .line 813
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->X:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 814
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 816
    :cond_25
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 818
    :cond_26
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->Z:I

    if-eq v0, v6, :cond_27

    .line 819
    const-string/jumbo v0, "referral_type"

    iget v1, p0, Lcom/twitter/analytics/model/ScribeLog;->Z:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 821
    :cond_27
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->aa:Ljava/lang/String;

    if-eqz v0, :cond_28

    .line 822
    const-string/jumbo v0, "medium"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->aa:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 824
    :cond_28
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->ab:Ljava/lang/String;

    if-eqz v0, :cond_29

    .line 825
    const-string/jumbo v0, "campaign"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->ab:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 827
    :cond_29
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->ac:Ljava/lang/String;

    if-eqz v0, :cond_2a

    .line 828
    const-string/jumbo v0, "query_term"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->ac:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 830
    :cond_2a
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->ad:Ljava/lang/String;

    if-eqz v0, :cond_2b

    .line 831
    const-string/jumbo v0, "campaign_content"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->ad:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 833
    :cond_2b
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->ae:Ljava/lang/String;

    if-eqz v0, :cond_2c

    .line 834
    const-string/jumbo v0, "gclid"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->ae:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    :cond_2c
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->af:Ljava/lang/String;

    if-eqz v0, :cond_2d

    .line 837
    const-string/jumbo v0, "source"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->af:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 840
    const-string/jumbo v0, "external_referer"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->af:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 842
    :cond_2d
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->T:I

    if-eq v0, v6, :cond_2e

    .line 843
    const-string/jumbo v0, "cursor_or_page"

    iget v1, p0, Lcom/twitter/analytics/model/ScribeLog;->T:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 845
    :cond_2e
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->U:I

    if-eq v0, v6, :cond_2f

    .line 846
    const-string/jumbo v0, "item_count"

    iget v1, p0, Lcom/twitter/analytics/model/ScribeLog;->U:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 848
    :cond_2f
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->V:Ljava/util/List;

    if-eqz v0, :cond_31

    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->V:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_31

    .line 849
    const-string/jumbo v0, "targets"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->d(Ljava/lang/String;)V

    .line 850
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->V:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_30

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeItem;

    .line 851
    invoke-virtual {v0, p1}, Lcom/twitter/analytics/model/ScribeItem;->b(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    goto :goto_3

    .line 853
    :cond_30
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->b()V

    .line 855
    :cond_31
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->ag:Ljava/lang/String;

    if-eqz v0, :cond_32

    .line 856
    const-string/jumbo v0, "website_dest_url"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->ag:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 858
    :cond_32
    iget-boolean v0, p0, Lcom/twitter/analytics/model/ScribeLog;->C:Z

    if-eqz v0, :cond_33

    .line 859
    const-string/jumbo v0, "business_profile"

    invoke-virtual {p1, v0, v2}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 881
    :cond_33
    :goto_4
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeSection;

    .line 882
    invoke-virtual {v0, p1}, Lcom/twitter/analytics/model/ScribeSection;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    goto :goto_5

    .line 861
    :cond_34
    const-string/jumbo v0, "client_watch_error"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->E:I

    if-lez v0, :cond_33

    .line 862
    const-string/jumbo v0, "product_name"

    const-string/jumbo v1, "android"

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 863
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->E:I

    packed-switch v0, :pswitch_data_0

    .line 876
    :goto_6
    const-string/jumbo v0, "error"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->D:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->F:[B

    if-eqz v0, :cond_33

    .line 878
    const-string/jumbo v0, "error_details"

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/analytics/model/ScribeLog;->F:[B

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 865
    :pswitch_0
    const-string/jumbo v0, "type"

    const-string/jumbo v1, "crash"

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 869
    :pswitch_1
    const-string/jumbo v0, "type"

    const-string/jumbo v1, "error"

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 884
    :cond_35
    invoke-virtual {p0, p1}, Lcom/twitter/analytics/model/ScribeLog;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 885
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 886
    return-void

    .line 863
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private c(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 889
    iget-wide v0, p0, Lcom/twitter/analytics/model/ScribeLog;->P:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->O:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 900
    :goto_0
    return-void

    .line 892
    :cond_0
    const-string/jumbo v0, "sms_delivery_details"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->e(Ljava/lang/String;)V

    .line 893
    iget-wide v0, p0, Lcom/twitter/analytics/model/ScribeLog;->P:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 894
    const-string/jumbo v0, "time_since"

    iget-wide v2, p0, Lcom/twitter/analytics/model/ScribeLog;->P:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 896
    :cond_1
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->O:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 897
    const-string/jumbo v0, "phone_number"

    iget-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->O:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 899
    :cond_2
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    goto :goto_0
.end method

.method public static h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1377
    const-string/jumbo v0, "android"

    return-object v0
.end method

.method private static k()Z
    .locals 1

    .prologue
    .line 1391
    sget-object v0, Lcom/twitter/analytics/model/ScribeLog;->b:Lcok$a;

    invoke-virtual {v0}, Lcok$a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom;

    invoke-virtual {v0}, Lcom;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1274
    iput p1, p0, Lcom/twitter/analytics/model/ScribeLog;->Z:I

    .line 1275
    iput-object p2, p0, Lcom/twitter/analytics/model/ScribeLog;->af:Ljava/lang/String;

    .line 1276
    iput-object p3, p0, Lcom/twitter/analytics/model/ScribeLog;->aa:Ljava/lang/String;

    .line 1277
    iput-object p4, p0, Lcom/twitter/analytics/model/ScribeLog;->ab:Ljava/lang/String;

    .line 1278
    iput-object p5, p0, Lcom/twitter/analytics/model/ScribeLog;->ac:Ljava/lang/String;

    .line 1279
    iput-object p6, p0, Lcom/twitter/analytics/model/ScribeLog;->ad:Ljava/lang/String;

    .line 1280
    iput-object p7, p0, Lcom/twitter/analytics/model/ScribeLog;->ae:Ljava/lang/String;

    .line 1281
    iput-object p8, p0, Lcom/twitter/analytics/model/ScribeLog;->t:Ljava/lang/String;

    .line 1282
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public a(Landroid/content/Context;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1350
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/analytics/model/ScribeLog;->a(Landroid/content/Context;Z)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;Z)Lcom/twitter/analytics/model/ScribeLog;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Z)TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1355
    const-string/jumbo v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1356
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->H:Ljava/lang/String;

    .line 1357
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->I:Ljava/lang/String;

    .line 1358
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->J:Ljava/lang/String;

    .line 1359
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->K:Ljava/lang/String;

    .line 1360
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->L:Ljava/lang/String;

    .line 1361
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperatorName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/analytics/model/ScribeLog;->M:Ljava/lang/String;

    .line 1362
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/analytics/model/ScribeLog;->N:Z

    .line 1363
    if-eqz p2, :cond_0

    .line 1364
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->O:Ljava/lang/String;

    .line 1366
    :cond_0
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public a(Lcnz;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcnz;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 944
    iput-object p1, p0, Lcom/twitter/analytics/model/ScribeLog;->q:Lcnz;

    .line 945
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/analytics/model/ScribeAssociation;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1193
    if-eqz p1, :cond_1

    .line 1194
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->W:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1195
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->W:Ljava/util/List;

    .line 1197
    :cond_0
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->W:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199
    :cond_1
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/analytics/model/ScribeItem;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1164
    if-eqz p1, :cond_0

    .line 1165
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1167
    :cond_0
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public a(Lcom/twitter/analytics/model/ScribeLog$SearchDetails;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/analytics/model/ScribeLog$SearchDetails;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1054
    iput-object p1, p0, Lcom/twitter/analytics/model/ScribeLog;->x:Lcom/twitter/analytics/model/ScribeLog$SearchDetails;

    .line 1055
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public a(Lcom/twitter/analytics/model/ScribeSection;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/analytics/model/ScribeSection;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1287
    const-string/jumbo v0, "extended_media_details"

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->z:Ljava/lang/String;

    .line 1288
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1289
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public a(Lcom/twitter/analytics/model/a;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/analytics/model/a;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 960
    invoke-virtual {p1}, Lcom/twitter/analytics/model/a;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->Q:Ljava/lang/String;

    .line 961
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/analytics/model/ScribeLog;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZ)TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1048
    new-instance v0, Lcom/twitter/analytics/model/ScribeLog$SearchDetails;

    const/4 v3, 0x0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/analytics/model/ScribeLog$SearchDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->x:Lcom/twitter/analytics/model/ScribeLog$SearchDetails;

    .line 1049
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public a(Ljava/lang/Throwable;ILjava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1219
    const-string/jumbo v0, "client_watch_error"

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->z:Ljava/lang/String;

    .line 1220
    iput p2, p0, Lcom/twitter/analytics/model/ScribeLog;->E:I

    .line 1221
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->D:Ljava/lang/String;

    .line 1222
    invoke-static {p1}, Lcqj;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->F:[B

    .line 1223
    iput-object p3, p0, Lcom/twitter/analytics/model/ScribeLog;->c:Ljava/lang/String;

    .line 1224
    iput-object p4, p0, Lcom/twitter/analytics/model/ScribeLog;->s:Ljava/lang/String;

    .line 1225
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public a(Ljava/util/Collection;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/twitter/analytics/model/ScribeItem;",
            ">;)TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1150
    if-eqz p1, :cond_0

    .line 1151
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1153
    :cond_0
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public a(Z)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1114
    if-eqz p1, :cond_0

    const-string/jumbo v0, "1"

    :goto_0
    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->S:Ljava/lang/String;

    .line 1115
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0

    .line 1114
    :cond_0
    const-string/jumbo v0, "0"

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 904
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->Q:Ljava/lang/String;

    return-object v0
.end method

.method protected abstract a(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public a(Ljava/io/OutputStream;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 663
    .line 666
    :try_start_0
    new-instance v0, Ljava/io/OutputStreamWriter;

    invoke-direct {v0, p1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 667
    :try_start_1
    sget-object v2, Lcom/twitter/analytics/model/ScribeLog;->a:Lcom/fasterxml/jackson/core/JsonFactory;

    invoke-virtual {v2, p1}, Lcom/fasterxml/jackson/core/JsonFactory;->a(Ljava/io/OutputStream;)Lcom/fasterxml/jackson/core/JsonGenerator;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 668
    :try_start_2
    invoke-direct {p0, v1}, Lcom/twitter/analytics/model/ScribeLog;->b(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 669
    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 672
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 673
    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 675
    :goto_0
    return-void

    .line 670
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 672
    :goto_1
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 673
    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 672
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_2
    invoke-static {v2}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 673
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    throw v0

    .line 672
    :catchall_1
    move-exception v2

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v3

    goto :goto_2

    :catchall_2
    move-exception v2

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v3

    goto :goto_2

    .line 670
    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method public b(I)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 982
    iput p1, p0, Lcom/twitter/analytics/model/ScribeLog;->k:I

    .line 983
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public b(J)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1030
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->u:Ljava/lang/String;

    .line 1031
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public b(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/analytics/model/ScribeItem;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1182
    if-eqz p1, :cond_1

    .line 1183
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->V:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1184
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->V:Ljava/util/List;

    .line 1186
    :cond_0
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->V:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1188
    :cond_1
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1096
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->m:Ljava/lang/String;

    .line 1097
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public b(Ljava/util/List;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/twitter/analytics/model/ScribeItem;",
            ">;)TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1237
    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/analytics/model/ScribeLog;->r:I

    .line 1238
    if-eqz p1, :cond_0

    .line 1239
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeItem;

    .line 1240
    invoke-virtual {p0, v0}, Lcom/twitter/analytics/model/ScribeLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_0

    .line 1243
    :cond_0
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public b(Z)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1144
    iput-boolean p1, p0, Lcom/twitter/analytics/model/ScribeLog;->C:Z

    .line 1145
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public varargs b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 954
    invoke-static {p1}, Lcom/twitter/analytics/model/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->Q:Ljava/lang/String;

    .line 955
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 909
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->R:Ljava/lang/String;

    return-object v0
.end method

.method public c(I)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 988
    iput p1, p0, Lcom/twitter/analytics/model/ScribeLog;->l:I

    .line 989
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public c(J)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1132
    iput-wide p1, p0, Lcom/twitter/analytics/model/ScribeLog;->j:J

    .line 1133
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 970
    iput-object p1, p0, Lcom/twitter/analytics/model/ScribeLog;->R:Ljava/lang/String;

    .line 971
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1204
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->X:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 1205
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->X:Ljava/util/Map;

    .line 1207
    :cond_0
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->X:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1208
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 929
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->q:Lcnz;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Lcnz;
    .locals 1

    .prologue
    .line 934
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->q:Lcnz;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnz;

    return-object v0
.end method

.method public d(I)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1000
    iput p1, p0, Lcom/twitter/analytics/model/ScribeLog;->r:I

    .line 1001
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public d(J)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1254
    iput-wide p1, p0, Lcom/twitter/analytics/model/ScribeLog;->j:J

    .line 1255
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 994
    iput-object p1, p0, Lcom/twitter/analytics/model/ScribeLog;->p:Ljava/lang/String;

    .line 995
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1230
    iput-object p1, p0, Lcom/twitter/analytics/model/ScribeLog;->p:Ljava/lang/String;

    .line 1231
    iput-object p2, p0, Lcom/twitter/analytics/model/ScribeLog;->t:Ljava/lang/String;

    .line 1232
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public e()Lcom/twitter/analytics/model/ScribeItem;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Lcom/twitter/analytics/model/ScribeItem;",
            ">()TI;"
        }
    .end annotation

    .prologue
    .line 1172
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->g:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeItem;

    return-object v0
.end method

.method public e(I)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1060
    iput p1, p0, Lcom/twitter/analytics/model/ScribeLog;->d:I

    .line 1061
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public e(J)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1371
    iput-wide p1, p0, Lcom/twitter/analytics/model/ScribeLog;->P:J

    .line 1372
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1006
    iput-object p1, p0, Lcom/twitter/analytics/model/ScribeLog;->s:Ljava/lang/String;

    .line 1007
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public f(I)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1066
    iput p1, p0, Lcom/twitter/analytics/model/ScribeLog;->f:I

    .line 1067
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1012
    iput-object p1, p0, Lcom/twitter/analytics/model/ScribeLog;->t:Ljava/lang/String;

    .line 1013
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Lcom/twitter/analytics/model/ScribeItem;",
            ">()",
            "Ljava/util/List",
            "<TI;>;"
        }
    .end annotation

    .prologue
    .line 1177
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->g:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 1345
    iget-wide v0, p0, Lcom/twitter/analytics/model/ScribeLog;->i:J

    return-wide v0
.end method

.method public g(I)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1072
    iput p1, p0, Lcom/twitter/analytics/model/ScribeLog;->y:I

    .line 1073
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public g(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1018
    iput-object p1, p0, Lcom/twitter/analytics/model/ScribeLog;->v:Ljava/lang/String;

    .line 1019
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public h(I)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1126
    iput p1, p0, Lcom/twitter/analytics/model/ScribeLog;->U:I

    .line 1127
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1024
    iput-object p1, p0, Lcom/twitter/analytics/model/ScribeLog;->c:Ljava/lang/String;

    .line 1025
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public i()Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1382
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/analytics/model/ScribeLog;->Y:Z

    .line 1383
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public i(I)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1260
    iput p1, p0, Lcom/twitter/analytics/model/ScribeLog;->n:I

    .line 1261
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1041
    iput-object p1, p0, Lcom/twitter/analytics/model/ScribeLog;->w:Ljava/lang/String;

    .line 1042
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public j(I)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1266
    iput p1, p0, Lcom/twitter/analytics/model/ScribeLog;->o:I

    .line 1267
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public j(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1078
    iput-object p1, p0, Lcom/twitter/analytics/model/ScribeLog;->z:Ljava/lang/String;

    .line 1079
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 1387
    iget-boolean v0, p0, Lcom/twitter/analytics/model/ScribeLog;->Y:Z

    return v0
.end method

.method public k(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1084
    iput-object p1, p0, Lcom/twitter/analytics/model/ScribeLog;->A:Ljava/lang/String;

    .line 1085
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public l(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1090
    iput-object p1, p0, Lcom/twitter/analytics/model/ScribeLog;->B:Ljava/lang/String;

    .line 1091
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public m(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1102
    iput-object p1, p0, Lcom/twitter/analytics/model/ScribeLog;->D:Ljava/lang/String;

    .line 1103
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public n(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1108
    iput-object p1, p0, Lcom/twitter/analytics/model/ScribeLog;->G:Ljava/lang/String;

    .line 1109
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public o(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1138
    iput-object p1, p0, Lcom/twitter/analytics/model/ScribeLog;->ag:Ljava/lang/String;

    .line 1139
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public p(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TTScribe",
            "Log;"
        }
    .end annotation

    .prologue
    .line 1248
    iput-object p1, p0, Lcom/twitter/analytics/model/ScribeLog;->e:Ljava/lang/String;

    .line 1249
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 680
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 681
    invoke-virtual {p0, v0}, Lcom/twitter/analytics/model/ScribeLog;->a(Ljava/io/OutputStream;)V

    .line 682
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 591
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->q:Lcnz;

    sget-object v1, Lcnz;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 592
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->z:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 593
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->Q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 594
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->R:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 595
    iget-wide v0, p0, Lcom/twitter/analytics/model/ScribeLog;->i:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 596
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 597
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 598
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->S:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 599
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 600
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->n:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 601
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->o:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 602
    iget-wide v0, p0, Lcom/twitter/analytics/model/ScribeLog;->j:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 603
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->k:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 604
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->l:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 605
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->r:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 606
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 607
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 608
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 609
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 610
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->w:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 611
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->y:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 612
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->A:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 613
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->B:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 614
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->D:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 615
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->E:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 616
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->F:[B

    if-eqz v0, :cond_0

    .line 617
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->F:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 618
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->F:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 622
    :goto_0
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->G:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 623
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 624
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 625
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->H:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 626
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->I:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 627
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->J:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 628
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->K:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 629
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->L:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 630
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->M:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 631
    iget-boolean v0, p0, Lcom/twitter/analytics/model/ScribeLog;->N:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 632
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->O:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 633
    iget-wide v0, p0, Lcom/twitter/analytics/model/ScribeLog;->P:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 634
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->u:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 635
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->h:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 636
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->g:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 637
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->W:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 638
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->X:Ljava/util/Map;

    if-eqz v0, :cond_2

    .line 639
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->X:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 640
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->X:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 641
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 642
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2

    .line 620
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_1
    move v0, v3

    .line 631
    goto :goto_1

    .line 645
    :cond_2
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 647
    :cond_3
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->Z:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 648
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->af:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 649
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->aa:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 650
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->ab:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 651
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->ac:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 652
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->ad:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 653
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->ae:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 654
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->T:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 655
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->x:Lcom/twitter/analytics/model/ScribeLog$SearchDetails;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 656
    iget v0, p0, Lcom/twitter/analytics/model/ScribeLog;->U:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 657
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->V:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 658
    iget-object v0, p0, Lcom/twitter/analytics/model/ScribeLog;->ag:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 659
    iget-boolean v0, p0, Lcom/twitter/analytics/model/ScribeLog;->C:Z

    if-eqz v0, :cond_4

    :goto_3
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 660
    return-void

    :cond_4
    move v2, v3

    .line 659
    goto :goto_3
.end method
