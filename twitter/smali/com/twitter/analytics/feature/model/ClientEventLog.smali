.class public Lcom/twitter/analytics/feature/model/ClientEventLog;
.super Lcom/twitter/analytics/model/ScribeLog;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/analytics/feature/model/ClientEventLog$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/analytics/model/ScribeLog",
        "<",
        "Lcom/twitter/analytics/feature/model/ClientEventLog;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/analytics/feature/model/ClientEventLog;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Lcok$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcok$a",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:J

.field private i:Ljava/lang/String;

.field private j:J

.field private k:J

.field private l:Lcom/twitter/analytics/feature/model/WebsiteAssetsLog;

.field private m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 34
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog$1;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog$1;-><init>()V

    sput-object v0, Lcom/twitter/analytics/feature/model/ClientEventLog;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 55
    const-string/jumbo v0, "thrift_logging_base64_whitelisted_domains"

    .line 58
    invoke-static {}, Lcom/twitter/util/collection/o;->f()Ljava/util/Set;

    move-result-object v1

    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog$2;

    invoke-direct {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog$2;-><init>()V

    .line 56
    invoke-static {v0, v1, v2}, Lcok;->a(Ljava/lang/String;Ljava/lang/Object;Lcpp;)Lcok$a;

    move-result-object v0

    sput-object v0, Lcom/twitter/analytics/feature/model/ClientEventLog;->a:Lcok$a;

    .line 55
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 134
    invoke-direct {p0}, Lcom/twitter/analytics/model/ScribeLog;-><init>()V

    .line 92
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->h:J

    .line 102
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->j:J

    .line 107
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->k:J

    .line 134
    return-void
.end method

.method public constructor <init>(J)V
    .locals 3

    .prologue
    const-wide/16 v0, -0x1

    .line 145
    invoke-direct {p0, p1, p2}, Lcom/twitter/analytics/model/ScribeLog;-><init>(J)V

    .line 92
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->h:J

    .line 102
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->j:J

    .line 107
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->k:J

    .line 146
    return-void
.end method

.method public constructor <init>(JLcom/twitter/analytics/model/a;)V
    .locals 3

    .prologue
    const-wide/16 v0, -0x1

    .line 153
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/analytics/model/ScribeLog;-><init>(JLcom/twitter/analytics/model/a;)V

    .line 92
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->h:J

    .line 102
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->j:J

    .line 107
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->k:J

    .line 154
    return-void
.end method

.method public varargs constructor <init>(J[Ljava/lang/String;)V
    .locals 3

    .prologue
    const-wide/16 v0, -0x1

    .line 149
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/analytics/model/ScribeLog;-><init>(J[Ljava/lang/String;)V

    .line 92
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->h:J

    .line 102
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->j:J

    .line 107
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->k:J

    .line 150
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 114
    invoke-direct {p0, p1}, Lcom/twitter/analytics/model/ScribeLog;-><init>(Landroid/os/Parcel;)V

    .line 92
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->h:J

    .line 102
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->j:J

    .line 107
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->k:J

    .line 116
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->e:Ljava/lang/String;

    .line 117
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->f:Ljava/lang/String;

    .line 118
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->b:Ljava/lang/String;

    .line 119
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->c:I

    .line 120
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->d:Ljava/lang/String;

    .line 121
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->g:Ljava/lang/String;

    .line 122
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->h:J

    .line 123
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->i:Ljava/lang/String;

    .line 124
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->j:J

    .line 125
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->k:J

    .line 126
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->m:Ljava/lang/String;

    .line 129
    :try_start_0
    const-class v0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->l:Lcom/twitter/analytics/feature/model/WebsiteAssetsLog;
    :try_end_0
    .catch Landroid/os/BadParcelableException; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :goto_0
    return-void

    .line 130
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public varargs constructor <init>([Ljava/lang/String;)V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 137
    invoke-direct {p0, p1}, Lcom/twitter/analytics/model/ScribeLog;-><init>([Ljava/lang/String;)V

    .line 92
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->h:J

    .line 102
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->j:J

    .line 107
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->k:J

    .line 138
    return-void
.end method

.method public static a(J)Lcom/twitter/analytics/feature/model/ClientEventLog$a;
    .locals 2

    .prologue
    .line 350
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog$a;

    invoke-direct {v0, p0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog$a;-><init>(J)V

    return-object v0
.end method

.method public static a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 236
    if-eqz p0, :cond_0

    .line 237
    invoke-virtual {p0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "tweet"

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 238
    invoke-virtual {p0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    .line 243
    :goto_0
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    const/4 v1, 0x1

    aput-object v0, v2, v1

    const/4 v0, 0x2

    aput-object p1, v2, v0

    const/4 v0, 0x3

    aput-object p2, v2, v0

    const/4 v0, 0x4

    aput-object p3, v2, v0

    invoke-static {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 240
    :cond_0
    const-string/jumbo v1, "tweet"

    .line 241
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lorg/apache/thrift/TBase;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 332
    :try_start_0
    new-instance v0, Lorg/apache/thrift/c;

    invoke-direct {v0}, Lorg/apache/thrift/c;-><init>()V

    .line 333
    invoke-virtual {v0, p0}, Lorg/apache/thrift/c;->a(Lorg/apache/thrift/TBase;)[B

    move-result-object v0

    .line 334
    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Lorg/apache/thrift/TException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 337
    :goto_0
    return-object v0

    .line 335
    :catch_0
    move-exception v0

    .line 336
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 337
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static q(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 345
    sget-object v0, Lcom/twitter/analytics/feature/model/ClientEventLog;->a:Lcok$a;

    invoke-virtual {v0}, Lcok$a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom;

    invoke-virtual {v0}, Lcom;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(JJ)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 1

    .prologue
    .line 312
    iput-wide p1, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->j:J

    .line 313
    iput-wide p3, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->k:J

    .line 314
    return-object p0
.end method

.method public a(Lcom/twitter/analytics/feature/model/WebsiteAssetsLog;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->l:Lcom/twitter/analytics/feature/model/WebsiteAssetsLog;

    .line 320
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 0

    .prologue
    .line 306
    iput-object p1, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->g:Ljava/lang/String;

    .line 307
    return-object p0
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->b:Ljava/lang/String;

    .line 268
    iput p2, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->c:I

    .line 269
    iput-object p3, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->d:Ljava/lang/String;

    .line 271
    return-object p0
.end method

.method public a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 1

    .prologue
    .line 277
    if-nez p1, :cond_0

    .line 280
    :goto_0
    return-object p0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object p0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 4

    .prologue
    .line 287
    new-instance v1, Lcom/twitter/analytics/feature/model/NativeCardEvent;

    invoke-direct {v1, p1}, Lcom/twitter/analytics/feature/model/NativeCardEvent;-><init>(Ljava/lang/String;)V

    .line 288
    if-eqz p2, :cond_0

    .line 289
    invoke-virtual {v1, p2}, Lcom/twitter/analytics/feature/model/NativeCardEvent;->a(Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 291
    :cond_0
    if-eqz p3, :cond_1

    .line 292
    invoke-virtual {v1, p3}, Lcom/twitter/analytics/feature/model/NativeCardEvent;->a(Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;)V

    .line 294
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->e()Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 295
    if-eqz v0, :cond_3

    .line 296
    iget v2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->m:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 297
    iget v2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->m:I

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/NativeCardEvent;->a(I)V

    .line 299
    :cond_2
    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aw:Lcom/twitter/analytics/feature/model/NativeCardEvent;

    .line 301
    :cond_3
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 0

    .prologue
    .line 260
    iput-object p1, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->e:Ljava/lang/String;

    .line 261
    iput-object p2, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->f:Ljava/lang/String;

    .line 262
    return-object p0
.end method

.method public a(I)Lcom/twitter/analytics/model/ScribeLog;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/analytics/model/ScribeLog;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeItem;

    .line 250
    instance-of v2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    if-eqz v2, :cond_0

    .line 251
    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a(I)V

    goto :goto_0

    .line 254
    :cond_1
    return-object p0
.end method

.method protected a(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    .line 177
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 178
    const-string/jumbo v0, "settings_version_details"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->e(Ljava/lang/String;)V

    .line 179
    const-string/jumbo v0, "feature_switches"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const-string/jumbo v0, "experiments"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 184
    const-string/jumbo v0, "experiment_key"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const-string/jumbo v0, "version"

    iget v1, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 186
    const-string/jumbo v0, "bucket"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->g:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 189
    const-string/jumbo v0, "conversation_id"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :cond_2
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->h:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 192
    const-string/jumbo v0, "status_id"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->h:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 194
    :cond_3
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->i:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 195
    const-string/jumbo v0, "impression_id"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :cond_4
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->j:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    .line 198
    const-string/jumbo v0, "dm_id"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->j:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 200
    :cond_5
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->k:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_6

    .line 201
    const-string/jumbo v0, "dm_create_time"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->k:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 203
    :cond_6
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->l:Lcom/twitter/analytics/feature/model/WebsiteAssetsLog;

    if-eqz v0, :cond_7

    .line 204
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->l:Lcom/twitter/analytics/feature/model/WebsiteAssetsLog;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 206
    :cond_7
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->b:Ljava/lang/String;

    if-eqz v0, :cond_8

    const-string/jumbo v0, "experiment_details"

    invoke-static {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->q(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 207
    new-instance v0, Lcom/twitter/clientapp/thriftandroid/ExperimentDetails$a;

    invoke-direct {v0}, Lcom/twitter/clientapp/thriftandroid/ExperimentDetails$a;-><init>()V

    sget-object v1, Lcom/twitter/clientapp/thriftandroid/ExperimentDetails;->b:Lcom/twitter/clientapp/thriftandroid/ExperimentDetails$_Fields;

    iget-object v2, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->b:Ljava/lang/String;

    .line 208
    invoke-virtual {v0, v1, v2}, Lcom/twitter/clientapp/thriftandroid/ExperimentDetails$a;->a(Lcom/twitter/clientapp/thriftandroid/ExperimentDetails$_Fields;Ljava/lang/Object;)Lcom/twitter/clientapp/thriftandroid/ExperimentDetails$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/clientapp/thriftandroid/ExperimentDetails;->d:Lcom/twitter/clientapp/thriftandroid/ExperimentDetails$_Fields;

    iget v2, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->c:I

    .line 209
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/clientapp/thriftandroid/ExperimentDetails$a;->a(Lcom/twitter/clientapp/thriftandroid/ExperimentDetails$_Fields;Ljava/lang/Object;)Lcom/twitter/clientapp/thriftandroid/ExperimentDetails$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/clientapp/thriftandroid/ExperimentDetails;->c:Lcom/twitter/clientapp/thriftandroid/ExperimentDetails$_Fields;

    iget-object v2, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->d:Ljava/lang/String;

    .line 210
    invoke-virtual {v0, v1, v2}, Lcom/twitter/clientapp/thriftandroid/ExperimentDetails$a;->a(Lcom/twitter/clientapp/thriftandroid/ExperimentDetails$_Fields;Ljava/lang/Object;)Lcom/twitter/clientapp/thriftandroid/ExperimentDetails$a;

    move-result-object v0

    .line 211
    invoke-virtual {v0}, Lcom/twitter/clientapp/thriftandroid/ExperimentDetails$a;->a()Lcom/twitter/clientapp/thriftandroid/ExperimentDetails;

    move-result-object v0

    .line 212
    const-string/jumbo v1, "experiment_details_binary"

    invoke-static {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lorg/apache/thrift/TBase;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    :cond_8
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->m:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 216
    const-string/jumbo v0, "custom_json_payload"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :cond_9
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 226
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 227
    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_0

    .line 229
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 0

    .prologue
    .line 325
    iput-object p1, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->m:Ljava/lang/String;

    .line 326
    return-object p0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 158
    invoke-super {p0, p1, p2}, Lcom/twitter/analytics/model/ScribeLog;->writeToParcel(Landroid/os/Parcel;I)V

    .line 161
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 164
    iget v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 165
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 166
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 167
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->h:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 168
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 169
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->j:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 170
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->k:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 171
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog;->l:Lcom/twitter/analytics/feature/model/WebsiteAssetsLog;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 173
    return-void
.end method
