.class public Lcom/twitter/app/common/list/l;
.super Laog;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/common/list/l$a;,
        Lcom/twitter/app/common/list/l$f;,
        Lcom/twitter/app/common/list/l$e;,
        Lcom/twitter/app/common/list/l$c;,
        Lcom/twitter/app/common/list/l$b;,
        Lcom/twitter/app/common/list/l$d;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "A:",
        "Lcjr",
        "<TT;>;>",
        "Laog;"
    }
.end annotation


# instance fields
.field public final a:Landroid/widget/ListView;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final b:Landroid/view/View;

.field public final c:Landroid/view/View;

.field public final d:Z

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View$OnTouchListener;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/app/common/list/l$b;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/content/Context;

.field private final h:Landroid/view/View;

.field private final i:Lcom/twitter/app/common/list/l$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/app/common/list/l",
            "<TT;TA;>.a;"
        }
    .end annotation
.end field

.field private final j:Lcom/twitter/app/common/list/h;

.field private final k:Lcno;

.field private l:Lcjr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TA;"
        }
    .end annotation
.end field

.field private m:Lcjt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcjt",
            "<TT;>;"
        }
    .end annotation
.end field

.field private n:Lcom/twitter/refresh/widget/RefreshableListView$e;

.field private o:Lcom/twitter/app/common/list/l$c;

.field private p:Lcom/twitter/library/util/r;

.field private q:Lcnp;

.field private r:Lcnr;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Lcom/twitter/app/common/list/l$d;)V
    .locals 10

    .prologue
    const v6, 0x1020004

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 111
    invoke-direct {p0}, Laog;-><init>()V

    .line 81
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/common/list/l;->e:Ljava/util/Set;

    .line 82
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/common/list/l;->f:Ljava/util/Set;

    .line 112
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 113
    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/list/l;->g:Landroid/content/Context;

    .line 114
    invoke-virtual {p2}, Lcom/twitter/app/common/list/l$d;->e()I

    move-result v0

    invoke-virtual {p1, v0, v2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 115
    invoke-virtual {p0, v3}, Lcom/twitter/app/common/list/l;->a(Landroid/view/View;)V

    .line 118
    const v0, 0x7f1301ae

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 119
    invoke-virtual {p2}, Lcom/twitter/app/common/list/l$d;->g()I

    move-result v1

    const/4 v5, -0x1

    if-ne v1, v5, :cond_9

    const v1, 0x102000a

    .line 120
    :goto_0
    if-eqz v0, :cond_0

    .line 121
    invoke-virtual {p2}, Lcom/twitter/app/common/list/l$d;->f()I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 122
    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setInflatedId(I)V

    .line 123
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 125
    :cond_0
    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 126
    iput-object v0, p0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    .line 127
    new-instance v1, Lcnn;

    iget-object v5, p0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-direct {v1, v5}, Lcnn;-><init>(Landroid/widget/ListView;)V

    iput-object v1, p0, Lcom/twitter/app/common/list/l;->k:Lcno;

    .line 130
    const v1, 0x7f130424

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 131
    if-eqz v1, :cond_1

    .line 132
    invoke-virtual {p2}, Lcom/twitter/app/common/list/l$d;->h()I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 133
    invoke-virtual {v1, v6}, Landroid/view/ViewStub;->setInflatedId(I)V

    .line 134
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 136
    :cond_1
    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 137
    if-eqz v5, :cond_a

    .line 138
    iget-object v1, p0, Lcom/twitter/app/common/list/l;->k:Lcno;

    invoke-interface {v1, v5}, Lcno;->b(Landroid/view/View;)V

    .line 140
    const v1, 0x7f13004d

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 141
    if-eqz v6, :cond_3

    .line 142
    invoke-virtual {p2}, Lcom/twitter/app/common/list/l$d;->b()I

    move-result v1

    if-eqz v1, :cond_2

    .line 143
    const v1, 0x7f130030

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 144
    if-eqz v1, :cond_2

    .line 145
    invoke-virtual {p2}, Lcom/twitter/app/common/list/l$d;->b()I

    move-result v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(I)V

    .line 148
    :cond_2
    invoke-virtual {p2}, Lcom/twitter/app/common/list/l$d;->c()I

    move-result v1

    if-lez v1, :cond_3

    .line 149
    const v1, 0x7f13002f

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 150
    if-eqz v1, :cond_3

    .line 151
    invoke-virtual {p2}, Lcom/twitter/app/common/list/l$d;->c()I

    move-result v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(I)V

    .line 155
    :cond_3
    iput-object v6, p0, Lcom/twitter/app/common/list/l;->h:Landroid/view/View;

    .line 159
    :goto_1
    iput-object v5, p0, Lcom/twitter/app/common/list/l;->b:Landroid/view/View;

    .line 162
    const v1, 0x7f13004e

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/common/list/l;->c:Landroid/view/View;

    .line 165
    invoke-virtual {p2}, Lcom/twitter/app/common/list/l$d;->p()Lcom/twitter/app/common/list/h;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/common/list/l;->j:Lcom/twitter/app/common/list/h;

    .line 167
    invoke-virtual {p2}, Lcom/twitter/app/common/list/l$d;->k()Z

    move-result v1

    if-nez v1, :cond_4

    .line 168
    invoke-virtual {v0}, Landroid/widget/ListView;->getPaddingTop()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/ListView;->getPaddingBottom()I

    move-result v3

    invoke-virtual {v0, v8, v1, v8, v3}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 169
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f11000a

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setBackgroundColor(I)V

    .line 172
    :cond_4
    invoke-virtual {v0, v9}, Landroid/widget/ListView;->setScrollbarFadingEnabled(Z)V

    .line 173
    invoke-virtual {p2}, Lcom/twitter/app/common/list/l$d;->l()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 174
    new-instance v1, Lcom/twitter/app/common/list/l$1;

    invoke-direct {v1, p0}, Lcom/twitter/app/common/list/l$1;-><init>(Lcom/twitter/app/common/list/l;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 186
    new-instance v1, Lcom/twitter/app/common/list/l$2;

    invoke-direct {v1, p0, v0}, Lcom/twitter/app/common/list/l$2;-><init>(Lcom/twitter/app/common/list/l;Landroid/widget/ListView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 195
    invoke-virtual {p2}, Lcom/twitter/app/common/list/l$d;->i()I

    move-result v1

    .line 196
    invoke-virtual {p2}, Lcom/twitter/app/common/list/l$d;->j()I

    move-result v5

    .line 197
    if-nez v1, :cond_5

    if-eqz v5, :cond_6

    .line 198
    :cond_5
    new-instance v6, Lcnp;

    iget-object v7, p0, Lcom/twitter/app/common/list/l;->k:Lcno;

    if-eqz v1, :cond_b

    .line 199
    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    move-object v3, v1

    :goto_2
    if-eqz v5, :cond_c

    .line 200
    invoke-virtual {p1, v5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    :goto_3
    invoke-direct {v6, v7, v3, v1}, Lcnp;-><init>(Lcno;Landroid/view/View;Landroid/view/View;)V

    iput-object v6, p0, Lcom/twitter/app/common/list/l;->q:Lcnp;

    .line 203
    :cond_6
    invoke-virtual {p2}, Lcom/twitter/app/common/list/l$d;->o()Z

    move-result v1

    if-eqz v1, :cond_d

    instance-of v1, v0, Lcom/twitter/refresh/widget/RefreshableListView;

    if-nez v1, :cond_d

    .line 204
    iput-boolean v9, p0, Lcom/twitter/app/common/list/l;->d:Z

    .line 205
    new-instance v1, Lcnr;

    iget-object v3, p0, Lcom/twitter/app/common/list/l;->k:Lcno;

    invoke-direct {v1, v4, v3}, Lcnr;-><init>(Landroid/content/Context;Lcno;)V

    iput-object v1, p0, Lcom/twitter/app/common/list/l;->r:Lcnr;

    .line 206
    iget-object v1, p0, Lcom/twitter/app/common/list/l;->r:Lcnr;

    new-instance v3, Lcom/twitter/app/common/list/l$3;

    invoke-direct {v3, p0}, Lcom/twitter/app/common/list/l$3;-><init>(Lcom/twitter/app/common/list/l;)V

    invoke-virtual {v1, v3}, Lcnr;->a(Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;)V

    .line 228
    :goto_4
    const-string/jumbo v1, "home_timeline_scroll_framerate_enabled"

    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 229
    new-instance v1, Lcom/twitter/app/common/list/l$a;

    invoke-virtual {p2}, Lcom/twitter/app/common/list/l$d;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/twitter/app/common/list/l$a;-><init>(Lcom/twitter/app/common/list/l;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/twitter/app/common/list/l;->i:Lcom/twitter/app/common/list/l$a;

    .line 230
    iget-object v1, p0, Lcom/twitter/app/common/list/l;->i:Lcom/twitter/app/common/list/l$a;

    invoke-virtual {p0, v1}, Lcom/twitter/app/common/list/l;->a(Lcno$c;)V

    .line 235
    :goto_5
    invoke-virtual {p2}, Lcom/twitter/app/common/list/l$d;->n()I

    move-result v1

    if-eqz v1, :cond_7

    .line 236
    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setClipToPadding(Z)V

    .line 237
    invoke-virtual {v0}, Landroid/widget/ListView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/ListView;->getPaddingTop()I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/ListView;->getPaddingRight()I

    move-result v3

    .line 238
    invoke-virtual {v0}, Landroid/widget/ListView;->getPaddingBottom()I

    move-result v4

    invoke-virtual {p2}, Lcom/twitter/app/common/list/l$d;->n()I

    move-result v5

    add-int/2addr v4, v5

    .line 237
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 242
    :cond_7
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->c:Landroid/view/View;

    if-eqz v0, :cond_8

    .line 243
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 244
    invoke-virtual {p0}, Lcom/twitter/app/common/list/l;->o()Lanh;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/common/list/l$4;

    invoke-direct {v1, p0}, Lcom/twitter/app/common/list/l$4;-><init>(Lcom/twitter/app/common/list/l;)V

    invoke-virtual {v0, v1}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 258
    :cond_8
    return-void

    .line 119
    :cond_9
    invoke-virtual {p2}, Lcom/twitter/app/common/list/l$d;->g()I

    move-result v1

    goto/16 :goto_0

    .line 157
    :cond_a
    iput-object v2, p0, Lcom/twitter/app/common/list/l;->h:Landroid/view/View;

    goto/16 :goto_1

    :cond_b
    move-object v3, v2

    .line 199
    goto/16 :goto_2

    :cond_c
    move-object v1, v2

    .line 200
    goto/16 :goto_3

    .line 212
    :cond_d
    instance-of v1, v0, Lcom/twitter/refresh/widget/RefreshableListView;

    if-eqz v1, :cond_f

    move-object v1, v0

    .line 213
    check-cast v1, Lcom/twitter/refresh/widget/RefreshableListView;

    .line 214
    invoke-virtual {p2}, Lcom/twitter/app/common/list/l$d;->o()Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/app/common/list/l;->d:Z

    .line 215
    iget-boolean v3, p0, Lcom/twitter/app/common/list/l;->d:Z

    if-eqz v3, :cond_e

    .line 216
    new-instance v3, Lcom/twitter/app/common/list/l$e;

    invoke-direct {v3, p0, v2}, Lcom/twitter/app/common/list/l$e;-><init>(Lcom/twitter/app/common/list/l;Lcom/twitter/app/common/list/l$1;)V

    invoke-virtual {v1, v3}, Lcom/twitter/refresh/widget/RefreshableListView;->setRefreshListener(Lcom/twitter/refresh/widget/RefreshableListView$b;)V

    .line 217
    new-instance v3, Lcom/twitter/app/common/list/l$f;

    invoke-direct {v3, p0, v2}, Lcom/twitter/app/common/list/l$f;-><init>(Lcom/twitter/app/common/list/l;Lcom/twitter/app/common/list/l$1;)V

    invoke-virtual {v1, v3}, Lcom/twitter/refresh/widget/RefreshableListView;->setVisibilityListener(Lcom/twitter/refresh/widget/RefreshableListView$e;)V

    .line 218
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/twitter/refresh/widget/RefreshableListView;->setOverScrollMode(I)V

    .line 223
    :goto_6
    invoke-virtual {p2}, Lcom/twitter/app/common/list/l$d;->m()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/twitter/app/common/list/l;->a(I)V

    goto/16 :goto_4

    .line 220
    :cond_e
    invoke-virtual {v0, v9}, Landroid/widget/ListView;->setFooterDividersEnabled(Z)V

    goto :goto_6

    .line 225
    :cond_f
    iput-boolean v8, p0, Lcom/twitter/app/common/list/l;->d:Z

    goto/16 :goto_4

    .line 232
    :cond_10
    iput-object v2, p0, Lcom/twitter/app/common/list/l;->i:Lcom/twitter/app/common/list/l$a;

    goto/16 :goto_5
.end method

.method private A()V
    .locals 3

    .prologue
    .line 467
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/l$b;

    .line 468
    invoke-interface {v0}, Lcom/twitter/app/common/list/l$b;->V_()V

    goto :goto_0

    .line 470
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/common/list/l;->f()Lcno;

    move-result-object v0

    invoke-interface {v0}, Lcno;->a()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/common/list/l;->g:Landroid/content/Context;

    const v2, 0x7f0a074d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 471
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/common/list/l;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->e:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/app/common/list/l;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/twitter/app/common/list/l;->A()V

    return-void
.end method

.method static synthetic c(Lcom/twitter/app/common/list/l;)Lcjt;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->m:Lcjt;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/app/common/list/l;)Lcom/twitter/refresh/widget/RefreshableListView$e;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->n:Lcom/twitter/refresh/widget/RefreshableListView$e;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 410
    invoke-virtual {p0}, Lcom/twitter/app/common/list/l;->aN_()Landroid/view/View;

    move-result-object v0

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 411
    return-void
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 581
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/app/common/list/l;->a(IIZ)V

    .line 582
    return-void
.end method

.method public a(IIZ)V
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->k:Lcno;

    invoke-interface {v0, p1, p2, p3}, Lcno;->a(IIZ)V

    .line 586
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->o:Lcom/twitter/app/common/list/l$c;

    if-eqz v0, :cond_0

    .line 587
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->o:Lcom/twitter/app/common/list/l$c;

    invoke-interface {v0, p1, p2}, Lcom/twitter/app/common/list/l$c;->a(II)V

    .line 589
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 634
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 635
    return-void
.end method

.method public final a(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 360
    instance-of v0, p1, Lcjq;

    if-nez v0, :cond_0

    .line 361
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The adapter does not have an ItemProvider. Use setListAdapter(ListAdapter, ItemCollectionProvider) instead to explicitly provider one."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 364
    :cond_0
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcjq;

    invoke-interface {v0}, Lcjq;->a()Lcju;

    move-result-object v0

    .line 365
    instance-of v1, v0, Lcjt;

    if-nez v1, :cond_1

    .line 366
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The adapter does not have an ItemCollectionProvider. Use setListAdapter(ListAdapter, ItemCollectionProvider) instead to explicitly provider one."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 369
    :cond_1
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcjt;

    invoke-virtual {p0, p1, v0}, Lcom/twitter/app/common/list/l;->a(Landroid/widget/ListAdapter;Lcjt;)V

    .line 370
    return-void
.end method

.method public final a(Landroid/widget/ListAdapter;Lcjt;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ListAdapter;",
            "Lcjt",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 348
    invoke-virtual {p0}, Lcom/twitter/app/common/list/l;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The adapter has already been set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->k:Lcno;

    invoke-interface {v0, p1}, Lcno;->a(Landroid/widget/ListAdapter;)V

    .line 352
    iput-object p2, p0, Lcom/twitter/app/common/list/l;->m:Lcjt;

    .line 353
    return-void
.end method

.method public a(Lcbi;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 376
    invoke-virtual {p0}, Lcom/twitter/app/common/list/l;->j()Lcjt;

    move-result-object v0

    invoke-interface {v0, p1}, Lcjt;->a(Lcbi;)Lcbi;

    .line 379
    invoke-virtual {p0}, Lcom/twitter/app/common/list/l;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 380
    if-nez p1, :cond_1

    .line 381
    invoke-virtual {p0}, Lcom/twitter/app/common/list/l;->s()V

    .line 386
    :cond_0
    :goto_0
    return-void

    .line 383
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/app/common/list/l;->t()V

    goto :goto_0
.end method

.method public final a(Lcjr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)V"
        }
    .end annotation

    .prologue
    .line 326
    invoke-virtual {p0, p1, p1}, Lcom/twitter/app/common/list/l;->a(Lcjr;Landroid/widget/ListAdapter;)V

    .line 327
    return-void
.end method

.method public final a(Lcjr;Landroid/widget/ListAdapter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;",
            "Landroid/widget/ListAdapter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 335
    invoke-virtual {p0}, Lcom/twitter/app/common/list/l;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 336
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The adapter has already been set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->k:Lcno;

    invoke-interface {v0, p2}, Lcno;->a(Landroid/widget/ListAdapter;)V

    .line 339
    iput-object p1, p0, Lcom/twitter/app/common/list/l;->l:Lcjr;

    .line 340
    invoke-virtual {p1}, Lcjr;->k()Lcjt;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/list/l;->m:Lcjt;

    .line 341
    return-void
.end method

.method public final a(Lcno$c;)V
    .locals 1

    .prologue
    .line 559
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->k:Lcno;

    invoke-interface {v0, p1}, Lcno;->a(Lcno$c;)V

    .line 560
    return-void
.end method

.method public final a(Lcom/twitter/app/common/list/l$b;)V
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 530
    return-void
.end method

.method public a(Lcom/twitter/app/common/list/l$c;)V
    .locals 0

    .prologue
    .line 570
    iput-object p1, p0, Lcom/twitter/app/common/list/l;->o:Lcom/twitter/app/common/list/l$c;

    .line 571
    return-void
.end method

.method public a(Lcom/twitter/refresh/widget/RefreshableListView$e;)V
    .locals 0

    .prologue
    .line 533
    iput-object p1, p0, Lcom/twitter/app/common/list/l;->n:Lcom/twitter/refresh/widget/RefreshableListView$e;

    .line 534
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->q:Lcnp;

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->q:Lcnp;

    invoke-virtual {v0, p1}, Lcnp;->a(Z)V

    .line 447
    :cond_0
    return-void
.end method

.method public ak_()V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->i:Lcom/twitter/app/common/list/l$a;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->i:Lcom/twitter/app/common/list/l$a;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l$a;->a()V

    .line 270
    :cond_0
    invoke-super {p0}, Laog;->ak_()V

    .line 271
    return-void
.end method

.method public am_()V
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->p:Lcom/twitter/library/util/r;

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->p:Lcom/twitter/library/util/r;

    invoke-virtual {v0}, Lcom/twitter/library/util/r;->a()V

    .line 278
    :cond_0
    invoke-super {p0}, Laog;->am_()V

    .line 279
    return-void
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 456
    invoke-virtual {p0}, Lcom/twitter/app/common/list/l;->r()Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lcni;->b:Z

    if-eqz v0, :cond_1

    .line 457
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->p:Lcom/twitter/library/util/r;

    if-nez v0, :cond_0

    .line 458
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/util/r;->a(Landroid/content/Context;)Lcom/twitter/library/util/r;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/list/l;->p:Lcom/twitter/library/util/r;

    .line 460
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->p:Lcom/twitter/library/util/r;

    invoke-virtual {v0, p1}, Lcom/twitter/library/util/r;->a(I)I

    move-result v0

    .line 462
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->q:Lcnp;

    if-eqz v0, :cond_0

    .line 451
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->q:Lcnp;

    invoke-virtual {v0, p1}, Lcnp;->b(Z)V

    .line 453
    :cond_0
    return-void
.end method

.method public final c(Z)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 491
    iget-boolean v0, p0, Lcom/twitter/app/common/list/l;->d:Z

    if-eqz v0, :cond_0

    .line 492
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->r:Lcnr;

    if-eqz v0, :cond_1

    .line 493
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->r:Lcnr;

    invoke-virtual {v0}, Lcnr;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 494
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->r:Lcnr;

    invoke-virtual {v0, v1}, Lcnr;->a(Z)V

    .line 495
    invoke-virtual {p0, v3}, Lcom/twitter/app/common/list/l;->b(I)I

    :cond_0
    move v0, v1

    .line 505
    :goto_0
    return v0

    .line 498
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/refresh/widget/RefreshableListView;

    .line 499
    invoke-virtual {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 500
    invoke-virtual {p0, v3}, Lcom/twitter/app/common/list/l;->b(I)I

    .line 501
    invoke-virtual {v0, p1}, Lcom/twitter/refresh/widget/RefreshableListView;->a(Z)Z

    move-result v0

    goto :goto_0
.end method

.method public f()Lcno;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->k:Lcno;

    return-object v0
.end method

.method protected final g()Z
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->l:Lcjr;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcjr;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TA;"
        }
    .end annotation

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/twitter/app/common/list/l;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The list adapter has not been set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 296
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->l:Lcjr;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcjr;

    return-object v0
.end method

.method protected final i()Z
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->m:Lcjt;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcjt;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcjt",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 311
    invoke-virtual {p0}, Lcom/twitter/app/common/list/l;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 312
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The item provider has not been set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->m:Lcjt;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcjt;

    return-object v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/twitter/app/common/list/l;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/common/list/l;->j()Lcjt;

    move-result-object v0

    invoke-interface {v0}, Lcjt;->a()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->k:Lcno;

    invoke-interface {v0}, Lcno;->d()Z

    move-result v0

    return v0
.end method

.method public m()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 400
    iput-object v1, p0, Lcom/twitter/app/common/list/l;->l:Lcjr;

    .line 401
    iput-object v1, p0, Lcom/twitter/app/common/list/l;->m:Lcjt;

    .line 402
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->k:Lcno;

    invoke-interface {v0, v1}, Lcno;->a(Landroid/widget/ListAdapter;)V

    .line 403
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 406
    invoke-virtual {p0}, Lcom/twitter/app/common/list/l;->aN_()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public final s()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 414
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->c:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 415
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 416
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 417
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 418
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 422
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 424
    :cond_1
    return-void
.end method

.method public final t()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 427
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->c:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 428
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->c:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 429
    invoke-virtual {p0}, Lcom/twitter/app/common/list/l;->l()Z

    move-result v0

    .line 430
    if-nez v0, :cond_0

    .line 431
    iget-object v1, p0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 433
    :cond_0
    iget-object v1, p0, Lcom/twitter/app/common/list/l;->b:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 434
    if-nez v0, :cond_2

    .line 435
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 441
    :cond_1
    :goto_0
    return-void

    .line 436
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->h:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 437
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method final u()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 474
    iget-boolean v0, p0, Lcom/twitter/app/common/list/l;->d:Z

    if-eqz v0, :cond_0

    .line 475
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->r:Lcnr;

    if-eqz v0, :cond_1

    .line 476
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->r:Lcnr;

    invoke-virtual {v0}, Lcnr;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->r:Lcnr;

    invoke-virtual {v0, v2}, Lcnr;->a(Z)V

    .line 478
    invoke-virtual {p0, v2}, Lcom/twitter/app/common/list/l;->b(I)I

    .line 488
    :cond_0
    :goto_0
    return-void

    .line 481
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/refresh/widget/RefreshableListView;

    .line 482
    invoke-virtual {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 483
    invoke-virtual {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->a()V

    .line 484
    invoke-virtual {p0, v2}, Lcom/twitter/app/common/list/l;->b(I)I

    goto :goto_0
.end method

.method public final v()V
    .locals 1

    .prologue
    .line 509
    iget-boolean v0, p0, Lcom/twitter/app/common/list/l;->d:Z

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    instance-of v0, v0, Lcom/twitter/refresh/widget/RefreshableListView;

    if-nez v0, :cond_1

    .line 511
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/list/l;->b(Z)V

    .line 516
    :cond_0
    :goto_0
    return-void

    .line 513
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    check-cast v0, Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->d()V

    goto :goto_0
.end method

.method public final w()V
    .locals 1

    .prologue
    .line 519
    iget-boolean v0, p0, Lcom/twitter/app/common/list/l;->d:Z

    if-eqz v0, :cond_0

    .line 520
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    instance-of v0, v0, Lcom/twitter/refresh/widget/RefreshableListView;

    if-nez v0, :cond_1

    .line 521
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/list/l;->b(Z)V

    .line 526
    :cond_0
    :goto_0
    return-void

    .line 523
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    check-cast v0, Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->e()V

    goto :goto_0
.end method

.method public final x()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 541
    invoke-virtual {p0}, Lcom/twitter/app/common/list/l;->f()Lcno;

    move-result-object v0

    invoke-interface {v0}, Lcno;->a()Landroid/view/ViewGroup;

    move-result-object v0

    .line 542
    instance-of v2, v0, Lcom/twitter/refresh/widget/a;

    if-eqz v2, :cond_1

    .line 543
    check-cast v0, Lcom/twitter/refresh/widget/a;

    invoke-interface {v0}, Lcom/twitter/refresh/widget/a;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 545
    invoke-virtual {p0}, Lcom/twitter/app/common/list/l;->y()V

    .line 556
    :cond_0
    :goto_0
    return-void

    .line 548
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/app/common/list/l;->z()Lcnk;

    move-result-object v0

    iget v0, v0, Lcnk;->e:I

    .line 549
    const/4 v2, -0x1

    if-eq v0, v2, :cond_3

    .line 550
    const/16 v2, 0xf

    if-gt v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v1, v1, v0}, Lcom/twitter/app/common/list/l;->a(IIZ)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 553
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/app/common/list/l;->y()V

    goto :goto_0
.end method

.method public final y()V
    .locals 1

    .prologue
    .line 563
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->k:Lcno;

    invoke-interface {v0}, Lcno;->b()V

    .line 564
    return-void
.end method

.method public z()Lcnk;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 597
    iget-object v2, p0, Lcom/twitter/app/common/list/l;->k:Lcno;

    invoke-interface {v2}, Lcno;->c()Lcnm;

    move-result-object v3

    .line 598
    iget-object v2, p0, Lcom/twitter/app/common/list/l;->k:Lcno;

    invoke-interface {v2}, Lcno;->e()I

    move-result v2

    .line 602
    iget v4, v3, Lcnm;->c:I

    if-lt v4, v2, :cond_0

    .line 603
    iget v2, v3, Lcnm;->c:I

    .line 604
    iget v0, v3, Lcnm;->d:I

    move v3, v0

    .line 616
    :goto_0
    if-le v2, v1, :cond_3

    iget-object v0, p0, Lcom/twitter/app/common/list/l;->k:Lcno;

    .line 617
    invoke-interface {v0, v2}, Lcno;->a(I)J

    move-result-wide v0

    .line 618
    :goto_1
    iget-object v4, p0, Lcom/twitter/app/common/list/l;->k:Lcno;

    invoke-interface {v4}, Lcno;->f()I

    move-result v4

    .line 619
    :goto_2
    add-int/lit8 v5, v4, -0x1

    if-ge v2, v5, :cond_4

    iget-object v5, p0, Lcom/twitter/app/common/list/l;->j:Lcom/twitter/app/common/list/h;

    iget-object v5, v5, Lcom/twitter/app/common/list/h;->c:Ljava/util/Set;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 620
    add-int/lit8 v2, v2, 0x1

    .line 621
    iget-object v0, p0, Lcom/twitter/app/common/list/l;->k:Lcno;

    invoke-interface {v0, v2}, Lcno;->a(I)J

    move-result-wide v0

    goto :goto_2

    .line 605
    :cond_0
    iget-object v4, p0, Lcom/twitter/app/common/list/l;->j:Lcom/twitter/app/common/list/h;

    iget-boolean v4, v4, Lcom/twitter/app/common/list/h;->b:Z

    if-eqz v4, :cond_2

    .line 607
    iget-object v4, p0, Lcom/twitter/app/common/list/l;->k:Lcno;

    invoke-interface {v4}, Lcno;->a()Landroid/view/ViewGroup;

    move-result-object v4

    iget v3, v3, Lcnm;->c:I

    sub-int v3, v2, v3

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 608
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v0

    :cond_1
    move v3, v0

    .line 609
    goto :goto_0

    :cond_2
    move v3, v0

    move v2, v1

    .line 611
    goto :goto_0

    .line 617
    :cond_3
    const-wide/16 v0, -0x1

    goto :goto_1

    .line 625
    :cond_4
    new-instance v4, Lcnk;

    invoke-direct {v4, v0, v1, v3, v2}, Lcnk;-><init>(JII)V

    return-object v4
.end method
