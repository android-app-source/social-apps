.class public Lcom/twitter/app/common/util/b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/common/util/b$b;,
        Lcom/twitter/app/common/util/b$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/app/common/util/b$b;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Lcom/twitter/app/common/util/b$b;

    invoke-direct {v0}, Lcom/twitter/app/common/util/b$b;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/common/util/b;->a:Lcom/twitter/app/common/util/b$b;

    .line 34
    new-instance v0, Lcom/twitter/app/common/util/b$1;

    invoke-direct {v0, p0}, Lcom/twitter/app/common/util/b$1;-><init>(Lcom/twitter/app/common/util/b;)V

    invoke-virtual {p1, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 48
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/common/util/b;)Lcom/twitter/app/common/util/b$b;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/twitter/app/common/util/b;->a:Lcom/twitter/app/common/util/b$b;

    return-object v0
.end method

.method public static a()Lcom/twitter/app/common/util/b;
    .locals 1

    .prologue
    .line 29
    invoke-static {}, Lalc;->d()Lalc;

    move-result-object v0

    invoke-virtual {v0}, Lalc;->i()Lcom/twitter/app/common/util/b;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/app/common/util/b$a;)V
    .locals 1

    .prologue
    .line 52
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 53
    iget-object v0, p0, Lcom/twitter/app/common/util/b;->a:Lcom/twitter/app/common/util/b$b;

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/util/b$b;->a(Lcom/twitter/app/common/util/b$a;)V

    .line 54
    return-void
.end method

.method public b()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/app/common/util/b;->a:Lcom/twitter/app/common/util/b$b;

    invoke-virtual {v0}, Lcom/twitter/app/common/util/b$b;->a()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/twitter/app/common/util/b$a;)V
    .locals 1

    .prologue
    .line 58
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 59
    iget-object v0, p0, Lcom/twitter/app/common/util/b;->a:Lcom/twitter/app/common/util/b$b;

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/util/b$b;->b(Lcom/twitter/app/common/util/b$a;)V

    .line 60
    return-void
.end method
