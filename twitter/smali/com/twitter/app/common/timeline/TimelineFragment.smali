.class public abstract Lcom/twitter/app/common/timeline/TimelineFragment;
.super Lcom/twitter/android/TweetListFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/list/e$a;
.implements Lcom/twitter/library/provider/o$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/common/timeline/TimelineFragment$c;,
        Lcom/twitter/app/common/timeline/TimelineFragment$a;,
        Lcom/twitter/app/common/timeline/TimelineFragment$e;,
        Lcom/twitter/app/common/timeline/TimelineFragment$d;,
        Lcom/twitter/app/common/timeline/TimelineFragment$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/TweetListFragment",
        "<",
        "Lcom/twitter/android/timeline/bk;",
        "Lcom/twitter/android/cv;",
        ">;",
        "Lcom/twitter/app/common/list/e$a;",
        "Lcom/twitter/library/provider/o$a;"
    }
.end annotation


# instance fields
.field private A:Z

.field private B:Lcom/twitter/ui/view/h;

.field private C:Lcom/twitter/android/ci;

.field private D:Lcom/twitter/android/TimelineItemClickHandler;

.field private E:Lcom/twitter/android/util/v;

.field private F:Z

.field private a:Lcom/twitter/android/revenue/e;

.field private aa:Lbgw;

.field private final b:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private c:Lcom/twitter/app/common/timeline/TimelineFragment$d;

.field private d:Lcom/twitter/model/core/TwitterUser;

.field private e:Lcom/twitter/library/service/t;

.field private f:Z

.field protected g:Ljava/lang/String;

.field protected h:Z

.field protected i:Lcom/twitter/model/util/FriendshipCache;

.field protected j:Lcom/twitter/android/timeline/be;

.field protected k:Ljava/lang/String;

.field protected l:Ljava/lang/String;

.field protected m:Lcom/twitter/android/cp;

.field protected n:Lti;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lti",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation
.end field

.field protected o:Lcom/twitter/android/revenue/c;

.field protected p:Lbwb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbwb",
            "<",
            "Lcom/mopub/nativeads/NativeAd;",
            ">;"
        }
    .end annotation
.end field

.field protected q:I

.field protected r:Z

.field s:Lcom/twitter/android/metrics/b;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private t:Lcom/twitter/android/platform/DeviceStorageLowReceiver;

.field private u:Z

.field private v:Lcom/twitter/android/ab;

.field private w:Lcom/twitter/android/timeline/cs;

.field private x:Lcom/twitter/android/timeline/cu;

.field private y:Lcom/twitter/android/timeline/av;

.field private z:Lcom/twitter/android/timeline/w;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/twitter/android/TweetListFragment;-><init>()V

    .line 240
    new-instance v0, Lcom/twitter/app/common/timeline/TimelineFragment$1;

    invoke-direct {v0, p0}, Lcom/twitter/app/common/timeline/TimelineFragment$1;-><init>(Lcom/twitter/app/common/timeline/TimelineFragment;)V

    iput-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->b:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    return-void
.end method

.method private a(Lcom/twitter/android/cv;Lcom/twitter/app/common/timeline/TimelineFragment$e;)Lcom/twitter/android/util/v;
    .locals 3

    .prologue
    .line 635
    instance-of v0, p0, Lcom/twitter/android/util/v$d;

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/twitter/android/cf;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 636
    check-cast v0, Lcom/twitter/android/util/v$d;

    .line 637
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aK()Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v2

    .line 638
    invoke-interface {v0}, Lcom/twitter/android/util/v$d;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 639
    new-instance v1, Lcom/twitter/android/util/v;

    invoke-direct {v1, v2, v0}, Lcom/twitter/android/util/v;-><init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/android/util/v$d;)V

    .line 640
    invoke-interface {v0, v1}, Lcom/twitter/android/util/v$d;->a(Lcom/twitter/android/util/v;)V

    .line 641
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/timeline/TimelineFragment$e;->a(Lcom/twitter/android/util/v;)V

    move-object v0, v1

    .line 645
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/View;Lcom/twitter/android/timeline/bk;)V
    .locals 2

    .prologue
    .line 1537
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ad;

    .line 1538
    iget-object v0, v0, Lcom/twitter/android/ad;->a:Lcom/twitter/library/widget/InlineDismissView;

    .line 1539
    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->v:Lcom/twitter/android/ab;

    if-eqz v1, :cond_0

    .line 1540
    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->v:Lcom/twitter/android/ab;

    invoke-interface {v1, v0, p2}, Lcom/twitter/android/ab;->a(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/android/timeline/bk;)V

    .line 1542
    :cond_0
    return-void
.end method

.method private a(Landroid/view/View;Lcom/twitter/android/timeline/bk;I)V
    .locals 7

    .prologue
    const/4 v6, 0x6

    .line 1236
    instance-of v0, p2, Lcom/twitter/android/timeline/aq;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/twitter/android/timeline/cf;

    if-eqz v0, :cond_2

    .line 1237
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/GapView;

    .line 1240
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GapView;->setSpinnerActive(Z)V

    .line 1241
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    iget-wide v2, p2, Lcom/twitter/android/timeline/bk;->d:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/cv;->a(J)V

    .line 1243
    invoke-virtual {p0, v6}, Lcom/twitter/app/common/timeline/TimelineFragment;->i(I)Lajc$a;

    move-result-object v1

    .line 1245
    instance-of v0, p2, Lcom/twitter/android/timeline/aq;

    if-eqz v0, :cond_1

    .line 1248
    invoke-static {p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/aq;

    .line 1249
    iget-object v2, v0, Lcom/twitter/android/timeline/aq;->a:Lcom/twitter/model/timeline/p;

    invoke-virtual {v1, v2}, Lajc$a;->a(Lcom/twitter/model/timeline/p;)Lajc$a;

    move-result-object v2

    iget-wide v4, v0, Lcom/twitter/android/timeline/aq;->b:J

    .line 1250
    invoke-virtual {v2, v4, v5}, Lajc$a;->c(J)Lajc$a;

    .line 1258
    :goto_0
    new-instance v2, Lajd;

    iget-object v3, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->T:Landroid/content/Context;

    invoke-virtual {v1}, Lajc$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajc;

    invoke-direct {v2, v3, v0}, Lajd;-><init>(Landroid/content/Context;Lajc;)V

    .line 1259
    invoke-virtual {v2}, Lajd;->a()Lcom/twitter/library/service/b;

    move-result-object v0

    .line 1262
    invoke-direct {p0, v6, v0}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(ILcom/twitter/library/service/s;)Z

    .line 1270
    :goto_1
    return-void

    .line 1253
    :cond_1
    invoke-static {p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cf;

    .line 1254
    new-instance v2, Lbgk;

    invoke-direct {v2, v0}, Lbgk;-><init>(Lcom/twitter/android/timeline/cf;)V

    invoke-virtual {v1, v2}, Lajc$a;->c(Lbgp;)Lajc$a;

    goto :goto_0

    .line 1264
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->D:Lcom/twitter/android/TimelineItemClickHandler;

    iget v4, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->J:I

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v5

    new-instance v1, Lcom/twitter/android/TweetActivity$a;

    .line 1265
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/android/TweetActivity$a;-><init>(Landroid/content/Context;)V

    .line 1266
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aw()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/TweetActivity$a;->a(Z)Lcom/twitter/android/TweetActivity$a;

    move-result-object v2

    .line 1267
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v2, v1}, Lcom/twitter/android/TweetActivity$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/TweetActivity$a;

    move-result-object v1

    .line 1268
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aG()Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/TweetActivity$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeItem;)Lcom/twitter/android/TweetActivity$a;

    move-result-object v6

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    .line 1264
    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/TimelineItemClickHandler;->a(Landroid/view/View;Lcom/twitter/android/timeline/bk;IILcom/twitter/library/client/Session;Lcom/twitter/android/TweetActivity$a;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/app/common/timeline/TimelineFragment;Landroid/view/View;Lcom/twitter/android/timeline/bk;)V
    .locals 0

    .prologue
    .line 177
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Landroid/view/View;Lcom/twitter/android/timeline/bk;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/common/timeline/TimelineFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 177
    invoke-direct {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->b(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 877
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-direct {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>([Ljava/lang/String;)V

    .line 878
    invoke-virtual {v0, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 879
    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->d:Lcom/twitter/model/core/TwitterUser;

    invoke-static {v0, v1}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 880
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 881
    return-void
.end method

.method private a(ILcom/twitter/library/service/s;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1095
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->c_(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1100
    :cond_0
    :goto_0
    return v0

    .line 1099
    :cond_1
    if-eqz p2, :cond_2

    .line 1100
    :goto_1
    if-eqz p2, :cond_0

    iget v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->J:I

    invoke-virtual {p0, p2, v1, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->c(Lcom/twitter/library/service/s;II)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 1099
    :cond_2
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->e(I)Lcom/twitter/library/service/s;

    move-result-object p2

    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/app/common/timeline/TimelineFragment;)Z
    .locals 1

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->ay()Z

    move-result v0

    return v0
.end method

.method public static a(ZI)Z
    .locals 1

    .prologue
    .line 711
    if-eqz p0, :cond_1

    const/16 v0, 0x9

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    const/16 v0, 0x17

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aU()V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    const/4 v4, 0x6

    const/4 v0, 0x0

    .line 1356
    iget v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->J:I

    sparse-switch v1, :sswitch_data_0

    .line 1410
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Invalid status type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1359
    :sswitch_0
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->i()Ljava/lang/String;

    move-result-object v1

    move-object v2, v0

    move-object v3, v1

    move-object v1, v0

    .line 1413
    :goto_0
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    .line 1414
    invoke-virtual {v0, v4}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1415
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1416
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1417
    invoke-virtual {v0, v4}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1413
    invoke-virtual {p0, v0}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 1418
    return-void

    .line 1365
    :sswitch_1
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->i()Ljava/lang/String;

    move-result-object v2

    .line 1366
    const-string/jumbo v1, "highlights"

    .line 1367
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->g:Ljava/lang/String;

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    .line 1368
    goto :goto_0

    .line 1374
    :sswitch_2
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->i()Ljava/lang/String;

    move-result-object v2

    .line 1375
    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->l:Ljava/lang/String;

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    .line 1377
    goto :goto_0

    .line 1380
    :sswitch_3
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->i()Ljava/lang/String;

    move-result-object v2

    .line 1381
    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->l:Ljava/lang/String;

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    .line 1383
    goto :goto_0

    .line 1386
    :sswitch_4
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->i()Ljava/lang/String;

    move-result-object v2

    .line 1387
    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->l:Ljava/lang/String;

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    .line 1389
    goto :goto_0

    .line 1392
    :sswitch_5
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->i()Ljava/lang/String;

    move-result-object v2

    .line 1393
    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->l:Ljava/lang/String;

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    .line 1395
    goto :goto_0

    .line 1398
    :sswitch_6
    const-string/jumbo v2, "list"

    .line 1399
    const-string/jumbo v1, "tweets"

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    .line 1401
    goto :goto_0

    .line 1404
    :sswitch_7
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->i()Ljava/lang/String;

    move-result-object v2

    .line 1405
    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->l:Ljava/lang/String;

    .line 1406
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->g:Ljava/lang/String;

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    .line 1407
    goto :goto_0

    .line 1356
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_4
        0x2 -> :sswitch_5
        0x3 -> :sswitch_1
        0x9 -> :sswitch_6
        0x13 -> :sswitch_3
        0x1b -> :sswitch_7
        0x1c -> :sswitch_2
        0x1e -> :sswitch_0
        0x26 -> :sswitch_2
        0x27 -> :sswitch_2
        0x28 -> :sswitch_2
    .end sparse-switch
.end method

.method private aV()Lcom/twitter/android/av;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Lcom/twitter/android/timeline/bk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1497
    new-instance v0, Lcom/twitter/app/common/timeline/TimelineFragment$5;

    invoke-direct {v0, p0}, Lcom/twitter/app/common/timeline/TimelineFragment$5;-><init>(Lcom/twitter/app/common/timeline/TimelineFragment;)V

    return-object v0
.end method

.method private aW()Lcom/twitter/android/av;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Lcom/twitter/android/timeline/bk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1510
    new-instance v0, Lcom/twitter/app/common/timeline/TimelineFragment$6;

    invoke-direct {v0, p0}, Lcom/twitter/app/common/timeline/TimelineFragment$6;-><init>(Lcom/twitter/app/common/timeline/TimelineFragment;)V

    return-object v0
.end method

.method private aX()Lcom/twitter/android/av;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Lcom/twitter/android/timeline/az;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1524
    new-instance v0, Lcom/twitter/app/common/timeline/TimelineFragment$7;

    invoke-direct {v0, p0}, Lcom/twitter/app/common/timeline/TimelineFragment$7;-><init>(Lcom/twitter/app/common/timeline/TimelineFragment;)V

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/app/common/timeline/TimelineFragment;)Lcjr;
    .locals 1

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->az()Lcjr;

    move-result-object v0

    return-object v0
.end method

.method private b(J)V
    .locals 5

    .prologue
    .line 1315
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1316
    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->m:Lcom/twitter/android/cp;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 1317
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x2

    const-string/jumbo v3, "stream::results"

    aput-object v3, v2, v0

    .line 1316
    invoke-static {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, p2, v0}, Lcom/twitter/android/cp;->a(JLjava/lang/String;)V

    .line 1318
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->n:Lti;

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-interface {v0, p1, p2, v2, v3}, Lti;->a(JJ)V

    .line 1319
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->C:Lcom/twitter/android/ci;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/ci;->b(J)V

    .line 1320
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1661
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1663
    invoke-direct {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->c(Ljava/lang/String;)J

    move-result-wide v2

    .line 1664
    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    .line 1666
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v4

    .line 1667
    iget-object v5, v4, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    .line 1668
    invoke-virtual {v5}, Landroid/widget/ListView;->getCount()I

    move-result v6

    move v0, v1

    .line 1669
    :goto_0
    if-ge v0, v6, :cond_1

    .line 1670
    invoke-virtual {v5, v0}, Landroid/widget/ListView;->getItemIdAtPosition(I)J

    move-result-wide v8

    cmp-long v7, v8, v2

    if-nez v7, :cond_2

    .line 1672
    invoke-virtual {v5}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    if-le v0, v2, :cond_0

    invoke-virtual {v5}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v2

    if-le v0, v2, :cond_1

    .line 1673
    :cond_0
    invoke-virtual {v4, v0, v1}, Lcom/twitter/app/common/list/l;->a(II)V

    .line 1680
    :cond_1
    return-void

    .line 1669
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 1695
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->N()Lcom/twitter/android/timeline/bl;

    move-result-object v1

    .line 1696
    if-eqz v1, :cond_1

    .line 1697
    invoke-virtual {v1}, Lcom/twitter/android/timeline/bl;->be_()I

    move-result v2

    .line 1698
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    .line 1699
    invoke-virtual {v1, v0}, Lcom/twitter/android/timeline/bl;->i(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1700
    invoke-virtual {v1, v0}, Lcom/twitter/android/timeline/bl;->d(I)J

    move-result-wide v0

    .line 1704
    :goto_1
    return-wide v0

    .line 1698
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1704
    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_1
.end method

.method static synthetic c(Lcom/twitter/app/common/timeline/TimelineFragment;)Lcom/twitter/android/util/v;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->E:Lcom/twitter/android/util/v;

    return-object v0
.end method

.method private c(J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1492
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "last_account_server_fetch"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->q:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/app/common/timeline/TimelineFragment;)Lcom/twitter/android/ab;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->v:Lcom/twitter/android/ab;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/app/common/timeline/TimelineFragment;)Lcom/twitter/android/ci;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->C:Lcom/twitter/android/ci;

    return-object v0
.end method

.method private k(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1019
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1020
    iget v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->J:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->J:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 1022
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v1

    .line 1023
    :goto_0
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1022
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private l(I)I
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    .line 1145
    packed-switch p1, :pswitch_data_0

    .line 1154
    const/16 v0, 0x28

    :goto_0
    return v0

    .line 1148
    :pswitch_0
    const/16 v0, 0x64

    goto :goto_0

    .line 1151
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->G()I

    move-result v0

    goto :goto_0

    .line 1145
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private s()J
    .locals 2

    .prologue
    .line 515
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->d:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->d:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private t()V
    .locals 3

    .prologue
    .line 978
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->y()Laiz;

    move-result-object v0

    invoke-static {v0}, Laja;->a(Laiz;)Lapb;

    move-result-object v0

    iget-object v0, v0, Lapb;->d:Landroid/net/Uri;

    .line 979
    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->T:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 980
    return-void
.end method

.method private x()Z
    .locals 1

    .prologue
    .line 1081
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->t:Lcom/twitter/android/platform/DeviceStorageLowReceiver;

    invoke-virtual {v0}, Lcom/twitter/android/platform/DeviceStorageLowReceiver;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->u:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected A()I
    .locals 1

    .prologue
    .line 378
    const/16 v0, 0x190

    return v0
.end method

.method protected B()V
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->s:Lcom/twitter/android/metrics/b;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/b;->j()V

    .line 383
    return-void
.end method

.method protected C()V
    .locals 2

    .prologue
    .line 783
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->az()Lcjr;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 784
    return-void
.end method

.method protected D()Lcom/twitter/app/common/timeline/TimelineFragment$e;
    .locals 10

    .prologue
    .line 788
    new-instance v1, Lcom/twitter/app/common/timeline/TimelineFragment$e;

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aO()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->H:Lcom/twitter/android/ck;

    .line 789
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/android/ck;

    invoke-direct {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->s()J

    move-result-wide v6

    iget-object v8, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->j:Lcom/twitter/android/timeline/be;

    iget v9, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->q:I

    move-object v2, p0

    invoke-direct/range {v1 .. v9}, Lcom/twitter/app/common/timeline/TimelineFragment$e;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lcom/twitter/android/ck;JLcom/twitter/android/timeline/be;I)V

    .line 788
    return-object v1
.end method

.method protected E()Lcom/twitter/app/common/timeline/TimelineFragment$d;
    .locals 1

    .prologue
    .line 795
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->E:Lcom/twitter/android/util/v;

    if-eqz v0, :cond_0

    .line 796
    new-instance v0, Lcom/twitter/app/common/timeline/TimelineFragment$d;

    invoke-direct {v0, p0}, Lcom/twitter/app/common/timeline/TimelineFragment$d;-><init>(Lcom/twitter/app/common/timeline/TimelineFragment;)V

    .line 798
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected F()V
    .locals 1

    .prologue
    .line 983
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->ak()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 984
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/timeline/TimelineFragment;->c(I)Z

    .line 985
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->h:Z

    .line 987
    :cond_0
    return-void
.end method

.method protected G()I
    .locals 1

    .prologue
    .line 1160
    const/4 v0, -0x1

    return v0
.end method

.method public synthetic H()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->k()Lcom/twitter/app/common/timeline/c;

    move-result-object v0

    return-object v0
.end method

.method protected H_()V
    .locals 1

    .prologue
    .line 991
    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->H_()V

    .line 992
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/timeline/TimelineFragment;->c(I)Z

    .line 993
    return-void
.end method

.method public synthetic I()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->k()Lcom/twitter/app/common/timeline/c;

    move-result-object v0

    return-object v0
.end method

.method public M()V
    .locals 1

    .prologue
    .line 1164
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1165
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    invoke-virtual {v0}, Lcom/twitter/android/cv;->notifyDataSetChanged()V

    .line 1167
    :cond_0
    return-void
.end method

.method protected M_()Z
    .locals 2

    .prologue
    .line 1446
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/timeline/TimelineFragment;->c(I)Z

    move-result v0

    .line 1447
    if-eqz v0, :cond_0

    .line 1448
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aT()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1449
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aR()V

    .line 1452
    :cond_0
    return v0
.end method

.method public N()Lcom/twitter/android/timeline/bl;
    .locals 1

    .prologue
    .line 1171
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    invoke-virtual {v0}, Lcom/twitter/android/cv;->f()Lcom/twitter/android/timeline/bl;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bl;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected O()Lcom/twitter/android/ck;
    .locals 6

    .prologue
    .line 553
    new-instance v0, Lcom/twitter/app/common/timeline/TimelineFragment$a;

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->o:Lcom/twitter/android/revenue/c;

    iget-object v4, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->j:Lcom/twitter/android/timeline/be;

    iget v5, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->q:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/app/common/timeline/TimelineFragment$a;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/revenue/c;Lcom/twitter/android/timeline/be;I)V

    return-object v0
.end method

.method protected P()Z
    .locals 1

    .prologue
    .line 559
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->B:Lcom/twitter/ui/view/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->B:Lcom/twitter/ui/view/h;

    iget-boolean v0, v0, Lcom/twitter/ui/view/h;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/android/ct;ZZ)Lcom/twitter/android/cf;
    .locals 32

    .prologue
    .line 724
    invoke-direct/range {p0 .. p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aV()Lcom/twitter/android/av;

    move-result-object v20

    .line 725
    invoke-direct/range {p0 .. p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aW()Lcom/twitter/android/av;

    move-result-object v21

    .line 726
    invoke-direct/range {p0 .. p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aX()Lcom/twitter/android/av;

    move-result-object v22

    .line 727
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v12

    .line 728
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->B:Lcom/twitter/ui/view/h;

    sget-object v3, Lcom/twitter/library/widget/TweetView;->c:Lcom/twitter/ui/view/h;

    invoke-static {v2, v3}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/twitter/ui/view/h;

    .line 730
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->getContext()Landroid/content/Context;

    move-result-object v11

    .line 731
    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 732
    invoke-static {v11}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 733
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v8

    .line 734
    new-instance v4, Lcom/twitter/app/users/c;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->S:Lcom/twitter/library/client/p;

    .line 735
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->i:Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v4, v2, v5, v6, v12}, Lcom/twitter/app/users/c;-><init>(Lcom/twitter/library/client/p;Lcom/twitter/library/client/Session;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 736
    new-instance v2, Lcom/twitter/app/users/d;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->i:Lcom/twitter/model/util/FriendshipCache;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->a_:J

    invoke-direct/range {v2 .. v7}, Lcom/twitter/app/users/d;-><init>(Landroid/view/LayoutInflater;Lcom/twitter/app/users/c;Lcom/twitter/model/util/FriendshipCache;J)V

    .line 738
    new-instance v13, Lcom/twitter/android/timeline/s;

    .line 739
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v13, v12, v4}, Lcom/twitter/android/timeline/s;-><init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Landroid/content/Context;)V

    .line 740
    new-instance v28, Ladj;

    move-object/from16 v0, v28

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v9}, Ladj;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/content/res/Resources;)V

    .line 742
    new-instance v29, Lcom/twitter/android/timeline/ch;

    new-instance v4, Lcom/twitter/android/client/n;

    .line 743
    invoke-virtual {v8}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    move-object/from16 v0, p1

    invoke-direct {v4, v0, v6, v7}, Lcom/twitter/android/client/n;-><init>(Landroid/content/Context;J)V

    move-object/from16 v0, v29

    invoke-direct {v0, v3, v9, v4}, Lcom/twitter/android/timeline/ch;-><init>(Landroid/view/LayoutInflater;Landroid/content/res/Resources;Lcom/twitter/android/client/n;)V

    .line 744
    new-instance v4, Lcom/twitter/android/timeline/r;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->j:Lcom/twitter/android/timeline/be;

    .line 745
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    .line 746
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aP()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->S:Lcom/twitter/library/client/p;

    move-object v7, v11

    invoke-direct/range {v4 .. v10}, Lcom/twitter/android/timeline/r;-><init>(Lcom/twitter/android/timeline/be;Landroid/support/v4/app/FragmentManager;Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/client/p;)V

    .line 747
    new-instance v30, Lcom/twitter/android/timeline/t;

    move-object/from16 v0, v30

    invoke-direct {v0, v3, v13, v4}, Lcom/twitter/android/timeline/t;-><init>(Landroid/view/LayoutInflater;Lcom/twitter/android/timeline/s;Lcom/twitter/android/timeline/r;)V

    .line 749
    new-instance v31, Lcom/twitter/android/timeline/ag;

    move-object/from16 v0, v31

    invoke-direct {v0, v3}, Lcom/twitter/android/timeline/ag;-><init>(Landroid/view/LayoutInflater;)V

    .line 750
    new-instance v3, Lcom/twitter/android/cf;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->J:I

    new-instance v13, Lcom/twitter/android/timeline/bn;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->q:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->j:Lcom/twitter/android/timeline/be;

    invoke-direct {v13, v4, v6}, Lcom/twitter/android/timeline/bn;-><init>(ILcom/twitter/android/timeline/be;)V

    new-instance v9, Lcom/twitter/app/users/c;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->S:Lcom/twitter/library/client/p;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->i:Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v9, v4, v8, v6, v12}, Lcom/twitter/app/users/c;-><init>(Lcom/twitter/library/client/p;Lcom/twitter/library/client/Session;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    new-instance v10, Lcom/twitter/android/ch;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->S:Lcom/twitter/library/client/p;

    .line 756
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v6

    invoke-direct {v10, v11, v4, v6, v12}, Lcom/twitter/android/ch;-><init>(Landroid/content/Context;Lcom/twitter/library/client/p;Lcom/twitter/library/client/v;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->i:Lcom/twitter/model/util/FriendshipCache;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->w:Lcom/twitter/android/timeline/cs;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->x:Lcom/twitter/android/timeline/cu;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->y:Lcom/twitter/android/timeline/av;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->z:Lcom/twitter/android/timeline/w;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->H:Lcom/twitter/android/ck;

    .line 760
    invoke-static {v4}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/twitter/android/ck;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->o:Lcom/twitter/android/revenue/c;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->a:Lcom/twitter/android/revenue/e;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->W:Lcom/twitter/android/av/j;

    move-object/from16 v25, v0

    new-instance v26, Lcom/twitter/android/ap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->j:Lcom/twitter/android/timeline/be;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v4}, Lcom/twitter/android/ap;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/android/timeline/be;)V

    move-object/from16 v4, p1

    move/from16 v6, p3

    move-object/from16 v7, p2

    move-object v8, v13

    move/from16 v13, p4

    move-object/from16 v27, v2

    invoke-direct/range {v3 .. v31}, Lcom/twitter/android/cf;-><init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;IZLcom/twitter/library/view/d;Lcom/twitter/android/timeline/bn;Lcom/twitter/app/users/c;Lcom/twitter/android/ch;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ZLcom/twitter/android/timeline/cs;Lcom/twitter/android/timeline/cu;Lcom/twitter/android/timeline/av;Lcom/twitter/android/timeline/w;Lcom/twitter/android/ck;Lcom/twitter/ui/view/h;Lcom/twitter/android/av;Lcom/twitter/android/av;Lcom/twitter/android/av;Lcom/twitter/android/revenue/c;Lcom/twitter/android/revenue/e;Lcom/twitter/android/av/t;Lcom/twitter/android/ap;Lcom/twitter/app/users/d;Ladj;Lcom/twitter/android/timeline/ch;Lcom/twitter/android/timeline/t;Lcom/twitter/android/timeline/ag;)V

    .line 767
    const/16 v2, 0x1c

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->J:I

    if-eq v2, v4, :cond_0

    .line 768
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 770
    :cond_0
    return-object v3
.end method

.method protected a(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/android/ct;Z)Lcom/twitter/android/cv;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 655
    iget-boolean v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->r:Z

    if-eqz v0, :cond_0

    .line 656
    invoke-virtual {p0, p1, p2, p3, v1}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/android/ct;ZZ)Lcom/twitter/android/cf;

    move-result-object v0

    .line 675
    :goto_0
    return-object v0

    .line 658
    :cond_0
    iget v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->J:I

    sparse-switch v0, :sswitch_data_0

    .line 669
    iget v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->J:I

    invoke-static {p3, v0}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(ZI)Z

    move-result v0

    .line 670
    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/app/common/timeline/TimelineFragment;->b(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/android/ct;Z)Lcom/twitter/android/cv;

    move-result-object v0

    goto :goto_0

    .line 664
    :sswitch_0
    invoke-virtual {p0, p1, p2, p3, v1}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/android/ct;ZZ)Lcom/twitter/android/cf;

    move-result-object v0

    goto :goto_0

    .line 658
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x3 -> :sswitch_0
        0x1b -> :sswitch_0
        0x1c -> :sswitch_0
        0x1e -> :sswitch_0
    .end sparse-switch
.end method

.method protected a(JJ)V
    .locals 1

    .prologue
    .line 941
    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/TweetListFragment;->a(JJ)V

    .line 942
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->s:Lcom/twitter/android/metrics/b;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/b;->k()V

    .line 943
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/common/timeline/TimelineFragment;->b(J)V

    .line 944
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->C:Lcom/twitter/android/ci;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/ci;->a(J)V

    .line 945
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->m:Lcom/twitter/android/cp;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/cp;->a(J)V

    .line 946
    return-void
.end method

.method public a(Landroid/view/View;Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 680
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/TweetListFragment;->a(Landroid/view/View;Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)V

    .line 681
    if-eqz p2, :cond_1

    .line 683
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->m:Lcom/twitter/android/cp;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/android/cp;->b(Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)V

    .line 684
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->n:Lti;

    invoke-interface {v0, p1, p2}, Lti;->a(Landroid/view/View;Ljava/lang/Object;)V

    .line 689
    :cond_0
    :goto_0
    return-void

    .line 685
    :cond_1
    const-string/jumbo v0, "ad_slot_id"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 687
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->m:Lcom/twitter/android/cp;

    invoke-virtual {v0, p3}, Lcom/twitter/android/cp;->a(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 177
    check-cast p2, Lcom/twitter/model/core/Tweet;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Landroid/view/View;Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 1274
    invoke-super/range {p0 .. p5}, Lcom/twitter/android/TweetListFragment;->a(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 1275
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk;

    .line 1276
    if-eqz v0, :cond_0

    .line 1277
    invoke-direct {p0, p2, v0, p3}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Landroid/view/View;Lcom/twitter/android/timeline/bk;I)V

    .line 1279
    :cond_0
    return-void
.end method

.method public a(Lbhq;)V
    .locals 2

    .prologue
    .line 409
    invoke-virtual {p1}, Lbhq;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 410
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->ay()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 412
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    invoke-virtual {v0}, Lcom/twitter/android/cv;->notifyDataSetChanged()V

    .line 414
    :cond_0
    return-void
.end method

.method public a(Lbhs;)V
    .locals 0

    .prologue
    .line 419
    return-void
.end method

.method protected a(Lcbi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/android/timeline/bk;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 348
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->v:Lcom/twitter/android/ab;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->v:Lcom/twitter/android/ab;

    invoke-interface {v0}, Lcom/twitter/android/ab;->c()V

    .line 352
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->F:Z

    if-nez v0, :cond_1

    .line 353
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->a(Lcbi;)V

    .line 355
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->s:Lcom/twitter/android/metrics/b;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/b;->aR_()V

    .line 356
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->Y:Lcom/twitter/app/common/list/a;

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->r()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/a;->a(Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 357
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->B()V

    .line 362
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->h:Z

    .line 363
    return-void

    .line 359
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->s:Lcom/twitter/android/metrics/b;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/b;->aS_()V

    .line 360
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->z()V

    goto :goto_0
.end method

.method public a(Lcno;I)V
    .locals 1

    .prologue
    .line 705
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->n:Lti;

    invoke-interface {v0, p2}, Lti;->a(I)V

    .line 706
    invoke-super {p0, p1, p2}, Lcom/twitter/android/TweetListFragment;->a(Lcno;I)V

    .line 707
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1113
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/TweetListFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 1114
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    if-ne p3, v0, :cond_0

    .line 1115
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    .line 1116
    invoke-virtual {v0}, Lcom/twitter/android/cv;->e()V

    .line 1117
    invoke-virtual {v0}, Lcom/twitter/android/cv;->notifyDataSetChanged()V

    .line 1120
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 1121
    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->s:Lcom/twitter/android/metrics/b;

    invoke-virtual {v1}, Lcom/twitter/android/metrics/b;->aT_()V

    .line 1122
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->B()V

    .line 1123
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1124
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v1

    const/16 v2, 0x191

    if-ne v1, v2, :cond_4

    .line 1125
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a09a4

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1131
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->E:Lcom/twitter/android/util/v;

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    if-eq p3, v0, :cond_2

    const/4 v0, 0x4

    if-eq p3, v0, :cond_2

    const/4 v0, 0x3

    if-ne p3, v0, :cond_3

    .line 1133
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->E:Lcom/twitter/android/util/v;

    invoke-virtual {v0, p2}, Lcom/twitter/android/util/v;->a(I)V

    .line 1135
    :cond_3
    return-void

    .line 1126
    :cond_4
    iget-object v0, v0, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "cancelled_no_messaging_required"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1127
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->T:Landroid/content/Context;

    invoke-virtual {p0, p2, p3}, Lcom/twitter/app/common/timeline/TimelineFragment;->b(II)I

    move-result v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 2

    .prologue
    .line 949
    iget-wide v0, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    iput-wide v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->a_:J

    .line 950
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->F()V

    .line 951
    return-void
.end method

.method public a(Lcom/twitter/ui/view/h;)V
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->B:Lcom/twitter/ui/view/h;

    .line 309
    return-void
.end method

.method protected a(Z)V
    .locals 1

    .prologue
    .line 693
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->a(Z)V

    .line 694
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->m:Lcom/twitter/android/cp;

    invoke-virtual {v0}, Lcom/twitter/android/cp;->a()V

    .line 695
    return-void
.end method

.method public a(JLcom/twitter/model/core/Tweet;Ljava/lang/Runnable;)Z
    .locals 1

    .prologue
    .line 962
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 963
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    .line 962
    invoke-static {v0, p1, p2, p3, p4}, Lcom/twitter/android/cv;->a(Landroid/widget/ListView;JLcom/twitter/model/core/Tweet;Ljava/lang/Runnable;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected aO()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    .line 1325
    iget v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->J:I

    sparse-switch v0, :sswitch_data_0

    .line 1346
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1328
    :sswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "::tweet:link:open_link"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1331
    :sswitch_1
    const-string/jumbo v0, "profile::tweet:link:open_link"

    goto :goto_0

    .line 1334
    :sswitch_2
    const-string/jumbo v0, "favorites::tweet:link:open_link"

    goto :goto_0

    .line 1337
    :sswitch_3
    const-string/jumbo v0, "connect:mentions:tweet:link:open_link"

    goto :goto_0

    .line 1340
    :sswitch_4
    const-string/jumbo v0, "connect:mentions_filtered:tweet:link:open_link"

    goto :goto_0

    .line 1343
    :sswitch_5
    const-string/jumbo v0, "connect:mentions_following:tweet:link:open_link"

    goto :goto_0

    .line 1325
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x5 -> :sswitch_3
        0x17 -> :sswitch_4
        0x18 -> :sswitch_5
        0x1b -> :sswitch_0
    .end sparse-switch
.end method

.method public aP()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 1429
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    return-object v0
.end method

.method protected aP_()V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 823
    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->aP_()V

    .line 824
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->k()Lcom/twitter/app/common/timeline/c;

    move-result-object v0

    .line 826
    const-string/jumbo v2, "ref_event"

    invoke-virtual {v0, v2}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->A:Z

    if-nez v2, :cond_0

    .line 827
    const-string/jumbo v2, "ref_event"

    invoke-virtual {v0, v2}, Lcom/twitter/app/common/list/i;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 828
    iput-boolean v5, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->A:Z

    .line 832
    :goto_0
    iget v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->J:I

    sparse-switch v2, :sswitch_data_0

    .line 874
    :goto_1
    return-void

    :cond_0
    move-object v0, v1

    .line 830
    goto :goto_0

    .line 836
    :sswitch_0
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    .line 837
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->i()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->l:Ljava/lang/String;

    aput-object v4, v3, v5

    const/4 v4, 0x2

    aput-object v1, v3, v4

    const/4 v4, 0x3

    aput-object v1, v3, v4

    const/4 v1, 0x4

    const-string/jumbo v4, "impression"

    aput-object v4, v3, v1

    invoke-direct {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>([Ljava/lang/String;)V

    .line 838
    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 839
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_1

    .line 843
    :sswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->l:Ljava/lang/String;

    invoke-static {v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":::impression"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 847
    :sswitch_2
    const-string/jumbo v1, "connect:mentions:::impression"

    invoke-direct {p0, v1, v0}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 851
    :sswitch_3
    const-string/jumbo v1, "connect:mentions_filtered:::impression"

    invoke-direct {p0, v1, v0}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 855
    :sswitch_4
    const-string/jumbo v1, "connect:mentions_following:::impression"

    invoke-direct {p0, v1, v0}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 859
    :sswitch_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->l:Ljava/lang/String;

    invoke-static {v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":::impression"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 863
    :sswitch_6
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    new-array v1, v5, [Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 864
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "::::impression"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-direct {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>([Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->g:Ljava/lang/String;

    const/4 v2, -0x1

    .line 866
    invoke-static {v1, v2}, Lcom/twitter/library/scribe/b;->b(Ljava/lang/String;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    .line 865
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 867
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_1

    .line 832
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_5
        0x5 -> :sswitch_2
        0x17 -> :sswitch_3
        0x18 -> :sswitch_4
        0x1b -> :sswitch_6
        0x1c -> :sswitch_0
        0x26 -> :sswitch_0
    .end sparse-switch
.end method

.method protected aQ()J
    .locals 4

    .prologue
    .line 1438
    const-string/jumbo v0, "timeline_auto_refresh_on_foreground_timeout_millis"

    const-wide/32 v2, 0xea60

    invoke-static {v0, v2, v3}, Lcoj;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 1441
    const-wide/16 v2, 0x0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public aR()V
    .locals 6

    .prologue
    .line 1468
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 1469
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 1470
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 1471
    invoke-direct {p0, v0, v1}, Lcom/twitter/app/common/timeline/TimelineFragment;->c(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v4

    invoke-interface {v2, v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1472
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1473
    return-void
.end method

.method aS()Z
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 1482
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->w()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aQ()J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method aT()Z
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 1713
    iget v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->q:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->q:I

    const/16 v1, 0xd

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected aa_()Lcom/twitter/app/common/list/b;
    .locals 1

    .prologue
    .line 626
    new-instance v0, Lcom/twitter/app/common/timeline/TimelineFragment$c;

    invoke-direct {v0, p0}, Lcom/twitter/app/common/timeline/TimelineFragment$c;-><init>(Lcom/twitter/app/common/timeline/TimelineFragment;)V

    return-object v0
.end method

.method protected ar()Z
    .locals 1

    .prologue
    .line 1457
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->q()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/timeline/TimelineFragment;->c(I)Z

    move-result v0

    return v0
.end method

.method protected b(II)I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 1140
    const v0, 0x7f0a0998

    return v0
.end method

.method protected b(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/android/ct;Z)Lcom/twitter/android/cv;
    .locals 6

    .prologue
    .line 778
    new-instance v0, Lcom/twitter/android/cv;

    iget-object v4, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->i:Lcom/twitter/model/util/FriendshipCache;

    .line 779
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v5

    move-object v1, p1

    move v2, p3

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/cv;-><init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;ZLcom/twitter/library/view/d;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 778
    return-object v0
.end method

.method protected b()V
    .locals 4

    .prologue
    .line 803
    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->b()V

    .line 805
    iget-wide v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->a_:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aE()Z

    move-result v0

    if-nez v0, :cond_1

    .line 807
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->F()V

    .line 810
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->S:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->e:Lcom/twitter/library/service/t;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/t;)V

    .line 811
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->n:Lti;

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-interface {v0, v1}, Lti;->a(Landroid/view/ViewGroup;)V

    .line 812
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->a:Lcom/twitter/android/revenue/e;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/e;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 813
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->M()V

    .line 815
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->a:Lcom/twitter/android/revenue/e;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/e;->b()V

    .line 816
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->c:Lcom/twitter/app/common/timeline/TimelineFragment$d;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/u;)V

    .line 817
    return-void
.end method

.method public b(Lcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 971
    invoke-direct {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->t()V

    .line 972
    return-void
.end method

.method protected c(I)Z
    .locals 1

    .prologue
    .line 1088
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(ILcom/twitter/library/service/s;)Z

    move-result v0

    return v0
.end method

.method protected c_(I)Z
    .locals 1

    .prologue
    .line 1108
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->c_(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v0

    invoke-virtual {v0}, Lcrr;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected e(I)Lcom/twitter/library/service/s;
    .locals 3

    .prologue
    .line 1036
    new-instance v1, Lajd;

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->i(I)Lajc$a;

    move-result-object v0

    invoke-virtual {v0}, Lajc$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajc;

    invoke-direct {v1, v2, v0}, Lajd;-><init>(Landroid/content/Context;Lajc;)V

    .line 1037
    invoke-virtual {v1}, Lajd;->a()Lcom/twitter/library/service/b;

    move-result-object v0

    .line 1036
    return-object v0
.end method

.method public e(J)V
    .locals 0

    .prologue
    .line 955
    invoke-direct {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->t()V

    .line 956
    return-void
.end method

.method protected f()Lcom/twitter/android/cp;
    .locals 5

    .prologue
    .line 541
    new-instance v0, Lcom/twitter/android/cq;

    invoke-direct {v0}, Lcom/twitter/android/cq;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v2

    iget v3, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->J:I

    .line 542
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v4

    .line 541
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/cq;->a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ILcom/twitter/library/client/v;)Lcom/twitter/android/cp;

    move-result-object v0

    return-object v0
.end method

.method protected i(I)Lajc$a;
    .locals 7

    .prologue
    .line 1042
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 1043
    new-instance v1, Lajc$a;

    invoke-direct {v1, v0}, Lajc$a;-><init>(Lcom/twitter/library/client/Session;)V

    iget v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->q:I

    .line 1044
    invoke-virtual {v1, v2}, Lajc$a;->a(I)Lajc$a;

    move-result-object v1

    .line 1045
    invoke-virtual {v1, p1}, Lajc$a;->b(I)Lajc$a;

    move-result-object v1

    .line 1046
    invoke-direct {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->l(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lajc$a;->c(I)Lajc$a;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->a_:J

    .line 1047
    invoke-virtual {v1, v2, v3}, Lajc$a;->d(J)Lajc$a;

    move-result-object v1

    .line 1048
    invoke-direct {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->k(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lajc$a;->a(Ljava/lang/String;)Lajc$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->g:Ljava/lang/String;

    .line 1049
    invoke-virtual {v1, v2}, Lajc$a;->b(Ljava/lang/String;)Lajc$a;

    move-result-object v1

    .line 1050
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->j(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lajc$a;->d(I)Lajc$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->o:Lcom/twitter/android/revenue/c;

    .line 1051
    invoke-virtual {v1, v2}, Lajc$a;->a(Lcom/twitter/android/revenue/c;)Lajc$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->p:Lbwb;

    .line 1052
    invoke-virtual {v1, v2}, Lajc$a;->a(Lbwb;)Lajc$a;

    move-result-object v6

    .line 1055
    iget-boolean v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->r:Z

    if-eqz v1, :cond_0

    .line 1056
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->N()Lcom/twitter/android/timeline/bl;

    move-result-object v1

    .line 1057
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/twitter/android/timeline/bl;->g()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1060
    new-instance v0, Lbgk;

    invoke-virtual {v1}, Lcom/twitter/android/timeline/bl;->d()Lcom/twitter/android/timeline/cf;

    move-result-object v2

    invoke-direct {v0, v2}, Lbgk;-><init>(Lcom/twitter/android/timeline/cf;)V

    invoke-virtual {v6, v0}, Lajc$a;->a(Lbgp;)Lajc$a;

    .line 1061
    new-instance v0, Lbgk;

    invoke-virtual {v1}, Lcom/twitter/android/timeline/bl;->e()Lcom/twitter/android/timeline/cf;

    move-result-object v1

    invoke-direct {v0, v1}, Lbgk;-><init>(Lcom/twitter/android/timeline/cf;)V

    invoke-virtual {v6, v0}, Lajc$a;->b(Lbgp;)Lajc$a;

    .line 1074
    :goto_0
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->aa:Lbgw;

    invoke-virtual {v6, v0}, Lajc$a;->a(Lbgw;)Lajc$a;

    .line 1076
    :cond_0
    return-object v6

    .line 1066
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    .line 1067
    new-instance v5, Lcom/twitter/android/timeline/ce;

    invoke-direct {v5}, Lcom/twitter/android/timeline/ce;-><init>()V

    .line 1068
    iget v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->q:I

    iget-wide v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->a_:J

    iget-object v4, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->g:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lbgl;->a(Lcom/twitter/library/provider/t;IJLjava/lang/String;Lcom/twitter/android/timeline/ce;)Lbgp;

    move-result-object v1

    invoke-virtual {v6, v1}, Lajc$a;->a(Lbgp;)Lajc$a;

    .line 1070
    iget v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->q:I

    iget-wide v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->a_:J

    iget-object v4, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->g:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lbgl;->b(Lcom/twitter/library/provider/t;IJLjava/lang/String;Lcom/twitter/android/timeline/ce;)Lbgp;

    move-result-object v0

    invoke-virtual {v6, v0}, Lajc$a;->b(Lbgp;)Lajc$a;

    goto :goto_0
.end method

.method protected i(Lank;)Lanm;
    .locals 2

    .prologue
    .line 424
    invoke-static {}, Lcom/twitter/app/common/timeline/b;->c()Lcom/twitter/app/common/timeline/b$a;

    move-result-object v0

    .line 425
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/timeline/b$a;->a(Lamu;)Lcom/twitter/app/common/timeline/b$a;

    move-result-object v0

    .line 426
    invoke-virtual {v0}, Lcom/twitter/app/common/timeline/b$a;->a()Lcom/twitter/app/common/timeline/d;

    move-result-object v0

    .line 424
    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    .line 1201
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1202
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->k:Ljava/lang/String;

    .line 1225
    :goto_0
    return-object v0

    .line 1204
    :cond_0
    iget v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->J:I

    sparse-switch v0, :sswitch_data_0

    .line 1225
    const-string/jumbo v0, "unknown"

    goto :goto_0

    .line 1206
    :sswitch_0
    const-string/jumbo v0, "home"

    goto :goto_0

    .line 1210
    :sswitch_1
    const-string/jumbo v0, "trendsplus"

    goto :goto_0

    .line 1213
    :sswitch_2
    const-string/jumbo v0, "custom"

    goto :goto_0

    .line 1216
    :sswitch_3
    const-string/jumbo v0, "favorites"

    goto :goto_0

    .line 1219
    :sswitch_4
    const-string/jumbo v0, "place"

    goto :goto_0

    .line 1222
    :sswitch_5
    const-string/jumbo v0, "live_video_timeline"

    goto :goto_0

    .line 1204
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_3
        0x3 -> :sswitch_5
        0x1b -> :sswitch_2
        0x1c -> :sswitch_1
        0x1e -> :sswitch_4
        0x26 -> :sswitch_1
    .end sparse-switch
.end method

.method protected j(I)I
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    .line 1177
    packed-switch p1, :pswitch_data_0

    .line 1192
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1179
    :pswitch_1
    const/4 v0, 0x1

    .line 1189
    :goto_0
    return v0

    .line 1182
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 1185
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 1189
    :pswitch_4
    const/4 v0, 0x2

    goto :goto_0

    .line 1177
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_4
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected j()Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 301
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->s:Lcom/twitter/android/metrics/b;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/b;->aQ_()V

    .line 302
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->y()Laiz;

    move-result-object v0

    invoke-static {v0}, Laja;->a(Laiz;)Lapb;

    move-result-object v6

    .line 303
    new-instance v0, Lcom/twitter/util/android/d;

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, v6, Lapb;->d:Landroid/net/Uri;

    iget-object v3, v6, Lapb;->e:[Ljava/lang/String;

    iget-object v4, v6, Lapb;->a:Ljava/lang/String;

    iget-object v5, v6, Lapb;->b:[Ljava/lang/String;

    iget-object v6, v6, Lapb;->c:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public abstract k()Lcom/twitter/app/common/timeline/c;
.end method

.method protected l()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 997
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aC()Landroid/database/Cursor;

    move-result-object v2

    .line 998
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->moveToLast()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 999
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->A()I

    move-result v3

    .line 1000
    iget-boolean v4, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->f:Z

    if-eqz v4, :cond_2

    invoke-direct {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->x()Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1001
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->an()Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v3, 0x10

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_1

    if-eqz v0, :cond_1

    .line 1002
    invoke-virtual {p0, v1}, Lcom/twitter/app/common/timeline/TimelineFragment;->c(I)Z

    .line 1005
    :cond_1
    return-void

    .line 1000
    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-ge v4, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method protected n()Lti;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lti",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 547
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    invoke-static {v0, v1}, Ltj;->a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lti;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 21

    .prologue
    .line 564
    invoke-super/range {p0 .. p1}, Lcom/twitter/android/TweetListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 566
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aK()Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v20

    .line 567
    move-object/from16 v0, v20

    instance-of v2, v0, Lcom/twitter/android/ProfileActivity;

    if-eqz v2, :cond_0

    move-object/from16 v2, v20

    .line 570
    check-cast v2, Lcom/twitter/android/ProfileActivity;

    invoke-virtual {v2}, Lcom/twitter/android/ProfileActivity;->b()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->i:Lcom/twitter/model/util/FriendshipCache;

    .line 573
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->i:Lcom/twitter/model/util/FriendshipCache;

    if-nez v2, :cond_1

    .line 575
    new-instance v2, Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v2}, Lcom/twitter/model/util/FriendshipCache;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->i:Lcom/twitter/model/util/FriendshipCache;

    .line 578
    :cond_1
    new-instance v7, Lbwm;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    .line 579
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v3

    invoke-direct {v7, v2, v3}, Lbwm;-><init>(Lcom/twitter/library/client/v;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 580
    new-instance v10, Lajb;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->T:Landroid/content/Context;

    new-instance v3, Lcom/twitter/library/service/v;

    .line 581
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    invoke-direct {v10, v2, v3}, Lajb;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V

    .line 582
    new-instance v11, Lcom/twitter/android/ac;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->q:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->T:Landroid/content/Context;

    .line 583
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->S:Lcom/twitter/library/client/p;

    new-instance v2, Lcom/twitter/android/cg;

    new-instance v3, Lcom/twitter/app/common/timeline/TimelineFragment$4;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/twitter/app/common/timeline/TimelineFragment$4;-><init>(Lcom/twitter/app/common/timeline/TimelineFragment;)V

    .line 591
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->S:Lcom/twitter/library/client/p;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->T:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->H:Lcom/twitter/android/ck;

    .line 592
    invoke-static {v8}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/android/ck;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->i:Lcom/twitter/model/util/FriendshipCache;

    invoke-direct/range {v2 .. v10}, Lcom/twitter/android/cg;-><init>(Lcom/twitter/util/object/j;Lcom/twitter/library/client/v;Lcom/twitter/library/client/p;Landroid/content/Context;Lbwm;Lcom/twitter/android/ck;Lcom/twitter/model/util/FriendshipCache;Lajb;)V

    move-object/from16 v16, v7

    move-object/from16 v17, v2

    move-object/from16 v18, p1

    move-object/from16 v19, v10

    invoke-direct/range {v11 .. v19}, Lcom/twitter/android/ac;-><init>(ILandroid/content/Context;Lcom/twitter/library/client/v;Lcom/twitter/library/client/p;Lbwm;Lcom/twitter/library/widget/InlineDismissView$a;Landroid/os/Bundle;Lajb;)V

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->v:Lcom/twitter/android/ab;

    .line 595
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->ay()Z

    move-result v2

    if-nez v2, :cond_4

    .line 596
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->D()Lcom/twitter/app/common/timeline/TimelineFragment$e;

    move-result-object v3

    .line 597
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->T:Landroid/content/Context;

    .line 598
    invoke-static {v2}, Lcom/twitter/android/client/k;->a(Landroid/content/Context;)Lcom/twitter/android/client/k;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/client/k;->a()Z

    move-result v4

    .line 599
    invoke-static/range {v20 .. v20}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/android/ct;Z)Lcom/twitter/android/cv;

    move-result-object v4

    .line 601
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v3}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcom/twitter/android/cv;Lcom/twitter/app/common/timeline/TimelineFragment$e;)Lcom/twitter/android/util/v;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->E:Lcom/twitter/android/util/v;

    .line 602
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->E()Lcom/twitter/app/common/timeline/TimelineFragment$d;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/app/common/timeline/TimelineFragment;->c:Lcom/twitter/app/common/timeline/TimelineFragment$d;

    .line 603
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcom/twitter/android/client/j;)Lcom/twitter/app/common/list/TwitterListFragment;

    .line 604
    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lcom/twitter/android/cv;->b(Lcom/twitter/android/av;)V

    .line 606
    if-eqz p1, :cond_3

    .line 608
    const-string/jumbo v2, "spinning_gap_ids"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v3

    .line 609
    if-eqz v3, :cond_3

    .line 610
    array-length v5, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v5, :cond_2

    aget-wide v6, v3, v2

    .line 611
    invoke-virtual {v4, v6, v7}, Lcom/twitter/android/cv;->a(J)V

    .line 610
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 613
    :cond_2
    invoke-virtual {v4}, Lcom/twitter/android/cv;->notifyDataSetChanged()V

    .line 617
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/twitter/app/common/list/l;->a(Lcjr;)V

    .line 620
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->C()V

    .line 621
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 897
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    if-ne p1, v6, :cond_1

    if-eqz p3, :cond_1

    const-string/jumbo v0, "woeid"

    .line 898
    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 899
    const-string/jumbo v0, "woeid"

    const-wide/16 v2, 0x1

    invoke-virtual {p3, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 901
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 902
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v3

    .line 903
    if-eqz v3, :cond_1

    iget-boolean v4, v3, Lcom/twitter/model/account/UserSettings;->C:Z

    if-nez v4, :cond_0

    iget-wide v4, v3, Lcom/twitter/model/account/UserSettings;->a:J

    cmp-long v4, v4, v0

    if-eqz v4, :cond_1

    .line 905
    :cond_0
    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/twitter/model/account/UserSettings;->C:Z

    .line 906
    iput-wide v0, v3, Lcom/twitter/model/account/UserSettings;->a:J

    .line 907
    const-string/jumbo v0, "loc_name"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/twitter/model/account/UserSettings;->b:Ljava/lang/String;

    .line 909
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->S:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->T:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v6, v4}, Lbbg;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;ZLjava/lang/String;)Lbbg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 913
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v2, 0x1

    .line 431
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 432
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->ai()Lanm;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/timeline/a;

    .line 433
    invoke-interface {v0}, Lcom/twitter/app/common/timeline/a;->a()Lcom/twitter/android/metrics/b;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->s:Lcom/twitter/android/metrics/b;

    .line 434
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->s:Lcom/twitter/android/metrics/b;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/b;->i()V

    .line 436
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->k()Lcom/twitter/app/common/timeline/c;

    move-result-object v7

    .line 437
    invoke-static {p1}, Lank;->a(Landroid/os/Bundle;)Lank;

    move-result-object v3

    .line 439
    invoke-virtual {v7}, Lcom/twitter/app/common/timeline/c;->d()I

    move-result v0

    iput v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->J:I

    .line 440
    invoke-virtual {v7}, Lcom/twitter/app/common/timeline/c;->c()I

    move-result v0

    iput v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->q:I

    .line 441
    invoke-virtual {v7}, Lcom/twitter/app/common/timeline/c;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->r:Z

    .line 442
    invoke-virtual {v7}, Lcom/twitter/app/common/timeline/c;->b()Lbgw;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->aa:Lbgw;

    .line 443
    iget-object v0, v7, Lcom/twitter/app/common/timeline/c;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->g:Ljava/lang/String;

    .line 444
    iget-object v0, v7, Lcom/twitter/app/common/timeline/c;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->l:Ljava/lang/String;

    .line 445
    iget-object v0, v7, Lcom/twitter/app/common/timeline/c;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->k:Ljava/lang/String;

    .line 446
    iget-object v0, v7, Lcom/twitter/app/common/timeline/c;->h:Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->d:Lcom/twitter/model/core/TwitterUser;

    .line 449
    if-eqz p1, :cond_5

    .line 450
    const-string/jumbo v0, "impressed_who_to_follow_modules"

    .line 451
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 450
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 452
    const-string/jumbo v1, "impressed_who_to_follow_users"

    .line 453
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    .line 452
    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashSet;

    .line 454
    const-string/jumbo v4, "scribed_ref_event"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->A:Z

    .line 459
    :goto_0
    invoke-direct {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aU()V

    .line 461
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    .line 462
    invoke-static {v8}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->b:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 463
    invoke-interface {v4, v5}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 465
    new-instance v4, Lcom/twitter/app/common/timeline/TimelineFragment$b;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/twitter/app/common/timeline/TimelineFragment$b;-><init>(Lcom/twitter/app/common/timeline/TimelineFragment;Lcom/twitter/app/common/timeline/TimelineFragment$1;)V

    iput-object v4, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->e:Lcom/twitter/library/service/t;

    .line 467
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v5

    .line 468
    new-instance v4, Lcom/twitter/android/timeline/be;

    iget-object v6, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->S:Lcom/twitter/library/client/p;

    .line 469
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v9

    new-instance v10, Lcom/twitter/app/common/timeline/TimelineFragment$2;

    invoke-direct {v10, p0}, Lcom/twitter/app/common/timeline/TimelineFragment$2;-><init>(Lcom/twitter/app/common/timeline/TimelineFragment;)V

    invoke-direct {v4, v8, v6, v9, v10}, Lcom/twitter/android/timeline/be;-><init>(Landroid/content/Context;Lcom/twitter/library/client/p;Lcom/twitter/library/client/v;Lcom/twitter/android/timeline/be$a;)V

    iput-object v4, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->j:Lcom/twitter/android/timeline/be;

    .line 477
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v4

    .line 478
    iget v6, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->J:I

    .line 479
    invoke-static {v6}, Lcom/twitter/model/core/w$a;->a(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 480
    invoke-static {v4}, Lcom/twitter/android/util/ak;->a(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    iget v6, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->J:I

    if-ne v6, v2, :cond_6

    .line 482
    invoke-direct {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->s()J

    move-result-wide v10

    invoke-static {v4, v10, v11}, Lcom/twitter/android/util/ak;->a(Lcom/twitter/model/core/TwitterUser;J)Z

    move-result v4

    if-eqz v4, :cond_6

    :cond_1
    :goto_1
    iput-boolean v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->f:Z

    .line 483
    iget-boolean v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->f:Z

    if-eqz v2, :cond_3

    .line 484
    if-eqz p1, :cond_2

    .line 486
    const-string/jumbo v2, "is_device_storage_low"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->u:Z

    .line 488
    :cond_2
    new-instance v2, Lcom/twitter/android/platform/DeviceStorageLowReceiver;

    invoke-direct {v2}, Lcom/twitter/android/platform/DeviceStorageLowReceiver;-><init>()V

    iput-object v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->t:Lcom/twitter/android/platform/DeviceStorageLowReceiver;

    .line 489
    iget-object v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->t:Lcom/twitter/android/platform/DeviceStorageLowReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string/jumbo v6, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-direct {v4, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2, v4}, Landroid/support/v4/app/FragmentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 493
    :cond_3
    new-instance v2, Lcom/twitter/android/timeline/cs;

    invoke-direct {v2, v5, v0}, Lcom/twitter/android/timeline/cs;-><init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/util/HashSet;)V

    iput-object v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->w:Lcom/twitter/android/timeline/cs;

    .line 495
    new-instance v0, Lcom/twitter/android/timeline/cu;

    invoke-direct {v0, v5, v1}, Lcom/twitter/android/timeline/cu;-><init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/util/HashSet;)V

    iput-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->x:Lcom/twitter/android/timeline/cu;

    .line 497
    new-instance v0, Lcom/twitter/android/timeline/av;

    invoke-direct {v0, v5, p1}, Lcom/twitter/android/timeline/av;-><init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->y:Lcom/twitter/android/timeline/av;

    .line 499
    new-instance v1, Lcom/twitter/android/timeline/w;

    const-string/jumbo v0, "footer_impression_helper_id"

    .line 500
    invoke-virtual {v3, v0}, Lank;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/FooterImpressionState;

    invoke-direct {v1, v5, v0}, Lcom/twitter/android/timeline/w;-><init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/timeline/FooterImpressionState;)V

    iput-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->z:Lcom/twitter/android/timeline/w;

    .line 501
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->ag()Lanh;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->z:Lcom/twitter/android/timeline/w;

    invoke-virtual {v0, v1}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 502
    new-instance v0, Lcom/twitter/android/ci;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->S:Lcom/twitter/library/client/p;

    iget-object v3, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->T:Landroid/content/Context;

    iget v4, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->q:I

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ci;-><init>(Lcom/twitter/library/client/v;Lcom/twitter/library/client/p;Landroid/content/Context;ILcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    iput-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->C:Lcom/twitter/android/ci;

    .line 504
    new-instance v0, Lcom/twitter/android/revenue/c;

    iget-wide v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->a_:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/android/revenue/c;-><init>(J)V

    iput-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->o:Lcom/twitter/android/revenue/c;

    .line 505
    new-instance v0, Lcom/twitter/android/revenue/e;

    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->o:Lcom/twitter/android/revenue/c;

    invoke-direct {v0, v1}, Lcom/twitter/android/revenue/e;-><init>(Lcom/twitter/android/revenue/c;)V

    iput-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->a:Lcom/twitter/android/revenue/e;

    .line 506
    new-instance v0, Lcom/twitter/android/TimelineItemClickHandler;

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->i()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->l:Ljava/lang/String;

    .line 507
    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->o:Lcom/twitter/android/revenue/c;

    move-object v1, v8

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/TimelineItemClickHandler;-><init>(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/app/FragmentManager;Lcom/twitter/android/revenue/c;)V

    iput-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->D:Lcom/twitter/android/TimelineItemClickHandler;

    .line 509
    invoke-virtual {v7}, Lcom/twitter/app/common/timeline/c;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 510
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lafx;->a(Landroid/content/Context;)Lafx;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->p:Lbwb;

    .line 512
    :cond_4
    return-void

    .line 456
    :cond_5
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 457
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    goto/16 :goto_0

    .line 482
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 917
    invoke-static {p0}, Lcom/twitter/library/provider/o;->a(Lcom/twitter/library/provider/o$a;)V

    .line 919
    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->onDestroy()V

    .line 921
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 922
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->b:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 923
    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 925
    iget-boolean v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->f:Z

    if-eqz v1, :cond_0

    .line 926
    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->t:Lcom/twitter/android/platform/DeviceStorageLowReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 927
    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->S:Lcom/twitter/library/client/p;

    .line 928
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 927
    invoke-static {v0, v2}, Lcom/twitter/android/util/ak;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lcom/twitter/android/util/ak$a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 930
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->v:Lcom/twitter/android/ab;

    if-eqz v0, :cond_1

    .line 931
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->v:Lcom/twitter/android/ab;

    invoke-interface {v0}, Lcom/twitter/android/ab;->b()V

    .line 934
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->p:Lbwb;

    if-eqz v0, :cond_2

    .line 935
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->p:Lbwb;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 937
    :cond_2
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1283
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1284
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1285
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    invoke-virtual {v0}, Lcom/twitter/android/cv;->d()Ljava/util/List;

    move-result-object v0

    .line 1286
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1287
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v0

    .line 1288
    const-string/jumbo v1, "spinning_gap_ids"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 1291
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->t:Lcom/twitter/android/platform/DeviceStorageLowReceiver;

    if-eqz v0, :cond_1

    .line 1292
    const-string/jumbo v0, "is_device_storage_low"

    invoke-direct {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->x()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1294
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->w:Lcom/twitter/android/timeline/cs;

    if-eqz v0, :cond_2

    .line 1295
    const-string/jumbo v0, "impressed_who_to_follow_modules"

    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->w:Lcom/twitter/android/timeline/cs;

    .line 1296
    invoke-virtual {v1}, Lcom/twitter/android/timeline/cs;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 1295
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1298
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->x:Lcom/twitter/android/timeline/cu;

    if-eqz v0, :cond_3

    .line 1299
    const-string/jumbo v0, "impressed_who_to_follow_users"

    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->x:Lcom/twitter/android/timeline/cu;

    .line 1300
    invoke-virtual {v1}, Lcom/twitter/android/timeline/cu;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 1299
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1302
    :cond_3
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->y:Lcom/twitter/android/timeline/av;

    if-eqz v0, :cond_4

    .line 1303
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->y:Lcom/twitter/android/timeline/av;

    invoke-virtual {v0, p1}, Lcom/twitter/android/timeline/av;->a(Landroid/os/Bundle;)V

    .line 1305
    :cond_4
    const-string/jumbo v0, "scribed_ref_event"

    iget-boolean v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->A:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1307
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->v:Lcom/twitter/android/ab;

    if-eqz v0, :cond_5

    .line 1308
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->v:Lcom/twitter/android/ab;

    invoke-interface {v0, p1}, Lcom/twitter/android/ab;->a(Landroid/os/Bundle;)V

    .line 1310
    :cond_5
    return-void
.end method

.method public onStop()V
    .locals 4

    .prologue
    .line 699
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->m:Lcom/twitter/android/cp;

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/cp;->a(J)V

    .line 700
    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->onStop()V

    .line 701
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 520
    invoke-super {p0, p1, p2}, Lcom/twitter/android/TweetListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 522
    invoke-static {p0}, Lcom/twitter/library/provider/o;->b(Lcom/twitter/library/provider/o$a;)V

    .line 523
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/common/timeline/TimelineFragment$3;

    invoke-direct {v1, p0}, Lcom/twitter/app/common/timeline/TimelineFragment$3;-><init>(Lcom/twitter/app/common/timeline/TimelineFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l;->a(Lcno$c;)V

    .line 534
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->a:Lcom/twitter/android/revenue/e;

    invoke-virtual {v1}, Lcom/twitter/android/revenue/e;->e()Lcno$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l;->a(Lcno$c;)V

    .line 535
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->f()Lcom/twitter/android/cp;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->m:Lcom/twitter/android/cp;

    .line 536
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->n()Lti;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->n:Lti;

    .line 537
    return-void
.end method

.method protected q()I
    .locals 1

    .prologue
    .line 331
    const/4 v0, 0x3

    return v0
.end method

.method protected q_()V
    .locals 4

    .prologue
    .line 885
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->s:Lcom/twitter/android/metrics/b;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/b;->k()V

    .line 886
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/twitter/app/common/timeline/TimelineFragment;->b(J)V

    .line 887
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->C:Lcom/twitter/android/ci;

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/ci;->a(J)V

    .line 888
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->S:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->e:Lcom/twitter/library/service/t;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->b(Lcom/twitter/library/service/t;)V

    .line 889
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->n:Lti;

    invoke-interface {v0}, Lti;->a()V

    .line 890
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->a:Lcom/twitter/android/revenue/e;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/e;->c()V

    .line 891
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->c:Lcom/twitter/app/common/timeline/TimelineFragment$d;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/u;)V

    .line 892
    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->q_()V

    .line 893
    return-void
.end method

.method protected r()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1008
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1009
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    .line 1010
    invoke-virtual {v0}, Lcom/twitter/android/cv;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    iget v3, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->J:I

    if-ne v3, v2, :cond_1

    iget-boolean v3, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->h:Z

    if-nez v3, :cond_1

    .line 1011
    invoke-virtual {v0}, Lcom/twitter/android/cv;->getCount()I

    move-result v0

    const/16 v3, 0x14

    if-ge v0, v3, :cond_1

    :cond_0
    move v0, v2

    .line 1013
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1011
    goto :goto_0

    :cond_2
    move v0, v1

    .line 1013
    goto :goto_0
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 1754
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Z
    .locals 1

    .prologue
    .line 1759
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aE()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w()J
    .locals 6

    .prologue
    .line 1765
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 1766
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 1767
    invoke-direct {p0, v0, v1}, Lcom/twitter/app/common/timeline/TimelineFragment;->c(J)Ljava/lang/String;

    move-result-object v0

    .line 1768
    const-wide/16 v4, 0x0

    invoke-interface {v2, v0, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method protected y()Laiz;
    .locals 4

    .prologue
    .line 313
    new-instance v0, Laiz$a;

    invoke-direct {v0}, Laiz$a;-><init>()V

    iget v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->q:I

    .line 314
    invoke-virtual {v0, v1}, Laiz$a;->a(I)Laiz$a;

    move-result-object v0

    .line 315
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Laiz$a;->a(J)Laiz$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->a_:J

    .line 316
    invoke-virtual {v0, v2, v3}, Laiz$a;->b(J)Laiz$a;

    move-result-object v0

    .line 317
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->k()Lcom/twitter/app/common/timeline/c;

    move-result-object v1

    const-string/jumbo v2, "is_me"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/twitter/app/common/timeline/c;->a(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Laiz$a;->a(Z)Laiz$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment;->g:Ljava/lang/String;

    .line 318
    invoke-virtual {v0, v1}, Laiz$a;->a(Ljava/lang/String;)Laiz$a;

    move-result-object v0

    .line 319
    invoke-virtual {v0}, Laiz$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laiz;

    .line 313
    return-object v0
.end method

.method protected z()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 372
    return-void
.end method
