.class public Lcom/twitter/app/common/app/TwitterApplication;
.super Lcom/twitter/app/common/app/InjectedApplication;
.source "Twttr"


# instance fields
.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-string/jumbo v0, "com.twitter.android"

    invoke-static {v0}, Lcog;->a(Ljava/lang/String;)V

    .line 53
    invoke-static {}, Lcom/twitter/app/common/app/TwitterApplication;->b()V

    .line 54
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/twitter/app/common/app/InjectedApplication;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;J)V
    .locals 5

    .prologue
    .line 152
    invoke-static {p0}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;)Lcom/twitter/util/math/Size;

    move-result-object v1

    .line 153
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "app::::launch"

    aput-object v4, v2, v3

    .line 154
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "display_info:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 155
    invoke-virtual {v1}, Lcom/twitter/util/math/Size;->c()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/twitter/util/math/Size;->d()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 156
    invoke-static {}, Lcom/twitter/util/z;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 155
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 157
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v1

    invoke-virtual {v1}, Lbqg;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "location_enabled"

    :goto_0
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->k(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 159
    invoke-virtual {v0, p0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Landroid/content/Context;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 153
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 160
    return-void

    .line 157
    :cond_0
    const-string/jumbo v1, "location_disabled"

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 139
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {p0, v0}, Lcom/twitter/util/d;->a(Landroid/content/Context;I)Landroid/app/ActivityManager$RunningAppProcessInfo;

    move-result-object v0

    .line 140
    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    const-string/jumbo v1, ":"

    .line 141
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 140
    :goto_0
    return v0

    .line 141
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b()V
    .locals 2

    .prologue
    .line 145
    const-string/jumbo v0, "com.twitter.android"

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Default authority is incorrect"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :cond_0
    return-void
.end method


# virtual methods
.method protected synthetic a(J)Lalc;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/common/app/TwitterApplication;->b(J)Lalg;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcnz;)Lams;
    .locals 2

    .prologue
    .line 119
    invoke-static {}, Lamt;->q()Lamt$a;

    move-result-object v0

    .line 120
    invoke-static {}, Lalg;->ag()Lalg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lamt$a;->a(Lalg;)Lamt$a;

    move-result-object v0

    new-instance v1, Lamv;

    invoke-direct {v1, p1}, Lamv;-><init>(Lcnz;)V

    .line 121
    invoke-virtual {v0, v1}, Lamt$a;->a(Lamv;)Lamt$a;

    move-result-object v0

    .line 122
    invoke-virtual {v0}, Lamt$a;->a()Lamu;

    move-result-object v0

    .line 119
    return-object v0
.end method

.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/twitter/app/common/app/InjectedApplication;->attachBaseContext(Landroid/content/Context;)V

    .line 59
    invoke-static {p0}, Landroid/support/multidex/MultiDex;->install(Landroid/content/Context;)V

    .line 60
    return-void
.end method

.method protected b(J)Lalg;
    .locals 3

    .prologue
    .line 109
    invoke-static {}, Lale;->q()Lale$a;

    move-result-object v0

    new-instance v1, Lali;

    invoke-direct {v1, p0}, Lali;-><init>(Landroid/app/Application;)V

    .line 110
    invoke-virtual {v0, v1}, Lale$a;->a(Lali;)Lale$a;

    move-result-object v0

    new-instance v1, Lamp;

    invoke-direct {v1, p1, p2}, Lamp;-><init>(J)V

    .line 111
    invoke-virtual {v0, v1}, Lale$a;->a(Lamp;)Lale$a;

    move-result-object v0

    new-instance v1, Lamr;

    iget-boolean v2, p0, Lcom/twitter/app/common/app/TwitterApplication;->d:Z

    invoke-direct {v1, v2}, Lamr;-><init>(Z)V

    .line 112
    invoke-virtual {v0, v1}, Lale$a;->a(Lamg;)Lale$a;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Lale$a;->a()Lalg;

    move-result-object v0

    .line 109
    return-object v0
.end method

.method public onCreate()V
    .locals 11

    .prologue
    .line 64
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 65
    invoke-virtual {p0}, Lcom/twitter/app/common/app/TwitterApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    .line 66
    invoke-static {v8}, Lcom/twitter/app/common/app/TwitterApplication;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/common/app/TwitterApplication;->d:Z

    .line 68
    invoke-static {}, Lcom/twitter/app/common/app/TwitterApplication;->b()V

    .line 70
    invoke-static {v8}, Lcom/twitter/library/scribe/ScribeService;->c(Landroid/content/Context;)V

    .line 72
    invoke-static {v8}, Lcom/twitter/library/util/d;->a(Landroid/content/Context;)V

    .line 74
    invoke-super {p0}, Lcom/twitter/app/common/app/InjectedApplication;->onCreate()V

    .line 76
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v9

    .line 77
    iget-boolean v0, p0, Lcom/twitter/app/common/app/TwitterApplication;->d:Z

    if-eqz v0, :cond_0

    invoke-virtual {v9}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 79
    invoke-virtual {v9}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    .line 80
    invoke-static {}, Lcom/twitter/app/common/util/b;->a()Lcom/twitter/app/common/util/b;

    move-result-object v10

    new-instance v0, Lcom/twitter/app/common/app/TwitterApplication$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/app/common/app/TwitterApplication$1;-><init>(Lcom/twitter/app/common/app/TwitterApplication;JJJ)V

    invoke-virtual {v10, v0}, Lcom/twitter/app/common/util/b;->a(Lcom/twitter/app/common/util/b$a;)V

    .line 102
    invoke-virtual {v9}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v8, v0, v1}, Lcom/twitter/app/common/app/TwitterApplication;->a(Landroid/content/Context;J)V

    .line 104
    :cond_0
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 127
    invoke-super {p0}, Lcom/twitter/app/common/app/InjectedApplication;->onLowMemory()V

    .line 128
    invoke-static {p0}, Lcom/twitter/library/media/manager/g;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/media/manager/g;->onLowMemory()V

    .line 129
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1

    .prologue
    .line 133
    invoke-super {p0, p1}, Lcom/twitter/app/common/app/InjectedApplication;->onTrimMemory(I)V

    .line 134
    invoke-static {}, Lbza;->a()Lbza;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbza;->a(I)V

    .line 135
    return-void
.end method
