.class public abstract Lcom/twitter/app/common/app/InjectedApplication;
.super Lcom/twitter/app/common/base/BaseApplication;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/twitter/app/common/base/BaseApplication;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a(J)Lalc;
.end method

.method protected abstract a(Lcnz;)Lams;
.end method

.method protected a()V
    .locals 1

    .prologue
    .line 58
    invoke-static {}, Lalc;->d()Lalc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lalc;->a(Lcom/twitter/app/common/app/InjectedApplication;)V

    .line 59
    return-void
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 19
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    .line 21
    new-instance v2, Lcom/twitter/app/common/app/InjectedApplication$1;

    invoke-direct {v2, p0, v0, v1}, Lcom/twitter/app/common/app/InjectedApplication$1;-><init>(Lcom/twitter/app/common/app/InjectedApplication;J)V

    invoke-static {v2}, Lalc;->a(Lcom/twitter/util/object/j;)V

    .line 29
    new-instance v0, Lcom/twitter/app/common/app/InjectedApplication$2;

    invoke-direct {v0, p0}, Lcom/twitter/app/common/app/InjectedApplication$2;-><init>(Lcom/twitter/app/common/app/InjectedApplication;)V

    invoke-static {v0}, Lams;->a(Lcom/twitter/util/object/d;)V

    .line 37
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lcof;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcof;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    :cond_0
    invoke-static {}, Lcwl;->c()V

    .line 44
    :cond_1
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseApplication;->onCreate()V

    .line 47
    invoke-static {}, Lalc;->d()Lalc;

    move-result-object v0

    invoke-virtual {v0}, Lalc;->i()Lcom/twitter/app/common/util/b;

    .line 48
    return-void
.end method
