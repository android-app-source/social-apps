.class Lcom/twitter/app/common/app/TwitterApplication$1;
.super Lcom/twitter/app/common/util/b$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/common/app/TwitterApplication;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:J

.field final synthetic c:J

.field final synthetic d:Lcom/twitter/app/common/app/TwitterApplication;


# direct methods
.method constructor <init>(Lcom/twitter/app/common/app/TwitterApplication;JJJ)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/twitter/app/common/app/TwitterApplication$1;->d:Lcom/twitter/app/common/app/TwitterApplication;

    iput-wide p2, p0, Lcom/twitter/app/common/app/TwitterApplication$1;->a:J

    iput-wide p4, p0, Lcom/twitter/app/common/app/TwitterApplication$1;->b:J

    iput-wide p6, p0, Lcom/twitter/app/common/app/TwitterApplication$1;->c:J

    invoke-direct {p0}, Lcom/twitter/app/common/util/b$a;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 84
    invoke-static {}, Lcom/twitter/app/common/util/b;->a()Lcom/twitter/app/common/util/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/app/common/util/b;->b(Lcom/twitter/app/common/util/b$a;)V

    .line 85
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 86
    iget-wide v2, p0, Lcom/twitter/app/common/app/TwitterApplication$1;->a:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 90
    new-instance v0, Lcom/twitter/metrics/g;

    const-string/jumbo v1, "app:init"

    sget-object v2, Lcom/twitter/metrics/g;->n:Lcom/twitter/metrics/g$b;

    iget-wide v4, p0, Lcom/twitter/app/common/app/TwitterApplication$1;->a:J

    iget-wide v6, p0, Lcom/twitter/app/common/app/TwitterApplication$1;->b:J

    sub-long/2addr v4, v6

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/twitter/metrics/g;-><init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;J)V

    invoke-static {v0}, Lcom/twitter/library/metrics/c;->a(Lcom/twitter/metrics/g;)V

    .line 92
    invoke-static {}, Lcom/twitter/library/metrics/c;->a()Lcom/twitter/metrics/g;

    move-result-object v0

    .line 93
    if-eqz v0, :cond_0

    .line 94
    iget-wide v2, p0, Lcom/twitter/app/common/app/TwitterApplication$1;->c:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/metrics/g;->b(J)V

    .line 95
    const-string/jumbo v1, "AppMetrics"

    invoke-virtual {v0, v1}, Lcom/twitter/metrics/g;->e(Ljava/lang/String;)V

    .line 96
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/metrics/j;->a(Lcom/twitter/metrics/g;)V

    .line 99
    :cond_0
    return-void
.end method
