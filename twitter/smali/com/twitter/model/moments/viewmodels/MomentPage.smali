.class public abstract Lcom/twitter/model/moments/viewmodels/MomentPage;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/moments/viewmodels/MomentPage$a;,
        Lcom/twitter/model/moments/viewmodels/MomentPage$Type;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/model/moments/Moment;

.field private final b:Lcom/twitter/model/moments/r;

.field private final c:Lcom/twitter/model/moments/MomentPageDisplayMode;

.field private final d:Lcom/twitter/model/moments/n;

.field private final e:Lcom/twitter/model/moments/o;

.field private final f:Lcom/twitter/model/moments/s;


# direct methods
.method protected constructor <init>(Lcom/twitter/model/moments/viewmodels/MomentPage$a;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->c:Lcom/twitter/model/moments/r;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->d:Lcom/twitter/model/moments/MomentPageDisplayMode;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->b:Lcom/twitter/model/moments/Moment;

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage;->a:Lcom/twitter/model/moments/Moment;

    .line 44
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->c:Lcom/twitter/model/moments/r;

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage;->b:Lcom/twitter/model/moments/r;

    .line 45
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->d:Lcom/twitter/model/moments/MomentPageDisplayMode;

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage;->c:Lcom/twitter/model/moments/MomentPageDisplayMode;

    .line 46
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->e:Lcom/twitter/model/moments/n;

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage;->d:Lcom/twitter/model/moments/n;

    .line 47
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->f:Lcom/twitter/model/moments/o;

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage;->e:Lcom/twitter/model/moments/o;

    .line 48
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->g:Lcom/twitter/model/moments/s;

    iput-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage;->f:Lcom/twitter/model/moments/s;

    .line 49
    return-void
.end method

.method public static a(Lcom/twitter/model/moments/r;)Lcpv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/r;",
            ")",
            "Lcpv",
            "<",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 199
    new-instance v0, Lcom/twitter/model/moments/viewmodels/MomentPage$1;

    invoke-direct {v0, p0}, Lcom/twitter/model/moments/viewmodels/MomentPage$1;-><init>(Lcom/twitter/model/moments/r;)V

    return-object v0
.end method


# virtual methods
.method public abstract e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;
.end method

.method public f()Lcom/twitter/model/moments/Moment;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage;->a:Lcom/twitter/model/moments/Moment;

    return-object v0
.end method

.method public g()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->f()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->f()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Lcom/twitter/model/moments/MomentPageDisplayMode;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage;->c:Lcom/twitter/model/moments/MomentPageDisplayMode;

    return-object v0
.end method

.method public i()Lcom/twitter/model/moments/r;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage;->b:Lcom/twitter/model/moments/r;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage;->b:Lcom/twitter/model/moments/r;

    invoke-virtual {v0}, Lcom/twitter/model/moments/r;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage;->c:Lcom/twitter/model/moments/MomentPageDisplayMode;

    sget-object v1, Lcom/twitter/model/moments/MomentPageDisplayMode;->a:Lcom/twitter/model/moments/MomentPageDisplayMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Z
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage;->c:Lcom/twitter/model/moments/MomentPageDisplayMode;

    sget-object v1, Lcom/twitter/model/moments/MomentPageDisplayMode;->c:Lcom/twitter/model/moments/MomentPageDisplayMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->f()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->f()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/moments/Moment;->c:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/io/Serializable;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->f()Lcom/twitter/model/moments/Moment;

    move-result-object v3

    iget-object v3, v3, Lcom/twitter/model/moments/Moment;->l:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->f()Lcom/twitter/model/moments/Moment;

    move-result-object v3

    iget-boolean v3, v3, Lcom/twitter/model/moments/Moment;->f:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 106
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/o;->f()Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public n()Lcom/twitter/model/moments/s;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage;->f:Lcom/twitter/model/moments/s;

    return-object v0
.end method

.method public o()Z
    .locals 2

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->b:Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    if-eq v0, v1, :cond_0

    .line 117
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->f:Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->c:Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    if-eq v0, v1, :cond_0

    .line 118
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->g:Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 116
    :goto_0
    return v0

    .line 118
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract p()Lcom/twitter/model/moments/viewmodels/MomentPage$a;
.end method

.method public q()Lcom/twitter/model/moments/n;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage;->d:Lcom/twitter/model/moments/n;

    return-object v0
.end method

.method public r()Lcom/twitter/model/moments/o;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/twitter/model/moments/viewmodels/MomentPage;->e:Lcom/twitter/model/moments/o;

    return-object v0
.end method
