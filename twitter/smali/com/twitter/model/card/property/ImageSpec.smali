.class public Lcom/twitter/model/card/property/ImageSpec;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/twitter/model/core/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/model/card/property/ImageSpec$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/model/card/property/ImageSpec;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/model/card/property/ImageSpec;",
            ">;"
        }
    .end annotation
.end field

.field private static transient e:F


# instance fields
.field public b:Ljava/lang/String;

.field public c:Lcom/twitter/model/card/property/Vector2F;

.field public d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/twitter/model/card/property/ImageSpec$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/model/card/property/ImageSpec$a;-><init>(Lcom/twitter/model/card/property/ImageSpec$1;)V

    sput-object v0, Lcom/twitter/model/card/property/ImageSpec;->a:Lcom/twitter/util/serialization/l;

    .line 24
    new-instance v0, Lcom/twitter/model/card/property/ImageSpec$1;

    invoke-direct {v0}, Lcom/twitter/model/card/property/ImageSpec$1;-><init>()V

    sput-object v0, Lcom/twitter/model/card/property/ImageSpec;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 39
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/twitter/model/card/property/ImageSpec;->e:F

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    return-void
.end method

.method public constructor <init>(FF)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Lcom/twitter/model/card/property/Vector2F;

    invoke-direct {v0, p1, p2}, Lcom/twitter/model/card/property/Vector2F;-><init>(FF)V

    iput-object v0, p0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/card/property/ImageSpec;->b:Ljava/lang/String;

    .line 54
    sget-object v0, Lcom/twitter/model/card/property/Vector2F;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/card/property/Vector2F;

    iput-object v0, p0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/model/card/property/ImageSpec;->d:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/twitter/model/card/property/Vector2F;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/twitter/model/card/property/ImageSpec;->b:Ljava/lang/String;

    .line 60
    iput-object p2, p0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    .line 61
    iput-object p3, p0, Lcom/twitter/model/card/property/ImageSpec;->d:Ljava/lang/String;

    .line 62
    return-void
.end method

.method public static a()F
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 70
    sget v0, Lcom/twitter/model/card/property/ImageSpec;->e:F

    return v0
.end method

.method public static a(F)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 79
    sput p0, Lcom/twitter/model/card/property/ImageSpec;->e:F

    .line 80
    const-class v0, Lcom/twitter/model/card/property/ImageSpec;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 81
    return-void
.end method


# virtual methods
.method public bv_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/model/card/property/ImageSpec;->d:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 102
    if-ne p0, p1, :cond_1

    .line 110
    :cond_0
    :goto_0
    return v0

    .line 105
    :cond_1
    instance-of v2, p1, Lcom/twitter/model/card/property/ImageSpec;

    if-nez v2, :cond_2

    move v0, v1

    .line 106
    goto :goto_0

    .line 109
    :cond_2
    check-cast p1, Lcom/twitter/model/card/property/ImageSpec;

    .line 110
    iget-object v2, p0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    iget-object v3, p1, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/twitter/model/card/property/ImageSpec;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/model/card/property/ImageSpec;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/twitter/model/card/property/ImageSpec;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/model/card/property/ImageSpec;->d:Ljava/lang/String;

    .line 111
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/twitter/model/card/property/ImageSpec;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v0

    .line 117
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 118
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/model/card/property/ImageSpec;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 119
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/model/card/property/ImageSpec;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    sget-object v1, Lcom/twitter/model/card/property/Vector2F;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 97
    iget-object v0, p0, Lcom/twitter/model/card/property/ImageSpec;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 98
    return-void
.end method
