.class public Lcom/twitter/model/timeline/am$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/model/timeline/am;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# direct methods
.method public static a(I)I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 88
    packed-switch p0, :pswitch_data_0

    .line 146
    :pswitch_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Timeline type %d has no matching TweetGroupType value"

    new-array v1, v1, [Ljava/lang/Object;

    .line 147
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v0

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_1
    move v0, v1

    .line 143
    :goto_0
    :pswitch_2
    return v0

    .line 98
    :pswitch_3
    const/16 v0, 0x1c

    goto :goto_0

    .line 101
    :pswitch_4
    const/16 v0, 0x1b

    goto :goto_0

    .line 104
    :pswitch_5
    const/16 v0, 0x1e

    goto :goto_0

    .line 107
    :pswitch_6
    const/4 v0, 0x2

    goto :goto_0

    .line 110
    :pswitch_7
    const/16 v0, 0x11

    goto :goto_0

    .line 113
    :pswitch_8
    const/4 v0, 0x5

    goto :goto_0

    .line 116
    :pswitch_9
    const/16 v0, 0x17

    goto :goto_0

    .line 119
    :pswitch_a
    const/16 v0, 0x18

    goto :goto_0

    .line 122
    :pswitch_b
    const/4 v0, 0x3

    goto :goto_0

    .line 125
    :pswitch_c
    const/16 v0, 0x25

    goto :goto_0

    .line 128
    :pswitch_d
    const/16 v0, 0x9

    goto :goto_0

    .line 131
    :pswitch_e
    const/16 v0, 0x26

    goto :goto_0

    .line 134
    :pswitch_f
    const/16 v0, 0x13

    goto :goto_0

    .line 137
    :pswitch_10
    const/16 v0, 0x27

    goto :goto_0

    .line 140
    :pswitch_11
    const/16 v0, 0x28

    goto :goto_0

    .line 143
    :pswitch_12
    const/16 v0, 0x10

    goto :goto_0

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_7
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_6
        :pswitch_2
        :pswitch_1
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method

.method public static a()Ljava/lang/Iterable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 191
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v1

    .line 192
    sget-object v0, Lcom/twitter/model/timeline/am;->a:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 193
    invoke-static {v0}, Lcom/twitter/model/timeline/am$a;->b(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    goto :goto_0

    .line 195
    :cond_0
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/o;->d(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 196
    invoke-virtual {v1}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    return-object v0
.end method

.method public static b(I)I
    .locals 1

    .prologue
    .line 157
    packed-switch p0, :pswitch_data_0

    .line 182
    :pswitch_0
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 159
    :pswitch_1
    const/4 v0, 0x4

    goto :goto_0

    .line 163
    :pswitch_2
    const/16 v0, 0xf

    goto :goto_0

    .line 171
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 179
    :pswitch_4
    const/16 v0, 0x10

    goto :goto_0

    .line 157
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public static c(I)Z
    .locals 1

    .prologue
    .line 200
    const/16 v0, 0x9

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-eq p0, v0, :cond_0

    const/16 v0, 0xc

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
