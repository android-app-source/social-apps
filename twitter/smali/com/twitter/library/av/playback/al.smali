.class public Lcom/twitter/library/av/playback/al;
.super Lbjb;
.source "Twttr"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/playback/al$a;
    }
.end annotation


# instance fields
.field protected a:I
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private b:Z

.field private final c:Lcom/twitter/library/av/playback/al$a;

.field private final d:Lcom/twitter/library/av/playback/an;

.field private final e:Lcrr;

.field private final f:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/playback/al$a;)V
    .locals 6

    .prologue
    .line 39
    new-instance v3, Lcom/twitter/library/av/playback/an;

    new-instance v0, Lcom/twitter/library/av/playback/aj;

    invoke-direct {v0}, Lcom/twitter/library/av/playback/aj;-><init>()V

    const/4 v1, 0x1

    .line 42
    invoke-static {v1}, Lcom/twitter/library/av/playback/ak;->a(Z)Z

    move-result v1

    invoke-direct {v3, v0, v1}, Lcom/twitter/library/av/playback/an;-><init>(Lcom/twitter/library/av/playback/aj;Z)V

    .line 43
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 44
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v5

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    .line 39
    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/av/playback/al;-><init>(Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/playback/al$a;Lcom/twitter/library/av/playback/an;Landroid/content/SharedPreferences;Lcrr;)V

    .line 45
    return-void
.end method

.method constructor <init>(Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/playback/al$a;Lcom/twitter/library/av/playback/an;Landroid/content/SharedPreferences;Lcrr;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lbjb;-><init>(Lcom/twitter/model/av/AVMedia;)V

    .line 27
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/library/av/playback/al;->a:I

    .line 52
    iput-object p2, p0, Lcom/twitter/library/av/playback/al;->c:Lcom/twitter/library/av/playback/al$a;

    .line 53
    iput-object p3, p0, Lcom/twitter/library/av/playback/al;->d:Lcom/twitter/library/av/playback/an;

    .line 54
    iput-object p5, p0, Lcom/twitter/library/av/playback/al;->e:Lcrr;

    .line 55
    iput-object p4, p0, Lcom/twitter/library/av/playback/al;->f:Landroid/content/SharedPreferences;

    .line 56
    invoke-interface {p4, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 57
    invoke-direct {p0, p4}, Lcom/twitter/library/av/playback/al;->a(Landroid/content/SharedPreferences;)V

    .line 58
    return-void
.end method

.method private a(Landroid/content/SharedPreferences;)V
    .locals 1

    .prologue
    .line 89
    invoke-static {p1}, Lcom/twitter/library/av/playback/ak;->a(Landroid/content/SharedPreferences;)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/av/playback/al;->a:I

    .line 90
    return-void
.end method

.method private a(ZZ)V
    .locals 4

    .prologue
    .line 105
    iget-object v0, p0, Lcom/twitter/library/av/playback/al;->c:Lcom/twitter/library/av/playback/al$a;

    iget-object v1, p0, Lcom/twitter/library/av/playback/al;->d:Lcom/twitter/library/av/playback/an;

    .line 106
    invoke-virtual {v1, p1, p2}, Lcom/twitter/library/av/playback/an;->a(ZZ)J

    move-result-wide v2

    .line 105
    invoke-interface {v0, v2, v3}, Lcom/twitter/library/av/playback/al$a;->a(J)V

    .line 107
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/library/av/playback/al;->f:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 62
    return-void
.end method

.method public a(Lbiw;)Z
    .locals 1

    .prologue
    .line 69
    invoke-super {p0, p1}, Lbjb;->a(Lbiw;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lbjz;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected e()Z
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 97
    iget-object v2, p0, Lcom/twitter/library/av/playback/al;->e:Lcrr;

    invoke-virtual {v2}, Lcrr;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 98
    iget v2, p0, Lcom/twitter/library/av/playback/al;->a:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_1

    .line 100
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 98
    goto :goto_0

    .line 100
    :cond_2
    iget v2, p0, Lcom/twitter/library/av/playback/al;->a:I

    if-eq v2, v0, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public onPlaybackConfigEvent(Lbjz;)V
    .locals 2
    .annotation runtime Lbiz;
        a = Lbjz;
    .end annotation

    .prologue
    .line 75
    iget-object v0, p1, Lbjz;->b:Lbyf;

    invoke-interface {v0}, Lbyf;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/av/playback/al;->b:Z

    .line 76
    iget-object v0, p0, Lcom/twitter/library/av/playback/al;->c:Lcom/twitter/library/av/playback/al$a;

    iget-boolean v1, p0, Lcom/twitter/library/av/playback/al;->b:Z

    invoke-interface {v0, v1}, Lcom/twitter/library/av/playback/al$a;->a(Z)V

    .line 77
    iget-boolean v0, p0, Lcom/twitter/library/av/playback/al;->b:Z

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/al;->e()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/library/av/playback/al;->a(ZZ)V

    .line 78
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 82
    const-string/jumbo v0, "video_quality"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    invoke-direct {p0, p1}, Lcom/twitter/library/av/playback/al;->a(Landroid/content/SharedPreferences;)V

    .line 84
    iget-boolean v0, p0, Lcom/twitter/library/av/playback/al;->b:Z

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/al;->e()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/library/av/playback/al;->a(ZZ)V

    .line 86
    :cond_0
    return-void
.end method
