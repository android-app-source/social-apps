.class public Lcom/twitter/library/client/v;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/client/v$h;,
        Lcom/twitter/library/client/v$b;,
        Lcom/twitter/library/client/v$d;,
        Lcom/twitter/library/client/v$f;,
        Lcom/twitter/library/client/v$e;,
        Lcom/twitter/library/client/v$g;,
        Lcom/twitter/library/client/v$a;,
        Lcom/twitter/library/client/v$c;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/u;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/Object;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/twitter/library/client/Session;

.field private final f:Landroid/os/Handler;

.field private g:Lcom/twitter/library/client/p;

.field private final h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/client/v$f;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private j:Z


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    .line 82
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/client/v;->c:Ljava/lang/Object;

    .line 83
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/client/v;->d:Ljava/util/Map;

    .line 87
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/client/v;->h:Ljava/util/HashMap;

    .line 90
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/client/v;->i:Ljava/util/LinkedList;

    .line 95
    invoke-static {}, Lcom/twitter/util/f;->d()V

    .line 96
    iput-object v1, p0, Lcom/twitter/library/client/v;->a:Landroid/content/Context;

    .line 97
    iput-object v1, p0, Lcom/twitter/library/client/v;->f:Landroid/os/Handler;

    .line 98
    new-instance v0, Lcom/twitter/library/client/Session;

    invoke-direct {v0}, Lcom/twitter/library/client/Session;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/client/v;->e:Lcom/twitter/library/client/Session;

    .line 99
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    .line 82
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/client/v;->c:Ljava/lang/Object;

    .line 83
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/client/v;->d:Ljava/util/Map;

    .line 87
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/client/v;->h:Ljava/util/HashMap;

    .line 90
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/client/v;->i:Ljava/util/LinkedList;

    .line 102
    iput-object p1, p0, Lcom/twitter/library/client/v;->a:Landroid/content/Context;

    .line 103
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/v;->g:Lcom/twitter/library/client/p;

    .line 104
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/library/client/v;->f:Landroid/os/Handler;

    .line 105
    invoke-direct {p0}, Lcom/twitter/library/client/v;->i()V

    .line 106
    return-void
.end method

.method private static a(Lcom/twitter/util/a;)J
    .locals 4

    .prologue
    .line 862
    const-string/jumbo v0, "session_active_state_transition_timestamp"

    const-wide/16 v2, -0x1

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/util/a;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(Lakm;)Lcom/twitter/library/client/Session;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 415
    invoke-direct {p0}, Lcom/twitter/library/client/v;->h()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 416
    invoke-static {p1}, Lcom/twitter/library/util/b;->c(Lakm;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 419
    invoke-virtual {p1}, Lakm;->f()Ljava/lang/String;

    move-result-object v3

    .line 420
    if-eqz v3, :cond_0

    move v0, v1

    .line 421
    :goto_0
    if-eqz v0, :cond_1

    .line 423
    invoke-virtual {p1}, Lakm;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/library/client/Session;->a(Ljava/lang/String;)V

    .line 424
    invoke-virtual {v2, v1}, Lcom/twitter/library/client/Session;->a(Z)V

    .line 427
    invoke-virtual {p1}, Lakm;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v2, v0, v3, v1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/v$d;)Ljava/lang/String;

    .line 436
    :goto_1
    return-object v2

    .line 420
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 429
    :cond_1
    invoke-direct {p0, v2, p1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/Session;Lakm;)V

    goto :goto_1

    .line 432
    :cond_2
    new-instance v0, Lcpb;

    invoke-direct {v0}, Lcpb;-><init>()V

    const-string/jumbo v1, "accountName"

    .line 433
    invoke-virtual {p1}, Lakm;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "Count not read userdata from account."

    invoke-direct {v1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 434
    invoke-virtual {v0, v1}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v0

    .line 432
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    goto :goto_1
.end method

.method private a(Ljava/lang/String;J)Lcom/twitter/library/client/Session;
    .locals 8

    .prologue
    .line 358
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    .line 359
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_3

    const/4 v0, 0x1

    move v1, v0

    .line 360
    :goto_0
    iget-object v3, p0, Lcom/twitter/library/client/v;->c:Ljava/lang/Object;

    monitor-enter v3

    .line 361
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/v;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    .line 362
    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    cmp-long v5, p2, v6

    if-eqz v5, :cond_2

    :cond_1
    if-eqz v2, :cond_0

    .line 363
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 364
    :cond_2
    monitor-exit v3

    .line 368
    :goto_1
    return-object v0

    .line 359
    :cond_3
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 367
    :cond_4
    monitor-exit v3

    .line 368
    const/4 v0, 0x0

    goto :goto_1

    .line 367
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Ljava/lang/String;Lakm;J)Lcom/twitter/library/client/Session;
    .locals 5

    .prologue
    .line 313
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    .line 314
    const-wide/16 v2, 0x0

    cmp-long v0, p3, v2

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    .line 315
    :goto_0
    iget-object v2, p0, Lcom/twitter/library/client/v;->c:Ljava/lang/Object;

    monitor-enter v2

    .line 316
    if-nez v1, :cond_0

    if-eqz v0, :cond_3

    .line 318
    :cond_0
    :try_start_0
    invoke-direct {p0, p1, p3, p4}, Lcom/twitter/library/client/v;->a(Ljava/lang/String;J)Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 319
    if-eqz v0, :cond_2

    .line 320
    monitor-exit v2

    .line 336
    :goto_1
    return-object v0

    .line 314
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 324
    :cond_2
    if-nez p2, :cond_3

    .line 325
    if-eqz v1, :cond_4

    .line 326
    invoke-static {p1}, Lcom/twitter/library/util/b;->b(Ljava/lang/String;)Lakm;

    move-result-object p2

    .line 333
    :cond_3
    :goto_2
    if-nez p2, :cond_5

    .line 334
    invoke-direct {p0}, Lcom/twitter/library/client/v;->h()Lcom/twitter/library/client/Session;

    move-result-object v0

    monitor-exit v2

    goto :goto_1

    .line 338
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 328
    :cond_4
    :try_start_1
    invoke-static {p3, p4}, Lcom/twitter/library/util/b;->a(J)Lakm;

    move-result-object p2

    goto :goto_2

    .line 336
    :cond_5
    invoke-direct {p0, p2}, Lcom/twitter/library/client/v;->a(Lakm;)Lcom/twitter/library/client/Session;

    move-result-object v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public static declared-synchronized a()Lcom/twitter/library/client/v;
    .locals 2

    .prologue
    .line 110
    const-class v1, Lcom/twitter/library/client/v;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lbms;->ah()Lbms;

    move-result-object v0

    invoke-virtual {v0}, Lbms;->X()Lcom/twitter/library/client/v;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/LoginResponse;Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 489
    iget-object v3, p2, Lcom/twitter/model/account/LoginResponse;->a:Lcom/twitter/model/account/OAuthToken;

    .line 491
    iget-object v2, p3, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Lcom/twitter/model/account/OAuthToken;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V

    .line 493
    invoke-static {p3}, Lcom/twitter/library/util/b;->b(Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;

    move-result-object v1

    .line 494
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/model/account/OAuthToken;

    move-result-object v0

    .line 497
    iget-object v2, p2, Lcom/twitter/model/account/LoginResponse;->c:Ljava/lang/String;

    invoke-static {v2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 498
    iget-object v2, p0, Lcom/twitter/library/client/v;->a:Landroid/content/Context;

    iget-object v3, p2, Lcom/twitter/model/account/LoginResponse;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lbar;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 501
    :cond_0
    invoke-static {v1, v0}, Lcom/twitter/library/util/b;->a(Ljava/lang/String;Lcom/twitter/model/account/OAuthToken;)Lakm;

    move-result-object v0

    .line 502
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->i()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 503
    invoke-virtual {p1, v7}, Lcom/twitter/library/client/Session;->a(Z)V

    .line 508
    :cond_1
    :goto_0
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v0

    invoke-virtual {v0}, Lakn;->b()I

    move-result v0

    if-ne v0, v6, :cond_4

    move v0, v6

    .line 509
    :goto_1
    invoke-direct {p0, p1, v0}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/Session;Z)V

    .line 510
    if-eqz v0, :cond_2

    .line 511
    invoke-direct {p0, p1}, Lcom/twitter/library/client/v;->d(Lcom/twitter/library/client/Session;)V

    .line 513
    :cond_2
    return-object v1

    .line 504
    :cond_3
    if-eqz v0, :cond_1

    .line 506
    sget-object v2, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    invoke-static {v0, v2, v6}, Lcom/twitter/library/util/b;->a(Lakm;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_4
    move v0, v7

    .line 508
    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/library/client/v;Lcom/twitter/library/client/Session;Lcom/twitter/model/account/LoginResponse;Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/LoginResponse;Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/library/client/v;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/library/client/v;->h:Ljava/util/HashMap;

    return-object v0
.end method

.method private a(Lcom/twitter/library/client/Session;J)V
    .locals 4

    .prologue
    .line 727
    iget-object v2, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    monitor-enter v2

    .line 728
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 729
    iget-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/u;

    invoke-interface {v0, p1, p2, p3}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/client/Session;J)V

    .line 728
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 731
    :cond_0
    monitor-exit v2

    .line 732
    return-void

    .line 731
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Lcom/twitter/library/client/Session;JZ)V
    .locals 8

    .prologue
    .line 817
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 818
    const-wide/16 v0, -0x1

    .line 819
    invoke-direct {p0, p1}, Lcom/twitter/library/client/v;->h(Lcom/twitter/library/client/Session;)Lcom/twitter/util/a;

    move-result-object v2

    .line 820
    if-eqz v2, :cond_1

    .line 821
    invoke-static {v2}, Lcom/twitter/library/client/v;->a(Lcom/twitter/util/a;)J

    move-result-wide v4

    .line 822
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-ltz v3, :cond_0

    .line 823
    sub-long v0, p2, v4

    .line 825
    :cond_0
    invoke-static {v2, p2, p3}, Lcom/twitter/library/client/v;->a(Lcom/twitter/util/a;J)V

    .line 828
    :cond_1
    if-eqz p4, :cond_2

    .line 829
    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/Session;J)V

    .line 833
    :goto_0
    return-void

    .line 831
    :cond_2
    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/Session;J)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/client/Session;Lakm;)V
    .locals 6

    .prologue
    .line 440
    invoke-static {p2}, Lcom/twitter/library/util/b;->b(Lakm;)Lcom/twitter/model/account/OAuthToken;

    move-result-object v3

    .line 442
    if-eqz v3, :cond_0

    .line 443
    invoke-static {p2}, Lcom/twitter/library/util/b;->c(Lakm;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v4

    .line 444
    if-eqz v4, :cond_0

    .line 446
    invoke-static {p2}, Lcom/twitter/library/util/b;->a(Lakm;)Lcom/twitter/model/account/UserSettings;

    move-result-object v5

    .line 447
    invoke-virtual {p2}, Lakm;->d()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Lcom/twitter/model/account/OAuthToken;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V

    .line 450
    :cond_0
    return-void
.end method

.method private a(Lcom/twitter/library/client/Session;Lcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    .line 647
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 648
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/Session;Lcom/twitter/library/client/Session;)V

    .line 657
    :goto_0
    return-void

    .line 650
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/client/v;->f:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/library/client/v$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/twitter/library/client/v$2;-><init>(Lcom/twitter/library/client/v;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/Session;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 472
    invoke-static {p2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 473
    invoke-static {p2}, Lcom/twitter/library/util/b;->b(Ljava/lang/String;)Lakm;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 475
    invoke-virtual {p0, p2}, Lcom/twitter/library/client/v;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 476
    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 477
    sget-object v1, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p1, v1}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    .line 479
    :cond_0
    invoke-virtual {p0, v0}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    .line 483
    :goto_0
    return-void

    .line 481
    :cond_1
    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/client/Session;Ljava/lang/String;Lcom/twitter/model/account/OAuthToken;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V
    .locals 1

    .prologue
    .line 520
    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->c:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    .line 523
    invoke-virtual {p1, p4}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 524
    invoke-virtual {p1, p2}, Lcom/twitter/library/client/Session;->a(Ljava/lang/String;)V

    .line 525
    invoke-virtual {p1, p3}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/model/account/OAuthToken;)V

    .line 527
    if-eqz p5, :cond_0

    .line 528
    invoke-virtual {p1, p5}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/model/account/UserSettings;)V

    .line 531
    :cond_0
    new-instance v0, Lcom/twitter/library/client/v$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/client/v$1;-><init>(Lcom/twitter/library/client/v;Lcom/twitter/library/client/Session;)V

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$a;)V

    .line 542
    return-void
.end method

.method private a(Lcom/twitter/library/client/Session;Z)V
    .locals 3

    .prologue
    .line 691
    iget-object v2, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    monitor-enter v2

    .line 692
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 693
    iget-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/u;

    invoke-interface {v0, p1, p2}, Lcom/twitter/library/client/u;->b(Lcom/twitter/library/client/Session;Z)V

    .line 692
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 695
    :cond_0
    monitor-exit v2

    .line 696
    return-void

    .line 695
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/twitter/library/client/v;Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/twitter/library/client/v;->f(Lcom/twitter/library/client/Session;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/client/v;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/Session;Lcom/twitter/library/client/Session;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/client/v;Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/client/v;Lcom/twitter/library/client/Session;Z)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/Session;Z)V

    return-void
.end method

.method private static a(Lcom/twitter/util/a;J)V
    .locals 3

    .prologue
    .line 875
    invoke-virtual {p0}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "session_active_state_transition_timestamp"

    .line 876
    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;J)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 877
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 878
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/client/v;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/twitter/library/client/v;->f(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/library/client/v;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/library/client/v;->d:Ljava/util/Map;

    return-object v0
.end method

.method private b(Lcom/twitter/library/client/Session;J)V
    .locals 4

    .prologue
    .line 736
    iget-object v2, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    monitor-enter v2

    .line 737
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 738
    iget-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/u;

    invoke-interface {v0, p1, p2, p3}, Lcom/twitter/library/client/u;->b(Lcom/twitter/library/client/Session;J)V

    .line 737
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 740
    :cond_0
    monitor-exit v2

    .line 741
    return-void

    .line 740
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private b(Lcom/twitter/library/client/Session;Lcom/twitter/library/client/Session;)V
    .locals 3

    .prologue
    .line 661
    invoke-direct {p0, p2}, Lcom/twitter/library/client/v;->d(Lcom/twitter/library/client/Session;)V

    .line 662
    iget-boolean v0, p0, Lcom/twitter/library/client/v;->j:Z

    if-eqz v0, :cond_1

    invoke-static {p1, p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 663
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    .line 664
    if-eqz p1, :cond_0

    .line 665
    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/Session;JZ)V

    .line 667
    :cond_0
    const/4 v2, 0x1

    invoke-direct {p0, p2, v0, v1, v2}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/Session;JZ)V

    .line 669
    :cond_1
    return-void
.end method

.method private b(Lcom/twitter/library/client/Session;Z)V
    .locals 3

    .prologue
    .line 700
    iget-object v2, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    monitor-enter v2

    .line 701
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 702
    iget-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/u;

    invoke-interface {v0, p1, p2}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/client/Session;Z)V

    .line 701
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 704
    :cond_0
    monitor-exit v2

    .line 705
    return-void

    .line 704
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic b(Lcom/twitter/library/client/v;Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/twitter/library/client/v;->g(Lcom/twitter/library/client/Session;)V

    return-void
.end method

.method static synthetic c(Lcom/twitter/library/client/v;Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/twitter/library/client/v;->e(Lcom/twitter/library/client/Session;)V

    return-void
.end method

.method private d(Lcom/twitter/library/client/Session;)V
    .locals 3

    .prologue
    .line 673
    iget-object v2, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    monitor-enter v2

    .line 674
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 675
    iget-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/u;

    invoke-interface {v0, p1}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/client/Session;)V

    .line 674
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 677
    :cond_0
    monitor-exit v2

    .line 678
    return-void

    .line 677
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private e(Lcom/twitter/library/client/Session;)V
    .locals 3

    .prologue
    .line 682
    iget-object v2, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    monitor-enter v2

    .line 683
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 684
    iget-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/u;

    invoke-interface {v0, p1}, Lcom/twitter/library/client/u;->b(Lcom/twitter/library/client/Session;)V

    .line 683
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 686
    :cond_0
    monitor-exit v2

    .line 687
    return-void

    .line 686
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private f(Lcom/twitter/library/client/Session;)V
    .locals 3

    .prologue
    .line 709
    iget-object v2, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    monitor-enter v2

    .line 710
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 711
    iget-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/u;

    invoke-interface {v0, p1}, Lcom/twitter/library/client/u;->d(Lcom/twitter/library/client/Session;)V

    .line 710
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 713
    :cond_0
    monitor-exit v2

    .line 714
    return-void

    .line 713
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private f(Ljava/lang/String;)Z
    .locals 6

    .prologue
    .line 453
    iget-object v1, p0, Lcom/twitter/library/client/v;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 454
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/v;->e:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    iget-object v0, p0, Lcom/twitter/library/client/v;->a:Landroid/content/Context;

    const/4 v2, 0x0

    const-wide/16 v4, 0x0

    invoke-static {v0, v2, v4, v5}, Lcom/twitter/library/provider/x;->a(Landroid/content/Context;Ljava/lang/String;J)V

    .line 457
    invoke-direct {p0}, Lcom/twitter/library/client/v;->i()V

    .line 460
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/client/v;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    .line 461
    if-eqz v0, :cond_1

    .line 463
    iget-object v0, p0, Lcom/twitter/library/client/v;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 465
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 467
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v0

    invoke-virtual {v0}, Lakn;->b()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    .line 465
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 467
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g(Lcom/twitter/library/client/Session;)V
    .locals 3

    .prologue
    .line 718
    iget-object v2, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    monitor-enter v2

    .line 719
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 720
    iget-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/u;

    invoke-interface {v0, p1}, Lcom/twitter/library/client/u;->c(Lcom/twitter/library/client/Session;)V

    .line 719
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 722
    :cond_0
    monitor-exit v2

    .line 723
    return-void

    .line 722
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private h()Lcom/twitter/library/client/Session;
    .locals 8

    .prologue
    .line 397
    iget-object v1, p0, Lcom/twitter/library/client/v;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 400
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/v;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    .line 401
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->b()Lcom/twitter/library/client/Session$LoginStatus;

    move-result-object v3

    sget-object v4, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    if-ne v3, v4, :cond_0

    .line 402
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 403
    monitor-exit v1

    .line 409
    :goto_0
    return-object v0

    .line 406
    :cond_1
    new-instance v0, Lcom/twitter/library/client/Session;

    invoke-direct {v0}, Lcom/twitter/library/client/Session;-><init>()V

    .line 407
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v2

    .line 408
    iget-object v3, p0, Lcom/twitter/library/client/v;->d:Ljava/util/Map;

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    monitor-exit v1

    goto :goto_0

    .line 410
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private h(Lcom/twitter/library/client/Session;)Lcom/twitter/util/a;
    .locals 4

    .prologue
    .line 846
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 847
    new-instance v0, Lcom/twitter/util/a;

    iget-object v1, p0, Lcom/twitter/library/client/v;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 849
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()V
    .locals 6

    .prologue
    .line 902
    iget-object v1, p0, Lcom/twitter/library/client/v;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 903
    :try_start_0
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v0

    invoke-virtual {v0}, Lakn;->c()Ljava/util/List;

    move-result-object v2

    .line 906
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 908
    iget-object v0, p0, Lcom/twitter/library/client/v;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/provider/x;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 909
    invoke-static {v0}, Lcom/twitter/library/util/b;->b(Ljava/lang/String;)Lakm;

    move-result-object v0

    .line 912
    if-nez v0, :cond_0

    .line 913
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    .line 915
    :cond_0
    invoke-virtual {v0}, Lakm;->d()Ljava/lang/String;

    move-result-object v2

    const-wide/16 v4, -0x1

    invoke-direct {p0, v2, v0, v4, v5}, Lcom/twitter/library/client/v;->a(Ljava/lang/String;Lakm;J)Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 919
    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/Session;)V

    .line 920
    monitor-exit v1

    .line 921
    return-void

    .line 917
    :cond_1
    invoke-direct {p0}, Lcom/twitter/library/client/v;->h()Lcom/twitter/library/client/Session;

    move-result-object v0

    goto :goto_0

    .line 920
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(J)Lcom/twitter/library/client/Session;
    .locals 3

    .prologue
    .line 275
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcnz;)Lcom/twitter/library/client/Session;
    .locals 4

    .prologue
    .line 353
    const/4 v0, 0x0

    invoke-virtual {p1}, Lcnz;->b()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Lcom/twitter/library/client/v;->a(Ljava/lang/String;J)Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/service/s;)Lcom/twitter/library/client/Session;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 763
    if-nez p1, :cond_1

    .line 770
    :cond_0
    :goto_0
    return-object v0

    .line 766
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v1

    .line 767
    if-eqz v1, :cond_0

    .line 770
    iget-object v0, v1, Lcom/twitter/library/service/v;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/library/client/v;->c(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/client/Session;
    .locals 3

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/twitter/library/client/v;->b()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    .line 195
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    .line 196
    if-eqz v2, :cond_0

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 209
    :cond_1
    :goto_0
    return-object v0

    .line 204
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 206
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 207
    invoke-direct {p0}, Lcom/twitter/library/client/v;->h()Lcom/twitter/library/client/Session;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 221
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->b()Lcom/twitter/library/client/Session$LoginStatus;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/client/Session$LoginStatus;->c:Lcom/twitter/library/client/Session$LoginStatus;

    if-eq v0, v1, :cond_0

    .line 222
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->b()Lcom/twitter/library/client/Session$LoginStatus;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/client/Session$LoginStatus;->b:Lcom/twitter/library/client/Session$LoginStatus;

    if-ne v0, v1, :cond_1

    .line 223
    :cond_0
    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->d:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    .line 224
    iget-object v0, p0, Lcom/twitter/library/client/v;->g:Lcom/twitter/library/client/p;

    new-instance v1, Lbay;

    iget-object v2, p0, Lcom/twitter/library/client/v;->a:Landroid/content/Context;

    invoke-direct {v1, v2, p1}, Lbay;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    new-instance v2, Lcom/twitter/library/client/v$e;

    const/4 v3, 0x1

    invoke-direct {v2, p0, v3}, Lcom/twitter/library/client/v$e;-><init>(Lcom/twitter/library/client/v;I)V

    .line 225
    invoke-virtual {v1, v2}, Lbay;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    move-result-object v1

    .line 224
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    move-result-object v0

    .line 227
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;JLjava/lang/String;Lcom/twitter/library/client/v$b;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 127
    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->b:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    .line 128
    new-instance v1, Lbah;

    iget-object v2, p0, Lcom/twitter/library/client/v;->a:Landroid/content/Context;

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lbah;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;)V

    .line 129
    iget-object v0, v1, Lbah;->d:Ljava/lang/String;

    invoke-virtual {p0, v0, p5}, Lcom/twitter/library/client/v;->a(Ljava/lang/String;Lcom/twitter/library/client/v$f;)V

    .line 130
    iget-object v0, p0, Lcom/twitter/library/client/v;->g:Lcom/twitter/library/client/p;

    new-instance v2, Lcom/twitter/library/client/v$a;

    invoke-direct {v2, p0}, Lcom/twitter/library/client/v$a;-><init>(Lcom/twitter/library/client/v;)V

    .line 131
    invoke-virtual {v1, v2}, Lbah;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    move-result-object v1

    .line 130
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/v$b;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 148
    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->b:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    .line 149
    new-instance v1, Lbah;

    iget-object v2, p0, Lcom/twitter/library/client/v;->a:Landroid/content/Context;

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lbah;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;)V

    .line 150
    iget-object v0, v1, Lbah;->d:Ljava/lang/String;

    invoke-virtual {p0, v0, p6}, Lcom/twitter/library/client/v;->a(Ljava/lang/String;Lcom/twitter/library/client/v$f;)V

    .line 151
    iget-object v0, p0, Lcom/twitter/library/client/v;->g:Lcom/twitter/library/client/p;

    .line 152
    invoke-virtual {v1, p5}, Lbah;->a(Ljava/lang/String;)Lbah;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/client/v$a;

    invoke-direct {v2, p0}, Lcom/twitter/library/client/v$a;-><init>(Lcom/twitter/library/client/v;)V

    .line 153
    invoke-virtual {v1, v2}, Lbah;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    move-result-object v1

    .line 151
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;Lcom/twitter/library/service/s;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 262
    iget-object v1, p0, Lcom/twitter/library/client/v;->g:Lcom/twitter/library/client/p;

    new-instance v0, Lbbh;

    iget-object v2, p0, Lcom/twitter/library/client/v;->a:Landroid/content/Context;

    new-instance v3, Lcom/twitter/library/network/t;

    .line 263
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/model/account/OAuthToken;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/twitter/library/network/t;-><init>(Lcom/twitter/model/account/OAuthToken;)V

    invoke-direct {v0, v2, p1, v3}, Lbbh;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/network/t;)V

    new-instance v2, Lcom/twitter/library/client/v$e;

    const/4 v3, 0x2

    invoke-direct {v2, p0, v3}, Lcom/twitter/library/client/v$e;-><init>(Lcom/twitter/library/client/v;I)V

    .line 264
    invoke-virtual {v0, v2}, Lbbh;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/s;

    .line 265
    invoke-virtual {v0, p2}, Lcom/twitter/library/service/s;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/service/s;

    move-result-object v0

    .line 262
    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/v$d;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 183
    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->b:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    .line 184
    new-instance v0, Lbas;

    iget-object v1, p0, Lcom/twitter/library/client/v;->a:Landroid/content/Context;

    const-class v2, Lbas;

    .line 185
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lbas;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v1, v0, Lbas;->d:Ljava/lang/String;

    invoke-virtual {p0, v1, p4}, Lcom/twitter/library/client/v;->a(Ljava/lang/String;Lcom/twitter/library/client/v$f;)V

    .line 187
    iget-object v1, p0, Lcom/twitter/library/client/v;->g:Lcom/twitter/library/client/p;

    new-instance v2, Lcom/twitter/library/client/v$c;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/twitter/library/client/v$c;-><init>(Lcom/twitter/library/client/v;Lcom/twitter/library/client/v$1;)V

    .line 188
    invoke-virtual {v0, v2}, Lbas;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    move-result-object v0

    .line 187
    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/v$d;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    invoke-virtual {p0, p1}, Lcom/twitter/library/client/v;->a(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/v$d;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/v$h;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    .prologue
    .line 235
    invoke-static {}, Lcom/twitter/library/util/b;->a()I

    move-result v1

    if-lez v1, :cond_1

    .line 236
    invoke-direct {p0}, Lcom/twitter/library/client/v;->h()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 240
    :goto_0
    iget-object v1, p0, Lcom/twitter/library/client/v;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v10, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 242
    new-instance v1, Lbak;

    iget-object v2, p0, Lcom/twitter/library/client/v;->a:Landroid/content/Context;

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object/from16 v7, p8

    move-object v8, p4

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lbak;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    if-eqz v10, :cond_0

    invoke-virtual {v10}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 247
    invoke-static {v10}, Lcom/twitter/util/b;->b(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbak;->a(Ljava/lang/String;)Lbak;

    .line 249
    :cond_0
    iget-object v2, v1, Lbak;->d:Ljava/lang/String;

    move-object/from16 v0, p7

    invoke-virtual {p0, v2, v0}, Lcom/twitter/library/client/v;->a(Ljava/lang/String;Lcom/twitter/library/client/v$f;)V

    .line 250
    iget-object v2, p0, Lcom/twitter/library/client/v;->g:Lcom/twitter/library/client/p;

    new-instance v3, Lcom/twitter/library/client/v$g;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/client/v$g;-><init>(Lcom/twitter/library/client/v;Lcom/twitter/library/client/v$1;)V

    invoke-virtual {v1, v3}, Lbak;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 238
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/u;)V
    .locals 2

    .prologue
    .line 612
    if-eqz p1, :cond_1

    .line 613
    iget-object v1, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    monitor-enter v1

    .line 614
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 615
    iget-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 617
    :cond_0
    monitor-exit v1

    .line 619
    :cond_1
    return-void

    .line 617
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;Lcom/twitter/library/client/v$f;)V
    .locals 1

    .prologue
    .line 751
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 752
    iget-object v0, p0, Lcom/twitter/library/client/v;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 754
    :cond_0
    return-void
.end method

.method public b(J)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 285
    invoke-direct {p0, v0, v0, p1, p2}, Lcom/twitter/library/client/v;->a(Ljava/lang/String;Lakm;J)Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/library/client/Session;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 299
    const/4 v0, 0x0

    const-wide/16 v2, -0x1

    invoke-direct {p0, p1, v0, v2, v3}, Lcom/twitter/library/client/v;->a(Ljava/lang/String;Lakm;J)Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation

    .prologue
    .line 385
    iget-object v1, p0, Lcom/twitter/library/client/v;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 386
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/twitter/library/client/v;->d:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v1

    return-object v0

    .line 387
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/twitter/library/client/Session;)V
    .locals 6

    .prologue
    .line 561
    iget-object v1, p0, Lcom/twitter/library/client/v;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 562
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/v;->e:Lcom/twitter/library/client/Session;

    if-eqz v0, :cond_1

    .line 563
    iget-object v0, p0, Lcom/twitter/library/client/v;->e:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 564
    iget-object v0, p0, Lcom/twitter/library/client/v;->i:Ljava/util/LinkedList;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 565
    iget-object v0, p0, Lcom/twitter/library/client/v;->i:Ljava/util/LinkedList;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 567
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/client/v;->i:Ljava/util/LinkedList;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 568
    iget-object v0, p0, Lcom/twitter/library/client/v;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v2, 0x2

    if-le v0, v2, :cond_1

    .line 569
    iget-object v0, p0, Lcom/twitter/library/client/v;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    .line 572
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/client/v;->e:Lcom/twitter/library/client/Session;

    .line 573
    iput-object p1, p0, Lcom/twitter/library/client/v;->e:Lcom/twitter/library/client/Session;

    .line 574
    iget-object v2, p0, Lcom/twitter/library/client/v;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lcom/twitter/library/provider/x;->a(Landroid/content/Context;Ljava/lang/String;J)V

    .line 575
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 576
    invoke-direct {p0, v0, p1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/client/Session;)V

    .line 577
    return-void

    .line 575
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b(Lcom/twitter/library/client/u;)V
    .locals 2

    .prologue
    .line 625
    if-eqz p1, :cond_0

    .line 626
    iget-object v1, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    monitor-enter v1

    .line 627
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/v;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 628
    monitor-exit v1

    .line 630
    :cond_0
    return-void

    .line 628
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()Lcom/twitter/library/client/Session;
    .locals 2

    .prologue
    .line 586
    iget-object v1, p0, Lcom/twitter/library/client/v;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 587
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/v;->e:Lcom/twitter/library/client/Session;

    monitor-exit v1

    return-object v0

    .line 588
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c(J)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 348
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lcom/twitter/library/client/v;->a(Ljava/lang/String;J)Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/library/client/Session;
    .locals 2

    .prologue
    .line 373
    iget-object v1, p0, Lcom/twitter/library/client/v;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 374
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/v;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    monitor-exit v1

    return-object v0

    .line 375
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c(Lcom/twitter/library/client/Session;)Z
    .locals 3

    .prologue
    .line 595
    iget-object v1, p0, Lcom/twitter/library/client/v;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 596
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/v;->e:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 597
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 601
    iget-object v0, p0, Lcom/twitter/library/client/v;->i:Ljava/util/LinkedList;

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 549
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 550
    invoke-virtual {p0, p1}, Lcom/twitter/library/client/v;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 552
    invoke-virtual {p0, v0}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/Session;)V

    .line 554
    :cond_0
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 640
    if-eqz p1, :cond_0

    .line 641
    iget-object v0, p0, Lcom/twitter/library/client/v;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 643
    :cond_0
    return-void
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 774
    iget-object v1, p0, Lcom/twitter/library/client/v;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 775
    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 776
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public f()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 786
    iget-boolean v0, p0, Lcom/twitter/library/client/v;->j:Z

    if-nez v0, :cond_0

    .line 787
    iput-boolean v1, p0, Lcom/twitter/library/client/v;->j:Z

    .line 788
    invoke-virtual {p0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/Session;JZ)V

    .line 790
    :cond_0
    return-void
.end method

.method public g()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 799
    iget-boolean v0, p0, Lcom/twitter/library/client/v;->j:Z

    if-eqz v0, :cond_0

    .line 800
    iput-boolean v1, p0, Lcom/twitter/library/client/v;->j:Z

    .line 801
    invoke-virtual {p0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/Session;JZ)V

    .line 803
    :cond_0
    return-void
.end method
