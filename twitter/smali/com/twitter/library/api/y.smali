.class public Lcom/twitter/library/api/y;
.super Lcom/twitter/library/service/c;
.source "Twttr"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:I

.field private final b:Lcom/twitter/model/core/TwitterUser;

.field private c:Ljava/lang/Object;

.field private d:Z


# direct methods
.method private constructor <init>(Lcom/twitter/model/core/TwitterUser;I)V
    .locals 1

    .prologue
    .line 214
    invoke-direct {p0}, Lcom/twitter/library/service/c;-><init>()V

    .line 212
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/api/y;->d:Z

    .line 215
    iput-object p1, p0, Lcom/twitter/library/api/y;->b:Lcom/twitter/model/core/TwitterUser;

    .line 216
    iput p2, p0, Lcom/twitter/library/api/y;->a:I

    .line 217
    return-void
.end method

.method public static a(I)Lcom/twitter/library/api/y;
    .locals 2

    .prologue
    .line 439
    new-instance v0, Lcom/twitter/library/api/y;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/twitter/library/api/y;-><init>(Lcom/twitter/model/core/TwitterUser;I)V

    return-object v0
.end method

.method public static a(ILcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/api/y;
    .locals 1

    .prologue
    .line 443
    new-instance v0, Lcom/twitter/library/api/y;

    invoke-direct {v0, p1, p0}, Lcom/twitter/library/api/y;-><init>(Lcom/twitter/model/core/TwitterUser;I)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/library/service/a;
    .locals 1

    .prologue
    .line 459
    iget-boolean v0, p0, Lcom/twitter/library/api/y;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/model/core/z;

    invoke-static {v0}, Lcom/twitter/library/service/a;->a(Lcom/twitter/model/core/z;)Lcom/twitter/library/service/a;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ILjava/io/InputStream;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v3, 0xc8

    .line 222
    if-nez p4, :cond_0

    .line 431
    :goto_0
    return-void

    .line 226
    :cond_0
    const-string/jumbo v0, "application/json"

    invoke-virtual {p4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 227
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Reader could not validate. content-type=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "], or encoding=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 231
    :cond_1
    const/4 v1, 0x0

    .line 233
    :try_start_0
    invoke-static {p2}, Lcom/twitter/library/api/z;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 235
    if-ne p1, v3, :cond_2

    .line 236
    :try_start_1
    iget v0, p0, Lcom/twitter/library/api/y;->a:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    packed-switch v0, :pswitch_data_0

    .line 429
    :goto_1
    :pswitch_0
    invoke-static {v2}, Lcqc;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 238
    :pswitch_1
    :try_start_2
    invoke-static {v2}, Lcom/twitter/library/api/z;->l(Lcom/fasterxml/jackson/core/JsonParser;)Landroid/util/Pair;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 429
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    throw v0

    .line 242
    :pswitch_2
    :try_start_3
    invoke-static {v2}, Lcom/twitter/library/api/z;->w(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto :goto_1

    .line 246
    :pswitch_3
    invoke-static {v2}, Lcom/twitter/library/api/z;->m(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto :goto_1

    .line 250
    :pswitch_4
    invoke-static {v2}, Lcom/twitter/library/api/z;->z(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/ActivitySummary;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto :goto_1

    .line 254
    :pswitch_5
    invoke-static {v2}, Lcom/twitter/library/api/z;->C(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/x;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto :goto_1

    .line 258
    :pswitch_6
    invoke-static {v2}, Lcom/twitter/library/api/z;->A(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto :goto_1

    .line 262
    :pswitch_7
    invoke-static {v2}, Lcom/twitter/library/api/z;->B(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/search/TwitterTypeAheadGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto :goto_1

    .line 266
    :pswitch_8
    const-class v0, Lcom/twitter/model/core/TwitterUser;

    invoke-static {v2, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto :goto_1

    .line 270
    :pswitch_9
    invoke-static {v2}, Lcom/twitter/library/api/z;->f(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/ClientConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto :goto_1

    .line 274
    :pswitch_a
    invoke-static {v2}, Lcom/twitter/library/api/z;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/x;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto :goto_1

    .line 278
    :pswitch_b
    iget-object v0, p0, Lcom/twitter/library/api/y;->b:Lcom/twitter/model/core/TwitterUser;

    invoke-static {v2, v0}, Lcom/twitter/library/api/z;->d(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/api/w;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto :goto_1

    .line 282
    :pswitch_c
    iget-object v0, p0, Lcom/twitter/library/api/y;->b:Lcom/twitter/model/core/TwitterUser;

    invoke-static {v2, v0}, Lcom/twitter/library/api/z;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/api/search/g;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto :goto_1

    .line 286
    :pswitch_d
    invoke-static {v2}, Lcom/twitter/library/api/z;->k(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/geo/b;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto :goto_1

    .line 290
    :pswitch_e
    invoke-static {v2}, Lcom/twitter/library/api/z;->h(Lcom/fasterxml/jackson/core/JsonParser;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto :goto_1

    .line 294
    :pswitch_f
    invoke-static {v2}, Lcom/twitter/library/api/z;->i(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto :goto_1

    .line 298
    :pswitch_10
    invoke-static {v2}, Lcom/twitter/library/api/z;->n(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 302
    :pswitch_11
    invoke-static {v2}, Lcom/twitter/library/api/z;->o(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 306
    :pswitch_12
    invoke-static {v2}, Lcom/twitter/library/api/z;->y(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 310
    :pswitch_13
    invoke-static {v2}, Lcom/twitter/library/api/z;->g(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 314
    :pswitch_14
    invoke-static {v2}, Lcom/twitter/library/api/z;->d(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 318
    :pswitch_15
    invoke-static {v2}, Lcom/twitter/library/api/z;->b(Lcom/fasterxml/jackson/core/JsonParser;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 322
    :pswitch_16
    invoke-static {v2}, Lcom/twitter/library/api/z;->c(Lcom/fasterxml/jackson/core/JsonParser;)Lcbv;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 326
    :pswitch_17
    invoke-static {v2}, Lcom/twitter/library/api/z;->D(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 330
    :pswitch_18
    invoke-static {v2}, Lcom/twitter/library/api/z;->p(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/timeline/n;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 334
    :pswitch_19
    iget-object v0, p0, Lcom/twitter/library/api/y;->b:Lcom/twitter/model/core/TwitterUser;

    invoke-static {v2, v0}, Lcom/twitter/library/api/z;->b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/api/t;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 338
    :pswitch_1a
    invoke-static {v2}, Lcom/twitter/library/api/z;->I(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 342
    :pswitch_1b
    invoke-static {v2}, Lcom/twitter/library/api/z;->J(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 346
    :pswitch_1c
    invoke-static {v2}, Lcom/twitter/library/api/z;->K(Lcom/fasterxml/jackson/core/JsonParser;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 350
    :pswitch_1d
    invoke-static {v2}, Lcom/twitter/library/api/z;->L(Lcom/fasterxml/jackson/core/JsonParser;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 354
    :pswitch_1e
    invoke-static {v2}, Lcom/twitter/library/api/z;->G(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 358
    :pswitch_1f
    invoke-static {v2}, Lcom/twitter/library/api/z;->H(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/n;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 362
    :pswitch_20
    invoke-static {v2}, Lcom/twitter/library/api/z;->N(Lcom/fasterxml/jackson/core/JsonParser;)Lbil;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 366
    :pswitch_21
    invoke-static {v2}, Lcom/twitter/library/api/z;->j(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 370
    :pswitch_22
    invoke-static {v2}, Lcom/twitter/library/api/z;->M(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/m;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 374
    :pswitch_23
    invoke-static {v2}, Lcom/twitter/library/card/k;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcar;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 378
    :pswitch_24
    invoke-static {v2}, Lcom/twitter/library/card/k;->b(Lcom/fasterxml/jackson/core/JsonParser;)Lcax;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 382
    :pswitch_25
    invoke-static {v2}, Lcom/twitter/library/api/z;->O(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 386
    :pswitch_26
    invoke-static {v2}, Lcom/twitter/library/api/z;->P(Lcom/fasterxml/jackson/core/JsonParser;)Lbsj;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 390
    :pswitch_27
    iget-object v0, p0, Lcom/twitter/library/api/y;->b:Lcom/twitter/model/core/TwitterUser;

    invoke-static {v2, v0}, Lcom/twitter/library/api/z;->c(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/core/TwitterUser;)Lbdf;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 398
    :cond_2
    iget v0, p0, Lcom/twitter/library/api/y;->a:I

    sparse-switch v0, :sswitch_data_0

    .line 422
    const-class v0, Lcom/twitter/model/core/z;

    invoke-static {v2, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    .line 423
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/api/y;->d:Z

    goto/16 :goto_1

    .line 400
    :sswitch_0
    const-class v0, Lcom/twitter/model/core/z;

    invoke-static {v2, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/z;

    .line 401
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/y;

    .line 402
    const/16 v3, 0x193

    if-ne p1, v3, :cond_3

    if-eqz v1, :cond_3

    iget v1, v1, Lcom/twitter/model/core/y;->b:I

    const/16 v3, 0x55

    if-ne v1, v3, :cond_3

    .line 404
    invoke-static {v0}, Lcom/twitter/library/api/z;->a(Lcom/twitter/model/core/z;)Lcom/twitter/library/api/u;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 406
    :cond_3
    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    .line 407
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/api/y;->d:Z

    goto/16 :goto_1

    .line 413
    :sswitch_1
    if-lt p1, v3, :cond_4

    const/16 v0, 0x12c

    if-ge p1, v0, :cond_4

    .line 414
    invoke-static {v2}, Lcom/twitter/library/api/z;->d(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    goto/16 :goto_1

    .line 416
    :cond_4
    const-class v0, Lcom/twitter/model/core/z;

    invoke-static {v2, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    .line 417
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/api/y;->d:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 429
    :catchall_1
    move-exception v0

    goto/16 :goto_2

    .line 236
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_17
        :pswitch_18
        :pswitch_0
        :pswitch_0
        :pswitch_19
        :pswitch_27
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_16
        :pswitch_0
        :pswitch_1a
        :pswitch_1b
        :pswitch_14
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1e
        :pswitch_1c
        :pswitch_1d
        :pswitch_1f
        :pswitch_1
        :pswitch_20
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_21
        :pswitch_0
        :pswitch_22
        :pswitch_23
        :pswitch_0
        :pswitch_25
        :pswitch_26
        :pswitch_24
    .end packed-switch

    .line 398
    :sswitch_data_0
    .sparse-switch
        0x37 -> :sswitch_1
        0x3c -> :sswitch_0
    .end sparse-switch
.end method

.method public a(Lcom/twitter/network/l;)V
    .locals 1

    .prologue
    .line 448
    iget-boolean v0, p0, Lcom/twitter/library/api/y;->d:Z

    if-eqz v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/model/core/z;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/y;

    .line 450
    if-eqz v0, :cond_0

    .line 451
    iget v0, v0, Lcom/twitter/model/core/y;->b:I

    iput v0, p1, Lcom/twitter/network/l;->j:I

    .line 454
    :cond_0
    return-void
.end method

.method public b()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()TT;"
        }
    .end annotation

    .prologue
    .line 434
    iget-object v0, p0, Lcom/twitter/library/api/y;->c:Ljava/lang/Object;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
