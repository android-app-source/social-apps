.class public Lcom/twitter/library/util/b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/util/b$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".auth.login"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/util/b;->a:Ljava/lang/String;

    .line 66
    const-class v0, Lcom/twitter/model/profile/TranslatorType;

    new-instance v1, Lcom/twitter/model/json/profiles/b;

    invoke-direct {v1}, Lcom/twitter/model/json/profiles/b;-><init>()V

    invoke-static {v0, v1}, Lcom/twitter/model/json/common/e;->a(Ljava/lang/Class;Lcom/bluelinelabs/logansquare/typeconverters/TypeConverter;)V

    .line 67
    return-void
.end method

.method public static a()I
    .locals 1

    .prologue
    .line 75
    invoke-static {}, Lcom/twitter/library/util/b;->b()Lakn;

    move-result-object v0

    invoke-virtual {v0}, Lakn;->b()I

    move-result v0

    return v0
.end method

.method public static a(J)Lakm;
    .locals 2

    .prologue
    .line 256
    invoke-static {}, Lcom/twitter/library/util/b;->b()Lakn;

    move-result-object v0

    new-instance v1, Lcnz;

    invoke-direct {v1, p0, p1}, Lcnz;-><init>(J)V

    invoke-virtual {v0, v1}, Lakn;->a(Lcnz;)Lakm;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/OAuthToken;)Lakm;
    .locals 4

    .prologue
    .line 277
    new-instance v0, Lcnz;

    iget-wide v2, p0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-direct {v0, v2, v3}, Lcnz;-><init>(J)V

    .line 278
    invoke-static {}, Lcom/twitter/library/util/b;->b()Lakn;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lakn;->a(Lcnz;Ljava/lang/String;)Lakm;

    move-result-object v0

    .line 279
    if-eqz v0, :cond_0

    .line 281
    const-string/jumbo v1, "account_user_info"

    invoke-static {p0}, Lcom/twitter/library/util/b;->b(Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lakm;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    const-string/jumbo v1, "com.twitter.android.oauth.token"

    iget-object v2, p1, Lcom/twitter/model/account/OAuthToken;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lakm;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    const-string/jumbo v1, "com.twitter.android.oauth.token.secret"

    iget-object v2, p1, Lcom/twitter/model/account/OAuthToken;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lakm;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/model/account/OAuthToken;)Lakm;
    .locals 1

    .prologue
    .line 268
    invoke-static {p0}, Lcom/twitter/library/util/b;->c(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 269
    if-eqz v0, :cond_0

    .line 270
    invoke-static {v0, p1}, Lcom/twitter/library/util/b;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/OAuthToken;)Lakm;

    move-result-object v0

    .line 272
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Landroid/accounts/AccountManagerFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 91
    invoke-static {p0}, Lcom/twitter/library/util/b;->b(Ljava/lang/String;)Lakm;

    move-result-object v0

    .line 92
    if-eqz v0, :cond_0

    .line 93
    invoke-virtual {v0}, Lakm;->c()Landroid/accounts/AccountManagerFuture;

    move-result-object v0

    .line 95
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lakm;Z)Lcom/twitter/model/account/OAuthToken;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AuthenticatorException;,
            Landroid/accounts/OperationCanceledException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 211
    const-string/jumbo v0, "com.twitter.android.oauth.token"

    invoke-virtual {p0, v0, p1}, Lakm;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 212
    const-string/jumbo v0, "com.twitter.android.oauth.token.secret"

    invoke-virtual {p0, v0, p1}, Lakm;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 214
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 215
    new-instance v0, Lcom/twitter/model/account/OAuthToken;

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/account/OAuthToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lakm;)Lcom/twitter/model/account/UserSettings;
    .locals 2

    .prologue
    .line 181
    const-string/jumbo v0, "account_settings"

    invoke-virtual {p0, v0}, Lakm;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 182
    if-eqz v0, :cond_0

    .line 183
    const-class v1, Lcom/twitter/model/account/UserSettings;

    invoke-static {v0, v1}, Lcom/twitter/model/json/common/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/UserSettings;

    .line 185
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/accounts/Account;)Lcom/twitter/model/core/TwitterUser;
    .locals 1

    .prologue
    .line 140
    invoke-static {}, Lcom/twitter/library/util/b;->b()Lakn;

    move-result-object v0

    invoke-virtual {v0, p0}, Lakn;->a(Landroid/accounts/Account;)Lakm;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/b;->c(Lakm;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;Lcom/twitter/library/client/v;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lakm;",
            ">;",
            "Lcom/twitter/library/client/v;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/account/UserAccount;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 358
    invoke-virtual {p1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    .line 360
    invoke-virtual {p1}, Lcom/twitter/library/client/v;->d()Ljava/util/List;

    move-result-object v4

    .line 361
    new-instance v5, Ljava/util/HashMap;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v5, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 362
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v6

    move v1, v2

    .line 364
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 365
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    .line 366
    invoke-virtual {v0}, Lakm;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 367
    new-instance v7, Lcom/twitter/model/account/UserAccount;

    invoke-virtual {v0}, Lakm;->a()Landroid/accounts/Account;

    move-result-object v8

    invoke-static {v0}, Lcom/twitter/library/util/b;->c(Lakm;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    invoke-direct {v7, v8, v0}, Lcom/twitter/model/account/UserAccount;-><init>(Landroid/accounts/Account;Lcom/twitter/model/core/TwitterUser;)V

    .line 369
    iget-object v0, v7, Lcom/twitter/model/account/UserAccount;->b:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, v7, Lcom/twitter/model/account/UserAccount;->b:Lcom/twitter/model/core/TwitterUser;

    iget-wide v8, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 371
    const/4 v8, -0x1

    if-eq v0, v8, :cond_1

    .line 372
    iget-object v0, v7, Lcom/twitter/model/account/UserAccount;->b:Lcom/twitter/model/core/TwitterUser;

    iget-wide v8, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v5, v0, v7}, Ljava/util/AbstractMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 374
    :cond_1
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 381
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 382
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/AbstractMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/UserAccount;

    .line 383
    if-eqz v0, :cond_3

    .line 384
    invoke-interface {v6, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_2

    .line 388
    :cond_4
    return-object v6
.end method

.method public static a(Lakm;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V
    .locals 2

    .prologue
    .line 333
    if-eqz p1, :cond_0

    .line 334
    const-string/jumbo v0, "account_user_info"

    invoke-static {p1}, Lcom/twitter/library/util/b;->b(Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lakm;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    :cond_0
    if-eqz p2, :cond_1

    .line 337
    const-string/jumbo v0, "account_settings"

    invoke-virtual {p2}, Lcom/twitter/model/account/UserSettings;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lakm;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    :cond_1
    return-void
.end method

.method public static a(Lakm;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 117
    if-eqz p0, :cond_0

    sget-boolean v0, Lcom/twitter/library/util/b;->b:Z

    if-nez v0, :cond_0

    .line 118
    invoke-virtual {p0}, Lakm;->a()Landroid/accounts/Account;

    move-result-object v0

    invoke-static {v0, p1, p2}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 120
    :cond_0
    return-void
.end method

.method public static a(Lakn;Lcom/twitter/model/core/TwitterUser;)V
    .locals 4

    .prologue
    .line 154
    new-instance v0, Lcnz;

    iget-wide v2, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-direct {v0, v2, v3}, Lcnz;-><init>(J)V

    invoke-virtual {p0, v0}, Lakn;->a(Lcnz;)Lakm;

    move-result-object v0

    .line 155
    if-eqz v0, :cond_0

    .line 156
    const-string/jumbo v1, "account_user_info"

    invoke-virtual {v0, v1}, Lakm;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 157
    invoke-static {p1}, Lcom/twitter/library/util/b;->b(Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;

    move-result-object v2

    .line 158
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 159
    const-string/jumbo v1, "account_user_info"

    invoke-virtual {v0, v1, v2}, Lakm;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    :cond_0
    return-void
.end method

.method public static a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 1

    .prologue
    .line 147
    invoke-static {}, Lcom/twitter/library/util/b;->b()Lakn;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/twitter/library/util/b;->a(Lakn;Lcom/twitter/model/core/TwitterUser;)V

    .line 148
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V
    .locals 1

    .prologue
    .line 320
    invoke-static {p0}, Lcom/twitter/library/util/b;->b(Ljava/lang/String;)Lakm;

    move-result-object v0

    .line 321
    if-eqz v0, :cond_0

    .line 322
    invoke-static {v0, p1, p2}, Lcom/twitter/library/util/b;->a(Lakm;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V

    .line 324
    :cond_0
    return-void
.end method

.method public static a(Lakm;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 106
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lakm;->a()Landroid/accounts/Account;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)[Lcom/twitter/model/account/UserAccount;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lakm;",
            ">;)[",
            "Lcom/twitter/model/account/UserAccount;"
        }
    .end annotation

    .prologue
    .line 343
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    .line 344
    new-array v3, v2, [Lcom/twitter/model/account/UserAccount;

    .line 345
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 346
    new-instance v4, Lcom/twitter/model/account/UserAccount;

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    invoke-virtual {v0}, Lakm;->a()Landroid/accounts/Account;

    move-result-object v5

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    invoke-static {v0}, Lcom/twitter/library/util/b;->c(Lakm;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    invoke-direct {v4, v5, v0}, Lcom/twitter/model/account/UserAccount;-><init>(Landroid/accounts/Account;Lcom/twitter/model/core/TwitterUser;)V

    aput-object v4, v3, v1

    .line 345
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 348
    :cond_0
    return-object v3
.end method

.method public static b(Lakm;Ljava/lang/String;)Lakm;
    .locals 2

    .prologue
    .line 301
    invoke-static {}, Lcom/twitter/library/util/b;->b()Lakn;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/util/b$1;

    invoke-direct {v1}, Lcom/twitter/library/util/b$1;-><init>()V

    invoke-virtual {v0, p0, p1, v1}, Lakn;->a(Lakm;Ljava/lang/String;Lakn$a;)Lakm;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lakm;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 241
    invoke-static {}, Lcom/twitter/library/util/b;->b()Lakn;

    move-result-object v0

    invoke-virtual {v0}, Lakn;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    .line 242
    invoke-virtual {v0}, Lakm;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 246
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b()Lakn;
    .locals 1

    .prologue
    .line 415
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lakm;)Lcom/twitter/model/account/OAuthToken;
    .locals 3

    .prologue
    .line 197
    const-string/jumbo v0, "com.twitter.android.oauth.token"

    invoke-virtual {p0, v0}, Lakm;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 198
    const-string/jumbo v0, "com.twitter.android.oauth.token.secret"

    invoke-virtual {p0, v0}, Lakm;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 200
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 201
    new-instance v0, Lcom/twitter/model/account/OAuthToken;

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/account/OAuthToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    :try_start_0
    invoke-static {p0}, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/json/core/JsonTwitterAccountUser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/json/common/g;->a(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 175
    :goto_0
    return-object v0

    .line 174
    :catch_0
    move-exception v0

    .line 175
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public static c(Lakm;)Lcom/twitter/model/core/TwitterUser;
    .locals 1

    .prologue
    .line 223
    if-eqz p0, :cond_0

    .line 224
    const-string/jumbo v0, "account_user_info"

    invoke-virtual {p0, v0}, Lakm;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 225
    if-eqz v0, :cond_0

    .line 226
    invoke-static {v0}, Lcom/twitter/library/util/b;->c(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 229
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser;
    .locals 1

    .prologue
    .line 405
    :try_start_0
    const-class v0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;

    invoke-static {p0, v0}, Lcom/bluelinelabs/logansquare/LoganSquare;->parse(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/core/JsonTwitterAccountUser;

    .line 406
    if-eqz v0, :cond_0

    .line 407
    invoke-virtual {v0}, Lcom/twitter/model/json/core/JsonTwitterAccountUser;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 411
    :goto_0
    return-object v0

    .line 409
    :catch_0
    move-exception v0

    .line 411
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
