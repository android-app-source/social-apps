.class Lcom/twitter/library/scribe/d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/scribe/d$a;
    }
.end annotation


# static fields
.field private static a:Lcom/twitter/library/scribe/d;


# instance fields
.field private final b:Lcrr;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/twitter/library/util/m;

.field private final e:Lcom/twitter/library/scribe/d$a;

.field private final f:Lcom/twitter/library/scribe/f;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcrr;Lcom/twitter/library/util/m;Lcom/twitter/library/scribe/d$a;Lcom/twitter/library/scribe/f;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object p1, p0, Lcom/twitter/library/scribe/d;->c:Landroid/content/Context;

    .line 105
    iput-object p2, p0, Lcom/twitter/library/scribe/d;->b:Lcrr;

    .line 106
    iput-object p3, p0, Lcom/twitter/library/scribe/d;->d:Lcom/twitter/library/util/m;

    .line 107
    iput-object p4, p0, Lcom/twitter/library/scribe/d;->e:Lcom/twitter/library/scribe/d$a;

    .line 108
    iput-object p5, p0, Lcom/twitter/library/scribe/d;->f:Lcom/twitter/library/scribe/f;

    .line 109
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/twitter/library/scribe/d;
    .locals 6

    .prologue
    .line 39
    sget-object v0, Lcom/twitter/library/scribe/d;->a:Lcom/twitter/library/scribe/d;

    if-nez v0, :cond_0

    .line 40
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 41
    new-instance v0, Lcom/twitter/library/scribe/d;

    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/util/m;

    .line 42
    invoke-static {v1}, Lcom/evernote/android/job/d;->a(Landroid/content/Context;)Lcom/evernote/android/job/d;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/twitter/library/util/m;-><init>(Lcom/evernote/android/job/d;)V

    new-instance v4, Lcom/twitter/library/scribe/d$a;

    invoke-direct {v4}, Lcom/twitter/library/scribe/d$a;-><init>()V

    .line 43
    invoke-static {}, Lcom/twitter/library/scribe/f;->a()Lcom/twitter/library/scribe/f;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/scribe/d;-><init>(Landroid/content/Context;Lcrr;Lcom/twitter/library/util/m;Lcom/twitter/library/scribe/d$a;Lcom/twitter/library/scribe/f;)V

    sput-object v0, Lcom/twitter/library/scribe/d;->a:Lcom/twitter/library/scribe/d;

    .line 44
    const-class v0, Lcom/twitter/library/scribe/d;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 46
    :cond_0
    sget-object v0, Lcom/twitter/library/scribe/d;->a:Lcom/twitter/library/scribe/d;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 117
    invoke-static {}, Lcom/twitter/library/scribe/j;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 118
    invoke-static {}, Lcom/twitter/util/g;->c()V

    .line 119
    iget-object v0, p0, Lcom/twitter/library/scribe/d;->f:Lcom/twitter/library/scribe/f;

    invoke-virtual {v0}, Lcom/twitter/library/scribe/f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/scribe/d;->d:Lcom/twitter/library/util/m;

    const-string/jumbo v1, "ScribeFlushJob"

    invoke-virtual {v0, v1}, Lcom/twitter/library/util/m;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/twitter/library/scribe/d;->f:Lcom/twitter/library/scribe/f;

    invoke-virtual {v0}, Lcom/twitter/library/scribe/f;->e()J

    move-result-wide v0

    .line 121
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 122
    iget-object v2, p0, Lcom/twitter/library/scribe/d;->e:Lcom/twitter/library/scribe/d$a;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/library/scribe/d$a;->b(J)V

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/scribe/d;->e:Lcom/twitter/library/scribe/d$a;

    iget-object v1, p0, Lcom/twitter/library/scribe/d;->f:Lcom/twitter/library/scribe/f;

    invoke-virtual {v1}, Lcom/twitter/library/scribe/f;->d()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/scribe/d$a;->a(J)V

    goto :goto_0

    .line 128
    :cond_2
    iget-object v0, p0, Lcom/twitter/library/scribe/d;->c:Landroid/content/Context;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/library/scribe/d;->a(Landroid/content/Context;ZLjava/lang/String;)Z

    goto :goto_0
.end method

.method public a(Landroid/content/Context;ZLjava/lang/String;)Z
    .locals 12
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 67
    iget-object v0, p0, Lcom/twitter/library/scribe/d;->b:Lcrr;

    invoke-virtual {v0}, Lcrr;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/scribe/d;->f:Lcom/twitter/library/scribe/f;

    invoke-virtual {v0}, Lcom/twitter/library/scribe/f;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 70
    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 71
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v6

    .line 74
    if-eqz p2, :cond_0

    move v0, v1

    .line 83
    :goto_0
    if-eqz v0, :cond_2

    .line 84
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "log_last_flush_request"

    invoke-interface {v0, v2, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 85
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/library/scribe/ScribeService;

    invoke-direct {v0, v3, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "FLUSH"

    .line 86
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "flush_request_id"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 85
    invoke-virtual {v3, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 90
    :goto_1
    return v1

    .line 77
    :cond_0
    invoke-static {}, Lcom/twitter/library/scribe/f;->a()Lcom/twitter/library/scribe/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/scribe/f;->d()J

    move-result-wide v8

    .line 78
    const-string/jumbo v0, "log_last_flush_request"

    const-wide/16 v10, 0x0

    invoke-interface {v4, v0, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    .line 80
    add-long/2addr v8, v10

    cmp-long v0, v8, v6

    if-gez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    .line 90
    goto :goto_1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 138
    invoke-static {}, Lcom/twitter/util/g;->c()V

    .line 139
    iget-object v0, p0, Lcom/twitter/library/scribe/d;->b:Lcrr;

    invoke-virtual {v0}, Lcrr;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/twitter/library/scribe/d;->d:Lcom/twitter/library/util/m;

    const-string/jumbo v1, "ScribeFlushJob"

    invoke-virtual {v0, v1}, Lcom/twitter/library/util/m;->c(Ljava/lang/String;)I

    .line 141
    iget-object v0, p0, Lcom/twitter/library/scribe/d;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;)V

    .line 145
    :goto_0
    return-void

    .line 143
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/scribe/d;->a()V

    goto :goto_0
.end method
