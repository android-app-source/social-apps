.class public Lcom/twitter/library/scribe/ScribeService;
.super Landroid/app/IntentService;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/scribe/ScribeService$a;,
        Lcom/twitter/library/scribe/ScribeService$c;,
        Lcom/twitter/library/scribe/ScribeService$b;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/scribe/ScribeService$b;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Lcom/twitter/library/scribe/ScribeService$c;

.field private static e:Lcom/twitter/library/scribe/ScribeService$c;

.field private static f:Lcom/twitter/metrics/c;


# instance fields
.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 214
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/library/scribe/ScribeService;->a:Ljava/util/Map;

    .line 216
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/library/scribe/ScribeService;->b:Ljava/util/Map;

    .line 217
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/library/scribe/ScribeService;->c:Ljava/util/Map;

    .line 226
    new-instance v0, Lcom/twitter/library/scribe/ScribeService$a;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeService$a;-><init>()V

    sput-object v0, Lcom/twitter/library/scribe/ScribeService;->d:Lcom/twitter/library/scribe/ScribeService$c;

    .line 227
    sget-object v0, Lcom/twitter/library/scribe/ScribeService;->d:Lcom/twitter/library/scribe/ScribeService$c;

    sput-object v0, Lcom/twitter/library/scribe/ScribeService;->e:Lcom/twitter/library/scribe/ScribeService$c;

    .line 235
    sget-object v0, Lcom/twitter/library/scribe/ScribeService;->a:Ljava/util/Map;

    const-string/jumbo v1, "LOG"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    sget-object v0, Lcom/twitter/library/scribe/ScribeService;->a:Ljava/util/Map;

    const-string/jumbo v1, "FLUSH"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    sget-object v0, Lcom/twitter/library/scribe/ScribeService;->a:Ljava/util/Map;

    const-string/jumbo v1, "FLUSH_IMMEDIATELY"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    sget-object v0, Lcom/twitter/library/scribe/ScribeService;->a:Ljava/util/Map;

    const-string/jumbo v1, "RESEND_EXPERIMENTS"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    sget-object v0, Lcom/twitter/library/scribe/ScribeService;->a:Ljava/util/Map;

    const-string/jumbo v1, "UPDATE_EXP_LOG_TIMESTAMP"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    sget-object v0, Lcom/twitter/library/scribe/ScribeService;->a:Ljava/util/Map;

    const-string/jumbo v1, "UPDATE_ENDPOINT_URL"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    sget-object v0, Lcom/twitter/library/scribe/ScribeService;->a:Ljava/util/Map;

    const-string/jumbo v1, "LOG_THRIFT"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 467
    const-string/jumbo v0, "ScribeService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 468
    return-void
.end method

.method public static a()Lcom/twitter/common_header/thriftandroid/VersionedCommonHeader;
    .locals 6

    .prologue
    .line 840
    new-instance v0, Lcom/twitter/common_header/thriftandroid/ClientHeader$a;

    invoke-direct {v0}, Lcom/twitter/common_header/thriftandroid/ClientHeader$a;-><init>()V

    .line 842
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lcom/twitter/library/scribe/ScribeService;->a(JLjava/util/TimeZone;)S

    move-result v1

    .line 843
    sget-object v2, Lcom/twitter/common_header/thriftandroid/ClientHeader;->b:Lcom/twitter/common_header/thriftandroid/ClientHeader$_Fields;

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/common_header/thriftandroid/ClientHeader$a;->a(Lcom/twitter/common_header/thriftandroid/ClientHeader$_Fields;Ljava/lang/Object;)Lcom/twitter/common_header/thriftandroid/ClientHeader$a;

    move-result-object v2

    sget-object v3, Lcom/twitter/common_header/thriftandroid/ClientHeader;->c:Lcom/twitter/common_header/thriftandroid/ClientHeader$_Fields;

    .line 844
    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/twitter/common_header/thriftandroid/ClientHeader$a;->a(Lcom/twitter/common_header/thriftandroid/ClientHeader$_Fields;Ljava/lang/Object;)Lcom/twitter/common_header/thriftandroid/ClientHeader$a;

    .line 846
    new-instance v1, Lcom/twitter/common_header/thriftandroid/CommonHeader;

    invoke-direct {v1}, Lcom/twitter/common_header/thriftandroid/CommonHeader;-><init>()V

    .line 847
    sget-object v2, Lcom/twitter/common_header/thriftandroid/CommonHeader;->b:Lcom/twitter/common_header/thriftandroid/CommonHeader$_Fields;

    invoke-virtual {v0}, Lcom/twitter/common_header/thriftandroid/ClientHeader$a;->a()Lcom/twitter/common_header/thriftandroid/ClientHeader;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/twitter/common_header/thriftandroid/CommonHeader;->b(Lorg/apache/thrift/b;Ljava/lang/Object;)V

    .line 849
    new-instance v0, Lcom/twitter/common_header/thriftandroid/VersionedCommonHeader;

    invoke-direct {v0}, Lcom/twitter/common_header/thriftandroid/VersionedCommonHeader;-><init>()V

    .line 851
    sget-object v2, Lcom/twitter/common_header/thriftandroid/VersionedCommonHeader;->c:Lcom/twitter/common_header/thriftandroid/VersionedCommonHeader$_Fields;

    invoke-virtual {v0, v2, v1}, Lcom/twitter/common_header/thriftandroid/VersionedCommonHeader;->b(Lorg/apache/thrift/b;Ljava/lang/Object;)V

    .line 852
    return-object v0
.end method

.method private static a(JJ)Ljava/lang/Long;
    .locals 4

    .prologue
    .line 732
    sget-object v0, Lcom/twitter/library/scribe/ScribeService;->b:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 733
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Map;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 734
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 740
    :goto_0
    return-object v0

    .line 737
    :cond_0
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeService$b;

    .line 738
    iget-object v0, v0, Lcom/twitter/library/scribe/ScribeService$b;->e:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3, p2, p3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p2

    goto :goto_1

    .line 740
    :cond_1
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method static a(JLjava/util/TimeZone;)S
    .locals 4

    .prologue
    .line 835
    invoke-virtual {p2, p0, p1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    .line 836
    int-to-long v0, v0

    const-wide/32 v2, 0xea60

    div-long/2addr v0, v2

    long-to-int v0, v0

    int-to-short v0, v0

    return v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 262
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/library/scribe/ScribeService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "FLUSH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 263
    return-void
.end method

.method private static a(Landroid/content/Context;J)V
    .locals 5

    .prologue
    .line 760
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/library/scribe/ScribeService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "RESEND_EXPERIMENTS"

    .line 761
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    .line 762
    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "exp_request_time"

    .line 763
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 760
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 764
    return-void
.end method

.method private static a(Landroid/content/Context;JLjava/lang/String;ILjava/lang/String;J)V
    .locals 2

    .prologue
    .line 746
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/library/scribe/ScribeService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "UPDATE_EXP_LOG_TIMESTAMP"

    .line 747
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    .line 748
    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "exp_request_time"

    .line 749
    invoke-virtual {v0, v1, p6, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "exp_key"

    .line 750
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "exp_version"

    .line 751
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "exp_bucket"

    .line 752
    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 746
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 753
    return-void
.end method

.method public static a(Landroid/content/Context;JLjava/lang/Throwable;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 897
    const-string/jumbo v0, "scribe_crash_sample_size"

    const/16 v1, 0x2710

    invoke-static {v0, v1}, Lcom/twitter/library/scribe/ScribeService;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 898
    invoke-static {p0}, Lcom/twitter/library/network/ab;->a(Landroid/content/Context;)Lcom/twitter/library/network/ab;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/network/ab;->c:Lcom/twitter/library/network/ae;

    .line 899
    invoke-virtual {v0}, Lcom/twitter/library/network/ae;->toString()Ljava/lang/String;

    move-result-object v1

    .line 900
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "app::::crash"

    aput-object v4, v2, v3

    .line 901
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 902
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->e(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 903
    if-eqz p3, :cond_0

    .line 904
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 905
    invoke-static {p3}, Lcqj;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 908
    :cond_0
    invoke-static {p0, v0, v5}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/analytics/model/ScribeLog;Z)V

    .line 910
    :cond_1
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/analytics/model/ScribeLog;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/analytics/model/ScribeLog;",
            ">(",
            "Landroid/content/Context;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 881
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/library/scribe/ScribeService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "LOG"

    .line 882
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "log"

    .line 883
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 881
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 887
    :goto_0
    return-void

    .line 884
    :catch_0
    move-exception v0

    .line 885
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/analytics/model/ScribeLog;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/analytics/model/ScribeLog;",
            ">(",
            "Landroid/content/Context;",
            "TT;Z)V"
        }
    .end annotation

    .prologue
    .line 823
    invoke-virtual {p1}, Lcom/twitter/analytics/model/ScribeLog;->c()Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Logged event with no user ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 824
    if-eqz p2, :cond_0

    .line 828
    invoke-virtual {p1}, Lcom/twitter/analytics/model/ScribeLog;->d()Lcnz;

    move-result-object v0

    invoke-virtual {v0}, Lcnz;->b()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;J)V

    .line 831
    :cond_0
    invoke-static {p0, p1}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/analytics/model/ScribeLog;)V

    .line 832
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/scribe/LogCategory;JLorg/apache/thrift/TBase;)V
    .locals 4

    .prologue
    .line 857
    const-string/jumbo v0, "thrift_logging_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 874
    :cond_0
    :goto_0
    return-void

    .line 862
    :cond_1
    :try_start_0
    new-instance v0, Lorg/apache/thrift/c;

    invoke-direct {v0}, Lorg/apache/thrift/c;-><init>()V

    .line 863
    invoke-virtual {v0, p4}, Lorg/apache/thrift/c;->a(Lorg/apache/thrift/TBase;)[B

    move-result-object v0

    .line 864
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/library/scribe/ScribeService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "LOG_THRIFT"

    .line 865
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "scribe_category"

    .line 866
    invoke-virtual {p1}, Lcom/twitter/library/scribe/LogCategory;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    .line 867
    invoke-virtual {v1, v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "thrift_log"

    .line 868
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    .line 864
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Lorg/apache/thrift/TException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 869
    :catch_0
    move-exception v0

    .line 870
    const-string/jumbo v1, "thrift_logging_crash_report_enabled"

    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 871
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 8

    .prologue
    .line 940
    .line 941
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v6

    move-object v0, p0

    move-wide v1, p4

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    .line 940
    invoke-static/range {v0 .. v7}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;JLjava/lang/String;ILjava/lang/String;J)V

    .line 944
    invoke-static {p0, p4, p5}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;J)V

    .line 945
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 917
    const-string/jumbo v0, "ScribeService"

    const-string/jumbo v1, "Error"

    invoke-static {v0, v1, p1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 919
    const-string/jumbo v0, "scribe_error_sample_size"

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/twitter/library/scribe/ScribeService;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 920
    invoke-static {p0}, Lcom/twitter/library/network/ab;->a(Landroid/content/Context;)Lcom/twitter/library/network/ab;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/network/ab;->c:Lcom/twitter/library/network/ae;

    .line 921
    invoke-virtual {v0}, Lcom/twitter/library/network/ae;->toString()Ljava/lang/String;

    move-result-object v0

    .line 922
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-wide/16 v2, 0x0

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x2

    const/4 v3, 0x0

    .line 923
    invoke-virtual {v1, p1, v2, v3, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/Throwable;ILjava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 925
    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/analytics/model/ScribeLog;Z)V

    .line 927
    :cond_0
    return-void
.end method

.method public static a(Lcom/twitter/metrics/c;)V
    .locals 1

    .prologue
    .line 305
    sput-object p0, Lcom/twitter/library/scribe/ScribeService;->f:Lcom/twitter/metrics/c;

    .line 306
    const-class v0, Lcom/twitter/library/scribe/ScribeService;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 307
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 961
    invoke-virtual {p0}, Lcom/twitter/library/scribe/ScribeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 963
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 964
    invoke-static {}, Lcom/twitter/library/scribe/ScribeService;->b()I

    move-result v1

    .line 965
    if-eqz p1, :cond_1

    .line 966
    if-eqz v1, :cond_0

    .line 967
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "log_failure_cnt"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 974
    :cond_0
    :goto_0
    return-void

    .line 970
    :cond_1
    const/4 v2, 0x5

    if-ge v1, v2, :cond_0

    .line 971
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "log_failure_cnt"

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method public static a(Lcom/twitter/network/HttpOperation;)Z
    .locals 2

    .prologue
    .line 795
    invoke-virtual {p0}, Lcom/twitter/network/HttpOperation;->i()Ljava/net/URI;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/ac;->a(Ljava/net/URI;)Ljava/lang/String;

    move-result-object v0

    .line 796
    invoke-static {}, Lcom/twitter/library/scribe/f;->a()Lcom/twitter/library/scribe/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/scribe/f;->c()Ljava/util/Set;

    move-result-object v1

    .line 797
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 804
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;I)Z
    .locals 3

    .prologue
    const/16 v2, 0x2710

    .line 815
    .line 817
    invoke-static {p0, p1}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v0

    .line 815
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 818
    sget-object v1, Lcom/twitter/util/y;->a:Ljava/security/SecureRandom;

    invoke-virtual {v1, v2}, Ljava/security/SecureRandom;->nextInt(I)I

    move-result v1

    .line 819
    if-ge v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b()I
    .locals 3

    .prologue
    .line 977
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->b()Lcqs;

    move-result-object v0

    const-string/jumbo v1, "log_failure_cnt"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcqs;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 269
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/library/scribe/ScribeService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "FLUSH_IMMEDIATELY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 270
    return-void
.end method

.method public static c(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 725
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 726
    const-string/jumbo v1, "log_last_flush_request"

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "log_failure_cnt"

    const/4 v2, 0x0

    .line 727
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 728
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 729
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;[BJ)V
    .locals 3

    .prologue
    .line 780
    if-eqz p2, :cond_1

    .line 781
    invoke-static {}, Lcqj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 782
    const-string/jumbo v0, "ScribeService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "thrift log: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " bytes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 784
    :cond_0
    invoke-static {p3, p4}, Lcom/twitter/library/scribe/ScribeDatabaseHelper;->a(J)Lcom/twitter/library/scribe/ScribeDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/scribe/ScribeDatabaseHelper;->a(Ljava/lang/String;[B)J

    .line 785
    invoke-static {p0}, Lcom/twitter/library/scribe/d;->a(Landroid/content/Context;)Lcom/twitter/library/scribe/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/scribe/d;->a()V

    .line 787
    :cond_1
    return-void
.end method

.method public a([BJ)V
    .locals 4

    .prologue
    .line 767
    if-eqz p1, :cond_1

    .line 768
    invoke-static {}, Lcqj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 770
    :try_start_0
    const-string/jumbo v0, "ScribeService"

    new-instance v1, Lorg/json/JSONObject;

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p1}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 774
    :cond_0
    :goto_0
    invoke-static {p2, p3}, Lcom/twitter/library/scribe/ScribeDatabaseHelper;->a(J)Lcom/twitter/library/scribe/ScribeDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/library/scribe/ScribeDatabaseHelper;->a([B)J

    .line 775
    invoke-static {p0}, Lcom/twitter/library/scribe/d;->a(Landroid/content/Context;)Lcom/twitter/library/scribe/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/scribe/d;->a()V

    .line 777
    :cond_1
    return-void

    .line 771
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 497
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 472
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 473
    const-string/jumbo v0, "debug_prefs"

    invoke-virtual {p0, v0, v4}, Lcom/twitter/library/scribe/ScribeService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 475
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->p()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "scribe_endpoint_enabled"

    .line 476
    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 477
    const-string/jumbo v1, "scribe_endpoint_url"

    const-string/jumbo v2, "https://twitter.com/scribe"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/scribe/ScribeService;->g:Ljava/lang/String;

    .line 484
    :goto_0
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "scribe_thrift_endpoint_enabled"

    .line 485
    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 486
    const-string/jumbo v1, "scribe_thrift_endpoint_url"

    const-string/jumbo v2, "https://api.twitter.com/1.1/jot/t"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeService;->h:Ljava/lang/String;

    .line 492
    :goto_1
    invoke-static {}, Lcom/twitter/library/scribe/f;->a()Lcom/twitter/library/scribe/f;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/library/scribe/f;->a(Landroid/content/Context;)V

    .line 493
    return-void

    .line 480
    :cond_0
    const-string/jumbo v1, "config"

    invoke-virtual {p0, v1, v4}, Lcom/twitter/library/scribe/ScribeService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "scribe_url"

    const-string/jumbo v3, "https://twitter.com/scribe"

    .line 481
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/scribe/ScribeService;->g:Ljava/lang/String;

    goto :goto_0

    .line 489
    :cond_1
    const-string/jumbo v0, "https://api.twitter.com/1.1/jot/t"

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeService;->h:Ljava/lang/String;

    goto :goto_1
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 18

    .prologue
    .line 502
    if-eqz p1, :cond_0

    invoke-static/range {p1 .. p1}, Lcom/twitter/util/v;->a(Landroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 719
    :cond_0
    :goto_0
    return-void

    .line 505
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 506
    sget-object v3, Lcom/twitter/library/scribe/ScribeService;->a:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 507
    if-nez v2, :cond_2

    const/4 v2, 0x0

    .line 509
    :goto_1
    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 511
    :pswitch_1
    const-string/jumbo v2, "log"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/model/ScribeLog;

    .line 512
    if-eqz v2, :cond_0

    .line 513
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 514
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/model/ScribeLog;->a(Ljava/io/OutputStream;)V

    .line 515
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v2}, Lcom/twitter/analytics/model/ScribeLog;->d()Lcnz;

    move-result-object v2

    invoke-virtual {v2}, Lcnz;->b()J

    move-result-wide v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Lcom/twitter/library/scribe/ScribeService;->a([BJ)V

    goto :goto_0

    .line 507
    :cond_2
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_1

    .line 520
    :pswitch_2
    const-string/jumbo v2, "thrift_log"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v2

    .line 521
    const-string/jumbo v3, "owner_id"

    const-wide/16 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 522
    const-string/jumbo v3, "scribe_category"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 523
    if-eqz v2, :cond_0

    .line 524
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2, v4, v5}, Lcom/twitter/library/scribe/ScribeService;->a(Ljava/lang/String;[BJ)V

    goto :goto_0

    .line 529
    :pswitch_3
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v2

    invoke-virtual {v2}, Lcrr;->d()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 531
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v2

    invoke-virtual {v2}, Lakn;->c()Ljava/util/List;

    move-result-object v4

    .line 532
    const/4 v3, 0x1

    .line 533
    const/4 v2, 0x1

    .line 535
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    move v11, v2

    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_19

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lakm;

    .line 536
    invoke-static {v2}, Lcom/twitter/library/util/b;->c(Lakm;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v4

    .line 537
    if-eqz v4, :cond_8

    .line 538
    invoke-static {v2}, Lcom/twitter/library/util/b;->a(Lakm;)Lcom/twitter/model/account/UserSettings;

    move-result-object v5

    .line 540
    if-eqz v5, :cond_7

    .line 541
    iget-object v6, v5, Lcom/twitter/model/account/UserSettings;->h:Ljava/lang/String;

    .line 545
    :goto_3
    iget-wide v4, v4, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 547
    invoke-static {v2}, Lcom/twitter/library/util/b;->b(Lakm;)Lcom/twitter/model/account/OAuthToken;

    move-result-object v7

    .line 548
    if-eqz v3, :cond_3

    .line 549
    new-instance v2, Lcom/twitter/library/scribe/ScribeServiceJsonLogFlusher;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/scribe/ScribeService;->g:Ljava/lang/String;

    sget-object v9, Lcom/twitter/library/scribe/ScribeService;->f:Lcom/twitter/metrics/c;

    sget-object v10, Lcom/twitter/library/scribe/ScribeService;->e:Lcom/twitter/library/scribe/ScribeService$c;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v10}, Lcom/twitter/library/scribe/ScribeServiceJsonLogFlusher;-><init>(Landroid/content/Context;JLjava/lang/String;Lcom/twitter/model/account/OAuthToken;Ljava/lang/String;Lcom/twitter/metrics/c;Lcom/twitter/library/scribe/ScribeService$c;)V

    .line 550
    invoke-virtual {v2}, Lcom/twitter/library/scribe/ScribeServiceJsonLogFlusher;->a()Z

    move-result v3

    .line 552
    :cond_3
    if-eqz v11, :cond_18

    .line 553
    new-instance v8, Lcom/twitter/library/scribe/l;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/twitter/library/scribe/ScribeService;->h:Ljava/lang/String;

    sget-object v14, Lcom/twitter/library/scribe/ScribeService;->f:Lcom/twitter/metrics/c;

    sget-object v15, Lcom/twitter/library/scribe/ScribeService;->e:Lcom/twitter/library/scribe/ScribeService$c;

    move-object/from16 v9, p0

    move-wide v10, v4

    move-object v12, v7

    invoke-direct/range {v8 .. v15}, Lcom/twitter/library/scribe/l;-><init>(Landroid/content/Context;JLcom/twitter/model/account/OAuthToken;Ljava/lang/String;Lcom/twitter/metrics/c;Lcom/twitter/library/scribe/ScribeService$c;)V

    .line 554
    invoke-virtual {v8}, Lcom/twitter/library/scribe/l;->a()Z

    move-result v2

    .line 556
    :goto_4
    if-nez v3, :cond_9

    if-nez v2, :cond_9

    move v11, v2

    move v2, v3

    .line 562
    :goto_5
    if-eqz v2, :cond_17

    .line 564
    new-instance v2, Lcom/twitter/library/scribe/ScribeServiceJsonLogFlusher;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/scribe/ScribeService;->g:Ljava/lang/String;

    sget-object v9, Lcom/twitter/library/scribe/ScribeService;->f:Lcom/twitter/metrics/c;

    sget-object v10, Lcom/twitter/library/scribe/ScribeService;->e:Lcom/twitter/library/scribe/ScribeService$c;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v10}, Lcom/twitter/library/scribe/ScribeServiceJsonLogFlusher;-><init>(Landroid/content/Context;JLjava/lang/String;Lcom/twitter/model/account/OAuthToken;Ljava/lang/String;Lcom/twitter/metrics/c;Lcom/twitter/library/scribe/ScribeService$c;)V

    .line 565
    invoke-virtual {v2}, Lcom/twitter/library/scribe/ScribeServiceJsonLogFlusher;->a()Z

    move-result v2

    move v10, v2

    .line 567
    :goto_6
    if-eqz v11, :cond_16

    .line 568
    new-instance v2, Lcom/twitter/library/scribe/l;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/scribe/ScribeService;->h:Ljava/lang/String;

    sget-object v8, Lcom/twitter/library/scribe/ScribeService;->f:Lcom/twitter/metrics/c;

    sget-object v9, Lcom/twitter/library/scribe/ScribeService;->e:Lcom/twitter/library/scribe/ScribeService$c;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v9}, Lcom/twitter/library/scribe/l;-><init>(Landroid/content/Context;JLcom/twitter/model/account/OAuthToken;Ljava/lang/String;Lcom/twitter/metrics/c;Lcom/twitter/library/scribe/ScribeService$c;)V

    .line 569
    invoke-virtual {v2}, Lcom/twitter/library/scribe/l;->a()Z

    move-result v2

    move v3, v2

    .line 571
    :goto_7
    if-eqz v10, :cond_a

    if-eqz v3, :cond_a

    const/4 v2, 0x1

    :goto_8
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/twitter/library/scribe/ScribeService;->a(Z)V

    .line 573
    if-eqz v10, :cond_4

    if-nez v3, :cond_5

    .line 574
    :cond_4
    invoke-static {}, Lcom/twitter/library/scribe/j;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 575
    invoke-static {}, Lcom/twitter/library/scribe/ScribeService;->b()I

    move-result v2

    const/4 v3, 0x5

    if-ge v2, v3, :cond_5

    .line 576
    invoke-static/range {p0 .. p0}, Lcom/twitter/library/scribe/d;->a(Landroid/content/Context;)Lcom/twitter/library/scribe/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/scribe/d;->a()V

    .line 581
    :cond_5
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v2

    invoke-virtual {v2}, Lcof;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 582
    const-string/jumbo v2, "flush_request_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 583
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/scribe/ScribeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 584
    new-instance v4, Landroid/content/Intent;

    const-string/jumbo v5, "SCRIBE_FLUSH_COMPLETED"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 585
    if-eqz v2, :cond_6

    .line 586
    const-string/jumbo v5, "flush_request_id"

    invoke-virtual {v4, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 588
    :cond_6
    invoke-static {v3}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto/16 :goto_0

    .line 543
    :cond_7
    const/4 v6, 0x0

    goto/16 :goto_3

    :cond_8
    move v2, v11

    :cond_9
    move v11, v2

    .line 560
    goto/16 :goto_2

    .line 571
    :cond_a
    const/4 v2, 0x0

    goto :goto_8

    .line 594
    :pswitch_4
    invoke-static/range {p0 .. p0}, Lcom/twitter/library/scribe/d;->a(Landroid/content/Context;)Lcom/twitter/library/scribe/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/scribe/d;->b()V

    goto/16 :goto_0

    .line 598
    :pswitch_5
    const-string/jumbo v2, "user_id"

    const-wide/16 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    .line 599
    const-string/jumbo v2, "exp_request_time"

    const-wide/16 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    .line 601
    sget-object v2, Lcom/twitter/library/scribe/ScribeService;->c:Ljava/util/Map;

    invoke-interface {v2, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 602
    sget-object v3, Lcom/twitter/library/scribe/ScribeService;->b:Ljava/util/Map;

    .line 603
    invoke-interface {v3, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    .line 608
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v4

    invoke-virtual {v4}, Lcof;->p()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 609
    invoke-static {}, Lcom/twitter/library/scribe/f;->a()Lcom/twitter/library/scribe/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/scribe/f;->e()J

    move-result-wide v6

    const-wide/16 v4, 0x0

    cmp-long v4, v6, v4

    if-lez v4, :cond_e

    .line 611
    const-wide/32 v4, 0x36ee80

    move-wide v8, v4

    move-wide v10, v6

    .line 617
    :goto_9
    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 618
    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    cmp-long v2, v4, v10

    if-lez v2, :cond_0

    .line 620
    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 621
    :cond_b
    :goto_a
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 622
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 623
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/scribe/ScribeService$b;

    .line 624
    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v3, v2, Lcom/twitter/library/scribe/ScribeService$b;->e:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 625
    invoke-static {v2}, Lcom/twitter/library/scribe/ScribeService$b;->a(Lcom/twitter/library/scribe/ScribeService$b;)Z

    move-result v3

    if-nez v3, :cond_c

    cmp-long v3, v4, v10

    if-lez v3, :cond_f

    :cond_c
    const/4 v3, 0x1

    .line 628
    :goto_b
    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-object v15, v2, Lcom/twitter/library/scribe/ScribeService$b;->d:Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    sub-long v6, v6, v16

    cmp-long v6, v6, v10

    if-lez v6, :cond_d

    cmp-long v4, v4, v8

    if-gtz v4, :cond_10

    :cond_d
    const/4 v4, 0x1

    .line 632
    :goto_c
    if-eqz v3, :cond_11

    if-eqz v4, :cond_11

    .line 633
    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/twitter/library/scribe/ScribeService$b;->a(J)V

    .line 634
    iget-object v15, v2, Lcom/twitter/library/scribe/ScribeService$b;->a:Ljava/lang/String;

    .line 635
    iget v0, v2, Lcom/twitter/library/scribe/ScribeService$b;->b:I

    move/from16 v16, v0

    .line 636
    iget-object v0, v2, Lcom/twitter/library/scribe/ScribeService$b;->c:Ljava/lang/String;

    move-object/from16 v17, v0

    .line 639
    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(J)Lcom/twitter/analytics/feature/model/ClientEventLog$a;

    move-result-object v2

    const-string/jumbo v3, "ddg"

    .line 640
    invoke-virtual {v15}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string/jumbo v7, "experiment"

    invoke-virtual/range {v2 .. v7}, Lcom/twitter/analytics/feature/model/ClientEventLog$a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog$a;

    move-result-object v2

    .line 641
    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v2, v15, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog$a;->a(Ljava/lang/String;ILjava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog$a;

    move-result-object v2

    .line 642
    invoke-virtual {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog$a;->a()Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v2

    .line 644
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 645
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/io/OutputStream;)V

    .line 646
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->d()Lcnz;

    move-result-object v2

    invoke-virtual {v2}, Lcnz;->b()J

    move-result-wide v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Lcom/twitter/library/scribe/ScribeService;->a([BJ)V

    .line 649
    const-string/jumbo v2, "thrift_logging_ddg_double_write_enabled"

    invoke-static {v2}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 651
    new-instance v2, Lcom/twitter/experiments/client/thriftandroid/DdgImpression$a;

    invoke-direct {v2}, Lcom/twitter/experiments/client/thriftandroid/DdgImpression$a;-><init>()V

    sget-object v3, Lcom/twitter/experiments/client/thriftandroid/DdgImpression;->b:Lcom/twitter/experiments/client/thriftandroid/DdgImpression$_Fields;

    .line 653
    invoke-static {}, Lcom/twitter/library/scribe/ScribeService;->a()Lcom/twitter/common_header/thriftandroid/VersionedCommonHeader;

    move-result-object v4

    .line 652
    invoke-virtual {v2, v3, v4}, Lcom/twitter/experiments/client/thriftandroid/DdgImpression$a;->a(Lcom/twitter/experiments/client/thriftandroid/DdgImpression$_Fields;Ljava/lang/Object;)Lcom/twitter/experiments/client/thriftandroid/DdgImpression$a;

    move-result-object v2

    sget-object v3, Lcom/twitter/experiments/client/thriftandroid/DdgImpression;->c:Lcom/twitter/experiments/client/thriftandroid/DdgImpression$_Fields;

    .line 654
    invoke-virtual {v2, v3, v15}, Lcom/twitter/experiments/client/thriftandroid/DdgImpression$a;->a(Lcom/twitter/experiments/client/thriftandroid/DdgImpression$_Fields;Ljava/lang/Object;)Lcom/twitter/experiments/client/thriftandroid/DdgImpression$a;

    move-result-object v2

    sget-object v3, Lcom/twitter/experiments/client/thriftandroid/DdgImpression;->d:Lcom/twitter/experiments/client/thriftandroid/DdgImpression$_Fields;

    .line 655
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/twitter/experiments/client/thriftandroid/DdgImpression$a;->a(Lcom/twitter/experiments/client/thriftandroid/DdgImpression$_Fields;Ljava/lang/Object;)Lcom/twitter/experiments/client/thriftandroid/DdgImpression$a;

    move-result-object v2

    sget-object v3, Lcom/twitter/experiments/client/thriftandroid/DdgImpression;->e:Lcom/twitter/experiments/client/thriftandroid/DdgImpression$_Fields;

    .line 656
    move-object/from16 v0, v17

    invoke-virtual {v2, v3, v0}, Lcom/twitter/experiments/client/thriftandroid/DdgImpression$a;->a(Lcom/twitter/experiments/client/thriftandroid/DdgImpression$_Fields;Ljava/lang/Object;)Lcom/twitter/experiments/client/thriftandroid/DdgImpression$a;

    move-result-object v2

    .line 657
    invoke-virtual {v2}, Lcom/twitter/experiments/client/thriftandroid/DdgImpression$a;->a()Lcom/twitter/experiments/client/thriftandroid/DdgImpression;

    move-result-object v2

    .line 658
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/scribe/ScribeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, Lcom/twitter/library/scribe/LogCategory;->c:Lcom/twitter/library/scribe/LogCategory;

    .line 660
    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 658
    invoke-static {v3, v4, v6, v7, v2}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/LogCategory;JLorg/apache/thrift/TBase;)V

    goto/16 :goto_a

    .line 613
    :cond_e
    const-wide/32 v6, 0x19a280

    .line 614
    const-wide/32 v4, 0x334500

    move-wide v8, v4

    move-wide v10, v6

    goto/16 :goto_9

    .line 625
    :cond_f
    const/4 v3, 0x0

    goto/16 :goto_b

    .line 628
    :cond_10
    const/4 v4, 0x0

    goto/16 :goto_c

    .line 662
    :cond_11
    if-nez v4, :cond_b

    .line 663
    invoke-interface {v14}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_a

    .line 666
    :cond_12
    sget-object v2, Lcom/twitter/library/scribe/ScribeService;->c:Ljava/util/Map;

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/twitter/library/scribe/ScribeService;->a(JJ)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v12, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 671
    :pswitch_6
    const-string/jumbo v2, "user_id"

    const-wide/16 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 672
    const-string/jumbo v2, "exp_request_time"

    const-wide/16 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 673
    const-string/jumbo v2, "exp_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 674
    const-string/jumbo v2, "exp_version"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 675
    const-string/jumbo v2, "exp_bucket"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 678
    sget-object v2, Lcom/twitter/library/scribe/ScribeService;->b:Ljava/util/Map;

    .line 679
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    .line 681
    if-nez v2, :cond_13

    .line 682
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 683
    sget-object v8, Lcom/twitter/library/scribe/ScribeService;->b:Ljava/util/Map;

    invoke-interface {v8, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 686
    :cond_13
    invoke-virtual {v2, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_14

    .line 687
    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/scribe/ScribeService$b;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v2, v5, v7, v8, v9}, Lcom/twitter/library/scribe/ScribeService$b;->a(ILjava/lang/String;J)V

    .line 692
    :goto_d
    sget-object v2, Lcom/twitter/library/scribe/ScribeService;->c:Ljava/util/Map;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v6, v7, v4, v5}, Lcom/twitter/library/scribe/ScribeService;->a(JJ)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 689
    :cond_14
    new-instance v8, Lcom/twitter/library/scribe/ScribeService$b;

    .line 690
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct {v8, v5, v6, v7, v4}, Lcom/twitter/library/scribe/ScribeService$b;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Long;)V

    .line 689
    invoke-virtual {v2, v5, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_d

    .line 696
    :pswitch_7
    const-string/jumbo v2, "debug_prefs"

    const/4 v3, 0x0

    .line 697
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/scribe/ScribeService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 698
    const-string/jumbo v3, "endpoint_url"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 699
    if-eqz v3, :cond_15

    .line 700
    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/library/scribe/ScribeService;->g:Ljava/lang/String;

    .line 701
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v4, "scribe_endpoint_enabled"

    const/4 v5, 0x1

    .line 702
    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v4, "scribe_endpoint_url"

    .line 703
    invoke-interface {v2, v4, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 704
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    .line 706
    :cond_15
    const-string/jumbo v3, "https://twitter.com/scribe"

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/library/scribe/ScribeService;->g:Ljava/lang/String;

    .line 707
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "scribe_endpoint_enabled"

    const/4 v4, 0x0

    .line 708
    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "scribe_endpoint_url"

    const-string/jumbo v4, "https://twitter.com/scribe"

    .line 709
    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 711
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    :cond_16
    move v3, v11

    goto/16 :goto_7

    :cond_17
    move v10, v2

    goto/16 :goto_6

    :cond_18
    move v2, v11

    goto/16 :goto_4

    :cond_19
    move v2, v3

    goto/16 :goto_5

    .line 509
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method
