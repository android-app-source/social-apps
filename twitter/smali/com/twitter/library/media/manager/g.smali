.class public Lcom/twitter/library/media/manager/g;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/ComponentCallbacks;


# static fields
.field private static final a:Landroid/os/Looper;

.field private static final b:Ljava/lang/Object;

.field private static volatile c:Lcom/twitter/library/media/manager/g;


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Lcom/twitter/util/collection/ReferenceMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/ReferenceMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/media/manager/e;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcpy;

.field private final g:Lcom/twitter/library/media/manager/e;

.field private final h:Lcom/twitter/library/media/manager/e;

.field private final i:Lcom/twitter/library/media/manager/m;

.field private final j:Lcom/twitter/library/media/manager/e;

.field private final k:Lcom/twitter/library/media/manager/e;

.field private final l:Lcom/twitter/library/media/manager/e;

.field private final m:Lcom/twitter/library/media/manager/a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 96
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/twitter/library/media/manager/g;->b:Ljava/lang/Object;

    .line 113
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "CoordinationThread"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 115
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 116
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/media/manager/g;->a:Landroid/os/Looper;

    .line 117
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 9

    .prologue
    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-static {}, Lcom/twitter/util/collection/ReferenceMap;->a()Lcom/twitter/util/collection/ReferenceMap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/media/manager/g;->e:Lcom/twitter/util/collection/ReferenceMap;

    .line 149
    iput-object p1, p0, Lcom/twitter/library/media/manager/g;->d:Landroid/content/Context;

    .line 151
    invoke-static {p1}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;)Lcom/twitter/util/math/Size;

    move-result-object v0

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-virtual {v0, v1}, Lcom/twitter/util/math/Size;->a(F)Lcom/twitter/util/math/Size;

    move-result-object v7

    .line 153
    invoke-static {p1}, Lcom/twitter/util/d;->a(Landroid/content/Context;)I

    move-result v0

    div-int/lit8 v0, v0, 0x10

    const/high16 v1, 0x200000

    const/high16 v2, 0x1000000

    invoke-static {v0, v1, v2}, Lcom/twitter/util/math/b;->a(III)I

    move-result v8

    .line 155
    new-instance v0, Lcpy;

    const-string/jumbo v2, "photos"

    const/4 v3, 0x2

    const/high16 v4, 0x1900000

    const/high16 v5, 0x500000

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcpy;-><init>(Landroid/content/Context;Ljava/lang/String;III)V

    iput-object v0, p0, Lcom/twitter/library/media/manager/g;->f:Lcpy;

    .line 157
    new-instance v0, Lcom/twitter/library/media/manager/e$a;

    invoke-direct {v0}, Lcom/twitter/library/media/manager/e$a;-><init>()V

    invoke-direct {p0, v0}, Lcom/twitter/library/media/manager/g;->a(Lcom/twitter/library/media/manager/k$a;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    const-string/jumbo v1, "photo"

    .line 158
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/e$a;->a(Ljava/lang/String;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    .line 159
    invoke-virtual {v0, v7}, Lcom/twitter/library/media/manager/e$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/library/media/manager/e$a;

    move-result-object v0

    .line 160
    invoke-virtual {v0, v8}, Lcom/twitter/library/media/manager/e$a;->a(I)Lcom/twitter/library/media/manager/e$a;

    move-result-object v0

    new-instance v1, Lcom/twitter/util/collection/e;

    const/4 v2, 0x0

    sget-object v3, Lcom/twitter/media/util/a;->b:Lcom/twitter/util/collection/e$c;

    invoke-direct {v1, v2, v3}, Lcom/twitter/util/collection/e;-><init>(ILcom/twitter/util/collection/e$c;)V

    .line 161
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/e$a;->a(Lcom/twitter/util/collection/g;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    iget-object v1, p0, Lcom/twitter/library/media/manager/g;->f:Lcpy;

    .line 162
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/e$a;->a(Lcpy;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    .line 163
    invoke-virtual {v0}, Lcom/twitter/library/media/manager/e$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e;

    iput-object v0, p0, Lcom/twitter/library/media/manager/g;->g:Lcom/twitter/library/media/manager/e;

    .line 164
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->e:Lcom/twitter/util/collection/ReferenceMap;

    const-string/jumbo v1, "photo"

    iget-object v2, p0, Lcom/twitter/library/media/manager/g;->g:Lcom/twitter/library/media/manager/e;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/ReferenceMap;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    new-instance v0, Lcom/twitter/library/media/manager/e$a;

    invoke-direct {v0}, Lcom/twitter/library/media/manager/e$a;-><init>()V

    invoke-direct {p0, v0}, Lcom/twitter/library/media/manager/g;->a(Lcom/twitter/library/media/manager/k$a;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    const-string/jumbo v1, "user"

    .line 167
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/e$a;->a(Ljava/lang/String;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    new-instance v1, Lcom/twitter/util/collection/e;

    const/high16 v2, 0x200000

    sget-object v3, Lcom/twitter/media/util/a;->b:Lcom/twitter/util/collection/e$c;

    invoke-direct {v1, v2, v3}, Lcom/twitter/util/collection/e;-><init>(ILcom/twitter/util/collection/e$c;)V

    .line 168
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/e$a;->a(Lcom/twitter/util/collection/g;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    iget-object v1, p0, Lcom/twitter/library/media/manager/g;->f:Lcpy;

    .line 170
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/e$a;->a(Lcpy;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/twitter/library/media/manager/e$a;

    new-instance v0, Lcpy;

    iget-object v1, p0, Lcom/twitter/library/media/manager/g;->d:Landroid/content/Context;

    const-string/jumbo v2, "users"

    const/4 v3, 0x1

    const/high16 v4, 0xa00000

    const/high16 v5, 0x200000

    invoke-direct/range {v0 .. v5}, Lcpy;-><init>(Landroid/content/Context;Ljava/lang/String;III)V

    .line 171
    invoke-virtual {v6, v0}, Lcom/twitter/library/media/manager/e$a;->b(Lcpy;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    .line 174
    invoke-virtual {v0}, Lcom/twitter/library/media/manager/e$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e;

    iput-object v0, p0, Lcom/twitter/library/media/manager/g;->h:Lcom/twitter/library/media/manager/e;

    .line 176
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->e:Lcom/twitter/util/collection/ReferenceMap;

    const-string/jumbo v1, "user"

    iget-object v2, p0, Lcom/twitter/library/media/manager/g;->h:Lcom/twitter/library/media/manager/e;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/ReferenceMap;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    new-instance v0, Lcom/twitter/library/media/manager/m$a;

    invoke-direct {v0}, Lcom/twitter/library/media/manager/m$a;-><init>()V

    invoke-direct {p0, v0}, Lcom/twitter/library/media/manager/g;->a(Lcom/twitter/library/media/manager/k$a;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/m$a;

    const-string/jumbo v1, "video"

    .line 179
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/m$a;->a(Ljava/lang/String;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/m$a;

    new-instance v1, Lcom/twitter/util/collection/e;

    const/4 v2, 0x0

    sget-object v3, Lcom/twitter/media/model/MediaFile;->c:Lcom/twitter/util/collection/e$c;

    invoke-direct {v1, v2, v3}, Lcom/twitter/util/collection/e;-><init>(ILcom/twitter/util/collection/e$c;)V

    .line 180
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/m$a;->a(Lcom/twitter/util/collection/g;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/twitter/library/media/manager/m$a;

    new-instance v0, Lcpy;

    const-string/jumbo v2, "videos"

    const/4 v3, 0x1

    const/high16 v4, 0x6400000

    const/high16 v5, 0x3200000

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcpy;-><init>(Landroid/content/Context;Ljava/lang/String;III)V

    .line 181
    invoke-virtual {v6, v0}, Lcom/twitter/library/media/manager/m$a;->a(Lcpy;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/m$a;

    .line 184
    invoke-virtual {v0}, Lcom/twitter/library/media/manager/m$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/m;

    iput-object v0, p0, Lcom/twitter/library/media/manager/g;->i:Lcom/twitter/library/media/manager/m;

    .line 186
    new-instance v0, Lcom/twitter/library/media/manager/e$a;

    invoke-direct {v0}, Lcom/twitter/library/media/manager/e$a;-><init>()V

    invoke-direct {p0, v0}, Lcom/twitter/library/media/manager/g;->a(Lcom/twitter/library/media/manager/k$a;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    const-string/jumbo v1, "hashflags"

    .line 187
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/e$a;->a(Ljava/lang/String;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    new-instance v1, Lcom/twitter/util/collection/e;

    const v2, 0x64000

    sget-object v3, Lcom/twitter/media/util/a;->b:Lcom/twitter/util/collection/e$c;

    invoke-direct {v1, v2, v3}, Lcom/twitter/util/collection/e;-><init>(ILcom/twitter/util/collection/e$c;)V

    .line 188
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/e$a;->a(Lcom/twitter/util/collection/g;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/twitter/library/media/manager/e$a;

    new-instance v0, Lcpy;

    const-string/jumbo v2, "hashflags"

    const/4 v3, 0x1

    const/high16 v4, 0x200000

    const/high16 v5, 0x200000

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcpy;-><init>(Landroid/content/Context;Ljava/lang/String;III)V

    .line 190
    invoke-virtual {v6, v0}, Lcom/twitter/library/media/manager/e$a;->a(Lcpy;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    .line 193
    invoke-virtual {v0}, Lcom/twitter/library/media/manager/e$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e;

    iput-object v0, p0, Lcom/twitter/library/media/manager/g;->j:Lcom/twitter/library/media/manager/e;

    .line 194
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->e:Lcom/twitter/util/collection/ReferenceMap;

    const-string/jumbo v1, "hashflags"

    iget-object v2, p0, Lcom/twitter/library/media/manager/g;->j:Lcom/twitter/library/media/manager/e;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/ReferenceMap;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    new-instance v0, Lcom/twitter/library/media/manager/e$a;

    invoke-direct {v0}, Lcom/twitter/library/media/manager/e$a;-><init>()V

    invoke-direct {p0, v0}, Lcom/twitter/library/media/manager/g;->a(Lcom/twitter/library/media/manager/k$a;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    const-string/jumbo v1, "gallery"

    .line 197
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/e$a;->a(Ljava/lang/String;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    .line 198
    invoke-virtual {v0, v7}, Lcom/twitter/library/media/manager/e$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/library/media/manager/e$a;

    move-result-object v0

    .line 199
    invoke-virtual {v0, v8}, Lcom/twitter/library/media/manager/e$a;->a(I)Lcom/twitter/library/media/manager/e$a;

    move-result-object v0

    new-instance v1, Lcom/twitter/util/collection/e;

    const/high16 v2, 0x300000

    sget-object v3, Lcom/twitter/media/util/a;->b:Lcom/twitter/util/collection/e$c;

    invoke-direct {v1, v2, v3}, Lcom/twitter/util/collection/e;-><init>(ILcom/twitter/util/collection/e$c;)V

    .line 200
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/e$a;->a(Lcom/twitter/util/collection/g;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    iget-object v1, p0, Lcom/twitter/library/media/manager/g;->f:Lcpy;

    .line 202
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/e$a;->a(Lcpy;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/twitter/library/media/manager/e$a;

    new-instance v0, Lcpy;

    const-string/jumbo v2, "gallery"

    const/4 v3, 0x1

    const/high16 v4, 0x500000

    const/high16 v5, 0x300000

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcpy;-><init>(Landroid/content/Context;Ljava/lang/String;III)V

    .line 203
    invoke-virtual {v6, v0}, Lcom/twitter/library/media/manager/e$a;->b(Lcpy;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    .line 206
    invoke-virtual {v0}, Lcom/twitter/library/media/manager/e$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e;

    iput-object v0, p0, Lcom/twitter/library/media/manager/g;->k:Lcom/twitter/library/media/manager/e;

    .line 207
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->e:Lcom/twitter/util/collection/ReferenceMap;

    const-string/jumbo v1, "gallery"

    iget-object v2, p0, Lcom/twitter/library/media/manager/g;->k:Lcom/twitter/library/media/manager/e;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/ReferenceMap;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    new-instance v0, Lcom/twitter/library/media/manager/a$a;

    invoke-direct {v0}, Lcom/twitter/library/media/manager/a$a;-><init>()V

    invoke-direct {p0, v0}, Lcom/twitter/library/media/manager/g;->a(Lcom/twitter/library/media/manager/k$a;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/a$a;

    const-string/jumbo v1, "gif"

    .line 210
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/a$a;->a(Ljava/lang/String;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/a$a;

    new-instance v1, Lcom/twitter/util/collection/e;

    const/4 v2, 0x0

    new-instance v3, Lcom/twitter/library/media/manager/g$1;

    invoke-direct {v3, p0}, Lcom/twitter/library/media/manager/g$1;-><init>(Lcom/twitter/library/media/manager/g;)V

    invoke-direct {v1, v2, v3}, Lcom/twitter/util/collection/e;-><init>(ILcom/twitter/util/collection/e$c;)V

    .line 211
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/a$a;->a(Lcom/twitter/util/collection/g;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/twitter/library/media/manager/a$a;

    new-instance v0, Lcpy;

    const-string/jumbo v2, "gif_disk"

    const/4 v3, 0x0

    const/high16 v4, 0x1400000

    const/high16 v5, 0xa00000

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcpy;-><init>(Landroid/content/Context;Ljava/lang/String;III)V

    .line 218
    invoke-virtual {v6, v0}, Lcom/twitter/library/media/manager/a$a;->a(Lcpy;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/a$a;

    .line 220
    invoke-virtual {v0}, Lcom/twitter/library/media/manager/a$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/a;

    iput-object v0, p0, Lcom/twitter/library/media/manager/g;->m:Lcom/twitter/library/media/manager/a;

    .line 222
    new-instance v0, Lcom/twitter/library/media/manager/e$a;

    invoke-direct {v0}, Lcom/twitter/library/media/manager/e$a;-><init>()V

    invoke-direct {p0, v0}, Lcom/twitter/library/media/manager/g;->a(Lcom/twitter/library/media/manager/k$a;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    const-string/jumbo v1, "stickers"

    .line 223
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/e$a;->a(Ljava/lang/String;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    .line 224
    invoke-virtual {v0, v7}, Lcom/twitter/library/media/manager/e$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/library/media/manager/e$a;

    move-result-object v0

    .line 225
    invoke-virtual {v0, v8}, Lcom/twitter/library/media/manager/e$a;->a(I)Lcom/twitter/library/media/manager/e$a;

    move-result-object v0

    new-instance v1, Lcom/twitter/util/collection/e;

    const/4 v2, 0x0

    sget-object v3, Lcom/twitter/media/util/a;->b:Lcom/twitter/util/collection/e$c;

    invoke-direct {v1, v2, v3}, Lcom/twitter/util/collection/e;-><init>(ILcom/twitter/util/collection/e$c;)V

    .line 226
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/e$a;->a(Lcom/twitter/util/collection/g;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/twitter/library/media/manager/e$a;

    new-instance v0, Lcpy;

    const-string/jumbo v2, "stickers_disk"

    const/4 v3, 0x0

    const/high16 v4, 0xa00000

    const/high16 v5, 0x500000

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcpy;-><init>(Landroid/content/Context;Ljava/lang/String;III)V

    .line 227
    invoke-virtual {v6, v0}, Lcom/twitter/library/media/manager/e$a;->a(Lcpy;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    .line 229
    invoke-virtual {v0}, Lcom/twitter/library/media/manager/e$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e;

    iput-object v0, p0, Lcom/twitter/library/media/manager/g;->l:Lcom/twitter/library/media/manager/e;

    .line 231
    invoke-direct {p0}, Lcom/twitter/library/media/manager/g;->j()V

    .line 232
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/media/manager/g;)Lcom/twitter/library/media/manager/e;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->h:Lcom/twitter/library/media/manager/e;

    return-object v0
.end method

.method public static a()Lcom/twitter/library/media/manager/g;
    .locals 1

    .prologue
    .line 121
    sget-object v0, Lcom/twitter/library/media/manager/g;->c:Lcom/twitter/library/media/manager/g;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;
    .locals 3

    .prologue
    .line 126
    sget-object v0, Lcom/twitter/library/media/manager/g;->c:Lcom/twitter/library/media/manager/g;

    if-nez v0, :cond_1

    .line 127
    sget-object v1, Lcom/twitter/library/media/manager/g;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 128
    :try_start_0
    sget-object v0, Lcom/twitter/library/media/manager/g;->c:Lcom/twitter/library/media/manager/g;

    if-nez v0, :cond_0

    .line 129
    new-instance v0, Lcom/twitter/library/media/manager/g;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/library/media/manager/g;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/library/media/manager/g;->c:Lcom/twitter/library/media/manager/g;

    .line 130
    const-class v0, Lcom/twitter/library/media/manager/g;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 132
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    :cond_1
    sget-object v0, Lcom/twitter/library/media/manager/g;->c:Lcom/twitter/library/media/manager/g;

    return-object v0

    .line 132
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(Lcom/twitter/library/media/manager/k$a;)Lcom/twitter/library/media/manager/k$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<B:",
            "Lcom/twitter/library/media/manager/k$a",
            "<**TB;>;>(TB;)TB;"
        }
    .end annotation

    .prologue
    .line 417
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->d:Landroid/content/Context;

    .line 418
    invoke-virtual {p1, v0}, Lcom/twitter/library/media/manager/k$a;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/media/manager/g;->a:Landroid/os/Looper;

    .line 419
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/k$a;->a(Landroid/os/Looper;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/media/manager/j$a;->i:Lcom/twitter/library/media/manager/i$b;

    .line 420
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/k$a;->a(Lcom/twitter/library/media/manager/i$b;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    .line 417
    return-object v0
.end method

.method private j()V
    .locals 5

    .prologue
    .line 399
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 400
    sget v1, Lazw$e;->mini_user_image_size:I

    .line 401
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sget v2, Lazw$e;->medium_user_image_size:I

    .line 402
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sget v3, Lazw$e;->user_image_size:I

    .line 403
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sget v4, Lazw$e;->large_user_image_size:I

    .line 404
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 400
    invoke-static {v1, v2, v3, v0}, Lcom/twitter/media/manager/UserImageRequest;->a(IIII)V

    .line 405
    return-void
.end method

.method private k()Lcom/twitter/library/media/manager/e;
    .locals 4

    .prologue
    .line 409
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 410
    sget v1, Lazw$e;->media_thumbnail_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 411
    const-string/jumbo v1, "thumbnail"

    .line 413
    invoke-static {v0}, Lcom/twitter/util/math/Size;->a(I)Lcom/twitter/util/math/Size;

    move-result-object v0

    const/high16 v2, 0x100000

    const/high16 v3, 0x200000

    .line 411
    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/twitter/library/media/manager/g;->a(Ljava/lang/String;Lcom/twitter/util/math/Size;II)Lcom/twitter/library/media/manager/e;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/twitter/library/media/manager/e;
    .locals 2

    .prologue
    .line 330
    if-eqz p1, :cond_0

    const-string/jumbo v0, "photo"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->g:Lcom/twitter/library/media/manager/e;

    .line 344
    :goto_0
    return-object v0

    .line 332
    :cond_1
    const-string/jumbo v0, "user"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 333
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->h:Lcom/twitter/library/media/manager/e;

    goto :goto_0

    .line 334
    :cond_2
    const-string/jumbo v0, "thumbnail"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 335
    iget-object v1, p0, Lcom/twitter/library/media/manager/g;->e:Lcom/twitter/util/collection/ReferenceMap;

    monitor-enter v1

    .line 336
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->e:Lcom/twitter/util/collection/ReferenceMap;

    invoke-virtual {v0, p1}, Lcom/twitter/util/collection/ReferenceMap;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e;

    .line 337
    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/twitter/library/media/manager/g;->k()Lcom/twitter/library/media/manager/e;

    move-result-object v0

    :cond_3
    monitor-exit v1

    goto :goto_0

    .line 338
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 339
    :cond_4
    const-string/jumbo v0, "stickers"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 340
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->l:Lcom/twitter/library/media/manager/e;

    goto :goto_0

    .line 342
    :cond_5
    iget-object v1, p0, Lcom/twitter/library/media/manager/g;->e:Lcom/twitter/util/collection/ReferenceMap;

    monitor-enter v1

    .line 343
    :try_start_1
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->e:Lcom/twitter/util/collection/ReferenceMap;

    invoke-virtual {v0, p1}, Lcom/twitter/util/collection/ReferenceMap;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e;

    .line 344
    if-eqz v0, :cond_6

    :goto_1
    monitor-exit v1

    goto :goto_0

    .line 345
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 344
    :cond_6
    :try_start_2
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->g:Lcom/twitter/library/media/manager/e;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Lcom/twitter/util/math/Size;II)Lcom/twitter/library/media/manager/e;
    .locals 4

    .prologue
    .line 357
    iget-object v1, p0, Lcom/twitter/library/media/manager/g;->e:Lcom/twitter/util/collection/ReferenceMap;

    monitor-enter v1

    .line 358
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->e:Lcom/twitter/util/collection/ReferenceMap;

    invoke-virtual {v0, p1}, Lcom/twitter/util/collection/ReferenceMap;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e;

    .line 359
    if-nez v0, :cond_0

    .line 360
    if-ltz p4, :cond_1

    .line 361
    new-instance v0, Lcom/twitter/library/media/manager/e$a;

    invoke-direct {v0}, Lcom/twitter/library/media/manager/e$a;-><init>()V

    invoke-direct {p0, v0}, Lcom/twitter/library/media/manager/g;->a(Lcom/twitter/library/media/manager/k$a;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    .line 362
    invoke-virtual {v0, p1}, Lcom/twitter/library/media/manager/e$a;->a(Ljava/lang/String;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    .line 363
    invoke-virtual {v0, p2}, Lcom/twitter/library/media/manager/e$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/library/media/manager/e$a;

    move-result-object v0

    .line 364
    invoke-virtual {v0, p3}, Lcom/twitter/library/media/manager/e$a;->a(I)Lcom/twitter/library/media/manager/e$a;

    move-result-object v0

    new-instance v2, Lcom/twitter/util/collection/e;

    sget-object v3, Lcom/twitter/media/util/a;->b:Lcom/twitter/util/collection/e$c;

    invoke-direct {v2, p4, v3}, Lcom/twitter/util/collection/e;-><init>(ILcom/twitter/util/collection/e$c;)V

    .line 365
    invoke-virtual {v0, v2}, Lcom/twitter/library/media/manager/e$a;->a(Lcom/twitter/util/collection/g;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    iget-object v2, p0, Lcom/twitter/library/media/manager/g;->f:Lcpy;

    .line 367
    invoke-virtual {v0, v2}, Lcom/twitter/library/media/manager/e$a;->a(Lcpy;)Lcom/twitter/library/media/manager/k$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e$a;

    .line 368
    invoke-virtual {v0}, Lcom/twitter/library/media/manager/e$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e;

    .line 369
    iget-object v2, p0, Lcom/twitter/library/media/manager/g;->e:Lcom/twitter/util/collection/ReferenceMap;

    invoke-virtual {v2, p1, v0}, Lcom/twitter/util/collection/ReferenceMap;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    :cond_0
    :goto_0
    monitor-exit v1

    return-object v0

    .line 371
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->g:Lcom/twitter/library/media/manager/e;

    goto :goto_0

    .line 375
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/twitter/media/request/a$a;)Lcom/twitter/util/concurrent/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/media/request/a$a;",
            ")",
            "Lcom/twitter/util/concurrent/g",
            "<",
            "Lcom/twitter/media/request/ImageResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 289
    invoke-virtual {p1}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/media/manager/g;->b(Lcom/twitter/media/request/a;)Lcom/twitter/util/concurrent/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/media/request/a;)Lrx/a;
    .locals 1

    .prologue
    .line 279
    new-instance v0, Lcom/twitter/library/media/manager/g$2;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/media/manager/g$2;-><init>(Lcom/twitter/library/media/manager/g;Lcom/twitter/media/request/a;)V

    invoke-static {v0}, Lcre;->a(Lrx/functions/a;)Lrx/a;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/twitter/media/request/a$a;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 299
    invoke-virtual {p1}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/media/manager/g;->c(Lcom/twitter/media/request/a;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/twitter/library/media/manager/e;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->g:Lcom/twitter/library/media/manager/e;

    return-object v0
.end method

.method public b(Lcom/twitter/media/request/a;)Lcom/twitter/util/concurrent/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/media/request/a;",
            ")",
            "Lcom/twitter/util/concurrent/g",
            "<",
            "Lcom/twitter/media/request/ImageResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 294
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/media/manager/g;->a(Ljava/lang/String;)Lcom/twitter/library/media/manager/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/library/media/manager/e;->a(Lcom/twitter/media/request/a;)Lcom/twitter/util/concurrent/g;

    move-result-object v0

    return-object v0
.end method

.method public c(Lcom/twitter/media/request/a;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 304
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/media/manager/g;->a(Ljava/lang/String;)Lcom/twitter/library/media/manager/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/library/media/manager/e;->e(Lcom/twitter/media/request/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public c()Lcom/twitter/library/media/manager/e;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->h:Lcom/twitter/library/media/manager/e;

    return-object v0
.end method

.method public c(Lcom/twitter/media/request/a$a;)V
    .locals 1

    .prologue
    .line 308
    invoke-virtual {p1}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/media/manager/g;->d(Lcom/twitter/media/request/a;)V

    .line 309
    return-void
.end method

.method public d()Lcom/twitter/library/media/manager/e;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->j:Lcom/twitter/library/media/manager/e;

    return-object v0
.end method

.method public d(Lcom/twitter/media/request/a$a;)Ljava/io/File;
    .locals 1

    .prologue
    .line 325
    invoke-virtual {p1}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/media/manager/g;->e(Lcom/twitter/media/request/a;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public d(Lcom/twitter/media/request/a;)V
    .locals 2

    .prologue
    .line 312
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/media/manager/g;->a(Ljava/lang/String;)Lcom/twitter/library/media/manager/e;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/media/request/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/e;->b(Ljava/lang/String;)V

    .line 313
    return-void
.end method

.method public e()Lcom/twitter/library/media/manager/m;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->i:Lcom/twitter/library/media/manager/m;

    return-object v0
.end method

.method public e(Lcom/twitter/media/request/a;)Ljava/io/File;
    .locals 1

    .prologue
    .line 318
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 319
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/media/manager/g;->a(Ljava/lang/String;)Lcom/twitter/library/media/manager/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/library/media/manager/e;->c(Lcom/twitter/media/request/a;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/twitter/library/media/manager/e;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->k:Lcom/twitter/library/media/manager/e;

    return-object v0
.end method

.method public g()V
    .locals 3

    .prologue
    .line 260
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->i:Lcom/twitter/library/media/manager/m;

    invoke-virtual {v0}, Lcom/twitter/library/media/manager/m;->a()Lcom/twitter/util/collection/g;

    move-result-object v0

    .line 261
    if-eqz v0, :cond_0

    .line 262
    invoke-interface {v0}, Lcom/twitter/util/collection/g;->a()V

    .line 264
    :cond_0
    iget-object v1, p0, Lcom/twitter/library/media/manager/g;->e:Lcom/twitter/util/collection/ReferenceMap;

    monitor-enter v1

    .line 265
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->e:Lcom/twitter/util/collection/ReferenceMap;

    invoke-virtual {v0}, Lcom/twitter/util/collection/ReferenceMap;->h()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/e;

    .line 266
    invoke-virtual {v0}, Lcom/twitter/library/media/manager/e;->a()Lcom/twitter/util/collection/g;

    move-result-object v0

    .line 267
    if-eqz v0, :cond_1

    .line 268
    invoke-interface {v0}, Lcom/twitter/util/collection/g;->a()V

    goto :goto_0

    .line 271
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 272
    return-void
.end method

.method public h()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/media/manager/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 351
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->e:Lcom/twitter/util/collection/ReferenceMap;

    invoke-virtual {v0}, Lcom/twitter/util/collection/ReferenceMap;->f()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public i()Lcom/twitter/library/media/manager/a;
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/twitter/library/media/manager/g;->m:Lcom/twitter/library/media/manager/a;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 390
    invoke-direct {p0}, Lcom/twitter/library/media/manager/g;->j()V

    .line 391
    return-void
.end method

.method public onLowMemory()V
    .locals 0

    .prologue
    .line 395
    invoke-virtual {p0}, Lcom/twitter/library/media/manager/g;->g()V

    .line 396
    return-void
.end method
