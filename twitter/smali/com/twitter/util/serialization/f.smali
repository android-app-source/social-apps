.class public Lcom/twitter/util/serialization/f;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/lang/Short;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final j:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final k:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field public static final m:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<[I>;"
        }
    .end annotation
.end field

.field public static final n:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<[J>;"
        }
    .end annotation
.end field

.field public static final o:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<[F>;"
        }
    .end annotation
.end field

.field public static final p:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<[D>;"
        }
    .end annotation
.end field

.field public static final q:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/twitter/util/serialization/f$1;

    invoke-direct {v0}, Lcom/twitter/util/serialization/f$1;-><init>()V

    sput-object v0, Lcom/twitter/util/serialization/f;->a:Lcom/twitter/util/serialization/l;

    .line 37
    new-instance v0, Lcom/twitter/util/serialization/f$11;

    invoke-direct {v0}, Lcom/twitter/util/serialization/f$11;-><init>()V

    sput-object v0, Lcom/twitter/util/serialization/f;->b:Lcom/twitter/util/serialization/l;

    .line 50
    new-instance v0, Lcom/twitter/util/serialization/f$13;

    invoke-direct {v0}, Lcom/twitter/util/serialization/f$13;-><init>()V

    sput-object v0, Lcom/twitter/util/serialization/f;->c:Lcom/twitter/util/serialization/l;

    .line 63
    new-instance v0, Lcom/twitter/util/serialization/f$14;

    invoke-direct {v0}, Lcom/twitter/util/serialization/f$14;-><init>()V

    sput-object v0, Lcom/twitter/util/serialization/f;->d:Lcom/twitter/util/serialization/l;

    .line 76
    new-instance v0, Lcom/twitter/util/serialization/f$15;

    invoke-direct {v0}, Lcom/twitter/util/serialization/f$15;-><init>()V

    sput-object v0, Lcom/twitter/util/serialization/f;->e:Lcom/twitter/util/serialization/l;

    .line 89
    new-instance v0, Lcom/twitter/util/serialization/f$16;

    invoke-direct {v0}, Lcom/twitter/util/serialization/f$16;-><init>()V

    sput-object v0, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    .line 102
    new-instance v0, Lcom/twitter/util/serialization/f$17;

    invoke-direct {v0}, Lcom/twitter/util/serialization/f$17;-><init>()V

    sput-object v0, Lcom/twitter/util/serialization/f;->g:Lcom/twitter/util/serialization/l;

    .line 115
    new-instance v0, Lcom/twitter/util/serialization/f$18;

    invoke-direct {v0}, Lcom/twitter/util/serialization/f$18;-><init>()V

    sput-object v0, Lcom/twitter/util/serialization/f;->h:Lcom/twitter/util/serialization/l;

    .line 128
    new-instance v0, Lcom/twitter/util/serialization/f$19;

    invoke-direct {v0}, Lcom/twitter/util/serialization/f$19;-><init>()V

    sput-object v0, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 145
    sget-object v0, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/i;

    move-result-object v0

    sput-object v0, Lcom/twitter/util/serialization/f;->j:Lcom/twitter/util/serialization/l;

    .line 150
    new-instance v0, Lcom/twitter/util/serialization/f$2;

    invoke-direct {v0}, Lcom/twitter/util/serialization/f$2;-><init>()V

    sput-object v0, Lcom/twitter/util/serialization/f;->k:Lcom/twitter/util/serialization/l;

    .line 204
    new-instance v0, Lcom/twitter/util/serialization/f$3;

    invoke-direct {v0}, Lcom/twitter/util/serialization/f$3;-><init>()V

    sput-object v0, Lcom/twitter/util/serialization/f;->l:Lcom/twitter/util/serialization/l;

    .line 220
    new-instance v0, Lcom/twitter/util/serialization/f$4;

    invoke-direct {v0}, Lcom/twitter/util/serialization/f$4;-><init>()V

    sput-object v0, Lcom/twitter/util/serialization/f;->m:Lcom/twitter/util/serialization/l;

    .line 244
    new-instance v0, Lcom/twitter/util/serialization/f$5;

    invoke-direct {v0}, Lcom/twitter/util/serialization/f$5;-><init>()V

    sput-object v0, Lcom/twitter/util/serialization/f;->n:Lcom/twitter/util/serialization/l;

    .line 268
    new-instance v0, Lcom/twitter/util/serialization/f$6;

    invoke-direct {v0}, Lcom/twitter/util/serialization/f$6;-><init>()V

    sput-object v0, Lcom/twitter/util/serialization/f;->o:Lcom/twitter/util/serialization/l;

    .line 292
    new-instance v0, Lcom/twitter/util/serialization/f$7;

    invoke-direct {v0}, Lcom/twitter/util/serialization/f$7;-><init>()V

    sput-object v0, Lcom/twitter/util/serialization/f;->p:Lcom/twitter/util/serialization/l;

    .line 316
    new-instance v0, Lcom/twitter/util/serialization/f$8;

    invoke-direct {v0}, Lcom/twitter/util/serialization/f$8;-><init>()V

    sput-object v0, Lcom/twitter/util/serialization/f;->q:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public static a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/twitter/util/serialization/l",
            "<TT;>;)",
            "Lcom/twitter/util/serialization/i",
            "<TT;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 472
    instance-of v0, p0, Lcom/twitter/util/serialization/i;

    if-eqz v0, :cond_0

    .line 473
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/serialization/i;

    .line 475
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/util/serialization/f$12;

    invoke-direct {v0, p0}, Lcom/twitter/util/serialization/f$12;-><init>(Lcom/twitter/util/serialization/l;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum",
            "<TT;>;>(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lcom/twitter/util/serialization/l",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 354
    new-instance v0, Lcom/twitter/util/serialization/f$9;

    invoke-direct {v0, p0}, Lcom/twitter/util/serialization/f$9;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public static a(Ljava/util/List;)Lcom/twitter/util/serialization/l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<B:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/util/serialization/j",
            "<+TB;>;>;)",
            "Lcom/twitter/util/serialization/l",
            "<TB;>;"
        }
    .end annotation

    .prologue
    .line 383
    new-instance v0, Lcom/twitter/util/serialization/f$10;

    invoke-direct {v0, p0}, Lcom/twitter/util/serialization/f$10;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public static varargs a([Lcom/twitter/util/serialization/j;)Lcom/twitter/util/serialization/l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<B:",
            "Ljava/lang/Object;",
            ">([",
            "Lcom/twitter/util/serialization/j",
            "<+TB;>;)",
            "Lcom/twitter/util/serialization/l",
            "<TB;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/SafeVarargs;
    .end annotation

    .prologue
    .line 377
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Ljava/util/List;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/util/serialization/n;Ljava/lang/Class;)Ljava/lang/Enum;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-static {p0, p1}, Lcom/twitter/util/serialization/f;->b(Lcom/twitter/util/serialization/n;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/twitter/util/serialization/n;)Ljava/util/Comparator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/twitter/util/serialization/n;",
            ")",
            "Ljava/util/Comparator",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 443
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->c()B

    move-result v0

    .line 444
    packed-switch v0, :pswitch_data_0

    .line 458
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Failed to deserialize comparator"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 446
    :pswitch_0
    invoke-static {}, Lcom/twitter/util/object/ObjectUtils;->a()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    .line 455
    :goto_0
    return-object v0

    .line 449
    :pswitch_1
    invoke-static {}, Lcom/twitter/util/object/ObjectUtils;->b()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    goto :goto_0

    .line 452
    :pswitch_2
    invoke-static {}, Lcom/twitter/util/object/ObjectUtils;->c()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    goto :goto_0

    .line 455
    :pswitch_3
    invoke-static {p0}, Lcom/twitter/util/serialization/f;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    goto :goto_0

    .line 444
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/twitter/util/serialization/o;Ljava/lang/Enum;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum",
            "<TT;>;>(",
            "Lcom/twitter/util/serialization/o;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 371
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 372
    return-void
.end method

.method private static a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 505
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 507
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 512
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 513
    return-void

    .line 508
    :catch_0
    move-exception v0

    .line 509
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Class has no default constructor: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Lcom/twitter/util/serialization/o;Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/twitter/util/serialization/o;",
            "Ljava/util/Comparator",
            "<TT;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 428
    invoke-static {}, Lcom/twitter/util/object/ObjectUtils;->a()Ljava/util/Comparator;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 429
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/util/serialization/o;->b(B)Lcom/twitter/util/serialization/o;

    .line 438
    :goto_0
    return-void

    .line 430
    :cond_0
    invoke-static {}, Lcom/twitter/util/object/ObjectUtils;->b()Ljava/util/Comparator;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 431
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/util/serialization/o;->b(B)Lcom/twitter/util/serialization/o;

    goto :goto_0

    .line 432
    :cond_1
    invoke-static {}, Lcom/twitter/util/object/ObjectUtils;->c()Ljava/util/Comparator;

    move-result-object v0

    if-ne p1, v0, :cond_2

    .line 433
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/util/serialization/o;->b(B)Lcom/twitter/util/serialization/o;

    goto :goto_0

    .line 435
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/util/serialization/o;->b(B)Lcom/twitter/util/serialization/o;

    .line 436
    invoke-static {p0, p1}, Lcom/twitter/util/serialization/f;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static b(Lcom/twitter/util/serialization/n;Ljava/lang/Class;)Ljava/lang/Enum;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum",
            "<TT;>;>(",
            "Lcom/twitter/util/serialization/n;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 495
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 521
    invoke-virtual {p0}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 524
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 525
    :catch_0
    move-exception v1

    .line 528
    :goto_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Object has no default constructor: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 526
    :catch_1
    move-exception v1

    goto :goto_0
.end method
