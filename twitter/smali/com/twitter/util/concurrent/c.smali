.class public Lcom/twitter/util/concurrent/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/concurrent/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/twitter/util/concurrent/a",
        "<TV;>;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private a:Ljava/util/concurrent/Executor;

.field private b:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<TV;>;"
        }
    .end annotation
.end field

.field private c:Lcom/twitter/util/concurrent/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/concurrent/d",
            "<TV;>;"
        }
    .end annotation
.end field

.field private d:Lcom/twitter/util/concurrent/ObservablePromise;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/concurrent/ObservablePromise",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/concurrent/Callable;)Lcom/twitter/util/concurrent/c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;)",
            "Lcom/twitter/util/concurrent/c",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 46
    iput-object p1, p0, Lcom/twitter/util/concurrent/c;->b:Ljava/util/concurrent/Callable;

    .line 47
    return-object p0
.end method

.method public a(Ljava/util/concurrent/Executor;)Lcom/twitter/util/concurrent/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/twitter/util/concurrent/c",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 37
    invoke-static {}, Lcrt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/twitter/util/concurrent/j;->a:Lcom/twitter/util/concurrent/j;

    :cond_0
    iput-object p1, p0, Lcom/twitter/util/concurrent/c;->a:Ljava/util/concurrent/Executor;

    .line 38
    return-object p0
.end method

.method public a()Lcom/twitter/util/concurrent/g;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/concurrent/g",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/util/concurrent/c;->a:Ljava/util/concurrent/Executor;

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The executor must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/twitter/util/concurrent/c;->b:Ljava/util/concurrent/Callable;

    if-nez v0, :cond_1

    .line 85
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The callable must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88
    :cond_1
    iget-object v1, p0, Lcom/twitter/util/concurrent/c;->b:Ljava/util/concurrent/Callable;

    .line 89
    iget-object v2, p0, Lcom/twitter/util/concurrent/c;->c:Lcom/twitter/util/concurrent/d;

    .line 90
    iget-object v0, p0, Lcom/twitter/util/concurrent/c;->d:Lcom/twitter/util/concurrent/ObservablePromise;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/util/concurrent/c;->d:Lcom/twitter/util/concurrent/ObservablePromise;

    .line 92
    :goto_0
    iget-object v3, p0, Lcom/twitter/util/concurrent/c;->a:Ljava/util/concurrent/Executor;

    new-instance v4, Lcom/twitter/util/concurrent/c$1;

    invoke-direct {v4, p0, v0, v1, v2}, Lcom/twitter/util/concurrent/c$1;-><init>(Lcom/twitter/util/concurrent/c;Lcom/twitter/util/concurrent/ObservablePromise;Ljava/util/concurrent/Callable;Lcom/twitter/util/concurrent/d;)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 109
    return-object v0

    .line 90
    :cond_2
    new-instance v0, Lcom/twitter/util/concurrent/ObservablePromise;

    invoke-direct {v0}, Lcom/twitter/util/concurrent/ObservablePromise;-><init>()V

    goto :goto_0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/twitter/util/concurrent/c;->a()Lcom/twitter/util/concurrent/g;

    move-result-object v0

    return-object v0
.end method
