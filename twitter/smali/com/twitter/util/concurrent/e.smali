.class public Lcom/twitter/util/concurrent/e;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/concurrent/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/twitter/util/concurrent/d",
        "<TV;>;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private a:Ljava/util/concurrent/Executor;

.field private b:Lcom/twitter/util/concurrent/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/concurrent/d",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/util/concurrent/d;)Lcom/twitter/util/concurrent/e;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/concurrent/d",
            "<TV;>;)",
            "Lcom/twitter/util/concurrent/e",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 33
    iput-object p1, p0, Lcom/twitter/util/concurrent/e;->b:Lcom/twitter/util/concurrent/d;

    .line 34
    return-object p0
.end method

.method public a(Ljava/util/concurrent/Executor;)Lcom/twitter/util/concurrent/e;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/twitter/util/concurrent/e",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 24
    iput-object p1, p0, Lcom/twitter/util/concurrent/e;->a:Ljava/util/concurrent/Executor;

    .line 25
    return-object p0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/util/concurrent/e;->a:Ljava/util/concurrent/Executor;

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The executor must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/twitter/util/concurrent/e;->b:Lcom/twitter/util/concurrent/d;

    .line 43
    if-nez v0, :cond_1

    .line 44
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The callback must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_1
    iget-object v1, p0, Lcom/twitter/util/concurrent/e;->a:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/twitter/util/concurrent/e$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/twitter/util/concurrent/e$1;-><init>(Lcom/twitter/util/concurrent/e;Lcom/twitter/util/concurrent/d;Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 53
    return-void
.end method
