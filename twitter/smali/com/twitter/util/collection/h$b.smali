.class final Lcom/twitter/util/collection/h$b;
.super Lcom/twitter/util/collection/h$a;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/collection/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/util/collection/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/twitter/util/collection/h$a",
        "<TT;>;",
        "Lcom/twitter/util/collection/p",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<-TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/Comparator;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TT;>;I)V"
        }
    .end annotation

    .prologue
    .line 659
    invoke-direct {p0}, Lcom/twitter/util/collection/h$a;-><init>()V

    .line 660
    iput-object p1, p0, Lcom/twitter/util/collection/h$b;->b:Ljava/util/Comparator;

    .line 661
    const/4 v0, 0x1

    if-le p2, v0, :cond_0

    .line 663
    invoke-virtual {p0, p2}, Lcom/twitter/util/collection/h$b;->d(I)V

    .line 665
    :cond_0
    return-void
.end method


# virtual methods
.method protected b(ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)V"
        }
    .end annotation

    .prologue
    .line 680
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TT;>;"
        }
    .end annotation

    .prologue
    .line 670
    iget-object v0, p0, Lcom/twitter/util/collection/h$b;->b:Ljava/util/Comparator;

    return-object v0
.end method

.method protected d(I)V
    .locals 1

    .prologue
    .line 675
    iget-object v0, p0, Lcom/twitter/util/collection/h$b;->b:Ljava/util/Comparator;

    invoke-static {v0, p1}, Lcom/twitter/util/collection/MutableList;->a(Ljava/util/Comparator;I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/util/collection/h$b;->a:Ljava/util/List;

    .line 676
    return-void
.end method
