.class Lcom/twitter/util/collection/h$a;
.super Lcom/twitter/util/collection/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/util/collection/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/twitter/util/collection/h",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 503
    invoke-direct {p0}, Lcom/twitter/util/collection/h;-><init>()V

    .line 504
    return-void
.end method

.method constructor <init>(I)V
    .locals 1

    .prologue
    .line 506
    invoke-direct {p0}, Lcom/twitter/util/collection/h;-><init>()V

    .line 507
    const/4 v0, 0x1

    if-le p1, v0, :cond_0

    .line 508
    invoke-virtual {p0, p1}, Lcom/twitter/util/collection/h$a;->d(I)V

    .line 510
    :cond_0
    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Lcom/twitter/util/collection/c;
    .locals 1

    .prologue
    .line 500
    invoke-super {p0, p1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 500
    invoke-super {p0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 500
    invoke-super {p0}, Lcom/twitter/util/collection/h;->k()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected b(ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)V"
        }
    .end annotation

    .prologue
    .line 541
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/util/collection/h$a;->l()I

    move-result v0

    if-le p1, v0, :cond_1

    .line 542
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 544
    :cond_1
    iget-object v0, p0, Lcom/twitter/util/collection/h$a;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 545
    return-void
.end method

.method protected c(I)V
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Lcom/twitter/util/collection/h$a;->a:Ljava/util/List;

    instance-of v0, v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 520
    iget-object v0, p0, Lcom/twitter/util/collection/h$a;->a:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 522
    :cond_0
    return-void
.end method

.method protected d(I)V
    .locals 1

    .prologue
    .line 531
    if-eqz p1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    :goto_0
    iput-object v0, p0, Lcom/twitter/util/collection/h$a;->a:Ljava/util/List;

    .line 532
    return-void

    .line 531
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method protected e(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 536
    iget-object v0, p0, Lcom/twitter/util/collection/h$a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 537
    return-void
.end method

.method protected f(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 549
    iget-object v0, p0, Lcom/twitter/util/collection/h$a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 550
    return-void
.end method

.method protected l()I
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lcom/twitter/util/collection/h$a;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/util/collection/h$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected m()Z
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lcom/twitter/util/collection/h$a;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected n()V
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcom/twitter/util/collection/h$a;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 566
    return-void
.end method

.method protected o()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 560
    iget-object v0, p0, Lcom/twitter/util/collection/h$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method protected p()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 576
    iget-object v0, p0, Lcom/twitter/util/collection/h$a;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/ImmutableList;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 577
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/util/collection/h$a;->a:Ljava/util/List;

    .line 578
    return-object v0
.end method
