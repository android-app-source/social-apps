.class public Lcom/twitter/util/aa;
.super Lcom/twitter/util/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/util/aa$a;
    }
.end annotation


# static fields
.field private static final d:Lcom/twitter/util/aa$a;

.field private static final e:Lcok$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcok$a",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static f:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lcom/twitter/util/aa$a;

    invoke-direct {v0}, Lcom/twitter/util/aa$a;-><init>()V

    sput-object v0, Lcom/twitter/util/aa;->d:Lcom/twitter/util/aa$a;

    .line 29
    const-string/jumbo v0, "format_tweet_date_with_user_locale_enabled"

    const/4 v1, 0x0

    .line 30
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcok;->a(Ljava/lang/String;Ljava/lang/Object;)Lcok$a;

    move-result-object v0

    sput-object v0, Lcom/twitter/util/aa;->e:Lcok$a;

    .line 29
    return-void
.end method

.method public static a(JLjava/util/concurrent/TimeUnit;)J
    .locals 4

    .prologue
    .line 244
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    invoke-static {}, Lcom/twitter/util/aa;->e()J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-virtual {p2, p0, p1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public static a(Landroid/content/res/Resources;J)Ljava/lang/String;
    .locals 13

    .prologue
    const-wide/32 v10, 0x5265c00

    const-wide/32 v8, 0x36ee80

    const-wide/32 v6, 0xea60

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 66
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    sub-long/2addr v0, p1

    .line 67
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_5

    .line 68
    cmp-long v2, v0, v6

    if-gez v2, :cond_0

    .line 69
    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    .line 70
    sget v1, Lcom/twitter/util/w$e;->time_secs:I

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 101
    :goto_0
    return-object v0

    .line 71
    :cond_0
    cmp-long v2, v0, v8

    if-gez v2, :cond_1

    .line 72
    div-long/2addr v0, v6

    long-to-int v0, v0

    .line 73
    sget v1, Lcom/twitter/util/w$e;->time_mins:I

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 74
    :cond_1
    cmp-long v2, v0, v10

    if-gez v2, :cond_2

    .line 75
    div-long/2addr v0, v8

    long-to-int v0, v0

    .line 76
    sget v1, Lcom/twitter/util/w$e;->time_hours:I

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 77
    :cond_2
    const-wide/32 v2, 0x240c8400

    cmp-long v2, v0, v2

    if-gez v2, :cond_3

    .line 78
    div-long/2addr v0, v10

    long-to-int v0, v0

    .line 79
    sget v1, Lcom/twitter/util/w$e;->time_days:I

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 82
    :cond_3
    invoke-static {}, Lcom/twitter/util/aa;->d()Ljava/util/Calendar;

    move-result-object v0

    .line 85
    invoke-static {}, Lcom/twitter/util/aa;->d()Ljava/util/Calendar;

    move-result-object v1

    .line 86
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 87
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 89
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 91
    sget-object v0, Lcom/twitter/util/aa;->d:Lcom/twitter/util/aa$a;

    invoke-virtual {v0, p0, v2}, Lcom/twitter/util/aa$a;->c(Landroid/content/res/Resources;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 94
    :cond_4
    sget-object v0, Lcom/twitter/util/aa;->d:Lcom/twitter/util/aa$a;

    invoke-virtual {v0, p0, v2}, Lcom/twitter/util/aa$a;->b(Landroid/content/res/Resources;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 97
    :cond_5
    const-wide/32 v2, -0xea60

    cmp-long v0, v0, v2

    if-ltz v0, :cond_6

    .line 98
    sget v0, Lcom/twitter/util/w$g;->now:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 101
    :cond_6
    sget-object v0, Lcom/twitter/util/aa;->d:Lcom/twitter/util/aa$a;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, p0, v1}, Lcom/twitter/util/aa$a;->b(Landroid/content/res/Resources;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static b(Landroid/content/res/Resources;J)Ljava/lang/String;
    .locals 13

    .prologue
    const-wide/32 v10, 0x5265c00

    const-wide/32 v8, 0x36ee80

    const-wide/32 v6, 0xea60

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 114
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    sub-long/2addr v0, p1

    .line 115
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_5

    .line 116
    cmp-long v2, v0, v6

    if-gez v2, :cond_0

    .line 117
    sget v0, Lcom/twitter/util/w$g;->recent_tweets_header_title:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 148
    :goto_0
    return-object v0

    .line 118
    :cond_0
    cmp-long v2, v0, v8

    if-gez v2, :cond_1

    .line 119
    div-long/2addr v0, v6

    long-to-int v0, v0

    .line 120
    sget v1, Lcom/twitter/util/w$e;->time_mins_ago:I

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 121
    :cond_1
    cmp-long v2, v0, v10

    if-gez v2, :cond_2

    .line 122
    div-long/2addr v0, v8

    long-to-int v0, v0

    .line 123
    sget v1, Lcom/twitter/util/w$e;->time_hours_ago:I

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 124
    :cond_2
    const-wide/32 v2, 0x240c8400

    cmp-long v2, v0, v2

    if-gez v2, :cond_3

    .line 125
    div-long/2addr v0, v10

    long-to-int v0, v0

    .line 126
    sget v1, Lcom/twitter/util/w$e;->time_days_ago:I

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 129
    :cond_3
    invoke-static {}, Lcom/twitter/util/aa;->d()Ljava/util/Calendar;

    move-result-object v0

    .line 132
    invoke-static {}, Lcom/twitter/util/aa;->d()Ljava/util/Calendar;

    move-result-object v1

    .line 133
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 134
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 136
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 138
    sget-object v0, Lcom/twitter/util/aa;->d:Lcom/twitter/util/aa$a;

    invoke-virtual {v0, p0, v2}, Lcom/twitter/util/aa$a;->e(Landroid/content/res/Resources;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 141
    :cond_4
    sget-object v0, Lcom/twitter/util/aa;->d:Lcom/twitter/util/aa$a;

    invoke-virtual {v0, p0, v2}, Lcom/twitter/util/aa$a;->d(Landroid/content/res/Resources;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 144
    :cond_5
    const-wide/32 v2, -0xea60

    cmp-long v0, v0, v2

    if-ltz v0, :cond_6

    .line 145
    sget v0, Lcom/twitter/util/w$g;->now:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 148
    :cond_6
    sget-object v0, Lcom/twitter/util/aa;->d:Lcom/twitter/util/aa$a;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, p0, v1}, Lcom/twitter/util/aa$a;->d(Landroid/content/res/Resources;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static c(Landroid/content/res/Resources;J)Ljava/lang/String;
    .locals 13

    .prologue
    const-wide v10, 0x9ca41900L

    const-wide/32 v8, 0x240c8400

    const-wide/32 v6, 0x5265c00

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 159
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    sub-long/2addr v0, p1

    .line 160
    cmp-long v2, v0, v6

    if-gez v2, :cond_0

    .line 161
    const/4 v0, 0x0

    .line 173
    :goto_0
    return-object v0

    .line 162
    :cond_0
    cmp-long v2, v0, v8

    if-gez v2, :cond_1

    .line 163
    div-long/2addr v0, v6

    long-to-int v0, v0

    .line 164
    sget v1, Lcom/twitter/util/w$e;->days:I

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 165
    :cond_1
    cmp-long v2, v0, v10

    if-gez v2, :cond_2

    .line 166
    div-long/2addr v0, v8

    long-to-int v0, v0

    .line 167
    sget v1, Lcom/twitter/util/w$e;->weeks:I

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 168
    :cond_2
    const-wide v2, 0x757b12c00L

    cmp-long v2, v0, v2

    if-gez v2, :cond_3

    .line 169
    div-long/2addr v0, v10

    long-to-int v0, v0

    .line 170
    sget v1, Lcom/twitter/util/w$e;->months:I

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 172
    :cond_3
    const-wide v2, 0x757b12c00L

    div-long/2addr v0, v2

    long-to-int v0, v0

    .line 173
    sget v1, Lcom/twitter/util/w$e;->years:I

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(Landroid/content/res/Resources;J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 215
    sget-object v0, Lcom/twitter/util/aa;->d:Lcom/twitter/util/aa$a;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, p0, v1}, Lcom/twitter/util/aa$a;->c(Landroid/content/res/Resources;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static d()Ljava/util/Calendar;
    .locals 6

    .prologue
    .line 48
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 50
    sget-wide v2, Lcom/twitter/util/aa;->c:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 51
    invoke-static {}, Lcom/twitter/util/f;->d()V

    .line 52
    sget-wide v2, Lcom/twitter/util/aa;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 54
    :cond_0
    return-object v0
.end method

.method public static e()J
    .locals 4

    .prologue
    .line 237
    sget-wide v0, Lcom/twitter/util/aa;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    sget-wide v0, Lcom/twitter/util/aa;->f:J

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    goto :goto_0
.end method

.method public static e(Landroid/content/res/Resources;J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 230
    sget-object v0, Lcom/twitter/util/aa;->d:Lcom/twitter/util/aa$a;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, p0, v1}, Lcom/twitter/util/aa$a;->a(Landroid/content/res/Resources;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f()Z
    .locals 1

    .prologue
    .line 25
    invoke-static {}, Lcom/twitter/util/aa;->g()Z

    move-result v0

    return v0
.end method

.method public static g(J)Ljava/lang/String;
    .locals 12

    .prologue
    const-wide/16 v10, 0x3e8

    const-wide/32 v4, 0x5265c00

    const-wide/32 v8, 0x36ee80

    const-wide/32 v6, 0xea60

    .line 183
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-lez v0, :cond_2

    .line 184
    const-string/jumbo v0, ""

    .line 185
    cmp-long v1, p0, v4

    if-lez v1, :cond_3

    .line 186
    div-long v2, p0, v4

    long-to-int v1, v2

    .line 187
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "d"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 188
    rem-long v2, p0, v4

    .line 190
    :goto_0
    cmp-long v1, v2, v8

    if-lez v1, :cond_0

    .line 191
    div-long v4, v2, v8

    long-to-int v1, v4

    .line 192
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "h"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 193
    rem-long/2addr v2, v8

    .line 195
    :cond_0
    cmp-long v1, v2, v6

    if-lez v1, :cond_1

    .line 196
    div-long v4, v2, v6

    long-to-int v1, v4

    .line 197
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "m"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 198
    rem-long/2addr v2, v6

    .line 200
    :cond_1
    div-long v4, v2, v10

    long-to-int v1, v4

    .line 201
    rem-long/2addr v2, v10

    long-to-int v2, v2

    .line 202
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 205
    :goto_1
    return-object v0

    :cond_2
    const-string/jumbo v0, "invalid duration"

    goto :goto_1

    :cond_3
    move-wide v2, p0

    goto :goto_0
.end method

.method private static g()Z
    .locals 1

    .prologue
    .line 293
    sget-object v0, Lcom/twitter/util/aa;->e:Lcok$a;

    invoke-virtual {v0}, Lcok$a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom;

    invoke-virtual {v0}, Lcom;->a()Z

    move-result v0

    return v0
.end method
