.class public Lcom/twitter/util/a$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/SharedPreferences$Editor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/util/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/util/a;

.field private b:Landroid/content/SharedPreferences$Editor;


# direct methods
.method private constructor <init>(Lcom/twitter/util/a;Landroid/content/SharedPreferences$Editor;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/twitter/util/a$a;->a:Lcom/twitter/util/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    iput-object p2, p0, Lcom/twitter/util/a$a;->b:Landroid/content/SharedPreferences$Editor;

    .line 144
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/util/a;Landroid/content/SharedPreferences$Editor;Lcom/twitter/util/a$1;)V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Lcom/twitter/util/a$a;-><init>(Lcom/twitter/util/a;Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/util/a$a;
    .locals 3

    .prologue
    .line 207
    iget-object v1, p0, Lcom/twitter/util/a$a;->b:Landroid/content/SharedPreferences$Editor;

    .line 208
    iget-object v0, p0, Lcom/twitter/util/a$a;->a:Lcom/twitter/util/a;

    invoke-virtual {v0}, Lcom/twitter/util/a;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 209
    invoke-interface {v1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 211
    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/util/a$a;
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/twitter/util/a$a;->b:Landroid/content/SharedPreferences$Editor;

    iget-object v1, p0, Lcom/twitter/util/a$a;->a:Lcom/twitter/util/a;

    invoke-static {v1, p1}, Lcom/twitter/util/a;->a(Lcom/twitter/util/a;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 201
    return-object p0
.end method

.method public a(Ljava/lang/String;F)Lcom/twitter/util/a$a;
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/twitter/util/a$a;->b:Landroid/content/SharedPreferences$Editor;

    iget-object v1, p0, Lcom/twitter/util/a$a;->a:Lcom/twitter/util/a;

    invoke-static {v1, p1}, Lcom/twitter/util/a;->a(Lcom/twitter/util/a;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 178
    return-object p0
.end method

.method public a(Ljava/lang/String;I)Lcom/twitter/util/a$a;
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/twitter/util/a$a;->b:Landroid/content/SharedPreferences$Editor;

    iget-object v1, p0, Lcom/twitter/util/a$a;->a:Lcom/twitter/util/a;

    invoke-static {v1, p1}, Lcom/twitter/util/a;->a(Lcom/twitter/util/a;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 164
    return-object p0
.end method

.method public a(Ljava/lang/String;J)Lcom/twitter/util/a$a;
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/twitter/util/a$a;->b:Landroid/content/SharedPreferences$Editor;

    iget-object v1, p0, Lcom/twitter/util/a$a;->a:Lcom/twitter/util/a;

    invoke-static {v1, p1}, Lcom/twitter/util/a;->a(Lcom/twitter/util/a;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 171
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/a$a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "TT;",
            "Lcom/twitter/util/serialization/l",
            "<TT;>;)",
            "Lcom/twitter/util/a$a;"
        }
    .end annotation

    .prologue
    .line 190
    if-eqz p2, :cond_0

    .line 191
    invoke-static {p2, p3}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v0

    .line 192
    iget-object v1, p0, Lcom/twitter/util/a$a;->b:Landroid/content/SharedPreferences$Editor;

    iget-object v2, p0, Lcom/twitter/util/a$a;->a:Lcom/twitter/util/a;

    invoke-static {v2, p1}, Lcom/twitter/util/a;->a(Lcom/twitter/util/a;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 194
    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/util/a$a;
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/twitter/util/a$a;->b:Landroid/content/SharedPreferences$Editor;

    iget-object v1, p0, Lcom/twitter/util/a$a;->a:Lcom/twitter/util/a;

    invoke-static {v1, p1}, Lcom/twitter/util/a;->a(Lcom/twitter/util/a;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 150
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/util/Set;)Lcom/twitter/util/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/twitter/util/a$a;"
        }
    .end annotation

    .prologue
    .line 156
    sget-object v0, Lcom/twitter/util/serialization/f;->j:Lcom/twitter/util/serialization/l;

    invoke-static {v0}, Lcom/twitter/util/collection/d;->b(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/a$a;

    .line 157
    return-object p0
.end method

.method public a(Ljava/lang/String;Z)Lcom/twitter/util/a$a;
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/twitter/util/a$a;->b:Landroid/content/SharedPreferences$Editor;

    iget-object v1, p0, Lcom/twitter/util/a$a;->a:Lcom/twitter/util/a;

    invoke-static {v1, p1}, Lcom/twitter/util/a;->a(Lcom/twitter/util/a;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 185
    return-object p0
.end method

.method public apply()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 216
    iget-object v0, p0, Lcom/twitter/util/a$a;->b:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 217
    iput-object v1, p0, Lcom/twitter/util/a$a;->b:Landroid/content/SharedPreferences$Editor;

    .line 218
    iget-object v0, p0, Lcom/twitter/util/a$a;->a:Lcom/twitter/util/a;

    invoke-static {v0, v1}, Lcom/twitter/util/a;->a(Lcom/twitter/util/a;Lcom/twitter/util/a$a;)Lcom/twitter/util/a$a;

    .line 219
    return-void
.end method

.method public synthetic clear()Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/twitter/util/a$a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    return-object v0
.end method

.method public commit()Z
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 237
    iget-object v0, p0, Lcom/twitter/util/a$a;->b:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    .line 238
    iput-object v2, p0, Lcom/twitter/util/a$a;->b:Landroid/content/SharedPreferences$Editor;

    .line 239
    iget-object v1, p0, Lcom/twitter/util/a$a;->a:Lcom/twitter/util/a;

    invoke-static {v1, v2}, Lcom/twitter/util/a;->a(Lcom/twitter/util/a;Lcom/twitter/util/a$a;)Lcom/twitter/util/a$a;

    .line 240
    return v0
.end method

.method public synthetic putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0, p1, p2}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Z)Lcom/twitter/util/a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0, p1, p2}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;F)Lcom/twitter/util/a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0, p1, p2}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;I)Lcom/twitter/util/a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;
    .locals 2

    .prologue
    .line 139
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;J)Lcom/twitter/util/a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0, p1, p2}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/util/a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0, p1, p2}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Ljava/util/Set;)Lcom/twitter/util/a$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0, p1}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;)Lcom/twitter/util/a$a;

    move-result-object v0

    return-object v0
.end method
