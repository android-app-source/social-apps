.class public abstract Lcom/twitter/util/z;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:F

.field private static b:I

.field private static c:F

.field private static d:F

.field private static e:F

.field private static f:Z

.field private static g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 25
    sput v1, Lcom/twitter/util/z;->a:F

    .line 26
    const/16 v0, 0xa0

    sput v0, Lcom/twitter/util/z;->b:I

    .line 27
    sput v1, Lcom/twitter/util/z;->c:F

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(F)I
    .locals 1

    .prologue
    .line 74
    invoke-static {}, Lcom/twitter/util/z;->b()F

    move-result v0

    mul-float/2addr v0, p0

    float-to-int v0, v0

    return v0
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/twitter/util/z;->g:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 52
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 54
    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    sput v1, Lcom/twitter/util/z;->a:F

    .line 55
    iget v1, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    sput v1, Lcom/twitter/util/z;->b:I

    .line 56
    iget v0, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    sput v0, Lcom/twitter/util/z;->c:F

    .line 57
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    sput v0, Lcom/twitter/util/z;->d:F

    .line 58
    invoke-static {}, Lcom/twitter/util/z;->e()F

    move-result v0

    invoke-static {}, Lcom/twitter/util/z;->e()F

    move-result v1

    mul-float/2addr v0, v1

    sput v0, Lcom/twitter/util/z;->e:F

    .line 59
    invoke-static {p0}, Lcom/twitter/util/b;->a(Landroid/content/Context;)Z

    move-result v0

    sput-boolean v0, Lcom/twitter/util/z;->f:Z

    .line 61
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/util/z;->g:Ljava/lang/String;

    .line 62
    const-class v0, Lcom/twitter/util/z;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 63
    return-void
.end method

.method public static a(Z)V
    .locals 1

    .prologue
    .line 102
    sput-boolean p0, Lcom/twitter/util/z;->f:Z

    .line 103
    const-class v0, Lcom/twitter/util/z;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 104
    return-void
.end method

.method public static b()F
    .locals 1

    .prologue
    .line 78
    sget v0, Lcom/twitter/util/z;->a:F

    return v0
.end method

.method public static c()I
    .locals 1

    .prologue
    .line 82
    sget v0, Lcom/twitter/util/z;->b:I

    return v0
.end method

.method public static d()F
    .locals 1

    .prologue
    .line 86
    sget v0, Lcom/twitter/util/z;->c:F

    return v0
.end method

.method public static e()F
    .locals 1

    .prologue
    .line 90
    sget v0, Lcom/twitter/util/z;->d:F

    return v0
.end method

.method public static f()F
    .locals 1

    .prologue
    .line 94
    sget v0, Lcom/twitter/util/z;->e:F

    return v0
.end method

.method public static g()Z
    .locals 1

    .prologue
    .line 98
    sget-boolean v0, Lcom/twitter/util/z;->f:Z

    return v0
.end method
