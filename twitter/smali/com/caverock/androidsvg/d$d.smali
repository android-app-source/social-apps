.class Lcom/caverock/androidsvg/d$d;
.super Lcom/caverock/androidsvg/d$e;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/caverock/androidsvg/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/caverock/androidsvg/d;

.field private f:Landroid/graphics/Path;


# direct methods
.method public constructor <init>(Lcom/caverock/androidsvg/d;Landroid/graphics/Path;FF)V
    .locals 0

    .prologue
    .line 1664
    iput-object p1, p0, Lcom/caverock/androidsvg/d$d;->a:Lcom/caverock/androidsvg/d;

    .line 1665
    invoke-direct {p0, p1, p3, p4}, Lcom/caverock/androidsvg/d$e;-><init>(Lcom/caverock/androidsvg/d;FF)V

    .line 1666
    iput-object p2, p0, Lcom/caverock/androidsvg/d$d;->f:Landroid/graphics/Path;

    .line 1667
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1672
    iget-object v0, p0, Lcom/caverock/androidsvg/d$d;->a:Lcom/caverock/androidsvg/d;

    invoke-static {v0}, Lcom/caverock/androidsvg/d;->a(Lcom/caverock/androidsvg/d;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1674
    iget-object v0, p0, Lcom/caverock/androidsvg/d$d;->a:Lcom/caverock/androidsvg/d;

    invoke-static {v0}, Lcom/caverock/androidsvg/d;->b(Lcom/caverock/androidsvg/d;)Lcom/caverock/androidsvg/d$g;

    move-result-object v0

    iget-boolean v0, v0, Lcom/caverock/androidsvg/d$g;->b:Z

    if-eqz v0, :cond_0

    .line 1675
    iget-object v0, p0, Lcom/caverock/androidsvg/d$d;->a:Lcom/caverock/androidsvg/d;

    invoke-static {v0}, Lcom/caverock/androidsvg/d;->c(Lcom/caverock/androidsvg/d;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v2, p0, Lcom/caverock/androidsvg/d$d;->f:Landroid/graphics/Path;

    iget v3, p0, Lcom/caverock/androidsvg/d$d;->b:F

    iget v4, p0, Lcom/caverock/androidsvg/d$d;->c:F

    iget-object v1, p0, Lcom/caverock/androidsvg/d$d;->a:Lcom/caverock/androidsvg/d;

    invoke-static {v1}, Lcom/caverock/androidsvg/d;->b(Lcom/caverock/androidsvg/d;)Lcom/caverock/androidsvg/d$g;

    move-result-object v1

    iget-object v5, v1, Lcom/caverock/androidsvg/d$g;->d:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawTextOnPath(Ljava/lang/String;Landroid/graphics/Path;FFLandroid/graphics/Paint;)V

    .line 1676
    :cond_0
    iget-object v0, p0, Lcom/caverock/androidsvg/d$d;->a:Lcom/caverock/androidsvg/d;

    invoke-static {v0}, Lcom/caverock/androidsvg/d;->b(Lcom/caverock/androidsvg/d;)Lcom/caverock/androidsvg/d$g;

    move-result-object v0

    iget-boolean v0, v0, Lcom/caverock/androidsvg/d$g;->c:Z

    if-eqz v0, :cond_1

    .line 1677
    iget-object v0, p0, Lcom/caverock/androidsvg/d$d;->a:Lcom/caverock/androidsvg/d;

    invoke-static {v0}, Lcom/caverock/androidsvg/d;->c(Lcom/caverock/androidsvg/d;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v2, p0, Lcom/caverock/androidsvg/d$d;->f:Landroid/graphics/Path;

    iget v3, p0, Lcom/caverock/androidsvg/d$d;->b:F

    iget v4, p0, Lcom/caverock/androidsvg/d$d;->c:F

    iget-object v1, p0, Lcom/caverock/androidsvg/d$d;->a:Lcom/caverock/androidsvg/d;

    invoke-static {v1}, Lcom/caverock/androidsvg/d;->b(Lcom/caverock/androidsvg/d;)Lcom/caverock/androidsvg/d$g;

    move-result-object v1

    iget-object v5, v1, Lcom/caverock/androidsvg/d$g;->e:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawTextOnPath(Ljava/lang/String;Landroid/graphics/Path;FFLandroid/graphics/Paint;)V

    .line 1681
    :cond_1
    iget v0, p0, Lcom/caverock/androidsvg/d$d;->b:F

    iget-object v1, p0, Lcom/caverock/androidsvg/d$d;->a:Lcom/caverock/androidsvg/d;

    invoke-static {v1}, Lcom/caverock/androidsvg/d;->b(Lcom/caverock/androidsvg/d;)Lcom/caverock/androidsvg/d$g;

    move-result-object v1

    iget-object v1, v1, Lcom/caverock/androidsvg/d$g;->d:Landroid/graphics/Paint;

    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/caverock/androidsvg/d$d;->b:F

    .line 1682
    return-void
.end method
