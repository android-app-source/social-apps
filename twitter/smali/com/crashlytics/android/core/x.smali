.class Lcom/crashlytics/android/core/x;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/crashlytics/android/core/x$c;,
        Lcom/crashlytics/android/core/x$b;,
        Lcom/crashlytics/android/core/x$l;,
        Lcom/crashlytics/android/core/x$g;,
        Lcom/crashlytics/android/core/x$m;,
        Lcom/crashlytics/android/core/x$f;,
        Lcom/crashlytics/android/core/x$a;,
        Lcom/crashlytics/android/core/x$h;,
        Lcom/crashlytics/android/core/x$d;,
        Lcom/crashlytics/android/core/x$e;,
        Lcom/crashlytics/android/core/x$i;,
        Lcom/crashlytics/android/core/x$k;,
        Lcom/crashlytics/android/core/x$j;
    }
.end annotation


# static fields
.field private static final a:Lv;

.field private static final b:[Lcom/crashlytics/android/core/x$j;

.field private static final c:[Lcom/crashlytics/android/core/x$m;

.field private static final d:[Lcom/crashlytics/android/core/x$g;

.field private static final e:[Lcom/crashlytics/android/core/x$b;

.field private static final f:[Lcom/crashlytics/android/core/x$c;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 27
    new-instance v0, Lv;

    const-string/jumbo v1, ""

    const-string/jumbo v2, ""

    const-wide/16 v4, 0x0

    invoke-direct {v0, v1, v2, v4, v5}, Lv;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    sput-object v0, Lcom/crashlytics/android/core/x;->a:Lv;

    .line 29
    new-array v0, v3, [Lcom/crashlytics/android/core/x$j;

    sput-object v0, Lcom/crashlytics/android/core/x;->b:[Lcom/crashlytics/android/core/x$j;

    .line 30
    new-array v0, v3, [Lcom/crashlytics/android/core/x$m;

    sput-object v0, Lcom/crashlytics/android/core/x;->c:[Lcom/crashlytics/android/core/x$m;

    .line 31
    new-array v0, v3, [Lcom/crashlytics/android/core/x$g;

    sput-object v0, Lcom/crashlytics/android/core/x;->d:[Lcom/crashlytics/android/core/x$g;

    .line 32
    new-array v0, v3, [Lcom/crashlytics/android/core/x$b;

    sput-object v0, Lcom/crashlytics/android/core/x;->e:[Lcom/crashlytics/android/core/x$b;

    .line 34
    new-array v0, v3, [Lcom/crashlytics/android/core/x$c;

    sput-object v0, Lcom/crashlytics/android/core/x;->f:[Lcom/crashlytics/android/core/x$c;

    return-void
.end method

.method private static a(Lu;Lcom/crashlytics/android/core/s;Ljava/util/Map;)Lcom/crashlytics/android/core/x$e;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu;",
            "Lcom/crashlytics/android/core/s;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/crashlytics/android/core/x$e;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 501
    iget-object v0, p0, Lu;->b:Lv;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lu;->b:Lv;

    .line 503
    :goto_0
    new-instance v1, Lcom/crashlytics/android/core/x$l;

    invoke-direct {v1, v0}, Lcom/crashlytics/android/core/x$l;-><init>(Lv;)V

    .line 505
    iget-object v0, p0, Lu;->c:[Lw;

    invoke-static {v0}, Lcom/crashlytics/android/core/x;->a([Lw;)Lcom/crashlytics/android/core/x$k;

    move-result-object v0

    .line 506
    iget-object v2, p0, Lu;->d:[Lr;

    invoke-static {v2}, Lcom/crashlytics/android/core/x;->a([Lr;)Lcom/crashlytics/android/core/x$k;

    move-result-object v2

    .line 509
    new-instance v3, Lcom/crashlytics/android/core/x$f;

    invoke-direct {v3, v1, v0, v2}, Lcom/crashlytics/android/core/x$f;-><init>(Lcom/crashlytics/android/core/x$l;Lcom/crashlytics/android/core/x$k;Lcom/crashlytics/android/core/x$k;)V

    .line 511
    iget-object v0, p0, Lu;->e:[Ls;

    invoke-static {v0, p2}, Lcom/crashlytics/android/core/x;->a([Ls;Ljava/util/Map;)[Ls;

    move-result-object v0

    invoke-static {v0}, Lcom/crashlytics/android/core/x;->a([Ls;)Lcom/crashlytics/android/core/x$k;

    move-result-object v0

    .line 515
    new-instance v1, Lcom/crashlytics/android/core/x$a;

    invoke-direct {v1, v3, v0}, Lcom/crashlytics/android/core/x$a;-><init>(Lcom/crashlytics/android/core/x$f;Lcom/crashlytics/android/core/x$k;)V

    .line 517
    iget-object v0, p0, Lu;->f:Lt;

    invoke-static {v0}, Lcom/crashlytics/android/core/x;->a(Lt;)Lcom/crashlytics/android/core/x$j;

    move-result-object v2

    .line 519
    invoke-virtual {p1}, Lcom/crashlytics/android/core/s;->a()Lcom/crashlytics/android/core/a;

    move-result-object v3

    .line 521
    if-nez v3, :cond_0

    .line 522
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v0

    const-string/jumbo v4, "CrashlyticsCore"

    const-string/jumbo v5, "No log data to include with this event."

    invoke-interface {v0, v4, v5}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    :cond_0
    invoke-virtual {p1}, Lcom/crashlytics/android/core/s;->b()V

    .line 528
    if-eqz v3, :cond_2

    new-instance v0, Lcom/crashlytics/android/core/x$h;

    invoke-direct {v0, v3}, Lcom/crashlytics/android/core/x$h;-><init>(Lcom/crashlytics/android/core/a;)V

    .line 531
    :goto_1
    new-instance v3, Lcom/crashlytics/android/core/x$e;

    iget-wide v4, p0, Lu;->a:J

    const-string/jumbo v6, "ndk-crash"

    const/4 v7, 0x3

    new-array v7, v7, [Lcom/crashlytics/android/core/x$j;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    const/4 v1, 0x1

    aput-object v2, v7, v1

    const/4 v1, 0x2

    aput-object v0, v7, v1

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/crashlytics/android/core/x$e;-><init>(JLjava/lang/String;[Lcom/crashlytics/android/core/x$j;)V

    return-object v3

    .line 501
    :cond_1
    sget-object v0, Lcom/crashlytics/android/core/x;->a:Lv;

    goto :goto_0

    .line 528
    :cond_2
    new-instance v0, Lcom/crashlytics/android/core/x$i;

    invoke-direct {v0}, Lcom/crashlytics/android/core/x$i;-><init>()V

    goto :goto_1
.end method

.method private static a(Lt;)Lcom/crashlytics/android/core/x$j;
    .locals 12

    .prologue
    .line 557
    if-nez p0, :cond_0

    .line 558
    new-instance v1, Lcom/crashlytics/android/core/x$i;

    invoke-direct {v1}, Lcom/crashlytics/android/core/x$i;-><init>()V

    .line 560
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/crashlytics/android/core/x$d;

    iget v0, p0, Lt;->f:I

    int-to-float v0, v0

    const/high16 v2, 0x42c80000    # 100.0f

    div-float v2, v0, v2

    iget v3, p0, Lt;->g:I

    iget-boolean v4, p0, Lt;->h:Z

    iget v5, p0, Lt;->a:I

    iget-wide v6, p0, Lt;->b:J

    iget-wide v8, p0, Lt;->d:J

    sub-long/2addr v6, v8

    iget-wide v8, p0, Lt;->c:J

    iget-wide v10, p0, Lt;->e:J

    sub-long/2addr v8, v10

    invoke-direct/range {v1 .. v9}, Lcom/crashlytics/android/core/x$d;-><init>(FIZIJJ)V

    goto :goto_0
.end method

.method private static a([Lr;)Lcom/crashlytics/android/core/x$k;
    .locals 4

    .prologue
    .line 591
    if-eqz p0, :cond_0

    array-length v0, p0

    new-array v0, v0, [Lcom/crashlytics/android/core/x$b;

    .line 593
    :goto_0
    const/4 v1, 0x0

    :goto_1
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 594
    new-instance v2, Lcom/crashlytics/android/core/x$b;

    aget-object v3, p0, v1

    invoke-direct {v2, v3}, Lcom/crashlytics/android/core/x$b;-><init>(Lr;)V

    aput-object v2, v0, v1

    .line 593
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 591
    :cond_0
    sget-object v0, Lcom/crashlytics/android/core/x;->e:[Lcom/crashlytics/android/core/x$b;

    goto :goto_0

    .line 596
    :cond_1
    new-instance v1, Lcom/crashlytics/android/core/x$k;

    invoke-direct {v1, v0}, Lcom/crashlytics/android/core/x$k;-><init>([Lcom/crashlytics/android/core/x$j;)V

    return-object v1
.end method

.method private static a([Ls;)Lcom/crashlytics/android/core/x$k;
    .locals 4

    .prologue
    .line 601
    if-eqz p0, :cond_0

    array-length v0, p0

    new-array v0, v0, [Lcom/crashlytics/android/core/x$c;

    .line 604
    :goto_0
    const/4 v1, 0x0

    :goto_1
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 605
    new-instance v2, Lcom/crashlytics/android/core/x$c;

    aget-object v3, p0, v1

    invoke-direct {v2, v3}, Lcom/crashlytics/android/core/x$c;-><init>(Ls;)V

    aput-object v2, v0, v1

    .line 604
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 601
    :cond_0
    sget-object v0, Lcom/crashlytics/android/core/x;->f:[Lcom/crashlytics/android/core/x$c;

    goto :goto_0

    .line 607
    :cond_1
    new-instance v1, Lcom/crashlytics/android/core/x$k;

    invoke-direct {v1, v0}, Lcom/crashlytics/android/core/x$k;-><init>([Lcom/crashlytics/android/core/x$j;)V

    return-object v1
.end method

.method private static a([Lw$a;)Lcom/crashlytics/android/core/x$k;
    .locals 4

    .prologue
    .line 582
    if-eqz p0, :cond_0

    array-length v0, p0

    new-array v0, v0, [Lcom/crashlytics/android/core/x$g;

    .line 584
    :goto_0
    const/4 v1, 0x0

    :goto_1
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 585
    new-instance v2, Lcom/crashlytics/android/core/x$g;

    aget-object v3, p0, v1

    invoke-direct {v2, v3}, Lcom/crashlytics/android/core/x$g;-><init>(Lw$a;)V

    aput-object v2, v0, v1

    .line 584
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 582
    :cond_0
    sget-object v0, Lcom/crashlytics/android/core/x;->d:[Lcom/crashlytics/android/core/x$g;

    goto :goto_0

    .line 587
    :cond_1
    new-instance v1, Lcom/crashlytics/android/core/x$k;

    invoke-direct {v1, v0}, Lcom/crashlytics/android/core/x$k;-><init>([Lcom/crashlytics/android/core/x$j;)V

    return-object v1
.end method

.method private static a([Lw;)Lcom/crashlytics/android/core/x$k;
    .locals 5

    .prologue
    .line 571
    if-eqz p0, :cond_0

    array-length v0, p0

    new-array v0, v0, [Lcom/crashlytics/android/core/x$m;

    .line 573
    :goto_0
    const/4 v1, 0x0

    :goto_1
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 574
    aget-object v2, p0, v1

    .line 575
    new-instance v3, Lcom/crashlytics/android/core/x$m;

    iget-object v4, v2, Lw;->c:[Lw$a;

    invoke-static {v4}, Lcom/crashlytics/android/core/x;->a([Lw$a;)Lcom/crashlytics/android/core/x$k;

    move-result-object v4

    invoke-direct {v3, v2, v4}, Lcom/crashlytics/android/core/x$m;-><init>(Lw;Lcom/crashlytics/android/core/x$k;)V

    aput-object v3, v0, v1

    .line 573
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 571
    :cond_0
    sget-object v0, Lcom/crashlytics/android/core/x;->c:[Lcom/crashlytics/android/core/x$m;

    goto :goto_0

    .line 578
    :cond_1
    new-instance v1, Lcom/crashlytics/android/core/x$k;

    invoke-direct {v1, v0}, Lcom/crashlytics/android/core/x$k;-><init>([Lcom/crashlytics/android/core/x$j;)V

    return-object v1
.end method

.method public static a(Lu;Lcom/crashlytics/android/core/s;Ljava/util/Map;Lcom/crashlytics/android/core/CodedOutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu;",
            "Lcom/crashlytics/android/core/s;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/crashlytics/android/core/CodedOutputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 622
    invoke-static {p0, p1, p2}, Lcom/crashlytics/android/core/x;->a(Lu;Lcom/crashlytics/android/core/s;Ljava/util/Map;)Lcom/crashlytics/android/core/x$e;

    move-result-object v0

    .line 624
    invoke-virtual {v0, p3}, Lcom/crashlytics/android/core/x$e;->b(Lcom/crashlytics/android/core/CodedOutputStream;)V

    .line 625
    return-void
.end method

.method static synthetic a()[Lcom/crashlytics/android/core/x$j;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/crashlytics/android/core/x;->b:[Lcom/crashlytics/android/core/x$j;

    return-object v0
.end method

.method private static a([Ls;Ljava/util/Map;)[Ls;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ls;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)[",
            "Ls;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 538
    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2, p1}, Ljava/util/TreeMap;-><init>(Ljava/util/Map;)V

    .line 539
    if-eqz p0, :cond_0

    .line 540
    array-length v3, p0

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, p0, v0

    .line 541
    iget-object v5, v4, Ls;->a:Ljava/lang/String;

    iget-object v4, v4, Ls;->b:Ljava/lang/String;

    invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 540
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 546
    :cond_0
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    new-array v2, v2, [Ljava/util/Map$Entry;

    invoke-interface {v0, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/util/Map$Entry;

    .line 548
    array-length v2, v0

    new-array v4, v2, [Ls;

    move v3, v1

    .line 549
    :goto_1
    array-length v1, v4

    if-ge v3, v1, :cond_1

    .line 550
    new-instance v5, Ls;

    aget-object v1, v0, v3

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aget-object v2, v0, v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v5, v1, v2}, Ls;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v4, v3

    .line 549
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 552
    :cond_1
    return-object v4
.end method
