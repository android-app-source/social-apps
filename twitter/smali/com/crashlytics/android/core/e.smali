.class public Lcom/crashlytics/android/core/e;
.super Lio/fabric/sdk/android/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/crashlytics/android/core/e$c;,
        Lcom/crashlytics/android/core/e$b;,
        Lcom/crashlytics/android/core/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/fabric/sdk/android/h",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation

.annotation runtime Lio/fabric/sdk/android/services/concurrency/b;
    a = {
        Lq;
    }
.end annotation


# instance fields
.field private A:Lio/fabric/sdk/android/services/network/c;

.field private B:Lcom/crashlytics/android/core/f;

.field private C:Lq;

.field private final a:J

.field private final b:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/io/File;

.field private d:Lcsw;

.field private k:Lcom/crashlytics/android/core/g;

.field private l:Lcom/crashlytics/android/core/g;

.field private m:Lcom/crashlytics/android/core/i;

.field private n:Lcom/crashlytics/android/core/k;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:F

.field private y:Z

.field private final z:Lcom/crashlytics/android/core/y;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 200
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    invoke-direct {p0, v0, v2, v2, v1}, Lcom/crashlytics/android/core/e;-><init>(FLcom/crashlytics/android/core/i;Lcom/crashlytics/android/core/y;Z)V

    .line 201
    return-void
.end method

.method constructor <init>(FLcom/crashlytics/android/core/i;Lcom/crashlytics/android/core/y;Z)V
    .locals 6

    .prologue
    .line 205
    const-string/jumbo v0, "Crashlytics Exception Handler"

    invoke-static {v0}, Lio/fabric/sdk/android/services/common/l;->a(Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v5

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/crashlytics/android/core/e;-><init>(FLcom/crashlytics/android/core/i;Lcom/crashlytics/android/core/y;ZLjava/util/concurrent/ExecutorService;)V

    .line 207
    return-void
.end method

.method constructor <init>(FLcom/crashlytics/android/core/i;Lcom/crashlytics/android/core/y;ZLjava/util/concurrent/ExecutorService;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 214
    invoke-direct {p0}, Lio/fabric/sdk/android/h;-><init>()V

    .line 98
    iput-object v0, p0, Lcom/crashlytics/android/core/e;->o:Ljava/lang/String;

    .line 99
    iput-object v0, p0, Lcom/crashlytics/android/core/e;->p:Ljava/lang/String;

    .line 100
    iput-object v0, p0, Lcom/crashlytics/android/core/e;->q:Ljava/lang/String;

    .line 215
    iput p1, p0, Lcom/crashlytics/android/core/e;->x:F

    .line 216
    if-eqz p2, :cond_0

    :goto_0
    iput-object p2, p0, Lcom/crashlytics/android/core/e;->m:Lcom/crashlytics/android/core/i;

    .line 217
    iput-object p3, p0, Lcom/crashlytics/android/core/e;->z:Lcom/crashlytics/android/core/y;

    .line 218
    iput-boolean p4, p0, Lcom/crashlytics/android/core/e;->y:Z

    .line 219
    new-instance v0, Lcom/crashlytics/android/core/f;

    invoke-direct {v0, p5}, Lcom/crashlytics/android/core/f;-><init>(Ljava/util/concurrent/ExecutorService;)V

    iput-object v0, p0, Lcom/crashlytics/android/core/e;->B:Lcom/crashlytics/android/core/f;

    .line 221
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/crashlytics/android/core/e;->b:Ljava/util/concurrent/ConcurrentHashMap;

    .line 222
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/crashlytics/android/core/e;->a:J

    .line 223
    return-void

    .line 216
    :cond_0
    new-instance p2, Lcom/crashlytics/android/core/e$c;

    invoke-direct {p2, v0}, Lcom/crashlytics/android/core/e$c;-><init>(Lcom/crashlytics/android/core/e$1;)V

    goto :goto_0
.end method

.method static B()Lio/fabric/sdk/android/services/settings/p;
    .locals 1

    .prologue
    .line 1031
    invoke-static {}, Lio/fabric/sdk/android/services/settings/q;->a()Lio/fabric/sdk/android/services/settings/q;

    move-result-object v0

    invoke-virtual {v0}, Lio/fabric/sdk/android/services/settings/q;->b()Lio/fabric/sdk/android/services/settings/s;

    move-result-object v0

    .line 1032
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lio/fabric/sdk/android/services/settings/s;->b:Lio/fabric/sdk/android/services/settings/p;

    goto :goto_0
.end method

.method private J()V
    .locals 4

    .prologue
    .line 789
    new-instance v1, Lcom/crashlytics/android/core/e$1;

    invoke-direct {v1, p0}, Lcom/crashlytics/android/core/e$1;-><init>(Lcom/crashlytics/android/core/e;)V

    .line 801
    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->I()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/fabric/sdk/android/services/concurrency/i;

    .line 802
    invoke-virtual {v1, v0}, Lio/fabric/sdk/android/services/concurrency/d;->a(Lio/fabric/sdk/android/services/concurrency/i;)V

    goto :goto_0

    .line 805
    :cond_0
    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->F()Lio/fabric/sdk/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lio/fabric/sdk/android/c;->f()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 807
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v1

    const-string/jumbo v2, "CrashlyticsCore"

    const-string/jumbo v3, "Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously."

    invoke-interface {v1, v2, v3}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    const-wide/16 v2, 0x4

    :try_start_0
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v1}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    .line 820
    :goto_1
    return-void

    .line 813
    :catch_0
    move-exception v0

    .line 814
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v1

    const-string/jumbo v2, "CrashlyticsCore"

    const-string/jumbo v3, "Crashlytics was interrupted during initialization."

    invoke-interface {v1, v2, v3, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 815
    :catch_1
    move-exception v0

    .line 816
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v1

    const-string/jumbo v2, "CrashlyticsCore"

    const-string/jumbo v3, "Problem encountered during Crashlytics initialization."

    invoke-interface {v1, v2, v3, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 817
    :catch_2
    move-exception v0

    .line 818
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v1

    const-string/jumbo v2, "CrashlyticsCore"

    const-string/jumbo v3, "Crashlytics timed out during initialization."

    invoke-interface {v1, v2, v3, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private K()V
    .locals 4

    .prologue
    .line 971
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->B:Lcom/crashlytics/android/core/f;

    new-instance v1, Lcom/crashlytics/android/core/e$b;

    iget-object v2, p0, Lcom/crashlytics/android/core/e;->l:Lcom/crashlytics/android/core/g;

    invoke-direct {v1, v2}, Lcom/crashlytics/android/core/e$b;-><init>(Lcom/crashlytics/android/core/g;)V

    invoke-virtual {v0, v1}, Lcom/crashlytics/android/core/f;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 976
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 987
    :goto_0
    return-void

    .line 981
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->m:Lcom/crashlytics/android/core/i;

    invoke-interface {v0}, Lcom/crashlytics/android/core/i;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 982
    :catch_0
    move-exception v0

    .line 983
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v1

    const-string/jumbo v2, "CrashlyticsCore"

    const-string/jumbo v3, "Exception thrown by CrashlyticsListener while notifying of previous crash."

    invoke-interface {v1, v2, v3, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/crashlytics/android/core/e;)Lcom/crashlytics/android/core/g;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->k:Lcom/crashlytics/android/core/g;

    return-object v0
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 470
    iget-boolean v0, p0, Lcom/crashlytics/android/core/e;->y:Z

    if-eqz v0, :cond_1

    .line 480
    :cond_0
    :goto_0
    return-void

    .line 474
    :cond_1
    const-string/jumbo v0, "prior to logging messages."

    invoke-static {v0}, Lcom/crashlytics/android/core/e;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 478
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/crashlytics/android/core/e;->a:J

    sub-long/2addr v0, v2

    .line 479
    iget-object v2, p0, Lcom/crashlytics/android/core/e;->n:Lcom/crashlytics/android/core/k;

    invoke-static {p1, p2, p3}, Lcom/crashlytics/android/core/e;->b(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v1, v3}, Lcom/crashlytics/android/core/k;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method private a(Landroid/app/Activity;Lio/fabric/sdk/android/services/settings/o;)Z
    .locals 4

    .prologue
    .line 1000
    new-instance v0, Lcom/crashlytics/android/core/e$7;

    invoke-direct {v0, p0}, Lcom/crashlytics/android/core/e$7;-><init>(Lcom/crashlytics/android/core/e;)V

    .line 1008
    invoke-static {p1, p2, v0}, Lcom/crashlytics/android/core/d;->a(Landroid/app/Activity;Lio/fabric/sdk/android/services/settings/o;Lcom/crashlytics/android/core/d$a;)Lcom/crashlytics/android/core/d;

    move-result-object v0

    .line 1011
    new-instance v1, Lcom/crashlytics/android/core/e$8;

    invoke-direct {v1, p0, v0}, Lcom/crashlytics/android/core/e$8;-><init>(Lcom/crashlytics/android/core/e;Lcom/crashlytics/android/core/d;)V

    invoke-virtual {p1, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1018
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v1

    const-string/jumbo v2, "CrashlyticsCore"

    const-string/jumbo v3, "Waiting for user opt-in."

    invoke-interface {v1, v2, v3}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1019
    invoke-virtual {v0}, Lcom/crashlytics/android/core/d;->b()V

    .line 1020
    invoke-virtual {v0}, Lcom/crashlytics/android/core/d;->c()Z

    move-result v0

    return v0
.end method

.method private a(Lcom/crashlytics/android/core/ah;)Z
    .locals 7

    .prologue
    .line 322
    :try_start_0
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v0

    const-string/jumbo v1, "CrashlyticsCore"

    const-string/jumbo v2, "Installing exception handler..."

    invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    new-instance v0, Lcom/crashlytics/android/core/k;

    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/crashlytics/android/core/e;->B:Lcom/crashlytics/android/core/f;

    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->D()Lio/fabric/sdk/android/services/common/IdManager;

    move-result-object v3

    iget-object v5, p0, Lcom/crashlytics/android/core/e;->d:Lcsw;

    move-object v4, p1

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/crashlytics/android/core/k;-><init>(Ljava/lang/Thread$UncaughtExceptionHandler;Lcom/crashlytics/android/core/f;Lio/fabric/sdk/android/services/common/IdManager;Lcom/crashlytics/android/core/ah;Lcsw;Lcom/crashlytics/android/core/e;)V

    iput-object v0, p0, Lcom/crashlytics/android/core/e;->n:Lcom/crashlytics/android/core/k;

    .line 334
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->n:Lcom/crashlytics/android/core/k;

    invoke-virtual {v0}, Lcom/crashlytics/android/core/k;->c()V

    .line 336
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->n:Lcom/crashlytics/android/core/k;

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 337
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v0

    const-string/jumbo v1, "CrashlyticsCore"

    const-string/jumbo v2, "Successfully installed exception handler."

    invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 338
    const/4 v0, 0x1

    .line 344
    :goto_0
    return v0

    .line 339
    :catch_0
    move-exception v0

    .line 340
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v1

    const-string/jumbo v2, "CrashlyticsCore"

    const-string/jumbo v3, "There was a problem installing the exception handler."

    invoke-interface {v1, v2, v3, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 342
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/crashlytics/android/core/e;->n:Lcom/crashlytics/android/core/k;

    .line 344
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/crashlytics/android/core/e;Landroid/app/Activity;Lio/fabric/sdk/android/services/settings/o;)Z
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/crashlytics/android/core/e;->a(Landroid/app/Activity;Lio/fabric/sdk/android/services/settings/o;)Z

    move-result v0

    return v0
.end method

.method static a(Ljava/lang/String;Z)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1088
    if-nez p1, :cond_1

    .line 1089
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v1

    const-string/jumbo v2, "CrashlyticsCore"

    const-string/jumbo v3, "Configured not to require a build ID."

    invoke-interface {v1, v2, v3}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117
    :cond_0
    :goto_0
    return v0

    .line 1093
    :cond_1
    invoke-static {p0}, Lio/fabric/sdk/android/services/common/CommonUtils;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1097
    const-string/jumbo v0, "CrashlyticsCore"

    const-string/jumbo v1, "."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1098
    const-string/jumbo v0, "CrashlyticsCore"

    const-string/jumbo v1, ".     |  | "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1099
    const-string/jumbo v0, "CrashlyticsCore"

    const-string/jumbo v1, ".     |  |"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1100
    const-string/jumbo v0, "CrashlyticsCore"

    const-string/jumbo v1, ".     |  |"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1101
    const-string/jumbo v0, "CrashlyticsCore"

    const-string/jumbo v1, ".   \\ |  | /"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1102
    const-string/jumbo v0, "CrashlyticsCore"

    const-string/jumbo v1, ".    \\    /"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1103
    const-string/jumbo v0, "CrashlyticsCore"

    const-string/jumbo v1, ".     \\  /"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1104
    const-string/jumbo v0, "CrashlyticsCore"

    const-string/jumbo v1, ".      \\/"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1105
    const-string/jumbo v0, "CrashlyticsCore"

    const-string/jumbo v1, "."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1106
    const-string/jumbo v0, "CrashlyticsCore"

    const-string/jumbo v1, "This app relies on Crashlytics. Please sign up for access at https://fabric.io/sign_up,\ninstall an Android build tool and ask a team member to invite you to this app\'s organization."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1107
    const-string/jumbo v0, "CrashlyticsCore"

    const-string/jumbo v1, "."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1108
    const-string/jumbo v0, "CrashlyticsCore"

    const-string/jumbo v1, ".      /\\"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1109
    const-string/jumbo v0, "CrashlyticsCore"

    const-string/jumbo v1, ".     /  \\"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1110
    const-string/jumbo v0, "CrashlyticsCore"

    const-string/jumbo v1, ".    /    \\"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1111
    const-string/jumbo v0, "CrashlyticsCore"

    const-string/jumbo v1, ".   / |  | \\"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1112
    const-string/jumbo v0, "CrashlyticsCore"

    const-string/jumbo v1, ".     |  |"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1113
    const-string/jumbo v0, "CrashlyticsCore"

    const-string/jumbo v1, ".     |  |"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1114
    const-string/jumbo v0, "CrashlyticsCore"

    const-string/jumbo v1, ".     |  |"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1115
    const-string/jumbo v0, "CrashlyticsCore"

    const-string/jumbo v1, "."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1117
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private static b(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1036
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lio/fabric/sdk/android/services/common/CommonUtils;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/content/Context;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    .prologue
    .line 304
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->z:Lcom/crashlytics/android/core/y;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/crashlytics/android/core/j;

    iget-object v1, p0, Lcom/crashlytics/android/core/e;->z:Lcom/crashlytics/android/core/y;

    invoke-direct {v0, v1}, Lcom/crashlytics/android/core/j;-><init>(Lcom/crashlytics/android/core/y;)V

    .line 306
    :goto_0
    new-instance v1, Lio/fabric/sdk/android/services/network/b;

    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v2

    invoke-direct {v1, v2}, Lio/fabric/sdk/android/services/network/b;-><init>(Lio/fabric/sdk/android/k;)V

    iput-object v1, p0, Lcom/crashlytics/android/core/e;->A:Lio/fabric/sdk/android/services/network/c;

    .line 307
    iget-object v1, p0, Lcom/crashlytics/android/core/e;->A:Lio/fabric/sdk/android/services/network/c;

    invoke-interface {v1, v0}, Lio/fabric/sdk/android/services/network/c;->a(Lio/fabric/sdk/android/services/network/e;)V

    .line 309
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/crashlytics/android/core/e;->s:Ljava/lang/String;

    .line 310
    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->D()Lio/fabric/sdk/android/services/common/IdManager;

    move-result-object v0

    invoke-virtual {v0}, Lio/fabric/sdk/android/services/common/IdManager;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/crashlytics/android/core/e;->u:Ljava/lang/String;

    .line 311
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v0

    const-string/jumbo v1, "CrashlyticsCore"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Installer package name is: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/crashlytics/android/core/e;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 314
    iget-object v1, p0, Lcom/crashlytics/android/core/e;->s:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 315
    iget v1, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/crashlytics/android/core/e;->v:Ljava/lang/String;

    .line 316
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-nez v1, :cond_1

    const-string/jumbo v0, "0.0"

    :goto_1
    iput-object v0, p0, Lcom/crashlytics/android/core/e;->w:Ljava/lang/String;

    .line 318
    return-void

    .line 304
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 316
    :cond_1
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    goto :goto_1
.end method

.method static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 702
    const-class v0, Lcom/crashlytics/android/answers/a;

    invoke-static {v0}, Lio/fabric/sdk/android/c;->a(Ljava/lang/Class;)Lio/fabric/sdk/android/h;

    move-result-object v0

    check-cast v0, Lcom/crashlytics/android/answers/a;

    .line 703
    if-eqz v0, :cond_0

    .line 704
    new-instance v1, Lio/fabric/sdk/android/services/common/i$b;

    invoke-direct {v1, p0, p1}, Lio/fabric/sdk/android/services/common/i$b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/crashlytics/android/answers/a;->a(Lio/fabric/sdk/android/services/common/i$b;)V

    .line 707
    :cond_0
    return-void
.end method

.method static c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 713
    const-class v0, Lcom/crashlytics/android/answers/a;

    invoke-static {v0}, Lio/fabric/sdk/android/c;->a(Ljava/lang/Class;)Lio/fabric/sdk/android/h;

    move-result-object v0

    check-cast v0, Lcom/crashlytics/android/answers/a;

    .line 714
    if-eqz v0, :cond_0

    .line 715
    new-instance v1, Lio/fabric/sdk/android/services/common/i$a;

    invoke-direct {v1, p0, p1}, Lio/fabric/sdk/android/services/common/i$a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/crashlytics/android/answers/a;->a(Lio/fabric/sdk/android/services/common/i$a;)V

    .line 718
    :cond_0
    return-void
.end method

.method private static c(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 1040
    invoke-static {}, Lcom/crashlytics/android/core/e;->e()Lcom/crashlytics/android/core/e;

    move-result-object v0

    .line 1041
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/crashlytics/android/core/e;->n:Lcom/crashlytics/android/core/k;

    if-nez v0, :cond_1

    .line 1042
    :cond_0
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v0

    const-string/jumbo v1, "CrashlyticsCore"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Crashlytics must be initialized by calling Fabric.with(Context) "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1045
    const/4 v0, 0x0

    .line 1047
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    const/16 v1, 0x400

    .line 1052
    if-eqz p0, :cond_0

    .line 1053
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 1054
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 1055
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 1058
    :cond_0
    return-object p0
.end method

.method public static e()Lcom/crashlytics/android/core/e;
    .locals 1

    .prologue
    .line 422
    const-class v0, Lcom/crashlytics/android/core/e;

    invoke-static {v0}, Lio/fabric/sdk/android/c;->a(Ljava/lang/Class;)Lio/fabric/sdk/android/h;

    move-result-object v0

    check-cast v0, Lcom/crashlytics/android/core/e;

    return-object v0
.end method


# virtual methods
.method A()V
    .locals 1

    .prologue
    .line 994
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->l:Lcom/crashlytics/android/core/g;

    invoke-virtual {v0}, Lcom/crashlytics/android/core/g;->a()Z

    .line 995
    return-void
.end method

.method a(Lio/fabric/sdk/android/services/settings/s;)Lcom/crashlytics/android/core/m;
    .locals 4

    .prologue
    .line 959
    if-eqz p1, :cond_0

    .line 960
    new-instance v0, Lcom/crashlytics/android/core/n;

    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->m()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lio/fabric/sdk/android/services/settings/s;->a:Lio/fabric/sdk/android/services/settings/e;

    iget-object v2, v2, Lio/fabric/sdk/android/services/settings/e;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/crashlytics/android/core/e;->A:Lio/fabric/sdk/android/services/network/c;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/crashlytics/android/core/n;-><init>(Lio/fabric/sdk/android/h;Ljava/lang/String;Ljava/lang/String;Lio/fabric/sdk/android/services/network/c;)V

    .line 964
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 411
    const-string/jumbo v0, "2.3.14.151"

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 466
    const/4 v0, 0x3

    const-string/jumbo v1, "CrashlyticsCore"

    invoke-direct {p0, v0, v1, p1}, Lcom/crashlytics/android/core/e;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 467
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 581
    iget-boolean v0, p0, Lcom/crashlytics/android/core/e;->y:Z

    if-eqz v0, :cond_1

    .line 611
    :cond_0
    :goto_0
    return-void

    .line 585
    :cond_1
    const-string/jumbo v0, "prior to setting keys."

    invoke-static {v0}, Lcom/crashlytics/android/core/e;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589
    if-nez p1, :cond_3

    .line 590
    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->E()Landroid/content/Context;

    move-result-object v0

    .line 591
    if-eqz v0, :cond_2

    invoke-static {v0}, Lio/fabric/sdk/android/services/common/CommonUtils;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 592
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Custom attribute key must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 594
    :cond_2
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v0

    const-string/jumbo v1, "CrashlyticsCore"

    const-string/jumbo v2, "Attempting to set custom attribute with null key, ignoring."

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 600
    :cond_3
    invoke-static {p1}, Lcom/crashlytics/android/core/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 602
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    const/16 v2, 0x40

    if-lt v0, v2, :cond_4

    iget-object v0, p0, Lcom/crashlytics/android/core/e;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 603
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v0

    const-string/jumbo v1, "CrashlyticsCore"

    const-string/jumbo v2, "Exceeded maximum number of custom attributes (64)"

    invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 608
    :cond_4
    if-nez p2, :cond_5

    const-string/jumbo v0, ""

    .line 609
    :goto_1
    iget-object v2, p0, Lcom/crashlytics/android/core/e;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 610
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->n:Lcom/crashlytics/android/core/k;

    iget-object v1, p0, Lcom/crashlytics/android/core/e;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v1}, Lcom/crashlytics/android/core/k;->a(Ljava/util/Map;)V

    goto :goto_0

    .line 608
    :cond_5
    invoke-static {p2}, Lcom/crashlytics/android/core/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 438
    iget-boolean v0, p0, Lcom/crashlytics/android/core/e;->y:Z

    if-eqz v0, :cond_1

    .line 453
    :cond_0
    :goto_0
    return-void

    .line 442
    :cond_1
    const-string/jumbo v0, "prior to logging exceptions."

    invoke-static {v0}, Lcom/crashlytics/android/core/e;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446
    if-nez p1, :cond_2

    .line 447
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v0

    const/4 v1, 0x5

    const-string/jumbo v2, "CrashlyticsCore"

    const-string/jumbo v3, "Crashlytics is ignoring a request to log a null exception."

    invoke-interface {v0, v1, v2, v3}, Lio/fabric/sdk/android/k;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 452
    :cond_2
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->n:Lcom/crashlytics/android/core/k;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/crashlytics/android/core/k;->a(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method a(Lq;)V
    .locals 0

    .prologue
    .line 873
    iput-object p1, p0, Lcom/crashlytics/android/core/e;->C:Lq;

    .line 874
    return-void
.end method

.method a(Z)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitPrefEdits"
        }
    .end annotation

    .prologue
    .line 933
    new-instance v0, Lcsz;

    invoke-direct {v0, p0}, Lcsz;-><init>(Lio/fabric/sdk/android/h;)V

    .line 934
    invoke-interface {v0}, Lcsy;->b()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "always_send_reports_opt_in"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v0, v1}, Lcsy;->a(Landroid/content/SharedPreferences$Editor;)Z

    .line 935
    return-void
.end method

.method a(Landroid/content/Context;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 239
    iget-boolean v2, p0, Lcom/crashlytics/android/core/e;->y:Z

    if-eqz v2, :cond_1

    .line 299
    :cond_0
    :goto_0
    return v0

    .line 244
    :cond_1
    new-instance v2, Lio/fabric/sdk/android/services/common/g;

    invoke-direct {v2}, Lio/fabric/sdk/android/services/common/g;-><init>()V

    invoke-virtual {v2, p1}, Lio/fabric/sdk/android/services/common/g;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/crashlytics/android/core/e;->t:Ljava/lang/String;

    .line 246
    iget-object v2, p0, Lcom/crashlytics/android/core/e;->t:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 252
    invoke-static {p1}, Lio/fabric/sdk/android/services/common/CommonUtils;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/crashlytics/android/core/e;->r:Ljava/lang/String;

    .line 255
    const-string/jumbo v2, "com.crashlytics.RequireBuildId"

    invoke-static {p1, v2, v1}, Lio/fabric/sdk/android/services/common/CommonUtils;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v2

    .line 258
    iget-object v3, p0, Lcom/crashlytics/android/core/e;->r:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/crashlytics/android/core/e;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_2

    .line 259
    new-instance v0, Lio/fabric/sdk/android/services/concurrency/UnmetDependencyException;

    const-string/jumbo v1, "This app relies on Crashlytics. Please sign up for access at https://fabric.io/sign_up,\ninstall an Android build tool and ask a team member to invite you to this app\'s organization."

    invoke-direct {v0, v1}, Lio/fabric/sdk/android/services/concurrency/UnmetDependencyException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 262
    :cond_2
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v2

    const-string/jumbo v3, "CrashlyticsCore"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Initializing Crashlytics "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lio/fabric/sdk/android/k;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    new-instance v2, Lcsx;

    invoke-direct {v2, p0}, Lcsx;-><init>(Lio/fabric/sdk/android/h;)V

    iput-object v2, p0, Lcom/crashlytics/android/core/e;->d:Lcsw;

    .line 265
    new-instance v2, Lcom/crashlytics/android/core/g;

    const-string/jumbo v3, "crash_marker"

    iget-object v4, p0, Lcom/crashlytics/android/core/e;->d:Lcsw;

    invoke-direct {v2, v3, v4}, Lcom/crashlytics/android/core/g;-><init>(Ljava/lang/String;Lcsw;)V

    iput-object v2, p0, Lcom/crashlytics/android/core/e;->l:Lcom/crashlytics/android/core/g;

    .line 266
    new-instance v2, Lcom/crashlytics/android/core/g;

    const-string/jumbo v3, "initialization_marker"

    iget-object v4, p0, Lcom/crashlytics/android/core/e;->d:Lcsw;

    invoke-direct {v2, v3, v4}, Lcom/crashlytics/android/core/g;-><init>(Ljava/lang/String;Lcsw;)V

    iput-object v2, p0, Lcom/crashlytics/android/core/e;->k:Lcom/crashlytics/android/core/g;

    .line 270
    :try_start_0
    invoke-direct {p0, p1}, Lcom/crashlytics/android/core/e;->b(Landroid/content/Context;)V

    .line 272
    new-instance v2, Lcom/crashlytics/android/core/t;

    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->h()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Lcom/crashlytics/android/core/t;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 279
    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->u()Z

    move-result v3

    .line 281
    invoke-direct {p0}, Lcom/crashlytics/android/core/e;->K()V

    .line 283
    invoke-direct {p0, v2}, Lcom/crashlytics/android/core/e;->a(Lcom/crashlytics/android/core/ah;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 287
    if-eqz v3, :cond_3

    invoke-static {p1}, Lio/fabric/sdk/android/services/common/CommonUtils;->n(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 290
    invoke-direct {p0}, Lcom/crashlytics/android/core/e;->J()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 295
    :catch_0
    move-exception v1

    .line 296
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v2

    const-string/jumbo v3, "CrashlyticsCore"

    const-string/jumbo v4, "Crashlytics was not started due to an exception during initialization"

    invoke-interface {v2, v3, v4, v1}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_3
    move v0, v1

    .line 294
    goto/16 :goto_0
.end method

.method protected a_()Z
    .locals 1

    .prologue
    .line 231
    invoke-super {p0}, Lio/fabric/sdk/android/h;->E()Landroid/content/Context;

    move-result-object v0

    .line 232
    invoke-virtual {p0, v0}, Lcom/crashlytics/android/core/e;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 406
    const-string/jumbo v0, "com.crashlytics.sdk.android.crashlytics-core"

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 542
    iget-boolean v0, p0, Lcom/crashlytics/android/core/e;->y:Z

    if-eqz v0, :cond_1

    .line 552
    :cond_0
    :goto_0
    return-void

    .line 546
    :cond_1
    const-string/jumbo v0, "prior to setting user data."

    invoke-static {v0}, Lcom/crashlytics/android/core/e;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 550
    invoke-static {p1}, Lcom/crashlytics/android/core/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/crashlytics/android/core/e;->q:Ljava/lang/String;

    .line 551
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->n:Lcom/crashlytics/android/core/k;

    iget-object v1, p0, Lcom/crashlytics/android/core/e;->o:Ljava/lang/String;

    iget-object v2, p0, Lcom/crashlytics/android/core/e;->q:Ljava/lang/String;

    iget-object v3, p0, Lcom/crashlytics/android/core/e;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/crashlytics/android/core/k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected d()Ljava/lang/Void;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 350
    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->s()V

    .line 352
    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->v()Lu;

    move-result-object v0

    .line 353
    if-eqz v0, :cond_0

    .line 354
    iget-object v1, p0, Lcom/crashlytics/android/core/e;->n:Lcom/crashlytics/android/core/k;

    invoke-virtual {v1, v0}, Lcom/crashlytics/android/core/k;->a(Lu;)V

    .line 357
    :cond_0
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->n:Lcom/crashlytics/android/core/k;

    invoke-virtual {v0}, Lcom/crashlytics/android/core/k;->h()V

    .line 360
    :try_start_0
    invoke-static {}, Lio/fabric/sdk/android/services/settings/q;->a()Lio/fabric/sdk/android/services/settings/q;

    move-result-object v0

    invoke-virtual {v0}, Lio/fabric/sdk/android/services/settings/q;->b()Lio/fabric/sdk/android/services/settings/s;

    move-result-object v0

    .line 362
    if-nez v0, :cond_1

    .line 363
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v0

    const-string/jumbo v1, "CrashlyticsCore"

    const-string/jumbo v2, "Received null settings, skipping initialization!"

    invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/k;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 394
    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->t()V

    .line 397
    :goto_0
    return-object v4

    .line 367
    :cond_1
    :try_start_1
    iget-object v1, v0, Lio/fabric/sdk/android/services/settings/s;->d:Lio/fabric/sdk/android/services/settings/m;

    iget-boolean v1, v1, Lio/fabric/sdk/android/services/settings/m;->c:Z

    if-nez v1, :cond_2

    .line 368
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v0

    const-string/jumbo v1, "CrashlyticsCore"

    const-string/jumbo v2, "Collection of crash reports disabled in Crashlytics settings."

    invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 394
    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->t()V

    goto :goto_0

    .line 373
    :cond_2
    :try_start_2
    iget-object v1, p0, Lcom/crashlytics/android/core/e;->n:Lcom/crashlytics/android/core/k;

    invoke-virtual {v1}, Lcom/crashlytics/android/core/k;->d()Z

    .line 375
    invoke-virtual {p0, v0}, Lcom/crashlytics/android/core/e;->a(Lio/fabric/sdk/android/services/settings/s;)Lcom/crashlytics/android/core/m;

    move-result-object v0

    .line 376
    if-nez v0, :cond_3

    .line 377
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v0

    const-string/jumbo v1, "CrashlyticsCore"

    const-string/jumbo v2, "Unable to create a call to upload reports."

    invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/k;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 394
    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->t()V

    goto :goto_0

    .line 381
    :cond_3
    :try_start_3
    new-instance v1, Lcom/crashlytics/android/core/ac;

    iget-object v2, p0, Lcom/crashlytics/android/core/e;->t:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/crashlytics/android/core/ac;-><init>(Ljava/lang/String;Lcom/crashlytics/android/core/m;)V

    iget v0, p0, Lcom/crashlytics/android/core/e;->x:F

    invoke-virtual {v1, v0}, Lcom/crashlytics/android/core/ac;->a(F)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 394
    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->t()V

    goto :goto_0

    .line 382
    :catch_0
    move-exception v0

    .line 383
    :try_start_4
    invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;

    move-result-object v1

    const-string/jumbo v2, "CrashlyticsCore"

    const-string/jumbo v3, "Crashlytics encountered a problem during asynchronous initialization."

    invoke-interface {v1, v2, v3, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 394
    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->t()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->t()V

    throw v0
.end method

.method protected synthetic f()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->d()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method g()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 726
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 730
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->s:Ljava/lang/String;

    return-object v0
.end method

.method i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 734
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->t:Ljava/lang/String;

    return-object v0
.end method

.method j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 744
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->u:Ljava/lang/String;

    return-object v0
.end method

.method k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 748
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->w:Ljava/lang/String;

    return-object v0
.end method

.method l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 752
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->v:Ljava/lang/String;

    return-object v0
.end method

.method m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 756
    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->E()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "com.crashlytics.ApiEndpoint"

    invoke-static {v0, v1}, Lio/fabric/sdk/android/services/common/CommonUtils;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 760
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->r:Ljava/lang/String;

    return-object v0
.end method

.method o()Lcom/crashlytics/android/core/k;
    .locals 1

    .prologue
    .line 764
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->n:Lcom/crashlytics/android/core/k;

    return-object v0
.end method

.method p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 768
    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->D()Lio/fabric/sdk/android/services/common/IdManager;

    move-result-object v0

    invoke-virtual {v0}, Lio/fabric/sdk/android/services/common/IdManager;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/crashlytics/android/core/e;->o:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 772
    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->D()Lio/fabric/sdk/android/services/common/IdManager;

    move-result-object v0

    invoke-virtual {v0}, Lio/fabric/sdk/android/services/common/IdManager;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/crashlytics/android/core/e;->p:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 776
    invoke-virtual {p0}, Lcom/crashlytics/android/core/e;->D()Lio/fabric/sdk/android/services/common/IdManager;

    move-result-object v0

    invoke-virtual {v0}, Lio/fabric/sdk/android/services/common/IdManager;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/crashlytics/android/core/e;->q:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method s()V
    .locals 2

    .prologue
    .line 826
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->B:Lcom/crashlytics/android/core/f;

    new-instance v1, Lcom/crashlytics/android/core/e$2;

    invoke-direct {v1, p0}, Lcom/crashlytics/android/core/e$2;-><init>(Lcom/crashlytics/android/core/e;)V

    invoke-virtual {v0, v1}, Lcom/crashlytics/android/core/f;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    .line 836
    return-void
.end method

.method t()V
    .locals 2

    .prologue
    .line 842
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->B:Lcom/crashlytics/android/core/f;

    new-instance v1, Lcom/crashlytics/android/core/e$3;

    invoke-direct {v1, p0}, Lcom/crashlytics/android/core/e$3;-><init>(Lcom/crashlytics/android/core/e;)V

    invoke-virtual {v0, v1}, Lcom/crashlytics/android/core/f;->b(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    .line 857
    return-void
.end method

.method u()Z
    .locals 2

    .prologue
    .line 860
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->B:Lcom/crashlytics/android/core/f;

    new-instance v1, Lcom/crashlytics/android/core/e$4;

    invoke-direct {v1, p0}, Lcom/crashlytics/android/core/e$4;-><init>(Lcom/crashlytics/android/core/e;)V

    invoke-virtual {v0, v1}, Lcom/crashlytics/android/core/f;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method v()Lu;
    .locals 2

    .prologue
    .line 881
    const/4 v0, 0x0

    .line 882
    iget-object v1, p0, Lcom/crashlytics/android/core/e;->C:Lq;

    if-eqz v1, :cond_0

    .line 883
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->C:Lq;

    invoke-interface {v0}, Lq;->d()Lu;

    move-result-object v0

    .line 885
    :cond_0
    return-object v0
.end method

.method w()Ljava/io/File;
    .locals 1

    .prologue
    .line 907
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->c:Ljava/io/File;

    if-nez v0, :cond_0

    .line 908
    new-instance v0, Lcsx;

    invoke-direct {v0, p0}, Lcsx;-><init>(Lio/fabric/sdk/android/h;)V

    invoke-virtual {v0}, Lcsx;->a()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/crashlytics/android/core/e;->c:Ljava/io/File;

    .line 910
    :cond_0
    iget-object v0, p0, Lcom/crashlytics/android/core/e;->c:Ljava/io/File;

    return-object v0
.end method

.method x()Z
    .locals 3

    .prologue
    .line 914
    invoke-static {}, Lio/fabric/sdk/android/services/settings/q;->a()Lio/fabric/sdk/android/services/settings/q;

    move-result-object v0

    new-instance v1, Lcom/crashlytics/android/core/e$5;

    invoke-direct {v1, p0}, Lcom/crashlytics/android/core/e$5;-><init>(Lcom/crashlytics/android/core/e;)V

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/fabric/sdk/android/services/settings/q;->a(Lio/fabric/sdk/android/services/settings/q$b;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method y()Z
    .locals 3

    .prologue
    .line 927
    new-instance v0, Lcsz;

    invoke-direct {v0, p0}, Lcsz;-><init>(Lio/fabric/sdk/android/h;)V

    .line 928
    invoke-interface {v0}, Lcsy;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "always_send_reports_opt_in"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method z()Z
    .locals 3

    .prologue
    .line 938
    invoke-static {}, Lio/fabric/sdk/android/services/settings/q;->a()Lio/fabric/sdk/android/services/settings/q;

    move-result-object v0

    new-instance v1, Lcom/crashlytics/android/core/e$6;

    invoke-direct {v1, p0}, Lcom/crashlytics/android/core/e$6;-><init>(Lcom/crashlytics/android/core/e;)V

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/fabric/sdk/android/services/settings/q;->a(Lio/fabric/sdk/android/services/settings/q$b;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
