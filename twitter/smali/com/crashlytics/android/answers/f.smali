.class Lcom/crashlytics/android/answers/f;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcss;


# instance fields
.field private final a:Lcom/crashlytics/android/answers/p;

.field private final b:Lcom/crashlytics/android/answers/m;


# direct methods
.method constructor <init>(Lcom/crashlytics/android/answers/p;Lcom/crashlytics/android/answers/m;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/crashlytics/android/answers/f;->a:Lcom/crashlytics/android/answers/p;

    .line 42
    iput-object p2, p0, Lcom/crashlytics/android/answers/f;->b:Lcom/crashlytics/android/answers/m;

    .line 43
    return-void
.end method

.method public static a(Lcom/crashlytics/android/answers/p;)Lcom/crashlytics/android/answers/f;
    .locals 5

    .prologue
    .line 31
    new-instance v0, Lcom/crashlytics/android/answers/l;

    new-instance v1, Lcsk;

    const-wide/16 v2, 0x3e8

    const/16 v4, 0x8

    invoke-direct {v1, v2, v3, v4}, Lcsk;-><init>(JI)V

    const-wide v2, 0x3fb999999999999aL    # 0.1

    invoke-direct {v0, v1, v2, v3}, Lcom/crashlytics/android/answers/l;-><init>(Lcsi;D)V

    .line 33
    new-instance v1, Lcsj;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Lcsj;-><init>(I)V

    .line 34
    new-instance v2, Lcsm;

    invoke-direct {v2, v0, v1}, Lcsm;-><init>(Lcsi;Lcsl;)V

    .line 35
    new-instance v0, Lcom/crashlytics/android/answers/m;

    invoke-direct {v0, v2}, Lcom/crashlytics/android/answers/m;-><init>(Lcsm;)V

    .line 36
    new-instance v1, Lcom/crashlytics/android/answers/f;

    invoke-direct {v1, p0, v0}, Lcom/crashlytics/android/answers/f;-><init>(Lcom/crashlytics/android/answers/p;Lcom/crashlytics/android/answers/m;)V

    return-object v1
.end method


# virtual methods
.method public a(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 48
    iget-object v1, p0, Lcom/crashlytics/android/answers/f;->b:Lcom/crashlytics/android/answers/m;

    invoke-virtual {v1, v2, v3}, Lcom/crashlytics/android/answers/m;->a(J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    iget-object v1, p0, Lcom/crashlytics/android/answers/f;->a:Lcom/crashlytics/android/answers/p;

    invoke-virtual {v1, p1}, Lcom/crashlytics/android/answers/p;->a(Ljava/util/List;)Z

    move-result v1

    .line 50
    if-eqz v1, :cond_1

    .line 51
    iget-object v0, p0, Lcom/crashlytics/android/answers/f;->b:Lcom/crashlytics/android/answers/m;

    invoke-virtual {v0}, Lcom/crashlytics/android/answers/m;->a()V

    .line 52
    const/4 v0, 0x1

    .line 58
    :cond_0
    :goto_0
    return v0

    .line 54
    :cond_1
    iget-object v1, p0, Lcom/crashlytics/android/answers/f;->b:Lcom/crashlytics/android/answers/m;

    invoke-virtual {v1, v2, v3}, Lcom/crashlytics/android/answers/m;->b(J)V

    goto :goto_0
.end method
