.class public final Lcom/google/android/gms/common/api/f;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/common/api/f$a;
    }
.end annotation


# direct methods
.method public static a(Lcom/google/android/gms/common/api/h;Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R::",
            "Lcom/google/android/gms/common/api/h;",
            ">(TR;",
            "Lcom/google/android/gms/common/api/c;",
            ")",
            "Lcom/google/android/gms/common/api/d",
            "<TR;>;"
        }
    .end annotation

    const-string/jumbo v0, "Result must not be null"

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/common/api/f$a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/f$a;-><init>(Lcom/google/android/gms/common/api/c;)V

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/f$a;->b(Lcom/google/android/gms/common/api/h;)V

    new-instance v1, Lcom/google/android/gms/internal/cp;

    invoke-direct {v1, v0}, Lcom/google/android/gms/internal/cp;-><init>(Lcom/google/android/gms/common/api/e;)V

    return-object v1
.end method
