.class public Lcom/google/android/gms/location/internal/k;
.super Lcom/google/android/gms/location/internal/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/location/internal/k$a;
    }
.end annotation


# instance fields
.field private final e:Lcom/google/android/gms/location/internal/j;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/c$b;Lcom/google/android/gms/common/api/c$c;Ljava/lang/String;Lcom/google/android/gms/common/internal/m;)V
    .locals 2

    invoke-direct/range {p0 .. p6}, Lcom/google/android/gms/location/internal/a;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/c$b;Lcom/google/android/gms/common/api/c$c;Ljava/lang/String;Lcom/google/android/gms/common/internal/m;)V

    new-instance v0, Lcom/google/android/gms/location/internal/j;

    iget-object v1, p0, Lcom/google/android/gms/location/internal/k;->d:Lcom/google/android/gms/location/internal/o;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/location/internal/j;-><init>(Landroid/content/Context;Lcom/google/android/gms/location/internal/o;)V

    iput-object v0, p0, Lcom/google/android/gms/location/internal/k;->e:Lcom/google/android/gms/location/internal/j;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;Lcom/google/android/gms/location/internal/f;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/location/internal/k;->e:Lcom/google/android/gms/location/internal/j;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/location/internal/j;->a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;Lcom/google/android/gms/location/internal/f;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/f;Landroid/os/Looper;Lcom/google/android/gms/location/internal/f;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/gms/location/internal/k;->e:Lcom/google/android/gms/location/internal/j;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/location/internal/k;->e:Lcom/google/android/gms/location/internal/j;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/location/internal/j;->a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/f;Landroid/os/Looper;Lcom/google/android/gms/location/internal/f;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/gms/location/LocationSettingsRequest;Lcom/google/android/gms/internal/bs$b;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/location/LocationSettingsRequest;",
            "Lcom/google/android/gms/internal/bs$b",
            "<",
            "Lcom/google/android/gms/location/LocationSettingsResult;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/location/internal/k;->r()V

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    const-string/jumbo v3, "locationSettingsRequest can\'t be null nor empty."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/c;->b(ZLjava/lang/Object;)V

    if-eqz p2, :cond_1

    :goto_1
    const-string/jumbo v0, "listener can\'t be null."

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/c;->b(ZLjava/lang/Object;)V

    new-instance v1, Lcom/google/android/gms/location/internal/k$a;

    invoke-direct {v1, p2}, Lcom/google/android/gms/location/internal/k$a;-><init>(Lcom/google/android/gms/internal/bs$b;)V

    invoke-virtual {p0}, Lcom/google/android/gms/location/internal/k;->t()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/h;

    invoke-interface {v0, p1, v1, p3}, Lcom/google/android/gms/location/internal/h;->a(Lcom/google/android/gms/location/LocationSettingsRequest;Lcom/google/android/gms/location/internal/i;Ljava/lang/String;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public a(Lcom/google/android/gms/location/f;Lcom/google/android/gms/location/internal/f;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/location/internal/k;->e:Lcom/google/android/gms/location/internal/j;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/location/internal/j;->a(Lcom/google/android/gms/location/f;Lcom/google/android/gms/location/internal/f;)V

    return-void
.end method

.method public f()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/internal/k;->e:Lcom/google/android/gms/location/internal/j;

    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/j;->a()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/gms/location/internal/k;->e:Lcom/google/android/gms/location/internal/j;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/location/internal/k;->h()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/location/internal/k;->e:Lcom/google/android/gms/location/internal/j;

    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/j;->b()V

    iget-object v0, p0, Lcom/google/android/gms/location/internal/k;->e:Lcom/google/android/gms/location/internal/j;

    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/j;->c()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    invoke-super {p0}, Lcom/google/android/gms/location/internal/a;->g()V

    monitor-exit v1

    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v2, "LocationClientImpl"

    const-string/jumbo v3, "Client disconnected before listeners could be cleaned up"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
