.class public Lcom/google/android/gms/location/places/w;
.super Lcom/google/android/gms/location/places/internal/g$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/location/places/w$f;,
        Lcom/google/android/gms/location/places/w$a;,
        Lcom/google/android/gms/location/places/w$e;,
        Lcom/google/android/gms/location/places/w$c;,
        Lcom/google/android/gms/location/places/w$d;,
        Lcom/google/android/gms/location/places/w$b;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/gms/location/places/w$d;

.field private final c:Lcom/google/android/gms/location/places/w$a;

.field private final d:Lcom/google/android/gms/location/places/w$e;

.field private final e:Lcom/google/android/gms/location/places/w$f;

.field private final f:Lcom/google/android/gms/location/places/w$c;

.field private final g:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/location/places/w;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/places/w;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/location/places/w$a;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/location/places/internal/g$a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/location/places/w;->b:Lcom/google/android/gms/location/places/w$d;

    iput-object p1, p0, Lcom/google/android/gms/location/places/w;->c:Lcom/google/android/gms/location/places/w$a;

    iput-object v0, p0, Lcom/google/android/gms/location/places/w;->d:Lcom/google/android/gms/location/places/w$e;

    iput-object v0, p0, Lcom/google/android/gms/location/places/w;->e:Lcom/google/android/gms/location/places/w$f;

    iput-object v0, p0, Lcom/google/android/gms/location/places/w;->f:Lcom/google/android/gms/location/places/w$c;

    iput-object v0, p0, Lcom/google/android/gms/location/places/w;->g:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/location/places/w$c;Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/location/places/internal/g$a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/location/places/w;->b:Lcom/google/android/gms/location/places/w$d;

    iput-object v0, p0, Lcom/google/android/gms/location/places/w;->c:Lcom/google/android/gms/location/places/w$a;

    iput-object v0, p0, Lcom/google/android/gms/location/places/w;->d:Lcom/google/android/gms/location/places/w$e;

    iput-object v0, p0, Lcom/google/android/gms/location/places/w;->e:Lcom/google/android/gms/location/places/w$f;

    iput-object p1, p0, Lcom/google/android/gms/location/places/w;->f:Lcom/google/android/gms/location/places/w$c;

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/location/places/w;->g:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/common/api/Status;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/location/places/w;->e:Lcom/google/android/gms/location/places/w$f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/location/places/w$f;->b(Lcom/google/android/gms/common/api/h;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/location/places/w;->b:Lcom/google/android/gms/location/places/w$d;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string/jumbo v1, "placeEstimator cannot be null"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/c;->a(ZLjava/lang/Object;)V

    if-nez p1, :cond_3

    sget-object v0, Lcom/google/android/gms/location/places/w;->a:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/gms/location/places/w;->a:Ljava/lang/String;

    const-string/jumbo v2, "onPlaceEstimated received null DataHolder: "

    invoke-static {}, Lcom/google/android/gms/common/util/k;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/location/places/w;->b:Lcom/google/android/gms/location/places/w$d;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/places/w$d;->c(Lcom/google/android/gms/common/api/Status;)V

    :goto_2
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->f()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_4

    const/16 v0, 0x64

    :goto_3
    new-instance v1, Lcom/google/android/gms/location/places/h;

    iget-object v2, p0, Lcom/google/android/gms/location/places/w;->g:Landroid/content/Context;

    invoke-direct {v1, p1, v0, v2}, Lcom/google/android/gms/location/places/h;-><init>(Lcom/google/android/gms/common/data/DataHolder;ILandroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/gms/location/places/w;->b:Lcom/google/android/gms/location/places/w$d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/places/w$d;->b(Lcom/google/android/gms/common/api/h;)V

    goto :goto_2

    :cond_4
    invoke-static {v0}, Lcom/google/android/gms/location/places/h;->a(Landroid/os/Bundle;)I

    move-result v0

    goto :goto_3
.end method

.method public b(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    if-nez p1, :cond_2

    sget-object v0, Lcom/google/android/gms/location/places/w;->a:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/gms/location/places/w;->a:Ljava/lang/String;

    const-string/jumbo v2, "onAutocompletePrediction received null DataHolder: "

    invoke-static {}, Lcom/google/android/gms/common/util/k;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/location/places/w;->c:Lcom/google/android/gms/location/places/w$a;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/places/w$a;->c(Lcom/google/android/gms/common/api/Status;)V

    :goto_1
    return-void

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/location/places/w;->c:Lcom/google/android/gms/location/places/w$a;

    new-instance v1, Lcom/google/android/gms/location/places/b;

    invoke-direct {v1, p1}, Lcom/google/android/gms/location/places/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/places/w$a;->b(Lcom/google/android/gms/common/api/h;)V

    goto :goto_1
.end method

.method public c(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v4, 0x0

    if-nez p1, :cond_2

    sget-object v0, Lcom/google/android/gms/location/places/w;->a:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/gms/location/places/w;->a:Ljava/lang/String;

    const-string/jumbo v2, "onPlaceUserDataFetched received null DataHolder: "

    invoke-static {}, Lcom/google/android/gms/common/util/k;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v0, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v4, v0}, Lcom/google/android/gms/location/places/w$e;->c(Lcom/google/android/gms/common/api/Status;)V

    :goto_1
    return-void

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/gms/location/places/personalized/e;

    invoke-direct {v0, p1}, Lcom/google/android/gms/location/places/personalized/e;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v4, v0}, Lcom/google/android/gms/location/places/w$e;->b(Lcom/google/android/gms/common/api/h;)V

    goto :goto_1
.end method

.method public d(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/gms/location/places/e;

    iget-object v1, p0, Lcom/google/android/gms/location/places/w;->g:Landroid/content/Context;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/location/places/e;-><init>(Lcom/google/android/gms/common/data/DataHolder;Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/gms/location/places/w;->f:Lcom/google/android/gms/location/places/w$c;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/location/places/w$c;->b(Lcom/google/android/gms/common/api/h;)V

    return-void
.end method
