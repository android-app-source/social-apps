.class Lcom/google/android/gms/location/places/internal/b$1;
.super Lcom/google/android/gms/location/places/w$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/location/places/internal/b;->a(Lcom/google/android/gms/common/api/c;[Ljava/lang/String;)Lcom/google/android/gms/common/api/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/location/places/w$c",
        "<",
        "Lcom/google/android/gms/location/places/internal/c;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:[Ljava/lang/String;

.field final synthetic b:Lcom/google/android/gms/location/places/internal/b;


# direct methods
.method constructor <init>(Lcom/google/android/gms/location/places/internal/b;Lcom/google/android/gms/common/api/a;Lcom/google/android/gms/common/api/c;[Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/location/places/internal/b$1;->b:Lcom/google/android/gms/location/places/internal/b;

    iput-object p4, p0, Lcom/google/android/gms/location/places/internal/b$1;->a:[Ljava/lang/String;

    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/location/places/w$c;-><init>(Lcom/google/android/gms/common/api/a;Lcom/google/android/gms/common/api/c;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Lcom/google/android/gms/common/api/a$c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/gms/location/places/internal/c;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/location/places/internal/b$1;->a(Lcom/google/android/gms/location/places/internal/c;)V

    return-void
.end method

.method protected a(Lcom/google/android/gms/location/places/internal/c;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/location/places/internal/b$1;->a:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/location/places/w;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/internal/c;->o()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/location/places/w;-><init>(Lcom/google/android/gms/location/places/w$c;Landroid/content/Context;)V

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/location/places/internal/c;->a(Lcom/google/android/gms/location/places/w;Ljava/util/List;)V

    return-void
.end method
