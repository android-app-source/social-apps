.class public Lcom/google/android/gms/location/places/internal/o;
.super Lcom/google/android/gms/location/places/internal/q;

# interfaces
.implements Lcom/google/android/gms/location/places/d;


# instance fields
.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;ILandroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/location/places/internal/q;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    const-string/jumbo v0, "place_id"

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/location/places/internal/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/location/places/internal/o;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/CharSequence;
    .locals 2

    const-string/jumbo v0, "place_name"

    const-string/jumbo v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/location/places/internal/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
