.class Lcom/google/android/gms/location/places/internal/b$2;
.super Lcom/google/android/gms/location/places/w$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/location/places/internal/b;->a(Lcom/google/android/gms/common/api/c;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;)Lcom/google/android/gms/common/api/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/location/places/w$a",
        "<",
        "Lcom/google/android/gms/location/places/internal/c;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/gms/maps/model/LatLngBounds;

.field final synthetic c:Lcom/google/android/gms/location/places/AutocompleteFilter;

.field final synthetic g:Lcom/google/android/gms/location/places/internal/b;


# direct methods
.method constructor <init>(Lcom/google/android/gms/location/places/internal/b;Lcom/google/android/gms/common/api/a;Lcom/google/android/gms/common/api/c;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/location/places/internal/b$2;->g:Lcom/google/android/gms/location/places/internal/b;

    iput-object p4, p0, Lcom/google/android/gms/location/places/internal/b$2;->a:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/location/places/internal/b$2;->b:Lcom/google/android/gms/maps/model/LatLngBounds;

    iput-object p6, p0, Lcom/google/android/gms/location/places/internal/b$2;->c:Lcom/google/android/gms/location/places/AutocompleteFilter;

    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/location/places/w$a;-><init>(Lcom/google/android/gms/common/api/a;Lcom/google/android/gms/common/api/c;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Lcom/google/android/gms/common/api/a$c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/gms/location/places/internal/c;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/location/places/internal/b$2;->a(Lcom/google/android/gms/location/places/internal/c;)V

    return-void
.end method

.method protected a(Lcom/google/android/gms/location/places/internal/c;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/gms/location/places/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/location/places/w;-><init>(Lcom/google/android/gms/location/places/w$a;)V

    iget-object v1, p0, Lcom/google/android/gms/location/places/internal/b$2;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/b$2;->b:Lcom/google/android/gms/maps/model/LatLngBounds;

    iget-object v3, p0, Lcom/google/android/gms/location/places/internal/b$2;->c:Lcom/google/android/gms/location/places/AutocompleteFilter;

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/google/android/gms/location/places/internal/c;->a(Lcom/google/android/gms/location/places/w;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;)V

    return-void
.end method
