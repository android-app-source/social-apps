.class Lcom/google/android/gms/auth/api/signin/internal/c$1;
.super Lcom/google/android/gms/auth/api/signin/internal/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/auth/api/signin/internal/c;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;)Lcom/google/android/gms/common/api/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/auth/api/signin/internal/c$a",
        "<",
        "Lcom/google/android/gms/auth/api/signin/b;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;

.field final synthetic b:Lcom/google/android/gms/auth/api/signin/internal/c;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/api/signin/internal/c;Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/auth/api/signin/internal/c$1;->b:Lcom/google/android/gms/auth/api/signin/internal/c;

    iput-object p3, p0, Lcom/google/android/gms/auth/api/signin/internal/c$1;->a:Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/auth/api/signin/internal/c$a;-><init>(Lcom/google/android/gms/auth/api/signin/internal/c;Lcom/google/android/gms/common/api/c;)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/auth/api/signin/b;
    .locals 2

    new-instance v0, Lcom/google/android/gms/auth/api/signin/b;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/auth/api/signin/b;-><init>(Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;Lcom/google/android/gms/common/api/Status;)V

    return-object v0
.end method

.method protected a(Lcom/google/android/gms/auth/api/signin/internal/d;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/gms/auth/api/signin/internal/d;->o()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/api/signin/internal/k;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/api/signin/internal/k;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/api/signin/internal/d;->t()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/api/signin/internal/h;

    new-instance v2, Lcom/google/android/gms/auth/api/signin/internal/c$1$1;

    invoke-direct {v2, p0, v1}, Lcom/google/android/gms/auth/api/signin/internal/c$1$1;-><init>(Lcom/google/android/gms/auth/api/signin/internal/c$1;Lcom/google/android/gms/auth/api/signin/internal/k;)V

    iget-object v1, p0, Lcom/google/android/gms/auth/api/signin/internal/c$1;->a:Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;

    invoke-interface {v0, v2, v1}, Lcom/google/android/gms/auth/api/signin/internal/h;->a(Lcom/google/android/gms/auth/api/signin/internal/g;Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/google/android/gms/common/api/a$c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/gms/auth/api/signin/internal/d;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/auth/api/signin/internal/c$1;->a(Lcom/google/android/gms/auth/api/signin/internal/d;)V

    return-void
.end method

.method protected synthetic b(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/h;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/auth/api/signin/internal/c$1;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/auth/api/signin/b;

    move-result-object v0

    return-object v0
.end method
