.class public Lcom/google/android/gms/internal/u;
.super Lcom/google/android/gms/internal/p$a;


# instance fields
.field private final a:Lcom/google/android/gms/ads/formats/NativeAppInstallAd$OnAppInstallAdLoadedListener;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/formats/NativeAppInstallAd$OnAppInstallAdLoadedListener;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/p$a;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/u;->a:Lcom/google/android/gms/ads/formats/NativeAppInstallAd$OnAppInstallAdLoadedListener;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/internal/j;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/u;->a:Lcom/google/android/gms/ads/formats/NativeAppInstallAd$OnAppInstallAdLoadedListener;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/u;->b(Lcom/google/android/gms/internal/j;)Lcom/google/android/gms/internal/k;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/ads/formats/NativeAppInstallAd$OnAppInstallAdLoadedListener;->onAppInstallAdLoaded(Lcom/google/android/gms/ads/formats/NativeAppInstallAd;)V

    return-void
.end method

.method b(Lcom/google/android/gms/internal/j;)Lcom/google/android/gms/internal/k;
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/k;

    invoke-direct {v0, p1}, Lcom/google/android/gms/internal/k;-><init>(Lcom/google/android/gms/internal/j;)V

    return-object v0
.end method
