.class public interface abstract Lcom/google/android/gms/internal/ch;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/ch$a;
    }
.end annotation


# virtual methods
.method public abstract a(Lcom/google/android/gms/internal/bs$a;)Lcom/google/android/gms/internal/bs$a;
    .param p1    # Lcom/google/android/gms/internal/bs$a;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Lcom/google/android/gms/common/api/a$c;",
            "R::",
            "Lcom/google/android/gms/common/api/h;",
            "T:",
            "Lcom/google/android/gms/internal/bs$a",
            "<TR;TA;>;>(TT;)TT;"
        }
    .end annotation
.end method

.method public abstract a()V
.end method

.method public abstract a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/cs;)Z
.end method

.method public abstract b()Lcom/google/android/gms/common/ConnectionResult;
.end method

.method public abstract b(Lcom/google/android/gms/internal/bs$a;)Lcom/google/android/gms/internal/bs$a;
    .param p1    # Lcom/google/android/gms/internal/bs$a;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Lcom/google/android/gms/common/api/a$c;",
            "T:",
            "Lcom/google/android/gms/internal/bs$a",
            "<+",
            "Lcom/google/android/gms/common/api/h;",
            "TA;>;>(TT;)TT;"
        }
    .end annotation
.end method

.method public abstract c()V
.end method

.method public abstract d()Z
.end method

.method public abstract e()Z
.end method

.method public abstract f()V
.end method

.method public abstract g()V
.end method
