.class public interface abstract Lcom/google/android/gms/internal/ck;
.super Ljava/lang/Object;


# virtual methods
.method public abstract a()Landroid/app/Activity;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/internal/cj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/gms/internal/cj;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/gms/internal/cj;)V
    .param p2    # Lcom/google/android/gms/internal/cj;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract startActivityForResult(Landroid/content/Intent;I)V
.end method
