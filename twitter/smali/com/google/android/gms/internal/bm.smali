.class public interface abstract Lcom/google/android/gms/internal/bm;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/bm$a;
    }
.end annotation


# virtual methods
.method public abstract a(Lcom/google/android/gms/internal/bl;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/android/gms/internal/bl;Lcom/google/android/gms/auth/api/proxy/ProxyGrpcRequest;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/android/gms/internal/bl;Lcom/google/android/gms/auth/api/proxy/ProxyRequest;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
