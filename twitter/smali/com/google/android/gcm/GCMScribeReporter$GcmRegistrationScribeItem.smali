.class Lcom/google/android/gcm/GCMScribeReporter$GcmRegistrationScribeItem;
.super Lcom/twitter/analytics/model/ScribeItem;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gcm/GCMScribeReporter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GcmRegistrationScribeItem"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gcm/GCMScribeReporter$GcmRegistrationScribeItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 117
    new-instance v0, Lcom/google/android/gcm/GCMScribeReporter$GcmRegistrationScribeItem$1;

    invoke-direct {v0}, Lcom/google/android/gcm/GCMScribeReporter$GcmRegistrationScribeItem$1;-><init>()V

    sput-object v0, Lcom/google/android/gcm/GCMScribeReporter$GcmRegistrationScribeItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 135
    invoke-direct {p0, p1}, Lcom/twitter/analytics/model/ScribeItem;-><init>(Landroid/os/Parcel;)V

    .line 136
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gcm/GCMScribeReporter$GcmRegistrationScribeItem;->a:Ljava/lang/String;

    .line 137
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gcm/GCMScribeReporter$GcmRegistrationScribeItem;->b:Ljava/lang/String;

    .line 138
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/twitter/analytics/model/ScribeItem;-><init>()V

    .line 141
    iput-object p1, p0, Lcom/google/android/gcm/GCMScribeReporter$GcmRegistrationScribeItem;->a:Ljava/lang/String;

    .line 142
    iput-object p2, p0, Lcom/google/android/gcm/GCMScribeReporter$GcmRegistrationScribeItem;->b:Ljava/lang/String;

    .line 143
    return-void
.end method


# virtual methods
.method protected a(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 153
    const-string/jumbo v1, "newRegistrationToken"

    iget-object v0, p0, Lcom/google/android/gcm/GCMScribeReporter$GcmRegistrationScribeItem;->a:Ljava/lang/String;

    .line 154
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, ""

    .line 153
    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string/jumbo v1, "oldRegistrationToken"

    iget-object v0, p0, Lcom/google/android/gcm/GCMScribeReporter$GcmRegistrationScribeItem;->b:Ljava/lang/String;

    .line 156
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, ""

    .line 155
    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/google/android/gcm/GCMScribeReporter$GcmRegistrationScribeItem;->a:Ljava/lang/String;

    goto :goto_0

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/google/android/gcm/GCMScribeReporter$GcmRegistrationScribeItem;->b:Ljava/lang/String;

    goto :goto_1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/gcm/GCMScribeReporter$GcmRegistrationScribeItem;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/gcm/GCMScribeReporter$GcmRegistrationScribeItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 149
    return-void
.end method
