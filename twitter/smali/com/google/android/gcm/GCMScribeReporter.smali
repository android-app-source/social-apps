.class public Lcom/google/android/gcm/GCMScribeReporter;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gcm/GCMScribeReporter$GCMDeletedMessageScribeItem;,
        Lcom/google/android/gcm/GCMScribeReporter$GcmRegistrationScribeItem;
    }
.end annotation


# direct methods
.method public static a()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 77
    invoke-static {}, Lcom/google/android/gcm/GCMScribeReporter;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    .line 78
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v0}, Lakm;->b()Lcnz;

    move-result-object v0

    invoke-virtual {v0}, Lcnz;->b()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "notification"

    aput-object v4, v0, v3

    const/4 v3, 0x1

    aput-object v6, v0, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "gcm_registration"

    aput-object v4, v0, v3

    const/4 v3, 0x3

    aput-object v6, v0, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "external_unregister"

    aput-object v4, v0, v3

    .line 79
    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 78
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 81
    :cond_0
    return-void
.end method

.method public static a(ILjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 48
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "notification"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "status_bar"

    aput-object v3, v1, v2

    aput-object v5, v1, v4

    const/4 v2, 0x3

    aput-object v5, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "push_data_dropped"

    aput-object v3, v1, v2

    .line 49
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    new-instance v1, Lcom/google/android/gcm/GCMScribeReporter$GCMDeletedMessageScribeItem;

    invoke-direct {v1, p0}, Lcom/google/android/gcm/GCMScribeReporter$GCMDeletedMessageScribeItem;-><init>(I)V

    .line 50
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 51
    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->g(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 52
    invoke-virtual {v0, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->d(I)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 48
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 53
    return-void
.end method

.method public static a(JLjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 40
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "notification"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "status_bar"

    aput-object v3, v1, v2

    aput-object v5, v1, v4

    const/4 v2, 0x3

    aput-object v5, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "push_data_received"

    aput-object v3, v1, v2

    .line 41
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 42
    invoke-virtual {v0, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->g(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 43
    invoke-virtual {v0, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->d(I)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 40
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 44
    return-void
.end method

.method public static a(JLjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    .line 32
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "notification"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "status_bar"

    aput-object v3, v1, v2

    const/4 v2, 0x0

    aput-object v2, v1, v4

    const/4 v2, 0x3

    aput-object p2, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "push_data_received"

    aput-object v3, v1, v2

    .line 33
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 34
    invoke-virtual {v0, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->g(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 35
    invoke-virtual {v0, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->d(I)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 32
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 36
    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 57
    invoke-static {p0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v2

    .line 58
    :goto_0
    invoke-static {}, Lcom/google/android/gcm/GCMScribeReporter;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    .line 59
    new-instance v5, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v0}, Lakm;->b()Lcnz;

    move-result-object v0

    invoke-virtual {v0}, Lcnz;->b()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v6, v0, [Ljava/lang/String;

    const-string/jumbo v0, "notification"

    aput-object v0, v6, v3

    aput-object v8, v6, v2

    const/4 v0, 0x2

    const-string/jumbo v7, "gcm_registration"

    aput-object v7, v6, v0

    const/4 v0, 0x3

    aput-object v8, v6, v0

    const/4 v7, 0x4

    if-eqz v1, :cond_1

    const-string/jumbo v0, "success"

    :goto_2
    aput-object v0, v6, v7

    .line 60
    invoke-virtual {v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 59
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_1

    :cond_0
    move v1, v3

    .line 57
    goto :goto_0

    .line 59
    :cond_1
    const-string/jumbo v0, "failure"

    goto :goto_2

    .line 63
    :cond_2
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 68
    invoke-static {}, Lcom/google/android/gcm/GCMScribeReporter;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    .line 69
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v0}, Lakm;->b()Lcnz;

    move-result-object v0

    invoke-virtual {v0}, Lcnz;->b()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "notification"

    aput-object v4, v0, v3

    const/4 v3, 0x1

    const/4 v4, 0x0

    aput-object v4, v0, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "gcm_registration"

    aput-object v4, v0, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "token"

    aput-object v4, v0, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "changed"

    aput-object v4, v0, v3

    .line 70
    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    new-instance v2, Lcom/google/android/gcm/GCMScribeReporter$GcmRegistrationScribeItem;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gcm/GCMScribeReporter$GcmRegistrationScribeItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 69
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 73
    :cond_0
    return-void
.end method

.method public static b()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 85
    invoke-static {}, Lcom/google/android/gcm/GCMScribeReporter;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    .line 86
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v0}, Lakm;->b()Lcnz;

    move-result-object v0

    invoke-virtual {v0}, Lcnz;->b()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "notification"

    aput-object v4, v0, v3

    const/4 v3, 0x1

    aput-object v6, v0, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "gcm_registration"

    aput-object v4, v0, v3

    const/4 v3, 0x3

    aput-object v6, v0, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "error"

    aput-object v4, v0, v3

    .line 87
    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 86
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 89
    :cond_0
    return-void
.end method

.method public static b(JLjava/lang/String;)V
    .locals 4

    .prologue
    .line 101
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "notification"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "status_bar"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object p2, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "blocked"

    aput-object v3, v1, v2

    .line 102
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 101
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 103
    return-void
.end method

.method public static c()V
    .locals 6

    .prologue
    .line 93
    invoke-static {}, Lcom/google/android/gcm/GCMScribeReporter;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    .line 94
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v0}, Lakm;->b()Lcnz;

    move-result-object v0

    invoke-virtual {v0}, Lcnz;->b()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "app"

    aput-object v4, v0, v3

    const/4 v3, 0x1

    const/4 v4, 0x0

    aput-object v4, v0, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "gcm_registration"

    aput-object v4, v0, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "udid"

    aput-object v4, v0, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "unavailable"

    aput-object v4, v0, v3

    .line 95
    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 94
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 97
    :cond_0
    return-void
.end method

.method private static d()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lakm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v0

    invoke-virtual {v0}, Lakn;->c()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/google/android/gcm/GCMScribeReporter$1;

    invoke-direct {v1}, Lcom/google/android/gcm/GCMScribeReporter$1;-><init>()V

    invoke-static {v0, v1}, Lcpt;->a(Ljava/lang/Iterable;Lcpv;)Ljava/lang/Iterable;

    move-result-object v0

    .line 107
    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
