.class public final Lcom/google/android/exoplayer/R$string;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abbr_number_unit_billions:I = 0x7f0a0b7c

.field public static final abbr_number_unit_millions:I = 0x7f0a0b7a

.field public static final abbr_number_unit_thousands:I = 0x7f0a0b7b

.field public static final amazon_app_store_url_format:I = 0x7f0a0b8e

.field public static final date_format_long:I = 0x7f0a0b71

.field public static final date_format_long_accessible:I = 0x7f0a0b9b

.field public static final date_format_short:I = 0x7f0a0b72

.field public static final date_format_short_accessible:I = 0x7f0a0b9c

.field public static final foot_abbr:I = 0x7f0a0ba8

.field public static final google_play_details_url_format:I = 0x7f0a0bc2

.field public static final google_play_web_details_url_format:I = 0x7f0a0bc3

.field public static final kilometer:I = 0x7f0a0434

.field public static final meter:I = 0x7f0a054d

.field public static final mile_abbr:I = 0x7f0a0bd6

.field public static final now:I = 0x7f0a0611

.field public static final recent_tweets_header_title:I = 0x7f0a074a

.field public static final time_of_day_format:I = 0x7f0a0c37


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
