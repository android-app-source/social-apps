.class public Lcom/facebook/imagepipeline/producers/p;
.super Lcom/facebook/imagepipeline/producers/aa;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/imagepipeline/producers/aa",
        "<",
        "Landroid/util/Pair",
        "<",
        "Lcom/facebook/cache/common/a;",
        "Lcom/facebook/imagepipeline/request/ImageRequest$RequestLevel;",
        ">;",
        "Lds;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Lcn;


# direct methods
.method public constructor <init>(Lcn;Lcom/facebook/imagepipeline/producers/af;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p2}, Lcom/facebook/imagepipeline/producers/aa;-><init>(Lcom/facebook/imagepipeline/producers/af;)V

    .line 29
    iput-object p1, p0, Lcom/facebook/imagepipeline/producers/p;->b:Lcn;

    .line 30
    return-void
.end method


# virtual methods
.method protected a(Lcom/facebook/imagepipeline/producers/ag;)Landroid/util/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/ag;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/facebook/cache/common/a;",
            "Lcom/facebook/imagepipeline/request/ImageRequest$RequestLevel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/p;->b:Lcn;

    invoke-interface {p1}, Lcom/facebook/imagepipeline/producers/ag;->a()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v1

    invoke-interface {v0, v1}, Lcn;->c(Lcom/facebook/imagepipeline/request/ImageRequest;)Lcom/facebook/cache/common/a;

    move-result-object v0

    invoke-interface {p1}, Lcom/facebook/imagepipeline/producers/ag;->e()Lcom/facebook/imagepipeline/request/ImageRequest$RequestLevel;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public a(Lds;)Lds;
    .locals 1

    .prologue
    .line 39
    invoke-static {p1}, Lds;->a(Lds;)Lds;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/io/Closeable;)Ljava/io/Closeable;
    .locals 1

    .prologue
    .line 22
    check-cast p1, Lds;

    invoke-virtual {p0, p1}, Lcom/facebook/imagepipeline/producers/p;->a(Lds;)Lds;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic b(Lcom/facebook/imagepipeline/producers/ag;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/facebook/imagepipeline/producers/p;->a(Lcom/facebook/imagepipeline/producers/ag;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method
