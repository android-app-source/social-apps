.class Lcom/facebook/imagepipeline/producers/n$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbolts/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/facebook/imagepipeline/producers/n;->a(Lcom/facebook/imagepipeline/producers/j;Lcom/facebook/imagepipeline/producers/ag;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbolts/c",
        "<",
        "Lds;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/facebook/imagepipeline/producers/ai;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/facebook/imagepipeline/producers/j;

.field final synthetic d:Lcm;

.field final synthetic e:Lcom/facebook/cache/common/a;

.field final synthetic f:Lcom/facebook/imagepipeline/producers/ag;

.field final synthetic g:Lcom/facebook/imagepipeline/producers/n;


# direct methods
.method constructor <init>(Lcom/facebook/imagepipeline/producers/n;Lcom/facebook/imagepipeline/producers/ai;Ljava/lang/String;Lcom/facebook/imagepipeline/producers/j;Lcm;Lcom/facebook/cache/common/a;Lcom/facebook/imagepipeline/producers/ag;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/facebook/imagepipeline/producers/n$1;->g:Lcom/facebook/imagepipeline/producers/n;

    iput-object p2, p0, Lcom/facebook/imagepipeline/producers/n$1;->a:Lcom/facebook/imagepipeline/producers/ai;

    iput-object p3, p0, Lcom/facebook/imagepipeline/producers/n$1;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/facebook/imagepipeline/producers/n$1;->c:Lcom/facebook/imagepipeline/producers/j;

    iput-object p5, p0, Lcom/facebook/imagepipeline/producers/n$1;->d:Lcm;

    iput-object p6, p0, Lcom/facebook/imagepipeline/producers/n$1;->e:Lcom/facebook/cache/common/a;

    iput-object p7, p0, Lcom/facebook/imagepipeline/producers/n$1;->f:Lcom/facebook/imagepipeline/producers/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic a(Lbolts/d;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/facebook/imagepipeline/producers/n$1;->b(Lbolts/d;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public b(Lbolts/d;)Ljava/lang/Void;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbolts/d",
            "<",
            "Lds;",
            ">;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 79
    invoke-virtual {p1}, Lbolts/d;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lbolts/d;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lbolts/d;->f()Ljava/lang/Exception;

    move-result-object v0

    instance-of v0, v0, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_1

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/n$1;->a:Lcom/facebook/imagepipeline/producers/ai;

    iget-object v1, p0, Lcom/facebook/imagepipeline/producers/n$1;->b:Ljava/lang/String;

    const-string/jumbo v2, "DiskCacheProducer"

    invoke-interface {v0, v1, v2, v5}, Lcom/facebook/imagepipeline/producers/ai;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 82
    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/n$1;->c:Lcom/facebook/imagepipeline/producers/j;

    invoke-interface {v0}, Lcom/facebook/imagepipeline/producers/j;->b()V

    .line 110
    :goto_0
    return-object v5

    .line 83
    :cond_1
    invoke-virtual {p1}, Lbolts/d;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 84
    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/n$1;->a:Lcom/facebook/imagepipeline/producers/ai;

    iget-object v1, p0, Lcom/facebook/imagepipeline/producers/n$1;->b:Ljava/lang/String;

    const-string/jumbo v2, "DiskCacheProducer"

    invoke-virtual {p1}, Lbolts/d;->f()Ljava/lang/Exception;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3, v5}, Lcom/facebook/imagepipeline/producers/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    .line 85
    iget-object v6, p0, Lcom/facebook/imagepipeline/producers/n$1;->g:Lcom/facebook/imagepipeline/producers/n;

    iget-object v7, p0, Lcom/facebook/imagepipeline/producers/n$1;->c:Lcom/facebook/imagepipeline/producers/j;

    new-instance v0, Lcom/facebook/imagepipeline/producers/n$a;

    iget-object v1, p0, Lcom/facebook/imagepipeline/producers/n$1;->g:Lcom/facebook/imagepipeline/producers/n;

    iget-object v2, p0, Lcom/facebook/imagepipeline/producers/n$1;->c:Lcom/facebook/imagepipeline/producers/j;

    iget-object v3, p0, Lcom/facebook/imagepipeline/producers/n$1;->d:Lcm;

    iget-object v4, p0, Lcom/facebook/imagepipeline/producers/n$1;->e:Lcom/facebook/cache/common/a;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/imagepipeline/producers/n$a;-><init>(Lcom/facebook/imagepipeline/producers/n;Lcom/facebook/imagepipeline/producers/j;Lcm;Lcom/facebook/cache/common/a;Lcom/facebook/imagepipeline/producers/n$1;)V

    iget-object v1, p0, Lcom/facebook/imagepipeline/producers/n$1;->f:Lcom/facebook/imagepipeline/producers/ag;

    invoke-static {v6, v7, v0, v1}, Lcom/facebook/imagepipeline/producers/n;->a(Lcom/facebook/imagepipeline/producers/n;Lcom/facebook/imagepipeline/producers/j;Lcom/facebook/imagepipeline/producers/j;Lcom/facebook/imagepipeline/producers/ag;)V

    goto :goto_0

    .line 90
    :cond_2
    invoke-virtual {p1}, Lbolts/d;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lds;

    .line 91
    if-eqz v0, :cond_3

    .line 92
    iget-object v1, p0, Lcom/facebook/imagepipeline/producers/n$1;->a:Lcom/facebook/imagepipeline/producers/ai;

    iget-object v2, p0, Lcom/facebook/imagepipeline/producers/n$1;->b:Ljava/lang/String;

    const-string/jumbo v3, "DiskCacheProducer"

    iget-object v4, p0, Lcom/facebook/imagepipeline/producers/n$1;->a:Lcom/facebook/imagepipeline/producers/ai;

    iget-object v6, p0, Lcom/facebook/imagepipeline/producers/n$1;->b:Ljava/lang/String;

    invoke-static {v4, v6, v7}, Lcom/facebook/imagepipeline/producers/n;->a(Lcom/facebook/imagepipeline/producers/ai;Ljava/lang/String;Z)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/imagepipeline/producers/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 96
    iget-object v1, p0, Lcom/facebook/imagepipeline/producers/n$1;->c:Lcom/facebook/imagepipeline/producers/j;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v1, v2}, Lcom/facebook/imagepipeline/producers/j;->b(F)V

    .line 97
    iget-object v1, p0, Lcom/facebook/imagepipeline/producers/n$1;->c:Lcom/facebook/imagepipeline/producers/j;

    invoke-interface {v1, v0, v7}, Lcom/facebook/imagepipeline/producers/j;->b(Ljava/lang/Object;Z)V

    .line 98
    invoke-virtual {v0}, Lds;->close()V

    goto :goto_0

    .line 100
    :cond_3
    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/n$1;->a:Lcom/facebook/imagepipeline/producers/ai;

    iget-object v1, p0, Lcom/facebook/imagepipeline/producers/n$1;->b:Ljava/lang/String;

    const-string/jumbo v2, "DiskCacheProducer"

    iget-object v3, p0, Lcom/facebook/imagepipeline/producers/n$1;->a:Lcom/facebook/imagepipeline/producers/ai;

    iget-object v4, p0, Lcom/facebook/imagepipeline/producers/n$1;->b:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v3, v4, v6}, Lcom/facebook/imagepipeline/producers/n;->a(Lcom/facebook/imagepipeline/producers/ai;Ljava/lang/String;Z)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/imagepipeline/producers/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 104
    iget-object v6, p0, Lcom/facebook/imagepipeline/producers/n$1;->g:Lcom/facebook/imagepipeline/producers/n;

    iget-object v7, p0, Lcom/facebook/imagepipeline/producers/n$1;->c:Lcom/facebook/imagepipeline/producers/j;

    new-instance v0, Lcom/facebook/imagepipeline/producers/n$a;

    iget-object v1, p0, Lcom/facebook/imagepipeline/producers/n$1;->g:Lcom/facebook/imagepipeline/producers/n;

    iget-object v2, p0, Lcom/facebook/imagepipeline/producers/n$1;->c:Lcom/facebook/imagepipeline/producers/j;

    iget-object v3, p0, Lcom/facebook/imagepipeline/producers/n$1;->d:Lcm;

    iget-object v4, p0, Lcom/facebook/imagepipeline/producers/n$1;->e:Lcom/facebook/cache/common/a;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/imagepipeline/producers/n$a;-><init>(Lcom/facebook/imagepipeline/producers/n;Lcom/facebook/imagepipeline/producers/j;Lcm;Lcom/facebook/cache/common/a;Lcom/facebook/imagepipeline/producers/n$1;)V

    iget-object v1, p0, Lcom/facebook/imagepipeline/producers/n$1;->f:Lcom/facebook/imagepipeline/producers/ag;

    invoke-static {v6, v7, v0, v1}, Lcom/facebook/imagepipeline/producers/n;->a(Lcom/facebook/imagepipeline/producers/n;Lcom/facebook/imagepipeline/producers/j;Lcom/facebook/imagepipeline/producers/j;Lcom/facebook/imagepipeline/producers/ag;)V

    goto :goto_0
.end method
