.class public Lbsu$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbsu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:I

.field private b:Z

.field private c:J

.field private d:J

.field private e:J

.field private f:J

.field private g:J

.field private h:J

.field private i:J

.field private j:J

.field private k:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    const/4 v0, -0x1

    iput v0, p0, Lbsu$a;->a:I

    .line 201
    iput-wide v2, p0, Lbsu$a;->c:J

    .line 202
    iput-wide v2, p0, Lbsu$a;->d:J

    .line 203
    iput-wide v2, p0, Lbsu$a;->e:J

    .line 204
    iput-wide v2, p0, Lbsu$a;->f:J

    .line 205
    iput-wide v2, p0, Lbsu$a;->g:J

    .line 206
    iput-wide v2, p0, Lbsu$a;->h:J

    .line 207
    iput-wide v2, p0, Lbsu$a;->i:J

    .line 208
    iput-wide v2, p0, Lbsu$a;->j:J

    .line 209
    iput-wide v2, p0, Lbsu$a;->k:J

    return-void
.end method

.method static synthetic a(Lbsu$a;)Z
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Lbsu$a;->b:Z

    return v0
.end method

.method static synthetic b(Lbsu$a;)I
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lbsu$a;->a:I

    return v0
.end method

.method static synthetic c(Lbsu$a;)J
    .locals 2

    .prologue
    .line 198
    iget-wide v0, p0, Lbsu$a;->c:J

    return-wide v0
.end method

.method static synthetic d(Lbsu$a;)J
    .locals 2

    .prologue
    .line 198
    iget-wide v0, p0, Lbsu$a;->d:J

    return-wide v0
.end method

.method static synthetic e(Lbsu$a;)J
    .locals 2

    .prologue
    .line 198
    iget-wide v0, p0, Lbsu$a;->i:J

    return-wide v0
.end method

.method static synthetic f(Lbsu$a;)J
    .locals 2

    .prologue
    .line 198
    iget-wide v0, p0, Lbsu$a;->e:J

    return-wide v0
.end method

.method static synthetic g(Lbsu$a;)J
    .locals 2

    .prologue
    .line 198
    iget-wide v0, p0, Lbsu$a;->f:J

    return-wide v0
.end method

.method static synthetic h(Lbsu$a;)J
    .locals 2

    .prologue
    .line 198
    iget-wide v0, p0, Lbsu$a;->g:J

    return-wide v0
.end method

.method static synthetic i(Lbsu$a;)J
    .locals 2

    .prologue
    .line 198
    iget-wide v0, p0, Lbsu$a;->h:J

    return-wide v0
.end method

.method static synthetic j(Lbsu$a;)J
    .locals 2

    .prologue
    .line 198
    iget-wide v0, p0, Lbsu$a;->j:J

    return-wide v0
.end method

.method static synthetic k(Lbsu$a;)J
    .locals 2

    .prologue
    .line 198
    iget-wide v0, p0, Lbsu$a;->k:J

    return-wide v0
.end method


# virtual methods
.method public a(I)Lbsu$a;
    .locals 0

    .prologue
    .line 217
    iput p1, p0, Lbsu$a;->a:I

    .line 218
    return-object p0
.end method

.method public a(J)Lbsu$a;
    .locals 1

    .prologue
    .line 237
    iput-wide p1, p0, Lbsu$a;->c:J

    .line 238
    return-object p0
.end method

.method public a(Z)Lbsu$a;
    .locals 0

    .prologue
    .line 227
    iput-boolean p1, p0, Lbsu$a;->b:Z

    .line 228
    return-object p0
.end method

.method public a()Lbsu;
    .locals 2

    .prologue
    .line 322
    new-instance v0, Lbsu;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lbsu;-><init>(Lbsu$a;Lbsu$1;)V

    return-object v0
.end method

.method public b(J)Lbsu$a;
    .locals 1

    .prologue
    .line 247
    iput-wide p1, p0, Lbsu$a;->d:J

    .line 248
    return-object p0
.end method

.method public c(J)Lbsu$a;
    .locals 1

    .prologue
    .line 257
    iput-wide p1, p0, Lbsu$a;->e:J

    .line 258
    return-object p0
.end method

.method public d(J)Lbsu$a;
    .locals 1

    .prologue
    .line 267
    iput-wide p1, p0, Lbsu$a;->f:J

    .line 268
    return-object p0
.end method

.method public e(J)Lbsu$a;
    .locals 1

    .prologue
    .line 277
    iput-wide p1, p0, Lbsu$a;->g:J

    .line 278
    return-object p0
.end method

.method public f(J)Lbsu$a;
    .locals 1

    .prologue
    .line 287
    iput-wide p1, p0, Lbsu$a;->h:J

    .line 288
    return-object p0
.end method

.method public g(J)Lbsu$a;
    .locals 1

    .prologue
    .line 297
    iput-wide p1, p0, Lbsu$a;->i:J

    .line 298
    return-object p0
.end method

.method public h(J)Lbsu$a;
    .locals 1

    .prologue
    .line 307
    iput-wide p1, p0, Lbsu$a;->j:J

    .line 308
    return-object p0
.end method

.method public i(J)Lbsu$a;
    .locals 1

    .prologue
    .line 317
    iput-wide p1, p0, Lbsu$a;->k:J

    .line 318
    return-object p0
.end method
