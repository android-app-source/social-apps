.class public abstract Lapv$a;
.super Lapn$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lapv;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<H:",
        "Lapv$c;",
        "B:",
        "Lapv$a",
        "<TH;TB;>;>",
        "Lapn$a",
        "<",
        "Lcom/twitter/model/dms/e;",
        "TH;",
        "Lapv$a",
        "<TH;TB;>;>;"
    }
.end annotation


# instance fields
.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Lcom/twitter/app/dm/v;

.field private i:Lcom/twitter/app/dm/f;

.field private j:Lcne;

.field private k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private l:Lcom/twitter/library/network/t;

.field private m:Ljava/text/SimpleDateFormat;

.field private n:Ljava/text/SimpleDateFormat;

.field private o:Ljava/text/SimpleDateFormat;

.field private p:Lcom/twitter/android/bn;

.field private q:Lcdu;

.field private r:Lcom/twitter/app/dm/p;

.field private s:Lcom/twitter/app/dm/o;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 895
    invoke-direct {p0}, Lapn$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lapv$a;)Z
    .locals 1

    .prologue
    .line 895
    iget-boolean v0, p0, Lapv$a;->d:Z

    return v0
.end method

.method static synthetic b(Lapv$a;)Z
    .locals 1

    .prologue
    .line 895
    iget-boolean v0, p0, Lapv$a;->e:Z

    return v0
.end method

.method static synthetic c(Lapv$a;)Lcom/twitter/app/dm/v;
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lapv$a;->h:Lcom/twitter/app/dm/v;

    return-object v0
.end method

.method static synthetic d(Lapv$a;)Lcom/twitter/app/dm/f;
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lapv$a;->i:Lcom/twitter/app/dm/f;

    return-object v0
.end method

.method static synthetic e(Lapv$a;)Lcne;
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lapv$a;->j:Lcne;

    return-object v0
.end method

.method static synthetic f(Lapv$a;)Lcom/twitter/library/network/t;
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lapv$a;->l:Lcom/twitter/library/network/t;

    return-object v0
.end method

.method static synthetic g(Lapv$a;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lapv$a;->k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    return-object v0
.end method

.method static synthetic h(Lapv$a;)Ljava/text/SimpleDateFormat;
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lapv$a;->m:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method static synthetic i(Lapv$a;)Ljava/text/SimpleDateFormat;
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lapv$a;->n:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method static synthetic j(Lapv$a;)Ljava/text/SimpleDateFormat;
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lapv$a;->o:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method static synthetic k(Lapv$a;)Z
    .locals 1

    .prologue
    .line 895
    iget-boolean v0, p0, Lapv$a;->f:Z

    return v0
.end method

.method static synthetic l(Lapv$a;)Lcom/twitter/android/bn;
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lapv$a;->p:Lcom/twitter/android/bn;

    return-object v0
.end method

.method static synthetic m(Lapv$a;)Lcdu;
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lapv$a;->q:Lcdu;

    return-object v0
.end method

.method static synthetic n(Lapv$a;)Lcom/twitter/app/dm/p;
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lapv$a;->r:Lcom/twitter/app/dm/p;

    return-object v0
.end method

.method static synthetic o(Lapv$a;)Lcom/twitter/app/dm/o;
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lapv$a;->s:Lcom/twitter/app/dm/o;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 916
    invoke-super {p0}, Lapn$a;->R_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapv$a;->h:Lcom/twitter/app/dm/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapv$a;->i:Lcom/twitter/app/dm/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapv$a;->j:Lcne;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapv$a;->k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapv$a;->l:Lcom/twitter/library/network/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapv$a;->m:Ljava/text/SimpleDateFormat;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapv$a;->n:Ljava/text/SimpleDateFormat;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapv$a;->o:Ljava/text/SimpleDateFormat;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapv$a;->r:Lcom/twitter/app/dm/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapv$a;->s:Lcom/twitter/app/dm/o;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcdu;)Lapv$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcdu;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 1009
    iput-object p1, p0, Lapv$a;->q:Lcdu;

    .line 1010
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapv$a;

    return-object v0
.end method

.method public a(Lcne;)Lapv$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcne;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 967
    iput-object p1, p0, Lapv$a;->j:Lcne;

    .line 968
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapv$a;

    return-object v0
.end method

.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lapv$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 973
    iput-object p1, p0, Lapv$a;->k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 974
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapv$a;

    return-object v0
.end method

.method public a(Lcom/twitter/android/bn;)Lapv$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/bn;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 1003
    iput-object p1, p0, Lapv$a;->p:Lcom/twitter/android/bn;

    .line 1004
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapv$a;

    return-object v0
.end method

.method public a(Lcom/twitter/app/dm/f;)Lapv$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/app/dm/f;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 961
    iput-object p1, p0, Lapv$a;->i:Lcom/twitter/app/dm/f;

    .line 962
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapv$a;

    return-object v0
.end method

.method public a(Lcom/twitter/app/dm/o;)Lapv$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/app/dm/o;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 1021
    iput-object p1, p0, Lapv$a;->s:Lcom/twitter/app/dm/o;

    .line 1022
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapv$a;

    return-object v0
.end method

.method public a(Lcom/twitter/app/dm/p;)Lapv$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/app/dm/p;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 1015
    iput-object p1, p0, Lapv$a;->r:Lcom/twitter/app/dm/p;

    .line 1016
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapv$a;

    return-object v0
.end method

.method public a(Lcom/twitter/app/dm/v;)Lapv$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/app/dm/v;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 955
    iput-object p1, p0, Lapv$a;->h:Lcom/twitter/app/dm/v;

    .line 956
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapv$a;

    return-object v0
.end method

.method public a(Lcom/twitter/library/network/t;)Lapv$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/network/t;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 979
    iput-object p1, p0, Lapv$a;->l:Lcom/twitter/library/network/t;

    .line 980
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapv$a;

    return-object v0
.end method

.method public a(Ljava/text/SimpleDateFormat;)Lapv$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/text/SimpleDateFormat;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 985
    iput-object p1, p0, Lapv$a;->m:Ljava/text/SimpleDateFormat;

    .line 986
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapv$a;

    return-object v0
.end method

.method public b(Ljava/text/SimpleDateFormat;)Lapv$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/text/SimpleDateFormat;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 991
    iput-object p1, p0, Lapv$a;->n:Ljava/text/SimpleDateFormat;

    .line 992
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapv$a;

    return-object v0
.end method

.method public b(Z)Lapv$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TB;"
        }
    .end annotation

    .prologue
    .line 931
    iput-boolean p1, p0, Lapv$a;->d:Z

    .line 932
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapv$a;

    return-object v0
.end method

.method public c(Ljava/text/SimpleDateFormat;)Lapv$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/text/SimpleDateFormat;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 997
    iput-object p1, p0, Lapv$a;->o:Ljava/text/SimpleDateFormat;

    .line 998
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapv$a;

    return-object v0
.end method

.method public c(Z)Lapv$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TB;"
        }
    .end annotation

    .prologue
    .line 937
    iput-boolean p1, p0, Lapv$a;->e:Z

    .line 938
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapv$a;

    return-object v0
.end method

.method protected c_()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 924
    invoke-super {p0}, Lapn$a;->c_()V

    .line 925
    iget-object v0, p0, Lapv$a;->b:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    iget-boolean v1, p0, Lapv$a;->e:Z

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    iget-boolean v4, p0, Lapv$a;->f:Z

    if-nez v4, :cond_1

    move v4, v2

    :goto_1
    iget-boolean v5, p0, Lapv$a;->g:Z

    if-nez v5, :cond_2

    :goto_2
    invoke-virtual {v0, v1, v4, v2}, Lcom/twitter/model/dms/e;->b(ZZZ)Lcom/twitter/model/dms/e;

    move-result-object v0

    iput-object v0, p0, Lapv$a;->b:Lcom/twitter/model/dms/c;

    .line 927
    return-void

    :cond_0
    move v1, v3

    .line 925
    goto :goto_0

    :cond_1
    move v4, v3

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2
.end method

.method public d(Z)Lapv$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TB;"
        }
    .end annotation

    .prologue
    .line 943
    iput-boolean p1, p0, Lapv$a;->f:Z

    .line 944
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapv$a;

    return-object v0
.end method

.method public e(Z)Lapv$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TB;"
        }
    .end annotation

    .prologue
    .line 949
    iput-boolean p1, p0, Lapv$a;->g:Z

    .line 950
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapv$a;

    return-object v0
.end method
