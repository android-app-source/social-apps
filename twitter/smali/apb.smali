.class public Lapb;
.super Laop;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lapb$a;
    }
.end annotation


# instance fields
.field public final d:Landroid/net/Uri;

.field public final e:[Ljava/lang/String;


# direct methods
.method private constructor <init>(Lapb$a;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1}, Laop;-><init>(Laop$a;)V

    .line 21
    iget-object v0, p1, Lapb$a;->d:Landroid/net/Uri;

    iput-object v0, p0, Lapb;->d:Landroid/net/Uri;

    .line 22
    iget-object v0, p1, Lapb$a;->e:[Ljava/lang/String;

    iput-object v0, p0, Lapb;->e:[Ljava/lang/String;

    .line 23
    return-void
.end method

.method synthetic constructor <init>(Lapb$a;Lapb$1;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lapb;-><init>(Lapb$a;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-super {p0, p1}, Laop;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    instance-of v0, p1, Lapb;

    if-eqz v0, :cond_1

    .line 58
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapb;

    .line 59
    iget-object v2, p0, Lapb;->d:Landroid/net/Uri;

    iget-object v3, v0, Lapb;->d:Landroid/net/Uri;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lapb;->e:[Ljava/lang/String;

    iget-object v0, v0, Lapb;->e:[Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 61
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 59
    goto :goto_0

    :cond_1
    move v0, v1

    .line 61
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 67
    invoke-super {p0}, Laop;->hashCode()I

    move-result v0

    .line 68
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lapb;->d:Landroid/net/Uri;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lapb;->e:[Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    return v0
.end method
