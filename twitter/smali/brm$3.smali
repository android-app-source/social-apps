.class Lbrm$3;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbrm;->a(Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/Map;

.field final synthetic b:Lbrm;


# direct methods
.method constructor <init>(Lbrm;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lbrm$3;->b:Lbrm;

    iput-object p2, p0, Lbrm$3;->a:Ljava/util/Map;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 143
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lbrm$3;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 6

    .prologue
    .line 146
    check-cast p1, Lbrl;

    .line 147
    invoke-virtual {p1}, Lbrl;->g()Lcdr;

    move-result-object v0

    .line 148
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcdr;->a:Ljava/util/List;

    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 149
    iget-object v1, v0, Lcdr;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 150
    iget-object v0, v0, Lcdr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdq;

    .line 151
    iget-object v3, p0, Lbrm$3;->a:Ljava/util/Map;

    iget-wide v4, v0, Lcdq;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    iget-wide v4, v0, Lcdq;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 154
    :cond_0
    new-instance v2, Lbrm$a;

    iget-object v0, p0, Lbrm$3;->b:Lbrm;

    .line 155
    invoke-static {v0}, Lbrm;->a(Lbrm;)Lcom/twitter/library/provider/t;

    move-result-object v3

    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v1, p0, Lbrm$3;->a:Ljava/util/Map;

    .line 156
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v2, v3, v0, v1}, Lbrm$a;-><init>(Lcom/twitter/library/provider/t;Ljava/util/List;Ljava/util/Set;)V

    .line 157
    iget-object v0, p0, Lbrm$3;->b:Lbrm;

    invoke-static {v0}, Lbrm;->b(Lbrm;)Lcom/twitter/library/client/p;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 159
    :cond_1
    iget-object v0, p0, Lbrm$3;->b:Lbrm;

    iget-object v1, p0, Lbrm$3;->a:Ljava/util/Map;

    invoke-static {v0, v1}, Lbrm;->b(Lbrm;Ljava/util/Map;)V

    .line 160
    return-void
.end method
