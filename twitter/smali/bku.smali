.class public Lbku;
.super Lbkq;
.source "Twttr"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field protected b:Z


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVDataSource;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lbkq;-><init>(Lcom/twitter/library/av/playback/AVDataSource;)V

    .line 65
    return-void
.end method

.method private a([Lcom/twitter/model/av/Video;)Lcom/twitter/model/av/Video;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 137
    if-eqz p1, :cond_0

    array-length v0, p1

    if-ge v0, v1, :cond_1

    .line 138
    :cond_0
    const/4 v0, 0x0

    .line 141
    :goto_0
    return-object v0

    :cond_1
    array-length v0, p1

    if-le v0, v1, :cond_2

    aget-object v0, p1, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    aget-object v0, p1, v0

    goto :goto_0
.end method

.method private a([Lcom/twitter/model/av/Video;Lcom/twitter/model/av/DynamicAdInfo;)Lcom/twitter/model/av/Video;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 118
    iget-boolean v1, p0, Lbku;->b:Z

    if-eqz v1, :cond_1

    .line 132
    :cond_0
    :goto_0
    return-object v0

    .line 123
    :cond_1
    if-eqz p2, :cond_2

    iget-object v1, p2, Lcom/twitter/model/av/DynamicAdInfo;->a:Lcom/twitter/model/av/DynamicAd;

    if-eqz v1, :cond_2

    .line 124
    iget-object v1, p2, Lcom/twitter/model/av/DynamicAdInfo;->a:Lcom/twitter/model/av/DynamicAd;

    .line 126
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v2

    invoke-virtual {v2}, Lcrr;->e()Lcom/twitter/util/network/c;

    move-result-object v2

    .line 127
    invoke-virtual {v1}, Lcom/twitter/model/av/DynamicAd;->c()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lbku;->a(Lcom/twitter/util/network/c;Ljava/util/List;)Lcom/twitter/util/collection/k;

    move-result-object v2

    .line 129
    invoke-virtual {v2}, Lcom/twitter/util/collection/k;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/twitter/util/collection/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/model/av/DynamicAd;->a(Ljava/lang/String;)Lcom/twitter/model/av/Video;

    move-result-object v0

    goto :goto_0

    .line 132
    :cond_2
    if-eqz p1, :cond_0

    array-length v1, p1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_0

    const/4 v0, 0x0

    aget-object v0, p1, v0

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/twitter/network/j;Lcom/twitter/network/HttpOperation;Ljava/util/Map;Lcom/twitter/model/av/DynamicAdInfo;)Lcom/twitter/model/av/VideoPlaylist;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/j;",
            "Lcom/twitter/network/HttpOperation;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/twitter/model/av/DynamicAdInfo;",
            ")",
            "Lcom/twitter/model/av/VideoPlaylist;"
        }
    .end annotation

    .prologue
    .line 101
    check-cast p1, Lcom/twitter/library/av/model/parser/b;

    .line 102
    const/4 v5, 0x0

    .line 103
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 104
    iget v0, p1, Lcom/twitter/library/av/model/parser/b;->b:I

    if-nez v0, :cond_0

    .line 105
    const/4 v0, 0x1

    iput v0, p1, Lcom/twitter/library/av/model/parser/b;->b:I

    .line 107
    :cond_0
    invoke-static {p2}, Lbku;->a(Lcom/twitter/network/HttpOperation;)Ljava/lang/String;

    move-result-object v5

    .line 110
    :cond_1
    iget-object v0, p1, Lcom/twitter/library/av/model/parser/b;->a:[Lcom/twitter/model/av/Video;

    invoke-direct {p0, v0, p4}, Lbku;->a([Lcom/twitter/model/av/Video;Lcom/twitter/model/av/DynamicAdInfo;)Lcom/twitter/model/av/Video;

    move-result-object v2

    .line 111
    iget-object v0, p1, Lcom/twitter/library/av/model/parser/b;->a:[Lcom/twitter/model/av/Video;

    invoke-direct {p0, v0}, Lbku;->a([Lcom/twitter/model/av/Video;)Lcom/twitter/model/av/Video;

    move-result-object v1

    .line 113
    new-instance v0, Lcom/twitter/model/av/VideoPlaylist;

    iget v3, p1, Lcom/twitter/library/av/model/parser/b;->b:I

    move-object v4, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/model/av/VideoPlaylist;-><init>(Lcom/twitter/model/av/Video;Lcom/twitter/model/av/Video;ILjava/util/Map;Ljava/lang/String;Lcom/twitter/model/av/DynamicAdInfo;)V

    return-object v0
.end method

.method protected synthetic a(Landroid/content/Context;)Lcom/twitter/network/j;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lbku;->f(Landroid/content/Context;)Lcom/twitter/library/av/model/parser/b;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/util/network/DownloadQuality;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 230
    invoke-virtual {p0}, Lbku;->c()Ljava/util/List;

    move-result-object v0

    .line 233
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {}, Lcom/twitter/util/network/DownloadQuality;->a()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 234
    invoke-virtual {p1}, Lcom/twitter/util/network/DownloadQuality;->b()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 239
    :goto_0
    return-object v0

    .line 236
    :cond_0
    invoke-virtual {p0}, Lbku;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/util/network/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p1, Lcom/twitter/util/network/c;->a:Lcom/twitter/util/network/DownloadQuality;

    invoke-virtual {p0, v0}, Lbku;->a(Lcom/twitter/util/network/DownloadQuality;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/content/Context;Ljava/util/Map;Lcom/twitter/util/network/c;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/twitter/util/network/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 175
    iget-object v0, p3, Lcom/twitter/util/network/c;->a:Lcom/twitter/util/network/DownloadQuality;

    .line 181
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->p()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 182
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 183
    const-string/jumbo v2, "video_multi_bitrate_network_type"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 184
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 186
    :try_start_0
    invoke-static {v1}, Lcom/twitter/util/network/DownloadQuality;->valueOf(Ljava/lang/String;)Lcom/twitter/util/network/DownloadQuality;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 193
    :cond_0
    :goto_0
    invoke-virtual {p0, p3}, Lbku;->a(Lcom/twitter/util/network/c;)Ljava/lang/String;

    move-result-object v1

    .line 195
    const-string/jumbo v2, "Detected-Bandwidth"

    invoke-interface {p2, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    const-string/jumbo v1, "Network-Quality-Bucket"

    invoke-virtual {v0}, Lcom/twitter/util/network/DownloadQuality;->name()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    const-string/jumbo v0, "Carrier-Name"

    iget-object v1, p3, Lcom/twitter/util/network/c;->c:Ljava/lang/String;

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    invoke-static {}, Lbiv;->a()Lbiv;

    move-result-object v0

    .line 206
    const-string/jumbo v1, "Android-Profile-Main"

    invoke-virtual {v0}, Lbiv;->b()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    const-string/jumbo v1, "Android-Profile-High"

    invoke-virtual {v0}, Lbiv;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    return-void

    .line 187
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method protected a(Landroid/net/Uri$Builder;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri$Builder;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 166
    const-string/jumbo v1, "Detected-Bandwidth"

    const-string/jumbo v0, "Detected-Bandwidth"

    .line 167
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 166
    invoke-virtual {p1, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 168
    const-string/jumbo v1, "Android-Profile-Main"

    const-string/jumbo v0, "Android-Profile-Main"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 169
    const-string/jumbo v1, "Android-Profile-High"

    const-string/jumbo v0, "Android-Profile-High"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 170
    return-void
.end method

.method protected synthetic b(Lcom/twitter/network/j;Lcom/twitter/network/HttpOperation;Ljava/util/Map;Lcom/twitter/model/av/DynamicAdInfo;)Lcom/twitter/model/av/AVMediaPlaylist;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1, p2, p3, p4}, Lbku;->a(Lcom/twitter/network/j;Lcom/twitter/network/HttpOperation;Ljava/util/Map;Lcom/twitter/model/av/DynamicAdInfo;)Lcom/twitter/model/av/VideoPlaylist;

    move-result-object v0

    return-object v0
.end method

.method protected b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 246
    invoke-virtual {p0}, Lbku;->d()Ljava/lang/String;

    move-result-object v0

    .line 248
    const-string/jumbo v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    if-nez v0, :cond_1

    .line 249
    :cond_0
    const-string/jumbo v0, "600"

    .line 252
    :cond_1
    return-object v0
.end method

.method protected c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 259
    const-string/jumbo v0, "amplify_video_bitrate_buckets"

    invoke-static {v0}, Lcoj;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 266
    const-string/jumbo v0, "amplify_video_bitrate_default"

    invoke-static {v0}, Lcoj;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected f(Landroid/content/Context;)Lcom/twitter/library/av/model/parser/b;
    .locals 1

    .prologue
    .line 86
    invoke-static {}, Lcom/twitter/library/av/model/parser/VideoVmapPlaylistParser;->a()Lcom/twitter/library/av/model/parser/VideoVmapPlaylistParser;

    move-result-object v0

    return-object v0
.end method
