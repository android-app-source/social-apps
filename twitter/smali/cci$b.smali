.class public Lcci$b;
.super Lcbx$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcci;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbx$b",
        "<",
        "Lcci;",
        "Lcci$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcbx$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcci$a;
    .locals 1

    .prologue
    .line 105
    new-instance v0, Lcci$a;

    invoke-direct {v0}, Lcci$a;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcbx$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 93
    check-cast p2, Lcci$a;

    invoke-virtual {p0, p1, p2, p3}, Lcci$b;->a(Lcom/twitter/util/serialization/n;Lcci$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcci$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 112
    invoke-super {p0, p1, p2, p3}, Lcbx$b;->a(Lcom/twitter/util/serialization/n;Lcbx$a;I)V

    .line 113
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcci$a;->a(J)Lcci$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/core/r;->a:Lcom/twitter/util/serialization/l;

    .line 114
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/r;

    invoke-virtual {v1, v0}, Lcci$a;->a(Lcom/twitter/model/core/r;)Lcci$a;

    .line 115
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 93
    check-cast p2, Lcci$a;

    invoke-virtual {p0, p1, p2, p3}, Lcci$b;->a(Lcom/twitter/util/serialization/n;Lcci$a;I)V

    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/util/serialization/o;Lcbx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    check-cast p2, Lcci;

    invoke-virtual {p0, p1, p2}, Lcci$b;->a(Lcom/twitter/util/serialization/o;Lcci;)V

    return-void
.end method

.method public a(Lcom/twitter/util/serialization/o;Lcci;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    invoke-super {p0, p1, p2}, Lcbx$b;->a(Lcom/twitter/util/serialization/o;Lcbx;)V

    .line 98
    iget-wide v0, p2, Lcci;->c:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcci;->d:Lcom/twitter/model/core/r;

    sget-object v2, Lcom/twitter/model/core/r;->a:Lcom/twitter/util/serialization/l;

    .line 99
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 100
    return-void
.end method

.method public synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    check-cast p2, Lcci;

    invoke-virtual {p0, p1, p2}, Lcci$b;->a(Lcom/twitter/util/serialization/o;Lcci;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lcci$b;->a()Lcci$a;

    move-result-object v0

    return-object v0
.end method
