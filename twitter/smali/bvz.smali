.class public abstract Lbvz;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbvz$b;,
        Lbvz$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lbvz$a",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lbvz$a",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/Object;

.field private final d:I

.field private final e:J


# direct methods
.method constructor <init>(IIJ)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbvz;->c:Ljava/lang/Object;

    .line 43
    new-instance v0, Lbvz$1;

    invoke-direct {v0, p0, p1}, Lbvz$1;-><init>(Lbvz;I)V

    iput-object v0, p0, Lbvz;->a:Landroid/util/LruCache;

    .line 49
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0, p2}, Ljava/util/LinkedHashSet;-><init>(I)V

    iput-object v0, p0, Lbvz;->b:Ljava/util/Set;

    .line 50
    iput p2, p0, Lbvz;->d:I

    .line 51
    iput-wide p3, p0, Lbvz;->e:J

    .line 52
    return-void
.end method

.method private b(Ljava/lang/Object;)Lbvz$a;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lbvz$a",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 145
    new-instance v0, Lbvz$a;

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    iget-wide v4, p0, Lbvz;->e:J

    add-long/2addr v2, v4

    const/4 v1, 0x0

    invoke-direct {v0, p1, v2, v3, v1}, Lbvz$a;-><init>(Ljava/lang/Object;JLbvz$1;)V

    return-object v0
.end method

.method private c()Lbvz$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbvz$a",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v1, p0, Lbvz;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 71
    :try_start_0
    invoke-direct {p0}, Lbvz;->d()V

    .line 72
    iget-object v0, p0, Lbvz;->b:Ljava/util/Set;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvz$a;

    .line 73
    if-eqz v0, :cond_0

    .line 74
    iget-object v2, p0, Lbvz;->b:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 76
    :cond_0
    monitor-exit v1

    return-object v0

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 122
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 123
    iget-object v0, p0, Lbvz;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvz$a;

    .line 124
    invoke-virtual {v0}, Lbvz$a;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 125
    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 129
    :cond_1
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 130
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 131
    new-instance v1, Lbvz$b;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lbvz$b;-><init>(Lbvz$1;)V

    .line 132
    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;Lcpp;)Ljava/util/List;

    move-result-object v1

    .line 134
    invoke-virtual {p0, v1}, Lbvz;->a(Ljava/lang/Iterable;)V

    .line 135
    iget-object v1, p0, Lbvz;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 137
    :cond_2
    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 100
    iget-object v1, p0, Lbvz;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 101
    :try_start_0
    invoke-direct {p0}, Lbvz;->d()V

    .line 102
    iget-object v0, p0, Lbvz;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lbvz;->a:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvz$a;

    .line 57
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbvz$a;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 58
    iget-object v0, v0, Lbvz$a;->a:Ljava/lang/Object;

    .line 66
    :goto_0
    return-object v0

    .line 61
    :cond_0
    invoke-direct {p0}, Lbvz;->c()Lbvz$a;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_1

    .line 63
    iget-object v1, p0, Lbvz;->a:Landroid/util/LruCache;

    invoke-virtual {v1, p1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    :cond_1
    if-eqz v0, :cond_2

    iget-object v0, v0, Lbvz$a;->a:Ljava/lang/Object;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method abstract a(Ljava/lang/Iterable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<TT;>;)V"
        }
    .end annotation
.end method

.method abstract a(ZLjava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "TT;TT;)V"
        }
    .end annotation
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v1, p0, Lbvz;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 82
    :try_start_0
    invoke-direct {p0}, Lbvz;->d()V

    .line 83
    iget-object v0, p0, Lbvz;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    iget v2, p0, Lbvz;->d:I

    if-ge v0, v2, :cond_0

    .line 84
    iget-object v0, p0, Lbvz;->b:Ljava/util/Set;

    invoke-direct {p0, p1}, Lbvz;->b(Ljava/lang/Object;)Lbvz$a;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    .line 86
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lbvz;->d:I

    return v0
.end method
