.class public Lakm;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lakm$a;
    }
.end annotation


# static fields
.field public static final a:Lakm$a;


# instance fields
.field private final b:Landroid/accounts/AccountManager;

.field private final c:Landroid/accounts/Account;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lakm$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lakm$a;-><init>(Lakm$1;)V

    sput-object v0, Lakm;->a:Lakm$a;

    return-void
.end method

.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lakm;->b:Landroid/accounts/AccountManager;

    .line 41
    iput-object p2, p0, Lakm;->c:Landroid/accounts/Account;

    .line 42
    return-void
.end method


# virtual methods
.method public a()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lakm;->c:Landroid/accounts/Account;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lakm;->b:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lakm;->c:Landroid/accounts/Account;

    invoke-virtual {v0, v1, p1}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AuthenticatorException;,
            Landroid/accounts/OperationCanceledException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    iget-object v0, p0, Lakm;->b:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lakm;->c:Landroid/accounts/Account;

    invoke-virtual {v0, v1, p1, p2}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 153
    const-string/jumbo v0, "account_field_version"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lakm;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    return-void
.end method

.method public a(Lcnz;)V
    .locals 2

    .prologue
    .line 110
    const-string/jumbo v0, "account_user_id"

    invoke-virtual {p1}, Lcnz;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lakm;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lakm;->b:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lakm;->c:Landroid/accounts/Account;

    invoke-virtual {v0, v1, p1, p2}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method public b()Lcnz;
    .locals 4

    .prologue
    .line 97
    const-string/jumbo v0, "account_user_id"

    invoke-virtual {p0, v0}, Lakm;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 98
    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    sget-object v0, Lcnz;->b:Lcnz;

    .line 101
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcnz;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcnz;-><init>(J)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lakm;->b:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lakm;->c:Landroid/accounts/Account;

    invoke-virtual {v0, v1, p1}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 115
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lakm;->b:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lakm;->c:Landroid/accounts/Account;

    invoke-virtual {v0, v1, p1, p2}, Landroid/accounts/AccountManager;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    return-void
.end method

.method public c()Landroid/accounts/AccountManagerFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 119
    iget-object v0, p0, Lakm;->b:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lakm;->c:Landroid/accounts/Account;

    invoke-virtual {v0, v1, v2, v2}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lakm;->b:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lakm;->c:Landroid/accounts/Account;

    invoke-virtual {v0, v1, p1}, Landroid/accounts/AccountManager;->peekAuthToken(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lakm;->c:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 136
    const-string/jumbo v1, "account_field_version"

    invoke-virtual {p0, v1}, Lakm;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 137
    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 144
    :cond_0
    :goto_0
    return v0

    .line 140
    :cond_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 141
    if-lt v1, v0, :cond_0

    move v0, v1

    .line 144
    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 169
    if-ne p1, p0, :cond_1

    .line 175
    :cond_0
    :goto_0
    return v0

    .line 172
    :cond_1
    instance-of v1, p1, Landroid/accounts/Account;

    if-eqz v1, :cond_2

    .line 173
    iget-object v0, p0, Lakm;->c:Landroid/accounts/Account;

    invoke-virtual {v0, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 175
    :cond_2
    instance-of v1, p1, Lakm;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lakm;->c:Landroid/accounts/Account;

    check-cast p1, Lakm;

    iget-object v2, p1, Lakm;->c:Landroid/accounts/Account;

    invoke-virtual {v1, v2}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lakm;->b:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lakm;->c:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getPassword(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lakm;->c:Landroid/accounts/Account;

    invoke-virtual {v0}, Landroid/accounts/Account;->hashCode()I

    move-result v0

    return v0
.end method
