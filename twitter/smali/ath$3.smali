.class Lath$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lath;-><init>(Lath$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/q",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lath;


# direct methods
.method constructor <init>(Lath;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lath$3;->a:Lath;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEvent(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 250
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 251
    iget-object v0, p0, Lath$3;->a:Lath;

    invoke-static {v0}, Lath;->b(Lath;)Lcom/twitter/android/widget/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/widget/j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lath$3;->a:Lath;

    invoke-static {v0}, Lath;->b(Lath;)Lcom/twitter/android/widget/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/widget/j;->c()V

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 254
    :cond_1
    iget-object v0, p0, Lath$3;->a:Lath;

    invoke-static {v0}, Lath;->c(Lath;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lath$3;->a:Lath;

    invoke-static {v0}, Lath;->b(Lath;)Lcom/twitter/android/widget/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/widget/j;->b()V

    .line 256
    iget-object v0, p0, Lath$3;->a:Lath;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lath;->a(Lath;Z)Z

    goto :goto_0
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 247
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lath$3;->onEvent(Ljava/lang/Boolean;)V

    return-void
.end method
