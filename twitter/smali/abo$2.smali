.class Labo$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Labo;->e()Lrx/functions/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/d",
        "<",
        "Lcom/twitter/media/request/ImageResponse;",
        "Lrx/g",
        "<",
        "Ljava/lang/Integer;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Labo;


# direct methods
.method constructor <init>(Labo;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Labo$2;->a:Labo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 111
    check-cast p1, Lcom/twitter/media/request/ImageResponse;

    invoke-virtual {p0, p1}, Labo$2;->a(Lcom/twitter/media/request/ImageResponse;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/media/request/ImageResponse;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/media/request/ImageResponse;",
            ")",
            "Lrx/g",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 116
    iget-object v1, p0, Labo$2;->a:Labo;

    invoke-static {v1}, Labo;->b(Labo;)Lcom/twitter/android/moments/viewmodels/k;

    move-result-object v1

    invoke-interface {v1}, Lcom/twitter/android/moments/viewmodels/k;->c()Lcom/twitter/model/moments/e;

    move-result-object v1

    iget-boolean v1, v1, Lcom/twitter/model/moments/e;->h:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 117
    iget-object v1, p0, Labo$2;->a:Labo;

    invoke-static {v1}, Labo;->c(Labo;)Labn;

    move-result-object v1

    invoke-virtual {v1, v0}, Labn;->a(Landroid/graphics/Bitmap;)Lrx/g;

    move-result-object v0

    .line 119
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    goto :goto_0
.end method
