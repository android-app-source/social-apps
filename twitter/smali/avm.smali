.class public Lavm;
.super Lcbr;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbr",
        "<",
        "Lawx$a;",
        "Lcom/twitter/model/dms/m;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcbr;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lawx$a;)Lcom/twitter/model/dms/m;
    .locals 4

    .prologue
    .line 19
    const-class v0, Lawu$a;

    const-class v1, Lcom/twitter/model/dms/c;

    .line 20
    invoke-static {v0, v1}, Lcom/twitter/database/hydrator/b;->a(Ljava/lang/Class;Ljava/lang/Class;)Lcbr;

    move-result-object v0

    .line 23
    if-eqz v0, :cond_0

    .line 24
    invoke-virtual {v0, p1}, Lcbr;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/c;

    .line 29
    :goto_0
    new-instance v1, Lcom/twitter/model/dms/m$a;

    invoke-direct {v1}, Lcom/twitter/model/dms/m$a;-><init>()V

    .line 30
    invoke-interface {p1}, Lawx$a;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/model/dms/m$a;->a(J)Lcom/twitter/model/dms/m$a;

    move-result-object v1

    .line 31
    invoke-interface {p1}, Lawx$a;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/model/dms/m$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/m$a;

    move-result-object v1

    .line 32
    invoke-interface {p1}, Lawx$a;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/model/dms/m$a;->b(Ljava/lang/String;)Lcom/twitter/model/dms/m$a;

    move-result-object v1

    .line 33
    invoke-interface {p1}, Lawx$a;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/model/dms/m$a;->c(Ljava/lang/String;)Lcom/twitter/model/dms/m$a;

    move-result-object v1

    .line 34
    invoke-interface {p1}, Lawx$a;->k()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/model/dms/m$a;->a(Z)Lcom/twitter/model/dms/m$a;

    move-result-object v1

    .line 35
    invoke-virtual {v1, v0}, Lcom/twitter/model/dms/m$a;->a(Lcom/twitter/model/dms/c;)Lcom/twitter/model/dms/m$a;

    move-result-object v0

    .line 36
    invoke-virtual {v0}, Lcom/twitter/model/dms/m$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/m;

    .line 29
    return-object v0

    .line 26
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    check-cast p1, Lawx$a;

    invoke-virtual {p0, p1}, Lavm;->a(Lawx$a;)Lcom/twitter/model/dms/m;

    move-result-object v0

    return-object v0
.end method
