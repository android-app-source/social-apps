.class public interface abstract Laye;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/database/model/p;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laye$a;
    }
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 70
    const/16 v0, 0x1d

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "moments_title"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "moments_can_subscribe"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "moments_is_live"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "moments_is_sensitive"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "moments_subcategory_string"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "moments_subcategory_favicon_url"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "moments_time_string"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "moments_duration_string"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "moments_is_subscribed"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "moments_description"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "moments_moment_url"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "moments_num_subscribers"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "moments_author_info"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "moments_promoted_content"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "moments_event_id"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "moments_event_type"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "moment_sports_events_value"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "moments_pages_moment_id"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "moments_curation_metadata"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "moments_pages_tweet_id"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "moments_pages_page_id"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "moments_pages_page_number"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "moments_pages_last_read_timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "moments_pages_capsule_page_data"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "moments_pages_meta_type"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "tweet_flags"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "user_friendship"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "moments_is_liked"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "moments_total_likes"

    aput-object v2, v0, v1

    sput-object v0, Laye;->a:[Ljava/lang/String;

    return-void
.end method
