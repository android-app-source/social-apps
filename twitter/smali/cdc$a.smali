.class public Lcdc$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcdc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcdc$a$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcdc$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 314
    new-instance v0, Lcdc$a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcdc$a$a;-><init>(Lcdc$1;)V

    sput-object v0, Lcdc$a;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 324
    iput-object p1, p0, Lcdc$a;->b:Ljava/lang/String;

    .line 325
    iput p2, p0, Lcdc$a;->c:I

    .line 326
    iput-object p3, p0, Lcdc$a;->d:Ljava/lang/String;

    .line 327
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcdc$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lcdc$a;)Z
    .locals 2

    .prologue
    .line 357
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcdc$a;->b:Ljava/lang/String;

    iget-object v1, p1, Lcdc$a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcdc$a;->c:I

    iget v1, p1, Lcdc$a;->c:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcdc$a;->d:Ljava/lang/String;

    iget-object v1, p1, Lcdc$a;->d:Ljava/lang/String;

    .line 358
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 357
    :goto_0
    return v0

    .line 358
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 335
    iget v0, p0, Lcdc$a;->c:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcdc$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 353
    if-eq p1, p0, :cond_0

    instance-of v0, p1, Lcdc$a;

    if-eqz v0, :cond_1

    check-cast p1, Lcdc$a;

    invoke-virtual {p0, p1}, Lcdc$a;->a(Lcdc$a;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 345
    iget-object v0, p0, Lcdc$a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 346
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcdc$a;->c:I

    add-int/2addr v0, v1

    .line 347
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcdc$a;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 348
    return v0
.end method
