.class Lajz$c;
.super Lcom/twitter/library/view/a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lajz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "c"
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/twitter/android/cs;

.field private final c:Lcom/twitter/android/revenue/m;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/twitter/android/cs;Lcom/twitter/android/revenue/m;)V
    .locals 0

    .prologue
    .line 169
    invoke-direct {p0}, Lcom/twitter/library/view/a;-><init>()V

    .line 170
    iput-object p1, p0, Lajz$c;->a:Landroid/app/Activity;

    .line 171
    iput-object p2, p0, Lajz$c;->b:Lcom/twitter/android/cs;

    .line 172
    iput-object p3, p0, Lajz$c;->c:Lcom/twitter/android/revenue/m;

    .line 173
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;Lcom/twitter/library/widget/TweetView;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 183
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/twitter/library/widget/TweetView;->getContentContainer()Lcom/twitter/library/widget/renderablecontent/c;

    move-result-object v0

    .line 185
    :goto_0
    instance-of v2, v0, Lbya;

    if-eqz v2, :cond_0

    .line 186
    check-cast v0, Lbya;

    invoke-virtual {v0, p2}, Lbya;->a(Lcom/twitter/model/core/MediaEntity;)Lcom/twitter/media/ui/image/BaseMediaImageView;

    move-result-object v1

    .line 191
    :cond_0
    iget-object v0, p0, Lajz$c;->c:Lcom/twitter/android/revenue/m;

    invoke-interface {v0, p1, p2, v1}, Lcom/twitter/android/revenue/m;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;Lcom/twitter/media/ui/image/BaseMediaImageView;)V

    .line 192
    return-void

    :cond_1
    move-object v0, v1

    .line 183
    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/ad;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 177
    iget-object v0, p0, Lajz$c;->b:Lcom/twitter/android/cs;

    invoke-virtual {v0, p1, p2, v1, v1}, Lcom/twitter/android/cs;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/ad;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 178
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/h;)V
    .locals 2

    .prologue
    .line 196
    new-instance v0, Lajh$a;

    invoke-direct {v0}, Lajh$a;-><init>()V

    iget-object v1, p0, Lajz$c;->a:Landroid/app/Activity;

    .line 197
    invoke-virtual {v0, v1}, Lajh$a;->a(Landroid/app/Activity;)Lajh$a;

    move-result-object v0

    .line 198
    invoke-virtual {v0, p1}, Lajh$a;->a(Lcom/twitter/model/core/Tweet;)Lajh$a;

    move-result-object v0

    .line 199
    invoke-virtual {v0, p2}, Lajh$a;->a(Lcom/twitter/model/core/h;)Lajh$a;

    move-result-object v0

    const-string/jumbo v1, "hashtag"

    .line 200
    invoke-virtual {v0, v1}, Lajh$a;->a(Ljava/lang/String;)Lajh$a;

    move-result-object v0

    const-string/jumbo v1, "search"

    .line 201
    invoke-virtual {v0, v1}, Lajh$a;->b(Ljava/lang/String;)Lajh$a;

    move-result-object v0

    .line 202
    invoke-virtual {v0}, Lajh$a;->a()Lajh;

    move-result-object v0

    invoke-virtual {v0}, Lajh;->a()V

    .line 203
    return-void
.end method
