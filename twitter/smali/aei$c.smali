.class public Laei$c;
.super Laef$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Laei;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# instance fields
.field public final a:Lcom/twitter/media/ui/image/UserImageView;

.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/view/View;

.field public final d:Lcom/twitter/ui/widget/ActionButton;

.field public e:Lcom/twitter/model/core/TwitterUser;


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 170
    invoke-direct {p0, p1}, Laef$a;-><init>(Landroid/view/View;)V

    .line 171
    const v0, 0x7f130097

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Laei$c;->a:Lcom/twitter/media/ui/image/UserImageView;

    .line 172
    const v0, 0x7f130150

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laei$c;->b:Landroid/widget/TextView;

    .line 173
    const v0, 0x7f130003

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/ActionButton;

    iput-object v0, p0, Laei$c;->d:Lcom/twitter/ui/widget/ActionButton;

    .line 174
    iput-object p3, p0, Laei$c;->c:Landroid/view/View;

    .line 177
    iget-object v0, p0, Laei$c;->a:Lcom/twitter/media/ui/image/UserImageView;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Laei$c;->a:Lcom/twitter/media/ui/image/UserImageView;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/twitter/util/ui/a;->a(Landroid/view/View;I)V

    .line 181
    :cond_0
    return-void
.end method
