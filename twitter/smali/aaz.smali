.class public Laaz;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laap;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Laap",
        "<",
        "Laba;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Laas;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/library/client/Session;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/twitter/library/client/Session;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Laas;",
            ">;",
            "Lcom/twitter/library/client/Session;",
            ")V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Laaz;->a:Ljava/util/List;

    .line 22
    iput-object p2, p0, Laaz;->b:Lcom/twitter/library/client/Session;

    .line 23
    return-void
.end method


# virtual methods
.method public a(Laba;I)Laar;
    .locals 3

    .prologue
    .line 29
    packed-switch p2, :pswitch_data_0

    .line 37
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Binding for argument does not exist."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :pswitch_0
    new-instance v0, Laaw;

    invoke-virtual {p1}, Laba;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    iget-object v2, p0, Laaz;->b:Lcom/twitter/library/client/Session;

    invoke-direct {v0, v1, v2}, Laaw;-><init>(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/library/client/Session;)V

    .line 34
    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, Lafe;

    invoke-virtual {p1}, Laba;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    iget-object v2, p0, Laaz;->b:Lcom/twitter/library/client/Session;

    invoke-direct {v0, v1, v2}, Lafe;-><init>(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/library/client/Session;)V

    goto :goto_0

    .line 29
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic a(Laaq;)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Laba;

    invoke-virtual {p0, p1}, Laaz;->a(Laba;)V

    return-void
.end method

.method public a(Laba;)V
    .locals 3

    .prologue
    .line 44
    iget-object v0, p0, Laaz;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laas;

    .line 45
    invoke-interface {v0}, Laas;->a()I

    move-result v2

    invoke-virtual {p0, p1, v2}, Laaz;->a(Laba;I)Laar;

    move-result-object v2

    invoke-interface {v0, v2}, Laas;->a(Laar;)V

    goto :goto_0

    .line 47
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Laba;

    invoke-virtual {p0, p1}, Laaz;->a(Laba;)V

    return-void
.end method
