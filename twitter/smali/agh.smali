.class public Lagh;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Lage;

.field private c:Laii;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lage;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    sget-object v0, Laii;->b:Laii;

    iput-object v0, p0, Lagh;->c:Laii;

    .line 43
    iput-object p1, p0, Lagh;->a:Landroid/content/res/Resources;

    .line 44
    iput-object p2, p0, Lagh;->b:Lage;

    .line 45
    return-void
.end method

.method static synthetic a(Lagh;I)I
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lagh;->c(I)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lagh;)Lage;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lagh;->b:Lage;

    return-object v0
.end method

.method static synthetic a(Lagh;Ljava/lang/Class;Lcom/twitter/app/common/base/b;II)Lcom/twitter/library/client/m;
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3, p4}, Lagh;->a(Ljava/lang/Class;Lcom/twitter/app/common/base/b;II)Lcom/twitter/library/client/m;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Class;Lcom/twitter/app/common/base/b;II)Lcom/twitter/library/client/m;
    .locals 1
    .param p3    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/app/common/base/BaseFragment;",
            ">;",
            "Lcom/twitter/app/common/base/b;",
            "II)",
            "Lcom/twitter/library/client/m;"
        }
    .end annotation

    .prologue
    .line 168
    iget-object v0, p0, Lagh;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2, v0, p4}, Lagh;->a(Ljava/lang/Class;Lcom/twitter/app/common/base/b;Ljava/lang/String;I)Lcom/twitter/library/client/m;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Class;Lcom/twitter/app/common/base/b;Ljava/lang/String;I)Lcom/twitter/library/client/m;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/app/common/base/BaseFragment;",
            ">;",
            "Lcom/twitter/app/common/base/b;",
            "Ljava/lang/String;",
            "I)",
            "Lcom/twitter/library/client/m;"
        }
    .end annotation

    .prologue
    .line 175
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v1, "twitter"

    .line 176
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "search"

    .line 177
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 178
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 179
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 180
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 181
    new-instance v1, Lcom/twitter/library/client/m$a;

    invoke-direct {v1, v0, p0}, Lcom/twitter/library/client/m$a;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    .line 182
    invoke-virtual {v1, p2}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/CharSequence;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 183
    invoke-virtual {v0, p1}, Lcom/twitter/library/client/m$a;->a(Lcom/twitter/app/common/base/b;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 184
    invoke-virtual {v0}, Lcom/twitter/library/client/m$a;->a()Lcom/twitter/library/client/m;

    move-result-object v0

    .line 181
    return-object v0
.end method

.method static synthetic b(Lagh;)Laii;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lagh;->c:Laii;

    return-object v0
.end method

.method private c(I)I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 117
    packed-switch p1, :pswitch_data_0

    .line 148
    :pswitch_0
    const v0, 0x7f0a07e0

    :goto_0
    return v0

    .line 119
    :pswitch_1
    const v0, 0x7f0a0939

    goto :goto_0

    .line 123
    :pswitch_2
    const v0, 0x7f0a07d0

    goto :goto_0

    .line 127
    :pswitch_3
    const v0, 0x7f0a07e4

    goto :goto_0

    .line 131
    :pswitch_4
    const v0, 0x7f0a07d2

    goto :goto_0

    .line 135
    :pswitch_5
    const v0, 0x7f0a07cc

    goto :goto_0

    .line 139
    :pswitch_6
    const v0, 0x7f0a07cf

    goto :goto_0

    .line 143
    :pswitch_7
    const v0, 0x7f0a07e2

    goto :goto_0

    .line 117
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method

.method private c()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Lail;",
            "Lcom/twitter/library/client/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    new-instance v0, Lagh$1;

    invoke-direct {v0, p0}, Lagh$1;-><init>(Lagh;)V

    return-object v0
.end method

.method private d()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lail;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 155
    new-instance v0, Lail;

    invoke-direct {v0, v3, v3}, Lail;-><init>(IZ)V

    const/4 v1, 0x6

    new-array v1, v1, [Lail;

    new-instance v2, Lail;

    invoke-direct {v2, v5, v5}, Lail;-><init>(IZ)V

    aput-object v2, v1, v3

    new-instance v2, Lail;

    invoke-direct {v2, v4, v5}, Lail;-><init>(IZ)V

    aput-object v2, v1, v5

    new-instance v2, Lail;

    invoke-direct {v2, v6, v5}, Lail;-><init>(IZ)V

    aput-object v2, v1, v4

    new-instance v2, Lail;

    invoke-direct {v2, v7, v5}, Lail;-><init>(IZ)V

    aput-object v2, v1, v6

    const/4 v2, 0x4

    new-instance v3, Lail;

    const/4 v4, 0x6

    invoke-direct {v3, v4, v5}, Lail;-><init>(IZ)V

    aput-object v3, v1, v2

    new-instance v2, Lail;

    const/16 v3, 0xc

    invoke-direct {v2, v3, v5}, Lail;-><init>(IZ)V

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(I)I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 60
    invoke-direct {p0}, Lagh;->d()Ljava/util/List;

    move-result-object v3

    move v1, v2

    .line 61
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 62
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lail;

    .line 63
    iget v0, v0, Lail;->a:I

    if-ne v0, p1, :cond_0

    .line 67
    :goto_1
    return v1

    .line 61
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 67
    goto :goto_1
.end method

.method public a()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0}, Lagh;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lrx/c;->a(Ljava/lang/Iterable;)Lrx/c;

    move-result-object v0

    invoke-direct {p0}, Lagh;->c()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    invoke-virtual {v0}, Lrx/c;->q()Lrx/c;

    move-result-object v0

    invoke-virtual {v0}, Lrx/c;->p()Lcwa;

    move-result-object v0

    invoke-virtual {v0}, Lcwa;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public a(Laii;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lagh;->c:Laii;

    .line 72
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lagh;->b:Lage;

    invoke-virtual {v0}, Lage;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lagh;->a(I)I

    move-result v0

    return v0
.end method

.method public b(I)I
    .locals 2

    .prologue
    .line 75
    invoke-direct {p0}, Lagh;->d()Ljava/util/List;

    move-result-object v0

    .line 76
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 77
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lail;

    iget v0, v0, Lail;->a:I

    .line 79
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
