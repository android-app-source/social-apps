.class public Lapk;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/twitter/android/widget/DraggableDrawerLayout$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lapk$a;,
        Lapk$b;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/android/widget/DraggableDrawerLayout;

.field private final c:Landroid/widget/ListView;

.field private final d:Lapk$b;

.field private final e:Lapk$a;

.field private final f:I

.field private final g:Z

.field private final h:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/widget/DraggableDrawerLayout;Lapk$a;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lapk;->a:Landroid/content/Context;

    .line 52
    iput-object p3, p0, Lapk;->e:Lapk$a;

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e02ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lapk;->f:I

    .line 54
    new-instance v0, Lapk$b;

    const v1, 0x7f0400bf

    invoke-direct {v0, p1, v1}, Lapk$b;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lapk;->d:Lapk$b;

    .line 56
    iput-object p2, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    .line 57
    iget-object v0, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0, v6}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setAllowDrawerUpPositionIfKeyboard(Z)V

    .line 58
    iget-object v0, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setDraggableBelowUpPosition(Z)V

    .line 59
    iget-object v0, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setDispatchDragToChildren(Z)V

    .line 60
    iget-object v0, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setDrawerLayoutListener(Lcom/twitter/android/widget/DraggableDrawerLayout$b;)V

    .line 61
    iget-object v0, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    iget-object v1, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-direct {p0, v1}, Lapk;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setFullScreenHeaderView(Landroid/view/View;)V

    .line 63
    iget-object v0, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    const v1, 0x7f1302e5

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lapk;->c:Landroid/widget/ListView;

    .line 64
    iget-object v0, p0, Lapk;->c:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 65
    iget-object v0, p0, Lapk;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lapk;->d:Lapk$b;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 66
    iget-object v0, p0, Lapk;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 68
    const-string/jumbo v0, "dm_quick_reply_options_auto_dismiss_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lapk;->g:Z

    .line 70
    const-wide v0, 0x408f400000000000L    # 1000.0

    const-string/jumbo v2, "dm_quick_reply_options_auto_dismiss_delay_time"

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v2, v4, v5}, Lcoj;->a(Ljava/lang/String;D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lapk;->h:J

    .line 74
    invoke-virtual {p0, v6}, Lapk;->a(Z)V

    .line 75
    return-void
.end method

.method private a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 233
    iget-object v0, p0, Lapk;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0403f8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 235
    iget-object v1, p0, Lapk;->a:Landroid/content/Context;

    .line 236
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a028a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 235
    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 238
    new-instance v1, Lapk$2;

    invoke-direct {v1, p0}, Lapk$2;-><init>(Lapk;)V

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setOnToolBarItemSelectedListener(Lcmr$a;)V

    .line 255
    return-object v0
.end method

.method private a(Ljava/lang/String;Lccp;)V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lapk;->e:Lapk$a;

    invoke-interface {v0, p1, p2}, Lapk$a;->a(Ljava/lang/String;Lccp;)V

    .line 170
    return-void
.end method

.method static synthetic a(Lapk;)Z
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lapk;->i()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lapk;)Z
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lapk;->f()Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lapk;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lapk;->g()V

    return-void
.end method

.method private d()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 174
    iget-object v0, p0, Lapk;->d:Lapk$b;

    invoke-virtual {v0}, Lapk$b;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 175
    iget-object v0, p0, Lapk;->d:Lapk$b;

    invoke-virtual {v0, v1}, Lapk$b;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccp;

    .line 176
    if-eqz v0, :cond_0

    iget-object v0, v0, Lccp;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 178
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 176
    goto :goto_0

    :cond_1
    move v0, v1

    .line 178
    goto :goto_0
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 184
    invoke-direct {p0}, Lapk;->i()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lapk;->d:Lapk$b;

    invoke-virtual {v0}, Lapk$b;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 185
    :cond_0
    iget-object v0, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setDrawerDraggable(Z)V

    .line 186
    iget-object v0, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setLocked(Z)V

    .line 196
    :goto_0
    return-void

    .line 188
    :cond_1
    invoke-direct {p0}, Lapk;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    .line 189
    :goto_1
    iget v3, p0, Lapk;->f:I

    iget-object v4, p0, Lapk;->d:Lapk$b;

    invoke-virtual {v4}, Lapk$b;->getCount()I

    move-result v4

    mul-int/2addr v3, v4

    mul-int/2addr v0, v3

    .line 190
    iget-object v3, p0, Lapk;->a:Landroid/content/Context;

    .line 191
    invoke-static {v3}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;)Lcom/twitter/util/math/Size;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/util/math/Size;->b()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3ecccccd    # 0.4f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 192
    if-ge v0, v3, :cond_3

    move v0, v1

    .line 193
    :goto_2
    iget-object v3, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    if-nez v0, :cond_4

    :goto_3
    invoke-virtual {v3, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setDrawerDraggable(Z)V

    .line 194
    iget-object v1, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setLocked(Z)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 188
    goto :goto_1

    :cond_3
    move v0, v2

    .line 192
    goto :goto_2

    :cond_4
    move v1, v2

    .line 193
    goto :goto_3
.end method

.method private f()Z
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getDrawerPosition()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 207
    iget-object v0, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 209
    iget-object v0, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(ZZ)V

    .line 213
    :goto_0
    return-void

    .line 211
    :cond_0
    invoke-virtual {p0, v2}, Lapk;->a(Z)V

    goto :goto_0
.end method

.method private h()V
    .locals 4

    .prologue
    .line 217
    iget-boolean v0, p0, Lapk;->g:Z

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lapk;->c:Landroid/widget/ListView;

    new-instance v1, Lapk$1;

    invoke-direct {v1, p0}, Lapk$1;-><init>(Lapk;)V

    iget-wide v2, p0, Lapk;->h:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ListView;->postOnAnimationDelayed(Ljava/lang/Runnable;J)V

    .line 229
    :cond_0
    return-void
.end method

.method private i()Z
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lapk;->c:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(F)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 95
    iget-object v0, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0, v1, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(ZZ)V

    .line 96
    return-void
.end method

.method public a(Lccr;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 121
    invoke-virtual {p0, p1}, Lapk;->b(Lccr;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 122
    :goto_0
    if-eqz v0, :cond_0

    .line 123
    invoke-virtual {p0}, Lapk;->c()V

    .line 124
    iget-object v2, p0, Lapk;->c:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 125
    iget-object v2, p0, Lapk;->d:Lapk$b;

    iget-object v3, p1, Lccr;->c:Ljava/util/List;

    invoke-virtual {v2, v3}, Lapk$b;->addAll(Ljava/util/Collection;)V

    .line 126
    iget-object v2, p0, Lapk;->d:Lapk$b;

    invoke-virtual {p1}, Lccr;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lapk$b;->a(Lapk$b;Ljava/lang/String;)V

    .line 127
    iget-object v2, p0, Lapk;->d:Lapk$b;

    invoke-virtual {v2}, Lapk$b;->notifyDataSetChanged()V

    .line 128
    invoke-direct {p0}, Lapk;->e()V

    .line 131
    :cond_0
    invoke-virtual {p0, v1, v0}, Lapk;->a(ZZ)V

    .line 132
    return-void

    .line 121
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Z)V

    .line 144
    return-void
.end method

.method public a(ZZ)V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->e()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lapk;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    .line 136
    invoke-virtual {v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_1

    .line 138
    :cond_0
    iget-object v0, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(ZZ)V

    .line 140
    :cond_1
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lapk;->d:Lapk$b;

    invoke-static {v0}, Lapk$b;->a(Lapk$b;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lapk;->i()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 100
    invoke-virtual {p0}, Lapk;->b()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lapk;->d:Lapk$b;

    invoke-static {v0}, Lapk$b;->a(Lapk$b;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lapk;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 102
    :cond_0
    invoke-virtual {p0}, Lapk;->c()V

    .line 114
    :cond_1
    :goto_0
    return-void

    .line 103
    :cond_2
    iget-object v0, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getDrawerPosition()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 105
    invoke-direct {p0}, Lapk;->e()V

    .line 106
    iget-object v0, p0, Lapk;->e:Lapk$a;

    invoke-interface {v0}, Lapk$a;->f()V

    .line 109
    invoke-direct {p0}, Lapk;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    iget-object v0, p0, Lapk;->c:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 111
    invoke-direct {p0}, Lapk;->h()V

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getDrawerPosition()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapk;->b:Lcom/twitter/android/widget/DraggableDrawerLayout;

    .line 156
    invoke-virtual {v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getDrawerPosition()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    .line 155
    :goto_0
    return v0

    .line 156
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lccr;)Z
    .locals 2

    .prologue
    .line 147
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lccr;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lapk;->d:Lapk$b;

    invoke-static {v1}, Lapk$b;->a(Lapk$b;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 160
    invoke-direct {p0}, Lapk;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lapk;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lapk;->c:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 163
    :cond_0
    iget-object v0, p0, Lapk;->d:Lapk$b;

    invoke-virtual {v0}, Lapk$b;->clear()V

    .line 164
    iget-object v0, p0, Lapk;->d:Lapk$b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lapk$b;->a(Lapk$b;Ljava/lang/String;)V

    .line 165
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 79
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccp;

    .line 80
    iget-object v1, p0, Lapk;->d:Lapk$b;

    invoke-static {v1}, Lapk$b;->a(Lapk$b;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lapk;->a(Ljava/lang/String;Lccp;)V

    .line 81
    iget-object v0, p0, Lapk;->c:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, p3, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 82
    iget-object v0, p0, Lapk;->d:Lapk$b;

    invoke-virtual {v0}, Lapk$b;->notifyDataSetChanged()V

    .line 84
    invoke-direct {p0}, Lapk;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    invoke-direct {p0}, Lapk;->g()V

    .line 91
    :goto_0
    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lapk;->c:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 88
    invoke-direct {p0}, Lapk;->e()V

    .line 89
    invoke-direct {p0}, Lapk;->h()V

    goto :goto_0
.end method

.method public r()V
    .locals 0

    .prologue
    .line 118
    return-void
.end method
