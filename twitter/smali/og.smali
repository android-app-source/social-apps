.class public Log;
.super Lbjb;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/av/playback/AVPlayer;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p2}, Lbjb;-><init>(Lcom/twitter/model/av/AVMedia;)V

    .line 32
    iput-object p1, p0, Log;->a:Lcom/twitter/library/av/playback/AVPlayer;

    .line 33
    return-void
.end method


# virtual methods
.method public processCloseEvent(Lbjp;)V
    .locals 5
    .annotation runtime Lbiz;
        a = Lbjp;
    .end annotation

    .prologue
    .line 62
    new-instance v0, Lcom/twitter/library/av/m$a;

    invoke-direct {v0}, Lcom/twitter/library/av/m$a;-><init>()V

    iget-object v1, p1, Lbjp;->b:Lcom/twitter/model/av/AVMediaPlaylist;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/model/av/AVMediaPlaylist;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    .line 63
    iget-object v1, p0, Log;->a:Lcom/twitter/library/av/playback/AVPlayer;

    const-string/jumbo v2, "close"

    const/4 v3, 0x0

    iget-object v4, p0, Log;->k:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/m$a;)V

    .line 64
    return-void
.end method

.method public processOpen(Lbju;)V
    .locals 4
    .annotation runtime Lbiz;
        a = Lbju;
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Log;->a:Lcom/twitter/library/av/playback/AVPlayer;

    const-string/jumbo v1, "open"

    const/4 v2, 0x0

    iget-object v3, p0, Log;->k:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/av/playback/AVPlayer;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;)V

    .line 38
    return-void
.end method

.method public processPause(Lbjw;)V
    .locals 4
    .annotation runtime Lbiz;
        a = Lbjw;
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Log;->a:Lcom/twitter/library/av/playback/AVPlayer;

    const-string/jumbo v1, "pause"

    const/4 v2, 0x0

    iget-object v3, p0, Log;->k:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/av/playback/AVPlayer;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;)V

    .line 48
    return-void
.end method

.method public processPlay(Lbjx;)V
    .locals 4
    .annotation runtime Lbiz;
        a = Lbjx;
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Log;->a:Lcom/twitter/library/av/playback/AVPlayer;

    const-string/jumbo v1, "play"

    const/4 v2, 0x0

    iget-object v3, p0, Log;->k:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/av/playback/AVPlayer;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;)V

    .line 43
    return-void
.end method

.method public processReplay(Lbkd;)V
    .locals 4
    .annotation runtime Lbiz;
        a = Lbkd;
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Log;->a:Lcom/twitter/library/av/playback/AVPlayer;

    const-string/jumbo v1, "replay"

    const/4 v2, 0x0

    iget-object v3, p0, Log;->k:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/av/playback/AVPlayer;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;)V

    .line 53
    return-void
.end method

.method public processRewind(Lbkf;)V
    .locals 4
    .annotation runtime Lbiz;
        a = Lbkf;
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Log;->a:Lcom/twitter/library/av/playback/AVPlayer;

    const-string/jumbo v1, "rewind"

    const/4 v2, 0x0

    iget-object v3, p0, Log;->k:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/av/playback/AVPlayer;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;)V

    .line 58
    return-void
.end method
