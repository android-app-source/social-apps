.class public Lbdy;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/y;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lbdy;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 25
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lbdy;
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lbdy;->a:Ljava/lang/String;

    .line 29
    return-object p0
.end method

.method protected a()Lcom/twitter/library/service/d;
    .locals 4

    .prologue
    .line 45
    invoke-virtual {p0}, Lbdy;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "news"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "details"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 47
    const-string/jumbo v1, "country_code"

    iget-object v2, p0, Lbdy;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 48
    const-string/jumbo v1, "lang"

    iget-object v2, p0, Lbdy;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 49
    const-string/jumbo v1, "id"

    iget-object v2, p0, Lbdy;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 50
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V
    .locals 7

    .prologue
    .line 61
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {p3}, Lcom/twitter/library/api/y;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbsj;

    .line 63
    invoke-virtual {p0}, Lbdy;->S()Laut;

    move-result-object v5

    .line 64
    invoke-virtual {p0}, Lbdy;->R()Lcom/twitter/library/provider/t;

    move-result-object v1

    .line 65
    invoke-virtual {p0}, Lbdy;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-wide v2, v0, Lcom/twitter/library/service/v;->c:J

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/t;->a(JLbsj;Laut;Z)V

    .line 66
    invoke-virtual {v5}, Laut;->a()V

    .line 68
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 18
    check-cast p3, Lcom/twitter/library/api/y;

    invoke-virtual {p0, p1, p2, p3}, Lbdy;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V

    return-void
.end method

.method public b(Ljava/lang/String;)Lbdy;
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lbdy;->b:Ljava/lang/String;

    .line 34
    return-object p0
.end method

.method protected b()Lcom/twitter/library/api/y;
    .locals 1

    .prologue
    .line 55
    const/16 v0, 0x65

    invoke-static {v0}, Lcom/twitter/library/api/y;->a(I)Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lbdy;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lbdy;->c:Ljava/lang/String;

    .line 39
    return-object p0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lbdy;->b()Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method
