.class public Lafx;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbwb;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lafx$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbwb",
        "<",
        "Lcom/mopub/nativeads/NativeAd;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lbwk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbwk",
            "<",
            "Lcom/mopub/nativeads/NativeAd;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lbvz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbvz",
            "<",
            "Lcom/mopub/nativeads/NativeAd;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lbwa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbwa",
            "<",
            "Lcom/mopub/nativeads/NativeAd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lbwk;Lbvz;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbwk",
            "<",
            "Lcom/mopub/nativeads/NativeAd;",
            ">;",
            "Lbvz",
            "<",
            "Lcom/mopub/nativeads/NativeAd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lafx;->a:Lbwk;

    .line 29
    iput-object p2, p0, Lafx;->b:Lbvz;

    .line 30
    return-void
.end method

.method public static a(Landroid/content/Context;)Lafx;
    .locals 3

    .prologue
    .line 102
    .line 103
    invoke-static {}, Lafy;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lagb;->a(Landroid/content/Context;Ljava/lang/String;)Lagb;

    move-result-object v0

    .line 104
    new-instance v1, Lafx;

    invoke-static {}, Lbwd;->c()Lbwd;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lafx;-><init>(Lbwk;Lbvz;)V

    return-object v1
.end method

.method static synthetic a(Lafx;)Lbvz;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lafx;->b:Lbvz;

    return-object v0
.end method

.method static synthetic a(Lafx;Lcom/mopub/nativeads/NativeAd;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lafx;->a(Lcom/mopub/nativeads/NativeAd;)V

    return-void
.end method

.method static synthetic a(Lafx;Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lafx;->a(Ljava/lang/Exception;)V

    return-void
.end method

.method private a(Lcom/mopub/nativeads/NativeAd;)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lafx;->c:Lbwa;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lafx;->c:Lbwa;

    invoke-interface {v0, p1}, Lbwa;->a(Ljava/lang/Object;)V

    .line 78
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lafx;->c:Lbwa;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lafx;->c:Lbwa;

    invoke-interface {v0, p1}, Lbwa;->a(Ljava/lang/Exception;)V

    .line 84
    :cond_0
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lafx;->a:Lbwk;

    invoke-interface {v0}, Lbwk;->a()Lrx/c;

    move-result-object v0

    new-instance v1, Lafx$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lafx$a;-><init>(Lafx;Lafx$1;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 72
    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 49
    iget-object v1, p0, Lafx;->b:Lbvz;

    invoke-virtual {v1}, Lbvz;->a()I

    move-result v1

    .line 50
    if-gt p1, v1, :cond_0

    .line 61
    :goto_0
    return v0

    .line 54
    :cond_0
    iget-object v2, p0, Lafx;->b:Lbvz;

    .line 55
    invoke-virtual {v2}, Lbvz;->b()I

    move-result v2

    sub-int v1, v2, v1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 57
    :goto_1
    if-ge v0, v1, :cond_1

    .line 58
    invoke-direct {p0}, Lafx;->b()V

    .line 57
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 61
    goto :goto_0
.end method

.method public a(Lbwa;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbwa",
            "<",
            "Lcom/mopub/nativeads/NativeAd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    iput-object p1, p0, Lafx;->c:Lbwa;

    .line 35
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lafx;->b:Lbvz;

    invoke-virtual {v0}, Lbvz;->a()I

    move-result v0

    iget-object v1, p0, Lafx;->b:Lbvz;

    invoke-virtual {v1}, Lbvz;->b()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 41
    invoke-direct {p0}, Lafx;->b()V

    .line 42
    const/4 v0, 0x1

    .line 44
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lafx;->a:Lbwk;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lafx;->c:Lbwa;

    .line 68
    return-void
.end method
