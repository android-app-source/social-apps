.class public Lbvv;
.super Laum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laum",
        "<",
        "Lbvd$a;",
        "Lcom/twitter/util/collection/Pair",
        "<",
        "Lbei;",
        "Lcom/twitter/library/service/u;",
        ">;",
        "Lbei;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/v;

.field private final c:Lcom/twitter/util/android/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/v;Lcom/twitter/util/android/a;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Laum;-><init>()V

    .line 34
    iput-object p1, p0, Lbvv;->a:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lbvv;->b:Lcom/twitter/library/client/v;

    .line 36
    iput-object p3, p0, Lbvv;->c:Lcom/twitter/util/android/a;

    .line 37
    return-void
.end method


# virtual methods
.method protected a(Lbvd$a;)Lbei;
    .locals 6

    .prologue
    .line 48
    new-instance v0, Lbei;

    iget-object v1, p0, Lbvv;->a:Landroid/content/Context;

    iget-object v3, p0, Lbvv;->b:Lcom/twitter/library/client/v;

    .line 49
    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbvd$a;

    iget-wide v4, v2, Lbvd$a;->a:J

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-object v3, p0, Lbvv;->c:Lcom/twitter/util/android/a;

    invoke-virtual {v3}, Lcom/twitter/util/android/a;->a()Ljava/lang/String;

    move-result-object v3

    iget v4, p1, Lbvd$a;->b:I

    iget-object v5, p1, Lbvd$a;->c:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lbei;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V

    .line 48
    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Lcom/twitter/library/service/s;
    .locals 1

    .prologue
    .line 24
    check-cast p1, Lbvd$a;

    invoke-virtual {p0, p1}, Lbvv;->a(Lbvd$a;)Lbei;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lbei;)Lcom/twitter/util/collection/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbei;",
            ")",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Lbei;",
            "Lcom/twitter/library/service/u;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    invoke-virtual {p1}, Lbei;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/library/service/s;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    check-cast p1, Lbei;

    invoke-virtual {p0, p1}, Lbvv;->a(Lbei;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    return-object v0
.end method
