.class public Lbqn;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lbqk;
.implements Lbqv$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbqn$a;,
        Lbqn$b;
    }
.end annotation


# static fields
.field private static b:Lbqn;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lbqk;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/os/Handler;

.field private volatile f:Landroid/location/Location;

.field private final g:Landroid/location/LocationManager;

.field private final h:Lbqv;

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:J

.field private n:J

.field private o:J

.field private p:J

.field private q:J

.field private r:Z


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 6
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string/jumbo v3, "GeoModule must be initialized for construction of LocationProducer"

    invoke-static {v0, v3}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 142
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbqn;->c:Landroid/content/Context;

    .line 144
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbqn;->d:Ljava/util/Set;

    .line 145
    new-instance v0, Lbqn$b;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Lbqn$b;-><init>(Lbqn;Landroid/os/Looper;)V

    iput-object v0, p0, Lbqn;->e:Landroid/os/Handler;

    .line 146
    iget-object v0, p0, Lbqn;->c:Landroid/content/Context;

    const-string/jumbo v3, "location"

    .line 147
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lbqn;->g:Landroid/location/LocationManager;

    .line 149
    iput-boolean v2, p0, Lbqn;->i:Z

    .line 151
    iput-boolean v2, p0, Lbqn;->j:Z

    .line 152
    iput-boolean v2, p0, Lbqn;->k:Z

    .line 153
    iput-boolean v2, p0, Lbqn;->l:Z

    .line 154
    const-wide/16 v4, 0x2710

    iput-wide v4, p0, Lbqn;->m:J

    .line 155
    const-wide/32 v4, 0x493e0

    iput-wide v4, p0, Lbqn;->n:J

    .line 156
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v4

    iput-wide v4, p0, Lbqn;->q:J

    .line 158
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "geo_location_producer_file"

    .line 159
    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v3, "geo_location_producer_mock_enabled"

    .line 160
    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lbqn;->r:Z

    .line 162
    invoke-static {}, Lcom/twitter/library/geo/provider/param/a;->a()Lcom/twitter/library/geo/provider/param/a$a;

    move-result-object v0

    const-wide/16 v2, 0x7d0

    .line 163
    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/geo/provider/param/a$a;->a(J)Lcom/twitter/library/geo/provider/param/a$a;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    .line 164
    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/geo/provider/param/a$a;->b(J)Lcom/twitter/library/geo/provider/param/a$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/geo/provider/param/LocationPriority;->a:Lcom/twitter/library/geo/provider/param/LocationPriority;

    .line 165
    invoke-virtual {v0, v1}, Lcom/twitter/library/geo/provider/param/a$a;->a(Lcom/twitter/library/geo/provider/param/LocationPriority;)Lcom/twitter/library/geo/provider/param/a$a;

    move-result-object v0

    const/16 v1, 0xa

    .line 166
    invoke-virtual {v0, v1}, Lcom/twitter/library/geo/provider/param/a$a;->a(I)Lcom/twitter/library/geo/provider/param/a$a;

    move-result-object v0

    .line 167
    invoke-virtual {v0}, Lcom/twitter/library/geo/provider/param/a$a;->a()Lcom/twitter/library/geo/provider/param/a;

    move-result-object v0

    .line 168
    new-instance v1, Lbqv;

    iget-object v2, p0, Lbqn;->c:Landroid/content/Context;

    iget-boolean v3, p0, Lbqn;->r:Z

    invoke-direct {v1, v2, v0, p0, v3}, Lbqv;-><init>(Landroid/content/Context;Lcom/twitter/library/geo/provider/param/a;Lbql;Z)V

    iput-object v1, p0, Lbqn;->h:Lbqv;

    .line 169
    iget-object v0, p0, Lbqn;->h:Lbqv;

    invoke-virtual {v0, p0}, Lbqv;->a(Lbqv$a;)V

    .line 172
    new-instance v0, Lbqn$1;

    invoke-direct {v0, p0}, Lbqn$1;-><init>(Lbqn;)V

    invoke-static {v0}, Lcoj;->a(Lcoj$a;)V

    .line 209
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 210
    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 211
    return-void

    :cond_0
    move v0, v2

    .line 139
    goto/16 :goto_0

    :cond_1
    move v1, v2

    .line 160
    goto :goto_1
.end method

.method static synthetic a(Lbqn;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lbqn;->e:Landroid/os/Handler;

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lbqn;
    .locals 2

    .prologue
    .line 227
    const-class v1, Lbqn;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lbqn;->b:Lbqn;

    if-nez v0, :cond_0

    .line 228
    new-instance v0, Lbqn;

    invoke-direct {v0, p0}, Lbqn;-><init>(Landroid/content/Context;)V

    sput-object v0, Lbqn;->b:Lbqn;

    .line 229
    const-class v0, Lbqn;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 231
    :cond_0
    sget-object v0, Lbqn;->b:Lbqn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(ZII)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const-wide/16 v4, 0x3e8

    .line 699
    if-eq p2, v6, :cond_0

    .line 700
    int-to-long v0, p2

    mul-long/2addr v0, v4

    iput-wide v0, p0, Lbqn;->m:J

    .line 701
    int-to-long v0, p2

    mul-long/2addr v0, v4

    const-wide/16 v2, 0x1388

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 704
    const-wide/16 v0, 0x2710

    iput-wide v0, p0, Lbqn;->m:J

    .line 708
    :cond_0
    if-eq p3, v6, :cond_1

    .line 709
    int-to-long v0, p3

    mul-long/2addr v0, v4

    iput-wide v0, p0, Lbqn;->n:J

    .line 710
    int-to-long v0, p3

    mul-long/2addr v0, v4

    const-wide/16 v2, 0x7530

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 711
    const-wide/32 v0, 0x493e0

    iput-wide v0, p0, Lbqn;->n:J

    .line 715
    :cond_1
    iget-boolean v0, p0, Lbqn;->i:Z

    if-eq v0, p1, :cond_2

    .line 716
    iput-boolean p1, p0, Lbqn;->i:Z

    .line 717
    invoke-direct {p0}, Lbqn;->l()V

    .line 719
    :cond_2
    return-void
.end method

.method private c(Landroid/location/Location;)V
    .locals 1

    .prologue
    .line 684
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v0

    invoke-virtual {v0}, Lbqg;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbqn;->f:Landroid/location/Location;

    .line 685
    invoke-virtual {p0, p1, v0}, Lbqn;->a(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 686
    iput-object p1, p0, Lbqn;->f:Landroid/location/Location;

    .line 688
    :cond_0
    return-void
.end method

.method private declared-synchronized j()V
    .locals 4

    .prologue
    .line 639
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbqn;->e:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 640
    iget-object v0, p0, Lbqn;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqk;

    .line 641
    iget-object v2, p0, Lbqn;->f:Landroid/location/Location;

    invoke-interface {v0, v2}, Lbqk;->b(Landroid/location/Location;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 639
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 643
    :cond_0
    :try_start_1
    iget-object v0, p0, Lbqn;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 644
    invoke-virtual {p0}, Lbqn;->f()V

    .line 645
    iget-boolean v0, p0, Lbqn;->j:Z

    if-eqz v0, :cond_1

    .line 646
    invoke-direct {p0}, Lbqn;->k()J

    move-result-wide v0

    .line 647
    iget-object v2, p0, Lbqn;->e:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 649
    :cond_1
    monitor-exit p0

    return-void
.end method

.method private k()J
    .locals 8

    .prologue
    .line 657
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    .line 658
    iget-wide v2, p0, Lbqn;->p:J

    sub-long v2, v0, v2

    .line 660
    iget-wide v4, p0, Lbqn;->n:J

    cmp-long v4, v2, v4

    if-ltz v4, :cond_0

    .line 661
    const-wide/16 v0, 0x0

    .line 673
    :goto_0
    return-wide v0

    .line 663
    :cond_0
    iget-wide v4, p0, Lbqn;->q:J

    .line 664
    iget-wide v6, p0, Lbqn;->o:J

    cmp-long v6, v4, v6

    if-ltz v6, :cond_1

    iget-wide v6, p0, Lbqn;->p:J

    cmp-long v6, v4, v6

    if-gtz v6, :cond_1

    .line 668
    iget-wide v0, p0, Lbqn;->n:J

    sub-long/2addr v0, v2

    goto :goto_0

    .line 672
    :cond_1
    sub-long/2addr v0, v4

    .line 673
    iget-wide v2, p0, Lbqn;->n:J

    iget-wide v4, p0, Lbqn;->n:J

    rem-long/2addr v0, v4

    sub-long v0, v2, v0

    goto :goto_0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 727
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v0

    invoke-virtual {v0}, Lbqg;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lbqn;->i:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 729
    :goto_0
    iget-boolean v1, p0, Lbqn;->j:Z

    if-eq v0, v1, :cond_0

    .line 730
    iput-boolean v0, p0, Lbqn;->j:Z

    .line 731
    if-nez v0, :cond_2

    .line 732
    invoke-direct {p0}, Lbqn;->m()V

    .line 733
    invoke-virtual {p0}, Lbqn;->g()V

    .line 738
    :cond_0
    :goto_1
    return-void

    .line 727
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 734
    :cond_2
    iget-boolean v0, p0, Lbqn;->l:Z

    if-eqz v0, :cond_0

    .line 735
    invoke-virtual {p0}, Lbqn;->e()V

    goto :goto_1
.end method

.method private m()V
    .locals 2

    .prologue
    .line 746
    invoke-direct {p0}, Lbqn;->j()V

    .line 747
    iget-object v0, p0, Lbqn;->e:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 748
    iget-object v0, p0, Lbqn;->e:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 749
    return-void
.end method


# virtual methods
.method public a()Landroid/location/Location;
    .locals 1

    .prologue
    .line 255
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v0

    invoke-virtual {v0}, Lbqg;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbqn;->h:Lbqv;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lbqn;->h:Lbqv;

    invoke-virtual {v0}, Lbqv;->f()Landroid/location/Location;

    move-result-object v0

    invoke-direct {p0, v0}, Lbqn;->c(Landroid/location/Location;)V

    .line 258
    iget-object v0, p0, Lbqn;->f:Landroid/location/Location;

    .line 260
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected declared-synchronized a(JLbqk;)V
    .locals 3

    .prologue
    .line 555
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lbqn;->k:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lbqn;->h:Lbqv;

    if-eqz v0, :cond_0

    .line 556
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lbqn;->o:J

    .line 557
    iget-object v0, p0, Lbqn;->h:Lbqv;

    invoke-virtual {v0}, Lbqv;->g()V

    .line 558
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbqn;->k:Z

    .line 561
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_1

    .line 562
    if-nez p3, :cond_2

    .line 563
    iget-object v0, p0, Lbqn;->e:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 571
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 565
    :cond_2
    :try_start_1
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 566
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 567
    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 568
    iget-object v1, p0, Lbqn;->e:Landroid/os/Handler;

    invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 555
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Landroid/location/Location;)V
    .locals 5

    .prologue
    .line 296
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lbqn;->c(Landroid/location/Location;)V

    .line 297
    iget-object v0, p0, Lbqn;->f:Landroid/location/Location;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 316
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 300
    :cond_1
    :try_start_1
    iget-object v0, p0, Lbqn;->f:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lbqn;->q:J

    .line 304
    iget-object v0, p0, Lbqn;->d:Ljava/util/Set;

    iget-object v1, p0, Lbqn;->d:Ljava/util/Set;

    .line 305
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Lbqk;

    .line 304
    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbqk;

    .line 306
    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_3

    aget-object v3, v0, v1

    .line 307
    if-eqz v3, :cond_2

    iget-object v4, p0, Lbqn;->d:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 308
    invoke-interface {v3, p1}, Lbqk;->a(Landroid/location/Location;)V

    .line 306
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 313
    :cond_3
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v0}, Lbqn;->a(Landroid/location/Location;F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    invoke-direct {p0}, Lbqn;->j()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 296
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 579
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 598
    :goto_0
    return-void

    .line 581
    :pswitch_0
    iget-wide v0, p0, Lbqn;->m:J

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lbqn;->a(JLbqk;)V

    goto :goto_0

    .line 585
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lbqk;

    invoke-virtual {p0, v0}, Lbqn;->b(Lbqk;)V

    goto :goto_0

    .line 591
    :pswitch_2
    invoke-direct {p0}, Lbqn;->m()V

    goto :goto_0

    .line 579
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lbqk;)V
    .locals 2

    .prologue
    .line 368
    const-wide/16 v0, 0x2710

    invoke-virtual {p0, p1, v0, v1}, Lbqn;->a(Lbqk;J)V

    .line 369
    return-void
.end method

.method public declared-synchronized a(Lbqk;J)V
    .locals 6

    .prologue
    .line 385
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbqn;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 406
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 389
    :cond_1
    :try_start_1
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v0

    invoke-virtual {v0}, Lbqg;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    invoke-virtual {p0}, Lbqn;->a()Landroid/location/Location;

    move-result-object v0

    .line 392
    if-eqz v0, :cond_2

    .line 393
    iget-object v1, p0, Lbqn;->e:Landroid/os/Handler;

    new-instance v2, Lbqn$a;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, p1, v0, v3, v4}, Lbqn$a;-><init>(Lbqk;Landroid/location/Location;ILbqn$1;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 397
    :cond_2
    const-wide/16 v2, 0x7530

    invoke-virtual {p0, v0, v2, v3}, Lbqn;->a(Landroid/location/Location;J)Z

    move-result v1

    if-nez v1, :cond_3

    .line 398
    iget-object v0, p0, Lbqn;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 399
    iget-object v0, p0, Lbqn;->e:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 400
    invoke-virtual {p0, p2, p3, p1}, Lbqn;->a(JLbqk;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 385
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 402
    :cond_3
    :try_start_2
    iget-object v1, p0, Lbqn;->e:Landroid/os/Handler;

    new-instance v2, Lbqn$a;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v2, p1, v0, v3, v4}, Lbqn$a;-><init>(Lbqk;Landroid/location/Location;ILbqn$1;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method declared-synchronized a(Z)V
    .locals 3

    .prologue
    .line 457
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lbqn;->r:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p1, v0, :cond_0

    .line 469
    :goto_0
    monitor-exit p0

    return-void

    .line 461
    :cond_0
    :try_start_1
    iput-boolean p1, p0, Lbqn;->r:Z

    .line 462
    iget-object v0, p0, Lbqn;->c:Landroid/content/Context;

    const-string/jumbo v1, "geo_location_producer_file"

    const/4 v2, 0x0

    .line 463
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 464
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "geo_location_producer_mock_enabled"

    .line 465
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 466
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 467
    invoke-virtual {p0}, Lbqn;->g()V

    .line 468
    iget-object v0, p0, Lbqn;->h:Lbqv;

    iget-boolean v1, p0, Lbqn;->r:Z

    invoke-virtual {v0, v1}, Lbqv;->a(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 457
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(ZZII)V
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lbqn;->h:Lbqv;

    invoke-virtual {v0, p2}, Lbqv;->b(Z)V

    .line 444
    invoke-direct {p0, p1, p3, p4}, Lbqn;->a(ZII)V

    .line 446
    return-void
.end method

.method protected a(Landroid/location/Location;F)Z
    .locals 1

    .prologue
    .line 533
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 534
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    cmpg-float v0, v0, p2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    .line 533
    :goto_0
    return v0

    .line 534
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/location/Location;J)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 517
    if-nez p1, :cond_1

    .line 522
    :cond_0
    :goto_0
    return v0

    .line 521
    :cond_1
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 522
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    cmp-long v1, v2, p2

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected a(Landroid/location/Location;Landroid/location/Location;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 480
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 506
    :cond_1
    :goto_0
    return v0

    .line 482
    :cond_2
    if-eqz p2, :cond_1

    .line 487
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-virtual {p2}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    sub-long v4, v2, v4

    .line 488
    const-wide/16 v2, 0x7530

    cmp-long v2, v4, v2

    if-lez v2, :cond_3

    move v3, v0

    .line 489
    :goto_1
    const-wide/16 v6, -0x7530

    cmp-long v2, v4, v6

    if-gez v2, :cond_4

    move v2, v0

    .line 492
    :goto_2
    if-nez v3, :cond_1

    .line 494
    if-eqz v2, :cond_5

    move v0, v1

    .line 495
    goto :goto_0

    :cond_3
    move v3, v1

    .line 488
    goto :goto_1

    :cond_4
    move v2, v1

    .line 489
    goto :goto_2

    .line 500
    :cond_5
    const-wide/16 v2, 0x0

    cmp-long v2, v4, v2

    if-ltz v2, :cond_6

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v2}, Lbqn;->a(Landroid/location/Location;F)Z

    move-result v2

    if-nez v2, :cond_1

    .line 506
    :cond_6
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    invoke-virtual {p2}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    cmpg-float v2, v2, v3

    if-lez v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 271
    invoke-virtual {p0}, Lbqn;->a()Landroid/location/Location;

    move-result-object v0

    invoke-static {v0}, Lbqm;->a(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 320
    return-void
.end method

.method public declared-synchronized b(Lbqk;)V
    .locals 5

    .prologue
    .line 415
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 416
    :try_start_0
    iget-object v0, p0, Lbqn;->e:Landroid/os/Handler;

    new-instance v1, Lbqn$a;

    iget-object v2, p0, Lbqn;->f:Landroid/location/Location;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v1, p1, v2, v3, v4}, Lbqn$a;-><init>(Lbqk;Landroid/location/Location;ILbqn$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 418
    iget-object v0, p0, Lbqn;->e:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 419
    iget-object v0, p0, Lbqn;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 422
    :cond_0
    iget-object v0, p0, Lbqn;->e:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 423
    invoke-virtual {p0}, Lbqn;->f()V

    .line 424
    iget-boolean v0, p0, Lbqn;->j:Z

    if-eqz v0, :cond_1

    .line 425
    invoke-direct {p0}, Lbqn;->k()J

    move-result-wide v0

    .line 426
    iget-object v2, p0, Lbqn;->e:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 429
    :cond_1
    monitor-exit p0

    return-void

    .line 415
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lbqn;->g:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 4

    .prologue
    .line 330
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbqn;->l:Z

    .line 331
    iget-boolean v0, p0, Lbqn;->j:Z

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lbqn;->e:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 335
    :cond_0
    return-void
.end method

.method public e()V
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/4 v4, 0x0

    .line 343
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbqn;->l:Z

    .line 346
    iget-object v0, p0, Lbqn;->e:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lbqn;->e:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 350
    :cond_0
    iget-boolean v0, p0, Lbqn;->j:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lbqn;->k:Z

    if-nez v0, :cond_1

    .line 351
    invoke-direct {p0}, Lbqn;->k()J

    move-result-wide v0

    .line 352
    const-wide/16 v2, 0x3e8

    cmp-long v2, v0, v2

    if-gez v2, :cond_2

    .line 354
    iget-wide v0, p0, Lbqn;->m:J

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lbqn;->a(JLbqk;)V

    .line 359
    :cond_1
    :goto_0
    return-void

    .line 355
    :cond_2
    iget-object v2, p0, Lbqn;->e:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 356
    iget-object v2, p0, Lbqn;->e:Landroid/os/Handler;

    invoke-virtual {v2, v4, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method protected declared-synchronized f()V
    .locals 2

    .prologue
    .line 541
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lbqn;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbqn;->h:Lbqv;

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lbqn;->h:Lbqv;

    invoke-virtual {v0}, Lbqv;->h()V

    .line 543
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lbqn;->p:J

    .line 544
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbqn;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 546
    :cond_0
    monitor-exit p0

    return-void

    .line 541
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized g()V
    .locals 2

    .prologue
    .line 606
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lbqn;->f:Landroid/location/Location;

    .line 607
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lbqn;->q:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 608
    monitor-exit p0

    return-void

    .line 606
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public h()V
    .locals 0

    .prologue
    .line 623
    invoke-direct {p0}, Lbqn;->m()V

    .line 624
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 628
    iget-boolean v0, p0, Lbqn;->l:Z

    if-eqz v0, :cond_0

    .line 629
    invoke-virtual {p0}, Lbqn;->e()V

    .line 631
    :cond_0
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 215
    const-string/jumbo v0, "location"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    invoke-direct {p0}, Lbqn;->l()V

    .line 218
    :cond_0
    return-void
.end method
