.class public Lcbc;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcbc$a;
    }
.end annotation


# instance fields
.field public final a:J

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcba;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:I


# direct methods
.method constructor <init>(Lcbc$a;)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iget-wide v0, p1, Lcbc$a;->a:J

    iput-wide v0, p0, Lcbc;->a:J

    .line 34
    iget-object v0, p1, Lcbc$a;->b:Ljava/util/List;

    iput-object v0, p0, Lcbc;->b:Ljava/util/List;

    .line 35
    iget-object v0, p1, Lcbc$a;->c:Ljava/lang/String;

    iput-object v0, p0, Lcbc;->c:Ljava/lang/String;

    .line 36
    iget v0, p1, Lcbc$a;->d:I

    iput v0, p0, Lcbc;->d:I

    .line 37
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcbc;->b:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcba;

    invoke-virtual {v0}, Lcba;->b()I

    move-result v0

    .line 42
    packed-switch v0, :pswitch_data_0

    .line 48
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 44
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 46
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 42
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
