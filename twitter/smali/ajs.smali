.class public Lajs;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Item:",
        "Ljava/lang/Object;",
        "TypedItemController::",
        "Lajq",
        "<TItem;>;>",
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Laju",
        "<TTypedItemController;>;>;"
    }
.end annotation


# instance fields
.field private final a:Lajv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajv",
            "<TItem;>;"
        }
    .end annotation
.end field

.field private final b:Lajr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajr",
            "<TItem;TTypedItemController;>;"
        }
    .end annotation
.end field

.field private final c:Lcjs;


# direct methods
.method public constructor <init>(Lajv;Lajr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lajv",
            "<TItem;>;",
            "Lajr",
            "<TItem;TTypedItemController;>;)V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 17
    new-instance v0, Lcjx;

    invoke-direct {v0, p0}, Lcjx;-><init>(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    iput-object v0, p0, Lajs;->c:Lcjs;

    .line 21
    iput-object p1, p0, Lajs;->a:Lajv;

    .line 22
    iput-object p2, p0, Lajs;->b:Lajr;

    .line 23
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Laju;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Laju",
            "<TTypedItemController;>;"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v0, Laju;

    iget-object v1, p0, Lajs;->b:Lajr;

    iget-object v2, p0, Lajs;->c:Lcjs;

    invoke-interface {v1, p1, v2, p2}, Lajr;->b(Landroid/view/ViewGroup;Lcjs;I)Lajq;

    move-result-object v1

    invoke-direct {v0, v1}, Laju;-><init>(Laoe;)V

    return-object v0
.end method

.method public a(Laju;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laju",
            "<TTypedItemController;>;)V"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p1, Laju;->a:Laoe;

    check-cast v0, Lajq;

    .line 50
    instance-of v1, v0, Lajn;

    if-eqz v1, :cond_0

    .line 51
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajn;

    .line 52
    invoke-interface {v0}, Lajn;->b()V

    .line 54
    :cond_0
    return-void
.end method

.method public a(Laju;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laju",
            "<TTypedItemController;>;I)V"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p1, Laju;->a:Laoe;

    check-cast v0, Lajq;

    iget-object v1, p0, Lajs;->a:Lajv;

    invoke-interface {v1, p2}, Lajv;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lajq;->a(Ljava/lang/Object;)V

    .line 45
    return-void
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lajs;->a:Lajv;

    invoke-interface {v0}, Lajv;->b()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lajs;->a:Lajv;

    invoke-interface {v0, p1}, Lajv;->b(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lajs;->b:Lajr;

    iget-object v1, p0, Lajs;->a:Lajv;

    invoke-interface {v1, p1}, Lajv;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lajr;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Laju;

    invoke-virtual {p0, p1, p2}, Lajs;->a(Laju;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0, p1, p2}, Lajs;->a(Landroid/view/ViewGroup;I)Laju;

    move-result-object v0

    return-object v0
.end method

.method public synthetic onViewRecycled(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Laju;

    invoke-virtual {p0, p1}, Lajs;->a(Laju;)V

    return-void
.end method
