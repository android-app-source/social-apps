.class public Laki;
.super Lcom/twitter/library/widget/renderablecontent/a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/widget/renderablecontent/a",
        "<",
        "Lcom/twitter/model/core/Tweet;",
        "Lcom/twitter/library/widget/renderablecontent/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/graphics/Rect;

.field private final b:Lcom/twitter/model/core/Tweet;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct/range {p0 .. p5}, Lcom/twitter/library/widget/renderablecontent/a;-><init>(Landroid/app/Activity;Ljava/lang/Object;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 20
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Laki;->a:Landroid/graphics/Rect;

    .line 28
    iput-object p2, p0, Laki;->b:Lcom/twitter/model/core/Tweet;

    .line 29
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;IIII)Landroid/graphics/Rect;
    .locals 3

    .prologue
    .line 52
    iget-object v0, p0, Laki;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 53
    invoke-virtual {p0}, Laki;->d()Landroid/view/View;

    move-result-object v0

    .line 54
    if-eqz v0, :cond_0

    .line 55
    const/high16 v1, -0x80000000

    invoke-static {p4, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 56
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 57
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, p2

    .line 58
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, p3

    .line 60
    iget-object v2, p0, Laki;->a:Landroid/graphics/Rect;

    invoke-virtual {v2, p2, p3, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 62
    :cond_0
    iget-object v0, p0, Laki;->a:Landroid/graphics/Rect;

    return-object v0
.end method

.method protected a(Landroid/app/Activity;)Lcom/twitter/library/widget/renderablecontent/c;
    .locals 4

    .prologue
    .line 40
    new-instance v0, Lakh;

    iget-object v1, p0, Laki;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {p0}, Laki;->b()Lakh$b;

    move-result-object v2

    iget-object v3, p0, Laki;->i:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0, p1, v1, v2, v3}, Lakh;-><init>(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;Lakh$b;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    return-object v0
.end method

.method protected a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lakh$a;

    invoke-direct {v0}, Lakh$a;-><init>()V

    return-object v0
.end method

.method b()Lakh$b;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 45
    invoke-static {}, Lcom/twitter/android/revenue/k;->i()Z

    move-result v0

    .line 47
    new-instance v1, Lakh$b;

    invoke-direct {v1, v0, v2, v2}, Lakh$b;-><init>(ZZZ)V

    return-object v1
.end method
