.class public Lccp;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lccp$b;,
        Lccp$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Lccp;",
            "Lccp$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lccp$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lccp$b;-><init>(Lccp$1;)V

    sput-object v0, Lccp;->a:Lcom/twitter/util/serialization/b;

    return-void
.end method

.method private constructor <init>(Lccp$a;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {p1}, Lccp$a;->a(Lccp$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lccp;->b:Ljava/lang/String;

    .line 29
    invoke-static {p1}, Lccp$a;->b(Lccp$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lccp;->c:Ljava/lang/String;

    .line 30
    invoke-static {p1}, Lccp$a;->c(Lccp$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lccp;->d:Ljava/lang/String;

    .line 31
    return-void
.end method

.method synthetic constructor <init>(Lccp$a;Lccp$1;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lccp;-><init>(Lccp$a;)V

    return-void
.end method

.method private a(Lccp;)Z
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lccp;->b:Ljava/lang/String;

    iget-object v1, p1, Lccp;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lccp;->c:Ljava/lang/String;

    iget-object v1, p1, Lccp;->c:Ljava/lang/String;

    .line 46
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lccp;->d:Ljava/lang/String;

    iget-object v1, p1, Lccp;->d:Ljava/lang/String;

    .line 47
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 45
    :goto_0
    return v0

    .line 47
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 41
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lccp;

    if-eqz v0, :cond_1

    check-cast p1, Lccp;

    invoke-direct {p0, p1}, Lccp;->a(Lccp;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lccp;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 53
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lccp;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lccp;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lccp;->c:Ljava/lang/String;

    return-object v0
.end method
