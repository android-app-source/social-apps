.class public final Latb;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Latn;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Latb$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Latt;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Latr;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lpv;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/cn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Latb;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Latb;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Latb$a;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    sget-boolean v0, Latb;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 47
    :cond_0
    invoke-direct {p0, p1}, Latb;->a(Latb$a;)V

    .line 48
    return-void
.end method

.method synthetic constructor <init>(Latb$a;Latb$1;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Latb;-><init>(Latb$a;)V

    return-void
.end method

.method public static a()Latb$a;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Latb$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Latb$a;-><init>(Latb$1;)V

    return-object v0
.end method

.method private a(Latb$a;)V
    .locals 2

    .prologue
    .line 57
    new-instance v0, Latb$1;

    invoke-direct {v0, p0, p1}, Latb$1;-><init>(Latb;Latb$a;)V

    iput-object v0, p0, Latb;->b:Lcta;

    .line 70
    iget-object v0, p0, Latb;->b:Lcta;

    .line 72
    invoke-static {v0}, Lats;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 71
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Latb;->c:Lcta;

    .line 74
    iget-object v0, p0, Latb;->c:Lcta;

    .line 75
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Latb;->d:Lcta;

    .line 77
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 78
    invoke-static {v0, v1}, Ldagger/internal/f;->a(II)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Latb;->d:Lcta;

    .line 79
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Ldagger/internal/f$a;->a()Ldagger/internal/f;

    move-result-object v0

    iput-object v0, p0, Latb;->e:Lcta;

    .line 82
    new-instance v0, Latb$2;

    invoke-direct {v0, p0, p1}, Latb$2;-><init>(Latb;Latb$a;)V

    iput-object v0, p0, Latb;->f:Lcta;

    .line 95
    iget-object v0, p0, Latb;->f:Lcta;

    .line 97
    invoke-static {v0}, Lcom/twitter/app/common/abs/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 96
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Latb;->g:Lcta;

    .line 99
    new-instance v0, Latb$3;

    invoke-direct {v0, p0, p1}, Latb$3;-><init>(Latb;Latb$a;)V

    iput-object v0, p0, Latb;->h:Lcta;

    .line 115
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Latb;->h:Lcta;

    .line 114
    invoke-static {v0, v1}, Lcom/twitter/android/co;->a(Lcsd;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 113
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Latb;->i:Lcta;

    .line 117
    return-void
.end method


# virtual methods
.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Latb;->e:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c()Lcom/twitter/app/common/abs/j;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Latb;->g:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    return-object v0
.end method

.method public d()Lcom/twitter/android/cn;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Latb;->i:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cn;

    return-object v0
.end method

.method public e()Latr;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Latb;->c:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Latr;

    return-object v0
.end method
