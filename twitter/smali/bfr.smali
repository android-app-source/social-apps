.class public Lbfr;
.super Lbfy;
.source "Twttr"


# instance fields
.field private final j:Lcom/twitter/library/network/t;

.field private final k:Lcom/twitter/util/collection/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/h",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcom/twitter/android/revenue/c;

.field private final m:Z

.field private r:Lbwb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbwb",
            "<",
            "Lcom/mopub/nativeads/NativeAd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/TwitterUser;I)V
    .locals 1

    .prologue
    .line 89
    new-instance v0, Lcom/twitter/library/service/v;

    invoke-direct {v0, p2}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    invoke-direct {p0, p1, v0, p3, p4}, Lbfr;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/model/core/TwitterUser;I)V

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/model/core/TwitterUser;I)V
    .locals 6

    .prologue
    .line 94
    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lbfr;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/model/core/TwitterUser;ZI)V

    .line 95
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/model/core/TwitterUser;ZI)V
    .locals 6

    .prologue
    .line 99
    const-class v0, Lbfr;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lbfy;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;Lcom/twitter/model/core/TwitterUser;I)V

    .line 80
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    iput-object v0, p0, Lbfr;->k:Lcom/twitter/util/collection/h;

    .line 100
    new-instance v0, Lcom/twitter/library/network/t;

    iget-object v1, p2, Lcom/twitter/library/service/v;->d:Lcom/twitter/model/account/OAuthToken;

    invoke-direct {v0, v1}, Lcom/twitter/library/network/t;-><init>(Lcom/twitter/model/account/OAuthToken;)V

    iput-object v0, p0, Lbfr;->j:Lcom/twitter/library/network/t;

    .line 101
    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Lbfr;->c(I)Lbge;

    .line 102
    if-eqz p4, :cond_0

    .line 103
    invoke-direct {p0, p1}, Lbfr;->a(Landroid/content/Context;)V

    .line 105
    :cond_0
    const-string/jumbo v0, "ad_formats_ad_slots_android_4189"

    const-string/jumbo v1, "ad_slots"

    invoke-static {v0, v1}, Lcoi;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lbfr;->m:Z

    .line 107
    const-string/jumbo v0, "HomeTimeline"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Home Timeline request created for UserID #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "ANDROID-10803"

    invoke-static {v0, v1, v2}, Lcqi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 330
    new-instance v0, Lcom/twitter/library/service/f;

    invoke-direct {v0}, Lcom/twitter/library/service/f;-><init>()V

    .line 331
    new-instance v1, Lcom/twitter/library/service/n;

    invoke-direct {v1}, Lcom/twitter/library/service/n;-><init>()V

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/service/l;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lcom/twitter/library/service/l;-><init>(I)V

    .line 332
    invoke-virtual {v1, v2}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/service/g;

    invoke-direct {v2, p1}, Lcom/twitter/library/service/g;-><init>(Landroid/content/Context;)V

    .line 333
    invoke-virtual {v1, v2}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    .line 335
    const/16 v1, 0x7530

    invoke-virtual {p0, v1}, Lbfr;->f(I)V

    .line 336
    invoke-virtual {p0, v0}, Lbfr;->a(Lcom/twitter/async/service/k;)Lcom/twitter/async/service/AsyncOperation;

    .line 337
    return-void
.end method

.method private a(Lcom/twitter/library/service/d$a;)V
    .locals 4

    .prologue
    .line 229
    iget-object v0, p0, Lbfr;->l:Lcom/twitter/android/revenue/c;

    if-eqz v0, :cond_0

    .line 230
    const-string/jumbo v0, "jit_enabled"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 231
    const-string/jumbo v0, "num_unfilled_ad_slots_available"

    iget-object v1, p0, Lbfr;->l:Lcom/twitter/android/revenue/c;

    invoke-virtual {v1}, Lcom/twitter/android/revenue/c;->a()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {p1, v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    .line 232
    iget-object v0, p0, Lbfr;->l:Lcom/twitter/android/revenue/c;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/c;->b()J

    move-result-wide v0

    .line 233
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 234
    const-string/jumbo v2, "last_ad_pool_refresh_epoch_ms"

    invoke-virtual {p1, v2, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    .line 237
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lbwb;)Lbfr;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbwb",
            "<",
            "Lcom/mopub/nativeads/NativeAd;",
            ">;)",
            "Lbfr;"
        }
    .end annotation

    .prologue
    .line 116
    iput-object p1, p0, Lbfr;->r:Lbwb;

    .line 117
    return-object p0
.end method

.method public a(Lcom/twitter/android/revenue/c;)Lbfr;
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lbfr;->l:Lcom/twitter/android/revenue/c;

    .line 112
    return-object p0
.end method

.method public a(Lcom/twitter/async/service/j;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 168
    invoke-super {p0, p1}, Lbfy;->a(Lcom/twitter/async/service/j;)V

    .line 169
    if-eqz p1, :cond_0

    .line 170
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 171
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->f()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 172
    invoke-virtual {p0}, Lbfr;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    if-nez v0, :cond_1

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    const-string/jumbo v0, "timeline_request_scribe_sample"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lbfr;->p:Landroid/content/Context;

    .line 178
    invoke-virtual {p0}, Lbfr;->e()Ljava/lang/String;

    move-result-object v1

    .line 179
    invoke-virtual {p0}, Lbfr;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/service/v;->c:J

    .line 180
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/service/u;

    invoke-virtual {v4}, Lcom/twitter/library/service/u;->b()Z

    move-result v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v5, p1

    .line 176
    invoke-static/range {v0 .. v7}, Lcom/twitter/library/api/a;->a(Landroid/content/Context;Ljava/lang/String;JZLcom/twitter/async/service/j;ZLjava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v5, 0x1

    .line 259
    invoke-virtual {p0}, Lbfr;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 262
    invoke-virtual {p0}, Lbfr;->R()Lcom/twitter/library/provider/t;

    move-result-object v1

    .line 263
    invoke-virtual {p3}, Lcom/twitter/library/api/y;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/t;

    .line 265
    iget-object v2, v0, Lcom/twitter/library/api/t;->a:Ljava/util/List;

    .line 266
    iput-object v2, p0, Lbfr;->i:Ljava/util/List;

    .line 269
    iget-object v3, p0, Lbfr;->r:Lbwb;

    if-eqz v3, :cond_0

    .line 272
    iget-object v3, p0, Lbfr;->r:Lbwb;

    invoke-static {v2, v3}, Lbwj;->a(Ljava/util/List;Lbwb;)V

    .line 275
    :cond_0
    invoke-virtual {p0, v0}, Lbfr;->a(Lcom/twitter/library/api/t;)Lbfy$a;

    move-result-object v7

    .line 276
    invoke-virtual {p0}, Lbfr;->C()J

    move-result-wide v8

    .line 277
    invoke-virtual {p0}, Lbfr;->S()Laut;

    move-result-object v6

    .line 279
    iget-object v2, p0, Lbfr;->l:Lcom/twitter/android/revenue/c;

    if-eqz v2, :cond_5

    iget-boolean v2, p0, Lbfr;->m:Z

    if-eqz v2, :cond_5

    .line 280
    iget-object v2, p0, Lbfr;->l:Lcom/twitter/android/revenue/c;

    iget-object v3, v0, Lcom/twitter/library/api/t;->c:Ljava/util/List;

    invoke-virtual {p0}, Lbfr;->S()Laut;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/android/revenue/c;Ljava/util/List;Laut;)V

    .line 282
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1, v8, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v5, [Ljava/lang/String;

    const-string/jumbo v3, "home::stream:ads:received"

    aput-object v3, v2, v10

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 283
    iget-object v0, v0, Lcom/twitter/library/api/t;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/revenue/a;

    .line 284
    invoke-static {v0}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/model/revenue/a;)Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    .line 285
    if-eqz v0, :cond_1

    .line 286
    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_0

    .line 289
    :cond_2
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 291
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v8, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "home::stream:slots:received"

    aput-object v2, v1, v10

    .line 292
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 293
    iget-object v1, p0, Lbfr;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/timeline/y;

    .line 294
    instance-of v3, v1, Lcom/twitter/model/timeline/t;

    if-eqz v3, :cond_3

    .line 295
    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/timeline/t;

    .line 296
    invoke-static {v1}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/model/timeline/t;)Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_1

    .line 299
    :cond_4
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 302
    :cond_5
    iget-boolean v0, v7, Lbfy$a;->d:Z

    if-eqz v0, :cond_6

    .line 305
    invoke-static {}, Lcom/twitter/library/provider/j;->c()Lcom/twitter/library/provider/j;

    move-result-object v1

    .line 306
    iget-wide v2, p0, Lbfr;->b:J

    const-string/jumbo v0, "tweet"

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/library/provider/j;->a(JLjava/lang/String;)I

    move-result v0

    .line 307
    if-nez v0, :cond_6

    .line 308
    iget-wide v2, p0, Lbfr;->b:J

    const-string/jumbo v4, "tweet"

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/j;->a(JLjava/lang/String;ILaut;)I

    .line 309
    invoke-virtual {v6}, Laut;->a()V

    .line 313
    :cond_6
    invoke-virtual {p0, v7}, Lbfr;->a(Lbfy$a;)V

    .line 315
    invoke-virtual {p0}, Lbfr;->N()Z

    move-result v0

    if-nez v0, :cond_7

    iget v0, v7, Lbfy$a;->b:I

    if-lez v0, :cond_7

    .line 316
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v8, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "home::::tlv_proxy"

    aput-object v2, v1, v10

    .line 317
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 316
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 321
    :cond_7
    iget-object v0, p2, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "scribe_item_count"

    iget v2, v7, Lbfy$a;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 323
    invoke-virtual {p0}, Lbfr;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    .line 324
    const-string/jumbo v2, "HomeTimeline"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Home Timeline request complete for User ID "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v0, :cond_9

    iget-wide v0, v0, Lcom/twitter/library/service/v;->c:J

    :goto_2
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "ANDROID-10803"

    invoke-static {v2, v0, v1}, Lcqi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    :cond_8
    return-void

    .line 324
    :cond_9
    const-wide/16 v0, 0x0

    goto :goto_2
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 58
    check-cast p3, Lcom/twitter/library/api/y;

    invoke-virtual {p0, p1, p2, p3}, Lbfr;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V

    return-void
.end method

.method public b(Lcom/twitter/async/service/j;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 147
    invoke-super {p0, p1}, Lbfy;->b(Lcom/twitter/async/service/j;)V

    .line 148
    if-eqz p1, :cond_0

    .line 149
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 150
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->f()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 151
    invoke-virtual {p0}, Lbfr;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    if-nez v0, :cond_1

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    const-string/jumbo v0, "timeline_request_scribe_sample"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lbfr;->p:Landroid/content/Context;

    .line 157
    invoke-virtual {p0}, Lbfr;->e()Ljava/lang/String;

    move-result-object v1

    .line 158
    invoke-virtual {p0}, Lbfr;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/service/v;->c:J

    .line 159
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/service/u;

    invoke-virtual {v4}, Lcom/twitter/library/service/u;->b()Z

    move-result v4

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v5, p1

    .line 155
    invoke-static/range {v0 .. v7}, Lcom/twitter/library/api/a;->a(Landroid/content/Context;Ljava/lang/String;JZLcom/twitter/async/service/j;ZLjava/lang/String;)V

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    const-string/jumbo v0, "app:twitter_service:timeline:request"

    return-object v0
.end method

.method protected g()Lcom/twitter/library/service/d$a;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 189
    invoke-super {p0}, Lbfy;->g()Lcom/twitter/library/service/d$a;

    move-result-object v3

    .line 191
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string/jumbo v4, "timeline"

    aput-object v4, v1, v2

    const-string/jumbo v4, "home"

    aput-object v4, v1, v0

    invoke-virtual {v3, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    const-string/jumbo v4, "user_id"

    .line 192
    invoke-virtual {p0}, Lbfr;->C()J

    move-result-wide v6

    invoke-virtual {v1, v4, v6, v7}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    .line 193
    iget-boolean v1, p0, Lbfr;->m:Z

    if-eqz v1, :cond_0

    .line 194
    invoke-direct {p0, v3}, Lbfr;->a(Lcom/twitter/library/service/d$a;)V

    .line 199
    :cond_0
    const-string/jumbo v1, "pc"

    invoke-virtual {v3, v1, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 200
    const-string/jumbo v1, "earned"

    invoke-virtual {v3, v1, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 201
    const-string/jumbo v1, "include_my_retweet"

    invoke-virtual {v3, v1, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 205
    iget-object v1, p0, Lbfr;->k:Lcom/twitter/util/collection/h;

    if-eqz v1, :cond_3

    .line 206
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 208
    iget-object v1, p0, Lbfr;->k:Lcom/twitter/util/collection/h;

    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 209
    invoke-static {v0}, Lcoi;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 210
    const-string/jumbo v7, "unassigned"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 211
    if-nez v1, :cond_1

    .line 212
    const/16 v1, 0x2c

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 215
    :cond_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    const/16 v0, 0x2f

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 217
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v2

    :goto_1
    move v1, v0

    .line 219
    goto :goto_0

    .line 220
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 221
    const-string/jumbo v0, "force_buckets"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 225
    :cond_3
    return-object v3

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method protected s()I
    .locals 1

    .prologue
    .line 242
    const/4 v0, 0x0

    return v0
.end method

.method protected y()V
    .locals 6

    .prologue
    .line 249
    new-instance v0, Lben;

    iget-object v1, p0, Lbfr;->p:Landroid/content/Context;

    invoke-virtual {p0}, Lbfr;->C()J

    move-result-wide v2

    iget-object v4, p0, Lbfr;->c:Ljava/lang/String;

    iget-object v5, p0, Lbfr;->j:Lcom/twitter/library/network/t;

    .line 250
    invoke-virtual {v5}, Lcom/twitter/library/network/t;->b()Lcom/twitter/model/account/OAuthToken;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lben;-><init>(Landroid/content/Context;JLjava/lang/String;Lcom/twitter/model/account/OAuthToken;)V

    const-string/jumbo v1, "Retrying logging promoted event does not occur because of user interaction."

    invoke-virtual {v0, v1}, Lben;->l(Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v0

    .line 249
    invoke-virtual {p0, v0}, Lbfr;->b(Lcom/twitter/async/service/AsyncOperation;)V

    .line 252
    return-void
.end method
