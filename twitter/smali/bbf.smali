.class public Lbbf;
.super Lcom/twitter/library/service/j;
.source "Twttr"


# static fields
.field private static final i:[I


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Z

.field public g:Z

.field private j:I

.field private k:I

.field private l:Lcom/twitter/library/provider/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lbbf;->i:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x2
        0x4
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 75
    const-class v0, Lbbf;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lbbf;->i:[I

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/twitter/library/service/j;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;[I)V

    .line 64
    iput v2, p0, Lbbf;->j:I

    .line 66
    iput v2, p0, Lbbf;->k:I

    .line 76
    invoke-static {}, Lcom/twitter/library/provider/j;->c()Lcom/twitter/library/provider/j;

    move-result-object v0

    iput-object v0, p0, Lbbf;->l:Lcom/twitter/library/provider/j;

    .line 77
    return-void
.end method

.method private e(I)I
    .locals 2

    .prologue
    .line 155
    iget v0, p0, Lbbf;->j:I

    or-int/2addr v0, p1

    iget v1, p0, Lbbf;->k:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    return v0
.end method

.method private v()V
    .locals 7

    .prologue
    .line 159
    invoke-virtual {p0}, Lbbf;->h()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-wide v2, v0, Lcom/twitter/library/service/v;->c:J

    .line 160
    iget-object v1, p0, Lbbf;->l:Lcom/twitter/library/provider/j;

    invoke-virtual {p0, v2, v3}, Lbbf;->a(J)I

    move-result v0

    invoke-direct {p0, v0}, Lbbf;->e(I)I

    move-result v4

    .line 161
    invoke-virtual {p0}, Lbbf;->e()Z

    move-result v5

    const/4 v6, 0x0

    .line 160
    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/j;->a(JIZLaut;)I

    .line 162
    return-void
.end method


# virtual methods
.method protected a(J)I
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 193
    invoke-static {p1, p2}, Lcom/twitter/library/platform/notifications/PushRegistration;->a(J)I

    move-result v0

    return v0
.end method

.method public a(I)Lbbf;
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lbbf;->j:I

    invoke-static {v0, p1}, Lcga;->a(II)I

    move-result v0

    iput v0, p0, Lbbf;->j:I

    .line 82
    return-object p0
.end method

.method protected a()V
    .locals 7

    .prologue
    .line 105
    invoke-virtual {p0}, Lbbf;->h()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-wide v2, v0, Lcom/twitter/library/service/v;->c:J

    .line 106
    invoke-virtual {p0}, Lbbf;->h()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-object v4, v0, Lcom/twitter/library/service/v;->e:Ljava/lang/String;

    .line 107
    invoke-virtual {p0}, Lbbf;->u()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 148
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid action "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lbbf;->u()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :pswitch_1
    iget-object v0, p0, Lbbf;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lbbf;->a(Ljava/lang/String;)V

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 113
    :pswitch_2
    iget v5, p0, Lbbf;->b:I

    iget-boolean v6, p0, Lbbf;->c:Z

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lbbf;->a(JLjava/lang/String;IZ)V

    goto :goto_0

    .line 117
    :pswitch_3
    invoke-virtual {p0}, Lbbf;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 118
    invoke-virtual {p0, v2, v3, v4}, Lbbf;->a(JLjava/lang/String;)Z

    move-result v0

    .line 119
    if-eqz v0, :cond_1

    .line 122
    invoke-virtual {p0, v2, v3}, Lbbf;->a(J)I

    move-result v0

    .line 123
    invoke-direct {p0, v0}, Lbbf;->e(I)I

    move-result v5

    .line 124
    if-eq v0, v5, :cond_0

    .line 125
    iget-boolean v6, p0, Lbbf;->c:Z

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lbbf;->a(JLjava/lang/String;IZ)V

    goto :goto_0

    .line 131
    :cond_1
    invoke-direct {p0}, Lbbf;->v()V

    .line 134
    invoke-virtual {p0, v2, v3}, Lbbf;->b(J)Lcom/twitter/library/platform/notifications/t;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/t;->a(Z)V

    .line 135
    iget-boolean v0, p0, Lbbf;->g:Z

    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {p0}, Lbbf;->b()V

    goto :goto_0

    .line 143
    :cond_2
    invoke-direct {p0}, Lbbf;->v()V

    goto :goto_0

    .line 107
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected a(JLjava/lang/String;IZ)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lbbf;->h:Landroid/content/Context;

    invoke-static {v0, p1, p2, p4, p5}, Lcom/twitter/library/platform/notifications/PushRegistration;->a(Landroid/content/Context;JIZ)V

    .line 173
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 166
    iget-object v0, p0, Lbbf;->h:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/twitter/library/platform/notifications/PushRegistration;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 167
    return-void
.end method

.method protected a(JLjava/lang/String;)Z
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 187
    iget-object v0, p0, Lbbf;->h:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/twitter/library/platform/notifications/PushRegistration;->b(Landroid/content/Context;J)Z

    move-result v0

    return v0
.end method

.method protected b(J)Lcom/twitter/library/platform/notifications/t;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 199
    iget-object v0, p0, Lbbf;->h:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/twitter/library/platform/notifications/t;->a(Landroid/content/Context;J)Lcom/twitter/library/platform/notifications/t;

    move-result-object v0

    return-object v0
.end method

.method protected b()V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 177
    iget-object v0, p0, Lbbf;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/platform/notifications/PushRegistration;->a(Landroid/content/Context;)V

    .line 178
    return-void
.end method

.method public c(I)Lbbf;
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lbbf;->k:I

    invoke-static {v0, p1}, Lcga;->a(II)I

    move-result v0

    iput v0, p0, Lbbf;->k:I

    .line 100
    return-object p0
.end method

.method protected e()Z
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 182
    iget-object v0, p0, Lbbf;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
