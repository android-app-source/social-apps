.class public Lbrd;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/object/d;
.implements Lcom/twitter/util/object/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/object/d",
        "<",
        "Lcom/twitter/model/core/Tweet;",
        "Lbrc;",
        ">;",
        "Lcom/twitter/util/object/f",
        "<",
        "Ljava/lang/Long;",
        "Lcar;",
        "Lbrc;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/Tweet;)Lbrc;
    .locals 2

    .prologue
    .line 32
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcax;

    .line 33
    invoke-virtual {v0}, Lcax;->K()Lcar;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcar;

    .line 34
    new-instance v1, Lbrc;

    invoke-direct {v1, v0}, Lbrc;-><init>(Lcar;)V

    return-object v1
.end method

.method public a(Ljava/lang/Long;Lcar;)Lbrc;
    .locals 4

    .prologue
    .line 26
    new-instance v0, Lbrc;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v2, v3, p2}, Lbrc;-><init>(JLcar;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    check-cast p1, Lcom/twitter/model/core/Tweet;

    invoke-virtual {p0, p1}, Lbrd;->a(Lcom/twitter/model/core/Tweet;)Lbrc;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    check-cast p1, Ljava/lang/Long;

    check-cast p2, Lcar;

    invoke-virtual {p0, p1, p2}, Lbrd;->a(Ljava/lang/Long;Lcar;)Lbrc;

    move-result-object v0

    return-object v0
.end method
