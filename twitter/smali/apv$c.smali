.class public abstract Lapv$c;
.super Lapn$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lapv;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "c"
.end annotation


# instance fields
.field final a:Landroid/view/ViewGroup;

.field private final b:Landroid/view/View;

.field private final c:Landroid/view/ViewGroup;

.field private final d:Landroid/widget/TextView;

.field private final e:Lcom/twitter/library/av/DMVideoThumbnailView;

.field private final f:Lcom/twitter/android/av/video/VideoContainerHost;

.field private final g:Lcom/twitter/media/ui/image/MediaImageView;

.field private final h:Landroid/view/ViewGroup;

.field private final i:Landroid/view/View;

.field private final j:Lcom/twitter/library/view/QuoteView;

.field private final k:Landroid/view/View;

.field private final l:Landroid/view/View;

.field private final m:Landroid/view/View;

.field private final n:Landroid/widget/Button;

.field private final o:Landroid/widget/Button;

.field private final p:Landroid/view/View;

.field private final q:Lcom/twitter/media/ui/image/MediaImageView;


# direct methods
.method constructor <init>(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 858
    invoke-direct {p0, p1}, Lapn$b;-><init>(Landroid/view/View;)V

    .line 860
    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lapv$c;->b:Landroid/view/View;

    .line 861
    const v0, 0x7f130318

    .line 862
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 861
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lapv$c;->a:Landroid/view/ViewGroup;

    .line 863
    if-eqz p2, :cond_0

    const v0, 0x7f130328

    .line 864
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    :goto_0
    iput-object v0, p0, Lapv$c;->c:Landroid/view/ViewGroup;

    .line 867
    iget-object v0, p0, Lapv$c;->a:Landroid/view/ViewGroup;

    const v1, 0x7f130019

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lapv$c;->d:Landroid/widget/TextView;

    .line 868
    iget-object v0, p0, Lapv$c;->d:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/twitter/ui/view/g;->a(Landroid/widget/TextView;)V

    .line 869
    iget-object v0, p0, Lapv$c;->a:Landroid/view/ViewGroup;

    const v1, 0x7f130323

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lapv$c;->l:Landroid/view/View;

    .line 870
    iget-object v0, p0, Lapv$c;->a:Landroid/view/ViewGroup;

    const v1, 0x7f130322

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lapv$c;->k:Landroid/view/View;

    .line 872
    iget-object v0, p0, Lapv$c;->a:Landroid/view/ViewGroup;

    const v1, 0x7f130325

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lapv$c;->m:Landroid/view/View;

    .line 873
    iget-object v0, p0, Lapv$c;->m:Landroid/view/View;

    const v1, 0x7f130326

    .line 874
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lapv$c;->n:Landroid/widget/Button;

    .line 875
    iget-object v0, p0, Lapv$c;->m:Landroid/view/View;

    const v1, 0x7f130327

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lapv$c;->o:Landroid/widget/Button;

    .line 878
    iget-object v0, p0, Lapv$c;->a:Landroid/view/ViewGroup;

    const v1, 0x7f130319

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lapv$c;->i:Landroid/view/View;

    .line 879
    iget-object v0, p0, Lapv$c;->i:Landroid/view/View;

    const v1, 0x7f13031d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lapv$c;->g:Lcom/twitter/media/ui/image/MediaImageView;

    .line 880
    iget-object v0, p0, Lapv$c;->i:Landroid/view/View;

    const v1, 0x7f13031b

    .line 881
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/DMVideoThumbnailView;

    iput-object v0, p0, Lapv$c;->e:Lcom/twitter/library/av/DMVideoThumbnailView;

    .line 882
    iget-object v0, p0, Lapv$c;->i:Landroid/view/View;

    const v1, 0x7f13031c

    .line 883
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/video/VideoContainerHost;

    iput-object v0, p0, Lapv$c;->f:Lcom/twitter/android/av/video/VideoContainerHost;

    .line 885
    iget-object v0, p0, Lapv$c;->i:Landroid/view/View;

    const v1, 0x7f13027b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/view/QuoteView;

    iput-object v0, p0, Lapv$c;->j:Lcom/twitter/library/view/QuoteView;

    .line 886
    iget-object v0, p0, Lapv$c;->i:Landroid/view/View;

    const v1, 0x7f130320

    .line 887
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 886
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lapv$c;->q:Lcom/twitter/media/ui/image/MediaImageView;

    .line 888
    iget-object v0, p0, Lapv$c;->i:Landroid/view/View;

    const v1, 0x7f13031e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lapv$c;->p:Landroid/view/View;

    .line 890
    iget-object v0, p0, Lapv$c;->i:Landroid/view/View;

    const v1, 0x7f13031f

    .line 891
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lapv$c;->h:Landroid/view/ViewGroup;

    .line 892
    return-void

    .line 864
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method static synthetic a(Lapv$c;)Landroid/view/View;
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lapv$c;->b:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lapv$c;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lapv$c;->c:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic c(Lapv$c;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lapv$c;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lapv$c;)Landroid/view/View;
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lapv$c;->l:Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(Lapv$c;)Landroid/view/View;
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lapv$c;->k:Landroid/view/View;

    return-object v0
.end method

.method static synthetic f(Lapv$c;)Landroid/view/View;
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lapv$c;->m:Landroid/view/View;

    return-object v0
.end method

.method static synthetic g(Lapv$c;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lapv$c;->n:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic h(Lapv$c;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lapv$c;->o:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic i(Lapv$c;)Landroid/view/View;
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lapv$c;->i:Landroid/view/View;

    return-object v0
.end method

.method static synthetic j(Lapv$c;)Lcom/twitter/media/ui/image/MediaImageView;
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lapv$c;->g:Lcom/twitter/media/ui/image/MediaImageView;

    return-object v0
.end method

.method static synthetic k(Lapv$c;)Lcom/twitter/media/ui/image/MediaImageView;
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lapv$c;->q:Lcom/twitter/media/ui/image/MediaImageView;

    return-object v0
.end method

.method static synthetic l(Lapv$c;)Lcom/twitter/library/av/DMVideoThumbnailView;
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lapv$c;->e:Lcom/twitter/library/av/DMVideoThumbnailView;

    return-object v0
.end method

.method static synthetic m(Lapv$c;)Lcom/twitter/android/av/video/VideoContainerHost;
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lapv$c;->f:Lcom/twitter/android/av/video/VideoContainerHost;

    return-object v0
.end method

.method static synthetic n(Lapv$c;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lapv$c;->h:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic o(Lapv$c;)Lcom/twitter/library/view/QuoteView;
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lapv$c;->j:Lcom/twitter/library/view/QuoteView;

    return-object v0
.end method

.method static synthetic p(Lapv$c;)Landroid/view/View;
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lapv$c;->p:Landroid/view/View;

    return-object v0
.end method
