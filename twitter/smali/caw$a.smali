.class Lcaw$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcaw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcaw$a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcaw;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcaw$1;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcaw$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/util/serialization/n;I)Lcaw;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 70
    sget-object v0, Lcaw;->b:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    .line 71
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    .line 72
    new-instance v2, Lcaw;

    invoke-direct {v2, v0, v1}, Lcaw;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v2
.end method

.method public a(Lcom/twitter/util/serialization/o;Lcaw;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    sget-object v0, Lcaw;->b:Lcom/twitter/util/serialization/l;

    iget-object v1, p2, Lcaw;->c:Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    .line 63
    iget-object v0, p2, Lcaw;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 64
    return-void
.end method

.method public synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    check-cast p2, Lcaw;

    invoke-virtual {p0, p1, p2}, Lcaw$a;->a(Lcom/twitter/util/serialization/o;Lcaw;)V

    return-void
.end method

.method public synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 58
    invoke-virtual {p0, p1, p2}, Lcaw$a;->a(Lcom/twitter/util/serialization/n;I)Lcaw;

    move-result-object v0

    return-object v0
.end method
