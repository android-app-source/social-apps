.class public Lavy;
.super Lcbr;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbr",
        "<",
        "Laxp$a;",
        "Lcom/twitter/model/core/TwitterUser;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcbr;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    check-cast p1, Laxp$a;

    invoke-virtual {p0, p1}, Lavy;->b(Laxp$a;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    return-object v0
.end method

.method public a(Laxp$a;)Z
    .locals 4

    .prologue
    .line 22
    new-instance v0, Lcom/twitter/model/core/TwitterUser$a;

    invoke-direct {v0}, Lcom/twitter/model/core/TwitterUser$a;-><init>()V

    .line 23
    invoke-interface {p1}, Laxp$a;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/TwitterUser$a;->a(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser$a;->R_()Z

    move-result v0

    .line 22
    return v0
.end method

.method public b(Laxp$a;)Lcom/twitter/model/core/TwitterUser;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 30
    invoke-interface {p1}, Laxp$a;->p()I

    move-result v3

    .line 32
    new-instance v0, Lcom/twitter/model/core/TwitterUser$a;

    invoke-direct {v0}, Lcom/twitter/model/core/TwitterUser$a;-><init>()V

    .line 33
    invoke-interface {p1}, Laxp$a;->b()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/core/TwitterUser$a;->a(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 34
    invoke-interface {p1}, Laxp$a;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->b(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 35
    invoke-interface {p1}, Laxp$a;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->g(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 36
    invoke-interface {p1}, Laxp$a;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->c(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 37
    invoke-interface {p1}, Laxp$a;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->i(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 38
    invoke-interface {p1}, Laxp$a;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->e(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 39
    invoke-interface {p1}, Laxp$a;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->f(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 40
    invoke-interface {p1}, Laxp$a;->i()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->a(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 41
    invoke-interface {p1}, Laxp$a;->j()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->b(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 42
    invoke-interface {p1}, Laxp$a;->f()Lcom/twitter/model/core/v;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/core/v;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 43
    invoke-interface {p1}, Laxp$a;->h()Lcom/twitter/model/core/v;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->b(Lcom/twitter/model/core/v;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 44
    invoke-interface {p1}, Laxp$a;->y()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/core/TwitterUser$a;->b(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 45
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/TwitterUser$a;->a(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v4

    and-int/lit8 v0, v3, 0x1

    if-eqz v0, :cond_0

    move v0, v1

    .line 46
    :goto_0
    invoke-virtual {v4, v0}, Lcom/twitter/model/core/TwitterUser$a;->b(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v4

    and-int/lit8 v0, v3, 0x2

    if-eqz v0, :cond_1

    move v0, v1

    .line 47
    :goto_1
    invoke-virtual {v4, v0}, Lcom/twitter/model/core/TwitterUser$a;->c(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v4

    and-int/lit8 v0, v3, 0x4

    if-eqz v0, :cond_2

    move v0, v1

    .line 48
    :goto_2
    invoke-virtual {v4, v0}, Lcom/twitter/model/core/TwitterUser$a;->d(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v4

    and-int/lit8 v0, v3, 0x8

    if-eqz v0, :cond_3

    move v0, v1

    .line 49
    :goto_3
    invoke-virtual {v4, v0}, Lcom/twitter/model/core/TwitterUser$a;->e(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 50
    invoke-interface {p1}, Laxp$a;->q()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->c(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 51
    invoke-interface {p1}, Laxp$a;->r()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->d(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 52
    invoke-interface {p1}, Laxp$a;->s()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->e(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 53
    invoke-interface {p1}, Laxp$a;->u()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->h(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 54
    invoke-interface {p1}, Laxp$a;->t()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->f(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 55
    invoke-interface {p1}, Laxp$a;->v()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->g(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v4

    and-int/lit8 v0, v3, 0x10

    if-eqz v0, :cond_4

    move v0, v1

    .line 56
    :goto_4
    invoke-virtual {v4, v0}, Lcom/twitter/model/core/TwitterUser$a;->g(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v4

    and-int/lit16 v0, v3, 0x200

    if-eqz v0, :cond_5

    move v0, v1

    .line 57
    :goto_5
    invoke-virtual {v4, v0}, Lcom/twitter/model/core/TwitterUser$a;->f(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 58
    invoke-interface {p1}, Laxp$a;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->h(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 59
    invoke-interface {p1}, Laxp$a;->o()Lcom/twitter/util/collection/k;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/util/collection/k;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 60
    invoke-interface {p1}, Laxp$a;->m()Lcom/twitter/model/profile/ExtendedProfile;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/profile/ExtendedProfile;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 61
    invoke-interface {p1}, Laxp$a;->A()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/core/TwitterUser$a;->e(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v4

    .line 62
    invoke-interface {p1}, Laxp$a;->B()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 64
    invoke-static {}, Lcom/twitter/model/ads/AdvertiserType;->values()[Lcom/twitter/model/ads/AdvertiserType;

    move-result-object v0

    invoke-interface {p1}, Laxp$a;->B()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/twitter/util/object/ObjectUtils;->a([Ljava/lang/Enum;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    sget-object v5, Lcom/twitter/model/ads/AdvertiserType;->a:Lcom/twitter/model/ads/AdvertiserType;

    .line 63
    invoke-static {v0, v5}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/ads/AdvertiserType;

    .line 62
    :goto_6
    invoke-virtual {v4, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/ads/AdvertiserType;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v4

    .line 67
    invoke-interface {p1}, Laxp$a;->C()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 69
    invoke-static {}, Lcom/twitter/model/businessprofiles/BusinessProfileState;->values()[Lcom/twitter/model/businessprofiles/BusinessProfileState;

    move-result-object v0

    .line 70
    invoke-interface {p1}, Laxp$a;->C()Ljava/lang/String;

    move-result-object v5

    .line 69
    invoke-static {v0, v5}, Lcom/twitter/util/object/ObjectUtils;->a([Ljava/lang/Enum;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    sget-object v5, Lcom/twitter/model/businessprofiles/BusinessProfileState;->a:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    .line 68
    invoke-static {v0, v5}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/businessprofiles/BusinessProfileState;

    .line 67
    :goto_7
    invoke-virtual {v4, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/businessprofiles/BusinessProfileState;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v4

    .line 73
    invoke-interface {p1}, Laxp$a;->D()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 75
    invoke-static {}, Lcom/twitter/model/profile/TranslatorType;->values()[Lcom/twitter/model/profile/TranslatorType;

    move-result-object v0

    invoke-interface {p1}, Laxp$a;->D()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/twitter/util/object/ObjectUtils;->a([Ljava/lang/Enum;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    sget-object v5, Lcom/twitter/model/profile/TranslatorType;->a:Lcom/twitter/model/profile/TranslatorType;

    .line 74
    invoke-static {v0, v5}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/TranslatorType;

    .line 73
    :goto_8
    invoke-virtual {v4, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/profile/TranslatorType;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 79
    invoke-interface {p1}, Laxp$a;->p()I

    move-result v4

    invoke-static {v4}, Lcom/twitter/model/revenue/b;->b(I)Ljava/util/List;

    move-result-object v4

    .line 78
    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->a(Ljava/util/List;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v4

    and-int/lit16 v0, v3, 0x400

    if-eqz v0, :cond_9

    move v0, v1

    .line 80
    :goto_9
    invoke-virtual {v4, v0}, Lcom/twitter/model/core/TwitterUser$a;->j(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 81
    invoke-interface {p1}, Laxp$a;->w()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->i(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 82
    invoke-interface {p1}, Laxp$a;->x()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/core/TwitterUser$a;->c(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 83
    invoke-interface {p1}, Laxp$a;->z()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/core/TwitterUser$a;->d(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 84
    invoke-virtual {v0, v2}, Lcom/twitter/model/core/TwitterUser$a;->h(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    and-int/lit8 v4, v3, 0x20

    if-eqz v4, :cond_a

    .line 85
    :goto_a
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->i(Z)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 86
    invoke-virtual {v0, v3}, Lcom/twitter/model/core/TwitterUser$a;->j(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 32
    return-object v0

    :cond_0
    move v0, v2

    .line 45
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 46
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 47
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 48
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 55
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 56
    goto/16 :goto_5

    .line 63
    :cond_6
    sget-object v0, Lcom/twitter/model/ads/AdvertiserType;->a:Lcom/twitter/model/ads/AdvertiserType;

    goto/16 :goto_6

    .line 68
    :cond_7
    sget-object v0, Lcom/twitter/model/businessprofiles/BusinessProfileState;->a:Lcom/twitter/model/businessprofiles/BusinessProfileState;

    goto :goto_7

    .line 74
    :cond_8
    sget-object v0, Lcom/twitter/model/profile/TranslatorType;->a:Lcom/twitter/model/profile/TranslatorType;

    goto :goto_8

    :cond_9
    move v0, v2

    .line 78
    goto :goto_9

    :cond_a
    move v1, v2

    .line 84
    goto :goto_a
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 17
    check-cast p1, Laxp$a;

    invoke-virtual {p0, p1}, Lavy;->a(Laxp$a;)Z

    move-result v0

    return v0
.end method
