.class public Lchy;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lchy$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lchy;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/topic/trends/TrendBadge;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lchb;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lchy$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lchy$a;-><init>(Lchy$1;)V

    sput-object v0, Lchy;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lchb;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/topic/trends/TrendBadge;",
            ">;",
            "Lchb;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lchy;->b:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lchy;->c:Ljava/lang/String;

    .line 33
    iput-object p3, p0, Lchy;->d:Ljava/lang/String;

    .line 34
    iput-object p4, p0, Lchy;->e:Ljava/lang/String;

    .line 35
    invoke-static {p5}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lchy;->f:Ljava/util/List;

    .line 36
    iput-object p6, p0, Lchy;->g:Lchb;

    .line 37
    return-void
.end method
