.class final Laeu$7;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcpp;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Laeu;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcpp",
        "<",
        "Lcom/twitter/model/json/people/JsonModulePage;",
        "Lcom/twitter/model/people/h;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/json/people/JsonModulePage;)Lcom/twitter/model/people/h;
    .locals 2

    .prologue
    .line 250
    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    new-instance v0, Lcom/twitter/model/people/h$a;

    invoke-direct {v0}, Lcom/twitter/model/people/h$a;-><init>()V

    iget-object v1, p1, Lcom/twitter/model/json/people/JsonModulePage;->b:Lcom/twitter/model/people/d;

    .line 252
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/h$a;->a(Lcom/twitter/model/people/d;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/h$a;

    iget-object v1, p1, Lcom/twitter/model/json/people/JsonModulePage;->c:Ljava/lang/String;

    .line 253
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/h$a;->a(Ljava/lang/String;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/h$a;

    iget-object v1, p1, Lcom/twitter/model/json/people/JsonModulePage;->d:Lcom/twitter/model/people/k;

    .line 254
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/h$a;->a(Lcom/twitter/model/people/k;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/h$a;

    iget-object v1, p1, Lcom/twitter/model/json/people/JsonModulePage;->e:Ljava/util/List;

    .line 255
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/h$a;->a(Ljava/lang/Iterable;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/h$a;

    iget-object v1, p1, Lcom/twitter/model/json/people/JsonModulePage;->f:Ljava/util/List;

    .line 256
    invoke-static {v1}, Laeu;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/h$a;->b(Ljava/lang/Iterable;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/h$a;

    iget-object v1, p1, Lcom/twitter/model/json/people/JsonModulePage;->h:Lcom/twitter/model/people/i;

    .line 257
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/h$a;->a(Lcom/twitter/model/people/i;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/h$a;

    .line 258
    invoke-virtual {v0}, Lcom/twitter/model/people/h$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/h;

    .line 251
    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 246
    check-cast p1, Lcom/twitter/model/json/people/JsonModulePage;

    invoke-virtual {p0, p1}, Laeu$7;->a(Lcom/twitter/model/json/people/JsonModulePage;)Lcom/twitter/model/people/h;

    move-result-object v0

    return-object v0
.end method
