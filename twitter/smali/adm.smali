.class public Ladm;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcmu;


# instance fields
.field private final a:Ladn;

.field private final b:Lado;


# direct methods
.method public constructor <init>(Ladn;Lado;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Ladm;->a:Ladn;

    .line 24
    iput-object p2, p0, Ladm;->b:Lado;

    .line 25
    return-void
.end method


# virtual methods
.method public a(J)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/c",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Ladm;->a:Ladn;

    invoke-virtual {v0, p1, p2}, Ladn;->a(J)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Ladm;->b:Lado;

    .line 31
    invoke-virtual {v1, p1, p2}, Lado;->a(J)Lrx/c;

    move-result-object v1

    new-instance v2, Ladm$1;

    invoke-direct {v2, p0}, Ladm$1;-><init>(Ladm;)V

    .line 30
    invoke-static {v0, v1, v2}, Lrx/c;->a(Lrx/c;Lrx/c;Lrx/functions/e;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 43
    :try_start_0
    iget-object v0, p0, Ladm;->a:Ladn;

    invoke-virtual {v0}, Ladn;->close()V

    .line 44
    iget-object v0, p0, Ladm;->b:Lado;

    invoke-virtual {v0}, Lado;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :goto_0
    return-void

    .line 45
    :catch_0
    move-exception v0

    goto :goto_0
.end method
