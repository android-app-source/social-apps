.class public final Lasp;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Laso;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcsd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcsd",
            "<",
            "Laso;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lasp;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lasp;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcsd;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Laso;",
            ">;",
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    sget-boolean v0, Lasp;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 29
    :cond_0
    iput-object p1, p0, Lasp;->b:Lcsd;

    .line 31
    sget-boolean v0, Lasp;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_1
    iput-object p2, p0, Lasp;->c:Lcta;

    .line 33
    sget-boolean v0, Lasp;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 34
    :cond_2
    iput-object p3, p0, Lasp;->d:Lcta;

    .line 35
    return-void
.end method

.method public static a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Laso;",
            ">;",
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Laso;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Lasp;

    invoke-direct {v0, p0, p1, p2}, Lasp;-><init>(Lcsd;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Laso;
    .locals 4

    .prologue
    .line 39
    iget-object v2, p0, Lasp;->b:Lcsd;

    new-instance v3, Laso;

    iget-object v0, p0, Lasp;->c:Lcta;

    .line 41
    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lasp;->d:Lcta;

    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/client/Session;

    invoke-direct {v3, v0, v1}, Laso;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 39
    invoke-static {v2, v3}, Ldagger/internal/MembersInjectors;->a(Lcsd;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laso;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lasp;->a()Laso;

    move-result-object v0

    return-object v0
.end method
