.class public Lbfa;
.super Lbeq;
.source "Twttr"


# instance fields
.field private b:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lbfa;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lbeq;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 30
    new-instance v0, Lcom/twitter/library/service/o;

    invoke-direct {v0}, Lcom/twitter/library/service/o;-><init>()V

    invoke-virtual {p0, v0}, Lbfa;->a(Lcom/twitter/library/service/e;)V

    .line 31
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 6

    .prologue
    .line 46
    invoke-virtual {p0}, Lbfa;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 47
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "mutes/users/create"

    aput-object v3, v1, v2

    .line 48
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 49
    invoke-virtual {p0, v0}, Lbfa;->a(Lcom/twitter/library/service/d$a;)V

    .line 51
    iget-wide v2, p0, Lbfa;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 52
    const-string/jumbo v1, "expiry"

    iget-wide v2, p0, Lbfa;->b:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 54
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x2000

    .line 60
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/twitter/model/core/TwitterUser;

    .line 62
    invoke-virtual {p0}, Lbfa;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-wide v2, v0, Lcom/twitter/library/service/v;->c:J

    .line 65
    invoke-virtual {p0}, Lbfa;->R()Lcom/twitter/library/provider/t;

    move-result-object v0

    .line 66
    invoke-virtual {p0}, Lbfa;->S()Laut;

    move-result-object v10

    .line 67
    iget-wide v4, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v0, v4, v5, v6, v10}, Lcom/twitter/library/provider/t;->a(JILaut;)V

    .line 69
    iget-wide v4, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/provider/t;->h(J)I

    move-result v4

    .line 70
    new-instance v5, Lcom/twitter/model/core/TwitterUser$a;

    invoke-direct {v5, v1}, Lcom/twitter/model/core/TwitterUser$a;-><init>(Lcom/twitter/model/core/TwitterUser;)V

    .line 71
    invoke-static {v4, v6}, Lcom/twitter/model/core/g;->a(II)I

    move-result v1

    invoke-virtual {v5, v1}, Lcom/twitter/model/core/TwitterUser$a;->i(I)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v1

    .line 72
    invoke-virtual {v1}, Lcom/twitter/model/core/TwitterUser$a;->q()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/twitter/model/core/TwitterUser;

    .line 73
    invoke-static {v11}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const/16 v4, 0x1a

    const-wide/16 v5, -0x1

    const/4 v9, 0x1

    move-object v8, v7

    invoke-virtual/range {v0 .. v10}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;ZLaut;)I

    .line 76
    iget v1, v11, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-static {v1}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 78
    iget-wide v4, v11, Lcom/twitter/model/core/TwitterUser;->b:J

    move-object v1, v0

    move-object v6, v10

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/t;->d(JJLaut;)V

    .line 81
    :cond_0
    invoke-virtual {v10}, Laut;->a()V

    .line 83
    iget-object v0, p2, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "muted_username"

    iget-object v2, v11, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_1
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 24
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbfa;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    const-string/jumbo v0, "app:twitter_service:mute_user:create"

    return-object v0
.end method
