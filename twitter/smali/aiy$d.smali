.class Laiy$d;
.super Lcjr;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Laiy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laiy$d$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcjr",
        "<",
        "Laiy$e;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 173
    invoke-direct {p0, p1}, Lcjr;-><init>(Landroid/content/Context;)V

    .line 174
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;Laiy$e;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 181
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401bc

    const/4 v2, 0x0

    .line 182
    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 183
    new-instance v1, Laiy$d$a;

    invoke-direct {v1, p1, v0}, Laiy$d$a;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 184
    return-object v0
.end method

.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 171
    check-cast p2, Laiy$e;

    invoke-virtual {p0, p1, p2, p3}, Laiy$d;->a(Landroid/content/Context;Laiy$e;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/view/View;Landroid/content/Context;Laiy$e;)V
    .locals 3

    .prologue
    .line 191
    .line 192
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laiy$d$a;

    .line 194
    iget-object v1, p3, Laiy$e;->a:Lcgb$c;

    iget-object v1, v1, Lcgb$c;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Laiy$d$a;->b(Ljava/lang/String;)V

    .line 196
    iget-object v1, p3, Laiy$e;->a:Lcgb$c;

    iget-object v1, v1, Lcgb$c;->h:Ljava/util/Map;

    .line 197
    const-string/jumbo v2, "outer_desc"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 198
    invoke-virtual {v0, v1}, Laiy$d$a;->a(Ljava/lang/String;)V

    .line 200
    iget-object v1, p3, Laiy$e;->b:Ljava/lang/String;

    const-string/jumbo v2, "off"

    .line 201
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Laiy$d$a;->a(Z)V

    .line 202
    return-void

    .line 201
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 171
    check-cast p3, Laiy$e;

    invoke-virtual {p0, p1, p2, p3}, Laiy$d;->a(Landroid/view/View;Landroid/content/Context;Laiy$e;)V

    return-void
.end method
