.class public Lcdf;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcdf$a;,
        Lcdf$b;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcdf;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcdf;


# instance fields
.field public final c:Lcdd;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lccz;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lccv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lcdf$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcdf$b;-><init>(Lcdf$1;)V

    sput-object v0, Lcdf;->a:Lcom/twitter/util/serialization/l;

    .line 30
    new-instance v0, Lcdf$a;

    invoke-direct {v0}, Lcdf$a;-><init>()V

    .line 31
    invoke-virtual {v0}, Lcdf$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdf;

    sput-object v0, Lcdf;->b:Lcdf;

    .line 30
    return-void
.end method

.method private constructor <init>(Lcdf$a;)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-static {p1}, Lcdf$a;->a(Lcdf$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcdf;->d:Ljava/lang/String;

    .line 47
    invoke-static {p1}, Lcdf$a;->b(Lcdf$a;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/i;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcdf;->e:Ljava/util/Map;

    .line 48
    invoke-static {p1}, Lcdf$a;->c(Lcdf$a;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/o;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcdf;->f:Ljava/util/Set;

    .line 49
    invoke-static {p1}, Lcdf$a;->d(Lcdf$a;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/i;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcdf;->g:Ljava/util/Map;

    .line 51
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-static {p1}, Lcdf$a;->e(Lcdf$a;)Lcdd$a;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcdf;->a(Ljava/util/Date;Lcdd$a;)Lcdd$a;

    move-result-object v0

    invoke-virtual {v0}, Lcdd$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdd;

    iput-object v0, p0, Lcdf;->c:Lcdd;

    .line 52
    return-void
.end method

.method synthetic constructor <init>(Lcdf$a;Lcdf$1;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcdf;-><init>(Lcdf$a;)V

    return-void
.end method

.method private a(Ljava/util/Date;Lcdd$a;)Lcdd$a;
    .locals 10

    .prologue
    .line 102
    invoke-static {}, Lcoh;->a()J

    move-result-wide v2

    .line 104
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    iget-object v1, p2, Lcdd$a;->a:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/i;->b(Ljava/util/Map;)Lcom/twitter/util/collection/i;

    move-result-object v4

    .line 105
    iget-object v0, p0, Lcdf;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccz;

    .line 106
    iget-object v1, p2, Lcdd$a;->a:Ljava/util/Map;

    iget-object v6, v0, Lccz;->b:Ljava/lang/String;

    invoke-interface {v1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 107
    iget-object v1, v0, Lccz;->e:Ljava/util/Date;

    invoke-virtual {p1, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, Lccz;->f:Ljava/util/Date;

    invoke-virtual {p1, v1}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    .line 108
    :goto_1
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lccz;->b:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/twitter/model/util/b;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    .line 109
    if-eqz v1, :cond_3

    .line 110
    invoke-virtual {v0, v6}, Lccz;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 112
    :goto_2
    new-instance v6, Lcdc;

    iget-object v7, v0, Lccz;->b:Ljava/lang/String;

    invoke-direct {v6, v7, v1}, Lcdc;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    .line 113
    const-string/jumbo v7, "unassigned"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 114
    new-instance v7, Lcdc$a;

    iget-object v8, v0, Lccz;->b:Ljava/lang/String;

    iget v9, v0, Lccz;->c:I

    invoke-direct {v7, v8, v9, v1}, Lcdc$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v6, v7}, Lcdc;->a(Lcdc$a;)V

    .line 117
    :cond_1
    iget-object v0, v0, Lccz;->b:Ljava/lang/String;

    invoke-virtual {v4, v0, v6}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    goto :goto_0

    .line 107
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 110
    :cond_3
    const-string/jumbo v1, "unassigned"

    goto :goto_2

    .line 121
    :cond_4
    invoke-virtual {v4}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {p2, v0}, Lcdd$a;->a(Ljava/util/Map;)Lcdd$a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcdf;->c:Lcdd;

    invoke-virtual {v0, p1}, Lcdd;->a(Ljava/lang/String;)Lcdc;

    move-result-object v0

    .line 66
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcdc;->a()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Lcdc;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcdf;->c:Lcdd;

    invoke-virtual {v0, p1}, Lcdd;->a(Ljava/lang/String;)Lcdc;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 76
    if-ne p0, p1, :cond_1

    .line 84
    :cond_0
    :goto_0
    return v0

    .line 79
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 80
    goto :goto_0

    .line 83
    :cond_3
    check-cast p1, Lcdf;

    .line 84
    iget-object v2, p0, Lcdf;->c:Lcdd;

    iget-object v3, p1, Lcdf;->c:Lcdd;

    invoke-virtual {v2, v3}, Lcdd;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcdf;->d:Ljava/lang/String;

    iget-object v3, p1, Lcdf;->d:Ljava/lang/String;

    .line 85
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcdf;->e:Ljava/util/Map;

    iget-object v3, p1, Lcdf;->e:Ljava/util/Map;

    .line 86
    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcdf;->f:Ljava/util/Set;

    iget-object v3, p1, Lcdf;->f:Ljava/util/Set;

    .line 87
    invoke-interface {v2, v3}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcdf;->c:Lcdd;

    invoke-virtual {v0}, Lcdd;->hashCode()I

    move-result v0

    .line 93
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcdf;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 94
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcdf;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcdf;->f:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    return v0
.end method
