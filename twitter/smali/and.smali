.class public Land;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Land$a;
    }
.end annotation


# static fields
.field private static final a:Lrx/functions/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/b",
            "<",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lrx/f;

.field private final d:Lrx/f;

.field private final e:Lcqt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Land$1;

    invoke-direct {v0}, Land$1;-><init>()V

    sput-object v0, Land;->a:Lrx/functions/b;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lrx/f;Lrx/f;Lcqt;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p4, p0, Land;->e:Lcqt;

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Land;->b:Landroid/content/Context;

    .line 51
    iput-object p2, p0, Land;->c:Lrx/f;

    .line 52
    iput-object p3, p0, Land;->d:Lrx/f;

    .line 53
    return-void
.end method

.method static synthetic a(Land;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Land;->b:Landroid/content/Context;

    return-object v0
.end method

.method private a(Lanb;Ljava/lang/Object;Land$a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Ljava/lang/Object;",
            ">(",
            "Lanb",
            "<TC;>;TC;",
            "Land$a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Land;->e:Lcqt;

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v0

    .line 139
    iget-object v2, p0, Land;->b:Landroid/content/Context;

    invoke-virtual {p1, v2, p2}, Lanb;->a(Landroid/content/Context;Ljava/lang/Object;)V

    .line 140
    if-eqz p3, :cond_0

    .line 141
    iget-object v2, p0, Land;->e:Lcqt;

    invoke-interface {v2}, Lcqt;->b()J

    move-result-wide v2

    sub-long v0, v2, v0

    invoke-interface {p3, p1, v0, v1}, Land$a;->a(Lanb;J)V

    .line 143
    :cond_0
    return-void
.end method

.method static synthetic a(Land;Lanb;Ljava/lang/Object;Land$a;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Land;->a(Lanb;Ljava/lang/Object;Land$a;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Iterable;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<",
            "Lanb",
            "<TC;>;>;TC;)V"
        }
    .end annotation

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Land;->a(Ljava/lang/Iterable;Ljava/lang/Object;Land$a;)V

    .line 60
    return-void
.end method

.method public a(Ljava/lang/Iterable;Ljava/lang/Object;Land$a;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<",
            "Lanb",
            "<TC;>;>;TC;",
            "Land$a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 68
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v1

    .line 69
    if-eqz p3, :cond_0

    .line 70
    invoke-interface {p3}, Land$a;->a()V

    .line 72
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lanb;

    .line 73
    iget-object v3, p0, Land;->b:Landroid/content/Context;

    invoke-virtual {v0, v3, p2}, Lanb;->b(Landroid/content/Context;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 74
    invoke-virtual {v0}, Lanb;->a()I

    move-result v3

    .line 75
    packed-switch v3, :pswitch_data_0

    .line 109
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid priority "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :pswitch_0
    new-instance v3, Land$2;

    invoke-direct {v3, p0, v0, p2}, Land$2;-><init>(Land;Lanb;Ljava/lang/Object;)V

    iget-object v0, p0, Land;->c:Lrx/f;

    invoke-static {v3, v0}, Lcre;->a(Lrx/functions/a;Lrx/f;)Lrx/a;

    move-result-object v0

    sget-object v3, Land;->a:Lrx/functions/b;

    .line 90
    invoke-virtual {v0, v3}, Lrx/a;->a(Lrx/functions/b;)Lrx/a;

    move-result-object v0

    .line 91
    invoke-static {}, Lcqv;->b()Lcqv;

    move-result-object v3

    invoke-virtual {v0, v3}, Lrx/a;->b(Lrx/b;)V

    goto :goto_0

    .line 95
    :pswitch_1
    new-instance v3, Land$3;

    invoke-direct {v3, p0, v0, p2, p3}, Land$3;-><init>(Land;Lanb;Ljava/lang/Object;Land$a;)V

    iget-object v0, p0, Land;->d:Lrx/f;

    invoke-static {v3, v0}, Lcre;->a(Lrx/functions/a;Lrx/f;)Lrx/a;

    move-result-object v0

    .line 101
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 105
    :pswitch_2
    invoke-direct {p0, v0, p2, p3}, Land;->a(Lanb;Ljava/lang/Object;Land$a;)V

    goto :goto_0

    .line 115
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 116
    iget-object v0, p0, Land;->e:Lcqt;

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v2

    .line 117
    invoke-static {}, Lcom/twitter/util/g;->b()Z

    move-result v4

    .line 120
    const/4 v0, 0x1

    :try_start_0
    invoke-static {v0}, Lcom/twitter/util/f;->a(Z)V

    .line 121
    invoke-static {v1}, Lrx/a;->a(Ljava/lang/Iterable;)Lrx/a;

    move-result-object v0

    sget-object v1, Land;->a:Lrx/functions/b;

    invoke-virtual {v0, v1}, Lrx/a;->a(Lrx/functions/b;)Lrx/a;

    move-result-object v0

    invoke-virtual {v0}, Lrx/a;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    invoke-static {v4}, Lcom/twitter/util/f;->a(Z)V

    .line 126
    if-eqz p3, :cond_3

    .line 127
    iget-object v0, p0, Land;->e:Lcqt;

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v0

    sub-long/2addr v0, v2

    invoke-interface {p3, v0, v1}, Land$a;->a(J)V

    .line 131
    :cond_3
    if-eqz p3, :cond_4

    .line 132
    invoke-interface {p3}, Land$a;->b()V

    .line 134
    :cond_4
    return-void

    .line 124
    :catchall_0
    move-exception v0

    invoke-static {v4}, Lcom/twitter/util/f;->a(Z)V

    throw v0

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
