.class public Laht;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:J


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-wide p1, p0, Laht;->a:J

    .line 32
    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    .line 27
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Laht;-><init>(J)V

    .line 28
    return-void
.end method

.method private a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 156
    invoke-direct/range {p0 .. p5}, Laht;->b(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 157
    return-void
.end method

.method private a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJ)V
    .locals 3

    .prologue
    .line 161
    invoke-direct/range {p0 .. p5}, Laht;->b(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 162
    const/16 v1, 0xc

    invoke-static {p7, p8, p5, v1, p6}, Lcom/twitter/library/scribe/b;->a(JLjava/lang/String;II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 164
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 165
    return-void
.end method

.method private b(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 4

    .prologue
    .line 170
    const-string/jumbo v0, "search_box"

    invoke-static {p1, v0, p2, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 172
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Laht;->a:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 173
    invoke-static {p4}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 174
    invoke-virtual {v0, p4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v1, p5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 176
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 35
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Laht;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "search:universal::saved_search:add"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 36
    return-void
.end method

.method public a(I)V
    .locals 4

    .prologue
    .line 116
    packed-switch p1, :pswitch_data_0

    .line 144
    :pswitch_0
    const-string/jumbo v0, "search_filter_top"

    move-object v1, v0

    .line 147
    :goto_0
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const-string/jumbo v2, "search"

    .line 148
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 149
    const-string/jumbo v2, ""

    const-string/jumbo v3, "navigate"

    .line 150
    invoke-static {v0, v2, v1, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 151
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Laht;->a:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 152
    return-void

    .line 118
    :pswitch_1
    const-string/jumbo v0, "search_filter_users"

    move-object v1, v0

    .line 119
    goto :goto_0

    .line 122
    :pswitch_2
    const-string/jumbo v0, "search_filter_photos"

    move-object v1, v0

    .line 123
    goto :goto_0

    .line 126
    :pswitch_3
    const-string/jumbo v0, "search_filter_videos"

    move-object v1, v0

    .line 127
    goto :goto_0

    .line 130
    :pswitch_4
    const-string/jumbo v0, "search_filter_news"

    move-object v1, v0

    .line 131
    goto :goto_0

    .line 135
    :pswitch_5
    const-string/jumbo v0, "search_filter_periscopes"

    move-object v1, v0

    .line 136
    goto :goto_0

    .line 139
    :pswitch_6
    const-string/jumbo v0, "search_filter_latest"

    move-object v1, v0

    .line 140
    goto :goto_0

    .line 116
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public a(Lage;)V
    .locals 10

    .prologue
    .line 62
    invoke-virtual {p1}, Lage;->i()I

    move-result v1

    .line 63
    invoke-virtual {p1}, Lage;->a()Ljava/lang/String;

    move-result-object v6

    .line 64
    invoke-static {v6}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 112
    :goto_0
    return-void

    .line 68
    :cond_0
    invoke-virtual {p1}, Lage;->d()Ljava/lang/String;

    move-result-object v5

    .line 69
    invoke-virtual {p1}, Lage;->f()I

    move-result v7

    .line 70
    invoke-virtual {p1}, Lage;->g()J

    move-result-wide v8

    .line 71
    invoke-virtual {p1}, Lage;->e()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v2

    .line 72
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 74
    :pswitch_0
    const-string/jumbo v3, "go_to_user"

    const-string/jumbo v4, "click"

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Laht;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 78
    :pswitch_1
    const-string/jumbo v3, "typeahead"

    const-string/jumbo v4, "profile_click"

    move-object v1, p0

    invoke-direct/range {v1 .. v9}, Laht;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJ)V

    goto :goto_0

    .line 83
    :pswitch_2
    const-string/jumbo v3, "user"

    const-string/jumbo v4, "click"

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Laht;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 87
    :pswitch_3
    const/4 v3, 0x0

    const-string/jumbo v4, "search"

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Laht;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 91
    :pswitch_4
    const-string/jumbo v3, "recent"

    const-string/jumbo v4, "search"

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Laht;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 95
    :pswitch_5
    const-string/jumbo v3, "typeahead"

    const-string/jumbo v4, "search"

    move-object v1, p0

    invoke-direct/range {v1 .. v9}, Laht;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJ)V

    goto :goto_0

    .line 100
    :pswitch_6
    const-string/jumbo v3, "saved_search"

    const-string/jumbo v4, "search"

    move-object v1, p0

    invoke-direct/range {v1 .. v9}, Laht;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJ)V

    goto :goto_0

    .line 105
    :pswitch_7
    const-string/jumbo v3, "cluster"

    const-string/jumbo v4, "search"

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Laht;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public b()V
    .locals 4

    .prologue
    .line 39
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Laht;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "search:universal::saved_search:remove"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 40
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    .line 43
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Laht;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "search:universal::query:share"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 44
    return-void
.end method

.method public d()V
    .locals 4

    .prologue
    .line 47
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Laht;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "search:universal:filter_sheet::impression"

    aput-object v3, v1, v2

    .line 48
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 47
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 49
    return-void
.end method

.method public e()V
    .locals 4

    .prologue
    .line 52
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Laht;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "search:universal:filter_sheet::apply"

    aput-object v3, v1, v2

    .line 53
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 52
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 54
    return-void
.end method

.method public f()V
    .locals 4

    .prologue
    .line 57
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Laht;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "search:universal:filter_sheet::cancel"

    aput-object v3, v1, v2

    .line 58
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 57
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 59
    return-void
.end method
