.class public Lcnm;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcnm$a;
    }
.end annotation


# static fields
.field public static final a:Lcnm;

.field public static final b:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcnm;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:I

.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 20
    new-instance v0, Lcnm;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcnm;-><init>(II)V

    sput-object v0, Lcnm;->a:Lcnm;

    .line 21
    new-instance v0, Lcnm$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcnm$a;-><init>(Lcnm$1;)V

    sput-object v0, Lcnm;->b:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput p1, p0, Lcnm;->c:I

    .line 35
    iput p2, p0, Lcnm;->d:I

    .line 36
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcnm;->c:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 47
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcnm;

    if-eqz v0, :cond_1

    iget v1, p0, Lcnm;->c:I

    move-object v0, p1

    check-cast v0, Lcnm;

    iget v0, v0, Lcnm;->c:I

    if-ne v1, v0, :cond_1

    iget v0, p0, Lcnm;->d:I

    check-cast p1, Lcnm;

    iget v1, p1, Lcnm;->d:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 53
    iget v0, p0, Lcnm;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p0, Lcnm;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
