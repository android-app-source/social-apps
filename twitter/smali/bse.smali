.class public Lbse;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbse$a;,
        Lbse$b;
    }
.end annotation


# instance fields
.field private final a:Lbse$b;

.field private final b:Lbse$a;

.field private final c:Lbry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbry",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lrx/f;

.field private final e:Lrx/f;


# direct methods
.method constructor <init>(Lbse$b;Lbse$a;Lbry;Lrx/f;Lrx/f;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbse$b;",
            "Lbse$a;",
            "Lbry",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;",
            "Lrx/f;",
            "Lrx/f;",
            ")V"
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lbse;->a:Lbse$b;

    .line 73
    iput-object p2, p0, Lbse;->b:Lbse$a;

    .line 74
    iput-object p3, p0, Lbse;->c:Lbry;

    .line 75
    iput-object p4, p0, Lbse;->d:Lrx/f;

    .line 76
    iput-object p5, p0, Lbse;->e:Lrx/f;

    .line 77
    return-void
.end method

.method static synthetic a(Lbse;)Lbse$b;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lbse;->a:Lbse$b;

    return-object v0
.end method

.method public static a(Lcom/twitter/library/provider/t;)Lbse;
    .locals 6

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v2

    .line 54
    new-instance v3, Lbrx;

    sget-object v1, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    const-class v0, Layg;

    .line 56
    invoke-interface {v2, v0}, Lcom/twitter/database/model/i;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Layg;

    invoke-interface {v0}, Layg;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    const-class v4, Layg$c;

    .line 57
    invoke-interface {v2, v4}, Lcom/twitter/database/model/i;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v4

    invoke-direct {v3, v1, v0, v4}, Lbrx;-><init>(Lcom/twitter/util/serialization/l;Lcom/twitter/database/model/l;Lcom/twitter/database/model/m;)V

    .line 58
    new-instance v0, Lbse;

    .line 59
    invoke-static {v2}, Lbse$b;->a(Lcom/twitter/database/model/i;)Lbse$b;

    move-result-object v1

    .line 60
    invoke-static {v2}, Lbse$a;->a(Lcom/twitter/database/model/i;)Lbse$a;

    move-result-object v2

    .line 62
    invoke-static {}, Lcws;->c()Lrx/f;

    move-result-object v4

    .line 63
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lbse;-><init>(Lbse$b;Lbse$a;Lbry;Lrx/f;Lrx/f;)V

    .line 58
    return-object v0
.end method

.method private a()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lrx/c",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 98
    new-instance v0, Lbse$1;

    invoke-direct {v0, p0}, Lbse$1;-><init>(Lbse;)V

    return-object v0
.end method


# virtual methods
.method public a(J)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/c",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lbse;->c:Lbry;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbry;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/c",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lbse;->b:Lbse$a;

    invoke-virtual {v0, p1}, Lbse$a;->a(Ljava/lang/String;)Lrx/c;

    move-result-object v0

    .line 91
    invoke-direct {p0}, Lbse;->a()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lbse;->d:Lrx/f;

    .line 92
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lbse;->e:Lrx/f;

    .line 93
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 90
    return-object v0
.end method

.method public a(JJ)V
    .locals 3

    .prologue
    .line 119
    iget-object v0, p0, Lbse;->c:Lbry;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbry;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 120
    return-void
.end method
