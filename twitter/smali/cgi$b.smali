.class Lcgi$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcgi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcgi;",
        "Lcgi$a;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 246
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/util/serialization/b;-><init>(I)V

    .line 247
    return-void
.end method


# virtual methods
.method protected a()Lcgi$a;
    .locals 1

    .prologue
    .line 266
    new-instance v0, Lcgi$a;

    invoke-direct {v0}, Lcgi$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcgi$a;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 273
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcgi$a;->a(Ljava/lang/String;)Lcgi$a;

    move-result-object v0

    .line 274
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcgi$a;->b(Ljava/lang/String;)Lcgi$a;

    move-result-object v0

    .line 275
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcgi$a;->a(J)Lcgi$a;

    move-result-object v0

    .line 276
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcgi$a;->c(Ljava/lang/String;)Lcgi$a;

    move-result-object v0

    .line 277
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcgi$a;->d(Ljava/lang/String;)Lcgi$a;

    move-result-object v0

    .line 278
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcgi$a;->a(Z)Lcgi$a;

    move-result-object v0

    .line 279
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcgi$a;->b(Z)Lcgi$a;

    .line 280
    if-nez p3, :cond_0

    .line 282
    sget-object v0, Lcom/twitter/util/serialization/f;->j:Lcom/twitter/util/serialization/l;

    sget-object v1, Lcom/twitter/util/serialization/f;->j:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Ljava/util/Map;

    .line 284
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcgi$a;->b(J)Lcgi$a;

    .line 285
    if-nez p3, :cond_1

    .line 287
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    .line 289
    :cond_1
    sget-object v0, Lcom/twitter/util/serialization/f;->j:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0}, Lcom/twitter/util/collection/d;->d(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcgi$a;->a(Ljava/util/Set;)Lcgi$a;

    .line 290
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 242
    check-cast p2, Lcgi$a;

    invoke-virtual {p0, p1, p2, p3}, Lcgi$b;->a(Lcom/twitter/util/serialization/n;Lcgi$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcgi;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 252
    iget-object v0, p2, Lcgi;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcgi;->d:Ljava/lang/String;

    .line 253
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcgi;->e:J

    .line 254
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcgi;->f:Ljava/lang/String;

    .line 255
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcgi;->g:Ljava/lang/String;

    .line 256
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcgi;->h:Z

    .line 257
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcgi;->i:Z

    .line 258
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcgi;->j:J

    .line 259
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    .line 260
    iget-object v0, p2, Lcgi;->k:Ljava/util/Set;

    sget-object v1, Lcom/twitter/util/serialization/f;->j:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/Set;Lcom/twitter/util/serialization/l;)V

    .line 261
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 242
    check-cast p2, Lcgi;

    invoke-virtual {p0, p1, p2}, Lcgi$b;->a(Lcom/twitter/util/serialization/o;Lcgi;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 242
    invoke-virtual {p0}, Lcgi$b;->a()Lcgi$a;

    move-result-object v0

    return-object v0
.end method
