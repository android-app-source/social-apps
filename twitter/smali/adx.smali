.class public Ladx;
.super Lcbp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ladx$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbp",
        "<",
        "Lcai;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ladt;


# direct methods
.method public constructor <init>(Ladt;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcbp;-><init>()V

    .line 17
    iput-object p1, p0, Ladx;->a:Ladt;

    .line 18
    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Lcai;
    .locals 9

    .prologue
    .line 23
    iget-object v0, p0, Ladx;->a:Ladt;

    invoke-virtual {v0, p1}, Ladt;->a(Landroid/database/Cursor;)Lbzy;

    move-result-object v0

    .line 24
    iget-wide v2, v0, Lbzy;->b:J

    .line 25
    new-instance v8, Lcai;

    new-instance v1, Ladx$a;

    iget-wide v6, v0, Lbzy;->c:J

    move-wide v4, v2

    invoke-direct/range {v1 .. v7}, Ladx$a;-><init>(JJJ)V

    invoke-direct {v8, v0, v1}, Lcai;-><init>(Lbzy;Lcac;)V

    return-object v8
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Ladx;->a(Landroid/database/Cursor;)Lcai;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 30
    invoke-static {p1}, Ladt;->b(Landroid/database/Cursor;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 13
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Ladx;->b(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
