.class public Latj;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lakv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lakv",
        "<",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Laks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laks",
            "<",
            "Lcom/twitter/android/ReportFlowWebViewActivity$a;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lasy;


# direct methods
.method public constructor <init>(Laks;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laks",
            "<",
            "Lcom/twitter/android/ReportFlowWebViewActivity$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Latj;->a:Laks;

    .line 28
    iget-object v0, p0, Latj;->a:Laks;

    invoke-virtual {v0, p0}, Laks;->a(Lakv;)V

    .line 29
    return-void
.end method


# virtual methods
.method public a(ILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 43
    packed-switch p1, :pswitch_data_0

    .line 57
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 48
    :pswitch_1
    iget-object v0, p0, Latj;->b:Lasy;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Latj;->b:Lasy;

    invoke-interface {v0}, Lasy;->a()V

    goto :goto_0

    .line 43
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic a(ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p2, Landroid/content/Intent;

    invoke-virtual {p0, p1, p2}, Latj;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public a(Lasy;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Latj;->b:Lasy;

    .line 33
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Latj;->a:Laks;

    new-instance v1, Lcom/twitter/android/ReportFlowWebViewActivity$a;

    invoke-direct {v1}, Lcom/twitter/android/ReportFlowWebViewActivity$a;-><init>()V

    invoke-virtual {v1, p1, p2}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/ReportFlowWebViewActivity$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Laks;->c(Lako;)V

    .line 37
    return-void
.end method
