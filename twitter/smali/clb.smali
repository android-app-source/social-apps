.class public Lclb;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lckt;


# instance fields
.field private final a:Lcks;

.field private final b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x1

    iput v0, p0, Lclb;->b:I

    .line 24
    invoke-direct {p0, p1}, Lclb;->a(Landroid/content/Context;)Lcks;

    move-result-object v0

    iput-object v0, p0, Lclb;->a:Lcks;

    .line 25
    return-void
.end method

.method private a(Landroid/content/Context;)Lcks;
    .locals 4

    .prologue
    .line 29
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lckx$a;->dummy_dock_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 30
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lckx$a;->dummy_docked_content_margin:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 31
    new-instance v2, Lcks;

    mul-int/lit8 v3, v0, 0x1

    invoke-direct {v2, v3, v0}, Lcks;-><init>(II)V

    .line 32
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, v2, Lcks;->c:Landroid/graphics/Rect;

    .line 33
    return-object v2
.end method


# virtual methods
.method public a()Lcks;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lclb;->a:Lcks;

    return-object v0
.end method
