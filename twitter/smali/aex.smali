.class public Laex;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lafa$a;
.implements Ltv/periscope/android/ui/broadcast/an$a;
.implements Ltv/periscope/android/ui/chat/aj;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laex$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/Session;

.field private final c:Lcom/twitter/model/core/Tweet;

.field private final d:Lcom/twitter/android/periscope/q;

.field private final e:Ltv/periscope/android/ui/chat/ak;

.field private final f:Ltv/periscope/android/ui/broadcast/an;

.field private final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ltv/periscope/android/ui/broadcast/an$a;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/Tweet;Lcom/twitter/android/periscope/q;)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Laex;->g:Ljava/util/Set;

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Laex;->a:Landroid/content/Context;

    .line 47
    iput-object p2, p0, Laex;->b:Lcom/twitter/library/client/Session;

    .line 48
    iput-object p3, p0, Laex;->c:Lcom/twitter/model/core/Tweet;

    .line 49
    iput-object p4, p0, Laex;->d:Lcom/twitter/android/periscope/q;

    .line 50
    new-instance v0, Lafa;

    iget-object v1, p0, Laex;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lafa;-><init>(Landroid/content/res/Resources;Lafa$a;)V

    iput-object v0, p0, Laex;->e:Ltv/periscope/android/ui/chat/ak;

    .line 51
    new-instance v0, Ltv/periscope/android/ui/broadcast/an;

    invoke-direct {v0, p0}, Ltv/periscope/android/ui/broadcast/an;-><init>(Ltv/periscope/android/ui/broadcast/an$a;)V

    iput-object v0, p0, Laex;->f:Ltv/periscope/android/ui/broadcast/an;

    .line 53
    iget-object v0, p0, Laex;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laex;->c:Lcom/twitter/model/core/Tweet;

    iget-boolean v0, v0, Lcom/twitter/model/core/Tweet;->F:Z

    if-eqz v0, :cond_1

    .line 54
    :cond_0
    iget-object v0, p0, Laex;->f:Ltv/periscope/android/ui/broadcast/an;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/an;->e()V

    .line 56
    :cond_1
    invoke-direct {p0}, Laex;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 57
    iget-object v0, p0, Laex;->f:Ltv/periscope/android/ui/broadcast/an;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/an;->c()V

    .line 59
    :cond_2
    return-void
.end method

.method static synthetic a(Laex;)Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Laex;->c:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Laex;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laex;->c:Lcom/twitter/model/core/Tweet;

    iget-boolean v0, v0, Lcom/twitter/model/core/Tweet;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()V
    .locals 9

    .prologue
    .line 133
    iget-object v0, p0, Laex;->c:Lcom/twitter/model/core/Tweet;

    if-nez v0, :cond_0

    .line 147
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v0, p0, Laex;->d:Lcom/twitter/android/periscope/q;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/q;->d()V

    .line 137
    new-instance v1, Lbhd;

    iget-object v2, p0, Laex;->a:Landroid/content/Context;

    iget-object v3, p0, Laex;->b:Lcom/twitter/library/client/Session;

    iget-object v0, p0, Laex;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v0, Lcom/twitter/model/core/Tweet;->G:J

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Lbhd;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJLcgi;)V

    .line 138
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    new-instance v2, Laex$3;

    iget-object v3, p0, Laex;->f:Ltv/periscope/android/ui/broadcast/an;

    invoke-direct {v2, p0, v3}, Laex$3;-><init>(Laex;Ltv/periscope/android/ui/broadcast/an;)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    goto :goto_0
.end method

.method private i()V
    .locals 7

    .prologue
    .line 150
    iget-object v0, p0, Laex;->c:Lcom/twitter/model/core/Tweet;

    if-nez v0, :cond_0

    .line 163
    :goto_0
    return-void

    .line 153
    :cond_0
    new-instance v1, Lbhg;

    iget-object v2, p0, Laex;->a:Landroid/content/Context;

    iget-object v3, p0, Laex;->b:Lcom/twitter/library/client/Session;

    iget-object v0, p0, Laex;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v0, Lcom/twitter/model/core/Tweet;->G:J

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lbhg;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JZ)V

    .line 154
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    new-instance v2, Laex$4;

    iget-object v3, p0, Laex;->f:Ltv/periscope/android/ui/broadcast/an;

    invoke-direct {v2, p0, v3}, Laex$4;-><init>(Laex;Ltv/periscope/android/ui/broadcast/an;)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public a()Ltv/periscope/android/ui/chat/ak;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Laex;->e:Ltv/periscope/android/ui/chat/ak;

    return-object v0
.end method

.method public a(Ltv/periscope/android/ui/broadcast/an$a;)V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Laex;->g:Ljava/util/Set;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 73
    return-void
.end method

.method public b()Ltv/periscope/android/ui/broadcast/an;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Laex;->f:Ltv/periscope/android/ui/broadcast/an;

    return-object v0
.end method

.method public b(Ltv/periscope/android/ui/broadcast/an$a;)V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Laex;->g:Ljava/util/Set;

    new-instance v1, Laex$1;

    invoke-direct {v1, p0, p1}, Laex$1;-><init>(Laex;Ltv/periscope/android/ui/broadcast/an$a;)V

    .line 77
    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Lcpv;)Ljava/util/List;

    move-result-object v0

    .line 83
    iget-object v1, p0, Laex;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 84
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Laex;->g:Ljava/util/Set;

    new-instance v1, Laex$2;

    invoke-direct {v1, p0}, Laex$2;-><init>(Laex;)V

    .line 88
    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Lcpv;)Ljava/util/List;

    move-result-object v0

    .line 94
    iget-object v1, p0, Laex;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 95
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 99
    invoke-direct {p0}, Laex;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 100
    invoke-direct {p0}, Laex;->h()V

    .line 101
    iget-object v0, p0, Laex;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 102
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/an$a;

    .line 103
    if-eqz v0, :cond_0

    .line 104
    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/an$a;->d()V

    goto :goto_0

    .line 108
    :cond_1
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 112
    invoke-direct {p0}, Laex;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    invoke-direct {p0}, Laex;->i()V

    .line 114
    iget-object v0, p0, Laex;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 115
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/ui/broadcast/an$a;

    .line 116
    if-eqz v0, :cond_0

    .line 117
    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/an$a;->e()V

    goto :goto_0

    .line 121
    :cond_1
    return-void
.end method

.method public f()Z
    .locals 4

    .prologue
    .line 125
    iget-object v0, p0, Laex;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Laex;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Laex;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->b:J

    iget-object v2, p0, Laex;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Laex;->c:Lcom/twitter/model/core/Tweet;

    iget-boolean v0, v0, Lcom/twitter/model/core/Tweet;->F:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
