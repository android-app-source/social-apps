.class public Laeh$a;
.super Laef$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Laeh;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public final a:Landroid/widget/TextView;

.field public final b:Lcml;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcml",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcml;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcml",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcml;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcml",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcml;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcml",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Landroid/view/View;

.field public final g:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 182
    invoke-direct {p0, p1}, Laef$a;-><init>(Landroid/view/View;)V

    .line 183
    const v0, 0x7f1303ae

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laeh$a;->a:Landroid/widget/TextView;

    .line 184
    const v0, 0x7f1303ad

    .line 185
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 186
    new-instance v1, Lcml;

    invoke-direct {v1, v0}, Lcml;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Laeh$a;->b:Lcml;

    .line 187
    const v0, 0x7f1303af

    .line 188
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 189
    new-instance v1, Lcml;

    invoke-direct {v1, v0}, Lcml;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Laeh$a;->c:Lcml;

    .line 190
    const v0, 0x7f1303a9

    .line 191
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 192
    new-instance v1, Lcml;

    invoke-direct {v1, v0}, Lcml;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Laeh$a;->d:Lcml;

    .line 193
    const v0, 0x7f1303ab

    .line 194
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 195
    new-instance v1, Lcml;

    invoke-direct {v1, v0}, Lcml;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Laeh$a;->e:Lcml;

    .line 196
    const v0, 0x7f1303aa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Laeh$a;->f:Landroid/view/View;

    .line 197
    const v0, 0x7f1300fa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Laeh$a;->g:Landroid/widget/ImageView;

    .line 198
    return-void
.end method
