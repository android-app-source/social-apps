.class Lbrm$1;
.super Lcom/twitter/library/service/w;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbrm;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/w",
        "<",
        "Ljava/lang/Void;",
        "Lcom/twitter/async/service/AsyncOperation",
        "<",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcdu;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/util/collection/h;

.field final synthetic b:Lbrm;


# direct methods
.method constructor <init>(Lbrm;Lcom/twitter/util/collection/h;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lbrm$1;->b:Lbrm;

    iput-object p2, p0, Lbrm$1;->a:Lcom/twitter/util/collection/h;

    invoke-direct {p0}, Lcom/twitter/library/service/w;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/AsyncOperation",
            "<",
            "Ljava/lang/Void;",
            "Ljava/util/List",
            "<",
            "Lcdu;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 77
    invoke-virtual {p1}, Lcom/twitter/async/service/AsyncOperation;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 79
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    iget-object v1, p0, Lbrm$1;->b:Lbrm;

    iget-object v0, p0, Lbrm$1;->a:Lcom/twitter/util/collection/h;

    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1, v0, v2}, Lbrm;->a(Lbrm;Ljava/util/List;Ljava/util/Map;)V

    .line 94
    :goto_0
    return-void

    .line 82
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 83
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    .line 84
    iget-object v3, p0, Lbrm$1;->a:Lcom/twitter/util/collection/h;

    iget-wide v4, v0, Lcdu;->h:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/twitter/util/collection/h;->d(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 85
    iget-wide v4, v0, Lcdu;->h:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 88
    :cond_1
    iget-object v0, p0, Lbrm$1;->a:Lcom/twitter/util/collection/h;

    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->h()Z

    move-result v0

    if-nez v0, :cond_2

    .line 89
    iget-object v2, p0, Lbrm$1;->b:Lbrm;

    iget-object v0, p0, Lbrm$1;->a:Lcom/twitter/util/collection/h;

    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v2, v0, v1}, Lbrm;->a(Lbrm;Ljava/util/List;Ljava/util/Map;)V

    goto :goto_0

    .line 91
    :cond_2
    iget-object v0, p0, Lbrm$1;->b:Lbrm;

    invoke-static {v0, v1}, Lbrm;->a(Lbrm;Ljava/util/Map;)V

    goto :goto_0
.end method
