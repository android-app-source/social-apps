.class public final Lchx$b$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lchx$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lchx$b;",
        ">;"
    }
.end annotation


# instance fields
.field a:J

.field b:J

.field c:I

.field d:Ljava/lang/String;

.field e:I

.field f:Z

.field g:Z

.field h:Z

.field i:Lcgu;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method public static a(Lchx$b;)Lchx$b$a;
    .locals 4

    .prologue
    .line 116
    new-instance v0, Lchx$b$a;

    invoke-direct {v0}, Lchx$b$a;-><init>()V

    iget-wide v2, p0, Lchx$b;->a:J

    .line 117
    invoke-virtual {v0, v2, v3}, Lchx$b$a;->a(J)Lchx$b$a;

    move-result-object v0

    iget-wide v2, p0, Lchx$b;->b:J

    .line 118
    invoke-virtual {v0, v2, v3}, Lchx$b$a;->b(J)Lchx$b$a;

    move-result-object v0

    iget v1, p0, Lchx$b;->c:I

    .line 119
    invoke-virtual {v0, v1}, Lchx$b$a;->a(I)Lchx$b$a;

    move-result-object v0

    iget-object v1, p0, Lchx$b;->d:Ljava/lang/String;

    .line 120
    invoke-virtual {v0, v1}, Lchx$b$a;->a(Ljava/lang/String;)Lchx$b$a;

    move-result-object v0

    iget v1, p0, Lchx$b;->e:I

    .line 121
    invoke-virtual {v0, v1}, Lchx$b$a;->b(I)Lchx$b$a;

    move-result-object v0

    iget-boolean v1, p0, Lchx$b;->f:Z

    .line 122
    invoke-virtual {v0, v1}, Lchx$b$a;->a(Z)Lchx$b$a;

    move-result-object v0

    iget-boolean v1, p0, Lchx$b;->g:Z

    .line 123
    invoke-virtual {v0, v1}, Lchx$b$a;->b(Z)Lchx$b$a;

    move-result-object v0

    iget-boolean v1, p0, Lchx$b;->h:Z

    .line 124
    invoke-virtual {v0, v1}, Lchx$b$a;->c(Z)Lchx$b$a;

    move-result-object v0

    iget-object v1, p0, Lchx$b;->i:Lcgu;

    .line 125
    invoke-virtual {v0, v1}, Lchx$b$a;->a(Lcgu;)Lchx$b$a;

    move-result-object v0

    .line 116
    return-object v0
.end method


# virtual methods
.method public a(I)Lchx$b$a;
    .locals 0

    .prologue
    .line 142
    iput p1, p0, Lchx$b$a;->c:I

    .line 143
    return-object p0
.end method

.method public a(J)Lchx$b$a;
    .locals 1

    .prologue
    .line 130
    iput-wide p1, p0, Lchx$b$a;->a:J

    .line 131
    return-object p0
.end method

.method public a(Lcgu;)Lchx$b$a;
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lchx$b$a;->i:Lcgu;

    .line 179
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lchx$b$a;
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lchx$b$a;->d:Ljava/lang/String;

    .line 173
    return-object p0
.end method

.method public a(Z)Lchx$b$a;
    .locals 0

    .prologue
    .line 154
    iput-boolean p1, p0, Lchx$b$a;->f:Z

    .line 155
    return-object p0
.end method

.method public b(I)Lchx$b$a;
    .locals 0

    .prologue
    .line 148
    iput p1, p0, Lchx$b$a;->e:I

    .line 149
    return-object p0
.end method

.method public b(J)Lchx$b$a;
    .locals 1

    .prologue
    .line 136
    iput-wide p1, p0, Lchx$b$a;->b:J

    .line 137
    return-object p0
.end method

.method public b(Z)Lchx$b$a;
    .locals 0

    .prologue
    .line 160
    iput-boolean p1, p0, Lchx$b$a;->g:Z

    .line 161
    return-object p0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Lchx$b$a;->e()Lchx$b;

    move-result-object v0

    return-object v0
.end method

.method public c(Z)Lchx$b$a;
    .locals 0

    .prologue
    .line 166
    iput-boolean p1, p0, Lchx$b$a;->h:Z

    .line 167
    return-object p0
.end method

.method public e()Lchx$b;
    .locals 2

    .prologue
    .line 185
    new-instance v0, Lchx$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lchx$b;-><init>(Lchx$b$a;Lchx$1;)V

    return-object v0
.end method
