.class public abstract Lcoh;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Ljava/lang/String;

.field private static b:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    const-wide/16 v0, 0x0

    sput-wide v0, Lcoh;->b:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()J
    .locals 4

    .prologue
    .line 49
    sget-wide v0, Lcoh;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 50
    invoke-static {}, Lcoh;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcoh;->a(Ljava/lang/String;)J

    move-result-wide v0

    sput-wide v0, Lcoh;->b:J

    .line 51
    const-class v0, Lcoh;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 53
    :cond_0
    sget-wide v0, Lcoh;->b:J

    return-wide v0
.end method

.method static a(Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 33
    invoke-static {p0}, Lcom/twitter/util/h;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0xe

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 38
    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    const-wide v2, 0x1fffffffffffffL

    and-long/2addr v0, v2

    return-wide v0
.end method

.method public static b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 58
    sget-object v0, Lcoh;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->b()Lcqs;

    move-result-object v1

    .line 60
    const-string/jumbo v0, "client_uuid"

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcqs;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    if-nez v0, :cond_0

    .line 63
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 65
    invoke-interface {v1}, Lcqs;->a()Lcqs$b;

    move-result-object v1

    const-string/jumbo v2, "client_uuid"

    .line 66
    invoke-interface {v1, v2, v0}, Lcqs$b;->a(Ljava/lang/String;Ljava/lang/String;)Lcqs$b;

    move-result-object v1

    .line 67
    invoke-interface {v1}, Lcqs$b;->a()V

    .line 70
    :cond_0
    sput-object v0, Lcoh;->a:Ljava/lang/String;

    .line 71
    const-class v0, Lcoh;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 74
    :cond_1
    sget-object v0, Lcoh;->a:Ljava/lang/String;

    return-object v0
.end method
