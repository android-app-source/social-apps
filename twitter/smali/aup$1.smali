.class Laup$1;
.super Lcqy;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Laup;->a(Lrx/i;)Lrx/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqy",
        "<TC;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lrx/i;

.field final synthetic b:Laup;

.field private c:Ljava/io/Closeable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TC;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Laup;Lrx/i;Lrx/i;)V
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Laup$1;->b:Laup;

    iput-object p3, p0, Laup$1;->a:Lrx/i;

    invoke-direct {p0, p2}, Lcqy;-><init>(Lrx/i;)V

    return-void
.end method

.method static synthetic a(Laup$1;Ljava/io/Closeable;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Laup$1;->b(Ljava/io/Closeable;)V

    return-void
.end method

.method private b(Ljava/io/Closeable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;)V"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Laup$1;->b:Laup;

    iget-object v1, v0, Laup;->a:Lauq;

    monitor-enter v1

    .line 67
    :try_start_0
    iget-object v0, p0, Laup$1;->c:Ljava/io/Closeable;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Laup$1;->b:Laup;

    iget-object v0, v0, Laup;->a:Lauq;

    iget-object v2, p0, Laup$1;->c:Ljava/io/Closeable;

    invoke-virtual {v0, v2}, Lauq;->b(Ljava/io/Closeable;)V

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Laup$1;->c:Ljava/io/Closeable;

    .line 71
    :cond_0
    if-eqz p1, :cond_1

    .line 72
    iget-object v0, p0, Laup$1;->b:Laup;

    iget-object v0, v0, Laup;->a:Lauq;

    invoke-virtual {v0}, Lauq;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 73
    invoke-static {p1}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 78
    :cond_1
    :goto_0
    monitor-exit v1

    .line 79
    return-void

    .line 75
    :cond_2
    iput-object p1, p0, Laup$1;->c:Ljava/io/Closeable;

    goto :goto_0

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Ljava/io/Closeable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;)V"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Laup$1;->b:Laup;

    iget-object v0, v0, Laup;->a:Lauq;

    invoke-virtual {v0}, Lauq;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Laup$1;->a:Lrx/i;

    invoke-virtual {v0}, Lrx/i;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    iget-object v0, p0, Laup$1;->a:Lrx/i;

    invoke-virtual {v0, p1}, Lrx/i;->a(Ljava/lang/Object;)V

    .line 49
    :cond_0
    invoke-direct {p0, p1}, Laup$1;->b(Ljava/io/Closeable;)V

    .line 50
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Ljava/io/Closeable;

    invoke-virtual {p0, p1}, Laup$1;->a(Ljava/io/Closeable;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Laup$1;->a:Lrx/i;

    invoke-virtual {v0}, Lrx/i;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    iget-object v0, p0, Laup$1;->a:Lrx/i;

    invoke-virtual {v0, p1}, Lrx/i;->a(Ljava/lang/Throwable;)V

    .line 42
    :cond_0
    return-void
.end method

.method public by_()V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Laup$1;->a:Lrx/i;

    invoke-virtual {v0}, Lrx/i;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 33
    iget-object v0, p0, Laup$1;->a:Lrx/i;

    invoke-virtual {v0}, Lrx/i;->by_()V

    .line 35
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 54
    invoke-super {p0}, Lcqy;->c()V

    .line 57
    new-instance v0, Laup$1$1;

    invoke-direct {v0, p0}, Laup$1$1;-><init>(Laup$1;)V

    invoke-static {v0}, Lcwy;->a(Lrx/functions/a;)Lrx/j;

    move-result-object v0

    invoke-virtual {p0, v0}, Laup$1;->a(Lrx/j;)V

    .line 63
    return-void
.end method
