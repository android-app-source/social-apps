.class public Lcmx;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcmm;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:I

.field private final d:I

.field private e:Lcnb;

.field private f:Ljava/lang/CharSequence;

.field private g:Landroid/content/Intent;

.field private h:Landroid/view/View;

.field private i:Landroid/view/View;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcmm;",
            ">;"
        }
    .end annotation
.end field

.field private k:Z

.field private l:Z

.field private m:I

.field private n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;III)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcmx;->a:Landroid/content/Context;

    .line 38
    iput p2, p0, Lcmx;->b:I

    .line 39
    iput p3, p0, Lcmx;->c:I

    .line 40
    iput p4, p0, Lcmx;->d:I

    .line 41
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcmx;->b:I

    return v0
.end method

.method public a(I)Lcmx;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 117
    iget-object v0, p0, Lcmx;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcmx;->a(Ljava/lang/CharSequence;)Lcmx;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Intent;)Lcmx;
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcmx;->g:Landroid/content/Intent;

    .line 68
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Lcmx;
    .locals 1

    .prologue
    .line 122
    iput-object p1, p0, Lcmx;->f:Ljava/lang/CharSequence;

    .line 123
    iget-object v0, p0, Lcmx;->e:Lcnb;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcmx;->e:Lcnb;

    invoke-interface {v0}, Lcnb;->a()V

    .line 126
    :cond_0
    return-object p0
.end method

.method public a(Z)Lcmx;
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcmx;->k:Z

    if-eq v0, p1, :cond_0

    .line 51
    iput-boolean p1, p0, Lcmx;->k:Z

    .line 52
    iget-object v0, p0, Lcmx;->e:Lcnb;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcmx;->e:Lcnb;

    invoke-interface {v0}, Lcnb;->a()V

    .line 56
    :cond_0
    return-object p0
.end method

.method public a(Lcnb;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcmx;->e:Lcnb;

    .line 45
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcmm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 186
    iput-object p1, p0, Lcmx;->j:Ljava/util/List;

    .line 187
    iget-object v0, p0, Lcmx;->e:Lcnb;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcmx;->e:Lcnb;

    invoke-interface {v0}, Lcnb;->a()V

    .line 190
    :cond_0
    return-void
.end method

.method public a(Lcmx;)Z
    .locals 2

    .prologue
    .line 238
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcmx;->f:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcmx;->f:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcmx;->m:I

    iget v1, p1, Lcmx;->m:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)Lcmx;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 137
    iget v0, p0, Lcmx;->m:I

    if-eq v0, p1, :cond_0

    .line 138
    iput p1, p0, Lcmx;->m:I

    .line 139
    iget-object v0, p0, Lcmx;->e:Lcnb;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcmx;->e:Lcnb;

    invoke-interface {v0}, Lcnb;->a()V

    .line 143
    :cond_0
    return-object p0
.end method

.method public b(Z)Lcmx;
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lcmx;->l:Z

    if-eq v0, p1, :cond_0

    .line 101
    iput-boolean p1, p0, Lcmx;->l:Z

    .line 102
    iget-object v0, p0, Lcmx;->e:Lcnb;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcmx;->e:Lcnb;

    invoke-interface {v0}, Lcnb;->a()V

    .line 106
    :cond_0
    return-object p0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcmx;->k:Z

    return v0
.end method

.method public c()Landroid/view/View;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcmx;->i:Landroid/view/View;

    return-object v0
.end method

.method public c(I)Lcmx;
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 153
    if-eqz p1, :cond_0

    .line 154
    iget-object v0, p0, Lcmx;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcmx;->i:Landroid/view/View;

    .line 155
    iget-object v0, p0, Lcmx;->i:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {p0, v2}, Lcmx;->b(I)Lcmx;

    .line 160
    :cond_0
    return-object p0
.end method

.method public c(Z)Lcmx;
    .locals 1

    .prologue
    .line 200
    iput-boolean p1, p0, Lcmx;->n:Z

    .line 201
    iget-object v0, p0, Lcmx;->e:Lcnb;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcmx;->e:Lcnb;

    invoke-interface {v0}, Lcnb;->a()V

    .line 204
    :cond_0
    return-object p0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcmx;->c:I

    return v0
.end method

.method public d(I)Lcmx;
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 172
    if-eqz p1, :cond_0

    .line 173
    iget-object v0, p0, Lcmx;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcmx;->h:Landroid/view/View;

    .line 175
    :cond_0
    return-object p0
.end method

.method public e()Landroid/view/View;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcmx;->h:Landroid/view/View;

    return-object v0
.end method

.method public synthetic e(Z)Lcmm;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcmx;->b(Z)Lcmx;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 213
    if-ne p0, p1, :cond_1

    .line 221
    :cond_0
    :goto_0
    return v0

    .line 216
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 217
    goto :goto_0

    .line 220
    :cond_3
    check-cast p1, Lcmx;

    .line 221
    iget v2, p0, Lcmx;->b:I

    iget v3, p1, Lcmx;->b:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcmx;->c:I

    iget v3, p1, Lcmx;->c:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcmx;->d:I

    iget v3, p1, Lcmx;->d:I

    if-ne v2, v3, :cond_4

    .line 224
    invoke-virtual {p0, p1}, Lcmx;->a(Lcmx;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcmx;->g:Landroid/content/Intent;

    iget-object v3, p1, Lcmx;->g:Landroid/content/Intent;

    .line 225
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcmx;->e:Lcnb;

    iget-object v3, p1, Lcmx;->e:Lcnb;

    .line 226
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcmx;->k:Z

    iget-boolean v3, p1, Lcmx;->k:Z

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcmx;->l:Z

    iget-boolean v3, p1, Lcmx;->l:Z

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcmx;->d:I

    return v0
.end method

.method public synthetic f(Z)Lcmm;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcmx;->a(Z)Lcmx;

    move-result-object v0

    return-object v0
.end method

.method public synthetic g(I)Lcmm;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcmx;->a(I)Lcmx;

    move-result-object v0

    return-object v0
.end method

.method public g()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcmx;->f:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcmx;->m:I

    return v0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    .line 233
    iget v0, p0, Lcmx;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p0, Lcmx;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p0, Lcmx;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcmx;->f:Ljava/lang/CharSequence;

    iget v4, p0, Lcmx;->m:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget v5, p0, Lcmx;->m:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, Lcmx;->e:Lcnb;

    iget-boolean v7, p0, Lcmx;->k:Z

    .line 234
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iget-boolean v8, p0, Lcmx;->l:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 233
    invoke-static/range {v0 .. v8}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public i()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcmm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 195
    iget-object v0, p0, Lcmx;->j:Ljava/util/List;

    return-object v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 208
    iget-boolean v0, p0, Lcmx;->n:Z

    return v0
.end method

.method public n()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcmx;->g:Landroid/content/Intent;

    return-object v0
.end method
