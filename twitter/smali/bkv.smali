.class public Lbkv;
.super Lbkq;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbkv$a;
    }
.end annotation


# instance fields
.field private final b:Lcom/twitter/model/livevideo/a;

.field private final c:J

.field private final d:Lbkv$a;

.field private final e:Z


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/model/livevideo/a;JLbkv$a;)V
    .locals 9

    .prologue
    .line 53
    const-string/jumbo v0, "live_video_use_live_stream_acquisition_android_enabled"

    const/4 v1, 0x0

    .line 54
    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;Z)Z

    move-result v7

    sget-object v8, Lcom/twitter/library/av/model/parser/c;->a:Lcom/twitter/library/av/model/parser/c;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    .line 53
    invoke-direct/range {v1 .. v8}, Lbkv;-><init>(Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/model/livevideo/a;JLbkv$a;ZLcom/twitter/library/av/model/parser/c;)V

    .line 56
    return-void
.end method

.method constructor <init>(Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/model/livevideo/a;JLbkv$a;ZLcom/twitter/library/av/model/parser/c;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p7}, Lbkq;-><init>(Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/library/av/model/parser/c;)V

    .line 64
    iput-object p2, p0, Lbkv;->b:Lcom/twitter/model/livevideo/a;

    .line 65
    iput-object p5, p0, Lbkv;->d:Lbkv$a;

    .line 66
    iput-boolean p6, p0, Lbkv;->e:Z

    .line 67
    iput-wide p3, p0, Lbkv;->c:J

    .line 68
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/twitter/library/av/g;)Lcom/twitter/model/av/AVMediaPlaylist;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 74
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v1

    invoke-virtual {v1}, Lcrr;->e()Lcom/twitter/util/network/c;

    move-result-object v1

    .line 75
    invoke-virtual {p0, p2}, Lbkv;->a(Lcom/twitter/library/av/g;)Lcom/twitter/model/av/DynamicAdInfo;

    move-result-object v8

    .line 76
    invoke-static {v8}, Lcom/twitter/model/av/f;->a(Lcom/twitter/model/av/DynamicAdInfo;)Lcom/twitter/model/av/DynamicAd;

    move-result-object v2

    .line 79
    if-eqz v2, :cond_1

    .line 80
    invoke-virtual {v2}, Lcom/twitter/model/av/DynamicAd;->c()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v1, v3}, Lbkv;->a(Lcom/twitter/util/network/c;Ljava/util/List;)Lcom/twitter/util/collection/k;

    move-result-object v3

    .line 81
    invoke-virtual {v3}, Lcom/twitter/util/collection/k;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/twitter/util/collection/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/twitter/model/av/DynamicAd;->a(Ljava/lang/String;)Lcom/twitter/model/av/Video;

    move-result-object v0

    :cond_0
    move-object v6, v0

    .line 86
    :goto_0
    iget-boolean v0, p0, Lbkv;->e:Z

    if-eqz v0, :cond_3

    .line 87
    iget-object v0, p0, Lbkv;->d:Lbkv$a;

    iget-object v2, p0, Lbkv;->b:Lcom/twitter/model/livevideo/a;

    iget-wide v2, v2, Lcom/twitter/model/livevideo/a;->b:J

    invoke-interface {v0, v2, v3, p1}, Lbkv$a;->a(JLandroid/content/Context;)Lcom/twitter/model/av/g;

    move-result-object v3

    .line 89
    if-nez v3, :cond_2

    .line 90
    iget-object v0, p0, Lbkv;->d:Lbkv$a;

    invoke-interface {v0}, Lbkv$a;->a()Lbdc$a;

    move-result-object v2

    .line 91
    new-instance v0, Lcom/twitter/model/av/LiveVideoPlaylist;

    iget-object v1, v1, Lcom/twitter/util/network/c;->b:Ljava/lang/String;

    iget v3, v2, Lbdc$a;->b:I

    iget v4, v2, Lbdc$a;->b:I

    iget-object v2, v2, Lbdc$a;->c:Ljava/lang/String;

    .line 92
    invoke-static {v4, v2}, Lbkv;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v3, v2}, Lcom/twitter/model/av/LiveVideoPlaylist;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 99
    :goto_1
    return-object v0

    :cond_1
    move-object v6, v0

    .line 83
    goto :goto_0

    .line 94
    :cond_2
    new-instance v7, Lcom/twitter/model/av/LiveVideoPlaylist;

    iget-object v9, v1, Lcom/twitter/util/network/c;->b:Ljava/lang/String;

    new-instance v0, Lcom/twitter/model/av/LiveVideoMedia;

    iget-object v1, p0, Lbkv;->b:Lcom/twitter/model/livevideo/a;

    iget-wide v1, v1, Lcom/twitter/model/livevideo/a;->b:J

    .line 95
    invoke-virtual {v3}, Lcom/twitter/model/av/g;->a()Ljava/lang/String;

    move-result-object v3

    iget-wide v4, p0, Lbkv;->c:J

    invoke-direct/range {v0 .. v5}, Lcom/twitter/model/av/LiveVideoMedia;-><init>(JLjava/lang/String;J)V

    invoke-direct {v7, v9, v0, v6, v8}, Lcom/twitter/model/av/LiveVideoPlaylist;-><init>(Ljava/lang/String;Lcom/twitter/model/av/AVMedia;Lcom/twitter/model/av/AVMedia;Lcom/twitter/model/av/DynamicAdInfo;)V

    move-object v0, v7

    .line 94
    goto :goto_1

    .line 99
    :cond_3
    new-instance v7, Lcom/twitter/model/av/LiveVideoPlaylist;

    iget-object v9, v1, Lcom/twitter/util/network/c;->b:Ljava/lang/String;

    new-instance v0, Lcom/twitter/model/av/LiveVideoMedia;

    iget-object v1, p0, Lbkv;->b:Lcom/twitter/model/livevideo/a;

    iget-wide v1, v1, Lcom/twitter/model/livevideo/a;->b:J

    iget-object v3, p0, Lbkv;->b:Lcom/twitter/model/livevideo/a;

    iget-object v3, v3, Lcom/twitter/model/livevideo/a;->c:Ljava/lang/String;

    iget-wide v4, p0, Lbkv;->c:J

    invoke-direct/range {v0 .. v5}, Lcom/twitter/model/av/LiveVideoMedia;-><init>(JLjava/lang/String;J)V

    invoke-direct {v7, v9, v0, v6, v8}, Lcom/twitter/model/av/LiveVideoPlaylist;-><init>(Ljava/lang/String;Lcom/twitter/model/av/AVMedia;Lcom/twitter/model/av/AVMedia;Lcom/twitter/model/av/DynamicAdInfo;)V

    move-object v0, v7

    goto :goto_1
.end method

.method protected a(Lcom/twitter/library/av/g;)Lcom/twitter/model/av/DynamicAdInfo;
    .locals 1

    .prologue
    .line 107
    invoke-static {}, Lcom/twitter/library/av/v;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lbkv;->a:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-virtual {p1, v0}, Lcom/twitter/library/av/g;->a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/model/av/DynamicAdInfo;

    move-result-object v0

    .line 111
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/content/Context;)Lcom/twitter/network/j;
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Landroid/content/Context;Ljava/util/Map;Lcom/twitter/util/network/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/twitter/util/network/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 147
    return-void
.end method

.method protected a(Landroid/net/Uri$Builder;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri$Builder;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 134
    return-void
.end method

.method protected b(Lcom/twitter/network/j;Lcom/twitter/network/HttpOperation;Ljava/util/Map;Lcom/twitter/model/av/DynamicAdInfo;)Lcom/twitter/model/av/AVMediaPlaylist;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/j;",
            "Lcom/twitter/network/HttpOperation;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/twitter/model/av/DynamicAdInfo;",
            ")",
            "Lcom/twitter/model/av/AVMediaPlaylist;"
        }
    .end annotation

    .prologue
    .line 118
    const/4 v0, 0x0

    return-object v0
.end method
