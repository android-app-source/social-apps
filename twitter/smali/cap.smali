.class public Lcap;
.super Lcac;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcap$a;
    }
.end annotation


# instance fields
.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:Lbzw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbzw",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field public final h:I

.field public final i:I

.field public final j:Lbzw;

.field public final k:I

.field public final l:I

.field public final m:Lbzw;


# direct methods
.method private constructor <init>(Lcap$a;)V
    .locals 8

    .prologue
    .line 37
    invoke-static {p1}, Lcap$a;->a(Lcap$a;)J

    move-result-wide v2

    invoke-static {p1}, Lcap$a;->b(Lcap$a;)J

    move-result-wide v4

    invoke-static {p1}, Lcap$a;->c(Lcap$a;)J

    move-result-wide v6

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcac;-><init>(JJJ)V

    .line 38
    invoke-static {p1}, Lcap$a;->d(Lcap$a;)I

    move-result v0

    iput v0, p0, Lcap;->d:I

    .line 39
    invoke-static {p1}, Lcap$a;->e(Lcap$a;)I

    move-result v0

    iput v0, p0, Lcap;->e:I

    .line 40
    invoke-static {p1}, Lcap$a;->f(Lcap$a;)I

    move-result v0

    iput v0, p0, Lcap;->f:I

    .line 41
    invoke-static {p1}, Lcap$a;->g(Lcap$a;)I

    move-result v0

    iput v0, p0, Lcap;->h:I

    .line 42
    invoke-static {p1}, Lcap$a;->h(Lcap$a;)I

    move-result v0

    iput v0, p0, Lcap;->i:I

    .line 43
    invoke-static {p1}, Lcap$a;->i(Lcap$a;)I

    move-result v0

    iput v0, p0, Lcap;->k:I

    .line 44
    invoke-static {p1}, Lcap$a;->j(Lcap$a;)I

    move-result v0

    iput v0, p0, Lcap;->l:I

    .line 45
    invoke-static {p1}, Lcap$a;->k(Lcap$a;)Lbzw;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzw;

    iput-object v0, p0, Lcap;->g:Lbzw;

    .line 46
    invoke-static {p1}, Lcap$a;->l(Lcap$a;)Lbzw;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzw;

    iput-object v0, p0, Lcap;->j:Lbzw;

    .line 47
    invoke-static {p1}, Lcap$a;->m(Lcap$a;)Lbzw;

    move-result-object v0

    iput-object v0, p0, Lcap;->m:Lbzw;

    .line 48
    return-void
.end method

.method synthetic constructor <init>(Lcap$a;Lcap$1;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcap;-><init>(Lcap$a;)V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 98
    iget-wide v0, p0, Lcap;->b:J

    return-wide v0
.end method

.method public a(Lcap;)Z
    .locals 4

    .prologue
    .line 56
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    iget-wide v0, p0, Lcap;->a:J

    iget-wide v2, p1, Lcap;->a:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget v0, p0, Lcap;->d:I

    iget v1, p1, Lcap;->d:I

    if-ne v0, v1, :cond_1

    iget-wide v0, p0, Lcap;->b:J

    iget-wide v2, p1, Lcap;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-wide v0, p0, Lcap;->c:J

    iget-wide v2, p1, Lcap;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget v0, p0, Lcap;->f:I

    iget v1, p1, Lcap;->f:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcap;->e:I

    iget v1, p1, Lcap;->e:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcap;->l:I

    iget v1, p1, Lcap;->l:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcap;->k:I

    iget v1, p1, Lcap;->k:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcap;->i:I

    iget v1, p1, Lcap;->i:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcap;->h:I

    iget v1, p1, Lcap;->h:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcap;->g:Lbzw;

    iget-object v1, p1, Lcap;->g:Lbzw;

    .line 67
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcap;->j:Lbzw;

    iget-object v1, p1, Lcap;->j:Lbzw;

    .line 68
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcap;->m:Lbzw;

    iget-object v1, p1, Lcap;->m:Lbzw;

    .line 69
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 56
    :goto_0
    return v0

    .line 69
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 119
    invoke-virtual {p0}, Lcap;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 103
    iget-wide v0, p0, Lcap;->b:J

    return-wide v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 108
    iget-wide v0, p0, Lcap;->b:J

    return-wide v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 113
    iget-wide v0, p0, Lcap;->c:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 52
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcap;

    if-eqz v0, :cond_1

    check-cast p1, Lcap;

    invoke-virtual {p0, p1}, Lcap;->a(Lcap;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, Lcap;->g:Lbzw;

    iget-object v0, v0, Lbzw;->a:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 74
    iget v0, p0, Lcap;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcap;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lcap;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-wide v4, p0, Lcap;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcap;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget v3, p0, Lcap;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcap;->g:Lbzw;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget v3, p0, Lcap;->h:I

    .line 75
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget v3, p0, Lcap;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-object v3, p0, Lcap;->j:Lbzw;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    iget v3, p0, Lcap;->k:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xa

    iget v3, p0, Lcap;->l:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xb

    iget-object v3, p0, Lcap;->m:Lbzw;

    aput-object v3, v1, v2

    .line 74
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;[Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "event="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcap;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", createdAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcap;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", maxPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcap;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", minPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcap;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", sourcesSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcap;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", sourceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcap;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", targetsSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcap;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", targetType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcap;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", targetObjectsSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcap;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", targetObjectType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcap;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
