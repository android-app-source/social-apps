.class public Lagr;
.super Lcom/twitter/android/moments/data/d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lagr$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/moments/data/d",
        "<",
        "Ljava/util/List",
        "<",
        "Laij;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/twitter/library/client/Session;


# direct methods
.method protected constructor <init>(Landroid/app/Activity;Landroid/support/v4/app/LoaderManager;ILcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p2, p3}, Lcom/twitter/android/moments/data/d;-><init>(Landroid/support/v4/app/LoaderManager;I)V

    .line 43
    iput-object p1, p0, Lagr;->a:Landroid/app/Activity;

    .line 44
    iput-object p4, p0, Lagr;->b:Lcom/twitter/library/client/Session;

    .line 45
    return-void
.end method

.method public constructor <init>(Lcom/twitter/app/common/base/BaseFragmentActivity;Lcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    .line 37
    invoke-virtual {p1}, Lcom/twitter/app/common/base/BaseFragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/16 v1, 0x125d

    invoke-direct {p0, p1, v0, v1, p2}, Lagr;-><init>(Landroid/app/Activity;Landroid/support/v4/app/LoaderManager;ILcom/twitter/library/client/Session;)V

    .line 38
    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Laij;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 51
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_0

    .line 52
    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 60
    :goto_0
    return-object v0

    .line 55
    :cond_0
    const/4 v1, 0x2

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 56
    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 58
    new-instance v4, Laij;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1, v2, v3}, Laij;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v4}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 59
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 60
    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    goto :goto_0
.end method

.method public synthetic c(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Lagr;->a(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    new-instance v0, Lcom/twitter/util/android/d;

    iget-object v1, p0, Lagr;->a:Landroid/app/Activity;

    sget-object v2, Lcom/twitter/database/schema/a$s;->a:Landroid/net/Uri;

    iget-object v3, p0, Lagr;->b:Lcom/twitter/library/client/Session;

    .line 67
    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 66
    invoke-static {v2, v4, v5}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lagr$a;->a:[Ljava/lang/String;

    const-string/jumbo v4, "type=? AND latitude IS NULL AND longitude IS NULL"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x6

    .line 69
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const-string/jumbo v6, "query_id DESC, time ASC"

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    return-object v0
.end method
