.class public Laee$a;
.super Laef$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Laee;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public final a:Landroid/widget/ImageView;

.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/view/ViewGroup;

.field public final e:[Landroid/view/View;

.field public final f:[Landroid/view/View;

.field public final g:Landroid/view/ViewGroup;

.field public final h:Landroid/view/View;

.field public final i:Lcom/twitter/media/ui/image/UserImageView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 196
    invoke-direct {p0, p1}, Laef$a;-><init>(Landroid/view/View;)V

    .line 197
    const v0, 0x7f130044

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Laee$a;->a:Landroid/widget/ImageView;

    .line 198
    const v0, 0x7f130151

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laee$a;->h:Landroid/view/View;

    .line 199
    const v0, 0x7f130150

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laee$a;->b:Landroid/widget/TextView;

    .line 200
    const v0, 0x7f130014

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Laee$a;->d:Landroid/view/ViewGroup;

    .line 201
    const v0, 0x7f130153

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Laee$a;->g:Landroid/view/ViewGroup;

    .line 202
    const v0, 0x7f1304bb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Laee$a;->i:Lcom/twitter/media/ui/image/UserImageView;

    .line 203
    const v0, 0x7f130154

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laee$a;->c:Landroid/widget/TextView;

    .line 205
    iget-object v0, p0, Laee$a;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Laee$a;->e:[Landroid/view/View;

    .line 206
    iget-object v0, p0, Laee$a;->e:[Landroid/view/View;

    array-length v0, v0

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Laee$a;->f:[Landroid/view/View;

    .line 209
    iget-object v0, p0, Laee$a;->g:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 210
    iget-object v0, p0, Laee$a;->g:Landroid/view/ViewGroup;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/twitter/util/ui/a;->a(Landroid/view/View;I)V

    .line 214
    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Laee$a;->e:[Landroid/view/View;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 215
    iget-object v1, p0, Laee$a;->e:[Landroid/view/View;

    iget-object v2, p0, Laee$a;->d:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v1, v0

    .line 216
    iget-object v1, p0, Laee$a;->f:[Landroid/view/View;

    iget-object v2, p0, Laee$a;->e:[Landroid/view/View;

    aget-object v2, v2, v0

    const v3, 0x7f1304ba

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v1, v0

    .line 214
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 218
    :cond_1
    return-void
.end method
