.class public Ladl;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcmp;


# instance fields
.field private final a:Lcom/twitter/ui/widget/b;

.field private final b:Lcmm;


# direct methods
.method public constructor <init>(Lcom/twitter/ui/widget/b;Lcmm;)V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Ladl;->a:Lcom/twitter/ui/widget/b;

    .line 18
    iget-object v0, p0, Ladl;->a:Lcom/twitter/ui/widget/b;

    instance-of v0, v0, Lcom/twitter/ui/widget/FullBadgeView;

    if-eqz v0, :cond_0

    .line 19
    iget-object v0, p0, Ladl;->a:Lcom/twitter/ui/widget/b;

    check-cast v0, Lcom/twitter/ui/widget/FullBadgeView;

    const/16 v1, 0x63

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/FullBadgeView;->setMaxBadgeCount(I)V

    .line 21
    :cond_0
    iput-object p2, p0, Ladl;->b:Lcmm;

    .line 22
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Ladl;->a:Lcom/twitter/ui/widget/b;

    invoke-interface {v0, p1}, Lcom/twitter/ui/widget/b;->setBadgeNumber(I)V

    .line 27
    iget-object v1, p0, Ladl;->b:Lcmm;

    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcmm;->f(Z)Lcmm;

    .line 28
    return-void

    .line 27
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
