.class public Lckr;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lckq;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcki;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Landroid/support/v4/util/ArrayMap;

    invoke-direct {v0}, Landroid/support/v4/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lckr;->a:Ljava/util/Map;

    .line 24
    return-void
.end method


# virtual methods
.method public a(Lcki;)Lcki;
    .locals 2
    .annotation build Landroid/support/annotation/RequiresPermission;
        value = "android.permission.SYSTEM_ALERT_WINDOW"
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lckr;->a:Ljava/util/Map;

    invoke-interface {p1}, Lcki;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    invoke-interface {p1}, Lcki;->b()Lcki;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcki;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lckr;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
