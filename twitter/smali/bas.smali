.class public Lbas;
.super Lbao;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbao",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcom/twitter/model/account/LoginResponse;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private h:Lcom/twitter/model/core/TwitterUser;

.field private i:[I

.field private j:Lcom/twitter/model/account/LoginResponse;

.field private final k:Ljava/lang/String;

.field private final l:[C

.field private final m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 42
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lbas;->a:I

    .line 43
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lbas;->b:I

    .line 47
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/16 v3, 0x20

    .line 49
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const/16 v3, 0xe5

    .line 50
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const/16 v3, 0xe7

    .line 51
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const/16 v3, 0xf4

    .line 52
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const/16 v3, 0x131

    .line 53
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const/16 v3, 0x10b

    .line 54
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 47
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lbas;->c:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 66
    invoke-direct {p0, p1, p2, p3}, Lbao;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 67
    iput-object p4, p0, Lbas;->k:Ljava/lang/String;

    .line 68
    invoke-virtual {p5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lbas;->l:[C

    .line 69
    invoke-static {}, Lcom/twitter/library/util/aa;->a()Lcom/twitter/library/util/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/util/aa;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbas;->m:Ljava/lang/String;

    .line 70
    invoke-super {p0}, Lbao;->v()Lcom/twitter/library/service/f;

    move-result-object v0

    .line 71
    if-eqz v0, :cond_0

    .line 72
    new-instance v1, Lcom/twitter/library/service/o;

    const/16 v2, 0x1f4

    sget v3, Lbas;->a:I

    sget v4, Lbas;->b:I

    const/16 v5, 0xa

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/twitter/library/service/o;-><init>(IIII)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    .line 77
    invoke-virtual {p0, v0}, Lbas;->a(Lcom/twitter/async/service/k;)Lcom/twitter/async/service/AsyncOperation;

    .line 79
    :cond_0
    return-void
.end method

.method private a(Lcom/twitter/async/service/j;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 150
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-direct {p0, v0}, Lbas;->d(Lcom/twitter/library/service/u;)Z

    move-result v0

    .line 153
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 155
    if-eqz v0, :cond_1

    .line 156
    const-string/jumbo v0, "success"

    .line 163
    :goto_0
    invoke-virtual {p0}, Lbas;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    if-nez v2, :cond_3

    const-wide/16 v2, 0x0

    .line 164
    :goto_1
    new-instance v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v4, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v5, "app:login::authenticate"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    .line 165
    invoke-virtual {v4, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    int-to-long v2, v1

    .line 166
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 168
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/service/u;

    invoke-virtual {v1}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v2

    .line 169
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/service/u;

    invoke-virtual {v1}, Lcom/twitter/library/service/u;->f()Lcom/twitter/network/HttpOperation;

    move-result-object v1

    .line 170
    if-eqz v2, :cond_0

    .line 171
    invoke-static {v0, v2}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/network/l;)V

    .line 172
    invoke-virtual {v1}, Lcom/twitter/network/HttpOperation;->i()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/model/ScribeLog;Ljava/lang/String;Lcom/twitter/network/l;)V

    .line 174
    :cond_0
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 175
    return-void

    .line 157
    :cond_1
    if-eqz p2, :cond_2

    .line 158
    const-string/jumbo v0, "retry"

    goto :goto_0

    .line 160
    :cond_2
    const-string/jumbo v0, "failure"

    goto :goto_0

    .line 163
    :cond_3
    invoke-virtual {p0}, Lbas;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/service/v;->c:J

    goto :goto_1
.end method

.method private d(Lcom/twitter/library/service/u;)Z
    .locals 2

    .prologue
    .line 185
    invoke-virtual {p1}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    .line 186
    invoke-virtual {p1}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 187
    invoke-virtual {p1}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v1

    iget v1, v1, Lcom/twitter/network/l;->j:I

    .line 188
    if-nez v0, :cond_0

    sget-object v0, Lbas;->c:Ljava/util/Set;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 190
    :cond_1
    :goto_0
    return v0

    .line 188
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private x()V
    .locals 3

    .prologue
    .line 195
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lbas;->l:[C

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 196
    iget-object v1, p0, Lbas;->l:[C

    const/16 v2, 0x2a

    aput-char v2, v1, v0

    .line 195
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 198
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/async/service/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 122
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lbas;->a(Lcom/twitter/async/service/j;Z)V

    .line 123
    return-void
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/account/LoginResponse;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 128
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 129
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/LoginResponse;

    .line 130
    iput-object v0, p0, Lbas;->j:Lcom/twitter/model/account/LoginResponse;

    .line 131
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/twitter/model/account/LoginResponse;->d:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 132
    new-instance v1, Lbbh;

    iget-object v2, p0, Lbas;->p:Landroid/content/Context;

    .line 133
    invoke-virtual {p0}, Lbas;->M()Lcom/twitter/library/service/v;

    move-result-object v3

    new-instance v4, Lcom/twitter/library/network/t;

    iget-object v0, v0, Lcom/twitter/model/account/LoginResponse;->a:Lcom/twitter/model/account/OAuthToken;

    invoke-direct {v4, v0}, Lcom/twitter/library/network/t;-><init>(Lcom/twitter/model/account/OAuthToken;)V

    invoke-direct {v1, v2, v3, v4}, Lbbh;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/library/network/t;)V

    .line 134
    invoke-virtual {v1}, Lbbh;->O()Lcom/twitter/library/service/u;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/library/service/u;->a(Lcom/twitter/library/service/u;)V

    .line 135
    invoke-virtual {v1}, Lbbh;->g()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    iput-object v0, p0, Lbas;->h:Lcom/twitter/model/core/TwitterUser;

    .line 138
    invoke-direct {p0}, Lbas;->x()V

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/z;

    .line 142
    invoke-static {v0}, Lcom/twitter/model/core/z;->a(Lcom/twitter/model/core/z;)[I

    move-result-object v0

    iput-object v0, p0, Lbas;->i:[I

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 40
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbas;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/service/d$a;
    .locals 4

    .prologue
    .line 84
    invoke-virtual {p0}, Lbas;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 85
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x0

    .line 86
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "Accept"

    const-string/jumbo v2, "application/json"

    .line 87
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "auth"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "1"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "xauth_password"

    aput-object v3, v1, v2

    .line 88
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "x_auth_identifier"

    iget-object v2, p0, Lbas;->k:Ljava/lang/String;

    .line 89
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "x_auth_password"

    iget-object v2, p0, Lbas;->l:[C

    .line 90
    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "send_error_codes"

    const-string/jumbo v2, "true"

    .line 91
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 92
    const-string/jumbo v1, "native_login_verification_enabled"

    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    const-string/jumbo v1, "x_auth_login_verification"

    const-string/jumbo v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 95
    :cond_0
    const-string/jumbo v1, "login_challenge_enabled"

    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 96
    const-string/jumbo v1, "x_auth_login_challenge"

    const-string/jumbo v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 98
    :cond_1
    iget-object v1, p0, Lbas;->p:Landroid/content/Context;

    invoke-static {v1}, Lbar;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 99
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 100
    const-string/jumbo v2, "kdt"

    invoke-virtual {v0, v2, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 103
    :cond_2
    iget-object v1, p0, Lbas;->m:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 104
    const-string/jumbo v1, "x_auth_country_code"

    iget-object v2, p0, Lbas;->m:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 106
    :cond_3
    return-object v0
.end method

.method public b(Lcom/twitter/async/service/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 117
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lbas;->a(Lcom/twitter/async/service/j;Z)V

    .line 118
    return-void
.end method

.method protected e()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/account/LoginResponse;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    const-class v0, Lcom/twitter/model/account/LoginResponse;

    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lbas;->e()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lbas;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Lcom/twitter/model/core/TwitterUser;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lbas;->h:Lcom/twitter/model/core/TwitterUser;

    return-object v0
.end method

.method public final s()[I
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lbas;->i:[I

    return-object v0
.end method

.method public final t()Lcom/twitter/model/account/LoginResponse;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lbas;->j:Lcom/twitter/model/account/LoginResponse;

    return-object v0
.end method
