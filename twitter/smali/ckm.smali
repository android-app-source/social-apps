.class public Lckm;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/ViewGroup;

.field private final c:Landroid/view/ViewGroup;

.field private final d:Landroid/view/View;

.field private e:Lckp;

.field private f:Lcom/twitter/ui/anim/AnimatableParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Lcom/twitter/ui/anim/AnimatableParams;

    invoke-direct {v0}, Lcom/twitter/ui/anim/AnimatableParams;-><init>()V

    iput-object v0, p0, Lckm;->f:Lcom/twitter/ui/anim/AnimatableParams;

    .line 40
    iput-object p1, p0, Lckm;->a:Landroid/content/Context;

    .line 42
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lckm;->b:Landroid/view/ViewGroup;

    .line 43
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lckm;->c:Landroid/view/ViewGroup;

    .line 44
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lckm;->d:Landroid/view/View;

    .line 45
    iget-object v0, p0, Lckm;->b:Landroid/view/ViewGroup;

    iget-object v1, p0, Lckm;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 46
    return-void
.end method

.method private a()Landroid/view/WindowManager$LayoutParams;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 113
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 114
    invoke-virtual {p0}, Lckm;->g()Lcom/twitter/ui/anim/AnimatableParams;

    move-result-object v1

    .line 115
    iget v2, v1, Lcom/twitter/ui/anim/AnimatableParams;->type:I

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 116
    iget v2, v1, Lcom/twitter/ui/anim/AnimatableParams;->format:I

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 117
    iget v2, v1, Lcom/twitter/ui/anim/AnimatableParams;->gravity:I

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 118
    iget v1, v1, Lcom/twitter/ui/anim/AnimatableParams;->flags:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 119
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 120
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 121
    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 122
    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 123
    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/WindowManager;)V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lckm;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lckm;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 133
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Dock already added to window manager."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_1
    iget-object v0, p0, Lckm;->b:Landroid/view/ViewGroup;

    invoke-direct {p0}, Lckm;->a()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 138
    iget-object v0, p0, Lckm;->b:Landroid/view/ViewGroup;

    iget-object v1, p0, Lckm;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 139
    iget-object v0, p0, Lckm;->d:Landroid/view/View;

    invoke-virtual {p0}, Lckm;->g()Lcom/twitter/ui/anim/AnimatableParams;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 140
    invoke-virtual {p0}, Lckm;->e()V

    .line 141
    invoke-virtual {p0}, Lckm;->f()V

    .line 142
    return-void
.end method

.method public a(Lckp;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lckm;->e:Lckp;

    .line 50
    return-void
.end method

.method public a(Lcom/twitter/ui/anim/AnimatableParams;)V
    .locals 2

    .prologue
    .line 97
    iput-object p1, p0, Lckm;->f:Lcom/twitter/ui/anim/AnimatableParams;

    .line 98
    iget-object v0, p0, Lckm;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 99
    iget v1, p1, Lcom/twitter/ui/anim/AnimatableParams;->width:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 100
    iget v1, p1, Lcom/twitter/ui/anim/AnimatableParams;->height:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 101
    iget-object v1, p0, Lckm;->c:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 103
    iget-object v0, p0, Lckm;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 104
    invoke-virtual {p0}, Lckm;->e()V

    .line 106
    :cond_0
    iget-object v0, p0, Lckm;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 107
    invoke-virtual {p0}, Lckm;->f()V

    .line 109
    :cond_1
    return-void
.end method

.method public b()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lckm;->c:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public b(Landroid/view/WindowManager;)V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lckm;->b:Landroid/view/ViewGroup;

    invoke-interface {p1, v0}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 149
    iget-object v0, p0, Lckm;->d:Landroid/view/View;

    invoke-interface {p1, v0}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 150
    return-void
.end method

.method public c()Landroid/view/View;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lckm;->d:Landroid/view/View;

    return-object v0
.end method

.method public d()Landroid/content/Context;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lckm;->a:Landroid/content/Context;

    return-object v0
.end method

.method public e()V
    .locals 3

    .prologue
    .line 156
    invoke-virtual {p0}, Lckm;->g()Lcom/twitter/ui/anim/AnimatableParams;

    move-result-object v0

    .line 157
    iget-object v1, p0, Lckm;->c:Landroid/view/ViewGroup;

    iget v2, v0, Lcom/twitter/ui/anim/AnimatableParams;->x:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setTranslationX(F)V

    .line 158
    iget-object v1, p0, Lckm;->c:Landroid/view/ViewGroup;

    iget v0, v0, Lcom/twitter/ui/anim/AnimatableParams;->y:I

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 159
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lckm;->e:Lckp;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lckm;->e:Lckp;

    invoke-interface {v0, p0}, Lckp;->a(Lckm;)V

    .line 168
    :cond_0
    return-void
.end method

.method public g()Lcom/twitter/ui/anim/AnimatableParams;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lckm;->f:Lcom/twitter/ui/anim/AnimatableParams;

    return-object v0
.end method
