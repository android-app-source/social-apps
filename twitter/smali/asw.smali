.class public Lasw;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final a:Lass;

.field private final b:Laso;

.field private final c:Lcom/twitter/database/lru/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/String;",
            "Lcgl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lass;Laso;Lcom/twitter/database/lru/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lass;",
            "Laso;",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/String;",
            "Lcgl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lasw;->a:Lass;

    .line 37
    iput-object p2, p0, Lasw;->b:Laso;

    .line 38
    iput-object p3, p0, Lasw;->c:Lcom/twitter/database/lru/l;

    .line 39
    return-void
.end method

.method static synthetic a(Lasw;)Lcom/twitter/database/lru/l;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lasw;->c:Lcom/twitter/database/lru/l;

    return-object v0
.end method

.method private c(Ljava/lang/String;)Lrx/functions/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/functions/b",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcgl;",
            "Lcom/twitter/model/core/y;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 86
    new-instance v0, Lasw$2;

    invoke-direct {v0, p0, p1}, Lasw$2;-><init>(Lasw;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a()Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcbi",
            "<",
            "Lcgm;",
            ">;",
            "Lbfb;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lasw;->a:Lass;

    invoke-static {}, Last;->a()Last;

    move-result-object v1

    invoke-virtual {v0, v1}, Lass;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcgm;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcgm;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcbi",
            "<",
            "Lcgm;",
            ">;",
            "Lbfb;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lasw;->a:Lass;

    invoke-static {p1}, Last;->a(Lcgm;)Last;

    move-result-object v1

    invoke-virtual {v0, v1}, Lass;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/g",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcgl;",
            "Lcom/twitter/model/core/y;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lasw;->b:Laso;

    .line 59
    invoke-virtual {v0, p1}, Laso;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 60
    invoke-direct {p0, p1}, Lasw;->c(Ljava/lang/String;)Lrx/functions/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/functions/b;)Lrx/c;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lrx/c;->b()Lrx/g;

    move-result-object v0

    .line 63
    invoke-virtual {p0, p1}, Lasw;->b(Ljava/lang/String;)Lrx/g;

    move-result-object v1

    invoke-static {v1, v0}, Lrx/g;->a(Lrx/g;Lrx/g;)Lrx/c;

    move-result-object v0

    .line 64
    invoke-static {}, Lcre;->d()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->e(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lrx/c;->b()Lrx/g;

    move-result-object v0

    .line 62
    return-object v0
.end method

.method public b(Lcgm;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcgm;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcbi",
            "<",
            "Lcgm;",
            ">;",
            "Lbfb;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lasw;->a:Lass;

    invoke-static {p1}, Last;->b(Lcgm;)Last;

    move-result-object v1

    invoke-virtual {v0, v1}, Lass;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/g",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcgl;",
            "Lcom/twitter/model/core/y;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lasw;->c:Lcom/twitter/database/lru/l;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/database/lru/l;->b(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    new-instance v1, Lasw$1;

    invoke-direct {v1, p0}, Lasw$1;-><init>(Lasw;)V

    .line 72
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 71
    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lasw;->a:Lass;

    invoke-virtual {v0}, Lass;->close()V

    .line 100
    return-void
.end method
