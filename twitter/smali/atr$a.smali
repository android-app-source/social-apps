.class Latr$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Latr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Latr$1;)V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Latr$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 169
    iput-object v0, p0, Latr$a;->a:Ljava/lang/String;

    .line 170
    iput-object v0, p0, Latr$a;->b:Ljava/lang/String;

    .line 171
    iput-object v0, p0, Latr$a;->c:Ljava/lang/String;

    .line 172
    iput-object v0, p0, Latr$a;->d:Ljava/lang/String;

    .line 173
    iput-boolean v1, p0, Latr$a;->e:Z

    .line 174
    iput-boolean v1, p0, Latr$a;->f:Z

    .line 175
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 195
    iput-object p1, p0, Latr$a;->c:Ljava/lang/String;

    .line 196
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Latr$a;->a:Ljava/lang/String;

    .line 191
    iput-object p2, p0, Latr$a;->b:Ljava/lang/String;

    .line 192
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x1

    iput-boolean v0, p0, Latr$a;->e:Z

    .line 179
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Latr$a;->d:Ljava/lang/String;

    .line 200
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x1

    iput-boolean v0, p0, Latr$a;->f:Z

    .line 183
    return-void
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Latr$a;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Latr$a;->a:Ljava/lang/String;

    iget-object v1, p0, Latr$a;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Latr$a;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Latr$a;->b:Ljava/lang/String;

    iget-object v1, p0, Latr$a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
