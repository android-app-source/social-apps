.class public final Lcha;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcha$a;,
        Lcha$b;
    }
.end annotation


# static fields
.field public static final a:Lcha$b;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Z

.field public final d:Lcom/twitter/model/timeline/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcha$b;

    invoke-direct {v0}, Lcha$b;-><init>()V

    sput-object v0, Lcha;->a:Lcha$b;

    return-void
.end method

.method private constructor <init>(Lcha$a;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {p1}, Lcha$a;->a(Lcha$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcha;->b:Ljava/lang/String;

    .line 48
    invoke-static {p1}, Lcha$a;->b(Lcha$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcha;->c:Z

    .line 49
    invoke-static {p1}, Lcha$a;->c(Lcha$a;)Lcom/twitter/model/timeline/j;

    move-result-object v0

    iput-object v0, p0, Lcha;->d:Lcom/twitter/model/timeline/j;

    .line 50
    return-void
.end method

.method synthetic constructor <init>(Lcha$a;Lcha$1;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcha;-><init>(Lcha$a;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 59
    if-ne p0, p1, :cond_1

    .line 68
    :cond_0
    :goto_0
    return v0

    .line 62
    :cond_1
    instance-of v2, p1, Lcha;

    if-nez v2, :cond_2

    move v0, v1

    .line 63
    goto :goto_0

    .line 66
    :cond_2
    check-cast p1, Lcha;

    .line 68
    iget-object v2, p0, Lcha;->b:Ljava/lang/String;

    iget-object v3, p1, Lcha;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcha;->c:Z

    iget-boolean v3, p1, Lcha;->c:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p1, Lcha;->d:Lcom/twitter/model/timeline/j;

    iget-object v3, p0, Lcha;->d:Lcom/twitter/model/timeline/j;

    .line 69
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, Lcha;->b:Ljava/lang/String;

    iget-boolean v1, p0, Lcha;->c:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v2, p0, Lcha;->d:Lcom/twitter/model/timeline/j;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
