.class public Lceu$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lceu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lceu;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lceu;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 49
    new-instance v2, Lceu;

    sget-object v0, Lcom/twitter/model/moments/r;->a:Lcom/twitter/util/serialization/l;

    .line 50
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/r;

    sget-object v1, Lcom/twitter/model/moments/w;->a:Lcom/twitter/util/serialization/l;

    .line 51
    invoke-virtual {p1, v1}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/moments/w;

    invoke-direct {v2, v0, v1}, Lceu;-><init>(Lcom/twitter/model/moments/r;Lcom/twitter/model/moments/w;)V

    .line 49
    return-object v2
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lceu;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p2, Lceu;->a:Lcom/twitter/model/moments/r;

    sget-object v1, Lcom/twitter/model/moments/r;->a:Lcom/twitter/util/serialization/l;

    .line 41
    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lceu;->b:Lcom/twitter/model/moments/w;

    sget-object v2, Lcom/twitter/model/moments/w;->a:Lcom/twitter/util/serialization/l;

    .line 42
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 43
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    check-cast p2, Lceu;

    invoke-virtual {p0, p1, p2}, Lceu$a;->a(Lcom/twitter/util/serialization/o;Lceu;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 36
    invoke-virtual {p0, p1, p2}, Lceu$a;->a(Lcom/twitter/util/serialization/n;I)Lceu;

    move-result-object v0

    return-object v0
.end method
