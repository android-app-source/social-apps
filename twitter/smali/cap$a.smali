.class public final Lcap$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcap;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:J

.field private d:J

.field private e:I

.field private f:I

.field private g:Lbzw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbzw",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field private h:I

.field private i:I

.field private j:Lbzw;

.field private k:I

.field private l:I

.field private m:Lbzw;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 134
    const/4 v0, 0x0

    iput v0, p0, Lcap$a;->f:I

    return-void
.end method

.method static synthetic a(Lcap$a;)J
    .locals 2

    .prologue
    .line 127
    iget-wide v0, p0, Lcap$a;->b:J

    return-wide v0
.end method

.method static synthetic b(Lcap$a;)J
    .locals 2

    .prologue
    .line 127
    iget-wide v0, p0, Lcap$a;->c:J

    return-wide v0
.end method

.method static synthetic c(Lcap$a;)J
    .locals 2

    .prologue
    .line 127
    iget-wide v0, p0, Lcap$a;->d:J

    return-wide v0
.end method

.method static synthetic d(Lcap$a;)I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcap$a;->a:I

    return v0
.end method

.method static synthetic e(Lcap$a;)I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcap$a;->e:I

    return v0
.end method

.method static synthetic f(Lcap$a;)I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcap$a;->f:I

    return v0
.end method

.method static synthetic g(Lcap$a;)I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcap$a;->h:I

    return v0
.end method

.method static synthetic h(Lcap$a;)I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcap$a;->i:I

    return v0
.end method

.method static synthetic i(Lcap$a;)I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcap$a;->k:I

    return v0
.end method

.method static synthetic j(Lcap$a;)I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcap$a;->l:I

    return v0
.end method

.method static synthetic k(Lcap$a;)Lbzw;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcap$a;->g:Lbzw;

    return-object v0
.end method

.method static synthetic l(Lcap$a;)Lbzw;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcap$a;->j:Lbzw;

    return-object v0
.end method

.method static synthetic m(Lcap$a;)Lbzw;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcap$a;->m:Lbzw;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 242
    iget v0, p0, Lcap$a;->f:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcap$a;->g:Lbzw;

    if-eqz v0, :cond_1

    iget v0, p0, Lcap$a;->i:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcap$a;->j:Lbzw;

    if-eqz v0, :cond_1

    iget v0, p0, Lcap$a;->l:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcap$a;->m:Lbzw;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Lcap$a;
    .locals 0

    .prologue
    .line 151
    iput p1, p0, Lcap$a;->a:I

    .line 152
    return-object p0
.end method

.method public a(J)Lcap$a;
    .locals 1

    .prologue
    .line 157
    iput-wide p1, p0, Lcap$a;->b:J

    .line 158
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcap$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)",
            "Lcap$a;"
        }
    .end annotation

    .prologue
    .line 187
    if-eqz p1, :cond_0

    .line 188
    new-instance v0, Lbzw;

    invoke-direct {v0, p1}, Lbzw;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcap$a;->g:Lbzw;

    .line 190
    :cond_0
    return-object p0
.end method

.method public b(I)Lcap$a;
    .locals 0

    .prologue
    .line 175
    iput p1, p0, Lcap$a;->e:I

    .line 176
    return-object p0
.end method

.method public b(J)Lcap$a;
    .locals 1

    .prologue
    .line 163
    iput-wide p1, p0, Lcap$a;->c:J

    .line 164
    return-object p0
.end method

.method public b(Ljava/util/List;)Lcap$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/twitter/model/core/j;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)",
            "Lcap$a;"
        }
    .end annotation

    .prologue
    .line 207
    if-eqz p1, :cond_0

    .line 208
    new-instance v0, Lbzw;

    invoke-direct {v0, p1}, Lbzw;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcap$a;->j:Lbzw;

    .line 210
    :cond_0
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 127
    invoke-virtual {p0}, Lcap$a;->e()Lcap;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lcap$a;
    .locals 0

    .prologue
    .line 181
    iput p1, p0, Lcap$a;->f:I

    .line 182
    return-object p0
.end method

.method public c(J)Lcap$a;
    .locals 1

    .prologue
    .line 169
    iput-wide p1, p0, Lcap$a;->d:J

    .line 170
    return-object p0
.end method

.method public c(Ljava/util/List;)Lcap$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/twitter/model/core/j;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)",
            "Lcap$a;"
        }
    .end annotation

    .prologue
    .line 227
    if-eqz p1, :cond_0

    .line 228
    new-instance v0, Lbzw;

    invoke-direct {v0, p1}, Lbzw;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcap$a;->m:Lbzw;

    .line 230
    :cond_0
    return-object p0
.end method

.method public d(I)Lcap$a;
    .locals 0

    .prologue
    .line 195
    iput p1, p0, Lcap$a;->h:I

    .line 196
    return-object p0
.end method

.method public e(I)Lcap$a;
    .locals 0

    .prologue
    .line 201
    iput p1, p0, Lcap$a;->i:I

    .line 202
    return-object p0
.end method

.method protected e()Lcap;
    .locals 2

    .prologue
    .line 236
    new-instance v0, Lcap;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcap;-><init>(Lcap$a;Lcap$1;)V

    return-object v0
.end method

.method public f(I)Lcap$a;
    .locals 0

    .prologue
    .line 215
    iput p1, p0, Lcap$a;->k:I

    .line 216
    return-object p0
.end method

.method public g(I)Lcap$a;
    .locals 0

    .prologue
    .line 221
    iput p1, p0, Lcap$a;->l:I

    .line 222
    return-object p0
.end method
