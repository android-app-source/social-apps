.class Lcdg$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcdg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcdg;",
        "Lcdg$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcdg$1;)V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0}, Lcdg$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcdg$a;
    .locals 1

    .prologue
    .line 144
    new-instance v0, Lcdg$a;

    invoke-direct {v0}, Lcdg$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcdg$a;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 150
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v3

    .line 151
    invoke-static {v3}, Lcom/twitter/util/collection/MutableMap;->a(I)Ljava/util/Map;

    move-result-object v4

    move v2, v1

    .line 152
    :goto_0
    if-ge v2, v3, :cond_1

    .line 153
    sget-object v0, Lcdc;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdc;

    .line 154
    if-eqz v0, :cond_0

    .line 155
    iget-object v5, v0, Lcdc;->b:Ljava/lang/String;

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 159
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v2

    .line 160
    invoke-static {v2}, Lcom/twitter/util/collection/o;->a(I)Lcom/twitter/util/collection/o;

    move-result-object v3

    move v0, v1

    .line 161
    :goto_1
    if-ge v0, v2, :cond_2

    .line 162
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 161
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 165
    :cond_2
    sget-object v0, Lcdh;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdh;

    .line 167
    new-instance v1, Lcdd$a;

    invoke-direct {v1}, Lcdd$a;-><init>()V

    .line 169
    invoke-virtual {v1, v4}, Lcdd$a;->a(Ljava/util/Map;)Lcdd$a;

    move-result-object v1

    iget-object v2, v0, Lcdh;->c:Ljava/lang/String;

    .line 170
    invoke-virtual {v1, v2}, Lcdd$a;->b(Ljava/lang/String;)Lcdd$a;

    move-result-object v1

    iget-object v0, v0, Lcdh;->b:Ljava/lang/String;

    .line 171
    invoke-virtual {v1, v0}, Lcdd$a;->a(Ljava/lang/String;)Lcdd$a;

    move-result-object v0

    .line 172
    invoke-virtual {v0}, Lcdd$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdd;

    .line 167
    invoke-virtual {p2, v0}, Lcdg$a;->a(Lcdd;)Lcdg$a;

    move-result-object v1

    .line 173
    invoke-virtual {v3}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-virtual {v1, v0}, Lcdg$a;->a(Ljava/util/Set;)Lcdg$a;

    .line 174
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 133
    check-cast p2, Lcdg$a;

    invoke-virtual {p0, p1, p2, p3}, Lcdg$b;->a(Lcom/twitter/util/serialization/n;Lcdg$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcdg;)V
    .locals 1

    .prologue
    .line 138
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 133
    check-cast p2, Lcdg;

    invoke-virtual {p0, p1, p2}, Lcdg$b;->a(Lcom/twitter/util/serialization/o;Lcdg;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 133
    invoke-virtual {p0}, Lcdg$b;->a()Lcdg$a;

    move-result-object v0

    return-object v0
.end method
