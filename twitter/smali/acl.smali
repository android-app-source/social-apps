.class public Lacl;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lack;


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Lrx/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Lakr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lakr",
            "<",
            "Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lzp;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Lrx/c;Lakr;Lzp;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/LayoutInflater;",
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/h;",
            ">;>;",
            "Lakr",
            "<",
            "Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$c;",
            ">;",
            "Lzp;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lacl;->a:Landroid/view/LayoutInflater;

    .line 32
    iput-object p2, p0, Lacl;->b:Lrx/c;

    .line 33
    iput-object p3, p0, Lacl;->c:Lakr;

    .line 34
    iput-object p4, p0, Lacl;->d:Lzp;

    .line 35
    return-void
.end method


# virtual methods
.method public a(Landroid/support/v7/widget/RecyclerView;)Lacm;
    .locals 5

    .prologue
    .line 40
    iget-object v0, p0, Lacl;->a:Landroid/view/LayoutInflater;

    .line 41
    invoke-static {v0, p1}, Lcnv;->a(Landroid/view/LayoutInflater;Landroid/support/v7/widget/RecyclerView;)Lcnv;

    move-result-object v0

    .line 42
    new-instance v1, Lacn;

    iget-object v2, p0, Lacl;->b:Lrx/c;

    iget-object v3, p0, Lacl;->c:Lakr;

    iget-object v4, p0, Lacl;->d:Lzp;

    invoke-direct {v1, v0, v2, v3, v4}, Lacn;-><init>(Lcnw;Lrx/c;Lakr;Lzp;)V

    return-object v1
.end method

.method public a(Landroid/view/ViewGroup;)Lacm;
    .locals 5

    .prologue
    .line 58
    iget-object v0, p0, Lacl;->a:Landroid/view/LayoutInflater;

    .line 59
    invoke-static {v0, p1}, Laco;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Laco;

    move-result-object v0

    .line 60
    new-instance v1, Lacn;

    iget-object v2, p0, Lacl;->b:Lrx/c;

    iget-object v3, p0, Lacl;->c:Lakr;

    iget-object v4, p0, Lacl;->d:Lzp;

    invoke-direct {v1, v0, v2, v3, v4}, Lacn;-><init>(Lcnw;Lrx/c;Lakr;Lzp;)V

    return-object v1
.end method

.method public b(Landroid/support/v7/widget/RecyclerView;)Lacm;
    .locals 5

    .prologue
    .line 49
    iget-object v0, p0, Lacl;->a:Landroid/view/LayoutInflater;

    .line 50
    invoke-static {v0, p1}, Lcnv;->b(Landroid/view/LayoutInflater;Landroid/support/v7/widget/RecyclerView;)Lcnv;

    move-result-object v0

    .line 51
    new-instance v1, Lacn;

    iget-object v2, p0, Lacl;->b:Lrx/c;

    iget-object v3, p0, Lacl;->c:Lakr;

    iget-object v4, p0, Lacl;->d:Lzp;

    invoke-direct {v1, v0, v2, v3, v4}, Lacn;-><init>(Lcnw;Lrx/c;Lakr;Lzp;)V

    return-object v1
.end method
