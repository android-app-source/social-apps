.class public Labr;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lacg;
.implements Lcom/twitter/android/moments/ui/fullscreen/cu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lacg",
        "<",
        "Lcom/twitter/model/moments/viewmodels/n;",
        ">;",
        "Lcom/twitter/android/moments/ui/fullscreen/cu;"
    }
.end annotation


# instance fields
.field private final b:Lcom/twitter/android/moments/ui/fullscreen/as;

.field private final c:Labq;

.field private final d:Lcom/twitter/android/moments/ui/fullscreen/bp;

.field private final e:Landroid/content/res/Resources;

.field private final f:Lbxo;

.field private final g:Lcom/twitter/android/moments/ui/fullscreen/cs;


# direct methods
.method public constructor <init>(Labq;Lcom/twitter/android/moments/ui/fullscreen/bp;Lbxo;Lcom/twitter/android/moments/ui/fullscreen/cs;Landroid/content/res/Resources;Lcom/twitter/android/moments/ui/fullscreen/as;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Labr;->c:Labq;

    .line 54
    iput-object p2, p0, Labr;->d:Lcom/twitter/android/moments/ui/fullscreen/bp;

    .line 55
    iput-object p5, p0, Labr;->e:Landroid/content/res/Resources;

    .line 56
    iput-object p6, p0, Labr;->b:Lcom/twitter/android/moments/ui/fullscreen/as;

    .line 57
    iput-object p3, p0, Labr;->f:Lbxo;

    .line 58
    iput-object p4, p0, Labr;->g:Lcom/twitter/android/moments/ui/fullscreen/cs;

    .line 59
    return-void
.end method

.method public static a(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/content/res/Resources;Lcom/twitter/android/moments/ui/fullscreen/as;Lbxo;)Labr;
    .locals 7

    .prologue
    .line 40
    invoke-static {p1}, Labq;->a(Landroid/view/LayoutInflater;)Labq;

    move-result-object v1

    .line 41
    new-instance v0, Labr;

    .line 42
    invoke-static {p0}, Lcom/twitter/android/moments/ui/fullscreen/bp;->a(Landroid/content/Context;)Lcom/twitter/android/moments/ui/fullscreen/bp;

    move-result-object v2

    new-instance v4, Lcom/twitter/android/moments/ui/fullscreen/an;

    invoke-direct {v4}, Lcom/twitter/android/moments/ui/fullscreen/an;-><init>()V

    move-object v3, p4

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Labr;-><init>(Labq;Lcom/twitter/android/moments/ui/fullscreen/bp;Lbxo;Lcom/twitter/android/moments/ui/fullscreen/cs;Landroid/content/res/Resources;Lcom/twitter/android/moments/ui/fullscreen/as;)V

    .line 41
    return-object v0
.end method

.method static synthetic a(Labr;)Lcom/twitter/android/moments/ui/fullscreen/cs;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Labr;->g:Lcom/twitter/android/moments/ui/fullscreen/cs;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/viewmodels/n;Lcom/twitter/model/core/Tweet;)V
    .locals 6

    .prologue
    .line 63
    invoke-static {p2}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    iget-object v0, p0, Labr;->c:Labq;

    iget-object v1, p0, Labr;->d:Lcom/twitter/android/moments/ui/fullscreen/bp;

    iget-object v2, p0, Labr;->c:Labq;

    .line 65
    invoke-virtual {v2}, Labq;->a()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/twitter/android/moments/ui/fullscreen/bp;->a(Lcom/twitter/model/moments/viewmodels/n;Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 64
    invoke-virtual {v0, v1}, Labq;->a(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v0, p0, Labr;->c:Labq;

    iget-object v1, p2, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Labq;->b(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v0, p0, Labr;->c:Labq;

    iget-object v1, p0, Labr;->e:Landroid/content/res/Resources;

    const v2, 0x7f0a0b92

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p2, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Labq;->c(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p0, Labr;->c:Labq;

    iget-boolean v1, p2, Lcom/twitter/model/core/Tweet;->L:Z

    invoke-virtual {v0, v1}, Labq;->a(Z)V

    .line 69
    iget-object v0, p0, Labr;->b:Lcom/twitter/android/moments/ui/fullscreen/as;

    iget-object v1, p0, Labr;->c:Labq;

    invoke-virtual {v1}, Labq;->c()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p1}, Lcom/twitter/android/moments/ui/fullscreen/as;->a(Landroid/view/ViewGroup;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    .line 70
    iget-object v0, p0, Labr;->c:Labq;

    iget-object v1, p0, Labr;->f:Lbxo;

    invoke-virtual {v1, p2}, Lbxo;->a(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Labq;->d(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v0, p0, Labr;->c:Labq;

    new-instance v1, Labr$1;

    invoke-direct {v1, p0, p2}, Labr$1;-><init>(Labr;Lcom/twitter/model/core/Tweet;)V

    invoke-virtual {v0, v1}, Labq;->b(Landroid/view/View$OnClickListener;)V

    .line 77
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/n;->n()Lcom/twitter/model/moments/s;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Labr;->c:Labq;

    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/n;->n()Lcom/twitter/model/moments/s;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/model/moments/s;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Labq;->a(Ljava/lang/String;)V

    .line 80
    :cond_0
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Labr;->c:Labq;

    invoke-virtual {v0}, Labq;->d()Landroid/widget/FrameLayout;

    move-result-object v0

    return-object v0
.end method

.method public b()Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Lacg",
            "<",
            "Lcom/twitter/model/moments/viewmodels/n;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Labr;->b:Lcom/twitter/android/moments/ui/fullscreen/as;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/as;->a()Lrx/g;

    move-result-object v0

    .line 86
    invoke-static {p0}, Lcre;->a(Ljava/lang/Object;)Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 85
    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Labr;->b:Lcom/twitter/android/moments/ui/fullscreen/as;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/as;->b()V

    .line 92
    return-void
.end method

.method public e()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Labr;->c:Labq;

    invoke-virtual {v0}, Labq;->e()Lrx/c;

    move-result-object v0

    return-object v0
.end method
