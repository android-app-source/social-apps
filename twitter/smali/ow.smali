.class public Low;
.super Lbjb;
.source "Twttr"


# instance fields
.field final a:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/Date;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/library/av/playback/AVPlayer;

.field private c:Lcom/twitter/model/av/PlayerLayoutStates;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0, p2}, Lbjb;-><init>(Lcom/twitter/model/av/AVMedia;)V

    .line 37
    new-instance v0, Lcom/twitter/model/av/PlayerLayoutStates;

    invoke-direct {v0, v1, v1}, Lcom/twitter/model/av/PlayerLayoutStates;-><init>(ZZ)V

    iput-object v0, p0, Low;->c:Lcom/twitter/model/av/PlayerLayoutStates;

    .line 41
    iput-object p1, p0, Low;->b:Lcom/twitter/library/av/playback/AVPlayer;

    .line 42
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Low;->a:Ljava/util/concurrent/atomic/AtomicReference;

    .line 43
    return-void
.end method


# virtual methods
.method a(J)Ljava/util/Date;
    .locals 7
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Low;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/Pair;

    .line 99
    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {v0}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long v2, p1, v2

    .line 101
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Lcom/twitter/util/collection/Pair;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    move-object v0, v1

    .line 104
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public processForcedSkipForward(Lcom/twitter/library/av/playback/j$b;)V
    .locals 2
    .annotation runtime Lbiz;
        a = Lcom/twitter/library/av/playback/j$b;
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Low;->a:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 55
    return-void
.end method

.method public processFullscreenEnded(Lbjr;)V
    .locals 2
    .annotation runtime Lbiz;
        a = Lbjr;
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Low;->c:Lcom/twitter/model/av/PlayerLayoutStates;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/model/av/PlayerLayoutStates;->b(Z)V

    .line 65
    return-void
.end method

.method public processFullscreenStarted(Lbjs;)V
    .locals 2
    .annotation runtime Lbiz;
        a = Lbjs;
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Low;->c:Lcom/twitter/model/av/PlayerLayoutStates;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/model/av/PlayerLayoutStates;->b(Z)V

    .line 60
    return-void
.end method

.method public processHeartbeatEvent(Lbkl;)V
    .locals 11
    .annotation runtime Lbiz;
        a = Lbkl;
    .end annotation

    .prologue
    .line 74
    iget-wide v0, p1, Lbkl;->c:J

    invoke-virtual {p0, v0, v1}, Low;->a(J)Ljava/util/Date;

    move-result-object v0

    .line 75
    iget-wide v2, p1, Lbkl;->b:J

    invoke-virtual {p0, v2, v3}, Low;->a(J)Ljava/util/Date;

    move-result-object v2

    .line 77
    iget-object v1, p0, Low;->c:Lcom/twitter/model/av/PlayerLayoutStates;

    invoke-virtual {v1}, Lcom/twitter/model/av/PlayerLayoutStates;->c()V

    .line 79
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    .line 80
    new-instance v10, Lcom/twitter/library/av/m$a;

    invoke-direct {v10}, Lcom/twitter/library/av/m$a;-><init>()V

    .line 81
    new-instance v1, Lbiu;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 82
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    iget v6, p1, Lbkl;->d:I

    iget-wide v7, p1, Lbkl;->e:J

    iget-object v0, p0, Low;->c:Lcom/twitter/model/av/PlayerLayoutStates;

    invoke-virtual {v0}, Lcom/twitter/model/av/PlayerLayoutStates;->d()Ljava/util/List;

    move-result-object v9

    invoke-direct/range {v1 .. v9}, Lbiu;-><init>(JJIJLjava/util/List;)V

    .line 81
    invoke-virtual {v10, v1}, Lcom/twitter/library/av/m$a;->a(Lbyd;)Lcom/twitter/library/av/m$a;

    .line 83
    iget-object v0, p0, Low;->b:Lcom/twitter/library/av/playback/AVPlayer;

    const-string/jumbo v1, "heartbeat"

    invoke-virtual {v0, v1, v10}, Lcom/twitter/library/av/playback/AVPlayer;->a(Ljava/lang/String;Lcom/twitter/library/av/m$a;)V

    .line 84
    new-instance v0, Lcom/twitter/model/av/PlayerLayoutStates;

    iget-object v1, p0, Low;->c:Lcom/twitter/model/av/PlayerLayoutStates;

    .line 85
    invoke-virtual {v1}, Lcom/twitter/model/av/PlayerLayoutStates;->a()Z

    move-result v1

    iget-object v2, p0, Low;->c:Lcom/twitter/model/av/PlayerLayoutStates;

    invoke-virtual {v2}, Lcom/twitter/model/av/PlayerLayoutStates;->b()Z

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/av/PlayerLayoutStates;-><init>(ZZ)V

    iput-object v0, p0, Low;->c:Lcom/twitter/model/av/PlayerLayoutStates;

    .line 87
    :cond_0
    return-void
.end method

.method public processOrientationChangedEvent(Lbjv;)V
    .locals 2
    .annotation runtime Lbiz;
        a = Lbjv;
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Low;->c:Lcom/twitter/model/av/PlayerLayoutStates;

    invoke-virtual {p1}, Lbjv;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/av/PlayerLayoutStates;->a(Z)V

    .line 70
    return-void
.end method

.method public processProgramDateTimeUpdate(Lcom/twitter/library/av/playback/j$f;)V
    .locals 4
    .annotation runtime Lbiz;
        a = Lcom/twitter/library/av/playback/j$f;
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Low;->a:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, p1, Lcom/twitter/library/av/playback/j$f;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/library/av/playback/j$f;->b:Ljava/util/Date;

    invoke-static {v1, v2}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 49
    return-void
.end method
