.class public Lbhq;
.super Lcom/twitter/library/api/r;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/api/r",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcom/twitter/model/core/TwitterUser$a;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final b:J

.field private final c:Lcgi;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:[I

.field private m:J

.field private r:I

.field private s:Ljava/lang/Integer;

.field private t:Lcom/twitter/model/core/TwitterUser;

.field private final u:Ljava/lang/String;

.field private final v:Lcom/twitter/library/provider/t;

.field private final w:Lcom/twitter/library/api/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/TwitterUser$a;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V
    .locals 7

    .prologue
    .line 71
    new-instance v3, Lcom/twitter/library/service/v;

    invoke-direct {v3, p2}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lbhq;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;JLcgi;)V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;JLcgi;)V
    .locals 9

    .prologue
    .line 78
    iget-wide v0, p2, Lcom/twitter/library/service/v;->c:J

    .line 79
    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v7

    const-class v0, Lcom/twitter/model/core/TwitterUser$a;

    .line 80
    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    .line 78
    invoke-direct/range {v1 .. v8}, Lbhq;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;JLcgi;Lcom/twitter/library/provider/t;Lcom/twitter/library/api/i;)V

    .line 81
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;JLcgi;Lcom/twitter/library/provider/t;Lcom/twitter/library/api/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/service/v;",
            "J",
            "Lcgi;",
            "Lcom/twitter/library/provider/t;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/TwitterUser$a;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    const-class v0, Lbhq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/r;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lbhq;->r:I

    .line 87
    iput-wide p3, p0, Lbhq;->b:J

    .line 88
    iput-object p5, p0, Lbhq;->c:Lcgi;

    .line 89
    iput-object p6, p0, Lbhq;->v:Lcom/twitter/library/provider/t;

    .line 90
    iput-object p7, p0, Lbhq;->w:Lcom/twitter/library/api/i;

    .line 91
    new-instance v0, Lcom/twitter/library/service/o;

    invoke-direct {v0}, Lcom/twitter/library/service/o;-><init>()V

    invoke-virtual {p0, v0}, Lbhq;->a(Lcom/twitter/library/service/e;)V

    .line 94
    invoke-static {p3, p4, p2}, Lbhq;->a(JLcom/twitter/library/service/v;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbhq;->u:Ljava/lang/String;

    .line 95
    return-void
.end method

.method static synthetic a(Lbhq;)Laut;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lbhq;->S()Laut;

    move-result-object v0

    return-object v0
.end method

.method protected static a(JLcom/twitter/library/service/v;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 348
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string/jumbo v1, "follow_%d_%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p2, Lcom/twitter/library/service/v;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/twitter/model/core/z;)V
    .locals 4

    .prologue
    .line 273
    if-eqz p1, :cond_1

    .line 274
    invoke-virtual {p1}, Lcom/twitter/model/core/z;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/y;

    .line 275
    iget v2, v0, Lcom/twitter/model/core/y;->b:I

    const/16 v3, 0xfa

    if-ne v2, v3, :cond_0

    .line 276
    iget-wide v0, v0, Lcom/twitter/model/core/y;->d:J

    iput-wide v0, p0, Lbhq;->m:J

    .line 281
    :cond_1
    return-void
.end method

.method static synthetic b(Lbhq;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lbhq;->k:Z

    return v0
.end method

.method static synthetic c(Lbhq;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lbhq;->h:Z

    return v0
.end method

.method static synthetic d(Lbhq;)J
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lbhq;->b:J

    return-wide v0
.end method

.method static synthetic e(Lbhq;)Lcom/twitter/library/provider/t;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lbhq;->v:Lcom/twitter/library/provider/t;

    return-object v0
.end method


# virtual methods
.method public a(I)Lbhq;
    .locals 0

    .prologue
    .line 132
    iput p1, p0, Lbhq;->r:I

    .line 133
    return-object p0
.end method

.method public a(Ljava/lang/Integer;)Lbhq;
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lbhq;->s:Ljava/lang/Integer;

    .line 139
    return-object p0
.end method

.method public a(Z)Lbhq;
    .locals 0

    .prologue
    .line 108
    iput-boolean p1, p0, Lbhq;->g:Z

    .line 109
    return-object p0
.end method

.method protected a()Lcom/twitter/library/service/d;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 151
    invoke-virtual {p0}, Lbhq;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 152
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "friendships"

    aput-object v3, v1, v2

    const-string/jumbo v2, "create"

    aput-object v2, v1, v4

    .line 153
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "send_error_codes"

    .line 154
    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    iget-wide v2, p0, Lbhq;->b:J

    .line 155
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 157
    iget-boolean v1, p0, Lbhq;->g:Z

    if-eqz v1, :cond_0

    .line 158
    const-string/jumbo v1, "follow"

    const-string/jumbo v2, "true"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 161
    :cond_0
    iget-boolean v1, p0, Lbhq;->h:Z

    if-eqz v1, :cond_1

    .line 162
    const-string/jumbo v1, "lifeline"

    const-string/jumbo v2, "true"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 165
    :cond_1
    iget-object v1, p0, Lbhq;->c:Lcgi;

    if-eqz v1, :cond_3

    .line 166
    iget-object v1, p0, Lbhq;->c:Lcgi;

    iget-object v1, v1, Lcgi;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 167
    const-string/jumbo v1, "impression_id"

    iget-object v2, p0, Lbhq;->c:Lcgi;

    iget-object v2, v2, Lcgi;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 169
    :cond_2
    iget-object v1, p0, Lbhq;->c:Lcgi;

    invoke-virtual {v1}, Lcgi;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 170
    const-string/jumbo v1, "earned"

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 174
    :cond_3
    iget-boolean v1, p0, Lbhq;->i:Z

    if-eqz v1, :cond_4

    .line 175
    const-string/jumbo v1, "challenges_passed"

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 177
    :cond_4
    const-string/jumbo v1, "handles_challenges"

    const-string/jumbo v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 178
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 214
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/async/service/AsyncOperation;->cancel(Z)Z

    .line 215
    return-void
.end method

.method public a(Lcom/twitter/async/service/j;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v9, 0x1

    .line 219
    invoke-super {p0, p1}, Lcom/twitter/library/api/r;->a(Lcom/twitter/async/service/j;)V

    .line 220
    invoke-virtual {p0}, Lbhq;->S()Laut;

    move-result-object v10

    .line 221
    invoke-virtual {p0}, Lbhq;->T()Z

    move-result v0

    .line 222
    if-eqz v0, :cond_4

    .line 223
    iget-object v0, p0, Lbhq;->w:Lcom/twitter/library/api/i;

    invoke-virtual {v0}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser$a;

    .line 224
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser$a;->R_()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 228
    iget-object v1, p0, Lbhq;->v:Lcom/twitter/library/provider/t;

    iget-wide v2, p0, Lbhq;->b:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/provider/t;->h(J)I

    move-result v2

    .line 229
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser$a;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 230
    invoke-static {v2, v9}, Lcom/twitter/model/core/g;->b(II)I

    move-result v1

    .line 231
    const/16 v2, 0x4000

    invoke-static {v1, v2}, Lcom/twitter/model/core/g;->a(II)I

    move-result v1

    .line 236
    :goto_0
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->i(I)Lcom/twitter/model/core/TwitterUser$a;

    .line 239
    iget-object v1, p0, Lbhq;->v:Lcom/twitter/library/provider/t;

    iget-wide v2, p0, Lbhq;->b:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/provider/t;->b(J)Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    .line 240
    if-eqz v1, :cond_0

    .line 241
    iget v1, v1, Lcom/twitter/model/core/TwitterUser;->R:I

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->c(I)Lcom/twitter/model/core/TwitterUser$a;

    .line 243
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lbhq;->t:Lcom/twitter/model/core/TwitterUser;

    .line 246
    invoke-virtual {p0}, Lbhq;->M()Lcom/twitter/library/service/v;

    move-result-object v11

    .line 247
    iget-object v0, p0, Lbhq;->v:Lcom/twitter/library/provider/t;

    iget-object v1, p0, Lbhq;->t:Lcom/twitter/model/core/TwitterUser;

    invoke-static {v1}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iget-wide v2, v11, Lcom/twitter/library/service/v;->c:J

    const/4 v4, 0x0

    const-wide/16 v5, -0x1

    move-object v8, v7

    invoke-virtual/range {v0 .. v10}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;ZLaut;)I

    .line 251
    iget-object v4, p0, Lbhq;->v:Lcom/twitter/library/provider/t;

    const/4 v5, 0x2

    iget-wide v6, v11, Lcom/twitter/library/service/v;->c:J

    iget-wide v8, p0, Lbhq;->b:J

    invoke-virtual/range {v4 .. v10}, Lcom/twitter/library/provider/t;->a(IJJLaut;)V

    .line 254
    new-instance v0, Lcom/twitter/library/api/search/a;

    iget-object v1, p0, Lbhq;->p:Landroid/content/Context;

    iget-object v2, p0, Lbhq;->t:Lcom/twitter/model/core/TwitterUser;

    invoke-direct {v0, v1, v11, v2}, Lcom/twitter/library/api/search/a;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/model/core/TwitterUser;)V

    invoke-virtual {p0, v0}, Lbhq;->b(Lcom/twitter/async/service/AsyncOperation;)V

    .line 256
    invoke-static {}, Lbhu;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 257
    new-instance v0, Lbhx;

    iget-object v1, p0, Lbhq;->p:Landroid/content/Context;

    invoke-virtual {p0}, Lbhq;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbhx;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V

    .line 258
    iget-object v1, p0, Lbhq;->t:Lcom/twitter/model/core/TwitterUser;

    iput-object v1, v0, Lbhx;->a:Lcom/twitter/model/core/TwitterUser;

    .line 259
    invoke-virtual {p0, v0}, Lbhq;->b(Lcom/twitter/async/service/AsyncOperation;)V

    .line 269
    :cond_1
    :goto_1
    invoke-virtual {v10}, Laut;->a()V

    .line 270
    return-void

    .line 233
    :cond_2
    iget-boolean v1, p0, Lbhq;->h:Z

    if-eqz v1, :cond_3

    const/16 v1, 0x100

    :goto_2
    invoke-static {v2, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v1

    goto :goto_0

    :cond_3
    move v1, v9

    goto :goto_2

    .line 263
    :cond_4
    iget-object v0, p0, Lbhq;->w:Lcom/twitter/library/api/i;

    invoke-virtual {v0}, Lcom/twitter/library/api/i;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/z;

    .line 264
    invoke-static {v0}, Lcom/twitter/model/core/z;->a(Lcom/twitter/model/core/z;)[I

    move-result-object v1

    iput-object v1, p0, Lbhq;->l:[I

    .line 265
    invoke-direct {p0, v0}, Lbhq;->a(Lcom/twitter/model/core/z;)V

    .line 267
    iget-object v6, p0, Lbhq;->v:Lcom/twitter/library/provider/t;

    iget-wide v7, p0, Lbhq;->b:J

    invoke-virtual {p0}, Lbhq;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-wide v12, v0, Lcom/twitter/library/service/v;->c:J

    move v11, v9

    invoke-virtual/range {v6 .. v13}, Lcom/twitter/library/provider/t;->b(JILaut;ZJ)V

    goto :goto_1
.end method

.method public b(Z)Lbhq;
    .locals 0

    .prologue
    .line 114
    iput-boolean p1, p0, Lbhq;->h:Z

    .line 115
    return-object p0
.end method

.method protected b()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/TwitterUser$a;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    iget-object v0, p0, Lbhq;->w:Lcom/twitter/library/api/i;

    return-object v0
.end method

.method public c(Z)Lbhq;
    .locals 0

    .prologue
    .line 120
    iput-boolean p1, p0, Lbhq;->i:Z

    .line 121
    return-object p0
.end method

.method public c(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 191
    if-eqz p1, :cond_0

    .line 192
    invoke-virtual {p0, p1}, Lbhq;->a(Lcom/twitter/async/service/AsyncOperation;)V

    .line 194
    :cond_0
    new-instance v0, Lbhq$1;

    invoke-direct {v0, p0}, Lbhq$1;-><init>(Lbhq;)V

    return-object v0
.end method

.method public d(Z)Lbhq;
    .locals 0

    .prologue
    .line 126
    iput-boolean p1, p0, Lbhq;->j:Z

    .line 127
    return-object p0
.end method

.method protected d(Lcom/twitter/async/service/j;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 322
    invoke-super {p0, p1}, Lcom/twitter/library/api/r;->d(Lcom/twitter/async/service/j;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    :goto_0
    return v1

    .line 326
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 332
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    const/16 v2, 0x193

    if-ne v0, v2, :cond_1

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public e(Z)Lbhq;
    .locals 0

    .prologue
    .line 144
    iput-boolean p1, p0, Lbhq;->k:Z

    .line 145
    return-object p0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    const-string/jumbo v0, "app:twitter_service:follow:create"

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lbhq;->b()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method

.method public final g()[I
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lbhq;->l:[I

    return-object v0
.end method

.method public final h()J
    .locals 2

    .prologue
    .line 288
    iget-wide v0, p0, Lbhq;->m:J

    return-wide v0
.end method

.method protected o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lbhq;->u:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Lcom/twitter/model/core/TwitterUser;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lbhq;->t:Lcom/twitter/model/core/TwitterUser;

    return-object v0
.end method

.method public final t()J
    .locals 2

    .prologue
    .line 296
    iget-wide v0, p0, Lbhq;->b:J

    return-wide v0
.end method

.method public final u()Lcgi;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lbhq;->c:Lcgi;

    return-object v0
.end method

.method public final v()I
    .locals 1

    .prologue
    .line 305
    iget v0, p0, Lbhq;->r:I

    return v0
.end method

.method public final w()Z
    .locals 1

    .prologue
    .line 309
    iget-boolean v0, p0, Lbhq;->j:Z

    return v0
.end method
