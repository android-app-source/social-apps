.class public abstract Laue;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lauj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        "C::",
        "Ljava/io/Closeable;",
        "N:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lauj",
        "<TA;TC;>;"
    }
.end annotation


# instance fields
.field private final a:Lrx/f;

.field private final b:Lrx/f;

.field private final c:Lauq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauq",
            "<TC;>;"
        }
    .end annotation
.end field

.field private final d:Lcwb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcwb",
            "<TN;>;"
        }
    .end annotation
.end field

.field private final e:Lcwv;


# direct methods
.method protected constructor <init>(Lrx/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/c",
            "<TN;>;)V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v0

    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v1

    invoke-direct {p0, v0, v1, p1}, Laue;-><init>(Lrx/f;Lrx/f;Lrx/c;)V

    .line 56
    return-void
.end method

.method protected constructor <init>(Lrx/f;Lrx/f;Lrx/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/f;",
            "Lrx/f;",
            "Lrx/c",
            "<TN;>;)V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Laue;->a:Lrx/f;

    .line 61
    iput-object p2, p0, Laue;->b:Lrx/f;

    .line 62
    new-instance v0, Lauq;

    invoke-direct {v0}, Lauq;-><init>()V

    iput-object v0, p0, Laue;->c:Lauq;

    .line 63
    invoke-virtual {p3}, Lrx/c;->m()Lcwb;

    move-result-object v0

    iput-object v0, p0, Laue;->d:Lcwb;

    .line 64
    new-instance v0, Lcwv;

    invoke-direct {v0}, Lcwv;-><init>()V

    iput-object v0, p0, Laue;->e:Lcwv;

    .line 65
    iget-object v0, p0, Laue;->e:Lcwv;

    iget-object v1, p0, Laue;->d:Lcwb;

    invoke-virtual {v1}, Lcwb;->a()Lrx/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcwv;->a(Lrx/j;)V

    .line 66
    return-void
.end method

.method static synthetic a(Laue;)Lauq;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Laue;->c:Lauq;

    return-object v0
.end method


# virtual methods
.method public a_(Ljava/lang/Object;)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)",
            "Lrx/c",
            "<TC;>;"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Laue;->d:Lcwb;

    const/4 v1, 0x0

    .line 72
    invoke-virtual {v0, v1}, Lcwb;->d(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Laue;->b:Lrx/f;

    .line 73
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Laue$1;

    invoke-direct {v1, p0, p1}, Laue$1;-><init>(Laue;Ljava/lang/Object;)V

    .line 74
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Laue;->a:Lrx/f;

    .line 85
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Laup;

    iget-object v2, p0, Laue;->c:Lauq;

    invoke-direct {v1, v2}, Laup;-><init>(Lauq;)V

    .line 86
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    const/4 v1, 0x1

    .line 87
    invoke-virtual {v0, v1}, Lrx/c;->b(I)Lcwb;

    move-result-object v0

    .line 88
    iget-object v1, p0, Laue;->e:Lcwv;

    invoke-virtual {v0}, Lcwb;->a()Lrx/j;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcwv;->a(Lrx/j;)V

    .line 89
    return-object v0
.end method

.method protected abstract b(Ljava/lang/Object;)Ljava/io/Closeable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)TC;"
        }
    .end annotation
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Laue;->e:Lcwv;

    invoke-virtual {v0}, Lcwv;->B_()V

    .line 95
    iget-object v0, p0, Laue;->c:Lauq;

    invoke-virtual {v0}, Lauq;->b()V

    .line 96
    return-void
.end method
