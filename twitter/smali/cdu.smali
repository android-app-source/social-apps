.class public Lcdu;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcdu$b;,
        Lcdu$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcdu;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcdu;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:J

.field public final h:J

.field public final i:J

.field public final j:Lcea;

.field public final k:J

.field public final l:Ljava/lang/String;

.field public final m:Lcef;

.field public final n:J

.field public final o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcdu$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcdu$b;-><init>(Lcdu$1;)V

    sput-object v0, Lcdu;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcdu$a;)V
    .locals 4

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p1}, Lcdu$a;->a(Lcdu$a;)Lcea;

    move-result-object v0

    iput-object v0, p0, Lcdu;->j:Lcea;

    .line 37
    invoke-static {p1}, Lcdu$a;->b(Lcdu$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcdu;->h:J

    .line 38
    invoke-static {p1}, Lcdu$a;->c(Lcdu$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcdu;->i:J

    .line 39
    invoke-static {p1}, Lcdu$a;->d(Lcdu$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcdu;->g:J

    .line 40
    invoke-static {p1}, Lcdu$a;->e(Lcdu$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcdu;->d:Ljava/lang/String;

    .line 41
    invoke-static {p1}, Lcdu$a;->f(Lcdu$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcdu;->e:Ljava/lang/String;

    .line 42
    invoke-static {p1}, Lcdu$a;->g(Lcdu$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcdu;->c:Ljava/lang/String;

    .line 43
    invoke-static {p1}, Lcdu$a;->h(Lcdu$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcdu;->f:Ljava/lang/String;

    .line 44
    invoke-static {p1}, Lcdu$a;->i(Lcdu$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcdu;->b:J

    .line 45
    invoke-static {p1}, Lcdu$a;->j(Lcdu$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcdu;->k:J

    .line 46
    invoke-static {p1}, Lcdu$a;->k(Lcdu$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcdu;->l:Ljava/lang/String;

    .line 47
    invoke-static {p1}, Lcdu$a;->l(Lcdu$a;)J

    move-result-wide v0

    invoke-static {p1}, Lcdu$a;->m(Lcdu$a;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcef;->a(JJ)Lcef;

    move-result-object v0

    iput-object v0, p0, Lcdu;->m:Lcef;

    .line 48
    invoke-static {p1}, Lcdu$a;->n(Lcdu$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcdu;->n:J

    .line 49
    invoke-static {p1}, Lcdu$a;->o(Lcdu$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcdu;->o:Z

    .line 50
    return-void
.end method

.method synthetic constructor <init>(Lcdu$a;Lcdu$1;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcdu;-><init>(Lcdu$a;)V

    return-void
.end method


# virtual methods
.method public a(Lcdu;)I
    .locals 4

    .prologue
    .line 54
    iget-wide v0, p0, Lcdu;->n:J

    iget-wide v2, p1, Lcdu;->n:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 55
    const/4 v0, 0x0

    .line 57
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcdu;->n:J

    iget-wide v2, p1, Lcdu;->n:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 16
    check-cast p1, Lcdu;

    invoke-virtual {p0, p1}, Lcdu;->a(Lcdu;)I

    move-result v0

    return v0
.end method
