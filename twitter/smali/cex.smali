.class public Lcex;
.super Lcfd;
.source "Twttr"

# interfaces
.implements Lcey;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcex$b;,
        Lcex$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/Boolean;

.field public final d:Lcez;

.field public final e:Lcom/twitter/model/moments/w;

.field public final f:Lcom/twitter/model/moments/MomentVisibilityMode;

.field public final g:Ljava/lang/Boolean;


# direct methods
.method constructor <init>(Lcex$a;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcfd;-><init>()V

    .line 32
    iget-object v0, p1, Lcex$a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcex;->a:Ljava/lang/String;

    .line 33
    iget-object v0, p1, Lcex$a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcex;->b:Ljava/lang/String;

    .line 34
    iget-object v0, p1, Lcex$a;->c:Ljava/lang/Boolean;

    iput-object v0, p0, Lcex;->c:Ljava/lang/Boolean;

    .line 35
    iget-object v0, p1, Lcex$a;->d:Lcez;

    iput-object v0, p0, Lcex;->d:Lcez;

    .line 36
    iget-object v0, p1, Lcex$a;->e:Lcom/twitter/model/moments/w;

    iput-object v0, p0, Lcex;->e:Lcom/twitter/model/moments/w;

    .line 37
    iget-object v0, p1, Lcex$a;->f:Lcom/twitter/model/moments/MomentVisibilityMode;

    iput-object v0, p0, Lcex;->f:Lcom/twitter/model/moments/MomentVisibilityMode;

    .line 38
    iget-object v0, p1, Lcex$a;->g:Ljava/lang/Boolean;

    iput-object v0, p0, Lcex;->g:Ljava/lang/Boolean;

    .line 39
    return-void
.end method


# virtual methods
.method public a()Lcex;
    .locals 0

    .prologue
    .line 44
    return-object p0
.end method
