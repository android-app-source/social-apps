.class public Lajz;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/b;
.implements Lcom/twitter/library/widget/renderablecontent/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lajz$b;,
        Lajz$c;,
        Lajz$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/library/widget/b;",
        "Lcom/twitter/library/widget/renderablecontent/c",
        "<",
        "Lajz$a;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/library/widget/renderablecontent/DisplayMode;",
            ">;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/twitter/android/revenue/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/revenue/h",
            "<",
            "Lafu;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/model/core/Tweet;

.field private final d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

.field private final e:Lafw;

.field private final f:Lajz$b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Landroid/util/LruCache;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    sput-object v0, Lajz;->a:Landroid/util/LruCache;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V
    .locals 1

    .prologue
    .line 69
    invoke-static {}, Lbss;->a()Lbsp;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lajz;-><init>(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lbsp;)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lbsp;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/twitter/model/core/Tweet;",
            "Lcom/twitter/library/widget/renderablecontent/DisplayMode;",
            "Lbsp",
            "<",
            "Lbsq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p2, p0, Lajz;->c:Lcom/twitter/model/core/Tweet;

    .line 75
    iput-object p3, p0, Lajz;->d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 77
    new-instance v4, Lajz$c;

    .line 78
    invoke-direct {p0, p1}, Lajz;->a(Landroid/app/Activity;)Lcom/twitter/android/cs;

    move-result-object v0

    .line 79
    invoke-direct {p0, p1}, Lajz;->b(Landroid/app/Activity;)Lcom/twitter/android/revenue/m;

    move-result-object v1

    invoke-direct {v4, p1, v0, v1}, Lajz$c;-><init>(Landroid/app/Activity;Lcom/twitter/android/cs;Lcom/twitter/android/revenue/m;)V

    .line 80
    new-instance v0, Lcom/twitter/android/revenue/p;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/revenue/p;-><init>(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/library/view/d;Lbsp;)V

    iput-object v0, p0, Lajz;->b:Lcom/twitter/android/revenue/h;

    .line 81
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 82
    new-instance v2, Lafr;

    new-instance v3, Laft;

    .line 83
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4, v0, v1}, Laft;-><init>(Landroid/content/Context;J)V

    invoke-direct {v2, v0, v1, v3}, Lafr;-><init>(JLauj;)V

    iput-object v2, p0, Lajz;->e:Lafw;

    .line 84
    new-instance v0, Lajz$b;

    iget-object v1, p0, Lajz;->b:Lcom/twitter/android/revenue/h;

    iget-object v2, p0, Lajz;->c:Lcom/twitter/model/core/Tweet;

    iget-object v3, p0, Lajz;->d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-direct {v0, v1, v2, v3}, Lajz$b;-><init>(Lcom/twitter/android/revenue/h;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    iput-object v0, p0, Lajz;->f:Lajz$b;

    .line 85
    iget-object v0, p0, Lajz;->e:Lafw;

    iget-wide v2, p2, Lcom/twitter/model/core/Tweet;->af:J

    invoke-interface {v0, v2, v3}, Lafw;->a(J)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lajz;->f:Lajz$b;

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 86
    return-void
.end method

.method private a(Landroid/app/Activity;)Lcom/twitter/android/cs;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 90
    new-instance v0, Lcom/twitter/android/cs;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/cs;-><init>(Landroid/app/Activity;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    return-object v0
.end method

.method private b(Landroid/app/Activity;)Lcom/twitter/android/revenue/m;
    .locals 3

    .prologue
    .line 95
    new-instance v0, Lcom/twitter/android/revenue/n;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/twitter/android/revenue/n;-><init>(Landroid/app/Activity;Lcom/twitter/library/client/Session;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    return-object v0
.end method

.method static synthetic f()Landroid/util/LruCache;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lajz;->a:Landroid/util/LruCache;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 104
    iget-object v0, p0, Lajz;->e:Lafw;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 105
    iget-object v0, p0, Lajz;->f:Lajz$b;

    invoke-virtual {v0}, Lajz$b;->B_()V

    .line 106
    iget-object v0, p0, Lajz;->b:Lcom/twitter/android/revenue/h;

    invoke-interface {v0}, Lcom/twitter/android/revenue/h;->a()I

    move-result v0

    .line 107
    if-lez v0, :cond_0

    .line 108
    sget-object v1, Lajz;->a:Landroid/util/LruCache;

    iget-object v2, p0, Lajz;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v2, Lcom/twitter/model/core/Tweet;->af:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p0, Lajz;->d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-static {v2, v3}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    :cond_0
    iget-object v0, p0, Lajz;->b:Lcom/twitter/android/revenue/h;

    invoke-interface {v0}, Lcom/twitter/android/revenue/h;->c()V

    .line 111
    return-void
.end method

.method public a(Lajz$a;)V
    .locals 0

    .prologue
    .line 100
    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 147
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 51
    check-cast p1, Lajz$a;

    invoke-virtual {p0, p1}, Lajz;->a(Lajz$a;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 128
    return-void
.end method

.method public ai_()V
    .locals 0

    .prologue
    .line 124
    return-void
.end method

.method public aj_()V
    .locals 0

    .prologue
    .line 143
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 115
    return-void
.end method

.method public b(Z)V
    .locals 4

    .prologue
    .line 132
    if-nez p1, :cond_0

    .line 133
    sget-object v0, Lajz;->a:Landroid/util/LruCache;

    iget-object v1, p0, Lajz;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v1, Lcom/twitter/model/core/Tweet;->af:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lajz;->d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-static {v1, v2}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    :cond_0
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 139
    return-void
.end method

.method public e()Landroid/view/View;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lajz;->b:Lcom/twitter/android/revenue/h;

    invoke-interface {v0}, Lcom/twitter/android/revenue/h;->b()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getAutoPlayableItem()Lcom/twitter/library/widget/a;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lajz;->b:Lcom/twitter/android/revenue/h;

    instance-of v0, v0, Lcom/twitter/library/widget/b;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lajz;->b:Lcom/twitter/android/revenue/h;

    check-cast v0, Lcom/twitter/library/widget/b;

    invoke-interface {v0}, Lcom/twitter/library/widget/b;->getAutoPlayableItem()Lcom/twitter/library/widget/a;

    move-result-object v0

    .line 156
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/library/widget/a;->j:Lcom/twitter/library/widget/a;

    goto :goto_0
.end method
