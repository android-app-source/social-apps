.class public Lbkx;
.super Lbkq;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbkx$a;
    }
.end annotation


# instance fields
.field public final b:Lcom/twitter/model/core/MediaEntity;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/model/core/MediaEntity;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lbkq;-><init>(Lcom/twitter/library/av/playback/AVDataSource;)V

    .line 45
    iput-object p2, p0, Lbkx;->b:Lcom/twitter/model/core/MediaEntity;

    .line 46
    return-void
.end method

.method private b(Lcom/twitter/util/network/c;Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/network/c;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/p;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 149
    invoke-virtual {p0, p1, p2}, Lbkx;->a(Lcom/twitter/util/network/c;Ljava/util/List;)Lcom/twitter/util/collection/k;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/k;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/twitter/library/av/g;)Lcom/twitter/model/av/AVMediaPlaylist;
    .locals 6

    .prologue
    .line 61
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v0

    invoke-virtual {v0}, Lcrr;->e()Lcom/twitter/util/network/c;

    move-result-object v1

    .line 63
    iget-object v0, p0, Lbkx;->b:Lcom/twitter/model/core/MediaEntity;

    invoke-static {v0}, Lcom/twitter/model/util/c;->c(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    invoke-virtual {p0, p2}, Lbkx;->a(Lcom/twitter/library/av/g;)Lcom/twitter/model/av/DynamicAdInfo;

    move-result-object v2

    .line 65
    invoke-virtual {p0, v2, v1}, Lbkx;->a(Lcom/twitter/model/av/DynamicAdInfo;Lcom/twitter/util/network/c;)Lcom/twitter/model/av/Video;

    move-result-object v3

    .line 67
    new-instance v0, Lcom/twitter/model/av/MediaEntityPlaylist;

    iget-object v4, v1, Lcom/twitter/util/network/c;->b:Ljava/lang/String;

    iget-object v5, p0, Lbkx;->b:Lcom/twitter/model/core/MediaEntity;

    .line 69
    invoke-virtual {p0, v5, v1}, Lbkx;->a(Lcom/twitter/model/core/MediaEntity;Lcom/twitter/util/network/c;)Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lbkx;->b:Lcom/twitter/model/core/MediaEntity;

    .line 68
    invoke-static {v1, v5}, Lcom/twitter/model/av/Video;->a(Ljava/lang/String;Lcom/twitter/model/core/MediaEntity;)Lcom/twitter/model/av/Video;

    move-result-object v1

    invoke-direct {v0, v4, v1, v3, v2}, Lcom/twitter/model/av/MediaEntityPlaylist;-><init>(Ljava/lang/String;Lcom/twitter/model/av/Video;Lcom/twitter/model/av/Video;Lcom/twitter/model/av/DynamicAdInfo;)V

    .line 75
    const-string/jumbo v1, "video_consumption_prefetch_enabled"

    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    :try_start_0
    invoke-virtual {v0}, Lcom/twitter/model/av/AVMediaPlaylist;->c()Lcom/twitter/model/av/AVMedia;

    move-result-object v1

    .line 82
    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/twitter/model/av/b;->b(Lcom/twitter/model/av/AVMedia;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 83
    invoke-interface {v1}, Lcom/twitter/model/av/AVMedia;->b()Ljava/lang/String;

    move-result-object v1

    .line 84
    new-instance v2, Lcom/twitter/library/network/k;

    invoke-direct {v2, p1, v1}, Lcom/twitter/library/network/k;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    new-instance v1, Lbkx$a;

    const/4 v3, 0x0

    invoke-direct {v1, v3}, Lbkx$a;-><init>(Lbkx$1;)V

    .line 85
    invoke-virtual {v2, v1}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/j;)Lcom/twitter/library/network/k;

    move-result-object v1

    const/16 v2, 0x3a98

    .line 86
    invoke-virtual {v1, v2}, Lcom/twitter/library/network/k;->a(I)Lcom/twitter/library/network/k;

    move-result-object v1

    .line 87
    invoke-virtual {v1}, Lcom/twitter/library/network/k;->a()Lcom/twitter/network/HttpOperation;

    move-result-object v1

    .line 88
    const-string/jumbo v2, "Range"

    const-string/jumbo v3, "bytes=0-1"

    invoke-virtual {v1, v2, v3}, Lcom/twitter/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/network/HttpOperation;

    .line 89
    invoke-virtual {v1}, Lcom/twitter/network/HttpOperation;->c()Lcom/twitter/network/HttpOperation;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :cond_0
    :goto_0
    return-object v0

    .line 72
    :cond_1
    new-instance v0, Lcom/twitter/model/av/InvalidPlaylist;

    invoke-direct {v0}, Lcom/twitter/model/av/InvalidPlaylist;-><init>()V

    goto :goto_0

    .line 93
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method protected a(Landroid/content/Context;)Lcom/twitter/network/j;
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Lcom/twitter/model/core/MediaEntity;Lcom/twitter/util/network/c;)Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 139
    iget-object v0, p1, Lcom/twitter/model/core/MediaEntity;->p:Lcom/twitter/model/core/o;

    if-nez v0, :cond_0

    .line 140
    const-string/jumbo v0, ""

    .line 143
    :goto_0
    return-object v0

    .line 142
    :cond_0
    iget-object v0, p1, Lcom/twitter/model/core/MediaEntity;->p:Lcom/twitter/model/core/o;

    iget-object v0, v0, Lcom/twitter/model/core/o;->d:Ljava/util/List;

    .line 143
    invoke-direct {p0, p2, v0}, Lbkx;->b(Lcom/twitter/util/network/c;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Landroid/content/Context;Ljava/util/Map;Lcom/twitter/util/network/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/twitter/util/network/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 134
    return-void
.end method

.method protected a(Landroid/net/Uri$Builder;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri$Builder;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 122
    return-void
.end method

.method protected b(Lcom/twitter/network/j;Lcom/twitter/network/HttpOperation;Ljava/util/Map;Lcom/twitter/model/av/DynamicAdInfo;)Lcom/twitter/model/av/AVMediaPlaylist;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/j;",
            "Lcom/twitter/network/HttpOperation;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/twitter/model/av/DynamicAdInfo;",
            ")",
            "Lcom/twitter/model/av/AVMediaPlaylist;"
        }
    .end annotation

    .prologue
    .line 102
    const/4 v0, 0x0

    return-object v0
.end method
