.class public final Lcgc$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcgc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcgc;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/Boolean;

.field private h:Ljava/lang/Integer;

.field private i:Ljava/lang/Integer;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcgc$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcgc$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcgc$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcgc$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcgc$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcgc$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcgc$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcgc$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcgc$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcgc$a;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcgc$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcgc$a;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcgc$a;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcgc$a;->g:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic h(Lcgc$a;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcgc$a;->h:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic i(Lcgc$a;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcgc$a;->i:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic j(Lcgc$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcgc$a;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcgc$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcgc$a;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcgc$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcgc$a;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic m(Lcgc$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcgc$a;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic n(Lcgc$a;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcgc$a;->n:Ljava/lang/Boolean;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Boolean;)Lcgc$a;
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcgc$a;->g:Ljava/lang/Boolean;

    .line 104
    return-object p0
.end method

.method public a(Ljava/lang/Integer;)Lcgc$a;
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcgc$a;->h:Ljava/lang/Integer;

    .line 110
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcgc$a;
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcgc$a;->j:Ljava/lang/String;

    .line 122
    return-object p0
.end method

.method public b(Ljava/lang/Boolean;)Lcgc$a;
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcgc$a;->n:Ljava/lang/Boolean;

    .line 146
    return-object p0
.end method

.method public b(Ljava/lang/Integer;)Lcgc$a;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcgc$a;->i:Ljava/lang/Integer;

    .line 116
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcgc$a;
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcgc$a;->k:Ljava/lang/String;

    .line 128
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, Lcgc$a;->e()Lcgc;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcgc$a;
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcgc$a;->l:Ljava/lang/String;

    .line 134
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcgc$a;
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcgc$a;->m:Ljava/lang/String;

    .line 140
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcgc$a;
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcgc$a;->a:Ljava/lang/String;

    .line 152
    return-object p0
.end method

.method protected e()Lcgc;
    .locals 2

    .prologue
    .line 188
    new-instance v0, Lcgc;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcgc;-><init>(Lcgc$a;Lcgc$1;)V

    return-object v0
.end method

.method public f(Ljava/lang/String;)Lcgc$a;
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcgc$a;->b:Ljava/lang/String;

    .line 158
    return-object p0
.end method

.method public g(Ljava/lang/String;)Lcgc$a;
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcgc$a;->c:Ljava/lang/String;

    .line 164
    return-object p0
.end method

.method public h(Ljava/lang/String;)Lcgc$a;
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcgc$a;->d:Ljava/lang/String;

    .line 170
    return-object p0
.end method

.method public i(Ljava/lang/String;)Lcgc$a;
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcgc$a;->e:Ljava/lang/String;

    .line 176
    return-object p0
.end method

.method public j(Ljava/lang/String;)Lcgc$a;
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcgc$a;->f:Ljava/lang/String;

    .line 182
    return-object p0
.end method
