.class Lael$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lael;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lael;


# direct methods
.method constructor <init>(Lael;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lael$1;->a:Lael;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 65
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lael$a;

    .line 66
    iget-object v2, v0, Lael$a;->d:Lcom/twitter/library/widget/TweetView;

    .line 67
    invoke-virtual {v2}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/model/core/Tweet;

    move-result-object v3

    .line 68
    if-eqz v3, :cond_0

    .line 69
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 70
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 71
    iget-object v4, p0, Lael$1;->a:Lael;

    invoke-static {v4}, Lael;->a(Lael;)Landroid/app/Activity;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v1, v4, v3, v5}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 72
    new-array v4, v10, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lael$1;->a:Lael;

    .line 73
    invoke-static {v6}, Lael;->b(Lael;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v6

    const-string/jumbo v7, "tweet"

    const-string/jumbo v8, "tweet"

    const-string/jumbo v9, "click"

    invoke-static {v6, v7, v8, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v4, p0, Lael$1;->a:Lael;

    .line 74
    invoke-static {v4}, Lael;->b(Lael;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 75
    invoke-virtual {v2}, Lcom/twitter/library/widget/TweetView;->getScribeItem()Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    .line 72
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 76
    new-instance v1, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    .line 77
    invoke-virtual {v1, v10}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-wide v4, v3, Lcom/twitter/model/core/Tweet;->t:J

    .line 78
    invoke-virtual {v1, v4, v5}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v2, p0, Lael$1;->a:Lael;

    .line 79
    invoke-static {v2}, Lael;->b(Lael;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v2, p0, Lael$1;->a:Lael;

    .line 80
    invoke-static {v2}, Lael;->b(Lael;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v0, v0, Lael$a;->a:Ljava/lang/String;

    .line 81
    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->d(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 82
    new-instance v1, Lcom/twitter/android/TweetActivity$a;

    iget-object v2, p0, Lael$1;->a:Lael;

    invoke-static {v2}, Lael;->a(Lael;)Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/android/TweetActivity$a;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lael$1;->a:Lael;

    .line 83
    invoke-static {v2}, Lael;->c(Lael;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/TweetActivity$a;->a(Z)Lcom/twitter/android/TweetActivity$a;

    move-result-object v1

    .line 84
    invoke-virtual {v1, v0}, Lcom/twitter/android/TweetActivity$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/TweetActivity$a;

    move-result-object v0

    .line 85
    invoke-virtual {v0, v3}, Lcom/twitter/android/TweetActivity$a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/TweetActivity$a;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Lcom/twitter/android/TweetActivity$a;->b()V

    .line 88
    :cond_0
    return-void
.end method
