.class public Laff;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laas;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laff$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Laas",
        "<",
        "Lafe;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Laat;

.field private final b:Lafc;

.field private c:Laff$a;


# direct methods
.method public constructor <init>(Laat;Lafc;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Laff;->a:Laat;

    .line 49
    iput-object p2, p0, Laff;->b:Lafc;

    .line 50
    return-void
.end method

.method static synthetic a(Laff;)Laat;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Laff;->a:Laat;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Laat;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/library/client/Session;)Laff;
    .locals 4

    .prologue
    .line 33
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    .line 35
    invoke-virtual {p3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v1

    .line 36
    new-instance v2, Lais;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v0, p2, v3}, Lais;-><init>(Lcom/twitter/library/provider/t;Lcom/twitter/library/client/p;Lcom/twitter/model/core/TwitterUser;Lcgi;)V

    .line 38
    new-instance v0, Laio;

    invoke-direct {v0, v2}, Laio;-><init>(Lair;)V

    .line 41
    new-instance v1, Lafd;

    invoke-direct {v1, p0, p3, v0}, Lafd;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lain;)V

    .line 43
    new-instance v0, Laff;

    invoke-direct {v0, p1, v1}, Laff;-><init>(Laat;Lafc;)V

    return-object v0
.end method

.method static synthetic a(Laff;ZZZ)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Laff;->a(ZZZ)V

    return-void
.end method

.method private a(ZZZ)V
    .locals 2

    .prologue
    .line 109
    if-nez p1, :cond_0

    if-nez p2, :cond_1

    .line 110
    :cond_0
    iget-object v0, p0, Laff;->a:Laat;

    invoke-virtual {v0}, Laat;->c()V

    .line 120
    :goto_0
    return-void

    .line 112
    :cond_1
    iget-object v0, p0, Laff;->a:Laat;

    invoke-virtual {v0}, Laat;->b()V

    .line 113
    iget-object v0, p0, Laff;->a:Laat;

    invoke-virtual {v0, p3}, Laat;->a(Z)V

    .line 114
    if-eqz p3, :cond_2

    .line 115
    iget-object v0, p0, Laff;->a:Laat;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Laat;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 117
    :cond_2
    iget-object v0, p0, Laff;->a:Laat;

    const v1, 0x7f0a04a5

    invoke-virtual {v0, v1}, Laat;->a(I)V

    goto :goto_0
.end method

.method static synthetic b(Laff;)Lafc;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Laff;->b:Lafc;

    return-object v0
.end method

.method static synthetic c(Laff;)Laff$a;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Laff;->c:Laff$a;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x2

    return v0
.end method

.method public bridge synthetic a(Laar;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lafe;

    invoke-virtual {p0, p1}, Laff;->a(Lafe;)V

    return-void
.end method

.method public a(Lafe;)V
    .locals 5

    .prologue
    .line 80
    invoke-virtual {p1}, Lafe;->a()J

    move-result-wide v0

    .line 81
    invoke-virtual {p1}, Lafe;->d()Z

    move-result v2

    .line 82
    invoke-virtual {p1}, Lafe;->c()Z

    move-result v3

    .line 83
    invoke-virtual {p1}, Lafe;->b()Z

    move-result v4

    invoke-direct {p0, v2, v3, v4}, Laff;->a(ZZZ)V

    .line 85
    iget-object v3, p0, Laff;->a:Laat;

    new-instance v4, Laff$1;

    invoke-direct {v4, p0, v0, v1, v2}, Laff$1;-><init>(Laff;JZ)V

    invoke-virtual {v3, v4}, Laat;->a(Landroid/view/View$OnClickListener;)V

    .line 106
    return-void
.end method

.method public a(Laff$a;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Laff;->c:Laff$a;

    .line 54
    return-void
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Laff;->a:Laat;

    invoke-virtual {v0}, Laat;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Laff;->a:Laat;

    invoke-virtual {v0}, Laat;->b()V

    .line 71
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Laff;->a:Laat;

    invoke-virtual {v0}, Laat;->c()V

    .line 76
    return-void
.end method
