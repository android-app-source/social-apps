.class public Lasi;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lask;


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    if-gtz p1, :cond_0

    .line 15
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "max must be greater than zero"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 18
    :cond_0
    iput p1, p0, Lasi;->a:I

    .line 19
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 23
    invoke-static {p2}, Lcom/twitter/util/y;->e(Ljava/lang/String;)I

    move-result v0

    iget v1, p0, Lasi;->a:I

    if-le v0, v1, :cond_0

    .line 24
    const/4 v0, 0x1

    .line 27
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
