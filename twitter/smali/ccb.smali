.class public abstract Lccb;
.super Lcbz;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lccb$a;
    }
.end annotation


# instance fields
.field private final c:J

.field private final d:Z

.field private final e:Ljava/lang/String;


# direct methods
.method constructor <init>(Lccb$a;)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcbz;-><init>(Lcbz$a;)V

    .line 24
    invoke-static {p1}, Lccb$a;->a(Lccb$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lccb;->c:J

    .line 25
    invoke-static {p1}, Lccb$a;->b(Lccb$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lccb;->e:Ljava/lang/String;

    .line 26
    const-string/jumbo v0, "enabled"

    const-class v1, Ljava/lang/Boolean;

    invoke-virtual {p0, v0, v1}, Lccb;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 27
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lccb;->d:Z

    .line 28
    return-void

    .line 27
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lccb;)Z
    .locals 4

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcbz;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lccb;->c:J

    iget-wide v2, p1, Lccb;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lccb;->d:Z

    iget-boolean v1, p1, Lccb;->d:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lccb;->e:Ljava/lang/String;

    iget-object v1, p1, Lccb;->e:Ljava/lang/String;

    .line 53
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 50
    :goto_0
    return v0

    .line 53
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 46
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lccb;

    if-eqz v0, :cond_1

    check-cast p1, Lccb;

    invoke-direct {p0, p1}, Lccb;->a(Lccb;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-super {p0}, Lcbz;->hashCode()I

    move-result v0

    .line 59
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lccb;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 60
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lccb;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    .line 61
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lccb;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lccb;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 62
    return v0

    :cond_1
    move v0, v1

    .line 60
    goto :goto_0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 32
    invoke-super {p0}, Lcbz;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lccb;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()J
    .locals 2

    .prologue
    .line 36
    iget-wide v0, p0, Lccb;->c:J

    return-wide v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lccb;->e:Ljava/lang/String;

    return-object v0
.end method
