.class public final Lbqh;
.super Lbqg;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/util/android/f;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/util/android/f;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lbqg;-><init>()V

    .line 42
    iput-object p1, p0, Lbqh;->b:Landroid/content/Context;

    .line 43
    iput-object p2, p0, Lbqh;->a:Lcom/twitter/util/android/f;

    .line 44
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Z)V
    .locals 4

    .prologue
    .line 93
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 99
    :goto_0
    return-void

    .line 96
    :cond_0
    new-instance v0, Lcom/twitter/util/a;

    iget-object v1, p0, Lbqh;->b:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 97
    invoke-virtual {v0}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "location_enabled"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Z)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lbqh;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 117
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "location"

    .line 118
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 119
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 120
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;)Z
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lbqh;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lbqh;->b(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {p0}, Lbqh;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbqh;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 63
    :goto_0
    return v0

    .line 64
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lbqh;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbqh;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbqh;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcom/twitter/library/client/Session;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 75
    if-nez p1, :cond_1

    .line 79
    :cond_0
    :goto_0
    return v0

    .line 78
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v1

    .line 79
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/model/account/UserSettings;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Lcom/twitter/library/client/Session;Z)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 124
    if-nez p1, :cond_1

    .line 137
    :cond_0
    :goto_0
    return v0

    .line 127
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v1

    .line 128
    if-eqz v1, :cond_0

    .line 131
    invoke-virtual {v1}, Lcom/twitter/model/account/UserSettings;->g()Z

    move-result v2

    if-eq v2, p2, :cond_2

    .line 132
    iput-boolean p2, v1, Lcom/twitter/model/account/UserSettings;->c:Z

    .line 133
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v2

    iget-object v3, p0, Lbqh;->b:Landroid/content/Context;

    const/4 v4, 0x0

    .line 134
    invoke-static {v3, p1, v1, v0, v4}, Lbbg;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;ZLjava/lang/String;)Lbbg;

    move-result-object v0

    .line 133
    invoke-virtual {v2, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 137
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lbqh;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbqh;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbqh;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lcom/twitter/library/client/Session;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 84
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    .line 88
    :goto_0
    return v0

    .line 87
    :cond_0
    new-instance v1, Lcom/twitter/util/a;

    iget-object v2, p0, Lbqh;->b:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v1, v2, v4, v5}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 88
    const-string/jumbo v2, "location_enabled"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/util/a;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lbqh;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbqh;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbqh;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 3

    .prologue
    .line 69
    iget-object v0, p0, Lbqh;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "location"

    const/4 v2, 0x0

    .line 70
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 69
    return v0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lbqh;->b:Landroid/content/Context;

    const-string/jumbo v1, "location"

    .line 104
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 105
    const-string/jumbo v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "network"

    .line 106
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 105
    :goto_0
    return v0

    .line 106
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 5

    .prologue
    .line 111
    iget-object v0, p0, Lbqh;->a:Lcom/twitter/util/android/f;

    iget-object v1, p0, Lbqh;->b:Landroid/content/Context;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
