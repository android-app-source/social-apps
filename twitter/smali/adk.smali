.class public Ladk;
.super Lckb$a;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/widget/q;


# direct methods
.method constructor <init>(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lckb$a;-><init>(Landroid/view/View;)V

    .line 32
    new-instance v0, Lcom/twitter/android/widget/q;

    invoke-direct {v0, p1}, Lcom/twitter/android/widget/q;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Ladk;->a:Lcom/twitter/android/widget/q;

    .line 33
    iget-object v0, p0, Ladk;->a:Lcom/twitter/android/widget/q;

    invoke-virtual {v0}, Lcom/twitter/android/widget/q;->e()Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    sget v2, Lcni;->a:F

    .line 34
    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 35
    const v0, 0x7f130044

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 36
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f110190

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 38
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Ladk;
    .locals 2

    .prologue
    .line 25
    const v0, 0x7f040215

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 27
    new-instance v1, Ladk;

    invoke-direct {v1, v0}, Ladk;-><init>(Landroid/view/ViewGroup;)V

    return-object v1
.end method


# virtual methods
.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Ladk;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ladk;->a:Lcom/twitter/android/widget/q;

    invoke-virtual {v0}, Lcom/twitter/android/widget/q;->e()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Ladk;->a:Lcom/twitter/android/widget/q;

    invoke-virtual {v0}, Lcom/twitter/android/widget/q;->f()Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/twitter/util/ui/j;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 46
    return-void
.end method
