.class public Lavw;
.super Lcom/twitter/database/hydrator/a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/database/hydrator/a",
        "<",
        "Lcdu;",
        "Lazj$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/twitter/database/hydrator/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcdu;Lazj$a;)Lazj$a;
    .locals 6

    .prologue
    .line 15
    iget-wide v0, p1, Lcdu;->h:J

    .line 16
    invoke-interface {p2, v0, v1}, Lazj$a;->b(J)Lcom/twitter/database/model/n$c;

    move-result-object v0

    check-cast v0, Lazj$a;

    iget-object v1, p1, Lcdu;->c:Ljava/lang/String;

    .line 17
    invoke-interface {v0, v1}, Lazj$a;->e(Ljava/lang/String;)Lazj$a;

    move-result-object v0

    iget-object v1, p1, Lcdu;->m:Lcef;

    iget-wide v2, v1, Lcef;->b:J

    .line 18
    invoke-interface {v0, v2, v3}, Lazj$a;->g(J)Lazj$a;

    move-result-object v2

    iget-object v0, p1, Lcdu;->m:Lcef;

    iget-wide v0, v0, Lcef;->c:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcdu;->m:Lcef;

    iget-wide v0, v0, Lcef;->c:J

    .line 19
    :goto_0
    invoke-interface {v2, v0, v1}, Lazj$a;->h(J)Lazj$a;

    move-result-object v0

    iget-wide v2, p1, Lcdu;->n:J

    .line 22
    invoke-interface {v0, v2, v3}, Lazj$a;->i(J)Lazj$a;

    move-result-object v0

    iget-wide v2, p1, Lcdu;->g:J

    .line 23
    invoke-interface {v0, v2, v3}, Lazj$a;->d(J)Lazj$a;

    move-result-object v0

    iget-wide v2, p1, Lcdu;->b:J

    .line 24
    invoke-interface {v0, v2, v3}, Lazj$a;->c(J)Lazj$a;

    move-result-object v0

    iget-wide v2, p1, Lcdu;->k:J

    .line 25
    invoke-interface {v0, v2, v3}, Lazj$a;->e(J)Lazj$a;

    move-result-object v0

    iget-wide v2, p1, Lcdu;->i:J

    .line 26
    invoke-interface {v0, v2, v3}, Lazj$a;->j(J)Lazj$a;

    move-result-object v0

    iget-object v1, p1, Lcdu;->e:Ljava/lang/String;

    .line 27
    invoke-interface {v0, v1}, Lazj$a;->b(Ljava/lang/String;)Lazj$a;

    move-result-object v0

    iget-object v1, p1, Lcdu;->d:Ljava/lang/String;

    .line 28
    invoke-interface {v0, v1}, Lazj$a;->a(Ljava/lang/String;)Lazj$a;

    move-result-object v0

    iget-object v1, p1, Lcdu;->f:Ljava/lang/String;

    .line 29
    invoke-interface {v0, v1}, Lazj$a;->c(Ljava/lang/String;)Lazj$a;

    move-result-object v0

    iget-object v1, p1, Lcdu;->l:Ljava/lang/String;

    .line 30
    invoke-interface {v0, v1}, Lazj$a;->d(Ljava/lang/String;)Lazj$a;

    move-result-object v0

    iget-object v1, p1, Lcdu;->j:Lcea;

    .line 31
    invoke-interface {v0, v1}, Lazj$a;->a(Lcea;)Lazj$a;

    move-result-object v0

    .line 15
    return-object v0

    .line 18
    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    check-cast p1, Lcdu;

    check-cast p2, Lazj$a;

    invoke-virtual {p0, p1, p2}, Lavw;->a(Lcdu;Lazj$a;)Lazj$a;

    move-result-object v0

    return-object v0
.end method
