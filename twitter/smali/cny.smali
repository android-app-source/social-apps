.class public Lcny;
.super Lcom/twitter/ui/widget/e;
.source "Twttr"


# instance fields
.field private final a:Lcnx;


# direct methods
.method public constructor <init>(Lcnx;Landroid/content/Context;Z)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 22
    if-eqz p3, :cond_0

    sget v0, Lckh$c;->selection_highlight_text_color:I

    :goto_0
    invoke-static {p2, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    const/16 v2, 0x66

    if-eqz p3, :cond_1

    sget v0, Lckh$c;->selection_highlight_background:I

    .line 25
    :goto_1
    invoke-static {p2, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v4

    if-eqz p3, :cond_2

    sget v0, Lckh$c;->selection_highlight_background:I

    .line 28
    :goto_2
    invoke-static {p2, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v5

    if-eqz p3, :cond_3

    sget v0, Lckh$c;->selection_highlight_background:I

    .line 31
    :goto_3
    invoke-static {p2, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    .line 24
    invoke-static {v2, v4, v5, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    .line 35
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v4, Lckh$d;->selected_item_span_left_right_padding:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v4, v0

    .line 36
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v5, Lckh$d;->selected_item_span_top_padding:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v5, v0

    .line 37
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v6, Lckh$d;->selected_item_span_bottom_padding:I

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v6, v0

    .line 39
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v7, Lckh$d;->selected_item_span_right_margin:I

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v8, v0

    move-object v0, p0

    move v7, v3

    .line 22
    invoke-direct/range {v0 .. v8}, Lcom/twitter/ui/widget/e;-><init>(IIFFFFFF)V

    .line 40
    iput-object p1, p0, Lcny;->a:Lcnx;

    .line 41
    return-void

    .line 22
    :cond_0
    sget v0, Lckh$c;->selection_text_color:I

    goto :goto_0

    :cond_1
    sget v0, Lckh$c;->selection_background:I

    goto :goto_1

    .line 25
    :cond_2
    sget v0, Lckh$c;->selection_background:I

    goto :goto_2

    .line 28
    :cond_3
    sget v0, Lckh$c;->selection_background:I

    goto :goto_3
.end method


# virtual methods
.method public a()Lcnx;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcny;->a:Lcnx;

    return-object v0
.end method
