.class public final Lccd;
.super Lcby;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lccd$b;,
        Lccd$a;
    }
.end annotation


# static fields
.field public static final b:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lccd;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Lcom/twitter/model/geo/b;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/geo/TwitterPlace;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lccd$b;

    invoke-direct {v0}, Lccd$b;-><init>()V

    sput-object v0, Lccd;->b:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lccd$a;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcby;-><init>(Lcbx$a;)V

    .line 28
    invoke-static {p1}, Lccd$a;->a(Lccd$a;)Lcom/twitter/model/geo/b;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/geo/b;

    iput-object v0, p0, Lccd;->c:Lcom/twitter/model/geo/b;

    .line 29
    invoke-static {p1}, Lccd$a;->b(Lccd$a;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lccd;->d:Ljava/util/List;

    .line 30
    return-void
.end method

.method synthetic constructor <init>(Lccd$a;Lccd$1;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lccd;-><init>(Lccd$a;)V

    return-void
.end method

.method static synthetic a(Lccd;)Lcom/twitter/model/geo/b;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lccd;->c:Lcom/twitter/model/geo/b;

    return-object v0
.end method

.method private b(Lccd;)Z
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lccd;->c:Lcom/twitter/model/geo/b;

    iget-object v1, p1, Lccd;->c:Lcom/twitter/model/geo/b;

    invoke-virtual {v0, v1}, Lcom/twitter/model/geo/b;->b(Lcom/twitter/model/geo/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lccd;->d:Ljava/util/List;

    iget-object v1, p1, Lccd;->d:Ljava/util/List;

    .line 51
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 50
    :goto_0
    return v0

    .line 51
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 45
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lccd;

    if-eqz v0, :cond_1

    check-cast p1, Lccd;

    .line 46
    invoke-direct {p0, p1}, Lccd;->b(Lccd;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 45
    :goto_0
    return v0

    .line 46
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 40
    invoke-super {p0}, Lcby;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lccd;->c:Lcom/twitter/model/geo/b;

    iget-object v2, p0, Lccd;->d:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public j()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/geo/TwitterPlace;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lccd;->d:Ljava/util/List;

    return-object v0
.end method
