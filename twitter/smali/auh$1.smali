.class Lauh$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lauh;->a(Lapb;)Lrx/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/d",
        "<",
        "Ljava/lang/Void;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lapb;

.field final synthetic b:Lauh;


# direct methods
.method constructor <init>(Lauh;Lapb;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lauh$1;->b:Lauh;

    iput-object p2, p0, Lauh$1;->a:Lapb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Void;)Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 82
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 83
    iget-object v0, p0, Lauh$1;->b:Lauh;

    invoke-static {v0}, Lauh;->a(Lauh;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v6

    .line 95
    :goto_0
    return-object v0

    .line 86
    :cond_0
    iget-object v0, p0, Lauh$1;->b:Lauh;

    invoke-static {v0}, Lauh;->b(Lauh;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lauh$1;->a:Lapb;

    iget-object v1, v1, Lapb;->d:Landroid/net/Uri;

    iget-object v2, p0, Lauh$1;->a:Lapb;

    iget-object v2, v2, Lapb;->e:[Ljava/lang/String;

    iget-object v3, p0, Lauh$1;->a:Lapb;

    iget-object v3, v3, Lapb;->a:Ljava/lang/String;

    iget-object v4, p0, Lauh$1;->a:Lapb;

    iget-object v4, v4, Lapb;->b:[Ljava/lang/String;

    iget-object v5, p0, Lauh$1;->a:Lapb;

    iget-object v5, v5, Lapb;->c:Ljava/lang/String;

    .line 87
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lauh$1;->b:Lauh;

    invoke-static {v1}, Lauh;->c(Lauh;)Ljava/util/Set;

    move-result-object v1

    monitor-enter v1

    .line 89
    :try_start_0
    iget-object v2, p0, Lauh$1;->b:Lauh;

    invoke-static {v2}, Lauh;->a(Lauh;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 90
    invoke-static {v0}, Lcqc;->a(Landroid/database/Cursor;)V

    .line 91
    monitor-exit v1

    move-object v0, v6

    goto :goto_0

    .line 93
    :cond_1
    iget-object v2, p0, Lauh$1;->b:Lauh;

    invoke-static {v2}, Lauh;->c(Lauh;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 94
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 78
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lauh$1;->a(Ljava/lang/Void;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
