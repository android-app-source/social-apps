.class public Lcgm;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcgm$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/i",
            "<",
            "Lcgm;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:J

.field public final f:J

.field public g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 12
    new-instance v0, Lcgm$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcgm$a;-><init>(Lcgm$1;)V

    sput-object v0, Lcgm;->a:Lcom/twitter/util/serialization/i;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;JJZ)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-wide p1, p0, Lcgm;->b:J

    .line 28
    iput-object p3, p0, Lcgm;->c:Ljava/lang/String;

    .line 29
    iput-object p4, p0, Lcgm;->d:Ljava/lang/String;

    .line 30
    iput-wide p5, p0, Lcgm;->e:J

    .line 31
    iput-wide p7, p0, Lcgm;->f:J

    .line 32
    iput-boolean p9, p0, Lcgm;->g:Z

    .line 33
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 11

    .prologue
    const-wide/16 v2, 0x0

    .line 22
    const-string/jumbo v4, ""

    const/4 v10, 0x1

    move-object v1, p0

    move-object v5, p1

    move-wide v6, v2

    move-wide v8, v2

    invoke-direct/range {v1 .. v10}, Lcgm;-><init>(JLjava/lang/String;Ljava/lang/String;JJZ)V

    .line 23
    return-void
.end method
