.class public Lbsb;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbsb$c;,
        Lbsb$a;,
        Lbsb$b;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/ContentResolver;

.field private final b:Lcom/twitter/library/provider/t;

.field private final c:Lbsf;

.field private final d:Lbse;

.field private final e:J


# direct methods
.method constructor <init>(Landroid/content/ContentResolver;Lcom/twitter/library/provider/t;JLbse;Lbsf;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    iput-object p1, p0, Lbsb;->a:Landroid/content/ContentResolver;

    .line 129
    iput-object p2, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    .line 130
    iput-wide p3, p0, Lbsb;->e:J

    .line 131
    iput-object p5, p0, Lbsb;->d:Lbse;

    .line 132
    iput-object p6, p0, Lbsb;->c:Lbsf;

    .line 133
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/provider/t;J)V
    .locals 9

    .prologue
    .line 120
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {p2}, Lbse;->a(Lcom/twitter/library/provider/t;)Lbse;

    move-result-object v6

    .line 121
    invoke-static {p2}, Lbsf;->a(Lcom/twitter/library/provider/t;)Lbsf;

    move-result-object v7

    move-object v1, p0

    move-object v3, p2

    move-wide v4, p3

    .line 120
    invoke-direct/range {v1 .. v7}, Lbsb;-><init>(Landroid/content/ContentResolver;Lcom/twitter/library/provider/t;JLbse;Lbsf;)V

    .line 122
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/provider/t;Lcnz;)V
    .locals 2

    .prologue
    .line 115
    invoke-virtual {p3}, Lcnz;->b()J

    move-result-wide v0

    invoke-direct {p0, p1, p2, v0, v1}, Lbsb;-><init>(Landroid/content/Context;Lcom/twitter/library/provider/t;J)V

    .line 116
    return-void
.end method

.method private a(Lcen;JJ)Landroid/content/ContentValues;
    .locals 6

    .prologue
    .line 637
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 638
    const-string/jumbo v1, "item_type"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 639
    const-string/jumbo v1, "section_id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 640
    const-string/jumbo v1, "moment_id"

    iget-object v2, p1, Lcen;->a:Lcom/twitter/model/moments/Moment;

    iget-wide v2, v2, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 641
    const-string/jumbo v1, "impression_id"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 644
    iget-object v1, p1, Lcen;->d:Lceo;

    .line 645
    if-eqz v1, :cond_1

    .line 646
    iget-object v2, v1, Lceo;->g:Lcem;

    .line 647
    if-eqz v2, :cond_0

    .line 648
    const-string/jumbo v3, "media_id"

    iget-wide v4, v2, Lcem;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 649
    const-string/jumbo v3, "media_url"

    iget-object v4, v2, Lcem;->d:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    const-string/jumbo v3, "media_size"

    iget-object v2, v2, Lcem;->c:Lcom/twitter/util/math/Size;

    sget-object v4, Lcom/twitter/util/math/Size;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v2, v4}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 652
    :cond_0
    const-string/jumbo v2, "crop_data"

    iget-object v3, v1, Lceo;->e:Lcom/twitter/model/moments/e;

    sget-object v4, Lcom/twitter/model/moments/e;->a:Lcom/twitter/util/serialization/l;

    .line 653
    invoke-static {v3, v4}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v3

    .line 652
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 654
    const-string/jumbo v2, "tweet_id"

    iget-wide v4, v1, Lceo;->k:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 655
    const-string/jumbo v1, "display_type"

    iget-object v2, p1, Lcen;->e:Lcom/twitter/model/moments/DisplayStyle;

    const-class v3, Lcom/twitter/model/moments/DisplayStyle;

    .line 657
    invoke-static {v3}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v3

    .line 656
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    .line 655
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 659
    :cond_1
    const-string/jumbo v1, "context_string"

    iget-object v2, p1, Lcen;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 660
    const-string/jumbo v1, "context_scribe_info"

    iget-object v2, p1, Lcen;->h:Lcom/twitter/model/moments/u;

    sget-object v3, Lcom/twitter/model/moments/u;->a:Lcom/twitter/util/serialization/l;

    .line 661
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    .line 660
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 662
    iget-object v1, p1, Lcen;->i:Lceg;

    if-eqz v1, :cond_2

    .line 663
    const-string/jumbo v1, "cta"

    iget-object v2, p1, Lcen;->i:Lceg;

    sget-object v3, Lceg;->a:Lcom/twitter/util/serialization/l;

    .line 664
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    .line 663
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 666
    :cond_2
    return-object v0
.end method

.method private static a(Lcom/twitter/model/moments/Moment;)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 552
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 553
    const-string/jumbo v1, "_id"

    iget-wide v2, p0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 554
    const-string/jumbo v1, "title"

    iget-object v2, p0, Lcom/twitter/model/moments/Moment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    const-string/jumbo v1, "can_subscribe"

    iget-boolean v2, p0, Lcom/twitter/model/moments/Moment;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 556
    const-string/jumbo v1, "is_live"

    iget-boolean v2, p0, Lcom/twitter/model/moments/Moment;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 557
    const-string/jumbo v1, "is_sensitive"

    iget-boolean v2, p0, Lcom/twitter/model/moments/Moment;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 558
    const-string/jumbo v1, "subcategory_string"

    iget-object v2, p0, Lcom/twitter/model/moments/Moment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    const-string/jumbo v1, "subcategory_favicon_url"

    iget-object v2, p0, Lcom/twitter/model/moments/Moment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    const-string/jumbo v1, "time_string"

    iget-object v2, p0, Lcom/twitter/model/moments/Moment;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    const-string/jumbo v1, "duration_string"

    iget-object v2, p0, Lcom/twitter/model/moments/Moment;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    const-string/jumbo v1, "is_subscribed"

    iget-boolean v2, p0, Lcom/twitter/model/moments/Moment;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 563
    const-string/jumbo v1, "description"

    iget-object v2, p0, Lcom/twitter/model/moments/Moment;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    const-string/jumbo v1, "moment_url"

    iget-object v2, p0, Lcom/twitter/model/moments/Moment;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    const-string/jumbo v1, "num_subscribers"

    iget v2, p0, Lcom/twitter/model/moments/Moment;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 566
    const-string/jumbo v1, "capsule_content_version"

    iget-wide v2, p0, Lcom/twitter/model/moments/Moment;->s:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 567
    iget-object v1, p0, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    if-eqz v1, :cond_0

    .line 568
    const-string/jumbo v1, "author_info"

    iget-object v2, p0, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    sget-object v3, Lcom/twitter/model/moments/a;->a:Lcom/twitter/util/serialization/l;

    .line 569
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    .line 568
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 571
    :cond_0
    iget-object v1, p0, Lcom/twitter/model/moments/Moment;->p:Lcgi;

    if-eqz v1, :cond_1

    .line 572
    const-string/jumbo v1, "promoted_content"

    iget-object v2, p0, Lcom/twitter/model/moments/Moment;->p:Lcgi;

    invoke-virtual {v2}, Lcgi;->g()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 574
    :cond_1
    iget-object v1, p0, Lcom/twitter/model/moments/Moment;->q:Lcom/twitter/model/moments/g;

    if-eqz v1, :cond_2

    .line 575
    const-string/jumbo v1, "event_id"

    iget-object v2, p0, Lcom/twitter/model/moments/Moment;->q:Lcom/twitter/model/moments/g;

    iget-object v2, v2, Lcom/twitter/model/moments/g;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    const-string/jumbo v1, "event_type"

    iget-object v2, p0, Lcom/twitter/model/moments/Moment;->q:Lcom/twitter/model/moments/g;

    iget-object v2, v2, Lcom/twitter/model/moments/g;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    :cond_2
    iget-object v1, p0, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    if-eqz v1, :cond_3

    .line 579
    const-string/jumbo v1, "curation_metadata"

    iget-object v2, p0, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    sget-object v3, Lcom/twitter/model/moments/f;->a:Lcom/twitter/util/serialization/l;

    .line 580
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    .line 579
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 583
    :cond_3
    const-string/jumbo v1, "is_liked"

    iget-boolean v2, p0, Lcom/twitter/model/moments/Moment;->t:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 584
    const-string/jumbo v1, "total_likes"

    iget-wide v2, p0, Lcom/twitter/model/moments/Moment;->u:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 585
    return-object v0
.end method

.method private a(Lcom/twitter/model/moments/Moment;Lceh;Ljava/util/Map;)Landroid/content/ContentValues;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/Moment;",
            "Lceh;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lbsb$b;",
            ">;)",
            "Landroid/content/ContentValues;"
        }
    .end annotation

    .prologue
    .line 591
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 592
    const-string/jumbo v0, "moment_id"

    iget-wide v2, p1, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 593
    const-string/jumbo v0, "tweet_id"

    iget-object v2, p2, Lceh;->b:Lceo;

    iget-wide v2, v2, Lceo;->k:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 594
    const-string/jumbo v0, "page_id"

    iget-object v2, p2, Lceh;->d:Lcom/twitter/model/moments/r;

    invoke-virtual {v2}, Lcom/twitter/model/moments/r;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    const-string/jumbo v0, "content_version"

    iget-wide v2, p1, Lcom/twitter/model/moments/Moment;->s:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 596
    iget-object v0, p2, Lceh;->d:Lcom/twitter/model/moments/r;

    invoke-virtual {v0}, Lcom/twitter/model/moments/r;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsb$b;

    .line 597
    if-eqz v0, :cond_0

    iget-wide v2, v0, Lbsb$b;->c:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 598
    const-string/jumbo v2, "last_read_timestamp"

    iget-wide v4, v0, Lbsb$b;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 600
    :cond_0
    const-string/jumbo v0, "capsule_page_data"

    sget-object v2, Lceh;->a:Lcom/twitter/util/serialization/l;

    .line 601
    invoke-static {p2, v2}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    .line 600
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 602
    const-string/jumbo v0, "meta_type"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 603
    return-object v1
.end method

.method public static a(Landroid/content/Context;)Lbsb;
    .locals 6

    .prologue
    .line 107
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 108
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v1

    .line 109
    new-instance v2, Lbsb;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, p0, v1, v4, v5}, Lbsb;-><init>(Landroid/content/Context;Lcom/twitter/library/provider/t;J)V

    return-object v2
.end method

.method public static a(Landroid/database/Cursor;)Lcfn;
    .locals 2

    .prologue
    .line 157
    const-string/jumbo v0, "moments_event_type"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 158
    const-string/jumbo v1, "SPORTS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    const-string/jumbo v0, "moment_sports_events_value"

    .line 160
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcfn;->a:Lcom/twitter/util/serialization/l;

    .line 159
    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfn;

    .line 163
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/util/List;ILjava/lang/String;)Ljava/util/LinkedHashMap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcek;",
            ">;I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcek;",
            ">;"
        }
    .end annotation

    .prologue
    .line 754
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 755
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcek;

    .line 756
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 757
    const-string/jumbo v4, "section_title"

    iget-object v5, v0, Lcek;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    const-string/jumbo v4, "section_type"

    iget-object v5, v0, Lcek;->b:Lcom/twitter/model/moments/MomentGuideSectionType;

    const-class v6, Lcom/twitter/model/moments/MomentGuideSectionType;

    .line 760
    invoke-static {v6}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v6

    .line 759
    invoke-static {v5, v6}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v5

    .line 758
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 761
    const-string/jumbo v4, "section_group_type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 762
    const-string/jumbo v4, "section_group_id"

    invoke-virtual {v3, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 763
    const-string/jumbo v4, "section_category_id"

    iget-object v5, v0, Lcek;->d:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    const-string/jumbo v4, "section_footer"

    iget-object v5, v0, Lcek;->e:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    const-string/jumbo v4, "section_footer_deeplink"

    iget-object v0, v0, Lcek;->f:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    invoke-virtual {v1, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 768
    :cond_0
    iget-object v2, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-string/jumbo v3, "moments_sections"

    .line 770
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 769
    invoke-virtual {v2, v3, v0}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    .line 772
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 773
    const/4 v0, 0x0

    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 774
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 773
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 776
    :cond_1
    return-object v2
.end method

.method private a(ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 287
    const-string/jumbo v0, "section_group_type=? AND section_group_id=?"

    .line 288
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 289
    iget-object v1, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    invoke-virtual {v1}, Lcom/twitter/library/provider/t;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string/jumbo v2, "moments_sections"

    const-string/jumbo v3, "section_group_type=? AND section_group_id=?"

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 290
    return-void
.end method

.method private a(JLbsb$c;)V
    .locals 5

    .prologue
    .line 742
    iget-object v0, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-string/jumbo v1, "moments_guide_user_states"

    const-string/jumbo v2, "moment_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)I

    .line 743
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 744
    const-string/jumbo v1, "moment_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 745
    const-string/jumbo v1, "is_updated"

    iget-boolean v2, p3, Lbsb$c;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 746
    const-string/jumbo v1, "is_read"

    iget-boolean v2, p3, Lbsb$c;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 747
    iget-object v1, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-string/jumbo v2, "moments_guide_user_states"

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;

    .line 748
    return-void
.end method

.method private a(JLcek;Ljava/util/Map;J)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcek;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lbsb$a;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 720
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 721
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v9

    .line 722
    move-object/from16 v0, p3

    iget-object v2, v0, Lcek;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcen;

    .line 723
    iget-object v11, v3, Lcen;->a:Lcom/twitter/model/moments/Moment;

    .line 724
    iget-wide v4, v11, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v8, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 725
    iget-object v2, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-string/jumbo v4, "moments"

    const-string/jumbo v5, "_id"

    iget-wide v6, v11, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v4, v5, v6}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)I

    .line 726
    invoke-static {v11}, Lbsb;->a(Lcom/twitter/model/moments/Moment;)Landroid/content/ContentValues;

    move-result-object v2

    .line 727
    iget-wide v4, v11, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v8, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 729
    :cond_0
    iget-wide v4, v11, Lcom/twitter/model/moments/Moment;->b:J

    invoke-direct {p0, v4, v5}, Lbsb;->d(J)Ljava/util/Map;

    move-result-object v12

    .line 730
    iget-wide v4, v11, Lcom/twitter/model/moments/Moment;->b:J

    .line 731
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbsb$a;

    .line 730
    invoke-virtual {p0, v3, v12, v2}, Lbsb;->a(Lcen;Ljava/util/Map;Lbsb$a;)Lbsb$c;

    move-result-object v13

    move-object v2, p0

    move-wide/from16 v4, p1

    move-wide/from16 v6, p5

    .line 732
    invoke-direct/range {v2 .. v7}, Lbsb;->a(Lcen;JJ)Landroid/content/ContentValues;

    move-result-object v2

    invoke-virtual {v9, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 733
    invoke-direct {p0, v3, v12}, Lbsb;->b(Lcen;Ljava/util/Map;)V

    .line 734
    iget-wide v4, v11, Lcom/twitter/model/moments/Moment;->b:J

    invoke-direct {p0, v4, v5, v13}, Lbsb;->a(JLbsb$c;)V

    .line 735
    iget-wide v4, v11, Lcom/twitter/model/moments/Moment;->b:J

    iget-object v2, v3, Lcen;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {p0, v4, v5, v2}, Lbsb;->a(JLjava/util/Collection;)V

    goto :goto_0

    .line 737
    :cond_1
    iget-object v2, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-string/jumbo v3, "moments"

    invoke-interface {v8}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;

    .line 738
    iget-object v3, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-string/jumbo v4, "moments_guide"

    invoke-virtual {v9}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-virtual {v3, v4, v2}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;

    .line 739
    return-void
.end method

.method private a(Lcei;ILjava/lang/String;Landroid/net/Uri;)V
    .locals 10

    .prologue
    .line 294
    iget-object v0, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    .line 295
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 297
    :try_start_0
    iget-object v0, p1, Lcei;->d:Lcom/twitter/model/moments/j;

    if-eqz v0, :cond_0

    .line 298
    iget-object v1, p1, Lcei;->d:Lcom/twitter/model/moments/j;

    iget-wide v4, p1, Lcei;->g:J

    move-object v0, p0

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lbsb;->a(Lcom/twitter/model/moments/j;ILjava/lang/String;J)V

    .line 300
    :cond_0
    iget-object v0, p1, Lcei;->h:Lcej;

    if-eqz v0, :cond_1

    .line 301
    iget-object v1, p1, Lcei;->h:Lcej;

    iget-wide v4, p1, Lcei;->g:J

    move-object v0, p0

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lbsb;->a(Lcej;ILjava/lang/String;J)V

    .line 303
    :cond_1
    iget-object v0, p1, Lcei;->a:Lcom/twitter/model/moments/GuideCategories;

    invoke-virtual {v0}, Lcom/twitter/model/moments/GuideCategories;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 304
    iget-object v0, p1, Lcei;->a:Lcom/twitter/model/moments/GuideCategories;

    invoke-virtual {p0, v0}, Lbsb;->a(Lcom/twitter/model/moments/GuideCategories;)V

    .line 306
    :cond_2
    invoke-virtual {p0}, Lbsb;->a()Ljava/util/Map;

    move-result-object v5

    .line 307
    iget-object v0, p1, Lcei;->c:Ljava/util/List;

    .line 308
    invoke-direct {p0, v0, p2, p3}, Lbsb;->a(Ljava/util/List;ILjava/lang/String;)Ljava/util/LinkedHashMap;

    move-result-object v0

    .line 309
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 310
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 311
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcek;

    .line 312
    iget-wide v6, p1, Lcei;->g:J

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lbsb;->a(JLcek;Ljava/util/Map;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 316
    :catchall_0
    move-exception v0

    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 314
    :cond_3
    :try_start_1
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 316
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 318
    iget-object v0, p1, Lcei;->e:Lcfp;

    invoke-virtual {p0, v0}, Lbsb;->a(Lcfp;)V

    .line 319
    new-instance v0, Laut;

    iget-object v1, p0, Lbsb;->a:Landroid/content/ContentResolver;

    invoke-direct {v0, v1}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 320
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/net/Uri;

    const/4 v2, 0x0

    aput-object p4, v1, v2

    invoke-virtual {v0, v1}, Laut;->a([Landroid/net/Uri;)V

    .line 321
    invoke-virtual {v0}, Laut;->a()V

    .line 322
    return-void
.end method

.method private a(Lcej;ILjava/lang/String;J)V
    .locals 6

    .prologue
    .line 326
    invoke-direct {p0, p2, p3}, Lbsb;->b(ILjava/lang/String;)J

    move-result-wide v0

    .line 327
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 328
    const-string/jumbo v3, "impression_id"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 329
    const-string/jumbo v3, "item_type"

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 330
    const-string/jumbo v3, "section_id"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 331
    const-string/jumbo v0, "data"

    sget-object v1, Lcej;->a:Lcom/twitter/util/serialization/l;

    .line 332
    invoke-static {p1, v1}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v1

    .line 331
    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 333
    iget-object v0, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-string/jumbo v1, "moments_guide"

    invoke-static {v2}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;

    .line 334
    return-void
.end method

.method private a(Lcen;Ljava/util/Map;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcen;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lbsb$b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 449
    iget-object v0, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    .line 450
    iget-object v12, p1, Lcen;->a:Lcom/twitter/model/moments/Moment;

    .line 451
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 453
    :try_start_0
    iget-wide v0, v12, Lcom/twitter/model/moments/Moment;->b:J

    invoke-direct {p0, v0, v1}, Lbsb;->c(J)V

    .line 454
    iget-object v0, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-string/jumbo v1, "moments"

    const-string/jumbo v2, "_id"

    iget-wide v4, v12, Lcom/twitter/model/moments/Moment;->b:J

    .line 455
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 454
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)I

    .line 456
    iget-object v1, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-string/jumbo v2, "moments"

    .line 457
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    invoke-static {v12}, Lbsb;->a(Lcom/twitter/model/moments/Moment;)Landroid/content/ContentValues;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 456
    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;

    .line 458
    invoke-direct {p0, p1, p2}, Lbsb;->b(Lcen;Ljava/util/Map;)V

    .line 459
    iget-object v0, p1, Lcen;->a:Lcom/twitter/model/moments/Moment;

    iget-wide v0, v0, Lcom/twitter/model/moments/Moment;->b:J

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p1, Lcen;->b:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0, v1, v2}, Lbsb;->a(JLjava/util/Collection;)V

    .line 460
    iget-object v0, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    iget-object v1, p1, Lcen;->f:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    iget-wide v2, p0, Lbsb;->e:J

    const/4 v4, -0x1

    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-virtual/range {v0 .. v10}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;ZLaut;)I

    .line 462
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 464
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 466
    new-instance v0, Laut;

    iget-object v1, p0, Lbsb;->a:Landroid/content/ContentResolver;

    invoke-direct {v0, v1}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 467
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/net/Uri;

    const/4 v2, 0x0

    iget-wide v4, v12, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v4, v5}, Lcom/twitter/library/provider/m$c;->a(J)Landroid/net/Uri;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Laut;->a([Landroid/net/Uri;)V

    .line 468
    invoke-virtual {v0}, Laut;->a()V

    .line 469
    return-void

    .line 464
    :catchall_0
    move-exception v0

    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method private a(Lcom/twitter/model/moments/j;ILjava/lang/String;J)V
    .locals 8

    .prologue
    .line 339
    iget-object v0, p1, Lcom/twitter/model/moments/j;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 357
    :cond_0
    :goto_0
    return-void

    .line 342
    :cond_1
    invoke-direct {p0, p2, p3}, Lbsb;->b(ILjava/lang/String;)J

    move-result-wide v2

    .line 343
    const-wide/16 v0, -0x1

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    .line 346
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 347
    iget-object v0, p1, Lcom/twitter/model/moments/j;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/i;

    .line 348
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 349
    const-string/jumbo v6, "impression_id"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 350
    const-string/jumbo v6, "item_type"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 351
    const-string/jumbo v6, "section_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 352
    const-string/jumbo v6, "data"

    sget-object v7, Lcom/twitter/model/moments/i;->a:Lcom/twitter/util/serialization/l;

    .line 353
    invoke-static {v0, v7}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v0

    .line 352
    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 354
    invoke-virtual {v1, v5}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1

    .line 356
    :cond_2
    iget-object v2, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-string/jumbo v3, "moments_guide"

    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {v2, v3, v0}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;

    goto :goto_0
.end method

.method private b(ILjava/lang/String;)J
    .locals 4

    .prologue
    .line 360
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 361
    const-string/jumbo v1, "section_type"

    sget-object v2, Lcom/twitter/model/moments/MomentGuideSectionType;->c:Lcom/twitter/model/moments/MomentGuideSectionType;

    const-class v3, Lcom/twitter/model/moments/MomentGuideSectionType;

    .line 363
    invoke-static {v3}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v3

    .line 362
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    .line 361
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 364
    const-string/jumbo v1, "section_group_type"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 365
    const-string/jumbo v1, "section_group_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    iget-object v1, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-string/jumbo v2, "moments_sections"

    .line 367
    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 366
    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    .line 368
    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 369
    new-instance v1, Lcom/twitter/library/util/InvalidDataException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Error writing trend section information to the database: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/twitter/library/util/InvalidDataException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 371
    const-wide/16 v0, -0x1

    .line 373
    :goto_0
    return-wide v0

    :cond_0
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method private b(Lcom/twitter/model/moments/Moment;)Landroid/content/ContentValues;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 710
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 711
    const-string/jumbo v1, "moment_id"

    iget-wide v2, p1, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 712
    const-string/jumbo v1, "page_id"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 713
    const-string/jumbo v1, "meta_type"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 714
    const-string/jumbo v1, "capsule_page_data"

    new-array v2, v4, [B

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 715
    return-object v0
.end method

.method public static b(Landroid/database/Cursor;)Lcom/twitter/model/moments/Moment;
    .locals 24

    .prologue
    .line 168
    const-string/jumbo v2, "moments_guide_moment_id"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 170
    const-string/jumbo v2, "moments_title"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 171
    const-string/jumbo v2, "moments_can_subscribe"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    move v6, v2

    .line 173
    :goto_0
    const-string/jumbo v2, "moments_is_live"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    move v7, v2

    .line 175
    :goto_1
    const-string/jumbo v2, "moments_is_sensitive"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    move v8, v2

    .line 177
    :goto_2
    const-string/jumbo v2, "moments_subcategory_string"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 179
    const-string/jumbo v2, "moments_subcategory_favicon_url"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 181
    const-string/jumbo v2, "moments_time_string"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 183
    const-string/jumbo v2, "moments_duration_string"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 185
    const-string/jumbo v2, "moments_is_subscribed"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    const/4 v2, 0x1

    move v9, v2

    .line 187
    :goto_3
    const-string/jumbo v2, "moments_description"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 189
    const-string/jumbo v2, "moments_moment_url"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 190
    const-string/jumbo v2, "moments_num_subscribers"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 192
    const-string/jumbo v2, "moments_author_info"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    sget-object v3, Lcom/twitter/model/moments/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/a;

    .line 194
    const-string/jumbo v3, "moments_promoted_content"

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    sget-object v4, Lcgi;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v3, v4}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcgi;

    .line 196
    const-string/jumbo v4, "moments_event_id"

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 197
    const-string/jumbo v5, "moments_event_type"

    move-object/from16 v0, p0

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p0

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 198
    new-instance v10, Lcom/twitter/model/moments/g$a;

    invoke-direct {v10}, Lcom/twitter/model/moments/g$a;-><init>()V

    invoke-virtual {v10, v4}, Lcom/twitter/model/moments/g$a;->a(Ljava/lang/String;)Lcom/twitter/model/moments/g$a;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/twitter/model/moments/g$a;->b(Ljava/lang/String;)Lcom/twitter/model/moments/g$a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/model/moments/g$a;->r()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/moments/g;

    .line 199
    const-string/jumbo v5, "moments_curation_metadata"

    .line 200
    move-object/from16 v0, p0

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p0

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    sget-object v10, Lcom/twitter/model/moments/f;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v5, v10}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/model/moments/f;

    .line 202
    const-string/jumbo v10, "moments_is_liked"

    move-object/from16 v0, p0

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, p0

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v10, v0, :cond_4

    const/4 v10, 0x1

    .line 203
    :goto_4
    const-string/jumbo v21, "moments_total_likes"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 205
    new-instance v21, Lcom/twitter/model/moments/Moment$a;

    invoke-direct/range {v21 .. v21}, Lcom/twitter/model/moments/Moment$a;-><init>()V

    .line 206
    move-object/from16 v0, v21

    invoke-virtual {v0, v12, v13}, Lcom/twitter/model/moments/Moment$a;->a(J)Lcom/twitter/model/moments/Moment$a;

    move-result-object v12

    .line 207
    invoke-virtual {v12, v11}, Lcom/twitter/model/moments/Moment$a;->a(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v11

    .line 208
    invoke-virtual {v11, v6}, Lcom/twitter/model/moments/Moment$a;->a(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 209
    invoke-virtual {v6, v7}, Lcom/twitter/model/moments/Moment$a;->b(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 210
    invoke-virtual {v6, v8}, Lcom/twitter/model/moments/Moment$a;->c(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 211
    invoke-virtual {v6, v14}, Lcom/twitter/model/moments/Moment$a;->b(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 212
    invoke-virtual {v6, v15}, Lcom/twitter/model/moments/Moment$a;->c(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 213
    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/twitter/model/moments/Moment$a;->d(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 214
    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Lcom/twitter/model/moments/Moment$a;->e(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 215
    invoke-virtual {v6, v9}, Lcom/twitter/model/moments/Moment$a;->d(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 216
    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Lcom/twitter/model/moments/Moment$a;->f(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 217
    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Lcom/twitter/model/moments/Moment$a;->g(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 218
    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/twitter/model/moments/Moment$a;->a(I)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 219
    invoke-virtual {v6, v2}, Lcom/twitter/model/moments/Moment$a;->a(Lcom/twitter/model/moments/a;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v2

    .line 220
    invoke-virtual {v2, v3}, Lcom/twitter/model/moments/Moment$a;->a(Lcgi;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v2

    .line 221
    invoke-virtual {v2, v4}, Lcom/twitter/model/moments/Moment$a;->a(Lcom/twitter/model/moments/g;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v2

    .line 222
    invoke-virtual {v2, v5}, Lcom/twitter/model/moments/Moment$a;->a(Lcom/twitter/model/moments/f;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v2

    .line 223
    invoke-virtual {v2, v10}, Lcom/twitter/model/moments/Moment$a;->e(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v2

    .line 224
    move-wide/from16 v0, v22

    invoke-virtual {v2, v0, v1}, Lcom/twitter/model/moments/Moment$a;->c(J)Lcom/twitter/model/moments/Moment$a;

    move-result-object v2

    .line 225
    invoke-virtual {v2}, Lcom/twitter/model/moments/Moment$a;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/Moment;

    .line 205
    return-object v2

    .line 171
    :cond_0
    const/4 v2, 0x0

    move v6, v2

    goto/16 :goto_0

    .line 173
    :cond_1
    const/4 v2, 0x0

    move v7, v2

    goto/16 :goto_1

    .line 175
    :cond_2
    const/4 v2, 0x0

    move v8, v2

    goto/16 :goto_2

    .line 185
    :cond_3
    const/4 v2, 0x0

    move v9, v2

    goto/16 :goto_3

    .line 202
    :cond_4
    const/4 v10, 0x0

    goto/16 :goto_4
.end method

.method private b(Lcen;Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcen;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lbsb$b;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 685
    iget-object v1, p1, Lcen;->a:Lcom/twitter/model/moments/Moment;

    iget-wide v2, v1, Lcom/twitter/model/moments/Moment;->b:J

    .line 686
    iget-object v1, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-string/jumbo v4, "moments_pages"

    const-string/jumbo v5, "moment_id"

    .line 687
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 686
    invoke-virtual {v1, v4, v5, v2}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)I

    .line 688
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 689
    iget-object v1, p1, Lcen;->c:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 690
    iget-object v1, p1, Lcen;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 691
    iget-object v1, p1, Lcen;->a:Lcom/twitter/model/moments/Moment;

    invoke-direct {p0, v1}, Lbsb;->b(Lcom/twitter/model/moments/Moment;)Landroid/content/ContentValues;

    move-result-object v1

    .line 692
    const-string/jumbo v3, "page_number"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 693
    invoke-virtual {v2, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 705
    :cond_0
    iget-object v1, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-string/jumbo v3, "moments_pages"

    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {v1, v3, v0}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;

    .line 706
    return-void

    .line 696
    :cond_1
    iget-object v1, p1, Lcen;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lceh;

    .line 697
    iget-object v4, p1, Lcen;->a:Lcom/twitter/model/moments/Moment;

    invoke-direct {p0, v4, v0, p2}, Lbsb;->a(Lcom/twitter/model/moments/Moment;Lceh;Ljava/util/Map;)Landroid/content/ContentValues;

    move-result-object v0

    .line 698
    const-string/jumbo v4, "page_number"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 699
    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 700
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 701
    goto :goto_0
.end method

.method private b(JLjava/util/Collection;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Collection",
            "<",
            "Lbsb$b;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 676
    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsb$b;

    .line 677
    iget-wide v2, v0, Lbsb$b;->a:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    iget-wide v2, v0, Lbsb$b;->c:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 678
    const/4 v0, 0x1

    .line 681
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(J)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 472
    iget-object v0, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-class v1, Layf$b;

    const-string/jumbo v2, "section_group_type=? AND section_group_id=?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 474
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    .line 472
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/provider/t;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 475
    return-void
.end method

.method private d(J)Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lbsb$b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 530
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v8

    .line 531
    iget-object v0, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    .line 532
    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v0

    const-class v1, Layd;

    invoke-interface {v0, v1}, Lcom/twitter/database/schema/TwitterSchema;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Layd;

    invoke-interface {v0}, Layd;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    .line 533
    const-string/jumbo v1, "moment_id"

    .line 534
    invoke-static {v1}, Laux;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/twitter/database/model/l;->a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/twitter/database/model/g;

    move-result-object v9

    .line 536
    :try_start_0
    invoke-virtual {v9}, Lcom/twitter/database/model/g;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 538
    :cond_0
    iget-object v0, v9, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Layd$a;

    invoke-interface {v0}, Layd$a;->b()Ljava/lang/String;

    move-result-object v3

    .line 539
    iget-object v0, v9, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Layd$a;

    invoke-interface {v0}, Layd$a;->c()Ljava/lang/Long;

    move-result-object v0

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 540
    iget-object v0, v9, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Layd$a;

    invoke-interface {v0}, Layd$a;->d()J

    move-result-wide v6

    .line 541
    new-instance v0, Lbsb$b;

    move-wide v1, p1

    invoke-direct/range {v0 .. v7}, Lbsb$b;-><init>(JLjava/lang/String;JJ)V

    invoke-virtual {v8, v3, v0}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    .line 542
    invoke-virtual {v9}, Lcom/twitter/database/model/g;->d()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 545
    :cond_1
    invoke-virtual {v9}, Lcom/twitter/database/model/g;->close()V

    .line 547
    invoke-virtual {v8}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0

    .line 545
    :catchall_0
    move-exception v0

    invoke-virtual {v9}, Lcom/twitter/database/model/g;->close()V

    throw v0
.end method


# virtual methods
.method a(Lcen;Ljava/util/Map;Lbsb$a;)Lbsb$c;
    .locals 8
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcen;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lbsb$b;",
            ">;",
            "Lbsb$a;",
            ")",
            "Lbsb$c;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 616
    .line 618
    iget-object v0, p1, Lcen;->a:Lcom/twitter/model/moments/Moment;

    iget-wide v4, v0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p0, v4, v5, v0}, Lbsb;->b(JLjava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 620
    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 622
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsb$b;

    .line 623
    iget-object v3, p1, Lcen;->a:Lcom/twitter/model/moments/Moment;

    iget-wide v4, v3, Lcom/twitter/model/moments/Moment;->s:J

    iget-wide v6, v0, Lbsb$b;->d:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    move v3, v1

    move v4, v1

    .line 630
    :goto_0
    if-eqz p3, :cond_0

    iget-boolean v0, p3, Lbsb$a;->b:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    or-int/2addr v0, v4

    .line 631
    new-instance v1, Lbsb$c;

    invoke-direct {v1, v3, v0}, Lbsb$c;-><init>(ZZ)V

    return-object v1

    :cond_0
    move v0, v2

    .line 630
    goto :goto_1

    :cond_1
    move v3, v1

    move v4, v2

    goto :goto_0

    :cond_2
    move v3, v2

    move v4, v2

    goto :goto_0
.end method

.method a()Ljava/util/Map;
    .locals 8
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lbsb$a;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 505
    iget-object v0, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->o()Landroid/database/Cursor;

    move-result-object v2

    .line 506
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v3

    .line 508
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 510
    :cond_0
    const-string/jumbo v0, "moment_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 511
    const-string/jumbo v0, "is_updated"

    .line 512
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 511
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    .line 513
    :goto_0
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    new-instance v7, Lbsb$a;

    invoke-direct {v7, v4, v5, v0}, Lbsb$a;-><init>(JZ)V

    invoke-virtual {v3, v6, v7}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    .line 514
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 516
    :cond_1
    invoke-virtual {v3}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 518
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 516
    return-object v0

    .line 511
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 518
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/provider/t;->c(J)V

    .line 445
    return-void
.end method

.method public a(JLceq;)V
    .locals 11

    .prologue
    const/4 v9, 0x1

    .line 413
    new-instance v0, Lcek$a;

    invoke-direct {v0}, Lcek$a;-><init>()V

    sget-object v1, Lcom/twitter/model/moments/MomentGuideSectionType;->c:Lcom/twitter/model/moments/MomentGuideSectionType;

    .line 414
    invoke-virtual {v0, v1}, Lcek$a;->a(Lcom/twitter/model/moments/MomentGuideSectionType;)Lcek$a;

    move-result-object v0

    iget-object v1, p3, Lceq;->a:Ljava/util/List;

    .line 415
    invoke-virtual {v0, v1}, Lcek$a;->a(Ljava/lang/Iterable;)Lcek$a;

    move-result-object v0

    .line 416
    invoke-virtual {v0}, Lcek$a;->q()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcek;

    .line 417
    iget-object v0, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    .line 418
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 420
    :try_start_0
    invoke-virtual {p0}, Lbsb;->a()Ljava/util/Map;

    move-result-object v5

    .line 421
    iget-object v0, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-string/jumbo v1, "moments_sections"

    const-string/jumbo v2, "section_group_id"

    .line 422
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 421
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)I

    .line 423
    iget-object v0, p0, Lbsb;->d:Lbse;

    iget-wide v2, p3, Lceq;->b:J

    invoke-virtual {v0, p1, p2, v2, v3}, Lbse;->a(JJ)V

    .line 424
    invoke-static {v4}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    .line 425
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 424
    invoke-direct {p0, v0, v1, v2}, Lbsb;->a(Ljava/util/List;ILjava/lang/String;)Ljava/util/LinkedHashMap;

    move-result-object v0

    .line 425
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 424
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 426
    if-nez v0, :cond_0

    .line 432
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 437
    :goto_0
    return-void

    .line 429
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-wide v6, p3, Lceq;->b:J

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lbsb;->a(JLcek;Ljava/util/Map;J)V

    .line 430
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 432
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 434
    new-instance v0, Laut;

    iget-object v1, p0, Lbsb;->a:Landroid/content/ContentResolver;

    invoke-direct {v0, v1}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 435
    new-array v1, v9, [Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Lcom/twitter/library/provider/m$b;->a(J)Landroid/net/Uri;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Laut;->a([Landroid/net/Uri;)V

    .line 436
    invoke-virtual {v0}, Laut;->a()V

    goto :goto_0

    .line 432
    :catchall_0
    move-exception v0

    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public a(JLcom/twitter/model/moments/r;J)V
    .locals 3

    .prologue
    .line 484
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 485
    const-string/jumbo v1, "last_read_timestamp"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 486
    iget-object v1, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    invoke-virtual {p3}, Lcom/twitter/model/moments/r;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, p1, p2, v2}, Lcom/twitter/library/provider/t;->a(Landroid/content/ContentValues;JLjava/lang/String;)V

    .line 487
    return-void
.end method

.method public a(JLjava/util/Collection;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Collection",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 140
    new-instance v13, Laut;

    iget-object v1, p0, Lbsb;->a:Landroid/content/ContentResolver;

    invoke-direct {v13, v1}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 146
    iget-object v1, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    new-instance v2, Ljava/util/ArrayList;

    move-object/from16 v0, p3

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-wide v3, p0, Lbsb;->e:J

    const/16 v5, 0x21

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v14, 0x0

    move-wide/from16 v6, p1

    invoke-virtual/range {v1 .. v14}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;JIJZZZLjava/lang/String;ZLaut;Z)Ljava/util/Collection;

    move-result-object v1

    .line 148
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    .line 149
    if-lez v1, :cond_0

    .line 150
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-static/range {p1 .. p2}, Lcom/twitter/library/provider/m$c;->a(J)Landroid/net/Uri;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v13, v1}, Laut;->a([Landroid/net/Uri;)V

    .line 151
    invoke-virtual {v13}, Laut;->a()V

    .line 153
    :cond_0
    return-void
.end method

.method public a(JZ)V
    .locals 3

    .prologue
    .line 490
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 491
    const-string/jumbo v1, "is_updated"

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 492
    iget-object v1, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    invoke-virtual {v1, v0, p1, p2}, Lcom/twitter/library/provider/t;->a(Landroid/content/ContentValues;J)V

    .line 493
    return-void
.end method

.method public a(JZJ)V
    .locals 5

    .prologue
    .line 248
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 249
    const-string/jumbo v1, "is_liked"

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 250
    const-string/jumbo v1, "total_likes"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 251
    iget-object v1, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-string/jumbo v2, "moments"

    invoke-virtual {v1, v2, v0, p1, p2}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Landroid/content/ContentValues;J)V

    .line 252
    new-instance v0, Laut;

    iget-object v1, p0, Lbsb;->a:Landroid/content/ContentResolver;

    invoke-direct {v0, v1}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 253
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Lcom/twitter/library/provider/m$d;->a(J)Landroid/net/Uri;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Laut;->a([Landroid/net/Uri;)V

    .line 254
    invoke-virtual {v0}, Laut;->a()V

    .line 255
    return-void
.end method

.method public a(Lcei;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 265
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 267
    :try_start_0
    invoke-direct {p0, p2, p3}, Lbsb;->a(ILjava/lang/String;)V

    .line 269
    invoke-static {p2, p3}, Lcom/twitter/library/provider/m$b;->a(ILjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 268
    invoke-direct {p0, p1, p2, p3, v0}, Lbsb;->a(Lcei;ILjava/lang/String;Landroid/net/Uri;)V

    .line 270
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 272
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 274
    return-void

    .line 272
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public a(Lcen;)V
    .locals 2

    .prologue
    .line 440
    iget-object v0, p1, Lcen;->a:Lcom/twitter/model/moments/Moment;

    iget-wide v0, v0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-direct {p0, v0, v1}, Lbsb;->d(J)Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lbsb;->a(Lcen;Ljava/util/Map;)V

    .line 441
    return-void
.end method

.method public a(Lcfp;)V
    .locals 4

    .prologue
    .line 407
    iget-object v0, p1, Lcfp;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfn;

    .line 408
    iget-object v2, p0, Lbsb;->c:Lbsf;

    iget-object v3, v0, Lcfn;->b:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lbsf;->b(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 410
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/moments/GuideCategories;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 377
    iget-object v0, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 378
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->a()Lcqt;

    move-result-object v0

    invoke-interface {v0}, Lcqt;->a()J

    move-result-wide v2

    .line 379
    iget-object v0, p1, Lcom/twitter/model/moments/GuideCategories;->c:Ljava/util/List;

    .line 380
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 382
    :try_start_0
    iget-object v4, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string/jumbo v7, "moments_guide_categories"

    aput-object v7, v5, v6

    invoke-virtual {v4, v5}, Lcom/twitter/library/provider/t;->a([Ljava/lang/String;)V

    .line 383
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v4

    .line 384
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/h;

    .line 385
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 386
    const-string/jumbo v7, "category_id"

    iget-object v8, v0, Lcom/twitter/model/moments/h;->a:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    const-string/jumbo v7, "is_default_category"

    iget-object v8, v0, Lcom/twitter/model/moments/h;->a:Ljava/lang/String;

    iget-object v9, p1, Lcom/twitter/model/moments/GuideCategories;->b:Ljava/lang/String;

    .line 388
    invoke-static {v8, v9}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 387
    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 389
    const-string/jumbo v7, "category_name"

    iget-object v0, v0, Lcom/twitter/model/moments/h;->b:Ljava/lang/String;

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    const-string/jumbo v0, "fetch_timestamp"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v0, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 391
    invoke-virtual {v4, v6}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 397
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 393
    :cond_0
    :try_start_1
    iget-object v2, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-string/jumbo v3, "moments_guide_categories"

    .line 394
    invoke-virtual {v4}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 393
    invoke-virtual {v2, v3, v0}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/util/List;

    .line 395
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 397
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 400
    new-instance v0, Laut;

    iget-object v1, p0, Lbsb;->a:Landroid/content/ContentResolver;

    invoke-direct {v0, v1}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 401
    new-array v1, v11, [Landroid/net/Uri;

    sget-object v2, Lcom/twitter/library/provider/m$a;->a:Landroid/net/Uri;

    aput-object v2, v1, v10

    invoke-virtual {v0, v1}, Laut;->a([Landroid/net/Uri;)V

    .line 402
    invoke-virtual {v0}, Laut;->a()V

    .line 403
    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 238
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 239
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 240
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 241
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 242
    const-string/jumbo v3, "is_subscribed"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 243
    iget-object v0, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-string/jumbo v3, "moments"

    invoke-virtual {v0, v3, v1, v4, v5}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Landroid/content/ContentValues;J)V

    goto :goto_0

    .line 245
    :cond_0
    return-void
.end method

.method public b(J)V
    .locals 5

    .prologue
    .line 478
    iget-object v0, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-string/jumbo v1, "moments"

    const-string/jumbo v2, "_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)I

    .line 479
    iget-object v0, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-string/jumbo v1, "moments_pages"

    const-string/jumbo v2, "moment_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)I

    .line 480
    iget-object v0, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    const-string/jumbo v1, "moments_guide"

    const-string/jumbo v2, "moment_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)I

    .line 481
    return-void
.end method

.method public b(JZ)V
    .locals 3

    .prologue
    .line 496
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 497
    const-string/jumbo v1, "is_read"

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 498
    iget-object v1, p0, Lbsb;->b:Lcom/twitter/library/provider/t;

    invoke-virtual {v1, v0, p1, p2}, Lcom/twitter/library/provider/t;->a(Landroid/content/ContentValues;J)V

    .line 499
    return-void
.end method

.method public b(Lcei;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 282
    .line 283
    invoke-static {p2, p3}, Lcom/twitter/library/provider/m$b;->a(ILjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 282
    invoke-direct {p0, p1, p2, p3, v0}, Lbsb;->a(Lcei;ILjava/lang/String;Landroid/net/Uri;)V

    .line 284
    return-void
.end method
