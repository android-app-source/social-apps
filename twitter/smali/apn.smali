.class public abstract Lapn;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lapn$a;,
        Lapn$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/model/dms/c;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final a:Lcom/twitter/model/dms/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field final b:Lcom/twitter/model/dms/m;

.field final c:Lcom/twitter/model/dms/c;

.field final d:Z

.field final e:Landroid/view/View;

.field final f:Landroid/widget/TextView;

.field final g:Landroid/content/Context;

.field final h:Landroid/content/res/Resources;

.field final i:J

.field final j:Lcom/twitter/app/dm/m;


# direct methods
.method constructor <init>(Lapn$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lapn$a",
            "<TT;+",
            "Lapn$b;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iget-object v0, p1, Lapn$a;->b:Lcom/twitter/model/dms/c;

    iput-object v0, p0, Lapn;->a:Lcom/twitter/model/dms/c;

    .line 39
    invoke-static {p1}, Lapn$a;->a(Lapn$a;)Lcom/twitter/model/dms/m;

    move-result-object v0

    iput-object v0, p0, Lapn;->b:Lcom/twitter/model/dms/m;

    .line 40
    iget-object v0, p1, Lapn$a;->c:Lcom/twitter/model/dms/c;

    iput-object v0, p0, Lapn;->c:Lcom/twitter/model/dms/c;

    .line 41
    invoke-static {p1}, Lapn$a;->b(Lapn$a;)Z

    move-result v0

    iput-boolean v0, p0, Lapn;->d:Z

    .line 43
    invoke-static {p1}, Lapn$a;->c(Lapn$a;)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lapn;->g:Landroid/content/Context;

    .line 44
    iget-object v0, p0, Lapn;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lapn;->h:Landroid/content/res/Resources;

    .line 45
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lapn;->i:J

    .line 46
    invoke-static {p1}, Lapn$a;->d(Lapn$a;)Lcom/twitter/app/dm/m;

    move-result-object v0

    iput-object v0, p0, Lapn;->j:Lcom/twitter/app/dm/m;

    .line 48
    iget-object v0, p1, Lapn$a;->a:Lapn$b;

    .line 49
    invoke-static {v0}, Lapn$b;->a(Lapn$b;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lapn;->e:Landroid/view/View;

    .line 50
    invoke-static {v0}, Lapn$b;->b(Lapn$b;)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lapn;->f:Landroid/widget/TextView;

    .line 51
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method b()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 56
    iget-object v0, p0, Lapn;->e:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapn;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lapn;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 58
    iget-object v0, p0, Lapn;->j:Lcom/twitter/app/dm/m;

    iget-object v1, p0, Lapn;->a:Lcom/twitter/model/dms/c;

    iget-wide v2, v1, Lcom/twitter/model/dms/c;->e:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/app/dm/m;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lapn;->j:Lcom/twitter/app/dm/m;

    invoke-virtual {v0}, Lcom/twitter/app/dm/m;->c()I

    move-result v0

    .line 60
    iget-object v1, p0, Lapn;->e:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 61
    iget-object v1, p0, Lapn;->f:Landroid/widget/TextView;

    iget-object v2, p0, Lapn;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0008

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    .line 62
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 61
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    :cond_0
    return-void
.end method

.method c()Z
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Lapn;->a:Lcom/twitter/model/dms/c;

    iget-wide v2, p0, Lapn;->i:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/dms/c;->b(J)Z

    move-result v0

    return v0
.end method
