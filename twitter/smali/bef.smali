.class public abstract Lbef;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcfq;",
        "Ljava/lang/Void;",
        ">;>;"
    }
.end annotation


# instance fields
.field public a:Lcfq;

.field private final b:Lbed;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;Lbed;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 40
    iput-object p4, p0, Lbef;->b:Lbed;

    .line 41
    iput-object p5, p0, Lbef;->c:Ljava/lang/String;

    .line 42
    return-void
.end method

.method private e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0}, Lbef;->g()Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private g()Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;
    .locals 2

    .prologue
    .line 95
    new-instance v0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;

    invoke-direct {v0}, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;-><init>()V

    .line 96
    iget-object v1, p0, Lbef;->b:Lbed;

    iget-object v1, v1, Lbed;->e:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->a:Ljava/lang/String;

    .line 97
    const-string/jumbo v1, "dogfood"

    iput-object v1, v0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->b:Ljava/lang/String;

    .line 98
    iget-object v1, p0, Lbef;->b:Lbed;

    iget-object v1, v1, Lbed;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->c:Ljava/lang/String;

    .line 99
    const-string/jumbo v1, "push"

    iput-object v1, v0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->d:Ljava/lang/String;

    .line 100
    iget-object v1, p0, Lbef;->b:Lbed;

    iget-object v1, v1, Lbed;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->e:Ljava/lang/String;

    .line 101
    iget-object v1, p0, Lbef;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;->f:Ljava/lang/String;

    .line 102
    return-object v0
.end method


# virtual methods
.method public abstract a(Lcom/twitter/library/service/d$a;)Lcom/twitter/library/service/d$a;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final a()Lcom/twitter/library/service/d;
    .locals 3

    .prologue
    .line 64
    invoke-virtual {p0}, Lbef;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 66
    :try_start_0
    iget-object v1, p0, Lbef;->b:Lbed;

    .line 70
    invoke-direct {p0}, Lbef;->e()Ljava/util/List;

    move-result-object v2

    .line 67
    invoke-static {v0, v1, v2}, Lbeg;->a(Lcom/twitter/library/service/d$a;Lbed;Ljava/util/List;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 66
    invoke-virtual {p0, v1}, Lbef;->a(Lcom/twitter/library/service/d$a;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 71
    invoke-virtual {v1}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 74
    :goto_0
    return-object v0

    .line 72
    :catch_0
    move-exception v1

    .line 74
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcfq;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfq;

    iput-object v0, p0, Lbef;->a:Lcfq;

    .line 59
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 24
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbef;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method public final b()Lcom/twitter/library/api/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcfq;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    const-class v0, Lcfq;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lbef;->b()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
