.class public Laiv;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcfq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Laiv;->a:Ljava/util/Map;

    .line 20
    return-void
.end method


# virtual methods
.method public a()Lcfq;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Laiv;->b:Lcfq;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 81
    iget-object v1, p0, Laiv;->b:Lcfq;

    if-nez v1, :cond_1

    .line 88
    :cond_0
    :goto_0
    return-object v0

    .line 84
    :cond_1
    iget-object v1, p0, Laiv;->b:Lcfq;

    iget-object v1, v1, Lcfq;->e:Ljava/util/Map;

    .line 85
    if-eqz v1, :cond_0

    .line 88
    iget-object v0, p0, Laiv;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Laiv;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_2
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 59
    iget-object v1, p0, Laiv;->b:Lcfq;

    if-nez v1, :cond_1

    .line 76
    :cond_0
    :goto_0
    return-object v0

    .line 62
    :cond_1
    iget-object v1, p0, Laiv;->b:Lcfq;

    iget-object v1, v1, Lcfq;->e:Ljava/util/Map;

    .line 63
    if-eqz v1, :cond_0

    .line 66
    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 70
    iget-object v0, p0, Laiv;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 71
    iget-object v0, p0, Laiv;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 75
    :goto_1
    iget-object v1, p0, Laiv;->a:Ljava/util/Map;

    invoke-interface {v1, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 73
    :cond_2
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_1
.end method

.method public a(Lcfq;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Laiv;->b:Lcfq;

    .line 32
    return-void
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 93
    iget-object v0, p0, Laiv;->b:Lcfq;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 105
    :goto_0
    return-object v0

    .line 96
    :cond_0
    iget-object v0, p0, Laiv;->b:Lcfq;

    iget-object v0, v0, Lcfq;->f:Lcgb;

    .line 97
    if-nez v0, :cond_1

    move-object v0, v1

    .line 98
    goto :goto_0

    .line 100
    :cond_1
    iget-object v0, v0, Lcgb;->b:Lcgb$d;

    iget-object v0, v0, Lcgb$d;->c:Ljava/util/Map;

    .line 101
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgb$b;

    .line 102
    if-nez v0, :cond_2

    move-object v0, v1

    .line 103
    goto :goto_0

    .line 105
    :cond_2
    iget-object v0, v0, Lcgb$b;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public b()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 40
    iget-object v0, p0, Laiv;->b:Lcfq;

    if-nez v0, :cond_0

    move v0, v2

    .line 54
    :goto_0
    return v0

    .line 43
    :cond_0
    iget-object v0, p0, Laiv;->b:Lcfq;

    iget-object v3, v0, Lcfq;->e:Ljava/util/Map;

    .line 44
    if-nez v3, :cond_1

    move v0, v2

    .line 45
    goto :goto_0

    .line 49
    :cond_1
    iget-object v0, p0, Laiv;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 50
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 51
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 54
    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 110
    iget-object v0, p0, Laiv;->b:Lcfq;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 118
    :goto_0
    return-object v0

    .line 114
    :cond_0
    iget-object v0, p0, Laiv;->b:Lcfq;

    iget-object v0, v0, Lcfq;->b:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgc;

    .line 115
    if-nez v0, :cond_1

    move-object v0, v1

    .line 116
    goto :goto_0

    .line 118
    :cond_1
    iget-object v0, v0, Lcgc;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public d()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 127
    iget-object v1, p0, Laiv;->b:Lcfq;

    if-nez v1, :cond_1

    .line 136
    :cond_0
    :goto_0
    return-object v0

    .line 130
    :cond_1
    iget-object v1, p0, Laiv;->b:Lcfq;

    iget-object v1, v1, Lcfq;->e:Ljava/util/Map;

    .line 131
    if-eqz v1, :cond_0

    .line 135
    iget-object v0, p0, Laiv;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 136
    iget-object v0, p0, Laiv;->b:Lcfq;

    iget-object v0, v0, Lcfq;->e:Ljava/util/Map;

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x1

    return v0
.end method
