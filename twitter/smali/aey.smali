.class public Laey;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/chat/x;


# static fields
.field static final a:J
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field


# instance fields
.field private final b:J

.field private c:Lcom/twitter/model/core/TwitterUser;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 21
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Laey;->a:J

    return-void
.end method

.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-wide p1, p0, Laey;->b:J

    .line 29
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 33
    sget-wide v0, Laey;->a:J

    return-wide v0
.end method

.method public a(Ltv/periscope/android/api/PsUser;)Ltv/periscope/model/chat/Message;
    .locals 2

    .prologue
    .line 48
    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    sget-object v1, Ltv/periscope/model/chat/MessageType;->k:Ltv/periscope/model/chat/MessageType;

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-object v0, p0, Laey;->c:Lcom/twitter/model/core/TwitterUser;

    .line 49
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ltv/periscope/model/chat/Message$a;->e(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-object v0, p0, Laey;->c:Lcom/twitter/model/core/TwitterUser;

    .line 50
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ltv/periscope/model/chat/Message$a;->d(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget-object v1, p1, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    .line 51
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget-object v1, p1, Ltv/periscope/android/api/PsUser;->twitterId:Ljava/lang/String;

    .line 52
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-object v0, p0, Laey;->c:Lcom/twitter/model/core/TwitterUser;

    .line 53
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ltv/periscope/model/chat/Message$a;->g(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 48
    return-object v0
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Laey;->c:Lcom/twitter/model/core/TwitterUser;

    .line 59
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 38
    iget-object v1, p0, Laey;->c:Lcom/twitter/model/core/TwitterUser;

    if-eqz v1, :cond_0

    iget-wide v2, p0, Laey;->b:J

    iget-object v1, p0, Laey;->c:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 42
    :cond_0
    :goto_0
    return v0

    .line 41
    :cond_1
    iget-object v1, p0, Laey;->c:Lcom/twitter/model/core/TwitterUser;

    iget v1, v1, Lcom/twitter/model/core/TwitterUser;->U:I

    .line 42
    invoke-static {v1}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v1}, Lcom/twitter/model/core/g;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
