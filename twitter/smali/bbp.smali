.class public abstract Lbbp;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lbbq;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/model/av/h;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lbbq;)V
    .locals 1

    .prologue
    .line 33
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/library/service/b;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V

    .line 35
    invoke-virtual {p2}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    invoke-static {p3}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbbq;

    invoke-virtual {v0}, Lbbq;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/av/h;

    iput-object v0, p0, Lbbp;->a:Lcom/twitter/model/av/h;

    .line 38
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 15
    check-cast p3, Lbbq;

    invoke-virtual {p0, p1, p2, p3}, Lbbp;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lbbq;)V

    return-void
.end method

.method protected e()Lbbq;
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lbbq;

    invoke-direct {v0}, Lbbq;-><init>()V

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lbbp;->e()Lbbq;

    move-result-object v0

    return-object v0
.end method

.method public g()Lcom/twitter/model/av/h;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lbbp;->a:Lcom/twitter/model/av/h;

    return-object v0
.end method
