.class public Lakh;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/content/Loader$OnLoadCompleteListener;
.implements Lcom/twitter/library/widget/renderablecontent/c;
.implements Lcom/twitter/ui/user/BaseUserView$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lakh$c;,
        Lakh$b;,
        Lakh$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/content/Loader$OnLoadCompleteListener",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/twitter/library/widget/renderablecontent/c",
        "<",
        "Lakh$a;",
        ">;",
        "Lcom/twitter/ui/user/BaseUserView$a",
        "<",
        "Lcom/twitter/ui/user/UserView;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/view/ViewGroup;

.field private final d:Lcom/twitter/ui/user/ProfileCardView;

.field private e:Lcom/twitter/util/android/d;

.field private final f:Lcom/twitter/model/core/Tweet;

.field private final g:Lakh$b;

.field private final h:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final i:Lcom/twitter/model/util/FriendshipCache;

.field private final j:Lcom/twitter/library/util/s;

.field private k:Landroid/view/View;

.field private l:Lakh$c;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;Lakh$b;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 2

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    new-instance v0, Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/model/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lakh;->i:Lcom/twitter/model/util/FriendshipCache;

    .line 101
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lakh;->b:Ljava/lang/ref/WeakReference;

    .line 102
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lakh;->a:Landroid/content/Context;

    .line 103
    iput-object p2, p0, Lakh;->f:Lcom/twitter/model/core/Tweet;

    .line 104
    iput-object p4, p0, Lakh;->h:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 105
    iput-object p3, p0, Lakh;->g:Lakh$b;

    .line 107
    const v0, 0x7f040239

    invoke-direct {p0, p1, v0}, Lakh;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lakh;->c:Landroid/view/ViewGroup;

    .line 109
    iget-object v0, p0, Lakh;->c:Landroid/view/ViewGroup;

    const v1, 0x7f040264

    invoke-direct {p0, v0, v1}, Lakh;->a(Landroid/view/ViewGroup;I)Lcom/twitter/ui/user/ProfileCardView;

    move-result-object v0

    iput-object v0, p0, Lakh;->d:Lcom/twitter/ui/user/ProfileCardView;

    .line 110
    iget-object v0, p0, Lakh;->d:Lcom/twitter/ui/user/ProfileCardView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/ProfileCardView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 111
    iget-object v0, p0, Lakh;->d:Lcom/twitter/ui/user/ProfileCardView;

    invoke-virtual {v0}, Lcom/twitter/ui/user/ProfileCardView;->c()V

    .line 113
    new-instance v0, Lakh$1;

    invoke-direct {v0, p0}, Lakh$1;-><init>(Lakh;)V

    iput-object v0, p0, Lakh;->j:Lcom/twitter/library/util/s;

    .line 120
    iget-object v0, p0, Lakh;->c:Landroid/view/ViewGroup;

    const v1, 0x7f13056a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lakh;->k:Landroid/view/View;

    .line 121
    iget-object v0, p0, Lakh;->j:Lcom/twitter/library/util/s;

    iget-object v1, p0, Lakh;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/twitter/library/util/s;->a(Landroid/view/View;)V

    .line 123
    iget-object v0, p0, Lakh;->d:Lcom/twitter/ui/user/ProfileCardView;

    iget-object v1, p0, Lakh;->g:Lakh$b;

    invoke-direct {p0, v0, v1}, Lakh;->a(Lcom/twitter/ui/user/ProfileCardView;Lakh$b;)V

    .line 125
    iget-object v0, p0, Lakh;->i:Lcom/twitter/model/util/FriendshipCache;

    iget-object v1, p0, Lakh;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v1}, Lcom/twitter/model/util/FriendshipCache;->a(Lcom/twitter/model/core/Tweet;)V

    .line 126
    invoke-direct {p0}, Lakh;->f()V

    .line 127
    return-void
.end method

.method private a(Landroid/content/Context;I)Landroid/view/View;
    .locals 3

    .prologue
    .line 130
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p2, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/ViewGroup;I)Lcom/twitter/ui/user/ProfileCardView;
    .locals 1

    .prologue
    .line 134
    const v0, 0x7f130569

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 135
    invoke-virtual {v0, p2}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 136
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 137
    const v0, 0x7f1305d2

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/user/ProfileCardView;

    return-object v0
.end method

.method private a(Landroid/content/Context;JJ)Lcom/twitter/util/android/d;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 149
    sget-object v0, Lcom/twitter/database/schema/a$z;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 150
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "ownerId"

    .line 151
    invoke-static {p4, p5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 152
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 154
    new-instance v0, Lcom/twitter/util/android/d;

    sget-object v3, Lbum;->a:[Ljava/lang/String;

    move-object v1, p1

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic a(Lakh;Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lakh;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    return-void
.end method

.method private a(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 329
    iget-object v0, p0, Lakh;->f:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lcom/twitter/model/core/Tweet;->b(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v0

    .line 330
    iget-object v1, p0, Lakh;->h:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v2, "user_recommendation"

    const-string/jumbo v3, "profile_click"

    invoke-static {v1, v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 332
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {p0}, Lakh;->h()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 333
    iget-object v2, p0, Lakh;->a:Landroid/content/Context;

    invoke-static {v1, v2, p2, v7}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 334
    iget-wide v2, p2, Lcom/twitter/model/core/Tweet;->b:J

    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v4

    invoke-static {v1, v2, v3, v4, v7}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;)V

    .line 335
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lakh;->h:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 337
    iget-wide v2, p2, Lcom/twitter/model/core/Tweet;->b:J

    iget-object v4, p2, Lcom/twitter/model/core/Tweet;->p:Ljava/lang/String;

    .line 338
    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v5

    move-object v1, p1

    move-object v6, p3

    .line 337
    invoke-static/range {v1 .. v7}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/model/timeline/r;)V

    .line 339
    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    .line 280
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 281
    const v1, 0x7f130035

    if-eq v0, v1, :cond_0

    const v1, 0x7f13061d

    if-ne v0, v1, :cond_3

    .line 282
    :cond_0
    invoke-direct {p0}, Lakh;->g()Lajj;

    move-result-object v0

    invoke-interface {v0}, Lajj;->a()V

    .line 283
    iget-object v0, p0, Lakh;->i:Lcom/twitter/model/util/FriendshipCache;

    iget-object v1, p0, Lakh;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v1, Lcom/twitter/model/core/Tweet;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->k(J)Z

    move-result v0

    .line 284
    iget-object v1, p0, Lakh;->l:Lakh$c;

    if-eqz v1, :cond_2

    .line 285
    iget-object v1, p0, Lakh;->l:Lakh$c;

    invoke-virtual {v1, v0}, Lakh$c;->a(Z)V

    .line 295
    :cond_1
    :goto_0
    return-void

    .line 287
    :cond_2
    iget-object v1, p0, Lakh;->d:Lcom/twitter/ui/user/ProfileCardView;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/user/ProfileCardView;->setIsFollowing(Z)V

    goto :goto_0

    .line 290
    :cond_3
    iget-object v0, p0, Lakh;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 291
    if-eqz v0, :cond_1

    .line 292
    iget-object v1, p0, Lakh;->f:Lcom/twitter/model/core/Tweet;

    iget-object v2, p0, Lakh;->h:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {p0, v0, v1, v2}, Lakh;->a(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 4

    .prologue
    .line 158
    iget-object v0, p0, Lakh;->d:Lcom/twitter/ui/user/ProfileCardView;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/user/ProfileCardView;->setUser(Lcom/twitter/model/core/TwitterUser;)V

    .line 159
    iget-object v0, p0, Lakh;->i:Lcom/twitter/model/util/FriendshipCache;

    iget-wide v2, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    iget v1, p1, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-virtual {v0, v2, v3, v1}, Lcom/twitter/model/util/FriendshipCache;->c(JI)V

    .line 160
    iget-object v0, p0, Lakh;->g:Lakh$b;

    iget-boolean v0, v0, Lakh$b;->b:Z

    if-nez v0, :cond_0

    .line 161
    iget-object v0, p0, Lakh;->d:Lcom/twitter/ui/user/ProfileCardView;

    invoke-virtual {v0}, Lcom/twitter/ui/user/ProfileCardView;->a()V

    .line 163
    :cond_0
    iget-object v0, p0, Lakh;->l:Lakh$c;

    if-eqz v0, :cond_1

    .line 164
    iget-object v0, p0, Lakh;->l:Lakh$c;

    iget v1, p1, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-static {v1}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lakh$c;->a(Z)V

    .line 166
    :cond_1
    return-void
.end method

.method private a(Lcom/twitter/ui/user/ProfileCardView;Lakh$b;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, -0x1

    const/high16 v4, 0x41400000    # 12.0f

    const/4 v3, -0x2

    const/4 v2, 0x0

    .line 169
    const/4 v0, -0x4

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/ProfileCardView;->setUserImageSize(I)V

    .line 170
    sget v0, Lcni;->a:F

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/ProfileCardView;->setContentSize(F)V

    .line 171
    const v0, 0x7f0200af

    invoke-virtual {p1, v0, p0}, Lcom/twitter/ui/user/ProfileCardView;->a(ILcom/twitter/ui/user/BaseUserView$a;)V

    .line 173
    iget-boolean v0, p2, Lakh$b;->a:Z

    if-nez v0, :cond_0

    .line 174
    invoke-virtual {p1}, Lcom/twitter/ui/user/ProfileCardView;->d()V

    .line 177
    :cond_0
    iget-object v0, p0, Lakh;->j:Lcom/twitter/library/util/s;

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/ProfileCardView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 178
    invoke-static {}, Lcom/twitter/android/revenue/k;->k()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/ProfileCardView;->setProfileDescriptionMaxLines(I)V

    .line 180
    iget-object v0, p0, Lakh;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 181
    iget-boolean v1, p2, Lakh$b;->c:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 182
    const v1, 0x7f040282

    invoke-direct {p0, v0, v1}, Lakh;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 184
    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Lcom/twitter/ui/user/ProfileCardView;->setFollowVisibility(I)V

    .line 186
    iget-boolean v1, p2, Lakh$b;->a:Z

    if-eqz v1, :cond_3

    .line 187
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 188
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 191
    const v1, 0x7f130012

    invoke-virtual {v2, v6, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 192
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0x9

    .line 194
    :goto_0
    invoke-virtual {v2, v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 196
    invoke-static {v4}, Lcom/twitter/util/z;->a(F)I

    move-result v1

    iput v1, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 197
    invoke-static {v4}, Lcom/twitter/util/z;->a(F)I

    move-result v1

    iput v1, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 199
    const v1, 0x7f130041

    invoke-virtual {p1, v1}, Lcom/twitter/ui/user/ProfileCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 200
    invoke-virtual {v1, v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 209
    :goto_1
    new-instance v2, Lakh$c;

    const v1, 0x7f130035

    .line 210
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/ui/widget/TwitterButton;

    const v3, 0x7f13061d

    .line 211
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    invoke-direct {v2, v1, v0}, Lakh$c;-><init>(Lcom/twitter/ui/widget/TwitterButton;Lcom/twitter/ui/widget/TwitterButton;)V

    iput-object v2, p0, Lakh;->l:Lakh$c;

    .line 213
    iget-object v0, p0, Lakh;->l:Lakh$c;

    iget-object v0, v0, Lakh$c;->a:Lcom/twitter/ui/widget/TwitterButton;

    new-instance v1, Lakh$2;

    iget-object v2, p0, Lakh;->l:Lakh$c;

    iget-object v2, v2, Lakh$c;->a:Lcom/twitter/ui/widget/TwitterButton;

    invoke-direct {v1, p0, v2}, Lakh$2;-><init>(Lakh;Lcom/twitter/ui/widget/TwitterButton;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 220
    iget-object v0, p0, Lakh;->l:Lakh$c;

    iget-object v0, v0, Lakh$c;->b:Lcom/twitter/ui/widget/TwitterButton;

    new-instance v1, Lakh$3;

    iget-object v2, p0, Lakh;->l:Lakh$c;

    iget-object v2, v2, Lakh$c;->b:Lcom/twitter/ui/widget/TwitterButton;

    invoke-direct {v1, p0, v2}, Lakh$3;-><init>(Lakh;Lcom/twitter/ui/widget/TwitterButton;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 228
    :cond_1
    return-void

    .line 192
    :cond_2
    const/16 v1, 0xb

    goto :goto_0

    .line 202
    :cond_3
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 205
    const v2, 0x7f130098

    invoke-virtual {v1, v6, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 206
    invoke-virtual {p1, v0, v1}, Lcom/twitter/ui/user/ProfileCardView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method

.method private f()V
    .locals 6

    .prologue
    .line 141
    iget-object v1, p0, Lakh;->a:Landroid/content/Context;

    iget-object v0, p0, Lakh;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v0, Lcom/twitter/model/core/Tweet;->b:J

    .line 142
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lakh;->a(Landroid/content/Context;JJ)Lcom/twitter/util/android/d;

    move-result-object v0

    iput-object v0, p0, Lakh;->e:Lcom/twitter/util/android/d;

    .line 143
    iget-object v0, p0, Lakh;->e:Lcom/twitter/util/android/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/twitter/util/android/d;->registerListener(ILandroid/support/v4/content/Loader$OnLoadCompleteListener;)V

    .line 144
    iget-object v0, p0, Lakh;->e:Lcom/twitter/util/android/d;

    invoke-virtual {v0}, Lcom/twitter/util/android/d;->startLoading()V

    .line 145
    return-void
.end method

.method private g()Lajj;
    .locals 2

    .prologue
    .line 319
    iget-object v0, p0, Lakh;->a:Landroid/content/Context;

    iget-object v1, p0, Lakh;->h:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-static {v0, v1}, Lajk;->a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lajk;

    move-result-object v0

    .line 320
    iget-object v1, p0, Lakh;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lajg$a;->a(Landroid/content/Context;Lajk;)Lajg$a;

    move-result-object v0

    iget-object v1, p0, Lakh;->f:Lcom/twitter/model/core/Tweet;

    .line 321
    invoke-virtual {v0, v1}, Lajg$a;->a(Lcom/twitter/model/core/Tweet;)Lajg$a;

    move-result-object v0

    iget-object v1, p0, Lakh;->i:Lcom/twitter/model/util/FriendshipCache;

    .line 322
    invoke-virtual {v0, v1}, Lajg$a;->a(Lcom/twitter/model/util/FriendshipCache;)Lajg$a;

    move-result-object v0

    const-string/jumbo v1, "user_recommendation"

    .line 323
    invoke-virtual {v0, v1}, Lajg$a;->a(Ljava/lang/String;)Lajg$a;

    move-result-object v0

    .line 324
    invoke-virtual {v0}, Lajg$a;->a()Lajj;

    move-result-object v0

    .line 320
    return-object v0
.end method

.method private h()J
    .locals 2

    .prologue
    .line 342
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lakh;->e:Lcom/twitter/util/android/d;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lakh;->e:Lcom/twitter/util/android/d;

    invoke-virtual {v0, p0}, Lcom/twitter/util/android/d;->unregisterListener(Landroid/support/v4/content/Loader$OnLoadCompleteListener;)V

    .line 238
    iget-object v0, p0, Lakh;->e:Lcom/twitter/util/android/d;

    invoke-virtual {v0}, Lcom/twitter/util/android/d;->cancelLoad()Z

    .line 239
    iget-object v0, p0, Lakh;->e:Lcom/twitter/util/android/d;

    invoke-virtual {v0}, Lcom/twitter/util/android/d;->stopLoading()V

    .line 241
    :cond_0
    return-void
.end method

.method public a(Lakh$a;)V
    .locals 0

    .prologue
    .line 232
    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 277
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 306
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 307
    invoke-static {p2}, Lcom/twitter/library/provider/t;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 308
    invoke-direct {p0, v0}, Lakh;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 314
    :goto_0
    invoke-static {p2}, Lcqc;->a(Landroid/database/Cursor;)V

    .line 315
    return-void

    .line 310
    :cond_0
    new-instance v0, Lcpb;

    new-instance v1, Ljava/lang/Exception;

    const-string/jumbo v2, "UserContent failed to load user"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    const-string/jumbo v1, "tweet_id"

    iget-object v2, p0, Lakh;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v2, Lcom/twitter/model/core/Tweet;->G:J

    .line 311
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    iget-object v2, p0, Lakh;->f:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v2, Lcom/twitter/model/core/Tweet;->b:J

    .line 312
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    .line 310
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Lcom/twitter/ui/user/BaseUserView;JII)V
    .locals 6

    .prologue
    .line 54
    move-object v1, p1

    check-cast v1, Lcom/twitter/ui/user/UserView;

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lakh;->a(Lcom/twitter/ui/user/UserView;JII)V

    return-void
.end method

.method public a(Lcom/twitter/ui/user/UserView;JII)V
    .locals 1

    .prologue
    .line 299
    const v0, 0x7f130003

    if-ne v0, p4, :cond_0

    .line 300
    invoke-direct {p0}, Lakh;->g()Lajj;

    move-result-object v0

    invoke-interface {v0}, Lajj;->a()V

    .line 302
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 54
    check-cast p1, Lakh$a;

    invoke-virtual {p0, p1}, Lakh;->a(Lakh$a;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 258
    return-void
.end method

.method public ai_()V
    .locals 0

    .prologue
    .line 254
    return-void
.end method

.method public aj_()V
    .locals 0

    .prologue
    .line 273
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 245
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 262
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 268
    invoke-direct {p0}, Lakh;->f()V

    .line 269
    return-void
.end method

.method public e()Landroid/view/View;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lakh;->c:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public synthetic onLoadComplete(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 54
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lakh;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method
