.class public Lcit;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcit$a;
    }
.end annotation


# static fields
.field private static final a:Landroid/view/animation/Interpolator;


# instance fields
.field private final b:Landroid/view/View;

.field private final c:Landroid/graphics/RectF;

.field private final d:Lcit$a;

.field private final e:Lcis;

.field private final f:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private g:F

.field private h:F

.field private i:F

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/twitter/ui/anim/d;

    invoke-direct {v0}, Lcom/twitter/ui/anim/d;-><init>()V

    sput-object v0, Lcit;->a:Landroid/view/animation/Interpolator;

    return-void
.end method

.method constructor <init>(Landroid/view/View;Landroid/graphics/RectF;Lcit$a;Lcis;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcit;->f:Lrx/subjects/PublishSubject;

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcit;->j:I

    .line 54
    iput-object p1, p0, Lcit;->b:Landroid/view/View;

    .line 55
    iput-object p2, p0, Lcit;->c:Landroid/graphics/RectF;

    .line 56
    iput-object p3, p0, Lcit;->d:Lcit$a;

    .line 57
    iput-object p4, p0, Lcit;->e:Lcis;

    .line 58
    return-void
.end method

.method static synthetic a(Lcit;)Landroid/view/View;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcit;->b:Landroid/view/View;

    return-object v0
.end method

.method public static a(Landroid/view/View;Landroid/graphics/RectF;Lcis;)Lcit;
    .locals 2

    .prologue
    .line 45
    new-instance v0, Lcit;

    new-instance v1, Lcit$a;

    invoke-direct {v1}, Lcit$a;-><init>()V

    invoke-direct {v0, p0, p1, v1, p2}, Lcit;-><init>(Landroid/view/View;Landroid/graphics/RectF;Lcit$a;Lcis;)V

    return-object v0
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 200
    iput p1, p0, Lcit;->j:I

    .line 201
    iget-object v0, p0, Lcit;->f:Lrx/subjects/PublishSubject;

    iget v1, p0, Lcit;->j:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject;->a(Ljava/lang/Object;)V

    .line 202
    return-void
.end method

.method static synthetic b(Lcit;)Lcis;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcit;->e:Lcis;

    return-object v0
.end method

.method private b(F)Z
    .locals 1

    .prologue
    .line 213
    iget v0, p0, Lcit;->h:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic g()Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcit;->a:Landroid/view/animation/Interpolator;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 157
    iget-object v0, p0, Lcit;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getScaleX()F

    move-result v0

    .line 158
    iget v1, p0, Lcit;->h:F

    iget v2, p0, Lcit;->g:F

    add-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    .line 159
    invoke-direct {p0, v0}, Lcit;->b(F)Z

    move-result v2

    if-eqz v2, :cond_1

    cmpg-float v2, v0, v1

    if-gez v2, :cond_1

    .line 161
    invoke-virtual {p0}, Lcit;->d()V

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 162
    :cond_1
    invoke-direct {p0, v0}, Lcit;->b(F)Z

    move-result v2

    if-eqz v2, :cond_0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 164
    invoke-virtual {p0}, Lcit;->e()V

    goto :goto_0
.end method

.method public a(F)V
    .locals 3

    .prologue
    .line 141
    iget-object v0, p0, Lcit;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getScaleX()F

    move-result v0

    mul-float/2addr v0, p1

    iget v1, p0, Lcit;->g:F

    iget v2, p0, Lcit;->i:F

    invoke-static {v0, v1, v2}, Lcom/twitter/util/math/b;->a(FFF)F

    move-result v0

    .line 142
    iget-object v1, p0, Lcit;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 143
    iget-object v1, p0, Lcit;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 144
    iget-object v1, p0, Lcit;->e:Lcis;

    invoke-virtual {v1}, Lcis;->b()V

    .line 145
    invoke-direct {p0, v0}, Lcit;->b(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcit;->a(I)V

    .line 150
    :goto_0
    return-void

    .line 148
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcit;->a(I)V

    goto :goto_0
.end method

.method a(IILcom/twitter/util/math/c;ZZ)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 87
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    iget-object v2, p0, Lcit;->c:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v3

    .line 94
    iget-object v2, p0, Lcit;->c:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v4

    .line 96
    invoke-static {p1, p2}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v2

    invoke-static {v4, v3}, Lcom/twitter/util/math/Size;->a(FF)Lcom/twitter/util/math/Size;

    move-result-object v5

    invoke-static {v2, v5, v1}, Lcom/twitter/util/math/b;->a(Lcom/twitter/util/math/Size;Lcom/twitter/util/math/Size;Z)F

    move-result v2

    iput v2, p0, Lcit;->h:F

    .line 97
    iget v2, p0, Lcit;->h:F

    const/high16 v5, 0x40a00000    # 5.0f

    mul-float/2addr v2, v5

    iput v2, p0, Lcit;->i:F

    .line 101
    invoke-static {p1, p2}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v2

    invoke-static {v4, v3}, Lcom/twitter/util/math/Size;->a(FF)Lcom/twitter/util/math/Size;

    move-result-object v5

    invoke-static {v2, v5, v0}, Lcom/twitter/util/math/b;->a(Lcom/twitter/util/math/Size;Lcom/twitter/util/math/Size;Z)F

    move-result v2

    iput v2, p0, Lcit;->g:F

    .line 104
    if-nez p5, :cond_2

    .line 105
    iget v2, p0, Lcit;->h:F

    iput v2, p0, Lcit;->g:F

    .line 109
    :cond_2
    if-eqz p3, :cond_3

    if-nez p4, :cond_3

    move v2, v0

    .line 110
    :goto_1
    if-eqz p4, :cond_4

    :goto_2
    invoke-direct {p0, v0}, Lcit;->a(I)V

    .line 111
    if-eqz v2, :cond_5

    .line 112
    int-to-float v0, p1

    invoke-virtual {p3}, Lcom/twitter/util/math/c;->c()F

    move-result v5

    mul-float/2addr v0, v5

    .line 113
    int-to-float v5, p2

    invoke-virtual {p3}, Lcom/twitter/util/math/c;->d()F

    move-result v6

    mul-float/2addr v5, v6

    .line 115
    invoke-static {v0, v5}, Lcom/twitter/util/math/Size;->a(FF)Lcom/twitter/util/math/Size;

    move-result-object v0

    .line 116
    invoke-static {v4, v3}, Lcom/twitter/util/math/Size;->a(FF)Lcom/twitter/util/math/Size;

    move-result-object v3

    .line 115
    invoke-static {v0, v3, v1}, Lcom/twitter/util/math/b;->a(Lcom/twitter/util/math/Size;Lcom/twitter/util/math/Size;Z)F

    move-result v0

    .line 124
    :goto_3
    iget-object v1, p0, Lcit;->b:Landroid/view/View;

    iget-object v3, p0, Lcit;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setPivotX(F)V

    .line 125
    iget-object v1, p0, Lcit;->b:Landroid/view/View;

    iget-object v3, p0, Lcit;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setPivotY(F)V

    .line 126
    iget-object v1, p0, Lcit;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 127
    iget-object v1, p0, Lcit;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 130
    if-eqz v2, :cond_0

    .line 131
    iget-object v0, p0, Lcit;->e:Lcis;

    invoke-virtual {v0, p3}, Lcis;->a(Lcom/twitter/util/math/c;)V

    goto/16 :goto_0

    :cond_3
    move v2, v1

    .line 109
    goto :goto_1

    :cond_4
    move v0, v1

    .line 110
    goto :goto_2

    .line 117
    :cond_5
    if-eqz p4, :cond_6

    .line 118
    iget v0, p0, Lcit;->g:F

    goto :goto_3

    .line 120
    :cond_6
    iget v0, p0, Lcit;->h:F

    goto :goto_3
.end method

.method public a(Lcom/twitter/util/math/c;ZZ)V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcit;->b:Landroid/view/View;

    new-instance v1, Lcit$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcit$1;-><init>(Lcit;Lcom/twitter/util/math/c;ZZ)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 75
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 169
    invoke-virtual {p0}, Lcit;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {p0}, Lcit;->e()V

    .line 174
    :goto_0
    return-void

    .line 172
    :cond_0
    invoke-virtual {p0}, Lcit;->d()V

    goto :goto_0
.end method

.method public c()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lcit;->f:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method public d()V
    .locals 4

    .prologue
    .line 185
    iget-object v0, p0, Lcit;->d:Lcit$a;

    iget-object v1, p0, Lcit;->b:Landroid/view/View;

    iget v2, p0, Lcit;->g:F

    new-instance v3, Lcit$2;

    invoke-direct {v3, p0}, Lcit$2;-><init>(Lcit;)V

    invoke-virtual {v0, v1, v2, v3}, Lcit$a;->a(Landroid/view/View;FLandroid/support/v4/view/ViewPropertyAnimatorUpdateListener;)V

    .line 191
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcit;->a(I)V

    .line 192
    return-void
.end method

.method public e()V
    .locals 4

    .prologue
    .line 195
    iget-object v0, p0, Lcit;->d:Lcit$a;

    iget-object v1, p0, Lcit;->b:Landroid/view/View;

    iget v2, p0, Lcit;->h:F

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcit$a;->a(Landroid/view/View;FLandroid/support/v4/view/ViewPropertyAnimatorUpdateListener;)V

    .line 196
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcit;->a(I)V

    .line 197
    return-void
.end method

.method public f()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 205
    iget v1, p0, Lcit;->j:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
