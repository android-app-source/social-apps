.class public Lail;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:I

.field public final b:Z


# direct methods
.method public constructor <init>(IZ)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput p1, p0, Lail;->a:I

    .line 12
    iput-boolean p2, p0, Lail;->b:Z

    .line 13
    return-void
.end method


# virtual methods
.method public a(Lail;)Z
    .locals 2

    .prologue
    .line 21
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    iget v0, p0, Lail;->a:I

    iget v1, p1, Lail;->a:I

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lail;->b:Z

    iget-boolean v1, p1, Lail;->b:Z

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 17
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lail;

    if-eqz v0, :cond_1

    check-cast p1, Lail;

    invoke-virtual {p0, p1}, Lail;->a(Lail;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 28
    iget v0, p0, Lail;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-boolean v1, p0, Lail;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
