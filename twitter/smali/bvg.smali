.class public Lbvg;
.super Lbvd;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbvd",
        "<",
        "Lcom/twitter/util/collection/Pair",
        "<",
        "Lbec;",
        "Lcom/twitter/library/service/u;",
        ">;",
        "Lbec;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lbve;Lrx/f;Lbvo;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lbvd;-><init>(Laum;Lrx/f;Lbvo;)V

    .line 31
    return-void
.end method

.method protected static b(J)Lbvd$a;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lbvd$a$a;

    invoke-direct {v0}, Lbvd$a$a;-><init>()V

    const-string/jumbo v1, "app:twitter_service:gcm_registration:destroy_request"

    .line 41
    invoke-virtual {v0, v1}, Lbvd$a$a;->a(Ljava/lang/String;)Lbvd$a$a;

    move-result-object v0

    .line 42
    invoke-virtual {v0, p0, p1}, Lbvd$a$a;->a(J)Lbvd$a$a;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lbvd$a$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvd$a;

    .line 40
    return-object v0
.end method


# virtual methods
.method public a(J)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/g",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Lbec;",
            "Lcom/twitter/library/service/u;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 35
    invoke-static {p1, p2}, Lbvg;->b(J)Lbvd$a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbvg;->a(Lbvd$a;)Lrx/g;

    move-result-object v0

    return-object v0
.end method
