.class final Lcfw$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcfw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcfw;",
        "Lcfw$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcfw$1;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lcfw$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcfw$a;
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcfw$a;

    invoke-direct {v0}, Lcfw$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcfw$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 96
    sget-object v0, Lcfv;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfv;

    invoke-virtual {p2, v0}, Lcfw$a;->a(Lcfv;)Lcfw$a;

    move-result-object v1

    sget-object v0, Lcfv;->a:Lcom/twitter/util/serialization/l;

    .line 97
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfv;

    invoke-virtual {v1, v0}, Lcfw$a;->b(Lcfv;)Lcfw$a;

    move-result-object v1

    sget-object v0, Lcfv;->a:Lcom/twitter/util/serialization/l;

    .line 98
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfv;

    invoke-virtual {v1, v0}, Lcfw$a;->c(Lcfv;)Lcfw$a;

    move-result-object v1

    sget-object v0, Lcfs;->b:Lcom/twitter/util/serialization/l;

    .line 99
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcfw$a;->a(Ljava/util/List;)Lcfw$a;

    .line 100
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 86
    check-cast p2, Lcfw$a;

    invoke-virtual {p0, p1, p2, p3}, Lcfw$b;->a(Lcom/twitter/util/serialization/n;Lcfw$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcfw;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p2, Lcfw;->b:Lcfv;

    sget-object v1, Lcfv;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcfw;->c:Lcfv;

    sget-object v2, Lcfv;->a:Lcom/twitter/util/serialization/l;

    .line 106
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcfw;->d:Lcfv;

    sget-object v2, Lcfv;->a:Lcom/twitter/util/serialization/l;

    .line 107
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcfw;->e:Ljava/util/List;

    sget-object v2, Lcfs;->b:Lcom/twitter/util/serialization/l;

    .line 108
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 109
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    check-cast p2, Lcfw;

    invoke-virtual {p0, p1, p2}, Lcfw$b;->a(Lcom/twitter/util/serialization/o;Lcfw;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Lcfw$b;->a()Lcfw$a;

    move-result-object v0

    return-object v0
.end method
