.class public Lajk;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lajk;->a:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lajk;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 25
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lajk;
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lajk;

    invoke-direct {v0, p0, p1}, Lajk;-><init>(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    return-object v0
.end method

.method private a(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    const-string/jumbo v0, "focal"

    .line 43
    :goto_0
    return-object v0

    .line 40
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->B()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    const-string/jumbo v0, "ancestor"

    goto :goto_0

    .line 43
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 5

    .prologue
    .line 49
    invoke-static {p3}, Lcom/twitter/model/core/Tweet;->b(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v0

    .line 50
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    .line 51
    iget-object v2, p0, Lajk;->a:Landroid/content/Context;

    invoke-direct {p0, p3}, Lajk;->a(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, p3, v3}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 52
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lajk;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-static {v4, v0, p1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lajk;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 53
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 54
    invoke-virtual {v0, p4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 52
    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3, p4}, Lajk;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 35
    return-void
.end method
