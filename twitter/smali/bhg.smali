.class public Lbhg;
.super Lcom/twitter/library/api/r;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/api/r",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcom/twitter/model/core/ac;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final b:J

.field private final c:Lcom/twitter/library/api/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/ac;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/twitter/library/provider/t;

.field private final h:Z

.field private final i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JZ)V
    .locals 7

    .prologue
    .line 50
    new-instance v3, Lcom/twitter/library/service/v;

    invoke-direct {v3, p2}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p3

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lbhg;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;JZ)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;JZ)V
    .locals 9

    .prologue
    .line 55
    iget-wide v0, p2, Lcom/twitter/library/service/v;->c:J

    .line 56
    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v7

    const-class v0, Lcom/twitter/model/core/ac;

    .line 57
    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    .line 55
    invoke-direct/range {v1 .. v8}, Lbhg;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;JZLcom/twitter/library/provider/t;Lcom/twitter/library/api/i;)V

    .line 58
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;JZLcom/twitter/library/provider/t;Lcom/twitter/library/api/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/service/v;",
            "JZ",
            "Lcom/twitter/library/provider/t;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/ac;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    const-class v0, Lbhg;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/r;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 64
    iput-wide p3, p0, Lbhg;->b:J

    .line 65
    iput-boolean p5, p0, Lbhg;->h:Z

    .line 66
    iput-object p6, p0, Lbhg;->g:Lcom/twitter/library/provider/t;

    .line 67
    iput-object p7, p0, Lbhg;->c:Lcom/twitter/library/api/i;

    .line 70
    invoke-static {p3, p4, p2}, Lbhd;->a(JLcom/twitter/library/service/v;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbhg;->i:Ljava/lang/String;

    .line 71
    new-instance v0, Lcom/twitter/library/service/o;

    invoke-direct {v0}, Lcom/twitter/library/service/o;-><init>()V

    invoke-virtual {p0, v0}, Lbhg;->a(Lcom/twitter/async/service/k;)Lcom/twitter/async/service/AsyncOperation;

    .line 72
    return-void
.end method

.method static synthetic a(Lbhg;)Laut;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lbhg;->S()Laut;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lbhg;)J
    .locals 2

    .prologue
    .line 32
    iget-wide v0, p0, Lbhg;->b:J

    return-wide v0
.end method

.method static synthetic c(Lbhg;)Lcom/twitter/library/provider/t;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lbhg;->g:Lcom/twitter/library/provider/t;

    return-object v0
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 6

    .prologue
    .line 77
    invoke-virtual {p0}, Lbhg;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 78
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "statuses"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "unretweet"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-wide v4, p0, Lbhg;->b:J

    .line 79
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->d()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->c()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    .line 77
    return-object v0
.end method

.method public a(Lcom/twitter/async/service/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 105
    invoke-super {p0, p1}, Lcom/twitter/library/api/r;->a(Lcom/twitter/async/service/j;)V

    .line 106
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 107
    if-eqz v0, :cond_0

    .line 108
    invoke-virtual {p0, v0}, Lbhg;->c(Lcom/twitter/library/service/u;)V

    .line 110
    :cond_0
    return-void
.end method

.method protected b()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/ac;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lbhg;->c:Lcom/twitter/library/api/i;

    return-object v0
.end method

.method public c(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 89
    if-eqz p1, :cond_0

    .line 90
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/twitter/async/service/AsyncOperation;->cancel(Z)Z

    .line 92
    :cond_0
    new-instance v0, Lbhg$1;

    invoke-direct {v0, p0}, Lbhg$1;-><init>(Lbhg;)V

    return-object v0
.end method

.method c(Lcom/twitter/library/service/u;)V
    .locals 14

    .prologue
    .line 127
    invoke-virtual {p0}, Lbhg;->S()Laut;

    move-result-object v6

    .line 128
    invoke-virtual {p0}, Lbhg;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-wide v2, v0, Lcom/twitter/library/service/v;->c:J

    .line 129
    invoke-virtual {p1}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lbhg;->c:Lcom/twitter/library/api/i;

    invoke-virtual {v0}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/core/ac;

    .line 132
    if-eqz v4, :cond_0

    .line 133
    iget-object v1, p0, Lbhg;->g:Lcom/twitter/library/provider/t;

    iget-boolean v5, p0, Lbhg;->h:Z

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/t;->b(JLcom/twitter/model/core/ac;ZLaut;)V

    .line 138
    :cond_0
    iget-object v1, p0, Lbhg;->g:Lcom/twitter/library/provider/t;

    iget-wide v4, p0, Lbhg;->b:J

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/t;->a(JJLaut;)V

    .line 143
    :goto_0
    invoke-virtual {v6}, Laut;->a()V

    .line 144
    return-void

    .line 141
    :cond_1
    iget-object v7, p0, Lbhg;->g:Lcom/twitter/library/provider/t;

    iget-wide v10, p0, Lbhg;->b:J

    const/4 v12, 0x1

    move-wide v8, v2

    move-object v13, v6

    invoke-virtual/range {v7 .. v13}, Lcom/twitter/library/provider/t;->a(JJZLaut;)I

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    const-string/jumbo v0, "app:twitter_service:retweet:delete"

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lbhg;->b()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 123
    iget-wide v0, p0, Lbhg;->b:J

    return-wide v0
.end method

.method protected o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lbhg;->i:Ljava/lang/String;

    return-object v0
.end method
