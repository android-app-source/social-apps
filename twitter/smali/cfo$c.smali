.class Lcfo$c;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcfo;",
        "Lcfo$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcfo$1;)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Lcfo$c;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcfo$a;
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcfo$a;

    invoke-direct {v0}, Lcfo$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcfo$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 86
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcfo$a;->a(J)Lcfo$a;

    move-result-object v0

    .line 87
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcfo$a;->a(Ljava/lang/String;)Lcfo$a;

    move-result-object v1

    sget-object v0, Lcfo$b;->a:Lcom/twitter/util/serialization/l;

    .line 88
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfo$b;

    invoke-virtual {v1, v0}, Lcfo$a;->a(Lcfo$b;)Lcfo$a;

    .line 89
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 75
    check-cast p2, Lcfo$a;

    invoke-virtual {p0, p1, p2, p3}, Lcfo$c;->a(Lcom/twitter/util/serialization/n;Lcfo$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcfo;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    iget-wide v0, p2, Lcfo;->b:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcfo;->c:Ljava/lang/String;

    .line 95
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcfo;->d:Lcfo$b;

    sget-object v2, Lcfo$b;->a:Lcom/twitter/util/serialization/l;

    .line 96
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 97
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    check-cast p2, Lcfo;

    invoke-virtual {p0, p1, p2}, Lcfo$c;->a(Lcom/twitter/util/serialization/o;Lcfo;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0}, Lcfo$c;->a()Lcfo$a;

    move-result-object v0

    return-object v0
.end method
