.class public Lccv;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lccv$a;,
        Lccv$b;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lccv;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/Object;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lccv$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lccv$b;-><init>(Lccv$1;)V

    sput-object v0, Lccv;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(Lccv$a;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, Lccv$a;->a(Lccv$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lccv;->b:Ljava/lang/String;

    .line 34
    invoke-static {p1}, Lccv$a;->b(Lccv$a;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lccv;->c:Ljava/lang/Object;

    .line 35
    invoke-static {p1}, Lccv$a;->c(Lccv$a;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lccv;->d:Ljava/util/List;

    .line 36
    invoke-static {p1}, Lccv$a;->d(Lccv$a;)Z

    move-result v0

    iput-boolean v0, p0, Lccv;->e:Z

    .line 37
    return-void
.end method
