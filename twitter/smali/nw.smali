.class public Lnw;
.super Lbjb;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/av/playback/AVPlayer;

.field private final b:Lph;

.field private volatile c:J


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V
    .locals 4

    .prologue
    .line 34
    new-instance v0, Lph;

    const-wide/16 v2, 0x7530

    invoke-direct {v0, v2, v3}, Lph;-><init>(J)V

    invoke-direct {p0, p1, p2, v0}, Lnw;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;Lph;)V

    .line 35
    return-void
.end method

.method constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;Lph;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p2}, Lbjb;-><init>(Lcom/twitter/model/av/AVMedia;)V

    .line 40
    iput-object p1, p0, Lnw;->a:Lcom/twitter/library/av/playback/AVPlayer;

    .line 41
    iput-object p3, p0, Lnw;->b:Lph;

    .line 42
    return-void
.end method

.method private declared-synchronized a()J
    .locals 4

    .prologue
    .line 57
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lnw;->c:J

    const-wide/16 v2, 0xa

    add-long/2addr v0, v2

    iput-wide v0, p0, Lnw;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public processTick(Lbki;)V
    .locals 4
    .annotation runtime Lbiz;
        a = Lbki;
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lnw;->b:Lph;

    invoke-direct {p0}, Lnw;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lph;->a(J)V

    .line 48
    iget-object v0, p0, Lnw;->b:Lph;

    invoke-virtual {v0}, Lph;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lnw;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {p0}, Lnw;->b()Lcom/twitter/library/av/m;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/library/av/m;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    const-string/jumbo v2, "checkpoint"

    invoke-virtual {v1, v2}, Lcom/twitter/library/av/m$a;->a(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/av/m$a;->a()Lcom/twitter/library/av/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/m;)V

    .line 51
    :cond_0
    return-void
.end method
