.class public Lcnp;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcnp$a;
    }
.end annotation


# instance fields
.field private final a:Lcno;

.field private final b:Lcno$a;

.field private final c:Landroid/view/View;

.field private final d:Landroid/view/View;

.field private e:Lcnp$a;


# direct methods
.method public constructor <init>(Lcno;Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcnp;->a:Lcno;

    .line 27
    new-instance v0, Lcnp$1;

    invoke-direct {v0, p0}, Lcnp$1;-><init>(Lcnp;)V

    iput-object v0, p0, Lcnp;->b:Lcno$a;

    .line 58
    iget-object v0, p0, Lcnp;->a:Lcno;

    iget-object v1, p0, Lcnp;->b:Lcno$a;

    invoke-interface {v0, v1}, Lcno;->a(Lcno$c;)V

    .line 60
    iput-object p2, p0, Lcnp;->c:Landroid/view/View;

    .line 61
    if-eqz p2, :cond_0

    .line 62
    iget-object v0, p0, Lcnp;->a:Lcno;

    invoke-interface {v0, p2}, Lcno;->c(Landroid/view/View;)V

    .line 63
    invoke-virtual {p0, v2}, Lcnp;->a(Z)V

    .line 66
    :cond_0
    iput-object p3, p0, Lcnp;->d:Landroid/view/View;

    .line 67
    if-eqz p3, :cond_1

    .line 68
    iget-object v0, p0, Lcnp;->a:Lcno;

    invoke-interface {v0, p3}, Lcno;->d(Landroid/view/View;)V

    .line 69
    invoke-virtual {p0, v2}, Lcnp;->b(Z)V

    .line 71
    :cond_1
    return-void
.end method

.method static synthetic a(Lcnp;)Lcnp$a;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcnp;->e:Lcnp$a;

    return-object v0
.end method

.method static synthetic b(Lcnp;)Landroid/view/View;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcnp;->c:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lcnp;)Landroid/view/View;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcnp;->d:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public a(Z)V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcnp;->c:Landroid/view/View;

    if-nez v0, :cond_1

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    iget-object v0, p0, Lcnp;->c:Landroid/view/View;

    sget v1, Lckh$g;->progress_bar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 83
    if-eqz v1, :cond_0

    .line 84
    if-eqz p1, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public b(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 89
    iget-object v0, p0, Lcnp;->d:Landroid/view/View;

    if-nez v0, :cond_1

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    iget-object v0, p0, Lcnp;->d:Landroid/view/View;

    sget v3, Lckh$g;->progress_bar:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 94
    if-eqz v3, :cond_2

    .line 95
    if-eqz p1, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 98
    :cond_2
    iget-object v0, p0, Lcnp;->d:Landroid/view/View;

    sget v3, Lckh$g;->progress_dot:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 99
    if-eqz v0, :cond_0

    .line 100
    if-eqz p1, :cond_4

    :goto_2
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 95
    goto :goto_1

    :cond_4
    move v2, v1

    .line 100
    goto :goto_2
.end method
