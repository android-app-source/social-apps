.class public Lafr;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lafw;


# static fields
.field private static final a:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/util/List",
            "<",
            "Lafu$a;",
            ">;>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lafu$a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0xb

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    sput-object v0, Lafr;->a:Landroid/util/LruCache;

    return-void
.end method

.method public constructor <init>(JLauj;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lauj",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lafu$a;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-wide p1, p0, Lafr;->c:J

    .line 39
    iput-object p3, p0, Lafr;->b:Lauj;

    .line 40
    return-void
.end method

.method static synthetic a(Lafr;)J
    .locals 2

    .prologue
    .line 25
    iget-wide v0, p0, Lafr;->c:J

    return-wide v0
.end method

.method static synthetic a()Landroid/util/LruCache;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lafr;->a:Landroid/util/LruCache;

    return-object v0
.end method

.method static synthetic a(JJ)Lcom/twitter/util/collection/Pair;
    .locals 2

    .prologue
    .line 25
    invoke-static {p0, p1, p2, p3}, Lafr;->c(JJ)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    return-object v0
.end method

.method private b(JJ)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/List",
            "<",
            "Lafu$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    sget-object v0, Lafr;->a:Landroid/util/LruCache;

    .line 66
    invoke-static {p1, p2, p3, p4}, Lafr;->c(JJ)Lcom/twitter/util/collection/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 67
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(JJ)Lcom/twitter/util/collection/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(J)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<",
            "Lafu$a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 45
    iget-wide v0, p0, Lafr;->c:J

    invoke-direct {p0, v0, v1, p1, p2}, Lafr;->b(JJ)Ljava/util/List;

    move-result-object v0

    .line 46
    if-eqz v0, :cond_0

    .line 47
    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 50
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lafr;->b:Lauj;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    new-instance v1, Lafr$1;

    invoke-direct {v1, p0, p1, p2}, Lafr$1;-><init>(Lafr;J)V

    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    goto :goto_0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lafr;->b:Lauj;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 73
    return-void
.end method
