.class public abstract Laef;
.super Lckb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laef$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lbzz",
        "<*>;VH:",
        "Laef$a;",
        ">",
        "Lckb",
        "<TT;TVH;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/notificationtimeline/a;

.field private final b:Lcom/twitter/android/notificationtimeline/h;


# direct methods
.method protected constructor <init>(Lcom/twitter/android/notificationtimeline/a;Lcom/twitter/android/notificationtimeline/h;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lckb;-><init>()V

    .line 32
    iput-object p1, p0, Laef;->a:Lcom/twitter/android/notificationtimeline/a;

    .line 33
    iput-object p2, p0, Laef;->b:Lcom/twitter/android/notificationtimeline/h;

    .line 34
    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/content/Context;Lbzz;I)Lcom/twitter/analytics/model/ScribeItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TT;I)",
            "Lcom/twitter/analytics/model/ScribeItem;"
        }
    .end annotation
.end method

.method public a(Laef$a;Lbzz;)V
    .locals 7
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;TT;)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-virtual {p1}, Laef$a;->b()Landroid/view/View;

    move-result-object v0

    .line 40
    iget-object v1, p0, Laef;->b:Lcom/twitter/android/notificationtimeline/h;

    iget-object v2, p2, Lbzz;->b:Lcac;

    invoke-virtual {v2}, Lcac;->c()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/notificationtimeline/h;->a(J)V

    .line 41
    iget-object v1, p0, Laef;->a:Lcom/twitter/android/notificationtimeline/a;

    iget-object v2, p2, Lbzz;->b:Lcac;

    invoke-virtual {v2}, Lcac;->a()J

    move-result-wide v2

    .line 42
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    iget v5, p1, Laef$a;->j:I

    invoke-virtual {p0, v4, p2, v5}, Laef;->a(Landroid/content/Context;Lbzz;I)Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v4

    invoke-virtual {p0, p2}, Laef;->a(Lbzz;)Z

    move-result v5

    .line 43
    invoke-virtual {p2}, Lbzz;->a()Ljava/util/List;

    move-result-object v6

    invoke-static {v6}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/model/core/Tweet;

    .line 41
    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/notificationtimeline/a;->a(JLcom/twitter/analytics/model/ScribeItem;ZLcom/twitter/model/core/Tweet;)V

    .line 44
    instance-of v1, v0, Lcom/twitter/internal/android/widget/b;

    if-eqz v1, :cond_0

    .line 45
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/b;

    invoke-virtual {p0, p2}, Laef;->a(Lbzz;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-interface {v0, v1}, Lcom/twitter/internal/android/widget/b;->setHighlighted(Z)V

    .line 47
    :cond_0
    return-void

    .line 45
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Lckb$a;Ljava/lang/Object;)V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 23
    check-cast p1, Laef$a;

    check-cast p2, Lbzz;

    invoke-virtual {p0, p1, p2}, Laef;->a(Laef$a;Lbzz;)V

    return-void
.end method

.method protected a(Lbzz;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p1, Lbzz;->a:Lbzy;

    iget-boolean v0, v0, Lbzy;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p1, Lbzz;->b:Lcac;

    invoke-virtual {v0}, Lcac;->c()J

    move-result-wide v0

    iget-object v2, p0, Laef;->b:Lcom/twitter/android/notificationtimeline/h;

    invoke-virtual {v2}, Lcom/twitter/android/notificationtimeline/h;->a()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 23
    check-cast p1, Lbzz;

    invoke-virtual {p0, p1}, Laef;->b(Lbzz;)Z

    move-result v0

    return v0
.end method

.method public b(Lbzz;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 66
    const/4 v0, 0x1

    return v0
.end method
