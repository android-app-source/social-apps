.class public Lob;
.super Lbiy;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lbiy;-><init>()V

    return-void
.end method

.method private a(I)V
    .locals 8

    .prologue
    .line 41
    invoke-static {}, Lcom/twitter/library/network/b;->a()Lcom/twitter/library/network/b;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/DataUsageEvent;

    sget-object v2, Lcom/twitter/library/network/DataUsageEvent$Type;->d:Lcom/twitter/library/network/DataUsageEvent$Type;

    .line 43
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v3

    invoke-virtual {v3}, Lcrr;->c()Z

    move-result v3

    int-to-long v4, p1

    const-wide/16 v6, 0x0

    invoke-direct/range {v1 .. v7}, Lcom/twitter/library/network/DataUsageEvent;-><init>(Lcom/twitter/library/network/DataUsageEvent$Type;ZJJ)V

    .line 41
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/b;->a(Ljava/lang/Object;)V

    .line 47
    return-void
.end method


# virtual methods
.method public a(Lbiw;)Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    return v0
.end method

.method public processDataStreamRelease(Lbjq;)V
    .locals 6
    .annotation runtime Lbiz;
        a = Lbjq;
    .end annotation

    .prologue
    const-wide/32 v4, 0x7fffffff

    .line 21
    iget-wide v0, p1, Lbjq;->a:J

    .line 22
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 33
    :goto_0
    return-void

    .line 27
    :cond_0
    :goto_1
    cmp-long v2, v0, v4

    if-lez v2, :cond_1

    .line 28
    const v2, 0x7fffffff

    invoke-direct {p0, v2}, Lob;->a(I)V

    .line 29
    sub-long/2addr v0, v4

    goto :goto_1

    .line 32
    :cond_1
    long-to-int v0, v0

    invoke-direct {p0, v0}, Lob;->a(I)V

    goto :goto_0
.end method
