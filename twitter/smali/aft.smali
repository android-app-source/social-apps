.class public Laft;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lauj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lauj",
        "<",
        "Ljava/lang/Long;",
        "Ljava/util/List",
        "<",
        "Lafu$a;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lauh;

.field private final b:J


# direct methods
.method public constructor <init>(Landroid/content/Context;J)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Lauh;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1}, Lauh;-><init>(Landroid/content/ContentResolver;)V

    iput-object v0, p0, Laft;->a:Lauh;

    .line 35
    iput-wide p2, p0, Laft;->b:J

    .line 36
    return-void
.end method

.method static a(JJ)Lapb;
    .locals 6
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 60
    new-instance v1, Lapb$a;

    invoke-direct {v1}, Lapb$a;-><init>()V

    .line 61
    sget-object v0, Lcom/twitter/database/schema/a$e;->a:Landroid/net/Uri;

    invoke-static {v0, p0, p1}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapb$a;->a(Landroid/net/Uri;)Lapb$a;

    move-result-object v0

    sget-object v2, Lbth;->a:[Ljava/lang/String;

    .line 62
    invoke-virtual {v0, v2}, Lapb$a;->b([Ljava/lang/String;)Lapb$a;

    move-result-object v0

    const-string/jumbo v2, "carousel_collection_id=?"

    .line 63
    invoke-virtual {v0, v2}, Lapb$a;->a(Ljava/lang/String;)Laop$a;

    move-result-object v0

    check-cast v0, Lapb$a;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 64
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Lapb$a;->a([Ljava/lang/String;)Laop$a;

    move-result-object v0

    check-cast v0, Lapb$a;

    const-string/jumbo v2, "carousel_sort_index ASC"

    .line 65
    invoke-virtual {v0, v2}, Lapb$a;->b(Ljava/lang/String;)Laop$a;

    .line 66
    invoke-virtual {v1}, Lapb$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapb;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Long;)Lrx/c;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            ")",
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<",
            "Lafu$a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 41
    iget-wide v2, p0, Laft;->b:J

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Laft;->a(JJ)Lapb;

    move-result-object v0

    .line 43
    iget-object v1, p0, Laft;->a:Lauh;

    invoke-virtual {v1, v0}, Lauh;->a(Lapb;)Lrx/c;

    move-result-object v0

    new-instance v1, Laft$1;

    invoke-direct {v1, p0}, Laft$1;-><init>(Laft;)V

    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a_(Ljava/lang/Object;)Lrx/c;
    .locals 1

    .prologue
    .line 27
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Laft;->a(Ljava/lang/Long;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Laft;->a:Lauh;

    invoke-virtual {v0}, Lauh;->close()V

    .line 55
    return-void
.end method
