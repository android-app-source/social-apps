.class public Lccn;
.super Lcck;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lccn$b;,
        Lccn$a;
    }
.end annotation


# static fields
.field public static final b:Lccn$b;


# instance fields
.field public final c:Lccm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lccn$b;

    invoke-direct {v0}, Lccn$b;-><init>()V

    sput-object v0, Lccn;->b:Lccn$b;

    return-void
.end method

.method private constructor <init>(Lccn$a;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcck;-><init>(Lcck$a;)V

    .line 23
    invoke-static {p1}, Lccn$a;->a(Lccn$a;)Lccm;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccm;

    iput-object v0, p0, Lccn;->c:Lccm;

    .line 24
    return-void
.end method

.method synthetic constructor <init>(Lccn$a;Lccn$1;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lccn;-><init>(Lccn$a;)V

    return-void
.end method

.method private a(Lccn;)Z
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lccn;->c:Lccm;

    iget-object v1, p1, Lccn;->c:Lccm;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    const-string/jumbo v0, "encrypted_text_input"

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcck;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lccn;

    if-eqz v0, :cond_1

    check-cast p1, Lccn;

    .line 36
    invoke-direct {p0, p1}, Lccn;->a(Lccn;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 35
    :goto_0
    return v0

    .line 36
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 41
    invoke-super {p0}, Lcck;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lccn;->c:Lccm;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
