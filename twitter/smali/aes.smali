.class public Laes;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lauj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lauj",
        "<",
        "Ljava/lang/Iterable",
        "<",
        "Ljava/lang/Long;",
        ">;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/Long;",
        "Lcom/twitter/model/core/Tweet;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<",
            "Lapb;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/library/client/Session;


# direct methods
.method public constructor <init>(Lauj;Lcom/twitter/library/client/Session;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lauj",
            "<",
            "Lapb;",
            "Landroid/database/Cursor;",
            ">;",
            "Lcom/twitter/library/client/Session;",
            ")V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Laes;->a:Lauj;

    .line 45
    iput-object p2, p0, Laes;->b:Lcom/twitter/library/client/Session;

    .line 46
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Iterable;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lrx/c",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/Tweet;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Laes;->a:Lauj;

    invoke-virtual {p0, p1}, Laes;->b(Ljava/lang/Iterable;)Lapb;

    move-result-object v1

    invoke-interface {v0, v1}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    new-instance v1, Laes$2;

    invoke-direct {v1, p0}, Laes$2;-><init>(Laes;)V

    .line 52
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Laes$1;

    invoke-direct {v1, p0}, Laes$1;-><init>(Laes;)V

    .line 65
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 51
    return-object v0
.end method

.method public synthetic a_(Ljava/lang/Object;)Lrx/c;
    .locals 1

    .prologue
    .line 35
    check-cast p1, Ljava/lang/Iterable;

    invoke-virtual {p0, p1}, Laes;->a(Ljava/lang/Iterable;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method b(Ljava/lang/Iterable;)Lapb;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lapb;"
        }
    .end annotation

    .prologue
    .line 81
    new-instance v0, Lapb$a;

    invoke-direct {v0}, Lapb$a;-><init>()V

    sget-object v1, Lcom/twitter/database/schema/a$v;->a:Landroid/net/Uri;

    .line 83
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string/jumbo v2, "ownerId"

    iget-object v3, p0, Laes;->b:Lcom/twitter/library/client/Session;

    .line 86
    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 84
    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 87
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 82
    invoke-virtual {v0, v1}, Lapb$a;->a(Landroid/net/Uri;)Lapb$a;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "status_groups_g_status_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 88
    invoke-static {p1}, Lcom/twitter/library/provider/t;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lapb$a;->a(Ljava/lang/String;)Laop$a;

    move-result-object v0

    check-cast v0, Lapb$a;

    sget-object v1, Lbuj;->a:[Ljava/lang/String;

    .line 89
    invoke-virtual {v0, v1}, Lapb$a;->b([Ljava/lang/String;)Lapb$a;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Lapb$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapb;

    .line 81
    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Laes;->a:Lauj;

    invoke-interface {v0}, Lauj;->close()V

    .line 96
    return-void
.end method
