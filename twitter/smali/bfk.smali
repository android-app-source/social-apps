.class public Lbfk;
.super Lbao;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbao",
        "<",
        "Lcom/twitter/library/api/y;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lbfk;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lbao;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 26
    iput-object p3, p0, Lbfk;->a:Ljava/lang/String;

    .line 27
    iput p4, p0, Lbfk;->b:I

    .line 28
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V
    .locals 16

    .prologue
    .line 77
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 78
    invoke-virtual/range {p3 .. p3}, Lcom/twitter/library/api/y;->b()Ljava/lang/Object;

    move-result-object v2

    move-object v12, v2

    check-cast v12, Lcom/twitter/library/api/w;

    .line 79
    if-eqz v12, :cond_0

    .line 82
    move-object/from16 v0, p0

    iget v2, v0, Lbfk;->b:I

    packed-switch v2, :pswitch_data_0

    .line 94
    iget-object v10, v12, Lcom/twitter/library/api/w;->d:Ljava/lang/String;

    .line 95
    iget-object v11, v12, Lcom/twitter/library/api/w;->e:Ljava/lang/String;

    .line 99
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lbfk;->S()Laut;

    move-result-object v13

    .line 100
    invoke-virtual/range {p0 .. p0}, Lbfk;->R()Lcom/twitter/library/provider/t;

    move-result-object v2

    iget-object v3, v12, Lcom/twitter/library/api/w;->a:Lcom/twitter/model/topic/TwitterTopic;

    iget-object v4, v12, Lcom/twitter/library/api/w;->b:Lcom/twitter/model/core/TwitterUser;

    iget-object v5, v12, Lcom/twitter/library/api/w;->c:Ljava/util/List;

    .line 101
    invoke-virtual/range {p0 .. p0}, Lbfk;->M()Lcom/twitter/library/service/v;

    move-result-object v6

    iget-wide v6, v6, Lcom/twitter/library/service/v;->c:J

    const/4 v8, 0x5

    move-object/from16 v0, p0

    iget-object v9, v0, Lbfk;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v14, v0, Lbfk;->b:I

    const/4 v15, 0x2

    if-ne v14, v15, :cond_1

    iget-object v12, v12, Lcom/twitter/library/api/w;->c:Ljava/util/List;

    .line 103
    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v12

    if-eqz v12, :cond_1

    const/4 v12, 0x1

    .line 100
    :goto_1
    invoke-virtual/range {v2 .. v13}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/model/topic/TwitterTopic;Lcom/twitter/model/core/TwitterUser;Ljava/util/List;JILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLaut;)I

    .line 104
    invoke-virtual {v13}, Laut;->a()V

    .line 108
    :cond_0
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/u;->a(Lcom/twitter/network/HttpOperation;)V

    .line 109
    return-void

    .line 84
    :pswitch_0
    const/4 v10, 0x0

    .line 85
    iget-object v11, v12, Lcom/twitter/library/api/w;->e:Ljava/lang/String;

    goto :goto_0

    .line 89
    :pswitch_1
    iget-object v10, v12, Lcom/twitter/library/api/w;->d:Ljava/lang/String;

    .line 90
    const/4 v11, 0x0

    .line 91
    goto :goto_0

    .line 103
    :cond_1
    const/4 v12, 0x0

    goto :goto_1

    .line 82
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 20
    check-cast p3, Lcom/twitter/library/api/y;

    invoke-virtual {p0, p1, p2, p3}, Lbfk;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/service/d$a;
    .locals 9

    .prologue
    const/16 v2, 0x8

    const/4 v8, 0x3

    const/4 v3, 0x2

    .line 33
    invoke-virtual {p0}, Lbfk;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-wide v4, v0, Lcom/twitter/library/service/v;->c:J

    .line 35
    invoke-virtual {p0}, Lbfk;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    new-array v1, v8, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "beta"

    aput-object v7, v1, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "timelines"

    aput-object v7, v1, v6

    const-string/jumbo v6, "timeline"

    aput-object v6, v1, v3

    .line 36
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "pc"

    const-string/jumbo v6, "true"

    .line 37
    invoke-virtual {v0, v1, v6}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "id"

    iget-object v6, p0, Lbfk;->a:Ljava/lang/String;

    .line 38
    invoke-virtual {v0, v1, v6}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 40
    iget v1, p0, Lbfk;->b:I

    packed-switch v1, :pswitch_data_0

    .line 62
    :cond_0
    :goto_0
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->b()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->f()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->e()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->d()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->c()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 62
    return-object v0

    .line 42
    :pswitch_0
    invoke-virtual {p0}, Lbfk;->R()Lcom/twitter/library/provider/t;

    move-result-object v1

    iget-object v6, p0, Lbfk;->a:Ljava/lang/String;

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/t;->b(IIJLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 44
    if-eqz v1, :cond_0

    .line 45
    const-string/jumbo v2, "min_position"

    invoke-virtual {v0, v2, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto :goto_0

    .line 50
    :pswitch_1
    invoke-virtual {p0}, Lbfk;->R()Lcom/twitter/library/provider/t;

    move-result-object v1

    iget-object v6, p0, Lbfk;->a:Ljava/lang/String;

    move v3, v8

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/t;->b(IIJLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 52
    if-eqz v1, :cond_0

    .line 53
    const-string/jumbo v2, "max_position"

    invoke-virtual {v0, v2, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto :goto_0

    .line 40
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected e()Lcom/twitter/library/api/y;
    .locals 1

    .prologue
    .line 71
    const/16 v0, 0x39

    invoke-static {v0}, Lcom/twitter/library/api/y;->a(I)Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lbfk;->e()Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method
