.class public Laeg;
.super Laef;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laef",
        "<",
        "Lcai;",
        "Laef$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/android/notificationtimeline/a;Lcom/twitter/android/notificationtimeline/h;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Laef;-><init>(Lcom/twitter/android/notificationtimeline/a;Lcom/twitter/android/notificationtimeline/h;)V

    .line 25
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Laeg;->a:Ljava/util/List;

    .line 30
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)Laef$a;
    .locals 4

    .prologue
    .line 35
    new-instance v0, Laef$a;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040021

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Laef$a;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method protected bridge synthetic a(Landroid/content/Context;Lbzz;I)Lcom/twitter/analytics/model/ScribeItem;
    .locals 1

    .prologue
    .line 22
    check-cast p2, Lcai;

    invoke-virtual {p0, p1, p2, p3}, Laeg;->a(Landroid/content/Context;Lcai;I)Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/content/Context;Lcai;I)Lcom/twitter/analytics/model/ScribeItem;
    .locals 2

    .prologue
    .line 49
    invoke-static {p3}, Lcom/twitter/android/notificationtimeline/a;->a(I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 50
    const-string/jumbo v1, "break"

    iput-object v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->x:Ljava/lang/String;

    .line 51
    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Laeg;->a:Ljava/util/List;

    return-object v0
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 55
    iget-object v0, p0, Laeg;->a:Ljava/util/List;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    return-void
.end method

.method public bridge synthetic a(Laef$a;Lbzz;)V
    .locals 0

    .prologue
    .line 22
    check-cast p2, Lcai;

    invoke-virtual {p0, p1, p2}, Laeg;->a(Laef$a;Lcai;)V

    return-void
.end method

.method public a(Laef$a;Lcai;)V
    .locals 4

    .prologue
    .line 40
    invoke-super {p0, p1, p2}, Laef;->a(Laef$a;Lbzz;)V

    .line 41
    invoke-virtual {p1}, Laef$a;->b()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/GapView;

    .line 42
    iget-object v1, p0, Laeg;->a:Ljava/util/List;

    iget-object v2, p2, Lcai;->b:Lcac;

    invoke-virtual {v2}, Lcac;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GapView;->setSpinnerActive(Z)V

    .line 43
    return-void
.end method

.method public bridge synthetic a(Lckb$a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Laef$a;

    check-cast p2, Lcai;

    invoke-virtual {p0, p1, p2}, Laeg;->a(Laef$a;Lcai;)V

    return-void
.end method

.method public synthetic b(Landroid/view/ViewGroup;)Lckb$a;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Laeg;->a(Landroid/view/ViewGroup;)Laef$a;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Laeg;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 65
    return-void
.end method
