.class public Lchv;
.super Lcho;
.source "Twttr"


# instance fields
.field public final a:Lcht;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLcom/twitter/model/timeline/r;Lchj;Lcht;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct/range {p0 .. p5}, Lcho;-><init>(Ljava/lang/String;JLcom/twitter/model/timeline/r;Lchj;)V

    .line 16
    iput-object p6, p0, Lchv;->a:Lcht;

    .line 17
    return-void
.end method

.method private a(Lcgx;)Lchu$a;
    .locals 4

    .prologue
    .line 28
    iget-object v0, p0, Lchv;->a:Lcht;

    iget-object v0, v0, Lcht;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcgx;->c(Ljava/lang/String;)Lcom/twitter/model/moments/Moment;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/Moment;

    .line 30
    new-instance v1, Lchu$a;

    invoke-direct {v1, v0}, Lchu$a;-><init>(Lcom/twitter/model/moments/Moment;)V

    iget-wide v2, p0, Lchv;->c:J

    .line 31
    invoke-virtual {v1, v2, v3}, Lchu$a;->a(J)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lchu$a;

    iget-object v1, p0, Lchv;->d:Lcom/twitter/model/timeline/r;

    .line 32
    invoke-virtual {v0, v1}, Lchu$a;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lchu$a;

    iget-object v1, p0, Lchv;->b:Ljava/lang/String;

    .line 33
    invoke-virtual {v0, v1}, Lchu$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lchu$a;

    .line 30
    return-object v0
.end method


# virtual methods
.method public a(Lcgx;Lchc;)Lchu;
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lchv;->a(Lcgx;)Lchu$a;

    move-result-object v0

    invoke-virtual {v0}, Lchu$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchu;

    return-object v0
.end method

.method public synthetic b(Lcgx;Lchc;)Lcom/twitter/model/timeline/y;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0, p1, p2}, Lchv;->a(Lcgx;Lchc;)Lchu;

    move-result-object v0

    return-object v0
.end method
