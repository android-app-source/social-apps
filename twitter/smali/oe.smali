.class public Loe;
.super Lbjb;
.source "Twttr"


# instance fields
.field a:Z
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field b:J
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private final c:Lcom/twitter/library/av/playback/AVPlayer;

.field private final d:Lph;

.field private e:J

.field private f:J

.field private g:I


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;J)V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lph;

    invoke-direct {v0, p3, p4}, Lph;-><init>(J)V

    invoke-direct {p0, p1, p2, v0}, Loe;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;Lph;)V

    .line 47
    return-void
.end method

.method constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;Lph;)V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p2}, Lbjb;-><init>(Lcom/twitter/model/av/AVMedia;)V

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Loe;->a:Z

    .line 38
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Loe;->b:J

    .line 52
    iput-object p1, p0, Loe;->c:Lcom/twitter/library/av/playback/AVPlayer;

    .line 53
    iput-object p3, p0, Loe;->d:Lph;

    .line 54
    return-void
.end method

.method private a()Z
    .locals 4

    .prologue
    .line 153
    iget-wide v0, p0, Loe;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 160
    iget-wide v0, p0, Loe;->f:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_0

    .line 161
    iget-wide v0, p0, Loe;->e:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Loe;->f:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Loe;->e:J

    .line 162
    iput-wide v6, p0, Loe;->f:J

    .line 164
    :cond_0
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 170
    invoke-direct {p0}, Loe;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Loe;->d:Lph;

    invoke-virtual {v0}, Lph;->d()J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Loe;->a(JZ)V

    .line 173
    :cond_0
    return-void
.end method


# virtual methods
.method a(JZ)V
    .locals 13
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const-wide/16 v10, -0x1

    .line 133
    iget-wide v2, p0, Loe;->b:J

    .line 136
    invoke-direct {p0}, Loe;->e()V

    .line 138
    cmp-long v0, p1, v10

    if-eqz v0, :cond_0

    cmp-long v0, v2, v10

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Loe;->d:Lph;

    invoke-virtual {v0}, Lph;->c()V

    .line 140
    iget-object v0, p0, Loe;->c:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->h()Lbix;

    move-result-object v9

    new-instance v0, Lbkl;

    iget-object v1, p0, Loe;->k:Lcom/twitter/model/av/AVMedia;

    iget v6, p0, Loe;->g:I

    iget-wide v7, p0, Loe;->e:J

    move-wide v4, p1

    invoke-direct/range {v0 .. v8}, Lbkl;-><init>(Lcom/twitter/model/av/AVMedia;JJIJ)V

    .line 142
    invoke-virtual {p0}, Loe;->b()Lcom/twitter/library/av/m;

    move-result-object v1

    .line 140
    invoke-virtual {v9, v0, v1}, Lbix;->a(Lbiw;Lcom/twitter/library/av/m;)V

    .line 144
    if-eqz p3, :cond_1

    :goto_0
    iput-wide p1, p0, Loe;->b:J

    .line 145
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Loe;->e:J

    .line 147
    :cond_0
    return-void

    :cond_1
    move-wide p1, v10

    .line 144
    goto :goto_0
.end method

.method public processBitrateChange(Lbjm;)V
    .locals 1
    .annotation runtime Lbiz;
        a = Lbjm;
    .end annotation

    .prologue
    .line 84
    iget v0, p1, Lbjm;->b:I

    iput v0, p0, Loe;->g:I

    .line 85
    return-void
.end method

.method public processBufferingEnded(Lbjn;)V
    .locals 1
    .annotation runtime Lbiz;
        a = Lbjn;
    .end annotation

    .prologue
    .line 96
    iget-boolean v0, p0, Loe;->a:Z

    if-eqz v0, :cond_0

    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, Loe;->a:Z

    .line 101
    :goto_0
    return-void

    .line 99
    :cond_0
    invoke-direct {p0}, Loe;->e()V

    goto :goto_0
.end method

.method public processBufferingStarted(Lbjo;)V
    .locals 2
    .annotation runtime Lbiz;
        a = Lbjo;
    .end annotation

    .prologue
    .line 89
    iget-boolean v0, p0, Loe;->a:Z

    if-nez v0, :cond_0

    .line 90
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Loe;->f:J

    .line 92
    :cond_0
    return-void
.end method

.method public processForcedSkipForward(Lcom/twitter/library/av/playback/j$b;)V
    .locals 1
    .annotation runtime Lbiz;
        a = Lcom/twitter/library/av/playback/j$b;
    .end annotation

    .prologue
    .line 78
    invoke-direct {p0}, Loe;->f()V

    .line 79
    iget-object v0, p0, Loe;->d:Lph;

    invoke-virtual {v0}, Lph;->e()V

    .line 80
    return-void
.end method

.method public processMediaComplete(Lbkm;)V
    .locals 0
    .annotation runtime Lbiz;
        a = Lbkm;
    .end annotation

    .prologue
    .line 105
    invoke-direct {p0}, Loe;->f()V

    .line 106
    return-void
.end method

.method public processPause(Lbjw;)V
    .locals 1
    .annotation runtime Lbiz;
        a = Lbjw;
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0}, Loe;->f()V

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Loe;->a:Z

    .line 74
    return-void
.end method

.method public processTick(Lbki;)V
    .locals 4
    .annotation runtime Lbiz;
        a = Lbki;
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Loe;->d:Lph;

    iget-object v1, p1, Lbki;->b:Lcom/twitter/library/av/playback/aa;

    iget-wide v2, v1, Lcom/twitter/library/av/playback/aa;->b:J

    invoke-virtual {v0, v2, v3}, Lph;->a(J)V

    .line 60
    invoke-direct {p0}, Loe;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61
    iget-object v0, p0, Loe;->d:Lph;

    invoke-virtual {v0}, Lph;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Loe;->d:Lph;

    invoke-virtual {v0}, Lph;->d()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Loe;->a(JZ)V

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    iget-object v0, p1, Lbki;->b:Lcom/twitter/library/av/playback/aa;

    iget-wide v0, v0, Lcom/twitter/library/av/playback/aa;->b:J

    iput-wide v0, p0, Loe;->b:J

    .line 66
    iget-object v0, p0, Loe;->d:Lph;

    invoke-virtual {v0}, Lph;->a()V

    goto :goto_0
.end method
