.class public Lage$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/twitter/android/search/d;->e()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lage$a;->a:Landroid/content/Intent;

    .line 186
    iget-object v0, p0, Lage$a;->a:Landroid/content/Intent;

    const-string/jumbo v1, "query"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 187
    return-void
.end method


# virtual methods
.method public a(I)Lage$a;
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lage$a;->a:Landroid/content/Intent;

    const-string/jumbo v1, "search_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 222
    return-object p0
.end method

.method public a(J)Lage$a;
    .locals 3

    .prologue
    .line 233
    iget-object v0, p0, Lage$a;->a:Landroid/content/Intent;

    const-string/jumbo v1, "search_suggestion_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 234
    return-object p0
.end method

.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lage$a;
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lage$a;->a:Landroid/content/Intent;

    const-string/jumbo v1, "source_association"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 210
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lage$a;
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lage$a;->a:Landroid/content/Intent;

    const-string/jumbo v1, "query_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 192
    return-object p0
.end method

.method public a(Ljava/util/ArrayList;)Lage$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lage$a;"
        }
    .end annotation

    .prologue
    .line 245
    iget-object v0, p0, Lage$a;->a:Landroid/content/Intent;

    const-string/jumbo v1, "pinnedTweetIds"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 246
    return-object p0
.end method

.method public a(Z)Lage$a;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 255
    iget-object v0, p0, Lage$a;->a:Landroid/content/Intent;

    const-string/jumbo v1, "terminal"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 256
    return-object p0
.end method

.method public a()Lage;
    .locals 2

    .prologue
    .line 261
    new-instance v0, Lage;

    iget-object v1, p0, Lage$a;->a:Landroid/content/Intent;

    invoke-direct {v0, v1}, Lage;-><init>(Landroid/content/Intent;)V

    return-object v0
.end method

.method public b(I)Lage$a;
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lage$a;->a:Landroid/content/Intent;

    const-string/jumbo v1, "search_suggestion_position"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 228
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lage$a;
    .locals 2

    .prologue
    .line 197
    iget-object v0, p0, Lage$a;->a:Landroid/content/Intent;

    const-string/jumbo v1, "q_source"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 198
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lage$a;
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lage$a;->a:Landroid/content/Intent;

    const-string/jumbo v1, "user_query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 204
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lage$a;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lage$a;->a:Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 216
    return-object p0
.end method
