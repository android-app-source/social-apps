.class public Lcch$b;
.super Lcbx$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbx$b",
        "<",
        "Lcch;",
        "Lcch$a;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcbx$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcch$a;
    .locals 1

    .prologue
    .line 79
    new-instance v0, Lcch$a;

    invoke-direct {v0}, Lcch$a;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcbx$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 74
    check-cast p2, Lcch$a;

    invoke-virtual {p0, p1, p2, p3}, Lcch$b;->a(Lcom/twitter/util/serialization/n;Lcch$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcch$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 86
    invoke-super {p0, p1, p2, p3}, Lcbx$b;->a(Lcom/twitter/util/serialization/n;Lcbx$a;I)V

    .line 87
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcch$a;->a(J)Lcch$a;

    .line 88
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 74
    check-cast p2, Lcch$a;

    invoke-virtual {p0, p1, p2, p3}, Lcch$b;->a(Lcom/twitter/util/serialization/n;Lcch$a;I)V

    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/util/serialization/o;Lcbx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    check-cast p2, Lcch;

    invoke-virtual {p0, p1, p2}, Lcch$b;->a(Lcom/twitter/util/serialization/o;Lcch;)V

    return-void
.end method

.method public a(Lcom/twitter/util/serialization/o;Lcch;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    invoke-super {p0, p1, p2}, Lcbx$b;->a(Lcom/twitter/util/serialization/o;Lcbx;)V

    .line 95
    iget-wide v0, p2, Lcch;->c:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    .line 96
    return-void
.end method

.method public synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    check-cast p2, Lcch;

    invoke-virtual {p0, p1, p2}, Lcch$b;->a(Lcom/twitter/util/serialization/o;Lcch;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lcch$b;->a()Lcch$a;

    move-result-object v0

    return-object v0
.end method
