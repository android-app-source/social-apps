.class public Lok;
.super Lbjb;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/av/playback/AVPlayer;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p2}, Lbjb;-><init>(Lcom/twitter/model/av/AVMedia;)V

    .line 25
    iput-object p1, p0, Lok;->a:Lcom/twitter/library/av/playback/AVPlayer;

    .line 26
    return-void
.end method


# virtual methods
.method public processHeartbeatEvent(Lbkl;)V
    .locals 6
    .annotation runtime Lbiz;
        a = Lbkl;
    .end annotation

    .prologue
    .line 30
    new-instance v0, Lcom/twitter/library/av/m$a;

    invoke-direct {v0}, Lcom/twitter/library/av/m$a;-><init>()V

    new-instance v1, Lbyd;

    iget v2, p1, Lbkl;->d:I

    iget-wide v4, p1, Lbkl;->e:J

    invoke-direct {v1, v2, v4, v5}, Lbyd;-><init>(IJ)V

    .line 31
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Lbyd;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-object v1, p0, Lok;->k:Lcom/twitter/model/av/AVMedia;

    .line 32
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/model/av/AVMedia;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    .line 33
    iget-object v1, p0, Lok;->a:Lcom/twitter/library/av/playback/AVPlayer;

    const-string/jumbo v2, "heartbeat"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Ljava/lang/String;Lcom/twitter/library/av/m$a;)V

    .line 34
    return-void
.end method
