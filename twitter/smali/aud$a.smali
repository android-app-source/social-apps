.class public Laud$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Laud;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:J

.field private b:Z

.field private final c:J


# direct methods
.method constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 201
    iput-wide p1, p0, Laud$a;->c:J

    .line 202
    iput-wide p3, p0, Laud$a;->a:J

    .line 203
    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 2

    .prologue
    .line 216
    if-eqz p1, :cond_0

    .line 217
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iput-wide v0, p0, Laud$a;->a:J

    .line 219
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Laud$a;->b:Z

    .line 220
    return-void
.end method

.method public a()Z
    .locals 4

    .prologue
    .line 206
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    .line 207
    iget-wide v2, p0, Laud$a;->a:J

    sub-long/2addr v0, v2

    .line 208
    iget-wide v2, p0, Laud$a;->c:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-boolean v0, p0, Laud$a;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 212
    const/4 v0, 0x1

    iput-boolean v0, p0, Laud$a;->b:Z

    .line 213
    return-void
.end method
