.class Lbqb;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcol;


# static fields
.field private static b:Ljava/lang/String;


# instance fields
.field public a:Z

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcdg;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z

.field private e:Lcdf;

.field private final f:Landroid/content/Context;

.field private final g:Landroid/os/Handler;

.field private final h:Ljava/lang/Runnable;

.field private i:J

.field private final j:Ljava/lang/String;

.field private k:Lcol$a;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lbqb;->c:Ljava/util/Map;

    .line 62
    new-instance v0, Lbqb$1;

    invoke-direct {v0, p0}, Lbqb$1;-><init>(Lbqb;)V

    iput-object v0, p0, Lbqb;->h:Ljava/lang/Runnable;

    .line 93
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lbqb;->g:Landroid/os/Handler;

    .line 94
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbqb;->f:Landroid/content/Context;

    .line 96
    iget-object v0, p0, Lbqb;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/network/ab;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "release_feature_switch_manifest"

    :goto_0
    iput-object v0, p0, Lbqb;->j:Ljava/lang/String;

    .line 100
    invoke-static {}, Lcom/twitter/util/android/b;->a()Lcom/twitter/util/android/b;

    move-result-object v0

    new-instance v1, Lbqb$2;

    invoke-direct {v1, p0}, Lbqb$2;-><init>(Lbqb;)V

    invoke-virtual {v0, v1}, Lcom/twitter/util/android/b;->a(Lcom/twitter/util/android/b$a;)V

    .line 111
    return-void

    .line 96
    :cond_0
    const-string/jumbo v0, "feature_switch_manifest"

    goto :goto_0
.end method

.method static synthetic a(Lbqb;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lbqb;->f:Landroid/content/Context;

    return-object v0
.end method

.method private a(Lcdg;Lcdg;)Lcdg;
    .locals 5

    .prologue
    .line 344
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    iget-object v1, p2, Lcdg;->d:Lcdd;

    iget-object v1, v1, Lcdd;->c:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/i;->b(Ljava/util/Map;)Lcom/twitter/util/collection/i;

    move-result-object v1

    .line 345
    invoke-virtual {p0}, Lbqb;->b()Lcdf;

    move-result-object v2

    .line 346
    iget-object v0, v2, Lcdf;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 347
    invoke-static {v2, p1, v0}, Lbqb;->b(Lcdf;Lcdg;Ljava/lang/String;)Lcdc;

    move-result-object v4

    .line 348
    invoke-virtual {v1, v0, v4}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    goto :goto_0

    .line 350
    :cond_0
    new-instance v0, Lcdg$a;

    invoke-direct {v0}, Lcdg$a;-><init>()V

    iget-object v2, p2, Lcdg;->e:Ljava/util/Set;

    .line 351
    invoke-virtual {v0, v2}, Lcdg$a;->a(Ljava/util/Set;)Lcdg$a;

    move-result-object v2

    new-instance v3, Lcdd$a;

    iget-object v0, p2, Lcdg;->d:Lcdd;

    invoke-direct {v3, v0}, Lcdd$a;-><init>(Lcdd;)V

    .line 354
    invoke-virtual {v1}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v3, v0}, Lcdd$a;->a(Ljava/util/Map;)Lcdd$a;

    move-result-object v0

    .line 355
    invoke-virtual {v0}, Lcdd$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdd;

    .line 352
    invoke-virtual {v2, v0}, Lcdg$a;->a(Lcdd;)Lcdg$a;

    move-result-object v0

    .line 356
    invoke-virtual {v0}, Lcdg$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdg;

    .line 350
    return-object v0
.end method

.method private a(JLjava/lang/String;Lcdc;Z)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 170
    if-nez p4, :cond_0

    .line 171
    const/4 v0, 0x0

    .line 177
    :goto_0
    return-object v0

    .line 173
    :cond_0
    if-eqz p5, :cond_1

    invoke-virtual {p4}, Lcdc;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    invoke-virtual {p4}, Lcdc;->c()Lcdc$a;

    move-result-object v0

    .line 175
    invoke-virtual {v0}, Lcdc$a;->b()I

    move-result v2

    invoke-virtual {v0}, Lcdc$a;->c()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v1, p3

    move-wide v4, p1

    invoke-virtual/range {v0 .. v5}, Lbqb;->a(Ljava/lang/String;ILjava/lang/String;J)V

    .line 177
    :cond_1
    invoke-virtual {p4}, Lcdc;->a()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcdf;Lcdg;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p1, p2}, Lcdg;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 230
    if-eqz v0, :cond_1

    .line 236
    :cond_0
    :goto_0
    return-object v0

    .line 232
    :cond_1
    iget-object v0, p1, Lcdg;->e:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 233
    const-string/jumbo v0, "unassigned"

    goto :goto_0

    .line 235
    :cond_2
    invoke-virtual {p0, p2}, Lcdf;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 236
    if-nez v0, :cond_0

    const-string/jumbo v0, "unassigned"

    goto :goto_0
.end method

.method private declared-synchronized a(JLcdg;)V
    .locals 7

    .prologue
    .line 366
    monitor-enter p0

    :try_start_0
    new-instance v1, Lcom/twitter/metrics/g;

    const-string/jumbo v0, "fs:load:feature_switches"

    sget-object v2, Lcom/twitter/metrics/g;->m:Lcom/twitter/metrics/g$b;

    invoke-direct {v1, v0, v2}, Lcom/twitter/metrics/g;-><init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;)V

    .line 367
    const-string/jumbo v0, "Server_FCP"

    invoke-virtual {v1, v0}, Lcom/twitter/metrics/g;->e(Ljava/lang/String;)V

    .line 368
    invoke-virtual {v1}, Lcom/twitter/metrics/g;->i()V

    .line 370
    iget-object v0, p0, Lbqb;->c:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdg;

    .line 372
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Lcdg;->a()J

    move-result-wide v2

    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v4

    invoke-virtual {v4}, Lcof;->f()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    .line 373
    if-eqz v0, :cond_0

    .line 374
    invoke-direct {p0, v0, p3}, Lbqb;->a(Lcdg;Lcdg;)Lcdg;

    move-result-object p3

    .line 376
    iget-object v0, v0, Lcdg;->d:Lcdd;

    iget-object v2, p3, Lcdg;->d:Lcdd;

    invoke-virtual {v0, v2}, Lcdd;->b(Lcdd;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 377
    iget-object v0, p3, Lcdg;->d:Lcdd;

    iget-object v0, v0, Lcdd;->e:Ljava/lang/String;

    iget-object v2, p3, Lcdg;->d:Lcdd;

    iget-object v2, v2, Lcdd;->d:Ljava/lang/String;

    invoke-static {p1, p2, v0, v2}, Lbqb;->a(JLjava/lang/String;Ljava/lang/String;)V

    .line 382
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lbqb;->b(JLcdg;)V

    .line 393
    :cond_1
    :goto_0
    invoke-virtual {v1}, Lcom/twitter/metrics/g;->j()V

    .line 394
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/metrics/j;->a(Lcom/twitter/metrics/g;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 400
    :goto_1
    monitor-exit p0

    return-void

    .line 385
    :cond_2
    :try_start_1
    sget-object v2, Lcdg;->c:Lcdg;

    invoke-direct {p0, p1, p2, v2}, Lbqb;->b(JLcdg;)V

    .line 386
    invoke-virtual {p0}, Lbqb;->b()Lcdf;

    move-result-object v2

    .line 387
    if-eqz v0, :cond_1

    iget-object v0, v0, Lcdg;->d:Lcdd;

    iget-object v3, v2, Lcdf;->c:Lcdd;

    invoke-virtual {v0, v3}, Lcdd;->b(Lcdd;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 388
    iget-object v0, v2, Lcdf;->c:Lcdd;

    iget-object v0, v0, Lcdd;->e:Ljava/lang/String;

    iget-object v2, v2, Lcdf;->c:Lcdd;

    iget-object v2, v2, Lcdd;->d:Ljava/lang/String;

    invoke-static {p1, p2, v0, v2}, Lbqb;->a(JLjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 395
    :catch_0
    move-exception v0

    .line 397
    :try_start_2
    sget-object v1, Lcdg;->c:Lcdg;

    invoke-direct {p0, p1, p2, v1}, Lbqb;->b(JLcdg;)V

    .line 398
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 366
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static a(JLjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 306
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 307
    invoke-virtual {v0, p2, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "fs:settings:version:changed"

    aput-object v3, v1, v2

    .line 308
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 306
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 309
    return-void
.end method

.method public static a(Lcdf;Lcdg;Lcdg;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 255
    sget-object v0, Lcdg;->c:Lcdg;

    invoke-virtual {p1, v0}, Lcdg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lcdg;->d:Lcdd;

    iget-object v2, p2, Lcdg;->d:Lcdd;

    .line 256
    invoke-virtual {v0, v2}, Lcdd;->a(Lcdd;)Ljava/util/List;

    move-result-object v0

    .line 259
    :goto_0
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 260
    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-result-object v0

    iget-object v2, p0, Lcdf;->e:Ljava/util/Map;

    .line 261
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 262
    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 264
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 265
    iget-object v3, p0, Lcdf;->f:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 266
    iget-object v3, p0, Lcdf;->e:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 267
    invoke-static {p0, p1, v0}, Lbqb;->a(Lcdf;Lcdg;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 268
    invoke-static {p0, p2, v0}, Lbqb;->a(Lcdf;Lcdg;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 269
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 283
    :goto_1
    return v0

    .line 256
    :cond_1
    iget-object v0, p0, Lcdf;->c:Lcdd;

    iget-object v2, p2, Lcdg;->d:Lcdd;

    .line 257
    invoke-virtual {v0, v2}, Lcdd;->a(Lcdd;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 273
    :cond_2
    invoke-static {p0, p1, v0}, Lbqb;->b(Lcdf;Lcdg;Ljava/lang/String;)Lcdc;

    move-result-object v3

    .line 275
    invoke-static {p0, p2, v0}, Lbqb;->b(Lcdf;Lcdg;Ljava/lang/String;)Lcdc;

    move-result-object v0

    .line 276
    if-nez v3, :cond_3

    if-nez v0, :cond_4

    :cond_3
    if-eqz v3, :cond_0

    .line 277
    invoke-virtual {v3, v0}, Lcdc;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_4
    move v0, v1

    .line 278
    goto :goto_1

    .line 283
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static b(Lcdf;Lcdg;Ljava/lang/String;)Lcdc;
    .locals 1

    .prologue
    .line 249
    invoke-virtual {p1, p2}, Lcdg;->b(Ljava/lang/String;)Lcdc;

    move-result-object v0

    .line 250
    if-nez v0, :cond_0

    invoke-virtual {p0, p2}, Lcdf;->b(Ljava/lang/String;)Lcdc;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private b(JLcdg;)V
    .locals 3

    .prologue
    .line 447
    iget-object v0, p0, Lbqb;->c:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 448
    iget-object v0, p0, Lbqb;->k:Lcol$a;

    if-eqz v0, :cond_0

    .line 449
    iget-object v0, p0, Lbqb;->k:Lcol$a;

    invoke-interface {v0, p1, p2}, Lcol$a;->a(J)V

    .line 451
    :cond_0
    return-void
.end method

.method private g()J
    .locals 4

    .prologue
    .line 418
    iget-wide v0, p0, Lbqb;->i:J

    const-string/jumbo v2, "feature_switches_configs_wait_before_kill_minutes"

    .line 419
    invoke-virtual {p0, v0, v1, v2}, Lbqb;->a(JLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 418
    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    .line 419
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    .line 420
    int-to-long v0, v0

    const-wide/32 v2, 0xea60

    mul-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public declared-synchronized a(ZLjava/lang/String;)Lcdf;
    .locals 3

    .prologue
    .line 199
    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lbqb;->e:Lcdf;

    if-nez v0, :cond_1

    .line 200
    :cond_0
    new-instance v0, Lcom/twitter/metrics/g;

    const-string/jumbo v1, "fs:load:embedded_manifest"

    sget-object v2, Lcom/twitter/metrics/g;->m:Lcom/twitter/metrics/g$b;

    invoke-direct {v0, v1, v2}, Lcom/twitter/metrics/g;-><init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;)V

    .line 202
    const-string/jumbo v1, "Local_FCP"

    invoke-virtual {v0, v1}, Lcom/twitter/metrics/g;->e(Ljava/lang/String;)V

    .line 203
    invoke-virtual {v0}, Lcom/twitter/metrics/g;->i()V

    .line 205
    iget-object v1, p0, Lbqb;->f:Landroid/content/Context;

    invoke-static {v1, p2}, Lauc;->a(Landroid/content/Context;Ljava/lang/String;)Lcdf;

    move-result-object v1

    iput-object v1, p0, Lbqb;->e:Lcdf;

    .line 208
    invoke-virtual {v0}, Lcom/twitter/metrics/g;->j()V

    .line 209
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/metrics/j;->a(Lcom/twitter/metrics/g;)V

    .line 211
    :cond_1
    iget-object v0, p0, Lbqb;->e:Lcdf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(JLjava/lang/String;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 161
    const-wide/16 v4, -0x1

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lbqb;->a(JLjava/lang/String;J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(JLjava/lang/String;J)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 130
    iget-object v0, p0, Lbqb;->c:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdg;

    .line 131
    const-wide/16 v2, -0x1

    cmp-long v2, p4, v2

    if-eqz v2, :cond_1

    const/4 v6, 0x1

    .line 133
    :goto_0
    if-nez v0, :cond_0

    .line 135
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 136
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    .line 137
    invoke-virtual {p0, p1, p2, v1}, Lbqb;->a(JZ)V

    .line 138
    iget-object v0, p0, Lbqb;->c:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdg;

    .line 142
    :cond_0
    invoke-virtual {v0, p3}, Lcdg;->b(Ljava/lang/String;)Lcdc;

    move-result-object v5

    .line 143
    if-eqz v5, :cond_2

    move-object v1, p0

    move-wide v2, p4

    move-object v4, p3

    .line 144
    invoke-direct/range {v1 .. v6}, Lbqb;->a(JLjava/lang/String;Lcdc;Z)Ljava/lang/Object;

    move-result-object v0

    .line 155
    :goto_1
    return-object v0

    :cond_1
    move v6, v1

    .line 131
    goto :goto_0

    .line 147
    :cond_2
    invoke-virtual {p0}, Lbqb;->b()Lcdf;

    move-result-object v1

    .line 148
    iget-object v2, v1, Lcdf;->e:Ljava/util/Map;

    invoke-interface {v2, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 149
    iget-object v0, v0, Lcdg;->e:Ljava/util/Set;

    invoke-interface {v0, p3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 150
    const-string/jumbo v0, "unassigned"

    goto :goto_1

    .line 154
    :cond_3
    invoke-virtual {v1, p3}, Lcdf;->b(Ljava/lang/String;)Lcdc;

    move-result-object v5

    move-object v1, p0

    move-wide v2, p4

    move-object v4, p3

    .line 155
    invoke-direct/range {v1 .. v6}, Lbqb;->a(JLjava/lang/String;Lcdc;Z)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1
.end method

.method public declared-synchronized a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    monitor-enter p0

    :try_start_0
    sget-object v0, Lbqb;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 81
    invoke-virtual {p0}, Lbqb;->b()Lcdf;

    move-result-object v0

    iget-object v0, v0, Lcdf;->d:Ljava/lang/String;

    sput-object v0, Lbqb;->b:Ljava/lang/String;

    .line 82
    const-class v0, Lbqb;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 84
    :cond_0
    sget-object v0, Lbqb;->b:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(JLcdf;Lcdg;)V
    .locals 5

    .prologue
    .line 217
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbqb;->c:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdg;

    .line 218
    iget-wide v2, p0, Lbqb;->i:J

    cmp-long v1, p1, v2

    if-eqz v1, :cond_1

    .line 219
    invoke-virtual {p0, p1, p2}, Lbqb;->c(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 220
    :cond_1
    if-eqz v0, :cond_0

    .line 221
    :try_start_1
    iget-boolean v1, p0, Lbqb;->a:Z

    invoke-static {p3, v0, p4}, Lbqb;->a(Lcdf;Lcdg;Lcdg;)Z

    move-result v0

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lbqb;->a:Z

    .line 222
    invoke-direct {p0, p1, p2, p4}, Lbqb;->a(JLcdg;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 217
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(JZ)V
    .locals 3

    .prologue
    .line 318
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbqb;->c:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_1

    .line 320
    :cond_0
    :try_start_1
    iget-object v0, p0, Lbqb;->f:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Laud;->a(Landroid/content/Context;J)Ljava/io/File;

    move-result-object v0

    .line 323
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v1

    .line 322
    invoke-static {v0, v1}, Laud;->a(Ljava/io/File;Lcom/twitter/metrics/k;)Lcdg;

    move-result-object v0

    .line 324
    invoke-direct {p0, p1, p2, v0}, Lbqb;->a(JLcdg;)V

    .line 326
    if-nez v0, :cond_1

    .line 329
    invoke-static {p1, p2}, Laud;->b(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 337
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 331
    :catch_0
    move-exception v0

    .line 333
    :try_start_2
    sget-object v1, Lcdg;->c:Lcdg;

    invoke-direct {p0, p1, p2, v1}, Lbqb;->b(JLcdg;)V

    .line 334
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 318
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcol$a;)V
    .locals 0

    .prologue
    .line 414
    iput-object p1, p0, Lbqb;->k:Lcol$a;

    .line 415
    return-void
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;J)V
    .locals 6

    .prologue
    .line 114
    const-string/jumbo v0, "unassigned"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 115
    iget-object v0, p0, Lbqb;->f:Landroid/content/Context;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;J)V

    .line 117
    :cond_0
    return-void
.end method

.method public a(J)Z
    .locals 3

    .prologue
    .line 181
    iget-object v0, p0, Lbqb;->c:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbqb;->c:Ljava/util/Map;

    .line 182
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdg;

    sget-object v1, Lcdg;->c:Lcdg;

    invoke-virtual {v0, v1}, Lcdg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 181
    :goto_0
    return v0

    .line 182
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized b()Lcdf;
    .locals 2

    .prologue
    .line 193
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lbqb;->j:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lbqb;->a(ZLjava/lang/String;)Lcdf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(J)V
    .locals 1

    .prologue
    .line 186
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lbqb;->i:J

    .line 187
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbqb;->a:Z

    .line 188
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lbqb;->a(JZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    monitor-exit p0

    return-void

    .line 186
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()V
    .locals 2

    .prologue
    .line 409
    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lbqb;->d(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 410
    monitor-exit p0

    return-void

    .line 409
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c(J)V
    .locals 3

    .prologue
    .line 289
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lbqb;->i:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    .line 290
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lbqb;->a(JZ)V

    .line 294
    iget-object v0, p0, Lbqb;->f:Landroid/content/Context;

    .line 296
    invoke-virtual {p0}, Lbqb;->a()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lbqb;->d:Z

    .line 295
    invoke-static {v0, p1, p2, v1, v2}, Lbqe;->a(Landroid/content/Context;JLjava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 301
    :goto_0
    monitor-exit p0

    return-void

    .line 298
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "loadFeatureSwitchesForNonActiveUser should not be called for a current user"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 289
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()V
    .locals 6

    .prologue
    .line 424
    iget-object v1, p0, Lbqb;->h:Ljava/lang/Runnable;

    monitor-enter v1

    .line 425
    :try_start_0
    iget-boolean v0, p0, Lbqb;->a:Z

    if-eqz v0, :cond_0

    .line 426
    invoke-direct {p0}, Lbqb;->g()J

    move-result-wide v2

    .line 427
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 428
    iget-object v0, p0, Lbqb;->g:Landroid/os/Handler;

    iget-object v4, p0, Lbqb;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 431
    :cond_0
    monitor-exit v1

    .line 432
    return-void

    .line 431
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d(J)V
    .locals 3

    .prologue
    .line 403
    iget-object v0, p0, Lbqb;->f:Landroid/content/Context;

    .line 404
    invoke-virtual {p0}, Lbqb;->a()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lbqb;->d:Z

    invoke-static {v0, p1, p2, v1, v2}, Lbqe;->a(Landroid/content/Context;JLjava/lang/String;Z)V

    .line 406
    return-void
.end method

.method public e()V
    .locals 3

    .prologue
    .line 435
    iget-object v1, p0, Lbqb;->h:Ljava/lang/Runnable;

    monitor-enter v1

    .line 436
    :try_start_0
    iget-object v0, p0, Lbqb;->g:Landroid/os/Handler;

    iget-object v2, p0, Lbqb;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 437
    monitor-exit v1

    .line 438
    return-void

    .line 437
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized f()Ljava/lang/String;
    .locals 4

    .prologue
    .line 442
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbqb;->c:Ljava/util/Map;

    iget-wide v2, p0, Lbqb;->i:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdg;

    .line 443
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcdg;->d()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    const-string/jumbo v0, ""
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 442
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
