.class Lath$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/widget/AttachmentMediaView$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lath;->b(Lcom/twitter/android/media/selection/MediaAttachment;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lath;


# direct methods
.method constructor <init>(Lath;)V
    .locals 0

    .prologue
    .line 665
    iput-object p1, p0, Lath$2;->a:Lath;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 668
    iget-object v0, p0, Lath$2;->a:Lath;

    invoke-static {v0}, Lath;->f(Lath;)Latg$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 669
    iget-object v0, p0, Lath$2;->a:Lath;

    invoke-static {v0}, Lath;->f(Lath;)Latg$a;

    move-result-object v0

    invoke-interface {v0}, Latg$a;->e()V

    .line 671
    :cond_0
    iget-object v0, p0, Lath$2;->a:Lath;

    invoke-static {v0}, Lath;->n(Lath;)V

    .line 672
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/widget/AttachmentMediaView;)V
    .locals 4

    .prologue
    .line 676
    iget-object v0, p0, Lath$2;->a:Lath;

    invoke-static {v0}, Lath;->o(Lath;)Lcom/twitter/android/media/selection/MediaAttachment;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lath$2;->a:Lath;

    .line 677
    invoke-static {v0}, Lath;->o(Lath;)Lcom/twitter/android/media/selection/MediaAttachment;

    move-result-object v0

    iget v0, v0, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    if-nez v0, :cond_0

    .line 678
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v0

    sget-object v1, Lcom/twitter/media/model/MediaType;->c:Lcom/twitter/media/model/MediaType;

    if-ne v0, v1, :cond_1

    .line 686
    :cond_0
    :goto_0
    return-void

    .line 681
    :cond_1
    iget-object v0, p0, Lath$2;->a:Lath;

    invoke-static {v0}, Lath;->a(Lath;)Lcom/twitter/android/media/selection/c;

    move-result-object v1

    iget-object v0, p0, Lath$2;->a:Lath;

    .line 683
    invoke-static {v0}, Lath;->o(Lath;)Lcom/twitter/android/media/selection/MediaAttachment;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/selection/MediaAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 682
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableMedia;

    .line 683
    invoke-virtual {v0}, Lcom/twitter/model/media/EditableMedia;->b()Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    const/4 v2, 0x0

    iget-object v3, p0, Lath$2;->a:Lath;

    .line 681
    invoke-virtual {v1, v0, v2, v3}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;Lcom/twitter/android/media/selection/a;)V

    goto :goto_0
.end method

.method public b(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/widget/AttachmentMediaView;)V
    .locals 0

    .prologue
    .line 691
    return-void
.end method

.method public c(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/widget/AttachmentMediaView;)V
    .locals 0

    .prologue
    .line 696
    return-void
.end method

.method public d(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/widget/AttachmentMediaView;)V
    .locals 0

    .prologue
    .line 701
    return-void
.end method
