.class public Lcfu;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcfu$b;,
        Lcfu$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcfu;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:J

.field public final c:J

.field public final d:J

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Z

.field public final h:Lcom/twitter/model/core/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/model/core/f",
            "<",
            "Lcom/twitter/model/core/q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcfu$b;

    invoke-direct {v0}, Lcfu$b;-><init>()V

    sput-object v0, Lcfu;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcfu$a;)V
    .locals 4

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lcfu$a;->a(Lcfu$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcfu;->b:J

    .line 33
    invoke-static {p1}, Lcfu$a;->b(Lcfu$a;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 34
    invoke-static {p1}, Lcfu$a;->b(Lcfu$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcfu;->c:J

    .line 38
    :goto_0
    invoke-static {p1}, Lcfu$a;->c(Lcfu$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcfu;->d:J

    .line 39
    invoke-static {p1}, Lcfu$a;->d(Lcfu$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfu;->e:Ljava/lang/String;

    .line 43
    invoke-static {p1}, Lcfu$a;->e(Lcfu$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcfu;->f:Ljava/lang/String;

    .line 44
    invoke-static {p1}, Lcfu$a;->f(Lcfu$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcfu;->g:Z

    .line 45
    invoke-static {p1}, Lcfu$a;->g(Lcfu$a;)Lcom/twitter/model/core/f;

    move-result-object v0

    iput-object v0, p0, Lcfu;->h:Lcom/twitter/model/core/f;

    .line 46
    return-void

    .line 36
    :cond_0
    iget-wide v0, p0, Lcfu;->b:J

    iput-wide v0, p0, Lcfu;->c:J

    goto :goto_0

    .line 43
    :cond_1
    invoke-static {p1}, Lcfu$a;->e(Lcfu$a;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method synthetic constructor <init>(Lcfu$a;Lcfu$1;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcfu;-><init>(Lcfu$a;)V

    return-void
.end method
