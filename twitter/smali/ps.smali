.class public Lps;
.super Lpo;
.source "Twttr"


# instance fields
.field final c:Lpg;

.field volatile d:J

.field e:Z

.field private final f:Lcom/twitter/library/av/model/a;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lcom/twitter/library/av/model/a;

    invoke-direct {v0}, Lcom/twitter/library/av/model/a;-><init>()V

    new-instance v1, Lpg;

    invoke-direct {v1}, Lpg;-><init>()V

    invoke-direct {p0, p1, p2, v0, v1}, Lps;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/model/a;Lpg;)V

    .line 31
    return-void
.end method

.method constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/model/a;Lpg;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lpo;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V

    .line 37
    iput-object p3, p0, Lps;->f:Lcom/twitter/library/av/model/a;

    .line 38
    iput-object p4, p0, Lps;->c:Lpg;

    .line 39
    return-void
.end method


# virtual methods
.method public processTick(Lbki;)V
    .locals 7
    .annotation runtime Lbiz;
        a = Lbki;
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 43
    invoke-virtual {p0}, Lps;->b()Lcom/twitter/library/av/m;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lcom/twitter/library/av/m;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 45
    iget-object v1, p0, Lps;->c:Lpg;

    invoke-virtual {v1}, Lpg;->a()V

    .line 48
    :cond_0
    iget-boolean v1, p0, Lps;->e:Z

    if-nez v1, :cond_1

    .line 49
    iput-boolean v6, p0, Lps;->e:Z

    .line 50
    iget-object v1, p0, Lps;->f:Lcom/twitter/library/av/model/a;

    iget-object v2, p0, Lps;->k:Lcom/twitter/model/av/AVMedia;

    iget-object v3, p1, Lbki;->b:Lcom/twitter/library/av/playback/aa;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/av/model/a;->a(Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/playback/aa;)J

    move-result-wide v2

    iput-wide v2, p0, Lps;->d:J

    .line 57
    :cond_1
    iget-object v1, p0, Lps;->c:Lpg;

    invoke-virtual {v1}, Lpg;->b()J

    move-result-wide v2

    iget-wide v4, p0, Lps;->d:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_2

    .line 58
    iput-boolean v6, p0, Lps;->b:Z

    .line 59
    iget-object v1, p0, Lps;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-static {v0}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/library/av/m;)Lcom/twitter/library/av/m$a;

    move-result-object v2

    const-string/jumbo v3, "view_threshold"

    invoke-virtual {v2, v3}, Lcom/twitter/library/av/m$a;->a(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/av/m$a;->a()Lcom/twitter/library/av/m;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/m;)V

    .line 60
    iget-object v1, p0, Lps;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->h()Lbix;

    move-result-object v1

    new-instance v2, Lbkk;

    iget-object v3, p0, Lps;->k:Lcom/twitter/model/av/AVMedia;

    invoke-direct {v2, v3}, Lbkk;-><init>(Lcom/twitter/model/av/AVMedia;)V

    invoke-virtual {v1, v2, v0}, Lbix;->a(Lbiw;Lcom/twitter/library/av/m;)V

    .line 62
    :cond_2
    return-void
.end method
