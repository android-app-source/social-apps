.class public final Lzb$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lzb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:Lamu;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lzb$1;)V
    .locals 0

    .prologue
    .line 310
    invoke-direct {p0}, Lzb$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lzb$a;)Lamu;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lzb$a;->a:Lamu;

    return-object v0
.end method


# virtual methods
.method public a(Lamu;)Lzb$a;
    .locals 1

    .prologue
    .line 367
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamu;

    iput-object v0, p0, Lzb$a;->a:Lamu;

    .line 368
    return-object p0
.end method

.method public a(Lzh;)Lzb$a;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 362
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 363
    return-object p0
.end method

.method public a()Lzf;
    .locals 3

    .prologue
    .line 316
    iget-object v0, p0, Lzb$a;->a:Lamu;

    if-nez v0, :cond_0

    .line 317
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lamu;

    .line 318
    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 320
    :cond_0
    new-instance v0, Lzb;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lzb;-><init>(Lzb$a;Lzb$1;)V

    return-object v0
.end method
