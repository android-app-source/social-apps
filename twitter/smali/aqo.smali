.class public Laqo;
.super Lcno$a;
.source "Twttr"


# instance fields
.field private final a:J

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcno$a;-><init>()V

    .line 31
    iput-wide p2, p0, Laqo;->a:J

    .line 32
    iput-object p1, p0, Laqo;->b:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public a(Lcno;)V
    .locals 6

    .prologue
    .line 38
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog$a;

    iget-wide v2, p0, Laqo;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog$a;-><init>(J)V

    iget-object v1, p0, Laqo;->b:Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "stream"

    const-string/jumbo v4, "top"

    const-string/jumbo v5, "show"

    .line 39
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/analytics/feature/model/ClientEventLog$a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog$a;->a()Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 38
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 40
    return-void
.end method

.method public b(Lcno;)V
    .locals 6

    .prologue
    .line 45
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog$a;

    iget-wide v2, p0, Laqo;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog$a;-><init>(J)V

    iget-object v1, p0, Laqo;->b:Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "stream"

    const-string/jumbo v4, "top"

    const-string/jumbo v5, "hide"

    .line 46
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/analytics/feature/model/ClientEventLog$a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog$a;->a()Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 45
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 47
    return-void
.end method
