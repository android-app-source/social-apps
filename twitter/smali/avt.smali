.class public Lavt;
.super Lcbr;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbr",
        "<",
        "Lcom/twitter/database/schema/DraftsSchema$a$a;",
        "Lcom/twitter/model/drafts/a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcbr;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/database/schema/DraftsSchema$a$a;)Lcom/twitter/model/drafts/a;
    .locals 4

    .prologue
    .line 15
    new-instance v0, Lcom/twitter/model/drafts/a$a;

    invoke-direct {v0}, Lcom/twitter/model/drafts/a$a;-><init>()V

    .line 16
    invoke-interface {p1}, Lcom/twitter/database/schema/DraftsSchema$a$a;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/drafts/a$a;->a(J)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 17
    invoke-interface {p1}, Lcom/twitter/database/schema/DraftsSchema$a$a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Ljava/lang/String;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 18
    invoke-interface {p1}, Lcom/twitter/database/schema/DraftsSchema$a$a;->f()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Ljava/util/List;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 19
    invoke-interface {p1}, Lcom/twitter/database/schema/DraftsSchema$a$a;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/drafts/a$a;->b(J)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 20
    invoke-interface {p1}, Lcom/twitter/database/schema/DraftsSchema$a$a;->i()Lcom/twitter/model/geo/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Lcom/twitter/model/geo/c;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 21
    invoke-interface {p1}, Lcom/twitter/database/schema/DraftsSchema$a$a;->g()Ljava/util/List;

    move-result-object v1

    invoke-interface {p1}, Lcom/twitter/database/schema/DraftsSchema$a$a;->h()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/model/drafts/a$a;->a(Ljava/util/List;J)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 22
    invoke-interface {p1}, Lcom/twitter/database/schema/DraftsSchema$a$a;->d()Lcgi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Lcgi;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 23
    invoke-interface {p1}, Lcom/twitter/database/schema/DraftsSchema$a$a;->e()Lcom/twitter/model/core/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Lcom/twitter/model/core/r;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 24
    invoke-interface {p1}, Lcom/twitter/database/schema/DraftsSchema$a$a;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->b(Ljava/lang/String;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 25
    invoke-interface {p1}, Lcom/twitter/database/schema/DraftsSchema$a$a;->k()Lcau;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Lcau;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 26
    invoke-interface {p1}, Lcom/twitter/database/schema/DraftsSchema$a$a;->l()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Z)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 27
    invoke-interface {p1}, Lcom/twitter/database/schema/DraftsSchema$a$a;->m()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->b(Ljava/util/List;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 28
    invoke-interface {p1}, Lcom/twitter/database/schema/DraftsSchema$a$a;->n()Lcom/twitter/model/timeline/ap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Lcom/twitter/model/timeline/ap;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 29
    invoke-interface {p1}, Lcom/twitter/database/schema/DraftsSchema$a$a;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->c(Ljava/lang/String;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 30
    invoke-interface {p1}, Lcom/twitter/database/schema/DraftsSchema$a$a;->p()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->c(Ljava/util/List;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 31
    invoke-interface {p1}, Lcom/twitter/database/schema/DraftsSchema$a$a;->q()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/drafts/a$a;->c(J)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 32
    invoke-interface {p1}, Lcom/twitter/database/schema/DraftsSchema$a$a;->r()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->b(Z)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lcom/twitter/model/drafts/a$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/a;

    .line 15
    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    check-cast p1, Lcom/twitter/database/schema/DraftsSchema$a$a;

    invoke-virtual {p0, p1}, Lavt;->a(Lcom/twitter/database/schema/DraftsSchema$a$a;)Lcom/twitter/model/drafts/a;

    move-result-object v0

    return-object v0
.end method
