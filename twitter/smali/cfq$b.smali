.class final Lcfq$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcfq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcfq;",
        "Lcfq$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 175
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcfq$1;)V
    .locals 0

    .prologue
    .line 175
    invoke-direct {p0}, Lcfq$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcfq$a;
    .locals 1

    .prologue
    .line 180
    new-instance v0, Lcfq$a;

    invoke-direct {v0}, Lcfq$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcfq$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 205
    sget-object v0, Lcgc;->a:Lcgc$b;

    .line 206
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 205
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {p2, v0}, Lcfq$a;->a(Ljava/util/List;)Lcfq$a;

    .line 207
    sget-object v0, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v1, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 208
    invoke-static {v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 207
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {p2, v0}, Lcfq$a;->a(Ljava/util/Map;)Lcfq$a;

    .line 209
    sget-object v0, Lcgb;->a:Lcgb$f;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgb;

    invoke-virtual {p2, v0}, Lcfq$a;->a(Lcgb;)Lcfq$a;

    .line 210
    sget-object v0, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v1, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 211
    invoke-static {v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 210
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {p2, v0}, Lcfq$a;->b(Ljava/util/Map;)Lcfq$a;

    .line 212
    sget-object v0, Lcgb;->a:Lcgb$f;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgb;

    invoke-virtual {p2, v0}, Lcfq$a;->b(Lcgb;)Lcfq$a;

    .line 213
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    invoke-virtual {p2, v0}, Lcfq$a;->a(I)Lcfq$a;

    .line 214
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcfq$a;->c(Ljava/lang/String;)Lcfq$a;

    .line 215
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->h()D

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcfq$a;->a(D)Lcfq$a;

    .line 216
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcfq$a;->a(Z)Lcfq$a;

    .line 217
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcfq$a;->a(Ljava/lang/String;)Lcfq$a;

    .line 218
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcfq$a;->b(Ljava/lang/String;)Lcfq$a;

    .line 219
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 175
    check-cast p2, Lcfq$a;

    invoke-virtual {p0, p1, p2, p3}, Lcfq$b;->a(Lcom/twitter/util/serialization/n;Lcfq$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcfq;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 186
    iget-object v0, p2, Lcfq;->b:Ljava/util/List;

    sget-object v1, Lcgc;->a:Lcgc$b;

    .line 187
    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 188
    iget-object v0, p2, Lcfq;->c:Ljava/util/Map;

    sget-object v1, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v2, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 189
    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/Map;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)V

    .line 190
    iget-object v0, p2, Lcfq;->d:Lcgb;

    sget-object v1, Lcgb;->a:Lcgb$f;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 191
    iget-object v0, p2, Lcfq;->e:Ljava/util/Map;

    sget-object v1, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v2, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 192
    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/Map;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)V

    .line 193
    iget-object v0, p2, Lcfq;->f:Lcgb;

    sget-object v1, Lcgb;->a:Lcgb$f;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 194
    iget v0, p2, Lcfq;->g:I

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    .line 195
    iget-object v0, p2, Lcfq;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 196
    iget-wide v0, p2, Lcfq;->i:D

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(D)Lcom/twitter/util/serialization/o;

    .line 197
    iget-boolean v0, p2, Lcfq;->j:Z

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    .line 198
    iget-object v0, p2, Lcfq;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 199
    iget-object v0, p2, Lcfq;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 200
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 175
    check-cast p2, Lcfq;

    invoke-virtual {p0, p1, p2}, Lcfq$b;->a(Lcom/twitter/util/serialization/o;Lcfq;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lcfq$b;->a()Lcfq$a;

    move-result-object v0

    return-object v0
.end method
