.class public Lavx;
.super Lcom/twitter/database/hydrator/a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/database/hydrator/a",
        "<",
        "Lcec;",
        "Lazl$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/twitter/database/hydrator/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcec;Lazl$a;)Lazl$a;
    .locals 4

    .prologue
    .line 16
    iget-wide v0, p1, Lcec;->a:J

    .line 17
    invoke-interface {p2, v0, v1}, Lazl$a;->c(J)Lazl$a;

    move-result-object v0

    iget-wide v2, p1, Lcec;->b:J

    .line 18
    invoke-interface {v0, v2, v3}, Lazl$a;->d(J)Lazl$a;

    move-result-object v0

    iget-object v1, p1, Lcec;->e:Ljava/lang/String;

    .line 19
    invoke-interface {v0, v1}, Lazl$a;->a(Ljava/lang/String;)Lazl$a;

    move-result-object v0

    iget-object v1, p1, Lcec;->d:Lcdw;

    .line 20
    invoke-interface {v0, v1}, Lazl$a;->a(Lcdw;)Lazl$a;

    move-result-object v0

    iget-boolean v1, p1, Lcec;->g:Z

    .line 21
    invoke-interface {v0, v1}, Lazl$a;->b(Z)Lazl$a;

    move-result-object v0

    iget-object v1, p1, Lcec;->h:Ljava/lang/String;

    .line 22
    invoke-interface {v0, v1}, Lazl$a;->b(Ljava/lang/String;)Lazl$a;

    move-result-object v0

    iget-object v1, p1, Lcec;->i:Lcef;

    iget-wide v2, v1, Lcef;->b:J

    .line 23
    invoke-interface {v0, v2, v3}, Lazl$a;->e(J)Lazl$a;

    move-result-object v0

    iget-object v1, p1, Lcec;->i:Lcef;

    iget-wide v2, v1, Lcef;->c:J

    .line 24
    invoke-interface {v0, v2, v3}, Lazl$a;->f(J)Lazl$a;

    move-result-object v0

    .line 16
    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    check-cast p1, Lcec;

    check-cast p2, Lazl$a;

    invoke-virtual {p0, p1, p2}, Lavx;->a(Lcec;Lazl$a;)Lazl$a;

    move-result-object v0

    return-object v0
.end method
