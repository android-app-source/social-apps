.class public Lcci;
.super Lcbx;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcci$b;,
        Lcci$a;
    }
.end annotation


# static fields
.field public static final b:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcci;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:J

.field public final d:Lcom/twitter/model/core/r;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcci$b;

    invoke-direct {v0}, Lcci$b;-><init>()V

    sput-object v0, Lcci;->b:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcci$a;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcbx;-><init>(Lcbx$a;)V

    .line 30
    invoke-static {p1}, Lcci$a;->a(Lcci$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcci;->c:J

    .line 31
    invoke-static {p1}, Lcci$a;->b(Lcci$a;)Lcom/twitter/model/core/r;

    move-result-object v0

    iput-object v0, p0, Lcci;->d:Lcom/twitter/model/core/r;

    .line 32
    return-void
.end method

.method synthetic constructor <init>(Lcci$a;Lcci$1;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcci;-><init>(Lcci$a;)V

    return-void
.end method

.method private a(Lcci;)Z
    .locals 4

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcbx;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcci;->c:J

    iget-wide v2, p1, Lcci;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcci;->d:Lcom/twitter/model/core/r;

    iget-object v1, p1, Lcci;->d:Lcom/twitter/model/core/r;

    .line 54
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/r;->a(Lcom/twitter/model/core/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 52
    :goto_0
    return v0

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x4

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 48
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcci;

    if-eqz v0, :cond_1

    check-cast p1, Lcci;

    invoke-direct {p0, p1}, Lcci;->a(Lcci;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 59
    invoke-super {p0}, Lcbx;->hashCode()I

    move-result v0

    .line 60
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcci;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 61
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcci;->d:Lcom/twitter/model/core/r;

    invoke-virtual {v1}, Lcom/twitter/model/core/r;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    return v0
.end method
