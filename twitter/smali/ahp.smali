.class public Lahp;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Laib;

.field private final c:Lage;

.field private final d:Lahr;

.field private final e:Lahg;

.field private final f:Lahe;

.field private final g:Lahi;

.field private final h:Lahc;

.field private final i:Laha;

.field private final j:Lcom/twitter/library/client/Session;

.field private final k:Lcom/twitter/library/service/t;

.field private final l:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Ljava/util/List",
            "<",
            "Laij;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Lcmt;

.field private n:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Laib;Lage;Lahr;Lahg;Lahe;Lahi;Lahc;Laha;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lahp$1;

    invoke-direct {v0, p0}, Lahp$1;-><init>(Lahp;)V

    iput-object v0, p0, Lahp;->k:Lcom/twitter/library/service/t;

    .line 60
    new-instance v0, Lahp$2;

    invoke-direct {v0, p0}, Lahp$2;-><init>(Lahp;)V

    iput-object v0, p0, Lahp;->l:Lcom/twitter/util/q;

    .line 81
    iput-object p1, p0, Lahp;->a:Landroid/app/Activity;

    .line 82
    iput-object p2, p0, Lahp;->b:Laib;

    .line 83
    iput-object p3, p0, Lahp;->c:Lage;

    .line 84
    iput-object p4, p0, Lahp;->d:Lahr;

    .line 85
    iput-object p5, p0, Lahp;->e:Lahg;

    .line 86
    iput-object p6, p0, Lahp;->f:Lahe;

    .line 87
    iput-object p7, p0, Lahp;->g:Lahi;

    .line 88
    iput-object p8, p0, Lahp;->h:Lahc;

    .line 89
    iput-object p9, p0, Lahp;->i:Laha;

    .line 90
    iput-object p10, p0, Lahp;->j:Lcom/twitter/library/client/Session;

    .line 91
    return-void
.end method

.method static synthetic a(Lahp;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lahp;->b()V

    return-void
.end method

.method static synthetic a(Lahp;Lcom/twitter/library/service/s;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lahp;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method private a(Lcom/twitter/library/service/s;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 172
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->L()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    move v2, v0

    .line 173
    :goto_0
    if-eqz v2, :cond_1

    const v0, 0x7f0a07b9

    move v1, v0

    .line 176
    :goto_1
    if-eqz v2, :cond_2

    const v0, 0x7f0a07ba

    .line 179
    :goto_2
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 180
    iget-object v0, p0, Lahp;->a:Landroid/app/Activity;

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 184
    :goto_3
    invoke-direct {p0}, Lahp;->b()V

    .line 185
    return-void

    :cond_0
    move v2, v3

    .line 172
    goto :goto_0

    .line 173
    :cond_1
    const v0, 0x7f0a0265

    move v1, v0

    goto :goto_1

    .line 176
    :cond_2
    const v0, 0x7f0a0266

    goto :goto_2

    .line 182
    :cond_3
    iget-object v1, p0, Lahp;->a:Landroid/app/Activity;

    invoke-static {v1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_3
.end method

.method static synthetic b(Lahp;)Lage;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lahp;->c:Lage;

    return-object v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lahp;->m:Lcmt;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lahp;->m:Lcmt;

    invoke-virtual {v0}, Lcmt;->h()V

    .line 191
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lahp;->n:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lcmr;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 125
    const v0, 0x7f13088d

    invoke-interface {p1, v0}, Lcmr;->b(I)Lcmm;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmm;

    invoke-interface {v0, v1}, Lcmm;->f(Z)Lcmm;

    .line 126
    iget-object v0, p0, Lahp;->j:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 127
    const v0, 0x7f1308cd

    invoke-interface {p1, v0}, Lcmr;->b(I)Lcmm;

    move-result-object v0

    .line 128
    const v2, 0x7f1308ce

    invoke-interface {p1, v2}, Lcmr;->b(I)Lcmm;

    move-result-object v2

    .line 129
    iget-object v3, p0, Lahp;->c:Lage;

    invoke-virtual {v3}, Lage;->a()Ljava/lang/String;

    move-result-object v3

    .line 130
    iget-object v4, p0, Lahp;->d:Lahr;

    invoke-virtual {v4, v3}, Lahr;->a(Ljava/lang/String;)Z

    move-result v3

    .line 131
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmm;

    if-nez v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-interface {v0, v1}, Lcmm;->f(Z)Lcmm;

    .line 132
    invoke-static {v2}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmm;

    invoke-interface {v0, v3}, Lcmm;->f(Z)Lcmm;

    .line 134
    :cond_1
    return-void
.end method

.method public a(Lcmt;Lcom/twitter/android/search/SearchSuggestionController;)V
    .locals 3

    .prologue
    .line 100
    iput-object p1, p0, Lahp;->m:Lcmt;

    .line 101
    iget-object v0, p0, Lahp;->m:Lcmt;

    invoke-virtual {v0}, Lcmt;->c()Lcmr;

    move-result-object v0

    .line 102
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "search_features_bad_search_report_enabled"

    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 104
    const v1, 0x7f14002b

    invoke-interface {v0, v1}, Lcmr;->a(I)V

    .line 106
    :cond_0
    const v1, 0x7f140029

    invoke-interface {v0, v1}, Lcmr;->a(I)V

    .line 107
    const v1, 0x7f14002f

    invoke-interface {v0, v1}, Lcmr;->a(I)V

    .line 109
    iget-object v1, p0, Lahp;->c:Lage;

    invoke-virtual {v1}, Lage;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lahp;->n:Ljava/lang/String;

    .line 110
    iget-object v1, p0, Lahp;->b:Laib;

    iget-object v2, p0, Lahp;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Laib;->a(Ljava/lang/String;)V

    .line 112
    invoke-interface {v0}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 113
    iget-object v1, p0, Lahp;->b:Laib;

    invoke-virtual {v1}, Laib;->aN_()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setCustomView(Landroid/view/View;)V

    .line 114
    iget-object v0, p0, Lahp;->b:Laib;

    new-instance v1, Lahp$3;

    invoke-direct {v1, p0, p2}, Lahp$3;-><init>(Lahp;Lcom/twitter/android/search/SearchSuggestionController;)V

    invoke-virtual {v0, v1}, Laib;->a(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v0, p0, Lahp;->d:Lahr;

    iget-object v1, p0, Lahp;->l:Lcom/twitter/util/q;

    invoke-virtual {v0, v1}, Lahr;->a(Lcom/twitter/util/q;)V

    .line 122
    return-void
.end method

.method public a(Lcmm;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 137
    iget-object v0, p0, Lahp;->c:Lage;

    invoke-virtual {v0}, Lage;->a()Ljava/lang/String;

    move-result-object v3

    .line 138
    iget-object v0, p0, Lahp;->c:Lage;

    invoke-virtual {v0}, Lage;->b()Ljava/lang/String;

    move-result-object v4

    .line 139
    const-wide/16 v0, 0x0

    .line 140
    iget-object v5, p0, Lahp;->d:Lahr;

    invoke-virtual {v5, v3}, Lahr;->b(Ljava/lang/String;)Laij;

    move-result-object v5

    .line 141
    if-eqz v5, :cond_0

    .line 142
    invoke-virtual {v5}, Laij;->b()J

    move-result-wide v0

    .line 144
    :cond_0
    invoke-interface {p1}, Lcmm;->a()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 166
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 146
    :sswitch_0
    iget-object v4, p0, Lahp;->e:Lahg;

    iget-object v5, p0, Lahp;->k:Lcom/twitter/library/service/t;

    invoke-virtual {v4, v3, v0, v1, v5}, Lahg;->a(Ljava/lang/String;JLcom/twitter/library/service/t;)V

    move v0, v2

    .line 147
    goto :goto_0

    .line 150
    :sswitch_1
    iget-object v4, p0, Lahp;->f:Lahe;

    iget-object v5, p0, Lahp;->k:Lcom/twitter/library/service/t;

    invoke-virtual {v4, v3, v0, v1, v5}, Lahe;->a(Ljava/lang/String;JLcom/twitter/library/service/t;)V

    move v0, v2

    .line 151
    goto :goto_0

    .line 154
    :sswitch_2
    iget-object v0, p0, Lahp;->g:Lahi;

    invoke-virtual {v0, v3, v4}, Lahi;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 155
    goto :goto_0

    .line 158
    :sswitch_3
    iget-object v0, p0, Lahp;->h:Lahc;

    invoke-virtual {v0, v3}, Lahc;->a(Ljava/lang/String;)V

    move v0, v2

    .line 159
    goto :goto_0

    .line 162
    :sswitch_4
    iget-object v0, p0, Lahp;->i:Laha;

    invoke-virtual {v0}, Laha;->a()V

    move v0, v2

    .line 163
    goto :goto_0

    .line 144
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f1308bc -> :sswitch_2
        0x7f1308cc -> :sswitch_4
        0x7f1308cd -> :sswitch_0
        0x7f1308ce -> :sswitch_1
        0x7f1308d0 -> :sswitch_3
    .end sparse-switch
.end method
