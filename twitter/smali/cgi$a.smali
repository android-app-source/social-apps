.class public final Lcgi$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcgi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcgi;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:J

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Z

.field g:Z

.field h:J

.field i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field j:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 156
    return-void
.end method

.method public constructor <init>(Lcgi;)V
    .locals 2

    .prologue
    .line 158
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 159
    iget-object v0, p1, Lcgi;->c:Ljava/lang/String;

    iput-object v0, p0, Lcgi$a;->a:Ljava/lang/String;

    .line 160
    iget-object v0, p1, Lcgi;->d:Ljava/lang/String;

    iput-object v0, p0, Lcgi$a;->b:Ljava/lang/String;

    .line 161
    iget-wide v0, p1, Lcgi;->e:J

    iput-wide v0, p0, Lcgi$a;->c:J

    .line 162
    iget-object v0, p1, Lcgi;->f:Ljava/lang/String;

    iput-object v0, p0, Lcgi$a;->d:Ljava/lang/String;

    .line 163
    iget-object v0, p1, Lcgi;->g:Ljava/lang/String;

    iput-object v0, p0, Lcgi$a;->e:Ljava/lang/String;

    .line 164
    iget-boolean v0, p1, Lcgi;->h:Z

    iput-boolean v0, p0, Lcgi$a;->f:Z

    .line 165
    iget-boolean v0, p1, Lcgi;->i:Z

    iput-boolean v0, p0, Lcgi$a;->g:Z

    .line 166
    iget-wide v0, p1, Lcgi;->j:J

    iput-wide v0, p0, Lcgi$a;->h:J

    .line 167
    iget-object v0, p1, Lcgi;->k:Ljava/util/Set;

    iput-object v0, p0, Lcgi$a;->i:Ljava/util/Set;

    .line 168
    return-void
.end method


# virtual methods
.method public a(J)Lcgi$a;
    .locals 1

    .prologue
    .line 184
    iput-wide p1, p0, Lcgi$a;->c:J

    .line 185
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcgi$a;
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcgi$a;->a:Ljava/lang/String;

    .line 173
    return-object p0
.end method

.method public a(Ljava/util/Set;)Lcgi$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcgi$a;"
        }
    .end annotation

    .prologue
    .line 220
    iput-object p1, p0, Lcgi$a;->i:Ljava/util/Set;

    .line 221
    return-object p0
.end method

.method public a(Z)Lcgi$a;
    .locals 0

    .prologue
    .line 202
    iput-boolean p1, p0, Lcgi$a;->f:Z

    .line 203
    return-object p0
.end method

.method public b(J)Lcgi$a;
    .locals 1

    .prologue
    .line 214
    iput-wide p1, p0, Lcgi$a;->h:J

    .line 215
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcgi$a;
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcgi$a;->b:Ljava/lang/String;

    .line 179
    return-object p0
.end method

.method public b(Z)Lcgi$a;
    .locals 0

    .prologue
    .line 208
    iput-boolean p1, p0, Lcgi$a;->g:Z

    .line 209
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p0}, Lcgi$a;->f()Lcgi;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcgi$a;
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcgi$a;->d:Ljava/lang/String;

    .line 191
    return-object p0
.end method

.method public c(Z)Lcgi$a;
    .locals 0

    .prologue
    .line 226
    iput-boolean p1, p0, Lcgi$a;->j:Z

    .line 227
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcgi$a;
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcgi$a;->e:Ljava/lang/String;

    .line 197
    return-object p0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcgi$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method protected f()Lcgi;
    .locals 1

    .prologue
    .line 238
    new-instance v0, Lcgi;

    invoke-direct {v0, p0}, Lcgi;-><init>(Lcgi$a;)V

    return-object v0
.end method
