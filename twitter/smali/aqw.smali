.class public Laqw;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/onboarding/flowstep/common/a;
.implements Lcom/twitter/app/onboarding/flowstep/common/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<FragmentType:",
        "Lcom/twitter/app/common/abs/AbsFragment;",
        ":",
        "Lcom/twitter/app/onboarding/flowstep/common/h",
        "<",
        "Lcom/twitter/model/onboarding/a;",
        ">;>",
        "Ljava/lang/Object;",
        "Lcom/twitter/app/onboarding/flowstep/common/a;",
        "Lcom/twitter/app/onboarding/flowstep/common/h",
        "<",
        "Lcom/twitter/model/onboarding/a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/app/common/abs/AbsFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TFragmentType;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/app/common/abs/AbsFragment;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TFragmentType;)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Laqw;->a:Lcom/twitter/app/common/abs/AbsFragment;

    .line 42
    return-void
.end method

.method public static a(Lcom/twitter/android/Flow$Step;Landroid/support/v4/app/FragmentManager;)Laqw;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<FragmentType:",
            "Lcom/twitter/app/common/abs/AbsFragment;",
            ":",
            "Lcom/twitter/app/onboarding/flowstep/common/h",
            "<",
            "Lcom/twitter/model/onboarding/a;",
            ">;>(",
            "Lcom/twitter/android/Flow$Step;",
            "Landroid/support/v4/app/FragmentManager;",
            ")",
            "Laqw",
            "<TFragmentType;>;"
        }
    .end annotation

    .prologue
    .line 28
    new-instance v1, Lcom/twitter/app/common/base/b$a;

    invoke-direct {v1}, Lcom/twitter/app/common/base/b$a;-><init>()V

    .line 29
    invoke-virtual {p0, p1}, Lcom/twitter/android/Flow$Step;->a(Landroid/support/v4/app/FragmentManager;)Lcom/twitter/app/common/abs/AbsFragment;

    move-result-object v0

    .line 30
    if-nez v0, :cond_0

    .line 31
    invoke-virtual {p0, v1}, Lcom/twitter/android/Flow$Step;->a(Lcom/twitter/app/common/base/b$a;)Lcom/twitter/app/common/abs/AbsFragment;

    move-result-object v0

    .line 32
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/twitter/app/common/abs/AbsFragment;->setRetainInstance(Z)V

    .line 33
    invoke-virtual {v1}, Lcom/twitter/app/common/base/b$a;->c()Lcom/twitter/app/common/base/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/abs/AbsFragment;->a(Lcom/twitter/app/common/base/b;)V

    .line 34
    invoke-virtual {p0, v0}, Lcom/twitter/android/Flow$Step;->a(Lcom/twitter/app/common/abs/AbsFragment;)V

    .line 35
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f1302e4

    invoke-virtual {p0}, Lcom/twitter/android/Flow$Step;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 37
    :cond_0
    new-instance v1, Laqw;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/AbsFragment;

    invoke-direct {v1, v0}, Laqw;-><init>(Lcom/twitter/app/common/abs/AbsFragment;)V

    return-object v1
.end method


# virtual methods
.method public e()Lcom/twitter/model/onboarding/a;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Laqw;->a:Lcom/twitter/app/common/abs/AbsFragment;

    check-cast v0, Lcom/twitter/app/onboarding/flowstep/common/h;

    invoke-interface {v0}, Lcom/twitter/app/onboarding/flowstep/common/h;->e()Lcom/twitter/model/onboarding/a;

    move-result-object v0

    return-object v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Laqw;->a:Lcom/twitter/app/common/abs/AbsFragment;

    instance-of v0, v0, Lcom/twitter/app/onboarding/flowstep/common/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqw;->a:Lcom/twitter/app/common/abs/AbsFragment;

    check-cast v0, Lcom/twitter/app/onboarding/flowstep/common/a;

    invoke-interface {v0}, Lcom/twitter/app/onboarding/flowstep/common/a;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
