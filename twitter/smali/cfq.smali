.class public Lcfq;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcfq$b;,
        Lcfq$a;
    }
.end annotation


# static fields
.field public static final a:Lcfq$b;


# instance fields
.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcgc;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcgb;

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcgb;

.field public final g:I

.field public final h:Ljava/lang/String;

.field public final i:D

.field public final j:Z

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcfq$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcfq$b;-><init>(Lcfq$1;)V

    sput-object v0, Lcfq;->a:Lcfq$b;

    return-void
.end method

.method private constructor <init>(Lcfq$a;)V
    .locals 2

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    invoke-static {p1}, Lcfq$a;->a(Lcfq$a;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcfq;->b:Ljava/util/List;

    .line 72
    invoke-static {p1}, Lcfq$a;->b(Lcfq$a;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcfq;->c:Ljava/util/Map;

    .line 73
    invoke-static {p1}, Lcfq$a;->c(Lcfq$a;)Lcgb;

    move-result-object v0

    iput-object v0, p0, Lcfq;->d:Lcgb;

    .line 74
    invoke-static {p1}, Lcfq$a;->d(Lcfq$a;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcfq;->e:Ljava/util/Map;

    .line 75
    invoke-static {p1}, Lcfq$a;->e(Lcfq$a;)Lcgb;

    move-result-object v0

    iput-object v0, p0, Lcfq;->f:Lcgb;

    .line 76
    invoke-static {p1}, Lcfq$a;->f(Lcfq$a;)I

    move-result v0

    iput v0, p0, Lcfq;->g:I

    .line 77
    invoke-static {p1}, Lcfq$a;->g(Lcfq$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfq;->h:Ljava/lang/String;

    .line 78
    invoke-static {p1}, Lcfq$a;->h(Lcfq$a;)D

    move-result-wide v0

    iput-wide v0, p0, Lcfq;->i:D

    .line 79
    invoke-static {p1}, Lcfq$a;->i(Lcfq$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcfq;->j:Z

    .line 80
    invoke-static {p1}, Lcfq$a;->j(Lcfq$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfq;->k:Ljava/lang/String;

    .line 81
    invoke-static {p1}, Lcfq$a;->k(Lcfq$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfq;->l:Ljava/lang/String;

    .line 82
    return-void
.end method

.method synthetic constructor <init>(Lcfq$a;Lcfq$1;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcfq;-><init>(Lcfq$a;)V

    return-void
.end method


# virtual methods
.method public a()Lcfq$a;
    .locals 4

    .prologue
    .line 56
    new-instance v0, Lcfq$a;

    invoke-direct {v0}, Lcfq$a;-><init>()V

    iget-object v1, p0, Lcfq;->b:Ljava/util/List;

    .line 57
    invoke-virtual {v0, v1}, Lcfq$a;->a(Ljava/util/List;)Lcfq$a;

    move-result-object v0

    iget-object v1, p0, Lcfq;->c:Ljava/util/Map;

    .line 58
    invoke-virtual {v0, v1}, Lcfq$a;->a(Ljava/util/Map;)Lcfq$a;

    move-result-object v0

    iget-object v1, p0, Lcfq;->d:Lcgb;

    .line 59
    invoke-virtual {v0, v1}, Lcfq$a;->a(Lcgb;)Lcfq$a;

    move-result-object v0

    iget-object v1, p0, Lcfq;->e:Ljava/util/Map;

    .line 60
    invoke-virtual {v0, v1}, Lcfq$a;->b(Ljava/util/Map;)Lcfq$a;

    move-result-object v0

    iget-object v1, p0, Lcfq;->f:Lcgb;

    .line 61
    invoke-virtual {v0, v1}, Lcfq$a;->b(Lcgb;)Lcfq$a;

    move-result-object v0

    iget v1, p0, Lcfq;->g:I

    .line 62
    invoke-virtual {v0, v1}, Lcfq$a;->a(I)Lcfq$a;

    move-result-object v0

    iget-object v1, p0, Lcfq;->h:Ljava/lang/String;

    .line 63
    invoke-virtual {v0, v1}, Lcfq$a;->c(Ljava/lang/String;)Lcfq$a;

    move-result-object v0

    iget-wide v2, p0, Lcfq;->i:D

    .line 64
    invoke-virtual {v0, v2, v3}, Lcfq$a;->a(D)Lcfq$a;

    move-result-object v0

    iget-boolean v1, p0, Lcfq;->j:Z

    .line 65
    invoke-virtual {v0, v1}, Lcfq$a;->a(Z)Lcfq$a;

    move-result-object v0

    iget-object v1, p0, Lcfq;->k:Ljava/lang/String;

    .line 66
    invoke-virtual {v0, v1}, Lcfq$a;->a(Ljava/lang/String;)Lcfq$a;

    move-result-object v0

    iget-object v1, p0, Lcfq;->l:Ljava/lang/String;

    .line 67
    invoke-virtual {v0, v1}, Lcfq$a;->b(Ljava/lang/String;)Lcfq$a;

    move-result-object v0

    .line 56
    return-object v0
.end method
