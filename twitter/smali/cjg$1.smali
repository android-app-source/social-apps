.class Lcjg$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcjg;->a(Lcgg;)Lrx/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/d",
        "<",
        "Lcom/twitter/util/collection/m",
        "<",
        "Lcom/twitter/model/onboarding/Task;",
        "Ljava/lang/Object;",
        ">;",
        "Lcom/twitter/model/onboarding/b;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcjg;


# direct methods
.method constructor <init>(Lcjg;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcjg$1;->a:Lcjg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/util/collection/m;)Lcom/twitter/model/onboarding/b;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/onboarding/Task;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/twitter/model/onboarding/b;"
        }
    .end annotation

    .prologue
    .line 66
    invoke-virtual {p1}, Lcom/twitter/util/collection/m;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {p1}, Lcom/twitter/util/collection/m;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/onboarding/Task;

    .line 68
    iget-object v1, p0, Lcjg$1;->a:Lcjg;

    invoke-static {v1}, Lcjg;->a(Lcjg;)Lcje;

    move-result-object v1

    new-instance v2, Lcom/twitter/model/onboarding/TaskContext;

    .line 69
    invoke-virtual {v0}, Lcom/twitter/model/onboarding/Task;->a()Lcom/twitter/model/onboarding/Subtask;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/twitter/model/onboarding/TaskContext;-><init>(Lcom/twitter/model/onboarding/Task;Lcom/twitter/model/onboarding/Subtask;)V

    .line 68
    invoke-virtual {v1, v2}, Lcje;->a(Lcom/twitter/model/onboarding/TaskContext;)Lcom/twitter/model/onboarding/b;

    move-result-object v0

    .line 71
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/model/onboarding/d;->a:Lcom/twitter/model/onboarding/d;

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 62
    check-cast p1, Lcom/twitter/util/collection/m;

    invoke-virtual {p0, p1}, Lcjg$1;->a(Lcom/twitter/util/collection/m;)Lcom/twitter/model/onboarding/b;

    move-result-object v0

    return-object v0
.end method
