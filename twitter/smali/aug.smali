.class public Laug;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lauj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lauj",
        "<TA;TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<TA;TT;>;"
        }
    .end annotation
.end field

.field private final b:Lrx/subjects/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/d",
            "<TT;TT;>;"
        }
    .end annotation
.end field

.field private c:Z


# direct methods
.method public constructor <init>(Lauj;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lauj",
            "<TA;TT;>;)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Laug;->a:Lauj;

    .line 24
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Laug;->b:Lrx/subjects/d;

    .line 25
    return-void
.end method


# virtual methods
.method public a_(Ljava/lang/Object;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 30
    iget-boolean v0, p0, Laug;->c:Z

    if-eqz v0, :cond_0

    .line 31
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Querying an already closed data source"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Throwable;)Lrx/c;

    move-result-object v0

    .line 33
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Laug;->a:Lauj;

    invoke-interface {v0, p1}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Laug;->b:Lrx/subjects/d;

    invoke-virtual {v0, v1}, Lrx/c;->j(Lrx/c;)Lrx/c;

    move-result-object v0

    goto :goto_0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Laug;->b:Lrx/subjects/d;

    invoke-virtual {v0}, Lrx/subjects/d;->by_()V

    .line 39
    iget-object v0, p0, Laug;->a:Lauj;

    invoke-interface {v0}, Lauj;->close()V

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Laug;->c:Z

    .line 41
    return-void
.end method
