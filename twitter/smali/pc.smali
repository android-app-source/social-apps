.class public Lpc;
.super Lbjb;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V
    .locals 4

    .prologue
    .line 30
    new-instance v0, Lpd;

    invoke-direct {v0, p1}, Lpd;-><init>(Lcom/twitter/library/av/playback/AVPlayer;)V

    const/4 v1, 0x2

    new-array v1, v1, [Lpf;

    const/4 v2, 0x0

    new-instance v3, Lpb;

    invoke-direct {v3, p1}, Lpb;-><init>(Lcom/twitter/library/av/playback/AVPlayer;)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lpe;

    invoke-direct {v3, p1}, Lpe;-><init>(Lcom/twitter/library/av/playback/AVPlayer;)V

    aput-object v3, v1, v2

    .line 31
    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 30
    invoke-direct {p0, p2, v0}, Lpc;-><init>(Lcom/twitter/model/av/AVMedia;Ljava/util/List;)V

    .line 33
    return-void
.end method

.method constructor <init>(Lcom/twitter/model/av/AVMedia;Ljava/util/List;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/av/AVMedia;",
            "Ljava/util/List",
            "<",
            "Lpf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lbjb;-><init>(Lcom/twitter/model/av/AVMedia;)V

    .line 38
    iput-object p2, p0, Lpc;->a:Ljava/util/List;

    .line 39
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lpc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpf;

    .line 78
    invoke-virtual {v0}, Lpf;->c()V

    goto :goto_0

    .line 80
    :cond_0
    return-void
.end method


# virtual methods
.method public processLoop(Lbjt;)V
    .locals 1
    .annotation runtime Lbiz;
        a = Lbjt;
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lpc;->k:Lcom/twitter/model/av/AVMedia;

    invoke-interface {v0}, Lcom/twitter/model/av/AVMedia;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    invoke-direct {p0}, Lpc;->a()V

    .line 71
    :cond_0
    return-void
.end method

.method public processPlay(Lbjx;)V
    .locals 0
    .annotation runtime Lbiz;
        a = Lbjx;
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0}, Lpc;->a()V

    .line 64
    return-void
.end method

.method public processPlaybackStarted(Lbkc;)V
    .locals 3
    .annotation runtime Lbiz;
        a = Lbkc;
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lpc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpf;

    .line 52
    iget-object v2, p1, Lbkc;->b:Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;

    invoke-virtual {v0, v2}, Lpf;->a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V

    goto :goto_0

    .line 54
    :cond_0
    return-void
.end method

.method public processReplay(Lbkd;)V
    .locals 0
    .annotation runtime Lbiz;
        a = Lbkd;
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0}, Lpc;->a()V

    .line 59
    return-void
.end method

.method public processTick(Lbki;)V
    .locals 4
    .annotation runtime Lbiz;
        a = Lbki;
    .end annotation

    .prologue
    .line 43
    invoke-virtual {p0}, Lpc;->b()Lcom/twitter/library/av/m;

    move-result-object v1

    .line 44
    iget-object v0, p0, Lpc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpf;

    .line 45
    iget-object v3, p1, Lbki;->b:Lcom/twitter/library/av/playback/aa;

    invoke-virtual {v0, v3, v1}, Lpf;->b(Lcom/twitter/library/av/playback/aa;Lcom/twitter/library/av/m;)V

    goto :goto_0

    .line 47
    :cond_0
    return-void
.end method
