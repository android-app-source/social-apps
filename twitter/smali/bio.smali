.class public Lbio;
.super Lbao;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbao",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcom/twitter/model/core/TwitterUser$a;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field public a:Lcom/twitter/model/core/TwitterUser;

.field public b:Lcom/twitter/model/core/z;

.field private final c:J

.field private final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;)V
    .locals 7

    .prologue
    .line 66
    new-instance v3, Lcom/twitter/library/service/v;

    invoke-direct {v3, p2}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lbio;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;JLjava/lang/String;)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;JLjava/lang/String;)V
    .locals 5

    .prologue
    .line 76
    const-class v0, Lbio;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lbao;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 77
    iput-wide p3, p0, Lbio;->c:J

    .line 78
    iput-object p5, p0, Lbio;->h:Ljava/lang/String;

    .line 79
    iget-wide v0, p0, Lbio;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lbio;->h:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "userId must be non-zero or screenName must be non-null. userId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lbio;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", screenName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbio;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_0
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/TwitterUser$a;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 115
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 116
    invoke-virtual/range {p3 .. p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/twitter/model/core/TwitterUser$a;

    .line 117
    if-eqz v11, :cond_3

    .line 118
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    invoke-virtual {v11, v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->d(J)Lcom/twitter/model/core/TwitterUser$a;

    .line 119
    invoke-virtual {v11}, Lcom/twitter/model/core/TwitterUser$a;->q()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/twitter/model/core/TwitterUser;

    .line 121
    invoke-virtual {p0}, Lbio;->S()Laut;

    move-result-object v10

    .line 122
    invoke-virtual {p0}, Lbio;->R()Lcom/twitter/library/provider/t;

    move-result-object v0

    invoke-static {v12}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-wide/16 v2, -0x1

    const/4 v4, -0x1

    const-wide/16 v5, -0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-virtual/range {v0 .. v10}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;ZLaut;)I

    .line 124
    invoke-virtual {v10}, Laut;->a()V

    .line 126
    invoke-virtual {p0}, Lbio;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/library/service/v;->c:J

    iget-wide v2, v12, Lcom/twitter/model/core/TwitterUser;->b:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 127
    new-instance v0, Lbhy;

    iget-object v1, p0, Lbio;->p:Landroid/content/Context;

    invoke-virtual {p0}, Lbio;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbhy;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V

    .line 128
    invoke-virtual {v0, p0}, Lbhy;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/service/s;

    move-result-object v0

    check-cast v0, Lbhy;

    .line 129
    iget-wide v2, v12, Lcom/twitter/model/core/TwitterUser;->b:J

    iput-wide v2, v0, Lbhy;->a:J

    .line 130
    invoke-virtual {v0}, Lbhy;->O()Lcom/twitter/library/service/u;

    move-result-object v1

    .line 131
    invoke-virtual {v1}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    iget v0, v0, Lbhy;->b:I

    invoke-virtual {v11, v0}, Lcom/twitter/model/core/TwitterUser$a;->i(I)Lcom/twitter/model/core/TwitterUser$a;

    .line 135
    :cond_0
    iget-boolean v0, v12, Lcom/twitter/model/core/TwitterUser;->r:Z

    if-eqz v0, :cond_1

    .line 136
    invoke-static {}, Lbhu;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 137
    :cond_1
    new-instance v0, Lbhx;

    iget-object v1, p0, Lbio;->p:Landroid/content/Context;

    invoke-virtual {p0}, Lbio;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbhx;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V

    .line 138
    invoke-virtual {v0, p0}, Lbhx;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/service/s;

    move-result-object v0

    check-cast v0, Lbhx;

    .line 139
    iput-object v12, v0, Lbhx;->a:Lcom/twitter/model/core/TwitterUser;

    .line 141
    invoke-virtual {v0}, Lbhx;->O()Lcom/twitter/library/service/u;

    move-result-object v1

    .line 142
    invoke-virtual {v1}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 143
    iget-object v0, v0, Lbhx;->b:Lcom/twitter/model/profile/ExtendedProfile;

    invoke-virtual {v11, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/profile/ExtendedProfile;)Lcom/twitter/model/core/TwitterUser$a;

    .line 146
    :cond_2
    invoke-virtual {v11}, Lcom/twitter/model/core/TwitterUser$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lbio;->a:Lcom/twitter/model/core/TwitterUser;

    .line 151
    :cond_3
    :goto_0
    return-void

    .line 149
    :cond_4
    invoke-virtual/range {p3 .. p3}, Lcom/twitter/library/api/i;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/z;

    iput-object v0, p0, Lbio;->b:Lcom/twitter/model/core/z;

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 30
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbio;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/service/d$a;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 91
    invoke-virtual {p0}, Lbio;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "users"

    aput-object v3, v1, v2

    const-string/jumbo v2, "show"

    aput-object v2, v1, v4

    .line 92
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "include_media_features"

    .line 93
    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "include_user_entities"

    .line 94
    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "send_error_codes"

    .line 95
    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "include_nsfw_user_flag"

    .line 96
    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->d()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->c()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 99
    iget-wide v2, p0, Lbio;->c:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 100
    const-string/jumbo v1, "screen_name"

    iget-object v2, p0, Lbio;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 104
    :goto_0
    return-object v0

    .line 102
    :cond_0
    const-string/jumbo v1, "user_id"

    iget-wide v2, p0, Lbio;->c:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    goto :goto_0
.end method

.method protected e()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/TwitterUser$a;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    const-class v0, Lcom/twitter/model/core/TwitterUser$a;

    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lbio;->e()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
