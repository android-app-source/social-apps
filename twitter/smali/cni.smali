.class public Lcni;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static a:F

.field public static b:Z

.field private static final c:Lcqs$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcni$1;

    invoke-direct {v0}, Lcni$1;-><init>()V

    sput-object v0, Lcni;->c:Lcqs$a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()F
    .locals 2

    .prologue
    .line 115
    const/high16 v0, 0x41600000    # 14.0f

    invoke-static {}, Lcom/twitter/util/z;->d()F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method public static a(F)F
    .locals 2

    .prologue
    .line 123
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {}, Lcom/twitter/util/z;->d()F

    move-result v1

    mul-float/2addr v0, v1

    sub-float v0, p0, v0

    return v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 63
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 64
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v1

    invoke-virtual {v1}, Lcqq;->b()Lcqs;

    move-result-object v1

    .line 66
    const-string/jumbo v2, "one_time_font_size_check_completed"

    invoke-interface {v1, v2, v4}, Lcqs;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 67
    invoke-static {v0, v1}, Lcni;->a(Landroid/content/res/Resources;Lcqs;)V

    .line 72
    :cond_0
    const-string/jumbo v2, "android_system_font_5282"

    const-string/jumbo v3, "system_font_size_enabled"

    invoke-static {v2, v3}, Lcoi;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v2, "font_size_enabled_bucket_assigned"

    .line 73
    invoke-interface {v1, v2, v4}, Lcqs;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    .line 74
    invoke-interface {v1}, Lcqs;->a()Lcqs$b;

    move-result-object v2

    .line 75
    const-string/jumbo v3, "font_size_enabled_bucket_assigned"

    invoke-interface {v2, v3, v5}, Lcqs$b;->a(Ljava/lang/String;Z)Lcqs$b;

    .line 76
    const-string/jumbo v3, "font_size"

    sget v4, Lcjo$g;->default_system_font_size_enabled:I

    .line 77
    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 76
    invoke-interface {v2, v3, v4}, Lcqs$b;->a(Ljava/lang/String;Ljava/lang/String;)Lcqs$b;

    .line 78
    invoke-interface {v2}, Lcqs$b;->a()V

    .line 81
    :cond_1
    const-string/jumbo v2, "font_size"

    const-string/jumbo v3, ""

    invoke-interface {v1, v2, v3}, Lcqs;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 82
    invoke-static {v2}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 83
    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    invoke-static {}, Lcom/twitter/util/z;->d()F

    move-result v2

    mul-float/2addr v0, v2

    sput v0, Lcni;->a:F

    .line 88
    :goto_0
    const-string/jumbo v0, "sound_effects"

    invoke-interface {v1, v0, v5}, Lcqs;->a(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcni;->b:Z

    .line 89
    sget-object v0, Lcni;->c:Lcqs$a;

    invoke-interface {v1, v0}, Lcqs;->a(Lcqs$a;)V

    .line 90
    const-class v0, Lcni;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 91
    return-void

    .line 85
    :cond_2
    sget v2, Lcjo$c;->font_size_medium:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcni;->a:F

    goto :goto_0
.end method

.method private static a(Landroid/content/res/Resources;Lcqs;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 96
    const-string/jumbo v0, "font_size"

    const-string/jumbo v1, ""

    invoke-interface {p1, v0, v1}, Lcqs;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 97
    const-string/jumbo v0, "on"

    .line 99
    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 100
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-static {}, Lcom/twitter/util/z;->d()F

    move-result v2

    mul-float/2addr v1, v2

    .line 101
    sget v2, Lcjo$c;->font_size_medium:I

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 103
    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v2, 0x3f000000    # 0.5f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 104
    const-string/jumbo v0, "off"

    .line 108
    :cond_0
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-static {}, Lcox;->ax()Lcox;

    move-result-object v2

    invoke-virtual {v2}, Lcox;->ar()Lcnz;

    move-result-object v2

    invoke-virtual {v2}, Lcnz;->b()J

    move-result-wide v2

    new-array v4, v8, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "app:font_size::default:default_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 111
    invoke-interface {p1}, Lcqs;->a()Lcqs$b;

    move-result-object v0

    const-string/jumbo v1, "one_time_font_size_check_completed"

    invoke-interface {v0, v1, v8}, Lcqs$b;->a(Ljava/lang/String;Z)Lcqs$b;

    move-result-object v0

    invoke-interface {v0}, Lcqs$b;->a()V

    .line 112
    return-void
.end method

.method public static b()F
    .locals 2

    .prologue
    .line 119
    const/high16 v0, 0x41400000    # 12.0f

    invoke-static {}, Lcom/twitter/util/z;->d()F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method
