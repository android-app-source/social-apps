.class public Lbaa;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lbaa;


# instance fields
.field private final b:Landroid/content/Context;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Lcom/twitter/library/api/v;

.field private final h:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lazy;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lazz;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lbaa;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 66
    iput-object p1, p0, Lbaa;->b:Landroid/content/Context;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbaa;->i:Ljava/util/List;

    .line 68
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lbaa;
    .locals 3

    .prologue
    .line 53
    const-class v1, Lbaa;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lbaa;->a:Lbaa;

    if-nez v0, :cond_0

    .line 54
    const-class v0, Lbaa;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 55
    new-instance v0, Lbaa;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lbaa;-><init>(Landroid/content/Context;)V

    sput-object v0, Lbaa;->a:Lbaa;

    .line 57
    :cond_0
    sget-object v0, Lbaa;->a:Lbaa;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/SharedPreferences;)Lcom/twitter/library/api/v;
    .locals 2

    .prologue
    .line 78
    const-string/jumbo v0, "twitter_access_config"

    const-string/jumbo v1, ""

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 79
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    :try_start_0
    new-instance v0, Lcom/twitter/library/api/v;

    invoke-direct {v0, v1}, Lcom/twitter/library/api/v;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    :goto_0
    return-object v0

    .line 82
    :catch_0
    move-exception v0

    .line 83
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 86
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized b(Lcom/twitter/library/api/v;)V
    .locals 3

    .prologue
    .line 389
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lbaa;->g:Lcom/twitter/library/api/v;

    .line 390
    iput-object p1, p0, Lbaa;->g:Lcom/twitter/library/api/v;

    .line 391
    iget-object v0, p0, Lbaa;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazy;

    .line 392
    invoke-interface {v0, p1, v1}, Lazy;->a(Lcom/twitter/library/api/v;Lcom/twitter/library/api/v;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 389
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 394
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private n()Z
    .locals 1

    .prologue
    .line 288
    iget-boolean v0, p0, Lbaa;->c:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lbaa;->f:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lbaa;->d:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lbaa;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 97
    iget-object v0, p0, Lbaa;->j:Lazz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbaa;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lbaa;->j:Lazz;

    invoke-interface {v0}, Lazz;->a()V

    .line 102
    iget-object v0, p0, Lbaa;->b:Landroid/content/Context;

    .line 103
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 104
    const-string/jumbo v1, "data_alerts_links"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lbaa;->c:Z

    .line 105
    const-string/jumbo v1, "data_alerts_inline"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lbaa;->d:Z

    .line 106
    const-string/jumbo v1, "data_alerts_gifs"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lbaa;->e:Z

    .line 107
    const-string/jumbo v1, "data_alerts_capsule"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lbaa;->f:Z

    .line 109
    invoke-static {v0}, Lbaa;->a(Landroid/content/SharedPreferences;)Lcom/twitter/library/api/v;

    move-result-object v0

    invoke-direct {p0, v0}, Lbaa;->b(Lcom/twitter/library/api/v;)V

    .line 111
    :cond_0
    return-void
.end method

.method public declared-synchronized a(Lazy;)V
    .locals 1

    .prologue
    .line 309
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbaa;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 310
    monitor-exit p0

    return-void

    .line 309
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lazz;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lbaa;->j:Lazz;

    .line 91
    return-void
.end method

.method public a(Lcom/twitter/library/api/v;)V
    .locals 5

    .prologue
    .line 353
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 354
    if-eqz p1, :cond_0

    .line 355
    iget-object v2, p0, Lbaa;->b:Landroid/content/Context;

    const-string/jumbo v3, "config"

    const/4 v4, 0x0

    .line 356
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 358
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "twitter_access_timestamp"

    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 360
    :cond_0
    invoke-direct {p0, p1}, Lbaa;->b(Lcom/twitter/library/api/v;)V

    .line 361
    return-void
.end method

.method public a(Lcom/twitter/library/api/v;Landroid/content/SharedPreferences;)V
    .locals 3

    .prologue
    .line 298
    invoke-interface {p2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 299
    if-eqz p1, :cond_0

    .line 300
    const-string/jumbo v1, "twitter_access_config"

    invoke-virtual {p1}, Lcom/twitter/library/api/v;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 304
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 305
    invoke-direct {p0, p1}, Lbaa;->b(Lcom/twitter/library/api/v;)V

    .line 306
    return-void

    .line 302
    :cond_0
    const-string/jumbo v1, "twitter_access_config"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method public a(ZZ)V
    .locals 3

    .prologue
    .line 215
    iget-boolean v0, p0, Lbaa;->c:Z

    if-eq v0, p1, :cond_1

    .line 216
    iput-boolean p1, p0, Lbaa;->c:Z

    .line 217
    iget-object v0, p0, Lbaa;->b:Landroid/content/Context;

    .line 218
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 219
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 220
    const-string/jumbo v1, "data_alerts_links"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 221
    if-eqz p2, :cond_0

    invoke-direct {p0}, Lbaa;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 222
    const-string/jumbo v1, "data_charges_alerts"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 224
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 226
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 204
    invoke-virtual {p0}, Lbaa;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "twitter_access_android_media_forward_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 205
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ag()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 204
    :goto_0
    return v0

    .line 205
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(ZZ)V
    .locals 3

    .prologue
    .line 235
    iget-boolean v0, p0, Lbaa;->f:Z

    if-eq v0, p1, :cond_1

    .line 236
    iput-boolean p1, p0, Lbaa;->f:Z

    .line 237
    iget-object v0, p0, Lbaa;->b:Landroid/content/Context;

    .line 238
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 239
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 240
    const-string/jumbo v1, "data_alerts_capsule"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 241
    if-eqz p2, :cond_0

    invoke-direct {p0}, Lbaa;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 242
    const-string/jumbo v1, "data_charges_alerts"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 244
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 246
    :cond_1
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0}, Lbaa;->a()V

    .line 118
    const-string/jumbo v0, "twitter_access_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbaa;->g:Lcom/twitter/library/api/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbaa;->g:Lcom/twitter/library/api/v;

    iget-boolean v0, v0, Lcom/twitter/library/api/v;->d:Z

    if-eqz v0, :cond_0

    .line 120
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v0

    invoke-virtual {v0}, Lcrr;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 118
    :goto_0
    return v0

    .line 120
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    invoke-virtual {p0}, Lbaa;->a()V

    .line 129
    iget-object v0, p0, Lbaa;->g:Lcom/twitter/library/api/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbaa;->g:Lcom/twitter/library/api/v;

    iget-object v0, v0, Lcom/twitter/library/api/v;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(ZZ)V
    .locals 3

    .prologue
    .line 255
    iget-boolean v0, p0, Lbaa;->e:Z

    if-eq v0, p1, :cond_1

    .line 256
    iput-boolean p1, p0, Lbaa;->e:Z

    .line 257
    iget-object v0, p0, Lbaa;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 258
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 259
    const-string/jumbo v1, "data_alerts_gifs"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 260
    if-eqz p2, :cond_0

    invoke-direct {p0}, Lbaa;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 261
    const-string/jumbo v1, "data_charges_alerts"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 263
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 265
    :cond_1
    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p0}, Lbaa;->a()V

    .line 138
    iget-object v0, p0, Lbaa;->g:Lcom/twitter/library/api/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbaa;->g:Lcom/twitter/library/api/v;

    iget-object v0, v0, Lcom/twitter/library/api/v;->c:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(ZZ)V
    .locals 3

    .prologue
    .line 274
    iget-boolean v0, p0, Lbaa;->d:Z

    if-eq v0, p1, :cond_1

    .line 275
    iput-boolean p1, p0, Lbaa;->d:Z

    .line 276
    iget-object v0, p0, Lbaa;->b:Landroid/content/Context;

    .line 277
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 278
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 279
    const-string/jumbo v1, "data_alerts_inline"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 280
    if-eqz p2, :cond_0

    invoke-direct {p0}, Lbaa;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 281
    const-string/jumbo v1, "data_charges_alerts"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 283
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 285
    :cond_1
    return-void
.end method

.method public declared-synchronized e(ZZ)V
    .locals 2

    .prologue
    .line 317
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbaa;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazy;

    .line 318
    invoke-interface {v0, p1, p2}, Lazy;->a(ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 317
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 320
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Lbaa;->a()V

    .line 146
    iget-object v0, p0, Lbaa;->g:Lcom/twitter/library/api/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbaa;->g:Lcom/twitter/library/api/v;

    invoke-virtual {v0}, Lcom/twitter/library/api/v;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0}, Lbaa;->a()V

    .line 154
    iget-object v0, p0, Lbaa;->g:Lcom/twitter/library/api/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbaa;->g:Lcom/twitter/library/api/v;

    iget-object v0, v0, Lcom/twitter/library/api/v;->j:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0}, Lbaa;->a()V

    .line 162
    invoke-virtual {p0}, Lbaa;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbaa;->g:Lcom/twitter/library/api/v;

    iget-boolean v0, v0, Lcom/twitter/library/api/v;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbaa;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 170
    invoke-virtual {p0}, Lbaa;->a()V

    .line 171
    invoke-virtual {p0}, Lbaa;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbaa;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 178
    invoke-virtual {p0}, Lbaa;->a()V

    .line 179
    invoke-virtual {p0}, Lbaa;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbaa;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 186
    invoke-virtual {p0}, Lbaa;->a()V

    .line 187
    invoke-virtual {p0}, Lbaa;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbaa;->g:Lcom/twitter/library/api/v;

    iget-boolean v0, v0, Lcom/twitter/library/api/v;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbaa;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lbaa;->g:Lcom/twitter/library/api/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbaa;->g:Lcom/twitter/library/api/v;

    iget-boolean v0, v0, Lcom/twitter/library/api/v;->l:Z

    if-nez v0, :cond_0

    const-string/jumbo v0, "twitter_access_video_interstitial_enabled"

    .line 196
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 195
    :goto_0
    return v0

    .line 196
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()J
    .locals 4

    .prologue
    .line 367
    iget-object v0, p0, Lbaa;->b:Landroid/content/Context;

    const-string/jumbo v1, "config"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 368
    const-string/jumbo v1, "twitter_access_timestamp"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public m()Ljava/lang/String;
    .locals 3

    .prologue
    .line 373
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lbaa;->b:Landroid/content/Context;

    .line 375
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 376
    const-string/jumbo v1, "twitter_access_carrier"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 377
    const-string/jumbo v1, "twitter_access_carrier"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 380
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
