.class public Lcii;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcrc;


# direct methods
.method public constructor <init>(Lcrc;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcii;->a:Lcrc;

    .line 32
    return-void
.end method


# virtual methods
.method public a(J)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/c",
            "<",
            "Lcih;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "OperationErrorBroadcaster.error"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 49
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 50
    iget-object v1, p0, Lcii;->a:Lcrc;

    invoke-virtual {v1, v0}, Lcrc;->a(Landroid/content/IntentFilter;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcii$1;

    invoke-direct {v1, p0}, Lcii$1;-><init>(Lcii;)V

    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(JLcih;)V
    .locals 3

    .prologue
    .line 35
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 36
    const-string/jumbo v1, "OperationErrorBroadcaster.error"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 37
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 38
    const-string/jumbo v1, "extra_error"

    sget-object v2, Lcih;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1, p3, v2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 39
    iget-object v1, p0, Lcii;->a:Lcrc;

    invoke-virtual {v1, v0}, Lcrc;->a(Landroid/content/Intent;)V

    .line 40
    return-void
.end method
