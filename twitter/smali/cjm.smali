.class public Lcjm;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lcom/twitter/model/onboarding/Task;

.field public static final b:Lcom/twitter/model/onboarding/Task;

.field public static final c:Lcom/twitter/model/onboarding/Task;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 7
    new-instance v0, Lcom/twitter/model/onboarding/Task;

    const-string/jumbo v1, "task_name_entry"

    sget-object v2, Lcjl;->c:Lcom/twitter/model/onboarding/Subtask;

    .line 8
    invoke-static {v2}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/onboarding/Task;-><init>(Ljava/lang/String;Ljava/util/List;)V

    sput-object v0, Lcjm;->a:Lcom/twitter/model/onboarding/Task;

    .line 10
    new-instance v0, Lcom/twitter/model/onboarding/Task;

    const-string/jumbo v1, "task_add_email"

    sget-object v2, Lcjl;->d:Lcom/twitter/model/onboarding/Subtask;

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/twitter/model/onboarding/Subtask;

    const/4 v4, 0x0

    sget-object v5, Lcjl;->e:Lcom/twitter/model/onboarding/Subtask;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lcjl;->f:Lcom/twitter/model/onboarding/Subtask;

    aput-object v5, v3, v4

    .line 11
    invoke-static {v2, v3}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/onboarding/Task;-><init>(Ljava/lang/String;Ljava/util/List;)V

    sput-object v0, Lcjm;->b:Lcom/twitter/model/onboarding/Task;

    .line 14
    new-instance v0, Lcom/twitter/model/onboarding/Task;

    const-string/jumbo v1, "task_share_location"

    sget-object v2, Lcjl;->g:Lcom/twitter/model/onboarding/Subtask;

    .line 15
    invoke-static {v2}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/onboarding/Task;-><init>(Ljava/lang/String;Ljava/util/List;)V

    sput-object v0, Lcjm;->c:Lcom/twitter/model/onboarding/Task;

    .line 14
    return-void
.end method
