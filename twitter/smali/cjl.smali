.class public Lcjl;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lcom/twitter/model/onboarding/NavigationLink;

.field public static final b:Lcom/twitter/model/onboarding/NavigationLink;

.field public static final c:Lcom/twitter/model/onboarding/Subtask;

.field public static final d:Lcom/twitter/model/onboarding/Subtask;

.field public static final e:Lcom/twitter/model/onboarding/Subtask;

.field public static final f:Lcom/twitter/model/onboarding/Subtask;

.field public static final g:Lcom/twitter/model/onboarding/Subtask;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 22
    new-instance v0, Lcom/twitter/model/onboarding/SubtaskNavigationLink;

    const-string/jumbo v1, "interest_picker_1337"

    invoke-direct {v0, v1}, Lcom/twitter/model/onboarding/SubtaskNavigationLink;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcjl;->a:Lcom/twitter/model/onboarding/NavigationLink;

    .line 23
    new-instance v0, Lcom/twitter/model/onboarding/SubtaskNavigationLink;

    const-string/jumbo v1, "add_email123"

    invoke-direct {v0, v1}, Lcom/twitter/model/onboarding/SubtaskNavigationLink;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcjl;->b:Lcom/twitter/model/onboarding/NavigationLink;

    .line 25
    new-instance v1, Lcom/twitter/model/onboarding/subtask/NameEntrySubtask;

    const-string/jumbo v2, "name_entry_001"

    new-instance v0, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;

    invoke-direct {v0}, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;-><init>()V

    const-string/jumbo v3, "Hi. What\'s your name?"

    .line 27
    invoke-virtual {v0, v3}, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;->a(Ljava/lang/String;)Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;

    move-result-object v0

    const-string/jumbo v3, "Full name"

    .line 28
    invoke-virtual {v0, v3}, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;->b(Ljava/lang/String;)Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;

    move-result-object v0

    new-instance v3, Lcom/twitter/model/onboarding/UiLink;

    sget-object v4, Lcjk;->a:Lcom/twitter/model/onboarding/NavigationLink;

    const-string/jumbo v5, "Next"

    invoke-direct {v3, v4, v5}, Lcom/twitter/model/onboarding/UiLink;-><init>(Lcom/twitter/model/onboarding/NavigationLink;Ljava/lang/String;)V

    .line 29
    invoke-virtual {v0, v3}, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;->a(Lcom/twitter/model/onboarding/UiLink;)Lcom/twitter/util/object/i;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;

    new-instance v3, Lcom/twitter/model/onboarding/UiLink;

    sget-object v4, Lcjk;->a:Lcom/twitter/model/onboarding/NavigationLink;

    const-string/jumbo v5, "Not now"

    invoke-direct {v3, v4, v5}, Lcom/twitter/model/onboarding/UiLink;-><init>(Lcom/twitter/model/onboarding/NavigationLink;Ljava/lang/String;)V

    .line 30
    invoke-virtual {v0, v3}, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;->b(Lcom/twitter/model/onboarding/UiLink;)Lcom/twitter/util/object/i;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;

    .line 31
    invoke-virtual {v0}, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties;

    invoke-direct {v1, v2, v0}, Lcom/twitter/model/onboarding/subtask/NameEntrySubtask;-><init>(Ljava/lang/String;Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties;)V

    sput-object v1, Lcjl;->c:Lcom/twitter/model/onboarding/Subtask;

    .line 33
    new-instance v1, Lcom/twitter/model/onboarding/subtask/PasswordEntrySubtask;

    const-string/jumbo v2, "password_entry_1337"

    new-instance v0, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;

    invoke-direct {v0}, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;-><init>()V

    new-instance v3, Lcom/twitter/model/onboarding/UiLink;

    sget-object v4, Lcjl;->a:Lcom/twitter/model/onboarding/NavigationLink;

    const-string/jumbo v5, "Next"

    invoke-direct {v3, v4, v5}, Lcom/twitter/model/onboarding/UiLink;-><init>(Lcom/twitter/model/onboarding/NavigationLink;Ljava/lang/String;)V

    .line 35
    invoke-virtual {v0, v3}, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;->a(Lcom/twitter/model/onboarding/UiLink;)Lcom/twitter/util/object/i;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;

    .line 36
    invoke-virtual {v0}, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties;

    invoke-direct {v1, v2, v0}, Lcom/twitter/model/onboarding/subtask/PasswordEntrySubtask;-><init>(Ljava/lang/String;Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties;)V

    sput-object v1, Lcjl;->d:Lcom/twitter/model/onboarding/Subtask;

    .line 38
    new-instance v1, Lcom/twitter/model/onboarding/subtask/InterestPickerSubtask;

    const-string/jumbo v2, "interest_picker_1337"

    new-instance v0, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;

    invoke-direct {v0}, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;-><init>()V

    new-instance v3, Lcom/twitter/model/onboarding/UiLink;

    sget-object v4, Lcjl;->b:Lcom/twitter/model/onboarding/NavigationLink;

    const-string/jumbo v5, "Next"

    invoke-direct {v3, v4, v5}, Lcom/twitter/model/onboarding/UiLink;-><init>(Lcom/twitter/model/onboarding/NavigationLink;Ljava/lang/String;)V

    .line 40
    invoke-virtual {v0, v3}, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;->a(Lcom/twitter/model/onboarding/UiLink;)Lcom/twitter/util/object/i;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;

    .line 41
    invoke-virtual {v0}, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/onboarding/subtask/SubtaskProperties;

    invoke-direct {v1, v2, v0}, Lcom/twitter/model/onboarding/subtask/InterestPickerSubtask;-><init>(Ljava/lang/String;Lcom/twitter/model/onboarding/subtask/SubtaskProperties;)V

    sput-object v1, Lcjl;->e:Lcom/twitter/model/onboarding/Subtask;

    .line 43
    new-instance v1, Lcom/twitter/model/onboarding/subtask/AddEmailSubtask;

    const-string/jumbo v2, "add_email123"

    new-instance v0, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;

    invoke-direct {v0}, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;-><init>()V

    new-instance v3, Lcom/twitter/model/onboarding/UiLink;

    sget-object v4, Lcjk;->a:Lcom/twitter/model/onboarding/NavigationLink;

    const-string/jumbo v5, "Next"

    invoke-direct {v3, v4, v5}, Lcom/twitter/model/onboarding/UiLink;-><init>(Lcom/twitter/model/onboarding/NavigationLink;Ljava/lang/String;)V

    .line 45
    invoke-virtual {v0, v3}, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;->a(Lcom/twitter/model/onboarding/UiLink;)Lcom/twitter/util/object/i;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;

    .line 46
    invoke-virtual {v0}, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties;

    invoke-direct {v1, v2, v0}, Lcom/twitter/model/onboarding/subtask/AddEmailSubtask;-><init>(Ljava/lang/String;Lcom/twitter/model/onboarding/subtask/TextInputSubtaskProperties;)V

    sput-object v1, Lcjl;->f:Lcom/twitter/model/onboarding/Subtask;

    .line 48
    new-instance v1, Lcom/twitter/model/onboarding/subtask/ShareLocationSubtask;

    const-string/jumbo v2, "share_location_123"

    new-instance v0, Lcom/twitter/model/onboarding/subtask/SubtaskProperties$b;

    invoke-direct {v0}, Lcom/twitter/model/onboarding/subtask/SubtaskProperties$b;-><init>()V

    new-instance v3, Lcom/twitter/model/onboarding/UiLink;

    sget-object v4, Lcjk;->b:Lcom/twitter/model/onboarding/NavigationLink;

    const-string/jumbo v5, "Next"

    invoke-direct {v3, v4, v5}, Lcom/twitter/model/onboarding/UiLink;-><init>(Lcom/twitter/model/onboarding/NavigationLink;Ljava/lang/String;)V

    .line 50
    invoke-virtual {v0, v3}, Lcom/twitter/model/onboarding/subtask/SubtaskProperties$b;->a(Lcom/twitter/model/onboarding/UiLink;)Lcom/twitter/util/object/i;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/onboarding/subtask/SubtaskProperties$b;

    .line 51
    invoke-virtual {v0}, Lcom/twitter/model/onboarding/subtask/SubtaskProperties$b;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/onboarding/subtask/SubtaskProperties;

    invoke-direct {v1, v2, v0}, Lcom/twitter/model/onboarding/subtask/ShareLocationSubtask;-><init>(Ljava/lang/String;Lcom/twitter/model/onboarding/subtask/SubtaskProperties;)V

    sput-object v1, Lcjl;->g:Lcom/twitter/model/onboarding/Subtask;

    .line 48
    return-void
.end method
