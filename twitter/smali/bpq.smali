.class public Lbpq;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a()Z
    .locals 2

    .prologue
    .line 22
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 17
    const-string/jumbo v0, "tv.periscope.android"

    invoke-static {p0, v0}, Lcom/twitter/util/d;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "tv.periscope.android.beta"

    .line 18
    invoke-static {p0, v0}, Lcom/twitter/util/d;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 17
    :goto_0
    return v0

    .line 18
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/library/client/Session;)Z
    .locals 1

    .prologue
    .line 32
    invoke-static {p0}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    return v0
.end method
