.class public Laeh;
.super Laef;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laeh$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laef",
        "<",
        "Lcak;",
        "Laeh$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/View$OnClickListener;

.field private final b:Landroid/view/View$OnClickListener;

.field private final c:Lcom/twitter/android/notificationtimeline/e;


# direct methods
.method public constructor <init>(Lcom/twitter/android/notificationtimeline/a;Lcom/twitter/android/notificationtimeline/h;Lcom/twitter/android/notificationtimeline/d;Lcom/twitter/android/notificationtimeline/e;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Laef;-><init>(Lcom/twitter/android/notificationtimeline/a;Lcom/twitter/android/notificationtimeline/h;)V

    .line 53
    iput-object p4, p0, Laeh;->c:Lcom/twitter/android/notificationtimeline/e;

    .line 54
    new-instance v0, Laeh$1;

    invoke-direct {v0, p0, p3}, Laeh$1;-><init>(Laeh;Lcom/twitter/android/notificationtimeline/d;)V

    iput-object v0, p0, Laeh;->a:Landroid/view/View$OnClickListener;

    .line 67
    new-instance v0, Laeh$2;

    invoke-direct {v0, p0, p3}, Laeh$2;-><init>(Laeh;Lcom/twitter/android/notificationtimeline/d;)V

    iput-object v0, p0, Laeh;->b:Landroid/view/View$OnClickListener;

    .line 80
    return-void
.end method

.method private static a(Landroid/widget/TextView;Lcag;)V
    .locals 3

    .prologue
    .line 145
    new-instance v0, Landroid/text/SpannableStringBuilder;

    iget-object v1, p1, Lcag;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 146
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 147
    iget-object v2, p1, Lcag;->b:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 148
    iget-object v2, p1, Lcag;->b:Ljava/util/List;

    invoke-static {v2, v0, v1}, Lcom/twitter/library/widget/g;->a(Ljava/lang/Iterable;Landroid/text/SpannableStringBuilder;Landroid/content/Context;)V

    .line 150
    :cond_0
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)Laeh$a;
    .locals 4

    .prologue
    .line 85
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040111

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 86
    const v1, 0x7f1300a0

    new-instance v2, Laeh$a;

    invoke-direct {v2, v0}, Laeh$a;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 87
    iget-object v1, p0, Laeh;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    const v1, 0x7f1303a7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 91
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-static {}, Lbpi;->b()I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 92
    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    .line 94
    new-instance v1, Laeh$a;

    invoke-direct {v1, v0}, Laeh$a;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method protected bridge synthetic a(Landroid/content/Context;Lbzz;I)Lcom/twitter/analytics/model/ScribeItem;
    .locals 1

    .prologue
    .line 37
    check-cast p2, Lcak;

    invoke-virtual {p0, p1, p2, p3}, Laeh;->a(Landroid/content/Context;Lcak;I)Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/content/Context;Lcak;I)Lcom/twitter/analytics/model/ScribeItem;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p2, Lcak;->b:Lcac;

    check-cast v0, Lcaj;

    invoke-static {v0}, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;->a(Lcaj;)Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Laef$a;Lbzz;)V
    .locals 0

    .prologue
    .line 37
    check-cast p1, Laeh$a;

    check-cast p2, Lcak;

    invoke-virtual {p0, p1, p2}, Laeh;->a(Laeh$a;Lcak;)V

    return-void
.end method

.method public a(Laeh$a;Lcak;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 100
    invoke-super {p0, p1, p2}, Laef;->a(Laef$a;Lbzz;)V

    .line 101
    iget-object v0, p2, Lcak;->b:Lcac;

    check-cast v0, Lcaj;

    .line 102
    iget-object v2, p1, Laeh$a;->b:Lcml;

    iget-object v1, p2, Lcak;->b:Lcac;

    check-cast v1, Lcaj;

    iget-object v1, v1, Lcaj;->j:Ljava/util/List;

    invoke-static {v2, v1}, Laea;->a(Lcml;Ljava/util/List;)V

    .line 103
    iget-object v1, p1, Laeh$a;->a:Landroid/widget/TextView;

    iget-object v2, v0, Lcaj;->d:Lcag;

    invoke-static {v1, v2}, Laeh;->a(Landroid/widget/TextView;Lcag;)V

    .line 106
    iget-object v1, v0, Lcaj;->i:Lcan;

    if-eqz v1, :cond_0

    sget-object v1, Laec;->a:Ljava/util/Map;

    iget-object v2, v0, Lcaj;->i:Lcan;

    iget v2, v2, Lcan;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 107
    sget-object v1, Laec;->a:Ljava/util/Map;

    iget-object v2, v0, Lcaj;->i:Lcan;

    iget v2, v2, Lcan;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laed;

    .line 111
    :goto_0
    iget-object v2, p1, Laeh$a;->g:Landroid/widget/ImageView;

    invoke-static {v2, v1}, Laea;->a(Landroid/widget/ImageView;Laed;)V

    .line 113
    iget-object v1, v0, Lcaj;->k:Ljava/util/List;

    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/Tweet;

    .line 114
    if-eqz v1, :cond_1

    .line 115
    iget-object v2, p1, Laeh$a;->c:Lcml;

    invoke-virtual {v2}, Lcml;->b()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    :goto_1
    iget-object v1, v0, Lcaj;->e:Lcag;

    if-eqz v1, :cond_3

    .line 123
    iget-object v1, p1, Laeh$a;->d:Lcml;

    invoke-virtual {v1}, Lcml;->b()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, v0, Lcaj;->e:Lcag;

    invoke-static {v1, v2}, Laeh;->a(Landroid/widget/TextView;Lcag;)V

    .line 128
    :goto_2
    iget-object v1, v0, Lcaj;->n:Lcae;

    if-eqz v1, :cond_4

    iget-object v1, v0, Lcaj;->n:Lcae;

    iget-object v1, v1, Lcae;->a:Ljava/lang/String;

    const-string/jumbo v2, "CaretDropDown"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 129
    iget-object v1, p1, Laeh$a;->f:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 130
    iget-object v1, p1, Laeh$a;->e:Lcml;

    invoke-virtual {v1}, Lcml;->b()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v2, p0, Laeh;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    iget-object v1, p1, Laeh$a;->e:Lcml;

    invoke-virtual {v1}, Lcml;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, p2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 140
    :goto_3
    invoke-virtual {p1}, Laeh$a;->b()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f13000b

    invoke-virtual {v1, v2, p2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 141
    iget-object v1, p0, Laeh;->c:Lcom/twitter/android/notificationtimeline/e;

    invoke-virtual {v1, v0}, Lcom/twitter/android/notificationtimeline/e;->c(Lcaj;)V

    .line 142
    return-void

    .line 109
    :cond_0
    sget-object v1, Laec;->c:Laed;

    goto :goto_0

    .line 116
    :cond_1
    iget-object v1, v0, Lcaj;->m:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 117
    iget-object v1, p1, Laeh$a;->c:Lcml;

    invoke-virtual {v1}, Lcml;->b()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, v0, Lcaj;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 119
    :cond_2
    iget-object v1, p1, Laeh$a;->c:Lcml;

    invoke-virtual {v1, v3}, Lcml;->a(I)V

    goto :goto_1

    .line 125
    :cond_3
    iget-object v1, p1, Laeh$a;->d:Lcml;

    invoke-virtual {v1, v3}, Lcml;->a(I)V

    goto :goto_2

    .line 136
    :cond_4
    iget-object v1, p1, Laeh$a;->f:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 137
    iget-object v1, p1, Laeh$a;->e:Lcml;

    invoke-virtual {v1, v3}, Lcml;->a(I)V

    goto :goto_3
.end method

.method public bridge synthetic a(Lckb$a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 37
    check-cast p1, Laeh$a;

    check-cast p2, Lcak;

    invoke-virtual {p0, p1, p2}, Laeh;->a(Laeh$a;Lcak;)V

    return-void
.end method

.method public a(Lcak;)Z
    .locals 1

    .prologue
    .line 162
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 37
    check-cast p1, Lcak;

    invoke-virtual {p0, p1}, Laeh;->a(Lcak;)Z

    move-result v0

    return v0
.end method

.method public synthetic b(Landroid/view/ViewGroup;)Lckb$a;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Laeh;->a(Landroid/view/ViewGroup;)Laeh$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lbzz;)Z
    .locals 1

    .prologue
    .line 37
    check-cast p1, Lcak;

    invoke-virtual {p0, p1}, Laeh;->a(Lcak;)Z

    move-result v0

    return v0
.end method
