.class public Lcok;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcok$c;,
        Lcok$d;,
        Lcok$b;,
        Lcok$a;
    }
.end annotation


# direct methods
.method public static a(Ljava/lang/String;Ljava/lang/Object;)Lcok$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "TT;)",
            "Lcok$a",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 39
    invoke-static {}, Lcpq;->a()Lcpp;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcok;->a(Ljava/lang/String;Ljava/lang/Object;Lcpp;)Lcok$a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Object;Lcpp;)Lcok$a;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "TR;",
            "Lcpp",
            "<",
            "Lcom",
            "<TT;>;",
            "Lcom",
            "<TR;>;>;)",
            "Lcok$a",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 53
    new-instance v2, Lcom;

    invoke-direct {v2, p1}, Lcom;-><init>(Ljava/lang/Object;)V

    .line 54
    invoke-static {}, Lcrt;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    new-instance v0, Lcok$1;

    invoke-static {}, Lrx/c;->e()Lrx/c;

    move-result-object v1

    move-object v3, p0

    move-object v4, p2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcok$1;-><init>(Lrx/c;Lcom;Ljava/lang/String;Lcpp;Lcom;)V

    .line 73
    :goto_0
    return-object v0

    .line 64
    :cond_0
    invoke-static {p0}, Lcon;->a(Ljava/lang/String;)Lrx/c;

    move-result-object v0

    .line 65
    new-instance v1, Lcok$2;

    invoke-direct {v1, p2}, Lcok$2;-><init>(Lcpp;)V

    .line 66
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v1

    .line 73
    new-instance v0, Lcok$a;

    invoke-direct {v0, v1, v2}, Lcok$a;-><init>(Lrx/c;Lcom;)V

    goto :goto_0
.end method

.method public static a(Lcom/twitter/util/object/j;)Lcok$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcok$d;",
            ">(",
            "Lcom/twitter/util/object/j",
            "<TT;>;)",
            "Lcok$b",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 86
    invoke-static {}, Lcrt;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    new-instance v0, Lcok$3;

    invoke-direct {v0, p0}, Lcok$3;-><init>(Lcom/twitter/util/object/j;)V

    .line 95
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcok$c;

    invoke-direct {v0, p0}, Lcok$c;-><init>(Lcom/twitter/util/object/j;)V

    goto :goto_0
.end method
