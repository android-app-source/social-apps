.class public Lbva;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lbvq;

.field private final c:Lbvs;

.field private final d:Lbuq;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbvq;Lbvs;Lbuq;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lbva;->a:Landroid/content/Context;

    .line 34
    iput-object p2, p0, Lbva;->b:Lbvq;

    .line 35
    iput-object p3, p0, Lbva;->c:Lbvs;

    .line 36
    iput-object p4, p0, Lbva;->d:Lbuq;

    .line 37
    return-void
.end method

.method static synthetic a(Lbva;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lbva;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lbva;)Lbvq;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lbva;->b:Lbvq;

    return-object v0
.end method

.method static synthetic c(Lbva;)Lbuq;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lbva;->d:Lbuq;

    return-object v0
.end method


# virtual methods
.method public a(J)Lrx/g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/g",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lbva;->c:Lbvs;

    invoke-virtual {v0}, Lbvs;->a()Lrx/g;

    move-result-object v0

    new-instance v1, Lbva$2;

    invoke-direct {v1, p0, p1, p2}, Lbva$2;-><init>(Lbva;J)V

    .line 70
    invoke-virtual {v0, v1}, Lrx/g;->b(Lrx/functions/b;)Lrx/g;

    move-result-object v0

    new-instance v1, Lbva$1;

    invoke-direct {v1, p0}, Lbva$1;-><init>(Lbva;)V

    .line 76
    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/b;)Lrx/g;

    move-result-object v0

    .line 69
    return-object v0
.end method
