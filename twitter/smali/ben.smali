.class public Lben;
.super Lcom/twitter/library/service/s;
.source "Twttr"


# static fields
.field private static final a:Ljava/lang/Object;


# instance fields
.field private final b:J

.field private final c:Lcom/twitter/model/account/OAuthToken;

.field private final g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lben;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;JLjava/lang/String;Lcom/twitter/model/account/OAuthToken;)V
    .locals 2

    .prologue
    .line 37
    const-class v0, Lben;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/service/s;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 38
    iput-wide p2, p0, Lben;->b:J

    .line 39
    iput-object p5, p0, Lben;->c:Lcom/twitter/model/account/OAuthToken;

    .line 40
    iput-object p4, p0, Lben;->g:Ljava/lang/String;

    .line 41
    return-void
.end method


# virtual methods
.method protected a_(Lcom/twitter/library/service/u;)V
    .locals 15

    .prologue
    .line 47
    sget-object v10, Lben;->a:Ljava/lang/Object;

    monitor-enter v10

    .line 48
    :try_start_0
    iget-wide v2, p0, Lben;->b:J

    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v2

    .line 49
    invoke-static {}, Lcom/twitter/library/api/PromotedEvent;->a()Ljava/util/Map;

    move-result-object v11

    .line 50
    invoke-virtual {v2}, Lcom/twitter/library/provider/t;->g()Ljava/util/List;

    move-result-object v2

    .line 51
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/o;

    .line 52
    iget-object v3, v2, Lcom/twitter/library/api/o;->b:Ljava/lang/String;

    invoke-interface {v11, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/twitter/library/api/PromotedEvent;

    move-object v9, v0

    .line 53
    if-eqz v9, :cond_0

    .line 54
    new-instance v13, Lbel;

    iget-object v14, p0, Lben;->p:Landroid/content/Context;

    new-instance v3, Lcom/twitter/library/service/v;

    iget-wide v4, p0, Lben;->b:J

    iget-object v6, p0, Lben;->g:Ljava/lang/String;

    iget-object v7, p0, Lben;->c:Lcom/twitter/model/account/OAuthToken;

    const/4 v8, 0x1

    invoke-direct/range {v3 .. v8}, Lcom/twitter/library/service/v;-><init>(JLjava/lang/String;Lcom/twitter/model/account/OAuthToken;Z)V

    invoke-direct {v13, v14, v3, v9}, Lbel;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/library/api/PromotedEvent;)V

    iget-object v3, v2, Lcom/twitter/library/api/o;->a:Ljava/lang/String;

    .line 57
    invoke-virtual {v13, v3}, Lbel;->b(Ljava/lang/String;)Lbel;

    move-result-object v3

    iget-wide v4, v2, Lcom/twitter/library/api/o;->c:J

    .line 58
    invoke-virtual {v3, v4, v5}, Lbel;->a(J)Lbel;

    move-result-object v3

    iget-object v4, v2, Lcom/twitter/library/api/o;->d:Ljava/lang/String;

    .line 59
    invoke-virtual {v3, v4}, Lbel;->a(Ljava/lang/String;)Lbel;

    move-result-object v3

    iget-boolean v4, v2, Lcom/twitter/library/api/o;->e:Z

    .line 60
    invoke-virtual {v3, v4}, Lbel;->a(Z)Lbel;

    move-result-object v3

    const/4 v4, 0x1

    .line 61
    invoke-virtual {v3, v4}, Lbel;->b(Z)Lbel;

    move-result-object v3

    iget-object v4, v2, Lcom/twitter/library/api/o;->g:Ljava/lang/String;

    .line 62
    invoke-virtual {v3, v4}, Lbel;->c(Ljava/lang/String;)Lbel;

    move-result-object v3

    iget-object v4, v2, Lcom/twitter/library/api/o;->h:Ljava/lang/String;

    .line 63
    invoke-virtual {v3, v4}, Lbel;->d(Ljava/lang/String;)Lbel;

    move-result-object v3

    iget-object v4, v2, Lcom/twitter/library/api/o;->i:Ljava/lang/String;

    .line 64
    invoke-virtual {v3, v4}, Lbel;->e(Ljava/lang/String;)Lbel;

    move-result-object v3

    iget-object v4, v2, Lcom/twitter/library/api/o;->j:Ljava/lang/String;

    .line 65
    invoke-virtual {v3, v4}, Lbel;->f(Ljava/lang/String;)Lbel;

    move-result-object v3

    iget-object v4, v2, Lcom/twitter/library/api/o;->k:Ljava/lang/String;

    .line 66
    invoke-virtual {v3, v4}, Lbel;->g(Ljava/lang/String;)Lbel;

    move-result-object v3

    iget-object v4, v2, Lcom/twitter/library/api/o;->l:Ljava/lang/String;

    .line 67
    invoke-virtual {v3, v4}, Lbel;->h(Ljava/lang/String;)Lbel;

    move-result-object v3

    iget-object v4, v2, Lcom/twitter/library/api/o;->m:Ljava/lang/String;

    .line 68
    invoke-virtual {v3, v4}, Lbel;->i(Ljava/lang/String;)Lbel;

    move-result-object v3

    iget-object v2, v2, Lcom/twitter/library/api/o;->n:Ljava/lang/String;

    .line 69
    invoke-virtual {v3, v2}, Lbel;->j(Ljava/lang/String;)Lbel;

    move-result-object v2

    .line 70
    invoke-virtual {v2, p0}, Lbel;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/service/s;

    move-result-object v2

    .line 71
    invoke-virtual {v2}, Lcom/twitter/library/service/s;->O()Lcom/twitter/library/service/u;

    goto :goto_0

    .line 77
    :catchall_0
    move-exception v2

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 73
    :cond_0
    :try_start_1
    new-instance v2, Lcpb;

    new-instance v3, Ljava/lang/Exception;

    const-string/jumbo v4, "Unexpected promoted event stored"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    const-string/jumbo v3, "event"

    .line 74
    invoke-virtual {v2, v3, v9}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v2

    .line 73
    invoke-static {v2}, Lcpd;->c(Lcpb;)V

    goto/16 :goto_0

    .line 77
    :cond_1
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 78
    return-void
.end method
