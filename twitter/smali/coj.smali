.class public Lcoj;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcoj$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcol;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcoj$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lcol$a;

.field private static d:J

.field private static e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    sput-object v0, Lcoj;->a:Ljava/util/List;

    .line 25
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    sput-object v0, Lcoj;->b:Ljava/util/List;

    .line 27
    new-instance v0, Lcoj$1;

    invoke-direct {v0}, Lcoj$1;-><init>()V

    sput-object v0, Lcoj;->c:Lcol$a;

    .line 43
    const/4 v0, 0x1

    sput-boolean v0, Lcoj;->e:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(JLjava/lang/String;D)D
    .locals 1

    .prologue
    .line 176
    :try_start_0
    invoke-static {p0, p1, p2}, Lcoj;->a(JLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 177
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/twitter/util/object/k;->e(Ljava/lang/Object;)D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p3

    .line 181
    :cond_0
    :goto_0
    return-wide p3

    .line 178
    :catch_0
    move-exception v0

    .line 179
    invoke-static {p2, v0}, Lcoj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;D)D
    .locals 3

    .prologue
    .line 171
    sget-wide v0, Lcoj;->d:J

    invoke-static {v0, v1, p0, p1, p2}, Lcoj;->a(JLjava/lang/String;D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(JLjava/lang/String;F)F
    .locals 2

    .prologue
    .line 162
    :try_start_0
    invoke-static {p0, p1, p2}, Lcoj;->a(JLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 163
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/twitter/util/object/k;->d(Ljava/lang/Object;)F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p3

    .line 167
    :cond_0
    :goto_0
    return p3

    .line 164
    :catch_0
    move-exception v0

    .line 165
    invoke-static {p2, v0}, Lcoj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;F)F
    .locals 2

    .prologue
    .line 157
    sget-wide v0, Lcoj;->d:J

    invoke-static {v0, v1, p0, p1}, Lcoj;->a(JLjava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public static a(JLjava/lang/String;I)I
    .locals 2

    .prologue
    .line 134
    :try_start_0
    invoke-static {p0, p1, p2}, Lcoj;->a(JLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 135
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/twitter/util/object/k;->b(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p3

    .line 139
    :cond_0
    :goto_0
    return p3

    .line 136
    :catch_0
    move-exception v0

    .line 137
    invoke-static {p2, v0}, Lcoj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 129
    sget-wide v0, Lcoj;->d:J

    invoke-static {v0, v1, p0, p1}, Lcoj;->a(JLjava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static a()J
    .locals 2

    .prologue
    .line 81
    sget-wide v0, Lcoj;->d:J

    return-wide v0
.end method

.method public static a(JLjava/lang/String;J)J
    .locals 1

    .prologue
    .line 148
    :try_start_0
    invoke-static {p0, p1, p2}, Lcoj;->a(JLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 149
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/twitter/util/object/k;->c(Ljava/lang/Object;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p3

    .line 153
    :cond_0
    :goto_0
    return-wide p3

    .line 150
    :catch_0
    move-exception v0

    .line 151
    invoke-static {p2, v0}, Lcoj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;J)J
    .locals 3

    .prologue
    .line 143
    sget-wide v0, Lcoj;->d:J

    invoke-static {v0, v1, p0, p1, p2}, Lcoj;->a(JLjava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Object;)Lcom;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "TT;)",
            "Lcom",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 274
    invoke-static {p0}, Lcoj;->e(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 275
    if-eqz v1, :cond_0

    new-instance v0, Lcom;

    invoke-direct {v0, v1}, Lcom;-><init>(Ljava/lang/Object;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(JLjava/lang/String;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 280
    invoke-static {p0, p1, p2, p0, p1}, Lcoj;->b(JLjava/lang/String;J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(JLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 195
    :try_start_0
    invoke-static {p0, p1, p2}, Lcoj;->b(JLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 196
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/twitter/util/object/k;->f(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p3

    .line 200
    :cond_0
    :goto_0
    return-object p3

    .line 197
    :catch_0
    move-exception v0

    .line 198
    invoke-static {p2, v0}, Lcoj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 187
    sget-wide v0, Lcoj;->d:J

    invoke-static {v0, v1, p0, p1}, Lcoj;->a(JLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(JLjava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(J",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 244
    :try_start_0
    invoke-static {p0, p1, p2}, Lcoj;->a(JLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 245
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/twitter/util/object/k;->g(Ljava/lang/Object;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p3

    .line 249
    :cond_0
    :goto_0
    return-object p3

    .line 246
    :catch_0
    move-exception v0

    .line 247
    invoke-static {p2, v0}, Lcoj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 236
    sget-wide v0, Lcoj;->d:J

    invoke-static {v0, v1, p0, p1}, Lcoj;->a(JLjava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a(J)V
    .locals 2

    .prologue
    .line 85
    sget-wide v0, Lcoj;->d:J

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    .line 86
    const-class v0, Lcoj;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 87
    sput-wide p0, Lcoj;->d:J

    .line 88
    invoke-static {}, Lcoj;->d()V

    .line 90
    :cond_0
    return-void
.end method

.method public static a(Lcoj$a;)V
    .locals 1

    .prologue
    .line 71
    const-class v0, Lcoj;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 72
    sget-object v0, Lcoj;->b:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    return-void
.end method

.method public static a(Lcol;)V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcoj;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 47
    sget-object v0, Lcoj;->c:Lcol$a;

    invoke-interface {p0, v0}, Lcol;->a(Lcol$a;)V

    .line 48
    sget-object v0, Lcoj;->a:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    invoke-static {}, Lcoj;->d()V

    .line 50
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 310
    sget-boolean v0, Lcoj;->e:Z

    if-eqz v0, :cond_0

    .line 311
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Error retrieving configuration value. Key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 313
    :cond_0
    return-void
.end method

.method public static a(Z)V
    .locals 1

    .prologue
    .line 96
    const-class v0, Lcoj;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 97
    sput-boolean p0, Lcoj;->e:Z

    .line 98
    return-void
.end method

.method public static a(JLjava/lang/String;Z)Z
    .locals 2

    .prologue
    .line 110
    :try_start_0
    invoke-static {p0, p1, p2}, Lcoj;->a(JLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 111
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/twitter/util/object/k;->a(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p3

    .line 115
    :cond_0
    :goto_0
    return p3

    .line 112
    :catch_0
    move-exception v0

    .line 113
    invoke-static {p2, v0}, Lcoj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcoj;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;Z)Z
    .locals 2

    .prologue
    .line 105
    sget-wide v0, Lcoj;->d:J

    invoke-static {v0, v1, p0, p1}, Lcoj;->a(JLjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic b()J
    .locals 2

    .prologue
    .line 23
    sget-wide v0, Lcoj;->d:J

    return-wide v0
.end method

.method private static b(JLjava/lang/String;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 298
    sget-object v0, Lcoj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 299
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 300
    sget-object v0, Lcoj;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcol;

    .line 301
    invoke-interface {v0, p0, p1, p2}, Lcol;->a(JLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 302
    if-eqz v0, :cond_0

    .line 306
    :goto_1
    return-object v0

    .line 299
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 306
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static b(JLjava/lang/String;J)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 285
    sget-object v0, Lcoj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    .line 286
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v7, :cond_1

    .line 287
    sget-object v0, Lcoj;->a:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcol;

    move-wide v1, p0

    move-object v3, p2

    move-wide v4, p3

    .line 288
    invoke-interface/range {v0 .. v5}, Lcol;->a(JLjava/lang/String;J)Ljava/lang/Object;

    move-result-object v0

    .line 289
    if-eqz v0, :cond_0

    .line 293
    :goto_1
    return-object v0

    .line 286
    :cond_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 293
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(JLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 219
    :try_start_0
    invoke-static {p0, p1, p2}, Lcoj;->a(JLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 220
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/twitter/util/object/k;->f(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p3

    .line 224
    :cond_0
    :goto_0
    return-object p3

    .line 221
    :catch_0
    move-exception v0

    .line 222
    invoke-static {p2, v0}, Lcoj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    const-string/jumbo v0, ""

    invoke-static {p0, v0}, Lcoj;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 211
    sget-wide v0, Lcoj;->d:J

    invoke-static {v0, v1, p0, p1}, Lcoj;->b(JLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcoj$a;)V
    .locals 1

    .prologue
    .line 76
    const-class v0, Lcoj;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 77
    sget-object v0, Lcoj;->b:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 78
    return-void
.end method

.method public static b(Ljava/lang/String;Z)Z
    .locals 2

    .prologue
    .line 120
    :try_start_0
    sget-wide v0, Lcoj;->d:J

    invoke-static {v0, v1, p0}, Lcoj;->b(JLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 121
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/twitter/util/object/k;->a(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 125
    :cond_0
    :goto_0
    return p1

    .line 122
    :catch_0
    move-exception v0

    .line 123
    invoke-static {p0, v0}, Lcoj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 229
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, Lcoj;->a(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c()V
    .locals 0

    .prologue
    .line 23
    invoke-static {}, Lcoj;->d()V

    return-void
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 254
    sget-wide v0, Lcoj;->d:J

    invoke-static {v0, v1, p0}, Lcoj;->a(JLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static d()V
    .locals 2

    .prologue
    .line 316
    sget-object v0, Lcoj;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcoj$a;

    .line 317
    invoke-interface {v0}, Lcoj$a;->a()V

    goto :goto_0

    .line 319
    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 259
    sget-wide v0, Lcoj;->d:J

    invoke-static {v0, v1, p0}, Lcoj;->b(JLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
