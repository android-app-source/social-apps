.class Lckf;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lckd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lckd",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcke",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lckd;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lckd",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lckf;->b:Ljava/util/List;

    .line 22
    iput-object p1, p0, Lckf;->a:Lckd;

    .line 23
    return-void
.end method

.method static a(Lckb;Landroid/view/ViewGroup;ILjava/util/List;)Lckb$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lckb",
            "<TT;",
            "Lckb$a;",
            ">;",
            "Landroid/view/ViewGroup;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcke",
            "<TT;>;>;)",
            "Lckb$a;"
        }
    .end annotation

    .prologue
    .line 54
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcke;

    .line 55
    invoke-interface {v0, p0, p2}, Lcke;->a(Lckb;I)V

    goto :goto_0

    .line 57
    :cond_0
    invoke-virtual {p0, p1}, Lckb;->b(Landroid/view/ViewGroup;)Lckb$a;

    move-result-object v0

    .line 58
    const/4 v1, -0x1

    invoke-static {v0, v1}, Lckf;->a(Lckb$a;I)V

    .line 59
    return-object v0
.end method

.method private a(I)Lckb;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lckb",
            "<TT;",
            "Lckb$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lckf;->a:Lckd;

    invoke-interface {v0, p1}, Lckd;->a(I)Lckb;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lckb;

    return-object v0
.end method

.method private static a(Lckb$a;I)V
    .locals 1

    .prologue
    .line 117
    instance-of v0, p0, Lcka;

    if-eqz v0, :cond_0

    .line 118
    check-cast p0, Lcka;

    .line 119
    invoke-interface {p0, p1}, Lcka;->a(I)V

    .line 121
    :cond_0
    return-void
.end method

.method private a(Lckb$a;Lckb;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lckb$a;",
            "Lckb",
            "<TT;",
            "Lckb$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lckf;->b:Ljava/util/List;

    invoke-static {p1, p2, v0}, Lckf;->a(Lckb$a;Lckb;Ljava/util/List;)V

    .line 90
    return-void
.end method

.method static a(Lckb$a;Lckb;Ljava/lang/Object;ILjava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lckb$a;",
            "Lckb",
            "<TT;",
            "Lckb$a;",
            ">;TT;I",
            "Ljava/util/List",
            "<",
            "Lcke",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-static {p0, p3}, Lckf;->a(Lckb$a;I)V

    .line 71
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcke;

    .line 72
    invoke-interface {v0, p1, p0, p2}, Lcke;->a(Lckb;Lckb$a;Ljava/lang/Object;)V

    goto :goto_0

    .line 74
    :cond_0
    invoke-virtual {p1, p0, p2}, Lckb;->a(Lckb$a;Ljava/lang/Object;)V

    .line 75
    return-void
.end method

.method static a(Lckb$a;Lckb;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lckb$a;",
            "Lckb",
            "<TT;",
            "Lckb$a;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcke",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 95
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcke;

    .line 96
    invoke-interface {v0, p1, p0}, Lcke;->a(Lckb;Lckb$a;)V

    goto :goto_0

    .line 98
    :cond_0
    invoke-virtual {p1, p0}, Lckb;->a(Lckb$a;)V

    .line 99
    const/4 v0, -0x1

    invoke-static {p0, v0}, Lckf;->a(Lckb$a;I)V

    .line 100
    return-void
.end method

.method private c(Ljava/lang/Object;)Lckb;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lckb",
            "<TT;",
            "Lckb$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    invoke-virtual {p0, p1}, Lckf;->a(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, Lckf;->a(I)Lckb;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a()I
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lckf;->a:Lckd;

    invoke-interface {v0}, Lckd;->a()I

    move-result v0

    return v0
.end method

.method a(Ljava/lang/Object;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lckf;->a:Lckd;

    invoke-interface {v0, p1}, Lckd;->a(Ljava/lang/Object;)I

    move-result v0

    .line 31
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 32
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No view type for provided item: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_0
    return v0
.end method

.method a(Landroid/view/ViewGroup;I)Lckb$a;
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0, p2}, Lckf;->a(I)Lckb;

    move-result-object v0

    .line 48
    iget-object v1, p0, Lckf;->b:Ljava/util/List;

    invoke-static {v0, p1, p2, v1}, Lckf;->a(Lckb;Landroid/view/ViewGroup;ILjava/util/List;)Lckb$a;

    move-result-object v0

    return-object v0
.end method

.method a(Lckb$a;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lckb$a;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 78
    invoke-direct {p0, p2}, Lckf;->c(Ljava/lang/Object;)Lckb;

    move-result-object v0

    .line 79
    invoke-direct {p0, p1, v0}, Lckf;->a(Lckb$a;Lckb;)V

    .line 80
    return-void
.end method

.method a(Lckb$a;Ljava/lang/Object;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lckb$a;",
            "TT;I)V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p2}, Lckf;->c(Ljava/lang/Object;)Lckb;

    move-result-object v0

    .line 64
    iget-object v1, p0, Lckf;->b:Ljava/util/List;

    invoke-static {p1, v0, p2, p3, v1}, Lckf;->a(Lckb$a;Lckb;Ljava/lang/Object;ILjava/util/List;)V

    .line 65
    return-void
.end method

.method a(Lcke;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcke",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lckf;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    return-void
.end method

.method b(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lckf;->c(Ljava/lang/Object;)Lckb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lckb;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
