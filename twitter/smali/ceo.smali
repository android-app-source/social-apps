.class public Lceo;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lceo$a;,
        Lceo$b;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lceo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Lcom/twitter/model/moments/MomentPageType;

.field public final d:Lcom/twitter/model/moments/n;

.field public final e:Lcom/twitter/model/moments/e;

.field public final f:Lcom/twitter/model/moments/w;

.field public final g:Lcem;

.field public final h:Lcom/twitter/model/moments/l;

.field public final i:Lcom/twitter/model/moments/o;

.field public final j:Lcom/twitter/model/moments/s;

.field public final k:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lceo$b;

    invoke-direct {v0}, Lceo$b;-><init>()V

    sput-object v0, Lceo;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lceo$a;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iget-object v0, p1, Lceo$a;->a:Ljava/lang/String;

    iput-object v0, p0, Lceo;->b:Ljava/lang/String;

    .line 41
    iget-object v0, p1, Lceo$a;->b:Lcom/twitter/model/moments/MomentPageType;

    iput-object v0, p0, Lceo;->c:Lcom/twitter/model/moments/MomentPageType;

    .line 42
    iget-object v0, p1, Lceo$a;->c:Lcom/twitter/model/moments/n;

    iput-object v0, p0, Lceo;->d:Lcom/twitter/model/moments/n;

    .line 43
    iget-object v0, p1, Lceo$a;->d:Lcom/twitter/model/moments/e;

    sget-object v1, Lcom/twitter/model/moments/e;->b:Lcom/twitter/model/moments/e;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/e;

    iput-object v0, p0, Lceo;->e:Lcom/twitter/model/moments/e;

    .line 44
    iget-object v0, p1, Lceo$a;->e:Lcom/twitter/model/moments/w;

    iput-object v0, p0, Lceo;->f:Lcom/twitter/model/moments/w;

    .line 45
    iget-object v0, p1, Lceo$a;->f:Lcem;

    iput-object v0, p0, Lceo;->g:Lcem;

    .line 46
    iget-wide v0, p1, Lceo$a;->j:J

    iput-wide v0, p0, Lceo;->k:J

    .line 47
    iget-object v0, p1, Lceo$a;->g:Lcom/twitter/model/moments/l;

    iput-object v0, p0, Lceo;->h:Lcom/twitter/model/moments/l;

    .line 48
    iget-object v0, p1, Lceo$a;->h:Lcom/twitter/model/moments/o;

    iput-object v0, p0, Lceo;->i:Lcom/twitter/model/moments/o;

    .line 49
    iget-object v0, p1, Lceo$a;->i:Lcom/twitter/model/moments/s;

    iput-object v0, p0, Lceo;->j:Lcom/twitter/model/moments/s;

    .line 50
    return-void
.end method

.method synthetic constructor <init>(Lceo$a;Lceo$1;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lceo;-><init>(Lceo$a;)V

    return-void
.end method
