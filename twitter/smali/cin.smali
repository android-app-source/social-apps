.class public Lcin;
.super Lcim;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcin$a;
    }
.end annotation


# instance fields
.field private final a:J


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcim;-><init>()V

    .line 20
    iput-wide p1, p0, Lcin;->a:J

    .line 21
    return-void
.end method

.method static synthetic a(Lcin;)J
    .locals 2

    .prologue
    .line 16
    iget-wide v0, p0, Lcin;->a:J

    return-wide v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Z
    .locals 4

    .prologue
    .line 25
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/moments/r;->e:Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcin;->a:J

    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/model/moments/r;->e:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
