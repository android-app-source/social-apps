.class abstract Lcbz$b;
.super Lcbx$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcbz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcbz;",
        "B:",
        "Lcbz$a",
        "<TE;TB;>;>",
        "Lcbx$b",
        "<TE;TB;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcbx$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcbx$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 113
    check-cast p2, Lcbz$a;

    invoke-virtual {p0, p1, p2, p3}, Lcbz$b;->a(Lcom/twitter/util/serialization/n;Lcbz$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcbz$a;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/n;",
            "TB;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 126
    invoke-super {p0, p1, p2, p3}, Lcbx$b;->a(Lcom/twitter/util/serialization/n;Lcbx$a;I)V

    .line 127
    sget-object v0, Lcax;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcax;

    .line 128
    invoke-virtual {p2, v0}, Lcbz$a;->a(Lcax;)Lcbz$a;

    .line 129
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 113
    check-cast p2, Lcbz$a;

    invoke-virtual {p0, p1, p2, p3}, Lcbz$b;->a(Lcom/twitter/util/serialization/n;Lcbz$a;I)V

    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/util/serialization/o;Lcbx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    check-cast p2, Lcbz;

    invoke-virtual {p0, p1, p2}, Lcbz$b;->a(Lcom/twitter/util/serialization/o;Lcbz;)V

    return-void
.end method

.method public a(Lcom/twitter/util/serialization/o;Lcbz;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/o;",
            "TE;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 118
    invoke-super {p0, p1, p2}, Lcbx$b;->a(Lcom/twitter/util/serialization/o;Lcbx;)V

    .line 119
    iget-object v0, p2, Lcbz;->b:Lcax;

    sget-object v1, Lcax;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 120
    return-void
.end method

.method public synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    check-cast p2, Lcbz;

    invoke-virtual {p0, p1, p2}, Lcbz$b;->a(Lcom/twitter/util/serialization/o;Lcbz;)V

    return-void
.end method
