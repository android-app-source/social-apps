.class public final Laem$a$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Laem$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Laem$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/util/collection/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/i",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 176
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    iput-object v0, p0, Laem$a$a;->a:Lcom/twitter/util/collection/i;

    return-void
.end method

.method static synthetic a(Laem$a$a;)Lcom/twitter/util/collection/i;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Laem$a$a;->a:Lcom/twitter/util/collection/i;

    return-object v0
.end method


# virtual methods
.method protected T_()Z
    .locals 1

    .prologue
    .line 193
    invoke-super {p0}, Lcom/twitter/util/object/i;->T_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laem$a$a;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Laem$a$a;
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Laem$a$a;->b:Ljava/lang/String;

    .line 182
    return-object p0
.end method

.method public a(Ljava/util/Map;)Laem$a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Laem$a$a;"
        }
    .end annotation

    .prologue
    .line 187
    iget-object v0, p0, Laem$a$a;->a:Lcom/twitter/util/collection/i;

    invoke-virtual {v0, p1}, Lcom/twitter/util/collection/i;->b(Ljava/util/Map;)Lcom/twitter/util/collection/i;

    .line 188
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p0}, Laem$a$a;->e()Laem$a;

    move-result-object v0

    return-object v0
.end method

.method protected c_()V
    .locals 3

    .prologue
    .line 198
    invoke-super {p0}, Lcom/twitter/util/object/i;->c_()V

    .line 199
    iget-object v0, p0, Laem$a$a;->a:Lcom/twitter/util/collection/i;

    const-string/jumbo v1, "display_location"

    iget-object v2, p0, Laem$a$a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    .line 200
    return-void
.end method

.method protected e()Laem$a;
    .locals 2

    .prologue
    .line 205
    new-instance v0, Laem$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Laem$a;-><init>(Laem$a$a;Laem$1;)V

    return-object v0
.end method
