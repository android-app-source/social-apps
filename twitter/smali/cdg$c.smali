.class Lcdg$c;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcdg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcdg;",
        "Lcdg$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcdg$1;)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Lcdg$c;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcdg$a;
    .locals 1

    .prologue
    .line 120
    new-instance v0, Lcdg$a;

    invoke-direct {v0}, Lcdg$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcdg$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 126
    sget-object v0, Lcdd;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdd;

    .line 128
    invoke-virtual {p2, v0}, Lcdg$a;->a(Lcdd;)Lcdg$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/util/serialization/f;->j:Lcom/twitter/util/serialization/l;

    .line 129
    invoke-static {p1, v1}, Lcom/twitter/util/collection/d;->d(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcdg$a;->a(Ljava/util/Set;)Lcdg$a;

    .line 130
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 108
    check-cast p2, Lcdg$a;

    invoke-virtual {p0, p1, p2, p3}, Lcdg$c;->a(Lcom/twitter/util/serialization/n;Lcdg$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcdg;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    sget-object v0, Lcdd;->a:Lcom/twitter/util/serialization/l;

    iget-object v1, p2, Lcdg;->d:Lcdd;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    .line 114
    iget-object v0, p2, Lcdg;->e:Ljava/util/Set;

    sget-object v1, Lcom/twitter/util/serialization/f;->j:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/Set;Lcom/twitter/util/serialization/l;)V

    .line 115
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    check-cast p2, Lcdg;

    invoke-virtual {p0, p1, p2}, Lcdg$c;->a(Lcom/twitter/util/serialization/o;Lcdg;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p0}, Lcdg$c;->a()Lcdg$a;

    move-result-object v0

    return-object v0
.end method
