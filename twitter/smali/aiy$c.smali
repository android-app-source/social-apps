.class Laiy$c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Laiy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "c"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Laiv;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcgb$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;Laiv;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Laiv;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcgb$b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255
    iput-object p3, p0, Laiy$c;->c:Ljava/util/Map;

    .line 256
    iput-object p2, p0, Laiy$c;->b:Laiv;

    .line 257
    iput-object p1, p0, Laiy$c;->a:Landroid/content/Context;

    .line 258
    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 263
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Laiy$e;

    .line 264
    const v0, 0x7f1304c1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 265
    iget-object v0, p0, Laiy$c;->c:Ljava/util/Map;

    iget-object v1, v5, Laiy$e;->a:Lcgb$c;

    iget-object v1, v1, Lcgb$c;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgb$b;

    iget-object v2, v0, Lcgb$b;->c:Ljava/util/List;

    .line 266
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 268
    const/4 v1, 0x2

    if-le v0, v1, :cond_3

    .line 272
    mul-int/lit8 v1, v0, 0x2

    invoke-static {v1}, Lcom/twitter/util/collection/i;->a(I)Lcom/twitter/util/collection/i;

    move-result-object v6

    .line 274
    mul-int/lit8 v1, v0, 0x2

    invoke-static {v1}, Lcom/twitter/util/collection/i;->a(I)Lcom/twitter/util/collection/i;

    move-result-object v7

    .line 276
    new-array v1, v0, [Ljava/lang/String;

    .line 278
    const/4 v0, 0x0

    .line 279
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v0

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 280
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v3, v2

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 281
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    aput-object v2, v1, v3

    .line 282
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v6, v2, v10}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    .line 283
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    add-int/lit8 v2, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v7, v0, v3}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move v3, v2

    .line 284
    goto :goto_1

    :cond_0
    move v2, v3

    .line 285
    goto :goto_0

    .line 288
    :cond_1
    iget-object v0, p0, Laiy$c;->b:Laiv;

    iget-object v2, v5, Laiy$e;->a:Lcgb$c;

    iget-object v2, v2, Lcgb$c;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Laiv;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 289
    if-eqz v0, :cond_2

    move-object v2, v0

    .line 295
    :goto_2
    new-instance v8, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Laiy$c;->a:Landroid/content/Context;

    invoke-direct {v8, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 296
    iget-object v0, v5, Laiy$e;->a:Lcgb$c;

    iget-object v0, v0, Lcgb$c;->d:Ljava/lang/String;

    invoke-virtual {v8, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 297
    invoke-virtual {v7}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 298
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    new-instance v0, Laiy$b;

    iget-object v2, p0, Laiy$c;->b:Laiv;

    .line 301
    invoke-virtual {v6}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    iget-object v5, v5, Laiy$e;->a:Lcgb$c;

    iget-object v5, v5, Lcgb$c;->c:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Laiy$b;-><init>([Ljava/lang/String;Laiv;Ljava/util/Map;Landroid/widget/CheckBox;Ljava/lang/String;)V

    .line 297
    invoke-virtual {v8, v1, v7, v0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 304
    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 305
    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 312
    :goto_3
    return-void

    .line 292
    :cond_2
    iget-object v0, p0, Laiy$c;->b:Laiv;

    iget-object v2, v5, Laiy$e;->a:Lcgb$c;

    iget-object v2, v2, Lcgb$c;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Laiv;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_2

    .line 307
    :cond_3
    invoke-virtual {v4}, Landroid/widget/CheckBox;->toggle()V

    .line 308
    iget-object v1, p0, Laiy$c;->b:Laiv;

    iget-object v0, v5, Laiy$e;->a:Lcgb$c;

    iget-object v2, v0, Lcgb$c;->c:Ljava/lang/String;

    .line 309
    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "on"

    :goto_4
    invoke-virtual {v1, v2, v0}, Laiv;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_3

    :cond_4
    const-string/jumbo v0, "off"

    goto :goto_4
.end method
