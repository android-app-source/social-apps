.class Lapv$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lapv;->a(Lccf;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lccf;

.field final synthetic b:Lapv;


# direct methods
.method constructor <init>(Lapv;Lccf;)V
    .locals 0

    .prologue
    .line 584
    iput-object p1, p0, Lapv$3;->b:Lapv;

    iput-object p2, p0, Lapv$3;->a:Lccf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 587
    iget-object v0, p0, Lapv$3;->b:Lapv;

    iget-object v0, v0, Lapv;->g:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lapv$3;->b:Lapv;

    iget-object v2, v2, Lapv;->g:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/GalleryActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "dm"

    const/4 v3, 0x1

    .line 588
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "show_tw"

    .line 589
    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "media"

    iget-object v3, p0, Lapv$3;->a:Lccf;

    iget-object v3, v3, Lccf;->d:Lcom/twitter/model/core/MediaEntity;

    sget-object v4, Lcom/twitter/model/core/MediaEntity;->a:Lcom/twitter/util/serialization/l;

    .line 591
    invoke-static {v3, v4}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v3

    .line 590
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "source_tweet_id"

    iget-object v3, p0, Lapv$3;->a:Lccf;

    iget-object v3, v3, Lccf;->d:Lcom/twitter/model/core/MediaEntity;

    iget-wide v4, v3, Lcom/twitter/model/core/MediaEntity;->j:J

    .line 592
    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "should_show_sticker_visual_hashtags"

    .line 594
    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "association"

    iget-object v3, p0, Lapv$3;->b:Lapv;

    .line 595
    invoke-static {v3}, Lapv;->d(Lapv;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    .line 587
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 596
    return-void
.end method
