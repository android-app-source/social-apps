.class public Lbxp;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method private static a(Ljava/util/List;J)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/q;",
            ">;J)I"
        }
    .end annotation

    .prologue
    .line 103
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 104
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/q;

    iget-wide v2, v0, Lcom/twitter/model/core/q;->c:J

    cmp-long v0, p1, v2

    if-nez v0, :cond_0

    .line 108
    :goto_1
    return v1

    .line 103
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 108
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method static a(Landroid/content/res/Resources;JLjava/util/List;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "J",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/q;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 36
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v4

    .line 37
    if-lez v4, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/twitter/util/g;->b(Z)Z

    .line 39
    invoke-static {p3, p1, p2}, Lbxp;->a(Ljava/util/List;J)I

    move-result v5

    .line 40
    invoke-interface {p3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/q;

    invoke-virtual {v0}, Lcom/twitter/model/core/q;->d()Ljava/lang/String;

    move-result-object v6

    .line 42
    const/4 v0, -0x1

    if-ne v5, v0, :cond_1

    .line 43
    packed-switch v4, :pswitch_data_0

    .line 56
    sget v0, Lazw$i;->reply_context_to_someone_and_n_others:I

    add-int/lit8 v5, v4, -0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v6, v3, v2

    add-int/lit8 v2, v4, -0x1

    .line 57
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    .line 56
    invoke-virtual {p0, v0, v5, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 95
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 37
    goto :goto_0

    .line 46
    :pswitch_0
    sget v0, Lazw$k;->reply_context_to_someone:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v6, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 50
    :pswitch_1
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/q;

    invoke-virtual {v0}, Lcom/twitter/model/core/q;->d()Ljava/lang/String;

    move-result-object v0

    .line 51
    sget v4, Lazw$k;->reply_context_to_someone_and_someone:I

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v6, v3, v2

    aput-object v0, v3, v1

    invoke-virtual {p0, v4, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 60
    :cond_1
    if-nez v5, :cond_2

    .line 61
    packed-switch v4, :pswitch_data_1

    .line 74
    sget v0, Lazw$i;->reply_context_to_you_and_n_others:I

    add-int/lit8 v3, v4, -0x1

    new-array v1, v1, [Ljava/lang/Object;

    add-int/lit8 v4, v4, -0x1

    .line 75
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    .line 74
    invoke-virtual {p0, v0, v3, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 64
    :pswitch_2
    sget v0, Lazw$k;->reply_context_to_you:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 68
    :pswitch_3
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/q;

    invoke-virtual {v0}, Lcom/twitter/model/core/q;->d()Ljava/lang/String;

    move-result-object v0

    .line 69
    sget v3, Lazw$k;->reply_context_to_you_and_someone:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v2

    invoke-virtual {p0, v3, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 80
    :cond_2
    packed-switch v4, :pswitch_data_2

    .line 95
    sget v0, Lazw$i;->reply_context_to_someone_you_and_n_others:I

    add-int/lit8 v5, v4, -0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v6, v3, v2

    add-int/lit8 v2, v4, -0x2

    .line 96
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    .line 95
    invoke-virtual {p0, v0, v5, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 83
    :pswitch_4
    sget v0, Lazw$k;->reply_context_to_someone_and_you:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v6, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 89
    :pswitch_5
    if-eq v5, v1, :cond_3

    move v0, v1

    :goto_2
    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/q;

    invoke-virtual {v0}, Lcom/twitter/model/core/q;->d()Ljava/lang/String;

    move-result-object v0

    .line 90
    sget v4, Lazw$k;->reply_context_to_someone_you_and_someone:I

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v6, v3, v2

    aput-object v0, v3, v1

    invoke-virtual {p0, v4, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_3
    move v0, v3

    .line 89
    goto :goto_2

    .line 43
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 61
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 80
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
