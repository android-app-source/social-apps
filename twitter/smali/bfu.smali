.class public Lbfu;
.super Lcom/twitter/library/service/j;
.source "Twttr"


# instance fields
.field private final a:I

.field private final b:[J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;ILjava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/client/Session;",
            "I",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    const-class v0, Lbfu;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/j;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 36
    iput p3, p0, Lbfu;->a:I

    .line 37
    invoke-static {p4}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v0

    iput-object v0, p0, Lbfu;->b:[J

    .line 38
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 8

    .prologue
    .line 42
    iget-object v0, p0, Lbfu;->b:[J

    if-nez v0, :cond_0

    .line 54
    :goto_0
    return-void

    .line 45
    :cond_0
    invoke-virtual {p0}, Lbfu;->h()Lcom/twitter/library/service/v;

    move-result-object v0

    .line 46
    iget-wide v1, v0, Lcom/twitter/library/service/v;->c:J

    .line 47
    iget v3, p0, Lbfu;->a:I

    .line 48
    invoke-virtual {p0}, Lbfu;->s()Lcom/twitter/library/provider/t;

    move-result-object v0

    .line 49
    invoke-virtual {p0}, Lbfu;->t()Laut;

    move-result-object v7

    .line 52
    const-wide/16 v4, -0x1

    iget-object v6, p0, Lbfu;->b:[J

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/library/provider/t;->a(JIJ[JLaut;)V

    .line 53
    invoke-virtual {v7}, Laut;->a()V

    goto :goto_0
.end method
