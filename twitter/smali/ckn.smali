.class public Lckn;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lckq;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lclq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lckq;Lcln;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lckn;->b:Ljava/util/Set;

    .line 33
    iput-object p1, p0, Lckn;->a:Lckq;

    .line 34
    invoke-virtual {p2, p0}, Lcln;->a(Lckn;)V

    .line 35
    return-void
.end method

.method private static a(Ljava/util/Collection;Lcpv;)Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcki;",
            ">;",
            "Lcpv",
            "<",
            "Lcki;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lcki;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    sget-object v0, Lcpv;->f:Lcpv;

    if-ne p1, v0, :cond_0

    .line 76
    :goto_0
    return-object p0

    .line 65
    :cond_0
    sget-object v0, Lcpv;->g:Lcpv;

    if-ne p1, v0, :cond_1

    .line 66
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p0

    goto :goto_0

    .line 70
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 71
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcki;

    .line 72
    invoke-interface {p1, v0}, Lcpv;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 73
    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object p0, v1

    .line 76
    goto :goto_0
.end method


# virtual methods
.method public a(Lclm;)V
    .locals 4

    .prologue
    .line 47
    iget-object v0, p0, Lckn;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lclq;

    .line 48
    invoke-interface {v0, p1}, Lclq;->a(Lclm;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    iget-object v1, p0, Lckn;->a:Lckq;

    .line 52
    invoke-interface {v1}, Lckq;->a()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, p1}, Lclq;->b(Lclm;)Lcpv;

    move-result-object v3

    invoke-static {v1, v3}, Lckn;->a(Ljava/util/Collection;Lcpv;)Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    .line 53
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcki;

    .line 54
    invoke-interface {v0, p1, v1}, Lclq;->a(Lclm;Lcki;)V

    goto :goto_0

    .line 57
    :cond_1
    return-void
.end method

.method public a(Lclq;)V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lckn;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 39
    return-void
.end method
