.class public Lbkr;
.super Lbks;
.source "Twttr"


# instance fields
.field private final e:Lcom/twitter/model/core/MediaEntity;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/model/core/MediaEntity;)V
    .locals 1

    .prologue
    .line 24
    invoke-static {p2}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/MediaEntity;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lbkr;-><init>(Lcom/twitter/library/av/playback/AVDataSource;Ljava/lang/String;Lcom/twitter/model/core/MediaEntity;)V

    .line 25
    return-void
.end method

.method constructor <init>(Lcom/twitter/library/av/playback/AVDataSource;Ljava/lang/String;Lcom/twitter/model/core/MediaEntity;)V
    .locals 0

    .prologue
    .line 29
    if-eqz p2, :cond_0

    :goto_0
    invoke-direct {p0, p1, p2}, Lbks;-><init>(Lcom/twitter/library/av/playback/AVDataSource;Ljava/lang/String;)V

    .line 30
    iput-object p3, p0, Lbkr;->e:Lcom/twitter/model/core/MediaEntity;

    .line 31
    return-void

    .line 29
    :cond_0
    const-string/jumbo p2, ""

    goto :goto_0
.end method


# virtual methods
.method protected b(Lcom/twitter/network/j;Lcom/twitter/network/HttpOperation;Ljava/util/Map;Lcom/twitter/model/av/DynamicAdInfo;)Lcom/twitter/model/av/AVMediaPlaylist;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/j;",
            "Lcom/twitter/network/HttpOperation;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/twitter/model/av/DynamicAdInfo;",
            ")",
            "Lcom/twitter/model/av/AVMediaPlaylist;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 36
    iget-object v0, p0, Lbkr;->e:Lcom/twitter/model/core/MediaEntity;

    iget-wide v0, v0, Lcom/twitter/model/core/MediaEntity;->c:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 37
    new-instance v1, Lcom/twitter/model/av/Video$a;

    invoke-direct {v1}, Lcom/twitter/model/av/Video$a;-><init>()V

    invoke-virtual {v1, v0}, Lcom/twitter/model/av/Video$a;->a(Ljava/lang/String;)Lcom/twitter/model/av/Video$a;

    move-result-object v0

    const-string/jumbo v1, "video"

    .line 38
    invoke-virtual {v0, v1}, Lcom/twitter/model/av/Video$a;->b(Ljava/lang/String;)Lcom/twitter/model/av/Video$a;

    move-result-object v0

    .line 39
    invoke-virtual {v0, v2}, Lcom/twitter/model/av/Video$a;->b(Z)Lcom/twitter/model/av/Video$a;

    move-result-object v0

    .line 40
    invoke-virtual {v0, v2}, Lcom/twitter/model/av/Video$a;->d(Z)Lcom/twitter/model/av/Video$a;

    move-result-object v0

    .line 41
    invoke-virtual {p0}, Lbkr;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/av/Video$a;->c(Ljava/lang/String;)Lcom/twitter/model/av/Video$a;

    move-result-object v0

    .line 42
    invoke-virtual {v0, v2}, Lcom/twitter/model/av/Video$a;->a(Z)Lcom/twitter/model/av/Video$a;

    move-result-object v0

    iget-object v1, p0, Lbkr;->a:Lcom/twitter/library/av/playback/AVDataSource;

    .line 43
    invoke-interface {v1}, Lcom/twitter/library/av/playback/AVDataSource;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/av/Video$a;->c(Z)Lcom/twitter/model/av/Video$a;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lcom/twitter/model/av/Video$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/av/Video;

    .line 45
    new-instance v1, Lcom/twitter/model/av/MediaEntityPlaylist;

    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v2

    invoke-virtual {v2}, Lcrr;->e()Lcom/twitter/util/network/c;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/util/network/c;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v0, v3, v3}, Lcom/twitter/model/av/MediaEntityPlaylist;-><init>(Ljava/lang/String;Lcom/twitter/model/av/Video;Lcom/twitter/model/av/Video;Lcom/twitter/model/av/DynamicAdInfo;)V

    return-object v1
.end method
