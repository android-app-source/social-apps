.class public Lcbf;
.super Lcbl;
.source "Twttr"

# interfaces
.implements Lcbg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcbl",
        "<TT;>;",
        "Lcbg;"
    }
.end annotation


# static fields
.field private static final a:Lcbf;


# instance fields
.field private final b:Landroid/database/Cursor;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 12
    new-instance v0, Lcbf;

    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcbf;-><init>(Ljava/lang/Iterable;Landroid/database/Cursor;)V

    sput-object v0, Lcbf;->a:Lcbf;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Iterable;Landroid/database/Cursor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<TT;>;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    .line 24
    iput-object p2, p0, Lcbf;->b:Landroid/database/Cursor;

    .line 25
    return-void
.end method


# virtual methods
.method public a()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcbf;->b:Landroid/database/Cursor;

    return-object v0
.end method
