.class public final Lbnm;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lcom/twitter/library/network/forecaster/c;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/network/forecaster/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcrq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lbnm;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lbnm;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcom/twitter/library/network/forecaster/a;",
            ">;",
            "Lcta",
            "<",
            "Lcrq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    sget-boolean v0, Lbnm;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 25
    :cond_0
    iput-object p1, p0, Lbnm;->b:Lcta;

    .line 26
    sget-boolean v0, Lbnm;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 27
    :cond_1
    iput-object p2, p0, Lbnm;->c:Lcta;

    .line 28
    return-void
.end method

.method public static a(Lcta;Lcta;)Ldagger/internal/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcom/twitter/library/network/forecaster/a;",
            ">;",
            "Lcta",
            "<",
            "Lcrq;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Lcom/twitter/library/network/forecaster/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    new-instance v0, Lbnm;

    invoke-direct {v0, p0, p1}, Lbnm;-><init>(Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/library/network/forecaster/c;
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lbnm;->b:Lcta;

    .line 34
    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/network/forecaster/a;

    iget-object v1, p0, Lbnm;->c:Lcta;

    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcrq;

    .line 33
    invoke-static {v0, v1}, Lbnl;->a(Lcom/twitter/library/network/forecaster/a;Lcrq;)Lcom/twitter/library/network/forecaster/c;

    move-result-object v0

    const-string/jumbo v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 32
    invoke-static {v0, v1}, Ldagger/internal/e;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/network/forecaster/c;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lbnm;->a()Lcom/twitter/library/network/forecaster/c;

    move-result-object v0

    return-object v0
.end method
