.class public final Lagn;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lago;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lagn$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/base/BaseFragmentActivity;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lahz;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lage;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lagh;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laht;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/search/AdvancedSearchFiltersActivity$b;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lahn;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laib;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lagr;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lahr;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lahg;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lahe;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lahi;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lahc;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laha;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lahp;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laif;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lagn;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lagn;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lagn$a;)V
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    sget-boolean v0, Lagn;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 108
    :cond_0
    invoke-direct {p0, p1}, Lagn;->a(Lagn$a;)V

    .line 109
    return-void
.end method

.method synthetic constructor <init>(Lagn$a;Lagn$1;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lagn;-><init>(Lagn$a;)V

    return-void
.end method

.method private a(Lagn$a;)V
    .locals 10

    .prologue
    .line 118
    .line 120
    invoke-static {p1}, Lagn$a;->a(Lagn$a;)Lant;

    move-result-object v0

    invoke-static {v0}, Lanu;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 119
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->b:Lcta;

    .line 122
    iget-object v0, p0, Lagn;->b:Lcta;

    .line 124
    invoke-static {v0}, Lanv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 123
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->c:Lcta;

    .line 127
    iget-object v0, p0, Lagn;->b:Lcta;

    .line 129
    invoke-static {v0}, Lanz;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 128
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->d:Lcta;

    .line 131
    iget-object v0, p0, Lagn;->b:Lcta;

    .line 133
    invoke-static {v0}, Lanx;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 132
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->e:Lcta;

    .line 135
    iget-object v0, p0, Lagn;->d:Lcta;

    iget-object v1, p0, Lagn;->e:Lcta;

    .line 137
    invoke-static {v0, v1}, Laia;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 136
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->f:Lcta;

    .line 140
    iget-object v0, p0, Lagn;->b:Lcta;

    .line 142
    invoke-static {v0}, Lanw;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 141
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->g:Lcta;

    .line 147
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lagn;->g:Lcta;

    .line 146
    invoke-static {v0, v1}, Lagf;->a(Lcsd;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 145
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->h:Lcta;

    .line 149
    iget-object v0, p0, Lagn;->d:Lcta;

    iget-object v1, p0, Lagn;->h:Lcta;

    .line 151
    invoke-static {v0, v1}, Lagi;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 150
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->i:Lcta;

    .line 154
    new-instance v0, Lagn$1;

    invoke-direct {v0, p0, p1}, Lagn$1;-><init>(Lagn;Lagn$a;)V

    iput-object v0, p0, Lagn;->j:Lcta;

    .line 167
    iget-object v0, p0, Lagn;->j:Lcta;

    .line 168
    invoke-static {v0}, Lahu;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->k:Lcta;

    .line 170
    iget-object v0, p0, Lagn;->c:Lcta;

    .line 172
    invoke-static {v0}, Lagq;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 171
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->l:Lcta;

    .line 175
    iget-object v0, p0, Lagn;->c:Lcta;

    iget-object v1, p0, Lagn;->f:Lcta;

    iget-object v2, p0, Lagn;->i:Lcta;

    iget-object v3, p0, Lagn;->k:Lcta;

    iget-object v4, p0, Lagn;->h:Lcta;

    iget-object v5, p0, Lagn;->l:Lcta;

    .line 177
    invoke-static/range {v0 .. v5}, Laho;->a(Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 176
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->m:Lcta;

    .line 185
    iget-object v0, p0, Lagn;->e:Lcta;

    .line 187
    invoke-static {v0}, Laic;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 186
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->n:Lcta;

    .line 192
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lagn;->c:Lcta;

    iget-object v2, p0, Lagn;->j:Lcta;

    .line 191
    invoke-static {v0, v1, v2}, Lags;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 190
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->o:Lcta;

    .line 196
    iget-object v0, p0, Lagn;->o:Lcta;

    .line 197
    invoke-static {v0}, Lahs;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->p:Lcta;

    .line 199
    iget-object v0, p0, Lagn;->b:Lcta;

    iget-object v1, p0, Lagn;->j:Lcta;

    iget-object v2, p0, Lagn;->k:Lcta;

    .line 201
    invoke-static {v0, v1, v2}, Lahh;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 200
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->q:Lcta;

    .line 204
    iget-object v0, p0, Lagn;->b:Lcta;

    iget-object v1, p0, Lagn;->j:Lcta;

    iget-object v2, p0, Lagn;->k:Lcta;

    .line 206
    invoke-static {v0, v1, v2}, Lahf;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 205
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->r:Lcta;

    .line 209
    iget-object v0, p0, Lagn;->b:Lcta;

    iget-object v1, p0, Lagn;->k:Lcta;

    .line 211
    invoke-static {v0, v1}, Lahj;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 210
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->s:Lcta;

    .line 217
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lagn;->b:Lcta;

    .line 216
    invoke-static {v0, v1}, Lahd;->a(Lcsd;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 215
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->t:Lcta;

    .line 219
    iget-object v0, p0, Lagn;->l:Lcta;

    iget-object v1, p0, Lagn;->k:Lcta;

    .line 221
    invoke-static {v0, v1}, Lahb;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 220
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->u:Lcta;

    .line 224
    iget-object v0, p0, Lagn;->b:Lcta;

    iget-object v1, p0, Lagn;->n:Lcta;

    iget-object v2, p0, Lagn;->h:Lcta;

    iget-object v3, p0, Lagn;->p:Lcta;

    iget-object v4, p0, Lagn;->q:Lcta;

    iget-object v5, p0, Lagn;->r:Lcta;

    iget-object v6, p0, Lagn;->s:Lcta;

    iget-object v7, p0, Lagn;->t:Lcta;

    iget-object v8, p0, Lagn;->u:Lcta;

    iget-object v9, p0, Lagn;->j:Lcta;

    .line 226
    invoke-static/range {v0 .. v9}, Lahq;->a(Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 225
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->v:Lcta;

    .line 241
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lagn;->m:Lcta;

    iget-object v2, p0, Lagn;->v:Lcta;

    .line 240
    invoke-static {v0, v1, v2}, Laig;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 239
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagn;->w:Lcta;

    .line 244
    return-void
.end method

.method public static c()Lagn$a;
    .locals 2

    .prologue
    .line 112
    new-instance v0, Lagn$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lagn$a;-><init>(Lagn$1;)V

    return-object v0
.end method


# virtual methods
.method public synthetic a()Laog;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Lagn;->d()Laif;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 248
    invoke-static {}, Ldagger/internal/f;->a()Ldagger/internal/c;

    move-result-object v0

    invoke-interface {v0}, Ldagger/internal/c;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public d()Laif;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lagn;->w:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    return-object v0
.end method
