.class public Lbbb;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Ljava/lang/Void;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lbbb;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 59
    iput-object p3, p0, Lbbb;->b:Ljava/lang/String;

    .line 60
    iput-object p4, p0, Lbbb;->c:Ljava/lang/String;

    .line 61
    iput-object p5, p0, Lbbb;->a:Ljava/lang/String;

    .line 62
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 67
    invoke-virtual {p0}, Lbbb;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 68
    invoke-virtual {p0}, Lbbb;->L()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 97
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid/unknown action: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lbbb;->L()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :pswitch_0
    const-string/jumbo v1, "i"

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 74
    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    .line 75
    new-array v1, v2, [Ljava/lang/Object;

    const-string/jumbo v2, "account"

    aput-object v2, v1, v3

    const-string/jumbo v2, "change_password"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    const-string/jumbo v2, "current_password"

    iget-object v3, p0, Lbbb;->b:Ljava/lang/String;

    .line 77
    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    const-string/jumbo v2, "password"

    iget-object v3, p0, Lbbb;->c:Ljava/lang/String;

    .line 78
    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    const-string/jumbo v2, "password_confirmation"

    iget-object v3, p0, Lbbb;->c:Ljava/lang/String;

    .line 82
    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 101
    :goto_0
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0

    .line 87
    :pswitch_1
    const-string/jumbo v1, "1"

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 88
    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    .line 89
    new-array v1, v2, [Ljava/lang/Object;

    const-string/jumbo v2, "account"

    aput-object v2, v1, v3

    const-string/jumbo v2, "update_email"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    const-string/jumbo v2, "email"

    iget-object v3, p0, Lbbb;->a:Ljava/lang/String;

    .line 90
    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    const-string/jumbo v2, "password"

    iget-object v3, p0, Lbbb;->b:Ljava/lang/String;

    .line 92
    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto :goto_0

    .line 68
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Ljava/lang/Void;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114
    invoke-virtual {p0}, Lbbb;->L()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 116
    :pswitch_0
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 117
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/z;

    invoke-static {v0}, Lcom/twitter/model/core/z;->a(Lcom/twitter/model/core/z;)[I

    move-result-object v0

    .line 118
    array-length v1, v0

    if-lez v1, :cond_0

    .line 119
    iget-object v1, p2, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "custom_errors"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto :goto_0

    .line 114
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 21
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbbb;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Ljava/lang/Void;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lbbb;->b()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
