.class public Lbxu;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;Lcax;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/library/card/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/widget/renderablecontent/d;
    .locals 6

    .prologue
    .line 55
    invoke-static {p2}, Lcom/twitter/library/card/CardContextFactory;->a(Lcax;)Lcom/twitter/library/card/CardContext$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/card/CardContext$a;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/card/CardContext;

    move-object v0, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/card/ab;->a(Landroid/app/Activity;Lcom/twitter/library/card/CardContext;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/library/card/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/card/ab;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/app/Activity;Lcom/twitter/library/card/CardContext;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/library/card/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/widget/renderablecontent/d;
    .locals 1

    .prologue
    .line 67
    invoke-static/range {p1 .. p6}, Lcom/twitter/library/card/ab;->a(Landroid/app/Activity;Lcom/twitter/library/card/CardContext;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/library/card/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/card/ab;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/library/card/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/widget/renderablecontent/d;
    .locals 6

    .prologue
    .line 31
    invoke-static {p2}, Lcom/twitter/library/card/CardContextFactory;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/card/CardContext;

    move-result-object v1

    move-object v0, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/card/ab;->a(Landroid/app/Activity;Lcom/twitter/library/card/CardContext;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/library/card/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/card/ab;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/app/Activity;Lcom/twitter/model/dms/a;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/library/card/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/widget/renderablecontent/d;
    .locals 6

    .prologue
    .line 43
    invoke-static {p2}, Lcom/twitter/library/card/CardContextFactory;->a(Lcom/twitter/model/dms/a;)Lcom/twitter/library/card/CardContext;

    move-result-object v1

    move-object v0, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/card/ab;->a(Landroid/app/Activity;Lcom/twitter/library/card/CardContext;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/library/card/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/card/ab;

    move-result-object v0

    return-object v0
.end method
