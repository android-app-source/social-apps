.class public Lbbg;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcom/twitter/model/account/UserSettings;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Lcom/twitter/model/account/UserSettings;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Z

.field private m:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 135
    const-class v0, Lbbb;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 136
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 140
    const-class v0, Lbbb;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 141
    iput-object p3, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    .line 142
    iput-boolean p4, p0, Lbbg;->a:Z

    .line 143
    iput-object p5, p0, Lbbg;->c:Ljava/lang/String;

    .line 144
    if-eqz p5, :cond_0

    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lbbg;->g:Ljava/lang/String;

    .line 145
    return-void

    .line 144
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 167
    const-class v0, Lbbb;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 168
    iput-object p3, p0, Lbbg;->b:Ljava/lang/String;

    .line 169
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbbg;->g(I)Lcom/twitter/library/service/s;

    .line 170
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 149
    const-class v0, Lbbb;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 150
    iput-object p3, p0, Lbbg;->c:Ljava/lang/String;

    .line 151
    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbbg;->g:Ljava/lang/String;

    .line 152
    iput-object p4, p0, Lbbg;->i:Ljava/lang/String;

    .line 153
    iput-object p5, p0, Lbbg;->j:Ljava/lang/String;

    .line 154
    iput-object p6, p0, Lbbg;->k:Ljava/lang/String;

    .line 155
    iput-boolean p7, p0, Lbbg;->l:Z

    .line 156
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lbbg;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Lbbg;

    invoke-direct {v0, p0, p1}, Lbbg;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const-string/jumbo v1, "Settings fetch is never triggered by a user action."

    .line 88
    invoke-virtual {v0, v1}, Lbbg;->l(Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v0

    const/4 v1, 0x1

    .line 89
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/s;->g(I)Lcom/twitter/library/service/s;

    move-result-object v0

    check-cast v0, Lbbg;

    .line 87
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;ZLjava/lang/String;)Lbbg;
    .locals 6

    .prologue
    .line 95
    new-instance v0, Lbbg;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lbbg;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;ZLjava/lang/String;)V

    const/4 v1, 0x2

    .line 96
    invoke-virtual {v0, v1}, Lbbg;->g(I)Lcom/twitter/library/service/s;

    move-result-object v0

    check-cast v0, Lbbg;

    .line 95
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lbbg;
    .locals 8

    .prologue
    .line 110
    new-instance v0, Lbbg;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lbbg;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    const/4 v1, 0x3

    .line 112
    invoke-virtual {v0, v1}, Lbbg;->g(I)Lcom/twitter/library/service/s;

    move-result-object v0

    check-cast v0, Lbbg;

    .line 110
    return-object v0
.end method

.method private a(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;)V
    .locals 2

    .prologue
    .line 437
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    .line 438
    if-eqz v0, :cond_0

    .line 439
    iget-boolean v0, v0, Lcom/twitter/model/account/UserSettings;->C:Z

    iput-boolean v0, p2, Lcom/twitter/model/account/UserSettings;->C:Z

    .line 441
    :cond_0
    invoke-virtual {p1, p2}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/model/account/UserSettings;)V

    .line 442
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1, p2}, Lcom/twitter/library/util/b;->a(Ljava/lang/String;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V

    .line 443
    return-void
.end method

.method private a(Lcom/twitter/library/service/d$a;)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 476
    iget-object v0, p0, Lbbg;->p:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 477
    if-eqz v0, :cond_0

    .line 478
    const-string/jumbo v1, "locale"

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    const-string/jumbo v2, "lang"

    .line 479
    invoke-static {v0}, Lcom/twitter/util/b;->b(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 481
    :cond_0
    return-void
.end method

.method private a(Lcom/twitter/model/account/UserSettings;)V
    .locals 2

    .prologue
    .line 357
    invoke-virtual {p0}, Lbbg;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    .line 358
    if-eqz v0, :cond_1

    .line 359
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    iget-object v0, v0, Lcom/twitter/library/service/v;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/v;->c(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 360
    if-eqz v0, :cond_1

    .line 361
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v1

    .line 362
    if-eqz v1, :cond_0

    .line 363
    iget-boolean v1, v1, Lcom/twitter/model/account/UserSettings;->C:Z

    iput-boolean v1, p1, Lcom/twitter/model/account/UserSettings;->C:Z

    .line 365
    :cond_0
    invoke-virtual {v0, p1}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/model/account/UserSettings;)V

    .line 366
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lcom/twitter/library/util/b;->a(Ljava/lang/String;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V

    .line 369
    :cond_1
    return-void
.end method

.method private a(Lcom/twitter/model/account/UserSettings;Lcom/twitter/model/core/z;I)V
    .locals 3

    .prologue
    const/16 v2, 0xc8

    .line 402
    if-eq p3, v2, :cond_0

    const/16 v0, 0x193

    if-ne p3, v0, :cond_1

    .line 403
    :cond_0
    invoke-virtual {p0}, Lbbg;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    .line 404
    if-eqz v0, :cond_1

    .line 405
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    iget-object v0, v0, Lcom/twitter/library/service/v;->a:Ljava/lang/String;

    .line 406
    invoke-virtual {v1, v0}, Lcom/twitter/library/client/v;->c(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 407
    if-eqz v1, :cond_1

    .line 408
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    .line 409
    if-ne p3, v2, :cond_2

    .line 420
    :goto_0
    if-eqz p1, :cond_1

    .line 421
    invoke-direct {p0, v1, p1}, Lbbg;->a(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;)V

    .line 422
    invoke-static {p2}, Lcom/twitter/library/util/y;->a(Lcom/twitter/model/core/z;)Lcom/twitter/model/core/y;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbbg;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbbg;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbbg;->g:Ljava/lang/String;

    iget-object v2, p0, Lbbg;->c:Ljava/lang/String;

    .line 423
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 424
    iget-object v0, p0, Lbbg;->c:Ljava/lang/String;

    iput-object v0, p1, Lcom/twitter/model/account/UserSettings;->m:Ljava/lang/String;

    .line 425
    invoke-direct {p0, v1, p1}, Lbbg;->b(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;)V

    .line 431
    :cond_1
    return-void

    .line 415
    :cond_2
    if-eqz v0, :cond_3

    .line 417
    iget-boolean v2, p0, Lbbg;->l:Z

    iput-boolean v2, v0, Lcom/twitter/model/account/UserSettings;->j:Z

    :cond_3
    move-object p1, v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lbbg;
    .locals 8

    .prologue
    .line 118
    new-instance v0, Lbbg;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lbbg;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    const/4 v1, 0x4

    .line 120
    invoke-virtual {v0, v1}, Lbbg;->g(I)Lcom/twitter/library/service/s;

    move-result-object v0

    check-cast v0, Lbbg;

    .line 118
    return-object v0
.end method

.method private b(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;)V
    .locals 8

    .prologue
    .line 446
    iget-object v1, p2, Lcom/twitter/model/account/UserSettings;->m:Ljava/lang/String;

    .line 447
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    .line 448
    if-eqz v2, :cond_1

    .line 449
    new-instance v0, Lcom/twitter/model/core/TwitterUser$a;

    invoke-direct {v0, v2}, Lcom/twitter/model/core/TwitterUser$a;-><init>(Lcom/twitter/model/core/TwitterUser;)V

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->g(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 450
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v3

    new-instance v4, Lcnz;

    iget-wide v6, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-direct {v4, v6, v7}, Lcnz;-><init>(J)V

    invoke-virtual {v3, v4}, Lakn;->a(Lcnz;)Lakm;

    move-result-object v2

    .line 451
    if-eqz v2, :cond_0

    .line 452
    sget-object v3, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    .line 453
    invoke-static {v2, v3}, Lcom/twitter/library/util/b;->a(Lakm;Ljava/lang/String;)Z

    move-result v3

    .line 454
    invoke-static {v2, v1}, Lcom/twitter/library/util/b;->b(Lakm;Ljava/lang/String;)Lakm;

    move-result-object v2

    .line 455
    if-eqz v2, :cond_0

    .line 456
    invoke-static {v2, v0, p2}, Lcom/twitter/library/util/b;->a(Lakm;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V

    .line 457
    sget-object v4, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    invoke-static {v2, v4, v3}, Lcom/twitter/library/util/b;->a(Lakm;Ljava/lang/String;Z)V

    .line 460
    :cond_0
    sget-object v2, Lcom/twitter/util/concurrent/f;->a:Ljava/util/concurrent/Executor;

    new-instance v3, Lbbg$1;

    invoke-direct {v3, p0, p1, v0, v1}, Lbbg$1;-><init>(Lbbg;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/TwitterUser;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 468
    :cond_1
    return-void
.end method

.method private b(Lcom/twitter/model/account/UserSettings;)V
    .locals 7

    .prologue
    const/16 v6, 0x400

    .line 372
    iget-wide v0, p1, Lcom/twitter/model/account/UserSettings;->a:J

    iget-object v2, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-wide v2, v2, Lcom/twitter/model/account/UserSettings;->a:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 376
    iget-object v0, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-object v0, v0, Lcom/twitter/model/account/UserSettings;->b:Ljava/lang/String;

    iput-object v0, p1, Lcom/twitter/model/account/UserSettings;->b:Ljava/lang/String;

    .line 380
    :cond_0
    invoke-virtual {p0}, Lbbg;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    .line 381
    iget-object v1, p1, Lcom/twitter/model/account/UserSettings;->n:Ljava/lang/String;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 382
    invoke-virtual {p0}, Lbbg;->R()Lcom/twitter/library/provider/t;

    move-result-object v1

    .line 383
    invoke-virtual {p0}, Lbbg;->S()Laut;

    move-result-object v2

    .line 384
    iget-object v3, p1, Lcom/twitter/model/account/UserSettings;->n:Ljava/lang/String;

    const-string/jumbo v4, "none"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 385
    iget-wide v4, v0, Lcom/twitter/library/service/v;->c:J

    invoke-virtual {v1, v4, v5, v6, v2}, Lcom/twitter/library/provider/t;->b(JILaut;)V

    .line 390
    :cond_1
    :goto_0
    invoke-direct {p0, p1}, Lbbg;->a(Lcom/twitter/model/account/UserSettings;)V

    .line 392
    iget-object v1, p1, Lcom/twitter/model/account/UserSettings;->m:Ljava/lang/String;

    .line 393
    iget-object v2, p0, Lbbg;->g:Ljava/lang/String;

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    iget-object v2, p0, Lbbg;->g:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz v0, :cond_2

    .line 395
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    iget-object v0, v0, Lcom/twitter/library/service/v;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/v;->c(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    .line 396
    invoke-direct {p0, v0, p1}, Lbbg;->b(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;)V

    .line 398
    :cond_2
    return-void

    .line 387
    :cond_3
    iget-wide v4, v0, Lcom/twitter/library/service/v;->c:J

    invoke-virtual {v1, v4, v5, v6, v2}, Lcom/twitter/library/provider/t;->a(JILaut;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Z)Lbbg;
    .locals 0

    .prologue
    .line 129
    iput-boolean p1, p0, Lbbg;->m:Z

    .line 130
    return-object p0
.end method

.method protected a()Lcom/twitter/library/service/d;
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 175
    invoke-virtual {p0}, Lbbg;->J()Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 176
    invoke-virtual {p0}, Lbbg;->L()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 289
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid/unknown action: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lbbg;->L()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :pswitch_0
    new-array v0, v2, [Ljava/lang/Object;

    const-string/jumbo v2, "account"

    aput-object v2, v0, v3

    const-string/jumbo v2, "settings"

    aput-object v2, v0, v4

    invoke-virtual {v1, v0}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "include_alt_text_compose"

    .line 179
    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "include_ranked_timeline"

    .line 180
    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "include_mention_filter"

    .line 181
    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "include_universal_quality_filtering"

    .line 182
    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 183
    invoke-direct {p0, v1}, Lbbg;->a(Lcom/twitter/library/service/d$a;)V

    .line 292
    :cond_0
    :goto_0
    invoke-virtual {v1}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0

    .line 187
    :pswitch_1
    sget-object v0, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    invoke-virtual {v1, v0}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    .line 188
    new-array v0, v2, [Ljava/lang/Object;

    const-string/jumbo v2, "account"

    aput-object v2, v0, v3

    const-string/jumbo v2, "settings"

    aput-object v2, v0, v4

    invoke-virtual {v1, v0}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "include_alt_text_compose"

    .line 189
    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "include_ranked_timeline"

    .line 190
    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 191
    invoke-direct {p0, v1}, Lbbg;->a(Lcom/twitter/library/service/d$a;)V

    .line 193
    iget-object v0, p0, Lbbg;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 194
    iget-object v0, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-object v2, p0, Lbbg;->c:Ljava/lang/String;

    iput-object v2, v0, Lcom/twitter/model/account/UserSettings;->m:Ljava/lang/String;

    .line 195
    const-string/jumbo v0, "old_screen_name"

    iget-object v2, p0, Lbbg;->g:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 198
    :cond_1
    const-string/jumbo v0, "geo_enabled"

    iget-object v2, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-boolean v2, v2, Lcom/twitter/model/account/UserSettings;->c:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "protected"

    iget-object v3, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-boolean v3, v3, Lcom/twitter/model/account/UserSettings;->j:Z

    .line 199
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "discoverable_by_email"

    iget-object v3, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-boolean v3, v3, Lcom/twitter/model/account/UserSettings;->i:Z

    .line 201
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    .line 200
    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "discoverable_by_mobile_phone"

    iget-object v3, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-boolean v3, v3, Lcom/twitter/model/account/UserSettings;->l:Z

    .line 203
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    .line 202
    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "display_sensitive_media"

    iget-object v3, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-boolean v3, v3, Lcom/twitter/model/account/UserSettings;->k:Z

    .line 205
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    .line 204
    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "screen_name"

    iget-object v3, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-object v3, v3, Lcom/twitter/model/account/UserSettings;->m:Ljava/lang/String;

    .line 206
    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "email_follow_enabled"

    iget-object v3, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-boolean v3, v3, Lcom/twitter/model/account/UserSettings;->o:Z

    .line 208
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    .line 207
    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "allow_ads_personalization"

    iget-object v3, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-boolean v3, v3, Lcom/twitter/model/account/UserSettings;->q:Z

    .line 210
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    .line 209
    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "sleep_time_enabled"

    iget-object v3, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-boolean v3, v3, Lcom/twitter/model/account/UserSettings;->e:Z

    .line 211
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v2

    const-string/jumbo v3, "smart_mute"

    iget-object v0, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-boolean v0, v0, Lcom/twitter/model/account/UserSettings;->t:Z

    if-eqz v0, :cond_6

    const-string/jumbo v0, "enabled"

    .line 212
    :goto_1
    invoke-virtual {v2, v3, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "alt_text_compose_enabled"

    iget-object v3, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-boolean v3, v3, Lcom/twitter/model/account/UserSettings;->r:Z

    .line 214
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    .line 213
    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "dm_receipt_setting"

    iget-object v3, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-object v3, v3, Lcom/twitter/model/account/UserSettings;->y:Ljava/lang/String;

    .line 215
    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "universal_quality_filtering"

    iget-object v3, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-object v3, v3, Lcom/twitter/model/account/UserSettings;->z:Ljava/lang/String;

    .line 216
    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "mention_filter"

    iget-object v3, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-object v3, v3, Lcom/twitter/model/account/UserSettings;->A:Ljava/lang/String;

    .line 217
    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "include_mention_filter"

    .line 218
    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "include_universal_quality_filtering"

    .line 219
    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "allow_authenticated_periscope_requests"

    iget-object v3, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-boolean v3, v3, Lcom/twitter/model/account/UserSettings;->B:Z

    .line 221
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    .line 220
    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 223
    iget-boolean v0, p0, Lbbg;->m:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    invoke-virtual {v0}, Lcom/twitter/model/account/UserSettings;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 224
    const-string/jumbo v0, "ranked_timeline_setting"

    iget-object v2, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget v2, v2, Lcom/twitter/model/account/UserSettings;->v:I

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    .line 227
    :cond_2
    iget-object v0, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-boolean v0, v0, Lcom/twitter/model/account/UserSettings;->e:Z

    if-eqz v0, :cond_3

    .line 228
    const-string/jumbo v0, "start_sleep_time"

    iget-object v2, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    invoke-virtual {v2}, Lcom/twitter/model/account/UserSettings;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "end_sleep_time"

    iget-object v3, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    .line 229
    invoke-virtual {v3}, Lcom/twitter/model/account/UserSettings;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "time_zone"

    .line 230
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 233
    :cond_3
    iget-boolean v0, p0, Lbbg;->a:Z

    if-eqz v0, :cond_4

    .line 234
    iget-object v0, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-boolean v0, v0, Lcom/twitter/model/account/UserSettings;->C:Z

    if-eqz v0, :cond_7

    .line 235
    const-string/jumbo v0, "personalized_trends"

    const-string/jumbo v2, "true"

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 242
    :cond_4
    :goto_2
    iget-object v0, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-object v0, v0, Lcom/twitter/model/account/UserSettings;->n:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 243
    const-string/jumbo v0, "allow_media_tagging"

    iget-object v2, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-object v2, v2, Lcom/twitter/model/account/UserSettings;->n:Ljava/lang/String;

    .line 244
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 243
    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 247
    :cond_5
    iget-object v0, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-object v0, v0, Lcom/twitter/model/account/UserSettings;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 248
    const-string/jumbo v0, "allow_dms_from"

    iget-object v2, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-object v2, v2, Lcom/twitter/model/account/UserSettings;->s:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto/16 :goto_0

    .line 211
    :cond_6
    const-string/jumbo v0, "disabled"

    goto/16 :goto_1

    .line 237
    :cond_7
    const-string/jumbo v0, "trend_location_woeid"

    iget-object v2, p0, Lbbg;->h:Lcom/twitter/model/account/UserSettings;

    iget-wide v2, v2, Lcom/twitter/model/account/UserSettings;->a:J

    .line 238
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 237
    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto :goto_2

    .line 253
    :pswitch_2
    sget-object v0, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    invoke-virtual {v1, v0}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    .line 254
    new-array v0, v2, [Ljava/lang/Object;

    const-string/jumbo v2, "account"

    aput-object v2, v0, v3

    const-string/jumbo v2, "settings"

    aput-object v2, v0, v4

    invoke-virtual {v1, v0}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    .line 255
    invoke-direct {p0, v1}, Lbbg;->a(Lcom/twitter/library/service/d$a;)V

    .line 257
    const-string/jumbo v0, "protected"

    iget-boolean v2, p0, Lbbg;->l:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 259
    iget-object v0, p0, Lbbg;->c:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 260
    const-string/jumbo v0, "screen_name"

    iget-object v2, p0, Lbbg;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 262
    :cond_8
    iget-object v0, p0, Lbbg;->k:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 263
    const-string/jumbo v0, "email"

    iget-object v2, p0, Lbbg;->k:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 265
    :cond_9
    iget-object v0, p0, Lbbg;->i:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 266
    const-string/jumbo v0, "current_password"

    iget-object v2, p0, Lbbg;->i:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 268
    :cond_a
    iget-object v0, p0, Lbbg;->j:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 269
    const-string/jumbo v0, "new_password"

    iget-object v2, p0, Lbbg;->j:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 270
    const-string/jumbo v0, "password_confirmation"

    iget-object v2, p0, Lbbg;->j:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 272
    :cond_b
    iget-object v0, p0, Lbbg;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 273
    const-string/jumbo v0, "country_code"

    iget-object v2, p0, Lbbg;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto/16 :goto_0

    .line 278
    :pswitch_3
    sget-object v0, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    invoke-virtual {v1, v0}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    .line 279
    new-array v0, v2, [Ljava/lang/Object;

    const-string/jumbo v2, "account"

    aput-object v2, v0, v3

    const-string/jumbo v2, "resend_confirmation_email"

    aput-object v2, v0, v4

    invoke-virtual {v1, v0}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    .line 280
    invoke-direct {p0, v1}, Lbbg;->a(Lcom/twitter/library/service/d$a;)V

    .line 281
    const-string/jumbo v0, "protected"

    iget-boolean v2, p0, Lbbg;->l:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 282
    iget-object v0, p0, Lbbg;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 283
    const-string/jumbo v0, "email"

    iget-object v2, p0, Lbbg;->k:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto/16 :goto_0

    .line 176
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/account/UserSettings;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 304
    invoke-virtual {p0}, Lbbg;->L()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 354
    :cond_0
    :goto_0
    return-void

    .line 306
    :pswitch_0
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 307
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/UserSettings;

    .line 308
    if-eqz v0, :cond_0

    .line 309
    iget-object v1, p2, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "SETTINGS"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 310
    invoke-direct {p0, v0}, Lbbg;->a(Lcom/twitter/model/account/UserSettings;)V

    goto :goto_0

    .line 316
    :pswitch_1
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/UserSettings;

    .line 318
    if-eqz v0, :cond_0

    .line 319
    iget-object v1, p2, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "SETTINGS"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 320
    iget-object v1, p0, Lbbg;->g:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 321
    iget-object v1, p2, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "OLD_SCREEN_NAME"

    iget-object v3, p0, Lbbg;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    :cond_1
    invoke-direct {p0, v0}, Lbbg;->b(Lcom/twitter/model/account/UserSettings;)V

    goto :goto_0

    .line 333
    :pswitch_2
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 334
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/UserSettings;

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    .line 340
    :goto_1
    invoke-virtual {p2}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v2

    .line 341
    if-eqz v2, :cond_3

    .line 342
    iget v2, v2, Lcom/twitter/network/l;->a:I

    .line 346
    :goto_2
    invoke-direct {p0, v1, v0, v2}, Lbbg;->a(Lcom/twitter/model/account/UserSettings;Lcom/twitter/model/core/z;I)V

    goto :goto_0

    .line 336
    :cond_2
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/z;

    .line 337
    iget-object v2, p2, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "CUSTOM_ERRORS"

    sget-object v4, Lcom/twitter/model/core/z;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v2, v3, v0, v4}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    goto :goto_1

    .line 344
    :cond_3
    invoke-virtual {p2}, Lcom/twitter/library/service/u;->d()I

    move-result v2

    goto :goto_2

    .line 304
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 40
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbbg;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/account/UserSettings;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 298
    const-class v0, Lcom/twitter/model/account/UserSettings;

    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lbbg;->b()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
