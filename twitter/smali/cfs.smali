.class public Lcfs;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcfs$b;,
        Lcfs$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcfs;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/util/List",
            "<",
            "Lcfs;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcfs$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcfs$b;-><init>(Lcfs$1;)V

    sput-object v0, Lcfs;->a:Lcom/twitter/util/serialization/l;

    .line 23
    sget-object v0, Lcfs;->a:Lcom/twitter/util/serialization/l;

    .line 24
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    sput-object v0, Lcfs;->b:Lcom/twitter/util/serialization/l;

    .line 23
    return-void
.end method

.method private constructor <init>(Lcfs$a;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lcfs$a;->a(Lcfs$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfs;->c:Ljava/lang/String;

    .line 33
    invoke-static {p1}, Lcfs$a;->b(Lcfs$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfs;->d:Ljava/lang/String;

    .line 34
    return-void
.end method

.method synthetic constructor <init>(Lcfs$a;Lcfs$1;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcfs;-><init>(Lcfs$a;)V

    return-void
.end method
