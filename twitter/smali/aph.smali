.class public Laph;
.super Lcom/twitter/util/android/d;
.source "Twttr"


# instance fields
.field private final a:J

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;JLjava/lang/String;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 25
    sget-object v0, Lcom/twitter/database/schema/a$k;->a:Landroid/net/Uri;

    .line 27
    invoke-static {v0, p4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 26
    invoke-static {v0, p2, p3}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lawz;->a:[Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v5, v4

    move-object v6, v4

    .line 25
    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    iput-wide p2, p0, Laph;->a:J

    .line 30
    iput-object p4, p0, Laph;->b:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public a()Lbsy;
    .locals 6

    .prologue
    .line 36
    new-instance v1, Lbsy;

    .line 37
    invoke-super {p0}, Lcom/twitter/util/android/d;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    new-instance v2, Lbta;

    iget-wide v4, p0, Laph;->a:J

    .line 38
    invoke-static {v4, v5}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v3

    invoke-direct {v2, v3}, Lbta;-><init>(Lcom/twitter/database/model/i;)V

    iget-object v3, p0, Laph;->b:Ljava/lang/String;

    iget-wide v4, p0, Laph;->a:J

    .line 39
    invoke-virtual {v2, v3, v4, v5}, Lbta;->a(Ljava/lang/String;J)Ljava/util/Map;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lbsy;-><init>(Landroid/database/Cursor;Ljava/util/Map;)V

    .line 36
    return-object v1
.end method

.method public synthetic loadInBackground()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Laph;->a()Lbsy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Laph;->a()Lbsy;

    move-result-object v0

    return-object v0
.end method
