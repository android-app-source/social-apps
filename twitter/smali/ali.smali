.class public Lali;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/app/Application;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lali;->a:Landroid/app/Application;

    .line 33
    return-void
.end method

.method static a(Landroid/content/Context;)Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/app/Application;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static b(Landroid/content/Context;)Landroid/accounts/AccountManager;
    .locals 1

    .prologue
    .line 76
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    return-object v0
.end method

.method static b()Landroid/os/Handler;
    .locals 2

    .prologue
    .line 69
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-object v0
.end method

.method static c(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;
    .locals 1

    .prologue
    .line 83
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a()Landroid/app/Application;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lali;->a:Landroid/app/Application;

    return-object v0
.end method
