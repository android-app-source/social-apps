.class public Laii;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laii$b;,
        Laii$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Laii;",
            "Laii$a;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Laii;


# instance fields
.field private final c:Z

.field private final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Laii$b;

    invoke-direct {v0}, Laii$b;-><init>()V

    sput-object v0, Laii;->a:Lcom/twitter/util/serialization/b;

    .line 20
    new-instance v0, Laii$a;

    invoke-direct {v0}, Laii$a;-><init>()V

    invoke-virtual {v0}, Laii$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laii;

    sput-object v0, Laii;->b:Laii;

    return-void
.end method

.method constructor <init>(Laii$a;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {p1}, Laii$a;->a(Laii$a;)Z

    move-result v0

    iput-boolean v0, p0, Laii;->c:Z

    .line 27
    invoke-static {p1}, Laii$a;->b(Laii$a;)Z

    move-result v0

    iput-boolean v0, p0, Laii;->d:Z

    .line 28
    return-void
.end method

.method static synthetic a(Laii;)Z
    .locals 1

    .prologue
    .line 14
    iget-boolean v0, p0, Laii;->d:Z

    return v0
.end method

.method static synthetic b(Laii;)Z
    .locals 1

    .prologue
    .line 14
    iget-boolean v0, p0, Laii;->c:Z

    return v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Laii;->c:Z

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Laii;->d:Z

    return v0
.end method
