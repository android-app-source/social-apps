.class public Lbeh;
.super Lbef;
.source "Twttr"


# instance fields
.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lbed;Ljava/lang/String;Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/client/Session;",
            "Lbed;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    const-class v0, Lbeh;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lbef;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;Lbed;Ljava/lang/String;)V

    .line 32
    iput-object p5, p0, Lbeh;->b:Ljava/util/Map;

    .line 33
    return-void
.end method

.method private a(Ljava/util/Map;)Lcom/twitter/model/json/notifications/JsonSettingsNewStatePushRequest;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/twitter/model/json/notifications/JsonSettingsNewStatePushRequest;"
        }
    .end annotation

    .prologue
    .line 49
    new-instance v0, Lcom/twitter/model/json/notifications/JsonSettingsNewStatePushRequest;

    invoke-direct {v0}, Lcom/twitter/model/json/notifications/JsonSettingsNewStatePushRequest;-><init>()V

    .line 50
    new-instance v1, Lcom/twitter/model/json/notifications/JsonSettingsNewStatePushRequest$JsonSettings;

    invoke-direct {v1}, Lcom/twitter/model/json/notifications/JsonSettingsNewStatePushRequest$JsonSettings;-><init>()V

    iput-object v1, v0, Lcom/twitter/model/json/notifications/JsonSettingsNewStatePushRequest;->a:Lcom/twitter/model/json/notifications/JsonSettingsNewStatePushRequest$JsonSettings;

    .line 51
    iget-object v1, v0, Lcom/twitter/model/json/notifications/JsonSettingsNewStatePushRequest;->a:Lcom/twitter/model/json/notifications/JsonSettingsNewStatePushRequest$JsonSettings;

    new-instance v2, Lcom/twitter/model/json/notifications/JsonSettingsNewStatePushRequest$JsonSettingsValues;

    invoke-direct {v2}, Lcom/twitter/model/json/notifications/JsonSettingsNewStatePushRequest$JsonSettingsValues;-><init>()V

    iput-object v2, v1, Lcom/twitter/model/json/notifications/JsonSettingsNewStatePushRequest$JsonSettings;->a:Lcom/twitter/model/json/notifications/JsonSettingsNewStatePushRequest$JsonSettingsValues;

    .line 53
    iget-object v1, v0, Lcom/twitter/model/json/notifications/JsonSettingsNewStatePushRequest;->a:Lcom/twitter/model/json/notifications/JsonSettingsNewStatePushRequest$JsonSettings;

    iget-object v1, v1, Lcom/twitter/model/json/notifications/JsonSettingsNewStatePushRequest$JsonSettings;->a:Lcom/twitter/model/json/notifications/JsonSettingsNewStatePushRequest$JsonSettingsValues;

    iput-object p1, v1, Lcom/twitter/model/json/notifications/JsonSettingsNewStatePushRequest$JsonSettingsValues;->a:Ljava/util/Map;

    .line 54
    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/library/service/d$a;)Lcom/twitter/library/service/d$a;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string/jumbo v2, "mobile"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "settings"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "save"

    aput-object v2, v0, v1

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    .line 42
    sget-object v0, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    .line 43
    const-string/jumbo v0, "new_state"

    iget-object v1, p0, Lbeh;->b:Ljava/util/Map;

    .line 44
    invoke-direct {p0, v1}, Lbeh;->a(Ljava/util/Map;)Lcom/twitter/model/json/notifications/JsonSettingsNewStatePushRequest;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/model/json/common/g;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 43
    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 45
    return-object p1
.end method
