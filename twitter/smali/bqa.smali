.class public Lbqa;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:J

.field private static b:Lbqb;

.field private static c:Laua;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()V
    .locals 2

    .prologue
    .line 117
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Lbqa;->a(J)V

    .line 118
    return-void
.end method

.method public static declared-synchronized a(J)V
    .locals 2

    .prologue
    .line 121
    const-class v1, Lbqa;

    monitor-enter v1

    :try_start_0
    const-class v0, Lbqa;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 122
    sput-wide p0, Lbqa;->a:J

    .line 123
    invoke-static {p0, p1}, Lcoj;->a(J)V

    .line 124
    sget-object v0, Lbqa;->b:Lbqb;

    if-eqz v0, :cond_0

    .line 125
    sget-object v0, Lbqa;->b:Lbqb;

    invoke-virtual {v0, p0, p1}, Lbqb;->b(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    :cond_0
    monitor-exit v1

    return-void

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(JLcdf;Lcdg;)V
    .locals 2

    .prologue
    .line 192
    invoke-static {}, Lbqa;->g()Lbqb;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2, p3}, Lbqb;->a(JLcdf;Lcdg;)V

    .line 193
    return-void
.end method

.method public static a(JLcdg;)V
    .locals 2

    .prologue
    .line 186
    invoke-static {}, Lbqa;->e()Lcdf;

    move-result-object v0

    invoke-static {p0, p1, v0, p2}, Lbqa;->a(JLcdf;Lcdg;)V

    .line 187
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 56
    new-instance v0, Lbqd;

    sget-object v1, Lbqc;->a:Ljava/util/Map;

    invoke-direct {v0, v1}, Lbqd;-><init>(Ljava/util/Map;)V

    invoke-static {v0}, Lcoj;->a(Lcol;)V

    .line 58
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    sget-object v0, Lbqa;->c:Laua;

    if-nez v0, :cond_0

    .line 60
    const-class v0, Lbqa;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 61
    new-instance v0, Laua;

    invoke-direct {v0, p0}, Laua;-><init>(Landroid/content/Context;)V

    sput-object v0, Lbqa;->c:Laua;

    .line 62
    sget-object v0, Lbqa;->c:Laua;

    invoke-static {v0}, Lcoj;->a(Lcol;)V

    .line 65
    :cond_0
    :try_start_0
    sget v0, Lazw$j;->feature_switch_overrides:I

    .line 66
    invoke-static {p0, v0}, Lauc;->a(Landroid/content/Context;I)Lcdd;

    move-result-object v0

    .line 67
    new-instance v1, Laub;

    invoke-direct {v1, v0}, Laub;-><init>(Lcdd;)V

    invoke-static {v1}, Lcoj;->a(Lcol;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    :cond_1
    :goto_0
    sget-object v0, Lbqa;->b:Lbqb;

    if-nez v0, :cond_2

    .line 73
    new-instance v0, Lbqb;

    invoke-direct {v0, p0}, Lbqb;-><init>(Landroid/content/Context;)V

    .line 74
    invoke-static {v0}, Lcoj;->a(Lcol;)V

    .line 75
    invoke-static {v0}, Lbqa;->a(Lbqb;)V

    .line 77
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 78
    const/4 v0, 0x0

    invoke-static {v0}, Lcoj;->a(Z)V

    .line 79
    invoke-static {}, Lbqa;->h()Z

    move-result v0

    .line 80
    invoke-static {}, Lcon;->a()Lrx/c;

    move-result-object v1

    new-instance v2, Lbqa$1;

    invoke-direct {v2, v0}, Lbqa$1;-><init>(Z)V

    .line 81
    invoke-virtual {v1, v2}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 90
    :cond_2
    return-void

    .line 68
    :catch_0
    move-exception v0

    .line 69
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;J)V
    .locals 3

    .prologue
    .line 160
    invoke-static {p1, p2}, Lbqe;->a(J)V

    .line 161
    invoke-static {p0, p1, p2}, Laud;->b(Landroid/content/Context;J)V

    .line 162
    invoke-static {p1, p2}, Laud;->b(J)V

    .line 164
    sget-wide v0, Lbqa;->a:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    .line 165
    invoke-static {}, Lbqa;->a()V

    .line 167
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;JLcdg;)V
    .locals 1

    .prologue
    .line 180
    invoke-static {p0, p1, p2, p3}, Laud;->a(Landroid/content/Context;JLcdg;)V

    .line 182
    return-void
.end method

.method private static a(Lbqb;)V
    .locals 4

    .prologue
    .line 93
    const-class v0, Lbqa;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 94
    sput-object p0, Lbqa;->b:Lbqb;

    .line 95
    sget-object v0, Lbqa;->b:Lbqb;

    if-eqz v0, :cond_0

    sget-wide v0, Lbqa;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 96
    sget-object v0, Lbqa;->b:Lbqb;

    sget-wide v2, Lbqa;->a:J

    invoke-virtual {v0, v2, v3}, Lbqb;->b(J)V

    .line 98
    :cond_0
    return-void
.end method

.method public static declared-synchronized a(Lcom/twitter/library/client/Session;)V
    .locals 4

    .prologue
    .line 113
    const-class v1, Lbqa;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lbqa;->g()Lbqb;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lbqb;->c(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    monitor-exit v1

    return-void

    .line 113
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b()V
    .locals 1

    .prologue
    .line 136
    sget-object v0, Lbqa;->b:Lbqb;

    if-eqz v0, :cond_0

    .line 137
    sget-object v0, Lbqa;->b:Lbqb;

    invoke-virtual {v0}, Lbqb;->c()V

    .line 139
    :cond_0
    return-void
.end method

.method public static b(J)V
    .locals 2

    .prologue
    .line 130
    sget-object v0, Lbqa;->b:Lbqb;

    if-eqz v0, :cond_0

    .line 131
    sget-object v0, Lbqa;->b:Lbqb;

    invoke-virtual {v0, p0, p1}, Lbqb;->d(J)V

    .line 133
    :cond_0
    return-void
.end method

.method public static c()Laua;
    .locals 2

    .prologue
    .line 143
    sget-object v0, Lbqa;->c:Laua;

    if-eqz v0, :cond_0

    .line 144
    sget-object v0, Lbqa;->c:Laua;

    return-object v0

    .line 146
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "FeatureSwitches.initialize() must be called first."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static c(J)Z
    .locals 2

    .prologue
    .line 175
    invoke-static {}, Lbqa;->g()Lbqb;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lbqb;->a(J)Z

    move-result v0

    return v0
.end method

.method public static d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    invoke-static {}, Lbqa;->g()Lbqb;

    move-result-object v0

    invoke-virtual {v0}, Lbqb;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e()Lcdf;
    .locals 1

    .prologue
    .line 156
    invoke-static {}, Lbqa;->g()Lbqb;

    move-result-object v0

    invoke-virtual {v0}, Lbqb;->b()Lcdf;

    move-result-object v0

    return-object v0
.end method

.method public static f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    invoke-static {}, Lbqa;->g()Lbqb;

    move-result-object v0

    invoke-virtual {v0}, Lbqb;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static g()Lbqb;
    .locals 2

    .prologue
    .line 102
    sget-object v0, Lbqa;->b:Lbqb;

    if-eqz v0, :cond_0

    .line 103
    sget-object v0, Lbqa;->b:Lbqb;

    return-object v0

    .line 105
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "FeatureSwitches.initialize() must be called first."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static h()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 268
    :try_start_0
    invoke-static {}, Lcoh;->a()J

    move-result-wide v2

    .line 269
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "feature_switches_configs_crashlytics_enabled"

    invoke-static {v1, v2}, Lcom/twitter/model/util/b;->a(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 271
    if-lez v1, :cond_0

    const/16 v2, 0xa

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    .line 273
    :cond_0
    :goto_0
    return v0

    .line 272
    :catch_0
    move-exception v1

    goto :goto_0
.end method
