.class public Lbga;
.super Lbao;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbao",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcom/twitter/model/core/ac;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/twitter/library/service/v;

    invoke-direct {v0, p2}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    invoke-direct {p0, p1, v0, p3, p4}, Lbga;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;J)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;J)V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lbga;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lbao;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 41
    iput-wide p3, p0, Lbga;->a:J

    .line 42
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/ac;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 71
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    move-object/from16 v17, v0

    .line 72
    invoke-virtual/range {p3 .. p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/twitter/model/core/ac;

    .line 73
    if-eqz v3, :cond_2

    .line 74
    const-string/jumbo v2, "status_id"

    iget-wide v4, v3, Lcom/twitter/model/core/ac;->a:J

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 76
    invoke-virtual/range {p0 .. p0}, Lbga;->S()Laut;

    move-result-object v14

    .line 77
    invoke-virtual {v3}, Lcom/twitter/model/core/ac;->h()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v3}, Lcom/twitter/model/core/ac;->f()Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v16, 0x1

    .line 78
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lbga;->R()Lcom/twitter/library/provider/t;

    move-result-object v2

    invoke-static {v3}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lbga;->M()Lcom/twitter/library/service/v;

    move-result-object v4

    iget-wide v4, v4, Lcom/twitter/library/service/v;->c:J

    const/16 v6, 0xa

    const-wide/16 v7, -0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x1

    const/4 v15, 0x1

    invoke-virtual/range {v2 .. v16}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;JIJZZZLjava/lang/String;ZLaut;ZZ)Ljava/util/Collection;

    move-result-object v2

    .line 82
    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    .line 83
    invoke-virtual {v14}, Laut;->a()V

    .line 84
    const-string/jumbo v3, "scribe_item_count"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 99
    :cond_0
    :goto_1
    return-void

    .line 77
    :cond_1
    const/16 v16, 0x0

    goto :goto_0

    .line 87
    :cond_2
    invoke-virtual/range {p2 .. p2}, Lcom/twitter/library/service/u;->c()Ljava/lang/Exception;

    move-result-object v2

    if-nez v2, :cond_3

    .line 88
    new-instance v2, Ljava/lang/Exception;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v4, "ShowStatus failed for status %d with response code %d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-wide v8, v0, Lbga;->a:J

    .line 90
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-virtual/range {p2 .. p2}, Lcom/twitter/library/service/u;->d()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    .line 89
    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 94
    :goto_2
    new-instance v3, Lcpb;

    invoke-direct {v3}, Lcpb;-><init>()V

    const-string/jumbo v4, "failed.status"

    move-object/from16 v0, p0

    iget-wide v6, v0, Lbga;->a:J

    .line 95
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v3

    .line 96
    invoke-virtual {v3, v2}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v2

    .line 94
    invoke-static {v2}, Lcpd;->c(Lcpb;)V

    goto :goto_1

    .line 92
    :cond_3
    invoke-virtual/range {p2 .. p2}, Lcom/twitter/library/service/u;->c()Ljava/lang/Exception;

    move-result-object v2

    goto :goto_2
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 32
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbga;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected final b()Lcom/twitter/library/service/d$a;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 47
    invoke-virtual {p0}, Lbga;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "statuses"

    aput-object v3, v1, v2

    const-string/jumbo v2, "show"

    aput-object v2, v1, v4

    .line 48
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "id"

    iget-wide v2, p0, Lbga;->a:J

    .line 49
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 51
    const-string/jumbo v1, "include_entities"

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->e()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->b()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->f()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "include_media_features"

    .line 55
    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "include_user_entities"

    .line 56
    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->d()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->c()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 51
    return-object v0
.end method

.method protected e()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/ac;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    const-class v0, Lcom/twitter/model/core/ac;

    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lbga;->e()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
