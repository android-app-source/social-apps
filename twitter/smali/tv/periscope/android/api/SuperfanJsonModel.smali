.class public Ltv/periscope/android/api/SuperfanJsonModel;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public isFollowing:Z
    .annotation runtime Lkb;
        a = "is_following"
    .end annotation
.end field

.field public rank:I
    .annotation runtime Lkb;
        a = "rank"
    .end annotation
.end field

.field public score:I
    .annotation runtime Lkb;
        a = "score"
    .end annotation
.end field

.field public superfanSince:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "superfan_since"
    .end annotation
.end field

.field public userObject:Ltv/periscope/android/api/PsUser;
    .annotation runtime Lkb;
        a = "user"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private parseTime(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 37
    invoke-static {p1}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    invoke-static {p1}, Ldag;->a(Ljava/lang/String;)J

    move-result-wide v0

    .line 40
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public create()Ltv/periscope/model/ac$a;
    .locals 4

    .prologue
    .line 27
    invoke-static {}, Ltv/periscope/model/ac;->f()Ltv/periscope/model/ac$a;

    move-result-object v0

    .line 28
    iget-object v1, p0, Ltv/periscope/android/api/SuperfanJsonModel;->userObject:Ltv/periscope/android/api/PsUser;

    invoke-virtual {v0, v1}, Ltv/periscope/model/ac$a;->a(Ltv/periscope/model/user/UserItem;)Ltv/periscope/model/ac$a;

    move-result-object v0

    iget-boolean v1, p0, Ltv/periscope/android/api/SuperfanJsonModel;->isFollowing:Z

    .line 29
    invoke-virtual {v0, v1}, Ltv/periscope/model/ac$a;->a(Z)Ltv/periscope/model/ac$a;

    move-result-object v0

    iget v1, p0, Ltv/periscope/android/api/SuperfanJsonModel;->rank:I

    .line 30
    invoke-virtual {v0, v1}, Ltv/periscope/model/ac$a;->b(I)Ltv/periscope/model/ac$a;

    move-result-object v0

    iget v1, p0, Ltv/periscope/android/api/SuperfanJsonModel;->score:I

    .line 31
    invoke-virtual {v0, v1}, Ltv/periscope/model/ac$a;->a(I)Ltv/periscope/model/ac$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/SuperfanJsonModel;->superfanSince:Ljava/lang/String;

    .line 32
    invoke-direct {p0, v1}, Ltv/periscope/android/api/SuperfanJsonModel;->parseTime(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ltv/periscope/model/ac$a;->a(J)Ltv/periscope/model/ac$a;

    move-result-object v0

    .line 28
    return-object v0
.end method
