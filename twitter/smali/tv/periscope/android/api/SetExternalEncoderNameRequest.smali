.class public Ltv/periscope/android/api/SetExternalEncoderNameRequest;
.super Ltv/periscope/android/api/PsRequest;
.source "Twttr"


# instance fields
.field public encoderId:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "id"
    .end annotation
.end field

.field public name:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "name"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ltv/periscope/android/api/PsRequest;-><init>()V

    .line 16
    iput-object p1, p0, Ltv/periscope/android/api/SetExternalEncoderNameRequest;->cookie:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Ltv/periscope/android/api/SetExternalEncoderNameRequest;->encoderId:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Ltv/periscope/android/api/SetExternalEncoderNameRequest;->name:Ljava/lang/String;

    .line 19
    return-void
.end method
