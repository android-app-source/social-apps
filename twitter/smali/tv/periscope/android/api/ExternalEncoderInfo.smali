.class public Ltv/periscope/android/api/ExternalEncoderInfo;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public broadcast:Ltv/periscope/android/api/PsBroadcast;
    .annotation runtime Lkb;
        a = "broadcast"
    .end annotation
.end field

.field public id:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "id"
    .end annotation
.end field

.field public isStreamActive:Z
    .annotation runtime Lkb;
        a = "is_stream_active"
    .end annotation
.end field

.field public name:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "name"
    .end annotation
.end field

.field public rtmpUrl:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "rtmp_url"
    .end annotation
.end field

.field public streamCompatibilityInfo:Ltv/periscope/android/api/StreamCompatibilityInfo;
    .annotation runtime Lkb;
        a = "stream_compatibility_info"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
