.class public Ltv/periscope/android/api/CreateBroadcastRequest;
.super Ltv/periscope/android/api/PsRequest;
.source "Twttr"


# instance fields
.field public hasModeration:Z
    .annotation runtime Lkb;
        a = "has_moderation"
    .end annotation
.end field

.field public height:I
    .annotation runtime Lkb;
        a = "height"
    .end annotation
.end field

.field public languages:[Ljava/lang/String;
    .annotation runtime Lkb;
        a = "languages"
    .end annotation
.end field

.field public lat:D
    .annotation runtime Lkb;
        a = "lat"
    .end annotation
.end field

.field public lng:D
    .annotation runtime Lkb;
        a = "lng"
    .end annotation
.end field

.field public pspVersion:[I
    .annotation runtime Lkb;
        a = "supports_psp_version"
    .end annotation
.end field

.field public region:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "region"
    .end annotation
.end field

.field public width:I
    .annotation runtime Lkb;
        a = "width"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ltv/periscope/android/api/PsRequest;-><init>()V

    .line 35
    return-void
.end method
