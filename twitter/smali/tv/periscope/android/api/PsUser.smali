.class public Ltv/periscope/android/api/PsUser;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ltv/periscope/model/user/UserItem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/api/PsUser$VipBadge;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public className:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "class_name"
    .end annotation
.end field

.field public createdAt:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "created_at"
    .end annotation
.end field

.field public description:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "description"
    .end annotation
.end field

.field public displayName:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "display_name"
    .end annotation
.end field

.field public hasDigitsId:Z
    .annotation runtime Lkb;
        a = "has_digits_id"
    .end annotation
.end field

.field public id:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "id"
    .end annotation
.end field

.field public initials:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "initials"
    .end annotation
.end field

.field public isBlocked:Z
    .annotation runtime Lkb;
        a = "is_blocked"
    .end annotation
.end field

.field public isBluebirdUser:Z
    .annotation runtime Lkb;
        a = "is_bluebird_user"
    .end annotation
.end field

.field public isEmployee:Z
    .annotation runtime Lkb;
        a = "is_employee"
    .end annotation
.end field

.field public isFollowing:Z
    .annotation runtime Lkb;
        a = "is_following"
    .end annotation
.end field

.field public isMuted:Z
    .annotation runtime Lkb;
        a = "is_muted"
    .end annotation
.end field

.field public isVerified:Z
    .annotation runtime Lkb;
        a = "is_twitter_verified"
    .end annotation
.end field

.field public numFollowers:J
    .annotation runtime Lkb;
        a = "n_followers"
    .end annotation
.end field

.field public numFollowing:J
    .annotation runtime Lkb;
        a = "n_following"
    .end annotation
.end field

.field public numHearts:J
    .annotation runtime Lkb;
        a = "n_hearts"
    .end annotation
.end field

.field private numHeartsGiven:J
    .annotation runtime Lkb;
        a = "n_hearts_given"
    .end annotation
.end field

.field private participantIndex:J
    .annotation runtime Lkb;
        a = "participant_index"
    .end annotation
.end field

.field public profileImageUrls:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ltv/periscope/android/api/PsProfileImageUrl;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkb;
        a = "profile_image_urls"
    .end annotation
.end field

.field public transient profileUrlLarge:Ljava/lang/String;

.field public transient profileUrlMedium:Ljava/lang/String;

.field public transient profileUrlSmall:Ljava/lang/String;

.field public twitterId:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "twitter_id"
    .end annotation
.end field

.field public twitterUsername:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "twitter_username"
    .end annotation
.end field

.field public updatedAt:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "updated_at"
    .end annotation
.end field

.field public username:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "username"
    .end annotation
.end field

.field public vipBadge:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "vip"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 147
    new-instance v0, Ltv/periscope/android/api/PsUser$1;

    invoke-direct {v0}, Ltv/periscope/android/api/PsUser$1;-><init>()V

    sput-object v0, Ltv/periscope/android/api/PsUser;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsUser;->className:Ljava/lang/String;

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsUser;->createdAt:Ljava/lang/String;

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsUser;->updatedAt:Ljava/lang/String;

    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsUser;->username:Ljava/lang/String;

    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsUser;->displayName:Ljava/lang/String;

    .line 108
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsUser;->initials:Ljava/lang/String;

    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsUser;->description:Ljava/lang/String;

    .line 110
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsUser;->profileImageUrls:Ljava/util/ArrayList;

    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Ltv/periscope/android/api/PsUser;->numFollowers:J

    .line 112
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Ltv/periscope/android/api/PsUser;->numFollowing:J

    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Ltv/periscope/android/api/PsUser;->isFollowing:Z

    .line 114
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Ltv/periscope/android/api/PsUser;->isBlocked:Z

    .line 115
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Ltv/periscope/android/api/PsUser;->hasDigitsId:Z

    .line 116
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Ltv/periscope/android/api/PsUser;->numHearts:J

    .line 117
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    :goto_3
    iput-boolean v1, p0, Ltv/periscope/android/api/PsUser;->isMuted:Z

    .line 118
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsUser;->vipBadge:Ljava/lang/String;

    .line 119
    return-void

    :cond_0
    move v0, v2

    .line 113
    goto :goto_0

    :cond_1
    move v0, v2

    .line 114
    goto :goto_1

    :cond_2
    move v0, v2

    .line 115
    goto :goto_2

    :cond_3
    move v1, v2

    .line 117
    goto :goto_3
.end method

.method private loadProfileUrls()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 181
    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->profileImageUrls:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->profileImageUrls:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->profileUrlSmall:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->profileUrlMedium:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->profileUrlLarge:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 183
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    .line 184
    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->profileImageUrls:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/PsProfileImageUrl;

    .line 185
    iget v3, v0, Ltv/periscope/android/api/PsProfileImageUrl;->width:I

    iget v4, v0, Ltv/periscope/android/api/PsProfileImageUrl;->height:I

    mul-int/2addr v3, v4

    .line 186
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v0, v0, Ltv/periscope/android/api/PsProfileImageUrl;->url:Ljava/lang/String;

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 188
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 189
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 190
    if-lez v0, :cond_1

    .line 192
    add-int/lit8 v0, v0, -0x1

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 193
    const/4 v0, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 194
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ltv/periscope/android/api/PsUser;->profileUrlSmall:Ljava/lang/String;

    .line 195
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ltv/periscope/android/api/PsUser;->profileUrlMedium:Ljava/lang/String;

    .line 196
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ltv/periscope/android/api/PsUser;->profileUrlLarge:Ljava/lang/String;

    .line 199
    :cond_1
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    return v0
.end method

.method public getBadgeStatus()Ltv/periscope/android/api/PsUser$VipBadge;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->vipBadge:Ljava/lang/String;

    invoke-static {v0}, Ltv/periscope/android/api/PsUser$VipBadge;->fromString(Ljava/lang/String;)Ltv/periscope/android/api/PsUser$VipBadge;

    move-result-object v0

    return-object v0
.end method

.method public getNumHearts()J
    .locals 4

    .prologue
    .line 202
    iget-wide v0, p0, Ltv/periscope/android/api/PsUser;->numHearts:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 203
    const-wide/16 v0, 0x1

    .line 205
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Ltv/periscope/android/api/PsUser;->numHearts:J

    goto :goto_0
.end method

.method public getNumHeartsGiven()J
    .locals 2

    .prologue
    .line 224
    iget-wide v0, p0, Ltv/periscope/android/api/PsUser;->numHeartsGiven:J

    return-wide v0
.end method

.method public getParticipantIndex()J
    .locals 2

    .prologue
    .line 216
    iget-wide v0, p0, Ltv/periscope/android/api/PsUser;->participantIndex:J

    return-wide v0
.end method

.method public getProfileUrlLarge()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->profileUrlLarge:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 175
    invoke-direct {p0}, Ltv/periscope/android/api/PsUser;->loadProfileUrls()V

    .line 177
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->profileUrlLarge:Ljava/lang/String;

    return-object v0
.end method

.method public getProfileUrlMedium()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->profileUrlMedium:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 168
    invoke-direct {p0}, Ltv/periscope/android/api/PsUser;->loadProfileUrls()V

    .line 170
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->profileUrlMedium:Ljava/lang/String;

    return-object v0
.end method

.method public getProfileUrlSmall()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->profileUrlSmall:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 160
    invoke-direct {p0}, Ltv/periscope/android/api/PsUser;->loadProfileUrls()V

    .line 162
    :cond_0
    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->profileUrlSmall:Ljava/lang/String;

    return-object v0
.end method

.method public setNumHeartsGiven(J)V
    .locals 1

    .prologue
    .line 228
    iput-wide p1, p0, Ltv/periscope/android/api/PsUser;->numHeartsGiven:J

    .line 229
    return-void
.end method

.method public setParticipantIndex(J)V
    .locals 1

    .prologue
    .line 220
    iput-wide p1, p0, Ltv/periscope/android/api/PsUser;->participantIndex:J

    .line 221
    return-void
.end method

.method public type()Ltv/periscope/model/user/UserItem$Type;
    .locals 1

    .prologue
    .line 237
    sget-object v0, Ltv/periscope/model/user/UserItem$Type;->b:Ltv/periscope/model/user/UserItem$Type;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 128
    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->className:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 130
    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->createdAt:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->updatedAt:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->username:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->displayName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->initials:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->description:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->profileImageUrls:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 137
    iget-wide v4, p0, Ltv/periscope/android/api/PsUser;->numFollowers:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 138
    iget-wide v4, p0, Ltv/periscope/android/api/PsUser;->numFollowing:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 139
    iget-boolean v0, p0, Ltv/periscope/android/api/PsUser;->isFollowing:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 140
    iget-boolean v0, p0, Ltv/periscope/android/api/PsUser;->isBlocked:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 141
    iget-boolean v0, p0, Ltv/periscope/android/api/PsUser;->hasDigitsId:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 142
    iget-wide v4, p0, Ltv/periscope/android/api/PsUser;->numHearts:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 143
    iget-boolean v0, p0, Ltv/periscope/android/api/PsUser;->isMuted:Z

    if-eqz v0, :cond_3

    :goto_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 144
    iget-object v0, p0, Ltv/periscope/android/api/PsUser;->vipBadge:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 145
    return-void

    :cond_0
    move v0, v2

    .line 139
    goto :goto_0

    :cond_1
    move v0, v2

    .line 140
    goto :goto_1

    :cond_2
    move v0, v2

    .line 141
    goto :goto_2

    :cond_3
    move v1, v2

    .line 143
    goto :goto_3
.end method
