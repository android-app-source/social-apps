.class public Ltv/periscope/android/api/AccessChatResponse;
.super Ltv/periscope/android/api/PsResponse;
.source "Twttr"


# instance fields
.field public accessToken:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "access_token"
    .end annotation
.end field

.field public authToken:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "auth_token"
    .end annotation
.end field

.field public channel:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "channel"
    .end annotation
.end field

.field public channelPerms:Ltv/periscope/android/api/ChannelPermissions;
    .annotation runtime Lkb;
        a = "chan_perms"
    .end annotation
.end field

.field public endpoint:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "endpoint"
    .end annotation
.end field

.field public key:[B
    .annotation runtime Lkb;
        a = "key"
    .end annotation
.end field

.field public lifeCycleToken:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "life_cycle_token"
    .end annotation
.end field

.field public participantIndex:J
    .annotation runtime Lkb;
        a = "participant_index"
    .end annotation
.end field

.field public publisher:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "publisher"
    .end annotation
.end field

.field public replayAccessToken:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "replay_access_token"
    .end annotation
.end field

.field public replayEndpoint:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "replay_endpoint"
    .end annotation
.end field

.field public roomId:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "room_id"
    .end annotation
.end field

.field public shouldLog:Z
    .annotation runtime Lkb;
        a = "should_log"
    .end annotation
.end field

.field public shouldVerifySignature:Z
    .annotation runtime Lkb;
        a = "should_verify_signature"
    .end annotation
.end field

.field public signerKey:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "signer_key"
    .end annotation
.end field

.field public signerToken:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "signer_token"
    .end annotation
.end field

.field public subscriber:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "subscriber"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ltv/periscope/android/api/PsResponse;-><init>()V

    return-void
.end method


# virtual methods
.method public create()Ltv/periscope/model/u;
    .locals 10

    .prologue
    .line 65
    const/4 v9, 0x0

    .line 66
    iget-object v0, p0, Ltv/periscope/android/api/AccessChatResponse;->channelPerms:Ltv/periscope/android/api/ChannelPermissions;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Ltv/periscope/android/api/AccessChatResponse;->channelPerms:Ltv/periscope/android/api/ChannelPermissions;

    iget v9, v0, Ltv/periscope/android/api/ChannelPermissions;->chatmanOpts:I

    .line 69
    :cond_0
    iget-wide v0, p0, Ltv/periscope/android/api/AccessChatResponse;->participantIndex:J

    iget-object v2, p0, Ltv/periscope/android/api/AccessChatResponse;->roomId:Ljava/lang/String;

    iget-object v3, p0, Ltv/periscope/android/api/AccessChatResponse;->lifeCycleToken:Ljava/lang/String;

    iget-boolean v4, p0, Ltv/periscope/android/api/AccessChatResponse;->shouldLog:Z

    iget-object v5, p0, Ltv/periscope/android/api/AccessChatResponse;->accessToken:Ljava/lang/String;

    iget-object v6, p0, Ltv/periscope/android/api/AccessChatResponse;->replayAccessToken:Ljava/lang/String;

    iget-object v7, p0, Ltv/periscope/android/api/AccessChatResponse;->endpoint:Ljava/lang/String;

    iget-object v8, p0, Ltv/periscope/android/api/AccessChatResponse;->replayEndpoint:Ljava/lang/String;

    invoke-static/range {v0 .. v9}, Ltv/periscope/model/u;->a(JLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ltv/periscope/model/u;

    move-result-object v0

    return-object v0
.end method
