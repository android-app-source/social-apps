.class public Ltv/periscope/android/api/PsBroadcast;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public availableForReplay:Z
    .annotation runtime Lkb;
        a = "available_for_replay"
    .end annotation
.end field

.field public broadcastState:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "state"
    .end annotation
.end field

.field public cameraRotation:I
    .annotation runtime Lkb;
        a = "camera_rotation"
    .end annotation
.end field

.field public channelName:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "channel_name"
    .end annotation
.end field

.field public city:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "city"
    .end annotation
.end field

.field public className:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "class_name"
    .end annotation
.end field

.field public country:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "country"
    .end annotation
.end field

.field public countryState:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "country_state"
    .end annotation
.end field

.field public createdAt:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "created_at"
    .end annotation
.end field

.field public endTime:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "end"
    .end annotation
.end field

.field public expirationTime:I
    .annotation runtime Lkb;
        a = "expiration"
    .end annotation
.end field

.field public featured:Z
    .annotation runtime Lkb;
        a = "featured"
    .end annotation
.end field

.field public featuredCategory:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "featured_category"
    .end annotation
.end field

.field public featuredCategoryColor:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "featured_category_color"
    .end annotation
.end field

.field public featuredReason:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "featured_reason"
    .end annotation
.end field

.field public featuredTimecodeSec:J
    .annotation runtime Lkb;
        a = "featured_timecode"
    .end annotation
.end field

.field public hasLocation:Z
    .annotation runtime Lkb;
        a = "has_location"
    .end annotation
.end field

.field public heartThemes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkb;
        a = "heart_theme"
    .end annotation
.end field

.field public height:I
    .annotation runtime Lkb;
        a = "height"
    .end annotation
.end field

.field private highlightAvailable:Z
    .annotation runtime Lkb;
        a = "highlight_available"
    .end annotation
.end field

.field public id:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "id"
    .end annotation
.end field

.field public imageUrl:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "image_url"
    .end annotation
.end field

.field public imageUrlSmall:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "image_url_small"
    .end annotation
.end field

.field public ipLat:D
    .annotation runtime Lkb;
        a = "ip_lat"
    .end annotation
.end field

.field public ipLong:D
    .annotation runtime Lkb;
        a = "ip_lng"
    .end annotation
.end field

.field public is360:Z
    .annotation runtime Lkb;
        a = "is_360"
    .end annotation
.end field

.field public isLocked:Z
    .annotation runtime Lkb;
        a = "is_locked"
    .end annotation
.end field

.field public isTrusted:Z
    .annotation runtime Lkb;
        a = "is_trusted"
    .end annotation
.end field

.field public numTotalWatched:Ljava/lang/Long;
    .annotation runtime Lkb;
        a = "n_total_watched"
    .end annotation
.end field

.field public numTotalWatching:Ljava/lang/Long;
    .annotation runtime Lkb;
        a = "n_total_watching"
    .end annotation
.end field

.field public pingTime:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "ping"
    .end annotation
.end field

.field public profileImageUrl:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "profile_image_url"
    .end annotation
.end field

.field public shareUserDisplayNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkb;
        a = "share_display_names"
    .end annotation
.end field

.field public shareUserIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkb;
        a = "share_user_ids"
    .end annotation
.end field

.field public sortScore:J
    .annotation runtime Lkb;
        a = "sort_score"
    .end annotation
.end field

.field public startTime:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "start"
    .end annotation
.end field

.field public timedOutTime:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "timedout"
    .end annotation
.end field

.field public title:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "status"
    .end annotation
.end field

.field public tweetId:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "tweet_id"
    .end annotation
.end field

.field public twitterUsername:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "twitter_username"
    .end annotation
.end field

.field public updatedAt:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "updated_at"
    .end annotation
.end field

.field public userDisplayName:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "user_display_name"
    .end annotation
.end field

.field public userId:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "user_id"
    .end annotation
.end field

.field public username:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "username"
    .end annotation
.end field

.field public watchedTime:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "watched_time"
    .end annotation
.end field

.field public width:I
    .annotation runtime Lkb;
        a = "width"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private parseTime(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 163
    invoke-static {p1}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    invoke-static {p1}, Ldag;->a(Ljava/lang/String;)J

    move-result-wide v0

    .line 166
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public create()Ltv/periscope/model/p;
    .locals 4

    .prologue
    .line 170
    invoke-static {}, Ltv/periscope/model/p;->G()Ltv/periscope/model/p$a;

    move-result-object v0

    .line 171
    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->id:Ljava/lang/String;

    .line 172
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->a(Ljava/lang/String;)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->title:Ljava/lang/String;

    .line 173
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->b(Ljava/lang/String;)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->country:Ljava/lang/String;

    iget-object v2, p0, Ltv/periscope/android/api/PsBroadcast;->city:Ljava/lang/String;

    iget-object v3, p0, Ltv/periscope/android/api/PsBroadcast;->countryState:Ljava/lang/String;

    .line 174
    invoke-static {v1, v2, v3}, Ltv/periscope/model/z;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ltv/periscope/model/z;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->a(Ltv/periscope/model/z;)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->createdAt:Ljava/lang/String;

    .line 175
    invoke-direct {p0, v1}, Ltv/periscope/android/api/PsBroadcast;->parseTime(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ltv/periscope/model/p$a;->c(J)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-boolean v1, p0, Ltv/periscope/android/api/PsBroadcast;->featured:Z

    .line 176
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->a(Z)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->featuredCategory:Ljava/lang/String;

    .line 177
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->c(Ljava/lang/String;)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->featuredCategoryColor:Ljava/lang/String;

    .line 178
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->d(Ljava/lang/String;)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->featuredReason:Ljava/lang/String;

    .line 179
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->e(Ljava/lang/String;)Ltv/periscope/model/p$a;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, p0, Ltv/periscope/android/api/PsBroadcast;->featuredTimecodeSec:J

    .line 180
    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ltv/periscope/model/p$a;->d(J)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/android/api/PsBroadcast;->sortScore:J

    .line 181
    invoke-virtual {v0, v2, v3}, Ltv/periscope/model/p$a;->e(J)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->startTime:Ljava/lang/String;

    .line 182
    invoke-direct {p0, v1}, Ltv/periscope/android/api/PsBroadcast;->parseTime(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ltv/periscope/model/p$a;->f(J)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/android/api/PsBroadcast;->ipLat:D

    .line 183
    invoke-virtual {v0, v2, v3}, Ltv/periscope/model/p$a;->a(D)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/android/api/PsBroadcast;->ipLong:D

    .line 184
    invoke-virtual {v0, v2, v3}, Ltv/periscope/model/p$a;->b(D)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->userId:Ljava/lang/String;

    .line 185
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->f(Ljava/lang/String;)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-boolean v1, p0, Ltv/periscope/android/api/PsBroadcast;->isLocked:Z

    .line 186
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->b(Z)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->imageUrl:Ljava/lang/String;

    .line 187
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->g(Ljava/lang/String;)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->imageUrlSmall:Ljava/lang/String;

    .line 188
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->h(Ljava/lang/String;)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->userDisplayName:Ljava/lang/String;

    .line 189
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->i(Ljava/lang/String;)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->profileImageUrl:Ljava/lang/String;

    .line 190
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->j(Ljava/lang/String;)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->twitterUsername:Ljava/lang/String;

    .line 191
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->k(Ljava/lang/String;)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-boolean v1, p0, Ltv/periscope/android/api/PsBroadcast;->hasLocation:Z

    .line 192
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->c(Z)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->shareUserIds:Ljava/util/ArrayList;

    .line 193
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->a(Ljava/util/ArrayList;)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->shareUserDisplayNames:Ljava/util/ArrayList;

    .line 194
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->b(Ljava/util/ArrayList;)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->heartThemes:Ljava/util/ArrayList;

    .line 195
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->c(Ljava/util/ArrayList;)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->pingTime:Ljava/lang/String;

    .line 196
    invoke-direct {p0, v1}, Ltv/periscope/android/api/PsBroadcast;->parseTime(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ltv/periscope/model/p$a;->b(J)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->timedOutTime:Ljava/lang/String;

    .line 197
    invoke-direct {p0, v1}, Ltv/periscope/android/api/PsBroadcast;->parseTime(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ltv/periscope/model/p$a;->a(J)Ltv/periscope/model/p$a;

    move-result-object v0

    iget v1, p0, Ltv/periscope/android/api/PsBroadcast;->cameraRotation:I

    .line 198
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->a(I)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-boolean v1, p0, Ltv/periscope/android/api/PsBroadcast;->highlightAvailable:Z

    .line 199
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->d(Z)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->tweetId:Ljava/lang/String;

    .line 200
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->m(Ljava/lang/String;)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-boolean v1, p0, Ltv/periscope/android/api/PsBroadcast;->is360:Z

    .line 201
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->e(Z)Ltv/periscope/model/p$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsBroadcast;->username:Ljava/lang/String;

    .line 202
    invoke-virtual {v0, v1}, Ltv/periscope/model/p$a;->l(Ljava/lang/String;)Ltv/periscope/model/p$a;

    move-result-object v0

    .line 203
    invoke-virtual {v0}, Ltv/periscope/model/p$a;->a()Ltv/periscope/model/p;

    move-result-object v1

    .line 205
    iget-object v2, p0, Ltv/periscope/android/api/PsBroadcast;->broadcastState:Ljava/lang/String;

    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 227
    :goto_1
    iget-object v0, p0, Ltv/periscope/android/api/PsBroadcast;->endTime:Ljava/lang/String;

    invoke-direct {p0, v0}, Ltv/periscope/android/api/PsBroadcast;->parseTime(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ltv/periscope/model/p;->a(J)V

    .line 228
    iget-object v0, p0, Ltv/periscope/android/api/PsBroadcast;->numTotalWatching:Ljava/lang/Long;

    invoke-virtual {v1, v0}, Ltv/periscope/model/p;->a(Ljava/lang/Long;)V

    .line 229
    iget-boolean v0, p0, Ltv/periscope/android/api/PsBroadcast;->availableForReplay:Z

    invoke-virtual {v1, v0}, Ltv/periscope/model/p;->b(Z)V

    .line 230
    iget-boolean v0, p0, Ltv/periscope/android/api/PsBroadcast;->isTrusted:Z

    invoke-virtual {v1, v0}, Ltv/periscope/model/p;->c(Z)V

    .line 231
    iget v0, p0, Ltv/periscope/android/api/PsBroadcast;->expirationTime:I

    invoke-virtual {v1, v0}, Ltv/periscope/model/p;->a(I)V

    .line 232
    iget-object v0, p0, Ltv/periscope/android/api/PsBroadcast;->numTotalWatched:Ljava/lang/Long;

    invoke-virtual {v1, v0}, Ltv/periscope/model/p;->b(Ljava/lang/Long;)V

    .line 233
    iget-object v0, p0, Ltv/periscope/android/api/PsBroadcast;->channelName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ltv/periscope/model/p;->a(Ljava/lang/String;)V

    .line 234
    iget-object v0, p0, Ltv/periscope/android/api/PsBroadcast;->watchedTime:Ljava/lang/String;

    invoke-direct {p0, v0}, Ltv/periscope/android/api/PsBroadcast;->parseTime(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Ltv/periscope/model/p;->c(Ljava/lang/Long;)V

    .line 236
    return-object v1

    .line 205
    :sswitch_0
    const-string/jumbo v3, "NOT_STARTED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v3, "PUBLISHED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string/jumbo v3, "RUNNING"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string/jumbo v3, "TIMED_OUT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string/jumbo v3, "ENDED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    .line 207
    :pswitch_0
    sget-object v0, Ltv/periscope/model/BroadcastState;->a:Ltv/periscope/model/BroadcastState;

    invoke-virtual {v1, v0}, Ltv/periscope/model/p;->a(Ltv/periscope/model/BroadcastState;)V

    goto :goto_1

    .line 211
    :pswitch_1
    sget-object v0, Ltv/periscope/model/BroadcastState;->b:Ltv/periscope/model/BroadcastState;

    invoke-virtual {v1, v0}, Ltv/periscope/model/p;->a(Ltv/periscope/model/BroadcastState;)V

    goto :goto_1

    .line 215
    :pswitch_2
    sget-object v0, Ltv/periscope/model/BroadcastState;->c:Ltv/periscope/model/BroadcastState;

    invoke-virtual {v1, v0}, Ltv/periscope/model/p;->a(Ltv/periscope/model/BroadcastState;)V

    goto :goto_1

    .line 219
    :pswitch_3
    sget-object v0, Ltv/periscope/model/BroadcastState;->d:Ltv/periscope/model/BroadcastState;

    invoke-virtual {v1, v0}, Ltv/periscope/model/p;->a(Ltv/periscope/model/BroadcastState;)V

    goto/16 :goto_1

    .line 223
    :pswitch_4
    sget-object v0, Ltv/periscope/model/BroadcastState;->e:Ltv/periscope/model/BroadcastState;

    invoke-virtual {v1, v0}, Ltv/periscope/model/p;->a(Ltv/periscope/model/BroadcastState;)V

    goto/16 :goto_1

    .line 205
    nop

    :sswitch_data_0
    .sparse-switch
        -0x78c55e61 -> :sswitch_2
        -0x576cf1fa -> :sswitch_3
        -0x52ecc12b -> :sswitch_0
        -0x3a24e32 -> :sswitch_1
        0x3f0d29a -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
