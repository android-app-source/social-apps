.class public Ltv/periscope/android/api/service/highlights/PsHighlight;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public algorithm:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "algorithm"
    .end annotation
.end field

.field public imagePreviewUrl:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "image_preview_url"
    .end annotation
.end field

.field public metadata:Ltv/periscope/android/api/service/highlights/PsHighlightMetadata;
    .annotation runtime Lkb;
        a = "metadata"
    .end annotation
.end field

.field public totalScore:F
    .annotation runtime Lkb;
        a = "total_score"
    .end annotation
.end field

.field public url:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "url"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create()Ldcj;
    .locals 2

    .prologue
    .line 24
    invoke-static {}, Ldcj;->f()Ldcj$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/service/highlights/PsHighlight;->algorithm:Ljava/lang/String;

    .line 25
    invoke-virtual {v0, v1}, Ldcj$a;->a(Ljava/lang/String;)Ldcj$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/service/highlights/PsHighlight;->url:Ljava/lang/String;

    .line 26
    invoke-virtual {v0, v1}, Ldcj$a;->b(Ljava/lang/String;)Ldcj$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/service/highlights/PsHighlight;->imagePreviewUrl:Ljava/lang/String;

    .line 27
    invoke-virtual {v0, v1}, Ldcj$a;->c(Ljava/lang/String;)Ldcj$a;

    move-result-object v0

    iget v1, p0, Ltv/periscope/android/api/service/highlights/PsHighlight;->totalScore:F

    .line 28
    invoke-virtual {v0, v1}, Ldcj$a;->a(F)Ldcj$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/service/highlights/PsHighlight;->metadata:Ltv/periscope/android/api/service/highlights/PsHighlightMetadata;

    .line 29
    invoke-virtual {v1}, Ltv/periscope/android/api/service/highlights/PsHighlightMetadata;->create()Ldcm;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldcj$a;->a(Ldcm;)Ldcj$a;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Ldcj$a;->a()Ldcj;

    move-result-object v0

    .line 24
    return-object v0
.end method
