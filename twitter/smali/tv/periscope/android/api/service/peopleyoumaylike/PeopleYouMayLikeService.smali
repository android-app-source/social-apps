.class public interface abstract Ltv/periscope/android/api/service/peopleyoumaylike/PeopleYouMayLikeService;
.super Ljava/lang/Object;
.source "Twttr"


# virtual methods
.method public abstract deleteAddressBook(Ltv/periscope/android/api/service/peopleyoumaylike/DeleteAddressBookRequest;)Ljava/lang/Void;
    .param p1    # Ltv/periscope/android/api/service/peopleyoumaylike/DeleteAddressBookRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/deleteAddressBook"
    .end annotation
.end method

.method public abstract dismissFollowRecommendation(Ltv/periscope/android/api/service/peopleyoumaylike/DismissFollowRecommendationRequest;)Ljava/lang/Void;
    .param p1    # Ltv/periscope/android/api/service/peopleyoumaylike/DismissFollowRecommendationRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/dismissFollowRecommendation"
    .end annotation
.end method

.method public abstract dismissFollowRecommendations(Ltv/periscope/android/api/service/peopleyoumaylike/DismissFollowRecommendationsRequest;)Ljava/lang/Void;
    .param p1    # Ltv/periscope/android/api/service/peopleyoumaylike/DismissFollowRecommendationsRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/dismissFollowRecommendations"
    .end annotation
.end method

.method public abstract getFollowRecommendations(Ltv/periscope/android/api/service/peopleyoumaylike/FollowRecommendationRequest;)Ljava/util/List;
    .param p1    # Ltv/periscope/android/api/service/peopleyoumaylike/FollowRecommendationRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltv/periscope/android/api/service/peopleyoumaylike/FollowRecommendationRequest;",
            ")",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/peopleyoumaylike/PeopleYouMayLikeJSONModel;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/followRecommendations"
    .end annotation
.end method

.method public abstract uploadAddressBook(Ltv/periscope/android/api/service/peopleyoumaylike/UploadAddressBookRequest;)Ljava/lang/Void;
    .param p1    # Ltv/periscope/android/api/service/peopleyoumaylike/UploadAddressBookRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/uploadAddressBook"
    .end annotation
.end method
