.class public interface abstract Ltv/periscope/android/api/service/suggestedbroadcasts/SuggestedBroadcastsService;
.super Ljava/lang/Object;
.source "Twttr"


# virtual methods
.method public abstract getSuggestedBroadcasts(Ltv/periscope/android/api/service/suggestedbroadcasts/SuggestedBroadcastRequest;)Ljava/util/List;
    .param p1    # Ltv/periscope/android/api/service/suggestedbroadcasts/SuggestedBroadcastRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltv/periscope/android/api/service/suggestedbroadcasts/SuggestedBroadcastRequest;",
            ")",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/api/service/suggestedbroadcasts/model/SuggestedBroadcastsJSONModel;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/suggestedBroadcasts"
    .end annotation
.end method
