.class public abstract Ltv/periscope/android/api/service/suggestedbroadcasts/model/SuggestedBroadcastsJSONModel;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/android/api/service/suggestedbroadcasts/model/SuggestedBroadcastsJSONModel$Builder;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static builder()Ltv/periscope/android/api/service/suggestedbroadcasts/model/SuggestedBroadcastsJSONModel$Builder;
    .locals 1

    .prologue
    .line 21
    new-instance v0, Ltv/periscope/android/api/service/suggestedbroadcasts/model/$AutoValue_SuggestedBroadcastsJSONModel$Builder;

    invoke-direct {v0}, Ltv/periscope/android/api/service/suggestedbroadcasts/model/$AutoValue_SuggestedBroadcastsJSONModel$Builder;-><init>()V

    return-object v0
.end method

.method public static typeAdapter(Lcom/google/gson/e;)Lcom/google/gson/r;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/e;",
            ")",
            "Lcom/google/gson/r",
            "<",
            "Ltv/periscope/android/api/service/suggestedbroadcasts/model/SuggestedBroadcastsJSONModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16
    new-instance v0, Ltv/periscope/android/api/service/suggestedbroadcasts/model/AutoValue_SuggestedBroadcastsJSONModel$GsonTypeAdapter;

    invoke-direct {v0, p0}, Ltv/periscope/android/api/service/suggestedbroadcasts/model/AutoValue_SuggestedBroadcastsJSONModel$GsonTypeAdapter;-><init>(Lcom/google/gson/e;)V

    return-object v0
.end method


# virtual methods
.method public abstract broadcast()Ltv/periscope/android/api/PsBroadcast;
    .annotation runtime Lkb;
        a = "broadcast"
    .end annotation
.end method

.method public abstract reason()Ltv/periscope/android/api/service/suggestedbroadcasts/model/SuggestionReasonJSONModel;
    .annotation runtime Lkb;
        a = "reason"
    .end annotation
.end method

.method public abstract totalWatched()Ljava/lang/Long;
    .annotation runtime Lkb;
        a = "n_total_watched"
    .end annotation
.end method

.method public abstract totalWatching()Ljava/lang/Long;
    .annotation runtime Lkb;
        a = "n_total_watching"
    .end annotation
.end method

.method public abstract watching()Ljava/lang/Long;
    .annotation runtime Lkb;
        a = "n_watching"
    .end annotation
.end method

.method public abstract webWatching()Ljava/lang/Long;
    .annotation runtime Lkb;
        a = "n_web_watching"
    .end annotation
.end method
