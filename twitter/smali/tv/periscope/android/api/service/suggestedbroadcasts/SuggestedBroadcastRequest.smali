.class public Ltv/periscope/android/api/service/suggestedbroadcasts/SuggestedBroadcastRequest;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final cookie:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "cookie"
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Ltv/periscope/android/api/service/suggestedbroadcasts/SuggestedBroadcastRequest;->cookie:Ljava/lang/String;

    .line 17
    return-void
.end method

.method public static create(Ljava/lang/String;)Ltv/periscope/android/api/service/suggestedbroadcasts/SuggestedBroadcastRequest;
    .locals 1

    .prologue
    .line 12
    new-instance v0, Ltv/periscope/android/api/service/suggestedbroadcasts/SuggestedBroadcastRequest;

    invoke-direct {v0, p0}, Ltv/periscope/android/api/service/suggestedbroadcasts/SuggestedBroadcastRequest;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
