.class public Ltv/periscope/android/api/service/channels/PsChannelAction;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final TAG:Ljava/lang/String; = "PsChannelAction"


# instance fields
.field public actionType:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "ActionType"
    .end annotation
.end field

.field public broadcastId:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "BroadcastId"
    .end annotation
.end field

.field public channelId:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "CID"
    .end annotation
.end field

.field public channelName:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "ChannelName"
    .end annotation
.end field

.field public dateTime:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "Time"
    .end annotation
.end field

.field public memberId:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "MemberId"
    .end annotation
.end field

.field public userId:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "ById"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convert(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/api/service/channels/PsChannelAction;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/ChannelAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 45
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/service/channels/PsChannelAction;

    .line 47
    :try_start_0
    invoke-virtual {v0}, Ltv/periscope/android/api/service/channels/PsChannelAction;->create()Ltv/periscope/model/ChannelAction;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 48
    :catch_0
    move-exception v0

    .line 49
    const-string/jumbo v3, "PsChannelAction"

    const-string/jumbo v4, "Attempted to create an unsupported channel action"

    invoke-static {v3, v4, v0}, Ltv/periscope/android/util/t;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 52
    :cond_0
    return-object v1
.end method

.method public static toUserIds(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/api/service/channels/PsChannelAction;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 59
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/service/channels/PsChannelAction;

    .line 60
    iget-object v3, v0, Ltv/periscope/android/api/service/channels/PsChannelAction;->userId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 61
    iget-object v3, v0, Ltv/periscope/android/api/service/channels/PsChannelAction;->userId:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 63
    :cond_1
    iget-object v3, v0, Ltv/periscope/android/api/service/channels/PsChannelAction;->memberId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 64
    iget-object v0, v0, Ltv/periscope/android/api/service/channels/PsChannelAction;->memberId:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 67
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method


# virtual methods
.method public create()Ltv/periscope/model/ChannelAction;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 72
    invoke-static {}, Ltv/periscope/model/ChannelAction;->h()Ltv/periscope/model/ChannelAction$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/service/channels/PsChannelAction;->userId:Ljava/lang/String;

    .line 73
    invoke-virtual {v0, v1}, Ltv/periscope/model/ChannelAction$a;->a(Ljava/lang/String;)Ltv/periscope/model/ChannelAction$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/service/channels/PsChannelAction;->actionType:Ljava/lang/String;

    .line 74
    invoke-static {v1}, Ltv/periscope/model/ChannelAction$ActionType;->a(Ljava/lang/String;)Ltv/periscope/model/ChannelAction$ActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/ChannelAction$a;->a(Ltv/periscope/model/ChannelAction$ActionType;)Ltv/periscope/model/ChannelAction$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/service/channels/PsChannelAction;->channelId:Ljava/lang/String;

    .line 75
    invoke-virtual {v0, v1}, Ltv/periscope/model/ChannelAction$a;->b(Ljava/lang/String;)Ltv/periscope/model/ChannelAction$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/service/channels/PsChannelAction;->dateTime:Ljava/lang/String;

    .line 76
    invoke-static {v1}, Ldag;->a(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ltv/periscope/model/ChannelAction$a;->a(J)Ltv/periscope/model/ChannelAction$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/service/channels/PsChannelAction;->memberId:Ljava/lang/String;

    .line 77
    invoke-virtual {v0, v1}, Ltv/periscope/model/ChannelAction$a;->c(Ljava/lang/String;)Ltv/periscope/model/ChannelAction$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/service/channels/PsChannelAction;->broadcastId:Ljava/lang/String;

    .line 78
    invoke-virtual {v0, v1}, Ltv/periscope/model/ChannelAction$a;->d(Ljava/lang/String;)Ltv/periscope/model/ChannelAction$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/service/channels/PsChannelAction;->channelName:Ljava/lang/String;

    .line 79
    invoke-virtual {v0, v1}, Ltv/periscope/model/ChannelAction$a;->e(Ljava/lang/String;)Ltv/periscope/model/ChannelAction$a;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Ltv/periscope/model/ChannelAction$a;->a()Ltv/periscope/model/ChannelAction;

    move-result-object v0

    .line 72
    return-object v0
.end method
