.class public interface abstract Ltv/periscope/android/api/service/channels/ChannelsService;
.super Ljava/lang/Object;
.source "Twttr"


# virtual methods
.method public abstract addMembersToChannel(Ljava/lang/String;Ljava/lang/String;Ltv/periscope/android/api/service/channels/AddMembersToChannelRequest;)Ltv/periscope/android/api/PsResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "cid"
        .end annotation
    .end param
    .param p3    # Ltv/periscope/android/api/service/channels/AddMembersToChannelRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/channels/{cid}/members"
    .end annotation
.end method

.method public abstract createChannel(Ljava/lang/String;Ltv/periscope/android/api/service/channels/CreateChannelRequest;)Ltv/periscope/android/api/service/channels/PsCreateChannelResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Ltv/periscope/android/api/service/channels/CreateChannelRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/channels/"
    .end annotation
.end method

.method public abstract deleteChannel(Ljava/lang/String;Ljava/lang/String;)Ltv/periscope/android/api/PsResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "cid"
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/DELETE;
        value = "/channels/{cid}"
    .end annotation
.end method

.method public abstract deleteChannelMember(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ltv/periscope/android/api/PsResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "cid"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "uid"
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/DELETE;
        value = "/channels/{cid}/members/{uid}"
    .end annotation
.end method

.method public abstract getBroadcastsForChannel(Ljava/lang/String;Ljava/lang/String;)Ltv/periscope/android/api/service/channels/PsGetBroadcastsForChannelResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "id"
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/GET;
        value = "/channels/{id}/broadcasts"
    .end annotation
.end method

.method public abstract getChannelActions(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelActionsResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "cid"
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lretrofit/http/Query;
            value = "batchSize"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Query;
            value = "cursor"
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/GET;
        value = "/channels/{cid}/actions"
    .end annotation
.end method

.method public abstract getChannelInfo(Ljava/lang/String;Ljava/lang/String;)Ltv/periscope/android/api/service/channels/PsGetChannelInfoResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "id"
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/GET;
        value = "/channels/{id}"
    .end annotation
.end method

.method public abstract getChannelMembers(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ltv/periscope/android/api/service/channels/PsGetAndHydrateChannelMembersResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "cid"
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lretrofit/http/Query;
            value = "batchSize"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Query;
            value = "cursor"
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/GET;
        value = "/channels/{cid}/members"
    .end annotation
.end method

.method public abstract getChannels(Ljava/lang/String;Ljava/util/List;)Ltv/periscope/android/api/service/channels/PsGetChannelsResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Lretrofit/http/Query;
            value = "languages"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ltv/periscope/android/api/service/channels/PsGetChannelsResponse;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/GET;
        value = "/channels"
    .end annotation
.end method

.method public abstract getChannelsForMember(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ltv/periscope/android/api/service/channels/PsGetChannelsForMemberResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "uid"
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lretrofit/http/Query;
            value = "batchSize"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Query;
            value = "cursor"
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/GET;
        value = "/users/{uid}/channels"
    .end annotation
.end method

.method public abstract patchChannel(Ljava/lang/String;Ljava/lang/String;Ltv/periscope/android/api/service/channels/PatchChannelRequest;)Ltv/periscope/android/api/service/channels/PsCreateChannelResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "cid"
        .end annotation
    .end param
    .param p3    # Ltv/periscope/android/api/service/channels/PatchChannelRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/PATCH;
        value = "/channels/{cid}"
    .end annotation
.end method

.method public abstract patchChannelMember(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/android/api/service/channels/PatchChannelMemberRequest;)Ltv/periscope/android/api/PsResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "cid"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "uid"
        .end annotation
    .end param
    .param p4    # Ltv/periscope/android/api/service/channels/PatchChannelMemberRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/PATCH;
        value = "/channels/{cid}/members/{uid}"
    .end annotation
.end method

.method public abstract searchChannels(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ltv/periscope/android/api/service/channels/PsGetChannelSearchResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Query;
            value = "name"
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation runtime Lretrofit/http/Query;
            value = "languages"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ltv/periscope/android/api/service/channels/PsGetChannelSearchResponse;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/GET;
        value = "/channels"
    .end annotation
.end method
