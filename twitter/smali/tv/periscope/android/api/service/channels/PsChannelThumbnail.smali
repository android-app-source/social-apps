.class public Ltv/periscope/android/api/service/channels/PsChannelThumbnail;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public height:I
    .annotation runtime Lkb;
        a = "height"
    .end annotation
.end field

.field public sslUrl:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "ssl_url"
    .end annotation
.end field

.field public url:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "url"
    .end annotation
.end field

.field public width:I
    .annotation runtime Lkb;
        a = "width"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create()Ltv/periscope/model/t;
    .locals 2

    .prologue
    .line 21
    invoke-static {}, Ltv/periscope/model/t;->e()Ltv/periscope/model/t$a;

    move-result-object v0

    iget v1, p0, Ltv/periscope/android/api/service/channels/PsChannelThumbnail;->width:I

    .line 22
    invoke-virtual {v0, v1}, Ltv/periscope/model/t$a;->a(I)Ltv/periscope/model/t$a;

    move-result-object v0

    iget v1, p0, Ltv/periscope/android/api/service/channels/PsChannelThumbnail;->height:I

    .line 23
    invoke-virtual {v0, v1}, Ltv/periscope/model/t$a;->b(I)Ltv/periscope/model/t$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/service/channels/PsChannelThumbnail;->sslUrl:Ljava/lang/String;

    .line 24
    invoke-virtual {v0, v1}, Ltv/periscope/model/t$a;->a(Ljava/lang/String;)Ltv/periscope/model/t$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/service/channels/PsChannelThumbnail;->url:Ljava/lang/String;

    .line 25
    invoke-virtual {v0, v1}, Ltv/periscope/model/t$a;->b(Ljava/lang/String;)Ltv/periscope/model/t$a;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Ltv/periscope/model/t$a;->a()Ltv/periscope/model/t;

    move-result-object v0

    .line 21
    return-object v0
.end method
