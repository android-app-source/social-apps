.class public Ltv/periscope/android/api/service/channels/PsChannel;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public channelId:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "CID"
    .end annotation
.end field

.field public description:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "Description"
    .end annotation
.end field

.field public featured:Z
    .annotation runtime Lkb;
        a = "Featured"
    .end annotation
.end field

.field public memberCount:J
    .annotation runtime Lkb;
        a = "NMember"
    .end annotation
.end field

.field public name:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "Name"
    .end annotation
.end field

.field public numberOfLiveStreams:J
    .annotation runtime Lkb;
        a = "NLive"
    .end annotation
.end field

.field public ownerId:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "OwnerId"
    .end annotation
.end field

.field public publicTag:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "PublicTag"
    .end annotation
.end field

.field public thumbnails:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/api/service/channels/PsChannelThumbnail;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkb;
        a = "ThumbnailURLs"
    .end annotation
.end field

.field public type:I
    .annotation runtime Lkb;
        a = "Type"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toChannels(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/api/service/channels/PsChannel;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/r;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    if-nez p0, :cond_0

    .line 50
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 57
    :goto_0
    return-object v0

    .line 53
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 54
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/service/channels/PsChannel;

    .line 55
    invoke-virtual {v0}, Ltv/periscope/android/api/service/channels/PsChannel;->create()Ltv/periscope/model/r;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 57
    goto :goto_0
.end method


# virtual methods
.method public create()Ltv/periscope/model/r;
    .locals 4

    .prologue
    .line 61
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 62
    iget-object v0, p0, Ltv/periscope/android/api/service/channels/PsChannel;->thumbnails:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Ltv/periscope/android/api/service/channels/PsChannel;->thumbnails:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/service/channels/PsChannelThumbnail;

    .line 64
    invoke-virtual {v0}, Ltv/periscope/android/api/service/channels/PsChannelThumbnail;->create()Ltv/periscope/model/t;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 68
    :cond_0
    invoke-static {}, Ltv/periscope/model/r;->j()Ltv/periscope/model/r$a;

    move-result-object v0

    iget-object v2, p0, Ltv/periscope/android/api/service/channels/PsChannel;->channelId:Ljava/lang/String;

    .line 69
    invoke-virtual {v0, v2}, Ltv/periscope/model/r$a;->a(Ljava/lang/String;)Ltv/periscope/model/r$a;

    move-result-object v0

    iget-object v2, p0, Ltv/periscope/android/api/service/channels/PsChannel;->description:Ljava/lang/String;

    .line 70
    invoke-virtual {v0, v2}, Ltv/periscope/model/r$a;->b(Ljava/lang/String;)Ltv/periscope/model/r$a;

    move-result-object v0

    iget-wide v2, p0, Ltv/periscope/android/api/service/channels/PsChannel;->numberOfLiveStreams:J

    .line 71
    invoke-virtual {v0, v2, v3}, Ltv/periscope/model/r$a;->a(J)Ltv/periscope/model/r$a;

    move-result-object v0

    iget-boolean v2, p0, Ltv/periscope/android/api/service/channels/PsChannel;->featured:Z

    .line 72
    invoke-virtual {v0, v2}, Ltv/periscope/model/r$a;->a(Z)Ltv/periscope/model/r$a;

    move-result-object v0

    iget-object v2, p0, Ltv/periscope/android/api/service/channels/PsChannel;->publicTag:Ljava/lang/String;

    .line 73
    invoke-virtual {v0, v2}, Ltv/periscope/model/r$a;->c(Ljava/lang/String;)Ltv/periscope/model/r$a;

    move-result-object v0

    .line 74
    invoke-virtual {v0, v1}, Ltv/periscope/model/r$a;->a(Ljava/util/List;)Ltv/periscope/model/r$a;

    move-result-object v0

    iget v1, p0, Ltv/periscope/android/api/service/channels/PsChannel;->type:I

    .line 75
    invoke-static {v1}, Ltv/periscope/model/ChannelType;->a(I)Ltv/periscope/model/ChannelType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/r$a;->a(Ltv/periscope/model/ChannelType;)Ltv/periscope/model/r$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/service/channels/PsChannel;->ownerId:Ljava/lang/String;

    .line 76
    invoke-virtual {v0, v1}, Ltv/periscope/model/r$a;->d(Ljava/lang/String;)Ltv/periscope/model/r$a;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Ltv/periscope/model/r$a;->a()Ltv/periscope/model/r;

    move-result-object v0

    .line 78
    iget-object v1, p0, Ltv/periscope/android/api/service/channels/PsChannel;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ltv/periscope/model/r;->a(Ljava/lang/String;)V

    .line 79
    iget-wide v2, p0, Ltv/periscope/android/api/service/channels/PsChannel;->memberCount:J

    invoke-virtual {v0, v2, v3}, Ltv/periscope/model/r;->a(J)V

    .line 80
    return-object v0
.end method
