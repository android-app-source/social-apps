.class public Ltv/periscope/android/api/service/channels/PsChannelMember;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public channelId:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "CID"
    .end annotation
.end field

.field public muted:Z
    .annotation runtime Lkb;
        a = "Muted"
    .end annotation
.end field

.field public userId:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "UserId"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toChannelMembers(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/api/service/channels/PsChannelMember;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 34
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/service/channels/PsChannelMember;

    .line 35
    invoke-virtual {v0}, Ltv/periscope/android/api/service/channels/PsChannelMember;->create()Ltv/periscope/model/s;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 37
    :cond_0
    return-object v1
.end method

.method public static toUserIds(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/android/api/service/channels/PsChannelMember;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 25
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/api/service/channels/PsChannelMember;

    .line 26
    iget-object v0, v0, Ltv/periscope/android/api/service/channels/PsChannelMember;->userId:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 28
    :cond_0
    return-object v1
.end method


# virtual methods
.method public create()Ltv/periscope/model/s;
    .locals 2

    .prologue
    .line 42
    invoke-static {}, Ltv/periscope/model/s;->b()Ltv/periscope/model/s$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/service/channels/PsChannelMember;->userId:Ljava/lang/String;

    .line 43
    invoke-virtual {v0, v1}, Ltv/periscope/model/s$a;->a(Ljava/lang/String;)Ltv/periscope/model/s$a;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Ltv/periscope/model/s$a;->a()Ltv/periscope/model/s;

    move-result-object v0

    .line 45
    iget-boolean v1, p0, Ltv/periscope/android/api/service/channels/PsChannelMember;->muted:Z

    invoke-virtual {v0, v1}, Ltv/periscope/model/s;->a(Z)V

    .line 46
    return-object v0
.end method
