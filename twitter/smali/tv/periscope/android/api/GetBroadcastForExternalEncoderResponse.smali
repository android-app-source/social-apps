.class public Ltv/periscope/android/api/GetBroadcastForExternalEncoderResponse;
.super Ltv/periscope/android/api/PsResponse;
.source "Twttr"


# instance fields
.field public accessChatResponse:Ltv/periscope/android/api/AccessChatResponse;
    .annotation runtime Lkb;
        a = "chat_access"
    .end annotation
.end field

.field public accessVideoResponse:Ltv/periscope/android/api/AccessVideoResponse;
    .annotation runtime Lkb;
        a = "video_access"
    .end annotation
.end field

.field public broadcastResponse:Ltv/periscope/android/api/PsBroadcast;
    .annotation runtime Lkb;
        a = "broadcast"
    .end annotation
.end field

.field public credential:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "credential"
    .end annotation
.end field

.field public streamName:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "stream_name"
    .end annotation
.end field

.field public thumbnailUploadUrl:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "thumbnail_upload_url"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ltv/periscope/android/api/PsResponse;-><init>()V

    return-void
.end method
