.class public Ltv/periscope/android/api/AssociateTweetWithBroadcastRequest;
.super Ltv/periscope/android/api/PsRequest;
.source "Twttr"


# instance fields
.field public final broadcastId:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "broadcast_id"
    .end annotation
.end field

.field public tweetExternal:Z
    .annotation runtime Lkb;
        a = "tweet_external"
    .end annotation
.end field

.field public final tweetId:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "tweet_id"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ltv/periscope/android/api/PsRequest;-><init>()V

    .line 19
    iput-object p1, p0, Ltv/periscope/android/api/AssociateTweetWithBroadcastRequest;->cookie:Ljava/lang/String;

    .line 20
    iput-object p2, p0, Ltv/periscope/android/api/AssociateTweetWithBroadcastRequest;->broadcastId:Ljava/lang/String;

    .line 21
    iput-object p3, p0, Ltv/periscope/android/api/AssociateTweetWithBroadcastRequest;->tweetId:Ljava/lang/String;

    .line 22
    iput-boolean p4, p0, Ltv/periscope/android/api/AssociateTweetWithBroadcastRequest;->tweetExternal:Z

    .line 23
    return-void
.end method
