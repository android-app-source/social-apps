.class public Ltv/periscope/android/api/Features;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public enable360BroadcastRendering:Ljava/lang/Boolean;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lkb;
        a = "enable_360_rendering"
    .end annotation
.end field

.field public moderationEnabled:Z
    .annotation runtime Lkb;
        a = "moderation"
    .end annotation
.end field

.field public numBroadcastsPerGlobalChannel:Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lkb;
        a = "num_broadcasts_per_global_channel"
    .end annotation
.end field

.field public numCuratedGlobalChannels:Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lkb;
        a = "num_curated_global_channels"
    .end annotation
.end field

.field public showSuperfansInterval:Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lkb;
        a = "superfans_prompt_interval"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
