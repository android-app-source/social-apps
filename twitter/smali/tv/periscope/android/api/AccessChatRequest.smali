.class public Ltv/periscope/android/api/AccessChatRequest;
.super Ltv/periscope/android/api/PsRequest;
.source "Twttr"


# instance fields
.field public chatToken:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "chat_token"
    .end annotation
.end field

.field public languages:[Ljava/lang/String;
    .annotation runtime Lkb;
        a = "languages"
    .end annotation
.end field

.field public unlimitedChat:Z
    .annotation runtime Lkb;
        a = "unlimited_chat"
    .end annotation
.end field

.field public viewerModeration:Z
    .annotation runtime Lkb;
        a = "viewer_moderation"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ltv/periscope/android/api/PsRequest;-><init>()V

    .line 27
    return-void
.end method
