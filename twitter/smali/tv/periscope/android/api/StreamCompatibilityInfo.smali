.class public Ltv/periscope/android/api/StreamCompatibilityInfo;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public audioBitrate:I
    .annotation runtime Lkb;
        a = "audio_bitrate"
    .end annotation
.end field

.field public audioCodec:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "audio_codec"
    .end annotation
.end field

.field public audioNumChannels:I
    .annotation runtime Lkb;
        a = "audio_num_channels"
    .end annotation
.end field

.field public audioSamplingRate:I
    .annotation runtime Lkb;
        a = "audio_sampling_rate"
    .end annotation
.end field

.field public complianceViolations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ltv/periscope/android/api/ComplianceViolation;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkb;
        a = "compliance_violations"
    .end annotation
.end field

.field public streamIsCompliant:Z
    .annotation runtime Lkb;
        a = "stream_is_compliant"
    .end annotation
.end field

.field public suggestedSettingsViolations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ltv/periscope/android/api/ComplianceViolation;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkb;
        a = "suggested_violations"
    .end annotation
.end field

.field public videoBitrate:I
    .annotation runtime Lkb;
        a = "video_bitrate"
    .end annotation
.end field

.field public videoCodec:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "video_codec"
    .end annotation
.end field

.field public videoFrameRate:F
    .annotation runtime Lkb;
        a = "video_framerate"
    .end annotation
.end field

.field public videoHeight:F
    .annotation runtime Lkb;
        a = "video_height"
    .end annotation
.end field

.field public videoKeyframeIntervalInSeconds:F
    .annotation runtime Lkb;
        a = "video_keyframe_interval_in_seconds"
    .end annotation
.end field

.field public videoWidth:F
    .annotation runtime Lkb;
        a = "video_width"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
