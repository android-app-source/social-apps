.class Ltv/periscope/android/api/CreateBroadcastResponse;
.super Ltv/periscope/android/api/PsResponse;
.source "Twttr"


# instance fields
.field public accessToken:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "access_token"
    .end annotation
.end field

.field public application:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "application"
    .end annotation
.end field

.field public broadcast:Ltv/periscope/android/api/PsBroadcast;
    .annotation runtime Lkb;
        a = "broadcast"
    .end annotation
.end field

.field public canShareTwitter:Z
    .annotation runtime Lkb;
        a = "can_share_twitter"
    .end annotation
.end field

.field public channel:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "channel"
    .end annotation
.end field

.field public channelPerms:Ltv/periscope/android/api/ChannelPermissions;
    .annotation runtime Lkb;
        a = "chan_perms"
    .end annotation
.end field

.field public cipher:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "cipher"
    .end annotation
.end field

.field public credential:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "credential"
    .end annotation
.end field

.field public endpoint:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "endpoint"
    .end annotation
.end field

.field public host:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "host"
    .end annotation
.end field

.field public key:[B
    .annotation runtime Lkb;
        a = "key"
    .end annotation
.end field

.field public participantIndex:J
    .annotation runtime Lkb;
        a = "participant_index"
    .end annotation
.end field

.field public port:I
    .annotation runtime Lkb;
        a = "port"
    .end annotation
.end field

.field public privatePort:I
    .annotation runtime Lkb;
        a = "private_port"
    .end annotation
.end field

.field public privateProtocol:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "private_protocol"
    .end annotation
.end field

.field public protocol:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "protocol"
    .end annotation
.end field

.field public pspVersion:[I
    .annotation runtime Lkb;
        a = "psp_version"
    .end annotation
.end field

.field public readOnly:Z
    .annotation runtime Lkb;
        a = "read_only"
    .end annotation
.end field

.field public replayAccessToken:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "replay_access_token"
    .end annotation
.end field

.field public replayEndpoint:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "replay_endpoint"
    .end annotation
.end field

.field public roomId:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "room_id"
    .end annotation
.end field

.field public shareUrl:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "share_url"
    .end annotation
.end field

.field public shouldLog:Z
    .annotation runtime Lkb;
        a = "should_log"
    .end annotation
.end field

.field public streamName:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "stream_name"
    .end annotation
.end field

.field public thumbnailUploadUrl:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "thumbnail_upload_url"
    .end annotation
.end field

.field public uploadUrl:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "upload_url"
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ltv/periscope/android/api/PsResponse;-><init>()V

    return-void
.end method


# virtual methods
.method public create()Ltv/periscope/model/w;
    .locals 28

    .prologue
    .line 87
    move-object/from16 v0, p0

    iget-object v3, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->cipher:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v4, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->participantIndex:J

    move-object/from16 v0, p0

    iget-object v6, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->roomId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->shouldLog:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->broadcast:Ltv/periscope/android/api/PsBroadcast;

    .line 92
    invoke-virtual {v2}, Ltv/periscope/android/api/PsBroadcast;->create()Ltv/periscope/model/p;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->protocol:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->host:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v11, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->port:I

    move-object/from16 v0, p0

    iget-object v12, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->application:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->streamName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->credential:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->privateProtocol:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v0, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->privatePort:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->uploadUrl:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->thumbnailUploadUrl:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->canShareTwitter:Z

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->accessToken:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->replayAccessToken:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->key:[B

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->endpoint:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->replayEndpoint:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->channelPerms:Ltv/periscope/android/api/ChannelPermissions;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->channelPerms:Ltv/periscope/android/api/ChannelPermissions;

    iget v0, v2, Ltv/periscope/android/api/ChannelPermissions;->chatmanOpts:I

    move/from16 v25, v0

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->pspVersion:[I

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ltv/periscope/android/api/CreateBroadcastResponse;->shareUrl:Ljava/lang/String;

    move-object/from16 v27, v0

    .line 87
    invoke-static/range {v3 .. v27}, Ltv/periscope/model/w;->a(Ljava/lang/String;JLjava/lang/String;ZLtv/periscope/model/p;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;I[ILjava/lang/String;)Ltv/periscope/model/w;

    move-result-object v2

    return-object v2

    .line 92
    :cond_0
    const/16 v25, 0x0

    goto :goto_0
.end method
