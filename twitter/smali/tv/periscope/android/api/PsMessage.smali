.class public Ltv/periscope/android/api/PsMessage;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final blockedMessageUUID:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "blockedMessageUUID"
    .end annotation
.end field

.field public final body:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "body"
    .end annotation
.end field

.field public final broadcasterBlockedMessage:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "broadcasterBlockedMessageBody"
    .end annotation
.end field

.field public final broadcasterBlockedUserId:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "broadcasterBlockedRemoteID"
    .end annotation
.end field

.field public final broadcasterBlockedUsername:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "broadcasterBlockedUsername"
    .end annotation
.end field

.field public final broadcasterNtp:Ljava/math/BigInteger;
    .annotation runtime Lkb;
        a = "ntpForBroadcasterFrame"
    .end annotation
.end field

.field public final initials:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "initials"
    .end annotation
.end field

.field public final invitedCount:Ljava/lang/Long;
    .annotation runtime Lkb;
        a = "invited_count"
    .end annotation
.end field

.field public final juryDuration:Ljava/lang/Long;
    .annotation runtime Lkb;
        a = "jury_duration_sec"
    .end annotation
.end field

.field public final juryVerdict:I
    .annotation runtime Lkb;
        a = "verdict"
    .end annotation
.end field

.field public final latitude:Ljava/lang/Double;
    .annotation runtime Lkb;
        a = "lat"
    .end annotation
.end field

.field public final longitude:Ljava/lang/Double;
    .annotation runtime Lkb;
        a = "lng"
    .end annotation
.end field

.field public final ntpForLiveFrame:Ljava/math/BigInteger;
    .annotation runtime Lkb;
        a = "ntpForLiveFrame"
    .end annotation
.end field

.field public final reportType:I
    .annotation runtime Lkb;
        a = "report_type"
    .end annotation
.end field

.field public final reportedMessageBody:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "message_body"
    .end annotation
.end field

.field public final reportedMessageBroadcastID:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "broadcast_id"
    .end annotation
.end field

.field public final reportedMessageUUID:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "message_uuid"
    .end annotation
.end field

.field public final sentenceDuration:Ljava/lang/Long;
    .annotation runtime Lkb;
        a = "sentence_duration_sec"
    .end annotation
.end field

.field public final sentenceType:Ljava/lang/Integer;
    .annotation runtime Lkb;
        a = "sentence_type"
    .end annotation
.end field

.field public final signature:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "signature"
    .end annotation
.end field

.field public final timestamp:Ljava/lang/Long;
    .annotation runtime Lkb;
        a = "timestamp"
    .end annotation
.end field

.field public final timestampPlaybackOffset:Ljava/lang/Double;
    .annotation runtime Lkb;
        a = "timestampPlaybackOffset"
    .end annotation
.end field

.field public final type:I
    .annotation runtime Lkb;
        a = "type"
    .end annotation
.end field

.field public final uuid:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "uuid"
    .end annotation
.end field

.field public final version:Ljava/lang/Integer;
    .annotation runtime Lkb;
        a = "v"
    .end annotation
.end field

.field public final viewerBlockedMessage:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "viewerBlockedMessage"
    .end annotation
.end field

.field public final viewerBlockedUserId:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "viewerBlockedUserId"
    .end annotation
.end field

.field public final viewerBlockedUsername:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "viewerBlockedUsername"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ltv/periscope/model/chat/Message;)V
    .locals 4

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/model/chat/MessageType;->ordinal()I

    move-result v0

    iput v0, p0, Ltv/periscope/android/api/PsMessage;->type:I

    .line 104
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->a()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->version:Ljava/lang/Integer;

    .line 105
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->uuid:Ljava/lang/String;

    .line 106
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->f()Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->ntpForLiveFrame:Ljava/math/BigInteger;

    .line 107
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->body:Ljava/lang/String;

    .line 108
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->initials:Ljava/lang/String;

    .line 109
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->h()Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->timestamp:Ljava/lang/Long;

    .line 110
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->signature:Ljava/lang/String;

    .line 111
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->o()Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->timestampPlaybackOffset:Ljava/lang/Double;

    .line 112
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->p()Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->latitude:Ljava/lang/Double;

    .line 113
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->q()Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->longitude:Ljava/lang/Double;

    .line 114
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->r()Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->invitedCount:Ljava/lang/Long;

    .line 115
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->broadcasterBlockedMessage:Ljava/lang/String;

    .line 116
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->broadcasterBlockedUserId:Ljava/lang/String;

    .line 117
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->broadcasterBlockedUsername:Ljava/lang/String;

    .line 118
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->v()Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->broadcasterNtp:Ljava/math/BigInteger;

    .line 119
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->w()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->blockedMessageUUID:Ljava/lang/String;

    .line 120
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->x()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->viewerBlockedMessage:Ljava/lang/String;

    .line 121
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->y()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->viewerBlockedUserId:Ljava/lang/String;

    .line 122
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->z()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->viewerBlockedUsername:Ljava/lang/String;

    .line 123
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->A()Ltv/periscope/model/chat/MessageType$ReportType;

    move-result-object v0

    invoke-direct {p0, v0}, Ltv/periscope/android/api/PsMessage;->safe(Ltv/periscope/model/chat/MessageType$ReportType;)Ltv/periscope/model/chat/MessageType$ReportType;

    move-result-object v0

    iget v0, v0, Ltv/periscope/model/chat/MessageType$ReportType;->value:I

    iput v0, p0, Ltv/periscope/android/api/PsMessage;->reportType:I

    .line 124
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->B()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->reportedMessageUUID:Ljava/lang/String;

    .line 125
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->C()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->reportedMessageBody:Ljava/lang/String;

    .line 126
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->E()Ltv/periscope/model/chat/MessageType$VerdictType;

    move-result-object v0

    invoke-direct {p0, v0}, Ltv/periscope/android/api/PsMessage;->safe(Ltv/periscope/model/chat/MessageType$VerdictType;)Ltv/periscope/model/chat/MessageType$VerdictType;

    move-result-object v0

    iget v0, v0, Ltv/periscope/model/chat/MessageType$VerdictType;->value:I

    iput v0, p0, Ltv/periscope/android/api/PsMessage;->juryVerdict:I

    .line 127
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->F()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->reportedMessageBroadcastID:Ljava/lang/String;

    .line 128
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->G()Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v1}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->juryDuration:Ljava/lang/Long;

    .line 129
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->H()Ltv/periscope/model/chat/MessageType$SentenceType;

    move-result-object v0

    invoke-direct {p0, v0}, Ltv/periscope/android/api/PsMessage;->safe(Ltv/periscope/model/chat/MessageType$SentenceType;)Ltv/periscope/model/chat/MessageType$SentenceType;

    move-result-object v0

    iget v0, v0, Ltv/periscope/model/chat/MessageType$SentenceType;->value:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->sentenceType:Ljava/lang/Integer;

    .line 130
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->I()Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v1}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Ltv/periscope/android/api/PsMessage;->sentenceDuration:Ljava/lang/Long;

    .line 131
    return-void
.end method

.method private safe(Ljava/lang/Double;)Ljava/lang/Double;
    .locals 2

    .prologue
    .line 216
    if-nez p1, :cond_0

    .line 217
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    .line 219
    :cond_0
    return-object p1
.end method

.method private safe(Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 209
    if-nez p1, :cond_0

    .line 210
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 212
    :cond_0
    return-object p1
.end method

.method private safe(Ljava/lang/Long;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 202
    if-nez p1, :cond_0

    .line 203
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    .line 205
    :cond_0
    return-object p1
.end method

.method private safe(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    invoke-static {p1}, Ldcq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private safe(Ljava/math/BigInteger;)Ljava/math/BigInteger;
    .locals 0

    .prologue
    .line 223
    if-nez p1, :cond_0

    .line 224
    sget-object p1, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    .line 226
    :cond_0
    return-object p1
.end method

.method private safe(Ltv/periscope/model/chat/MessageType$ReportType;)Ltv/periscope/model/chat/MessageType$ReportType;
    .locals 0

    .prologue
    .line 180
    if-nez p1, :cond_0

    .line 181
    sget-object p1, Ltv/periscope/model/chat/MessageType$ReportType;->a:Ltv/periscope/model/chat/MessageType$ReportType;

    .line 183
    :cond_0
    return-object p1
.end method

.method private safe(Ltv/periscope/model/chat/MessageType$SentenceType;)Ltv/periscope/model/chat/MessageType$SentenceType;
    .locals 0

    .prologue
    .line 196
    if-nez p1, :cond_0

    .line 197
    sget-object p1, Ltv/periscope/model/chat/MessageType$SentenceType;->a:Ltv/periscope/model/chat/MessageType$SentenceType;

    .line 199
    :cond_0
    return-object p1
.end method

.method private safe(Ltv/periscope/model/chat/MessageType$VerdictType;)Ltv/periscope/model/chat/MessageType$VerdictType;
    .locals 0

    .prologue
    .line 188
    if-nez p1, :cond_0

    .line 189
    sget-object p1, Ltv/periscope/model/chat/MessageType$VerdictType;->a:Ltv/periscope/model/chat/MessageType$VerdictType;

    .line 191
    :cond_0
    return-object p1
.end method


# virtual methods
.method public toMessage(Ltv/periscope/chatman/model/j;)Ltv/periscope/model/chat/Message;
    .locals 5

    .prologue
    .line 137
    invoke-virtual {p1}, Ltv/periscope/chatman/model/j;->c()Ltv/periscope/chatman/api/Sender;

    move-result-object v0

    .line 138
    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/PsMessage;->version:Ljava/lang/Integer;

    .line 139
    invoke-direct {p0, v2}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->a(Ljava/lang/Integer;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget v2, p0, Ltv/periscope/android/api/PsMessage;->type:I

    .line 140
    invoke-static {v2}, Ltv/periscope/model/chat/MessageType;->a(I)Ltv/periscope/model/chat/MessageType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-object v2, v0, Ltv/periscope/chatman/api/Sender;->userId:Ljava/lang/String;

    .line 141
    invoke-direct {p0, v2}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->a(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-object v2, v0, Ltv/periscope/chatman/api/Sender;->twitterId:Ljava/lang/String;

    .line 142
    invoke-direct {p0, v2}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->b(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-object v2, v0, Ltv/periscope/chatman/api/Sender;->participantIndex:Ljava/lang/Long;

    .line 143
    invoke-direct {p0, v2}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->a(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-boolean v2, v0, Ltv/periscope/chatman/api/Sender;->superfan:Z

    .line 144
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->a(Ljava/lang/Boolean;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/PsMessage;->ntpForLiveFrame:Ljava/math/BigInteger;

    .line 145
    invoke-direct {p0, v2}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->a(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/PsMessage;->uuid:Ljava/lang/String;

    .line 146
    invoke-direct {p0, v2}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->c(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-object v2, p0, Ltv/periscope/android/api/PsMessage;->timestamp:Ljava/lang/Long;

    .line 147
    invoke-direct {p0, v2}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->b(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-object v2, v0, Ltv/periscope/chatman/api/Sender;->username:Ljava/lang/String;

    .line 148
    invoke-direct {p0, v2}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->d(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-object v2, v0, Ltv/periscope/chatman/api/Sender;->displayName:Ljava/lang/String;

    .line 149
    invoke-direct {p0, v2}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->e(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-object v2, v0, Ltv/periscope/chatman/api/Sender;->profileImageUrl:Ljava/lang/String;

    .line 150
    invoke-direct {p0, v2}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->g(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-object v2, v0, Ltv/periscope/chatman/api/Sender;->vipBadge:Ljava/lang/String;

    .line 151
    invoke-direct {p0, v2}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->u(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-boolean v0, v0, Ltv/periscope/chatman/api/Sender;->newUser:Z

    .line 152
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v0}, Ltv/periscope/model/chat/Message$a;->b(Ljava/lang/Boolean;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsMessage;->body:Ljava/lang/String;

    .line 153
    invoke-direct {p0, v1}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->h(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsMessage;->timestampPlaybackOffset:Ljava/lang/Double;

    .line 154
    invoke-direct {p0, v1}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/Double;)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ljava/lang/Double;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsMessage;->latitude:Ljava/lang/Double;

    .line 155
    invoke-direct {p0, v1}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/Double;)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/lang/Double;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsMessage;->longitude:Ljava/lang/Double;

    .line 156
    invoke-direct {p0, v1}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/Double;)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->c(Ljava/lang/Double;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsMessage;->invitedCount:Ljava/lang/Long;

    .line 157
    invoke-direct {p0, v1}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->c(Ljava/lang/Long;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsMessage;->broadcasterBlockedMessage:Ljava/lang/String;

    .line 158
    invoke-direct {p0, v1}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->i(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsMessage;->broadcasterBlockedUserId:Ljava/lang/String;

    .line 159
    invoke-direct {p0, v1}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->j(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsMessage;->broadcasterBlockedUsername:Ljava/lang/String;

    .line 160
    invoke-direct {p0, v1}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->k(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsMessage;->broadcasterNtp:Ljava/math/BigInteger;

    .line 161
    invoke-direct {p0, v1}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/math/BigInteger;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsMessage;->blockedMessageUUID:Ljava/lang/String;

    .line 162
    invoke-direct {p0, v1}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->l(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsMessage;->viewerBlockedMessage:Ljava/lang/String;

    .line 163
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->m(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsMessage;->viewerBlockedUserId:Ljava/lang/String;

    .line 164
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->n(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsMessage;->viewerBlockedUsername:Ljava/lang/String;

    .line 165
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->o(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget v1, p0, Ltv/periscope/android/api/PsMessage;->reportType:I

    .line 166
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v1}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Ltv/periscope/model/chat/MessageType$ReportType;->a(I)Ltv/periscope/model/chat/MessageType$ReportType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType$ReportType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsMessage;->reportedMessageUUID:Ljava/lang/String;

    .line 167
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->p(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsMessage;->reportedMessageBody:Ljava/lang/String;

    .line 168
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->q(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget v1, p0, Ltv/periscope/android/api/PsMessage;->juryVerdict:I

    .line 169
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v1}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Ltv/periscope/model/chat/MessageType$VerdictType;->a(I)Ltv/periscope/model/chat/MessageType$VerdictType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType$VerdictType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsMessage;->reportedMessageBroadcastID:Ljava/lang/String;

    .line 170
    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->s(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Ltv/periscope/android/api/PsMessage;->juryDuration:Ljava/lang/Long;

    .line 171
    invoke-direct {p0, v2}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->b(Ljava/lang/Integer;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/android/api/PsMessage;->sentenceType:Ljava/lang/Integer;

    .line 172
    invoke-direct {p0, v1}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Ltv/periscope/model/chat/MessageType$SentenceType;->a(I)Ltv/periscope/model/chat/MessageType$SentenceType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType$SentenceType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Ltv/periscope/android/api/PsMessage;->sentenceDuration:Ljava/lang/Long;

    .line 173
    invoke-direct {p0, v2}, Ltv/periscope/android/api/PsMessage;->safe(Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->c(Ljava/lang/Integer;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 174
    invoke-virtual {p1}, Ltv/periscope/chatman/model/j;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/model/chat/Message$a;->t(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v0

    .line 175
    invoke-virtual {v0}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v0

    .line 138
    return-object v0
.end method
