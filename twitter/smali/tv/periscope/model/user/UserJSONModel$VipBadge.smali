.class public final enum Ltv/periscope/model/user/UserJSONModel$VipBadge;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/periscope/model/user/UserJSONModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VipBadge"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ltv/periscope/model/user/UserJSONModel$VipBadge;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Ltv/periscope/model/user/UserJSONModel$VipBadge;

.field public static final enum BRONZE:Ltv/periscope/model/user/UserJSONModel$VipBadge;
    .annotation runtime Lkb;
        a = "bronze"
    .end annotation
.end field

.field public static final enum GOLD:Ltv/periscope/model/user/UserJSONModel$VipBadge;
    .annotation runtime Lkb;
        a = "gold"
    .end annotation
.end field

.field public static final enum NONE:Ltv/periscope/model/user/UserJSONModel$VipBadge;

.field public static final enum SILVER:Ltv/periscope/model/user/UserJSONModel$VipBadge;
    .annotation runtime Lkb;
        a = "silver"
    .end annotation
.end field


# instance fields
.field public final type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 116
    new-instance v0, Ltv/periscope/model/user/UserJSONModel$VipBadge;

    const-string/jumbo v1, "NONE"

    const-string/jumbo v2, ""

    invoke-direct {v0, v1, v3, v2}, Ltv/periscope/model/user/UserJSONModel$VipBadge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ltv/periscope/model/user/UserJSONModel$VipBadge;->NONE:Ltv/periscope/model/user/UserJSONModel$VipBadge;

    .line 118
    new-instance v0, Ltv/periscope/model/user/UserJSONModel$VipBadge;

    const-string/jumbo v1, "BRONZE"

    const-string/jumbo v2, "bronze"

    invoke-direct {v0, v1, v4, v2}, Ltv/periscope/model/user/UserJSONModel$VipBadge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ltv/periscope/model/user/UserJSONModel$VipBadge;->BRONZE:Ltv/periscope/model/user/UserJSONModel$VipBadge;

    .line 121
    new-instance v0, Ltv/periscope/model/user/UserJSONModel$VipBadge;

    const-string/jumbo v1, "SILVER"

    const-string/jumbo v2, "silver"

    invoke-direct {v0, v1, v5, v2}, Ltv/periscope/model/user/UserJSONModel$VipBadge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ltv/periscope/model/user/UserJSONModel$VipBadge;->SILVER:Ltv/periscope/model/user/UserJSONModel$VipBadge;

    .line 124
    new-instance v0, Ltv/periscope/model/user/UserJSONModel$VipBadge;

    const-string/jumbo v1, "GOLD"

    const-string/jumbo v2, "gold"

    invoke-direct {v0, v1, v6, v2}, Ltv/periscope/model/user/UserJSONModel$VipBadge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ltv/periscope/model/user/UserJSONModel$VipBadge;->GOLD:Ltv/periscope/model/user/UserJSONModel$VipBadge;

    .line 115
    const/4 v0, 0x4

    new-array v0, v0, [Ltv/periscope/model/user/UserJSONModel$VipBadge;

    sget-object v1, Ltv/periscope/model/user/UserJSONModel$VipBadge;->NONE:Ltv/periscope/model/user/UserJSONModel$VipBadge;

    aput-object v1, v0, v3

    sget-object v1, Ltv/periscope/model/user/UserJSONModel$VipBadge;->BRONZE:Ltv/periscope/model/user/UserJSONModel$VipBadge;

    aput-object v1, v0, v4

    sget-object v1, Ltv/periscope/model/user/UserJSONModel$VipBadge;->SILVER:Ltv/periscope/model/user/UserJSONModel$VipBadge;

    aput-object v1, v0, v5

    sget-object v1, Ltv/periscope/model/user/UserJSONModel$VipBadge;->GOLD:Ltv/periscope/model/user/UserJSONModel$VipBadge;

    aput-object v1, v0, v6

    sput-object v0, Ltv/periscope/model/user/UserJSONModel$VipBadge;->$VALUES:[Ltv/periscope/model/user/UserJSONModel$VipBadge;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 130
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 131
    iput-object p3, p0, Ltv/periscope/model/user/UserJSONModel$VipBadge;->type:Ljava/lang/String;

    .line 132
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ltv/periscope/model/user/UserJSONModel$VipBadge;
    .locals 1

    .prologue
    .line 115
    const-class v0, Ltv/periscope/model/user/UserJSONModel$VipBadge;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/user/UserJSONModel$VipBadge;

    return-object v0
.end method

.method public static values()[Ltv/periscope/model/user/UserJSONModel$VipBadge;
    .locals 1

    .prologue
    .line 115
    sget-object v0, Ltv/periscope/model/user/UserJSONModel$VipBadge;->$VALUES:[Ltv/periscope/model/user/UserJSONModel$VipBadge;

    invoke-virtual {v0}, [Ltv/periscope/model/user/UserJSONModel$VipBadge;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ltv/periscope/model/user/UserJSONModel$VipBadge;

    return-object v0
.end method
