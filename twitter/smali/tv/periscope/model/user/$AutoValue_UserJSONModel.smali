.class abstract Ltv/periscope/model/user/$AutoValue_UserJSONModel;
.super Ltv/periscope/model/user/UserJSONModel;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/model/user/$AutoValue_UserJSONModel$Builder;
    }
.end annotation


# instance fields
.field private final className:Ljava/lang/String;

.field private final createdAt:Ljava/lang/String;

.field private final description:Ljava/lang/String;

.field private final displayName:Ljava/lang/String;

.field private final hasDigitsId:Ljava/lang/Boolean;

.field private final id:Ljava/lang/String;

.field private final initials:Ljava/lang/String;

.field private final isBlocked:Ljava/lang/Boolean;

.field private final isBluebirdUser:Ljava/lang/Boolean;

.field private final isEmployee:Ljava/lang/Boolean;

.field private final isFollowing:Ljava/lang/Boolean;

.field private final isMuted:Ljava/lang/Boolean;

.field private final isVerified:Ljava/lang/Boolean;

.field private final numFollowers:Ljava/lang/Long;

.field private final numFollowing:Ljava/lang/Long;

.field private final numHearts:Ljava/lang/Long;

.field private final numHeartsGiven:Ljava/lang/Long;

.field private final participantIndex:Ljava/lang/Long;

.field private final profileImageUrls:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/ProfileImageUrlJSONModel;",
            ">;"
        }
    .end annotation
.end field

.field private final twitterId:Ljava/lang/String;

.field private final twitterUsername:Ljava/lang/String;

.field private final updatedAt:Ljava/lang/String;

.field private final username:Ljava/lang/String;

.field private final vipBadge:Ltv/periscope/model/user/UserJSONModel$VipBadge;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/user/UserJSONModel$VipBadge;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/ProfileImageUrlJSONModel;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ltv/periscope/model/user/UserJSONModel$VipBadge;",
            ")V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0}, Ltv/periscope/model/user/UserJSONModel;-><init>()V

    .line 63
    iput-object p1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->className:Ljava/lang/String;

    .line 64
    iput-object p2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->id:Ljava/lang/String;

    .line 65
    iput-object p3, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->createdAt:Ljava/lang/String;

    .line 66
    iput-object p4, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->updatedAt:Ljava/lang/String;

    .line 67
    iput-object p5, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->username:Ljava/lang/String;

    .line 68
    iput-object p6, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->displayName:Ljava/lang/String;

    .line 69
    iput-object p7, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->initials:Ljava/lang/String;

    .line 70
    iput-object p8, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->description:Ljava/lang/String;

    .line 71
    iput-object p9, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->profileImageUrls:Ljava/util/List;

    .line 72
    iput-object p10, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numFollowers:Ljava/lang/Long;

    .line 73
    iput-object p11, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numFollowing:Ljava/lang/Long;

    .line 74
    iput-object p12, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isFollowing:Ljava/lang/Boolean;

    .line 75
    iput-object p13, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isMuted:Ljava/lang/Boolean;

    .line 76
    iput-object p14, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isBlocked:Ljava/lang/Boolean;

    .line 77
    move-object/from16 v0, p15

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->hasDigitsId:Ljava/lang/Boolean;

    .line 78
    move-object/from16 v0, p16

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numHearts:Ljava/lang/Long;

    .line 79
    move-object/from16 v0, p17

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isEmployee:Ljava/lang/Boolean;

    .line 80
    move-object/from16 v0, p18

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numHeartsGiven:Ljava/lang/Long;

    .line 81
    move-object/from16 v0, p19

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->participantIndex:Ljava/lang/Long;

    .line 82
    move-object/from16 v0, p20

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isVerified:Ljava/lang/Boolean;

    .line 83
    move-object/from16 v0, p21

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isBluebirdUser:Ljava/lang/Boolean;

    .line 84
    move-object/from16 v0, p22

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->twitterUsername:Ljava/lang/String;

    .line 85
    move-object/from16 v0, p23

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->twitterId:Ljava/lang/String;

    .line 86
    move-object/from16 v0, p24

    iput-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->vipBadge:Ltv/periscope/model/user/UserJSONModel$VipBadge;

    .line 87
    return-void
.end method


# virtual methods
.method public className()Ljava/lang/String;
    .locals 1
    .annotation runtime Lkb;
        a = "class_name"
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->className:Ljava/lang/String;

    return-object v0
.end method

.method public createdAt()Ljava/lang/String;
    .locals 1
    .annotation runtime Lkb;
        a = "created_at"
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->createdAt:Ljava/lang/String;

    return-object v0
.end method

.method public description()Ljava/lang/String;
    .locals 1
    .annotation runtime Lkb;
        a = "description"
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->description:Ljava/lang/String;

    return-object v0
.end method

.method public displayName()Ljava/lang/String;
    .locals 1
    .annotation runtime Lkb;
        a = "display_name"
    .end annotation

    .prologue
    .line 123
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 283
    if-ne p1, p0, :cond_1

    .line 313
    :cond_0
    :goto_0
    return v0

    .line 286
    :cond_1
    instance-of v2, p1, Ltv/periscope/model/user/UserJSONModel;

    if-eqz v2, :cond_15

    .line 287
    check-cast p1, Ltv/periscope/model/user/UserJSONModel;

    .line 288
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->className:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->className()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->id:Ljava/lang/String;

    .line 289
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->id()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->createdAt:Ljava/lang/String;

    .line 290
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->createdAt()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->updatedAt:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 291
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->updatedAt()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->username:Ljava/lang/String;

    .line 292
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->username()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->displayName:Ljava/lang/String;

    .line 293
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->displayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->initials:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 294
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->initials()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_2
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->description:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 295
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->description()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_3
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->profileImageUrls:Ljava/util/List;

    .line 296
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->profileImageUrls()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numFollowers:Ljava/lang/Long;

    if-nez v2, :cond_6

    .line 297
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->numFollowers()Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_4
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numFollowing:Ljava/lang/Long;

    if-nez v2, :cond_7

    .line 298
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->numFollowing()Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_5
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isFollowing:Ljava/lang/Boolean;

    if-nez v2, :cond_8

    .line 299
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->isFollowing()Ljava/lang/Boolean;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_6
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isMuted:Ljava/lang/Boolean;

    if-nez v2, :cond_9

    .line 300
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->isMuted()Ljava/lang/Boolean;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_7
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isBlocked:Ljava/lang/Boolean;

    if-nez v2, :cond_a

    .line 301
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->isBlocked()Ljava/lang/Boolean;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_8
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->hasDigitsId:Ljava/lang/Boolean;

    if-nez v2, :cond_b

    .line 302
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->hasDigitsId()Ljava/lang/Boolean;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_9
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numHearts:Ljava/lang/Long;

    if-nez v2, :cond_c

    .line 303
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->numHearts()Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_a
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isEmployee:Ljava/lang/Boolean;

    if-nez v2, :cond_d

    .line 304
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->isEmployee()Ljava/lang/Boolean;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_b
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numHeartsGiven:Ljava/lang/Long;

    if-nez v2, :cond_e

    .line 305
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->numHeartsGiven()Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_c
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->participantIndex:Ljava/lang/Long;

    if-nez v2, :cond_f

    .line 306
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->participantIndex()Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_d
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isVerified:Ljava/lang/Boolean;

    if-nez v2, :cond_10

    .line 307
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->isVerified()Ljava/lang/Boolean;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_e
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isBluebirdUser:Ljava/lang/Boolean;

    if-nez v2, :cond_11

    .line 308
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->isBluebirdUser()Ljava/lang/Boolean;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_f
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->twitterUsername:Ljava/lang/String;

    if-nez v2, :cond_12

    .line 309
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->twitterUsername()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_10
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->twitterId:Ljava/lang/String;

    if-nez v2, :cond_13

    .line 310
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->twitterId()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_11
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->vipBadge:Ltv/periscope/model/user/UserJSONModel$VipBadge;

    if-nez v2, :cond_14

    .line 311
    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->vipBadge()Ltv/periscope/model/user/UserJSONModel$VipBadge;

    move-result-object v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto/16 :goto_0

    .line 291
    :cond_3
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->updatedAt:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->updatedAt()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_1

    .line 294
    :cond_4
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->initials:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->initials()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_2

    .line 295
    :cond_5
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->description:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->description()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_3

    .line 297
    :cond_6
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numFollowers:Ljava/lang/Long;

    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->numFollowers()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_4

    .line 298
    :cond_7
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numFollowing:Ljava/lang/Long;

    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->numFollowing()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_5

    .line 299
    :cond_8
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isFollowing:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->isFollowing()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_6

    .line 300
    :cond_9
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isMuted:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->isMuted()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_7

    .line 301
    :cond_a
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isBlocked:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->isBlocked()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_8

    .line 302
    :cond_b
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->hasDigitsId:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->hasDigitsId()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_9

    .line 303
    :cond_c
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numHearts:Ljava/lang/Long;

    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->numHearts()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_a

    .line 304
    :cond_d
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isEmployee:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->isEmployee()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_b

    .line 305
    :cond_e
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numHeartsGiven:Ljava/lang/Long;

    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->numHeartsGiven()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_c

    .line 306
    :cond_f
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->participantIndex:Ljava/lang/Long;

    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->participantIndex()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_d

    .line 307
    :cond_10
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isVerified:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->isVerified()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_e

    .line 308
    :cond_11
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isBluebirdUser:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->isBluebirdUser()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_f

    .line 309
    :cond_12
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->twitterUsername:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->twitterUsername()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_10

    .line 310
    :cond_13
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->twitterId:Ljava/lang/String;

    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->twitterId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_11

    .line 311
    :cond_14
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->vipBadge:Ltv/periscope/model/user/UserJSONModel$VipBadge;

    invoke-virtual {p1}, Ltv/periscope/model/user/UserJSONModel;->vipBadge()Ltv/periscope/model/user/UserJSONModel$VipBadge;

    move-result-object v3

    invoke-virtual {v2, v3}, Ltv/periscope/model/user/UserJSONModel$VipBadge;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_0

    :cond_15
    move v0, v1

    .line 313
    goto/16 :goto_0
.end method

.method public hasDigitsId()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lkb;
        a = "has_digits_id"
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->hasDigitsId:Ljava/lang/Boolean;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const v3, 0xf4243

    .line 318
    .line 320
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->className:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    xor-int/2addr v0, v3

    .line 321
    mul-int/2addr v0, v3

    .line 322
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->id:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    .line 323
    mul-int/2addr v0, v3

    .line 324
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->createdAt:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    .line 325
    mul-int v2, v0, v3

    .line 326
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->updatedAt:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    xor-int/2addr v0, v2

    .line 327
    mul-int/2addr v0, v3

    .line 328
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->username:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    .line 329
    mul-int/2addr v0, v3

    .line 330
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->displayName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    .line 331
    mul-int v2, v0, v3

    .line 332
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->initials:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    xor-int/2addr v0, v2

    .line 333
    mul-int v2, v0, v3

    .line 334
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->description:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    xor-int/2addr v0, v2

    .line 335
    mul-int/2addr v0, v3

    .line 336
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->profileImageUrls:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    .line 337
    mul-int v2, v0, v3

    .line 338
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numFollowers:Ljava/lang/Long;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    xor-int/2addr v0, v2

    .line 339
    mul-int v2, v0, v3

    .line 340
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numFollowing:Ljava/lang/Long;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    xor-int/2addr v0, v2

    .line 341
    mul-int v2, v0, v3

    .line 342
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isFollowing:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    xor-int/2addr v0, v2

    .line 343
    mul-int v2, v0, v3

    .line 344
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isMuted:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    xor-int/2addr v0, v2

    .line 345
    mul-int v2, v0, v3

    .line 346
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isBlocked:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    xor-int/2addr v0, v2

    .line 347
    mul-int v2, v0, v3

    .line 348
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->hasDigitsId:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    xor-int/2addr v0, v2

    .line 349
    mul-int v2, v0, v3

    .line 350
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numHearts:Ljava/lang/Long;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    xor-int/2addr v0, v2

    .line 351
    mul-int v2, v0, v3

    .line 352
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isEmployee:Ljava/lang/Boolean;

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    xor-int/2addr v0, v2

    .line 353
    mul-int v2, v0, v3

    .line 354
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numHeartsGiven:Ljava/lang/Long;

    if-nez v0, :cond_b

    move v0, v1

    :goto_b
    xor-int/2addr v0, v2

    .line 355
    mul-int v2, v0, v3

    .line 356
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->participantIndex:Ljava/lang/Long;

    if-nez v0, :cond_c

    move v0, v1

    :goto_c
    xor-int/2addr v0, v2

    .line 357
    mul-int v2, v0, v3

    .line 358
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isVerified:Ljava/lang/Boolean;

    if-nez v0, :cond_d

    move v0, v1

    :goto_d
    xor-int/2addr v0, v2

    .line 359
    mul-int v2, v0, v3

    .line 360
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isBluebirdUser:Ljava/lang/Boolean;

    if-nez v0, :cond_e

    move v0, v1

    :goto_e
    xor-int/2addr v0, v2

    .line 361
    mul-int v2, v0, v3

    .line 362
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->twitterUsername:Ljava/lang/String;

    if-nez v0, :cond_f

    move v0, v1

    :goto_f
    xor-int/2addr v0, v2

    .line 363
    mul-int v2, v0, v3

    .line 364
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->twitterId:Ljava/lang/String;

    if-nez v0, :cond_10

    move v0, v1

    :goto_10
    xor-int/2addr v0, v2

    .line 365
    mul-int/2addr v0, v3

    .line 366
    iget-object v2, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->vipBadge:Ltv/periscope/model/user/UserJSONModel$VipBadge;

    if-nez v2, :cond_11

    :goto_11
    xor-int/2addr v0, v1

    .line 367
    return v0

    .line 326
    :cond_0
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->updatedAt:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 332
    :cond_1
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->initials:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_1

    .line 334
    :cond_2
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->description:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_2

    .line 338
    :cond_3
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numFollowers:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto/16 :goto_3

    .line 340
    :cond_4
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numFollowing:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto/16 :goto_4

    .line 342
    :cond_5
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isFollowing:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_5

    .line 344
    :cond_6
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isMuted:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_6

    .line 346
    :cond_7
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isBlocked:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_7

    .line 348
    :cond_8
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->hasDigitsId:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_8

    .line 350
    :cond_9
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numHearts:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto/16 :goto_9

    .line 352
    :cond_a
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isEmployee:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_a

    .line 354
    :cond_b
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numHeartsGiven:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto/16 :goto_b

    .line 356
    :cond_c
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->participantIndex:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto/16 :goto_c

    .line 358
    :cond_d
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isVerified:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_d

    .line 360
    :cond_e
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isBluebirdUser:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_e

    .line 362
    :cond_f
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->twitterUsername:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_f

    .line 364
    :cond_10
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->twitterId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_10

    .line 366
    :cond_11
    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->vipBadge:Ltv/periscope/model/user/UserJSONModel$VipBadge;

    invoke-virtual {v1}, Ltv/periscope/model/user/UserJSONModel$VipBadge;->hashCode()I

    move-result v1

    goto/16 :goto_11
.end method

.method public id()Ljava/lang/String;
    .locals 1
    .annotation runtime Lkb;
        a = "id"
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->id:Ljava/lang/String;

    return-object v0
.end method

.method public initials()Ljava/lang/String;
    .locals 1
    .annotation runtime Lkb;
        a = "initials"
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->initials:Ljava/lang/String;

    return-object v0
.end method

.method public isBlocked()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lkb;
        a = "is_blocked"
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isBlocked:Ljava/lang/Boolean;

    return-object v0
.end method

.method public isBluebirdUser()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lkb;
        a = "is_bluebird_user"
    .end annotation

    .prologue
    .line 227
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isBluebirdUser:Ljava/lang/Boolean;

    return-object v0
.end method

.method public isEmployee()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lkb;
        a = "is_employee"
    .end annotation

    .prologue
    .line 199
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isEmployee:Ljava/lang/Boolean;

    return-object v0
.end method

.method public isFollowing()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lkb;
        a = "is_following"
    .end annotation

    .prologue
    .line 164
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isFollowing:Ljava/lang/Boolean;

    return-object v0
.end method

.method public isMuted()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lkb;
        a = "is_muted"
    .end annotation

    .prologue
    .line 171
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isMuted:Ljava/lang/Boolean;

    return-object v0
.end method

.method public isVerified()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lkb;
        a = "is_twitter_verified"
    .end annotation

    .prologue
    .line 220
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isVerified:Ljava/lang/Boolean;

    return-object v0
.end method

.method public numFollowers()Ljava/lang/Long;
    .locals 1
    .annotation runtime Lkb;
        a = "n_followers"
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numFollowers:Ljava/lang/Long;

    return-object v0
.end method

.method public numFollowing()Ljava/lang/Long;
    .locals 1
    .annotation runtime Lkb;
        a = "n_following"
    .end annotation

    .prologue
    .line 157
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numFollowing:Ljava/lang/Long;

    return-object v0
.end method

.method public numHearts()Ljava/lang/Long;
    .locals 1
    .annotation runtime Lkb;
        a = "n_hearts"
    .end annotation

    .prologue
    .line 192
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numHearts:Ljava/lang/Long;

    return-object v0
.end method

.method public numHeartsGiven()Ljava/lang/Long;
    .locals 1
    .annotation runtime Lkb;
        a = "n_hearts_given"
    .end annotation

    .prologue
    .line 206
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numHeartsGiven:Ljava/lang/Long;

    return-object v0
.end method

.method public participantIndex()Ljava/lang/Long;
    .locals 1
    .annotation runtime Lkb;
        a = "participant_index"
    .end annotation

    .prologue
    .line 213
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->participantIndex:Ljava/lang/Long;

    return-object v0
.end method

.method public profileImageUrls()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/ProfileImageUrlJSONModel;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkb;
        a = "profile_image_urls"
    .end annotation

    .prologue
    .line 143
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->profileImageUrls:Ljava/util/List;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 253
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "UserJSONModel{className="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->className:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", createdAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->createdAt:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", updatedAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->updatedAt:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", username="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->username:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", displayName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", initials="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->initials:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", profileImageUrls="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->profileImageUrls:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", numFollowers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numFollowers:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", numFollowing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numFollowing:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", isFollowing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isFollowing:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", isMuted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isMuted:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", isBlocked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isBlocked:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", hasDigitsId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->hasDigitsId:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", numHearts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numHearts:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", isEmployee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isEmployee:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", numHeartsGiven="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->numHeartsGiven:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", participantIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->participantIndex:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", isVerified="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isVerified:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", isBluebirdUser="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->isBluebirdUser:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", twitterUsername="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->twitterUsername:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", twitterId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->twitterId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", vipBadge="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->vipBadge:Ltv/periscope/model/user/UserJSONModel$VipBadge;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public twitterId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lkb;
        a = "twitter_id"
    .end annotation

    .prologue
    .line 241
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->twitterId:Ljava/lang/String;

    return-object v0
.end method

.method public twitterUsername()Ljava/lang/String;
    .locals 1
    .annotation runtime Lkb;
        a = "twitter_screen_name"
    .end annotation

    .prologue
    .line 234
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->twitterUsername:Ljava/lang/String;

    return-object v0
.end method

.method public updatedAt()Ljava/lang/String;
    .locals 1
    .annotation runtime Lkb;
        a = "updated_at"
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->updatedAt:Ljava/lang/String;

    return-object v0
.end method

.method public username()Ljava/lang/String;
    .locals 1
    .annotation runtime Lkb;
        a = "username"
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->username:Ljava/lang/String;

    return-object v0
.end method

.method public vipBadge()Ltv/periscope/model/user/UserJSONModel$VipBadge;
    .locals 1
    .annotation runtime Lkb;
        a = "vip"
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Ltv/periscope/model/user/$AutoValue_UserJSONModel;->vipBadge:Ltv/periscope/model/user/UserJSONModel$VipBadge;

    return-object v0
.end method
