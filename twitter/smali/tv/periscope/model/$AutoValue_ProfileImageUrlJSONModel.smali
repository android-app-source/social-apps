.class abstract Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;
.super Ltv/periscope/model/ProfileImageUrlJSONModel;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel$Builder;
    }
.end annotation


# instance fields
.field private final height:Ljava/lang/Integer;

.field private final sslUrl:Ljava/lang/String;

.field private final url:Ljava/lang/String;

.field private final width:Ljava/lang/Integer;


# direct methods
.method constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ltv/periscope/model/ProfileImageUrlJSONModel;-><init>()V

    .line 20
    iput-object p1, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;->width:Ljava/lang/Integer;

    .line 21
    iput-object p2, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;->height:Ljava/lang/Integer;

    .line 22
    iput-object p3, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;->url:Ljava/lang/String;

    .line 23
    iput-object p4, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;->sslUrl:Ljava/lang/String;

    .line 24
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 62
    if-ne p1, p0, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v0

    .line 65
    :cond_1
    instance-of v2, p1, Ltv/periscope/model/ProfileImageUrlJSONModel;

    if-eqz v2, :cond_3

    .line 66
    check-cast p1, Ltv/periscope/model/ProfileImageUrlJSONModel;

    .line 67
    iget-object v2, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;->width:Ljava/lang/Integer;

    invoke-virtual {p1}, Ltv/periscope/model/ProfileImageUrlJSONModel;->width()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;->height:Ljava/lang/Integer;

    .line 68
    invoke-virtual {p1}, Ltv/periscope/model/ProfileImageUrlJSONModel;->height()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;->url:Ljava/lang/String;

    .line 69
    invoke-virtual {p1}, Ltv/periscope/model/ProfileImageUrlJSONModel;->url()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;->sslUrl:Ljava/lang/String;

    .line 70
    invoke-virtual {p1}, Ltv/periscope/model/ProfileImageUrlJSONModel;->sslUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 72
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 77
    .line 79
    iget-object v0, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;->width:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    xor-int/2addr v0, v2

    .line 80
    mul-int/2addr v0, v2

    .line 81
    iget-object v1, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;->height:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 82
    mul-int/2addr v0, v2

    .line 83
    iget-object v1, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;->url:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 84
    mul-int/2addr v0, v2

    .line 85
    iget-object v1, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;->sslUrl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 86
    return v0
.end method

.method public height()Ljava/lang/Integer;
    .locals 1
    .annotation runtime Lkb;
        a = "height"
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;->height:Ljava/lang/Integer;

    return-object v0
.end method

.method public sslUrl()Ljava/lang/String;
    .locals 1
    .annotation runtime Lkb;
        a = "ssl_url"
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;->sslUrl:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "ProfileImageUrlJSONModel{width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;->width:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;->height:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", sslUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;->sslUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public url()Ljava/lang/String;
    .locals 1
    .annotation runtime Lkb;
        a = "url"
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;->url:Ljava/lang/String;

    return-object v0
.end method

.method public width()Ljava/lang/Integer;
    .locals 1
    .annotation runtime Lkb;
        a = "width"
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel;->width:Ljava/lang/Integer;

    return-object v0
.end method
