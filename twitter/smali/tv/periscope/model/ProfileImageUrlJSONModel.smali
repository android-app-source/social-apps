.class public abstract Ltv/periscope/model/ProfileImageUrlJSONModel;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/periscope/model/ProfileImageUrlJSONModel$Builder;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static builder()Ltv/periscope/model/ProfileImageUrlJSONModel$Builder;
    .locals 1

    .prologue
    .line 19
    new-instance v0, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel$Builder;

    invoke-direct {v0}, Ltv/periscope/model/$AutoValue_ProfileImageUrlJSONModel$Builder;-><init>()V

    return-object v0
.end method

.method public static typeAdapter(Lcom/google/gson/e;)Lcom/google/gson/r;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/e;",
            ")",
            "Lcom/google/gson/r",
            "<",
            "Ltv/periscope/model/ProfileImageUrlJSONModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 14
    new-instance v0, Ltv/periscope/model/AutoValue_ProfileImageUrlJSONModel$GsonTypeAdapter;

    invoke-direct {v0, p0}, Ltv/periscope/model/AutoValue_ProfileImageUrlJSONModel$GsonTypeAdapter;-><init>(Lcom/google/gson/e;)V

    return-object v0
.end method


# virtual methods
.method public abstract height()Ljava/lang/Integer;
    .annotation runtime Lkb;
        a = "height"
    .end annotation
.end method

.method public abstract sslUrl()Ljava/lang/String;
    .annotation runtime Lkb;
        a = "ssl_url"
    .end annotation
.end method

.method public abstract url()Ljava/lang/String;
    .annotation runtime Lkb;
        a = "url"
    .end annotation
.end method

.method public abstract width()Ljava/lang/Integer;
    .annotation runtime Lkb;
        a = "width"
    .end annotation
.end method
