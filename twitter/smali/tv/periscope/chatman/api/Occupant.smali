.class public Ltv/periscope/chatman/api/Occupant;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final displayName:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "display_name"
    .end annotation
.end field

.field public final participantIndex:J
    .annotation runtime Lkb;
        a = "participant_index"
    .end annotation
.end field

.field public final profileImageUrl:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "profile_image_url"
    .end annotation
.end field

.field public final twitterId:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "twitter_id"
    .end annotation
.end field

.field public final userId:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "user_id"
    .end annotation
.end field

.field public final username:Ljava/lang/String;
    .annotation runtime Lkb;
        a = "username"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Ltv/periscope/chatman/api/Occupant;->userId:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Ltv/periscope/chatman/api/Occupant;->displayName:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Ltv/periscope/chatman/api/Occupant;->username:Ljava/lang/String;

    .line 29
    iput-object p4, p0, Ltv/periscope/chatman/api/Occupant;->profileImageUrl:Ljava/lang/String;

    .line 30
    iput-wide p5, p0, Ltv/periscope/chatman/api/Occupant;->participantIndex:J

    .line 31
    iput-object p7, p0, Ltv/periscope/chatman/api/Occupant;->twitterId:Ljava/lang/String;

    .line 32
    return-void
.end method
