.class public Laei;
.super Laef;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laei$b;,
        Laei$a;,
        Laei$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laef",
        "<",
        "Lcah;",
        "Laei$c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/model/util/FriendshipCache;

.field private final b:Landroid/view/View$OnClickListener;

.field private final c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final d:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Landroid/view/View$OnClickListener;Lcom/twitter/android/notificationtimeline/a;Lcom/twitter/android/notificationtimeline/h;)V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0, p4, p5}, Laef;-><init>(Lcom/twitter/android/notificationtimeline/a;Lcom/twitter/android/notificationtimeline/h;)V

    .line 47
    new-instance v0, Laei$1;

    invoke-direct {v0, p0}, Laei$1;-><init>(Laei;)V

    iput-object v0, p0, Laei;->b:Landroid/view/View$OnClickListener;

    .line 73
    iput-object p1, p0, Laei;->a:Lcom/twitter/model/util/FriendshipCache;

    .line 74
    iput-object p2, p0, Laei;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 75
    iput-object p3, p0, Laei;->d:Landroid/view/View$OnClickListener;

    .line 76
    return-void
.end method

.method protected static a()Landroid/widget/AbsListView$LayoutParams;
    .locals 3

    .prologue
    .line 203
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method static synthetic a(Laei;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Laei;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    return-object v0
.end method

.method protected static b()Landroid/widget/LinearLayout$LayoutParams;
    .locals 3

    .prologue
    .line 210
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 213
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 214
    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)Laei$c;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 81
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 82
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 83
    const v2, 0x7f040027

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 85
    const v3, 0x7f040162

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 86
    new-instance v3, Laei$a;

    invoke-direct {v3, v1}, Laei$a;-><init>(Landroid/view/View;)V

    .line 87
    invoke-virtual {v1, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 88
    iget-object v4, v3, Laei$a;->b:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 89
    iget-object v3, v3, Laei$a;->b:Landroid/view/View;

    iget-object v4, p0, Laei;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    new-instance v3, Lcom/twitter/internal/android/widget/HighlightedLinearLayout;

    invoke-direct {v3, v0}, Lcom/twitter/internal/android/widget/HighlightedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 92
    new-instance v0, Laei$c;

    invoke-direct {v0, v3, v2, v1}, Laei$c;-><init>(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    .line 93
    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 95
    iget-object v4, v0, Laei$c;->d:Lcom/twitter/ui/widget/ActionButton;

    .line 96
    if-eqz v4, :cond_0

    .line 97
    new-instance v5, Laei$b;

    invoke-direct {v5}, Laei$b;-><init>()V

    invoke-virtual {v4, v5}, Lcom/twitter/ui/widget/ActionButton;->setTag(Ljava/lang/Object;)V

    .line 98
    const v5, 0x7f0200b0

    invoke-virtual {v4, v5}, Lcom/twitter/ui/widget/ActionButton;->a(I)V

    .line 99
    iget-object v5, p0, Laei;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Lcom/twitter/ui/widget/ActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    :cond_0
    invoke-static {}, Laei;->b()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v4

    .line 103
    invoke-virtual {v3, v0}, Lcom/twitter/internal/android/widget/HighlightedLinearLayout;->setTag(Ljava/lang/Object;)V

    .line 104
    invoke-static {}, Laei;->a()Landroid/widget/AbsListView$LayoutParams;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/twitter/internal/android/widget/HighlightedLinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 105
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/twitter/internal/android/widget/HighlightedLinearLayout;->setOrientation(I)V

    .line 106
    invoke-virtual {v3, v2, v4}, Lcom/twitter/internal/android/widget/HighlightedLinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 107
    invoke-virtual {v3, v1, v4}, Lcom/twitter/internal/android/widget/HighlightedLinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 109
    return-object v0
.end method

.method protected bridge synthetic a(Landroid/content/Context;Lbzz;I)Lcom/twitter/analytics/model/ScribeItem;
    .locals 1

    .prologue
    .line 43
    check-cast p2, Lcah;

    invoke-virtual {p0, p1, p2, p3}, Laei;->a(Landroid/content/Context;Lcah;I)Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/content/Context;Lcah;I)Lcom/twitter/analytics/model/ScribeItem;
    .locals 1

    .prologue
    .line 158
    const/16 v0, 0xd

    invoke-static {p3, v0}, Lcom/twitter/android/notificationtimeline/a;->a(II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Laef$a;Lbzz;)V
    .locals 0

    .prologue
    .line 43
    check-cast p1, Laei$c;

    check-cast p2, Lcah;

    invoke-virtual {p0, p1, p2}, Laei;->a(Laei$c;Lcah;)V

    return-void
.end method

.method public a(Laei$c;Lcah;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x1

    .line 114
    invoke-super {p0, p1, p2}, Laef;->a(Laef$a;Lbzz;)V

    .line 115
    invoke-virtual {p1}, Laei$c;->b()Landroid/view/View;

    move-result-object v2

    .line 116
    iget-object v0, p2, Lcah;->b:Lcac;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcap;

    invoke-virtual {v0}, Lcap;->f()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/TwitterUser;

    .line 117
    iget-object v0, p1, Laei$c;->a:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;)Z

    .line 118
    iget-object v0, p1, Laei$c;->a:Lcom/twitter/media/ui/image/UserImageView;

    invoke-static {}, Lbpi;->a()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/twitter/media/ui/image/UserImageView;->setSize(I)V

    .line 119
    iput-object v1, p1, Laei$c;->e:Lcom/twitter/model/core/TwitterUser;

    .line 121
    invoke-virtual {v1}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v4

    .line 122
    iget-object v0, v1, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    .line 123
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 124
    new-array v5, v3, [Lcom/twitter/internal/android/widget/TypefacesSpan;

    new-instance v6, Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-direct {v6, v2, v3}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    aput-object v6, v5, v9

    .line 126
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 127
    iget-object v2, p1, Laei$c;->b:Landroid/widget/TextView;

    const v7, 0x7f0a0433

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v4, v8, v9

    aput-object v0, v8, v3

    .line 128
    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/16 v7, 0x22

    .line 127
    invoke-static {v5, v0, v7}, Lcom/twitter/library/util/af;->a([Ljava/lang/Object;Ljava/lang/String;C)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v5

    .line 134
    iget-object v0, p1, Laei$c;->c:Landroid/view/View;

    .line 135
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laei$a;

    .line 136
    iput-object v1, v0, Laei$a;->c:Lcom/twitter/model/core/TwitterUser;

    .line 137
    iget-object v2, v0, Laei$a;->a:Landroid/widget/TextView;

    const v7, 0x7f0a098c

    new-array v8, v3, [Ljava/lang/Object;

    aput-object v4, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v2, v0, Laei$a;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 141
    invoke-static {}, Lbpi;->b()I

    move-result v4

    const v7, 0x7f0e0292

    .line 142
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    add-int/2addr v4, v6

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 143
    iget-object v0, v0, Laei$a;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 146
    iget-object v0, p1, Laei$c;->d:Lcom/twitter/ui/widget/ActionButton;

    iget-object v2, p0, Laei;->a:Lcom/twitter/model/util/FriendshipCache;

    .line 147
    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/widget/ActivityUserView;->a(Lcom/twitter/ui/widget/ActionButton;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/util/FriendshipCache;ZJ)V

    .line 149
    iget-object v0, p1, Laei$c;->d:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/ActionButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laei$b;

    iput-object v1, v0, Laei$b;->a:Lcom/twitter/model/core/TwitterUser;

    .line 151
    iget-object v0, p1, Laei$c;->d:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v1}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/ActionButton;->setUsername(Ljava/lang/String;)V

    .line 152
    return-void
.end method

.method public bridge synthetic a(Lckb$a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 43
    check-cast p1, Laei$c;

    check-cast p2, Lcah;

    invoke-virtual {p0, p1, p2}, Laei;->a(Laei$c;Lcah;)V

    return-void
.end method

.method public synthetic b(Landroid/view/ViewGroup;)Lckb$a;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Laei;->a(Landroid/view/ViewGroup;)Laei$c;

    move-result-object v0

    return-object v0
.end method
