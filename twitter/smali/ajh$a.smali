.class public Lajh$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lajh;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Lcom/twitter/model/core/Tweet;

.field private c:Lcom/twitter/model/core/h;

.field private d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lajh$a;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lajh$a;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b(Lajh$a;)Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lajh$a;->b:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method static synthetic c(Lajh$a;)Lcom/twitter/model/core/h;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lajh$a;->c:Lcom/twitter/model/core/h;

    return-object v0
.end method

.method static synthetic d(Lajh$a;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lajh$a;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    return-object v0
.end method

.method static synthetic e(Lajh$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lajh$a;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lajh$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lajh$a;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/app/Activity;)Lajh$a;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lajh$a;->a:Landroid/app/Activity;

    .line 76
    return-object p0
.end method

.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lajh$a;
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lajh$a;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 94
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/Tweet;)Lajh$a;
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lajh$a;->b:Lcom/twitter/model/core/Tweet;

    .line 82
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/h;)Lajh$a;
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lajh$a;->c:Lcom/twitter/model/core/h;

    .line 88
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lajh$a;
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lajh$a;->e:Ljava/lang/String;

    .line 100
    return-object p0
.end method

.method public a()Lajh;
    .locals 2

    .prologue
    .line 111
    new-instance v0, Lajh;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lajh;-><init>(Lajh$a;Lajh$1;)V

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lajh$a;
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lajh$a;->f:Ljava/lang/String;

    .line 106
    return-object p0
.end method
