.class final Lcgb$c$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcgb$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcgb$c;",
        "Lcgb$c$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 324
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcgb$1;)V
    .locals 0

    .prologue
    .line 324
    invoke-direct {p0}, Lcgb$c$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcgb$c$a;
    .locals 1

    .prologue
    .line 329
    new-instance v0, Lcgb$c$a;

    invoke-direct {v0}, Lcgb$c$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcgb$c$a;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 348
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcgb$c$a;->a(Ljava/lang/String;)Lcgb$c$a;

    move-result-object v0

    .line 349
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcgb$c$a;->b(Ljava/lang/String;)Lcgb$c$a;

    move-result-object v0

    .line 350
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcgb$c$a;->c(Ljava/lang/String;)Lcgb$c$a;

    move-result-object v0

    .line 351
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcgb$c$a;->d(Ljava/lang/String;)Lcgb$c$a;

    move-result-object v0

    .line 352
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcgb$c$a;->e(Ljava/lang/String;)Lcgb$c$a;

    move-result-object v0

    .line 353
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcgb$c$a;->f(Ljava/lang/String;)Lcgb$c$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v2, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 355
    invoke-static {v0, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 354
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v1, v0}, Lcgb$c$a;->a(Ljava/util/Map;)Lcgb$c$a;

    .line 356
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 324
    check-cast p2, Lcgb$c$a;

    invoke-virtual {p0, p1, p2, p3}, Lcgb$c$b;->a(Lcom/twitter/util/serialization/n;Lcgb$c$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcgb$c;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 335
    iget-object v0, p2, Lcgb$c;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 336
    iget-object v0, p2, Lcgb$c;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 337
    iget-object v0, p2, Lcgb$c;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 338
    iget-object v0, p2, Lcgb$c;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 339
    iget-object v0, p2, Lcgb$c;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 340
    iget-object v0, p2, Lcgb$c;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 341
    iget-object v0, p2, Lcgb$c;->h:Ljava/util/Map;

    sget-object v1, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v2, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 342
    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/Map;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)V

    .line 343
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 324
    check-cast p2, Lcgb$c;

    invoke-virtual {p0, p1, p2}, Lcgb$c$b;->a(Lcom/twitter/util/serialization/o;Lcgb$c;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 324
    invoke-virtual {p0}, Lcgb$c$b;->a()Lcgb$c$a;

    move-result-object v0

    return-object v0
.end method
