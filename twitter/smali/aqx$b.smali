.class public final Laqx$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Laqx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field private a:Lamu;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Laqx$1;)V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0}, Laqx$b;-><init>()V

    return-void
.end method

.method static synthetic a(Laqx$b;)Lamu;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Laqx$b;->a:Lamu;

    return-object v0
.end method


# virtual methods
.method public a(Lamu;)Laqx$b;
    .locals 1

    .prologue
    .line 189
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamu;

    iput-object v0, p0, Laqx$b;->a:Lamu;

    .line 190
    return-object p0
.end method

.method public a(Lano;)Laqx$b;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 184
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    return-object p0
.end method

.method public a()Laqy;
    .locals 3

    .prologue
    .line 171
    iget-object v0, p0, Laqx$b;->a:Lamu;

    if-nez v0, :cond_0

    .line 172
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lamu;

    .line 173
    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :cond_0
    new-instance v0, Laqx;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Laqx;-><init>(Laqx$b;Laqx$1;)V

    return-object v0
.end method
