.class public Lajg$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lajg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/twitter/library/client/v;

.field private c:Lcom/twitter/library/client/p;

.field private d:Lajk;

.field private e:Lcom/twitter/model/core/Tweet;

.field private f:Lcom/twitter/model/util/FriendshipCache;

.field private g:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

.field private h:Lcom/twitter/library/widget/h;

.field private i:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/v;Lcom/twitter/library/client/p;Lajk;)V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object p1, p0, Lajg$a;->a:Landroid/content/Context;

    .line 99
    iput-object p2, p0, Lajg$a;->b:Lcom/twitter/library/client/v;

    .line 100
    iput-object p3, p0, Lajg$a;->c:Lcom/twitter/library/client/p;

    .line 101
    iput-object p4, p0, Lajg$a;->d:Lajk;

    .line 102
    return-void
.end method

.method public static a(Landroid/content/Context;Lajk;)Lajg$a;
    .locals 3

    .prologue
    .line 141
    new-instance v0, Lajg$a;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p1}, Lajg$a;-><init>(Landroid/content/Context;Lcom/twitter/library/client/v;Lcom/twitter/library/client/p;Lajk;)V

    return-object v0
.end method

.method static synthetic a(Lajg$a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lajg$a;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lajg$a;)Lcom/twitter/library/client/v;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lajg$a;->b:Lcom/twitter/library/client/v;

    return-object v0
.end method

.method static synthetic c(Lajg$a;)Lcom/twitter/library/client/p;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lajg$a;->c:Lcom/twitter/library/client/p;

    return-object v0
.end method

.method static synthetic d(Lajg$a;)Lajk;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lajg$a;->d:Lajk;

    return-object v0
.end method

.method static synthetic e(Lajg$a;)Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lajg$a;->e:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method static synthetic f(Lajg$a;)Lcom/twitter/model/util/FriendshipCache;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lajg$a;->f:Lcom/twitter/model/util/FriendshipCache;

    return-object v0
.end method

.method static synthetic g(Lajg$a;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lajg$a;->g:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    return-object v0
.end method

.method static synthetic h(Lajg$a;)Lcom/twitter/library/widget/h;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lajg$a;->h:Lcom/twitter/library/widget/h;

    return-object v0
.end method

.method static synthetic i(Lajg$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lajg$a;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeItem;)Lajg$a;
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lajg$a;->g:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 119
    return-object p0
.end method

.method public a(Lcom/twitter/library/widget/h;)Lajg$a;
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lajg$a;->h:Lcom/twitter/library/widget/h;

    .line 125
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/Tweet;)Lajg$a;
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lajg$a;->e:Lcom/twitter/model/core/Tweet;

    .line 107
    return-object p0
.end method

.method public a(Lcom/twitter/model/util/FriendshipCache;)Lajg$a;
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lajg$a;->f:Lcom/twitter/model/util/FriendshipCache;

    .line 113
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lajg$a;
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lajg$a;->i:Ljava/lang/String;

    .line 131
    return-object p0
.end method

.method public a()Lajj;
    .locals 1

    .prologue
    .line 136
    new-instance v0, Lajg;

    invoke-direct {v0, p0}, Lajg;-><init>(Lajg$a;)V

    return-object v0
.end method
