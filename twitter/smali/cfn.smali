.class public Lcfn;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcfn$c;,
        Lcfn$b;,
        Lcfn$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcfn;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:J

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcfn$c;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcfn$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcfn$b;-><init>(Lcfn$1;)V

    sput-object v0, Lcfn;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method constructor <init>(Lcfn$a;)V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iget-object v0, p1, Lcfn$a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcfn;->b:Ljava/lang/String;

    .line 48
    invoke-static {p1}, Lcfn$a;->a(Lcfn$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcfn;->c:J

    .line 49
    invoke-static {p1}, Lcfn$a;->b(Lcfn$a;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcfn;->d:Ljava/util/List;

    .line 50
    invoke-static {p1}, Lcfn$a;->c(Lcfn$a;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcfn;->e:Ljava/util/List;

    .line 51
    invoke-static {p1}, Lcfn$a;->d(Lcfn$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfn;->f:Ljava/lang/String;

    .line 52
    return-void
.end method
