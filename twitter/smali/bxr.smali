.class public Lbxr;
.super Lbxn;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/ui/widget/TextLayoutView;

.field private final b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/twitter/ui/widget/TextLayoutView;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lbxn;-><init>()V

    .line 19
    iput-object p1, p0, Lbxr;->a:Lcom/twitter/ui/widget/TextLayoutView;

    .line 20
    iput-object p2, p0, Lbxr;->b:Landroid/content/res/Resources;

    .line 21
    return-void
.end method

.method protected static a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;)Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p1, Lcom/twitter/ui/view/h;->d:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Lbxd;->f(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;J)V
    .locals 3

    .prologue
    .line 24
    invoke-static {p1, p2}, Lbxr;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbxr;->b:Landroid/content/res/Resources;

    .line 25
    invoke-static {p1, p3, p4, v0}, Lbxo;->a(Lcom/twitter/model/core/Tweet;JLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    .line 26
    :goto_0
    invoke-virtual {p0, v0}, Lbxr;->a(Ljava/lang/CharSequence;)V

    .line 27
    iget-object v0, p0, Lbxr;->a:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->o()Z

    move-result v1

    invoke-static {v1}, Lcom/twitter/util/b;->a(Z)Landroid/text/Layout$Alignment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TextLayoutView;->a(Landroid/text/Layout$Alignment;)Lcom/twitter/ui/widget/TextLayoutView;

    .line 28
    return-void

    .line 25
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lbxr;->a:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/TextLayoutView;->setTextWithVisibility(Ljava/lang/CharSequence;)V

    .line 33
    return-void
.end method
