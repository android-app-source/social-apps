.class Laps$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Laps;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation


# instance fields
.field final a:Z

.field final b:Ljava/lang/String;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:J

.field private final f:Lcci;

.field private final g:I

.field private final h:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/twitter/model/dms/m;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 276
    iput-object p2, p0, Laps$b;->h:Ljava/lang/String;

    .line 278
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/twitter/model/dms/m;->c()Lcom/twitter/model/dms/c;

    move-result-object v1

    .line 280
    :goto_0
    if-nez v1, :cond_2

    const-wide/16 v2, 0x0

    :goto_1
    iput-wide v2, p0, Laps$b;->e:J

    .line 281
    if-nez p1, :cond_3

    const-string/jumbo v0, ""

    :goto_2
    iput-object v0, p0, Laps$b;->d:Ljava/lang/String;

    .line 282
    if-nez v1, :cond_4

    const/4 v0, -0x1

    :goto_3
    iput v0, p0, Laps$b;->c:I

    .line 283
    iget v0, p0, Laps$b;->c:I

    const/16 v2, 0x14

    if-ne v0, v2, :cond_5

    move-object v0, v1

    check-cast v0, Lcom/twitter/model/dms/ai;

    .line 284
    invoke-virtual {v0}, Lcom/twitter/model/dms/ai;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_4
    iput-boolean v0, p0, Laps$b;->a:Z

    .line 285
    invoke-direct {p0, v1}, Laps$b;->a(Lcom/twitter/model/dms/c;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laps$b;->b:Ljava/lang/String;

    .line 287
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/twitter/model/dms/c;->n()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 288
    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e;

    .line 289
    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->u()Lcbx;

    move-result-object v1

    .line 290
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcbx;->a()I

    move-result v4

    :cond_0
    iput v4, p0, Laps$b;->g:I

    .line 291
    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->A()Z

    move-result v0

    if-eqz v0, :cond_6

    move-object v0, v1

    check-cast v0, Lcci;

    :goto_5
    iput-object v0, p0, Laps$b;->f:Lcci;

    .line 296
    :goto_6
    return-void

    :cond_1
    move-object v1, v5

    .line 278
    goto :goto_0

    .line 280
    :cond_2
    invoke-virtual {v1}, Lcom/twitter/model/dms/c;->l()J

    move-result-wide v2

    goto :goto_1

    .line 281
    :cond_3
    invoke-virtual {p1}, Lcom/twitter/model/dms/m;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 282
    :cond_4
    invoke-virtual {v1}, Lcom/twitter/model/dms/c;->o()I

    move-result v0

    goto :goto_3

    :cond_5
    move v0, v4

    .line 284
    goto :goto_4

    :cond_6
    move-object v0, v5

    .line 291
    goto :goto_5

    .line 293
    :cond_7
    iput v4, p0, Laps$b;->g:I

    .line 294
    iput-object v5, p0, Laps$b;->f:Lcci;

    goto :goto_6
.end method

.method static synthetic a(Laps$b;)J
    .locals 2

    .prologue
    .line 263
    iget-wide v0, p0, Laps$b;->e:J

    return-wide v0
.end method

.method private a(Lcom/twitter/model/dms/c;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 300
    if-nez p1, :cond_0

    .line 301
    const-string/jumbo v0, ""

    .line 322
    :goto_0
    return-object v0

    .line 304
    :cond_0
    iget v0, p0, Laps$b;->c:I

    sparse-switch v0, :sswitch_data_0

    .line 322
    const-string/jumbo v0, ""

    goto :goto_0

    .line 306
    :sswitch_0
    const-string/jumbo v0, ", "

    check-cast p1, Lcom/twitter/model/dms/ae;

    invoke-virtual {p1}, Lcom/twitter/model/dms/ae;->e()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 309
    :sswitch_1
    check-cast p1, Lcom/twitter/model/dms/ak;

    invoke-virtual {p1}, Lcom/twitter/model/dms/ak;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 314
    :sswitch_2
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e;

    .line 315
    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->w()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 316
    iget-object v0, p0, Laps$b;->h:Ljava/lang/String;

    goto :goto_0

    .line 318
    :cond_1
    invoke-static {v0}, Laps$b;->a(Lcom/twitter/model/dms/e;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 304
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_2
        0x8 -> :sswitch_1
        0xa -> :sswitch_0
        0x13 -> :sswitch_2
    .end sparse-switch
.end method

.method private static a(Lcom/twitter/model/dms/e;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 329
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 331
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->r()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 332
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->A()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->D()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 336
    :cond_1
    invoke-static {v0}, Lcom/twitter/util/y;->c(Ljava/lang/CharSequence;)I

    move-result v1

    if-nez v1, :cond_2

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 337
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 339
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->t()Lcom/twitter/model/core/v;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/model/core/v;->a(Ljava/lang/StringBuilder;Lcom/twitter/model/core/v;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Laps$b;)I
    .locals 1

    .prologue
    .line 263
    iget v0, p0, Laps$b;->c:I

    return v0
.end method

.method static synthetic c(Laps$b;)I
    .locals 1

    .prologue
    .line 263
    iget v0, p0, Laps$b;->g:I

    return v0
.end method

.method static synthetic d(Laps$b;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Laps$b;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Laps$b;)Lcci;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Laps$b;->f:Lcci;

    return-object v0
.end method
