.class public Lavp;
.super Lcbr;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbr",
        "<",
        "Lawz$a;",
        "Lcom/twitter/model/dms/q;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcbr;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lawz$a;)Lcom/twitter/model/dms/q;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 18
    new-instance v0, Lcom/twitter/model/dms/q$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/q$a;-><init>()V

    .line 19
    invoke-interface {p1}, Lawz$a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/q$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    .line 20
    invoke-interface {p1}, Lawz$a;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/q$a;->b(Ljava/lang/String;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    .line 21
    invoke-interface {p1}, Lawz$a;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/q$a;->c(Ljava/lang/String;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    .line 22
    invoke-interface {p1}, Lawz$a;->q()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/q$a;->a(Z)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    .line 23
    invoke-interface {p1}, Lawz$a;->i()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/dms/q$a;->a(J)Lcom/twitter/model/dms/q$a;

    move-result-object v3

    .line 24
    invoke-interface {p1}, Lawz$a;->e()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/twitter/model/dms/q$a;->b(Z)Lcom/twitter/model/dms/q$a;

    move-result-object v3

    .line 25
    invoke-interface {p1}, Lawz$a;->j()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/twitter/model/dms/q$a;->c(Z)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    .line 26
    invoke-interface {p1}, Lawz$a;->n()I

    move-result v3

    if-lez v3, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/q$a;->d(Z)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    .line 27
    invoke-interface {p1}, Lawz$a;->o()Lcom/twitter/model/dms/x;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/q$a;->a(Lcom/twitter/model/dms/x;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    .line 31
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/q$a;->a(Ljava/util/List;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lcom/twitter/model/dms/q$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/q;

    .line 18
    return-object v0

    :cond_0
    move v0, v2

    .line 24
    goto :goto_0

    :cond_1
    move v0, v2

    .line 25
    goto :goto_1

    :cond_2
    move v1, v2

    .line 26
    goto :goto_2
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    check-cast p1, Lawz$a;

    invoke-virtual {p0, p1}, Lavp;->a(Lawz$a;)Lcom/twitter/model/dms/q;

    move-result-object v0

    return-object v0
.end method
