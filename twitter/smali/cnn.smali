.class public Lcnn;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcno;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcnn$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/widget/ListView;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcno$c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcnn$a;

.field private final d:Landroid/view/View$OnLayoutChangeListener;

.field private e:Lcnm;


# direct methods
.method public constructor <init>(Landroid/widget/ListView;)V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcnn;->b:Ljava/util/Set;

    .line 29
    new-instance v0, Lcnn$1;

    invoke-direct {v0, p0}, Lcnn$1;-><init>(Lcnn;)V

    iput-object v0, p0, Lcnn;->d:Landroid/view/View$OnLayoutChangeListener;

    .line 42
    iput-object p1, p0, Lcnn;->a:Landroid/widget/ListView;

    .line 43
    new-instance v0, Lcnn$a;

    invoke-direct {v0, p0}, Lcnn$a;-><init>(Lcnn;)V

    iput-object v0, p0, Lcnn;->c:Lcnn$a;

    .line 44
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcnn;->c:Lcnn$a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 45
    return-void
.end method

.method static synthetic a(Lcnn;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic a(Lcnn;Lcnm;)Lcnm;
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lcnn;->e:Lcnm;

    return-object p1
.end method

.method static synthetic b(Lcnn;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcnn;->b:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public a(I)J
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->getItemIdAtPosition(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public a()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    return-object v0
.end method

.method public a(IIZ)V
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lcnm;

    invoke-direct {v0, p1, p2}, Lcnm;-><init>(II)V

    invoke-virtual {p0, v0, p3}, Lcnn;->a(Lcnm;Z)V

    .line 99
    return-void
.end method

.method public a(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/AbsListView$RecyclerListener;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 53
    :cond_0
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 54
    instance-of v0, p1, Landroid/widget/AbsListView$RecyclerListener;

    if-eqz v0, :cond_1

    .line 55
    check-cast p1, Landroid/widget/AbsListView$RecyclerListener;

    .line 56
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 58
    :cond_1
    return-void
.end method

.method public a(Lcnm;Z)V
    .locals 3

    .prologue
    .line 103
    invoke-virtual {p1}, Lcnm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iput-object p1, p0, Lcnn;->e:Lcnm;

    .line 105
    if-eqz p2, :cond_1

    .line 106
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    iget v1, p1, Lcnm;->c:I

    iget v2, p1, Lcnm;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->smoothScrollToPositionFromTop(II)V

    .line 110
    :goto_0
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcnn;->d:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 112
    :cond_0
    return-void

    .line 108
    :cond_1
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    iget v1, p1, Lcnm;->c:I

    iget v2, p1, Lcnm;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_0
.end method

.method public a(Lcno$b;)V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    new-instance v1, Lcnn$2;

    invoke-direct {v1, p0, p1}, Lcnn$2;-><init>(Lcnn;Lcno$b;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 132
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    new-instance v1, Lcnn$3;

    invoke-direct {v1, p0, p1}, Lcnn$3;-><init>(Lcnn;Lcno$b;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 138
    return-void
.end method

.method public a(Lcno$c;)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcnn;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 69
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcnn;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcno$c;

    .line 80
    invoke-interface {v0, p0}, Lcno$c;->a(Lcno;)V

    goto :goto_0

    .line 82
    :cond_0
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 143
    return-void
.end method

.method public c()Lcnm;
    .locals 4

    .prologue
    .line 87
    iget-object v0, p0, Lcnn;->e:Lcnm;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcnn;->e:Lcnm;

    .line 92
    :goto_0
    return-object v0

    .line 89
    :cond_0
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 90
    sget-object v0, Lcnm;->a:Lcnm;

    goto :goto_0

    .line 92
    :cond_1
    new-instance v0, Lcnm;

    iget-object v1, p0, Lcnn;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    iget-object v2, p0, Lcnn;->a:Landroid/widget/ListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcnm;-><init>(II)V

    goto :goto_0
.end method

.method public c(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 153
    return-void
.end method

.method public d(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 163
    return-void
.end method

.method public d()Z
    .locals 3

    .prologue
    .line 147
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    iget-object v1, p0, Lcnn;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    iget-object v2, p0, Lcnn;->a:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v2

    add-int/2addr v1, v2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcnn;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    return v0
.end method
