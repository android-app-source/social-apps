.class public final Lale$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lale;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:Lamp;

.field private b:Lali;

.field private c:Lbmv;

.field private d:Lamg;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lale$1;)V
    .locals 0

    .prologue
    .line 1117
    invoke-direct {p0}, Lale$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lale$a;)Lamp;
    .locals 1

    .prologue
    .line 1117
    iget-object v0, p0, Lale$a;->a:Lamp;

    return-object v0
.end method

.method static synthetic b(Lale$a;)Lali;
    .locals 1

    .prologue
    .line 1117
    iget-object v0, p0, Lale$a;->b:Lali;

    return-object v0
.end method

.method static synthetic c(Lale$a;)Lbmv;
    .locals 1

    .prologue
    .line 1117
    iget-object v0, p0, Lale$a;->c:Lbmv;

    return-object v0
.end method

.method static synthetic d(Lale$a;)Lamg;
    .locals 1

    .prologue
    .line 1117
    iget-object v0, p0, Lale$a;->d:Lamg;

    return-object v0
.end method


# virtual methods
.method public a(Lali;)Lale$a;
    .locals 1

    .prologue
    .line 1146
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lali;

    iput-object v0, p0, Lale$a;->b:Lali;

    .line 1147
    return-object p0
.end method

.method public a(Lamg;)Lale$a;
    .locals 1

    .prologue
    .line 1162
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamg;

    iput-object v0, p0, Lale$a;->d:Lamg;

    .line 1163
    return-object p0
.end method

.method public a(Lamp;)Lale$a;
    .locals 1

    .prologue
    .line 1193
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamp;

    iput-object v0, p0, Lale$a;->a:Lamp;

    .line 1194
    return-object p0
.end method

.method public a()Lalg;
    .locals 3

    .prologue
    .line 1129
    iget-object v0, p0, Lale$a;->a:Lamp;

    if-nez v0, :cond_0

    .line 1130
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lamp;

    .line 1131
    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1133
    :cond_0
    iget-object v0, p0, Lale$a;->b:Lali;

    if-nez v0, :cond_1

    .line 1134
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lali;

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1136
    :cond_1
    iget-object v0, p0, Lale$a;->c:Lbmv;

    if-nez v0, :cond_2

    .line 1137
    new-instance v0, Lbmv;

    invoke-direct {v0}, Lbmv;-><init>()V

    iput-object v0, p0, Lale$a;->c:Lbmv;

    .line 1139
    :cond_2
    iget-object v0, p0, Lale$a;->d:Lamg;

    if-nez v0, :cond_3

    .line 1140
    new-instance v0, Lamg;

    invoke-direct {v0}, Lamg;-><init>()V

    iput-object v0, p0, Lale$a;->d:Lamg;

    .line 1142
    :cond_3
    new-instance v0, Lale;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lale;-><init>(Lale$a;Lale$1;)V

    return-object v0
.end method
