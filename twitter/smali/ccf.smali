.class public Lccf;
.super Lcbx;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lccf$b;,
        Lccf$a;
    }
.end annotation


# static fields
.field public static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lccf;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final d:Lcom/twitter/model/core/MediaEntity;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 22
    const-string/jumbo v0, "photo"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "video"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "animated_gif"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lccf;->b:Ljava/util/Set;

    .line 24
    new-instance v0, Lccf$b;

    invoke-direct {v0}, Lccf$b;-><init>()V

    sput-object v0, Lccf;->c:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lccf$a;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcbx;-><init>(Lcbx$a;)V

    .line 30
    invoke-static {p1}, Lccf$a;->a(Lccf$a;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    iput-object v0, p0, Lccf;->d:Lcom/twitter/model/core/MediaEntity;

    .line 31
    return-void
.end method

.method synthetic constructor <init>(Lccf$a;Lccf$1;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lccf;-><init>(Lccf$a;)V

    return-void
.end method

.method private a(Lccf;)Z
    .locals 2

    .prologue
    .line 56
    invoke-super {p0, p1}, Lcbx;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lccf;->d:Lcom/twitter/model/core/MediaEntity;

    iget-object v1, p1, Lccf;->d:Lcom/twitter/model/core/MediaEntity;

    .line 57
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/MediaEntity;->a(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 56
    :goto_0
    return v0

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 70
    sget-object v0, Lccf$1;->a:[I

    iget-object v1, p0, Lccf;->d:Lcom/twitter/model/core/MediaEntity;

    iget-object v1, v1, Lcom/twitter/model/core/MediaEntity;->n:Lcom/twitter/model/core/MediaEntity$Type;

    invoke-virtual {v1}, Lcom/twitter/model/core/MediaEntity$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 81
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 72
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 75
    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 78
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 70
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 52
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lccf;

    if-eqz v0, :cond_1

    check-cast p1, Lccf;

    invoke-direct {p0, p1}, Lccf;->a(Lccf;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 62
    invoke-super {p0}, Lcbx;->hashCode()I

    move-result v0

    .line 63
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lccf;->d:Lcom/twitter/model/core/MediaEntity;

    invoke-virtual {v1}, Lcom/twitter/model/core/MediaEntity;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 64
    return v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lccf;->d:Lcom/twitter/model/core/MediaEntity;

    iget-object v0, v0, Lcom/twitter/model/core/MediaEntity;->m:Ljava/lang/String;

    return-object v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lccf;->d:Lcom/twitter/model/core/MediaEntity;

    iget-object v0, v0, Lcom/twitter/model/core/MediaEntity;->o:Lcom/twitter/util/math/Size;

    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->a()I

    move-result v0

    return v0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lccf;->d:Lcom/twitter/model/core/MediaEntity;

    iget-object v0, v0, Lcom/twitter/model/core/MediaEntity;->o:Lcom/twitter/util/math/Size;

    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->b()I

    move-result v0

    return v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lccf;->d:Lcom/twitter/model/core/MediaEntity;

    iget-boolean v0, v0, Lcom/twitter/model/core/MediaEntity;->x:Z

    return v0
.end method
