.class Lasl$d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lasl$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lasl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation


# instance fields
.field private final a:Lcom/twitter/ui/widget/TwitterEditText;


# direct methods
.method constructor <init>(Lcom/twitter/ui/widget/TwitterEditText;)V
    .locals 0

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    iput-object p1, p0, Lasl$d;->a:Lcom/twitter/ui/widget/TwitterEditText;

    .line 153
    return-void
.end method


# virtual methods
.method public a()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lasl$d;->a:Lcom/twitter/ui/widget/TwitterEditText;

    return-object v0
.end method

.method public a(II)V
    .locals 2
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 163
    if-nez p1, :cond_1

    .line 164
    iget-object v0, p0, Lasl$d;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->e()V

    .line 165
    iget-object v0, p0, Lasl$d;->a:Lcom/twitter/ui/widget/TwitterEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setHelperMessage(Ljava/lang/CharSequence;)V

    .line 166
    iget-object v0, p0, Lasl$d;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-static {}, Lasl;->c()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setExtraState([I)V

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    if-eqz p2, :cond_0

    .line 168
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 169
    iget-object v0, p0, Lasl$d;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p2}, Lcom/twitter/ui/widget/TwitterEditText;->setError(I)V

    goto :goto_0

    .line 171
    :cond_2
    iget-object v0, p0, Lasl$d;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p2}, Lcom/twitter/ui/widget/TwitterEditText;->setHelperMessage(I)V

    .line 172
    iget-object v0, p0, Lasl$d;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-static {}, Lasl;->d()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setExtraState([I)V

    goto :goto_0
.end method
