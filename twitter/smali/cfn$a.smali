.class public final Lcfn$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcfn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcfn;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field private b:J

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcfn$c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcfn$a;)J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcfn$a;->b:J

    return-wide v0
.end method

.method static synthetic b(Lcfn$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcfn$a;->c:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcfn$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcfn$a;->d:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lcfn$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcfn$a;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(J)Lcfn$a;
    .locals 1

    .prologue
    .line 87
    iput-wide p1, p0, Lcfn$a;->b:J

    .line 88
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcfn$a;
    .locals 1

    .prologue
    .line 63
    invoke-static {p1}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfn$a;->a:Ljava/lang/String;

    .line 64
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcfn$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcfn$a;"
        }
    .end annotation

    .prologue
    .line 69
    iput-object p1, p0, Lcfn$a;->d:Ljava/util/List;

    .line 70
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcfn$a;
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcfn$a;->e:Ljava/lang/String;

    .line 82
    return-object p0
.end method

.method public b(Ljava/util/List;)Lcfn$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcfn$c;",
            ">;)",
            "Lcfn$a;"
        }
    .end annotation

    .prologue
    .line 75
    iput-object p1, p0, Lcfn$a;->c:Ljava/util/List;

    .line 76
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lcfn$a;->e()Lcfn;

    move-result-object v0

    return-object v0
.end method

.method protected c_()V
    .locals 1

    .prologue
    .line 93
    invoke-super {p0}, Lcom/twitter/util/object/i;->c_()V

    .line 94
    iget-object v0, p0, Lcfn$a;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 95
    const-string/jumbo v0, "UNDEFINED"

    iput-object v0, p0, Lcfn$a;->e:Ljava/lang/String;

    .line 97
    :cond_0
    return-void
.end method

.method protected e()Lcfn;
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lcfn;

    invoke-direct {v0, p0}, Lcfn;-><init>(Lcfn$a;)V

    return-object v0
.end method
