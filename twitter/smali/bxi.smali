.class public Lbxi;
.super Landroid/text/SpannableString;
.source "Twttr"


# direct methods
.method constructor <init>(Ljava/lang/String;Landroid/view/View$OnClickListener;I)V
    .locals 4

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 19
    const v0, 0xfeff

    invoke-static {p1, v0}, Lbxi;->a(Ljava/lang/String;C)[I

    move-result-object v0

    .line 20
    new-instance v1, Lbxi$1;

    invoke-direct {v1, p0, p3, p2}, Lbxi$1;-><init>(Lbxi;ILandroid/view/View$OnClickListener;)V

    .line 26
    const/4 v2, 0x0

    aget v2, v0, v2

    const/4 v3, 0x1

    aget v0, v0, v3

    const/16 v3, 0x21

    invoke-virtual {p0, v1, v2, v0, v3}, Lbxi;->setSpan(Ljava/lang/Object;III)V

    .line 27
    return-void
.end method

.method private static a(Ljava/lang/String;C)[I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 31
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 32
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    .line 33
    if-eq v1, v4, :cond_0

    if-eq v2, v4, :cond_0

    .line 34
    new-array v0, v6, [I

    add-int/lit8 v1, v1, 0x1

    aput v1, v0, v3

    aput v2, v0, v5

    .line 36
    :goto_0
    return-object v0

    :cond_0
    new-array v0, v6, [I

    aput v3, v0, v3

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    aput v1, v0, v5

    goto :goto_0
.end method
