.class public final Lcld$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcld;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:Lclf;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcld$1;)V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Lcld$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lcld$a;)Lclf;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcld$a;->a:Lclf;

    return-object v0
.end method


# virtual methods
.method public a(Lclf;)Lcld$a;
    .locals 1

    .prologue
    .line 111
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lclf;

    iput-object v0, p0, Lcld$a;->a:Lclf;

    .line 112
    return-object p0
.end method

.method public a()Lcle;
    .locals 3

    .prologue
    .line 103
    iget-object v0, p0, Lcld$a;->a:Lclf;

    if-nez v0, :cond_0

    .line 104
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lclf;

    .line 105
    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_0
    new-instance v0, Lcld;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcld;-><init>(Lcld$a;Lcld$1;)V

    return-object v0
.end method
