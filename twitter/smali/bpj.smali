.class public Lbpj;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbpj$a;
    }
.end annotation


# static fields
.field private static final a:Lcok$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcok$b",
            "<",
            "Lbpj$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lbpj$1;

    invoke-direct {v0}, Lbpj$1;-><init>()V

    .line 20
    invoke-static {v0}, Lcok;->a(Lcom/twitter/util/object/j;)Lcok$b;

    move-result-object v0

    sput-object v0, Lbpj;->a:Lcok$b;

    .line 19
    return-void
.end method

.method public static a(Lcom/twitter/model/core/Tweet;)J
    .locals 2

    .prologue
    .line 42
    invoke-static {}, Lbpj;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->u:J

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->t:J

    goto :goto_0
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lbpj;->a:Lcok$b;

    invoke-virtual {v0}, Lcok$b;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpj$a;

    iget-boolean v0, v0, Lbpj$a;->b:Z

    return v0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lbpj;->a:Lcok$b;

    invoke-virtual {v0}, Lcok$b;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpj$a;

    iget-boolean v0, v0, Lbpj$a;->a:Z

    return v0
.end method
