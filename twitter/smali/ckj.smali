.class public Lckj;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lckp;
.implements Lcku$a;
.implements Lckw$a;


# instance fields
.field private final a:Landroid/view/WindowManager;

.field private final b:Lckt;

.field private final c:Lckw;

.field private final d:Lckk;

.field private final e:Lcku;

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lckm;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcks;

.field private final h:Landroid/graphics/Rect;

.field private i:Z


# direct methods
.method public constructor <init>(Lckl;Lckt;Lcks;Landroid/view/WindowManager;Lckw;Lcku;)V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lckj;->f:Ljava/util/Set;

    .line 34
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lckj;->h:Landroid/graphics/Rect;

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lckj;->i:Z

    .line 45
    iput-object p4, p0, Lckj;->a:Landroid/view/WindowManager;

    .line 46
    iput-object p2, p0, Lckj;->b:Lckt;

    .line 47
    iput-object p5, p0, Lckj;->c:Lckw;

    .line 48
    iget-object v0, p0, Lckj;->c:Lckw;

    invoke-virtual {v0, p0}, Lckw;->a(Lckw$a;)V

    .line 49
    iput-object p6, p0, Lckj;->e:Lcku;

    .line 50
    iget-object v0, p0, Lckj;->e:Lcku;

    invoke-virtual {v0, p0}, Lcku;->a(Lcku$a;)V

    .line 52
    if-nez p3, :cond_0

    iget-object v0, p0, Lckj;->b:Lckt;

    invoke-interface {v0}, Lckt;->a()Lcks;

    move-result-object p3

    :cond_0
    iput-object p3, p0, Lckj;->g:Lcks;

    .line 53
    iget-object v0, p0, Lckj;->g:Lcks;

    iget-object v0, v0, Lcks;->a:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    if-ltz v0, :cond_1

    iget-object v0, p0, Lckj;->g:Lcks;

    iget-object v0, v0, Lcks;->a:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    if-gez v0, :cond_2

    .line 54
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Sorry, at the moment Dock only supports fixed size values in DockParams.size."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_2
    invoke-interface {p1}, Lckl;->a()Lckk;

    move-result-object v0

    iput-object v0, p0, Lckj;->d:Lckk;

    .line 59
    iget-object v0, p0, Lckj;->d:Lckk;

    invoke-virtual {v0, p0}, Lckk;->a(Lckp;)V

    .line 60
    invoke-direct {p0}, Lckj;->g()V

    .line 61
    iget-object v0, p0, Lckj;->f:Ljava/util/Set;

    iget-object v1, p0, Lckj;->d:Lckk;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 62
    return-void
.end method

.method private a(Landroid/view/WindowManager$LayoutParams;)V
    .locals 2

    .prologue
    .line 104
    const/16 v0, 0x7d3

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 105
    const/4 v0, -0x3

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 106
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v1, 0x1020228

    or-int/2addr v0, v1

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 111
    const/16 v0, 0x33

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 112
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lckj;->d:Lckk;

    invoke-virtual {v0}, Lckk;->g()Lcom/twitter/ui/anim/AnimatableParams;

    move-result-object v0

    .line 80
    invoke-direct {p0, v0}, Lckj;->a(Landroid/view/WindowManager$LayoutParams;)V

    .line 81
    iget-object v1, p0, Lckj;->g:Lcks;

    iget-object v1, v1, Lcks;->a:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iput v1, v0, Lcom/twitter/ui/anim/AnimatableParams;->width:I

    .line 82
    iget-object v1, p0, Lckj;->g:Lcks;

    iget-object v1, v1, Lcks;->a:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    iput v1, v0, Lcom/twitter/ui/anim/AnimatableParams;->height:I

    .line 84
    iget-object v1, p0, Lckj;->g:Lcks;

    iget-object v2, p0, Lckj;->h:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Lcks;->a(Landroid/graphics/Rect;)Landroid/graphics/PointF;

    move-result-object v1

    .line 85
    invoke-virtual {v0, v1}, Lcom/twitter/ui/anim/AnimatableParams;->b(Landroid/graphics/PointF;)V

    .line 86
    invoke-virtual {v0, v1}, Lcom/twitter/ui/anim/AnimatableParams;->a(Landroid/graphics/PointF;)V

    .line 87
    invoke-virtual {v0, v1}, Lcom/twitter/ui/anim/AnimatableParams;->c(Landroid/graphics/PointF;)V

    .line 89
    iget-object v1, p0, Lckj;->d:Lckk;

    invoke-virtual {v1, v0}, Lckk;->a(Lcom/twitter/ui/anim/AnimatableParams;)V

    .line 90
    iget-object v0, p0, Lckj;->d:Lckk;

    iget-object v1, p0, Lckj;->e:Lcku;

    invoke-virtual {v0, v1}, Lckk;->a(Lckk$a;)V

    .line 91
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lckm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lckj;->f:Ljava/util/Set;

    return-object v0
.end method

.method public a(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-lez v0, :cond_0

    .line 163
    iget-object v0, p0, Lckj;->h:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 164
    invoke-virtual {p0}, Lckj;->e()V

    .line 166
    :cond_0
    return-void
.end method

.method public a(Lckm;)V
    .locals 3

    .prologue
    .line 190
    iget-object v0, p0, Lckj;->a:Landroid/view/WindowManager;

    invoke-virtual {p1}, Lckm;->c()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p1}, Lckm;->g()Lcom/twitter/ui/anim/AnimatableParams;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 191
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 118
    invoke-virtual {p0}, Lckj;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 132
    :goto_0
    return-void

    .line 121
    :cond_0
    iget-object v0, p0, Lckj;->e:Lcku;

    invoke-virtual {v0, v3}, Lcku;->a(Z)V

    .line 123
    iget-object v0, p0, Lckj;->c:Lckw;

    invoke-virtual {v0}, Lckw;->a()Landroid/graphics/Rect;

    move-result-object v0

    .line 125
    iget-object v1, p0, Lckj;->h:Landroid/graphics/Rect;

    invoke-virtual {v1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 126
    invoke-direct {p0}, Lckj;->g()V

    .line 128
    invoke-virtual {p0}, Lckj;->a()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lckm;

    .line 129
    iget-object v2, p0, Lckj;->a:Landroid/view/WindowManager;

    invoke-virtual {v0, v2}, Lckm;->a(Landroid/view/WindowManager;)V

    goto :goto_1

    .line 131
    :cond_1
    iput-boolean v3, p0, Lckj;->i:Z

    goto :goto_0
.end method

.method public b(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 201
    invoke-virtual {p0, p1}, Lckj;->a(Landroid/graphics/Rect;)V

    .line 202
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 138
    invoke-virtual {p0}, Lckj;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    :goto_0
    return-void

    .line 141
    :cond_0
    invoke-virtual {p0}, Lckj;->a()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lckm;

    .line 142
    iget-object v2, p0, Lckj;->a:Landroid/view/WindowManager;

    invoke-virtual {v0, v2}, Lckm;->b(Landroid/view/WindowManager;)V

    goto :goto_1

    .line 144
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lckj;->i:Z

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 151
    iget-boolean v0, p0, Lckj;->i:Z

    return v0
.end method

.method protected e()V
    .locals 5

    .prologue
    .line 173
    iget-object v0, p0, Lckj;->h:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, p0, Lckj;->h:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-gtz v0, :cond_1

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    iget-object v0, p0, Lckj;->d:Lckk;

    invoke-virtual {v0}, Lckk;->g()Lcom/twitter/ui/anim/AnimatableParams;

    move-result-object v0

    .line 178
    iget-object v1, p0, Lckj;->g:Lcks;

    iget-object v2, p0, Lckj;->h:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Lcks;->a(Landroid/graphics/Rect;)Landroid/graphics/PointF;

    move-result-object v1

    .line 179
    invoke-virtual {v0}, Lcom/twitter/ui/anim/AnimatableParams;->a()Landroid/graphics/PointF;

    move-result-object v2

    iget v3, v1, Landroid/graphics/PointF;->x:F

    iget v4, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/PointF;->equals(FF)Z

    move-result v2

    if-nez v2, :cond_0

    .line 180
    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {v0}, Lcom/twitter/ui/anim/AnimatableParams;->a()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    .line 181
    invoke-virtual {v0}, Lcom/twitter/ui/anim/AnimatableParams;->a()Landroid/graphics/PointF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 182
    invoke-virtual {v0, v1, v2}, Lcom/twitter/ui/anim/AnimatableParams;->b(Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    .line 184
    iget-object v0, p0, Lckj;->d:Lckk;

    invoke-virtual {p0, v0}, Lckj;->a(Lckm;)V

    goto :goto_0
.end method

.method public f()V
    .locals 0

    .prologue
    .line 206
    return-void
.end method
