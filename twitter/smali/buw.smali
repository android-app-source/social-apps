.class public Lbuw;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbuw$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lbuw$a;

.field private final c:Lbus;

.field private final d:Lbva;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbuw$a;Lbus;Lbva;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lbuw;->a:Landroid/content/Context;

    .line 43
    iput-object p2, p0, Lbuw;->b:Lbuw$a;

    .line 44
    iput-object p3, p0, Lbuw;->c:Lbus;

    .line 45
    iput-object p4, p0, Lbuw;->d:Lbva;

    .line 46
    return-void
.end method

.method static synthetic a(Lbuw;)Lbuw$a;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lbuw;->b:Lbuw$a;

    return-object v0
.end method

.method public static a()Lbuw;
    .locals 1

    .prologue
    .line 34
    invoke-static {}, Lbms;->ah()Lbms;

    move-result-object v0

    invoke-virtual {v0}, Lbms;->U()Lbuw;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lbuw;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lbuw;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public b()Lrx/g;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lbuw;->d:Lbva;

    iget-object v1, p0, Lbuw;->a:Landroid/content/Context;

    .line 80
    invoke-static {v1}, Lcom/google/android/gcm/GCMBaseIntentService;->a(Landroid/content/Context;)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lbva;->a(J)Lrx/g;

    move-result-object v0

    new-instance v1, Lbuw$2;

    invoke-direct {v1, p0}, Lbuw$2;-><init>(Lbuw;)V

    .line 81
    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/b;)Lrx/g;

    move-result-object v0

    new-instance v1, Lbuw$1;

    invoke-direct {v1, p0}, Lbuw$1;-><init>(Lbuw;)V

    .line 87
    invoke-virtual {v0, v1}, Lrx/g;->b(Lrx/functions/b;)Lrx/g;

    move-result-object v0

    .line 79
    return-object v0
.end method
