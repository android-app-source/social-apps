.class public Lbfi;
.super Lbao;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbao",
        "<",
        "Lcom/twitter/library/api/y;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/client/Session;

.field private final b:Lcom/twitter/model/json/stratostore/JsonInterestSelections;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/json/stratostore/JsonInterestSelections;)V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lbfi;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lbao;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 32
    iput-object p2, p0, Lbfi;->a:Lcom/twitter/library/client/Session;

    .line 33
    iput-object p3, p0, Lbfi;->b:Lcom/twitter/model/json/stratostore/JsonInterestSelections;

    .line 34
    return-void
.end method


# virtual methods
.method protected b()Lcom/twitter/library/service/d$a;
    .locals 6

    .prologue
    .line 39
    invoke-virtual {p0}, Lbfi;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 40
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "strato"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "column"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "User"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lbfi;->a:Lcom/twitter/library/client/Session;

    .line 42
    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "interestSelections"

    aput-object v3, v1, v2

    .line 41
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->b([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 45
    :try_start_0
    new-instance v0, Lcom/twitter/network/apache/entity/c;

    iget-object v2, p0, Lbfi;->b:Lcom/twitter/model/json/stratostore/JsonInterestSelections;

    invoke-static {v2}, Lcom/twitter/model/json/common/g;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/twitter/network/apache/a;->a:Ljava/nio/charset/Charset;

    invoke-direct {v0, v2, v3}, Lcom/twitter/network/apache/entity/c;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V

    .line 46
    const-string/jumbo v2, "application/json"

    invoke-virtual {v0, v2}, Lcom/twitter/network/apache/entity/c;->a(Ljava/lang/String;)V

    .line 48
    invoke-virtual {v1, v0}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/apache/e;)Lcom/twitter/library/service/d$a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :goto_0
    return-object v1

    .line 49
    :catch_0
    move-exception v0

    .line 50
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected e()Lcom/twitter/library/api/y;
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lbfi;->e()Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method
