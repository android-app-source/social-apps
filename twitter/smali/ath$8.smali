.class Lath$8;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lath;->u()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lath;


# direct methods
.method constructor <init>(Lath;)V
    .locals 0

    .prologue
    .line 430
    iput-object p1, p0, Lath$8;->a:Lath;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 433
    iget-object v0, p0, Lath$8;->a:Lath;

    invoke-static {v0}, Lath;->f(Lath;)Latg$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lath$8;->a:Lath;

    invoke-static {v0}, Lath;->f(Lath;)Latg$a;

    move-result-object v0

    invoke-interface {v0}, Latg$a;->d()V

    .line 437
    :cond_0
    iget-object v0, p0, Lath$8;->a:Lath;

    invoke-static {v0}, Lath;->b(Lath;)Lcom/twitter/android/widget/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/widget/j;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 438
    iget-object v0, p0, Lath$8;->a:Lath;

    iget-object v0, v0, Lath;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 439
    iget-object v0, p0, Lath$8;->a:Lath;

    invoke-static {v0}, Lath;->d(Lath;)V

    .line 451
    :goto_0
    return-void

    .line 441
    :cond_1
    new-instance v0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    iget-object v1, p0, Lath$8;->a:Lath;

    iget-object v1, v1, Lath;->a:Landroid/support/v4/app/FragmentActivity;

    const v2, 0x7f0a03c9

    .line 442
    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lath$8;->a:Lath;

    iget-object v2, v2, Lath;->a:Landroid/support/v4/app/FragmentActivity;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v5, v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;-><init>(Ljava/lang/String;Landroid/content/Context;[Ljava/lang/String;)V

    const-string/jumbo v1, ":composition::add_photo"

    .line 444
    invoke-virtual {v0, v1}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->f(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    move-result-object v0

    .line 445
    invoke-virtual {v0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->a()Landroid/content/Intent;

    move-result-object v0

    .line 446
    iget-object v1, p0, Lath$8;->a:Lath;

    invoke-static {v1}, Lath;->m(Lath;)Lcom/twitter/app/common/base/d;

    move-result-object v1

    invoke-static {}, Lath;->s()I

    move-result v2

    invoke-virtual {v1, v2, v0}, Lcom/twitter/app/common/base/d;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 449
    :cond_2
    iget-object v0, p0, Lath$8;->a:Lath;

    invoke-static {v0}, Lath;->b(Lath;)Lcom/twitter/android/widget/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/widget/j;->c()V

    goto :goto_0
.end method
