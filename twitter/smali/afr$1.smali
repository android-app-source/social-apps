.class Lafr$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lafr;->a(J)Lrx/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/d",
        "<",
        "Ljava/util/List",
        "<",
        "Lafu$a;",
        ">;",
        "Ljava/util/List",
        "<",
        "Lafu$a;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Lafr;


# direct methods
.method constructor <init>(Lafr;J)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lafr$1;->b:Lafr;

    iput-wide p2, p0, Lafr$1;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lafr$1;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lafu$a;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lafu$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    invoke-static {p1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    invoke-static {}, Lafr;->a()Landroid/util/LruCache;

    move-result-object v0

    iget-object v1, p0, Lafr$1;->b:Lafr;

    invoke-static {v1}, Lafr;->a(Lafr;)J

    move-result-wide v2

    iget-wide v4, p0, Lafr$1;->a:J

    invoke-static {v2, v3, v4, v5}, Lafr;->a(JJ)Lcom/twitter/util/collection/Pair;

    move-result-object v1

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    :cond_0
    return-object p1
.end method
