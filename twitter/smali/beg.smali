.class Lbeg;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method static a(Lcom/twitter/library/service/d$a;Lbed;Ljava/util/List;)Lcom/twitter/library/service/d$a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/service/d$a;",
            "Lbed;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/json/notifications/JsonUserDevicesRequest2;",
            ">;)",
            "Lcom/twitter/library/service/d$a;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    const-string/jumbo v0, "app_id"

    iget-object v1, p1, Lbed;->b:Ljava/lang/String;

    .line 30
    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "push_protocol_version"

    iget v2, p1, Lbed;->f:I

    int-to-long v2, v2

    .line 31
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "app_version"

    iget-object v2, p1, Lbed;->d:Ljava/lang/String;

    .line 32
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "user_devices_request"

    .line 33
    invoke-static {p2}, Lcom/twitter/model/json/common/g;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 29
    return-object v0
.end method
