.class public Laej;
.super Laef;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laef",
        "<",
        "Lcam;",
        "Laef$a;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lael;

.field final b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method public constructor <init>(Lael;Lcom/twitter/android/notificationtimeline/a;Lcom/twitter/android/notificationtimeline/h;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p2, p3}, Laef;-><init>(Lcom/twitter/android/notificationtimeline/a;Lcom/twitter/android/notificationtimeline/h;)V

    .line 28
    iput-object p1, p0, Laej;->a:Lael;

    .line 29
    iput-object p4, p0, Laej;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 30
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)Laef$a;
    .locals 2

    .prologue
    .line 35
    new-instance v0, Laef$a;

    iget-object v1, p0, Laej;->a:Lael;

    invoke-virtual {v1, p1}, Lael;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Laef$a;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method protected bridge synthetic a(Landroid/content/Context;Lbzz;I)Lcom/twitter/analytics/model/ScribeItem;
    .locals 1

    .prologue
    .line 16
    check-cast p2, Lcam;

    invoke-virtual {p0, p1, p2, p3}, Laej;->a(Landroid/content/Context;Lcam;I)Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/content/Context;Lcam;I)Lcom/twitter/analytics/model/ScribeItem;
    .locals 2

    .prologue
    .line 48
    iget-object v0, p2, Lcam;->c:Lcom/twitter/model/core/Tweet;

    const/4 v1, 0x2

    invoke-static {p1, v0, p3, v1}, Lcom/twitter/android/notificationtimeline/a;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;II)Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Laef$a;Lbzz;)V
    .locals 0

    .prologue
    .line 16
    check-cast p2, Lcam;

    invoke-virtual {p0, p1, p2}, Laej;->a(Laef$a;Lcam;)V

    return-void
.end method

.method public a(Laef$a;Lcam;)V
    .locals 5

    .prologue
    .line 40
    invoke-super {p0, p1, p2}, Laef;->a(Laef$a;Lbzz;)V

    .line 41
    iget-object v0, p0, Laej;->a:Lael;

    invoke-virtual {p1}, Laef$a;->b()Landroid/view/View;

    move-result-object v1

    iget-object v2, p2, Lcam;->c:Lcom/twitter/model/core/Tweet;

    const-string/jumbo v3, "mention"

    invoke-virtual {p0, p2}, Laej;->a(Lbzz;)Z

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lael;->a(Landroid/view/View;Lcom/twitter/model/core/Tweet;Ljava/lang/String;Z)V

    .line 42
    return-void
.end method

.method public bridge synthetic a(Lckb$a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, Laef$a;

    check-cast p2, Lcam;

    invoke-virtual {p0, p1, p2}, Laej;->a(Laef$a;Lcam;)V

    return-void
.end method

.method public synthetic b(Landroid/view/ViewGroup;)Lckb$a;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Laej;->a(Landroid/view/ViewGroup;)Laef$a;

    move-result-object v0

    return-object v0
.end method
