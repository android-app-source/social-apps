.class public final Lcjf;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lcje;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/onboarding/Subtask;",
            "Landroid/content/Intent;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcjf;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcjf;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/onboarding/Subtask;",
            "Landroid/content/Intent;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    sget-boolean v0, Lcjf;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 20
    :cond_0
    iput-object p1, p0, Lcjf;->b:Lcta;

    .line 21
    return-void
.end method

.method public static a(Lcta;)Ldagger/internal/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/onboarding/Subtask;",
            "Landroid/content/Intent;",
            ">;>;)",
            "Ldagger/internal/c",
            "<",
            "Lcje;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    new-instance v0, Lcjf;

    invoke-direct {v0, p0}, Lcjf;-><init>(Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcje;
    .locals 2

    .prologue
    .line 25
    new-instance v1, Lcje;

    iget-object v0, p0, Lcjf;->b:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/object/d;

    invoke-direct {v1, v0}, Lcje;-><init>(Lcom/twitter/util/object/d;)V

    return-object v1
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lcjf;->a()Lcje;

    move-result-object v0

    return-object v0
.end method
