.class public Lchr;
.super Lchi;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lchr$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcho$a;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/lang/String;

.field public final f:Lchs;

.field public final g:Lcom/twitter/model/timeline/m;

.field public final h:Lcom/twitter/model/timeline/r;

.field public final i:Lchj;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 36
    const-string/jumbo v0, "Vertical"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "Carousel"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lchr;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lchr$a;)V
    .locals 4

    .prologue
    .line 58
    iget-object v0, p1, Lchr$a;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-wide v2, p1, Lchr$a;->b:J

    invoke-direct {p0, v0, v2, v3}, Lchi;-><init>(Ljava/lang/String;J)V

    .line 59
    iget-object v0, p1, Lchr$a;->c:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lchr;->d:Ljava/util/List;

    .line 61
    iget-object v0, p1, Lchr$a;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lchr;->e:Ljava/lang/String;

    .line 62
    iget-object v0, p1, Lchr$a;->e:Lchs;

    iput-object v0, p0, Lchr;->f:Lchs;

    .line 63
    iget-object v0, p1, Lchr$a;->f:Lcom/twitter/model/timeline/m;

    iput-object v0, p0, Lchr;->g:Lcom/twitter/model/timeline/m;

    .line 64
    iget-object v0, p1, Lchr$a;->g:Lcom/twitter/model/timeline/r;

    iput-object v0, p0, Lchr;->h:Lcom/twitter/model/timeline/r;

    .line 65
    iget-object v0, p1, Lchr$a;->h:Lchj;

    iput-object v0, p0, Lchr;->i:Lchj;

    .line 66
    return-void
.end method


# virtual methods
.method public b(Lcgx;Lchc;)Lcom/twitter/model/timeline/y;
    .locals 12

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 74
    iget-object v1, p0, Lchr;->e:Ljava/lang/String;

    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 122
    :goto_1
    return-object v7

    .line 74
    :sswitch_0
    const-string/jumbo v2, "Vertical"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v3

    goto :goto_0

    :sswitch_1
    const-string/jumbo v2, "Carousel"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v4

    goto :goto_0

    :pswitch_0
    move v2, v3

    .line 88
    :goto_2
    iget-object v0, p0, Lchr;->f:Lchs;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lchr;->f:Lchs;

    iget-boolean v0, v0, Lchs;->b:Z

    if-eqz v0, :cond_1

    move v5, v4

    .line 89
    :goto_3
    iget-object v0, p0, Lchr;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v8

    move v1, v3

    .line 90
    :goto_4
    iget-object v0, p0, Lchr;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 92
    iget-object v0, p0, Lchr;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcho$a;

    .line 93
    invoke-interface {v0, p1, p2}, Lcho$a;->a(Lcgx;Lchc;)Lcom/twitter/model/timeline/y$a;

    move-result-object v6

    .line 98
    if-nez v5, :cond_2

    move v0, v3

    .line 106
    :goto_5
    invoke-virtual {v6, v0}, Lcom/twitter/model/timeline/y$a;->b(I)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    iget-object v9, p0, Lchr;->b:Ljava/lang/String;

    .line 107
    invoke-virtual {v0, v9}, Lcom/twitter/model/timeline/y$a;->d(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    iget-wide v10, p0, Lchr;->c:J

    .line 108
    invoke-virtual {v0, v10, v11}, Lcom/twitter/model/timeline/y$a;->a(J)Lcom/twitter/model/timeline/y$a;

    .line 109
    invoke-virtual {v6}, Lcom/twitter/model/timeline/y$a;->r()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y;

    .line 110
    if-eqz v0, :cond_4

    .line 111
    invoke-virtual {v8, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 90
    :goto_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :pswitch_1
    move v2, v4

    .line 81
    goto :goto_2

    :cond_1
    move v5, v3

    .line 88
    goto :goto_3

    .line 100
    :cond_2
    iget-object v0, p0, Lchr;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v1, v0, :cond_3

    .line 102
    const/4 v0, 0x2

    goto :goto_5

    :cond_3
    move v0, v4

    .line 104
    goto :goto_5

    .line 113
    :cond_4
    new-instance v0, Lcpb;

    new-instance v9, Ljava/lang/IllegalStateException;

    const-string/jumbo v10, "moduleItem failed to build"

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v9}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    const-string/jumbo v9, "moduleItemBuilder"

    .line 114
    invoke-virtual {v0, v9, v6}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    .line 113
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    goto :goto_6

    .line 118
    :cond_5
    iget-object v0, p0, Lchr;->i:Lchj;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lchr;->i:Lchj;

    iget-object v0, v0, Lchj;->a:Ljava/util/List;

    .line 119
    invoke-virtual {p2, v0}, Lchc;->a(Ljava/util/List;)Lcom/twitter/model/timeline/k;

    move-result-object v0

    move-object v6, v0

    .line 120
    :goto_7
    iget-object v0, p0, Lchr;->i:Lchj;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lchr;->i:Lchj;

    iget-object v7, v0, Lchj;->b:Ljava/lang/String;

    .line 122
    :cond_6
    new-instance v0, Lcom/twitter/model/timeline/ad$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/ad$a;-><init>()V

    iget-object v1, p0, Lchr;->b:Ljava/lang/String;

    .line 123
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/ad$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ad$a;

    iget-wide v10, p0, Lchr;->c:J

    .line 124
    invoke-virtual {v0, v10, v11}, Lcom/twitter/model/timeline/ad$a;->a(J)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ad$a;

    iget-object v1, p0, Lchr;->f:Lchs;

    .line 125
    invoke-static {v1, p1, p2}, Lcie$a;->a(Lcie;Lcgx;Lchc;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcha;

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/ad$a;->a(Lcha;)Lcom/twitter/model/timeline/ad$a;

    move-result-object v0

    iget-object v1, p0, Lchr;->g:Lcom/twitter/model/timeline/m;

    .line 126
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/ad$a;->a(Lcom/twitter/model/timeline/m;)Lcom/twitter/model/timeline/ad$a;

    move-result-object v0

    .line 127
    invoke-virtual {v0, v2}, Lcom/twitter/model/timeline/ad$a;->c(I)Lcom/twitter/model/timeline/ad$a;

    move-result-object v1

    .line 128
    invoke-virtual {v8}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/twitter/model/timeline/ad$a;->a(Ljava/util/List;)Lcom/twitter/model/timeline/ad$a;

    move-result-object v0

    .line 129
    invoke-virtual {v0, v6}, Lcom/twitter/model/timeline/ad$a;->a(Lcom/twitter/model/timeline/k;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ad$a;

    .line 130
    invoke-virtual {v0, v7}, Lcom/twitter/model/timeline/ad$a;->a(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ad$a;

    iget-object v1, p0, Lchr;->h:Lcom/twitter/model/timeline/r;

    .line 131
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/ad$a;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ad$a;

    if-eqz v5, :cond_8

    .line 132
    :goto_8
    invoke-virtual {v0, v4}, Lcom/twitter/model/timeline/ad$a;->b(I)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ad$a;

    .line 133
    invoke-virtual {v0}, Lcom/twitter/model/timeline/ad$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y;

    move-object v7, v0

    .line 122
    goto/16 :goto_1

    :cond_7
    move-object v6, v7

    .line 119
    goto :goto_7

    :cond_8
    move v4, v3

    .line 131
    goto :goto_8

    .line 74
    :sswitch_data_0
    .sparse-switch
        -0x7269346a -> :sswitch_0
        0x406c580 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
