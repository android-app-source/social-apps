.class public final Lccu;
.super Lcck;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lccu$b;,
        Lccu$a;
    }
.end annotation


# static fields
.field public static final b:Lccu$b;


# instance fields
.field public final c:Lccs;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lccu$b;

    invoke-direct {v0}, Lccu$b;-><init>()V

    sput-object v0, Lccu;->b:Lccu$b;

    return-void
.end method

.method private constructor <init>(Lccu$a;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcck;-><init>(Lcck$a;)V

    .line 22
    invoke-static {p1}, Lccu$a;->a(Lccu$a;)Lccs;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccs;

    iput-object v0, p0, Lccu;->c:Lccs;

    .line 23
    return-void
.end method

.method synthetic constructor <init>(Lccu$a;Lccu$1;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lccu;-><init>(Lccu$a;)V

    return-void
.end method

.method private a(Lccu;)Z
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lccu;->c:Lccs;

    iget-object v1, p1, Lccu;->c:Lccs;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    const-string/jumbo v0, "text_input"

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcck;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lccu;

    if-eqz v0, :cond_1

    check-cast p1, Lccu;

    .line 35
    invoke-direct {p0, p1}, Lccu;->a(Lccu;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 34
    :goto_0
    return v0

    .line 35
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 40
    invoke-super {p0}, Lcck;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lccu;->c:Lccs;

    invoke-virtual {v1}, Lccs;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
