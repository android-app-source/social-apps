.class public Laek;
.super Laef;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laef",
        "<",
        "Lcah;",
        "Laef$a;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lael;

.field final b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method public constructor <init>(Lael;Lcom/twitter/android/notificationtimeline/a;Lcom/twitter/android/notificationtimeline/h;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p2, p3}, Laef;-><init>(Lcom/twitter/android/notificationtimeline/a;Lcom/twitter/android/notificationtimeline/h;)V

    .line 34
    iput-object p1, p0, Laek;->a:Lael;

    .line 35
    iput-object p4, p0, Laek;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 36
    return-void
.end method

.method private static a(Lcap;)Ljava/lang/String;
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    .line 81
    iget v1, p0, Lcap;->d:I

    .line 82
    sparse-switch v1, :sswitch_data_0

    .line 96
    sget-object v0, Lbzx$a;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 97
    new-instance v2, Lcpb;

    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Scribe component requested for unsupported eventType="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v0, :cond_0

    .line 99
    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    .line 97
    invoke-static {v2}, Lcpd;->c(Lcpb;)V

    .line 100
    const-string/jumbo v0, ""

    :goto_1
    return-object v0

    .line 84
    :sswitch_0
    const-string/jumbo v0, "media_tag"

    goto :goto_1

    .line 87
    :sswitch_1
    const-string/jumbo v0, "mention"

    goto :goto_1

    .line 90
    :sswitch_2
    const-string/jumbo v0, "quote"

    goto :goto_1

    .line 93
    :sswitch_3
    const-string/jumbo v0, "reply"

    goto :goto_1

    .line 99
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 82
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x3 -> :sswitch_3
        0xe -> :sswitch_2
        0xf -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)Laef$a;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Laef$a;

    iget-object v1, p0, Laek;->a:Lael;

    invoke-virtual {v1, p1}, Lael;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Laef$a;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method protected bridge synthetic a(Landroid/content/Context;Lbzz;I)Lcom/twitter/analytics/model/ScribeItem;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    .line 22
    check-cast p2, Lcah;

    invoke-virtual {p0, p1, p2, p3}, Laek;->a(Landroid/content/Context;Lcah;I)Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/content/Context;Lcah;I)Lcom/twitter/analytics/model/ScribeItem;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p2, Lcah;->b:Lcac;

    check-cast v0, Lcap;

    .line 64
    iget-object v1, p2, Lcah;->b:Lcac;

    check-cast v1, Lcap;

    iget v1, v1, Lcap;->d:I

    sparse-switch v1, :sswitch_data_0

    .line 73
    iget-object v0, p2, Lcah;->b:Lcac;

    check-cast v0, Lcap;

    iget v0, v0, Lcap;->d:I

    invoke-static {p3, v0}, Lcom/twitter/android/notificationtimeline/a;->a(II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    :goto_0
    return-object v0

    .line 70
    :sswitch_0
    invoke-virtual {p2}, Lcah;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/Tweet;

    iget v0, v0, Lcap;->d:I

    .line 69
    invoke-static {p1, v1, p3, v0}, Lcom/twitter/android/notificationtimeline/a;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;II)Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    goto :goto_0

    .line 64
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0xe -> :sswitch_0
        0xf -> :sswitch_0
    .end sparse-switch
.end method

.method public bridge synthetic a(Laef$a;Lbzz;)V
    .locals 0

    .prologue
    .line 22
    check-cast p2, Lcah;

    invoke-virtual {p0, p1, p2}, Laek;->a(Laef$a;Lcah;)V

    return-void
.end method

.method public a(Laef$a;Lcah;)V
    .locals 5

    .prologue
    .line 46
    invoke-super {p0, p1, p2}, Laef;->a(Laef$a;Lbzz;)V

    .line 47
    invoke-virtual {p2}, Lcah;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 48
    if-eqz v0, :cond_0

    .line 49
    iget-object v2, p0, Laek;->a:Lael;

    invoke-virtual {p1}, Laef$a;->b()Landroid/view/View;

    move-result-object v3

    iget-object v1, p2, Lcah;->b:Lcac;

    check-cast v1, Lcap;

    invoke-static {v1}, Laek;->a(Lcap;)Ljava/lang/String;

    move-result-object v1

    .line 50
    invoke-virtual {p0, p2}, Laek;->a(Lbzz;)Z

    move-result v4

    .line 49
    invoke-virtual {v2, v3, v0, v1, v4}, Lael;->a(Landroid/view/View;Lcom/twitter/model/core/Tweet;Ljava/lang/String;Z)V

    .line 55
    :goto_0
    return-void

    .line 52
    :cond_0
    new-instance v0, Lcpb;

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Tweet object was null when binding a tweet activity row"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Lckb$a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Laef$a;

    check-cast p2, Lcah;

    invoke-virtual {p0, p1, p2}, Laek;->a(Laef$a;Lcah;)V

    return-void
.end method

.method public synthetic b(Landroid/view/ViewGroup;)Lckb$a;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Laek;->a(Landroid/view/ViewGroup;)Laef$a;

    move-result-object v0

    return-object v0
.end method
