.class Lcfe$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcfe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcfe;",
        "Lcfe$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcfe$1;)V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Lcfe$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcfe$a;
    .locals 1

    .prologue
    .line 117
    new-instance v0, Lcfe$a;

    invoke-direct {v0}, Lcfe$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcfe$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 123
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcfe$a;->a(J)Lcfe$a;

    move-result-object v1

    sget-object v0, Lcfd;->h:Lcom/twitter/util/serialization/l;

    .line 125
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 124
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcfe$a;->a(Ljava/util/List;)Lcfe$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/util/serialization/f;->c:Lcom/twitter/util/serialization/l;

    .line 126
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v1, v0}, Lcfe$a;->a(Ljava/lang/Integer;)Lcfe$a;

    .line 127
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 104
    check-cast p2, Lcfe$a;

    invoke-virtual {p0, p1, p2, p3}, Lcfe$b;->a(Lcom/twitter/util/serialization/n;Lcfe$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcfe;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    iget-wide v0, p2, Lcfe;->b:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcfe;->c:Ljava/util/List;

    sget-object v2, Lcfd;->h:Lcom/twitter/util/serialization/l;

    .line 110
    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 109
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcfe;->d:Ljava/lang/Integer;

    sget-object v2, Lcom/twitter/util/serialization/f;->c:Lcom/twitter/util/serialization/l;

    .line 111
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 112
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    check-cast p2, Lcfe;

    invoke-virtual {p0, p1, p2}, Lcfe$b;->a(Lcom/twitter/util/serialization/o;Lcfe;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0}, Lcfe$b;->a()Lcfe$a;

    move-result-object v0

    return-object v0
.end method
