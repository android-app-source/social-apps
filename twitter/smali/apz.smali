.class public Lapz;
.super Lapv;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lapz$a;,
        Lapz$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lapv",
        "<",
        "Lapz$b;",
        ">;"
    }
.end annotation


# instance fields
.field private final v:Lcom/twitter/media/ui/image/UserImageView;

.field private final w:Lcom/twitter/app/dm/widget/ReceivedMessageBylineView;

.field private final x:Landroid/view/View;

.field private final y:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/app/dm/ag;",
            ">;"
        }
    .end annotation
.end field

.field private final z:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lapl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lapz$a;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lapv;-><init>(Lapv$a;)V

    .line 36
    iget-object v0, p1, Lapz$a;->a:Lapn$b;

    check-cast v0, Lapz$b;

    .line 37
    invoke-static {v0}, Lapz$b;->a(Lapz$b;)Lcom/twitter/media/ui/image/UserImageView;

    move-result-object v1

    iput-object v1, p0, Lapz;->v:Lcom/twitter/media/ui/image/UserImageView;

    .line 38
    invoke-static {v0}, Lapz$b;->b(Lapz$b;)Lcom/twitter/app/dm/widget/ReceivedMessageBylineView;

    move-result-object v1

    iput-object v1, p0, Lapz;->w:Lcom/twitter/app/dm/widget/ReceivedMessageBylineView;

    .line 39
    invoke-static {v0}, Lapz$b;->c(Lapz$b;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lapz;->x:Landroid/view/View;

    .line 40
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, Lapz$a;->a(Lapz$a;)Lcom/twitter/app/dm/ag;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lapz;->y:Ljava/lang/ref/WeakReference;

    .line 41
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, Lapz$a;->b(Lapz$a;)Lapl;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lapz;->z:Ljava/lang/ref/WeakReference;

    .line 42
    return-void
.end method

.method synthetic constructor <init>(Lapz$a;Lapz$1;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lapz;-><init>(Lapz$a;)V

    return-void
.end method

.method static synthetic a(Lapz;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lapz;->q()V

    return-void
.end method

.method private o()V
    .locals 4

    .prologue
    .line 69
    iget-object v0, p0, Lapz;->x:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 70
    iget-object v0, p0, Lapz;->z:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapl;

    .line 72
    if-eqz v0, :cond_0

    .line 73
    iget-object v1, p0, Lapz;->a:Lcom/twitter/model/dms/c;

    check-cast v1, Lcom/twitter/model/dms/a;

    invoke-virtual {v1}, Lcom/twitter/model/dms/a;->h()Lcck;

    move-result-object v1

    .line 74
    invoke-interface {v0, v1}, Lapl;->b(Lcck;)Z

    move-result v2

    .line 84
    instance-of v3, v1, Lcco;

    if-eqz v3, :cond_1

    .line 85
    if-eqz v2, :cond_0

    .line 86
    iget-object v2, p0, Lapz;->x:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 87
    iget-object v2, p0, Lapz;->x:Landroid/view/View;

    new-instance v3, Lapz$1;

    invoke-direct {v3, p0, v0, v1}, Lapz$1;-><init>(Lapz;Lapl;Lcck;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    iget-boolean v3, p0, Lapz;->d:Z

    if-eqz v3, :cond_0

    .line 95
    if-eqz v2, :cond_2

    .line 96
    invoke-interface {v0, v1}, Lapl;->a(Lcck;)V

    goto :goto_0

    .line 98
    :cond_2
    invoke-interface {v0}, Lapl;->q()V

    goto :goto_0
.end method

.method private p()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 109
    iget-object v0, p0, Lapz;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->a()J

    move-result-wide v2

    .line 112
    iget-object v0, p0, Lapz;->y:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/ag;

    .line 113
    if-eqz v0, :cond_2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/app/dm/ag;->f(J)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 120
    invoke-virtual {v0}, Lcom/twitter/app/dm/ag;->e()Z

    move-result v4

    if-nez v4, :cond_0

    .line 121
    iget-object v4, p0, Lapz;->u:Landroid/view/ViewGroup;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 123
    :cond_0
    invoke-virtual {v0, v2, v3}, Lcom/twitter/app/dm/ag;->g(J)V

    .line 124
    iget-object v2, p0, Lapz;->u:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/ag;->a(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v2

    .line 125
    if-eqz v2, :cond_1

    .line 126
    const/4 v0, 0x1

    .line 127
    new-instance v1, Lapz$2;

    invoke-direct {v1, p0}, Lapz$2;-><init>(Lapz;)V

    invoke-virtual {v2, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 137
    invoke-virtual {v2}, Landroid/animation/Animator;->start()V

    .line 143
    :goto_0
    return v0

    .line 139
    :cond_1
    iget-object v0, p0, Lapz;->u:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private q()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 183
    iget-object v0, p0, Lapz;->v:Lcom/twitter/media/ui/image/UserImageView;

    new-instance v1, Lapz$3;

    invoke-direct {v1, p0}, Lapz$3;-><init>(Lapz;)V

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    iget-object v0, p0, Lapz;->v:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0, v4}, Lcom/twitter/media/ui/image/UserImageView;->setVisibility(I)V

    .line 190
    iget-object v0, p0, Lapz;->v:Lcom/twitter/media/ui/image/UserImageView;

    iget-object v1, p0, Lapz;->b:Lcom/twitter/model/dms/m;

    iget-object v1, v1, Lcom/twitter/model/dms/m;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;)Z

    .line 193
    iget-object v0, p0, Lapz;->b:Lcom/twitter/model/dms/m;

    iget-object v0, v0, Lcom/twitter/model/dms/m;->b:Ljava/lang/String;

    .line 194
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    iget-object v1, p0, Lapz;->h:Landroid/content/res/Resources;

    const v2, 0x7f0a041c

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 196
    iget-object v1, p0, Lapz;->v:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 198
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lapz;->p()Z

    move-result v0

    .line 47
    if-nez v0, :cond_0

    .line 48
    invoke-direct {p0}, Lapz;->q()V

    .line 51
    :cond_0
    invoke-super {p0}, Lapv;->a()V

    .line 53
    invoke-direct {p0}, Lapz;->o()V

    .line 54
    return-void
.end method

.method a(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 148
    iget-object v2, p0, Lapz;->l:Landroid/view/View;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 149
    iget-object v0, p0, Lapz;->k:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 150
    return-void

    .line 148
    :cond_0
    const/4 v0, 0x4

    goto :goto_0

    .line 149
    :cond_1
    const/16 v1, 0x8

    goto :goto_1
.end method

.method b(Z)V
    .locals 2

    .prologue
    .line 58
    invoke-super {p0, p1}, Lapv;->b(Z)V

    .line 59
    iget-object v0, p0, Lapz;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->o()I

    move-result v0

    const/16 v1, 0x13

    if-ne v0, v1, :cond_0

    .line 60
    iget-object v0, p0, Lapz;->l:Landroid/view/View;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 61
    invoke-virtual {p0}, Lapz;->n()Lcom/twitter/app/dm/widget/ReceivedMessageBylineView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/ReceivedMessageBylineView;->b()V

    .line 66
    :goto_0
    return-void

    .line 63
    :cond_0
    invoke-virtual {p0}, Lapz;->e()V

    .line 64
    invoke-virtual {p0}, Lapz;->n()Lcom/twitter/app/dm/widget/ReceivedMessageBylineView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/ReceivedMessageBylineView;->a()V

    goto :goto_0
.end method

.method d()V
    .locals 2

    .prologue
    .line 154
    const v0, 0x7f020126

    const v1, 0x7f110087

    invoke-virtual {p0, v0, v1}, Lapz;->a(II)V

    .line 155
    return-void
.end method

.method f()V
    .locals 2

    .prologue
    .line 178
    invoke-super {p0}, Lapv;->f()V

    .line 179
    iget-object v0, p0, Lapz;->v:Lcom/twitter/media/ui/image/UserImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setVisibility(I)V

    .line 180
    return-void
.end method

.method synthetic h()Lcom/twitter/app/dm/widget/MessageBylineView;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lapz;->n()Lcom/twitter/app/dm/widget/ReceivedMessageBylineView;

    move-result-object v0

    return-object v0
.end method

.method i()Ljava/lang/String;
    .locals 5

    .prologue
    .line 160
    invoke-super {p0}, Lapv;->i()Ljava/lang/String;

    move-result-object v0

    .line 161
    iget-boolean v1, p0, Lapz;->q:Z

    if-eqz v1, :cond_0

    .line 163
    iget-object v1, p0, Lapz;->h:Landroid/content/res/Resources;

    const v2, 0x7f0a02d1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lapz;->b:Lcom/twitter/model/dms/m;

    iget-object v4, v4, Lcom/twitter/model/dms/m;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 166
    :cond_0
    return-object v0
.end method

.method n()Lcom/twitter/app/dm/widget/ReceivedMessageBylineView;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lapz;->w:Lcom/twitter/app/dm/widget/ReceivedMessageBylineView;

    return-object v0
.end method
