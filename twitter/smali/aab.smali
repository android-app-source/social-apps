.class public Laab;
.super Lcom/twitter/android/moments/ui/fullscreen/bg;
.source "Twttr"


# instance fields
.field private final a:Lzn;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/fullscreen/bh;Lzn;Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p4}, Lcom/twitter/android/moments/ui/fullscreen/bg;-><init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/fullscreen/bh;Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;)V

    .line 31
    iput-object p3, p0, Laab;->a:Lzn;

    .line 32
    return-void
.end method

.method public static a(Landroid/support/v4/app/FragmentActivity;Lzn;)Laab;
    .locals 4

    .prologue
    .line 21
    new-instance v0, Laab;

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/bh;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/bh;-><init>(Landroid/content/res/Resources;)V

    new-instance v2, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;

    .line 23
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;)V

    invoke-direct {v0, p0, v1, p1, v2}, Laab;-><init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/fullscreen/bh;Lzn;Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;)V

    .line 21
    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/Moment;)V
    .locals 2

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/bg;->a(Lcom/twitter/model/moments/Moment;)V

    .line 37
    iget-object v0, p0, Laab;->a:Lzn;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lzn;->a(I)V

    .line 38
    return-void
.end method

.method public b(Lcom/twitter/model/moments/Moment;)V
    .locals 2

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/bg;->b(Lcom/twitter/model/moments/Moment;)V

    .line 43
    iget-object v0, p0, Laab;->a:Lzn;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lzn;->a(I)V

    .line 44
    return-void
.end method
