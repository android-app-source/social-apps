.class Lafj$3;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lafj;->b(Lcom/twitter/library/client/Session;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lafj;


# direct methods
.method constructor <init>(Lafj;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lafj$3;->a:Lafj;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 103
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lafj$3;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 6

    .prologue
    .line 106
    iget-object v0, p0, Lafj$3;->a:Lafj;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lafj;->a(Lafj;Ljava/lang/String;)Ljava/lang/String;

    .line 107
    check-cast p1, Lbeu;

    .line 108
    invoke-virtual {p1}, Lbeu;->b()J

    move-result-wide v0

    iget-object v2, p0, Lafj$3;->a:Lafj;

    invoke-static {v2}, Lafj;->a(Lafj;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    invoke-virtual {p1}, Lbeu;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 112
    iget-object v2, p0, Lafj$3;->a:Lafj;

    invoke-static {v2}, Lafj;->b(Lafj;)Lcom/twitter/model/util/FriendshipCache;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/twitter/model/util/FriendshipCache;->h(J)V

    goto :goto_1

    .line 115
    :cond_2
    iget-object v0, p0, Lafj$3;->a:Lafj;

    .line 116
    invoke-static {v0}, Lafj;->c(Lafj;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lafj$3;->a:Lafj;

    invoke-static {v1}, Lafj;->a(Lafj;)Lcom/twitter/library/client/Session;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lbfd;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Z)Lber;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_0

    .line 118
    iget-object v1, p0, Lafj$3;->a:Lafj;

    invoke-static {v1}, Lafj;->d(Lafj;)Lcom/twitter/library/client/p;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    goto :goto_0
.end method
