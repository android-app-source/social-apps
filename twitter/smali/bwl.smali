.class public Lbwl;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/provider/t;

.field private final b:Lcom/twitter/library/provider/s;

.field private final c:Lcpb;

.field private final d:I

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/timeline/y;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/y;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/library/provider/t;Lcom/twitter/library/provider/s;Lcpb;ILjava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/provider/t;",
            "Lcom/twitter/library/provider/s;",
            "Lcpb;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/timeline/y;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbwl;->f:Ljava/util/List;

    .line 101
    iput-object p1, p0, Lbwl;->a:Lcom/twitter/library/provider/t;

    .line 102
    iput-object p2, p0, Lbwl;->b:Lcom/twitter/library/provider/s;

    .line 103
    iput-object p3, p0, Lbwl;->c:Lcpb;

    .line 104
    iput p4, p0, Lbwl;->d:I

    .line 105
    invoke-static {p5}, Lcom/twitter/util/collection/ImmutableMap;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lbwl;->e:Ljava/util/Map;

    .line 106
    return-void
.end method

.method private static a(Lcom/twitter/library/provider/t;Ljava/util/List;Ljava/util/Set;Landroid/util/LongSparseArray;)Landroid/util/Pair;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/provider/t;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/y;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/y;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 345
    new-instance v12, Ljava/util/LinkedList;

    invoke-direct {v12}, Ljava/util/LinkedList;-><init>()V

    .line 346
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v13

    .line 349
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move v11, v4

    :goto_0
    if-ltz v11, :cond_6

    .line 350
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/timeline/y;

    .line 352
    instance-of v5, v4, Lcom/twitter/model/timeline/w;

    if-eqz v5, :cond_3

    .line 353
    invoke-static {v4}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/model/timeline/w;

    .line 354
    const/4 v8, 0x1

    .line 357
    const/4 v7, 0x0

    .line 360
    invoke-virtual {v5}, Lcom/twitter/model/timeline/w;->a()Ljava/util/List;

    move-result-object v14

    .line 363
    invoke-virtual {v5}, Lcom/twitter/model/timeline/w;->d()Lcom/twitter/model/core/ac;

    move-result-object v15

    .line 368
    const/4 v6, 0x0

    move v9, v7

    move v10, v8

    move v8, v6

    :goto_1
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v6

    if-ge v8, v6, :cond_2

    .line 369
    invoke-interface {v14, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/model/core/ac;

    .line 370
    invoke-virtual {v6}, Lcom/twitter/model/core/ac;->a()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    .line 372
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-object/from16 v0, p3

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 373
    if-eqz v7, :cond_7

    .line 376
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v7}, Lcom/twitter/model/timeline/z$a;->c(I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 377
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 381
    const/4 v10, 0x0

    move v7, v9

    move v9, v10

    .line 391
    :goto_2
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    if-ne v6, v15, :cond_1

    const/16 v6, 0x8

    .line 392
    :goto_3
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 391
    move-object/from16 v0, p3

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2, v6}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 368
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    move v10, v9

    move v9, v7

    goto :goto_1

    .line 383
    :cond_0
    add-int/lit8 v9, v8, 0x1

    move v7, v9

    move v9, v10

    goto :goto_2

    .line 391
    :cond_1
    const/4 v6, 0x4

    goto :goto_3

    .line 395
    :cond_2
    if-eqz v10, :cond_3

    .line 396
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v6

    if-lt v9, v6, :cond_5

    .line 398
    const/4 v4, 0x0

    .line 409
    :cond_3
    :goto_4
    if-eqz v4, :cond_4

    .line 410
    invoke-virtual {v12, v4}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 349
    :cond_4
    add-int/lit8 v4, v11, -0x1

    move v11, v4

    goto/16 :goto_0

    .line 399
    :cond_5
    if-lez v9, :cond_3

    .line 400
    move-object/from16 v0, p0

    invoke-static {v0, v5, v9}, Lbwl;->a(Lcom/twitter/library/provider/t;Lcom/twitter/model/timeline/w;I)Lcom/twitter/model/timeline/w;

    move-result-object v5

    .line 404
    invoke-static {v5}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/timeline/w;

    .line 405
    invoke-virtual {v4}, Lcom/twitter/model/timeline/w;->a()Ljava/util/List;

    move-result-object v4

    const/4 v6, 0x0

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v13, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object v4, v5

    goto :goto_4

    .line 413
    :cond_6
    new-instance v4, Landroid/util/Pair;

    invoke-direct {v4, v12, v13}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v4

    :cond_7
    move v7, v9

    move v9, v10

    goto :goto_2
.end method

.method static a(Lcom/twitter/library/provider/t;Lcom/twitter/model/timeline/w;I)Lcom/twitter/model/timeline/w;
    .locals 5
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 438
    invoke-virtual {p1}, Lcom/twitter/model/timeline/w;->a()Ljava/util/List;

    move-result-object v1

    .line 439
    const/4 v0, 0x1

    if-lt p2, v0, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lt p2, v0, :cond_1

    .line 441
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string/jumbo v1, "Attempted to collapse an invalid tweet index from conversation"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 446
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 445
    invoke-interface {v1, p2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    .line 447
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    .line 448
    add-int/lit8 v3, p2, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/ac;

    iget-object v1, v1, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    .line 449
    invoke-static {p0, v0, v1}, Lbwl;->a(Lcom/twitter/library/provider/t;Lcom/twitter/model/core/ac;Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;

    move-result-object v3

    .line 450
    if-eqz v3, :cond_3

    .line 451
    iget-object v1, v0, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    if-nez v1, :cond_2

    .line 452
    new-instance v1, Lcom/twitter/model/search/e$a;

    invoke-direct {v1}, Lcom/twitter/model/search/e$a;-><init>()V

    invoke-virtual {v1}, Lcom/twitter/model/search/e$a;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/search/e;

    iput-object v1, v0, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    .line 454
    :cond_2
    iget-object v1, v0, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    new-instance v0, Lcom/twitter/model/core/TwitterSocialProof$a;

    invoke-direct {v0}, Lcom/twitter/model/core/TwitterSocialProof$a;-><init>()V

    const/16 v4, 0x18

    .line 455
    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterSocialProof$a;->a(I)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    .line 456
    invoke-virtual {v0, v3}, Lcom/twitter/model/core/TwitterSocialProof$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    .line 457
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterSocialProof$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterSocialProof;

    iput-object v0, v1, Lcom/twitter/model/search/e;->e:Lcom/twitter/model/core/TwitterSocialProof;

    .line 459
    :cond_3
    new-instance v0, Lcom/twitter/model/timeline/w$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/w$a;-><init>()V

    new-instance v1, Lcom/twitter/model/timeline/f;

    const/4 v3, 0x0

    invoke-direct {v1, v3, v2}, Lcom/twitter/model/timeline/f;-><init>(Lcom/twitter/model/timeline/f$a;Ljava/util/List;)V

    .line 460
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/w$a;->a(Lcom/twitter/model/timeline/f;)Lcom/twitter/model/timeline/w$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/timeline/w;->c:Ljava/lang/String;

    .line 461
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/w$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/w$a;

    iget-wide v2, p1, Lcom/twitter/model/timeline/w;->g:J

    .line 462
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/timeline/w$a;->a(J)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/w$a;

    iget-wide v2, p1, Lcom/twitter/model/timeline/w;->q:J

    .line 463
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/timeline/w$a;->b(J)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/w$a;

    .line 464
    invoke-virtual {v0}, Lcom/twitter/model/timeline/w$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/w;

    .line 459
    return-object v0
.end method

.method static a(Lcom/twitter/library/provider/t;Lcom/twitter/model/core/ac;Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;
    .locals 6
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 475
    iget-wide v2, p1, Lcom/twitter/model/core/ac;->i:J

    .line 476
    const/4 v0, 0x0

    .line 477
    iget-wide v4, p2, Lcom/twitter/model/core/TwitterUser;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 478
    iget-object v0, p2, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    .line 489
    :cond_0
    :goto_0
    return-object v0

    .line 480
    :cond_1
    invoke-virtual {p0, v2, v3}, Lcom/twitter/library/provider/t;->b(J)Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    .line 481
    if-eqz v1, :cond_2

    .line 482
    iget-object v0, v1, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    goto :goto_0

    .line 483
    :cond_2
    iget-object v1, p1, Lcom/twitter/model/core/ac;->k:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 486
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/core/ac;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static a(Landroid/database/sqlite/SQLiteDatabase;JILjava/util/List;)Ljava/util/Set;
    .locals 17
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "JI",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/w;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 212
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 213
    invoke-static {}, Lcom/twitter/util/collection/o;->f()Ljava/util/Set;

    move-result-object v2

    .line 309
    :goto_0
    return-object v2

    .line 216
    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v4

    .line 217
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/timeline/w;

    .line 218
    invoke-virtual {v2}, Lcom/twitter/model/timeline/w;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/model/core/ac;

    .line 219
    iget-wide v8, v3, Lcom/twitter/model/core/ac;->a:J

    .line 220
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 221
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    iget-wide v12, v2, Lcom/twitter/model/timeline/w;->g:J

    cmp-long v3, v10, v12

    if-gez v3, :cond_2

    .line 222
    :cond_3
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-wide v8, v2, Lcom/twitter/model/timeline/w;->g:J

    .line 223
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 222
    invoke-interface {v4, v3, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 229
    :cond_4
    invoke-static {v4}, Lcom/twitter/util/collection/ImmutableMap;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v12

    .line 237
    const-string/jumbo v3, "timeline"

    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v5, "data_id"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v5, "entity_id"

    aput-object v5, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v5, "sort_index"

    aput-object v5, v4, v2

    const-string/jumbo v5, "owner_id=? AND type=? AND data_type=1 AND flags&8=8"

    const/4 v2, 0x2

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 240
    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v2, 0x1

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p0

    .line 237
    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 242
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v13

    .line 243
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v4

    .line 245
    :cond_5
    :goto_2
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 246
    const/4 v2, 0x0

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 247
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v12, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 248
    const/4 v2, 0x1

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 249
    const/4 v2, 0x2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 253
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v12, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v2, v10, v8

    if-ltz v2, :cond_5

    .line 254
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v13, v2}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 255
    invoke-virtual {v4, v5}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 260
    :catchall_0
    move-exception v2

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_6
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 262
    invoke-virtual {v4}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 263
    const-string/jumbo v4, "timeline"

    const-string/jumbo v5, "owner_id=? AND type=? AND entity_id=?"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    .line 266
    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    .line 267
    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    .line 268
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v7

    .line 263
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_3

    .line 274
    :cond_7
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v14

    .line 275
    invoke-interface {v12}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_8
    :goto_4
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Ljava/util/Map$Entry;

    .line 276
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    move-object v11, v2

    check-cast v11, Ljava/lang/Long;

    .line 277
    invoke-interface {v12, v11}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 278
    const-string/jumbo v3, "timeline"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v5, "sort_index"

    aput-object v5, v4, v2

    const-string/jumbo v5, "data_type=1 AND data_id=? AND flags&1=1 AND owner_id=? AND type=?"

    const/4 v2, 0x3

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 283
    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v2, 0x1

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v2, 0x2

    .line 284
    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p0

    .line 278
    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 286
    :cond_9
    :goto_5
    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 287
    const/4 v2, 0x0

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 288
    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 292
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v2, v6, v4

    if-ltz v2, :cond_9

    .line 293
    invoke-virtual {v13, v11}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 294
    invoke-virtual {v14, v11}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_5

    .line 298
    :catchall_1
    move-exception v2

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_a
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_4

    .line 302
    :cond_b
    invoke-virtual {v14}, Lcom/twitter/util/collection/o;->g()Z

    move-result v2

    if-nez v2, :cond_c

    .line 303
    const-string/jumbo v3, "timeline"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "data_type=1 AND data_id=? AND flags&1=1 AND owner_id="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, " AND "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "type"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v4, 0x3d

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 307
    invoke-virtual {v14}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-static {v2}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v2

    .line 304
    move-object/from16 v0, p0

    invoke-static {v0, v3, v4, v2}, Lcom/twitter/library/provider/t;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[J)I

    .line 309
    :cond_c
    invoke-virtual {v13}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    goto/16 :goto_0
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lbwl;->f:Ljava/util/List;

    return-object v0
.end method

.method public b()Lbwl;
    .locals 11

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 118
    iget-object v0, p0, Lbwl;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v8

    .line 119
    new-instance v0, Lbwl$1;

    invoke-direct {v0, p0}, Lbwl$1;-><init>(Lbwl;)V

    .line 121
    invoke-static {v8, v0}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Lcpv;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lbwl$2;

    invoke-direct {v1, p0}, Lbwl$2;-><init>(Lbwl;)V

    .line 120
    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;Lcpp;)Ljava/util/List;

    move-result-object v9

    .line 136
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 137
    iget-object v0, p0, Lbwl;->c:Lcpb;

    const-string/jumbo v1, "convoEntityList.size"

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 143
    new-instance v10, Landroid/util/LongSparseArray;

    invoke-direct {v10}, Landroid/util/LongSparseArray;-><init>()V

    .line 144
    const-string/jumbo v0, "owner_id=? AND type=? AND data_type=1 AND (flags&4=4 OR flags&8=8)"

    .line 150
    new-array v4, v4, [Ljava/lang/String;

    iget-object v0, p0, Lbwl;->b:Lcom/twitter/library/provider/s;

    iget-wide v0, v0, Lcom/twitter/library/provider/s;->d:J

    .line 151
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v3

    iget-object v0, p0, Lbwl;->b:Lcom/twitter/library/provider/s;

    iget v0, v0, Lcom/twitter/library/provider/s;->e:I

    .line 152
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 154
    iget-object v0, p0, Lbwl;->a:Lcom/twitter/library/provider/t;

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 155
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 157
    :try_start_0
    const-string/jumbo v1, "timeline"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v5, "data_id"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v5, "flags"

    aput-object v5, v2, v3

    const-string/jumbo v3, "owner_id=? AND type=? AND data_type=1 AND (flags&4=4 OR flags&8=8)"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 161
    :goto_0
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 162
    const/4 v1, 0x0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v1, 0x1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v4, v5, v1}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 165
    :catchall_0
    move-exception v1

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 188
    :catchall_1
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    .line 165
    :cond_0
    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 167
    iget-object v1, p0, Lbwl;->c:Lcpb;

    const-string/jumbo v2, "deleteStaleTimelineEntities"

    const-string/jumbo v3, "BEGIN"

    invoke-virtual {v1, v2, v3}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 168
    iget-object v1, p0, Lbwl;->b:Lcom/twitter/library/provider/s;

    iget-wide v2, v1, Lcom/twitter/library/provider/s;->d:J

    iget-object v1, p0, Lbwl;->b:Lcom/twitter/library/provider/s;

    iget v1, v1, Lcom/twitter/library/provider/s;->e:I

    invoke-static {v0, v2, v3, v1, v9}, Lbwl;->a(Landroid/database/sqlite/SQLiteDatabase;JILjava/util/List;)Ljava/util/Set;

    move-result-object v1

    .line 171
    iget-object v2, p0, Lbwl;->c:Lcpb;

    const-string/jumbo v3, "deleteStaleTimelineEntities"

    const-string/jumbo v4, "END"

    invoke-virtual {v2, v3, v4}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 172
    iget-object v2, p0, Lbwl;->a:Lcom/twitter/library/provider/t;

    .line 173
    invoke-static {v2, v8, v1, v10}, Lbwl;->a(Lcom/twitter/library/provider/t;Ljava/util/List;Ljava/util/Set;Landroid/util/LongSparseArray;)Landroid/util/Pair;

    move-result-object v2

    .line 175
    iget-object v1, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/util/List;

    iput-object v1, p0, Lbwl;->f:Ljava/util/List;

    .line 176
    iget-object v3, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/util/Set;

    .line 177
    iget-object v1, p0, Lbwl;->c:Lcpb;

    const-string/jumbo v2, "statusesToUpdate.size"

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 178
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 180
    iget-object v1, p0, Lbwl;->c:Lcpb;

    const-string/jumbo v2, "mergeTimelineStatuses"

    const-string/jumbo v4, "BEGIN"

    invoke-virtual {v1, v2, v4}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 181
    iget-object v2, p0, Lbwl;->a:Lcom/twitter/library/provider/t;

    iget-object v1, p0, Lbwl;->b:Lcom/twitter/library/provider/s;

    iget-wide v4, v1, Lcom/twitter/library/provider/s;->d:J

    iget v6, p0, Lbwl;->d:I

    const-wide/16 v7, -0x1

    iget-object v1, p0, Lbwl;->b:Lcom/twitter/library/provider/s;

    iget-boolean v9, v1, Lcom/twitter/library/provider/s;->k:Z

    .line 182
    invoke-virtual/range {v2 .. v9}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;JIJZ)I

    .line 184
    iget-object v1, p0, Lbwl;->c:Lcpb;

    const-string/jumbo v2, "mergeTimelineStatuses"

    const-string/jumbo v3, "END"

    invoke-virtual {v1, v2, v3}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 186
    :cond_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 188
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 193
    :goto_1
    return-object p0

    .line 191
    :cond_2
    iput-object v8, p0, Lbwl;->f:Ljava/util/List;

    goto :goto_1
.end method
