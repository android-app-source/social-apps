.class public final Lcjo$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcjo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f11019d

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f11019e

.field public static final abc_btn_colored_borderless_text_material:I = 0x7f11019f

.field public static final abc_color_highlight_material:I = 0x7f1101a0

.field public static final abc_input_method_navigation_guard:I = 0x7f110024

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f1101a1

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f1101a2

.field public static final abc_primary_text_material_dark:I = 0x7f1101a3

.field public static final abc_primary_text_material_light:I = 0x7f1101a4

.field public static final abc_search_url_text:I = 0x7f1101a5

.field public static final abc_search_url_text_normal:I = 0x7f110025

.field public static final abc_search_url_text_pressed:I = 0x7f110026

.field public static final abc_search_url_text_selected:I = 0x7f110027

.field public static final abc_secondary_text_material_dark:I = 0x7f1101a6

.field public static final abc_secondary_text_material_light:I = 0x7f1101a7

.field public static final abc_tint_btn_checkable:I = 0x7f1101a8

.field public static final abc_tint_default:I = 0x7f1101a9

.field public static final abc_tint_edittext:I = 0x7f1101aa

.field public static final abc_tint_seek_thumb:I = 0x7f1101ab

.field public static final abc_tint_spinner:I = 0x7f1101ac

.field public static final abc_tint_switch_thumb:I = 0x7f1101ad

.field public static final abc_tint_switch_track:I = 0x7f1101ae

.field public static final accent_material_dark:I = 0x7f110028

.field public static final accent_material_light:I = 0x7f110029

.field public static final app_background:I = 0x7f110000

.field public static final background_floating_material_dark:I = 0x7f11002c

.field public static final background_floating_material_light:I = 0x7f11002d

.field public static final background_material_dark:I = 0x7f11002e

.field public static final background_material_light:I = 0x7f11002f

.field public static final black:I = 0x7f110030

.field public static final black_opacity_10:I = 0x7f110031

.field public static final black_opacity_30:I = 0x7f110032

.field public static final black_opacity_5:I = 0x7f110033

.field public static final black_opacity_50:I = 0x7f110034

.field public static final black_opacity_75:I = 0x7f110035

.field public static final border_color:I = 0x7f110001

.field public static final bright_foreground_disabled_material_dark:I = 0x7f110036

.field public static final bright_foreground_disabled_material_light:I = 0x7f110037

.field public static final bright_foreground_inverse_material_dark:I = 0x7f110038

.field public static final bright_foreground_inverse_material_light:I = 0x7f110039

.field public static final bright_foreground_material_dark:I = 0x7f11003a

.field public static final bright_foreground_material_light:I = 0x7f11003b

.field public static final button_material_dark:I = 0x7f110047

.field public static final button_material_light:I = 0x7f110048

.field public static final button_stroke_disabled:I = 0x7f110002

.field public static final clear:I = 0x7f11004e

.field public static final counter_state_list:I = 0x7f1101bd

.field public static final dark_transparent_black:I = 0x7f110066

.field public static final dark_transparent_gray:I = 0x7f110067

.field public static final deep_blue:I = 0x7f110068

.field public static final deep_gray:I = 0x7f110003

.field public static final deep_gray_30:I = 0x7f110069

.field public static final deep_gray_30_night:I = 0x7f11006a

.field public static final deep_gray_50_night:I = 0x7f11006b

.field public static final deep_gray_night:I = 0x7f11006c

.field public static final deep_green:I = 0x7f11006d

.field public static final deep_orange:I = 0x7f11006e

.field public static final deep_purple:I = 0x7f11006f

.field public static final deep_red:I = 0x7f110070

.field public static final deep_transparent_black:I = 0x7f110071

.field public static final deep_transparent_gray:I = 0x7f110072

.field public static final deep_yellow:I = 0x7f110074

.field public static final design_error:I = 0x7f1101be

.field public static final design_fab_shadow_end_color:I = 0x7f110077

.field public static final design_fab_shadow_mid_color:I = 0x7f110078

.field public static final design_fab_shadow_start_color:I = 0x7f110079

.field public static final design_fab_stroke_end_inner_color:I = 0x7f11007a

.field public static final design_fab_stroke_end_outer_color:I = 0x7f11007b

.field public static final design_fab_stroke_top_inner_color:I = 0x7f11007c

.field public static final design_fab_stroke_top_outer_color:I = 0x7f11007d

.field public static final design_snackbar_background_color:I = 0x7f11007e

.field public static final design_textinput_error_color_dark:I = 0x7f11007f

.field public static final design_textinput_error_color_light:I = 0x7f110080

.field public static final design_tint_password_toggle:I = 0x7f1101bf

.field public static final dim_foreground_disabled_material_dark:I = 0x7f110081

.field public static final dim_foreground_disabled_material_light:I = 0x7f110082

.field public static final dim_foreground_material_dark:I = 0x7f110083

.field public static final dim_foreground_material_light:I = 0x7f110084

.field public static final edittext_underline_color_state_list:I = 0x7f1101c2

.field public static final faded_blue:I = 0x7f11008c

.field public static final faded_gray:I = 0x7f11008d

.field public static final faded_green:I = 0x7f11008e

.field public static final faded_orange:I = 0x7f11008f

.field public static final faded_purple:I = 0x7f110090

.field public static final faded_red:I = 0x7f110091

.field public static final faded_yellow:I = 0x7f110092

.field public static final faint_blue:I = 0x7f110093

.field public static final faint_gray:I = 0x7f110094

.field public static final faint_gray_night:I = 0x7f110095

.field public static final faint_night_mode_white:I = 0x7f110096

.field public static final faint_transparent_black:I = 0x7f110097

.field public static final faint_transparent_blue:I = 0x7f110098

.field public static final faint_transparent_white:I = 0x7f110099

.field public static final focused_bg:I = 0x7f110004

.field public static final footer_logo:I = 0x7f110005

.field public static final foreground_material_dark:I = 0x7f11009a

.field public static final foreground_material_light:I = 0x7f11009b

.field public static final global_ripple_selector_color:I = 0x7f110006

.field public static final gray_opacity_30:I = 0x7f1100a6

.field public static final highlighted_text_material_dark:I = 0x7f1100a7

.field public static final highlighted_text_material_light:I = 0x7f1100a8

.field public static final hint_foreground_material_dark:I = 0x7f1100b3

.field public static final hint_foreground_material_light:I = 0x7f1100b4

.field public static final hint_state_list:I = 0x7f1101c5

.field public static final inline_action:I = 0x7f110007

.field public static final inline_action_analytics_on:I = 0x7f1100b6

.field public static final inline_action_disabled:I = 0x7f110008

.field public static final inline_action_like:I = 0x7f1100b7

.field public static final inline_action_like_pressed:I = 0x7f1100b8

.field public static final inline_action_pressed:I = 0x7f110009

.field public static final inline_action_retweet:I = 0x7f1100b9

.field public static final inline_action_retweet_pressed:I = 0x7f1100ba

.field public static final label_state_list:I = 0x7f1101c7

.field public static final light_blue:I = 0x7f1100bc

.field public static final light_gray:I = 0x7f1100bd

.field public static final light_gray_30:I = 0x7f1100be

.field public static final light_gray_night:I = 0x7f1100bf

.field public static final light_green:I = 0x7f1100c0

.field public static final light_orange:I = 0x7f1100c1

.field public static final light_purple:I = 0x7f1100c2

.field public static final light_red:I = 0x7f1100c3

.field public static final light_transparent_black:I = 0x7f1100c4

.field public static final light_yellow:I = 0x7f1100c5

.field public static final lighter_gray:I = 0x7f1100c6

.field public static final lighter_transparent_black:I = 0x7f1100c7

.field public static final list_bg:I = 0x7f11000a

.field public static final material_blue_grey_800:I = 0x7f1100cc

.field public static final material_blue_grey_900:I = 0x7f1100cd

.field public static final material_blue_grey_950:I = 0x7f1100ce

.field public static final material_deep_teal_200:I = 0x7f1100cf

.field public static final material_deep_teal_500:I = 0x7f1100d0

.field public static final material_grey_100:I = 0x7f1100d1

.field public static final material_grey_300:I = 0x7f1100d2

.field public static final material_grey_50:I = 0x7f1100d3

.field public static final material_grey_600:I = 0x7f1100d4

.field public static final material_grey_800:I = 0x7f1100d5

.field public static final material_grey_850:I = 0x7f1100d6

.field public static final material_grey_900:I = 0x7f1100d7

.field public static final medium_gray:I = 0x7f1100d9

.field public static final medium_gray_30:I = 0x7f1100da

.field public static final medium_gray_50:I = 0x7f1100db

.field public static final medium_gray_50_night:I = 0x7f1100dc

.field public static final medium_gray_night:I = 0x7f1100dd

.field public static final medium_green:I = 0x7f1100de

.field public static final medium_green_50:I = 0x7f1100df

.field public static final medium_green_fill_pressed:I = 0x7f1100e0

.field public static final medium_orange:I = 0x7f1100e1

.field public static final medium_orange_fill_pressed:I = 0x7f1100e2

.field public static final medium_purple:I = 0x7f1100e3

.field public static final medium_red:I = 0x7f1100e4

.field public static final medium_red_30:I = 0x7f1100e5

.field public static final medium_red_50:I = 0x7f1100e6

.field public static final medium_red_fill_pressed:I = 0x7f1100e7

.field public static final medium_yellow:I = 0x7f1100e8

.field public static final medium_yellow_30:I = 0x7f1100e9

.field public static final message_state_list:I = 0x7f1101c8

.field public static final navigation_pills_dark_default_color:I = 0x7f110107

.field public static final navigation_pills_dark_pressed_color:I = 0x7f110108

.field public static final navigation_pills_light_default_color:I = 0x7f110109

.field public static final navigation_pills_light_pressed_color:I = 0x7f11010a

.field public static final night_mode_pressed_fill:I = 0x7f11010b

.field public static final placeholder_bg:I = 0x7f11000b

.field public static final pressed:I = 0x7f11000c

.field public static final primary_dark_material_dark:I = 0x7f110117

.field public static final primary_dark_material_light:I = 0x7f110118

.field public static final primary_material_dark:I = 0x7f110119

.field public static final primary_material_light:I = 0x7f11011a

.field public static final primary_text_default_material_dark:I = 0x7f11011c

.field public static final primary_text_default_material_light:I = 0x7f11011d

.field public static final primary_text_disabled_material_dark:I = 0x7f11011e

.field public static final primary_text_disabled_material_light:I = 0x7f11011f

.field public static final ripple_material_dark:I = 0x7f110174

.field public static final ripple_material_light:I = 0x7f110175

.field public static final secondary_text_default_material_dark:I = 0x7f110178

.field public static final secondary_text_default_material_light:I = 0x7f110179

.field public static final secondary_text_disabled_material_dark:I = 0x7f11017a

.field public static final secondary_text_disabled_material_light:I = 0x7f11017b

.field public static final section_divider_color:I = 0x7f11000d

.field public static final selection_background:I = 0x7f11017d

.field public static final selection_highlight_background:I = 0x7f11017e

.field public static final selection_highlight_text_color:I = 0x7f11017f

.field public static final selection_text_color:I = 0x7f110180

.field public static final soft_white:I = 0x7f110182

.field public static final speech_bubble:I = 0x7f11000e

.field public static final status_bar_color:I = 0x7f11000f

.field public static final strong_white:I = 0x7f110184

.field public static final subtext:I = 0x7f110010

.field public static final switch_thumb_disabled_material_dark:I = 0x7f110185

.field public static final switch_thumb_disabled_material_light:I = 0x7f110186

.field public static final switch_thumb_material_dark:I = 0x7f1101d3

.field public static final switch_thumb_material_light:I = 0x7f1101d4

.field public static final switch_thumb_normal_material_dark:I = 0x7f110187

.field public static final switch_thumb_normal_material_light:I = 0x7f110188

.field public static final tertiary:I = 0x7f110011

.field public static final text:I = 0x7f110012

.field public static final text_black:I = 0x7f11018a

.field public static final text_blue:I = 0x7f110013

.field public static final text_disabled:I = 0x7f110014

.field public static final text_state_list:I = 0x7f1101d6

.field public static final toolbar_bg_color:I = 0x7f110015

.field public static final translate_logo:I = 0x7f110016

.field public static final transparent_black:I = 0x7f11018c

.field public static final transparent_black_85:I = 0x7f11018d

.field public static final transparent_white:I = 0x7f11018e

.field public static final transparent_white_10:I = 0x7f11018f

.field public static final twitter_blue:I = 0x7f110190

.field public static final twitter_blue_30:I = 0x7f110191

.field public static final twitter_blue_50:I = 0x7f110192

.field public static final twitter_blue_fill_pressed:I = 0x7f110193

.field public static final unread:I = 0x7f110017

.field public static final verified:I = 0x7f110018

.field public static final verified_timeline:I = 0x7f110019

.field public static final white:I = 0x7f110195

.field public static final white_opacity_10:I = 0x7f110196

.field public static final white_opacity_15:I = 0x7f110197

.field public static final white_opacity_20:I = 0x7f110198

.field public static final white_opacity_30:I = 0x7f110199

.field public static final white_opacity_40:I = 0x7f11019a

.field public static final white_opacity_50:I = 0x7f11019b

.field public static final white_opacity_75:I = 0x7f11019c
