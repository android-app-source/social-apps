.class public Lajh;
.super Lajf;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lajh$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lajf",
        "<",
        "Lcom/twitter/model/core/h;",
        ">;"
    }
.end annotation


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lajh$a;)V
    .locals 4

    .prologue
    .line 25
    invoke-static {p1}, Lajh$a;->a(Lajh$a;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {p1}, Lajh$a;->b(Lajh$a;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-static {p1}, Lajh$a;->c(Lajh$a;)Lcom/twitter/model/core/h;

    move-result-object v2

    invoke-static {p1}, Lajh$a;->d(Lajh$a;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, Lajf;-><init>(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/d;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 26
    invoke-static {p1}, Lajh$a;->e(Lajh$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lajh;->d:Ljava/lang/String;

    .line 27
    invoke-static {p1}, Lajh$a;->f(Lajh$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lajh;->e:Ljava/lang/String;

    .line 28
    return-void
.end method

.method synthetic constructor <init>(Lajh$a;Lajh$1;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lajh;-><init>(Lajh$a;)V

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/twitter/model/core/h;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 36
    invoke-static {p1, p2}, Lcom/twitter/android/t;->a(Landroid/content/Context;Lcom/twitter/model/core/h;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a(Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lajh;->d:Ljava/lang/String;

    iget-object v1, p0, Lajh;->e:Ljava/lang/String;

    invoke-static {p1, p2, v0, v1}, Lmg;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method b()V
    .locals 3

    .prologue
    .line 32
    iget-object v1, p0, Lajh;->a:Landroid/content/Context;

    iget-object v2, p0, Lajh;->a:Landroid/content/Context;

    iget-object v0, p0, Lajh;->c:Lcom/twitter/model/core/d;

    check-cast v0, Lcom/twitter/model/core/h;

    invoke-direct {p0, v2, v0}, Lajh;->a(Landroid/content/Context;Lcom/twitter/model/core/h;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 33
    return-void
.end method

.method c()Lbsq;
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lajh;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->e:Lcom/twitter/library/api/PromotedEvent;

    iget-object v1, p0, Lajh;->b:Lcom/twitter/model/core/Tweet;

    .line 43
    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v1

    invoke-static {v0, v1}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v0

    invoke-virtual {v0}, Lbsq$a;->a()Lbsq;

    move-result-object v0

    .line 42
    :goto_0
    return-object v0

    .line 43
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lajh;->c:Lcom/twitter/model/core/d;

    check-cast v0, Lcom/twitter/model/core/h;

    iget-object v0, v0, Lcom/twitter/model/core/h;->c:Ljava/lang/String;

    return-object v0
.end method

.method e()Lcom/twitter/analytics/model/ScribeItem;
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return-object v0
.end method
