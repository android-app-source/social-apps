.class public Lbsq$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbsq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Lcom/twitter/library/api/PromotedEvent;

.field private b:J

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:J


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method synthetic constructor <init>(Lbsq$1;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lbsq$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lbsq$a;)Lcom/twitter/library/api/PromotedEvent;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lbsq$a;->a:Lcom/twitter/library/api/PromotedEvent;

    return-object v0
.end method

.method static synthetic b(Lbsq$a;)J
    .locals 2

    .prologue
    .line 11
    iget-wide v0, p0, Lbsq$a;->b:J

    return-wide v0
.end method

.method static synthetic c(Lbsq$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lbsq$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lbsq$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lbsq$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lbsq$a;)Z
    .locals 1

    .prologue
    .line 11
    iget-boolean v0, p0, Lbsq$a;->e:Z

    return v0
.end method

.method static synthetic f(Lbsq$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lbsq$a;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lbsq$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lbsq$a;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lbsq$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lbsq$a;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lbsq$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lbsq$a;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lbsq$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lbsq$a;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lbsq$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lbsq$a;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lbsq$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lbsq$a;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic m(Lbsq$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lbsq$a;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic n(Lbsq$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lbsq$a;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Lbsq$a;)J
    .locals 2

    .prologue
    .line 11
    iget-wide v0, p0, Lbsq$a;->o:J

    return-wide v0
.end method


# virtual methods
.method public a(J)Lbsq$a;
    .locals 1

    .prologue
    .line 127
    iput-wide p1, p0, Lbsq$a;->o:J

    .line 128
    return-object p0
.end method

.method public a(Lcgi;)Lbsq$a;
    .locals 2

    .prologue
    .line 37
    iget-object v0, p1, Lcgi;->c:Ljava/lang/String;

    iput-object v0, p0, Lbsq$a;->d:Ljava/lang/String;

    .line 38
    invoke-virtual {p1}, Lcgi;->c()Z

    move-result v0

    iput-boolean v0, p0, Lbsq$a;->e:Z

    .line 39
    iget-wide v0, p1, Lcgi;->e:J

    iput-wide v0, p0, Lbsq$a;->b:J

    .line 40
    return-object p0
.end method

.method public a(Lcom/twitter/library/api/PromotedEvent;)Lbsq$a;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lbsq$a;->a:Lcom/twitter/library/api/PromotedEvent;

    .line 33
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lbsq$a;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lbsq$a;->c:Ljava/lang/String;

    .line 48
    return-object p0
.end method

.method public a()Lbsq;
    .locals 2

    .prologue
    .line 135
    new-instance v0, Lbsq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lbsq;-><init>(Lbsq$a;Lbsq$1;)V

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lbsq$a;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lbsq$a;->f:Ljava/lang/String;

    .line 56
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lbsq$a;
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lbsq$a;->l:Ljava/lang/String;

    .line 104
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lbsq$a;
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lbsq$a;->m:Ljava/lang/String;

    .line 112
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lbsq$a;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lbsq$a;->n:Ljava/lang/String;

    .line 120
    return-object p0
.end method
