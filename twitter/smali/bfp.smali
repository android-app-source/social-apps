.class public Lbfp;
.super Lbfj;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbfp$a;
    }
.end annotation


# instance fields
.field private final b:J

.field private c:Lcgi;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V
    .locals 11

    .prologue
    .line 41
    .line 42
    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v8

    const-class v0, Lcom/twitter/model/core/ac;

    .line 43
    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide/from16 v6, p5

    .line 41
    invoke-direct/range {v1 .. v9}, Lbfp;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJLcom/twitter/library/provider/t;Lcom/twitter/library/api/i;)V

    .line 44
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJLcom/twitter/library/provider/t;Lcom/twitter/library/api/i;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/client/Session;",
            "JJ",
            "Lcom/twitter/library/provider/t;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/ac;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v1 .. v8}, Lbfj;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JZLcom/twitter/library/provider/t;Lcom/twitter/library/api/i;)V

    .line 50
    iput-wide p5, p0, Lbfp;->b:J

    .line 51
    new-instance v0, Lcom/twitter/library/service/o;

    invoke-direct {v0}, Lcom/twitter/library/service/o;-><init>()V

    invoke-virtual {p0, v0}, Lbfp;->a(Lcom/twitter/library/service/e;)V

    .line 52
    return-void
.end method


# virtual methods
.method public a(Lcgi;)Lbfp;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lbfp;->c:Lcgi;

    .line 76
    return-object p0
.end method

.method protected a()Lcom/twitter/library/service/d;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 57
    invoke-virtual {p0}, Lbfp;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 58
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "favorites"

    aput-object v3, v1, v2

    const-string/jumbo v2, "destroy"

    aput-object v2, v1, v4

    .line 59
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "id"

    iget-wide v2, p0, Lbfp;->b:J

    .line 60
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->d()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->c()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 64
    iget-object v1, p0, Lbfp;->c:Lcgi;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbfp;->c:Lcgi;

    iget-object v1, v1, Lcgi;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 65
    const-string/jumbo v1, "impression_id"

    iget-object v2, p0, Lbfp;->c:Lcgi;

    iget-object v2, v2, Lcgi;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 66
    iget-object v1, p0, Lbfp;->c:Lcgi;

    invoke-virtual {v1}, Lcgi;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    const-string/jumbo v1, "earned"

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 71
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    const-string/jumbo v0, "app:twitter_service:favorite:delete"

    return-object v0
.end method
