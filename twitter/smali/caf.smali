.class public Lcaf;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcaf$b;,
        Lcaf$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcaf;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcaf;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:Lcad;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcaf$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcaf$b;-><init>(Lcaf$1;)V

    sput-object v0, Lcaf;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcaf$a;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, Lcaf$a;->a(Lcaf$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcaf;->b:Ljava/lang/String;

    .line 31
    invoke-static {p1}, Lcaf$a;->b(Lcaf$a;)I

    move-result v0

    iput v0, p0, Lcaf;->c:I

    .line 32
    invoke-static {p1}, Lcaf$a;->c(Lcaf$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcaf;->d:Ljava/lang/String;

    .line 33
    invoke-static {p1}, Lcaf$a;->d(Lcaf$a;)Lcad;

    move-result-object v0

    iput-object v0, p0, Lcaf;->e:Lcad;

    .line 34
    return-void
.end method

.method synthetic constructor <init>(Lcaf$a;Lcaf$1;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcaf;-><init>(Lcaf$a;)V

    return-void
.end method


# virtual methods
.method public a(Lcaf;)I
    .locals 2

    .prologue
    .line 38
    iget v0, p0, Lcaf;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p1, Lcaf;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 17
    check-cast p1, Lcaf;

    invoke-virtual {p0, p1}, Lcaf;->a(Lcaf;)I

    move-result v0

    return v0
.end method
