.class public final Lahq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lahp;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laib;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lage;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lahr;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lahg;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lahe;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lahi;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lahc;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laha;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lahq;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lahq;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Landroid/app/Activity;",
            ">;",
            "Lcta",
            "<",
            "Laib;",
            ">;",
            "Lcta",
            "<",
            "Lage;",
            ">;",
            "Lcta",
            "<",
            "Lahr;",
            ">;",
            "Lcta",
            "<",
            "Lahg;",
            ">;",
            "Lcta",
            "<",
            "Lahe;",
            ">;",
            "Lcta",
            "<",
            "Lahi;",
            ">;",
            "Lcta",
            "<",
            "Lahc;",
            ">;",
            "Lcta",
            "<",
            "Laha;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    sget-boolean v0, Lahq;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 54
    :cond_0
    iput-object p1, p0, Lahq;->b:Lcta;

    .line 55
    sget-boolean v0, Lahq;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 56
    :cond_1
    iput-object p2, p0, Lahq;->c:Lcta;

    .line 57
    sget-boolean v0, Lahq;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 58
    :cond_2
    iput-object p3, p0, Lahq;->d:Lcta;

    .line 59
    sget-boolean v0, Lahq;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 60
    :cond_3
    iput-object p4, p0, Lahq;->e:Lcta;

    .line 61
    sget-boolean v0, Lahq;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 62
    :cond_4
    iput-object p5, p0, Lahq;->f:Lcta;

    .line 63
    sget-boolean v0, Lahq;->a:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 64
    :cond_5
    iput-object p6, p0, Lahq;->g:Lcta;

    .line 65
    sget-boolean v0, Lahq;->a:Z

    if-nez v0, :cond_6

    if-nez p7, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 66
    :cond_6
    iput-object p7, p0, Lahq;->h:Lcta;

    .line 67
    sget-boolean v0, Lahq;->a:Z

    if-nez v0, :cond_7

    if-nez p8, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 68
    :cond_7
    iput-object p8, p0, Lahq;->i:Lcta;

    .line 69
    sget-boolean v0, Lahq;->a:Z

    if-nez v0, :cond_8

    if-nez p9, :cond_8

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 70
    :cond_8
    iput-object p9, p0, Lahq;->j:Lcta;

    .line 71
    sget-boolean v0, Lahq;->a:Z

    if-nez v0, :cond_9

    if-nez p10, :cond_9

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 72
    :cond_9
    iput-object p10, p0, Lahq;->k:Lcta;

    .line 73
    return-void
.end method

.method public static a(Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Landroid/app/Activity;",
            ">;",
            "Lcta",
            "<",
            "Laib;",
            ">;",
            "Lcta",
            "<",
            "Lage;",
            ">;",
            "Lcta",
            "<",
            "Lahr;",
            ">;",
            "Lcta",
            "<",
            "Lahg;",
            ">;",
            "Lcta",
            "<",
            "Lahe;",
            ">;",
            "Lcta",
            "<",
            "Lahi;",
            ">;",
            "Lcta",
            "<",
            "Lahc;",
            ">;",
            "Lcta",
            "<",
            "Laha;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Lahp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    new-instance v0, Lahq;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lahq;-><init>(Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lahp;
    .locals 11

    .prologue
    .line 77
    new-instance v0, Lahp;

    iget-object v1, p0, Lahq;->b:Lcta;

    .line 78
    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iget-object v2, p0, Lahq;->c:Lcta;

    .line 79
    invoke-interface {v2}, Lcta;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laib;

    iget-object v3, p0, Lahq;->d:Lcta;

    .line 80
    invoke-interface {v3}, Lcta;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lage;

    iget-object v4, p0, Lahq;->e:Lcta;

    .line 81
    invoke-interface {v4}, Lcta;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lahr;

    iget-object v5, p0, Lahq;->f:Lcta;

    .line 82
    invoke-interface {v5}, Lcta;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lahg;

    iget-object v6, p0, Lahq;->g:Lcta;

    .line 83
    invoke-interface {v6}, Lcta;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lahe;

    iget-object v7, p0, Lahq;->h:Lcta;

    .line 84
    invoke-interface {v7}, Lcta;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lahi;

    iget-object v8, p0, Lahq;->i:Lcta;

    .line 85
    invoke-interface {v8}, Lcta;->b()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lahc;

    iget-object v9, p0, Lahq;->j:Lcta;

    .line 86
    invoke-interface {v9}, Lcta;->b()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Laha;

    iget-object v10, p0, Lahq;->k:Lcta;

    .line 87
    invoke-interface {v10}, Lcta;->b()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/twitter/library/client/Session;

    invoke-direct/range {v0 .. v10}, Lahp;-><init>(Landroid/app/Activity;Laib;Lage;Lahr;Lahg;Lahe;Lahi;Lahc;Laha;Lcom/twitter/library/client/Session;)V

    .line 77
    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lahq;->a()Lahp;

    move-result-object v0

    return-object v0
.end method
