.class public Lbxa;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/util/object/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/j",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final e:Lcom/twitter/util/object/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/j",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/twitter/analytics/feature/model/TwitterScribeItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/util/object/j;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/util/object/j;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/util/object/j",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            "Lcom/twitter/util/object/j",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ")V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbxa;->a:Landroid/content/Context;

    .line 52
    iput-object p2, p0, Lbxa;->b:Lcom/twitter/util/object/j;

    .line 53
    iput-object p3, p0, Lbxa;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 54
    iput-object p4, p0, Lbxa;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 55
    iput-object p5, p0, Lbxa;->e:Lcom/twitter/util/object/j;

    .line 56
    iput-object p6, p0, Lbxa;->f:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 57
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 6

    .prologue
    .line 165
    iget-object v0, p0, Lbxa;->e:Lcom/twitter/util/object/j;

    invoke-interface {v0}, Lcom/twitter/util/object/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 166
    invoke-static {v0}, Lcom/twitter/model/core/Tweet;->b(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 167
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lbxa;->b:Lcom/twitter/util/object/j;

    invoke-interface {v2}, Lcom/twitter/util/object/j;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 168
    iget-object v2, p0, Lbxa;->a:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-static {v3, v2, v0, v4}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 169
    iget-object v0, p0, Lbxa;->f:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-virtual {v3, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lbxa;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 170
    invoke-static {v4, v1, p2, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 169
    return-object v0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 2

    .prologue
    .line 157
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lbxa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    const-string/jumbo v1, "tweet::tweet::impression"

    .line 158
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lbxa;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 159
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 157
    return-object v0
.end method

.method private h()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 143
    iget-object v0, p0, Lbxa;->e:Lcom/twitter/util/object/j;

    invoke-interface {v0}, Lcom/twitter/util/object/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 144
    const-string/jumbo v1, "avatar"

    const-string/jumbo v2, "profile_click"

    invoke-direct {p0, v4, v1, v2}, Lbxa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    .line 145
    iget-wide v2, v0, Lcom/twitter/model/core/Tweet;->s:J

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    invoke-static {v1, v2, v3, v0, v4}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lbxa;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 147
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 122
    const-string/jumbo v0, "tweet"

    const-string/jumbo v1, "quoted_tweet"

    const-string/jumbo v2, "click"

    invoke-direct {p0, v0, v1, v2}, Lbxa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    iget-object v1, p0, Lbxa;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 123
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 122
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 124
    return-void
.end method

.method a(Lcom/twitter/library/api/PromotedEvent;)V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lbxa;->e:Lcom/twitter/util/object/j;

    invoke-interface {v0}, Lcom/twitter/util/object/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 175
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 176
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    invoke-static {p1, v0}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v0

    .line 177
    invoke-virtual {v0}, Lbsq$a;->a()Lbsq;

    move-result-object v0

    .line 178
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 180
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/core/b;)V
    .locals 3

    .prologue
    .line 105
    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->u:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {p0, v0}, Lbxa;->a(Lcom/twitter/library/api/PromotedEvent;)V

    .line 106
    const/4 v0, 0x0

    const-string/jumbo v1, "cashtag"

    const-string/jumbo v2, "search"

    invoke-direct {p0, v0, v1, v2}, Lbxa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/core/b;->c:Ljava/lang/String;

    .line 107
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lbxa;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 108
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 106
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 109
    return-void
.end method

.method public a(Lcom/twitter/model/core/h;)V
    .locals 3

    .prologue
    .line 97
    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->e:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {p0, v0}, Lbxa;->a(Lcom/twitter/library/api/PromotedEvent;)V

    .line 98
    const/4 v0, 0x0

    const-string/jumbo v1, "hashtag"

    const-string/jumbo v2, "search"

    invoke-direct {p0, v0, v1, v2}, Lbxa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/core/h;->c:Ljava/lang/String;

    .line 99
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lbxa;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 100
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 98
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 101
    return-void
.end method

.method public a(Lcom/twitter/model/core/q;)V
    .locals 3

    .prologue
    .line 113
    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->f:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {p0, v0}, Lbxa;->a(Lcom/twitter/library/api/PromotedEvent;)V

    .line 114
    const/4 v0, 0x0

    const-string/jumbo v1, ""

    const-string/jumbo v2, "mention_click"

    invoke-direct {p0, v0, v1, v2}, Lbxa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/core/q;->j:Ljava/lang/String;

    .line 115
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p1, Lcom/twitter/model/core/q;->j:Ljava/lang/String;

    .line 116
    invoke-static {v1}, Lcom/twitter/library/scribe/b;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lbxa;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 117
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 114
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 118
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lbxa;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 65
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 66
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->e()Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 67
    if-nez v0, :cond_1

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    iget-object v0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->n:Ljava/lang/String;

    .line 72
    invoke-virtual {p0}, Lbxa;->f()Ljava/lang/String;

    move-result-object v1

    .line 73
    invoke-virtual {p0}, Lbxa;->g()Lcom/twitter/library/api/c;

    move-result-object v2

    .line 74
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 75
    invoke-direct {p0, p1, p2}, Lbxa;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v3

    .line 79
    const-string/jumbo v4, "app_download_client_event"

    invoke-virtual {v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->j(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 80
    const-string/jumbo v4, "4"

    invoke-virtual {v3, v4, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 83
    invoke-static {v0, v1}, Lcom/twitter/library/util/af;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 84
    const-string/jumbo v1, "3"

    invoke-virtual {v3, v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 86
    if-eqz v2, :cond_2

    .line 87
    const-string/jumbo v0, "6"

    .line 88
    invoke-virtual {v2}, Lcom/twitter/library/api/c;->a()Ljava/lang/String;

    move-result-object v1

    .line 87
    invoke-virtual {v3, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 89
    invoke-virtual {v2}, Lcom/twitter/library/api/c;->b()Z

    move-result v0

    invoke-virtual {v3, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Z)Lcom/twitter/analytics/model/ScribeLog;

    .line 91
    :cond_2
    invoke-static {v3}, Lcpm;->a(Lcpk;)V

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 128
    const-string/jumbo v0, "tweet"

    const-string/jumbo v1, "tweet_analytics"

    const-string/jumbo v2, "click"

    invoke-direct {p0, v0, v1, v2}, Lbxa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 129
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->c:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {p0, v0}, Lbxa;->a(Lcom/twitter/library/api/PromotedEvent;)V

    .line 133
    invoke-direct {p0}, Lbxa;->h()V

    .line 134
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 137
    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->d:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {p0, v0}, Lbxa;->a(Lcom/twitter/library/api/PromotedEvent;)V

    .line 138
    invoke-direct {p0}, Lbxa;->h()V

    .line 139
    return-void
.end method

.method public e()V
    .locals 3

    .prologue
    .line 151
    const-string/jumbo v0, "tweet"

    const-string/jumbo v1, "recipient_list"

    const-string/jumbo v2, "impression"

    invoke-direct {p0, v0, v1, v2}, Lbxa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    iget-object v1, p0, Lbxa;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 152
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 151
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 153
    return-void
.end method

.method f()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 185
    invoke-static {}, Lcom/twitter/util/z;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method g()Lcom/twitter/library/api/c;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 190
    sget-object v0, Lcom/twitter/library/client/b;->a:Lcom/twitter/library/client/b;

    invoke-virtual {v0}, Lcom/twitter/library/client/b;->a()Lcom/twitter/library/api/c;

    move-result-object v0

    return-object v0
.end method
