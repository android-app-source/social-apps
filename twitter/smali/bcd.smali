.class public Lbcd;
.super Lbbw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbcd$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbbw",
        "<",
        "Lcom/twitter/model/json/contacts/JsonUploadContactsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Z


# direct methods
.method protected constructor <init>(Lbcd$a;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lbbw;-><init>(Lbbw$a;)V

    .line 24
    invoke-static {p1}, Lbcd$a;->a(Lbcd$a;)Z

    move-result v0

    iput-boolean v0, p0, Lbcd;->c:Z

    .line 25
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/d$a;)V
    .locals 2

    .prologue
    .line 41
    iget-boolean v0, p0, Lbcd;->c:Z

    if-eqz v0, :cond_0

    .line 42
    const-string/jumbo v0, "live_sync_request"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 43
    const-string/jumbo v0, "Live sync is always performed in the background"

    invoke-virtual {p0, v0}, Lbcd;->l(Ljava/lang/String;)Lcom/twitter/library/service/s;

    .line 45
    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/json/contacts/JsonUploadContactsResponse;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-super {p0, p1, p2, p3}, Lbbw;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V

    .line 51
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/contacts/JsonUploadContactsResponse;

    .line 53
    if-eqz v0, :cond_0

    .line 54
    iget-object v0, v0, Lcom/twitter/model/json/contacts/JsonUploadContactsResponse;->c:Ljava/util/List;

    invoke-virtual {p0, v0}, Lbcd;->a(Ljava/util/List;)V

    .line 57
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 17
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbcd;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    const-string/jumbo v0, "upload"

    return-object v0
.end method

.method protected e()Lcom/twitter/library/api/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/json/contacts/JsonUploadContactsResponse;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    const-class v0, Lcom/twitter/model/json/contacts/JsonUploadContactsResponse;

    const-class v1, Lcom/twitter/model/core/z;

    invoke-static {v0, v1}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lbcd;->e()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
