.class public Lcje;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/onboarding/Subtask;",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/util/object/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/onboarding/Subtask;",
            "Landroid/content/Intent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcje;->a:Lcom/twitter/util/object/d;

    .line 26
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/onboarding/DeepLinkNavigationLink;)Lcom/twitter/model/onboarding/b;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/twitter/model/onboarding/d;->a:Lcom/twitter/model/onboarding/d;

    return-object v0
.end method

.method public a(Lcom/twitter/model/onboarding/TaskContext;)Lcom/twitter/model/onboarding/b;
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcje;->a:Lcom/twitter/util/object/d;

    invoke-virtual {p1}, Lcom/twitter/model/onboarding/TaskContext;->b()Lcom/twitter/model/onboarding/Subtask;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 31
    invoke-static {v0, p1}, Lcji;->a(Landroid/content/Intent;Lcom/twitter/model/onboarding/TaskContext;)V

    .line 32
    new-instance v1, Lcom/twitter/model/onboarding/f;

    invoke-direct {v1, v0}, Lcom/twitter/model/onboarding/f;-><init>(Landroid/content/Intent;)V

    return-object v1
.end method
