.class public Lbal;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/y;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lbax;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lbax;)V
    .locals 1

    .prologue
    .line 36
    const-string/jumbo v0, "EnrollLoginVerification"

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 37
    iput-object p3, p0, Lbal;->a:Lbax;

    .line 38
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 43
    invoke-virtual {p0}, Lbal;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "account"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "login_verification_enrollment"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 45
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "public_key"

    iget-object v2, p0, Lbal;->a:Lbax;

    iget-object v2, v2, Lbax;->b:Ljava/lang/String;

    .line 46
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "offline_code"

    iget-object v2, p0, Lbal;->a:Lbax;

    iget-object v2, v2, Lbax;->c:Ljava/lang/String;

    .line 47
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "iteration_count"

    iget-object v2, p0, Lbal;->a:Lbax;

    iget v2, v2, Lbax;->a:I

    .line 48
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "udid"

    .line 49
    invoke-static {}, Lcom/twitter/library/platform/notifications/PushRegistration;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "token"

    iget-object v2, p0, Lbal;->p:Landroid/content/Context;

    .line 50
    invoke-static {v2}, Lcom/google/android/gcm/b;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "update_push_destination"

    const-string/jumbo v2, "true"

    .line 51
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 52
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 53
    const-string/jumbo v1, "environment"

    .line 54
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 53
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 56
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V
    .locals 3

    .prologue
    .line 68
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    iget-object v1, p2, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "custom_errors"

    .line 70
    invoke-virtual {p3}, Lcom/twitter/library/api/y;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/z;

    invoke-static {v0}, Lcom/twitter/model/core/z;->a(Lcom/twitter/model/core/z;)[I

    move-result-object v0

    .line 69
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 72
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 23
    check-cast p3, Lcom/twitter/library/api/y;

    invoke-virtual {p0, p1, p2, p3}, Lbal;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/api/y;
    .locals 1

    .prologue
    .line 61
    const/16 v0, 0x2b

    invoke-static {v0}, Lcom/twitter/library/api/y;->a(I)Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lbal;->b()Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method
