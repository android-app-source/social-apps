.class public Lcch;
.super Lcbx;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcch$b;,
        Lcch$a;
    }
.end annotation


# static fields
.field public static final b:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcch;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcch$b;

    invoke-direct {v0}, Lcch$b;-><init>()V

    sput-object v0, Lcch;->b:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcch$a;)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcbx;-><init>(Lcbx$a;)V

    .line 22
    invoke-static {p1}, Lcch$a;->a(Lcch$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcch;->c:J

    .line 23
    return-void
.end method

.method synthetic constructor <init>(Lcch$a;Lcch$1;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcch;-><init>(Lcch$a;)V

    return-void
.end method

.method private a(Lcch;)Z
    .locals 4

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcbx;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcch;->c:J

    iget-wide v2, p1, Lcch;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x6

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 38
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcch;

    if-eqz v0, :cond_1

    check-cast p1, Lcch;

    invoke-direct {p0, p1}, Lcch;->a(Lcch;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x1

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 48
    invoke-super {p0}, Lcbx;->hashCode()I

    move-result v0

    .line 49
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcch;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 50
    return v0
.end method
