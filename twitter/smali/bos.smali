.class public Lbos;
.super Lbou;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbou",
        "<",
        "Lcom/twitter/model/dms/w;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/database/sqlite/SQLiteDatabase;JZZ)V
    .locals 0

    .prologue
    .line 15
    invoke-direct/range {p0 .. p5}, Lbou;-><init>(Landroid/database/sqlite/SQLiteDatabase;JZZ)V

    .line 16
    return-void
.end method


# virtual methods
.method bridge synthetic a(Lcom/twitter/model/dms/d;)V
    .locals 0

    .prologue
    .line 11
    check-cast p1, Lcom/twitter/model/dms/w;

    invoke-virtual {p0, p1}, Lbos;->a(Lcom/twitter/model/dms/w;)V

    return-void
.end method

.method a(Lcom/twitter/model/dms/w;)V
    .locals 5

    .prologue
    .line 20
    iget-object v0, p1, Lcom/twitter/model/dms/w;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 21
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    .line 22
    iget-object v2, p0, Lbos;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string/jumbo v3, "conversation_entries"

    const-string/jumbo v4, "entry_id=?"

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 25
    :cond_0
    return-void
.end method
