.class public Lbrj;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/google/android/exoplayer/upstream/BandwidthMeter;


# instance fields
.field private final a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/exoplayer/upstream/TransferListener;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/exoplayer/upstream/BandwidthMeter;


# direct methods
.method public varargs constructor <init>(Lcom/google/android/exoplayer/upstream/BandwidthMeter;[Lcom/google/android/exoplayer/upstream/TransferListener;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lbrj;->b:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p2

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lbrj;->a:Ljava/util/Collection;

    .line 29
    iget-object v0, p0, Lbrj;->a:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 30
    iget-object v0, p0, Lbrj;->a:Ljava/util/Collection;

    invoke-static {v0, p2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 31
    return-void
.end method

.method public varargs constructor <init>([Lcom/google/android/exoplayer/upstream/TransferListener;)V
    .locals 1

    .prologue
    .line 23
    invoke-static {}, Lcom/twitter/library/av/playback/a;->a()Lcom/twitter/library/av/playback/a;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lbrj;-><init>(Lcom/google/android/exoplayer/upstream/BandwidthMeter;[Lcom/google/android/exoplayer/upstream/TransferListener;)V

    .line 24
    return-void
.end method


# virtual methods
.method public getBitrateEstimate()J
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lbrj;->b:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/BandwidthMeter;->getBitrateEstimate()J

    move-result-wide v0

    return-wide v0
.end method

.method public onBytesTransferred(I)V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lbrj;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/upstream/TransferListener;

    .line 48
    invoke-interface {v0, p1}, Lcom/google/android/exoplayer/upstream/TransferListener;->onBytesTransferred(I)V

    goto :goto_0

    .line 50
    :cond_0
    return-void
.end method

.method public onTransferEnd()V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lbrj;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/upstream/TransferListener;

    .line 55
    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/TransferListener;->onTransferEnd()V

    goto :goto_0

    .line 57
    :cond_0
    return-void
.end method

.method public onTransferStart()V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lbrj;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/upstream/TransferListener;

    .line 41
    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/TransferListener;->onTransferStart()V

    goto :goto_0

    .line 43
    :cond_0
    return-void
.end method
