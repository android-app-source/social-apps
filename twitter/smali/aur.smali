.class public abstract Laur;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "Twttr"


# instance fields
.field private final a:Z

.field private b:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lcrt;->a()Z

    move-result v0

    invoke-direct {p0, p1, p2, p3, v0}, Laur;-><init>(Landroid/content/Context;Ljava/lang/String;IZ)V

    .line 28
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;IZ)V
    .locals 1

    .prologue
    .line 31
    if-eqz p4, :cond_0

    const-string/jumbo p2, ":memory:"

    :cond_0
    sget-object v0, Lavc;->a:Landroid/database/sqlite/SQLiteDatabase$CursorFactory;

    invoke-direct {p0, p1, p2, v0, p3}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 32
    iput-boolean p4, p0, Laur;->a:Z

    .line 33
    return-void
.end method


# virtual methods
.method protected b()V
    .locals 5

    .prologue
    .line 60
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p0}, Laur;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 62
    invoke-virtual {p0}, Laur;->bq_()Lcom/twitter/database/model/i;

    move-result-object v1

    .line 63
    if-eqz v1, :cond_0

    .line 64
    new-instance v2, Lavj;

    invoke-interface {v1}, Lcom/twitter/database/model/i;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lavj;-><init>(Ljava/lang/String;)V

    new-instance v1, Lavk;

    invoke-direct {v1, v0}, Lavk;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 65
    invoke-virtual {v2, v1}, Lavj;->a(Lavg$b;)Lavg;

    move-result-object v0

    new-instance v1, Lavd;

    .line 66
    invoke-virtual {p0}, Laur;->bq_()Lcom/twitter/database/model/i;

    move-result-object v2

    invoke-direct {v1, v2}, Lavd;-><init>(Lcom/twitter/database/model/i;)V

    invoke-virtual {v0, v1}, Lavg;->a(Lavg$b;)Lavg;

    move-result-object v0

    new-instance v1, Lavi;

    invoke-direct {v1}, Lavi;-><init>()V

    .line 67
    invoke-virtual {v0, v1}, Lavg;->a(Lavg$a;)Lavg;

    move-result-object v0

    new-instance v1, Lavf;

    const/4 v2, 0x2

    new-array v2, v2, [Lavh;

    const/4 v3, 0x0

    sget-object v4, Lavk;->a:Lavh;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lavd;->a:Lavh;

    aput-object v4, v2, v3

    invoke-direct {v1, v2}, Lavf;-><init>([Lavh;)V

    .line 68
    invoke-virtual {v0, v1}, Lavg;->a(Lavg$a;)Lavg;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lavg;->a()Lavg;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Lavg;->b()Lavg;

    .line 73
    :cond_0
    return-void
.end method

.method public bq_()Lcom/twitter/database/model/i;
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    .prologue
    .line 43
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 44
    iget-boolean v1, p0, Laur;->a:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Laur;->b:Z

    if-nez v1, :cond_0

    .line 46
    const-string/jumbo v1, "PRAGMA synchronous = off;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 48
    const-string/jumbo v1, "PRAGMA journal_mode = off;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 50
    const-string/jumbo v1, "PRAGMA locking_mode = exclusive;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 51
    const/4 v1, 0x1

    iput-boolean v1, p0, Laur;->b:Z

    .line 53
    :cond_0
    return-object v0
.end method
