.class public Lcfo;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcfo$b;,
        Lcfo$c;,
        Lcfo$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Lcfo$b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcfo$c;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcfo$c;-><init>(Lcfo$1;)V

    sput-object v0, Lcfo;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(Lcfo$a;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1}, Lcfo$a;->a(Lcfo$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcfo;->b:J

    .line 28
    invoke-static {p1}, Lcfo$a;->b(Lcfo$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfo;->c:Ljava/lang/String;

    .line 29
    invoke-static {p1}, Lcfo$a;->c(Lcfo$a;)Lcfo$b;

    move-result-object v0

    iput-object v0, p0, Lcfo;->d:Lcfo$b;

    .line 30
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcfo;->d:Lcfo$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcfo;->d:Lcfo$b;

    iget-object v0, v0, Lcfo$b;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
