.class public Lbxd;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/t;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 251
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 252
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/t;

    .line 253
    const/16 v3, 0x40

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, Lcom/twitter/model/core/t;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0x20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 255
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/twitter/model/core/f;Lcom/twitter/model/core/q;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/f",
            "<",
            "Lcom/twitter/model/core/q;",
            ">;",
            "Lcom/twitter/model/core/q;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v4

    .line 186
    if-eqz p1, :cond_2

    iget-wide v0, p1, Lcom/twitter/model/core/q;->c:J

    move-wide v2, v0

    .line 187
    :goto_0
    if-eqz p1, :cond_0

    .line 188
    invoke-virtual {v4, p1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 190
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/model/core/f;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/q;

    .line 192
    invoke-virtual {v0}, Lcom/twitter/model/core/q;->c()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-wide v6, v0, Lcom/twitter/model/core/q;->c:J

    cmp-long v5, v6, v2

    if-eqz v5, :cond_1

    .line 193
    invoke-virtual {v4, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1

    .line 186
    :cond_2
    const-wide/16 v0, 0x0

    move-wide v2, v0

    goto :goto_0

    .line 196
    :cond_3
    invoke-virtual {v4}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private static a(Lcom/twitter/model/core/Tweet;JLjava/util/Collection;Z)Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/Tweet;",
            "J",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;Z)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 384
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 385
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v0

    .line 389
    iget-wide v2, p0, Lcom/twitter/model/core/Tweet;->s:J

    iget-object v4, p0, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    move v9, p4

    invoke-static/range {v1 .. v9}, Lbxd;->a(Ljava/util/Map;JLjava/lang/String;Ljava/lang/String;JLjava/util/Collection;Z)V

    .line 393
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->aa()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 394
    iget-wide v2, p0, Lcom/twitter/model/core/Tweet;->b:J

    iget-object v4, p0, Lcom/twitter/model/core/Tweet;->z:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/model/core/Tweet;->p:Ljava/lang/String;

    move-wide v6, p1

    move-object v8, p3

    move v9, p4

    invoke-static/range {v1 .. v9}, Lbxd;->a(Ljava/util/Map;JLjava/lang/String;Ljava/lang/String;JLjava/util/Collection;Z)V

    .line 399
    :cond_0
    iget-object v2, v0, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-virtual {v2}, Lcom/twitter/model/core/f;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/core/q;

    move-wide v3, p1

    move-object v5, p3

    move v6, p4

    .line 400
    invoke-static/range {v1 .. v6}, Lbxd;->a(Ljava/util/Map;Lcom/twitter/model/core/q;JLjava/util/Collection;Z)V

    goto :goto_0

    .line 404
    :cond_1
    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-static {v0}, Lcom/twitter/model/util/d;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 405
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/n;

    .line 406
    iget-wide v2, v0, Lcom/twitter/model/core/n;->b:J

    iget-object v4, v0, Lcom/twitter/model/core/n;->c:Ljava/lang/String;

    iget-object v5, v0, Lcom/twitter/model/core/n;->d:Ljava/lang/String;

    move-wide v6, p1

    move-object v8, p3

    move v9, p4

    invoke-static/range {v1 .. v9}, Lbxd;->a(Ljava/util/Map;JLjava/lang/String;Ljava/lang/String;JLjava/util/Collection;Z)V

    goto :goto_1

    .line 411
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    .line 412
    if-eqz v0, :cond_3

    .line 413
    invoke-virtual {v0}, Lcax;->h()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 414
    if-eqz v0, :cond_3

    .line 415
    iget-wide v2, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v4, v0, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    iget-object v5, v0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    move-wide v6, p1

    move-object v8, p3

    move v9, p4

    invoke-static/range {v1 .. v9}, Lbxd;->a(Ljava/util/Map;JLjava/lang/String;Ljava/lang/String;JLjava/util/Collection;Z)V

    .line 420
    :cond_3
    return-object v1
.end method

.method public static a(Lcom/twitter/model/core/Tweet;JLjava/util/Collection;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/Tweet;",
            "J",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 347
    const/4 v0, 0x1

    .line 348
    invoke-static {p0, p1, p2, p3, v0}, Lbxd;->a(Lcom/twitter/model/core/Tweet;JLjava/util/Collection;Z)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 347
    invoke-static {v0}, Lcom/twitter/util/collection/ImmutableSet;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/Map;JLjava/lang/String;Ljava/lang/String;JLjava/util/Collection;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/q;",
            ">;J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 425
    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p5

    move-object v6, p7

    invoke-static/range {v1 .. v6}, Lbxd;->a(Ljava/util/Map;JJLjava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 426
    if-eqz p8, :cond_1

    const/4 v0, 0x0

    .line 428
    :goto_0
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 430
    :cond_0
    return-void

    .line 426
    :cond_1
    new-instance v0, Lcom/twitter/model/core/q$a;

    invoke-direct {v0}, Lcom/twitter/model/core/q$a;-><init>()V

    .line 427
    invoke-virtual {v0, p1, p2}, Lcom/twitter/model/core/q$a;->a(J)Lcom/twitter/model/core/q$a;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/twitter/model/core/q$a;->b(Ljava/lang/String;)Lcom/twitter/model/core/q$a;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/twitter/model/core/q$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/q$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/q$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/q;

    goto :goto_0
.end method

.method private static a(Ljava/util/Map;Lcom/twitter/model/core/q;JLjava/util/Collection;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/q;",
            ">;",
            "Lcom/twitter/model/core/q;",
            "J",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 434
    iget-wide v2, p1, Lcom/twitter/model/core/q;->c:J

    move-object v1, p0

    move-wide v4, p2

    move-object v6, p4

    invoke-static/range {v1 .. v6}, Lbxd;->a(Ljava/util/Map;JJLjava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 435
    iget-wide v0, p1, Lcom/twitter/model/core/q;->c:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    if-eqz p5, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-interface {p0, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 437
    :cond_1
    return-void
.end method

.method public static a(J)Z
    .locals 2

    .prologue
    .line 269
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 43
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->S()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/Tweet;J)Z
    .locals 3

    .prologue
    .line 119
    if-eqz p0, :cond_1

    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->b:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->aa()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->s:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/Tweet;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-static {p0, p1}, Lbxd;->c(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 84
    :cond_0
    :goto_0
    return v1

    .line 76
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->k()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    .line 81
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    .line 82
    invoke-virtual {v0}, Lcom/twitter/model/core/f;->b()I

    move-result v3

    .line 83
    invoke-virtual {v2}, Lcom/twitter/model/core/f;->b()I

    move-result v4

    .line 84
    if-ne v3, v4, :cond_3

    if-eqz v3, :cond_2

    .line 85
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Iterable;)Lcom/twitter/util/collection/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 86
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Iterable;)Lcom/twitter/util/collection/o;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 84
    goto :goto_0

    :cond_3
    move v0, v1

    .line 86
    goto :goto_1
.end method

.method public static a(Lcom/twitter/model/core/r;)Z
    .locals 4

    .prologue
    .line 135
    iget-wide v0, p0, Lcom/twitter/model/core/r;->q:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/model/core/r;->b:J

    iget-wide v2, p0, Lcom/twitter/model/core/r;->r:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/r;J)Z
    .locals 5

    .prologue
    .line 127
    iget-wide v0, p0, Lcom/twitter/model/core/r;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-wide v0, p0, Lcom/twitter/model/core/r;->b:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/model/core/r;->n:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/util/Map;JJLjava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/q;",
            ">;JJ",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 441
    cmp-long v0, p1, p3

    if-eqz v0, :cond_1

    if-eqz p5, :cond_0

    .line 442
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p5, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 443
    :cond_0
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v1, 0x32

    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    .line 441
    :goto_0
    return v0

    .line 443
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/twitter/model/core/Tweet;JLjava/util/Collection;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/Tweet;",
            "J",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 365
    const/4 v0, 0x0

    .line 366
    invoke-static {p0, p1, p2, p3, v0}, Lbxd;->a(Lcom/twitter/model/core/Tweet;JLjava/util/Collection;Z)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    .line 365
    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 47
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->T()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/twitter/model/core/Tweet;J)Z
    .locals 5

    .prologue
    .line 123
    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->u:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->b:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/model/core/Tweet;->F:Z

    if-nez v0, :cond_1

    :cond_0
    invoke-static {p0}, Lbxd;->j(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/Tweet;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 90
    invoke-static {p0, p1}, Lbxd;->c(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/Tweet;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v0

    .line 93
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v2

    .line 94
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v3

    .line 95
    if-nez v2, :cond_2

    if-nez v3, :cond_2

    move v0, v1

    .line 96
    goto :goto_0

    .line 98
    :cond_2
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    move v0, v1

    .line 101
    goto :goto_0
.end method

.method public static b(Lcom/twitter/model/core/r;)Z
    .locals 4

    .prologue
    .line 143
    invoke-static {p0}, Lbxd;->a(Lcom/twitter/model/core/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    iget-wide v2, p0, Lcom/twitter/model/core/r;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/v;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/twitter/model/core/Tweet;J)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/Tweet;",
            "J)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/t;",
            ">;"
        }
    .end annotation

    .prologue
    .line 278
    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-direct {v4}, Ljava/util/LinkedHashSet;-><init>()V

    .line 279
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v5

    .line 281
    const/4 v0, 0x0

    .line 284
    new-instance v1, Lcom/twitter/model/core/t;

    iget-object v2, p0, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    iget-wide v6, p0, Lcom/twitter/model/core/Tweet;->s:J

    .line 285
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v6, v7, v3}, Lcom/twitter/model/core/t;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    .line 287
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->aa()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lcom/twitter/model/core/Tweet;->s:J

    .line 288
    :goto_0
    cmp-long v2, p1, v2

    if-eqz v2, :cond_0

    .line 289
    invoke-virtual {v4, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 291
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->aa()Z

    move-result v1

    if-eqz v1, :cond_9

    iget-wide v2, p0, Lcom/twitter/model/core/Tweet;->b:J

    cmp-long v1, v2, p1

    if-eqz v1, :cond_9

    iget-wide v2, p0, Lcom/twitter/model/core/Tweet;->b:J

    iget-wide v6, p0, Lcom/twitter/model/core/Tweet;->s:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_9

    .line 292
    new-instance v1, Lcom/twitter/model/core/t;

    iget-object v2, p0, Lcom/twitter/model/core/Tweet;->p:Ljava/lang/String;

    iget-wide v6, p0, Lcom/twitter/model/core/Tweet;->b:J

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->e()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v6, v7, v3}, Lcom/twitter/model/core/t;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    .line 299
    :cond_0
    :goto_1
    iget-object v0, v5, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-virtual {v0}, Lcom/twitter/model/core/f;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/q;

    .line 300
    iget-wide v6, v0, Lcom/twitter/model/core/q;->c:J

    cmp-long v3, v6, p1

    if-eqz v3, :cond_1

    .line 301
    invoke-static {v0}, Lcom/twitter/model/core/t;->a(Lcom/twitter/model/core/q;)Lcom/twitter/model/core/t;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 287
    :cond_2
    iget-wide v2, p0, Lcom/twitter/model/core/Tweet;->b:J

    goto :goto_0

    .line 305
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 306
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    invoke-virtual {v0}, Lcax;->h()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 307
    if-eqz v0, :cond_4

    iget-wide v2, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    cmp-long v2, p1, v2

    if-eqz v2, :cond_4

    .line 308
    new-instance v2, Lcom/twitter/model/core/t;

    iget-object v3, v0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    iget-wide v6, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    invoke-direct {v2, v3, v6, v7, v0}, Lcom/twitter/model/core/t;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 313
    :cond_4
    iget-object v0, v5, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-static {v0}, Lcom/twitter/model/util/d;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 314
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/n;

    .line 315
    iget-wide v6, v0, Lcom/twitter/model/core/n;->b:J

    cmp-long v3, p1, v6

    if-eqz v3, :cond_5

    .line 316
    invoke-static {v0}, Lcom/twitter/model/core/t;->a(Lcom/twitter/model/core/n;)Lcom/twitter/model/core/t;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 321
    :cond_6
    const-string/jumbo v0, "remove_quote_tweet_author_from_replies_5386"

    const-string/jumbo v2, "treatment"

    invoke-static {v0, v2}, Lcoi;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 322
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->G()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->R()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 323
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->w:Lcom/twitter/model/core/r;

    invoke-virtual {v0, p1, p2, v4}, Lcom/twitter/model/core/r;->a(JLjava/util/Set;)V

    .line 327
    :cond_7
    invoke-virtual {v4}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 328
    invoke-virtual {v4, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 330
    :cond_8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0

    :cond_9
    move-object v1, v0

    goto/16 :goto_1
.end method

.method public static c(Lcom/twitter/model/core/r;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/r;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/twitter/model/core/r;->b()Z

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/g;->b(Z)Z

    .line 179
    iget-object v0, p0, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-static {p0}, Lbxd;->d(Lcom/twitter/model/core/r;)Lcom/twitter/model/core/q;

    move-result-object v1

    invoke-static {v0, v1}, Lbxd;->a(Lcom/twitter/model/core/f;Lcom/twitter/model/core/q;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 51
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/Tweet;)Z
    .locals 4

    .prologue
    .line 105
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->u:J

    iget-wide v2, p1, Lcom/twitter/model/core/Tweet;->u:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Lcom/twitter/model/core/r;)Lcom/twitter/model/core/q;
    .locals 4

    .prologue
    .line 233
    invoke-virtual {p0}, Lcom/twitter/model/core/r;->b()Z

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/g;->b(Z)Z

    .line 234
    iget-object v0, p0, Lcom/twitter/model/core/r;->h:Lcom/twitter/model/core/v;

    iget-wide v2, p0, Lcom/twitter/model/core/r;->r:J

    .line 235
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/v;->b(J)Lcom/twitter/model/core/q;

    move-result-object v0

    .line 236
    if-eqz v0, :cond_0

    .line 246
    :goto_0
    return-object v0

    .line 238
    :cond_0
    invoke-static {p0}, Lbxd;->b(Lcom/twitter/model/core/r;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 239
    new-instance v0, Lcom/twitter/model/core/q$a;

    invoke-direct {v0}, Lcom/twitter/model/core/q$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/core/r;->c:Ljava/lang/String;

    .line 240
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/q$a;->b(Ljava/lang/String;)Lcom/twitter/model/core/q$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/core/r;->b:J

    .line 241
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/q$a;->a(J)Lcom/twitter/model/core/q$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/core/r;->d:Ljava/lang/String;

    .line 242
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/q$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/q$a;

    move-result-object v0

    .line 243
    invoke-virtual {v0}, Lcom/twitter/model/core/q$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/q;

    goto :goto_0

    .line 246
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/twitter/model/core/Tweet;)Z
    .locals 2

    .prologue
    .line 57
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x16

    iget v1, p0, Lcom/twitter/model/core/Tweet;->f:I

    if-eq v0, v1, :cond_0

    const/16 v0, 0x23

    iget v1, p0, Lcom/twitter/model/core/Tweet;->f:I

    if-eq v0, v1, :cond_0

    const/16 v0, 0x24

    iget v1, p0, Lcom/twitter/model/core/Tweet;->f:I

    if-eq v0, v1, :cond_0

    const/16 v0, 0x25

    iget v1, p0, Lcom/twitter/model/core/Tweet;->f:I

    if-eq v0, v1, :cond_0

    const/16 v0, 0x1c

    iget v1, p0, Lcom/twitter/model/core/Tweet;->f:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 109
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->D()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->B()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 115
    invoke-static {p0}, Lbxd;->e(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->D()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Lcom/twitter/model/core/Tweet;)Z
    .locals 4

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->s:J

    iget-wide v2, p0, Lcom/twitter/model/core/Tweet;->E:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(Lcom/twitter/model/core/Tweet;)Z
    .locals 4

    .prologue
    .line 139
    invoke-static {p0}, Lbxd;->g(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/core/Tweet;->s:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/v;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i(Lcom/twitter/model/core/Tweet;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/Tweet;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->p()Z

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/g;->b(Z)Z

    .line 169
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-static {p0}, Lbxd;->k(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/core/q;

    move-result-object v1

    invoke-static {v0, v1}, Lbxd;->a(Lcom/twitter/model/core/f;Lcom/twitter/model/core/q;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static j(Lcom/twitter/model/core/Tweet;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 259
    const-string/jumbo v2, "ad_formats_no_screen_name_ads_favorites_android_4822"

    const-string/jumbo v3, "favorites_enabled"

    invoke-static {v2, v3}, Lcoi;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 262
    if-eqz v2, :cond_2

    .line 263
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->Y()Z

    move-result v2

    if-nez v2, :cond_1

    .line 265
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 263
    goto :goto_0

    .line 265
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->c()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->Y()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private static k(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/core/q;
    .locals 4

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->p()Z

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/g;->b(Z)Z

    .line 206
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/core/Tweet;->E:J

    .line 207
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/v;->b(J)Lcom/twitter/model/core/q;

    move-result-object v0

    .line 208
    if-eqz v0, :cond_0

    .line 223
    :goto_0
    return-object v0

    .line 210
    :cond_0
    invoke-static {p0}, Lbxd;->h(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 211
    new-instance v0, Lcom/twitter/model/core/q$a;

    invoke-direct {v0}, Lcom/twitter/model/core/q$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    .line 212
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/q$a;->b(Ljava/lang/String;)Lcom/twitter/model/core/q$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/core/Tweet;->s:J

    .line 213
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/q$a;->a(J)Lcom/twitter/model/core/q$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    .line 214
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/q$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/q$a;

    move-result-object v0

    .line 215
    invoke-virtual {v0}, Lcom/twitter/model/core/q$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/q;

    goto :goto_0

    .line 216
    :cond_1
    iget-object v0, p0, Lcom/twitter/model/core/Tweet;->D:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 217
    new-instance v0, Lcom/twitter/model/core/q$a;

    invoke-direct {v0}, Lcom/twitter/model/core/q$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/core/Tweet;->D:Ljava/lang/String;

    .line 218
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/q$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/q$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/model/core/Tweet;->E:J

    .line 219
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/q$a;->a(J)Lcom/twitter/model/core/q$a;

    move-result-object v0

    .line 220
    invoke-virtual {v0}, Lcom/twitter/model/core/q$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/q;

    goto :goto_0

    .line 223
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
