.class public Labo;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lacg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lacg",
        "<",
        "Lcom/twitter/model/moments/viewmodels/MomentPage;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/viewmodels/k;

.field private final b:Labn;

.field private final c:Lcom/twitter/android/moments/ui/maker/an;

.field private d:Lcom/twitter/media/request/b$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/media/request/b$b",
            "<",
            "Lcom/twitter/media/request/ImageResponse;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lrx/subjects/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/a",
            "<",
            "Lacg",
            "<",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Lrx/j;


# direct methods
.method public constructor <init>(Labn;Lcom/twitter/android/moments/viewmodels/k;Lcom/twitter/android/moments/ui/maker/an;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {}, Lrx/subjects/a;->r()Lrx/subjects/a;

    move-result-object v0

    iput-object v0, p0, Labo;->e:Lrx/subjects/a;

    .line 70
    iput-object p1, p0, Labo;->b:Labn;

    .line 71
    iput-object p2, p0, Labo;->a:Lcom/twitter/android/moments/viewmodels/k;

    .line 72
    iput-object p3, p0, Labo;->c:Lcom/twitter/android/moments/ui/maker/an;

    .line 73
    return-void
.end method

.method public static a(Labn;Lcom/twitter/android/moments/viewmodels/k;)Labo;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Labo;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/an;

    invoke-direct {v1}, Lcom/twitter/android/moments/ui/maker/an;-><init>()V

    invoke-direct {v0, p0, p1, v1}, Labo;-><init>(Labn;Lcom/twitter/android/moments/viewmodels/k;Lcom/twitter/android/moments/ui/maker/an;)V

    return-object v0
.end method

.method public static a(Landroid/view/LayoutInflater;Lcom/twitter/android/moments/viewmodels/k;Lcom/twitter/model/moments/e;Lcom/twitter/android/moments/ui/fullscreen/cu;)Labo;
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    .line 55
    invoke-static {p0, p2, p3, v0}, Labn;->a(Landroid/view/LayoutInflater;Lcom/twitter/model/moments/e;Lcom/twitter/android/moments/ui/fullscreen/cu;Z)Labn;

    move-result-object v0

    .line 54
    invoke-static {v0, p1}, Labo;->a(Labn;Lcom/twitter/android/moments/viewmodels/k;)Labo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Labo;)Lcom/twitter/media/request/b$b;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Labo;->d:Lcom/twitter/media/request/b$b;

    return-object v0
.end method

.method static synthetic b(Labo;)Lcom/twitter/android/moments/viewmodels/k;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Labo;->a:Lcom/twitter/android/moments/viewmodels/k;

    return-object v0
.end method

.method private b(Labn;Lcom/twitter/android/moments/viewmodels/k;)V
    .locals 2

    .prologue
    .line 127
    .line 128
    invoke-interface {p2}, Lcom/twitter/android/moments/viewmodels/k;->c()Lcom/twitter/model/moments/e;

    move-result-object v0

    invoke-virtual {p1}, Labn;->d()F

    move-result v1

    .line 127
    invoke-static {v0, v1}, Lcom/twitter/model/moments/e;->a(Lcom/twitter/model/moments/e;F)Lcom/twitter/model/moments/d;

    move-result-object v0

    .line 129
    if-eqz v0, :cond_0

    .line 130
    iget-object v1, v0, Lcom/twitter/model/moments/d;->f:Lcom/twitter/util/math/Size;

    invoke-virtual {v0}, Lcom/twitter/model/moments/d;->a()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Labn;->a(Lcom/twitter/util/math/Size;Landroid/graphics/Rect;)V

    .line 134
    :goto_0
    return-void

    .line 132
    :cond_0
    invoke-interface {p2}, Lcom/twitter/android/moments/viewmodels/k;->b()Lcom/twitter/util/math/Size;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Labn;->a(Lcom/twitter/util/math/Size;Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method static synthetic c(Labo;)Labn;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Labo;->b:Labn;

    return-object v0
.end method

.method private d()Lrx/functions/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/b",
            "<-",
            "Lcom/twitter/media/request/ImageResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    new-instance v0, Labo$1;

    invoke-direct {v0, p0}, Labo$1;-><init>(Labo;)V

    return-object v0
.end method

.method private e()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<-",
            "Lcom/twitter/media/request/ImageResponse;",
            "+",
            "Lrx/g",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 111
    new-instance v0, Labo$2;

    invoke-direct {v0, p0}, Labo$2;-><init>(Labo;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/media/request/b$b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/media/request/b$b",
            "<",
            "Lcom/twitter/media/request/ImageResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 77
    iput-object p1, p0, Labo;->d:Lcom/twitter/media/request/b$b;

    .line 78
    return-void
.end method

.method public a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/core/Tweet;)V
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, Labo;->c:Lcom/twitter/android/moments/ui/maker/an;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/an;->a()Lrx/f;

    move-result-object v0

    .line 84
    iget-object v1, p0, Labo;->b:Labn;

    .line 85
    invoke-virtual {v1}, Labn;->c()Lrx/g;

    move-result-object v1

    .line 86
    invoke-direct {p0}, Labo;->d()Lrx/functions/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/g;->c(Lrx/functions/b;)Lrx/g;

    move-result-object v1

    .line 87
    invoke-virtual {v1, v0}, Lrx/g;->a(Lrx/f;)Lrx/g;

    move-result-object v1

    .line 88
    invoke-direct {p0}, Labo;->e()Lrx/functions/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/g;->a(Lrx/functions/d;)Lrx/g;

    move-result-object v1

    .line 89
    invoke-virtual {v1, v0}, Lrx/g;->a(Lrx/f;)Lrx/g;

    move-result-object v0

    .line 90
    invoke-static {p0}, Lcre;->a(Ljava/lang/Object;)Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lrx/g;->c()Lrx/c;

    move-result-object v0

    iget-object v1, p0, Labo;->e:Lrx/subjects/a;

    .line 92
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/d;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Labo;->f:Lrx/j;

    .line 93
    iget-object v0, p0, Labo;->b:Labn;

    iget-object v1, p0, Labo;->a:Lcom/twitter/android/moments/viewmodels/k;

    invoke-interface {v1}, Lcom/twitter/android/moments/viewmodels/k;->a()Lcom/twitter/media/request/a$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Labn;->a(Lcom/twitter/media/request/a$a;)V

    .line 94
    iget-object v0, p0, Labo;->b:Labn;

    iget-object v1, p0, Labo;->a:Lcom/twitter/android/moments/viewmodels/k;

    invoke-direct {p0, v0, v1}, Labo;->b(Labn;Lcom/twitter/android/moments/viewmodels/k;)V

    .line 95
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Labo;->b:Labn;

    invoke-virtual {v0}, Labn;->a()Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method public b()Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Lacg",
            "<",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 139
    iget-object v0, p0, Labo;->e:Lrx/subjects/a;

    invoke-virtual {v0}, Lrx/subjects/a;->b()Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Labo;->f:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 145
    return-void
.end method
