.class public Lceu;
.super Lcfd;
.source "Twttr"

# interfaces
.implements Lcfb;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lceu$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/twitter/model/moments/r;

.field public final b:Lcom/twitter/model/moments/w;


# direct methods
.method public constructor <init>(Lcom/twitter/model/moments/r;Lcom/twitter/model/moments/w;)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Lcfd;-><init>()V

    .line 22
    iget-object v0, p1, Lcom/twitter/model/moments/r;->c:Lcom/twitter/model/moments/MomentPageDisplayMode;

    sget-object v1, Lcom/twitter/model/moments/MomentPageDisplayMode;->b:Lcom/twitter/model/moments/MomentPageDisplayMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 23
    iput-object p1, p0, Lceu;->a:Lcom/twitter/model/moments/r;

    .line 24
    iput-object p2, p0, Lceu;->b:Lcom/twitter/model/moments/w;

    .line 25
    return-void

    .line 22
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lcfa;
    .locals 3

    .prologue
    .line 30
    new-instance v1, Lcfa;

    new-instance v0, Lcez$a;

    invoke-direct {v0}, Lcez$a;-><init>()V

    iget-object v2, p0, Lceu;->a:Lcom/twitter/model/moments/r;

    .line 31
    invoke-virtual {v0, v2}, Lcez$a;->a(Lcom/twitter/model/moments/r;)Lcez$a;

    move-result-object v0

    iget-object v2, p0, Lceu;->b:Lcom/twitter/model/moments/w;

    .line 32
    invoke-virtual {v0, v2}, Lcez$a;->a(Lcom/twitter/model/moments/w;)Lcez$a;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lcez$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcez;

    invoke-direct {v1, v0}, Lcfa;-><init>(Lcez;)V

    .line 30
    return-object v1
.end method
