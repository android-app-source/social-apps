.class public Lakn;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lakn$a;,
        Lakn$b;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/accounts/AccountManager;

.field private final c:Ljava/lang/String;

.field private final d:Lakn$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lakn;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lakn;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/accounts/AccountManager;Ljava/lang/String;Lakn$b;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lakn;->b:Landroid/accounts/AccountManager;

    .line 37
    iput-object p2, p0, Lakn;->c:Ljava/lang/String;

    .line 38
    iput-object p3, p0, Lakn;->d:Lakn$b;

    .line 39
    return-void
.end method

.method private a(Lakm;)Lakm;
    .locals 6

    .prologue
    .line 163
    iget-object v0, p0, Lakn;->d:Lakn$b;

    if-eqz v0, :cond_0

    .line 164
    invoke-virtual {p1}, Lakm;->e()I

    move-result v0

    .line 165
    iget-object v1, p0, Lakn;->d:Lakn$b;

    invoke-interface {v1}, Lakn$b;->a()I

    move-result v1

    .line 166
    if-ge v0, v1, :cond_0

    .line 167
    iget-object v2, p0, Lakn;->b:Landroid/accounts/AccountManager;

    monitor-enter v2

    .line 168
    :try_start_0
    invoke-virtual {p1}, Lakm;->e()I

    move-result v3

    .line 169
    if-lt v3, v1, :cond_1

    .line 171
    monitor-exit v2

    .line 181
    :cond_0
    :goto_0
    return-object p1

    .line 173
    :cond_1
    iget-object v3, p0, Lakn;->d:Lakn$b;

    invoke-interface {v3, p1, v0, v1}, Lakn$b;->a(Lakm;II)V

    .line 174
    invoke-virtual {p1}, Lakm;->e()I

    move-result v3

    .line 175
    if-ne v3, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Expected AppAccount version "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, " after upgrade, but instead got version "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 178
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 175
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Lcnz;)Lakm;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 186
    new-instance v3, Landroid/accounts/Account;

    iget-object v1, p0, Lakn;->c:Ljava/lang/String;

    invoke-direct {v3, p1, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    new-instance v1, Lakm;

    iget-object v2, p0, Lakn;->b:Landroid/accounts/AccountManager;

    invoke-direct {v1, v2, v3}, Lakm;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;)V

    .line 188
    iget-object v2, p0, Lakn;->b:Landroid/accounts/AccountManager;

    invoke-virtual {v2, v3, v0, v0}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v2

    .line 193
    if-nez v2, :cond_0

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x16

    if-lt v4, v5, :cond_0

    iget-object v4, p0, Lakn;->b:Landroid/accounts/AccountManager;

    iget-object v5, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    .line 197
    invoke-virtual {v4, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v4

    array-length v4, v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lakn;->b:Landroid/accounts/AccountManager;

    .line 198
    invoke-virtual {v4, v3}, Landroid/accounts/AccountManager;->removeAccountExplicitly(Landroid/accounts/Account;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 199
    sget-object v2, Lakn;->a:Ljava/lang/String;

    const-string/jumbo v4, "hit Android N bug, trying to add account again"

    const-string/jumbo v5, "ANDROID-19374"

    invoke-static {v2, v4, v5}, Lcqi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    iget-object v2, p0, Lakn;->b:Landroid/accounts/AccountManager;

    invoke-virtual {v2, v3, v0, v0}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v2

    .line 201
    if-nez v2, :cond_0

    .line 202
    sget-object v3, Lakn;->a:Ljava/lang/String;

    const-string/jumbo v4, "hit Android N bug, failed again, could not add account"

    const-string/jumbo v5, "ANDROID-19374"

    invoke-static {v3, v4, v5}, Lcqi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :cond_0
    if-nez v2, :cond_1

    .line 215
    :goto_0
    return-object v0

    .line 209
    :cond_1
    invoke-virtual {v1, p2}, Lakm;->a(Lcnz;)V

    .line 211
    invoke-virtual {v1, v0}, Lakm;->b(Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lakn;->d:Lakn$b;

    if-eqz v0, :cond_2

    .line 213
    iget-object v0, p0, Lakn;->d:Lakn$b;

    invoke-interface {v0}, Lakn$b;->a()I

    move-result v0

    invoke-virtual {v1, v0}, Lakm;->a(I)V

    :cond_2
    move-object v0, v1

    .line 215
    goto :goto_0
.end method

.method public static a()Lakn;
    .locals 1

    .prologue
    .line 47
    invoke-static {}, Lalc;->d()Lalc;

    move-result-object v0

    invoke-virtual {v0}, Lalc;->l()Lakn;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lakm;Ljava/lang/String;Lakn$a;)Lakm;
    .locals 2

    .prologue
    .line 148
    iget-object v1, p0, Lakn;->b:Landroid/accounts/AccountManager;

    monitor-enter v1

    .line 149
    :try_start_0
    invoke-virtual {p1}, Lakm;->b()Lcnz;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lakn;->a(Ljava/lang/String;Lcnz;)Lakm;

    move-result-object v0

    .line 150
    if-eqz v0, :cond_1

    .line 151
    if-eqz p3, :cond_0

    .line 152
    invoke-interface {p3, p1, v0}, Lakn$a;->a(Lakm;Lakm;)V

    .line 154
    :cond_0
    invoke-virtual {p1}, Lakm;->c()Landroid/accounts/AccountManagerFuture;

    .line 155
    monitor-exit v1

    .line 158
    :goto_0
    return-object v0

    .line 157
    :cond_1
    monitor-exit v1

    .line 158
    const/4 v0, 0x0

    goto :goto_0

    .line 157
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Landroid/accounts/Account;)Lakm;
    .locals 2

    .prologue
    .line 73
    new-instance v0, Lakm;

    iget-object v1, p0, Lakn;->b:Landroid/accounts/AccountManager;

    invoke-direct {v0, v1, p1}, Lakm;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;)V

    invoke-direct {p0, v0}, Lakn;->a(Lakm;)Lakm;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcnz;)Lakm;
    .locals 5

    .prologue
    .line 59
    iget-object v0, p0, Lakn;->b:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lakn;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 60
    invoke-virtual {p0, v0}, Lakn;->a(Landroid/accounts/Account;)Lakm;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lakm;->b()Lcnz;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcnz;->a(Lcnz;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 65
    :goto_1
    return-object v0

    .line 59
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 65
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Lcnz;Ljava/lang/String;)Lakm;
    .locals 2

    .prologue
    .line 107
    invoke-virtual {p0, p1}, Lakn;->a(Lcnz;)Lakm;

    move-result-object v0

    .line 108
    if-nez v0, :cond_0

    .line 109
    iget-object v1, p0, Lakn;->b:Landroid/accounts/AccountManager;

    monitor-enter v1

    .line 110
    :try_start_0
    invoke-virtual {p0, p1}, Lakn;->a(Lcnz;)Lakm;

    move-result-object v0

    .line 111
    if-nez v0, :cond_1

    .line 112
    invoke-direct {p0, p2, p1}, Lakn;->a(Ljava/lang/String;Lcnz;)Lakm;

    move-result-object v0

    monitor-exit v1

    .line 117
    :cond_0
    :goto_0
    return-object v0

    .line 114
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lakn;->b:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lakn;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    return-void
.end method

.method public b()I
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lakn;->b:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lakn;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public c()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lakm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lakn;->b:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lakn;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 88
    array-length v0, v1

    new-array v2, v0, [Lakm;

    .line 89
    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 90
    aget-object v3, v1, v0

    invoke-virtual {p0, v3}, Lakn;->a(Landroid/accounts/Account;)Lakm;

    move-result-object v3

    aput-object v3, v2, v0

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 92
    :cond_0
    invoke-static {v2}, Lcom/twitter/util/collection/ImmutableList;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
