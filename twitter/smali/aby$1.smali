.class Laby$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Laby;->a(I)Lrx/functions/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/d",
        "<",
        "Landroid/support/v7/graphics/Palette;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Laby;


# direct methods
.method constructor <init>(Laby;I)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Laby$1;->b:Laby;

    iput p2, p0, Laby$1;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v7/graphics/Palette;)Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 32
    iget v0, p0, Laby$1;->a:I

    invoke-virtual {p1, v0}, Landroid/support/v7/graphics/Palette;->getDarkMutedColor(I)I

    move-result v0

    const v1, 0x3e333333    # 0.175f

    invoke-static {v0, v1}, Lcom/twitter/util/ui/g;->a(IF)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    check-cast p1, Landroid/support/v7/graphics/Palette;

    invoke-virtual {p0, p1}, Laby$1;->a(Landroid/support/v7/graphics/Palette;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
