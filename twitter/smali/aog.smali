.class public abstract Laog;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;
.implements Laof;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Laoe;",
        "Laof",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lani;

.field private b:Landroid/view/View;

.field private c:Z

.field private d:Z

.field private e:Z


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Lani;

    invoke-direct {v0}, Lani;-><init>()V

    iput-object v0, p0, Laog;->a:Lani;

    .line 35
    return-void
.end method

.method protected constructor <init>(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Lani;

    invoke-direct {v0}, Lani;-><init>()V

    iput-object v0, p0, Laog;->a:Lani;

    .line 38
    return-void
.end method


# virtual methods
.method public a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Laog;->a:Lani;

    invoke-virtual {v0, p1}, Lani;->a(Landroid/content/res/Configuration;)V

    .line 103
    return-void
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Laog;->a:Lani;

    invoke-virtual {v0, p1}, Lani;->a(Landroid/os/Bundle;)V

    .line 128
    return-void
.end method

.method protected final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Laog;->b:Landroid/view/View;

    .line 58
    return-void
.end method

.method public aB_()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Laog;->c:Z

    .line 76
    iget-object v0, p0, Laog;->a:Lani;

    invoke-virtual {v0}, Lani;->a()V

    .line 77
    return-void
.end method

.method public final aN_()Landroid/view/View;
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Laog;->b:Landroid/view/View;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Content view has not been set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    iget-object v0, p0, Laog;->b:Landroid/view/View;

    return-object v0
.end method

.method public aO_()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Laog;->d:Z

    .line 83
    iget-object v0, p0, Laog;->a:Lani;

    invoke-virtual {v0}, Lani;->b()V

    .line 84
    return-void
.end method

.method public ak_()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Laog;->d:Z

    .line 90
    iget-object v0, p0, Laog;->a:Lani;

    invoke-virtual {v0}, Lani;->c()V

    .line 91
    return-void
.end method

.method public al_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    const-string/jumbo v0, "ViewHost"

    return-object v0
.end method

.method public am_()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 136
    const/4 v0, 0x1

    iput-boolean v0, p0, Laog;->e:Z

    .line 137
    iget-object v0, p0, Laog;->a:Lani;

    invoke-virtual {v0}, Lani;->e()V

    .line 138
    return-void
.end method

.method public synthetic c()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Laog;->p()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Laog;->c:Z

    .line 97
    iget-object v0, p0, Laog;->a:Lani;

    invoke-virtual {v0}, Lani;->d()V

    .line 98
    return-void
.end method

.method public o()Lanh;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Laog;->a:Lani;

    return-object v0
.end method

.method public final p()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 117
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 118
    invoke-virtual {p0, v0}, Laog;->a(Landroid/os/Bundle;)V

    .line 119
    return-object v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 151
    iget-boolean v0, p0, Laog;->d:Z

    return v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 158
    iget-boolean v0, p0, Laog;->e:Z

    return v0
.end method
