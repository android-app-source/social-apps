.class public Lckw;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lckw$a;
    }
.end annotation


# instance fields
.field private a:Lckw$a;

.field private final b:I

.field private final c:Landroid/view/WindowManager;

.field private d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/WindowManager;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lckw;->d:Ljava/lang/ref/WeakReference;

    .line 28
    invoke-static {p1}, Lcom/twitter/util/ui/k;->e(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lckw;->b:I

    .line 29
    iput-object p2, p0, Lckw;->c:Landroid/view/WindowManager;

    .line 30
    return-void
.end method

.method private b()Landroid/graphics/Rect;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 73
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 74
    iget-object v0, p0, Lckw;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 76
    if-nez v0, :cond_0

    .line 77
    iget-object v0, p0, Lckw;->c:Landroid/view/WindowManager;

    invoke-static {v0}, Lcom/twitter/util/ui/k;->a(Landroid/view/WindowManager;)Landroid/graphics/Point;

    move-result-object v0

    .line 78
    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v3, v3, v2, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 83
    :goto_0
    return-object v1

    .line 80
    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/graphics/Rect;
    .locals 3

    .prologue
    .line 38
    invoke-direct {p0}, Lckw;->b()Landroid/graphics/Rect;

    move-result-object v0

    .line 40
    const/4 v1, 0x0

    iget v2, p0, Lckw;->b:I

    neg-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 41
    return-object v0
.end method

.method public a(Lckw$a;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lckw;->a:Lckw$a;

    .line 34
    return-void
.end method

.method public onGlobalLayout()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lckw;->a:Lckw$a;

    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {p0}, Lckw;->a()Landroid/graphics/Rect;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lckw;->a:Lckw$a;

    invoke-interface {v1, v0}, Lckw$a;->b(Landroid/graphics/Rect;)V

    .line 92
    :cond_0
    return-void
.end method
