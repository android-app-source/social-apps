.class Lafj$1;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lafj;->b(Lcom/twitter/library/client/Session;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lafj;


# direct methods
.method constructor <init>(Lafj;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lafj$1;->a:Lafj;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 73
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lafj$1;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 6

    .prologue
    .line 76
    check-cast p1, Lbhn;

    .line 77
    invoke-virtual {p1}, Lbhn;->b()J

    move-result-wide v0

    iget-object v2, p0, Lafj$1;->a:Lafj;

    invoke-static {v2}, Lafj;->a(Lafj;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 83
    :cond_0
    return-void

    .line 80
    :cond_1
    invoke-virtual {p1}, Lbhn;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 81
    iget-object v2, p0, Lafj$1;->a:Lafj;

    invoke-static {v2}, Lafj;->b(Lafj;)Lcom/twitter/model/util/FriendshipCache;

    move-result-object v2

    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v2, v4, v5}, Lcom/twitter/model/util/FriendshipCache;->b(J)V

    goto :goto_0
.end method
