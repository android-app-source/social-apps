.class public Lcjp;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcjt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Item:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcjt",
        "<TItem;>;"
    }
.end annotation


# instance fields
.field private a:Lcbi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcbi",
            "<TItem;>;"
        }
    .end annotation
.end field

.field private b:Lcjs;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcjp;->a:Lcbi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcjp;->a:Lcbi;

    invoke-virtual {v0}, Lcbi;->be_()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcbi;)Lcbi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<TItem;>;)",
            "Lcbi",
            "<TItem;>;"
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcjp;->a:Lcbi;

    .line 51
    iput-object p1, p0, Lcjp;->a:Lcbi;

    .line 52
    invoke-static {v0, p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 53
    iget-object v1, p0, Lcjp;->b:Lcjs;

    if-eqz v1, :cond_0

    .line 54
    iget-object v1, p0, Lcjp;->b:Lcjs;

    invoke-interface {v1}, Lcjs;->a()V

    .line 57
    :cond_0
    return-object v0
.end method

.method public a(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TItem;"
        }
    .end annotation

    .prologue
    .line 23
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcjp;->a()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 24
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "cannot get an item from an invalid position"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 27
    :cond_1
    iget-object v0, p0, Lcjp;->a:Lcbi;

    invoke-virtual {v0, p1}, Lcbi;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcjs;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcjp;->b:Lcjs;

    .line 38
    return-void
.end method

.method public b(I)J
    .locals 2

    .prologue
    .line 32
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcjp;->a:Lcbi;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Lcbi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcbi",
            "<TItem;>;"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcjp;->a:Lcbi;

    if-nez v0, :cond_0

    .line 75
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The items are null, call isInitialized first to check."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_0
    iget-object v0, p0, Lcjp;->a:Lcbi;

    return-object v0
.end method
