.class public Laje;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lajj;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laje$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/support/v4/app/FragmentActivity;

.field private final b:Lcom/twitter/model/core/Tweet;

.field private final c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final d:J


# direct methods
.method public constructor <init>(Laje$a;)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {p1}, Laje$a;->a(Laje$a;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Laje;->a:Landroid/support/v4/app/FragmentActivity;

    .line 23
    invoke-static {p1}, Laje$a;->b(Laje$a;)J

    move-result-wide v0

    iput-wide v0, p0, Laje;->d:J

    .line 24
    invoke-static {p1}, Laje$a;->c(Laje$a;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    iput-object v0, p0, Laje;->b:Lcom/twitter/model/core/Tweet;

    .line 25
    invoke-static {p1}, Laje$a;->d(Laje$a;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Laje;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 26
    return-void
.end method


# virtual methods
.method public a()V
    .locals 7

    .prologue
    .line 30
    iget-object v0, p0, Laje;->a:Landroid/support/v4/app/FragmentActivity;

    new-instance v1, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$a;

    iget-wide v2, p0, Laje;->d:J

    iget-object v4, p0, Laje;->b:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v4, Lcom/twitter/model/core/Tweet;->t:J

    iget-object v6, p0, Laje;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct/range {v1 .. v6}, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$a;-><init>(JJLcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    iget-object v2, p0, Laje;->a:Landroid/support/v4/app/FragmentActivity;

    const-class v3, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity;

    .line 31
    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$a;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    const v2, 0xffff

    .line 30
    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 33
    return-void
.end method
