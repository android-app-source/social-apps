.class public Lavs;
.super Lcom/twitter/database/hydrator/a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/database/hydrator/a",
        "<",
        "Lcom/twitter/model/drafts/a;",
        "Lcom/twitter/database/schema/DraftsSchema$c$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/twitter/database/hydrator/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/drafts/a;Lcom/twitter/database/schema/DraftsSchema$c$a;)Lcom/twitter/database/schema/DraftsSchema$c$a;
    .locals 4

    .prologue
    .line 17
    iget-object v0, p1, Lcom/twitter/model/drafts/a;->c:Ljava/lang/String;

    .line 18
    invoke-interface {p2, v0}, Lcom/twitter/database/schema/DraftsSchema$c$a;->a(Ljava/lang/String;)Lcom/twitter/database/schema/DraftsSchema$c$a;

    move-result-object v0

    .line 19
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/twitter/database/schema/DraftsSchema$c$a;->b(J)Lcom/twitter/database/schema/DraftsSchema$c$a;

    move-result-object v1

    iget-object v0, p1, Lcom/twitter/model/drafts/a;->d:Ljava/util/List;

    .line 21
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v0}, Lcom/twitter/database/schema/DraftsSchema$c$a;->a(Ljava/util/List;)Lcom/twitter/database/schema/DraftsSchema$c$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->h:Lcgi;

    .line 22
    invoke-interface {v0, v1}, Lcom/twitter/database/schema/DraftsSchema$c$a;->a(Lcgi;)Lcom/twitter/database/schema/DraftsSchema$c$a;

    move-result-object v0

    .line 23
    invoke-virtual {p1}, Lcom/twitter/model/drafts/a;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/database/schema/DraftsSchema$c$a;->b(Ljava/util/List;)Lcom/twitter/database/schema/DraftsSchema$c$a;

    move-result-object v0

    .line 24
    invoke-virtual {p1}, Lcom/twitter/model/drafts/a;->a()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/twitter/database/schema/DraftsSchema$c$a;->c(J)Lcom/twitter/database/schema/DraftsSchema$c$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->g:Lcom/twitter/model/geo/c;

    .line 25
    invoke-interface {v0, v1}, Lcom/twitter/database/schema/DraftsSchema$c$a;->a(Lcom/twitter/model/geo/c;)Lcom/twitter/database/schema/DraftsSchema$c$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->j:Ljava/lang/String;

    .line 26
    invoke-interface {v0, v1}, Lcom/twitter/database/schema/DraftsSchema$c$a;->b(Ljava/lang/String;)Lcom/twitter/database/schema/DraftsSchema$c$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->k:Lcau;

    .line 27
    invoke-interface {v0, v1}, Lcom/twitter/database/schema/DraftsSchema$c$a;->a(Lcau;)Lcom/twitter/database/schema/DraftsSchema$c$a;

    move-result-object v0

    iget-boolean v1, p1, Lcom/twitter/model/drafts/a;->f:Z

    .line 28
    invoke-interface {v0, v1}, Lcom/twitter/database/schema/DraftsSchema$c$a;->a(Z)Lcom/twitter/database/schema/DraftsSchema$c$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->l:Ljava/util/List;

    .line 29
    invoke-interface {v0, v1}, Lcom/twitter/database/schema/DraftsSchema$c$a;->c(Ljava/util/List;)Lcom/twitter/database/schema/DraftsSchema$c$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->m:Lcom/twitter/model/timeline/ap;

    .line 30
    invoke-interface {v0, v1}, Lcom/twitter/database/schema/DraftsSchema$c$a;->a(Lcom/twitter/model/timeline/ap;)Lcom/twitter/database/schema/DraftsSchema$c$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->n:Ljava/lang/String;

    .line 31
    invoke-interface {v0, v1}, Lcom/twitter/database/schema/DraftsSchema$c$a;->c(Ljava/lang/String;)Lcom/twitter/database/schema/DraftsSchema$c$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->i:Lcom/twitter/model/core/r;

    .line 32
    invoke-interface {v0, v1}, Lcom/twitter/database/schema/DraftsSchema$c$a;->a(Lcom/twitter/model/core/r;)Lcom/twitter/database/schema/DraftsSchema$c$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->o:Ljava/util/List;

    .line 33
    invoke-interface {v0, v1}, Lcom/twitter/database/schema/DraftsSchema$c$a;->d(Ljava/util/List;)Lcom/twitter/database/schema/DraftsSchema$c$a;

    move-result-object v0

    iget-wide v2, p1, Lcom/twitter/model/drafts/a;->p:J

    .line 34
    invoke-interface {v0, v2, v3}, Lcom/twitter/database/schema/DraftsSchema$c$a;->d(J)Lcom/twitter/database/schema/DraftsSchema$c$a;

    move-result-object v0

    iget-boolean v1, p1, Lcom/twitter/model/drafts/a;->q:Z

    .line 35
    invoke-interface {v0, v1}, Lcom/twitter/database/schema/DraftsSchema$c$a;->b(Z)Lcom/twitter/database/schema/DraftsSchema$c$a;

    .line 36
    return-object p2

    .line 21
    :cond_0
    iget-object v0, p1, Lcom/twitter/model/drafts/a;->d:Ljava/util/List;

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    check-cast p1, Lcom/twitter/model/drafts/a;

    check-cast p2, Lcom/twitter/database/schema/DraftsSchema$c$a;

    invoke-virtual {p0, p1, p2}, Lavs;->a(Lcom/twitter/model/drafts/a;Lcom/twitter/database/schema/DraftsSchema$c$a;)Lcom/twitter/database/schema/DraftsSchema$c$a;

    move-result-object v0

    return-object v0
.end method
