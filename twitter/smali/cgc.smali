.class public Lcgc;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcgc$b;,
        Lcgc$a;
    }
.end annotation


# static fields
.field public static final a:Lcgc$b;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/Boolean;

.field public final i:Ljava/lang/Integer;

.field public final j:Ljava/lang/Integer;

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public final m:Ljava/lang/String;

.field public final n:Ljava/lang/String;

.field public final o:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcgc$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcgc$b;-><init>(Lcgc$1;)V

    sput-object v0, Lcgc;->a:Lcgc$b;

    return-void
.end method

.method private constructor <init>(Lcgc$a;)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    invoke-static {p1}, Lcgc$a;->a(Lcgc$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcgc;->b:Ljava/lang/String;

    .line 70
    invoke-static {p1}, Lcgc$a;->b(Lcgc$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcgc;->c:Ljava/lang/String;

    .line 71
    invoke-static {p1}, Lcgc$a;->c(Lcgc$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcgc;->d:Ljava/lang/String;

    .line 72
    invoke-static {p1}, Lcgc$a;->d(Lcgc$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcgc;->e:Ljava/lang/String;

    .line 73
    invoke-static {p1}, Lcgc$a;->e(Lcgc$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcgc;->f:Ljava/lang/String;

    .line 74
    invoke-static {p1}, Lcgc$a;->f(Lcgc$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcgc;->g:Ljava/lang/String;

    .line 75
    invoke-static {p1}, Lcgc$a;->g(Lcgc$a;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcgc;->h:Ljava/lang/Boolean;

    .line 76
    invoke-static {p1}, Lcgc$a;->h(Lcgc$a;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcgc;->i:Ljava/lang/Integer;

    .line 77
    invoke-static {p1}, Lcgc$a;->i(Lcgc$a;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcgc;->j:Ljava/lang/Integer;

    .line 78
    invoke-static {p1}, Lcgc$a;->j(Lcgc$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcgc;->k:Ljava/lang/String;

    .line 79
    invoke-static {p1}, Lcgc$a;->k(Lcgc$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcgc;->l:Ljava/lang/String;

    .line 80
    invoke-static {p1}, Lcgc$a;->l(Lcgc$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcgc;->m:Ljava/lang/String;

    .line 81
    invoke-static {p1}, Lcgc$a;->m(Lcgc$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcgc;->n:Ljava/lang/String;

    .line 82
    invoke-static {p1}, Lcgc$a;->n(Lcgc$a;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcgc;->o:Ljava/lang/Boolean;

    .line 83
    return-void
.end method

.method synthetic constructor <init>(Lcgc$a;Lcgc$1;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcgc;-><init>(Lcgc$a;)V

    return-void
.end method
