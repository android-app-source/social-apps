.class final Lcgb$f;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcgb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcgb;",
        "Lcgb$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 429
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcgb$1;)V
    .locals 0

    .prologue
    .line 429
    invoke-direct {p0}, Lcgb$f;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcgb$a;
    .locals 1

    .prologue
    .line 433
    new-instance v0, Lcgb$a;

    invoke-direct {v0}, Lcgb$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcgb$a;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 446
    sget-object v0, Lcgb$d;->a:Lcgb$d$b;

    .line 447
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgb$d;

    .line 446
    invoke-virtual {p2, v0}, Lcgb$a;->a(Lcgb$d;)Lcgb$a;

    .line 448
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 429
    check-cast p2, Lcgb$a;

    invoke-virtual {p0, p1, p2, p3}, Lcgb$f;->a(Lcom/twitter/util/serialization/n;Lcgb$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcgb;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 439
    iget-object v0, p2, Lcgb;->b:Lcgb$d;

    sget-object v1, Lcgb$d;->a:Lcgb$d$b;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 441
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 429
    check-cast p2, Lcgb;

    invoke-virtual {p0, p1, p2}, Lcgb$f;->a(Lcom/twitter/util/serialization/o;Lcgb;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 429
    invoke-virtual {p0}, Lcgb$f;->a()Lcgb$a;

    move-result-object v0

    return-object v0
.end method
