.class public Lbgt;
.super Lbgv;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;ILbgp;Lbwb;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/service/v;",
            "I",
            "Lbgp;",
            "Lbwb",
            "<",
            "Lcom/mopub/nativeads/NativeAd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    const-class v0, Lbgt;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    iget-wide v4, p2, Lcom/twitter/library/service/v;->c:J

    const/16 v6, 0x11

    const/4 v9, 0x0

    sget-object v10, Lbgw;->a:Lbgw;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v7, p3

    move-object/from16 v8, p4

    move-object/from16 v11, p5

    invoke-direct/range {v0 .. v11}, Lbgv;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Ljava/lang/String;JIILbgp;Ljava/lang/String;Lbgw;Lbwb;)V

    .line 60
    invoke-direct {p0, p1}, Lbgt;->a(Landroid/content/Context;)V

    .line 61
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 136
    new-instance v0, Lcom/twitter/library/service/f;

    invoke-direct {v0}, Lcom/twitter/library/service/f;-><init>()V

    .line 137
    new-instance v1, Lcom/twitter/library/service/n;

    invoke-direct {v1}, Lcom/twitter/library/service/n;-><init>()V

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/service/l;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lcom/twitter/library/service/l;-><init>(I)V

    .line 138
    invoke-virtual {v1, v2}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/service/g;

    invoke-direct {v2, p1}, Lcom/twitter/library/service/g;-><init>(Landroid/content/Context;)V

    .line 139
    invoke-virtual {v1, v2}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    .line 141
    const/16 v1, 0x7530

    invoke-virtual {p0, v1}, Lbgt;->f(I)V

    .line 142
    invoke-virtual {p0, v0}, Lbgt;->a(Lcom/twitter/async/service/k;)Lcom/twitter/async/service/AsyncOperation;

    .line 143
    return-void
.end method

.method private a(Lcom/twitter/async/service/j;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 146
    if-eqz p1, :cond_0

    .line 147
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 148
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->f()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 149
    invoke-virtual {p0}, Lbgt;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    if-nez v0, :cond_1

    .line 162
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    const-string/jumbo v0, "timeline_request_scribe_sample"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lbgt;->p:Landroid/content/Context;

    const-string/jumbo v1, "app:twitter_service:timeline:request"

    .line 156
    invoke-virtual {p0}, Lbgt;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/service/v;->c:J

    .line 157
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/service/u;

    invoke-virtual {v4}, Lcom/twitter/library/service/u;->b()Z

    move-result v4

    const/4 v7, 0x0

    move-object v5, p1

    move v6, p2

    .line 153
    invoke-static/range {v0 .. v7}, Lcom/twitter/library/api/a;->a(Landroid/content/Context;Ljava/lang/String;JZLcom/twitter/async/service/j;ZLjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected a(Lchd;)V
    .locals 8

    .prologue
    .line 110
    invoke-super {p0, p1}, Lbgv;->a(Lchd;)V

    .line 111
    invoke-virtual {p0}, Lbgt;->M()Lcom/twitter/library/service/v;

    move-result-object v7

    .line 112
    invoke-virtual {p1}, Lchd;->a()I

    move-result v0

    if-lez v0, :cond_1

    if-eqz v7, :cond_1

    .line 115
    invoke-static {}, Lcom/twitter/library/provider/j;->c()Lcom/twitter/library/provider/j;

    move-result-object v1

    .line 116
    iget-wide v2, v7, Lcom/twitter/library/service/v;->c:J

    const-string/jumbo v0, "tweet"

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/library/provider/j;->a(JLjava/lang/String;)I

    move-result v0

    .line 117
    if-nez v0, :cond_0

    .line 118
    invoke-virtual {p0}, Lbgt;->S()Laut;

    move-result-object v6

    .line 119
    iget-wide v2, v7, Lcom/twitter/library/service/v;->c:J

    const-string/jumbo v4, "tweet"

    const/4 v5, 0x1

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/j;->a(JLjava/lang/String;ILaut;)I

    .line 120
    invoke-virtual {v6}, Laut;->a()V

    .line 124
    :cond_0
    new-instance v0, Lben;

    iget-object v1, p0, Lbgt;->p:Landroid/content/Context;

    iget-wide v2, v7, Lcom/twitter/library/service/v;->c:J

    iget-object v4, v7, Lcom/twitter/library/service/v;->e:Ljava/lang/String;

    iget-object v5, v7, Lcom/twitter/library/service/v;->d:Lcom/twitter/model/account/OAuthToken;

    invoke-direct/range {v0 .. v5}, Lben;-><init>(Landroid/content/Context;JLjava/lang/String;Lcom/twitter/model/account/OAuthToken;)V

    const-string/jumbo v1, "Retrying logging promoted event does not occur because of user interaction."

    .line 126
    invoke-virtual {v0, v1}, Lben;->l(Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v0

    .line 124
    invoke-virtual {p0, v0}, Lbgt;->b(Lcom/twitter/async/service/AsyncOperation;)V

    .line 128
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/async/service/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 104
    invoke-super {p0, p1}, Lbgv;->a(Lcom/twitter/async/service/j;)V

    .line 105
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lbgt;->a(Lcom/twitter/async/service/j;Z)V

    .line 106
    return-void
.end method

.method public b(Lcom/twitter/async/service/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 92
    invoke-super {p0, p1}, Lbgv;->b(Lcom/twitter/async/service/j;)V

    .line 93
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lbgt;->a(Lcom/twitter/async/service/j;Z)V

    .line 94
    return-void
.end method

.method public ba_()Z
    .locals 2

    .prologue
    .line 75
    invoke-virtual {p0}, Lbgt;->x()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected e()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 68
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "timeline"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "home"

    aput-object v2, v0, v1

    return-object v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method
