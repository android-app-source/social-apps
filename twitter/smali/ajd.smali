.class public Lajd;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lajc;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lajc;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lajd;->a:Landroid/content/Context;

    .line 43
    iput-object p2, p0, Lajd;->b:Lajc;

    .line 44
    return-void
.end method

.method public static a(I)I
    .locals 3

    .prologue
    .line 204
    packed-switch p0, :pswitch_data_0

    .line 216
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :pswitch_0
    const/4 v0, 0x0

    .line 213
    :goto_0
    return v0

    .line 210
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 213
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 204
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(ILbgp;Lbgp;Lbgp;)Lbgp;
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x2

    if-ne p0, v0, :cond_0

    .line 199
    :goto_0
    return-object p1

    .line 194
    :cond_0
    const/4 v0, 0x3

    if-ne p0, v0, :cond_1

    move-object p1, p2

    .line 195
    goto :goto_0

    .line 196
    :cond_1
    const/4 v0, 0x4

    if-ne p0, v0, :cond_2

    move-object p1, p3

    .line 197
    goto :goto_0

    .line 199
    :cond_2
    sget-object p1, Lbgk;->a:Lbgk;

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/twitter/library/service/b;
    .locals 23

    .prologue
    .line 50
    move-object/from16 v0, p0

    iget-object v2, v0, Lajd;->b:Lajc;

    iget v2, v2, Lajc;->a:I

    .line 51
    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget v15, v3, Lajc;->c:I

    .line 52
    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget-wide v10, v3, Lajc;->h:J

    .line 53
    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget-wide v0, v3, Lajc;->f:J

    move-wide/from16 v16, v0

    .line 54
    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget-wide v0, v3, Lajc;->g:J

    move-wide/from16 v18, v0

    .line 55
    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget-boolean v12, v3, Lajc;->d:Z

    .line 57
    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget-object v0, v3, Lajc;->i:Ljava/lang/String;

    move-object/from16 v20, v0

    .line 59
    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget-object v4, v3, Lajc;->j:Ljava/lang/String;

    .line 61
    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget-object v0, v3, Lajc;->k:Lcom/twitter/library/client/Session;

    move-object/from16 v21, v0

    .line 63
    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget v5, v3, Lajc;->l:I

    .line 65
    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget-object v3, v3, Lajc;->n:Lcom/twitter/model/timeline/p;

    .line 68
    invoke-static {}, Lcom/twitter/android/av/m;->a()I

    move-result v22

    .line 70
    packed-switch v2, :pswitch_data_0

    .line 180
    :pswitch_0
    const/4 v2, 0x0

    :goto_0
    return-object v2

    .line 72
    :pswitch_1
    invoke-virtual/range {v21 .. v21}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    if-nez v2, :cond_0

    .line 73
    const/4 v2, 0x0

    goto :goto_0

    .line 75
    :cond_0
    new-instance v2, Lbfr;

    move-object/from16 v0, p0

    iget-object v4, v0, Lajd;->a:Landroid/content/Context;

    invoke-virtual/range {v21 .. v21}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v6

    move-object/from16 v0, v21

    invoke-direct {v2, v4, v0, v6, v5}, Lbfr;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/TwitterUser;I)V

    .line 76
    invoke-virtual {v2, v3}, Lbfr;->a(Lcom/twitter/model/timeline/p;)Lbfy;

    move-result-object v2

    check-cast v2, Lbfr;

    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget-object v3, v3, Lajc;->m:Lcom/twitter/android/revenue/c;

    .line 77
    invoke-virtual {v2, v3}, Lbfr;->a(Lcom/twitter/android/revenue/c;)Lbfr;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget-object v3, v3, Lajc;->s:Lbwb;

    .line 78
    invoke-virtual {v2, v3}, Lbfr;->a(Lbwb;)Lbfr;

    move-result-object v2

    .line 79
    move-wide/from16 v0, v18

    invoke-virtual {v2, v0, v1}, Lbfr;->a(J)Lbfy;

    move-result-object v2

    .line 80
    invoke-virtual {v2, v15}, Lbfy;->c(I)Lbge;

    move-result-object v2

    .line 81
    move/from16 v0, v22

    invoke-virtual {v2, v0}, Lbge;->e(I)Lbge;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget-object v3, v3, Lajc;->r:Ljava/lang/String;

    .line 82
    invoke-virtual {v2, v3}, Lbge;->b(Ljava/lang/String;)Lbge;

    move-result-object v2

    const-string/jumbo v3, "scribe_event"

    .line 83
    move-object/from16 v0, v20

    invoke-virtual {v2, v3, v0}, Lbge;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v2

    check-cast v2, Lbfr;

    .line 84
    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget v3, v3, Lajc;->b:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    const-string/jumbo v3, "ptr"

    :goto_1
    invoke-virtual {v2, v3}, Lbfr;->c(Ljava/lang/String;)Lbge;

    goto :goto_0

    :cond_1
    const-string/jumbo v3, ""

    goto :goto_1

    .line 90
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lajd;->b:Lajc;

    iget v2, v2, Lajc;->l:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget-object v3, v3, Lajc;->o:Lbgp;

    move-object/from16 v0, p0

    iget-object v4, v0, Lajd;->b:Lajc;

    iget-object v4, v4, Lajc;->p:Lbgp;

    move-object/from16 v0, p0

    iget-object v6, v0, Lajd;->b:Lajc;

    iget-object v6, v6, Lajc;->q:Lbgp;

    invoke-static {v2, v3, v4, v6}, Lajd;->a(ILbgp;Lbgp;Lbgp;)Lbgp;

    move-result-object v6

    .line 93
    new-instance v2, Lbgt;

    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->a:Landroid/content/Context;

    new-instance v4, Lcom/twitter/library/service/v;

    move-object/from16 v0, v21

    invoke-direct {v4, v0}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lajd;->b:Lajc;

    iget-object v7, v7, Lajc;->s:Lbwb;

    invoke-direct/range {v2 .. v7}, Lbgt;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;ILbgp;Lbwb;)V

    .line 96
    invoke-virtual {v2, v15}, Lbgt;->c(I)Lbge;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget-object v3, v3, Lajc;->r:Ljava/lang/String;

    .line 97
    invoke-virtual {v2, v3}, Lbge;->b(Ljava/lang/String;)Lbge;

    move-result-object v2

    .line 98
    move/from16 v0, v22

    invoke-virtual {v2, v0}, Lbge;->e(I)Lbge;

    move-result-object v2

    const-string/jumbo v3, "scribe_event"

    .line 99
    move-object/from16 v0, v20

    invoke-virtual {v2, v3, v0}, Lbge;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v2

    check-cast v2, Lbgt;

    .line 100
    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget v3, v3, Lajc;->c:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    const-string/jumbo v3, "ptr"

    :goto_2
    invoke-virtual {v2, v3}, Lbgt;->c(Ljava/lang/String;)Lbge;

    goto/16 :goto_0

    :cond_2
    const-string/jumbo v3, ""

    goto :goto_2

    .line 106
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lajd;->b:Lajc;

    iget v2, v2, Lajc;->l:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget-object v3, v3, Lajc;->o:Lbgp;

    move-object/from16 v0, p0

    iget-object v4, v0, Lajd;->b:Lajc;

    iget-object v4, v4, Lajc;->p:Lbgp;

    move-object/from16 v0, p0

    iget-object v6, v0, Lajd;->b:Lajc;

    iget-object v6, v6, Lajc;->q:Lbgp;

    invoke-static {v2, v3, v4, v6}, Lajd;->a(ILbgp;Lbgp;Lbgp;)Lbgp;

    move-result-object v13

    .line 109
    new-instance v7, Lbgq;

    move-object/from16 v0, p0

    iget-object v8, v0, Lajd;->a:Landroid/content/Context;

    new-instance v9, Lcom/twitter/library/service/v;

    move-object/from16 v0, v21

    invoke-direct {v9, v0}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    move v12, v5

    invoke-direct/range {v7 .. v13}, Lbgq;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;JILbgp;)V

    const/16 v2, 0x14

    .line 111
    invoke-virtual {v7, v2}, Lbgq;->c(I)Lbge;

    move-result-object v2

    const-string/jumbo v3, "scribe_event"

    .line 112
    move-object/from16 v0, v20

    invoke-virtual {v2, v3, v0}, Lbge;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/service/b;

    goto/16 :goto_0

    .line 115
    :pswitch_4
    new-instance v7, Lbfx;

    move-object/from16 v0, p0

    iget-object v8, v0, Lajd;->a:Landroid/content/Context;

    const/4 v14, 0x1

    move-object/from16 v9, v21

    move v13, v5

    invoke-direct/range {v7 .. v14}, Lbfx;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JZII)V

    .line 117
    invoke-virtual {v7, v3}, Lbfx;->a(Lcom/twitter/model/timeline/p;)Lbfy;

    move-result-object v2

    .line 118
    move/from16 v0, v22

    invoke-virtual {v2, v0}, Lbfy;->e(I)Lbge;

    move-result-object v2

    check-cast v2, Lbfx;

    .line 119
    move-wide/from16 v0, v18

    invoke-virtual {v2, v0, v1}, Lbfx;->a(J)Lbfy;

    move-result-object v2

    .line 120
    invoke-virtual {v2, v15}, Lbfy;->c(I)Lbge;

    move-result-object v2

    const-string/jumbo v3, "scribe_event"

    .line 121
    move-object/from16 v0, v20

    invoke-virtual {v2, v3, v0}, Lbge;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/service/b;

    goto/16 :goto_0

    .line 124
    :pswitch_5
    new-instance v7, Lbfx;

    move-object/from16 v0, p0

    iget-object v8, v0, Lajd;->a:Landroid/content/Context;

    const/16 v14, 0x12

    move-object/from16 v9, v21

    move v13, v5

    invoke-direct/range {v7 .. v14}, Lbfx;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JZII)V

    .line 126
    invoke-virtual {v7, v3}, Lbfx;->a(Lcom/twitter/model/timeline/p;)Lbfy;

    move-result-object v2

    .line 127
    move/from16 v0, v22

    invoke-virtual {v2, v0}, Lbfy;->e(I)Lbge;

    move-result-object v2

    check-cast v2, Lbfx;

    .line 128
    move-wide/from16 v0, v18

    invoke-virtual {v2, v0, v1}, Lbfx;->a(J)Lbfy;

    move-result-object v2

    .line 129
    invoke-virtual {v2, v15}, Lbfy;->c(I)Lbge;

    move-result-object v2

    const-string/jumbo v3, "scribe_event"

    .line 130
    move-object/from16 v0, v20

    invoke-virtual {v2, v3, v0}, Lbge;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/service/b;

    goto/16 :goto_0

    .line 133
    :pswitch_6
    new-instance v3, Lbfk;

    move-object/from16 v0, p0

    iget-object v5, v0, Lajd;->a:Landroid/content/Context;

    .line 134
    invoke-static {v4}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lajd;->b:Lajc;

    iget v4, v4, Lajc;->b:I

    invoke-static {v4}, Lajd;->a(I)I

    move-result v4

    move-object/from16 v0, v21

    invoke-direct {v3, v5, v0, v2, v4}, Lbfk;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;I)V

    .line 135
    const-string/jumbo v2, "scribe_event"

    move-object/from16 v0, v20

    invoke-virtual {v3, v2, v0}, Lbfk;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-object v2, v3

    .line 136
    goto/16 :goto_0

    .line 139
    :pswitch_7
    new-instance v7, Lbfs;

    move-object/from16 v0, p0

    iget-object v8, v0, Lajd;->a:Landroid/content/Context;

    .line 140
    invoke-static {v4}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    move-object/from16 v9, v21

    invoke-direct/range {v7 .. v12}, Lbfs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lajd;->b:Lajc;

    iget-wide v2, v2, Lajc;->e:J

    .line 141
    invoke-virtual {v7, v2, v3}, Lbfs;->c(J)Lbge;

    move-result-object v2

    .line 142
    move-wide/from16 v0, v16

    invoke-virtual {v2, v0, v1}, Lbge;->b(J)Lbge;

    move-result-object v2

    .line 143
    invoke-virtual {v2, v15}, Lbge;->c(I)Lbge;

    move-result-object v2

    goto/16 :goto_0

    .line 146
    :pswitch_8
    new-instance v2, Lbgg;

    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->a:Landroid/content/Context;

    move-object/from16 v0, v21

    invoke-direct {v2, v3, v0}, Lbgg;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    goto/16 :goto_0

    .line 149
    :pswitch_9
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 152
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lajd;->b:Lajc;

    iget v2, v2, Lajc;->l:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget-object v3, v3, Lajc;->o:Lbgp;

    move-object/from16 v0, p0

    iget-object v4, v0, Lajd;->b:Lajc;

    iget-object v4, v4, Lajc;->p:Lbgp;

    move-object/from16 v0, p0

    iget-object v6, v0, Lajd;->b:Lajc;

    iget-object v6, v6, Lajc;->q:Lbgp;

    invoke-static {v2, v3, v4, v6}, Lajd;->a(ILbgp;Lbgp;Lbgp;)Lbgp;

    move-result-object v6

    .line 155
    new-instance v2, Lbgr;

    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->a:Landroid/content/Context;

    new-instance v4, Lcom/twitter/library/service/v;

    move-object/from16 v0, v21

    invoke-direct {v4, v0}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lajd;->b:Lajc;

    iget-object v7, v7, Lajc;->t:Lbgw;

    invoke-direct/range {v2 .. v7}, Lbgr;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;ILbgp;Lbgw;)V

    goto/16 :goto_0

    .line 159
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lajd;->b:Lajc;

    iget v2, v2, Lajc;->l:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget-object v3, v3, Lajc;->o:Lbgp;

    move-object/from16 v0, p0

    iget-object v4, v0, Lajd;->b:Lajc;

    iget-object v4, v4, Lajc;->p:Lbgp;

    move-object/from16 v0, p0

    iget-object v6, v0, Lajd;->b:Lajc;

    iget-object v6, v6, Lajc;->q:Lbgp;

    invoke-static {v2, v3, v4, v6}, Lajd;->a(ILbgp;Lbgp;Lbgp;)Lbgp;

    move-result-object v6

    .line 162
    new-instance v2, Lbid;

    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->a:Landroid/content/Context;

    new-instance v4, Lcom/twitter/library/service/v;

    move-object/from16 v0, v21

    invoke-direct {v4, v0}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lajd;->b:Lajc;

    iget-object v7, v7, Lajc;->j:Ljava/lang/String;

    .line 163
    invoke-static {v7}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lajd;->b:Lajc;

    iget-object v8, v8, Lajc;->t:Lbgw;

    invoke-direct/range {v2 .. v8}, Lbid;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;ILbgp;Ljava/lang/String;Lbgw;)V

    goto/16 :goto_0

    .line 166
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lajd;->b:Lajc;

    iget v2, v2, Lajc;->l:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget-object v3, v3, Lajc;->o:Lbgp;

    move-object/from16 v0, p0

    iget-object v4, v0, Lajd;->b:Lajc;

    iget-object v4, v4, Lajc;->p:Lbgp;

    move-object/from16 v0, p0

    iget-object v6, v0, Lajd;->b:Lajc;

    iget-object v6, v6, Lajc;->q:Lbgp;

    invoke-static {v2, v3, v4, v6}, Lajd;->a(ILbgp;Lbgp;Lbgp;)Lbgp;

    move-result-object v6

    .line 169
    new-instance v2, Lahv;

    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->a:Landroid/content/Context;

    new-instance v4, Lcom/twitter/library/service/v;

    move-object/from16 v0, v21

    invoke-direct {v4, v0}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lajd;->b:Lajc;

    iget-object v7, v7, Lajc;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lajd;->b:Lajc;

    iget-object v8, v8, Lajc;->t:Lbgw;

    invoke-direct/range {v2 .. v8}, Lahv;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;ILbgp;Ljava/lang/String;Lbgw;)V

    goto/16 :goto_0

    .line 173
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lajd;->b:Lajc;

    iget v2, v2, Lajc;->l:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->b:Lajc;

    iget-object v3, v3, Lajc;->o:Lbgp;

    move-object/from16 v0, p0

    iget-object v4, v0, Lajd;->b:Lajc;

    iget-object v4, v4, Lajc;->p:Lbgp;

    move-object/from16 v0, p0

    iget-object v6, v0, Lajd;->b:Lajc;

    iget-object v6, v6, Lajc;->q:Lbgp;

    invoke-static {v2, v3, v4, v6}, Lajd;->a(ILbgp;Lbgp;Lbgp;)Lbgp;

    move-result-object v6

    .line 176
    new-instance v2, Lbgr;

    move-object/from16 v0, p0

    iget-object v3, v0, Lajd;->a:Landroid/content/Context;

    new-instance v4, Lcom/twitter/library/service/v;

    move-object/from16 v0, v21

    invoke-direct {v4, v0}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lajd;->b:Lajc;

    iget-object v7, v7, Lajc;->t:Lbgw;

    invoke-direct/range {v2 .. v7}, Lbgr;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;ILbgp;Lbgw;)V

    goto/16 :goto_0

    .line 70
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_6
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_3
        :pswitch_2
        :pswitch_5
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method
