.class public final Laru;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lart;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcsd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcsd",
            "<",
            "Lart;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/b;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/l;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Laru;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Laru;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcsd;Lcta;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lart;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/b;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/l;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/i;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    sget-boolean v0, Laru;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 31
    :cond_0
    iput-object p1, p0, Laru;->b:Lcsd;

    .line 32
    sget-boolean v0, Laru;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 33
    :cond_1
    iput-object p2, p0, Laru;->c:Lcta;

    .line 34
    sget-boolean v0, Laru;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 35
    :cond_2
    iput-object p3, p0, Laru;->d:Lcta;

    .line 36
    sget-boolean v0, Laru;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 37
    :cond_3
    iput-object p4, p0, Laru;->e:Lcta;

    .line 38
    return-void
.end method

.method public static a(Lcsd;Lcta;Lcta;Lcta;)Ldagger/internal/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lart;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/b;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/l;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/i;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Lart;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    new-instance v0, Laru;

    invoke-direct {v0, p0, p1, p2, p3}, Laru;-><init>(Lcsd;Lcta;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lart;
    .locals 5

    .prologue
    .line 42
    iget-object v3, p0, Laru;->b:Lcsd;

    new-instance v4, Lart;

    iget-object v0, p0, Laru;->c:Lcta;

    .line 45
    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/onboarding/flowstep/common/b;

    iget-object v1, p0, Laru;->d:Lcta;

    .line 46
    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/app/onboarding/flowstep/common/l;

    iget-object v2, p0, Laru;->e:Lcta;

    .line 47
    invoke-interface {v2}, Lcta;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/app/onboarding/flowstep/common/i;

    invoke-direct {v4, v0, v1, v2}, Lart;-><init>(Lcom/twitter/app/onboarding/flowstep/common/b;Lcom/twitter/app/onboarding/flowstep/common/l;Lcom/twitter/app/onboarding/flowstep/common/i;)V

    .line 42
    invoke-static {v3, v4}, Ldagger/internal/MembersInjectors;->a(Lcsd;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lart;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Laru;->a()Lart;

    move-result-object v0

    return-object v0
.end method
