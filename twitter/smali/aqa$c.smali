.class public Laqa$c;
.super Lapv$c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Laqa;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# instance fields
.field private final b:Lcom/twitter/app/dm/widget/SentMessageBylineView;

.field private final c:Landroid/view/View;

.field private final d:Lcom/twitter/android/media/widget/AttachmentMediaView;

.field private final e:Lcom/twitter/media/ui/AnimatingProgressBar;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/twitter/app/dm/widget/SentMessageBylineView;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 377
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lapv$c;-><init>(Landroid/view/View;Z)V

    .line 379
    iput-object p2, p0, Laqa$c;->b:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    .line 381
    iget-object v0, p0, Laqa$c;->a:Landroid/view/ViewGroup;

    const v1, 0x7f130329

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laqa$c;->c:Landroid/view/View;

    .line 382
    iget-object v0, p0, Laqa$c;->c:Landroid/view/View;

    const v1, 0x7f13032a

    .line 383
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 382
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/AttachmentMediaView;

    iput-object v0, p0, Laqa$c;->d:Lcom/twitter/android/media/widget/AttachmentMediaView;

    .line 384
    iget-object v0, p0, Laqa$c;->c:Landroid/view/View;

    const v1, 0x7f13032b

    .line 385
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 384
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/AnimatingProgressBar;

    iput-object v0, p0, Laqa$c;->e:Lcom/twitter/media/ui/AnimatingProgressBar;

    .line 386
    iget-object v0, p0, Laqa$c;->e:Lcom/twitter/media/ui/AnimatingProgressBar;

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/AnimatingProgressBar;->setHideOnComplete(Z)V

    .line 387
    iget-object v0, p0, Laqa$c;->e:Lcom/twitter/media/ui/AnimatingProgressBar;

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/AnimatingProgressBar;->setResetPrimaryOnComplete(Z)V

    .line 388
    iget-object v0, p0, Laqa$c;->e:Lcom/twitter/media/ui/AnimatingProgressBar;

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/AnimatingProgressBar;->setResetSecondaryOnComplete(Z)V

    .line 389
    return-void
.end method

.method static synthetic a(Laqa$c;)Lcom/twitter/app/dm/widget/SentMessageBylineView;
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Laqa$c;->b:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    return-object v0
.end method

.method static synthetic b(Laqa$c;)Landroid/view/View;
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Laqa$c;->c:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Laqa$c;)Lcom/twitter/android/media/widget/AttachmentMediaView;
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Laqa$c;->d:Lcom/twitter/android/media/widget/AttachmentMediaView;

    return-object v0
.end method

.method static synthetic d(Laqa$c;)Lcom/twitter/media/ui/AnimatingProgressBar;
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Laqa$c;->e:Lcom/twitter/media/ui/AnimatingProgressBar;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const v3, 0x7f130027

    .line 393
    invoke-super {p0}, Lapv$c;->a()V

    .line 395
    iget-object v0, p0, Laqa$c;->b:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    invoke-virtual {v0, v3}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 396
    instance-of v1, v0, Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 397
    iget-object v1, p0, Laqa$c;->b:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    const/4 v2, 0x0

    invoke-virtual {v1, v3, v2}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->setTag(ILjava/lang/Object;)V

    .line 398
    iget-object v1, p0, Laqa$c;->b:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 400
    :cond_0
    return-void
.end method
