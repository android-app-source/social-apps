.class public Lbay;
.super Lcom/twitter/library/service/s;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lbay;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/s;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 28
    return-void
.end method


# virtual methods
.method protected a(J)V
    .locals 1

    .prologue
    .line 64
    invoke-static {}, Lbuu;->a()Lbuu;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lbuu;->a(J)V

    .line 65
    return-void
.end method

.method protected a_(Lcom/twitter/library/service/u;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 32
    invoke-virtual {p0}, Lbay;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/v;

    .line 33
    iget-wide v2, v0, Lcom/twitter/library/service/v;->c:J

    .line 34
    invoke-virtual {p0}, Lbay;->R()Lcom/twitter/library/provider/t;

    move-result-object v4

    .line 35
    invoke-static {v2, v3}, Lcom/twitter/library/scribe/ScribeDatabaseHelper;->a(J)Lcom/twitter/library/scribe/ScribeDatabaseHelper;

    move-result-object v5

    .line 36
    invoke-virtual {p0, v2, v3}, Lbay;->a(J)V

    .line 37
    iget-object v6, p0, Lbay;->p:Landroid/content/Context;

    invoke-static {v6, v2, v3, v7, v7}, Lcom/twitter/library/service/q;->a(Landroid/content/Context;JLjava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    .line 38
    invoke-virtual {v4}, Lcom/twitter/library/provider/t;->j()V

    .line 39
    invoke-virtual {v5}, Lcom/twitter/library/scribe/ScribeDatabaseHelper;->c()V

    .line 40
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v4

    invoke-virtual {v4}, Lcqq;->c()Lcqr;

    move-result-object v4

    invoke-interface {v4, v2, v3}, Lcqr;->b(J)V

    .line 41
    iget-object v0, v0, Lcom/twitter/library/service/v;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/util/b;->a(Ljava/lang/String;)Landroid/accounts/AccountManagerFuture;

    move-result-object v0

    .line 43
    if-eqz v0, :cond_0

    .line 45
    :try_start_0
    invoke-interface {v0}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    .line 56
    :goto_0
    invoke-static {}, Lcom/twitter/library/provider/j;->c()Lcom/twitter/library/provider/j;

    move-result-object v1

    .line 57
    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/provider/j;->d(J)I

    .line 58
    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/provider/j;->c(J)I

    .line 59
    invoke-virtual {p1, v0}, Lcom/twitter/library/service/u;->a(Z)V

    .line 60
    return-void

    .line 46
    :catch_0
    move-exception v0

    move v0, v1

    .line 52
    goto :goto_0

    .line 48
    :catch_1
    move-exception v0

    move v0, v1

    .line 52
    goto :goto_0

    .line 50
    :catch_2
    move-exception v0

    move v0, v1

    .line 52
    goto :goto_0

    :cond_0
    move v0, v1

    .line 54
    goto :goto_0
.end method
