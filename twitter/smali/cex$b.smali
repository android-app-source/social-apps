.class public Lcex$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcex;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcex;",
        "Lcex$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcex$a;
    .locals 1

    .prologue
    .line 111
    new-instance v0, Lcex$a;

    invoke-direct {v0}, Lcex$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcex$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 117
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcex$a;->a(Ljava/lang/String;)Lcex$a;

    move-result-object v0

    .line 118
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcex$a;->b(Ljava/lang/String;)Lcex$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/util/serialization/f;->b:Lcom/twitter/util/serialization/l;

    .line 119
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v1, v0}, Lcex$a;->a(Ljava/lang/Boolean;)Lcex$a;

    move-result-object v1

    sget-object v0, Lcez;->a:Lcez$b;

    .line 120
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcez;

    invoke-virtual {v1, v0}, Lcex$a;->a(Lcez;)Lcex$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/moments/w;->a:Lcom/twitter/util/serialization/l;

    .line 121
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/w;

    invoke-virtual {v1, v0}, Lcex$a;->a(Lcom/twitter/model/moments/w;)Lcex$a;

    move-result-object v1

    const-class v0, Lcom/twitter/model/moments/MomentVisibilityMode;

    .line 123
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/MomentVisibilityMode;

    .line 122
    invoke-virtual {v1, v0}, Lcex$a;->a(Lcom/twitter/model/moments/MomentVisibilityMode;)Lcex$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/util/serialization/f;->b:Lcom/twitter/util/serialization/l;

    .line 124
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v1, v0}, Lcex$a;->b(Ljava/lang/Boolean;)Lcex$a;

    .line 125
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 107
    check-cast p2, Lcex$a;

    invoke-virtual {p0, p1, p2, p3}, Lcex$b;->a(Lcom/twitter/util/serialization/n;Lcex$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcex;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p2, Lcex;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcex;->b:Ljava/lang/String;

    .line 131
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcex;->c:Ljava/lang/Boolean;

    sget-object v2, Lcom/twitter/util/serialization/f;->b:Lcom/twitter/util/serialization/l;

    .line 132
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcex;->d:Lcez;

    sget-object v2, Lcez;->a:Lcez$b;

    .line 133
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcex;->e:Lcom/twitter/model/moments/w;

    sget-object v2, Lcom/twitter/model/moments/w;->a:Lcom/twitter/util/serialization/l;

    .line 134
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcex;->f:Lcom/twitter/model/moments/MomentVisibilityMode;

    const-class v2, Lcom/twitter/model/moments/MomentVisibilityMode;

    .line 136
    invoke-static {v2}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 135
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcex;->g:Ljava/lang/Boolean;

    sget-object v2, Lcom/twitter/util/serialization/f;->b:Lcom/twitter/util/serialization/l;

    .line 137
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 138
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    check-cast p2, Lcex;

    invoke-virtual {p0, p1, p2}, Lcex$b;->a(Lcom/twitter/util/serialization/o;Lcex;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0}, Lcex$b;->a()Lcex$a;

    move-result-object v0

    return-object v0
.end method
