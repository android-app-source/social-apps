.class public final Lcct;
.super Lccl;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcct$b;,
        Lcct$a;
    }
.end annotation


# static fields
.field public static final c:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcct;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcct$b;

    invoke-direct {v0}, Lcct$b;-><init>()V

    sput-object v0, Lcct;->c:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcct$a;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lccl;-><init>(Lccl$a;)V

    .line 23
    invoke-static {p1}, Lcct$a;->a(Lcct$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcct;->d:Ljava/lang/String;

    .line 24
    return-void
.end method

.method synthetic constructor <init>(Lcct$a;Lcct$1;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcct;-><init>(Lcct$a;)V

    return-void
.end method

.method private a(Lcct;)Z
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcct;->d:Ljava/lang/String;

    iget-object v1, p1, Lcct;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    const-string/jumbo v0, "text_input"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcct;->d:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 40
    invoke-super {p0, p1}, Lccl;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcct;

    if-eqz v0, :cond_1

    check-cast p1, Lcct;

    .line 41
    invoke-direct {p0, p1}, Lcct;->a(Lcct;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 40
    :goto_0
    return v0

    .line 41
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 46
    invoke-super {p0}, Lccl;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcct;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
