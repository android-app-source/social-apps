.class public Lacj;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/q",
        "<",
        "Lcom/twitter/model/moments/GuideCategories;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lrx/subjects/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/b",
            "<",
            "Lcom/twitter/model/moments/GuideCategories;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/android/moments/data/b;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/data/b;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {}, Lrx/subjects/b;->r()Lrx/subjects/b;

    move-result-object v0

    iput-object v0, p0, Lacj;->a:Lrx/subjects/b;

    .line 24
    iput-object p1, p0, Lacj;->b:Lcom/twitter/android/moments/data/b;

    .line 25
    invoke-virtual {p1, p0}, Lcom/twitter/android/moments/data/b;->a(Lcom/twitter/util/q;)V

    .line 26
    return-void
.end method


# virtual methods
.method public a()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/moments/GuideCategories;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lacj;->a:Lrx/subjects/b;

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lacj;->b:Lcom/twitter/android/moments/data/b;

    invoke-virtual {v0, p0}, Lcom/twitter/android/moments/data/b;->b(Lcom/twitter/util/q;)V

    .line 46
    iget-object v0, p0, Lacj;->a:Lrx/subjects/b;

    invoke-virtual {v0}, Lrx/subjects/b;->by_()V

    .line 47
    return-void
.end method

.method public onEvent(Lcom/twitter/model/moments/GuideCategories;)V
    .locals 1

    .prologue
    .line 30
    if-eqz p1, :cond_0

    .line 31
    iget-object v0, p0, Lacj;->a:Lrx/subjects/b;

    invoke-virtual {v0, p1}, Lrx/subjects/b;->a(Ljava/lang/Object;)V

    .line 33
    :cond_0
    return-void
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 13
    check-cast p1, Lcom/twitter/model/moments/GuideCategories;

    invoke-virtual {p0, p1}, Lacj;->onEvent(Lcom/twitter/model/moments/GuideCategories;)V

    return-void
.end method
