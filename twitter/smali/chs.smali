.class public Lchs;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcie;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcie",
        "<",
        "Lcha;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z

.field public final c:Lchl;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLchl;)V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    invoke-static {p1}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lchs;->a:Ljava/lang/String;

    .line 15
    iput-boolean p2, p0, Lchs;->b:Z

    .line 16
    iput-object p3, p0, Lchs;->c:Lchl;

    .line 17
    return-void
.end method


# virtual methods
.method public a(Lcgx;Lchc;)Lcha;
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lcha$a;

    invoke-direct {v0}, Lcha$a;-><init>()V

    iget-object v1, p0, Lchs;->a:Ljava/lang/String;

    .line 24
    invoke-virtual {v0, v1}, Lcha$a;->a(Ljava/lang/String;)Lcha$a;

    move-result-object v0

    iget-boolean v1, p0, Lchs;->b:Z

    .line 25
    invoke-virtual {v0, v1}, Lcha$a;->a(Z)Lcha$a;

    move-result-object v1

    iget-object v0, p0, Lchs;->c:Lchl;

    .line 26
    invoke-static {v0, p1, p2}, Lcie$a;->a(Lcie;Lcgx;Lchc;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/j;

    invoke-virtual {v1, v0}, Lcha$a;->a(Lcom/twitter/model/timeline/j;)Lcha$a;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lcha$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcha;

    .line 23
    return-object v0
.end method

.method public synthetic b(Lcgx;Lchc;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0, p1, p2}, Lchs;->a(Lcgx;Lchc;)Lcha;

    move-result-object v0

    return-object v0
.end method
