.class Lcnp$1;
.super Lcno$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcnp;-><init>(Lcno;Landroid/view/View;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcnp;


# direct methods
.method constructor <init>(Lcnp;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcnp$1;->a:Lcnp;

    invoke-direct {p0}, Lcno$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcno;I)V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcnp$1;->a:Lcnp;

    invoke-static {v0}, Lcnp;->a(Lcnp;)Lcnp$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcnp$1;->a:Lcnp;

    invoke-static {v0}, Lcnp;->a(Lcnp;)Lcnp$a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcnp$a;->a(Lcno;I)V

    .line 33
    :cond_0
    return-void
.end method

.method public a(Lcno;IIIZ)V
    .locals 6

    .prologue
    .line 38
    iget-object v0, p0, Lcnp$1;->a:Lcnp;

    invoke-static {v0}, Lcnp;->a(Lcnp;)Lcnp$a;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    invoke-interface {p1}, Lcno;->f()I

    move-result v0

    if-nez v0, :cond_1

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    add-int v0, p2, p3

    .line 44
    iget-object v1, p0, Lcnp$1;->a:Lcnp;

    invoke-static {v1}, Lcnp;->b(Lcnp;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcnp$1;->a:Lcnp;

    invoke-static {v1}, Lcnp;->b(Lcnp;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcnp$1;->a:Lcnp;

    .line 45
    invoke-static {v1}, Lcnp;->b(Lcnp;)Landroid/view/View;

    move-result-object v1

    invoke-interface {p1, v1}, Lcno;->a(Landroid/view/View;)I

    move-result v1

    if-gt p2, v1, :cond_2

    .line 46
    iget-object v1, p0, Lcnp$1;->a:Lcnp;

    invoke-static {v1}, Lcnp;->a(Lcnp;)Lcnp$a;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcnp$a;->d(Lcno;)V

    .line 49
    :cond_2
    iget-object v1, p0, Lcnp$1;->a:Lcnp;

    invoke-static {v1}, Lcnp;->c(Lcnp;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcnp$1;->a:Lcnp;

    invoke-static {v1}, Lcnp;->c(Lcnp;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcnp$1;->a:Lcnp;

    .line 50
    invoke-static {v1}, Lcnp;->c(Lcnp;)Landroid/view/View;

    move-result-object v1

    invoke-interface {p1, v1}, Lcno;->a(Landroid/view/View;)I

    move-result v1

    if-lt v0, v1, :cond_3

    .line 51
    iget-object v0, p0, Lcnp$1;->a:Lcnp;

    invoke-static {v0}, Lcnp;->a(Lcnp;)Lcnp$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcnp$a;->e(Lcno;)V

    .line 54
    :cond_3
    iget-object v0, p0, Lcnp$1;->a:Lcnp;

    invoke-static {v0}, Lcnp;->a(Lcnp;)Lcnp$a;

    move-result-object v0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcnp$a;->a(Lcno;IIIZ)V

    goto :goto_0
.end method
