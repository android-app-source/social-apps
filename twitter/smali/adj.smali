.class public Ladj;
.super Lckb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lckb",
        "<",
        "Lcom/twitter/android/timeline/ac;",
        "Ladk;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lckb;-><init>()V

    .line 28
    iput-object p1, p0, Ladj;->a:Landroid/content/Context;

    .line 29
    iput-object p2, p0, Ladj;->b:Landroid/view/LayoutInflater;

    .line 30
    iput-object p3, p0, Ladj;->c:Landroid/content/res/Resources;

    .line 31
    return-void
.end method

.method static synthetic a(Ladj;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Ladj;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)Ladk;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Ladj;->b:Landroid/view/LayoutInflater;

    invoke-static {v0, p1}, Ladk;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Ladk;

    move-result-object v0

    return-object v0
.end method

.method public a(Ladk;Lcom/twitter/android/timeline/ac;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 42
    iget-object v0, p2, Lcom/twitter/android/timeline/ac;->a:Lcom/twitter/model/moments/x;

    iget-object v1, v0, Lcom/twitter/model/moments/x;->c:Lcom/twitter/model/moments/Moment;

    .line 43
    iget-object v0, v1, Lcom/twitter/model/moments/Moment;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ladk;->a(Ljava/lang/String;)V

    .line 45
    iget-object v0, p2, Lcom/twitter/android/timeline/ac;->a:Lcom/twitter/model/moments/x;

    iget v0, v0, Lcom/twitter/model/moments/x;->b:I

    if-ne v0, v3, :cond_1

    .line 46
    iget-object v0, v1, Lcom/twitter/model/moments/Moment;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Ladj;->c:Landroid/content/res/Resources;

    const v2, 0x7f0a0594

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 56
    :goto_0
    invoke-virtual {p1, v0}, Ladk;->b(Ljava/lang/String;)V

    .line 57
    new-instance v0, Ladj$1;

    invoke-direct {v0, p0, v1}, Ladj$1;-><init>(Ladj;Lcom/twitter/model/moments/Moment;)V

    invoke-virtual {p1, v0}, Ladk;->a(Landroid/view/View$OnClickListener;)V

    .line 65
    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Ladj;->c:Landroid/content/res/Resources;

    const v2, 0x7f0a0593

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v1, Lcom/twitter/model/moments/Moment;->i:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 51
    :cond_1
    iget-object v0, p2, Lcom/twitter/android/timeline/ac;->a:Lcom/twitter/model/moments/x;

    iget v0, v0, Lcom/twitter/model/moments/x;->b:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    .line 52
    iget-object v0, p0, Ladj;->c:Landroid/content/res/Resources;

    const v2, 0x7f0a0582

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 54
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Lckb$a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Ladk;

    check-cast p2, Lcom/twitter/android/timeline/ac;

    invoke-virtual {p0, p1, p2}, Ladj;->a(Ladk;Lcom/twitter/android/timeline/ac;)V

    return-void
.end method

.method public a(Lcom/twitter/android/timeline/ac;)Z
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 19
    check-cast p1, Lcom/twitter/android/timeline/ac;

    invoke-virtual {p0, p1}, Ladj;->a(Lcom/twitter/android/timeline/ac;)Z

    move-result v0

    return v0
.end method

.method public synthetic b(Landroid/view/ViewGroup;)Lckb$a;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Ladj;->a(Landroid/view/ViewGroup;)Ladk;

    move-result-object v0

    return-object v0
.end method
