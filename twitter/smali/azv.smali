.class public Lazv;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcmm;
.implements Lcom/twitter/ui/widget/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lazv$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/internal/android/widget/ToolBar;

.field private b:Lcom/twitter/internal/android/widget/ToolBarItemView;

.field private c:I

.field private d:I

.field private e:I

.field private f:Landroid/view/View;

.field private g:Lazv$a;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/CharSequence;

.field private l:Ljava/lang/CharSequence;

.field private m:Landroid/content/Intent;

.field private n:Z

.field private o:I

.field private p:Ljava/lang/CharSequence;

.field private q:Landroid/graphics/drawable/Drawable;

.field private r:I

.field private s:I

.field private t:Z


# direct methods
.method public constructor <init>(Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lazv;->a:Lcom/twitter/internal/android/widget/ToolBar;

    .line 65
    iput-boolean v0, p0, Lazv;->i:Z

    .line 66
    iput-boolean v0, p0, Lazv;->j:Z

    .line 67
    iput v0, p0, Lazv;->e:I

    .line 68
    return-void
.end method

.method public constructor <init>(Lcom/twitter/internal/android/widget/ToolBar;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v0, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lazv;->a:Lcom/twitter/internal/android/widget/ToolBar;

    .line 72
    sget-object v1, Lazw$l;->ToolBarItem:[I

    invoke-virtual {p2, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 73
    sget v2, Lazw$l;->ToolBarItem_android_id:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lazv;->d:I

    .line 74
    sget v2, Lazw$l;->ToolBarItem_android_icon:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lazv;->c:I

    .line 75
    sget v2, Lazw$l;->ToolBarItem_android_title:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, p0, Lazv;->k:Ljava/lang/CharSequence;

    .line 76
    iget v2, p0, Lazv;->c:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lazv;->k:Ljava/lang/CharSequence;

    if-nez v2, :cond_0

    .line 77
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "ToolBar item requires either icon or title."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_0
    sget v2, Lazw$l;->ToolBarItem_android_showAsAction:I

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lazv;->e:I

    .line 80
    sget v2, Lazw$l;->ToolBarItem_actionLayout:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {p0, v2}, Lazv;->b(I)Lazv;

    .line 81
    sget v2, Lazw$l;->ToolBarItem_android_enabled:I

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lazv;->j:Z

    .line 82
    sget v2, Lazw$l;->ToolBarItem_android_visible:I

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lazv;->i:Z

    .line 83
    sget v2, Lazw$l;->ToolBarItem_subtitle:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, p0, Lazv;->p:Ljava/lang/CharSequence;

    .line 84
    sget v2, Lazw$l;->ToolBarItem_alignLeft:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lazv;->t:Z

    .line 85
    sget v2, Lazw$l;->ToolBarItem_overflowIcon:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {p0, v2}, Lazv;->e(I)Lazv;

    .line 87
    sget v2, Lazw$l;->ToolBarItem_order:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 88
    sget v2, Lazw$l;->ToolBarItem_order:I

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 89
    if-gtz v0, :cond_1

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "ToolBar item order must be greater than 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_1
    iput v0, p0, Lazv;->r:I

    .line 96
    sget v0, Lazw$l;->ToolBarItem_priority:I

    const v2, 0x7fffffff

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lazv;->s:I

    .line 97
    invoke-direct {p0}, Lazv;->u()V

    .line 98
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 99
    return-void
.end method

.method private u()V
    .locals 2

    .prologue
    .line 467
    iget-object v0, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    if-eqz v0, :cond_0

    .line 468
    iget-object v0, p0, Lazv;->l:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 469
    iget-object v0, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    iget-object v1, p0, Lazv;->l:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 474
    :cond_0
    :goto_0
    return-void

    .line 470
    :cond_1
    iget-object v0, p0, Lazv;->k:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    .line 471
    iget-object v0, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    iget-object v1, p0, Lazv;->k:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lazv;->d:I

    return v0
.end method

.method public a(I)Lazv;
    .locals 0

    .prologue
    .line 109
    iput p1, p0, Lazv;->d:I

    .line 110
    return-object p0
.end method

.method public a(Landroid/content/Intent;)Lazv;
    .locals 0

    .prologue
    .line 349
    iput-object p1, p0, Lazv;->m:Landroid/content/Intent;

    .line 350
    return-object p0
.end method

.method public a(Landroid/graphics/Bitmap;)Lazv;
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    iput v0, p0, Lazv;->c:I

    .line 129
    iget-object v0, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 132
    :cond_0
    return-object p0
.end method

.method public a(Landroid/graphics/drawable/Drawable;)Lazv;
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lazv;->q:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    .line 400
    iput-object p1, p0, Lazv;->q:Landroid/graphics/drawable/Drawable;

    .line 401
    iget-object v0, p0, Lazv;->a:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->m()V

    .line 403
    :cond_0
    return-object p0
.end method

.method public a(Landroid/view/View;)Lazv;
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lazv;->f:Landroid/view/View;

    .line 184
    return-object p0
.end method

.method public a(Lazv$a;)Lazv;
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lazv;->g:Lazv$a;

    .line 221
    return-object p0
.end method

.method public a(Lcom/twitter/internal/android/widget/ToolBarItemView;)Lazv;
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    .line 153
    invoke-direct {p0}, Lazv;->u()V

    .line 154
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Lazv;
    .locals 1

    .prologue
    .line 262
    iput-object p1, p0, Lazv;->k:Ljava/lang/CharSequence;

    .line 263
    iget-object v0, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setLabel(Ljava/lang/CharSequence;)V

    .line 266
    :cond_0
    invoke-direct {p0}, Lazv;->u()V

    .line 267
    return-object p0
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcmm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 174
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 216
    iput-boolean p1, p0, Lazv;->h:Z

    .line 217
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lazv;->c:I

    return v0
.end method

.method public b(I)Lazv;
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 159
    if-eqz p1, :cond_0

    .line 160
    iget-object v0, p0, Lazv;->a:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->getThemedContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 161
    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lazv;->f:Landroid/view/View;

    .line 163
    :cond_0
    return-object p0
.end method

.method public b(Ljava/lang/CharSequence;)Lazv;
    .locals 0

    .prologue
    .line 287
    iput-object p1, p0, Lazv;->l:Ljava/lang/CharSequence;

    .line 288
    invoke-direct {p0}, Lazv;->u()V

    .line 289
    return-object p0
.end method

.method public b(Z)Lazv;
    .locals 1

    .prologue
    .line 232
    iget-boolean v0, p0, Lazv;->i:Z

    if-eq v0, p1, :cond_0

    .line 233
    iput-boolean p1, p0, Lazv;->i:Z

    .line 234
    iget-object v0, p0, Lazv;->a:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/ToolBar;->f(Lazv;)V

    .line 236
    :cond_0
    return-object p0
.end method

.method public c()Landroid/view/View;
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(I)Lazv;
    .locals 0

    .prologue
    .line 244
    iput p1, p0, Lazv;->e:I

    .line 245
    return-object p0
.end method

.method public c(Ljava/lang/CharSequence;)Lazv;
    .locals 0

    .prologue
    .line 387
    iput-object p1, p0, Lazv;->p:Ljava/lang/CharSequence;

    .line 388
    return-object p0
.end method

.method public c(Z)Lazv;
    .locals 2

    .prologue
    .line 328
    iput-boolean p1, p0, Lazv;->j:Z

    .line 329
    iget-object v0, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setEnabled(Z)V

    .line 331
    if-eqz p1, :cond_1

    .line 332
    iget-object v0, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setAlpha(F)V

    .line 337
    :cond_0
    :goto_0
    return-object p0

    .line 334
    :cond_1
    iget-object v0, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setAlpha(F)V

    goto :goto_0
.end method

.method public d()Landroid/view/View;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    return-object v0
.end method

.method public d(I)Lazv;
    .locals 1

    .prologue
    .line 255
    if-eqz p1, :cond_0

    .line 256
    iget-object v0, p0, Lazv;->a:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->getThemedContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lazv;->a(Ljava/lang/CharSequence;)Lazv;

    .line 258
    :cond_0
    return-object p0
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 354
    iput-boolean p1, p0, Lazv;->n:Z

    .line 355
    return-void
.end method

.method public e()Landroid/view/View;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lazv;->f:Landroid/view/View;

    return-object v0
.end method

.method public e(I)Lazv;
    .locals 1

    .prologue
    .line 407
    if-eqz p1, :cond_0

    .line 408
    iget-object v0, p0, Lazv;->a:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->getThemedContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lazv;->q:Landroid/graphics/drawable/Drawable;

    .line 410
    :cond_0
    return-object p0
.end method

.method public synthetic e(Z)Lcmm;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lazv;->c(Z)Lazv;

    move-result-object v0

    return-object v0
.end method

.method public f(I)Lazv;
    .locals 0

    .prologue
    .line 418
    iput p1, p0, Lazv;->r:I

    .line 419
    return-object p0
.end method

.method public synthetic f(Z)Lcmm;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lazv;->b(Z)Lazv;

    move-result-object v0

    return-object v0
.end method

.method public f()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 188
    iget-boolean v1, p0, Lazv;->h:Z

    if-nez v1, :cond_0

    iget v1, p0, Lazv;->e:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_0

    iget-object v1, p0, Lazv;->f:Landroid/view/View;

    if-nez v1, :cond_1

    .line 196
    :cond_0
    :goto_0
    return v0

    .line 192
    :cond_1
    iget-object v1, p0, Lazv;->g:Lazv$a;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lazv;->g:Lazv$a;

    .line 193
    invoke-interface {v1, p0}, Lazv$a;->b(Lazv;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 194
    :cond_2
    iget-object v0, p0, Lazv;->a:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/ToolBar;->e(Lazv;)Z

    move-result v0

    goto :goto_0
.end method

.method public synthetic g(I)Lcmm;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lazv;->d(I)Lazv;

    move-result-object v0

    return-object v0
.end method

.method public g()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 200
    iget-boolean v1, p0, Lazv;->h:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lazv;->e:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_0

    iget-object v1, p0, Lazv;->f:Landroid/view/View;

    if-nez v1, :cond_1

    .line 208
    :cond_0
    :goto_0
    return v0

    .line 204
    :cond_1
    iget-object v1, p0, Lazv;->g:Lazv$a;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lazv;->g:Lazv$a;

    .line 205
    invoke-interface {v1, p0}, Lazv$a;->a(Lazv;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206
    :cond_2
    iget-object v0, p0, Lazv;->a:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/ToolBar;->d(Lazv;)Z

    move-result v0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 212
    iget-boolean v0, p0, Lazv;->h:Z

    return v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 226
    iget-boolean v0, p0, Lazv;->i:Z

    return v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 240
    iget v0, p0, Lazv;->e:I

    return v0
.end method

.method public k()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lazv;->k:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public l()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 295
    iget-object v0, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 296
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 297
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 298
    iget-object v3, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    invoke-virtual {v3, v0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getLocationOnScreen([I)V

    .line 299
    iget-object v3, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    invoke-virtual {v3, v2}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 300
    iget-object v3, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    invoke-virtual {v3}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getWidth()I

    move-result v3

    .line 301
    iget-object v4, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    invoke-virtual {v4}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getHeight()I

    move-result v4

    .line 302
    const/4 v5, 0x1

    aget v5, v0, v5

    div-int/lit8 v6, v4, 0x2

    add-int/2addr v5, v6

    .line 303
    aget v0, v0, v7

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v0, v3

    .line 304
    iget-object v3, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    invoke-static {v3}, Landroid/support/v4/view/ViewCompat;->getLayoutDirection(Landroid/view/View;)I

    move-result v3

    if-nez v3, :cond_0

    .line 305
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 306
    sub-int v0, v3, v0

    .line 309
    :cond_0
    invoke-virtual {p0}, Lazv;->k()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v1, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 310
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    if-ge v5, v2, :cond_1

    .line 312
    const v2, 0x800035

    invoke-virtual {v1, v2, v0, v4}, Landroid/widget/Toast;->setGravity(III)V

    .line 317
    :goto_0
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 318
    return-void

    .line 315
    :cond_1
    const/16 v0, 0x51

    invoke-virtual {v1, v0, v7, v4}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 322
    iget-boolean v0, p0, Lazv;->j:Z

    return v0
.end method

.method public n()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lazv;->m:Landroid/content/Intent;

    return-object v0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 358
    iget-boolean v0, p0, Lazv;->n:Z

    return v0
.end method

.method public p()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lazv;->p:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public q()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lazv;->q:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 423
    iget v0, p0, Lazv;->r:I

    return v0
.end method

.method public s()I
    .locals 1

    .prologue
    .line 430
    iget v0, p0, Lazv;->s:I

    return v0
.end method

.method public setBadgeMode(I)V
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setBadgeMode(I)V

    .line 370
    :cond_0
    return-void
.end method

.method public setBadgeNumber(I)V
    .locals 1

    .prologue
    .line 374
    iget v0, p0, Lazv;->o:I

    if-eq v0, p1, :cond_0

    .line 375
    iput p1, p0, Lazv;->o:I

    .line 376
    iget-object v0, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lazv;->b:Lcom/twitter/internal/android/widget/ToolBarItemView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setBadgeNumber(I)V

    .line 380
    :cond_0
    return-void
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 449
    iget-boolean v0, p0, Lazv;->t:Z

    return v0
.end method
