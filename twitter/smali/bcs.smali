.class public Lbcs;
.super Lbao;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbao",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcdj;",
        "Ljava/lang/Void;",
        ">;>;"
    }
.end annotation


# instance fields
.field private a:Lcdj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lbcs;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lbao;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 30
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcdj;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-super {p0, p1, p2, p3}, Lbao;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V

    .line 49
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdj;

    iput-object v0, p0, Lbcs;->a:Lcdj;

    .line 52
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 24
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbcs;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/service/d$a;
    .locals 4

    .prologue
    .line 35
    invoke-virtual {p0}, Lbcs;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "interests"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "suggestions"

    aput-object v3, v1, v2

    .line 36
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 35
    return-object v0
.end method

.method protected e()Lcom/twitter/library/api/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcdj;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    const-class v0, Lcdj;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lbcs;->e()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method

.method public g()Lcdj;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lbcs;->a:Lcdj;

    return-object v0
.end method
