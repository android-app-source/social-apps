.class public abstract Lbbw$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbbw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<U:",
        "Lbbw;",
        ">",
        "Lcom/twitter/util/object/i",
        "<TU;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/Session;

.field private c:I

.field private d:I

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 92
    iput-object p1, p0, Lbbw$a;->a:Landroid/content/Context;

    .line 93
    iput-object p2, p0, Lbbw$a;->b:Lcom/twitter/library/client/Session;

    .line 94
    invoke-static {}, Lcom/twitter/util/collection/i;->f()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lbbw$a;->e:Ljava/util/Map;

    .line 95
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbbw$a;->f:Ljava/util/List;

    .line 96
    return-void
.end method

.method static synthetic a(Lbbw$a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lbbw$a;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lbbw$a;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lbbw$a;->b:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method static synthetic c(Lbbw$a;)I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lbbw$a;->c:I

    return v0
.end method

.method static synthetic d(Lbbw$a;)I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lbbw$a;->d:I

    return v0
.end method

.method static synthetic e(Lbbw$a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lbbw$a;->e:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Lbbw$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lbbw$a;->f:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a(I)Lbbw$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lbbw$a",
            "<TU;>;"
        }
    .end annotation

    .prologue
    .line 100
    iput p1, p0, Lbbw$a;->c:I

    .line 101
    return-object p0
.end method

.method public a(Ljava/util/List;)Lbbw$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lbbw$a",
            "<TU;>;"
        }
    .end annotation

    .prologue
    .line 118
    iput-object p1, p0, Lbbw$a;->f:Ljava/util/List;

    .line 119
    return-object p0
.end method

.method public a(Ljava/util/Map;)Lbbw$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/nio/ByteBuffer;",
            ">;)",
            "Lbbw$a",
            "<TU;>;"
        }
    .end annotation

    .prologue
    .line 112
    iput-object p1, p0, Lbbw$a;->e:Ljava/util/Map;

    .line 113
    return-object p0
.end method

.method public b(I)Lbbw$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lbbw$a",
            "<TU;>;"
        }
    .end annotation

    .prologue
    .line 106
    iput p1, p0, Lbbw$a;->d:I

    .line 107
    return-object p0
.end method
