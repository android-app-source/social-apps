.class public Lath$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lath;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lath$a",
        "<TT;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected b:Landroid/view/View;

.field protected c:Landroid/support/v4/app/FragmentActivity;

.field protected d:Landroid/os/Bundle;

.field protected e:Lcom/twitter/app/common/base/d;

.field protected f:Lcom/twitter/android/media/selection/c;

.field protected g:Late;

.field protected h:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lath$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 181
    iput-object p1, p0, Lath$a;->d:Landroid/os/Bundle;

    .line 182
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lath$a;

    return-object v0
.end method

.method public a(Landroid/support/v4/app/FragmentActivity;)Lath$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentActivity;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 145
    iput-object p1, p0, Lath$a;->c:Landroid/support/v4/app/FragmentActivity;

    .line 146
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lath$a;

    return-object v0
.end method

.method public a(Landroid/view/View;)Lath$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 139
    iput-object p1, p0, Lath$a;->b:Landroid/view/View;

    .line 140
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lath$a;

    return-object v0
.end method

.method public a(Late;)Lath$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Late;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 162
    iput-object p1, p0, Lath$a;->g:Late;

    .line 163
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lath$a;

    return-object v0
.end method

.method public a(Lcom/twitter/android/media/selection/c;)Lath$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/media/selection/c;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 157
    iput-object p1, p0, Lath$a;->f:Lcom/twitter/android/media/selection/c;

    .line 158
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lath$a;

    return-object v0
.end method

.method public a(Lcom/twitter/app/common/base/d;)Lath$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/app/common/base/d;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 151
    iput-object p1, p0, Lath$a;->e:Lcom/twitter/app/common/base/d;

    .line 152
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lath$a;

    return-object v0
.end method

.method public a(Ljava/lang/CharSequence;)Lath$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 175
    iput-object p1, p0, Lath$a;->h:Ljava/lang/CharSequence;

    .line 176
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lath$a;

    return-object v0
.end method

.method public b()Lath;
    .locals 1

    .prologue
    .line 187
    new-instance v0, Lath;

    invoke-direct {v0, p0}, Lath;-><init>(Lath$a;)V

    return-object v0
.end method
