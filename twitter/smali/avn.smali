.class public Lavn;
.super Lcbr;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbr",
        "<",
        "Lawy$a;",
        "Lcom/twitter/model/dms/i;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcbr;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lawy$a;)Lcom/twitter/model/dms/i;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 15
    new-instance v0, Lcom/twitter/model/dms/i$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/i$a;-><init>()V

    .line 16
    invoke-interface {p1}, Lawy$a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/i$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/i$a;

    move-result-object v0

    .line 17
    invoke-interface {p1}, Lawy$a;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/i$a;->b(Ljava/lang/String;)Lcom/twitter/model/dms/i$a;

    move-result-object v0

    .line 18
    invoke-interface {p1}, Lawy$a;->e()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/i$a;->a(I)Lcom/twitter/model/dms/i$a;

    move-result-object v0

    .line 19
    invoke-interface {p1}, Lawy$a;->f()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/dms/i$a;->a(J)Lcom/twitter/model/dms/i$a;

    move-result-object v0

    .line 20
    invoke-interface {p1}, Lawy$a;->g()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/dms/i$a;->b(J)Lcom/twitter/model/dms/i$a;

    move-result-object v0

    .line 21
    invoke-interface {p1}, Lawy$a;->h()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/dms/i$a;->c(J)Lcom/twitter/model/dms/i$a;

    move-result-object v0

    .line 22
    invoke-interface {p1}, Lawy$a;->i()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/dms/i$a;->e(J)Lcom/twitter/model/dms/i$a;

    move-result-object v3

    .line 23
    invoke-interface {p1}, Lawy$a;->j()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/twitter/model/dms/i$a;->c(Z)Lcom/twitter/model/dms/i$a;

    move-result-object v0

    .line 24
    invoke-interface {p1}, Lawy$a;->k()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/dms/i$a;->d(J)Lcom/twitter/model/dms/i$a;

    move-result-object v3

    .line 25
    invoke-interface {p1}, Lawy$a;->l()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/twitter/model/dms/i$a;->b(Z)Lcom/twitter/model/dms/i$a;

    move-result-object v3

    .line 26
    invoke-interface {p1}, Lawy$a;->m()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lcom/twitter/model/dms/i$a;->a(Z)Lcom/twitter/model/dms/i$a;

    move-result-object v0

    .line 27
    invoke-interface {p1}, Lawy$a;->n()I

    move-result v3

    if-lez v3, :cond_3

    :goto_3
    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/i$a;->d(Z)Lcom/twitter/model/dms/i$a;

    move-result-object v0

    .line 28
    invoke-interface {p1}, Lawy$a;->o()Lcom/twitter/model/dms/x;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/i$a;->a(Lcom/twitter/model/dms/x;)Lcom/twitter/model/dms/i$a;

    move-result-object v0

    .line 29
    invoke-interface {p1}, Lawy$a;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/i$a;->c(Ljava/lang/String;)Lcom/twitter/model/dms/i$a;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lcom/twitter/model/dms/i$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/i;

    .line 15
    return-object v0

    :cond_0
    move v0, v2

    .line 23
    goto :goto_0

    :cond_1
    move v0, v2

    .line 25
    goto :goto_1

    :cond_2
    move v0, v2

    .line 26
    goto :goto_2

    :cond_3
    move v1, v2

    .line 27
    goto :goto_3
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    check-cast p1, Lawy$a;

    invoke-virtual {p0, p1}, Lavn;->a(Lawy$a;)Lcom/twitter/model/dms/i;

    move-result-object v0

    return-object v0
.end method
