.class public Lckc;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:I

.field private static final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    sget v0, Lcjw$a;->view_holder_compat:I

    sput v0, Lckc;->a:I

    .line 20
    sget v0, Lcjw$a;->view_binder_compat:I

    sput v0, Lckc;->b:I

    return-void
.end method

.method public static a(Landroid/view/ViewGroup;Lckb;)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Lckb",
            "<**>;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 24
    .line 25
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lckb;

    .line 24
    invoke-static {v0, p0}, Lckc;->a(Lckb;Landroid/view/ViewGroup;)Lckb$a;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lckb$a;->b()Landroid/view/View;

    move-result-object v1

    .line 27
    sget v2, Lckc;->a:I

    invoke-virtual {v1, v2, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 28
    sget v0, Lckc;->b:I

    invoke-virtual {v1, v0, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 29
    return-object v1
.end method

.method private static a(Lckb;Landroid/view/ViewGroup;)Lckb$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lckb",
            "<TT;",
            "Lckb$a;",
            ">;",
            "Landroid/view/ViewGroup;",
            ")",
            "Lckb$a;"
        }
    .end annotation

    .prologue
    .line 35
    const/4 v0, 0x0

    .line 36
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    .line 35
    invoke-static {p0, p1, v0, v1}, Lckf;->a(Lckb;Landroid/view/ViewGroup;ILjava/util/List;)Lckb$a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 53
    sget v0, Lckc;->b:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 54
    sget v1, Lckc;->a:I

    invoke-virtual {p0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    .line 55
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 56
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lckb;

    .line 57
    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lckb$a;

    .line 58
    invoke-static {v1, v0}, Lckc;->a(Lckb$a;Lckb;)V

    .line 60
    :cond_0
    return-void
.end method

.method public static a(Landroid/view/View;Lckb;Ljava/lang/Object;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "VH:",
            "Lckb$a;",
            ">(",
            "Landroid/view/View;",
            "Lckb",
            "<TT;TVH;>;TT;I)V"
        }
    .end annotation

    .prologue
    .line 41
    sget v0, Lckc;->a:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lckb$a;

    .line 43
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lckb;

    .line 42
    invoke-static {v0, v1, p2, p3}, Lckc;->a(Lckb$a;Lckb;Ljava/lang/Object;I)V

    .line 44
    return-void
.end method

.method private static a(Lckb$a;Lckb;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lckb$a;",
            "Lckb",
            "<TT;",
            "Lckb$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    .line 65
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    .line 64
    invoke-static {p0, p1, v0}, Lckf;->a(Lckb$a;Lckb;Ljava/util/List;)V

    .line 66
    return-void
.end method

.method private static a(Lckb$a;Lckb;Ljava/lang/Object;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lckb$a;",
            "Lckb",
            "<TT;",
            "Lckb$a;",
            ">;TT;I)V"
        }
    .end annotation

    .prologue
    .line 48
    .line 49
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    .line 48
    invoke-static {p0, p1, p2, p3, v0}, Lckf;->a(Lckb$a;Lckb;Ljava/lang/Object;ILjava/util/List;)V

    .line 50
    return-void
.end method
