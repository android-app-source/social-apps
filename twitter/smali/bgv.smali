.class public abstract Lbgv;
.super Lbfl;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbfl",
        "<",
        "Lbgu;",
        ">;"
    }
.end annotation


# instance fields
.field protected final b:J

.field private final c:J

.field private final h:Lbgn;

.field private final i:I

.field private final j:Ljava/lang/String;

.field private final k:Lbgp;

.field private final l:Lbgw;

.field private m:Lcom/twitter/android/timeline/cf;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;Ljava/lang/String;JIILbgp;Ljava/lang/String;Lbgw;)V
    .locals 12

    .prologue
    .line 53
    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lbgv;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Ljava/lang/String;JIILbgp;Ljava/lang/String;Lbgw;Lbwb;)V

    .line 55
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;Ljava/lang/String;JIILbgp;Ljava/lang/String;Lbgw;Lbwb;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/service/v;",
            "Ljava/lang/String;",
            "JII",
            "Lbgp;",
            "Ljava/lang/String;",
            "Lbgw;",
            "Lbwb",
            "<",
            "Lcom/mopub/nativeads/NativeAd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0, p1, p3, p2, p7}, Lbfl;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;I)V

    .line 62
    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string/jumbo v1, "You must supply a valid timelineOwnerId"

    invoke-static {v0, v1}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 63
    iput-wide p4, p0, Lbgv;->b:J

    .line 64
    iget-wide v0, p2, Lcom/twitter/library/service/v;->c:J

    iput-wide v0, p0, Lbgv;->c:J

    .line 65
    iput p6, p0, Lbgv;->i:I

    .line 66
    iput-object p9, p0, Lbgv;->j:Ljava/lang/String;

    .line 67
    iput-object p10, p0, Lbgv;->l:Lbgw;

    .line 68
    new-instance v0, Lbgn;

    iget-wide v2, p0, Lbgv;->c:J

    invoke-direct {v0, p1, v2, v3, p11}, Lbgn;-><init>(Landroid/content/Context;JLbwb;)V

    iput-object v0, p0, Lbgv;->h:Lbgn;

    .line 69
    iput-object p8, p0, Lbgv;->k:Lbgp;

    .line 70
    return-void

    .line 62
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(Lchd;)V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 149
    invoke-virtual {p1}, Lchd;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lbgv;->d(I)Lbge;

    .line 150
    invoke-virtual {p1}, Lchd;->b()Lcom/twitter/model/timeline/u;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbgv;->a(Lcom/twitter/model/timeline/u;)Lbge;

    .line 151
    return-void
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lbgu;)V
    .locals 3

    .prologue
    .line 136
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 137
    invoke-virtual {p3}, Lbgu;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchx;

    .line 138
    if-eqz v0, :cond_0

    .line 139
    iget-object v1, p0, Lbgv;->h:Lbgn;

    iget-object v2, p0, Lbgv;->m:Lcom/twitter/android/timeline/cf;

    invoke-virtual {v1, v0, v2}, Lbgn;->a(Lchx;Lcom/twitter/android/timeline/cf;)Lchd;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbgv;->a(Lchd;)V

    .line 142
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 26
    check-cast p3, Lbgu;

    invoke-virtual {p0, p1, p2, p3}, Lbgv;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lbgu;)V

    return-void
.end method

.method protected aZ_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    const-string/jumbo v0, "2"

    return-object v0
.end method

.method public abstract ba_()Z
.end method

.method protected abstract e()[Ljava/lang/String;
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lbgv;->y()Lbgu;

    move-result-object v0

    return-object v0
.end method

.method protected g()Lcom/twitter/library/service/d$a;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 89
    invoke-virtual {p0}, Lbgv;->e()[Ljava/lang/String;

    move-result-object v2

    .line 90
    invoke-static {v2}, Lcom/twitter/util/collection/CollectionUtils;->a([Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string/jumbo v3, "You must supply a non-empty json path."

    invoke-static {v0, v3}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 92
    iget-object v0, p0, Lbgv;->k:Lbgp;

    invoke-interface {v0}, Lbgp;->a()Lcom/twitter/android/timeline/cf;

    move-result-object v0

    iput-object v0, p0, Lbgv;->m:Lcom/twitter/android/timeline/cf;

    .line 93
    invoke-virtual {p0}, Lbgv;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 94
    invoke-virtual {p0}, Lbgv;->aZ_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 95
    invoke-virtual {v0, v2}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 97
    invoke-virtual {p0}, Lbgv;->x()I

    move-result v2

    if-eq v2, v1, :cond_0

    iget-object v1, p0, Lbgv;->m:Lcom/twitter/android/timeline/cf;

    if-eqz v1, :cond_0

    .line 98
    const-string/jumbo v1, "cursor"

    iget-object v2, p0, Lbgv;->m:Lcom/twitter/android/timeline/cf;

    iget-object v2, v2, Lcom/twitter/android/timeline/cf;->a:Lcom/twitter/model/timeline/ar;

    iget-object v2, v2, Lcom/twitter/model/timeline/ar;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 100
    :cond_0
    iget-object v1, p0, Lbgv;->l:Lbgw;

    invoke-virtual {v1}, Lbgw;->a()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/util/Map;)Lcom/twitter/library/service/d$a;

    .line 101
    return-object v0

    .line 90
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract h()Z
.end method

.method protected s()Lchx$b;
    .locals 4

    .prologue
    .line 121
    new-instance v0, Lchx$b$a;

    invoke-direct {v0}, Lchx$b$a;-><init>()V

    .line 122
    invoke-virtual {p0}, Lbgv;->x()I

    move-result v1

    invoke-virtual {v0, v1}, Lchx$b$a;->b(I)Lchx$b$a;

    move-result-object v0

    .line 123
    invoke-virtual {p0}, Lbgv;->N()Z

    move-result v1

    invoke-virtual {v0, v1}, Lchx$b$a;->c(Z)Lchx$b$a;

    move-result-object v0

    iget-wide v2, p0, Lbgv;->c:J

    .line 124
    invoke-virtual {v0, v2, v3}, Lchx$b$a;->a(J)Lchx$b$a;

    move-result-object v0

    .line 125
    invoke-virtual {p0}, Lbgv;->h()Z

    move-result v1

    invoke-virtual {v0, v1}, Lchx$b$a;->a(Z)Lchx$b$a;

    move-result-object v0

    .line 126
    invoke-virtual {p0}, Lbgv;->ba_()Z

    move-result v1

    invoke-virtual {v0, v1}, Lchx$b$a;->b(Z)Lchx$b$a;

    move-result-object v0

    iget-wide v2, p0, Lbgv;->b:J

    .line 127
    invoke-virtual {v0, v2, v3}, Lchx$b$a;->b(J)Lchx$b$a;

    move-result-object v0

    iget v1, p0, Lbgv;->i:I

    .line 128
    invoke-virtual {v0, v1}, Lchx$b$a;->a(I)Lchx$b$a;

    move-result-object v0

    iget-object v1, p0, Lbgv;->j:Ljava/lang/String;

    .line 129
    invoke-virtual {v0, v1}, Lchx$b$a;->a(Ljava/lang/String;)Lchx$b$a;

    move-result-object v0

    .line 130
    invoke-virtual {v0}, Lchx$b$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchx$b;

    .line 121
    return-object v0
.end method

.method public t()Lcom/twitter/android/timeline/cf;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lbgv;->m:Lcom/twitter/android/timeline/cf;

    return-object v0
.end method

.method protected y()Lbgu;
    .locals 2

    .prologue
    .line 112
    new-instance v0, Lbgu;

    invoke-virtual {p0}, Lbgv;->s()Lchx$b;

    move-result-object v1

    invoke-direct {v0, v1}, Lbgu;-><init>(Lchx$b;)V

    return-object v0
.end method
