.class public Labh;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Landroid/view/ViewGroup;

.field private final c:Lcom/twitter/android/moments/ui/fullscreen/ac;

.field private final d:Lacr;

.field private final e:Landroid/view/View;

.field private final f:Landroid/view/View;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/TextView;

.field private i:Landroid/view/View$OnClickListener;

.field private j:Landroid/animation/ObjectAnimator;


# direct methods
.method constructor <init>(Landroid/content/res/Resources;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/fullscreen/ac;Lacr;Landroid/view/View;Landroid/widget/TextView;Landroid/view/View;Landroid/widget/TextView;)V
    .locals 4

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Labh;->a:Landroid/content/res/Resources;

    .line 61
    iput-object p2, p0, Labh;->b:Landroid/view/ViewGroup;

    .line 62
    iput-object p3, p0, Labh;->c:Lcom/twitter/android/moments/ui/fullscreen/ac;

    .line 63
    iput-object p4, p0, Labh;->d:Lacr;

    .line 64
    iput-object p5, p0, Labh;->e:Landroid/view/View;

    .line 65
    iput-object p7, p0, Labh;->f:Landroid/view/View;

    .line 66
    iput-object p6, p0, Labh;->g:Landroid/widget/TextView;

    .line 67
    iput-object p8, p0, Labh;->h:Landroid/widget/TextView;

    .line 68
    iget-object v0, p0, Labh;->h:Landroid/widget/TextView;

    iget-object v1, p0, Labh;->a:Landroid/content/res/Resources;

    const v2, 0x7f110195

    .line 69
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iget-object v2, p0, Labh;->a:Landroid/content/res/Resources;

    const v3, 0x7f110196

    .line 70
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const/16 v3, 0x4b0

    .line 68
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/util/e;->a(Landroid/widget/TextView;III)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Labh;->j:Landroid/animation/ObjectAnimator;

    .line 72
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;)Labh;
    .locals 9

    .prologue
    .line 41
    const v0, 0x7f0401e7

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 42
    invoke-virtual {p0, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 43
    new-instance v0, Labh;

    .line 44
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-instance v3, Lcom/twitter/android/moments/ui/fullscreen/ac;

    invoke-direct {v3, v2}, Lcom/twitter/android/moments/ui/fullscreen/ac;-><init>(Landroid/view/ViewGroup;)V

    const v4, 0x7f1304ff

    .line 48
    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 47
    invoke-static {v4}, Lacr;->b(Landroid/view/View;)Lacr;

    move-result-object v4

    const v5, 0x7f1304fe

    .line 49
    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const v6, 0x7f130517

    .line 50
    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    const v7, 0x7f13005e

    .line 51
    invoke-virtual {v2, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f13045e

    .line 52
    invoke-virtual {v2, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    invoke-direct/range {v0 .. v8}, Labh;-><init>(Landroid/content/res/Resources;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/fullscreen/ac;Lacr;Landroid/view/View;Landroid/widget/TextView;Landroid/view/View;Landroid/widget/TextView;)V

    .line 43
    return-object v0
.end method

.method static synthetic a(Labh;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Labh;->i:Landroid/view/View$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 101
    iget-object v0, p0, Labh;->c:Lcom/twitter/android/moments/ui/fullscreen/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ac;->g()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 102
    iget-object v0, p0, Labh;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 103
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Labh;->c:Lcom/twitter/android/moments/ui/fullscreen/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ac;->d()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    iget-object v0, p0, Labh;->c:Lcom/twitter/android/moments/ui/fullscreen/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ac;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    return-void
.end method

.method public a(Lcom/twitter/model/moments/a;)V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Labh;->d:Lacr;

    invoke-virtual {v0, p1}, Lacr;->a(Lcom/twitter/model/moments/a;)V

    .line 130
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Labh;->c:Lcom/twitter/android/moments/ui/fullscreen/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ac;->e()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Labh;->c:Lcom/twitter/android/moments/ui/fullscreen/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ac;->d()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Labh;->c:Lcom/twitter/android/moments/ui/fullscreen/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ac;->b()Landroid/widget/TextView;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 107
    return-void

    .line 106
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 110
    iget-object v0, p0, Labh;->c:Lcom/twitter/android/moments/ui/fullscreen/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ac;->d()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 111
    iget-object v0, p0, Labh;->c:Lcom/twitter/android/moments/ui/fullscreen/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ac;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 112
    return-void
.end method

.method public b(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Labh;->d:Lacr;

    invoke-virtual {v0, p1}, Lacr;->a(Landroid/view/View$OnClickListener;)V

    .line 134
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Labh;->c:Lcom/twitter/android/moments/ui/fullscreen/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ac;->f()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 115
    iget-object v0, p0, Labh;->c:Lcom/twitter/android/moments/ui/fullscreen/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ac;->d()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Labh;->c:Lcom/twitter/android/moments/ui/fullscreen/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ac;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 117
    return-void
.end method

.method public c(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Labh;->i:Landroid/view/View$OnClickListener;

    .line 142
    return-void
.end method

.method public c(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Labh;->c:Lcom/twitter/android/moments/ui/fullscreen/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ac;->c()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v0, p0, Labh;->c:Lcom/twitter/android/moments/ui/fullscreen/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ac;->c()Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Labh$1;

    invoke-direct {v1, p0}, Labh$1;-><init>(Labh;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Labh;->d:Lacr;

    invoke-virtual {v0}, Lacr;->e()V

    .line 138
    return-void
.end method

.method public d(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Labh;->f:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    return-void
.end method

.method public d(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Labh;->c:Lcom/twitter/android/moments/ui/fullscreen/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ac;->g()Landroid/widget/TextView;

    move-result-object v0

    .line 96
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Labh;->c:Lcom/twitter/android/moments/ui/fullscreen/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ac;->g()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Labh;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 146
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Labh;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 150
    return-void
.end method

.method public g()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 157
    iget-object v0, p0, Labh;->h:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 158
    iget-object v0, p0, Labh;->j:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->end()V

    .line 159
    iget-object v0, p0, Labh;->a:Landroid/content/res/Resources;

    const v1, 0x7f02048f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 160
    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Labh;->a:Landroid/content/res/Resources;

    const v3, 0x7f1100fa

    .line 161
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 163
    iget-object v0, p0, Labh;->g:Landroid/widget/TextView;

    invoke-static {v0, v1, v4, v4, v4}, Lcom/twitter/util/ui/j;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 165
    iget-object v0, p0, Labh;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 166
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Labh;->g:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 170
    iget-object v0, p0, Labh;->h:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 171
    iget-object v0, p0, Labh;->j:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 172
    return-void
.end method

.method public i()Landroid/view/View;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Labh;->b:Landroid/view/ViewGroup;

    return-object v0
.end method
