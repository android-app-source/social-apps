.class Lbrm$b;
.super Lcom/twitter/async/service/AsyncOperation;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbrm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/async/service/AsyncOperation",
        "<",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcdu;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/provider/t;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/twitter/library/provider/t;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/provider/t;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 185
    const-string/jumbo v0, "load_stickers"

    invoke-direct {p0, v0}, Lcom/twitter/async/service/AsyncOperation;-><init>(Ljava/lang/String;)V

    .line 186
    iput-object p1, p0, Lbrm$b;->a:Lcom/twitter/library/provider/t;

    .line 187
    iput-object p2, p0, Lbrm$b;->b:Ljava/util/List;

    .line 188
    return-void
.end method


# virtual methods
.method protected a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcdu;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 192
    iget-object v0, p0, Lbrm$b;->a:Lcom/twitter/library/provider/t;

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v0

    const-class v1, Lazk;

    invoke-interface {v0, v1}, Lcom/twitter/database/schema/TwitterSchema;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    .line 193
    invoke-interface {v0}, Lcom/twitter/database/model/k;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    .line 194
    const-string/jumbo v1, "_id"

    iget-object v2, p0, Lbrm$b;->b:Ljava/util/List;

    .line 195
    invoke-static {v1, v2}, Laux;->a(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    .line 194
    invoke-interface {v0, v1, v2}, Lcom/twitter/database/model/l;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/twitter/database/model/g;

    move-result-object v1

    .line 196
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 198
    :goto_0
    :try_start_0
    invoke-virtual {v1}, Lcom/twitter/database/model/g;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    new-instance v3, Lcdu$a;

    invoke-direct {v3}, Lcdu$a;-><init>()V

    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazk$a;

    .line 200
    invoke-interface {v0}, Lazk$a;->a()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcdu$a;->c(J)Lcdu$a;

    move-result-object v3

    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazk$a;

    .line 201
    invoke-interface {v0}, Lazk$a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcdu$a;->a(Ljava/lang/String;)Lcdu$a;

    move-result-object v3

    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazk$a;

    .line 202
    invoke-interface {v0}, Lazk$a;->l()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcdu$a;->f(J)Lcdu$a;

    move-result-object v3

    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazk$a;

    .line 203
    invoke-interface {v0}, Lazk$a;->m()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcdu$a;->g(J)Lcdu$a;

    move-result-object v3

    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazk$a;

    .line 204
    invoke-interface {v0}, Lazk$a;->n()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcdu$a;->h(J)Lcdu$a;

    move-result-object v3

    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazk$a;

    .line 205
    invoke-interface {v0}, Lazk$a;->g()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcdu$a;->b(J)Lcdu$a;

    move-result-object v3

    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazk$a;

    .line 206
    invoke-interface {v0}, Lazk$a;->d()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcdu$a;->a(J)Lcdu$a;

    move-result-object v3

    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazk$a;

    .line 207
    invoke-interface {v0}, Lazk$a;->e()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcdu$a;->e(J)Lcdu$a;

    move-result-object v3

    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazk$a;

    .line 208
    invoke-interface {v0}, Lazk$a;->h()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcdu$a;->d(J)Lcdu$a;

    move-result-object v3

    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazk$a;

    .line 209
    invoke-interface {v0}, Lazk$a;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcdu$a;->c(Ljava/lang/String;)Lcdu$a;

    move-result-object v3

    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazk$a;

    .line 210
    invoke-interface {v0}, Lazk$a;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcdu$a;->b(Ljava/lang/String;)Lcdu$a;

    move-result-object v3

    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazk$a;

    .line 211
    invoke-interface {v0}, Lazk$a;->p()Lcea;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcdu$a;->a(Lcea;)Lcdu$a;

    move-result-object v3

    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazk$a;

    .line 212
    invoke-interface {v0}, Lazk$a;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcdu$a;->d(Ljava/lang/String;)Lcdu$a;

    move-result-object v3

    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazk$a;

    .line 213
    invoke-interface {v0}, Lazk$a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcdu$a;->e(Ljava/lang/String;)Lcdu$a;

    move-result-object v3

    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lazk$a;

    .line 214
    invoke-interface {v0}, Lazk$a;->o()Z

    move-result v0

    invoke-virtual {v3, v0}, Lcdu$a;->a(Z)Lcdu$a;

    move-result-object v0

    .line 215
    invoke-virtual {v0}, Lcdu$a;->q()Ljava/lang/Object;

    move-result-object v0

    .line 199
    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 218
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/twitter/database/model/g;->close()V

    throw v0

    :cond_0
    invoke-virtual {v1}, Lcom/twitter/database/model/g;->close()V

    .line 220
    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method protected b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcdu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 176
    invoke-virtual {p0}, Lbrm$b;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 176
    invoke-virtual {p0}, Lbrm$b;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
