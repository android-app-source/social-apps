.class public Lnx;
.super Lbiy;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/av/playback/AVPlayer;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lbiy;-><init>()V

    .line 22
    iput-object p1, p0, Lnx;->a:Lcom/twitter/library/av/playback/AVPlayer;

    .line 23
    return-void
.end method


# virtual methods
.method public a(Lbiw;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lnx;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public processTick(Lbki;)V
    .locals 3
    .annotation runtime Lbiz;
        a = Lbki;
    .end annotation

    .prologue
    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnx;->b:Z

    .line 28
    iget-object v0, p0, Lnx;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {p0}, Lnx;->b()Lcom/twitter/library/av/m;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/library/av/m;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    const-string/jumbo v2, "playlist_start"

    invoke-virtual {v1, v2}, Lcom/twitter/library/av/m$a;->a(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/av/m$a;->a()Lcom/twitter/library/av/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/m;)V

    .line 29
    return-void
.end method
