.class public abstract Lcof;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static m()Lcof;
    .locals 1

    .prologue
    .line 13
    invoke-static {}, Lcom/twitter/util/m;->ai()Lcom/twitter/util/m$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/util/m$a;->r()Lcof;

    move-result-object v0

    return-object v0
.end method

.method public static n()Z
    .locals 1

    .prologue
    .line 17
    invoke-static {}, Lcom/twitter/util/m;->aj()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public abstract a()Z
.end method

.method public abstract b()Z
.end method

.method public abstract c()Z
.end method

.method public abstract d()Z
.end method

.method public abstract e()Z
.end method

.method public abstract f()J
.end method

.method public abstract g()J
.end method

.method public abstract h()Ljava/lang/String;
.end method

.method public abstract i()I
.end method

.method public abstract j()Z
.end method

.method public abstract k()Z
.end method

.method public abstract l()Z
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lcof;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcof;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcof;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcof;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
