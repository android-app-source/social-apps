.class public Lbdv;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcel;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lbsb;

.field private final b:Lcom/twitter/model/moments/Moment;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbsb;Lcom/twitter/library/client/Session;Lcom/twitter/model/moments/Moment;)V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lbdv;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 33
    iput-object p2, p0, Lbdv;->a:Lbsb;

    .line 34
    iput-object p4, p0, Lbdv;->b:Lcom/twitter/model/moments/Moment;

    .line 35
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 4

    .prologue
    .line 40
    invoke-virtual {p0}, Lbdv;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->b()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "v"

    const-wide/32 v2, 0x57d6f22e

    .line 42
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "X-Twitter-UTCOffset"

    .line 43
    invoke-static {}, Lcom/twitter/util/aa;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "moments"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "unlike"

    aput-object v3, v1, v2

    .line 44
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "moment_id"

    iget-object v2, p0, Lbdv;->b:Lcom/twitter/model/moments/Moment;

    iget-wide v2, v2, Lcom/twitter/model/moments/Moment;->b:J

    .line 45
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 46
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->e()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    .line 40
    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcel;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    invoke-static {p3}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/i;

    invoke-virtual {v0}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcel;

    .line 62
    if-eqz v0, :cond_0

    .line 63
    iget-object v4, v0, Lcel;->a:Lcom/twitter/model/moments/Moment;

    .line 64
    iget-object v0, p0, Lbdv;->a:Lbsb;

    iget-wide v1, v4, Lcom/twitter/model/moments/Moment;->b:J

    iget-boolean v3, v4, Lcom/twitter/model/moments/Moment;->t:Z

    iget-wide v4, v4, Lcom/twitter/model/moments/Moment;->u:J

    invoke-virtual/range {v0 .. v5}, Lbsb;->a(JZJ)V

    .line 68
    :cond_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/u;->a(Lcom/twitter/network/HttpOperation;)V

    .line 69
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 23
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbdv;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcel;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    const-class v0, Lcel;

    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lbdv;->b()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
