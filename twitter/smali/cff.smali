.class public Lcff;
.super Lcfd;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcff$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/twitter/model/moments/r;

.field public final b:Lcom/twitter/model/moments/e;


# direct methods
.method public constructor <init>(Lcom/twitter/model/moments/r;Lcom/twitter/model/moments/e;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcfd;-><init>()V

    .line 20
    iput-object p1, p0, Lcff;->a:Lcom/twitter/model/moments/r;

    .line 21
    iput-object p2, p0, Lcff;->b:Lcom/twitter/model/moments/e;

    .line 22
    return-void
.end method

.method private d()Lcez;
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lcez$a;

    invoke-direct {v0}, Lcez$a;-><init>()V

    iget-object v1, p0, Lcff;->a:Lcom/twitter/model/moments/r;

    .line 39
    invoke-virtual {v0, v1}, Lcez$a;->a(Lcom/twitter/model/moments/r;)Lcez$a;

    move-result-object v0

    iget-object v1, p0, Lcff;->b:Lcom/twitter/model/moments/e;

    .line 40
    invoke-virtual {v0, v1}, Lcez$a;->a(Lcom/twitter/model/moments/e;)Lcez$a;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Lcez$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcez;

    .line 38
    return-object v0
.end method


# virtual methods
.method public a()Lcex;
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcex$a;

    invoke-direct {v0}, Lcex$a;-><init>()V

    .line 27
    invoke-direct {p0}, Lcff;->d()Lcez;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcex$a;->a(Lcez;)Lcex$a;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lcex$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcex;

    .line 26
    return-object v0
.end method

.method public b()Lcfa;
    .locals 2

    .prologue
    .line 33
    new-instance v0, Lcfa;

    invoke-direct {p0}, Lcff;->d()Lcez;

    move-result-object v1

    invoke-direct {v0, v1}, Lcfa;-><init>(Lcez;)V

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcff;->a:Lcom/twitter/model/moments/r;

    iget-object v0, v0, Lcom/twitter/model/moments/r;->c:Lcom/twitter/model/moments/MomentPageDisplayMode;

    sget-object v1, Lcom/twitter/model/moments/MomentPageDisplayMode;->a:Lcom/twitter/model/moments/MomentPageDisplayMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
