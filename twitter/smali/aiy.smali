.class public Laiy;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laiy$a;,
        Laiy$b;,
        Laiy$c;,
        Laiy$d;,
        Laiy$e;
    }
.end annotation


# instance fields
.field private final a:Landroid/widget/ListView;

.field private final b:Landroid/content/Context;

.field private final c:Z

.field private final d:Lcom/twitter/internal/android/widget/ToolBar;

.field private final e:Landroid/widget/TextView;

.field private final f:Laiy$d;


# direct methods
.method public constructor <init>(Landroid/widget/ListView;Landroid/content/Context;ZLandroid/widget/TextView;Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Laiy;->a:Landroid/widget/ListView;

    .line 68
    iput-object p2, p0, Laiy;->b:Landroid/content/Context;

    .line 69
    iput-boolean p3, p0, Laiy;->c:Z

    .line 70
    iput-object p4, p0, Laiy;->e:Landroid/widget/TextView;

    .line 71
    iput-object p5, p0, Laiy;->d:Lcom/twitter/internal/android/widget/ToolBar;

    .line 72
    new-instance v0, Laiy$d;

    iget-object v1, p0, Laiy;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Laiy$d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Laiy;->f:Laiy$d;

    .line 74
    iget-object v0, p0, Laiy;->a:Landroid/widget/ListView;

    iget-object v1, p0, Laiy;->f:Laiy$d;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 75
    return-void
.end method

.method private static a(Lcgb$c;Ljava/util/Map;Ljava/util/Map;)Laiy$e;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcgb$c;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcgb$b;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Laiy$e;"
        }
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lcgb$c;->b:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgb$b;

    iget-object v1, v0, Lcgb$b;->b:Ljava/lang/String;

    .line 155
    iget-object v0, p0, Lcgb$c;->c:Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 156
    new-instance v2, Laiy$e;

    if-eqz v0, :cond_0

    :goto_0
    invoke-direct {v2, p0, v0}, Laiy$e;-><init>(Lcgb$c;Ljava/lang/String;)V

    return-object v2

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Laiv;)V
    .locals 8

    .prologue
    .line 78
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 80
    invoke-virtual {p1}, Laiv;->a()Lcfq;

    move-result-object v0

    .line 85
    if-nez v0, :cond_0

    .line 86
    iget-object v0, p0, Laiy;->a:Landroid/widget/ListView;

    iget-object v1, p0, Laiy;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 148
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-object v1, v0, Lcfq;->f:Lcgb;

    iget-object v1, v1, Lcgb;->b:Lcgb$d;

    iget-object v3, v1, Lcgb$d;->c:Ljava/util/Map;

    .line 95
    iget-object v1, v0, Lcfq;->f:Lcgb;

    .line 96
    iget-object v4, v0, Lcfq;->e:Ljava/util/Map;

    .line 98
    iget-object v0, v1, Lcgb;->b:Lcgb$d;

    iget-object v0, v0, Lcgb$d;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgb$c;

    .line 99
    iget-object v1, v0, Lcgb$c;->e:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 101
    invoke-static {v0, v3, v4}, Laiy;->a(Lcgb$c;Ljava/util/Map;Ljava/util/Map;)Laiy$e;

    move-result-object v0

    .line 100
    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1

    .line 105
    :cond_2
    iget-object v6, v0, Lcgb$c;->e:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_3
    :goto_2
    packed-switch v1, :pswitch_data_0

    .line 122
    invoke-static {v0, v3, v4}, Laiy;->a(Lcgb$c;Ljava/util/Map;Ljava/util/Map;)Laiy$e;

    move-result-object v0

    .line 121
    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1

    .line 105
    :sswitch_0
    const-string/jumbo v7, "vit-only"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v1, 0x0

    goto :goto_2

    :sswitch_1
    const-string/jumbo v7, "vit-off"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v1, 0x1

    goto :goto_2

    .line 107
    :pswitch_0
    iget-boolean v1, p0, Laiy;->c:Z

    if-eqz v1, :cond_1

    .line 109
    invoke-static {v0, v3, v4}, Laiy;->a(Lcgb$c;Ljava/util/Map;Ljava/util/Map;)Laiy$e;

    move-result-object v0

    .line 108
    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1

    .line 114
    :pswitch_1
    iget-boolean v1, p0, Laiy;->c:Z

    if-nez v1, :cond_1

    .line 116
    invoke-static {v0, v3, v4}, Laiy;->a(Lcgb$c;Ljava/util/Map;Ljava/util/Map;)Laiy$e;

    move-result-object v0

    .line 115
    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1

    .line 127
    :cond_4
    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->h()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 128
    iget-object v0, p0, Laiy;->a:Landroid/widget/ListView;

    iget-object v1, p0, Laiy;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    goto :goto_0

    .line 132
    :cond_5
    iget-object v0, p0, Laiy;->f:Laiy$d;

    invoke-virtual {v0}, Laiy$d;->k()Lcjt;

    move-result-object v1

    new-instance v4, Lcbl;

    .line 133
    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-direct {v4, v0}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    .line 132
    invoke-interface {v1, v4}, Lcjt;->a(Lcbi;)Lcbi;

    .line 134
    iget-object v0, p0, Laiy;->a:Landroid/widget/ListView;

    new-instance v1, Laiy$c;

    iget-object v2, p0, Laiy;->b:Landroid/content/Context;

    invoke-direct {v1, v2, p1, v3}, Laiy$c;-><init>(Landroid/content/Context;Laiv;Ljava/util/Map;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 139
    new-instance v0, Lazu;

    iget-object v1, p0, Laiy;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lazu;-><init>(Landroid/content/Context;)V

    const v1, 0x7f140023

    iget-object v2, p0, Laiy;->d:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0, v1, v2}, Lazu;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    .line 141
    iget-object v0, p0, Laiy;->d:Lcom/twitter/internal/android/widget/ToolBar;

    const v1, 0x7f1308bb

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    .line 142
    invoke-virtual {v0}, Lazv;->e()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    .line 144
    new-instance v1, Laiy$a;

    iget-object v2, p0, Laiy;->b:Landroid/content/Context;

    iget-object v3, p0, Laiy;->a:Landroid/widget/ListView;

    invoke-direct {v1, v0, v2, v3}, Laiy$a;-><init>(Landroid/widget/Switch;Landroid/content/Context;Landroid/widget/ListView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 147
    invoke-virtual {p1}, Laiv;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    goto/16 :goto_0

    .line 105
    :sswitch_data_0
    .sparse-switch
        0x1bc4f1c3 -> :sswitch_1
        0x5cd965d8 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
