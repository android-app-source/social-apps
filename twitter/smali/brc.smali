.class public Lbrc;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:J

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Lcas;

.field private final g:J

.field private final h:J

.field private final i:Lcom/twitter/model/livevideo/BroadcastState;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:J

.field private final m:J

.field private final n:Ljava/lang/String;

.field private final o:Z

.field private final p:Z


# direct methods
.method public constructor <init>(JLcar;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, -0x1

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-wide p1, p0, Lbrc;->m:J

    .line 77
    const-string/jumbo v0, "event_id"

    invoke-static {v0, p3}, Lcom/twitter/library/card/y;->a(Ljava/lang/String;Lcar;)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lbrc;->a:J

    .line 78
    const-string/jumbo v0, "title"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbrc;->b:Ljava/lang/String;

    .line 79
    const-string/jumbo v0, "subtitle"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbrc;->c:Ljava/lang/String;

    .line 80
    const-string/jumbo v0, "event_url"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbrc;->d:Ljava/lang/String;

    .line 81
    const-string/jumbo v0, "stream_url"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbrc;->e:Ljava/lang/String;

    .line 82
    const-string/jumbo v0, "thumbnail"

    invoke-static {v0, p3}, Lcas;->a(Ljava/lang/String;Lcar;)Lcas;

    move-result-object v0

    iput-object v0, p0, Lbrc;->f:Lcas;

    .line 83
    const-string/jumbo v0, "media_id"

    invoke-static {v0, p3}, Lcom/twitter/library/card/y;->a(Ljava/lang/String;Lcar;)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lbrc;->g:J

    .line 84
    const-string/jumbo v0, "owner_user_id"

    invoke-static {v0, p3}, Lcom/twitter/library/card/y;->a(Ljava/lang/String;Lcar;)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lbrc;->h:J

    .line 86
    const-string/jumbo v0, "state"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "live"

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 87
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/livevideo/BroadcastState;->valueOf(Ljava/lang/String;)Lcom/twitter/model/livevideo/BroadcastState;

    move-result-object v0

    iput-object v0, p0, Lbrc;->i:Lcom/twitter/model/livevideo/BroadcastState;

    .line 88
    const-string/jumbo v0, "api"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbrc;->j:Ljava/lang/String;

    .line 89
    const-string/jumbo v0, "card_url"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbrc;->k:Ljava/lang/String;

    .line 90
    const-string/jumbo v0, "start_time"

    invoke-static {v0, p3}, Lcom/twitter/library/card/y;->a(Ljava/lang/String;Lcar;)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lbrc;->l:J

    .line 91
    const-string/jumbo v0, "remind_me_notification_id"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbrc;->n:Ljava/lang/String;

    .line 92
    const-string/jumbo v0, "remind_me_toggle_visible"

    .line 93
    invoke-static {v0, p3}, Lcom/twitter/library/card/g;->a(Ljava/lang/String;Lcar;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lbrc;->o:Z

    .line 94
    const-string/jumbo v0, "remind_me_subscribed"

    invoke-static {v0, p3}, Lcom/twitter/library/card/g;->a(Ljava/lang/String;Lcar;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lbrc;->p:Z

    .line 95
    return-void
.end method

.method public constructor <init>(Lcar;)V
    .locals 2

    .prologue
    .line 72
    const-wide/16 v0, -0x1

    invoke-direct {p0, v0, v1, p1}, Lbrc;-><init>(JLcar;)V

    .line 73
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lbrc;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lbrc;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 108
    iget-wide v0, p0, Lbrc;->a:J

    return-wide v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lbrc;->e:Ljava/lang/String;

    return-object v0
.end method

.method public e()Lcas;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lbrc;->f:Lcas;

    return-object v0
.end method

.method public f()Lcom/twitter/model/livevideo/BroadcastState;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lbrc;->i:Lcom/twitter/model/livevideo/BroadcastState;

    return-object v0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 136
    iget-wide v0, p0, Lbrc;->l:J

    return-wide v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lbrc;->j:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lbrc;->k:Ljava/lang/String;

    return-object v0
.end method

.method public j()Z
    .locals 4

    .prologue
    .line 150
    iget-wide v0, p0, Lbrc;->a:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()J
    .locals 2

    .prologue
    .line 154
    iget-wide v0, p0, Lbrc;->h:J

    return-wide v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lbrc;->n:Ljava/lang/String;

    return-object v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lbrc;->o:Z

    return v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lbrc;->p:Z

    return v0
.end method

.method public o()Ljava/lang/String;
    .locals 6

    .prologue
    .line 172
    iget-wide v0, p0, Lbrc;->a:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 173
    iget-wide v2, p0, Lbrc;->m:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 174
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lbrc;->m:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 176
    :cond_0
    return-object v0
.end method

.method public p()Lcom/twitter/model/livevideo/b;
    .locals 6

    .prologue
    .line 187
    iget-object v0, p0, Lbrc;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 188
    const/4 v0, 0x0

    .line 214
    :goto_0
    return-object v0

    .line 191
    :cond_0
    new-instance v0, Lcom/twitter/model/livevideo/a$b;

    invoke-direct {v0}, Lcom/twitter/model/livevideo/a$b;-><init>()V

    iget-wide v2, p0, Lbrc;->g:J

    .line 192
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/livevideo/a$b;->a(J)Lcom/twitter/model/livevideo/a$b;

    move-result-object v0

    iget-object v1, p0, Lbrc;->e:Ljava/lang/String;

    .line 193
    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/a$b;->a(Ljava/lang/String;)Lcom/twitter/model/livevideo/a$b;

    move-result-object v0

    iget-object v1, p0, Lbrc;->i:Lcom/twitter/model/livevideo/BroadcastState;

    .line 194
    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/a$b;->a(Lcom/twitter/model/livevideo/BroadcastState;)Lcom/twitter/model/livevideo/a$b;

    move-result-object v0

    .line 195
    invoke-virtual {v0}, Lcom/twitter/model/livevideo/a$b;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/livevideo/a;

    .line 197
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    .line 198
    iget-object v2, p0, Lbrc;->f:Lcas;

    if-eqz v2, :cond_1

    .line 199
    new-instance v1, Lcom/twitter/model/card/property/ImageSpec;

    iget-object v2, p0, Lbrc;->f:Lcas;

    iget v2, v2, Lcas;->b:I

    int-to-float v2, v2

    iget-object v3, p0, Lbrc;->f:Lcas;

    iget v3, v3, Lcas;->c:I

    int-to-float v3, v3

    invoke-direct {v1, v2, v3}, Lcom/twitter/model/card/property/ImageSpec;-><init>(FF)V

    .line 200
    iget-object v2, p0, Lbrc;->f:Lcas;

    iget-object v2, v2, Lcas;->a:Ljava/lang/String;

    iput-object v2, v1, Lcom/twitter/model/card/property/ImageSpec;->b:Ljava/lang/String;

    .line 201
    invoke-static {v1}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    move-object v2, v1

    .line 204
    :goto_1
    new-instance v1, Lcom/twitter/model/livevideo/c$a;

    invoke-direct {v1}, Lcom/twitter/model/livevideo/c$a;-><init>()V

    .line 205
    invoke-virtual {p0}, Lbrc;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/twitter/model/livevideo/c$a;->a(Ljava/lang/String;)Lcom/twitter/model/livevideo/c$a;

    move-result-object v1

    .line 206
    invoke-virtual {p0}, Lbrc;->n()Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/twitter/model/livevideo/c$a;->b(Z)Lcom/twitter/model/livevideo/c$a;

    move-result-object v1

    .line 207
    invoke-virtual {p0}, Lbrc;->m()Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/twitter/model/livevideo/c$a;->a(Z)Lcom/twitter/model/livevideo/c$a;

    move-result-object v1

    .line 208
    invoke-virtual {v1}, Lcom/twitter/model/livevideo/c$a;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/livevideo/c;

    .line 210
    new-instance v3, Lcom/twitter/model/livevideo/d$a;

    invoke-direct {v3}, Lcom/twitter/model/livevideo/d$a;-><init>()V

    .line 211
    invoke-virtual {v3, v1}, Lcom/twitter/model/livevideo/d$a;->a(Lcom/twitter/model/livevideo/c;)Lcom/twitter/model/livevideo/d$a;

    move-result-object v1

    .line 212
    invoke-virtual {v1}, Lcom/twitter/model/livevideo/d$a;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/livevideo/d;

    .line 214
    new-instance v3, Lcom/twitter/model/livevideo/b$a;

    invoke-direct {v3}, Lcom/twitter/model/livevideo/b$a;-><init>()V

    .line 215
    invoke-virtual {p0}, Lbrc;->c()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/twitter/model/livevideo/b$a;->a(J)Lcom/twitter/model/livevideo/b$a;

    move-result-object v3

    .line 216
    invoke-virtual {p0}, Lbrc;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/twitter/model/livevideo/b$a;->b(Ljava/lang/String;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v3

    .line 217
    invoke-virtual {p0}, Lbrc;->k()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/twitter/model/livevideo/b$a;->b(J)Lcom/twitter/model/livevideo/b$a;

    move-result-object v3

    .line 218
    invoke-virtual {p0}, Lbrc;->g()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/twitter/model/livevideo/b$a;->c(J)Lcom/twitter/model/livevideo/b$a;

    move-result-object v3

    .line 219
    invoke-virtual {v3, v2}, Lcom/twitter/model/livevideo/b$a;->a(Ljava/util/List;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v2

    .line 220
    invoke-virtual {v2, v0}, Lcom/twitter/model/livevideo/b$a;->a(Lcom/twitter/model/livevideo/a;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v0

    .line 221
    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/b$a;->a(Lcom/twitter/model/livevideo/d;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v0

    .line 222
    invoke-virtual {v0}, Lcom/twitter/model/livevideo/b$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/livevideo/b;

    goto/16 :goto_0

    :cond_1
    move-object v2, v1

    goto :goto_1
.end method
