.class public final Latc;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Latp;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Latc$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laog;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/base/BaseFragmentActivity;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laks",
            "<",
            "Lcom/twitter/android/ReportFlowWebViewActivity$a;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Latj;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Latc;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Latc;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Latc$a;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    sget-boolean v0, Latc;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 41
    :cond_0
    invoke-direct {p0, p1}, Latc;->a(Latc$a;)V

    .line 42
    return-void
.end method

.method synthetic constructor <init>(Latc$a;Latc$1;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Latc;-><init>(Latc$a;)V

    return-void
.end method

.method private a(Latc$a;)V
    .locals 1

    .prologue
    .line 51
    .line 52
    invoke-static {}, Laty;->c()Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Latc;->b:Lcta;

    .line 56
    invoke-static {p1}, Latc$a;->a(Latc$a;)Lant;

    move-result-object v0

    invoke-static {v0}, Lanu;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 55
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Latc;->c:Lcta;

    .line 58
    iget-object v0, p0, Latc;->c:Lcta;

    .line 60
    invoke-static {v0}, Lanv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 59
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Latc;->d:Lcta;

    .line 63
    iget-object v0, p0, Latc;->d:Lcta;

    .line 65
    invoke-static {v0}, Latw;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 64
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Latc;->e:Lcta;

    .line 68
    iget-object v0, p0, Latc;->e:Lcta;

    .line 69
    invoke-static {v0}, Latk;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Latc;->f:Lcta;

    .line 70
    return-void
.end method

.method public static c()Latc$a;
    .locals 2

    .prologue
    .line 45
    new-instance v0, Latc$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Latc$a;-><init>(Latc$1;)V

    return-object v0
.end method


# virtual methods
.method public a()Laog;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Latc;->b:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laog;

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    invoke-static {}, Ldagger/internal/f;->a()Ldagger/internal/c;

    move-result-object v0

    invoke-interface {v0}, Ldagger/internal/c;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public d()Latj;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Latc;->f:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Latj;

    return-object v0
.end method
