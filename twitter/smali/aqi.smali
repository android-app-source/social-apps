.class public final Laqi;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lauj",
        "<",
        "Lcom/twitter/database/model/f;",
        "Lcbi",
        "<",
        "Lcom/twitter/model/drafts/a;",
        ">;>;>;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/database/schema/DraftsSchema;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Laqi;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Laqi;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcom/twitter/database/schema/DraftsSchema;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-boolean v0, Laqi;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 23
    :cond_0
    iput-object p1, p0, Laqi;->b:Lcta;

    .line 24
    return-void
.end method

.method public static a(Lcta;)Ldagger/internal/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcom/twitter/database/schema/DraftsSchema;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Lauj",
            "<",
            "Lcom/twitter/database/model/f;",
            "Lcbi",
            "<",
            "Lcom/twitter/model/drafts/a;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 35
    new-instance v0, Laqi;

    invoke-direct {v0, p0}, Laqi;-><init>(Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lauj;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lauj",
            "<",
            "Lcom/twitter/database/model/f;",
            "Lcbi",
            "<",
            "Lcom/twitter/model/drafts/a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Laqi;->b:Lcta;

    .line 29
    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/database/schema/DraftsSchema;

    invoke-static {v0}, Laqh;->a(Lcom/twitter/database/schema/DraftsSchema;)Lauj;

    move-result-object v0

    const-string/jumbo v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 28
    invoke-static {v0, v1}, Ldagger/internal/e;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lauj;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Laqi;->a()Lauj;

    move-result-object v0

    return-object v0
.end method
