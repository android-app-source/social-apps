.class public Lbff;
.super Lbeq;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lbff;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lbeq;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 4

    .prologue
    .line 27
    invoke-virtual {p0}, Lbff;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 28
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "mutes/users/destroy"

    aput-object v3, v1, v2

    .line 29
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 30
    invoke-virtual {p0, v0}, Lbff;->a(Lcom/twitter/library/service/d$a;)V

    .line 31
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/twitter/model/core/TwitterUser;

    .line 39
    invoke-virtual {p0}, Lbff;->R()Lcom/twitter/library/provider/t;

    move-result-object v0

    .line 40
    iget-wide v4, v7, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 41
    invoke-virtual {p0}, Lbff;->S()Laut;

    move-result-object v6

    .line 42
    const/16 v1, 0x1a

    invoke-virtual {p0}, Lbff;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/service/v;->c:J

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/library/provider/t;->a(IJJLaut;)V

    .line 44
    const/16 v1, 0x26

    invoke-virtual {p0}, Lbff;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/service/v;->c:J

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/library/provider/t;->a(IJJLaut;)V

    .line 46
    const/16 v1, 0x2000

    invoke-virtual {v0, v4, v5, v1, v6}, Lcom/twitter/library/provider/t;->b(JILaut;)V

    .line 47
    invoke-virtual {v6}, Laut;->a()V

    .line 48
    iget-object v0, p2, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "muted_username"

    iget-object v2, v7, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 19
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbff;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return-object v0
.end method
