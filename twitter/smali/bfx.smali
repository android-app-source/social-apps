.class public Lbfx;
.super Lbfy;
.source "Twttr"


# instance fields
.field private final j:Z

.field private final k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JZII)V
    .locals 7

    .prologue
    .line 25
    const-class v0, Lbfx;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/service/v;

    invoke-direct {v3, p2}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    move-object v0, p0

    move-object v1, p1

    move-wide v4, p3

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lbfy;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;JI)V

    .line 26
    iput-boolean p5, p0, Lbfx;->j:Z

    .line 27
    iput p7, p0, Lbfx;->k:I

    .line 28
    return-void
.end method


# virtual methods
.method protected g()Lcom/twitter/library/service/d$a;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    .line 32
    invoke-super {p0}, Lbfy;->g()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 33
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string/jumbo v2, "timeline"

    aput-object v2, v1, v6

    const-string/jumbo v2, "user"

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    const-string/jumbo v2, "id"

    .line 34
    invoke-virtual {p0}, Lbfx;->C()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v1

    const-string/jumbo v2, "include_my_retweet"

    .line 35
    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v1

    const-string/jumbo v2, "earned"

    .line 36
    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v1

    const-string/jumbo v2, "exclude_pinned_tweets"

    .line 37
    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 39
    iget v1, p0, Lbfx;->k:I

    const/16 v2, 0x12

    if-ne v1, v2, :cond_1

    .line 41
    const-string/jumbo v1, "include_tweet_replies"

    invoke-virtual {v0, v1, v6}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 45
    :goto_0
    iget-boolean v1, p0, Lbfx;->j:Z

    if-eqz v1, :cond_0

    .line 46
    const-string/jumbo v1, "pc"

    invoke-virtual {v0, v1, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 48
    :cond_0
    return-object v0

    .line 43
    :cond_1
    const-string/jumbo v1, "include_tweet_replies"

    invoke-virtual {v0, v1, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    goto :goto_0
.end method

.method protected s()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lbfx;->k:I

    return v0
.end method
