.class public Lbsy;
.super Landroid/database/CursorWrapper;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/dms/m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/database/Cursor;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-static {}, Lcom/twitter/util/collection/i;->f()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lbsy;-><init>(Landroid/database/Cursor;Ljava/util/Map;Ljava/util/Map;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/dms/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 51
    iput-object p2, p0, Lbsy;->a:Ljava/util/Map;

    .line 52
    iput-object p3, p0, Lbsy;->b:Ljava/util/Map;

    .line 53
    return-void
.end method


# virtual methods
.method a()Lcom/twitter/model/dms/q;
    .locals 4

    .prologue
    .line 57
    new-instance v0, Lavo;

    invoke-direct {v0}, Lavo;-><init>()V

    invoke-virtual {p0}, Lbsy;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lavo;->a(Landroid/database/Cursor;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    .line 58
    invoke-virtual {v2}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 59
    iget-object v1, p0, Lbsy;->a:Ljava/util/Map;

    .line 60
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    new-instance v3, Lbsy$1;

    invoke-direct {v3, p0}, Lbsy$1;-><init>(Lbsy;)V

    .line 59
    invoke-static {v1, v3}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;Lcpp;)Ljava/util/List;

    move-result-object v3

    .line 69
    invoke-virtual {v2}, Lcom/twitter/util/collection/Pair;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/q$a;

    .line 71
    invoke-virtual {v1, v3}, Lcom/twitter/model/dms/q$a;->a(Ljava/util/List;)Lcom/twitter/model/dms/q$a;

    move-result-object v1

    iget-object v2, p0, Lbsy;->b:Ljava/util/Map;

    .line 72
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/m;

    invoke-virtual {v1, v0}, Lcom/twitter/model/dms/q$a;->a(Lcom/twitter/model/dms/m;)Lcom/twitter/model/dms/q$a;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lcom/twitter/model/dms/q$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/q;

    .line 70
    return-object v0
.end method
