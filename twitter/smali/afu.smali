.class public Lafu;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lafu$a;
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:Lcom/twitter/model/core/Tweet;

.field public final c:Lcax;

.field public final d:I

.field public final e:J

.field public final f:J


# direct methods
.method constructor <init>(Lafu$a;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p1}, Lafu$a;->a(Lafu$a;)I

    move-result v0

    iput v0, p0, Lafu;->a:I

    .line 37
    invoke-static {p1}, Lafu$a;->b(Lafu$a;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lafu$a;->b(Lafu$a;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet$a;->a()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lafu;->b:Lcom/twitter/model/core/Tweet;

    .line 38
    invoke-static {p1}, Lafu$a;->c(Lafu$a;)Lcax;

    move-result-object v0

    iput-object v0, p0, Lafu;->c:Lcax;

    .line 39
    invoke-static {p1}, Lafu$a;->d(Lafu$a;)I

    move-result v0

    iput v0, p0, Lafu;->d:I

    .line 40
    invoke-static {p1}, Lafu$a;->e(Lafu$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lafu;->e:J

    .line 41
    invoke-static {p1}, Lafu$a;->f(Lafu$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lafu;->f:J

    .line 42
    return-void

    .line 37
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 46
    if-ne p0, p1, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v0

    .line 49
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 50
    goto :goto_0

    .line 52
    :cond_3
    check-cast p1, Lafu;

    .line 53
    iget v2, p0, Lafu;->a:I

    iget v3, p1, Lafu;->a:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lafu;->d:I

    iget v3, p1, Lafu;->d:I

    if-ne v2, v3, :cond_4

    iget-wide v2, p0, Lafu;->e:J

    iget-wide v4, p1, Lafu;->e:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-wide v2, p0, Lafu;->f:J

    iget-wide v4, p1, Lafu;->f:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-object v2, p0, Lafu;->b:Lcom/twitter/model/core/Tweet;

    iget-object v3, p1, Lafu;->b:Lcom/twitter/model/core/Tweet;

    .line 55
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lafu;->c:Lcax;

    iget-object v3, p1, Lafu;->c:Lcax;

    .line 56
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 61
    iget v0, p0, Lafu;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p0, Lafu;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-wide v2, p0, Lafu;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p0, Lafu;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, p0, Lafu;->b:Lcom/twitter/model/core/Tweet;

    iget-object v5, p0, Lafu;->c:Lcax;

    invoke-static/range {v0 .. v5}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
