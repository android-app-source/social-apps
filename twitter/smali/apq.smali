.class public Lapq;
.super Lapt;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lapq$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lapt",
        "<",
        "Lcom/twitter/model/dms/ak;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>(Lapq$a;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lapt;-><init>(Lapn$a;)V

    .line 16
    return-void
.end method

.method synthetic constructor <init>(Lapq$a;Lapq$1;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lapq;-><init>(Lapq$a;)V

    return-void
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    invoke-static {p2}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 30
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    const v0, 0x7f0a02a0

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 43
    :goto_0
    return-object v0

    .line 34
    :cond_0
    if-eqz p3, :cond_1

    const v0, 0x7f0a030a

    .line 35
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const v0, 0x7f0a02fa

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    .line 36
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 39
    :cond_2
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 40
    const v0, 0x7f0a029f

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 43
    :cond_3
    if-eqz p3, :cond_4

    const v0, 0x7f0a0309

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    .line 44
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    const v0, 0x7f0a02f8

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    aput-object p2, v1, v3

    .line 45
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 20
    iget-object v1, p0, Lapq;->k:Landroid/widget/TextView;

    iget-object v2, p0, Lapq;->g:Landroid/content/Context;

    iget-object v0, p0, Lapq;->b:Lcom/twitter/model/dms/m;

    iget-object v3, v0, Lcom/twitter/model/dms/m;->b:Ljava/lang/String;

    iget-object v0, p0, Lapq;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/ak;

    invoke-virtual {v0}, Lcom/twitter/model/dms/ak;->d()Ljava/lang/String;

    move-result-object v0

    .line 21
    invoke-virtual {p0}, Lapq;->c()Z

    move-result v4

    .line 20
    invoke-static {v2, v3, v0, v4}, Lapq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 22
    invoke-virtual {p0}, Lapq;->d()V

    .line 23
    invoke-virtual {p0}, Lapq;->b()V

    .line 24
    return-void
.end method
