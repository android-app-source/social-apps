.class public Lbfy$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbfy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbfy$a$a;
    }
.end annotation


# instance fields
.field public final a:Z

.field public final b:I

.field public final c:Z

.field public final d:Z

.field public final e:Z

.field public final f:Lcom/twitter/model/timeline/u;

.field public final g:I


# direct methods
.method private constructor <init>(Lbfy$a$a;)V
    .locals 2

    .prologue
    .line 328
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 329
    iget-boolean v0, p1, Lbfy$a$a;->a:Z

    iput-boolean v0, p0, Lbfy$a;->a:Z

    .line 330
    iget v0, p1, Lbfy$a$a;->b:I

    iput v0, p0, Lbfy$a;->b:I

    .line 331
    iget-boolean v0, p1, Lbfy$a$a;->c:Z

    iput-boolean v0, p0, Lbfy$a;->c:Z

    .line 332
    iget-boolean v0, p1, Lbfy$a$a;->d:Z

    iput-boolean v0, p0, Lbfy$a;->d:Z

    .line 333
    iget-boolean v0, p1, Lbfy$a$a;->e:Z

    iput-boolean v0, p0, Lbfy$a;->e:Z

    .line 334
    iget-object v0, p1, Lbfy$a$a;->f:Lcom/twitter/model/timeline/u;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lbfy$a$a;->f:Lcom/twitter/model/timeline/u;

    iget-object v0, v0, Lcom/twitter/model/timeline/u;->b:Lcom/twitter/model/timeline/AlertType;

    sget-object v1, Lcom/twitter/model/timeline/AlertType;->b:Lcom/twitter/model/timeline/AlertType;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lbfy$a$a;->f:Lcom/twitter/model/timeline/u;

    :goto_0
    iput-object v0, p0, Lbfy$a;->f:Lcom/twitter/model/timeline/u;

    .line 337
    iget v0, p1, Lbfy$a$a;->g:I

    iput v0, p0, Lbfy$a;->g:I

    .line 338
    return-void

    .line 334
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Lbfy$a$a;Lbfy$1;)V
    .locals 0

    .prologue
    .line 318
    invoke-direct {p0, p1}, Lbfy$a;-><init>(Lbfy$a$a;)V

    return-void
.end method
