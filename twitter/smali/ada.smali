.class public abstract Lada;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/twitter/model/moments/viewmodels/a;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ")",
            "Ljava/util/List",
            "<",
            "Lada;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 19
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/a;->c()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 20
    if-eqz v0, :cond_0

    instance-of v2, v0, Lcom/twitter/model/moments/viewmodels/k;

    if-eqz v2, :cond_2

    .line 21
    :cond_0
    new-instance v2, Ladi;

    if-eqz v0, :cond_1

    .line 22
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/k;

    .line 23
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ladi;-><init>(Lcom/twitter/model/moments/viewmodels/k;Lcom/twitter/model/moments/Moment;)V

    .line 21
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/a;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    .line 29
    new-instance v3, Ladc;

    invoke-direct {v3, v0}, Ladc;-><init>(Lcom/twitter/model/moments/viewmodels/g;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 22
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 25
    :cond_2
    new-instance v2, Lade;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/twitter/model/moments/viewmodels/MomentPage;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v3}, Lcom/twitter/model/moments/viewmodels/g;->a([Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/moments/viewmodels/g;

    move-result-object v0

    invoke-direct {v2, v0}, Lade;-><init>(Lcom/twitter/model/moments/viewmodels/g;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 31
    :cond_3
    return-object v1
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract b()Lcom/twitter/model/moments/viewmodels/g;
.end method
