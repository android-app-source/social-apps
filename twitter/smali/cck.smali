.class public abstract Lcck;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcck$b;,
        Lcck$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcck;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 27
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/twitter/util/serialization/j;

    const/4 v1, 0x0

    const-class v2, Lccr;

    new-instance v3, Lccr$b;

    invoke-direct {v3}, Lccr$b;-><init>()V

    .line 28
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lccu;

    new-instance v3, Lccu$b;

    invoke-direct {v3}, Lccu$b;-><init>()V

    .line 30
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lcco;

    new-instance v3, Lcco$b;

    invoke-direct {v3}, Lcco$b;-><init>()V

    .line 32
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lccn;

    new-instance v3, Lccn$b;

    invoke-direct {v3}, Lccn$b;-><init>()V

    .line 34
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    .line 27
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a([Lcom/twitter/util/serialization/j;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    sput-object v0, Lcck;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method protected constructor <init>(Lcck$a;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {p1}, Lcck$a;->a(Lcck$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcck;->b:Ljava/lang/String;

    .line 41
    return-void
.end method

.method private a(Lcck;)Z
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcck;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcck;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcck;->b:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 55
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcck;

    if-eqz v0, :cond_1

    check-cast p1, Lcck;

    invoke-direct {p0, p1}, Lcck;->a(Lcck;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p0}, Lcck;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
