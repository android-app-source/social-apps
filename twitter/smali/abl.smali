.class public Labl;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lacg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lacg",
        "<",
        "Lcom/twitter/model/moments/viewmodels/MomentPage;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Labk;


# direct methods
.method constructor <init>(Labk;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Labl;->a:Labk;

    .line 33
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Lcom/twitter/model/moments/viewmodels/MomentPage;)Labl;
    .locals 2

    .prologue
    .line 24
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->f()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/Moment;

    invoke-static {v0}, Lbsd;->a(Lcom/twitter/model/moments/Moment;)Z

    move-result v0

    .line 25
    if-eqz v0, :cond_0

    .line 26
    invoke-static {p0}, Labk;->a(Landroid/view/LayoutInflater;)Labk;

    move-result-object v0

    .line 28
    :goto_0
    new-instance v1, Labl;

    invoke-direct {v1, v0}, Labl;-><init>(Labk;)V

    return-object v1

    .line 27
    :cond_0
    invoke-static {p0}, Labk;->b(Landroid/view/LayoutInflater;)Labk;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/core/Tweet;)V
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->f()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/Moment;

    .line 48
    invoke-static {v0}, Lbsd;->a(Lcom/twitter/model/moments/Moment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    iget-object v0, p0, Labl;->a:Labk;

    invoke-virtual {v0}, Labk;->g()V

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    invoke-static {}, Lbsd;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Labl;->a:Labk;

    invoke-virtual {v0}, Labk;->b()V

    .line 52
    iget-object v0, p0, Labl;->a:Labk;

    invoke-virtual {v0}, Labk;->c()V

    goto :goto_0
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Labl;->a:Labk;

    invoke-virtual {v0}, Labk;->a()Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method public b()Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Lacg",
            "<",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 59
    invoke-static {p0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public d()Landroid/view/View;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Labl;->a:Labk;

    invoke-virtual {v0}, Labk;->a()Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method public e()Labk;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Labl;->a:Labk;

    return-object v0
.end method
