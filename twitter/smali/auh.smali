.class public Lauh;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lauj;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lauh$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lauj",
        "<",
        "Lapb;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Laui;

.field private final b:Landroid/content/ContentResolver;

.field private final c:Lrx/f;

.field private final d:Lrx/f;

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private volatile f:Z


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 3

    .prologue
    .line 55
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v0

    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v1

    new-instance v2, Laul;

    invoke-direct {v2, p1}, Laul;-><init>(Landroid/content/ContentResolver;)V

    invoke-direct {p0, v0, v1, p1, v2}, Lauh;-><init>(Lrx/f;Lrx/f;Landroid/content/ContentResolver;Laui;)V

    .line 57
    return-void
.end method

.method constructor <init>(Lrx/f;Lrx/f;Landroid/content/ContentResolver;Laui;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lauh;->c:Lrx/f;

    .line 63
    iput-object p2, p0, Lauh;->d:Lrx/f;

    .line 64
    iput-object p3, p0, Lauh;->b:Landroid/content/ContentResolver;

    .line 65
    iput-object p4, p0, Lauh;->a:Laui;

    .line 66
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lauh;->e:Ljava/util/Set;

    .line 67
    return-void
.end method

.method static synthetic a(Lauh;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lauh;->f:Z

    return v0
.end method

.method static synthetic b(Lauh;)Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lauh;->b:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic c(Lauh;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lauh;->e:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public a(Lapb;)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lapb;",
            ")",
            "Lrx/c",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 72
    iget-object v0, p0, Lauh;->a:Laui;

    iget-object v2, p1, Lapb;->d:Landroid/net/Uri;

    .line 73
    invoke-interface {v0, v2}, Laui;->a(Landroid/net/Uri;)Lrx/c;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Ljava/lang/Void;

    .line 74
    invoke-virtual {v2, v0}, Lrx/c;->d(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    iget-object v2, p0, Lauh;->c:Lrx/f;

    .line 75
    invoke-virtual {v0, v2}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    iget-object v2, p0, Lauh;->d:Lrx/f;

    .line 77
    invoke-virtual {v0, v2}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v2, Lauh$1;

    invoke-direct {v2, p0, p1}, Lauh$1;-><init>(Lauh;Lapb;)V

    .line 78
    invoke-virtual {v0, v2}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v2, p0, Lauh;->c:Lrx/f;

    .line 98
    invoke-virtual {v0, v2}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v2, Lauh$a;

    invoke-direct {v2, p0, v1}, Lauh$a;-><init>(Lauh;Lauh$1;)V

    .line 99
    invoke-virtual {v0, v2}, Lrx/c;->a(Lrx/c$b;)Lrx/c;

    move-result-object v0

    .line 72
    return-object v0
.end method

.method public synthetic a_(Ljava/lang/Object;)Lrx/c;
    .locals 1

    .prologue
    .line 35
    check-cast p1, Lapb;

    invoke-virtual {p0, p1}, Lauh;->a(Lapb;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 3

    .prologue
    .line 104
    iget-object v1, p0, Lauh;->e:Ljava/util/Set;

    monitor-enter v1

    .line 105
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lauh;->f:Z

    .line 106
    iget-object v0, p0, Lauh;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 107
    invoke-static {v0}, Lcqc;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 109
    :cond_0
    :try_start_1
    iget-object v0, p0, Lauh;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 110
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111
    return-void
.end method
