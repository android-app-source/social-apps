.class public Lbca;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcom/twitter/model/json/core/JsonTwitterCursorArray;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Z

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lbca;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 28
    const/16 v0, 0x64

    iput v0, p0, Lbca;->a:I

    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbca;->b:Z

    .line 34
    iput-object p3, p0, Lbca;->c:Ljava/lang/String;

    .line 35
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 4

    .prologue
    .line 40
    invoke-virtual {p0}, Lbca;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->a:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 41
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "contacts"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "users"

    aput-object v3, v1, v2

    .line 42
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "include_relationships"

    iget-boolean v2, p0, Lbca;->b:Z

    .line 43
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "count"

    iget v2, p0, Lbca;->a:I

    int-to-long v2, v2

    .line 44
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lbca;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 46
    const-string/jumbo v1, "cursor"

    iget-object v2, p0, Lbca;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 48
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/json/core/JsonTwitterCursorArray;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 67
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 68
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/core/JsonTwitterCursorArray;

    .line 69
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/twitter/model/json/core/JsonTwitterCursorArray;->a:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 70
    iget-object v1, v0, Lcom/twitter/model/json/core/JsonTwitterCursorArray;->a:Ljava/util/List;

    .line 71
    iget-object v0, v0, Lcom/twitter/model/json/core/JsonTwitterCursorArray;->b:Ljava/lang/String;

    .line 72
    if-eqz v0, :cond_0

    .line 73
    iget-object v2, p0, Lbca;->o:Landroid/os/Bundle;

    const-string/jumbo v3, "next_cursor"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :cond_0
    invoke-virtual {p0}, Lbca;->S()Laut;

    move-result-object v10

    .line 76
    invoke-virtual {p0}, Lbca;->R()Lcom/twitter/library/provider/t;

    move-result-object v0

    invoke-virtual {p0}, Lbca;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/service/v;->c:J

    const/16 v4, 0x27

    const-wide/16 v5, -0x1

    const/4 v9, 0x1

    move-object v8, v7

    invoke-virtual/range {v0 .. v10}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;ZLaut;)I

    move-result v0

    .line 78
    invoke-virtual {v10}, Laut;->a()V

    .line 79
    iget-object v1, p0, Lbca;->o:Landroid/os/Bundle;

    const-string/jumbo v2, "count"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 82
    :cond_1
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 25
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbca;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/api/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/json/core/JsonTwitterCursorArray;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    const-class v0, Lcom/twitter/model/json/core/JsonTwitterCursorArray;

    const-class v1, Lcom/twitter/model/core/z;

    invoke-static {v0, v1}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lbca;->b()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
