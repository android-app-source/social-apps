.class public Laqa;
.super Lapv;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laqa$b;,
        Laqa$a;,
        Laqa$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lapv",
        "<",
        "Laqa$c;",
        ">;",
        "Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;"
    }
.end annotation


# instance fields
.field private final A:I

.field private final B:Landroid/view/View;

.field private final C:Lcom/twitter/android/media/widget/AttachmentMediaView;

.field private final D:Lcom/twitter/media/ui/AnimatingProgressBar;

.field private final E:Ljava/lang/String;

.field private final F:Landroid/text/style/CharacterStyle;

.field private final v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

.field private final w:Z

.field private final x:Z

.field private final y:Lcom/twitter/model/dms/n;

.field private final z:Z


# direct methods
.method private constructor <init>(Laqa$a;)V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lapv;-><init>(Lapv$a;)V

    .line 70
    invoke-static {p1}, Laqa$a;->a(Laqa$a;)Z

    move-result v0

    iput-boolean v0, p0, Laqa;->w:Z

    .line 71
    invoke-static {p1}, Laqa$a;->b(Laqa$a;)Lcom/twitter/model/dms/n;

    move-result-object v0

    iput-object v0, p0, Laqa;->y:Lcom/twitter/model/dms/n;

    .line 72
    invoke-static {p1}, Laqa$a;->c(Laqa$a;)Z

    move-result v0

    iput-boolean v0, p0, Laqa;->z:Z

    .line 73
    invoke-static {p1}, Laqa$a;->d(Laqa$a;)I

    move-result v0

    iput v0, p0, Laqa;->A:I

    .line 75
    iget-object v0, p1, Laqa$a;->a:Lapn$b;

    check-cast v0, Laqa$c;

    .line 76
    invoke-static {v0}, Laqa$c;->a(Laqa$c;)Lcom/twitter/app/dm/widget/SentMessageBylineView;

    move-result-object v1

    iput-object v1, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    .line 77
    iget-object v1, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    instance-of v1, v1, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;

    iput-boolean v1, p0, Laqa;->x:Z

    .line 79
    invoke-static {v0}, Laqa$c;->b(Laqa$c;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Laqa;->B:Landroid/view/View;

    .line 80
    invoke-static {v0}, Laqa$c;->c(Laqa$c;)Lcom/twitter/android/media/widget/AttachmentMediaView;

    move-result-object v1

    iput-object v1, p0, Laqa;->C:Lcom/twitter/android/media/widget/AttachmentMediaView;

    .line 81
    invoke-static {v0}, Laqa$c;->d(Laqa$c;)Lcom/twitter/media/ui/AnimatingProgressBar;

    move-result-object v0

    iput-object v0, p0, Laqa;->D:Lcom/twitter/media/ui/AnimatingProgressBar;

    .line 83
    iget-object v0, p0, Laqa;->g:Landroid/content/Context;

    const v1, 0x7f0a0275

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laqa;->E:Ljava/lang/String;

    .line 84
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iput-object v0, p0, Laqa;->F:Landroid/text/style/CharacterStyle;

    .line 85
    return-void
.end method

.method synthetic constructor <init>(Laqa$a;Laqa$1;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Laqa;-><init>(Laqa$a;)V

    return-void
.end method

.method static synthetic a(Laqa;)Lcom/twitter/app/dm/widget/SentMessageBylineView;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    return-object v0
.end method

.method private a(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 253
    iget-object v0, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->setDraftStatusColor(I)V

    .line 254
    return-void
.end method

.method private a(Lcom/twitter/model/drafts/DraftAttachment;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 299
    iget-object v0, p0, Laqa;->C:Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {v0, v3}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setClickable(Z)V

    .line 301
    const/4 v0, 0x3

    .line 302
    invoke-virtual {p1, v0}, Lcom/twitter/model/drafts/DraftAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableMedia;

    .line 303
    iget-object v1, p0, Laqa;->C:Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {v0}, Lcom/twitter/model/media/EditableMedia;->a()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setAspectRatio(F)V

    .line 305
    invoke-direct {p0, v0}, Laqa;->a(Lcom/twitter/model/media/EditableMedia;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Laqa;->C:Lcom/twitter/android/media/widget/AttachmentMediaView;

    new-instance v1, Lcom/twitter/android/media/selection/MediaAttachment;

    invoke-direct {v1, p1}, Lcom/twitter/android/media/selection/MediaAttachment;-><init>(Lcom/twitter/model/drafts/DraftAttachment;)V

    sget-object v2, Lcom/twitter/android/composer/ComposerType;->c:Lcom/twitter/android/composer/ComposerType;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/media/widget/AttachmentMediaView;->a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/composer/ComposerType;)V

    .line 309
    :cond_0
    iget-boolean v0, p0, Laqa;->w:Z

    if-nez v0, :cond_1

    .line 310
    iget-object v0, p0, Laqa;->D:Lcom/twitter/media/ui/AnimatingProgressBar;

    invoke-virtual {v0, p2}, Lcom/twitter/media/ui/AnimatingProgressBar;->setProgress(I)V

    .line 311
    iget-object v0, p0, Laqa;->D:Lcom/twitter/media/ui/AnimatingProgressBar;

    invoke-virtual {v0, v3}, Lcom/twitter/media/ui/AnimatingProgressBar;->setVisibility(I)V

    .line 314
    :cond_1
    iget-object v0, p0, Laqa;->B:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 316
    invoke-virtual {p0}, Laqa;->l()V

    .line 317
    iget-object v0, p0, Laqa;->r:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 318
    iget-object v0, p0, Laqa;->s:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 320
    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->v()Ljava/lang/String;

    move-result-object v0

    .line 321
    if-eqz v0, :cond_2

    .line 322
    iget-object v1, p0, Laqa;->m:Lcom/twitter/app/dm/v;

    invoke-interface {v1, v0, p1}, Lcom/twitter/app/dm/v;->a(Ljava/lang/String;Lcom/twitter/model/drafts/DraftAttachment;)V

    .line 324
    :cond_2
    return-void
.end method

.method private a(Lcom/twitter/model/media/EditableMedia;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 333
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->c()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Laqa;->C:Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {v2}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getAttachmentMediaKey()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 338
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Laqa;->C:Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {v1}, Lcom/twitter/android/media/widget/AttachmentMediaView;->getEditableMedia()Lcom/twitter/model/media/EditableMedia;

    move-result-object v1

    new-array v0, v0, [Lcom/twitter/model/media/EditableMedia;

    const/4 v2, 0x0

    aput-object p1, v0, v2

    invoke-static {v1, v0}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    new-instance v1, Laqa$2;

    invoke-direct {v1, p0}, Laqa$2;-><init>(Laqa;)V

    invoke-static {v0, v1}, Lcpt;->b(Ljava/lang/Iterable;Lcpv;)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic b(Laqa;)Landroid/text/style/CharacterStyle;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Laqa;->F:Landroid/text/style/CharacterStyle;

    return-object v0
.end method

.method private b(Lcom/twitter/model/dms/e;Lcom/twitter/model/dms/e;)Z
    .locals 6

    .prologue
    .line 103
    iget-object v0, p0, Laqa;->y:Lcom/twitter/model/dms/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqa;->y:Lcom/twitter/model/dms/n;

    .line 104
    invoke-virtual {p1}, Lcom/twitter/model/dms/e;->q()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/twitter/model/dms/e;->a()J

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/twitter/model/dms/n;->b(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 103
    :goto_0
    return v0

    .line 104
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Laqa;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Laqa;->E:Ljava/lang/String;

    return-object v0
.end method

.method private c(Z)V
    .locals 3

    .prologue
    const v2, 0x7f0a0274

    .line 213
    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->d()Z

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 214
    iget-object v1, p0, Laqa;->m:Lcom/twitter/app/dm/v;

    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->v()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/twitter/app/dm/v;->g(Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->c()V

    .line 218
    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/r;

    invoke-virtual {v0}, Lcom/twitter/model/dms/r;->g()I

    move-result v0

    .line 220
    if-nez v0, :cond_2

    .line 221
    iget-boolean v0, p0, Laqa;->w:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->D()Z

    move-result v0

    if-nez v0, :cond_1

    .line 222
    if-nez p1, :cond_0

    .line 223
    invoke-direct {p0}, Laqa;->q()V

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    invoke-virtual {p0}, Laqa;->o()Lcom/twitter/app/dm/widget/SentMessageBylineView;

    move-result-object v0

    iget-object v1, p0, Laqa;->g:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->setDraftStatusText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 229
    :cond_2
    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 230
    iget-boolean v0, p0, Laqa;->w:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->D()Z

    move-result v0

    if-nez v0, :cond_4

    .line 231
    if-nez p1, :cond_3

    .line 232
    invoke-direct {p0}, Laqa;->q()V

    .line 240
    :cond_3
    :goto_1
    iget-object v0, p0, Laqa;->k:Landroid/widget/TextView;

    iget-object v1, p0, Laqa;->g:Landroid/content/Context;

    const v2, 0x7f110086

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 241
    const v0, 0x7f020128

    const v1, 0x7f110085

    invoke-virtual {p0, v0, v1}, Laqa;->a(II)V

    .line 242
    const v0, 0x7f1100e4

    invoke-direct {p0, v0}, Laqa;->a(I)V

    goto :goto_0

    .line 235
    :cond_4
    invoke-virtual {p0}, Laqa;->o()Lcom/twitter/app/dm/widget/SentMessageBylineView;

    move-result-object v0

    iget-object v1, p0, Laqa;->g:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->setDraftStatusText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 238
    :cond_5
    invoke-virtual {p0}, Laqa;->o()Lcom/twitter/app/dm/widget/SentMessageBylineView;

    move-result-object v0

    iget-object v1, p0, Laqa;->g:Landroid/content/Context;

    const v2, 0x7f0a0273

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->setDraftStatusText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private p()I
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    .line 189
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/r;

    invoke-virtual {v0}, Lcom/twitter/model/dms/r;->g()I

    move-result v0

    .line 188
    :goto_0
    return v0

    .line 189
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private q()V
    .locals 3

    .prologue
    .line 194
    new-instance v0, Laqa$b;

    invoke-direct {v0, p0}, Laqa$b;-><init>(Laqa;)V

    .line 195
    iget-object v1, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    const v2, 0x7f130027

    invoke-virtual {v1, v2, v0}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->setTag(ILjava/lang/Object;)V

    .line 196
    iget-object v1, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->post(Ljava/lang/Runnable;)Z

    .line 197
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Laqa;->l:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    invoke-super {p0}, Lapv;->a()V

    .line 91
    return-void
.end method

.method public a(JZ)V
    .locals 2

    .prologue
    const v1, 0x7f110089

    .line 258
    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->x()Z

    move-result v0

    if-nez v0, :cond_0

    .line 259
    if-eqz p3, :cond_1

    .line 260
    const v0, 0x7f020129

    invoke-virtual {p0, v0, v1}, Laqa;->a(II)V

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 262
    :cond_1
    const v0, 0x7f020128

    invoke-virtual {p0, v0, v1}, Laqa;->a(II)V

    goto :goto_0
.end method

.method public a(JZLcom/twitter/app/dm/widget/SentMessageBylineView;)V
    .locals 2

    .prologue
    const v1, 0x7f1100bc

    .line 269
    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->x()Z

    move-result v0

    if-nez v0, :cond_0

    .line 270
    if-eqz p3, :cond_1

    .line 271
    const v0, 0x7f020129

    invoke-virtual {p0, v0, v1}, Laqa;->a(II)V

    .line 276
    :cond_0
    :goto_0
    return-void

    .line 273
    :cond_1
    const v0, 0x7f020128

    invoke-virtual {p0, v0, v1}, Laqa;->a(II)V

    goto :goto_0
.end method

.method a(Lccf;)V
    .locals 2

    .prologue
    .line 350
    invoke-super {p0, p1}, Lapv;->a(Lccf;)V

    .line 352
    iget-object v1, p0, Laqa;->m:Lcom/twitter/app/dm/v;

    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->v()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/twitter/app/dm/v;->h(Ljava/lang/String;)Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v0

    .line 353
    if-eqz v0, :cond_0

    .line 354
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Laqa;->a(Lcom/twitter/model/drafts/DraftAttachment;I)V

    .line 356
    iget-object v0, p0, Laqa;->t:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 357
    iget-object v0, p0, Laqa;->t:Lcom/twitter/media/ui/image/MediaImageView;

    new-instance v1, Laqa$3;

    invoke-direct {v1, p0}, Laqa$3;-><init>(Laqa;)V

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setOnImageLoadedListener(Lcom/twitter/media/ui/image/BaseMediaImageView$b;)V

    .line 367
    :goto_0
    return-void

    .line 365
    :cond_0
    iget-object v0, p0, Laqa;->B:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method a(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 109
    iget-object v3, p0, Laqa;->l:Landroid/view/View;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Laqa;->k:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 111
    return-void

    :cond_0
    move v0, v2

    .line 109
    goto :goto_0

    :cond_1
    move v1, v2

    .line 110
    goto :goto_1
.end method

.method a(Lcom/twitter/model/dms/e;Lcom/twitter/model/dms/e;)Z
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Laqa;->b(Lcom/twitter/model/dms/e;Lcom/twitter/model/dms/e;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    invoke-super {p0, p1, p2}, Lapv;->a(Lcom/twitter/model/dms/e;Lcom/twitter/model/dms/e;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 97
    :goto_0
    return v0

    .line 98
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Z)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 122
    const v0, 0x7f110177

    invoke-direct {p0, v0}, Laqa;->a(I)V

    .line 125
    iget-boolean v0, p0, Laqa;->w:Z

    if-eqz v0, :cond_3

    .line 126
    iget-object v0, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    const v1, 0x7f130027

    .line 127
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 128
    if-nez v0, :cond_1

    move v1, v2

    .line 141
    :goto_0
    iget-object v3, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    const v4, 0x7f130024

    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->v()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->setTag(ILjava/lang/Object;)V

    .line 142
    iget-object v3, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    const v4, 0x7f130025

    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->o()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->setTag(ILjava/lang/Object;)V

    .line 143
    iget-object v0, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    const v3, 0x7f130022

    invoke-direct {p0}, Laqa;->p()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->setTag(ILjava/lang/Object;)V

    move v8, v1

    .line 148
    :goto_1
    iget-object v0, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->setVisibility(I)V

    .line 150
    iget-boolean v0, p0, Laqa;->x:Z

    if-eqz v0, :cond_4

    .line 151
    iget-object v0, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;

    iget-object v1, p0, Laqa;->y:Lcom/twitter/model/dms/n;

    iget-object v2, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v2, Lcom/twitter/model/dms/e;

    iget-boolean v3, p0, Laqa;->z:Z

    iget v4, p0, Laqa;->A:I

    iget-boolean v7, p0, Laqa;->q:Z

    move v5, p1

    move-object v6, p0

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->a(Lcom/twitter/model/dms/n;Lcom/twitter/model/dms/e;ZIZLcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;Z)V

    .line 154
    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 155
    new-instance v0, Laqa$1;

    invoke-direct {v0, p0}, Laqa$1;-><init>(Laqa;)V

    .line 161
    iget-object v1, p0, Laqa;->l:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    iget-object v1, p0, Laqa;->u:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    :cond_0
    :goto_2
    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 169
    invoke-direct {p0, v8}, Laqa;->c(Z)V

    .line 174
    :goto_3
    return-void

    .line 132
    :cond_1
    invoke-virtual {p0}, Laqa;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 133
    const/4 v0, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 136
    :cond_2
    iget-object v1, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->removeCallbacks(Ljava/lang/Runnable;)Z

    move v1, v2

    .line 137
    goto/16 :goto_0

    :cond_3
    move v8, v2

    .line 145
    goto :goto_1

    .line 165
    :cond_4
    iget-object v0, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    iget-boolean v1, p0, Laqa;->z:Z

    invoke-virtual {v0, v1, p1}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->a(ZZ)V

    goto :goto_2

    .line 171
    :cond_5
    invoke-super {p0, p1}, Lapv;->b(Z)V

    .line 172
    invoke-virtual {p0}, Laqa;->e()V

    goto :goto_3
.end method

.method d()V
    .locals 3

    .prologue
    .line 115
    const v1, 0x7f020128

    iget-boolean v0, p0, Laqa;->z:Z

    if-eqz v0, :cond_0

    const v0, 0x7f1100bc

    :goto_0
    invoke-virtual {p0, v1, v0}, Laqa;->a(II)V

    .line 117
    iget-object v0, p0, Laqa;->k:Landroid/widget/TextView;

    iget-object v1, p0, Laqa;->g:Landroid/content/Context;

    const v2, 0x7f11008a

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 118
    return-void

    .line 115
    :cond_0
    const v0, 0x7f110089

    goto :goto_0
.end method

.method e()V
    .locals 2

    .prologue
    .line 201
    invoke-super {p0}, Lapv;->e()V

    .line 203
    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->v()Ljava/lang/String;

    move-result-object v0

    .line 204
    if-eqz v0, :cond_0

    iget-object v1, p0, Laqa;->m:Lcom/twitter/app/dm/v;

    invoke-interface {v1, v0}, Lcom/twitter/app/dm/v;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 205
    :goto_0
    if-eqz v0, :cond_1

    .line 206
    iget-object v0, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->d()V

    .line 210
    :goto_1
    return-void

    .line 204
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 208
    :cond_1
    iget-object v0, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->b()V

    goto :goto_1
.end method

.method synthetic h()Lcom/twitter/app/dm/widget/MessageBylineView;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Laqa;->o()Lcom/twitter/app/dm/widget/SentMessageBylineView;

    move-result-object v0

    return-object v0
.end method

.method k()V
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->D()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 288
    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/r;

    .line 289
    invoke-virtual {v0}, Lcom/twitter/model/dms/r;->h()Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v1

    .line 290
    if-eqz v1, :cond_0

    .line 291
    invoke-virtual {v0}, Lcom/twitter/model/dms/r;->G()I

    move-result v0

    invoke-direct {p0, v1, v0}, Laqa;->a(Lcom/twitter/model/drafts/DraftAttachment;I)V

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 294
    :cond_1
    invoke-super {p0}, Lapv;->k()V

    goto :goto_0
.end method

.method m()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 280
    invoke-super {p0}, Lapv;->m()V

    .line 281
    iget-object v0, p0, Laqa;->B:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 282
    iget-object v0, p0, Laqa;->D:Lcom/twitter/media/ui/AnimatingProgressBar;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/AnimatingProgressBar;->setVisibility(I)V

    .line 283
    return-void
.end method

.method n()Z
    .locals 3

    .prologue
    .line 180
    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->v()Ljava/lang/String;

    move-result-object v0

    .line 181
    if-eqz v0, :cond_0

    iget-object v1, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    const v2, 0x7f130024

    invoke-virtual {v1, v2}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqa;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/e;

    .line 182
    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->o()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    const v2, 0x7f130025

    invoke-virtual {v1, v2}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    invoke-direct {p0}, Laqa;->p()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    const v2, 0x7f130022

    invoke-virtual {v1, v2}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 181
    :goto_0
    return v0

    .line 183
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method o()Lcom/twitter/app/dm/widget/SentMessageBylineView;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Laqa;->v:Lcom/twitter/app/dm/widget/SentMessageBylineView;

    return-object v0
.end method
