.class Laps$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Laps;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Laps;


# direct methods
.method constructor <init>(Laps;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Laps$1;->a:Laps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 125
    iget-object v0, p0, Laps$1;->a:Laps;

    invoke-static {v0}, Laps;->a(Laps;)Lcom/twitter/model/dms/q;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/dms/q;->i:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 126
    if-eqz v0, :cond_0

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 127
    :goto_0
    iget-object v2, p0, Laps$1;->a:Laps;

    invoke-static {v2}, Laps;->b(Laps;)Landroid/content/Context;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Laps$1;->a:Laps;

    invoke-static {v4}, Laps;->b(Laps;)Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v4, "user_id"

    .line 128
    invoke-virtual {v3, v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 127
    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 130
    return-void

    .line 126
    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method
