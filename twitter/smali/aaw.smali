.class public Laaw;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laar;


# instance fields
.field private final a:Lcom/twitter/model/core/TwitterUser;

.field private final b:Lcom/twitter/library/client/Session;


# direct methods
.method public constructor <init>(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Laaw;->a:Lcom/twitter/model/core/TwitterUser;

    .line 17
    iput-object p2, p0, Laaw;->b:Lcom/twitter/library/client/Session;

    .line 18
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Laaw;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    return-wide v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Laaw;->a:Lcom/twitter/model/core/TwitterUser;

    iget v0, v0, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-static {v0}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 4

    .prologue
    .line 29
    iget-object v0, p0, Laaw;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v2, p0, Laaw;->a:Lcom/twitter/model/core/TwitterUser;

    iget-object v2, v2, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    iget-object v3, p0, Laaw;->b:Lcom/twitter/library/client/Session;

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/profiles/v;->a(JLjava/lang/String;Lcom/twitter/library/client/Session;)Z

    move-result v0

    return v0
.end method
