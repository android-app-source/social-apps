.class public Lajb;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/object/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/object/g",
        "<",
        "Lcom/twitter/android/timeline/bk;",
        "Lcom/twitter/model/timeline/g;",
        "Ljava/lang/Boolean;",
        "Lcom/twitter/library/service/s;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/twitter/library/service/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lajb;->a:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lajb;->b:Lcom/twitter/library/service/v;

    .line 29
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/g;Ljava/lang/Boolean;)Lcom/twitter/library/service/s;
    .locals 9

    .prologue
    .line 35
    iget-object v0, p1, Lcom/twitter/android/timeline/bk;->g:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Lbgh;

    iget-object v1, p0, Lajb;->a:Landroid/content/Context;

    iget-object v2, p0, Lajb;->b:Lcom/twitter/library/service/v;

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    move-object v3, p2

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lbgh;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/model/timeline/g;Lcom/twitter/android/timeline/bk;Z)V

    .line 47
    :goto_0
    return-object v0

    .line 43
    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v6

    .line 44
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v7

    .line 45
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v8

    .line 46
    invoke-virtual {p1, v6, v7, v8}, Lcom/twitter/android/timeline/bk;->a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 47
    new-instance v0, Lbfq;

    iget-object v1, p0, Lajb;->a:Landroid/content/Context;

    iget-object v2, p0, Lajb;->b:Lcom/twitter/library/service/v;

    iget-object v4, p1, Lcom/twitter/android/timeline/bk;->e:Lcom/twitter/model/timeline/r;

    .line 48
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    move-object v3, p2

    invoke-direct/range {v0 .. v8}, Lbfq;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/model/timeline/g;Lcom/twitter/model/timeline/r;ZLjava/util/List;Ljava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    check-cast p1, Lcom/twitter/android/timeline/bk;

    check-cast p2, Lcom/twitter/model/timeline/g;

    check-cast p3, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2, p3}, Lajb;->a(Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/g;Ljava/lang/Boolean;)Lcom/twitter/library/service/s;

    move-result-object v0

    return-object v0
.end method
