.class public Lbzh;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbzi;


# static fields
.field private static final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lcom/twitter/media/filters/Filters;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:I

.field private final c:Z

.field private final d:Landroid/content/Context;

.field private final e:F

.field private final f:Lcom/twitter/media/filters/Filters;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lbzh;->a:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IZF)V
    .locals 6

    .prologue
    .line 33
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lbzh;-><init>(Landroid/content/Context;IZFLcom/twitter/media/filters/Filters;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IZFLcom/twitter/media/filters/Filters;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lbzh;->d:Landroid/content/Context;

    .line 39
    iput p2, p0, Lbzh;->b:I

    .line 40
    iput-boolean p3, p0, Lbzh;->c:Z

    .line 41
    iput-object p5, p0, Lbzh;->f:Lcom/twitter/media/filters/Filters;

    .line 42
    iput p4, p0, Lbzh;->e:F

    .line 43
    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 57
    invoke-static {p1}, Lcom/twitter/util/math/Size;->a(Landroid/graphics/Bitmap;)Lcom/twitter/util/math/Size;

    move-result-object v0

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1}, Lcom/twitter/media/util/a;->a(Lcom/twitter/util/math/Size;Landroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 59
    if-eqz v3, :cond_5

    .line 62
    iget-object v0, p0, Lbzh;->f:Lcom/twitter/media/filters/Filters;

    if-nez v0, :cond_2

    .line 65
    sget-object v0, Lbzh;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 66
    new-instance v1, Lcom/twitter/media/filters/Filters;

    invoke-direct {v1}, Lcom/twitter/media/filters/Filters;-><init>()V

    .line 67
    iget-object v2, p0, Lbzh;->d:Landroid/content/Context;

    iget v0, p0, Lbzh;->b:I

    const/16 v4, 0x8

    if-le v0, v4, :cond_1

    move v0, v6

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/twitter/media/filters/Filters;->a(Landroid/content/Context;Z)Z

    .line 68
    sget-object v0, Lbzh;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 70
    :cond_0
    sget-object v0, Lbzh;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/filters/Filters;

    .line 74
    :goto_1
    iget-boolean v1, p0, Lbzh;->c:Z

    invoke-virtual {v0, p1, v1}, Lcom/twitter/media/filters/Filters;->a(Landroid/graphics/Bitmap;Z)I

    move-result v2

    .line 75
    if-lez v2, :cond_3

    iget v1, p0, Lbzh;->b:I

    const/high16 v4, 0x3f800000    # 1.0f

    iget v5, p0, Lbzh;->e:F

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/media/filters/Filters;->a(IILandroid/graphics/Bitmap;FF)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 76
    :goto_2
    invoke-virtual {v0, v2}, Lcom/twitter/media/filters/Filters;->a(I)V

    .line 78
    if-eqz v6, :cond_4

    .line 84
    :goto_3
    return-object v3

    :cond_1
    move v0, v7

    .line 67
    goto :goto_0

    .line 72
    :cond_2
    iget-object v0, p0, Lbzh;->f:Lcom/twitter/media/filters/Filters;

    goto :goto_1

    :cond_3
    move v6, v7

    .line 75
    goto :goto_2

    .line 81
    :cond_4
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    :cond_5
    move-object v3, p1

    .line 84
    goto :goto_3
.end method

.method public a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 90
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string/jumbo v1, "filter_%d_enhance_%b_intensity%f"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lbzh;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-boolean v4, p0, Lbzh;->c:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lbzh;->e:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/util/math/Size;Lcom/twitter/util/math/Size;Lcom/twitter/util/math/c;I)Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lbzh;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/media/filters/c;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lbzh;->b:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lbzh;->c:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
