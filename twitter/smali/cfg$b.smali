.class public abstract Lcfg$b;
.super Lcfl$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcfg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcfg;",
        "B:",
        "Lcfg$a",
        "<TT;TB;>;>",
        "Lcfl$b",
        "<TT;TB;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcfl$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;Lcfg$a;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/n;",
            "TB;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 41
    invoke-super {p0, p1, p2, p3}, Lcfl$b;->a(Lcom/twitter/util/serialization/n;Lcfl$a;I)V

    .line 42
    sget-object v0, Lcfi;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfi;

    invoke-virtual {p2, v0}, Lcfg$a;->a(Lcfi;)Lcfg$a;

    .line 43
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcfl$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 27
    check-cast p2, Lcfg$a;

    invoke-virtual {p0, p1, p2, p3}, Lcfg$b;->a(Lcom/twitter/util/serialization/n;Lcfg$a;I)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 27
    check-cast p2, Lcfg$a;

    invoke-virtual {p0, p1, p2, p3}, Lcfg$b;->a(Lcom/twitter/util/serialization/n;Lcfg$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcfg;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/o;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33
    invoke-super {p0, p1, p2}, Lcfl$b;->a(Lcom/twitter/util/serialization/o;Lcfl;)V

    .line 34
    iget-object v0, p2, Lcfg;->b:Lcfi;

    sget-object v1, Lcfi;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 35
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lcfl;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27
    check-cast p2, Lcfg;

    invoke-virtual {p0, p1, p2}, Lcfg$b;->a(Lcom/twitter/util/serialization/o;Lcfg;)V

    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27
    check-cast p2, Lcfg;

    invoke-virtual {p0, p1, p2}, Lcfg$b;->a(Lcom/twitter/util/serialization/o;Lcfg;)V

    return-void
.end method
