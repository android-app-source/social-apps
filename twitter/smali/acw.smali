.class public Lacw;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lacw$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/navigation/ah;

.field private final b:Lcom/twitter/android/moments/ui/maker/viewdelegate/o;

.field private final c:Lajt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajt",
            "<",
            "Ladg;",
            "Lajq",
            "<",
            "Ladg;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lajx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajx",
            "<",
            "Ladg;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lwy;

.field private final f:Lcom/twitter/android/moments/ui/maker/q;

.field private g:Lrx/j;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/viewdelegate/o;Lajt;Lajx;Lwy;Lcom/twitter/android/moments/ui/maker/q;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/maker/navigation/ah;",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/o;",
            "Lajt",
            "<",
            "Ladg;",
            "Lajq",
            "<",
            "Ladg;",
            ">;>;",
            "Lajx",
            "<",
            "Ladg;",
            ">;",
            "Lwy;",
            "Lcom/twitter/android/moments/ui/maker/q;",
            ")V"
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lacw;->a:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    .line 78
    iput-object p2, p0, Lacw;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/o;

    .line 79
    iput-object p3, p0, Lacw;->c:Lajt;

    .line 80
    iput-object p4, p0, Lacw;->d:Lajx;

    .line 81
    iput-object p5, p0, Lacw;->e:Lwy;

    .line 82
    iput-object p6, p0, Lacw;->f:Lcom/twitter/android/moments/ui/maker/q;

    .line 83
    iget-object v0, p0, Lacw;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/o;

    invoke-virtual {v0, p3}, Lcom/twitter/android/moments/ui/maker/viewdelegate/o;->a(Lajt;)V

    .line 84
    iget-object v0, p0, Lacw;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/o;

    new-instance v1, Lacw$1;

    invoke-direct {v1, p0}, Lacw$1;-><init>(Lacw;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/o;->a(Landroid/view/View$OnClickListener;)V

    .line 90
    return-void
.end method

.method public static a(Lcom/twitter/app/common/base/BaseFragmentActivity;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lwy;Lcom/twitter/android/moments/ui/maker/q;J)Lacw;
    .locals 11

    .prologue
    .line 59
    invoke-static {p0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/o;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/o;

    move-result-object v8

    .line 60
    new-instance v9, Lajx;

    .line 61
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    invoke-direct {v9, v0}, Lajx;-><init>(Ljava/util/List;)V

    .line 62
    new-instance v0, Lacw$a;

    .line 63
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    new-instance v5, Lcom/twitter/android/util/q;

    sget-object v1, Lcom/twitter/android/util/r;->a:Lcom/twitter/android/util/r$a;

    const/16 v3, 0x1b0

    .line 65
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v4

    invoke-direct {v5, v1, v3, v4, p0}, Lcom/twitter/android/util/q;-><init>(Lcom/twitter/android/util/p;ILcom/twitter/util/android/f;Lcom/twitter/app/common/base/BaseFragmentActivity;)V

    move-object v1, p0

    move-object v3, p2

    move-object v4, p4

    move-wide/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lacw$a;-><init>(Landroid/app/Activity;Landroid/view/LayoutInflater;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/q;Lcom/twitter/android/util/q;J)V

    .line 66
    new-instance v3, Lajt;

    new-instance v1, Lajs;

    invoke-direct {v1, v9, v0}, Lajs;-><init>(Lajv;Lajr;)V

    invoke-direct {v3, v1}, Lajt;-><init>(Lajs;)V

    .line 69
    new-instance v0, Lacw;

    move-object v1, p2

    move-object v2, v8

    move-object v4, v9

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lacw;-><init>(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/viewdelegate/o;Lajt;Lajx;Lwy;Lcom/twitter/android/moments/ui/maker/q;)V

    return-object v0
.end method

.method static synthetic a(Lacw;)Lcom/twitter/android/moments/ui/maker/navigation/ah;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lacw;->a:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    return-object v0
.end method

.method static synthetic a(Lacw;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lacw;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ladg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lacw;->d:Lajx;

    invoke-virtual {v0, p1}, Lajx;->a(Ljava/util/List;)V

    .line 109
    iget-object v0, p0, Lacw;->c:Lajt;

    invoke-virtual {v0}, Lajt;->a()V

    .line 110
    return-void
.end method

.method static synthetic d()Lrx/functions/d;
    .locals 1

    .prologue
    .line 43
    invoke-static {}, Lacw;->g()Lrx/functions/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e()Lrx/functions/d;
    .locals 1

    .prologue
    .line 43
    invoke-static {}, Lacw;->h()Lrx/functions/d;

    move-result-object v0

    return-object v0
.end method

.method private static f()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Lcom/twitter/model/moments/viewmodels/a;",
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<",
            "Ladg;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 119
    new-instance v0, Lacw$3;

    invoke-direct {v0}, Lacw$3;-><init>()V

    return-object v0
.end method

.method private static g()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            "Ladg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    new-instance v0, Lacw$4;

    invoke-direct {v0}, Lacw$4;-><init>()V

    return-object v0
.end method

.method private static h()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    new-instance v0, Lacw$5;

    invoke-direct {v0}, Lacw$5;-><init>()V

    return-object v0
.end method


# virtual methods
.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lacw;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/o;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/o;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lacw;->g:Lrx/j;

    if-eqz v0, :cond_0

    .line 105
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lacw;->e:Lwy;

    invoke-virtual {v0}, Lwy;->a()Lrx/c;

    move-result-object v0

    .line 98
    invoke-static {}, Lacw;->f()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lacw$2;

    invoke-direct {v1, p0}, Lacw$2;-><init>(Lacw;)V

    .line 99
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lacw;->g:Lrx/j;

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lacw;->g:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 114
    iget-object v0, p0, Lacw;->f:Lcom/twitter/android/moments/ui/maker/q;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/q;->b()V

    .line 115
    return-void
.end method
