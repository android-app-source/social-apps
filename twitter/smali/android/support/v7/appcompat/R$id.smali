.class public final Landroid/support/v7/appcompat/R$id;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/appcompat/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action0:I = 0x7f1305e1

.field public static final action_bar:I = 0x7f130133

.field public static final action_bar_activity_content:I = 0x7f130001

.field public static final action_bar_container:I = 0x7f130132

.field public static final action_bar_root:I = 0x7f13012e

.field public static final action_bar_spinner:I = 0x7f130002

.field public static final action_bar_subtitle:I = 0x7f130115

.field public static final action_bar_title:I = 0x7f130114

.field public static final action_context_bar:I = 0x7f130134

.field public static final action_divider:I = 0x7f1305e5

.field public static final action_menu_divider:I = 0x7f130009

.field public static final action_menu_presenter:I = 0x7f13000a

.field public static final action_mode_bar:I = 0x7f130130

.field public static final action_mode_bar_stub:I = 0x7f13012f

.field public static final action_mode_close_button:I = 0x7f130116

.field public static final activity_chooser_view_content:I = 0x7f130117

.field public static final add:I = 0x7f1300bb

.field public static final alertTitle:I = 0x7f130121

.field public static final always:I = 0x7f1300f5

.field public static final beginning:I = 0x7f1300ec

.field public static final bottom:I = 0x7f1300a4

.field public static final buttonPanel:I = 0x7f13011c

.field public static final cancel_action:I = 0x7f1305e2

.field public static final checkbox:I = 0x7f13012a

.field public static final chronometer:I = 0x7f1305e7

.field public static final collapseActionView:I = 0x7f1300f6

.field public static final contentPanel:I = 0x7f130122

.field public static final custom:I = 0x7f130128

.field public static final customPanel:I = 0x7f130127

.field public static final decor_content_parent:I = 0x7f130131

.field public static final default_activity_button:I = 0x7f130119

.field public static final disableHome:I = 0x7f1300b0

.field public static final edit_query:I = 0x7f130135

.field public static final end:I = 0x7f1300d0

.field public static final end_padder:I = 0x7f1305eb

.field public static final expand_activities_button:I = 0x7f130118

.field public static final expanded_menu:I = 0x7f130129

.field public static final home:I = 0x7f130043

.field public static final homeAsUp:I = 0x7f1300b1

.field public static final icon:I = 0x7f1300fa

.field public static final ifRoom:I = 0x7f1300f7

.field public static final image:I = 0x7f130044

.field public static final info:I = 0x7f1305ea

.field public static final line1:I = 0x7f1305e6

.field public static final line3:I = 0x7f1305e9

.field public static final listMode:I = 0x7f1300ae

.field public static final list_item:I = 0x7f13011a

.field public static final media_actions:I = 0x7f1305e4

.field public static final middle:I = 0x7f1300a5

.field public static final multiply:I = 0x7f1300bc

.field public static final never:I = 0x7f1300f8

.field public static final none:I = 0x7f1300a6

.field public static final normal:I = 0x7f1300ad

.field public static final parentPanel:I = 0x7f13011e

.field public static final progress_circular:I = 0x7f130065

.field public static final progress_horizontal:I = 0x7f130067

.field public static final radio:I = 0x7f13012c

.field public static final screen:I = 0x7f1300bd

.field public static final scrollIndicatorDown:I = 0x7f130126

.field public static final scrollIndicatorUp:I = 0x7f130123

.field public static final scrollView:I = 0x7f130124

.field public static final search_badge:I = 0x7f130137

.field public static final search_bar:I = 0x7f130136

.field public static final search_button:I = 0x7f130138

.field public static final search_close_btn:I = 0x7f13013d

.field public static final search_edit_frame:I = 0x7f130139

.field public static final search_go_btn:I = 0x7f13013f

.field public static final search_mag_icon:I = 0x7f13013a

.field public static final search_plate:I = 0x7f13013b

.field public static final search_src_text:I = 0x7f13013c

.field public static final search_voice_btn:I = 0x7f130140

.field public static final select_dialog_listview:I = 0x7f130141

.field public static final shortcut:I = 0x7f13012b

.field public static final showCustom:I = 0x7f1300b2

.field public static final showHome:I = 0x7f1300b3

.field public static final showTitle:I = 0x7f1300b4

.field public static final spacer:I = 0x7f13011d

.field public static final split_action_bar:I = 0x7f13007b

.field public static final src_atop:I = 0x7f1300be

.field public static final src_in:I = 0x7f1300bf

.field public static final src_over:I = 0x7f1300c0

.field public static final status_bar_latest_event_content:I = 0x7f1305e3

.field public static final submenuarrow:I = 0x7f13012d

.field public static final submit_area:I = 0x7f13013e

.field public static final tabMode:I = 0x7f1300af

.field public static final text:I = 0x7f13007d

.field public static final text2:I = 0x7f1305e8

.field public static final textSpacerNoButtons:I = 0x7f130125

.field public static final time:I = 0x7f130187

.field public static final title:I = 0x7f13011b

.field public static final title_template:I = 0x7f130120

.field public static final top:I = 0x7f1300a7

.field public static final topPanel:I = 0x7f13011f

.field public static final up:I = 0x7f130094

.field public static final useLogo:I = 0x7f1300b5

.field public static final withText:I = 0x7f1300f9

.field public static final wrap_content:I = 0x7f1300c1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
