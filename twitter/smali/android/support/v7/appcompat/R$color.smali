.class public final Landroid/support/v7/appcompat/R$color;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/appcompat/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f11019d

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f11019e

.field public static final abc_btn_colored_borderless_text_material:I = 0x7f11019f

.field public static final abc_color_highlight_material:I = 0x7f1101a0

.field public static final abc_input_method_navigation_guard:I = 0x7f110024

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f1101a1

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f1101a2

.field public static final abc_primary_text_material_dark:I = 0x7f1101a3

.field public static final abc_primary_text_material_light:I = 0x7f1101a4

.field public static final abc_search_url_text:I = 0x7f1101a5

.field public static final abc_search_url_text_normal:I = 0x7f110025

.field public static final abc_search_url_text_pressed:I = 0x7f110026

.field public static final abc_search_url_text_selected:I = 0x7f110027

.field public static final abc_secondary_text_material_dark:I = 0x7f1101a6

.field public static final abc_secondary_text_material_light:I = 0x7f1101a7

.field public static final abc_tint_btn_checkable:I = 0x7f1101a8

.field public static final abc_tint_default:I = 0x7f1101a9

.field public static final abc_tint_edittext:I = 0x7f1101aa

.field public static final abc_tint_seek_thumb:I = 0x7f1101ab

.field public static final abc_tint_spinner:I = 0x7f1101ac

.field public static final abc_tint_switch_thumb:I = 0x7f1101ad

.field public static final abc_tint_switch_track:I = 0x7f1101ae

.field public static final accent_material_dark:I = 0x7f110028

.field public static final accent_material_light:I = 0x7f110029

.field public static final background_floating_material_dark:I = 0x7f11002c

.field public static final background_floating_material_light:I = 0x7f11002d

.field public static final background_material_dark:I = 0x7f11002e

.field public static final background_material_light:I = 0x7f11002f

.field public static final bright_foreground_disabled_material_dark:I = 0x7f110036

.field public static final bright_foreground_disabled_material_light:I = 0x7f110037

.field public static final bright_foreground_inverse_material_dark:I = 0x7f110038

.field public static final bright_foreground_inverse_material_light:I = 0x7f110039

.field public static final bright_foreground_material_dark:I = 0x7f11003a

.field public static final bright_foreground_material_light:I = 0x7f11003b

.field public static final button_material_dark:I = 0x7f110047

.field public static final button_material_light:I = 0x7f110048

.field public static final dim_foreground_disabled_material_dark:I = 0x7f110081

.field public static final dim_foreground_disabled_material_light:I = 0x7f110082

.field public static final dim_foreground_material_dark:I = 0x7f110083

.field public static final dim_foreground_material_light:I = 0x7f110084

.field public static final foreground_material_dark:I = 0x7f11009a

.field public static final foreground_material_light:I = 0x7f11009b

.field public static final highlighted_text_material_dark:I = 0x7f1100a7

.field public static final highlighted_text_material_light:I = 0x7f1100a8

.field public static final hint_foreground_material_dark:I = 0x7f1100b3

.field public static final hint_foreground_material_light:I = 0x7f1100b4

.field public static final material_blue_grey_800:I = 0x7f1100cc

.field public static final material_blue_grey_900:I = 0x7f1100cd

.field public static final material_blue_grey_950:I = 0x7f1100ce

.field public static final material_deep_teal_200:I = 0x7f1100cf

.field public static final material_deep_teal_500:I = 0x7f1100d0

.field public static final material_grey_100:I = 0x7f1100d1

.field public static final material_grey_300:I = 0x7f1100d2

.field public static final material_grey_50:I = 0x7f1100d3

.field public static final material_grey_600:I = 0x7f1100d4

.field public static final material_grey_800:I = 0x7f1100d5

.field public static final material_grey_850:I = 0x7f1100d6

.field public static final material_grey_900:I = 0x7f1100d7

.field public static final primary_dark_material_dark:I = 0x7f110117

.field public static final primary_dark_material_light:I = 0x7f110118

.field public static final primary_material_dark:I = 0x7f110119

.field public static final primary_material_light:I = 0x7f11011a

.field public static final primary_text_default_material_dark:I = 0x7f11011c

.field public static final primary_text_default_material_light:I = 0x7f11011d

.field public static final primary_text_disabled_material_dark:I = 0x7f11011e

.field public static final primary_text_disabled_material_light:I = 0x7f11011f

.field public static final ripple_material_dark:I = 0x7f110174

.field public static final ripple_material_light:I = 0x7f110175

.field public static final secondary_text_default_material_dark:I = 0x7f110178

.field public static final secondary_text_default_material_light:I = 0x7f110179

.field public static final secondary_text_disabled_material_dark:I = 0x7f11017a

.field public static final secondary_text_disabled_material_light:I = 0x7f11017b

.field public static final switch_thumb_disabled_material_dark:I = 0x7f110185

.field public static final switch_thumb_disabled_material_light:I = 0x7f110186

.field public static final switch_thumb_material_dark:I = 0x7f1101d3

.field public static final switch_thumb_material_light:I = 0x7f1101d4

.field public static final switch_thumb_normal_material_dark:I = 0x7f110187

.field public static final switch_thumb_normal_material_light:I = 0x7f110188


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
