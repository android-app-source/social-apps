.class public final Landroid/support/v7/appcompat/R$dimen;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/appcompat/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final abc_action_bar_content_inset_material:I = 0x7f0e0038

.field public static final abc_action_bar_content_inset_with_nav:I = 0x7f0e0039

.field public static final abc_action_bar_default_height_material:I = 0x7f0e0007

.field public static final abc_action_bar_default_padding_end_material:I = 0x7f0e003a

.field public static final abc_action_bar_default_padding_start_material:I = 0x7f0e003b

.field public static final abc_action_bar_elevation_material:I = 0x7f0e008d

.field public static final abc_action_bar_icon_vertical_padding_material:I = 0x7f0e008e

.field public static final abc_action_bar_overflow_padding_end_material:I = 0x7f0e008f

.field public static final abc_action_bar_overflow_padding_start_material:I = 0x7f0e0090

.field public static final abc_action_bar_progress_bar_size:I = 0x7f0e0008

.field public static final abc_action_bar_stacked_max_height:I = 0x7f0e0091

.field public static final abc_action_bar_stacked_tab_max_width:I = 0x7f0e0092

.field public static final abc_action_bar_subtitle_bottom_margin_material:I = 0x7f0e0093

.field public static final abc_action_bar_subtitle_top_margin_material:I = 0x7f0e0094

.field public static final abc_action_button_min_height_material:I = 0x7f0e0095

.field public static final abc_action_button_min_width_material:I = 0x7f0e0096

.field public static final abc_action_button_min_width_overflow_material:I = 0x7f0e0097

.field public static final abc_alert_dialog_button_bar_height:I = 0x7f0e0000

.field public static final abc_button_inset_horizontal_material:I = 0x7f0e0098

.field public static final abc_button_inset_vertical_material:I = 0x7f0e0099

.field public static final abc_button_padding_horizontal_material:I = 0x7f0e009a

.field public static final abc_button_padding_vertical_material:I = 0x7f0e009b

.field public static final abc_cascading_menus_min_smallest_width:I = 0x7f0e009c

.field public static final abc_config_prefDialogWidth:I = 0x7f0e002e

.field public static final abc_control_corner_material:I = 0x7f0e009d

.field public static final abc_control_inset_material:I = 0x7f0e009e

.field public static final abc_control_padding_material:I = 0x7f0e009f

.field public static final abc_dialog_fixed_height_major:I = 0x7f0e002f

.field public static final abc_dialog_fixed_height_minor:I = 0x7f0e0030

.field public static final abc_dialog_fixed_width_major:I = 0x7f0e0031

.field public static final abc_dialog_fixed_width_minor:I = 0x7f0e0032

.field public static final abc_dialog_list_padding_vertical_material:I = 0x7f0e00a0

.field public static final abc_dialog_min_width_major:I = 0x7f0e0033

.field public static final abc_dialog_min_width_minor:I = 0x7f0e0034

.field public static final abc_dialog_padding_material:I = 0x7f0e00a1

.field public static final abc_dialog_padding_top_material:I = 0x7f0e00a2

.field public static final abc_disabled_alpha_material_dark:I = 0x7f0e00a3

.field public static final abc_disabled_alpha_material_light:I = 0x7f0e00a4

.field public static final abc_dropdownitem_icon_width:I = 0x7f0e00a5

.field public static final abc_dropdownitem_text_padding_left:I = 0x7f0e00a6

.field public static final abc_dropdownitem_text_padding_right:I = 0x7f0e00a7

.field public static final abc_edit_text_inset_bottom_material:I = 0x7f0e00a8

.field public static final abc_edit_text_inset_horizontal_material:I = 0x7f0e00a9

.field public static final abc_edit_text_inset_top_material:I = 0x7f0e00aa

.field public static final abc_floating_window_z:I = 0x7f0e00ab

.field public static final abc_list_item_padding_horizontal_material:I = 0x7f0e00ac

.field public static final abc_panel_menu_list_width:I = 0x7f0e00ad

.field public static final abc_progress_bar_height_material:I = 0x7f0e00ae

.field public static final abc_search_view_preferred_height:I = 0x7f0e00af

.field public static final abc_search_view_preferred_width:I = 0x7f0e00b0

.field public static final abc_seekbar_track_background_height_material:I = 0x7f0e00b1

.field public static final abc_seekbar_track_progress_height_material:I = 0x7f0e00b2

.field public static final abc_select_dialog_padding_start_material:I = 0x7f0e00b3

.field public static final abc_switch_padding:I = 0x7f0e005d

.field public static final abc_text_size_body_1_material:I = 0x7f0e00b4

.field public static final abc_text_size_body_2_material:I = 0x7f0e00b5

.field public static final abc_text_size_button_material:I = 0x7f0e00b6

.field public static final abc_text_size_caption_material:I = 0x7f0e00b7

.field public static final abc_text_size_display_1_material:I = 0x7f0e00b8

.field public static final abc_text_size_display_2_material:I = 0x7f0e00b9

.field public static final abc_text_size_display_3_material:I = 0x7f0e00ba

.field public static final abc_text_size_display_4_material:I = 0x7f0e00bb

.field public static final abc_text_size_headline_material:I = 0x7f0e00bc

.field public static final abc_text_size_large_material:I = 0x7f0e00bd

.field public static final abc_text_size_medium_material:I = 0x7f0e00be

.field public static final abc_text_size_menu_header_material:I = 0x7f0e00bf

.field public static final abc_text_size_menu_material:I = 0x7f0e00c0

.field public static final abc_text_size_small_material:I = 0x7f0e00c1

.field public static final abc_text_size_subhead_material:I = 0x7f0e00c2

.field public static final abc_text_size_subtitle_material_toolbar:I = 0x7f0e0009

.field public static final abc_text_size_title_material:I = 0x7f0e00c3

.field public static final abc_text_size_title_material_toolbar:I = 0x7f0e000a

.field public static final disabled_alpha_material_dark:I = 0x7f0e01cc

.field public static final disabled_alpha_material_light:I = 0x7f0e01cd

.field public static final highlight_alpha_material_colored:I = 0x7f0e0237

.field public static final highlight_alpha_material_dark:I = 0x7f0e0238

.field public static final highlight_alpha_material_light:I = 0x7f0e0239

.field public static final notification_large_icon_height:I = 0x7f0e0399

.field public static final notification_large_icon_width:I = 0x7f0e039a

.field public static final notification_subtext_size:I = 0x7f0e039c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
