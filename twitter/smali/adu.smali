.class public Ladu;
.super Lcbh;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbh",
        "<",
        "Lbzz;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 20
    invoke-static {}, Ladu;->c()Lcbp;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcbh;-><init>(Landroid/database/Cursor;Lcbp;)V

    .line 21
    iput-object p1, p0, Ladu;->a:Landroid/database/Cursor;

    .line 22
    return-void
.end method

.method private static c()Lcbp;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcbp",
            "<",
            "Lbzz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 26
    new-instance v1, Ladw;

    invoke-direct {v1}, Ladw;-><init>()V

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 27
    new-instance v1, Ladt;

    invoke-direct {v1}, Ladt;-><init>()V

    .line 28
    new-instance v2, Lady;

    invoke-direct {v2, v1}, Lady;-><init>(Ladt;)V

    invoke-virtual {v0, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 29
    new-instance v2, Ladz;

    invoke-direct {v2, v1}, Ladz;-><init>(Ladt;)V

    invoke-virtual {v0, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 30
    new-instance v2, Ladx;

    invoke-direct {v2, v1}, Ladx;-><init>(Ladt;)V

    invoke-virtual {v0, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 31
    new-instance v1, Lcbn;

    new-instance v2, Lcbo;

    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v2, v0}, Lcbo;-><init>(Ljava/util/Collection;)V

    invoke-direct {v1, v2}, Lcbn;-><init>(Lcbp;)V

    return-object v1
.end method


# virtual methods
.method public aX_()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 35
    iget-object v1, p0, Ladu;->a:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToLast()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ladu;->a:Landroid/database/Cursor;

    const/4 v2, 0x7

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
