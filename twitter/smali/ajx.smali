.class public Lajx;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lajv;
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lajx$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Item:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lajv",
        "<TItem;>;",
        "Ljava/lang/Iterable",
        "<TItem;>;"
    }
.end annotation


# instance fields
.field private final a:Lajx$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajx$a",
            "<TItem;>;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TItem;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TItem;>;)V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-static {}, Lajx;->c()Lajx$a;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lajx;-><init>(Ljava/util/List;Lajx$a;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lajx$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TItem;>;",
            "Lajx$a",
            "<TItem;>;)V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-direct {p0, p1}, Lajx;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lajx;->b:Ljava/util/List;

    .line 22
    iput-object p2, p0, Lajx;->a:Lajx$a;

    .line 23
    return-void
.end method

.method private b(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TItem;>;)",
            "Ljava/util/List",
            "<TItem;>;"
        }
    .end annotation

    .prologue
    .line 68
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0, p1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public static c()Lajx$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Item:",
            "Ljava/lang/Object;",
            ">()",
            "Lajx$a",
            "<TItem;>;"
        }
    .end annotation

    .prologue
    .line 57
    new-instance v0, Lajx$1;

    invoke-direct {v0}, Lajx$1;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TItem;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lajx;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lajx;->b:Ljava/util/List;

    invoke-static {v0, p1, p2}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;II)V

    .line 47
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TItem;>;)V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lajx;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lajx;->b:Ljava/util/List;

    .line 43
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lajx;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public b(I)J
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lajx;->a:Lajx$a;

    invoke-virtual {p0, p1}, Lajx;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lajx$a;->a(Ljava/lang/Object;)J

    move-result-wide v0

    return-wide v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TItem;>;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lajx;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
