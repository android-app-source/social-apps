.class public abstract Lcbx$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcbx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40c
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcbx;",
        "B:",
        "Lcbx$a",
        "<TE;TB;>;>",
        "Lcom/twitter/util/object/i",
        "<TE;>;"
    }
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:I

.field e:I


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 133
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 138
    iput v0, p0, Lcbx$a;->d:I

    .line 139
    iput v0, p0, Lcbx$a;->e:I

    return-void
.end method


# virtual methods
.method public a(I)Lcbx$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TB;"
        }
    .end annotation

    .prologue
    .line 161
    iput p1, p0, Lcbx$a;->d:I

    .line 162
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbx$a;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcbx$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 143
    iput-object p1, p0, Lcbx$a;->a:Ljava/lang/String;

    .line 144
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbx$a;

    return-object v0
.end method

.method public b(I)Lcbx$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TB;"
        }
    .end annotation

    .prologue
    .line 167
    iput p1, p0, Lcbx$a;->e:I

    .line 168
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbx$a;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcbx$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 149
    iput-object p1, p0, Lcbx$a;->b:Ljava/lang/String;

    .line 150
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbx$a;

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcbx$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 155
    iput-object p1, p0, Lcbx$a;->c:Ljava/lang/String;

    .line 156
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbx$a;

    return-object v0
.end method
