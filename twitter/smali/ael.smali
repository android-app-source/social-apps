.class public Lael;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lael$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/ct;

.field private final b:Lcom/twitter/model/util/FriendshipCache;

.field private final c:Lcom/twitter/util/collection/ReferenceList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/ReferenceList",
            "<",
            "Lael$a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/app/Activity;

.field private final e:Lcom/twitter/ui/view/h;

.field private final f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final g:Z

.field private h:Z

.field private i:Z

.field private final j:Landroid/view/View$OnLongClickListener;

.field private final k:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/twitter/android/ct;Landroid/view/View$OnLongClickListener;Lcom/twitter/model/util/FriendshipCache;Landroid/app/Activity;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ZLcom/twitter/util/collection/ReferenceList;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/ct;",
            "Landroid/view/View$OnLongClickListener;",
            "Lcom/twitter/model/util/FriendshipCache;",
            "Landroid/app/Activity;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            "Z",
            "Lcom/twitter/util/collection/ReferenceList",
            "<",
            "Lael$a;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Lcom/twitter/ui/view/h$a;

    invoke-direct {v0}, Lcom/twitter/ui/view/h$a;-><init>()V

    const/4 v1, 0x1

    .line 49
    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/h$a;->c(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Lcom/twitter/ui/view/h$a;->a()Lcom/twitter/ui/view/h;

    move-result-object v0

    iput-object v0, p0, Lael;->e:Lcom/twitter/ui/view/h;

    .line 59
    new-instance v0, Lael$1;

    invoke-direct {v0, p0}, Lael$1;-><init>(Lael;)V

    iput-object v0, p0, Lael;->k:Landroid/view/View$OnClickListener;

    .line 97
    iput-object p1, p0, Lael;->a:Lcom/twitter/android/ct;

    .line 98
    iput-object p2, p0, Lael;->j:Landroid/view/View$OnLongClickListener;

    .line 99
    iput-object p3, p0, Lael;->b:Lcom/twitter/model/util/FriendshipCache;

    .line 100
    iput-object p4, p0, Lael;->d:Landroid/app/Activity;

    .line 101
    iput-object p5, p0, Lael;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 102
    iput-object p7, p0, Lael;->c:Lcom/twitter/util/collection/ReferenceList;

    .line 103
    iput-boolean p6, p0, Lael;->h:Z

    .line 104
    iput-boolean p8, p0, Lael;->g:Z

    .line 105
    return-void
.end method

.method static synthetic a(Lael;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lael;->d:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b(Lael;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lael;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    return-object v0
.end method

.method static synthetic c(Lael;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lael;->g:Z

    return v0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 109
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040414

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 111
    new-instance v1, Lael$a;

    invoke-direct {v1, v0}, Lael$a;-><init>(Landroid/view/View;)V

    .line 112
    iget-object v2, v1, Lael$a;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v3, p0, Lael;->a:Lcom/twitter/android/ct;

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/TweetView;->setOnTweetViewClickListener(Lcom/twitter/library/view/d;)V

    .line 113
    iget-object v2, v1, Lael$a;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v3, p0, Lael;->b:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/TweetView;->setFriendshipCache(Lcom/twitter/model/util/FriendshipCache;)V

    .line 114
    iget-object v2, v1, Lael$a;->d:Lcom/twitter/library/widget/TweetView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/TweetView;->setQuoteDisplayMode(I)V

    .line 115
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 116
    iget-object v2, p0, Lael;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    iget-object v2, p0, Lael;->j:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 118
    iget-object v2, p0, Lael;->c:Lcom/twitter/util/collection/ReferenceList;

    invoke-virtual {v2, v1}, Lcom/twitter/util/collection/ReferenceList;->b(Ljava/lang/Object;)V

    .line 119
    return-object v0
.end method

.method public a(Landroid/view/View;Lcom/twitter/model/core/Tweet;Ljava/lang/String;Z)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 124
    iput-boolean p4, p2, Lcom/twitter/model/core/Tweet;->e:Z

    .line 125
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lael$a;

    .line 126
    iput-object p3, v7, Lael$a;->a:Ljava/lang/String;

    .line 127
    iget-object v0, p0, Lael;->b:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p2}, Lcom/twitter/model/util/FriendshipCache;->a(Lcom/twitter/model/core/Tweet;)V

    .line 128
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    .line 129
    iget-object v2, v7, Lael$a;->d:Lcom/twitter/library/widget/TweetView;

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/twitter/model/account/UserSettings;->k:Z

    if-eqz v0, :cond_0

    move v0, v8

    :goto_0
    invoke-virtual {v2, v0}, Lcom/twitter/library/widget/TweetView;->setDisplaySensitiveMedia(Z)V

    .line 130
    iget-object v2, v7, Lael$a;->d:Lcom/twitter/library/widget/TweetView;

    iget-boolean v0, p0, Lael;->h:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lael;->d:Landroid/app/Activity;

    .line 131
    invoke-static {v0}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v0

    invoke-virtual {v0, p2}, Lbaa;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v8

    .line 130
    :goto_1
    invoke-virtual {v2, v0}, Lcom/twitter/library/widget/TweetView;->setAlwaysExpandMedia(Z)V

    .line 132
    iget-object v0, v7, Lael$a;->d:Lcom/twitter/library/widget/TweetView;

    sget v2, Lcni;->a:F

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/TweetView;->setContentSize(F)V

    .line 134
    new-instance v0, Lbxy;

    iget-object v2, p0, Lael;->d:Landroid/app/Activity;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    iget-object v5, p0, Lael;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const/4 v6, 0x0

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lbxy;-><init>(ZLandroid/app/Activity;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 136
    iget-object v1, v7, Lael$a;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v2, p0, Lael;->e:Lcom/twitter/ui/view/h;

    iget-boolean v3, p0, Lael;->i:Z

    invoke-virtual {v1, p2, v2, v3, v0}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;ZLbxy;)V

    .line 140
    iget-object v0, v7, Lael$a;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0, v8}, Lcom/twitter/library/widget/TweetView;->setCurationAction(I)V

    .line 141
    return-void

    :cond_0
    move v0, v1

    .line 129
    goto :goto_0

    :cond_1
    move v0, v1

    .line 131
    goto :goto_1
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 144
    iput-boolean p1, p0, Lael;->h:Z

    .line 145
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 148
    iget-boolean v0, p0, Lael;->i:Z

    if-eq v0, p1, :cond_0

    .line 149
    iput-boolean p1, p0, Lael;->i:Z

    .line 150
    if-nez p1, :cond_0

    .line 151
    iget-object v0, p0, Lael;->c:Lcom/twitter/util/collection/ReferenceList;

    invoke-virtual {v0}, Lcom/twitter/util/collection/ReferenceList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cu;

    .line 152
    iget-object v0, v0, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->k()V

    goto :goto_0

    .line 156
    :cond_0
    return-void
.end method
