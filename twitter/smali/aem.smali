.class public Laem;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laem$a;
    }
.end annotation


# instance fields
.field private final a:Laeq;

.field private final b:Lcom/twitter/model/util/FriendshipCache;

.field private final c:Laet;

.field private final d:Laes;

.field private final e:Lcom/twitter/database/lru/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/people/j;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Laem$a;


# direct methods
.method public constructor <init>(Lcom/twitter/model/util/FriendshipCache;Laem$a;Laet;Laes;Laeq;Lcom/twitter/database/lru/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/util/FriendshipCache;",
            "Laem$a;",
            "Laet;",
            "Laes;",
            "Laeq;",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/people/j;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Laem;->b:Lcom/twitter/model/util/FriendshipCache;

    .line 55
    iput-object p2, p0, Laem;->f:Laem$a;

    .line 57
    iput-object p3, p0, Laem;->c:Laet;

    .line 58
    iput-object p4, p0, Laem;->d:Laes;

    .line 59
    iput-object p5, p0, Laem;->a:Laeq;

    .line 60
    iput-object p6, p0, Laem;->e:Lcom/twitter/database/lru/l;

    .line 61
    return-void
.end method

.method static synthetic a(Laem;)Lcom/twitter/model/util/FriendshipCache;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Laem;->b:Lcom/twitter/model/util/FriendshipCache;

    return-object v0
.end method

.method private a()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;",
            "Lrx/c",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 125
    new-instance v0, Laem$4;

    invoke-direct {v0, p0}, Laem$4;-><init>(Laem;)V

    return-object v0
.end method

.method static synthetic b(Laem;)Laet;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Laem;->c:Laet;

    return-object v0
.end method

.method private b(Ljava/util/Map;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/people/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Laem;->f:Laem$a;

    invoke-virtual {v0}, Laem$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    invoke-interface {p1}, Ljava/util/Map;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 116
    iget-object v1, p0, Laem;->e:Lcom/twitter/database/lru/l;

    invoke-interface {v1, v0}, Lcom/twitter/database/lru/l;->b(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    invoke-virtual {v0}, Lrx/g;->c()Lrx/c;

    move-result-object v0

    .line 120
    :goto_0
    return-object v0

    .line 118
    :cond_0
    invoke-static {}, Lrx/c;->d()Lrx/c;

    move-result-object v0

    goto :goto_0
.end method

.method private b()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;",
            "Lrx/c",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/Tweet;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 135
    new-instance v0, Laem$5;

    invoke-direct {v0, p0}, Laem$5;-><init>(Laem;)V

    return-object v0
.end method

.method static synthetic c(Laem;)Laes;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Laem;->d:Laes;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/util/Map;)Lrx/c;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 71
    invoke-static {p1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Laem;->f:Laem$a;

    iget-object v0, v0, Laem$a;->a:Ljava/util/Map;

    .line 81
    :goto_0
    invoke-direct {p0, v0}, Laem;->b(Ljava/util/Map;)Lrx/c;

    move-result-object v1

    iget-object v2, p0, Laem;->a:Laeq;

    invoke-virtual {v2, v0}, Laeq;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    invoke-static {v1, v0}, Lrx/c;->b(Lrx/c;Lrx/c;)Lrx/c;

    move-result-object v0

    .line 82
    invoke-static {}, Lcre;->d()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->e(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Laem$1;

    invoke-direct {v1, p0}, Laem$1;-><init>(Laem;)V

    .line 83
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Lrx/c;->m()Lcwb;

    move-result-object v0

    .line 89
    invoke-direct {p0}, Laem;->a()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcwb;->j(Lrx/functions/d;)Lrx/c;

    move-result-object v1

    .line 90
    invoke-direct {p0}, Laem;->b()Lrx/functions/d;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcwb;->j(Lrx/functions/d;)Lrx/c;

    move-result-object v2

    .line 92
    new-instance v3, Laem$2;

    invoke-direct {v3, p0}, Laem$2;-><init>(Laem;)V

    invoke-static {v0, v1, v3}, Lrx/c;->a(Lrx/c;Lrx/c;Lrx/functions/e;)Lrx/c;

    move-result-object v1

    .line 100
    new-instance v3, Laem$3;

    invoke-direct {v3, p0}, Laem$3;-><init>(Laem;)V

    invoke-static {v1, v2, v3}, Lrx/c;->a(Lrx/c;Lrx/c;Lrx/functions/e;)Lrx/c;

    move-result-object v1

    .line 107
    invoke-virtual {v0}, Lcwb;->a()Lrx/j;

    .line 108
    return-object v1

    .line 74
    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    iget-object v1, p0, Laem;->f:Laem$a;

    iget-object v1, v1, Laem$a;->a:Ljava/util/Map;

    .line 75
    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/i;->b(Ljava/util/Map;)Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 76
    invoke-virtual {v0, p1}, Lcom/twitter/util/collection/i;->b(Ljava/util/Map;)Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    goto :goto_0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    iget-object v0, p0, Laem;->a:Laeq;

    invoke-virtual {v0}, Laeq;->close()V

    .line 150
    iget-object v0, p0, Laem;->c:Laet;

    invoke-virtual {v0}, Laet;->close()V

    .line 151
    iget-object v0, p0, Laem;->d:Laes;

    invoke-virtual {v0}, Laes;->close()V

    .line 152
    return-void
.end method
