.class public Lbse$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lauj;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lauj",
        "<",
        "Ljava/lang/String;",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Long;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/database/model/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/model/l",
            "<",
            "Layf$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/database/model/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/database/model/l",
            "<",
            "Layf$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    iput-object p1, p0, Lbse$a;->a:Lcom/twitter/database/model/l;

    .line 186
    return-void
.end method

.method public static a(Lcom/twitter/database/model/i;)Lbse$a;
    .locals 2

    .prologue
    .line 178
    const-class v0, Layf;

    .line 179
    invoke-interface {p0, v0}, Lcom/twitter/database/model/i;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Layf;

    invoke-interface {v0}, Layf;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    .line 180
    new-instance v1, Lbse$a;

    invoke-direct {v1, v0}, Lbse$a;-><init>(Lcom/twitter/database/model/l;)V

    return-object v1
.end method

.method static synthetic a(Lbse$a;)Lcom/twitter/database/model/l;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lbse$a;->a:Lcom/twitter/database/model/l;

    return-object v0
.end method

.method private a()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 201
    new-instance v0, Lbse$a$1;

    invoke-direct {v0, p0}, Lbse$a$1;-><init>(Lbse$a;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 196
    invoke-static {p1}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    invoke-direct {p0}, Lbse$a;->a()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a_(Ljava/lang/Object;)Lrx/c;
    .locals 1

    .prologue
    .line 173
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lbse$a;->a(Ljava/lang/String;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 228
    return-void
.end method
