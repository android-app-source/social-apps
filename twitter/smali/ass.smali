.class public Lass;
.super Laum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laum",
        "<",
        "Last;",
        "Lcom/twitter/util/collection/m",
        "<",
        "Lcbi",
        "<",
        "Lcgm;",
        ">;",
        "Lbfb;",
        ">;",
        "Lbep;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/app/Application;

.field private final b:Lcom/twitter/library/client/Session;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Laum;-><init>()V

    .line 38
    iput-object p1, p0, Lass;->a:Landroid/app/Application;

    .line 39
    iput-object p2, p0, Lass;->b:Lcom/twitter/library/client/Session;

    .line 40
    const v0, 0x7f0a03ce

    invoke-virtual {p1, v0}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lass;->c:Ljava/lang/String;

    .line 41
    return-void
.end method


# virtual methods
.method protected a(Last;)Lbep;
    .locals 5

    .prologue
    .line 59
    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    iget v0, p1, Last;->a:I

    packed-switch v0, :pswitch_data_0

    .line 73
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unhandled request type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Last;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :pswitch_0
    new-instance v0, Lbey;

    iget-object v1, p0, Lass;->a:Landroid/app/Application;

    iget-object v2, p0, Lass;->b:Lcom/twitter/library/client/Session;

    iget-object v3, p0, Lass;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lbey;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    .line 70
    :goto_0
    return-object v0

    .line 65
    :pswitch_1
    iget-object v0, p1, Last;->b:Lcgm;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    new-instance v0, Lbev;

    iget-object v1, p0, Lass;->a:Landroid/app/Application;

    iget-object v2, p0, Lass;->b:Lcom/twitter/library/client/Session;

    iget-object v3, p0, Lass;->c:Ljava/lang/String;

    iget-object v4, p1, Last;->b:Lcgm;

    invoke-direct {v0, v1, v2, v3, v4}, Lbev;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Lcgm;)V

    goto :goto_0

    .line 69
    :pswitch_2
    iget-object v0, p1, Last;->b:Lcgm;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    new-instance v0, Lbew;

    iget-object v1, p0, Lass;->a:Landroid/app/Application;

    iget-object v2, p0, Lass;->b:Lcom/twitter/library/client/Session;

    iget-object v3, p0, Lass;->c:Ljava/lang/String;

    iget-object v4, p1, Last;->b:Lcgm;

    invoke-direct {v0, v1, v2, v3, v4}, Lbew;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Lcgm;)V

    goto :goto_0

    .line 60
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Lcom/twitter/library/service/s;
    .locals 1

    .prologue
    .line 26
    check-cast p1, Last;

    invoke-virtual {p0, p1}, Lass;->a(Last;)Lbep;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lbep;)Lcom/twitter/util/collection/m;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbep;",
            ")",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcbi",
            "<",
            "Lcgm;",
            ">;",
            "Lbfb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    invoke-virtual {p1}, Lbep;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    new-instance v1, Lcbl;

    .line 49
    invoke-virtual {p1}, Lbep;->g()Lcgn;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgn;

    iget-object v0, v0, Lcgn;->a:Ljava/util/List;

    invoke-direct {v1, v0}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    .line 50
    invoke-static {v1}, Lcom/twitter/util/collection/m;->a(Ljava/lang/Object;)Lcom/twitter/util/collection/m;

    move-result-object v0

    .line 52
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lbep;->h()Lbfb;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/m;->b(Ljava/lang/Object;)Lcom/twitter/util/collection/m;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/library/service/s;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    check-cast p1, Lbep;

    invoke-virtual {p0, p1}, Lass;->a(Lbep;)Lcom/twitter/util/collection/m;

    move-result-object v0

    return-object v0
.end method
