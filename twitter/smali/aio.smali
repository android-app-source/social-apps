.class public Laio;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lain;


# instance fields
.field private final a:Lair;

.field private final b:Lait;


# direct methods
.method public constructor <init>(Lair;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Laio;->a:Lair;

    .line 20
    new-instance v0, Lait;

    invoke-direct {v0}, Lait;-><init>()V

    iput-object v0, p0, Laio;->b:Lait;

    .line 21
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Laio;->b:Lait;

    invoke-virtual {v0}, Lait;->a()I

    move-result v0

    return v0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/client/Session;",
            ")",
            "Lrx/c",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Laio;->a:Lair;

    invoke-interface {v0, p1, p2}, Lair;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Laio;->b:Lait;

    invoke-virtual {v0, p1}, Lait;->b(I)V

    .line 26
    return-void
.end method

.method public b(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Laio;->b:Lait;

    invoke-virtual {v0}, Lait;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    :goto_0
    return-void

    .line 45
    :cond_0
    iget-object v0, p0, Laio;->a:Lair;

    iget-object v1, p0, Laio;->b:Lait;

    invoke-interface {v0, p1, p2, v1}, Lair;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lait;)V

    goto :goto_0
.end method
