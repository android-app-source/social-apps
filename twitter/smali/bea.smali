.class public abstract Lbea;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcfz;",
        "Lcfy;",
        ">;>;"
    }
.end annotation


# instance fields
.field protected final a:Ljava/lang/String;

.field protected final b:Ljava/lang/String;

.field protected c:I

.field protected g:I


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 46
    iput-object p4, p0, Lbea;->a:Ljava/lang/String;

    .line 47
    iput-object p5, p0, Lbea;->b:Ljava/lang/String;

    .line 48
    const-string/jumbo v0, "Push destination changes are always considered as a non-user action"

    invoke-virtual {p0, v0}, Lbea;->l(Ljava/lang/String;)Lcom/twitter/library/service/s;

    .line 49
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/api/i;)Lcfz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcfz;",
            "Lcfy;",
            ">;)",
            "Lcfz;"
        }
    .end annotation

    .prologue
    .line 106
    invoke-virtual {p1}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfz;

    return-object v0
.end method

.method protected a(Lcom/twitter/library/service/d$a;)Lcom/twitter/library/service/d$a;
    .locals 4

    .prologue
    .line 83
    const-string/jumbo v0, "udid"

    iget-object v1, p0, Lbea;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "enabled_for"

    iget v2, p0, Lbea;->c:I

    int-to-long v2, v2

    .line 84
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "app_version"

    const-wide/16 v2, 0x11

    .line 85
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "system_version"

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    int-to-long v2, v2

    .line 86
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    .line 87
    iget-object v0, p0, Lbea;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 88
    const-string/jumbo v0, "token"

    iget-object v1, p0, Lbea;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 90
    :cond_0
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    const-string/jumbo v0, "environment"

    const-wide/16 v2, 0x2

    invoke-virtual {p1, v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    .line 93
    :cond_1
    return-object p1
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 8
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcfz;",
            "Lcfy;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v1, 0x130

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 60
    invoke-virtual {p0}, Lbea;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/v;

    iget-wide v2, v0, Lcom/twitter/library/service/v;->c:J

    .line 61
    invoke-static {v2, v3}, Lbuz;->a(J)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 62
    invoke-virtual {p2}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    if-ne v0, v1, :cond_2

    :cond_0
    move v0, v7

    :goto_0
    invoke-virtual {p2, v0}, Lcom/twitter/library/service/u;->a(Z)V

    .line 68
    :goto_1
    iget v0, p0, Lbea;->g:I

    const/16 v1, 0x86

    if-ne v0, v1, :cond_1

    .line 69
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "notification::gcm_registration::device_limit_exceeded"

    aput-object v2, v1, v6

    .line 70
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 69
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 72
    :cond_1
    return-void

    :cond_2
    move v0, v6

    .line 62
    goto :goto_0

    .line 64
    :cond_3
    invoke-virtual {p2}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p2}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    if-ne v0, v1, :cond_5

    :cond_4
    move v4, v7

    .line 65
    :goto_2
    iget-object v0, p0, Lbea;->p:Landroid/content/Context;

    invoke-virtual {p0}, Lbea;->b()Ljava/lang/String;

    move-result-object v1

    move-object v5, p2

    invoke-static/range {v0 .. v6}, Lcom/twitter/library/api/a;->a(Landroid/content/Context;Ljava/lang/String;JZLcom/twitter/library/service/u;Z)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_1

    :cond_5
    move v4, v6

    .line 64
    goto :goto_2
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 31
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbea;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b(Lcom/twitter/library/api/i;)Lcfy;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcfz;",
            "Lcfy;",
            ">;)",
            "Lcfy;"
        }
    .end annotation

    .prologue
    .line 113
    invoke-virtual {p1}, Lcom/twitter/library/api/i;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfy;

    return-object v0
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method protected e()Lcom/twitter/library/api/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcfz;",
            "Lcfy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    const-class v0, Lcfz;

    const-class v1, Lcfy;

    invoke-static {v0, v1}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lbea;->e()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
