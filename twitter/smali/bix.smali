.class public Lbix;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbix$a;
    }
.end annotation


# instance fields
.field a:Ljava/util/Map;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lbiw;",
            ">;",
            "Landroid/util/Pair",
            "<",
            "Lbiw;",
            "Lcom/twitter/library/av/m;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbiy;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/library/av/playback/z;

.field private final d:Landroid/os/HandlerThread;

.field private final e:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/z;)V
    .locals 2

    .prologue
    .line 57
    new-instance v0, Landroid/os/HandlerThread;

    const-class v1, Lbix;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1, v0}, Lbix;-><init>(Lcom/twitter/library/av/playback/z;Landroid/os/HandlerThread;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/av/playback/z;Landroid/os/HandlerThread;)V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lbix;->a:Ljava/util/Map;

    .line 46
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lbix;->b:Ljava/util/List;

    .line 62
    iput-object p1, p0, Lbix;->c:Lcom/twitter/library/av/playback/z;

    .line 63
    iput-object p2, p0, Lbix;->d:Landroid/os/HandlerThread;

    .line 64
    iget-object v0, p0, Lbix;->d:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 65
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lbix;->d:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lbix;->e:Landroid/os/Handler;

    .line 66
    return-void
.end method

.method static synthetic a(Lbix;)Ljava/util/List;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lbix;->b:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a(Lbiy;)Lbix;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lbix;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    invoke-virtual {p0, p1}, Lbix;->c(Lbiy;)V

    .line 81
    return-object p0
.end method

.method public a(Ljava/util/Collection;)Lbix;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lbiy;",
            ">;)",
            "Lbix;"
        }
    .end annotation

    .prologue
    .line 70
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbiy;

    .line 71
    invoke-virtual {p0, v0}, Lbix;->c(Lbiy;)V

    goto :goto_0

    .line 73
    :cond_0
    iget-object v0, p0, Lbix;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 74
    return-object p0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 154
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 155
    iget-object v0, p0, Lbix;->d:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 159
    :goto_0
    return-void

    .line 157
    :cond_0
    iget-object v0, p0, Lbix;->d:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    goto :goto_0
.end method

.method public a(Lbiw;)V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lbix;->c:Lcom/twitter/library/av/playback/z;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/z;->a()Lcom/twitter/library/av/m;

    move-result-object v0

    .line 122
    invoke-virtual {p0, p1, v0}, Lbix;->a(Lbiw;Lcom/twitter/library/av/m;)V

    .line 123
    return-void
.end method

.method public a(Lbiw;Lcom/twitter/library/av/m;)V
    .locals 3

    .prologue
    .line 137
    iget-object v0, p0, Lbix;->a:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    iget-object v0, p0, Lbix;->e:Landroid/os/Handler;

    new-instance v1, Lbix$1;

    invoke-direct {v1, p0, p1, p2}, Lbix$1;-><init>(Lbix;Lbiw;Lcom/twitter/library/av/m;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 148
    return-void
.end method

.method public b(Lbiy;)Lbix;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lbix;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 87
    return-object p0
.end method

.method c(Lbiy;)V
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lbix;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 99
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lbix;->a:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 102
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 103
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lbiw;

    .line 104
    invoke-virtual {p1, v1}, Lbiy;->c(Lbiw;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 105
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/library/av/m;

    invoke-virtual {p1, v1, v0}, Lbiy;->a(Lbiw;Lcom/twitter/library/av/m;)V

    goto :goto_0

    .line 109
    :cond_1
    return-void
.end method
