.class public abstract Lbks;
.super Lbkq;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbks$a;
    }
.end annotation


# static fields
.field static final b:Ljava/util/concurrent/TimeUnit;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field


# instance fields
.field public final c:Ljava/lang/String;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field d:Ljava/lang/String;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private final e:Lbks$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sput-object v0, Lbks;->b:Ljava/util/concurrent/TimeUnit;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/av/playback/AVDataSource;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lbks$a;

    invoke-direct {v0}, Lbks$a;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lbks;-><init>(Lcom/twitter/library/av/playback/AVDataSource;Ljava/lang/String;Lbks$a;)V

    .line 45
    return-void
.end method

.method protected constructor <init>(Lcom/twitter/library/av/playback/AVDataSource;Ljava/lang/String;Lbks$a;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lbkq;-><init>(Lcom/twitter/library/av/playback/AVDataSource;)V

    .line 54
    iput-object p2, p0, Lbks;->c:Ljava/lang/String;

    .line 55
    iput-object p3, p0, Lbks;->e:Lbks$a;

    .line 56
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;Ljava/util/Map;Lcom/twitter/network/j;Ljava/lang/String;)Lcom/twitter/network/HttpOperation;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/twitter/network/j;",
            "Ljava/lang/String;",
            ")",
            "Lcom/twitter/network/HttpOperation;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 62
    iget-object v0, p0, Lbks;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/media/manager/n;->a(Ljava/lang/String;)Lcom/twitter/library/media/manager/n$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/media/manager/n$a;->a()Lcom/twitter/library/media/manager/n;

    move-result-object v0

    .line 63
    iget-object v2, p0, Lbks;->e:Lbks$a;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbks$a;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    move-result-object v2

    .line 64
    invoke-virtual {v2}, Lcom/twitter/library/media/manager/g;->e()Lcom/twitter/library/media/manager/m;

    move-result-object v2

    .line 65
    invoke-virtual {v2, v0}, Lcom/twitter/library/media/manager/m;->d(Lcom/twitter/media/request/b;)Lcom/twitter/util/concurrent/g;

    move-result-object v0

    .line 67
    const-wide/16 v2, 0x3a98

    :try_start_0
    sget-object v4, Lbks;->b:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v4}, Lcom/twitter/util/concurrent/g;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/ResourceResponse;

    .line 68
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/media/request/ResourceResponse;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/VideoFile;

    .line 69
    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/twitter/media/model/VideoFile;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 70
    :goto_1
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 71
    iput-object v0, p0, Lbks;->d:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    .line 75
    :cond_0
    :goto_2
    return-object v1

    :cond_1
    move-object v0, v1

    .line 68
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 69
    goto :goto_1

    .line 73
    :catch_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_2
.end method

.method protected a(Landroid/content/Context;)Lcom/twitter/network/j;
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Landroid/content/Context;Ljava/util/Map;Lcom/twitter/util/network/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/twitter/util/network/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 99
    return-void
.end method

.method protected a(Landroid/net/Uri$Builder;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri$Builder;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 94
    return-void
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lbks;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbks;->d:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbks;->c:Ljava/lang/String;

    goto :goto_0
.end method
