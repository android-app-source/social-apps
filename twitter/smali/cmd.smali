.class public final Lcmd;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lcmc;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcsd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcsd",
            "<",
            "Lcmc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcmd;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcmd;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcsd;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lcmc;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    sget-boolean v0, Lcmd;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 18
    :cond_0
    iput-object p1, p0, Lcmd;->b:Lcsd;

    .line 19
    return-void
.end method

.method public static a(Lcsd;)Ldagger/internal/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lcmc;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Lcmc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    new-instance v0, Lcmd;

    invoke-direct {v0, p0}, Lcmd;-><init>(Lcsd;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcmc;
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcmd;->b:Lcsd;

    new-instance v1, Lcmc;

    invoke-direct {v1}, Lcmc;-><init>()V

    invoke-static {v0, v1}, Ldagger/internal/MembersInjectors;->a(Lcsd;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmc;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lcmd;->a()Lcmc;

    move-result-object v0

    return-object v0
.end method
