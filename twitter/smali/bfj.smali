.class public abstract Lbfj;
.super Lcom/twitter/library/api/r;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/api/r",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcom/twitter/model/core/ac;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final b:J

.field private final c:Z

.field private final g:Ljava/lang/String;

.field private final h:Lcom/twitter/library/provider/t;

.field private final i:Lcom/twitter/library/api/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/ac;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation
.end field

.field private j:[I

.field private k:I


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JZLcom/twitter/library/provider/t;Lcom/twitter/library/api/i;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/client/Session;",
            "JZ",
            "Lcom/twitter/library/provider/t;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/ac;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    const-class v0, Lbfm;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/r;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 36
    sget-object v0, Lcom/twitter/model/core/z;->b:[I

    iput-object v0, p0, Lbfj;->j:[I

    .line 46
    iput-object p6, p0, Lbfj;->h:Lcom/twitter/library/provider/t;

    .line 47
    iput-object p7, p0, Lbfj;->i:Lcom/twitter/library/api/i;

    .line 48
    iput-boolean p5, p0, Lbfj;->c:Z

    .line 49
    iput-wide p3, p0, Lbfj;->b:J

    .line 51
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string/jumbo v1, "request_favorite_%d_%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lbfj;->M()Lcom/twitter/library/service/v;

    move-result-object v4

    iget-wide v4, v4, Lcom/twitter/library/service/v;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbfj;->g:Ljava/lang/String;

    .line 52
    return-void
.end method

.method static synthetic a(Lbfj;I)I
    .locals 0

    .prologue
    .line 28
    iput p1, p0, Lbfj;->k:I

    return p1
.end method

.method static synthetic a(Lbfj;)Laut;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lbfj;->S()Laut;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lbfj;)J
    .locals 2

    .prologue
    .line 28
    iget-wide v0, p0, Lbfj;->b:J

    return-wide v0
.end method

.method static synthetic c(Lbfj;)Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lbfj;->c:Z

    return v0
.end method

.method static synthetic d(Lbfj;)Lcom/twitter/library/provider/t;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lbfj;->h:Lcom/twitter/library/provider/t;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/async/service/j;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 81
    invoke-super {p0, p1}, Lcom/twitter/library/api/r;->a(Lcom/twitter/async/service/j;)V

    .line 83
    invoke-virtual {p0}, Lbfj;->T()Z

    move-result v0

    .line 84
    invoke-virtual {p0}, Lbfj;->M()Lcom/twitter/library/service/v;

    move-result-object v1

    iget-wide v2, v1, Lcom/twitter/library/service/v;->c:J

    .line 85
    if-eqz v0, :cond_2

    .line 86
    iget-object v0, p0, Lbfj;->i:Lcom/twitter/library/api/i;

    invoke-virtual {v0}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/twitter/model/core/ac;

    .line 87
    if-eqz v1, :cond_1

    .line 89
    iget-boolean v0, p0, Lbfj;->c:Z

    iput-boolean v0, v1, Lcom/twitter/model/core/ac;->G:Z

    .line 92
    iget v0, v1, Lcom/twitter/model/core/ac;->H:I

    iget v4, p0, Lbfj;->k:I

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v1, Lcom/twitter/model/core/ac;->H:I

    .line 93
    iget v0, v1, Lcom/twitter/model/core/ac;->H:I

    iput v0, p0, Lbfj;->k:I

    .line 95
    invoke-virtual {p0}, Lbfj;->S()Laut;

    move-result-object v12

    .line 96
    iget-boolean v0, p0, Lbfj;->c:Z

    if-eqz v0, :cond_0

    const/4 v4, 0x2

    .line 99
    :goto_0
    iget-object v0, p0, Lbfj;->h:Lcom/twitter/library/provider/t;

    .line 100
    invoke-static {v1}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-wide/16 v5, -0x1

    const/4 v8, 0x0

    const/4 v10, 0x0

    move v9, v7

    move v11, v7

    move v13, v7

    .line 99
    invoke-virtual/range {v0 .. v13}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;JIJZZZLjava/lang/String;ZLaut;Z)Ljava/util/Collection;

    .line 105
    invoke-virtual {v12}, Laut;->a()V

    .line 113
    :goto_1
    return-void

    .line 96
    :cond_0
    const/4 v4, -0x1

    goto :goto_0

    .line 107
    :cond_1
    new-instance v0, Lcom/twitter/library/util/InvalidDataException;

    const-string/jumbo v1, "Received null status."

    invoke-direct {v0, v1}, Lcom/twitter/library/util/InvalidDataException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 111
    :cond_2
    iget-object v0, p0, Lbfj;->i:Lcom/twitter/library/api/i;

    invoke-virtual {v0}, Lcom/twitter/library/api/i;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/z;

    invoke-static {v0}, Lcom/twitter/model/core/z;->a(Lcom/twitter/model/core/z;)[I

    move-result-object v0

    iput-object v0, p0, Lbfj;->j:[I

    goto :goto_1
.end method

.method protected b()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/ac;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lbfj;->i:Lcom/twitter/library/api/i;

    return-object v0
.end method

.method public c(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 62
    if-eqz p1, :cond_0

    .line 66
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/twitter/async/service/AsyncOperation;->cancel(Z)Z

    .line 68
    :cond_0
    new-instance v0, Lbfj$1;

    invoke-direct {v0, p0}, Lbfj$1;-><init>(Lbfj;)V

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lbfj;->b()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 116
    iget-wide v0, p0, Lbfj;->b:J

    return-wide v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lbfj;->k:I

    return v0
.end method

.method protected o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lbfj;->g:Ljava/lang/String;

    return-object v0
.end method

.method public s()[I
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lbfj;->j:[I

    return-object v0
.end method
