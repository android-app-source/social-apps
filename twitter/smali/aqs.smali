.class public Laqs;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/object/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/object/d",
        "<",
        "Lcom/twitter/model/onboarding/Subtask;",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Laqs;->a:Landroid/content/Context;

    .line 35
    return-void
.end method

.method private a(I)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 60
    new-instance v0, Lcom/twitter/app/onboarding/flowstep/common/g;

    invoke-direct {v0, p1}, Lcom/twitter/app/onboarding/flowstep/common/g;-><init>(I)V

    .line 61
    invoke-virtual {v0}, Lcom/twitter/app/onboarding/flowstep/common/g;->z()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Laqs;->a:Landroid/content/Context;

    const-class v2, Lcom/twitter/app/onboarding/flowstep/common/FlowStepWrapperActivity;

    .line 62
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 60
    return-object v0
.end method

.method private b(I)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 67
    new-instance v0, Lcom/twitter/app/onboarding/smartfollowstep/a;

    invoke-direct {v0, p1}, Lcom/twitter/app/onboarding/smartfollowstep/a;-><init>(I)V

    .line 68
    invoke-virtual {v0}, Lcom/twitter/app/onboarding/smartfollowstep/a;->z()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Laqs;->a:Landroid/content/Context;

    const-class v2, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;

    .line 69
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 67
    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/onboarding/Subtask;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 40
    instance-of v0, p1, Lcom/twitter/model/onboarding/subtask/NameEntrySubtask;

    if-eqz v0, :cond_0

    .line 41
    invoke-direct {p0, v3}, Laqs;->a(I)Landroid/content/Intent;

    move-result-object v0

    .line 54
    :goto_0
    return-object v0

    .line 42
    :cond_0
    instance-of v0, p1, Lcom/twitter/model/onboarding/subtask/AddEmailSubtask;

    if-eqz v0, :cond_1

    .line 43
    invoke-direct {p0, v1}, Laqs;->a(I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 44
    :cond_1
    instance-of v0, p1, Lcom/twitter/model/onboarding/subtask/PasswordEntrySubtask;

    if-eqz v0, :cond_2

    .line 45
    invoke-direct {p0, v2}, Laqs;->a(I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 46
    :cond_2
    instance-of v0, p1, Lcom/twitter/model/onboarding/subtask/InterestPickerSubtask;

    if-eqz v0, :cond_3

    .line 47
    invoke-direct {p0, v2}, Laqs;->b(I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 48
    :cond_3
    instance-of v0, p1, Lcom/twitter/model/onboarding/subtask/ShareLocationSubtask;

    if-eqz v0, :cond_4

    .line 49
    invoke-direct {p0, v3}, Laqs;->b(I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 50
    :cond_4
    instance-of v0, p1, Lcom/twitter/model/onboarding/subtask/FollowPeopleSubtask;

    if-eqz v0, :cond_5

    .line 51
    invoke-direct {p0, v1}, Laqs;->b(I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 53
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Got unsupported subtask: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Laqs;->a:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/app/main/MainActivity;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    check-cast p1, Lcom/twitter/model/onboarding/Subtask;

    invoke-virtual {p0, p1}, Laqs;->a(Lcom/twitter/model/onboarding/Subtask;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
