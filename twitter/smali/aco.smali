.class public Laco;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcnw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcns;",
        ">",
        "Ljava/lang/Object;",
        "Lcnw",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Landroid/view/LayoutInflater;

.field private c:Lcom/twitter/ui/widget/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/ui/widget/d",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Laco;->b:Landroid/view/LayoutInflater;

    .line 39
    iput-object p2, p0, Laco;->a:Landroid/view/ViewGroup;

    .line 40
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Laco;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<N::",
            "Lcns;",
            ">(",
            "Landroid/view/LayoutInflater;",
            "Landroid/view/ViewGroup;",
            ")",
            "Laco",
            "<TN;>;"
        }
    .end annotation

    .prologue
    .line 33
    const v0, 0x7f0401fd

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 34
    new-instance v1, Laco;

    invoke-direct {v1, p0, v0}, Laco;-><init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    return-object v1
.end method

.method static synthetic a(Laco;Lcns;)Landroid/view/View;
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1}, Laco;->a(Lcns;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcns;)Landroid/view/View;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Laco;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f0401fe

    iget-object v2, p0, Laco;->a:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    .line 56
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 57
    const v1, 0x7f1304f2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 58
    invoke-interface {p1}, Lcns;->a()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    new-instance v1, Laco$2;

    invoke-direct {v1, p0, p1}, Laco$2;-><init>(Laco;Lcns;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    return-object v0
.end method

.method static synthetic a(Laco;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Laco;->a:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic b(Laco;)Lcom/twitter/ui/widget/d;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Laco;->c:Lcom/twitter/ui/widget/d;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Laco;->a:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public a(Lcom/twitter/ui/widget/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/ui/widget/d",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 70
    iput-object p1, p0, Laco;->c:Lcom/twitter/ui/widget/d;

    .line 71
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Laco;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 45
    invoke-static {p1}, Lrx/c;->a(Ljava/lang/Iterable;)Lrx/c;

    move-result-object v0

    new-instance v1, Laco$1;

    invoke-direct {v1, p0}, Laco$1;-><init>(Laco;)V

    invoke-virtual {v0, v1}, Lrx/c;->c(Lrx/functions/b;)V

    .line 51
    return-void
.end method
