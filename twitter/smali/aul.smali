.class Laul;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laui;


# static fields
.field private static final a:Landroid/os/Handler;


# instance fields
.field private final b:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Laul;->a:Landroid/os/Handler;

    return-void
.end method

.method constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Laul;->b:Landroid/content/ContentResolver;

    .line 34
    return-void
.end method

.method static synthetic a(Laul;)Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Laul;->b:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic a()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Laul;->a:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/net/Uri;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lrx/c",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    new-instance v0, Laul$2;

    invoke-direct {v0, p0, p1}, Laul$2;-><init>(Laul;Landroid/net/Uri;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lrx/c;->l()Lrx/c;

    move-result-object v0

    new-instance v1, Laul$1;

    invoke-direct {v1, p0, p1}, Laul$1;-><init>(Laul;Landroid/net/Uri;)V

    .line 67
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/functions/b;)Lrx/c;

    move-result-object v0

    .line 39
    return-object v0
.end method
