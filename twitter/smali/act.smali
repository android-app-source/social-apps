.class public Lact;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lact$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/viewdelegate/l;

.field private final b:Lzv;

.field private final c:Lajt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajt",
            "<",
            "Ladd;",
            "Lajq",
            "<",
            "Ladd;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lajx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajx",
            "<",
            "Ladd;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lwy;

.field private final f:Lcom/twitter/android/moments/ui/maker/o;

.field private final g:Lrx/j;

.field private final h:Lcom/twitter/android/moments/ui/maker/navigation/ah;

.field private final i:Lcom/twitter/model/moments/w;

.field private final j:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/l;Lajt;Lajx;Lwy;Lcom/twitter/android/moments/ui/maker/o;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/model/moments/r;Lcom/twitter/model/moments/w;Landroid/content/res/Resources;Lzv;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/l;",
            "Lajt",
            "<",
            "Ladd;",
            "Lajq",
            "<",
            "Ladd;",
            ">;>;",
            "Lajx",
            "<",
            "Ladd;",
            ">;",
            "Lwy;",
            "Lcom/twitter/android/moments/ui/maker/o;",
            "Lcom/twitter/android/moments/ui/maker/navigation/ah;",
            "Lcom/twitter/model/moments/r;",
            "Lcom/twitter/model/moments/w;",
            "Landroid/content/res/Resources;",
            "Lzv;",
            ")V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object p1, p0, Lact;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/l;

    .line 91
    iput-object p10, p0, Lact;->b:Lzv;

    .line 92
    iget-object v0, p0, Lact;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/l;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/l;->a()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lact$1;

    invoke-direct {v1, p0, p6}, Lact$1;-><init>(Lact;Lcom/twitter/android/moments/ui/maker/navigation/ah;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    iput-object p2, p0, Lact;->c:Lajt;

    .line 99
    iput-object p3, p0, Lact;->d:Lajx;

    .line 100
    iput-object p4, p0, Lact;->e:Lwy;

    .line 101
    iget-object v0, p0, Lact;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/l;

    iget-object v1, p0, Lact;->c:Lajt;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/l;->a(Lajt;)V

    .line 102
    iput-object p5, p0, Lact;->f:Lcom/twitter/android/moments/ui/maker/o;

    .line 103
    iput-object p6, p0, Lact;->h:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    .line 104
    iget-object v0, p0, Lact;->f:Lcom/twitter/android/moments/ui/maker/o;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/o;->b()Lrx/c;

    move-result-object v0

    invoke-direct {p0}, Lact;->d()Lrx/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lact;->g:Lrx/j;

    .line 105
    iput-object p8, p0, Lact;->i:Lcom/twitter/model/moments/w;

    .line 106
    iput-object p9, p0, Lact;->j:Landroid/content/res/Resources;

    .line 107
    iget-object v0, p0, Lact;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/l;

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lact;->a(Lcom/twitter/model/moments/r;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/l;->a(Landroid/view/View$OnClickListener;)V

    .line 108
    iget-object v0, p0, Lact;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/l;

    invoke-direct {p0, p7}, Lact;->a(Lcom/twitter/model/moments/r;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/l;->b(Landroid/view/View$OnClickListener;)V

    .line 109
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;Lwy;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/model/moments/r;Lcom/twitter/model/moments/w;Landroid/content/res/Resources;J)Lact;
    .locals 11

    .prologue
    .line 66
    .line 67
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/l;->a(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/l;

    move-result-object v1

    .line 68
    new-instance v3, Lajx;

    .line 69
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    invoke-direct {v3, v0}, Lajx;-><init>(Ljava/util/List;)V

    .line 70
    new-instance v5, Lcom/twitter/android/moments/ui/maker/o;

    invoke-direct {v5}, Lcom/twitter/android/moments/ui/maker/o;-><init>()V

    .line 71
    new-instance v0, Lact$a;

    .line 72
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const/4 v4, 0x0

    invoke-direct {v0, v2, v5, v4}, Lact$a;-><init>(Landroid/view/LayoutInflater;Lcom/twitter/android/moments/ui/maker/o;Lact$1;)V

    .line 73
    new-instance v2, Lajt;

    new-instance v4, Lajs;

    invoke-direct {v4, v3, v0}, Lajs;-><init>(Lajv;Lajr;)V

    invoke-direct {v2, v4}, Lajt;-><init>(Lajs;)V

    .line 76
    invoke-static/range {p7 .. p8}, Lzv;->a(J)Lzv;

    move-result-object v10

    .line 77
    new-instance v0, Lact;

    move-object v4, p2

    move-object v6, p3

    move-object v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    invoke-direct/range {v0 .. v10}, Lact;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/l;Lajt;Lajx;Lwy;Lcom/twitter/android/moments/ui/maker/o;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/model/moments/r;Lcom/twitter/model/moments/w;Landroid/content/res/Resources;Lzv;)V

    return-object v0
.end method

.method private a(Lcom/twitter/model/moments/r;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 117
    new-instance v0, Lact$2;

    invoke-direct {v0, p0, p1}, Lact$2;-><init>(Lact;Lcom/twitter/model/moments/r;)V

    return-object v0
.end method

.method private a(I)Lcfd;
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 135
    iget-object v0, p0, Lact;->i:Lcom/twitter/model/moments/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lact;->i:Lcom/twitter/model/moments/w;

    iget v0, v0, Lcom/twitter/model/moments/w;->c:I

    .line 137
    :goto_0
    new-instance v1, Lcet;

    new-instance v2, Lcom/twitter/model/moments/w;

    invoke-direct {v2, v0, p1}, Lcom/twitter/model/moments/w;-><init>(II)V

    invoke-direct {v1, v2}, Lcet;-><init>(Lcom/twitter/model/moments/w;)V

    return-object v1

    .line 135
    :cond_0
    iget-object v0, p0, Lact;->j:Landroid/content/res/Resources;

    .line 136
    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/bf;->a(Landroid/content/res/Resources;)I

    move-result v0

    goto :goto_0
.end method

.method private a(ILcom/twitter/model/moments/r;)Lcfd;
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 142
    iget-object v0, p0, Lact;->i:Lcom/twitter/model/moments/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lact;->i:Lcom/twitter/model/moments/w;

    iget v0, v0, Lcom/twitter/model/moments/w;->c:I

    .line 144
    :goto_0
    new-instance v1, Lceu;

    new-instance v2, Lcom/twitter/model/moments/w;

    invoke-direct {v2, v0, p1}, Lcom/twitter/model/moments/w;-><init>(II)V

    invoke-direct {v1, p2, v2}, Lceu;-><init>(Lcom/twitter/model/moments/r;Lcom/twitter/model/moments/w;)V

    return-object v1

    .line 142
    :cond_0
    iget-object v0, p0, Lact;->j:Landroid/content/res/Resources;

    .line 143
    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/bf;->a(Landroid/content/res/Resources;)I

    move-result v0

    goto :goto_0
.end method

.method static synthetic a(Lact;I)Lcfd;
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lact;->a(I)Lcfd;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lact;ILcom/twitter/model/moments/r;)Lcfd;
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lact;->a(ILcom/twitter/model/moments/r;)Lcfd;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lact;)Lcom/twitter/android/moments/ui/maker/o;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lact;->f:Lcom/twitter/android/moments/ui/maker/o;

    return-object v0
.end method

.method static synthetic b(Lact;)Lwy;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lact;->e:Lwy;

    return-object v0
.end method

.method static synthetic c(Lact;)Lzv;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lact;->b:Lzv;

    return-object v0
.end method

.method static synthetic d(Lact;)Lcom/twitter/android/moments/ui/maker/navigation/ah;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lact;->h:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    return-object v0
.end method

.method private d()Lrx/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/i",
            "<",
            "Ladd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 149
    new-instance v0, Lact$3;

    invoke-direct {v0, p0}, Lact$3;-><init>(Lact;)V

    return-object v0
.end method

.method static synthetic e(Lact;)Lajt;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lact;->c:Lajt;

    return-object v0
.end method

.method private static e()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ladd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 174
    const-string/jumbo v0, "moments_config_moment_maker_hex_color_array"

    .line 175
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 176
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 177
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 178
    instance-of v3, v0, Ljava/lang/Number;

    if-eqz v3, :cond_0

    .line 179
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    .line 180
    const/high16 v3, -0x1000000

    or-int/2addr v0, v3

    .line 181
    new-instance v3, Ladd;

    invoke-direct {v3, v0}, Ladd;-><init>(I)V

    invoke-virtual {v1, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 183
    :cond_0
    new-instance v3, Lcom/twitter/library/util/InvalidDataException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Color options should be integer values but were "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 184
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/twitter/library/util/InvalidDataException;-><init>(Ljava/lang/String;)V

    .line 183
    invoke-static {v3}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 187
    :cond_1
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lact;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/l;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/l;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lact;->d:Lajx;

    invoke-static {}, Lact;->e()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lajx;->a(Ljava/util/List;)V

    .line 165
    iget-object v0, p0, Lact;->c:Lajt;

    invoke-virtual {v0}, Lajt;->a()V

    .line 166
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lact;->g:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 170
    return-void
.end method
