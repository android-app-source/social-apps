.class public Lcei;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Lcom/twitter/model/moments/GuideCategories;

.field public final b:Lcom/twitter/model/moments/h;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcek;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/twitter/model/moments/j;

.field public final e:Lcfp;

.field public final f:Ljava/lang/String;

.field public final g:J

.field public final h:Lcej;


# direct methods
.method public constructor <init>(Lcom/twitter/model/moments/GuideCategories;Lcom/twitter/model/moments/h;Ljava/util/List;Lcom/twitter/model/moments/j;Lcfp;JLjava/lang/String;Lcej;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/GuideCategories;",
            "Lcom/twitter/model/moments/h;",
            "Ljava/util/List",
            "<",
            "Lcek;",
            ">;",
            "Lcom/twitter/model/moments/j;",
            "Lcfp;",
            "J",
            "Ljava/lang/String;",
            "Lcej;",
            ")V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcei;->a:Lcom/twitter/model/moments/GuideCategories;

    .line 43
    iput-object p2, p0, Lcei;->b:Lcom/twitter/model/moments/h;

    .line 44
    invoke-static {p3}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcei;->c:Ljava/util/List;

    .line 45
    iput-object p4, p0, Lcei;->d:Lcom/twitter/model/moments/j;

    .line 46
    iput-object p5, p0, Lcei;->e:Lcfp;

    .line 47
    iput-wide p6, p0, Lcei;->g:J

    .line 48
    iput-object p8, p0, Lcei;->f:Ljava/lang/String;

    .line 49
    iput-object p9, p0, Lcei;->h:Lcej;

    .line 50
    return-void
.end method
