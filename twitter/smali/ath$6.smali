.class Lath$6;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/composer/TweetBox$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lath;-><init>(Lath$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lath;


# direct methods
.method constructor <init>(Lath;)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, Lath$6;->a:Lath;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lath$6;->a:Lath;

    invoke-static {v0}, Lath;->g(Lath;)Lcom/twitter/android/composer/ComposerCountView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/ComposerCountView;->a(I)I

    .line 308
    iget-object v0, p0, Lath$6;->a:Lath;

    invoke-static {v0}, Lath;->h(Lath;)Lcom/twitter/android/composer/TweetBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lath$6;->a:Lath;

    invoke-static {v0}, Lath;->i(Lath;)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f0a06b9

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 313
    :goto_0
    iget-object v0, p0, Lath$6;->a:Lath;

    invoke-static {v0}, Lath;->i(Lath;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lath$6;->a:Lath;

    invoke-static {v1}, Lath;->h(Lath;)Lcom/twitter/android/composer/TweetBox;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/composer/TweetBox;->p()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 314
    return-void

    .line 311
    :cond_0
    iget-object v0, p0, Lath$6;->a:Lath;

    invoke-static {v0}, Lath;->i(Lath;)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f0a06c0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 332
    iget-object v0, p0, Lath$6;->a:Lath;

    invoke-static {v0}, Lath;->j(Lath;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 333
    iget-object v0, p0, Lath$6;->a:Lath;

    invoke-static {v0}, Lath;->k(Lath;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 334
    iget-object v0, p0, Lath$6;->a:Lath;

    invoke-static {v0}, Lath;->l(Lath;)V

    .line 335
    invoke-static {}, Lbpk;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 336
    iget-object v0, p0, Lath$6;->a:Lath;

    invoke-static {v0}, Lath;->h(Lath;)Lcom/twitter/android/composer/TweetBox;

    move-result-object v0

    const v1, 0x7f13027a

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 337
    invoke-virtual {v0}, Landroid/widget/EditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02082b

    .line 338
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 340
    invoke-virtual {v0}, Landroid/widget/EditText;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f110190

    invoke-static {v2, v3}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    invoke-static {v1, v2}, Lcnd;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    .line 342
    invoke-virtual {v0}, Landroid/widget/EditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 343
    aget-object v3, v2, v4

    const/4 v4, 0x1

    aget-object v4, v2, v4

    const/4 v5, 0x3

    aget-object v2, v2, v5

    invoke-virtual {v0, v3, v4, v1, v2}, Landroid/widget/EditText;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 347
    invoke-virtual {v0}, Landroid/widget/EditText;->invalidate()V

    .line 348
    new-instance v1, Lath$6$1;

    invoke-direct {v1, p0, v0}, Lath$6$1;-><init>(Lath$6;Landroid/widget/EditText;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 366
    :cond_0
    iget-object v0, p0, Lath$6;->a:Lath;

    iget-object v1, p0, Lath$6;->a:Lath;

    invoke-static {v1}, Lath;->j(Lath;)Z

    move-result v1

    or-int/2addr v1, p1

    invoke-static {v0, v1}, Lath;->b(Lath;Z)Z

    .line 367
    iget-object v0, p0, Lath$6;->a:Lath;

    invoke-static {v0}, Lath;->i(Lath;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lath$6;->a:Lath;

    invoke-static {v1}, Lath;->h(Lath;)Lcom/twitter/android/composer/TweetBox;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/composer/TweetBox;->p()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 368
    return-void
.end method

.method public a(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 379
    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lath$6;->a:Lath;

    invoke-static {v0}, Lath;->f(Lath;)Latg$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lath$6;->a:Lath;

    invoke-static {v0}, Lath;->f(Lath;)Latg$a;

    move-result-object v0

    invoke-interface {v0}, Latg$a;->b()V

    .line 328
    :cond_0
    return-void
.end method

.method public bk_()V
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lath$6;->a:Lath;

    invoke-static {v0}, Lath;->f(Lath;)Latg$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lath$6;->a:Lath;

    invoke-static {v0}, Lath;->f(Lath;)Latg$a;

    move-result-object v0

    invoke-interface {v0}, Latg$a;->a()V

    .line 321
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lath$6;->a:Lath;

    invoke-static {v0}, Lath;->f(Lath;)Latg$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 373
    iget-object v0, p0, Lath$6;->a:Lath;

    invoke-static {v0}, Lath;->f(Lath;)Latg$a;

    move-result-object v0

    invoke-interface {v0}, Latg$a;->a()V

    .line 375
    :cond_0
    return-void
.end method
