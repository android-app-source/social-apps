.class public final Lcaj$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcaj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TweetType:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/twitter/util/object/i",
        "<",
        "Lcaj",
        "<TTweetType;>;>;"
    }
.end annotation


# instance fields
.field private a:J

.field private b:J

.field private c:J

.field private d:Lcag;

.field private e:Lcag;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TTweetType;>;"
        }
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Lcan;

.field private n:Lcae;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcaj$a;)J
    .locals 2

    .prologue
    .line 93
    iget-wide v0, p0, Lcaj$a;->a:J

    return-wide v0
.end method

.method static synthetic b(Lcaj$a;)J
    .locals 2

    .prologue
    .line 93
    iget-wide v0, p0, Lcaj$a;->b:J

    return-wide v0
.end method

.method static synthetic c(Lcaj$a;)J
    .locals 2

    .prologue
    .line 93
    iget-wide v0, p0, Lcaj$a;->c:J

    return-wide v0
.end method

.method static synthetic d(Lcaj$a;)Lcag;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcaj$a;->d:Lcag;

    return-object v0
.end method

.method static synthetic e(Lcaj$a;)Lcag;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcaj$a;->e:Lcag;

    return-object v0
.end method

.method static synthetic f(Lcaj$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcaj$a;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcaj$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcaj$a;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcaj$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcaj$a;->h:Ljava/util/List;

    return-object v0
.end method

.method static synthetic i(Lcaj$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcaj$a;->i:Ljava/util/List;

    return-object v0
.end method

.method static synthetic j(Lcaj$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcaj$a;->j:Ljava/util/List;

    return-object v0
.end method

.method static synthetic k(Lcaj$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcaj$a;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcaj$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcaj$a;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic m(Lcaj$a;)Lcan;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcaj$a;->m:Lcan;

    return-object v0
.end method

.method static synthetic n(Lcaj$a;)Lcae;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcaj$a;->n:Lcae;

    return-object v0
.end method

.method static synthetic o(Lcaj$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcaj$a;->o:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p(Lcaj$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcaj$a;->p:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic q(Lcaj$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcaj$a;->q:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcaj$a;->d:Lcag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcaj$a;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcaj$a;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcaj$a;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)Lcaj$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lcaj$a",
            "<TTweetType;>;"
        }
    .end annotation

    .prologue
    .line 128
    iput-wide p1, p0, Lcaj$a;->a:J

    .line 129
    return-object p0
.end method

.method public a(Lcae;)Lcaj$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcae;",
            ")",
            "Lcaj$a",
            "<TTweetType;>;"
        }
    .end annotation

    .prologue
    .line 200
    iput-object p1, p0, Lcaj$a;->n:Lcae;

    .line 201
    return-object p0
.end method

.method public a(Lcag;)Lcaj$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcag;",
            ")",
            "Lcaj$a",
            "<TTweetType;>;"
        }
    .end annotation

    .prologue
    .line 146
    iput-object p1, p0, Lcaj$a;->d:Lcag;

    .line 147
    return-object p0
.end method

.method public a(Lcan;)Lcaj$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcan;",
            ")",
            "Lcaj$a",
            "<TTweetType;>;"
        }
    .end annotation

    .prologue
    .line 188
    iput-object p1, p0, Lcaj$a;->m:Lcan;

    .line 189
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcaj$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcaj$a",
            "<TTweetType;>;"
        }
    .end annotation

    .prologue
    .line 152
    iput-object p1, p0, Lcaj$a;->f:Ljava/lang/String;

    .line 153
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcaj$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)",
            "Lcaj$a",
            "<TTweetType;>;"
        }
    .end annotation

    .prologue
    .line 164
    iput-object p1, p0, Lcaj$a;->h:Ljava/util/List;

    .line 165
    return-object p0
.end method

.method public b(J)Lcaj$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lcaj$a",
            "<TTweetType;>;"
        }
    .end annotation

    .prologue
    .line 134
    iput-wide p1, p0, Lcaj$a;->b:J

    .line 135
    return-object p0
.end method

.method public b(Lcag;)Lcaj$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcag;",
            ")",
            "Lcaj$a",
            "<TTweetType;>;"
        }
    .end annotation

    .prologue
    .line 194
    iput-object p1, p0, Lcaj$a;->e:Lcag;

    .line 195
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcaj$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcaj$a",
            "<TTweetType;>;"
        }
    .end annotation

    .prologue
    .line 158
    iput-object p1, p0, Lcaj$a;->g:Ljava/lang/String;

    .line 159
    return-object p0
.end method

.method public b(Ljava/util/List;)Lcaj$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TTweetType;>;)",
            "Lcaj$a",
            "<TTweetType;>;"
        }
    .end annotation

    .prologue
    .line 170
    iput-object p1, p0, Lcaj$a;->i:Ljava/util/List;

    .line 171
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lcaj$a;->e()Lcaj;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Lcaj$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lcaj$a",
            "<TTweetType;>;"
        }
    .end annotation

    .prologue
    .line 140
    iput-wide p1, p0, Lcaj$a;->c:J

    .line 141
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcaj$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcaj$a",
            "<TTweetType;>;"
        }
    .end annotation

    .prologue
    .line 182
    iput-object p1, p0, Lcaj$a;->l:Ljava/lang/String;

    .line 183
    return-object p0
.end method

.method public c(Ljava/util/List;)Lcaj$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)",
            "Lcaj$a",
            "<TTweetType;>;"
        }
    .end annotation

    .prologue
    .line 176
    iput-object p1, p0, Lcaj$a;->j:Ljava/util/List;

    .line 177
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcaj$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcaj$a",
            "<TTweetType;>;"
        }
    .end annotation

    .prologue
    .line 206
    iput-object p1, p0, Lcaj$a;->o:Ljava/lang/String;

    .line 207
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcaj$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcaj$a",
            "<TTweetType;>;"
        }
    .end annotation

    .prologue
    .line 212
    iput-object p1, p0, Lcaj$a;->p:Ljava/lang/String;

    .line 213
    return-object p0
.end method

.method protected e()Lcaj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcaj",
            "<TTweetType;>;"
        }
    .end annotation

    .prologue
    .line 239
    new-instance v0, Lcaj;

    invoke-direct {v0, p0}, Lcaj;-><init>(Lcaj$a;)V

    return-object v0
.end method

.method public f(Ljava/lang/String;)Lcaj$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcaj$a",
            "<TTweetType;>;"
        }
    .end annotation

    .prologue
    .line 218
    iput-object p1, p0, Lcaj$a;->k:Ljava/lang/String;

    .line 219
    return-object p0
.end method

.method public g(Ljava/lang/String;)Lcaj$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcaj$a",
            "<TTweetType;>;"
        }
    .end annotation

    .prologue
    .line 224
    iput-object p1, p0, Lcaj$a;->q:Ljava/lang/String;

    .line 225
    return-object p0
.end method
