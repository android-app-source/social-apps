.class public Lbkw;
.super Lbks;
.source "Twttr"


# instance fields
.field private final e:Lcom/twitter/model/core/MediaEntity;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/model/core/MediaEntity;)V
    .locals 1

    .prologue
    .line 26
    invoke-static {p2}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/MediaEntity;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lbkw;-><init>(Lcom/twitter/library/av/playback/AVDataSource;Ljava/lang/String;Lcom/twitter/model/core/MediaEntity;)V

    .line 27
    return-void
.end method

.method constructor <init>(Lcom/twitter/library/av/playback/AVDataSource;Ljava/lang/String;Lcom/twitter/model/core/MediaEntity;)V
    .locals 1

    .prologue
    .line 31
    invoke-static {p2}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lbks;-><init>(Lcom/twitter/library/av/playback/AVDataSource;Ljava/lang/String;)V

    .line 32
    iput-object p3, p0, Lbkw;->e:Lcom/twitter/model/core/MediaEntity;

    .line 33
    return-void
.end method


# virtual methods
.method protected b(Lcom/twitter/network/j;Lcom/twitter/network/HttpOperation;Ljava/util/Map;Lcom/twitter/model/av/DynamicAdInfo;)Lcom/twitter/model/av/AVMediaPlaylist;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/j;",
            "Lcom/twitter/network/HttpOperation;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/twitter/model/av/DynamicAdInfo;",
            ")",
            "Lcom/twitter/model/av/AVMediaPlaylist;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 38
    iget-object v0, p0, Lbkw;->e:Lcom/twitter/model/core/MediaEntity;

    iget-wide v0, v0, Lcom/twitter/model/core/MediaEntity;->c:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 39
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v1

    invoke-virtual {v1}, Lcrr;->e()Lcom/twitter/util/network/c;

    move-result-object v1

    .line 41
    invoke-virtual {p0, p4, v1}, Lbkw;->a(Lcom/twitter/model/av/DynamicAdInfo;Lcom/twitter/util/network/c;)Lcom/twitter/model/av/Video;

    move-result-object v2

    .line 42
    new-instance v3, Lcom/twitter/model/av/Video$a;

    invoke-direct {v3}, Lcom/twitter/model/av/Video$a;-><init>()V

    invoke-virtual {v3, v0}, Lcom/twitter/model/av/Video$a;->a(Ljava/lang/String;)Lcom/twitter/model/av/Video$a;

    move-result-object v0

    const-string/jumbo v3, "video"

    .line 43
    invoke-virtual {v0, v3}, Lcom/twitter/model/av/Video$a;->b(Ljava/lang/String;)Lcom/twitter/model/av/Video$a;

    move-result-object v0

    .line 44
    invoke-virtual {v0, v4}, Lcom/twitter/model/av/Video$a;->b(Z)Lcom/twitter/model/av/Video$a;

    move-result-object v0

    .line 45
    invoke-virtual {v0, v4}, Lcom/twitter/model/av/Video$a;->d(Z)Lcom/twitter/model/av/Video$a;

    move-result-object v0

    .line 46
    invoke-virtual {p0}, Lbkw;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/model/av/Video$a;->c(Ljava/lang/String;)Lcom/twitter/model/av/Video$a;

    move-result-object v0

    .line 47
    invoke-virtual {v0, v4}, Lcom/twitter/model/av/Video$a;->a(Z)Lcom/twitter/model/av/Video$a;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lcom/twitter/model/av/Video$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/av/Video;

    .line 50
    new-instance v3, Lcom/twitter/model/av/MediaEntityPlaylist;

    iget-object v1, v1, Lcom/twitter/util/network/c;->b:Ljava/lang/String;

    invoke-direct {v3, v1, v0, v2, p4}, Lcom/twitter/model/av/MediaEntityPlaylist;-><init>(Ljava/lang/String;Lcom/twitter/model/av/Video;Lcom/twitter/model/av/Video;Lcom/twitter/model/av/DynamicAdInfo;)V

    return-object v3
.end method
