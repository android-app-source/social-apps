.class final Laqx$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lare;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Laqx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Laqx;

.field private final b:Lant;

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/base/BaseFragmentActivity;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/support/v4/app/FragmentManager;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/d;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/Flow$Step;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laqw;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/model/onboarding/TaskContext;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laqs;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/onboarding/Subtask;",
            "Landroid/content/Intent;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcje;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcjg;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcjd;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/k;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lank;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/l;",
            ">;>;"
        }
    .end annotation
.end field

.field private s:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/l;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/i;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/b;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Larq;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Laqx;Lant;)V
    .locals 1

    .prologue
    .line 242
    iput-object p1, p0, Laqx$a;->a:Laqx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243
    invoke-static {p2}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lant;

    iput-object v0, p0, Laqx$a;->b:Lant;

    .line 244
    invoke-direct {p0}, Laqx$a;->c()V

    .line 245
    return-void
.end method

.method synthetic constructor <init>(Laqx;Lant;Laqx$1;)V
    .locals 0

    .prologue
    .line 194
    invoke-direct {p0, p1, p2}, Laqx$a;-><init>(Laqx;Lant;)V

    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 250
    iget-object v0, p0, Laqx$a;->b:Lant;

    .line 252
    invoke-static {v0}, Lanu;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 251
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->c:Lcta;

    .line 254
    iget-object v0, p0, Laqx$a;->c:Lcta;

    .line 256
    invoke-static {v0}, Lanx;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 255
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->d:Lcta;

    .line 258
    iget-object v0, p0, Laqx$a;->c:Lcta;

    .line 260
    invoke-static {v0}, Lanv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 259
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->e:Lcta;

    .line 263
    iget-object v0, p0, Laqx$a;->e:Lcta;

    .line 265
    invoke-static {v0}, Laob;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 264
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->f:Lcta;

    .line 268
    iget-object v0, p0, Laqx$a;->c:Lcta;

    iget-object v1, p0, Laqx$a;->d:Lcta;

    iget-object v2, p0, Laqx$a;->f:Lcta;

    .line 270
    invoke-static {v0, v1, v2}, Lcom/twitter/app/onboarding/flowstep/common/e;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 269
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->g:Lcta;

    .line 276
    invoke-static {}, Lard;->c()Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->h:Lcta;

    .line 278
    iget-object v0, p0, Laqx$a;->h:Lcta;

    iget-object v1, p0, Laqx$a;->f:Lcta;

    .line 280
    invoke-static {v0, v1}, Larh;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 279
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->i:Lcta;

    .line 283
    iget-object v0, p0, Laqx$a;->c:Lcta;

    .line 285
    invoke-static {v0}, Laqv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 284
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->j:Lcta;

    .line 288
    iget-object v0, p0, Laqx$a;->a:Laqx;

    .line 290
    invoke-static {v0}, Laqx;->a(Laqx;)Lcta;

    move-result-object v0

    .line 289
    invoke-static {v0}, Laqt;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->k:Lcta;

    .line 292
    iget-object v0, p0, Laqx$a;->k:Lcta;

    .line 293
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->l:Lcta;

    .line 295
    iget-object v0, p0, Laqx$a;->l:Lcta;

    .line 297
    invoke-static {v0}, Lcjf;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 296
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->m:Lcta;

    .line 299
    iget-object v0, p0, Laqx$a;->j:Lcta;

    iget-object v1, p0, Laqx$a;->m:Lcta;

    iget-object v2, p0, Laqx$a;->a:Laqx;

    .line 304
    invoke-static {v2}, Laqx;->b(Laqx;)Lcta;

    move-result-object v2

    .line 305
    invoke-static {}, Lcjc;->c()Ldagger/internal/c;

    move-result-object v3

    .line 301
    invoke-static {v0, v1, v2, v3}, Lcjh;->a(Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 300
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->n:Lcta;

    .line 307
    iget-object v0, p0, Laqx$a;->n:Lcta;

    .line 308
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->o:Lcta;

    .line 310
    iget-object v0, p0, Laqx$a;->g:Lcta;

    .line 312
    invoke-static {v0}, Lari;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 311
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->p:Lcta;

    .line 315
    iget-object v0, p0, Laqx$a;->b:Lant;

    .line 317
    invoke-static {v0}, Laoa;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 316
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->q:Lcta;

    .line 319
    iget-object v0, p0, Laqx$a;->q:Lcta;

    .line 321
    invoke-static {v0}, Larj;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 320
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->r:Lcta;

    .line 324
    iget-object v0, p0, Laqx$a;->r:Lcta;

    .line 326
    invoke-static {v0}, Lcom/twitter/app/onboarding/flowstep/common/m;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 325
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->s:Lcta;

    .line 329
    iget-object v0, p0, Laqx$a;->p:Lcta;

    iget-object v1, p0, Laqx$a;->s:Lcta;

    .line 331
    invoke-static {v0, v1}, Lcom/twitter/app/onboarding/flowstep/common/j;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 330
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->t:Lcta;

    .line 334
    iget-object v0, p0, Laqx$a;->g:Lcta;

    iget-object v1, p0, Laqx$a;->i:Lcta;

    iget-object v2, p0, Laqx$a;->o:Lcta;

    iget-object v3, p0, Laqx$a;->t:Lcta;

    .line 336
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/app/onboarding/flowstep/common/c;->a(Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 335
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->u:Lcta;

    .line 345
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Laqx$a;->u:Lcta;

    iget-object v2, p0, Laqx$a;->s:Lcta;

    iget-object v3, p0, Laqx$a;->t:Lcta;

    .line 344
    invoke-static {v0, v1, v2, v3}, Larr;->a(Lcsd;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 343
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->v:Lcta;

    .line 350
    iget-object v0, p0, Laqx$a;->v:Lcta;

    .line 351
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$a;->w:Lcta;

    .line 352
    return-void
.end method


# virtual methods
.method public a()Laog;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Laqx$a;->w:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laog;

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 356
    invoke-static {}, Ldagger/internal/f;->a()Ldagger/internal/c;

    move-result-object v0

    invoke-interface {v0}, Ldagger/internal/c;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method
