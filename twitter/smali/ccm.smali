.class public Lccm;
.super Lccs;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lccm$b;,
        Lccm$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Lccm;",
            "Lccm$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    new-instance v0, Lccm$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lccm$b;-><init>(Lccm$1;)V

    sput-object v0, Lccm;->a:Lcom/twitter/util/serialization/b;

    return-void
.end method

.method private constructor <init>(Lccm$a;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lccs;-><init>(Lccs$a;)V

    .line 28
    invoke-static {p1}, Lccm$a;->a(Lccm$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lccm;->b:Ljava/lang/String;

    .line 29
    invoke-static {p1}, Lccm$a;->b(Lccm$a;)Z

    move-result v0

    iput-boolean v0, p0, Lccm;->c:Z

    .line 30
    return-void
.end method

.method synthetic constructor <init>(Lccm$a;Lccm$1;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lccm;-><init>(Lccm$a;)V

    return-void
.end method

.method private a(Lccm;)Z
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lccm;->b:Ljava/lang/String;

    iget-object v1, p1, Lccm;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lccm;->c:Z

    iget-boolean v1, p1, Lccm;->c:Z

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 34
    invoke-super {p0, p1}, Lccs;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lccm;

    if-eqz v0, :cond_0

    check-cast p1, Lccm;

    .line 35
    invoke-direct {p0, p1}, Lccm;->a(Lccm;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 34
    :goto_0
    return v0

    .line 35
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 45
    invoke-super {p0}, Lccs;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lccm;->e:Ljava/lang/String;

    iget-object v2, p0, Lccm;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
