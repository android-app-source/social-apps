.class public Lapu;
.super Lapt;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/dm/u$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lapu$a;,
        Lapu$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lapt",
        "<",
        "Lcom/twitter/model/dms/z;",
        ">;",
        "Lcom/twitter/app/dm/u$a;"
    }
.end annotation


# instance fields
.field private final l:Landroid/widget/TextView;

.field private final m:Landroid/view/ViewGroup;

.field private final n:Landroid/widget/TextView;

.field private final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Lcom/twitter/app/dm/u;

.field private final q:Z


# direct methods
.method private constructor <init>(Lapu$a;)V
    .locals 4

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lapt;-><init>(Lapn$a;)V

    .line 43
    invoke-static {p1}, Lapu$a;->a(Lapu$a;)Z

    move-result v0

    iput-boolean v0, p0, Lapu;->q:Z

    .line 45
    iget-object v0, p1, Lapu$a;->a:Lapn$b;

    check-cast v0, Lapu$b;

    .line 46
    invoke-static {v0}, Lapu$b;->a(Lapu$b;)Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Lapu;->l:Landroid/widget/TextView;

    .line 47
    invoke-static {v0}, Lapu$b;->b(Lapu$b;)Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p0, Lapu;->m:Landroid/view/ViewGroup;

    .line 48
    invoke-static {v0}, Lapu$b;->c(Lapu$b;)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lapu;->n:Landroid/widget/TextView;

    .line 50
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    iget-object v0, p0, Lapu;->a:Lcom/twitter/model/dms/c;

    check-cast v0, Lcom/twitter/model/dms/z;

    .line 51
    invoke-virtual {v0}, Lcom/twitter/model/dms/z;->d()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-result-object v0

    iget-wide v2, p0, Lapu;->i:J

    .line 52
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->d(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lapu;->o:Ljava/util/List;

    .line 54
    invoke-static {p1}, Lapu$a;->b(Lapu$a;)Lcom/twitter/app/dm/u;

    move-result-object v0

    iput-object v0, p0, Lapu;->p:Lcom/twitter/app/dm/u;

    .line 55
    return-void
.end method

.method synthetic constructor <init>(Lapu$a;Lapu$1;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lapu;-><init>(Lapu$a;)V

    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 96
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lapu;->h:Landroid/content/res/Resources;

    const v1, 0x7f0a0280

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    .line 97
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/4 v5, 0x0

    .line 73
    iget-object v0, p0, Lapu;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lapu;->b:Lcom/twitter/model/dms/m;

    iget-object v1, v1, Lcom/twitter/model/dms/m;->b:Ljava/lang/String;

    invoke-direct {p0, v1}, Lapu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    iget-object v0, p0, Lapu;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 76
    iget-object v0, p0, Lapu;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 77
    iget-object v0, p0, Lapu;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 78
    iget-object v0, p0, Lapu;->m:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 80
    iget-object v0, p0, Lapu;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lapu;->h:Landroid/content/res/Resources;

    const v2, 0x7f0a02b6

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lapu;->o:Ljava/util/List;

    .line 81
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    .line 80
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v0, p0, Lapu;->p:Lcom/twitter/app/dm/u;

    iget-object v1, p0, Lapu;->o:Ljava/util/List;

    invoke-virtual {v0, v1, p0}, Lcom/twitter/app/dm/u;->a(Ljava/util/Collection;Lcom/twitter/app/dm/u$a;)V

    .line 85
    iget-boolean v0, p0, Lapu;->q:Z

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lapu;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    iget-object v0, p0, Lapu;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 90
    iget-object v0, p0, Lapu;->m:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lapu;->m:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 61
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    const/4 v0, 0x6

    if-ge v2, v0, :cond_0

    iget-object v0, p0, Lapu;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 62
    iget-object v0, p0, Lapu;->g:Landroid/content/Context;

    const v1, 0x7f0400ae

    const/4 v3, 0x0

    invoke-static {v0, v1, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/user/UserView;

    .line 63
    const v1, 0x7f130097

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/media/ui/image/UserImageView;

    .line 64
    sget-object v3, Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;->c:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    invoke-virtual {v1, v3}, Lcom/twitter/media/ui/image/UserImageView;->setRoundingStrategy(Lcom/twitter/media/ui/image/config/g;)V

    .line 65
    new-instance v1, Lcom/twitter/android/db;

    invoke-direct {v1, v0}, Lcom/twitter/android/db;-><init>(Lcom/twitter/ui/user/BaseUserView;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserView;->setTag(Ljava/lang/Object;)V

    .line 66
    iget-object v1, p0, Lapu;->m:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 67
    invoke-virtual {v0}, Lcom/twitter/ui/user/UserView;->getImageView()Lcom/twitter/media/ui/image/UserImageView;

    move-result-object v1

    iget-object v0, p0, Lapu;->o:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;)Z

    .line 61
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 69
    :cond_0
    return-void
.end method
