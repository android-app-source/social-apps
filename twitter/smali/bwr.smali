.class public abstract Lbwr;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 16
    invoke-static {p0}, Lbwr;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    invoke-virtual {v0}, Lcax;->g()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->av()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/Tweet;ZZ)Z
    .locals 2

    .prologue
    .line 34
    invoke-static {}, Lcmj;->g()Z

    move-result v0

    .line 35
    if-eqz p0, :cond_2

    .line 36
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->x()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez p1, :cond_0

    if-eqz v0, :cond_2

    :cond_0
    if-eqz p2, :cond_1

    const-string/jumbo v0, "twitter_access_android_media_forward_enabled"

    .line 37
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 39
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 35
    :goto_0
    return v0

    .line 39
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/twitter/model/core/Tweet;)Z
    .locals 2

    .prologue
    .line 24
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 25
    invoke-static {}, Lcom/twitter/library/card/ae;->b()Lcom/twitter/library/card/ae;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/ae;->e(Lcax;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 24
    :goto_0
    return v0

    .line 25
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/twitter/model/core/Tweet;)Z
    .locals 2

    .prologue
    .line 56
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 57
    invoke-static {}, Lcom/twitter/library/card/ae;->b()Lcom/twitter/library/card/ae;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/ae;->d(Lcax;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 56
    :goto_0
    return v0

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
