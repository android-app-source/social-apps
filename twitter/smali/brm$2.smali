.class Lbrm$2;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbrm;->a(Ljava/util/List;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/Map;

.field final synthetic b:Lbrm;


# direct methods
.method constructor <init>(Lbrm;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lbrm$2;->b:Lbrm;

    iput-object p2, p0, Lbrm$2;->a:Ljava/util/Map;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 102
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lbrm$2;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 6

    .prologue
    .line 105
    check-cast p1, Lbro;

    invoke-virtual {p1}, Lbro;->g()Lcdx;

    move-result-object v0

    .line 106
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcdx;->a:Ljava/util/List;

    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 107
    new-instance v1, Lbrm$c;

    iget-object v2, p0, Lbrm$2;->b:Lbrm;

    .line 108
    invoke-static {v2}, Lbrm;->a(Lbrm;)Lcom/twitter/library/provider/t;

    move-result-object v2

    iget-object v3, v0, Lcdx;->a:Ljava/util/List;

    invoke-direct {v1, v2, v3}, Lbrm$c;-><init>(Lcom/twitter/library/provider/t;Ljava/util/List;)V

    .line 109
    iget-object v2, p0, Lbrm$2;->b:Lbrm;

    invoke-static {v2}, Lbrm;->b(Lbrm;)Lcom/twitter/library/client/p;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 110
    iget-object v0, v0, Lcdx;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    .line 111
    iget-object v2, p0, Lbrm$2;->a:Ljava/util/Map;

    iget-wide v4, v0, Lcdu;->h:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 114
    :cond_0
    iget-object v0, p0, Lbrm$2;->b:Lbrm;

    iget-object v1, p0, Lbrm$2;->a:Ljava/util/Map;

    invoke-static {v0, v1}, Lbrm;->a(Lbrm;Ljava/util/Map;)V

    .line 115
    return-void
.end method
