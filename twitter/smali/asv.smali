.class public abstract Lasv;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lala;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lasv$a;,
        Lasv$b;
    }
.end annotation


# instance fields
.field protected final a:Lasw;

.field protected final b:Lcom/twitter/library/client/Session;

.field protected c:Lcbi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcbi",
            "<",
            "Lcgm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lasw;Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lasv;->a:Lasw;

    .line 35
    iput-object p2, p0, Lasv;->b:Lcom/twitter/library/client/Session;

    .line 36
    return-void
.end method

.method protected static a(Ljava/lang/Iterable;Lcgm;)Lcgm;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcgm;",
            ">;",
            "Lcgm;",
            ")",
            "Lcgm;"
        }
    .end annotation

    .prologue
    .line 125
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgm;

    .line 126
    iget-object v2, v0, Lcgm;->d:Ljava/lang/String;

    iget-object v3, p1, Lcgm;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 130
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method


# virtual methods
.method public a(Lcgm;Lasv$a;)V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lasv;->a:Lasw;

    invoke-virtual {v0, p1}, Lasw;->a(Lcgm;)Lrx/c;

    move-result-object v0

    new-instance v1, Lasv$2;

    invoke-direct {v1, p0, p2, p1}, Lasv$2;-><init>(Lasv;Lasv$a;Lcgm;)V

    .line 65
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 87
    return-void
.end method

.method public a(Lcom/twitter/app/common/di/scope/InjectionScope;)V
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lcom/twitter/app/common/di/scope/InjectionScope;->c:Lcom/twitter/app/common/di/scope/InjectionScope;

    if-ne p1, v0, :cond_0

    .line 108
    iget-object v0, p0, Lasv;->a:Lasw;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 110
    :cond_0
    return-void
.end method

.method public a(ZLasv$b;)V
    .locals 2

    .prologue
    .line 39
    if-eqz p1, :cond_1

    iget-object v0, p0, Lasv;->c:Lcbi;

    if-eqz v0, :cond_1

    .line 40
    if-eqz p2, :cond_0

    .line 41
    iget-object v0, p0, Lasv;->c:Lcbi;

    invoke-interface {p2, v0}, Lasv$b;->a(Lcbi;)V

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 45
    :cond_1
    iget-object v0, p0, Lasv;->a:Lasw;

    invoke-virtual {v0}, Lasw;->a()Lrx/c;

    move-result-object v0

    new-instance v1, Lasv$1;

    invoke-direct {v1, p0, p2}, Lasv$1;-><init>(Lasv;Lasv$b;)V

    .line 46
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    goto :goto_0
.end method

.method public b(Lcgm;Lasv$a;)V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lasv;->a:Lasw;

    invoke-virtual {v0, p1}, Lasw;->b(Lcgm;)Lrx/c;

    move-result-object v0

    new-instance v1, Lasv$3;

    invoke-direct {v1, p0, p2, p1}, Lasv$3;-><init>(Lasv;Lasv$a;Lcgm;)V

    .line 91
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 103
    return-void
.end method
