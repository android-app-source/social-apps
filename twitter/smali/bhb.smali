.class public Lbhb;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcom/twitter/model/core/ac;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:J

.field private c:Z

.field private g:J

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V
    .locals 3

    .prologue
    .line 40
    const-class v0, Lbhb;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 41
    iput-wide p3, p0, Lbhb;->a:J

    .line 42
    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lbhb;->b:J

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;J)V
    .locals 3

    .prologue
    .line 48
    const-class v0, Lbhb;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 49
    iput-wide p3, p0, Lbhb;->a:J

    .line 50
    iget-wide v0, p2, Lcom/twitter/library/service/v;->c:J

    iput-wide v0, p0, Lbhb;->b:J

    .line 51
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/Tweet;)Lbhb;
    .locals 4

    .prologue
    .line 36
    new-instance v0, Lbhb;

    iget-wide v2, p2, Lcom/twitter/model/core/Tweet;->u:J

    invoke-direct {v0, p0, p1, v2, v3}, Lbhb;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    invoke-virtual {v0, p2}, Lbhb;->a(Lcom/twitter/model/core/Tweet;)Lbhb;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/Tweet;)Lbhb;
    .locals 2

    .prologue
    .line 55
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet;->c:Z

    iput-boolean v0, p0, Lbhb;->c:Z

    .line 56
    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->t:J

    iput-wide v0, p0, Lbhb;->g:J

    .line 57
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->Z()Z

    move-result v0

    iput-boolean v0, p0, Lbhb;->h:Z

    .line 58
    return-object p0
.end method

.method protected a()Lcom/twitter/library/service/d;
    .locals 6

    .prologue
    .line 64
    invoke-virtual {p0}, Lbhb;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 65
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "statuses"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "destroy"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 66
    invoke-virtual {p0}, Lbhb;->e()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->c()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    .line 64
    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/ac;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 79
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/library/service/b;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V

    .line 81
    iget-wide v0, p0, Lbhb;->b:J

    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v1

    .line 82
    invoke-virtual {p0}, Lbhb;->S()Laut;

    move-result-object v2

    .line 83
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    .line 86
    iget-wide v4, p0, Lbhb;->b:J

    invoke-virtual {v1, v4, v5, v0, v2}, Lcom/twitter/library/provider/t;->a(JLcom/twitter/model/core/ac;Laut;)V

    .line 87
    invoke-virtual {v2}, Laut;->a()V

    .line 88
    if-eqz v0, :cond_0

    iget-wide v2, v0, Lcom/twitter/model/core/ac;->j:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 90
    new-instance v1, Lbga;

    iget-object v2, p0, Lbhb;->p:Landroid/content/Context;

    invoke-virtual {p0}, Lbhb;->M()Lcom/twitter/library/service/v;

    move-result-object v3

    iget-wide v4, v0, Lcom/twitter/model/core/ac;->j:J

    invoke-direct {v1, v2, v3, v4, v5}, Lbga;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;J)V

    invoke-virtual {p0, v1}, Lbhb;->b(Lcom/twitter/async/service/AsyncOperation;)V

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    invoke-virtual {p2}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    const/16 v3, 0x194

    if-ne v0, v3, :cond_0

    .line 97
    invoke-virtual {p0}, Lbhb;->e()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/twitter/library/provider/t;->d(J)Lcom/twitter/model/core/ac;

    move-result-object v0

    .line 98
    if-eqz v0, :cond_2

    .line 99
    iget-wide v4, p0, Lbhb;->b:J

    invoke-virtual {v1, v4, v5, v0, v2}, Lcom/twitter/library/provider/t;->a(JLcom/twitter/model/core/ac;Laut;)V

    .line 100
    invoke-virtual {v2}, Laut;->a()V

    .line 101
    invoke-virtual {p2, v6}, Lcom/twitter/library/service/u;->a(Z)V

    goto :goto_0

    .line 105
    :cond_2
    iget-wide v2, p0, Lbhb;->a:J

    iget-wide v4, p0, Lbhb;->b:J

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/library/provider/t;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {p2, v6}, Lcom/twitter/library/service/u;->a(Z)V

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 26
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbhb;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/ac;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    const-class v0, Lcom/twitter/model/core/ac;

    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method protected b(Lcom/twitter/library/service/u;)Z
    .locals 7

    .prologue
    .line 115
    invoke-super {p0, p1}, Lcom/twitter/library/service/b;->b(Lcom/twitter/library/service/u;)Z

    move-result v0

    .line 117
    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lbhb;->c:Z

    if-eqz v1, :cond_1

    .line 118
    new-instance v1, Lbhg;

    iget-object v2, p0, Lbhb;->p:Landroid/content/Context;

    invoke-virtual {p0}, Lbhb;->M()Lcom/twitter/library/service/v;

    move-result-object v3

    iget-wide v4, p0, Lbhb;->g:J

    iget-boolean v6, p0, Lbhb;->h:Z

    invoke-direct/range {v1 .. v6}, Lbhg;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;JZ)V

    .line 120
    invoke-virtual {v1}, Lbhg;->O()Lcom/twitter/library/service/u;

    move-result-object v0

    .line 121
    invoke-virtual {v1, v0}, Lbhg;->c(Lcom/twitter/library/service/u;)V

    .line 122
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 123
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/twitter/library/service/u;->a(Z)V

    .line 125
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    .line 127
    :cond_1
    return v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 131
    iget-boolean v0, p0, Lbhb;->c:Z

    if-eqz v0, :cond_0

    .line 132
    iget-wide v0, p0, Lbhb;->g:J

    .line 134
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lbhb;->a:J

    goto :goto_0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lbhb;->b()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
