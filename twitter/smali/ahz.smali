.class public Lahz;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Lcom/twitter/internal/android/widget/HorizontalListView;

.field private final c:Landroid/support/v4/view/ViewPager;

.field private final d:Lcom/twitter/library/widget/StatusToolBar;

.field private final e:Lcom/twitter/android/composer/ComposerDockLayout;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Landroid/view/LayoutInflater;)V
    .locals 3

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const v0, 0x7f04039b

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lahz;->a:Landroid/view/View;

    .line 42
    iget-object v0, p0, Lahz;->a:Landroid/view/View;

    const v1, 0x7f13037a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/HorizontalListView;

    iput-object v0, p0, Lahz;->b:Lcom/twitter/internal/android/widget/HorizontalListView;

    .line 43
    iget-object v0, p0, Lahz;->a:Landroid/view/View;

    const v1, 0x7f130378

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lahz;->c:Landroid/support/v4/view/ViewPager;

    .line 44
    iget-object v0, p0, Lahz;->a:Landroid/view/View;

    const v1, 0x7f13007f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/StatusToolBar;

    iput-object v0, p0, Lahz;->d:Lcom/twitter/library/widget/StatusToolBar;

    .line 45
    iget-object v0, p0, Lahz;->a:Landroid/view/View;

    const v1, 0x7f1302e3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/composer/ComposerDockLayout;

    iput-object v0, p0, Lahz;->e:Lcom/twitter/android/composer/ComposerDockLayout;

    .line 46
    iget-object v0, p0, Lahz;->c:Landroid/support/v4/view/ViewPager;

    const v1, 0x7f0e0286

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 47
    iget-object v0, p0, Lahz;->c:Landroid/support/v4/view/ViewPager;

    const v1, 0x7f1100ca

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMarginDrawable(I)V

    .line 48
    return-void
.end method

.method private a(Lcom/twitter/library/client/m;)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0}, Lahz;->h()Lcom/twitter/android/AbsPagesAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/AbsPagesAdapter;->c(Lcom/twitter/library/client/m;)Lcom/twitter/app/common/base/BaseFragment;

    move-result-object v0

    return-object v0
.end method

.method private g()Lcom/twitter/app/common/list/TwitterListFragment;
    .locals 2

    .prologue
    .line 101
    invoke-direct {p0}, Lahz;->h()Lcom/twitter/android/AbsPagesAdapter;

    move-result-object v0

    iget-object v1, p0, Lahz;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/AbsPagesAdapter;->a(I)Lcom/twitter/library/client/m;

    move-result-object v0

    invoke-direct {p0, v0}, Lahz;->a(Lcom/twitter/library/client/m;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/TwitterListFragment;

    return-object v0
.end method

.method private h()Lcom/twitter/android/AbsPagesAdapter;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lahz;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/AbsPagesAdapter;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lahz;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 90
    return-void
.end method

.method public a(Landroid/support/v4/view/PagerAdapter;)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lahz;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 78
    return-void
.end method

.method public a(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lahz;->b:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 82
    return-void
.end method

.method public a(Landroid/widget/ListAdapter;)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lahz;->b:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 86
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lahz;->a:Landroid/view/View;

    return-object v0
.end method

.method public b()Lcom/twitter/internal/android/widget/HorizontalListView;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lahz;->b:Lcom/twitter/internal/android/widget/HorizontalListView;

    return-object v0
.end method

.method public c()Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lahz;->c:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method public d()Lcom/twitter/android/composer/ComposerDockLayout;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lahz;->e:Lcom/twitter/android/composer/ComposerDockLayout;

    return-object v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Lahz;->g()Lcom/twitter/app/common/list/TwitterListFragment;

    move-result-object v0

    .line 94
    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {v0}, Lcom/twitter/app/common/list/TwitterListFragment;->aL()V

    .line 97
    :cond_0
    return-void
.end method

.method public f()I
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lahz;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    return v0
.end method
