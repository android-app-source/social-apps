.class public Lbzr;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbzq;


# instance fields
.field a:Z

.field private final b:[J

.field private final c:Lbzs;

.field private d:I

.field private e:I

.field private f:J


# direct methods
.method constructor <init>(Lbzs;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-virtual {p0}, Lbzr;->e()V

    .line 57
    const/16 v0, 0x1e

    new-array v0, v0, [J

    iput-object v0, p0, Lbzr;->b:[J

    .line 58
    iput-object p1, p0, Lbzr;->c:Lbzs;

    .line 59
    return-void
.end method

.method public static a()Lbzr;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lbzr;

    invoke-static {}, Lbzs;->a()Lbzs;

    move-result-object v1

    invoke-direct {v0, v1}, Lbzr;-><init>(Lbzs;)V

    return-object v0
.end method

.method public static f()Z
    .locals 2

    .prologue
    .line 106
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()J
    .locals 7

    .prologue
    const/16 v1, 0x1e

    .line 122
    iget v0, p0, Lbzr;->e:I

    .line 123
    if-ge v0, v1, :cond_0

    .line 124
    const/4 v1, 0x0

    .line 129
    :goto_0
    iget v2, p0, Lbzr;->e:I

    add-int/lit8 v2, v2, -0x1

    rem-int/lit8 v2, v2, 0x1e

    .line 130
    iget-object v3, p0, Lbzr;->b:[J

    aget-wide v2, v3, v2

    iget-object v4, p0, Lbzr;->b:[J

    aget-wide v4, v4, v1

    sub-long/2addr v2, v4

    .line 131
    add-int/lit8 v0, v0, -0x1

    int-to-long v0, v0

    const-wide/32 v4, 0x3b9aca00

    mul-long/2addr v0, v4

    div-long/2addr v0, v2

    return-wide v0

    .line 127
    :cond_0
    iget v0, p0, Lbzr;->e:I

    rem-int/lit8 v0, v0, 0x1e

    move v6, v1

    move v1, v0

    move v0, v6

    goto :goto_0
.end method


# virtual methods
.method public a(J)V
    .locals 5

    .prologue
    .line 93
    iget-boolean v0, p0, Lbzr;->a:Z

    if-eqz v0, :cond_0

    .line 94
    iget v0, p0, Lbzr;->e:I

    rem-int/lit8 v0, v0, 0x1e

    .line 95
    iget-object v1, p0, Lbzr;->b:[J

    aput-wide p1, v1, v0

    .line 96
    iget v0, p0, Lbzr;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbzr;->e:I

    .line 97
    iget v0, p0, Lbzr;->e:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget v0, p0, Lbzr;->e:I

    rem-int/lit8 v0, v0, 0xa

    if-nez v0, :cond_0

    .line 99
    iget v0, p0, Lbzr;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbzr;->d:I

    .line 100
    iget-wide v0, p0, Lbzr;->f:J

    invoke-direct {p0}, Lbzr;->g()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lbzr;->f:J

    .line 103
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lbzr;->e()V

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbzr;->a:Z

    .line 69
    iget-object v0, p0, Lbzr;->c:Lbzs;

    invoke-virtual {v0, p0}, Lbzs;->a(Lbzr;)V

    .line 70
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbzr;->a:Z

    .line 74
    iget-object v0, p0, Lbzr;->c:Lbzs;

    invoke-virtual {v0, p0}, Lbzs;->b(Lbzr;)V

    .line 75
    return-void
.end method

.method public d()J
    .locals 4

    .prologue
    .line 81
    iget v0, p0, Lbzr;->d:I

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lbzr;->f:J

    iget v2, p0, Lbzr;->d:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    goto :goto_0
.end method

.method e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 85
    iput v2, p0, Lbzr;->d:I

    .line 86
    iput v2, p0, Lbzr;->e:I

    .line 87
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbzr;->f:J

    .line 88
    iput-boolean v2, p0, Lbzr;->a:Z

    .line 89
    return-void
.end method
