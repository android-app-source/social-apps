.class public Lbsh;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbsh$b;,
        Lbsh$a;
    }
.end annotation


# direct methods
.method public static a(Landroid/content/Context;)Lbsh$a;
    .locals 1

    .prologue
    .line 211
    new-instance v0, Lbsh$a;

    invoke-direct {v0, p0}, Lbsh$a;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/network/HttpOperation;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest;
    .locals 5

    .prologue
    .line 110
    new-instance v0, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;

    invoke-direct {v0}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;-><init>()V

    .line 111
    sget-object v1, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest;->e:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;

    .line 112
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->h()Lcom/twitter/network/HttpOperation$RequestMethod;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/network/HttpOperation$RequestMethod;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;

    .line 114
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->m()Lcom/twitter/network/l;

    move-result-object v1

    .line 115
    sget-object v2, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest;->k:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;

    iget v3, v1, Lcom/twitter/network/l;->a:I

    .line 116
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;

    move-result-object v2

    sget-object v3, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest;->i:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;

    iget-object v4, v1, Lcom/twitter/network/l;->q:Ljava/lang/String;

    .line 117
    invoke-virtual {v2, v3, v4}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;

    move-result-object v2

    sget-object v3, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest;->l:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;

    iget v4, v1, Lcom/twitter/network/l;->j:I

    .line 118
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;

    move-result-object v2

    sget-object v3, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest;->f:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;

    iget-object v4, v1, Lcom/twitter/network/l;->o:Lcom/twitter/network/HttpOperation$Protocol;

    .line 119
    invoke-virtual {v4}, Lcom/twitter/network/HttpOperation$Protocol;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;

    .line 121
    iget-object v2, v1, Lcom/twitter/network/l;->c:Ljava/lang/Exception;

    if-eqz v2, :cond_0

    .line 122
    sget-object v2, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest;->m:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;

    iget-object v1, v1, Lcom/twitter/network/l;->c:Ljava/lang/Exception;

    .line 123
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;

    .line 126
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->i()Ljava/net/URI;

    move-result-object v1

    .line 127
    sget-object v2, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest;->c:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;

    .line 128
    invoke-virtual {v1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;

    move-result-object v2

    sget-object v3, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest;->d:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;

    .line 129
    invoke-virtual {v1}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;

    move-result-object v2

    sget-object v3, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest;->b:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;

    .line 130
    invoke-virtual {v1}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;

    move-result-object v2

    sget-object v3, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest;->r:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;

    .line 131
    invoke-virtual {v1}, Ljava/net/URI;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;

    .line 133
    invoke-static {p1}, Lbsh;->b(Lcom/twitter/network/HttpOperation;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails;

    move-result-object v1

    .line 134
    sget-object v2, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest;->o:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;

    .line 135
    invoke-virtual {v0, v2, v1}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;

    .line 137
    invoke-static {p1}, Lbsh;->a(Lcom/twitter/network/HttpOperation;)Lcom/twitter/client_network/thriftandroid/ClientNetworkResponseSource;

    move-result-object v1

    .line 138
    sget-object v2, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest;->n:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;

    .line 139
    invoke-virtual {v0, v2, v1}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;

    .line 142
    invoke-static {p0}, Lbsh;->a(Landroid/content/Context;)Lbsh$a;

    move-result-object v1

    invoke-virtual {v1}, Lbsh$a;->c()Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus;

    move-result-object v1

    .line 143
    sget-object v2, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest;->h:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;

    .line 144
    invoke-virtual {v0, v2, v1}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;

    .line 146
    invoke-static {p0}, Lbsh;->b(Landroid/content/Context;)Z

    move-result v1

    .line 147
    sget-object v2, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest;->q:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;

    .line 148
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;

    .line 150
    sget-object v1, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest;->s:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;

    const-string/jumbo v2, "X-B3-TraceId"

    .line 151
    invoke-virtual {p1, v2}, Lcom/twitter/network/HttpOperation;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;

    .line 152
    sget-object v1, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest;->t:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;

    .line 153
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->u()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;

    .line 155
    invoke-virtual {v0}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest$a;->a()Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/twitter/network/HttpOperation;)Lcom/twitter/client_network/thriftandroid/ClientNetworkResponseSource;
    .locals 2

    .prologue
    .line 171
    const-string/jumbo v0, "x-cache"

    invoke-virtual {p0, v0}, Lcom/twitter/network/HttpOperation;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 172
    if-eqz v0, :cond_1

    .line 173
    const-string/jumbo v1, "HIT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    sget-object v0, Lcom/twitter/client_network/thriftandroid/ClientNetworkResponseSource;->b:Lcom/twitter/client_network/thriftandroid/ClientNetworkResponseSource;

    .line 182
    :goto_0
    return-object v0

    .line 176
    :cond_0
    sget-object v0, Lcom/twitter/client_network/thriftandroid/ClientNetworkResponseSource;->c:Lcom/twitter/client_network/thriftandroid/ClientNetworkResponseSource;

    goto :goto_0

    .line 179
    :cond_1
    sget-object v0, Lcom/twitter/client_network/thriftandroid/ClientNetworkResponseSource;->a:Lcom/twitter/client_network/thriftandroid/ClientNetworkResponseSource;

    goto :goto_0
.end method

.method private static b(Lcom/twitter/network/HttpOperation;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails;
    .locals 6

    .prologue
    .line 187
    new-instance v0, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$a;

    invoke-direct {v0}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$a;-><init>()V

    .line 188
    sget-object v1, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails;->i:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$_Fields;

    .line 189
    invoke-virtual {p0}, Lcom/twitter/network/HttpOperation;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$a;

    .line 191
    invoke-virtual {p0}, Lcom/twitter/network/HttpOperation;->m()Lcom/twitter/network/l;

    move-result-object v1

    .line 192
    sget-object v2, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails;->b:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$_Fields;

    iget-wide v4, v1, Lcom/twitter/network/l;->e:J

    .line 193
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$a;

    move-result-object v2

    sget-object v3, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails;->c:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$_Fields;

    iget v1, v1, Lcom/twitter/network/l;->n:I

    .line 194
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$a;

    .line 196
    invoke-static {}, Lcom/twitter/library/network/forecaster/c;->a()Lcom/twitter/library/network/forecaster/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/forecaster/c;->e()Lcom/twitter/util/units/duration/Milliseconds;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/util/units/duration/Milliseconds;->longValue()J

    move-result-wide v2

    .line 197
    sget-object v1, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails;->f:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$_Fields;

    .line 198
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$a;

    .line 200
    const-string/jumbo v1, "X-Response-Time"

    invoke-virtual {p0, v1}, Lcom/twitter/network/HttpOperation;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 201
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 202
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 203
    sget-object v1, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails;->k:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$_Fields;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$a;

    .line 206
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails$a;->a()Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestDetails;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 220
    invoke-static {p0}, Lcom/twitter/library/provider/q;->c(Landroid/content/Context;)Lcom/twitter/network/f;

    move-result-object v0

    .line 221
    invoke-virtual {v0}, Lcom/twitter/network/f;->a()Lcom/twitter/network/k;

    move-result-object v0

    .line 222
    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/twitter/network/k;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
