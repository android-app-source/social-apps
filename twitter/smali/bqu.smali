.class public Lbqu;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbqs;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/gms/maps/MapView;

.field private final c:Landroid/widget/ImageView;

.field private final d:I

.field private final e:I

.field private final f:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private final g:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private h:Lcom/google/android/gms/maps/model/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lbqu;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbqu;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup;Lcom/google/android/gms/maps/MapView;Landroid/widget/ImageView;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Lcom/google/android/gms/maps/MapView;->setVisibility(I)V

    .line 62
    iput-object p2, p0, Lbqu;->b:Lcom/google/android/gms/maps/MapView;

    .line 63
    iput-object p3, p0, Lbqu;->c:Landroid/widget/ImageView;

    .line 64
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 65
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 66
    sget v2, Lazw$e;->map_bounding_box_padding:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, p0, Lbqu;->d:I

    .line 67
    sget v2, Lazw$e;->geo_json_region_stroke_width:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, p0, Lbqu;->e:I

    .line 68
    sget v2, Lazw$d;->geo_json_region_stroke_color:I

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p0, Lbqu;->f:I

    .line 69
    sget v2, Lazw$d;->geo_json_region_fill_color:I

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p0, Lbqu;->g:I

    .line 71
    invoke-virtual {p2}, Lcom/google/android/gms/maps/MapView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_0

    .line 72
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p2, v2}, Lcom/google/android/gms/maps/MapView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 74
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 76
    :cond_0
    invoke-virtual {p3}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_1

    .line 77
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p3, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 79
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 82
    :cond_1
    invoke-static {v1}, Lcom/google/android/gms/maps/d;->a(Landroid/content/Context;)I

    .line 84
    iget-object v1, p0, Lbqu;->b:Lcom/google/android/gms/maps/MapView;

    new-instance v2, Lbqu$1;

    invoke-direct {v2, p0, v0}, Lbqu$1;-><init>(Lbqu;Landroid/content/res/Resources;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/MapView;->a(Lcom/google/android/gms/maps/e;)V

    .line 95
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/maps/MapView;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 98
    new-instance v0, Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;-><init>()V

    .line 99
    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMapOptions;->j(Z)Lcom/google/android/gms/maps/GoogleMapOptions;

    move-result-object v0

    .line 100
    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMapOptions;->c(Z)Lcom/google/android/gms/maps/GoogleMapOptions;

    move-result-object v0

    .line 101
    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMapOptions;->f(Z)Lcom/google/android/gms/maps/GoogleMapOptions;

    move-result-object v0

    .line 102
    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMapOptions;->h(Z)Lcom/google/android/gms/maps/GoogleMapOptions;

    move-result-object v0

    .line 103
    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMapOptions;->e(Z)Lcom/google/android/gms/maps/GoogleMapOptions;

    move-result-object v0

    .line 104
    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMapOptions;->g(Z)Lcom/google/android/gms/maps/GoogleMapOptions;

    move-result-object v0

    .line 105
    new-instance v1, Lcom/google/android/gms/maps/MapView;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/maps/MapView;-><init>(Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V

    return-object v1
.end method

.method static synthetic a(Lbqu;)Lcom/google/android/gms/maps/MapView;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lbqu;->b:Lcom/google/android/gms/maps/MapView;

    return-object v0
.end method

.method private static a(Lcom/twitter/model/geo/b;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 6

    .prologue
    .line 110
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p0}, Lcom/twitter/model/geo/b;->a()D

    move-result-wide v2

    invoke-virtual {p0}, Lcom/twitter/model/geo/b;->b()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    return-object v0
.end method

.method static synthetic a(Lbqu;Lcom/google/android/gms/maps/c;Lbqs$a;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lbqu;->a(Lcom/google/android/gms/maps/c;Lbqs$a;)V

    return-void
.end method

.method static synthetic a(Lbqu;Lcom/google/android/gms/maps/c;Lcom/google/android/gms/maps/a;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lbqu;->a(Lcom/google/android/gms/maps/c;Lcom/google/android/gms/maps/a;)V

    return-void
.end method

.method static synthetic a(Lbqu;Lcom/google/android/gms/maps/c;Lcom/twitter/model/geo/b;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lbqu;->a(Lcom/google/android/gms/maps/c;Lcom/twitter/model/geo/b;)V

    return-void
.end method

.method static synthetic a(Lbqu;Lcom/twitter/model/geo/b;Lcom/twitter/model/geo/b;Lbqs$a;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Lbqu;->b(Lcom/twitter/model/geo/b;Lcom/twitter/model/geo/b;Lbqs$a;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/maps/a;Lcom/twitter/model/geo/b;Lbqs$a;)V
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lbqu;->b:Lcom/google/android/gms/maps/MapView;

    new-instance v1, Lbqu$3;

    invoke-direct {v1, p0, p1, p2, p3}, Lbqu$3;-><init>(Lbqu;Lcom/google/android/gms/maps/a;Lcom/twitter/model/geo/b;Lbqs$a;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/MapView;->a(Lcom/google/android/gms/maps/e;)V

    .line 208
    return-void
.end method

.method private a(Lcom/google/android/gms/maps/c;Lbqs$a;)V
    .locals 1

    .prologue
    .line 284
    new-instance v0, Lbqu$5;

    invoke-direct {v0, p0, p1, p2}, Lbqu$5;-><init>(Lbqu;Lcom/google/android/gms/maps/c;Lbqs$a;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/c$d;)V

    .line 303
    return-void
.end method

.method private a(Lcom/google/android/gms/maps/c;Lcom/google/android/gms/maps/a;)V
    .locals 1

    .prologue
    .line 273
    new-instance v0, Lbqu$4;

    invoke-direct {v0, p0, p1, p2}, Lbqu$4;-><init>(Lbqu;Lcom/google/android/gms/maps/c;Lcom/google/android/gms/maps/a;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/c$b;)V

    .line 280
    invoke-virtual {p1, p2}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/a;)V

    .line 281
    return-void
.end method

.method private a(Lcom/google/android/gms/maps/c;Lcom/twitter/model/geo/b;)V
    .locals 6

    .prologue
    .line 254
    iget-object v0, p0, Lbqu;->h:Lcom/google/android/gms/maps/model/d;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lbqu;->h:Lcom/google/android/gms/maps/model/d;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/d;->a()V

    .line 256
    const/4 v0, 0x0

    iput-object v0, p0, Lbqu;->h:Lcom/google/android/gms/maps/model/d;

    .line 260
    :cond_0
    new-instance v0, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    .line 262
    invoke-virtual {p2}, Lcom/twitter/model/geo/b;->a()D

    move-result-wide v2

    invoke-virtual {p2}, Lcom/twitter/model/geo/b;->b()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v0

    sget v1, Lazw$f;->ic_location_pin:I

    .line 263
    invoke-static {v1}, Lcom/google/android/gms/maps/model/b;->a(I)Lcom/google/android/gms/maps/model/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/a;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v0

    .line 260
    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/d;

    move-result-object v0

    iput-object v0, p0, Lbqu;->h:Lcom/google/android/gms/maps/model/d;

    .line 265
    return-void
.end method

.method static synthetic b(Lbqu;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lbqu;->e()V

    return-void
.end method

.method private b(Lcom/twitter/model/geo/b;Lcom/twitter/model/geo/b;Lbqs$a;)V
    .locals 3

    .prologue
    .line 151
    new-instance v0, Lcom/google/android/gms/maps/model/LatLngBounds;

    .line 153
    invoke-static {p1}, Lbqu;->a(Lcom/twitter/model/geo/b;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-static {p2}, Lbqu;->a(Lcom/twitter/model/geo/b;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/maps/model/LatLngBounds;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;)V

    const/4 v1, 0x0

    .line 152
    invoke-static {v0, v1}, Lcom/google/android/gms/maps/b;->a(Lcom/google/android/gms/maps/model/LatLngBounds;I)Lcom/google/android/gms/maps/a;

    move-result-object v0

    .line 154
    invoke-static {p2, p1}, Lbqf;->a(Lcom/twitter/model/geo/b;Lcom/twitter/model/geo/b;)Lcom/twitter/model/geo/b;

    move-result-object v1

    .line 155
    invoke-direct {p0, v0, v1, p3}, Lbqu;->a(Lcom/google/android/gms/maps/a;Lcom/twitter/model/geo/b;Lbqs$a;)V

    .line 156
    return-void
.end method

.method static synthetic c(Lbqu;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lbqu;->c:Landroid/widget/ImageView;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 249
    iget-object v0, p0, Lbqu;->b:Lcom/google/android/gms/maps/MapView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/MapView;->setVisibility(I)V

    .line 250
    iget-object v0, p0, Lbqu;->c:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 251
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lbqu;->b:Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapView;->a()V

    .line 313
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lbqu;->b:Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/maps/MapView;->a(Landroid/os/Bundle;)V

    .line 308
    return-void
.end method

.method public a(Lcom/twitter/model/geo/b;Lbqs$a;)V
    .locals 6

    .prologue
    .line 161
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    .line 162
    invoke-virtual {p1}, Lcom/twitter/model/geo/b;->a()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/twitter/model/geo/b;->b()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    const/high16 v1, 0x41700000    # 15.0f

    .line 161
    invoke-static {v0, v1}, Lcom/google/android/gms/maps/b;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/a;

    move-result-object v0

    .line 164
    invoke-direct {p0, v0, p1, p2}, Lbqu;->a(Lcom/google/android/gms/maps/a;Lcom/twitter/model/geo/b;Lbqs$a;)V

    .line 165
    return-void
.end method

.method public a(Lcom/twitter/model/geo/b;Lcom/twitter/model/geo/b;Lbqs$a;)V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lbqu;->b:Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapView;->getLeft()I

    move-result v0

    iget-object v1, p0, Lbqu;->b:Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/MapView;->getRight()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 134
    invoke-direct {p0, p1, p2, p3}, Lbqu;->b(Lcom/twitter/model/geo/b;Lcom/twitter/model/geo/b;Lbqs$a;)V

    .line 145
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v0, p0, Lbqu;->b:Lcom/google/android/gms/maps/MapView;

    new-instance v1, Lbqu$2;

    invoke-direct {v1, p0, p1, p2, p3}, Lbqu$2;-><init>(Lbqu;Lcom/twitter/model/geo/b;Lcom/twitter/model/geo/b;Lbqs$a;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/MapView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lbqu;->b:Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapView;->b()V

    .line 318
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lbqu;->b:Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/maps/MapView;->b(Landroid/os/Bundle;)V

    .line 328
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lbqu;->b:Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapView;->c()V

    .line 323
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lbqu;->b:Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapView;->d()V

    .line 333
    return-void
.end method
