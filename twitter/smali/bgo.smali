.class public Lbgo;
.super Lbgv;
.source "Twttr"


# instance fields
.field private final c:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJILbgp;)V
    .locals 11

    .prologue
    .line 23
    new-instance v3, Lcom/twitter/library/service/v;

    invoke-direct {v3, p2}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p3

    move-wide/from16 v6, p5

    move/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lbgo;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;JJILbgp;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;JJILbgp;)V
    .locals 13

    .prologue
    .line 29
    const-class v2, Lbgo;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    const/16 v8, 0xe

    .line 30
    invoke-static/range {p5 .. p6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    sget-object v12, Lbgw;->a:Lbgw;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-wide/from16 v6, p3

    move/from16 v9, p7

    move-object/from16 v10, p8

    .line 29
    invoke-direct/range {v2 .. v12}, Lbgv;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Ljava/lang/String;JIILbgp;Ljava/lang/String;Lbgw;)V

    .line 32
    move-wide/from16 v0, p5

    iput-wide v0, p0, Lbgo;->c:J

    .line 33
    return-void
.end method


# virtual methods
.method public ba_()Z
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return v0
.end method

.method protected e()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 38
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "timeline"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "conversation"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lbgo;->c:J

    .line 39
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 38
    return-object v0
.end method

.method public h()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 49
    invoke-virtual {p0}, Lbgo;->x()I

    move-result v1

    if-eq v1, v0, :cond_0

    invoke-virtual {p0}, Lbgo;->x()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected s()Lchx$b;
    .locals 6

    .prologue
    .line 55
    .line 56
    invoke-super {p0}, Lbgv;->s()Lchx$b;

    move-result-object v0

    invoke-static {v0}, Lchx$b$a;->a(Lchx$b;)Lchx$b$a;

    move-result-object v0

    .line 57
    invoke-virtual {p0}, Lbgo;->t()Lcom/twitter/android/timeline/cf;

    move-result-object v1

    .line 58
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/twitter/android/timeline/cf;->a:Lcom/twitter/model/timeline/ar;

    iget v2, v2, Lcom/twitter/model/timeline/ar;->c:I

    const/4 v3, 0x7

    if-ne v2, v3, :cond_0

    .line 59
    new-instance v2, Lcgu;

    .line 60
    invoke-virtual {v1}, Lcom/twitter/android/timeline/cf;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/twitter/android/timeline/cf;->g()J

    move-result-wide v4

    invoke-direct {v2, v3, v4, v5}, Lcgu;-><init>(Ljava/lang/String;J)V

    .line 59
    invoke-virtual {v0, v2}, Lchx$b$a;->a(Lcgu;)Lchx$b$a;

    .line 62
    :cond_0
    invoke-virtual {v0}, Lchx$b$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchx$b;

    return-object v0
.end method
