.class public Laad;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lzu;


# direct methods
.method constructor <init>(Lzu;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Laad;->a:Lzu;

    .line 29
    return-void
.end method

.method public static a()Laad;
    .locals 2

    .prologue
    .line 24
    new-instance v0, Laad;

    invoke-static {}, Lzu;->a()Lzu;

    move-result-object v1

    invoke-direct {v0, v1}, Laad;-><init>(Lzu;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/Moment;)V
    .locals 4

    .prologue
    .line 36
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    iget-wide v2, p1, Lcom/twitter/model/moments/Moment;->b:J

    .line 37
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/moments/MomentVisibilityMode;->a:Lcom/twitter/model/moments/MomentVisibilityMode;

    .line 38
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(Lcom/twitter/model/moments/MomentVisibilityMode;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    .line 40
    iget-object v1, p0, Laad;->a:Lzu;

    const-string/jumbo v2, "me:user_moments:moment_capsule::click"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 41
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 5

    .prologue
    .line 52
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    .line 53
    invoke-virtual {v0, p3, p4}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    .line 54
    iget-object v1, p0, Laad;->a:Lzu;

    const-string/jumbo v2, "%s:%s:tweet_actions_dropdown:create_moment_button:click"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 55
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/moments/Moment;J)V
    .locals 6

    .prologue
    .line 59
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    iget-wide v2, p3, Lcom/twitter/model/moments/Moment;->b:J

    .line 60
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    .line 61
    invoke-virtual {v0, p4, p5}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    .line 62
    iget-object v1, p3, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    if-eqz v1, :cond_0

    .line 63
    iget-object v1, p3, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    iget-object v1, v1, Lcom/twitter/model/moments/f;->c:Lcom/twitter/model/moments/MomentVisibilityMode;

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(Lcom/twitter/model/moments/MomentVisibilityMode;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    .line 65
    :cond_0
    iget-object v1, p0, Laad;->a:Lzu;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "%s:%s:tweet_actions_dropdown:moment:add"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 66
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 32
    iget-object v0, p0, Laad;->a:Lzu;

    const-string/jumbo v1, "moments:guide::create_moment_button:click"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lzu;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method public b(Lcom/twitter/model/moments/Moment;)V
    .locals 4

    .prologue
    .line 44
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    iget-wide v2, p1, Lcom/twitter/model/moments/Moment;->b:J

    .line 45
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/moments/MomentVisibilityMode;->b:Lcom/twitter/model/moments/MomentVisibilityMode;

    .line 46
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(Lcom/twitter/model/moments/MomentVisibilityMode;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    .line 48
    iget-object v1, p0, Laad;->a:Lzu;

    const-string/jumbo v2, "me:user_moments:moment_capsule::click"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method public c(Lcom/twitter/model/moments/Moment;)V
    .locals 4

    .prologue
    .line 73
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    iget-wide v2, p1, Lcom/twitter/model/moments/Moment;->b:J

    .line 74
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    .line 76
    iget-object v1, p0, Laad;->a:Lzu;

    const-string/jumbo v2, "profile:profile:dashboard_nav:user_moments:navigate"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 77
    return-void
.end method
