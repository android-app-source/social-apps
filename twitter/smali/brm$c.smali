.class Lbrm$c;
.super Lcom/twitter/async/service/AsyncOperation;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbrm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/async/service/AsyncOperation",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/provider/t;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcdu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/twitter/library/provider/t;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/provider/t;",
            "Ljava/util/List",
            "<",
            "Lcdu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 238
    const-string/jumbo v0, "write_stickers_to_db"

    invoke-direct {p0, v0}, Lcom/twitter/async/service/AsyncOperation;-><init>(Ljava/lang/String;)V

    .line 239
    iput-object p1, p0, Lbrm$c;->a:Lcom/twitter/library/provider/t;

    .line 240
    iput-object p2, p0, Lbrm$c;->b:Ljava/util/List;

    .line 241
    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/Boolean;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 245
    iget-object v0, p0, Lbrm$c;->a:Lcom/twitter/library/provider/t;

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/database/hydrator/d;->a(Lcom/twitter/database/model/i;)Lcom/twitter/database/hydrator/d;

    move-result-object v0

    .line 246
    iget-object v1, p0, Lbrm$c;->b:Ljava/util/List;

    const-class v2, Lcdu;

    new-instance v3, Lbrm$c$1;

    invoke-direct {v3, p0}, Lbrm$c$1;-><init>(Lbrm$c;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/database/hydrator/d;->a(Ljava/util/Collection;Ljava/lang/Class;Lcom/twitter/database/hydrator/d$a;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0}, Lbrm$c;->b()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 229
    invoke-virtual {p0}, Lbrm$c;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
