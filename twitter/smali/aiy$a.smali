.class Laiy$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Laiy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/widget/Switch;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Landroid/widget/Switch;Landroid/content/Context;Landroid/widget/ListView;)V
    .locals 0

    .prologue
    .line 364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 365
    iput-object p1, p0, Laiy$a;->a:Landroid/widget/Switch;

    .line 366
    iput-object p2, p0, Laiy$a;->b:Landroid/content/Context;

    .line 367
    iput-object p3, p0, Laiy$a;->c:Landroid/widget/ListView;

    .line 368
    return-void
.end method

.method static synthetic a(Laiy$a;)Landroid/widget/Switch;
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Laiy$a;->a:Landroid/widget/Switch;

    return-object v0
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 375
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 376
    iget-object v0, p0, Laiy$a;->a:Landroid/widget/Switch;

    invoke-virtual {v0, p2}, Landroid/widget/Switch;->setChecked(Z)V

    .line 387
    :goto_0
    if-nez p2, :cond_1

    .line 388
    iget-object v0, p0, Laiy$a;->c:Landroid/widget/ListView;

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAlpha(F)V

    .line 389
    iget-object v0, p0, Laiy$a;->c:Landroid/widget/ListView;

    const v1, -0x9090a

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setBackgroundColor(I)V

    .line 395
    :goto_1
    iget-object v0, p0, Laiy$a;->c:Landroid/widget/ListView;

    invoke-virtual {v0, p2}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 396
    iget-object v0, p0, Laiy$a;->c:Landroid/widget/ListView;

    invoke-virtual {v0, p2}, Landroid/widget/ListView;->setClickable(Z)V

    .line 397
    return-void

    .line 378
    :cond_0
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Laiy$a;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 379
    new-instance v1, Laiy$a$1;

    invoke-direct {v1, p0, p2}, Laiy$a$1;-><init>(Laiy$a;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 391
    :cond_1
    iget-object v0, p0, Laiy$a;->c:Landroid/widget/ListView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAlpha(F)V

    .line 392
    iget-object v0, p0, Laiy$a;->c:Landroid/widget/ListView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setBackgroundColor(I)V

    goto :goto_1
.end method
