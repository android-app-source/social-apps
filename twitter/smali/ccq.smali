.class public final Lccq;
.super Lccl;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lccq$b;,
        Lccq$a;
    }
.end annotation


# static fields
.field public static final c:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lccq;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Lccp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lccq$b;

    invoke-direct {v0}, Lccq$b;-><init>()V

    sput-object v0, Lccq;->c:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lccq$a;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lccl;-><init>(Lccl$a;)V

    .line 23
    invoke-static {p1}, Lccq$a;->a(Lccq$a;)Lccp;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccp;

    iput-object v0, p0, Lccq;->d:Lccp;

    .line 24
    return-void
.end method

.method synthetic constructor <init>(Lccq$a;Lccq$1;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lccq;-><init>(Lccq$a;)V

    return-void
.end method

.method static synthetic a(Lccq;)Lccp;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lccq;->d:Lccp;

    return-object v0
.end method

.method private b(Lccq;)Z
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lccq;->d:Lccp;

    iget-object v1, p1, Lccq;->d:Lccp;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    const-string/jumbo v0, "options"

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lccq;->d:Lccp;

    iget-object v0, v0, Lccp;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lccq;->d:Lccp;

    iget-object v0, v0, Lccp;->b:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 46
    invoke-super {p0, p1}, Lccl;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lccq;

    if-eqz v0, :cond_1

    check-cast p1, Lccq;

    .line 47
    invoke-direct {p0, p1}, Lccq;->b(Lccq;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 46
    :goto_0
    return v0

    .line 47
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 52
    invoke-super {p0}, Lccl;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lccq;->d:Lccp;

    invoke-virtual {v1}, Lccp;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
