.class Lais$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lais;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/c$a",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/provider/t;

.field private final b:Lcom/twitter/library/client/p;

.field private final c:Lbhy;

.field private final d:Lcom/twitter/model/core/TwitterUser;


# direct methods
.method constructor <init>(Lcom/twitter/library/provider/t;Lcom/twitter/library/client/p;Lbhy;Lcom/twitter/model/core/TwitterUser;)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lais$a;->a:Lcom/twitter/library/provider/t;

    .line 77
    iput-object p2, p0, Lais$a;->b:Lcom/twitter/library/client/p;

    .line 78
    iput-object p3, p0, Lais$a;->c:Lbhy;

    .line 79
    iput-object p4, p0, Lais$a;->d:Lcom/twitter/model/core/TwitterUser;

    .line 80
    return-void
.end method


# virtual methods
.method public a(Lrx/i;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lais$a;->a:Lcom/twitter/library/provider/t;

    iget-object v1, p0, Lais$a;->d:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/provider/t;->h(J)I

    move-result v0

    .line 85
    invoke-virtual {p1}, Lrx/i;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 86
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/i;->a(Ljava/lang/Object;)V

    .line 89
    :cond_0
    iget-object v0, p0, Lais$a;->b:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lais$a;->c:Lbhy;

    new-instance v2, Lais$a$1;

    invoke-direct {v2, p0, p1}, Lais$a$1;-><init>(Lais$a;Lrx/i;)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 98
    return-void
.end method

.method public synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 67
    check-cast p1, Lrx/i;

    invoke-virtual {p0, p1}, Lais$a;->a(Lrx/i;)V

    return-void
.end method
