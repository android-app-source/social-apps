.class public abstract Lcjr;
.super Landroid/widget/BaseAdapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/widget/BaseAdapter;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcjt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcjt",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 19
    iput-object p1, p0, Lcjr;->a:Landroid/content/Context;

    .line 20
    invoke-virtual {p0}, Lcjr;->ac_()Lcjt;

    move-result-object v0

    iput-object v0, p0, Lcjr;->b:Lcjt;

    .line 21
    iget-object v0, p0, Lcjr;->b:Lcjt;

    new-instance v1, Lcjv;

    invoke-direct {v1, p0}, Lcjv;-><init>(Landroid/widget/BaseAdapter;)V

    invoke-interface {v0, v1}, Lcjt;->a(Lcjs;)V

    .line 22
    return-void
.end method

.method public static b(Landroid/content/Context;)Lcjr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/content/Context;",
            ")",
            "Lcjr",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 41
    new-instance v0, Lcjr$1;

    invoke-direct {v0, p0}, Lcjr$1;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method protected a(Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    .prologue
    .line 99
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TT;",
            "Landroid/view/ViewGroup;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TT;",
            "Landroid/view/ViewGroup;",
            "I)",
            "Landroid/view/View;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 137
    invoke-virtual {p0, p1, p2, p3}, Lcjr;->a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/content/Context;",
            "TT;)V"
        }
    .end annotation
.end method

.method protected a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/content/Context;",
            "TT;I)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 146
    invoke-virtual {p0, p1, p2, p3}, Lcjr;->a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V

    .line 147
    return-void
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TT;)Z"
        }
    .end annotation

    .prologue
    .line 155
    const/4 v0, 0x1

    return v0
.end method

.method protected ac_()Lcjt;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcjt",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 26
    new-instance v0, Lcjp;

    invoke-direct {v0}, Lcjp;-><init>()V

    return-object v0
.end method

.method public g()Lcbi;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcbi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcjr;->b:Lcjt;

    invoke-interface {v0}, Lcjt;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    const/4 v0, 0x0

    .line 67
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcjr;->b:Lcjt;

    invoke-interface {v0}, Lcjt;->c()Lcbi;

    move-result-object v0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcjr;->b:Lcjt;

    invoke-interface {v0}, Lcjt;->a()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 79
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcjr;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 80
    :cond_0
    const/4 v0, 0x0

    .line 82
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcjr;->b:Lcjt;

    invoke-interface {v0, p1}, Lcjt;->a(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcjr;->b:Lcjt;

    invoke-interface {v0, p1}, Lcjt;->b(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0, p1}, Lcjr;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 91
    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {p0, v0}, Lcjr;->a(Ljava/lang/Object;)I

    move-result v0

    .line 94
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Lcjr;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 106
    if-nez v1, :cond_1

    .line 107
    const/4 v0, 0x0

    .line 115
    :cond_0
    :goto_0
    return-object v0

    .line 110
    :cond_1
    if-eqz p2, :cond_2

    move-object v0, p2

    .line 111
    :goto_1
    if-eqz v0, :cond_0

    .line 112
    iget-object v2, p0, Lcjr;->a:Landroid/content/Context;

    invoke-virtual {p0, v0, v2, v1, p1}, Lcjr;->a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;I)V

    goto :goto_0

    .line 110
    :cond_2
    iget-object v0, p0, Lcjr;->a:Landroid/content/Context;

    invoke-virtual {p0, v0, v1, p3, p1}, Lcjr;->a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    goto :goto_1
.end method

.method public isEnabled(I)Z
    .locals 2

    .prologue
    .line 125
    invoke-virtual {p0, p1}, Lcjr;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 126
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcjr;->a:Landroid/content/Context;

    invoke-virtual {p0, v1, v0}, Lcjr;->a(Landroid/content/Context;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()Landroid/content/Context;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcjr;->a:Landroid/content/Context;

    return-object v0
.end method

.method public k()Lcjt;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcjt",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcjr;->b:Lcjt;

    return-object v0
.end method
