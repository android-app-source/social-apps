.class public Laax;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laas;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laax$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Laas",
        "<",
        "Laaw;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Laat;

.field private final b:Laau;

.field private c:Laax$a;


# direct methods
.method public constructor <init>(Laat;Laau;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Laax;->a:Laat;

    .line 36
    iput-object p2, p0, Laax;->b:Laau;

    .line 37
    return-void
.end method

.method static synthetic a(Laax;)Laat;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Laax;->a:Laat;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Laat;)Laax;
    .locals 3

    .prologue
    .line 26
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 27
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    .line 28
    new-instance v2, Laav;

    invoke-direct {v2, p0, v0, v1}, Laav;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;)V

    .line 30
    new-instance v0, Laax;

    invoke-direct {v0, p1, v2}, Laax;-><init>(Laat;Laau;)V

    return-object v0
.end method

.method static synthetic a(Laax;ZZ)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Laax;->a(ZZ)V

    return-void
.end method

.method private a(ZZ)V
    .locals 2

    .prologue
    .line 91
    if-eqz p1, :cond_0

    .line 92
    iget-object v0, p0, Laax;->a:Laat;

    invoke-virtual {v0}, Laat;->c()V

    .line 102
    :goto_0
    return-void

    .line 94
    :cond_0
    iget-object v0, p0, Laax;->a:Laat;

    invoke-virtual {v0}, Laat;->b()V

    .line 95
    iget-object v0, p0, Laax;->a:Laat;

    invoke-virtual {v0, p2}, Laat;->a(Z)V

    .line 96
    if-eqz p2, :cond_1

    .line 97
    iget-object v0, p0, Laax;->a:Laat;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Laat;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 99
    :cond_1
    iget-object v0, p0, Laax;->a:Laat;

    const v1, 0x7f0a03a5

    invoke-virtual {v0, v1}, Laat;->a(I)V

    goto :goto_0
.end method

.method static synthetic b(Laax;)Laau;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Laax;->b:Laau;

    return-object v0
.end method

.method static synthetic c(Laax;)Laax$a;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Laax;->c:Laax$a;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic a(Laar;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Laaw;

    invoke-virtual {p0, p1}, Laax;->a(Laaw;)V

    return-void
.end method

.method public a(Laaw;)V
    .locals 3

    .prologue
    .line 67
    invoke-virtual {p1}, Laaw;->c()Z

    move-result v0

    .line 68
    invoke-virtual {p1}, Laaw;->b()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Laax;->a(ZZ)V

    .line 70
    iget-object v1, p0, Laax;->a:Laat;

    new-instance v2, Laax$1;

    invoke-direct {v2, p0, p1, v0}, Laax$1;-><init>(Laax;Laaw;Z)V

    invoke-virtual {v1, v2}, Laat;->a(Landroid/view/View$OnClickListener;)V

    .line 88
    return-void
.end method

.method public a(Laax$a;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Laax;->c:Laax$a;

    .line 41
    return-void
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Laax;->a:Laat;

    invoke-virtual {v0}, Laat;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
