.class Lcit$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcit;->a(Lcom/twitter/util/math/c;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/util/math/c;

.field final synthetic b:Z

.field final synthetic c:Z

.field final synthetic d:Lcit;


# direct methods
.method constructor <init>(Lcit;Lcom/twitter/util/math/c;ZZ)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcit$1;->d:Lcit;

    iput-object p2, p0, Lcit$1;->a:Lcom/twitter/util/math/c;

    iput-boolean p3, p0, Lcit$1;->b:Z

    iput-boolean p4, p0, Lcit$1;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 6

    .prologue
    .line 67
    sub-int v1, p4, p2

    .line 68
    sub-int v2, p5, p3

    .line 69
    if-lez v1, :cond_0

    if-lez v2, :cond_0

    .line 70
    iget-object v0, p0, Lcit$1;->d:Lcit;

    iget-object v3, p0, Lcit$1;->a:Lcom/twitter/util/math/c;

    iget-boolean v4, p0, Lcit$1;->b:Z

    iget-boolean v5, p0, Lcit$1;->c:Z

    invoke-virtual/range {v0 .. v5}, Lcit;->a(IILcom/twitter/util/math/c;ZZ)V

    .line 71
    iget-object v0, p0, Lcit$1;->d:Lcit;

    invoke-static {v0}, Lcit;->a(Lcit;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 73
    :cond_0
    return-void
.end method
