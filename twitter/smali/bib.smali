.class public Lbib;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/y;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:[J

.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lbib;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbib;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lbib;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 41
    iput p3, p0, Lbib;->c:I

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;I)V
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lbib;->a:Ljava/lang/String;

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 46
    iput p3, p0, Lbib;->c:I

    .line 47
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 52
    invoke-virtual {p0}, Lbib;->R()Lcom/twitter/library/provider/t;

    move-result-object v1

    .line 53
    const/16 v3, 0x12

    const-wide/16 v4, 0x0

    iget v6, p0, Lbib;->c:I

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/t;->a(IIJI)Ljava/lang/String;

    move-result-object v0

    .line 54
    invoke-virtual {p0}, Lbib;->J()Lcom/twitter/library/service/d$a;

    move-result-object v1

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, "friendships"

    aput-object v5, v3, v4

    const-string/jumbo v4, "incoming"

    aput-object v4, v3, v2

    .line 55
    invoke-virtual {v1, v3}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 56
    if-eqz v0, :cond_0

    .line 57
    const-string/jumbo v2, "cursor"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 60
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V
    .locals 18

    .prologue
    .line 73
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v2

    if-nez v2, :cond_0

    .line 122
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lbib;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-wide v4, v2, Lcom/twitter/library/service/v;->c:J

    .line 78
    invoke-virtual/range {p0 .. p0}, Lbib;->R()Lcom/twitter/library/provider/t;

    move-result-object v2

    .line 79
    invoke-virtual/range {p0 .. p0}, Lbib;->S()Laut;

    move-result-object v13

    .line 80
    const/16 v3, 0x12

    invoke-virtual {v2, v3, v4, v5, v13}, Lcom/twitter/library/provider/t;->a(IJLaut;)V

    .line 82
    invoke-virtual/range {p3 .. p3}, Lcom/twitter/library/api/y;->b()Ljava/lang/Object;

    move-result-object v3

    move-object v10, v3

    check-cast v10, Lcom/twitter/library/api/x;

    .line 83
    invoke-virtual {v10}, Lcom/twitter/library/api/x;->b()Ljava/util/List;

    move-result-object v14

    .line 85
    invoke-static {v14}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 86
    const/4 v3, 0x1

    new-array v3, v3, [Landroid/net/Uri;

    const/4 v6, 0x0

    sget-object v7, Lcom/twitter/database/schema/a$y;->u:Landroid/net/Uri;

    invoke-static {v7, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    invoke-static {v7, v4, v5}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    aput-object v7, v3, v6

    invoke-virtual {v13, v3}, Laut;->a([Landroid/net/Uri;)V

    .line 91
    invoke-static {v14}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v15

    .line 92
    invoke-virtual {v2, v15}, Lcom/twitter/library/provider/t;->a([J)Ljava/util/List;

    move-result-object v3

    .line 93
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v7

    if-ge v6, v7, :cond_2

    .line 94
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7, v14}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 95
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/model/core/TwitterUser;

    .line 96
    iget-wide v0, v6, Lcom/twitter/model/core/TwitterUser;->b:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v7, v6}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 99
    :cond_1
    new-instance v6, Lbho;

    move-object/from16 v0, p0

    iget-object v8, v0, Lbib;->p:Landroid/content/Context;

    invoke-virtual/range {p0 .. p0}, Lbib;->M()Lcom/twitter/library/service/v;

    move-result-object v9

    invoke-direct {v6, v8, v9}, Lbho;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V

    .line 100
    move-object/from16 v0, p0

    invoke-virtual {v6, v0}, Lbho;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/service/s;

    .line 101
    iget-object v8, v6, Lbho;->a:Lbic$a;

    invoke-static {v7}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v7

    invoke-virtual {v8, v7}, Lbic$a;->a([J)Lbic$a;

    move-result-object v7

    const/4 v8, 0x1

    .line 102
    invoke-virtual {v7, v8}, Lbic$a;->a(Z)Lbic$a;

    move-result-object v7

    const/16 v8, 0x12

    .line 103
    invoke-virtual {v7, v8}, Lbic$a;->a(I)Lbic$a;

    move-result-object v7

    const-wide/16 v8, -0x1

    .line 104
    invoke-virtual {v7, v8, v9}, Lbic$a;->a(J)Lbic$a;

    move-result-object v7

    .line 105
    invoke-virtual {v10}, Lcom/twitter/library/api/x;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lbic$a;->a(Ljava/lang/String;)Lbic$a;

    .line 107
    invoke-virtual {v6}, Lbho;->O()Lcom/twitter/library/service/u;

    .line 111
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    .line 112
    const/16 v6, 0x12

    const-wide/16 v7, -0x1

    const/4 v9, 0x0

    .line 113
    invoke-virtual {v10}, Lcom/twitter/library/api/x;->a()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    const/4 v12, 0x0

    .line 112
    invoke-virtual/range {v2 .. v12}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;ZLaut;)I

    .line 117
    :cond_3
    const/16 v3, 0x20

    const/4 v4, 0x0

    invoke-virtual {v2, v14, v3, v4}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;ILaut;)V

    .line 118
    move-object/from16 v0, p0

    iput-object v15, v0, Lbib;->b:[J

    .line 121
    :cond_4
    invoke-virtual {v13}, Laut;->a()V

    goto/16 :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 33
    check-cast p3, Lcom/twitter/library/api/y;

    invoke-virtual {p0, p1, p2, p3}, Lbib;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/api/y;
    .locals 1

    .prologue
    .line 65
    const/16 v0, 0xf

    .line 66
    invoke-static {v0}, Lcom/twitter/library/api/y;->a(I)Lcom/twitter/library/api/y;

    move-result-object v0

    .line 65
    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lbib;->b()Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method
