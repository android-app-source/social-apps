.class public Lapi;
.super Lcom/twitter/util/android/d;
.source "Twttr"


# instance fields
.field private final a:J


# direct methods
.method public constructor <init>(Landroid/content/Context;J)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 23
    sget-object v0, Lcom/twitter/database/schema/a$k;->a:Landroid/net/Uri;

    .line 25
    invoke-static {v0, p2, p3}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lawz;->a:[Ljava/lang/String;

    const-string/jumbo v4, "conversations_sort_event_id!=0"

    move-object v0, p0

    move-object v1, p1

    move-object v6, v5

    .line 23
    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    iput-wide p2, p0, Lapi;->a:J

    .line 31
    return-void
.end method


# virtual methods
.method public a()Lbsy;
    .locals 6

    .prologue
    .line 36
    new-instance v1, Lbta;

    iget-wide v2, p0, Lapi;->a:J

    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v0

    invoke-direct {v1, v0}, Lbta;-><init>(Lcom/twitter/database/model/i;)V

    .line 37
    new-instance v2, Lbsy;

    .line 38
    invoke-super {p0}, Lcom/twitter/util/android/d;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const/4 v3, 0x0

    iget-wide v4, p0, Lapi;->a:J

    .line 39
    invoke-virtual {v1, v3, v4, v5}, Lbta;->a(Ljava/lang/String;J)Ljava/util/Map;

    move-result-object v3

    .line 40
    invoke-virtual {v1}, Lbta;->a()Ljava/util/Map;

    move-result-object v1

    invoke-direct {v2, v0, v3, v1}, Lbsy;-><init>(Landroid/database/Cursor;Ljava/util/Map;Ljava/util/Map;)V

    .line 37
    return-object v2
.end method

.method public synthetic loadInBackground()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lapi;->a()Lbsy;

    move-result-object v0

    return-object v0
.end method

.method public synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lapi;->a()Lbsy;

    move-result-object v0

    return-object v0
.end method
