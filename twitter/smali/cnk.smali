.class public Lcnk;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcnk$a;
    }
.end annotation


# static fields
.field public static final a:Lcnk;

.field public static final b:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcnk;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:J

.field public final d:I

.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 21
    new-instance v0, Lcnk;

    const-wide/high16 v2, -0x8000000000000000L

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcnk;-><init>(JI)V

    sput-object v0, Lcnk;->a:Lcnk;

    .line 22
    new-instance v0, Lcnk$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcnk$a;-><init>(Lcnk$1;)V

    sput-object v0, Lcnk;->b:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(JI)V
    .locals 1

    .prologue
    .line 40
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcnk;-><init>(JII)V

    .line 41
    return-void
.end method

.method public constructor <init>(JII)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-wide p1, p0, Lcnk;->c:J

    .line 45
    iput p3, p0, Lcnk;->d:I

    .line 46
    iput p4, p0, Lcnk;->e:I

    .line 47
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 4

    .prologue
    .line 53
    iget-wide v0, p0, Lcnk;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcnk;)Z
    .locals 4

    .prologue
    .line 62
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    iget-wide v0, p0, Lcnk;->c:J

    iget-wide v2, p1, Lcnk;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget v0, p0, Lcnk;->d:I

    iget v1, p1, Lcnk;->d:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcnk;->e:I

    iget v1, p1, Lcnk;->e:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 58
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcnk;

    if-eqz v0, :cond_1

    check-cast p1, Lcnk;

    invoke-virtual {p0, p1}, Lcnk;->a(Lcnk;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 68
    iget-wide v0, p0, Lcnk;->c:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget v1, p0, Lcnk;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p0, Lcnk;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
