.class public Laik;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Lail;

.field public final d:Z

.field public final e:Z

.field public final f:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lail;ZZZ)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Laik;->a:Ljava/lang/String;

    .line 18
    iput-object p2, p0, Laik;->b:Ljava/lang/String;

    .line 19
    iput-object p3, p0, Laik;->c:Lail;

    .line 20
    iput-boolean p4, p0, Laik;->d:Z

    .line 21
    iput-boolean p5, p0, Laik;->e:Z

    .line 22
    iput-boolean p6, p0, Laik;->f:Z

    .line 23
    return-void
.end method


# virtual methods
.method public a(Laik;)Z
    .locals 2

    .prologue
    .line 31
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Laik;->a:Ljava/lang/String;

    iget-object v1, p1, Laik;->a:Ljava/lang/String;

    .line 32
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Laik;->b:Ljava/lang/String;

    iget-object v1, p1, Laik;->b:Ljava/lang/String;

    .line 33
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Laik;->c:Lail;

    iget-object v1, p1, Laik;->c:Lail;

    .line 34
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Laik;->d:Z

    iget-boolean v1, p1, Laik;->d:Z

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Laik;->e:Z

    iget-boolean v1, p1, Laik;->e:Z

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Laik;->f:Z

    iget-boolean v1, p1, Laik;->f:Z

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 31
    :goto_0
    return v0

    .line 34
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 27
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Laik;

    if-eqz v0, :cond_1

    check-cast p1, Laik;

    invoke-virtual {p0, p1}, Laik;->a(Laik;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 42
    iget-object v0, p0, Laik;->a:Ljava/lang/String;

    iget-object v1, p0, Laik;->b:Ljava/lang/String;

    iget-object v2, p0, Laik;->c:Lail;

    iget-boolean v3, p0, Laik;->d:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iget-boolean v4, p0, Laik;->e:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iget-boolean v5, p0, Laik;->f:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
