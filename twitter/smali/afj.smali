.class public Lafj;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator$a;
.implements Ldbr;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/model/util/FriendshipCache;

.field private final c:Lcom/twitter/library/client/p;

.field private final d:Lcom/twitter/android/periscope/q;

.field private e:Ljava/lang/String;

.field private f:Lcom/twitter/library/client/Session;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/model/util/FriendshipCache;-><init>()V

    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lafj;-><init>(Landroid/content/Context;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/library/client/p;)V

    .line 41
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/library/client/p;)V
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lafj;->a:Landroid/content/Context;

    .line 47
    iput-object p2, p0, Lafj;->b:Lcom/twitter/model/util/FriendshipCache;

    .line 48
    iput-object p3, p0, Lafj;->c:Lcom/twitter/library/client/p;

    .line 49
    new-instance v0, Lcom/twitter/android/periscope/q;

    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/twitter/android/periscope/q;-><init>(JLjava/lang/String;)V

    iput-object v0, p0, Lafj;->d:Lcom/twitter/android/periscope/q;

    .line 50
    return-void
.end method

.method static synthetic a(Lafj;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lafj;->f:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method static synthetic a(Lafj;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lafj;->e:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lafj;)Lcom/twitter/model/util/FriendshipCache;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lafj;->b:Lcom/twitter/model/util/FriendshipCache;

    return-object v0
.end method

.method static synthetic c(Lafj;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lafj;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lafj;)Lcom/twitter/library/client/p;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lafj;->c:Lcom/twitter/library/client/p;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lafj;->b(Lcom/twitter/library/client/Session;)V

    .line 55
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 132
    const-wide/16 v0, -0x1

    invoke-static {p1, v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v4

    .line 133
    iget-object v0, p0, Lafj;->b:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/util/FriendshipCache;->h(J)V

    .line 134
    iget-object v0, p0, Lafj;->c:Lcom/twitter/library/client/p;

    new-instance v1, Lbes;

    iget-object v2, p0, Lafj;->a:Landroid/content/Context;

    iget-object v3, p0, Lafj;->f:Lcom/twitter/library/client/Session;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct/range {v1 .. v7}, Lbes;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;I)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 136
    iget-object v0, p0, Lafj;->d:Lcom/twitter/android/periscope/q;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/q;->l()V

    .line 137
    return-void
.end method

.method b(Lcom/twitter/library/client/Session;)V
    .locals 6
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 62
    iput-object p1, p0, Lafj;->f:Lcom/twitter/library/client/Session;

    .line 63
    iget-object v0, p0, Lafj;->d:Lcom/twitter/android/periscope/q;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/periscope/q;->a(J)V

    .line 65
    iget-object v0, p0, Lafj;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lafj;->c:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lafj;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->b(Ljava/lang/String;)V

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lafj;->e:Ljava/lang/String;

    .line 70
    :cond_0
    iget-object v0, p0, Lafj;->b:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/model/util/FriendshipCache;->b()V

    .line 72
    iget-object v0, p0, Lafj;->c:Lcom/twitter/library/client/p;

    new-instance v1, Lbhn;

    iget-object v2, p0, Lafj;->a:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {v1, v2, p1, v3}, Lbhn;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    new-instance v2, Lafj$1;

    invoke-direct {v2, p0}, Lafj$1;-><init>(Lafj;)V

    .line 73
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 86
    iget-object v0, p0, Lafj;->c:Lcom/twitter/library/client/p;

    new-instance v1, Lbhn;

    iget-object v2, p0, Lafj;->a:Landroid/content/Context;

    const/4 v3, 0x2

    invoke-direct {v1, v2, p1, v3}, Lbhn;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    new-instance v2, Lafj$2;

    invoke-direct {v2, p0}, Lafj$2;-><init>(Lafj;)V

    .line 87
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 100
    const-string/jumbo v0, "android_periscope_sync_blocked_ids_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lafj;->c:Lcom/twitter/library/client/p;

    new-instance v1, Lbeu;

    iget-object v2, p0, Lafj;->a:Landroid/content/Context;

    .line 102
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    .line 103
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v4

    invoke-direct {v1, v2, v3, p1, v4}, Lbeu;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;Lcom/twitter/library/provider/t;)V

    new-instance v2, Lafj$3;

    invoke-direct {v2, p0}, Lafj$3;-><init>(Lafj;)V

    .line 102
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lafj;->e:Ljava/lang/String;

    .line 123
    :cond_1
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 146
    const-wide/16 v0, -0x1

    invoke-static {p1, v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v4

    .line 147
    iget-object v0, p0, Lafj;->b:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/util/FriendshipCache;->i(J)V

    .line 148
    iget-object v0, p0, Lafj;->c:Lcom/twitter/library/client/p;

    new-instance v1, Lbes;

    iget-object v2, p0, Lafj;->a:Landroid/content/Context;

    iget-object v3, p0, Lafj;->f:Lcom/twitter/library/client/Session;

    const/4 v6, 0x0

    const/4 v7, 0x3

    invoke-direct/range {v1 .. v7}, Lbes;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;I)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 150
    iget-object v0, p0, Lafj;->d:Lcom/twitter/android/periscope/q;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/q;->m()V

    .line 151
    return-void
.end method

.method public c(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 161
    const-wide/16 v0, -0x1

    invoke-static {p1, v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 162
    iget-object v2, p0, Lafj;->b:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/model/util/FriendshipCache;->l(J)Z

    move-result v0

    return v0
.end method

.method public d(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 173
    const-wide/16 v0, -0x1

    invoke-static {p1, v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 174
    iget-object v2, p0, Lafj;->b:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/model/util/FriendshipCache;->k(J)Z

    move-result v0

    return v0
.end method
