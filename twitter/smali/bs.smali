.class public Lbs;
.super Lbw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbw",
        "<",
        "Lbs;",
        "Lcom/facebook/imagepipeline/request/ImageRequest;",
        "Lcom/facebook/common/references/a",
        "<",
        "Ldq;",
        ">;",
        "Ldt;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ldg;

.field private final b:Lbu;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbu;Ldg;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lbu;",
            "Ldg;",
            "Ljava/util/Set",
            "<",
            "Lby;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p4}, Lbw;-><init>(Landroid/content/Context;Ljava/util/Set;)V

    .line 46
    iput-object p3, p0, Lbs;->a:Ldg;

    .line 47
    iput-object p2, p0, Lbs;->b:Lbu;

    .line 48
    return-void
.end method


# virtual methods
.method protected a()Lbr;
    .locals 4

    .prologue
    .line 57
    invoke-virtual {p0}, Lbs;->g()Lcb;

    move-result-object v0

    .line 59
    instance-of v1, v0, Lbr;

    if-eqz v1, :cond_0

    .line 60
    check-cast v0, Lbr;

    .line 61
    invoke-virtual {p0}, Lbs;->l()Laz;

    move-result-object v1

    invoke-static {}, Lbs;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lbs;->e()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lbr;->a(Laz;Ljava/lang/String;Ljava/lang/Object;)V

    .line 71
    :goto_0
    return-object v0

    .line 66
    :cond_0
    iget-object v0, p0, Lbs;->b:Lbu;

    invoke-virtual {p0}, Lbs;->l()Laz;

    move-result-object v1

    invoke-static {}, Lbs;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lbs;->e()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lbu;->a(Laz;Ljava/lang/String;Ljava/lang/Object;)Lbr;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;)Lbs;
    .locals 1

    .prologue
    .line 52
    invoke-static {p1}, Lcom/facebook/imagepipeline/request/ImageRequest;->a(Landroid/net/Uri;)Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v0

    invoke-super {p0, v0}, Lbw;->b(Ljava/lang/Object;)Lbw;

    move-result-object v0

    check-cast v0, Lbs;

    return-object v0
.end method

.method protected a(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;Z)Lcom/facebook/datasource/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/request/ImageRequest;",
            "Ljava/lang/Object;",
            "Z)",
            "Lcom/facebook/datasource/b",
            "<",
            "Lcom/facebook/common/references/a",
            "<",
            "Ldq;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 79
    if-eqz p3, :cond_0

    .line 80
    iget-object v0, p0, Lbs;->a:Ldg;

    invoke-virtual {v0, p1, p2}, Ldg;->a(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;)Lcom/facebook/datasource/b;

    move-result-object v0

    .line 82
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbs;->a:Ldg;

    invoke-virtual {v0, p1, p2}, Ldg;->b(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;)Lcom/facebook/datasource/b;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;Z)Lcom/facebook/datasource/b;
    .locals 1

    .prologue
    .line 31
    check-cast p1, Lcom/facebook/imagepipeline/request/ImageRequest;

    invoke-virtual {p0, p1, p2, p3}, Lbs;->a(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;Z)Lcom/facebook/datasource/b;

    move-result-object v0

    return-object v0
.end method

.method protected b()Lbs;
    .locals 0

    .prologue
    .line 88
    return-object p0
.end method

.method public synthetic b(Landroid/net/Uri;)Lce;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lbs;->a(Landroid/net/Uri;)Lbs;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c()Lbw;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lbs;->b()Lbs;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d()Lbv;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lbs;->a()Lbr;

    move-result-object v0

    return-object v0
.end method
