.class public Lber;
.super Lcom/twitter/library/service/s;
.source "Twttr"


# instance fields
.field public a:I

.field private final b:Lbfd;

.field private final c:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lbfd;)V
    .locals 4

    .prologue
    .line 29
    const-class v0, Lber;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/s;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 30
    iput-object p3, p0, Lber;->b:Lbfd;

    .line 31
    iget-object v0, p0, Lber;->b:Lbfd;

    invoke-virtual {v0}, Lbfd;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lber;->c:J

    .line 33
    iget-object v0, p0, Lber;->b:Lbfd;

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lbfd;->a(J)V

    .line 34
    return-void
.end method


# virtual methods
.method protected a_(Lcom/twitter/library/service/u;)V
    .locals 4

    .prologue
    .line 38
    new-instance v0, Lbhn;

    iget-object v1, p0, Lber;->p:Landroid/content/Context;

    invoke-virtual {p0}, Lber;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Lbhn;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;I)V

    .line 40
    const/16 v1, 0x190

    invoke-virtual {v0, v1}, Lbhn;->a(I)Lbhn;

    move-result-object v1

    invoke-virtual {v1, p0}, Lbhn;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/service/s;

    .line 41
    invoke-virtual {v0}, Lbhn;->O()Lcom/twitter/library/service/u;

    move-result-object v1

    .line 42
    invoke-virtual {p1, v1}, Lcom/twitter/library/service/u;->a(Lcom/twitter/library/service/u;)V

    .line 44
    invoke-virtual {v1}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 45
    invoke-virtual {v0}, Lbhn;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lber;->a:I

    .line 46
    iget-object v0, p0, Lber;->b:Lbfd;

    iget v1, p0, Lber;->a:I

    invoke-virtual {v0, v1}, Lbfd;->a(I)V

    .line 47
    iget v0, p0, Lber;->a:I

    if-lez v0, :cond_0

    .line 48
    new-instance v0, Laut;

    iget-object v1, p0, Lber;->p:Landroid/content/Context;

    .line 49
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 50
    invoke-virtual {p0}, Lber;->R()Lcom/twitter/library/provider/t;

    move-result-object v1

    invoke-virtual {p0}, Lber;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/service/v;->c:J

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/library/provider/t;->d(JLaut;)V

    .line 51
    invoke-virtual {v0}, Laut;->a()V

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    iget-object v0, p0, Lber;->b:Lbfd;

    iget-wide v2, p0, Lber;->c:J

    invoke-virtual {v0, v2, v3}, Lbfd;->a(J)V

    goto :goto_0
.end method
