.class public final Laji;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laji$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Laji;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field public final d:Z

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Laji$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Laji$a;-><init>(Laji$1;)V

    sput-object v0, Laji;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZLjava/util/List;)V
    .locals 0
    .param p2    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZ",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Laji;->b:Ljava/lang/String;

    .line 62
    iput p2, p0, Laji;->c:I

    .line 63
    iput-boolean p3, p0, Laji;->d:Z

    .line 64
    iput-object p4, p0, Laji;->e:Ljava/util/List;

    .line 65
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IZLjava/util/List;Laji$1;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3, p4}, Laji;-><init>(Ljava/lang/String;IZLjava/util/List;)V

    return-void
.end method

.method public static a(Ljava/lang/String;IZ)Laji;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 80
    new-instance v0, Laji;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Laji;-><init>(Ljava/lang/String;IZLjava/util/List;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;IZLjava/util/List;)Laji;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZ",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Laji;"
        }
    .end annotation

    .prologue
    .line 104
    new-instance v0, Laji;

    invoke-static {p1}, Lcom/twitter/ui/socialproof/a;->a(I)I

    move-result v1

    invoke-direct {v0, p0, v1, p2, p3}, Laji;-><init>(Ljava/lang/String;IZLjava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Laji;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Laji;->c:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 121
    if-ne p0, p1, :cond_1

    .line 131
    :cond_0
    :goto_0
    return v0

    .line 124
    :cond_1
    instance-of v2, p1, Laji;

    if-eqz v2, :cond_3

    .line 125
    check-cast p1, Laji;

    .line 126
    iget-object v2, p0, Laji;->b:Ljava/lang/String;

    iget-object v3, p1, Laji;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Laji;->c:I

    iget v3, p1, Laji;->c:I

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Laji;->d:Z

    iget-boolean v3, p1, Laji;->d:Z

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Laji;->e:Ljava/util/List;

    iget-object v3, p1, Laji;->e:Ljava/util/List;

    .line 129
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 131
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 136
    iget-object v0, p0, Laji;->b:Ljava/lang/String;

    iget v1, p0, Laji;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-boolean v2, p0, Laji;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v3, p0, Laji;->e:Ljava/util/List;

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
