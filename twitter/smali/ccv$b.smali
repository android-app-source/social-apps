.class Lccv$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lccv;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lccv;",
        "Lccv$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lccv$1;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lccv$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lccv$a;
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lccv$a;

    invoke-direct {v0}, Lccv$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lccv$a;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 59
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    .line 60
    sget-object v1, Lcom/twitter/util/serialization/f;->k:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v1}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    .line 61
    sget-object v2, Lcom/twitter/util/serialization/f;->k:Lcom/twitter/util/serialization/l;

    .line 62
    invoke-static {p1, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/List;

    move-result-object v2

    .line 63
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v3

    .line 65
    invoke-virtual {p2, v0}, Lccv$a;->a(Ljava/lang/String;)Lccv$a;

    move-result-object v0

    .line 66
    invoke-virtual {v0, v1}, Lccv$a;->a(Ljava/lang/Object;)Lccv$a;

    move-result-object v0

    .line 67
    invoke-virtual {v0, v2}, Lccv$a;->a(Ljava/util/List;)Lccv$a;

    move-result-object v0

    .line 68
    invoke-virtual {v0, v3}, Lccv$a;->a(Z)Lccv$a;

    .line 69
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 39
    check-cast p2, Lccv$a;

    invoke-virtual {p0, p1, p2, p3}, Lccv$b;->a(Lcom/twitter/util/serialization/n;Lccv$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lccv;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p2, Lccv;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 45
    iget-object v0, p2, Lccv;->c:Ljava/lang/Object;

    sget-object v1, Lcom/twitter/util/serialization/f;->k:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 46
    iget-object v0, p2, Lccv;->d:Ljava/util/List;

    sget-object v1, Lcom/twitter/util/serialization/f;->k:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 47
    iget-boolean v0, p2, Lccv;->e:Z

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    .line 48
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    check-cast p2, Lccv;

    invoke-virtual {p0, p1, p2}, Lccv$b;->a(Lcom/twitter/util/serialization/o;Lccv;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lccv$b;->a()Lccv$a;

    move-result-object v0

    return-object v0
.end method
