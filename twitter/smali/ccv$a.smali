.class public final Lccv$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lccv;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lccv;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/Object;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lccv$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lccv$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lccv$a;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lccv$a;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Lccv$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lccv$a;->c:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lccv$a;)Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lccv$a;->d:Z

    return v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Lccv$a;
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lccv$a;->b:Ljava/lang/Object;

    .line 90
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lccv$a;
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lccv$a;->a:Ljava/lang/String;

    .line 84
    return-object p0
.end method

.method public a(Ljava/util/List;)Lccv$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Lccv$a;"
        }
    .end annotation

    .prologue
    .line 95
    iput-object p1, p0, Lccv$a;->c:Ljava/util/List;

    .line 96
    return-object p0
.end method

.method public a(Z)Lccv$a;
    .locals 0

    .prologue
    .line 101
    iput-boolean p1, p0, Lccv$a;->d:Z

    .line 102
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lccv$a;->e()Lccv;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lccv;
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lccv;

    invoke-direct {v0, p0}, Lccv;-><init>(Lccv$a;)V

    return-object v0
.end method
