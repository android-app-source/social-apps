.class public Lacr;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Lcom/twitter/media/ui/image/MediaImageView;

.field private final e:Landroid/widget/ImageView;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private final h:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private final i:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/res/Resources;Landroid/view/View;III)V
    .locals 2
    .param p3    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lacr;->a:Landroid/content/res/Resources;

    .line 62
    iput-object p2, p0, Lacr;->b:Landroid/view/View;

    .line 63
    iput p3, p0, Lacr;->h:I

    .line 64
    iput p4, p0, Lacr;->i:I

    .line 65
    const v0, 0x7f1304ee

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lacr;->c:Landroid/widget/TextView;

    .line 66
    const v0, 0x7f1304ed

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lacr;->d:Lcom/twitter/media/ui/image/MediaImageView;

    .line 67
    const v0, 0x7f130300

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lacr;->e:Landroid/widget/ImageView;

    .line 68
    iget-object v0, p0, Lacr;->e:Landroid/widget/ImageView;

    const v1, 0x7f110018

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 69
    iget-object v0, p0, Lacr;->e:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 70
    iget-object v0, p0, Lacr;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, p5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 71
    const v0, 0x7f13040f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lacr;->f:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f130524

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lacr;->g:Landroid/widget/TextView;

    .line 73
    return-void
.end method

.method public static a(Landroid/view/View;)Lacr;
    .locals 6

    .prologue
    .line 44
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 45
    new-instance v0, Lacr;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f110012

    .line 46
    invoke-static {v2, v3}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    const v4, 0x7f110003

    invoke-static {v2, v4}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v4

    const v5, 0x7f020699

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lacr;-><init>(Landroid/content/res/Resources;Landroid/view/View;III)V

    .line 45
    return-object v0
.end method

.method static synthetic a(Lacr;)Landroid/view/View;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lacr;->b:Landroid/view/View;

    return-object v0
.end method

.method public static b(Landroid/view/View;)Lacr;
    .locals 6

    .prologue
    .line 52
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 53
    new-instance v0, Lacr;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f110195

    .line 54
    invoke-static {v2, v3}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    const v4, 0x7f110182

    invoke-static {v2, v4}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v4

    const v5, 0x7f0204a4

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lacr;-><init>(Landroid/content/res/Resources;Landroid/view/View;III)V

    .line 53
    return-object v0
.end method

.method static synthetic b(Lacr;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lacr;->g()V

    return-void
.end method

.method private c(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 132
    const/4 v1, 0x0

    .line 133
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 134
    instance-of v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v2, :cond_0

    .line 135
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 136
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    .line 138
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 108
    invoke-virtual {p0}, Lacr;->a()I

    move-result v0

    invoke-virtual {p0}, Lacr;->b()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 109
    invoke-virtual {p0}, Lacr;->b()I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3f266666    # 0.65f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 110
    invoke-virtual {p0}, Lacr;->b()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3e800000    # 0.25f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 111
    iget-object v2, p0, Lacr;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 112
    iget-object v0, p0, Lacr;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 114
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()I
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lacr;->c:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lacr;->c(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Lacr;->f:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lacr;->c(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lacr;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    return-void
.end method

.method public a(Lcom/twitter/model/moments/a;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 79
    iget-object v0, p0, Lacr;->b:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 80
    iget-object v0, p0, Lacr;->d:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v5}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 81
    iget-object v0, p0, Lacr;->d:Lcom/twitter/media/ui/image/MediaImageView;

    new-instance v1, Lcom/twitter/media/request/a$a;

    iget-object v2, p1, Lcom/twitter/model/moments/a;->f:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/twitter/media/request/a$a;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 82
    iget-object v0, p0, Lacr;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lacr$1;

    invoke-direct {v1, p0}, Lacr$1;-><init>(Lacr;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 91
    iget-object v0, p0, Lacr;->c:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/twitter/model/moments/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v0, p0, Lacr;->c:Landroid/widget/TextView;

    iget v1, p0, Lacr;->h:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 93
    iget-object v0, p0, Lacr;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setAllCaps(Z)V

    .line 94
    iget-object v0, p0, Lacr;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 95
    iget-object v0, p0, Lacr;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lacr;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a0b92

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/twitter/model/moments/a;->e:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-boolean v0, p1, Lcom/twitter/model/moments/a;->c:Z

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lacr;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 101
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lacr;->e:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 145
    iget-object v0, p0, Lacr;->d:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v4}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 146
    iget-object v0, p0, Lacr;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    iget-object v0, p0, Lacr;->c:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAllCaps(Z)V

    .line 148
    iget-object v0, p0, Lacr;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    iget-object v2, p0, Lacr;->c:Landroid/widget/TextView;

    .line 149
    invoke-virtual {v2}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0358

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    .line 148
    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 150
    iget-object v0, p0, Lacr;->c:Landroid/widget/TextView;

    iget v1, p0, Lacr;->i:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 151
    iget-object v0, p0, Lacr;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 152
    iget-object v0, p0, Lacr;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 153
    return-void
.end method

.method protected b()I
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lacr;->d:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 128
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iget-object v1, p0, Lacr;->d:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-direct {p0, v1}, Lacr;->c(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lacr;->e:Landroid/widget/ImageView;

    invoke-direct {p0, v1}, Lacr;->c(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lacr;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lacr;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 181
    iget-object v0, p0, Lacr;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 156
    iget-object v0, p0, Lacr;->d:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 157
    iget-object v0, p0, Lacr;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 158
    iget-object v0, p0, Lacr;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 159
    iget-object v0, p0, Lacr;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 160
    return-void
.end method

.method public d()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Lacr;->c:Landroid/widget/TextView;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/view/View;

    const/4 v2, 0x0

    iget-object v3, p0, Lacr;->f:Landroid/widget/TextView;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lacr;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 172
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 186
    iget-object v0, p0, Lacr;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lacr;->g:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 189
    :cond_0
    return-void
.end method
