.class public final Lclh;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lckj;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lclf;

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lckl;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lckt;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcks;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/view/WindowManager;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lckw;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcku;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lclh;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lclh;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lclf;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lclf;",
            "Lcta",
            "<",
            "Lckl;",
            ">;",
            "Lcta",
            "<",
            "Lckt;",
            ">;",
            "Lcta",
            "<",
            "Lcks;",
            ">;",
            "Lcta",
            "<",
            "Landroid/view/WindowManager;",
            ">;",
            "Lcta",
            "<",
            "Lckw;",
            ">;",
            "Lcta",
            "<",
            "Lcku;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    sget-boolean v0, Lclh;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 44
    :cond_0
    iput-object p1, p0, Lclh;->b:Lclf;

    .line 45
    sget-boolean v0, Lclh;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 46
    :cond_1
    iput-object p2, p0, Lclh;->c:Lcta;

    .line 47
    sget-boolean v0, Lclh;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 48
    :cond_2
    iput-object p3, p0, Lclh;->d:Lcta;

    .line 49
    sget-boolean v0, Lclh;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 50
    :cond_3
    iput-object p4, p0, Lclh;->e:Lcta;

    .line 51
    sget-boolean v0, Lclh;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 52
    :cond_4
    iput-object p5, p0, Lclh;->f:Lcta;

    .line 53
    sget-boolean v0, Lclh;->a:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 54
    :cond_5
    iput-object p6, p0, Lclh;->g:Lcta;

    .line 55
    sget-boolean v0, Lclh;->a:Z

    if-nez v0, :cond_6

    if-nez p7, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 56
    :cond_6
    iput-object p7, p0, Lclh;->h:Lcta;

    .line 57
    return-void
.end method

.method public static a(Lclf;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lclf;",
            "Lcta",
            "<",
            "Lckl;",
            ">;",
            "Lcta",
            "<",
            "Lckt;",
            ">;",
            "Lcta",
            "<",
            "Lcks;",
            ">;",
            "Lcta",
            "<",
            "Landroid/view/WindowManager;",
            ">;",
            "Lcta",
            "<",
            "Lckw;",
            ">;",
            "Lcta",
            "<",
            "Lcku;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Lckj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    new-instance v0, Lclh;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lclh;-><init>(Lclf;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lckj;
    .locals 7

    .prologue
    .line 61
    iget-object v0, p0, Lclh;->b:Lclf;

    iget-object v1, p0, Lclh;->c:Lcta;

    .line 63
    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lckl;

    iget-object v2, p0, Lclh;->d:Lcta;

    .line 64
    invoke-interface {v2}, Lcta;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lckt;

    iget-object v3, p0, Lclh;->e:Lcta;

    .line 65
    invoke-interface {v3}, Lcta;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcks;

    iget-object v4, p0, Lclh;->f:Lcta;

    .line 66
    invoke-interface {v4}, Lcta;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    iget-object v5, p0, Lclh;->g:Lcta;

    .line 67
    invoke-interface {v5}, Lcta;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lckw;

    iget-object v6, p0, Lclh;->h:Lcta;

    .line 68
    invoke-interface {v6}, Lcta;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcku;

    .line 62
    invoke-virtual/range {v0 .. v6}, Lclf;->a(Lckl;Lckt;Lcks;Landroid/view/WindowManager;Lckw;Lcku;)Lckj;

    move-result-object v0

    const-string/jumbo v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 61
    invoke-static {v0, v1}, Ldagger/internal/e;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lckj;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lclh;->a()Lckj;

    move-result-object v0

    return-object v0
.end method
