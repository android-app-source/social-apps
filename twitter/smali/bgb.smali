.class public abstract Lbgb;
.super Lbge;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbge",
        "<",
        "Lbgc;",
        ">;"
    }
.end annotation


# instance fields
.field protected final a:J

.field private final b:I

.field private final c:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;JILjava/lang/String;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lbge;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 31
    iput-wide p4, p0, Lbgb;->a:J

    .line 32
    iput p6, p0, Lbgb;->b:I

    .line 33
    iput-object p7, p0, Lbgb;->c:Ljava/lang/String;

    .line 34
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lbgc;)V
    .locals 10

    .prologue
    .line 45
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 48
    iget-object v3, p2, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    .line 49
    invoke-virtual {p3}, Lbgc;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/s;

    .line 50
    if-eqz v0, :cond_0

    .line 51
    iget-object v4, v0, Lcom/twitter/model/timeline/s;->b:Ljava/util/List;

    .line 52
    invoke-virtual {p0}, Lbgb;->D()J

    move-result-wide v6

    .line 53
    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-lez v1, :cond_1

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 54
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/timeline/ah;

    iget-object v1, v1, Lcom/twitter/model/timeline/ah;->a:Lcom/twitter/model/core/ac;

    iget-wide v8, v1, Lcom/twitter/model/core/ac;->a:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 58
    :goto_0
    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-lez v1, :cond_2

    const/4 v1, 0x1

    .line 59
    :goto_1
    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    .line 61
    :goto_2
    invoke-virtual {p0}, Lbgb;->R()Lcom/twitter/library/provider/t;

    move-result-object v4

    .line 62
    invoke-virtual {p0}, Lbgb;->S()Laut;

    move-result-object v5

    .line 64
    invoke-static {v0}, Lcom/twitter/library/provider/s$a;->a(Lcom/twitter/model/timeline/s;)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    iget-wide v6, p0, Lbgb;->a:J

    .line 65
    invoke-virtual {v0, v6, v7}, Lcom/twitter/library/provider/s$a;->a(J)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    iget v6, p0, Lbgb;->b:I

    .line 66
    invoke-virtual {v0, v6}, Lcom/twitter/library/provider/s$a;->a(I)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    iget-object v6, p0, Lbgb;->c:Ljava/lang/String;

    .line 67
    invoke-virtual {v0, v6}, Lcom/twitter/library/provider/s$a;->a(Ljava/lang/String;)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 68
    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/s$a;->a(Z)Lcom/twitter/library/provider/s$a;

    move-result-object v1

    if-nez v2, :cond_4

    const/4 v0, 0x1

    .line 69
    :goto_3
    invoke-virtual {v1, v0}, Lcom/twitter/library/provider/s$a;->c(Z)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 70
    invoke-virtual {v0, v2}, Lcom/twitter/library/provider/s$a;->b(Ljava/lang/String;)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 71
    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/s$a;->d(Z)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 72
    invoke-virtual {v0, v5}, Lcom/twitter/library/provider/s$a;->a(Laut;)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 73
    invoke-virtual {p0}, Lbgb;->N()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/s$a;->e(Z)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lcom/twitter/library/provider/s$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/s;

    .line 63
    invoke-virtual {v4, v0}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/library/provider/s;)I

    move-result v0

    .line 75
    invoke-virtual {p0, v0}, Lbgb;->d(I)Lbge;

    .line 76
    invoke-virtual {v5}, Laut;->a()V

    .line 79
    const-string/jumbo v1, "scribe_item_count"

    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 82
    :cond_0
    return-void

    .line 56
    :cond_1
    const/4 v1, 0x0

    move-object v2, v1

    goto :goto_0

    .line 58
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 59
    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    .line 68
    :cond_4
    const/4 v0, 0x0

    goto :goto_3
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 21
    check-cast p3, Lbgc;

    invoke-virtual {p0, p1, p2, p3}, Lbgb;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lbgc;)V

    return-void
.end method

.method protected e()Lbgc;
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lbgc;

    invoke-direct {v0}, Lbgc;-><init>()V

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lbgb;->e()Lbgc;

    move-result-object v0

    return-object v0
.end method
