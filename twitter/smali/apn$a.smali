.class public abstract Lapn$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lapn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/twitter/model/dms/c;",
        "H:",
        "Lapn$b;",
        "B:",
        "Lapn$a",
        "<TE;TH;TB;>;>",
        "Lcom/twitter/util/object/i",
        "<",
        "Lapn;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lapn$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TH;"
        }
    .end annotation
.end field

.field b:Lcom/twitter/model/dms/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field c:Lcom/twitter/model/dms/c;

.field private d:Lcom/twitter/model/dms/m;

.field private e:Landroid/content/Context;

.field private f:Lcom/twitter/app/dm/m;

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lapn$a;)Lcom/twitter/model/dms/m;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lapn$a;->d:Lcom/twitter/model/dms/m;

    return-object v0
.end method

.method static synthetic b(Lapn$a;)Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lapn$a;->g:Z

    return v0
.end method

.method static synthetic c(Lapn$a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lapn$a;->e:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lapn$a;)Lcom/twitter/app/dm/m;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lapn$a;->f:Lcom/twitter/app/dm/m;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 141
    invoke-super {p0}, Lcom/twitter/util/object/i;->R_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapn$a;->b:Lcom/twitter/model/dms/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapn$a;->d:Lcom/twitter/model/dms/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapn$a;->e:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapn$a;->f:Lcom/twitter/app/dm/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapn$a;->a:Lapn$b;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)Lapn$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 117
    iput-object p1, p0, Lapn$a;->e:Landroid/content/Context;

    .line 118
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapn$a;

    return-object v0
.end method

.method public a(Lapn$b;)Lapn$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TH;)TB;"
        }
    .end annotation

    .prologue
    .line 135
    iput-object p1, p0, Lapn$a;->a:Lapn$b;

    .line 136
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapn$a;

    return-object v0
.end method

.method public a(Lcom/twitter/app/dm/m;)Lapn$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/app/dm/m;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 123
    iput-object p1, p0, Lapn$a;->f:Lcom/twitter/app/dm/m;

    .line 124
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapn$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/dms/c;)Lapn$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TB;"
        }
    .end annotation

    .prologue
    .line 99
    iput-object p1, p0, Lapn$a;->b:Lcom/twitter/model/dms/c;

    .line 100
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapn$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/dms/m;)Lapn$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/dms/m;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 111
    iput-object p1, p0, Lapn$a;->d:Lcom/twitter/model/dms/m;

    .line 112
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapn$a;

    return-object v0
.end method

.method public a(Z)Lapn$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TB;"
        }
    .end annotation

    .prologue
    .line 129
    iput-boolean p1, p0, Lapn$a;->g:Z

    .line 130
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapn$a;

    return-object v0
.end method

.method public b(Lcom/twitter/model/dms/c;)Lapn$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/dms/c;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 105
    iput-object p1, p0, Lapn$a;->c:Lcom/twitter/model/dms/c;

    .line 106
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapn$a;

    return-object v0
.end method
