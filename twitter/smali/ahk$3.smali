.class Lahk$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lahk;-><init>(Lcom/twitter/app/common/base/BaseFragmentActivity;Laii;Lahx;Lcom/twitter/android/search/AdvancedSearchFiltersActivity$a;Laht;Lbqg;Lcom/twitter/android/geo/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lahk;


# direct methods
.method constructor <init>(Lahk;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lahk$3;->a:Lahk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 90
    new-instance v2, Laii$a;

    iget-object v3, p0, Lahk$3;->a:Lahk;

    iget-object v3, v3, Lahk;->a:Laii;

    invoke-direct {v2, v3}, Laii$a;-><init>(Laii;)V

    .line 91
    invoke-virtual {p1}, Landroid/widget/RadioGroup;->getId()I

    move-result v3

    .line 92
    const v4, 0x7f1307de

    if-ne v3, v4, :cond_2

    .line 93
    invoke-virtual {p1}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v3

    const v4, 0x7f1307c4

    if-ne v3, v4, :cond_1

    .line 94
    :goto_0
    invoke-virtual {v2, v0}, Laii$a;->a(Z)Laii$a;

    .line 95
    iget-object v0, p0, Lahk$3;->a:Lahk;

    invoke-virtual {v2}, Laii$a;->e()Laii;

    move-result-object v1

    iput-object v1, v0, Lahk;->a:Laii;

    .line 105
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 93
    goto :goto_0

    .line 96
    :cond_2
    const v4, 0x7f1307df

    if-ne v3, v4, :cond_0

    .line 97
    invoke-virtual {p1}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v3

    const v4, 0x7f1307e1

    if-ne v3, v4, :cond_3

    move v1, v0

    .line 98
    :cond_3
    if-eqz v1, :cond_4

    iget-object v3, p0, Lahk$3;->a:Lahk;

    invoke-static {v3}, Lahk;->c(Lahk;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 99
    iget-object v1, p0, Lahk$3;->a:Lahk;

    invoke-static {v1}, Lahk;->d(Lahk;)Lcom/twitter/android/geo/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/geo/a;->a(I)V

    goto :goto_1

    .line 101
    :cond_4
    invoke-virtual {v2, v1}, Laii$a;->b(Z)Laii$a;

    .line 102
    iget-object v0, p0, Lahk$3;->a:Lahk;

    invoke-virtual {v2}, Laii$a;->e()Laii;

    move-result-object v1

    iput-object v1, v0, Lahk;->a:Laii;

    goto :goto_1
.end method
