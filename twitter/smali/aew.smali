.class public Laew;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcxq;


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/library/client/p;

.field private final c:Lcom/twitter/android/periscope/q;

.field private final d:Lcom/twitter/library/client/Session;

.field private final e:Lcom/twitter/model/core/Tweet;

.field private final f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/client/p;Lcom/twitter/android/periscope/q;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Laew;->a:Ljava/lang/ref/WeakReference;

    .line 35
    iput-object p2, p0, Laew;->e:Lcom/twitter/model/core/Tweet;

    .line 36
    iput-object p3, p0, Laew;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 37
    iput-object p4, p0, Laew;->b:Lcom/twitter/library/client/p;

    .line 38
    iput-object p5, p0, Laew;->c:Lcom/twitter/android/periscope/q;

    .line 39
    iput-object p6, p0, Laew;->d:Lcom/twitter/library/client/Session;

    .line 40
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 49
    iget-object v0, p0, Laew;->e:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laew;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    iget-object v0, p0, Laew;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Laew;->e:Lcom/twitter/model/core/Tweet;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Z)V

    .line 53
    iget-object v0, p0, Laew;->c:Lcom/twitter/android/periscope/q;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/q;->b()V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 63
    iget-object v0, p0, Laew;->e:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laew;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    iget-object v0, p0, Laew;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 67
    new-instance v1, Lcom/twitter/android/ReportFlowWebViewActivity$a;

    invoke-direct {v1}, Lcom/twitter/android/ReportFlowWebViewActivity$a;-><init>()V

    iget-object v2, p0, Laew;->e:Lcom/twitter/model/core/Tweet;

    iget-object v3, p0, Laew;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 68
    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/ReportFlowWebViewActivity$a;

    move-result-object v1

    const-string/jumbo v2, "reportvideo"

    .line 69
    invoke-virtual {v1, v2}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->a(Ljava/lang/String;)Lcom/twitter/android/ReportFlowWebViewActivity$a;

    move-result-object v1

    .line 70
    invoke-virtual {v1, v0}, Lcom/twitter/android/ReportFlowWebViewActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 71
    iget-object v0, p0, Laew;->c:Lcom/twitter/android/periscope/q;

    invoke-virtual {v0}, Lcom/twitter/android/periscope/q;->c()V

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 82
    iget-object v0, p0, Laew;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 91
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Laew;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 86
    iget-object v1, p0, Laew;->b:Lcom/twitter/library/client/p;

    new-instance v2, Lbhb;

    iget-object v3, p0, Laew;->d:Lcom/twitter/library/client/Session;

    const-wide/16 v4, 0x0

    .line 89
    invoke-static {p1, v4, v5}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-direct {v2, v0, v3, v4, v5}, Lbhb;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    .line 86
    invoke-virtual {v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 90
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/app/main/MainActivity;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
