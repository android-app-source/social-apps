.class Lcoi$b;
.super Lcoi$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcoi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private final a:Lrx/j;

.field private final b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Z


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 226
    invoke-direct {p0}, Lcoi$a;-><init>()V

    .line 223
    const-string/jumbo v0, "unassigned"

    iput-object v0, p0, Lcoi$b;->c:Ljava/lang/String;

    .line 227
    iput-object p1, p0, Lcoi$b;->b:Ljava/lang/String;

    .line 228
    invoke-static {}, Lcon;->a()Lrx/c;

    move-result-object v0

    new-instance v1, Lcoi$b$1;

    invoke-direct {v1, p0}, Lcoi$b$1;-><init>(Lcoi$b;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcoi$b;->a:Lrx/j;

    .line 237
    return-void
.end method

.method static synthetic a(Lcoi$b;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcoi$b;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcoi$b;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcoi$b;->c:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcoi$b;Z)Z
    .locals 0

    .prologue
    .line 218
    iput-boolean p1, p0, Lcoi$b;->d:Z

    return p1
.end method

.method static synthetic b(Lcoi$b;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcoi$b;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 247
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcoi$b;->a:Lrx/j;

    invoke-interface {v0}, Lrx/j;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Call to get() on a closed experiment cache."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 250
    :cond_0
    iget-boolean v0, p0, Lcoi$b;->d:Z

    if-eqz v0, :cond_1

    .line 252
    iget-object v0, p0, Lcoi$b;->b:Ljava/lang/String;

    invoke-static {v0}, Lcoi;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 253
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcoi$b;->d:Z

    .line 255
    :cond_1
    iget-object v0, p0, Lcoi$b;->c:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 247
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 218
    invoke-virtual {p0}, Lcoi$b;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcoi$b;->a:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 242
    return-void
.end method
