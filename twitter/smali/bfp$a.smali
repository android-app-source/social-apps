.class public abstract Lbfp$a;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbfp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    .line 92
    iput-object p1, p0, Lbfp$a;->a:Landroid/content/Context;

    .line 93
    return-void
.end method


# virtual methods
.method protected abstract a(JZI)V
.end method

.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 87
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lbfp$a;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 4

    .prologue
    .line 97
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 98
    check-cast p1, Lbfp;

    .line 99
    invoke-virtual {p1}, Lbfp;->g()J

    move-result-wide v2

    .line 100
    invoke-virtual {p1}, Lbfp;->h()I

    move-result v1

    .line 102
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v3, v0, v1}, Lbfp$a;->a(JZI)V

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v3, v0, v1}, Lbfp$a;->a(JZI)V

    goto :goto_0
.end method
