.class public final Lcgb$c$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcgb$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcgb$c;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 261
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcgb$c$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcgb$c$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcgb$c$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcgb$c$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcgb$c$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcgb$c$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcgb$c$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcgb$c$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcgb$c$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcgb$c$a;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcgb$c$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcgb$c$a;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcgb$c$a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcgb$c$a;->g:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcgb$c$a;->g:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcgb$c$a;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcgb$c$a;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcgb$c$a;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcgb$c$a;
    .locals 0

    .prologue
    .line 272
    iput-object p1, p0, Lcgb$c$a;->a:Ljava/lang/String;

    .line 273
    return-object p0
.end method

.method public a(Ljava/util/Map;)Lcgb$c$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcgb$c$a;"
        }
    .end annotation

    .prologue
    .line 308
    iput-object p1, p0, Lcgb$c$a;->g:Ljava/util/Map;

    .line 309
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcgb$c$a;
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcgb$c$a;->b:Ljava/lang/String;

    .line 279
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 261
    invoke-virtual {p0}, Lcgb$c$a;->e()Lcgb$c;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcgb$c$a;
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lcgb$c$a;->c:Ljava/lang/String;

    .line 285
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcgb$c$a;
    .locals 0

    .prologue
    .line 290
    iput-object p1, p0, Lcgb$c$a;->d:Ljava/lang/String;

    .line 291
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcgb$c$a;
    .locals 0

    .prologue
    .line 296
    iput-object p1, p0, Lcgb$c$a;->e:Ljava/lang/String;

    .line 297
    return-object p0
.end method

.method protected e()Lcgb$c;
    .locals 2

    .prologue
    .line 315
    new-instance v0, Lcgb$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcgb$c;-><init>(Lcgb$c$a;Lcgb$1;)V

    return-object v0
.end method

.method public f(Ljava/lang/String;)Lcgb$c$a;
    .locals 0

    .prologue
    .line 302
    iput-object p1, p0, Lcgb$c$a;->f:Ljava/lang/String;

    .line 303
    return-object p0
.end method
