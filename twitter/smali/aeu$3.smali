.class final Laeu$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcpp;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Laeu;->a(Ljava/lang/Iterable;Ljava/util/Map;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcpp",
        "<",
        "Lcom/twitter/model/people/l;",
        "Lcom/twitter/model/people/l;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/util/Map;


# direct methods
.method constructor <init>(Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Laeu$3;->a:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/people/l;)Lcom/twitter/model/people/l;
    .locals 4

    .prologue
    .line 126
    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    iget-object v0, p0, Laeu$3;->a:Ljava/util/Map;

    iget-object v1, p1, Lcom/twitter/model/people/l;->a:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v1}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 129
    if-nez v0, :cond_0

    .line 132
    :goto_0
    return-object p1

    :cond_0
    new-instance v1, Lcom/twitter/model/people/l$a;

    invoke-direct {v1}, Lcom/twitter/model/people/l$a;-><init>()V

    .line 133
    invoke-virtual {v1, v0}, Lcom/twitter/model/people/l$a;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/people/l$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/people/l;->b:Ljava/lang/String;

    .line 134
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/l$a;->a(Ljava/lang/String;)Lcom/twitter/model/people/l$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/people/l;->c:Ljava/lang/String;

    .line 135
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/l$a;->b(Ljava/lang/String;)Lcom/twitter/model/people/l$a;

    move-result-object v0

    iget-boolean v1, p1, Lcom/twitter/model/people/l;->d:Z

    .line 136
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/l$a;->a(Z)Lcom/twitter/model/people/l$a;

    move-result-object v0

    .line 137
    invoke-virtual {v0}, Lcom/twitter/model/people/l$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/l;

    move-object p1, v0

    .line 132
    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 122
    check-cast p1, Lcom/twitter/model/people/l;

    invoke-virtual {p0, p1}, Laeu$3;->a(Lcom/twitter/model/people/l;)Lcom/twitter/model/people/l;

    move-result-object v0

    return-object v0
.end method
