.class public Lcjg;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcjd;


# instance fields
.field private final a:Lcom/twitter/model/onboarding/TaskContext;

.field private final b:Lcje;

.field private final c:Lcjj;

.field private final d:Lcjb;


# direct methods
.method public constructor <init>(Lcom/twitter/model/onboarding/TaskContext;Lcje;Lcjj;Lcjb;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcjg;->a:Lcom/twitter/model/onboarding/TaskContext;

    .line 37
    iput-object p2, p0, Lcjg;->b:Lcje;

    .line 38
    iput-object p3, p0, Lcjg;->c:Lcjj;

    .line 39
    iput-object p4, p0, Lcjg;->d:Lcjb;

    .line 40
    return-void
.end method

.method static synthetic a(Lcjg;)Lcje;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcjg;->b:Lcje;

    return-object v0
.end method


# virtual methods
.method public a(Lcgg;)Lrx/g;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcgg;",
            ")",
            "Lrx/g",
            "<",
            "Lcom/twitter/model/onboarding/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcjg;->a:Lcom/twitter/model/onboarding/TaskContext;

    invoke-virtual {v0}, Lcom/twitter/model/onboarding/TaskContext;->a()Lcom/twitter/model/onboarding/Task;

    move-result-object v1

    .line 46
    iget-object v0, p0, Lcjg;->a:Lcom/twitter/model/onboarding/TaskContext;

    invoke-virtual {v0}, Lcom/twitter/model/onboarding/TaskContext;->b()Lcom/twitter/model/onboarding/Subtask;

    move-result-object v0

    .line 47
    iget-object v2, p0, Lcjg;->d:Lcjb;

    .line 48
    invoke-virtual {v0}, Lcom/twitter/model/onboarding/Subtask;->a()Lcom/twitter/model/onboarding/subtask/SubtaskProperties;

    move-result-object v0

    invoke-virtual {v2, v0, p1}, Lcjb;->a(Lcom/twitter/model/onboarding/subtask/SubtaskProperties;Lcgg;)Lcom/twitter/model/onboarding/NavigationLink;

    move-result-object v0

    .line 49
    instance-of v2, v0, Lcom/twitter/model/onboarding/SubtaskNavigationLink;

    if-eqz v2, :cond_1

    .line 50
    check-cast v0, Lcom/twitter/model/onboarding/SubtaskNavigationLink;

    iget-object v0, v0, Lcom/twitter/model/onboarding/SubtaskNavigationLink;->subtaskId:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/model/onboarding/Task;->a(Ljava/lang/String;)Lcom/twitter/model/onboarding/Subtask;

    move-result-object v0

    .line 51
    if-nez v0, :cond_0

    .line 52
    sget-object v0, Lcom/twitter/model/onboarding/d;->a:Lcom/twitter/model/onboarding/d;

    invoke-static {v0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    .line 75
    :goto_0
    return-object v0

    .line 54
    :cond_0
    iget-object v1, p0, Lcjg;->b:Lcje;

    new-instance v2, Lcom/twitter/model/onboarding/TaskContext;

    iget-object v3, p0, Lcjg;->a:Lcom/twitter/model/onboarding/TaskContext;

    .line 55
    invoke-virtual {v3}, Lcom/twitter/model/onboarding/TaskContext;->a()Lcom/twitter/model/onboarding/Task;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/twitter/model/onboarding/TaskContext;-><init>(Lcom/twitter/model/onboarding/Task;Lcom/twitter/model/onboarding/Subtask;)V

    .line 54
    invoke-virtual {v1, v2}, Lcje;->a(Lcom/twitter/model/onboarding/TaskContext;)Lcom/twitter/model/onboarding/b;

    move-result-object v0

    invoke-static {v0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    goto :goto_0

    .line 56
    :cond_1
    instance-of v2, v0, Lcom/twitter/model/onboarding/DeepLinkNavigationLink;

    if-eqz v2, :cond_2

    .line 57
    iget-object v1, p0, Lcjg;->b:Lcje;

    check-cast v0, Lcom/twitter/model/onboarding/DeepLinkNavigationLink;

    invoke-virtual {v1, v0}, Lcje;->a(Lcom/twitter/model/onboarding/DeepLinkNavigationLink;)Lcom/twitter/model/onboarding/b;

    move-result-object v0

    invoke-static {v0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    goto :goto_0

    .line 59
    :cond_2
    instance-of v0, v0, Lcom/twitter/model/onboarding/GetNextTaskNavigationLink;

    if-eqz v0, :cond_3

    .line 61
    iget-object v0, p0, Lcjg;->c:Lcjj;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcjj;->a(Lcom/twitter/model/onboarding/Task;Lcom/twitter/model/onboarding/a;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcjg$1;

    invoke-direct {v1, p0}, Lcjg$1;-><init>(Lcjg;)V

    .line 62
    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    goto :goto_0

    .line 75
    :cond_3
    sget-object v0, Lcom/twitter/model/onboarding/d;->a:Lcom/twitter/model/onboarding/d;

    invoke-static {v0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    goto :goto_0
.end method
