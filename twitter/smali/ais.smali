.class public Lais;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lair;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lais$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/provider/t;

.field private final b:Lcom/twitter/library/client/p;

.field private final c:Lcom/twitter/model/core/TwitterUser;

.field private final d:Lcgi;


# direct methods
.method public constructor <init>(Lcom/twitter/library/provider/t;Lcom/twitter/library/client/p;Lcom/twitter/model/core/TwitterUser;Lcgi;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lais;->a:Lcom/twitter/library/provider/t;

    .line 34
    iput-object p2, p0, Lais;->b:Lcom/twitter/library/client/p;

    .line 35
    iput-object p3, p0, Lais;->c:Lcom/twitter/model/core/TwitterUser;

    .line 36
    iput-object p4, p0, Lais;->d:Lcgi;

    .line 37
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lrx/c;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/client/Session;",
            ")",
            "Lrx/c",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    new-instance v0, Lbhy;

    invoke-direct {v0, p1, p2}, Lbhy;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 43
    iget-object v1, p0, Lais;->c:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    iput-wide v2, v0, Lbhy;->a:J

    .line 44
    new-instance v1, Lais$a;

    iget-object v2, p0, Lais;->a:Lcom/twitter/library/provider/t;

    iget-object v3, p0, Lais;->b:Lcom/twitter/library/client/p;

    iget-object v4, p0, Lais;->c:Lcom/twitter/model/core/TwitterUser;

    invoke-direct {v1, v2, v3, v0, v4}, Lais$a;-><init>(Lcom/twitter/library/provider/t;Lcom/twitter/library/client/p;Lbhy;Lcom/twitter/model/core/TwitterUser;)V

    invoke-static {v1}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lait;)V
    .locals 6

    .prologue
    const/16 v5, 0x10

    const/16 v4, 0x8

    const/4 v3, 0x1

    .line 51
    new-instance v0, Lbij;

    iget-object v1, p0, Lais;->c:Lcom/twitter/model/core/TwitterUser;

    iget-object v2, p0, Lais;->d:Lcgi;

    invoke-direct {v0, p1, p2, v1, v2}, Lbij;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/TwitterUser;Lcgi;)V

    .line 53
    invoke-virtual {p3}, Lait;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 54
    invoke-virtual {v0, v5, v3}, Lbij;->a(IZ)Lbij;

    .line 55
    invoke-virtual {v0, v3}, Lbij;->a(I)Lbij;

    .line 56
    invoke-virtual {v0, v4}, Lbij;->a(I)Lbij;

    .line 64
    :goto_0
    iget-object v1, p0, Lais;->b:Lcom/twitter/library/client/p;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    .line 65
    return-void

    .line 59
    :cond_0
    invoke-virtual {p3}, Lait;->e()Z

    move-result v1

    .line 58
    invoke-virtual {v0, v3, v1}, Lbij;->a(IZ)Lbij;

    .line 61
    invoke-virtual {p3}, Lait;->c()Z

    move-result v1

    .line 60
    invoke-virtual {v0, v4, v1}, Lbij;->a(IZ)Lbij;

    .line 62
    invoke-virtual {v0, v5}, Lbij;->a(I)Lbij;

    goto :goto_0
.end method
