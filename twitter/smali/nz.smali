.class public Lnz;
.super Lbjb;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/av/playback/AVPlayer;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p2}, Lbjb;-><init>(Lcom/twitter/model/av/AVMedia;)V

    .line 31
    iput-object p1, p0, Lnz;->a:Lcom/twitter/library/av/playback/AVPlayer;

    .line 32
    return-void
.end method


# virtual methods
.method public processCTAClickOpenEvent(Lbjd;)V
    .locals 3
    .annotation runtime Lbiz;
        a = Lbjd;
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lnz;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {p0}, Lnz;->b()Lcom/twitter/library/av/m;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/library/av/m;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    const-string/jumbo v2, "cta_click_open"

    invoke-virtual {v1, v2}, Lcom/twitter/library/av/m$a;->a(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/av/m$a;->a()Lcom/twitter/library/av/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/m;)V

    .line 42
    return-void
.end method

.method public processCTAClickSignupEvent(Lbje;)V
    .locals 3
    .annotation runtime Lbiz;
        a = Lbje;
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lnz;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {p0}, Lnz;->b()Lcom/twitter/library/av/m;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/library/av/m;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    const-string/jumbo v2, "cta_click_signup"

    invoke-virtual {v1, v2}, Lcom/twitter/library/av/m$a;->a(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/av/m$a;->a()Lcom/twitter/library/av/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/m;)V

    .line 52
    return-void
.end method

.method public processCTAImpressionOpenEvent(Lbjf;)V
    .locals 3
    .annotation runtime Lbiz;
        a = Lbjf;
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lnz;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {p0}, Lnz;->b()Lcom/twitter/library/av/m;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/library/av/m;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    const-string/jumbo v2, "cta_impression_open"

    invoke-virtual {v1, v2}, Lcom/twitter/library/av/m$a;->a(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/av/m$a;->a()Lcom/twitter/library/av/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/m;)V

    .line 37
    return-void
.end method

.method public processCTAImpressionSignupEvent(Lbjg;)V
    .locals 3
    .annotation runtime Lbiz;
        a = Lbjg;
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lnz;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {p0}, Lnz;->b()Lcom/twitter/library/av/m;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/library/av/m;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    const-string/jumbo v2, "cta_impression_signup"

    invoke-virtual {v1, v2}, Lcom/twitter/library/av/m$a;->a(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/av/m$a;->a()Lcom/twitter/library/av/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/m;)V

    .line 47
    return-void
.end method
