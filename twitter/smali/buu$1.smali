.class Lbuu$1;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbuu;->b(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Lcom/twitter/util/collection/Pair",
        "<",
        "Lbec;",
        "Lcom/twitter/library/service/u;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lbuu;


# direct methods
.method constructor <init>(Lbuu;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lbuu$1;->a:Lbuu;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/util/collection/Pair;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Lbec;",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lbuu$1;->a:Lbuu;

    invoke-static {v0}, Lbuu;->a(Lbuu;)Lcom/twitter/util/android/g;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/util/collection/Pair;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lazw$k;->preference_notification_success:I

    :goto_0
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/twitter/util/android/g;->a(II)Landroid/widget/Toast;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 49
    return-void

    .line 47
    :cond_0
    sget v0, Lazw$k;->preference_notification_error:I

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 44
    check-cast p1, Lcom/twitter/util/collection/Pair;

    invoke-virtual {p0, p1}, Lbuu$1;->a(Lcom/twitter/util/collection/Pair;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcqw;->a(Ljava/lang/Throwable;)V

    .line 54
    iget-object v0, p0, Lbuu$1;->a:Lbuu;

    invoke-static {v0}, Lbuu;->a(Lbuu;)Lcom/twitter/util/android/g;

    move-result-object v0

    sget v1, Lazw$k;->preference_notification_error:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/android/g;->a(II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 55
    return-void
.end method
