.class public Lauz;
.super Landroid/support/v4/content/AsyncTaskLoader;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lauz$b;,
        Lauz$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MODE",
        "L:Ljava/lang/Object;",
        ">",
        "Landroid/support/v4/content/AsyncTaskLoader",
        "<",
        "Lcbi",
        "<TMODE",
        "L;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/database/hydrator/c;

.field private final b:Lcom/twitter/database/model/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/model/l",
            "<*>;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TMODE",
            "L;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/database/model/f;

.field private final e:Landroid/database/ContentObserver;

.field private f:Lcbi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcbi",
            "<TMODE",
            "L;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lrx/j;

.field private h:Z


# direct methods
.method private constructor <init>(Lauz$a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lauz$a",
            "<TMODE",
            "L;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-static {p1}, Lauz$a;->a(Lauz$a;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v4/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    .line 46
    invoke-static {p1}, Lauz$a;->b(Lauz$a;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lauz;->c:Ljava/lang/Class;

    .line 47
    invoke-static {p1}, Lauz$a;->c(Lauz$a;)Lcom/twitter/database/model/f;

    move-result-object v0

    iput-object v0, p0, Lauz;->d:Lcom/twitter/database/model/f;

    .line 49
    invoke-static {p1}, Lauz$a;->d(Lauz$a;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 50
    new-instance v0, Lauz$b;

    invoke-direct {v0, p0}, Lauz$b;-><init>(Lauz;)V

    iput-object v0, p0, Lauz;->e:Landroid/database/ContentObserver;

    .line 51
    invoke-static {p1}, Lauz$a;->a(Lauz$a;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 52
    invoke-static {p1}, Lauz$a;->d(Lauz$a;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lauz;->e:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 57
    :goto_0
    invoke-static {p1}, Lauz$a;->e(Lauz$a;)Lcom/twitter/database/model/l;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lauz$a;->e(Lauz$a;)Lcom/twitter/database/model/l;

    move-result-object v0

    .line 58
    :goto_1
    iput-object v0, p0, Lauz;->b:Lcom/twitter/database/model/l;

    .line 59
    invoke-static {p1}, Lauz$a;->g(Lauz$a;)Lcom/twitter/database/model/i;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/database/hydrator/c;->a(Lcom/twitter/database/model/i;)Lcom/twitter/database/hydrator/c;

    move-result-object v0

    iput-object v0, p0, Lauz;->a:Lcom/twitter/database/hydrator/c;

    .line 60
    return-void

    .line 54
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lauz;->e:Landroid/database/ContentObserver;

    goto :goto_0

    .line 58
    :cond_1
    invoke-static {p1}, Lauz$a;->g(Lauz$a;)Lcom/twitter/database/model/i;

    move-result-object v1

    invoke-static {p1}, Lauz$a;->f(Lauz$a;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-interface {v1, v0}, Lcom/twitter/database/model/i;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/database/model/k;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    goto :goto_1
.end method

.method synthetic constructor <init>(Lauz$a;Lauz$1;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lauz;-><init>(Lauz$a;)V

    return-void
.end method


# virtual methods
.method public a()Lcbi;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcbi",
            "<TMODE",
            "L;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lauz;->a:Lcom/twitter/database/hydrator/c;

    iget-object v1, p0, Lauz;->b:Lcom/twitter/database/model/l;

    iget-object v2, p0, Lauz;->d:Lcom/twitter/database/model/f;

    iget-object v3, p0, Lauz;->c:Ljava/lang/Class;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/database/hydrator/c;->a(Lcom/twitter/database/model/l;Lcom/twitter/database/model/f;Ljava/lang/Class;)Lcbi;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcbi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<TMODE",
            "L;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lauz;->h:Z

    .line 77
    invoke-virtual {p0}, Lauz;->isReset()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    if-eqz p1, :cond_0

    .line 80
    invoke-static {p1}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 94
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    iget-object v0, p0, Lauz;->f:Lcbi;

    .line 85
    iput-object p1, p0, Lauz;->f:Lcbi;

    .line 87
    invoke-virtual {p0}, Lauz;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 88
    invoke-super {p0, p1}, Landroid/support/v4/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    .line 91
    :cond_2
    if-eqz v0, :cond_0

    if-eq v0, p1, :cond_0

    invoke-virtual {v0}, Lcbi;->h()Z

    move-result v1

    if-nez v1, :cond_0

    .line 92
    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    goto :goto_0
.end method

.method public b(Lcbi;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<TMODE",
            "L;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 152
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcbi;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    invoke-static {p1}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 155
    :cond_0
    return-void
.end method

.method public cancelLoadInBackground()V
    .locals 0

    .prologue
    .line 71
    return-void
.end method

.method public synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lcbi;

    invoke-virtual {p0, p1}, Lauz;->a(Lcbi;)V

    return-void
.end method

.method public synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lauz;->a()Lcbi;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelLoad()Z
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    iput-boolean v0, p0, Lauz;->h:Z

    .line 113
    invoke-super {p0}, Landroid/support/v4/content/AsyncTaskLoader;->onCancelLoad()Z

    move-result v0

    return v0
.end method

.method public synthetic onCanceled(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lcbi;

    invoke-virtual {p0, p1}, Lauz;->b(Lcbi;)V

    return-void
.end method

.method protected onForceLoad()V
    .locals 1

    .prologue
    .line 106
    invoke-super {p0}, Landroid/support/v4/content/AsyncTaskLoader;->onForceLoad()V

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lauz;->h:Z

    .line 108
    return-void
.end method

.method protected onReset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 159
    invoke-super {p0}, Landroid/support/v4/content/AsyncTaskLoader;->onReset()V

    .line 162
    invoke-virtual {p0}, Lauz;->onStopLoading()V

    .line 164
    iget-object v0, p0, Lauz;->f:Lcbi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lauz;->f:Lcbi;

    invoke-virtual {v0}, Lcbi;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    iget-object v0, p0, Lauz;->f:Lcbi;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 167
    :cond_0
    iput-object v1, p0, Lauz;->f:Lcbi;

    .line 169
    iget-object v0, p0, Lauz;->g:Lrx/j;

    if-eqz v0, :cond_1

    .line 170
    iget-object v0, p0, Lauz;->g:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 171
    iput-object v1, p0, Lauz;->g:Lrx/j;

    .line 174
    :cond_1
    iget-object v0, p0, Lauz;->e:Landroid/database/ContentObserver;

    if-eqz v0, :cond_2

    .line 175
    invoke-virtual {p0}, Lauz;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lauz;->e:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 177
    :cond_2
    return-void
.end method

.method protected onStartLoading()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lauz;->f:Lcbi;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lauz;->f:Lcbi;

    invoke-virtual {p0, v0}, Lauz;->a(Lcbi;)V

    .line 128
    :cond_0
    iget-object v0, p0, Lauz;->g:Lrx/j;

    if-nez v0, :cond_1

    .line 129
    iget-object v0, p0, Lauz;->b:Lcom/twitter/database/model/l;

    invoke-interface {v0}, Lcom/twitter/database/model/l;->d()Lrx/c;

    move-result-object v0

    new-instance v1, Lauz$1;

    invoke-direct {v1, p0}, Lauz$1;-><init>(Lauz;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lauz;->g:Lrx/j;

    .line 136
    :cond_1
    invoke-virtual {p0}, Lauz;->takeContentChanged()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lauz;->f:Lcbi;

    if-nez v0, :cond_3

    .line 137
    :cond_2
    invoke-virtual {p0}, Lauz;->forceLoad()V

    .line 139
    :cond_3
    return-void
.end method

.method protected onStopLoading()V
    .locals 0

    .prologue
    .line 147
    invoke-virtual {p0}, Lauz;->cancelLoad()Z

    .line 148
    return-void
.end method
