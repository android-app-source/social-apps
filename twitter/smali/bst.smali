.class public Lbst;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcpj;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/p;

.field private final c:Lcom/twitter/library/client/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/p;Lcom/twitter/library/client/v;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbst;->a:Landroid/content/Context;

    .line 27
    iput-object p2, p0, Lbst;->b:Lcom/twitter/library/client/p;

    .line 28
    iput-object p3, p0, Lbst;->c:Lcom/twitter/library/client/v;

    .line 29
    return-void
.end method


# virtual methods
.method public a(Lcnz;Lcpk;)V
    .locals 5

    .prologue
    .line 33
    invoke-virtual {p0, p2}, Lbst;->a(Lcpk;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Event must be a PromotedLog, is a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 35
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_0
    check-cast p2, Lbsq;

    .line 39
    new-instance v0, Lcom/twitter/library/service/v;

    iget-object v1, p0, Lbst;->c:Lcom/twitter/library/client/v;

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    .line 40
    iget-object v1, p0, Lbst;->b:Lcom/twitter/library/client/p;

    new-instance v2, Lbel;

    iget-object v3, p0, Lbst;->a:Landroid/content/Context;

    iget-object v4, p2, Lbsq;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-direct {v2, v3, v0, v4}, Lbel;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/library/api/PromotedEvent;)V

    iget-object v0, p2, Lbsq;->b:Ljava/lang/String;

    .line 42
    invoke-virtual {v2, v0}, Lbel;->b(Ljava/lang/String;)Lbel;

    move-result-object v0

    iget-boolean v2, p2, Lbsq;->e:Z

    .line 43
    invoke-virtual {v0, v2}, Lbel;->a(Z)Lbel;

    move-result-object v0

    iget-wide v2, p2, Lbsq;->c:J

    .line 44
    invoke-virtual {v0, v2, v3}, Lbel;->a(J)Lbel;

    move-result-object v0

    iget-object v2, p2, Lbsq;->d:Ljava/lang/String;

    .line 45
    invoke-virtual {v0, v2}, Lbel;->a(Ljava/lang/String;)Lbel;

    move-result-object v0

    iget-object v2, p2, Lbsq;->f:Ljava/lang/String;

    .line 46
    invoke-virtual {v0, v2}, Lbel;->c(Ljava/lang/String;)Lbel;

    move-result-object v0

    iget-object v2, p2, Lbsq;->g:Ljava/lang/String;

    .line 47
    invoke-virtual {v0, v2}, Lbel;->d(Ljava/lang/String;)Lbel;

    move-result-object v0

    iget-object v2, p2, Lbsq;->h:Ljava/lang/String;

    .line 48
    invoke-virtual {v0, v2}, Lbel;->e(Ljava/lang/String;)Lbel;

    move-result-object v0

    iget-object v2, p2, Lbsq;->i:Ljava/lang/String;

    .line 49
    invoke-virtual {v0, v2}, Lbel;->f(Ljava/lang/String;)Lbel;

    move-result-object v0

    iget-object v2, p2, Lbsq;->j:Ljava/lang/String;

    .line 50
    invoke-virtual {v0, v2}, Lbel;->g(Ljava/lang/String;)Lbel;

    move-result-object v0

    iget-object v2, p2, Lbsq;->k:Ljava/lang/String;

    .line 51
    invoke-virtual {v0, v2}, Lbel;->h(Ljava/lang/String;)Lbel;

    move-result-object v0

    iget-object v2, p2, Lbsq;->l:Ljava/lang/String;

    .line 52
    invoke-virtual {v0, v2}, Lbel;->i(Ljava/lang/String;)Lbel;

    move-result-object v0

    iget-object v2, p2, Lbsq;->m:Ljava/lang/String;

    .line 53
    invoke-virtual {v0, v2}, Lbel;->j(Ljava/lang/String;)Lbel;

    move-result-object v0

    iget-wide v2, p2, Lbsq;->o:J

    .line 54
    invoke-virtual {v0, v2, v3}, Lbel;->b(J)Lbel;

    move-result-object v0

    iget-object v2, p2, Lbsq;->n:Ljava/lang/String;

    .line 55
    invoke-virtual {v0, v2}, Lbel;->k(Ljava/lang/String;)Lbel;

    move-result-object v0

    .line 40
    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 56
    return-void
.end method

.method public a(Lcpk;)Z
    .locals 1

    .prologue
    .line 60
    instance-of v0, p1, Lbsq;

    return v0
.end method
