.class Lcaq$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcaq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcaq;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcaq$1;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcaq$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcaq;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 40
    new-instance v1, Lcaq;

    invoke-direct {v1}, Lcaq;-><init>()V

    .line 41
    new-instance v2, Ljava/util/HashMap;

    sget-object v0, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v3, Lcom/twitter/util/serialization/f;->k:Lcom/twitter/util/serialization/l;

    .line 42
    invoke-static {p1, v0, v3}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Ljava/util/Map;

    move-result-object v0

    .line 41
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-direct {v2, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v2, v1, Lcaq;->b:Ljava/util/Map;

    .line 44
    return-object v1
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcaq;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p2, Lcaq;->b:Ljava/util/Map;

    sget-object v1, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v2, Lcom/twitter/util/serialization/f;->k:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/Map;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)V

    .line 34
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    check-cast p2, Lcaq;

    invoke-virtual {p0, p1, p2}, Lcaq$a;->a(Lcom/twitter/util/serialization/o;Lcaq;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcaq$a;->a(Lcom/twitter/util/serialization/n;I)Lcaq;

    move-result-object v0

    return-object v0
.end method
