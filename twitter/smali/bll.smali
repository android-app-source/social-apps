.class public Lbll;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lblk;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/commerce/model/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/commerce/model/g;)V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lbll;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 23
    iput-object p3, p0, Lbll;->a:Lcom/twitter/library/commerce/model/g;

    .line 24
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 4

    .prologue
    .line 29
    const-string/jumbo v0, "contact_emails"

    .line 30
    invoke-virtual {p0}, Lbll;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 31
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "commerce"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "contact_emails"

    aput-object v3, v1, v2

    .line 32
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->b([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "contact_email"

    iget-object v2, p0, Lbll;->a:Lcom/twitter/library/commerce/model/g;

    .line 33
    invoke-virtual {v2}, Lcom/twitter/library/commerce/model/g;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 34
    iget-object v1, p0, Lbll;->p:Landroid/content/Context;

    invoke-static {v0, v1}, Lblx;->a(Lcom/twitter/library/service/d$a;Landroid/content/Context;)V

    .line 35
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lblk;)V
    .locals 3

    .prologue
    .line 46
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {p3}, Lblk;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 48
    iget-object v1, p2, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "create_contact_email_bundle"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 52
    :goto_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/u;->a(Lcom/twitter/network/HttpOperation;)V

    .line 53
    return-void

    .line 50
    :cond_0
    invoke-static {p2, p3}, Lblx;->a(Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 16
    check-cast p3, Lblk;

    invoke-virtual {p0, p1, p2, p3}, Lbll;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lblk;)V

    return-void
.end method

.method protected b()Lblk;
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lblk;

    invoke-direct {v0}, Lblk;-><init>()V

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lbll;->b()Lblk;

    move-result-object v0

    return-object v0
.end method
