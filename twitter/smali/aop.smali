.class public abstract Laop;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laop$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:[Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method constructor <init>(Laop$a;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iget-object v0, p1, Laop$a;->a:Ljava/lang/String;

    iput-object v0, p0, Laop;->a:Ljava/lang/String;

    .line 23
    iget-object v0, p1, Laop$a;->b:[Ljava/lang/String;

    iput-object v0, p0, Laop;->b:[Ljava/lang/String;

    .line 24
    iget-object v0, p1, Laop$a;->c:Ljava/lang/String;

    iput-object v0, p0, Laop;->c:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 29
    if-ne p0, p1, :cond_1

    .line 35
    :cond_0
    :goto_0
    return v0

    .line 31
    :cond_1
    instance-of v2, p1, Lapb;

    if-nez v2, :cond_2

    move v0, v1

    .line 32
    goto :goto_0

    .line 34
    :cond_2
    check-cast p1, Lapb;

    .line 35
    iget-object v2, p0, Laop;->a:Ljava/lang/String;

    iget-object v3, p1, Lapb;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Laop;->b:[Ljava/lang/String;

    iget-object v3, p1, Lapb;->b:[Ljava/lang/String;

    .line 36
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Laop;->c:Ljava/lang/String;

    iget-object v3, p1, Lapb;->c:Ljava/lang/String;

    .line 37
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 43
    iget-object v0, p0, Laop;->a:Ljava/lang/String;

    iget-object v1, p0, Laop;->b:[Ljava/lang/String;

    iget-object v2, p0, Laop;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
