.class public final Lcfw$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcfw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcfw;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcfv;

.field private b:Lcfv;

.field private c:Lcfv;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcfs;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 48
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcfw$a;->d:Ljava/util/List;

    .line 47
    return-void
.end method

.method static synthetic a(Lcfw$a;)Lcfv;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcfw$a;->a:Lcfv;

    return-object v0
.end method

.method static synthetic b(Lcfw$a;)Lcfv;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcfw$a;->b:Lcfv;

    return-object v0
.end method

.method static synthetic c(Lcfw$a;)Lcfv;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcfw$a;->c:Lcfv;

    return-object v0
.end method

.method static synthetic d(Lcfw$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcfw$a;->d:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcfw$a;->a:Lcfv;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcfv;)Lcfw$a;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcfw$a;->a:Lcfv;

    .line 53
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcfw$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcfs;",
            ">;)",
            "Lcfw$a;"
        }
    .end annotation

    .prologue
    .line 70
    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Lcfw$a;->d:Ljava/util/List;

    .line 71
    return-object p0

    .line 70
    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object p1

    goto :goto_0
.end method

.method public b(Lcfv;)Lcfw$a;
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcfw$a;->b:Lcfv;

    .line 59
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcfw$a;->e()Lcfw;

    move-result-object v0

    return-object v0
.end method

.method public c(Lcfv;)Lcfw$a;
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcfw$a;->c:Lcfv;

    .line 65
    return-object p0
.end method

.method protected e()Lcfw;
    .locals 2

    .prologue
    .line 82
    new-instance v0, Lcfw;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcfw;-><init>(Lcfw$a;Lcfw$1;)V

    return-object v0
.end method
