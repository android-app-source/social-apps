.class public final Lagn$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lagn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:Lant;

.field private b:Lamu;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lagn$1;)V
    .locals 0

    .prologue
    .line 256
    invoke-direct {p0}, Lagn$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lagn$a;)Lant;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lagn$a;->a:Lant;

    return-object v0
.end method

.method static synthetic b(Lagn$a;)Lamu;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lagn$a;->b:Lamu;

    return-object v0
.end method


# virtual methods
.method public a(Lamu;)Lagn$a;
    .locals 1

    .prologue
    .line 281
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamu;

    iput-object v0, p0, Lagn$a;->b:Lamu;

    .line 282
    return-object p0
.end method

.method public a(Lant;)Lagn$a;
    .locals 1

    .prologue
    .line 276
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lant;

    iput-object v0, p0, Lagn$a;->a:Lant;

    .line 277
    return-object p0
.end method

.method public a()Lago;
    .locals 3

    .prologue
    .line 264
    iget-object v0, p0, Lagn$a;->a:Lant;

    if-nez v0, :cond_0

    .line 265
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lant;

    .line 266
    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 268
    :cond_0
    iget-object v0, p0, Lagn$a;->b:Lamu;

    if-nez v0, :cond_1

    .line 269
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lamu;

    .line 270
    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 272
    :cond_1
    new-instance v0, Lagn;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lagn;-><init>(Lagn$a;Lagn$1;)V

    return-object v0
.end method
