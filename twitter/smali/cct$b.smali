.class public final Lcct$b;
.super Lccl$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcct;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lccl$b",
        "<",
        "Lcct;",
        "Lcct$a;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lccl$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcct$a;
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcct$a;

    invoke-direct {v0}, Lcct$a;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lccl$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 74
    check-cast p2, Lcct$a;

    invoke-virtual {p0, p1, p2, p3}, Lcct$b;->a(Lcom/twitter/util/serialization/n;Lcct$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcct$a;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 87
    invoke-super {p0, p1, p2, p3}, Lccl$b;->a(Lcom/twitter/util/serialization/n;Lccl$a;I)V

    .line 88
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcct$a;->b(Ljava/lang/String;)Lcct$a;

    .line 89
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 74
    check-cast p2, Lcct$a;

    invoke-virtual {p0, p1, p2, p3}, Lcct$b;->a(Lcom/twitter/util/serialization/n;Lcct$a;I)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lccl;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    check-cast p2, Lcct;

    invoke-virtual {p0, p1, p2}, Lcct$b;->a(Lcom/twitter/util/serialization/o;Lcct;)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcct;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    invoke-super {p0, p1, p2}, Lccl$b;->a(Lcom/twitter/util/serialization/o;Lccl;)V

    .line 96
    invoke-virtual {p2}, Lcct;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 97
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    check-cast p2, Lcct;

    invoke-virtual {p0, p1, p2}, Lcct$b;->a(Lcom/twitter/util/serialization/o;Lcct;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lcct$b;->a()Lcct$a;

    move-result-object v0

    return-object v0
.end method
