.class public Labi;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lacg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lacg",
        "<",
        "Lcom/twitter/model/moments/viewmodels/MomentPage;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Labh;

.field private final b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Labh;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Labi;->b:Landroid/content/res/Resources;

    .line 28
    iput-object p2, p0, Labi;->a:Labh;

    .line 29
    return-void
.end method

.method private a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/moments/viewmodels/MomentPage;)V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Labi;->a:Labh;

    iget-object v1, p1, Lcom/twitter/model/moments/Moment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Labh;->a(Ljava/lang/CharSequence;)V

    .line 49
    iget-object v0, p0, Labi;->a:Labh;

    iget-object v1, p1, Lcom/twitter/model/moments/Moment;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Labh;->b(Ljava/lang/CharSequence;)V

    .line 50
    iget-object v0, p1, Lcom/twitter/model/moments/Moment;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 51
    invoke-direct {p0, p3, p2}, Labi;->b(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v0

    .line 52
    iget-object v1, p0, Labi;->a:Labh;

    invoke-virtual {v1, v0}, Labh;->c(Ljava/lang/CharSequence;)V

    .line 54
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/model/moments/Moment;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    iget-object v0, p0, Labi;->a:Labh;

    invoke-virtual {v0}, Labh;->a()V

    .line 59
    :goto_0
    iget-object v0, p0, Labi;->a:Labh;

    iget-boolean v1, p1, Lcom/twitter/model/moments/Moment;->f:Z

    invoke-virtual {v0, v1}, Labh;->a(Z)V

    .line 61
    iget-object v0, p1, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    invoke-static {v0}, Lcom/twitter/model/moments/a;->a(Lcom/twitter/model/moments/a;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 62
    iget-object v0, p1, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    .line 63
    iget-object v1, p0, Labi;->a:Labh;

    invoke-virtual {v1, v0}, Labh;->a(Lcom/twitter/model/moments/a;)V

    .line 67
    :goto_1
    return-void

    .line 57
    :cond_1
    iget-object v0, p0, Labi;->a:Labh;

    iget-object v1, p0, Labi;->b:Landroid/content/res/Resources;

    invoke-static {v1, p1}, Lcom/twitter/android/moments/data/x;->a(Landroid/content/res/Resources;Lcom/twitter/model/moments/Moment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Labh;->d(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 65
    :cond_2
    iget-object v0, p0, Labi;->a:Labh;

    invoke-virtual {v0}, Labh;->d()V

    goto :goto_1
.end method

.method private b(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/core/Tweet;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 88
    if-eqz p1, :cond_0

    .line 89
    sget-object v0, Labi$1;->b:[I

    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 113
    const-string/jumbo v0, ""

    .line 119
    :goto_0
    return-object v0

    .line 91
    :pswitch_0
    check-cast p1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    .line 92
    sget-object v0, Labi$1;->a:[I

    iget-object v1, p1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 102
    const v0, 0x7f0a057c

    .line 119
    :goto_1
    iget-object v1, p0, Labi;->b:Landroid/content/res/Resources;

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Labi;->b:Landroid/content/res/Resources;

    const v4, 0x7f0a0b92

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p2, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 94
    :pswitch_1
    const v0, 0x7f0a0579

    .line 95
    goto :goto_1

    .line 98
    :pswitch_2
    const v0, 0x7f0a057d

    .line 99
    goto :goto_1

    .line 109
    :pswitch_3
    const v0, 0x7f0a057a

    .line 110
    goto :goto_1

    .line 117
    :cond_0
    const v0, 0x7f0a057b

    goto :goto_1

    .line 89
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 92
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/core/Tweet;)V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Labi;->a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    .line 42
    iget-object v0, p0, Labi;->a:Labh;

    invoke-virtual {v0}, Labh;->h()V

    .line 43
    iget-object v0, p0, Labi;->a:Labh;

    invoke-virtual {v0}, Labh;->f()V

    .line 44
    return-void
.end method

.method public a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/core/Tweet;)V
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->f()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/Moment;

    .line 34
    invoke-direct {p0, v0, p2, p1}, Labi;->a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    .line 36
    iget-object v0, p0, Labi;->a:Labh;

    invoke-virtual {v0}, Labh;->g()V

    .line 37
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Labi;->a:Labh;

    invoke-virtual {v0}, Labh;->i()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b()Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Lacg",
            "<",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 72
    invoke-static {p0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method
