.class final Laqx$c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Larm;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Laqx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Laqx;

.field private final b:Lant;

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/base/BaseFragmentActivity;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/support/v4/app/FragmentManager;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/d;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/Flow$Step;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laqw;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/model/onboarding/TaskContext;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laqs;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/onboarding/Subtask;",
            "Landroid/content/Intent;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcje;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcjg;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcjd;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/k;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lank;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/l;",
            ">;>;"
        }
    .end annotation
.end field

.field private s:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/l;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/i;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/b;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lart;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Laqx;Lant;)V
    .locals 1

    .prologue
    .line 413
    iput-object p1, p0, Laqx$c;->a:Laqx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 414
    invoke-static {p2}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lant;

    iput-object v0, p0, Laqx$c;->b:Lant;

    .line 415
    invoke-direct {p0}, Laqx$c;->c()V

    .line 416
    return-void
.end method

.method synthetic constructor <init>(Laqx;Lant;Laqx$1;)V
    .locals 0

    .prologue
    .line 365
    invoke-direct {p0, p1, p2}, Laqx$c;-><init>(Laqx;Lant;)V

    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 421
    iget-object v0, p0, Laqx$c;->b:Lant;

    .line 423
    invoke-static {v0}, Lanu;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 422
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->c:Lcta;

    .line 425
    iget-object v0, p0, Laqx$c;->c:Lcta;

    .line 427
    invoke-static {v0}, Lanx;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 426
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->d:Lcta;

    .line 429
    iget-object v0, p0, Laqx$c;->c:Lcta;

    .line 431
    invoke-static {v0}, Lanv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 430
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->e:Lcta;

    .line 434
    iget-object v0, p0, Laqx$c;->e:Lcta;

    .line 436
    invoke-static {v0}, Laob;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 435
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->f:Lcta;

    .line 439
    iget-object v0, p0, Laqx$c;->c:Lcta;

    iget-object v1, p0, Laqx$c;->d:Lcta;

    iget-object v2, p0, Laqx$c;->f:Lcta;

    .line 441
    invoke-static {v0, v1, v2}, Lcom/twitter/app/onboarding/flowstep/common/e;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 440
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->g:Lcta;

    .line 447
    invoke-static {}, Larl;->c()Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->h:Lcta;

    .line 449
    iget-object v0, p0, Laqx$c;->h:Lcta;

    iget-object v1, p0, Laqx$c;->f:Lcta;

    .line 451
    invoke-static {v0, v1}, Larh;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 450
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->i:Lcta;

    .line 454
    iget-object v0, p0, Laqx$c;->c:Lcta;

    .line 456
    invoke-static {v0}, Laqv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 455
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->j:Lcta;

    .line 459
    iget-object v0, p0, Laqx$c;->a:Laqx;

    .line 461
    invoke-static {v0}, Laqx;->a(Laqx;)Lcta;

    move-result-object v0

    .line 460
    invoke-static {v0}, Laqt;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->k:Lcta;

    .line 463
    iget-object v0, p0, Laqx$c;->k:Lcta;

    .line 464
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->l:Lcta;

    .line 466
    iget-object v0, p0, Laqx$c;->l:Lcta;

    .line 468
    invoke-static {v0}, Lcjf;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 467
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->m:Lcta;

    .line 470
    iget-object v0, p0, Laqx$c;->j:Lcta;

    iget-object v1, p0, Laqx$c;->m:Lcta;

    iget-object v2, p0, Laqx$c;->a:Laqx;

    .line 475
    invoke-static {v2}, Laqx;->b(Laqx;)Lcta;

    move-result-object v2

    .line 476
    invoke-static {}, Lcjc;->c()Ldagger/internal/c;

    move-result-object v3

    .line 472
    invoke-static {v0, v1, v2, v3}, Lcjh;->a(Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 471
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->n:Lcta;

    .line 478
    iget-object v0, p0, Laqx$c;->n:Lcta;

    .line 479
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->o:Lcta;

    .line 481
    iget-object v0, p0, Laqx$c;->g:Lcta;

    .line 483
    invoke-static {v0}, Lari;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 482
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->p:Lcta;

    .line 486
    iget-object v0, p0, Laqx$c;->b:Lant;

    .line 488
    invoke-static {v0}, Laoa;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 487
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->q:Lcta;

    .line 490
    iget-object v0, p0, Laqx$c;->q:Lcta;

    .line 492
    invoke-static {v0}, Larj;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 491
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->r:Lcta;

    .line 495
    iget-object v0, p0, Laqx$c;->r:Lcta;

    .line 497
    invoke-static {v0}, Lcom/twitter/app/onboarding/flowstep/common/m;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 496
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->s:Lcta;

    .line 500
    iget-object v0, p0, Laqx$c;->p:Lcta;

    iget-object v1, p0, Laqx$c;->s:Lcta;

    .line 502
    invoke-static {v0, v1}, Lcom/twitter/app/onboarding/flowstep/common/j;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 501
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->t:Lcta;

    .line 505
    iget-object v0, p0, Laqx$c;->g:Lcta;

    iget-object v1, p0, Laqx$c;->i:Lcta;

    iget-object v2, p0, Laqx$c;->o:Lcta;

    iget-object v3, p0, Laqx$c;->t:Lcta;

    .line 507
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/app/onboarding/flowstep/common/c;->a(Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 506
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->u:Lcta;

    .line 516
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Laqx$c;->u:Lcta;

    iget-object v2, p0, Laqx$c;->s:Lcta;

    iget-object v3, p0, Laqx$c;->t:Lcta;

    .line 515
    invoke-static {v0, v1, v2, v3}, Laru;->a(Lcsd;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 514
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->v:Lcta;

    .line 521
    iget-object v0, p0, Laqx$c;->v:Lcta;

    .line 522
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$c;->w:Lcta;

    .line 523
    return-void
.end method


# virtual methods
.method public a()Laog;
    .locals 1

    .prologue
    .line 532
    iget-object v0, p0, Laqx$c;->w:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laog;

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 527
    invoke-static {}, Ldagger/internal/f;->a()Ldagger/internal/c;

    move-result-object v0

    invoke-interface {v0}, Ldagger/internal/c;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method
