.class public Lbsf;
.super Lbrx;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbrx",
        "<",
        "Lcfn;",
        "Laxx$a;",
        "Laxx$b;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/twitter/util/serialization/l;Lcom/twitter/database/model/l;Lcom/twitter/database/model/m;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcfn;",
            ">;",
            "Lcom/twitter/database/model/l",
            "<",
            "Laxx$a;",
            ">;",
            "Lcom/twitter/database/model/m",
            "<",
            "Laxx$b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lbrx;-><init>(Lcom/twitter/util/serialization/l;Lcom/twitter/database/model/l;Lcom/twitter/database/model/m;)V

    .line 32
    return-void
.end method

.method public static a(Lcom/twitter/library/provider/t;)Lbsf;
    .locals 5

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v1

    .line 23
    new-instance v2, Lbsf;

    sget-object v3, Lcfn;->a:Lcom/twitter/util/serialization/l;

    const-class v0, Laxx;

    .line 24
    invoke-interface {v1, v0}, Lcom/twitter/database/model/i;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Laxx;

    invoke-interface {v0}, Laxx;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    const-class v4, Laxx$c;

    .line 25
    invoke-interface {v1, v4}, Lcom/twitter/database/model/i;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v1

    invoke-direct {v2, v3, v0, v1}, Lbsf;-><init>(Lcom/twitter/util/serialization/l;Lcom/twitter/database/model/l;Lcom/twitter/database/model/m;)V

    .line 23
    return-object v2
.end method
