.class Lcdd$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcdd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcdd;",
        "Lcdd$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcdd$1;)V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Lcdd$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcdd$a;
    .locals 1

    .prologue
    .line 142
    new-instance v0, Lcdd$a;

    invoke-direct {v0}, Lcdd$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcdd$a;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 148
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v2

    .line 149
    invoke-static {v2}, Lcom/twitter/util/collection/i;->a(I)Lcom/twitter/util/collection/i;

    move-result-object v3

    .line 150
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 151
    sget-object v0, Lcdc;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdc;

    .line 152
    if-eqz v0, :cond_0

    .line 153
    iget-object v4, v0, Lcdc;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    .line 150
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 157
    :cond_1
    invoke-virtual {v3}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {p2, v0}, Lcdd$a;->a(Ljava/util/Map;)Lcdd$a;

    move-result-object v0

    .line 158
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcdd$a;->a(Ljava/lang/String;)Lcdd$a;

    move-result-object v0

    .line 159
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcdd$a;->b(Ljava/lang/String;)Lcdd$a;

    move-result-object v0

    .line 160
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcdd$a;->a(J)Lcdd$a;

    .line 161
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 121
    check-cast p2, Lcdd$a;

    invoke-virtual {p0, p1, p2, p3}, Lcdd$b;->a(Lcom/twitter/util/serialization/n;Lcdd$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcdd;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    iget-object v0, p2, Lcdd;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    .line 126
    iget-object v0, p2, Lcdd;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 127
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcdc;

    .line 128
    sget-object v3, Lcdc;->a:Lcom/twitter/util/serialization/l;

    if-eqz v1, :cond_0

    :goto_1
    invoke-virtual {v3, p1, v1}, Lcom/twitter/util/serialization/l;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance v1, Lcdc;

    .line 131
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v1, v0, v4}, Lcdc;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 134
    :cond_1
    iget-object v0, p2, Lcdd;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 135
    iget-object v0, p2, Lcdd;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 136
    iget-wide v0, p2, Lcdd;->b:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    .line 137
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    check-cast p2, Lcdd;

    invoke-virtual {p0, p1, p2}, Lcdd$b;->a(Lcom/twitter/util/serialization/o;Lcdd;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0}, Lcdd$b;->a()Lcdd$a;

    move-result-object v0

    return-object v0
.end method
