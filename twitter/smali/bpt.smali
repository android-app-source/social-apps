.class public Lbpt;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a()Z
    .locals 1

    .prologue
    .line 9
    const-string/jumbo v0, "photo_stickers_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static a(Z)Z
    .locals 1

    .prologue
    .line 13
    invoke-static {}, Lbpt;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p0, :cond_0

    invoke-static {}, Lcom/twitter/library/dm/d;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 17
    invoke-static {}, Lbpt;->a()Z

    move-result v0

    return v0
.end method

.method public static c()Z
    .locals 1

    .prologue
    .line 21
    invoke-static {}, Lbpt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "photo_stickers_timeline_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d()Z
    .locals 1

    .prologue
    .line 25
    invoke-static {}, Lbpt;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbpt;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e()Z
    .locals 1

    .prologue
    .line 29
    invoke-static {}, Lbpt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbpt;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
