.class public abstract Lcbx;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcbx$b;,
        Lcbx$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcbx;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 26
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/twitter/util/serialization/j;

    const/4 v1, 0x0

    const-class v2, Lccf;

    new-instance v3, Lccf$b;

    invoke-direct {v3}, Lccf$b;-><init>()V

    .line 28
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcci;

    new-instance v3, Lcci$b;

    invoke-direct {v3}, Lcci$b;-><init>()V

    .line 30
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lccc;

    new-instance v3, Lccc$b;

    invoke-direct {v3}, Lccc$b;-><init>()V

    .line 31
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lcca;

    new-instance v3, Lcca$b;

    invoke-direct {v3}, Lcca$b;-><init>()V

    .line 33
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Lccj;

    new-instance v3, Lccj$b;

    invoke-direct {v3}, Lccj$b;-><init>()V

    .line 35
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-class v2, Lccg;

    new-instance v3, Lccg$b;

    invoke-direct {v3}, Lccg$b;-><init>()V

    .line 37
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-class v2, Lcch;

    new-instance v3, Lcch$b;

    invoke-direct {v3}, Lcch$b;-><init>()V

    .line 39
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-class v2, Lcce;

    new-instance v3, Lcce$b;

    invoke-direct {v3}, Lcce$b;-><init>()V

    .line 41
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-class v2, Lccd;

    new-instance v3, Lccd$b;

    invoke-direct {v3}, Lccd$b;-><init>()V

    .line 43
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    .line 27
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a([Lcom/twitter/util/serialization/j;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    sput-object v0, Lcbx;->a:Lcom/twitter/util/serialization/l;

    .line 26
    return-void
.end method

.method protected constructor <init>(Lcbx$a;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iget-object v0, p1, Lcbx$a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcbx;->b:Ljava/lang/String;

    .line 55
    iget-object v0, p1, Lcbx$a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcbx;->c:Ljava/lang/String;

    .line 56
    iget-object v0, p1, Lcbx$a;->c:Ljava/lang/String;

    iput-object v0, p0, Lcbx;->d:Ljava/lang/String;

    .line 57
    iget v0, p1, Lcbx$a;->d:I

    iput v0, p0, Lcbx;->e:I

    .line 58
    iget v0, p1, Lcbx$a;->e:I

    iput v0, p0, Lcbx;->f:I

    .line 59
    return-void
.end method

.method private a(Lcbx;)Z
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcbx;->b:Ljava/lang/String;

    iget-object v1, p1, Lcbx;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcbx;->c:Ljava/lang/String;

    iget-object v1, p1, Lcbx;->c:Ljava/lang/String;

    .line 122
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcbx;->d:Ljava/lang/String;

    iget-object v1, p1, Lcbx;->d:Ljava/lang/String;

    .line 123
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcbx;->e:I

    iget v1, p1, Lcbx;->e:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcbx;->f:I

    iget v1, p1, Lcbx;->f:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 121
    :goto_0
    return v0

    .line 123
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a()I
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcbx;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcbx;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcbx;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcbx;->e:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 117
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcbx;

    if-eqz v0, :cond_1

    check-cast p1, Lcbx;

    invoke-direct {p0, p1}, Lcbx;->a(Lcbx;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcbx;->f:I

    return v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 93
    invoke-virtual {p0}, Lcbx;->e()I

    move-result v0

    invoke-virtual {p0}, Lcbx;->f()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Lcbx;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {p0}, Lcbx;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    invoke-virtual {p0}, Lcbx;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 97
    :goto_0
    return v0

    .line 99
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 130
    iget-object v0, p0, Lcbx;->b:Ljava/lang/String;

    iget-object v1, p0, Lcbx;->c:Ljava/lang/String;

    iget-object v2, p0, Lcbx;->d:Ljava/lang/String;

    iget v3, p0, Lcbx;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget v4, p0, Lcbx;->f:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public i()Lcom/twitter/model/core/ad;
    .locals 2

    .prologue
    .line 104
    invoke-virtual {p0}, Lcbx;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    new-instance v0, Lcom/twitter/model/core/ad$c;

    invoke-direct {v0}, Lcom/twitter/model/core/ad$c;-><init>()V

    .line 106
    invoke-virtual {p0}, Lcbx;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/ad$c;->e(Ljava/lang/String;)Lcom/twitter/model/core/ad$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad$c;

    .line 107
    invoke-virtual {p0}, Lcbx;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/ad$c;->f(Ljava/lang/String;)Lcom/twitter/model/core/ad$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad$c;

    .line 108
    invoke-virtual {p0}, Lcbx;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/ad$c;->g(Ljava/lang/String;)Lcom/twitter/model/core/ad$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad$c;

    .line 109
    invoke-virtual {v0}, Lcom/twitter/model/core/ad$c;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad;

    .line 111
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
