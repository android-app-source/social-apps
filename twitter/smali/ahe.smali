.class public Lahe;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/twitter/library/client/Session;

.field private final c:Laht;

.field private final d:Lcom/twitter/library/client/p;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/library/client/Session;Laht;)V
    .locals 1

    .prologue
    .line 29
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lahe;-><init>(Landroid/app/Activity;Lcom/twitter/library/client/Session;Laht;Lcom/twitter/library/client/p;)V

    .line 30
    return-void
.end method

.method constructor <init>(Landroid/app/Activity;Lcom/twitter/library/client/Session;Laht;Lcom/twitter/library/client/p;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lahe;->a:Landroid/app/Activity;

    .line 36
    iput-object p2, p0, Lahe;->b:Lcom/twitter/library/client/Session;

    .line 37
    iput-object p3, p0, Lahe;->c:Laht;

    .line 38
    iput-object p4, p0, Lahe;->d:Lcom/twitter/library/client/p;

    .line 39
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;JLcom/twitter/library/service/t;)V
    .locals 8

    .prologue
    .line 42
    iget-object v6, p0, Lahe;->d:Lcom/twitter/library/client/p;

    new-instance v0, Lcom/twitter/library/api/search/b;

    iget-object v1, p0, Lahe;->a:Landroid/app/Activity;

    iget-object v2, p0, Lahe;->b:Lcom/twitter/library/client/Session;

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/search/b;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;J)V

    const/4 v1, 0x1

    .line 43
    invoke-virtual {v0, v1}, Lcom/twitter/library/api/search/b;->g(I)Lcom/twitter/library/service/s;

    move-result-object v0

    .line 42
    invoke-virtual {v6, v0, p4}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 44
    iget-object v0, p0, Lahe;->c:Laht;

    invoke-virtual {v0}, Laht;->b()V

    .line 45
    return-void
.end method
