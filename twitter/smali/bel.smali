.class public Lbel;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/api/PromotedEvent;

.field private b:J

.field private c:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/PromotedEvent;)V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/twitter/library/service/v;

    invoke-direct {v0, p2}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    invoke-direct {p0, p1, v0, p3}, Lbel;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/library/api/PromotedEvent;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/library/api/PromotedEvent;)V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lbel;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 49
    iput-object p3, p0, Lbel;->a:Lcom/twitter/library/api/PromotedEvent;

    .line 50
    return-void
.end method

.method private static a(Lbel;)Ljava/lang/String;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 276
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 278
    const-string/jumbo v1, "event="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbel;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    iget-object v1, p0, Lbel;->g:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 280
    const-string/jumbo v1, "impression_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbel;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    :cond_0
    iget-wide v2, p0, Lbel;->b:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 283
    const-string/jumbo v1, "promoted_trend_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lbel;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    :cond_1
    iget-boolean v1, p0, Lbel;->h:Z

    if-eqz v1, :cond_2

    .line 286
    const-string/jumbo v1, "earned="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lbel;->h:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 288
    :cond_2
    iget-object v1, p0, Lbel;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 289
    const-string/jumbo v1, "url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbel;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    :cond_3
    iget-object v1, p0, Lbel;->j:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 292
    const-string/jumbo v1, "playlist_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbel;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    :cond_4
    iget-object v1, p0, Lbel;->k:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 295
    const-string/jumbo v1, "video_uuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbel;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    :cond_5
    iget-object v1, p0, Lbel;->l:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 298
    const-string/jumbo v1, "video_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbel;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    :cond_6
    iget-object v1, p0, Lbel;->m:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 301
    const-string/jumbo v1, "cta_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbel;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    :cond_7
    iget-object v1, p0, Lbel;->r:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 304
    const-string/jumbo v1, "play_store_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbel;->r:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    :cond_8
    iget-object v1, p0, Lbel;->s:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 307
    const-string/jumbo v1, "play_store_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbel;->s:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    :cond_9
    iget-object v1, p0, Lbel;->t:Ljava/lang/String;

    invoke-static {v1}, Lbel;->n(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 310
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 311
    const-string/jumbo v2, "card_event="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 313
    :cond_a
    iget-object v1, p0, Lbel;->u:Ljava/lang/String;

    invoke-static {v1}, Lbel;->n(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 314
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 315
    const-string/jumbo v2, "engagement_metadata="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    :cond_b
    iget-object v1, p0, Lbel;->v:Ljava/lang/String;

    invoke-static {v1}, Lbel;->n(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 318
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 319
    const-string/jumbo v2, "adSlotDetails="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    :cond_c
    iget-wide v2, p0, Lbel;->w:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_d

    .line 322
    const-string/jumbo v1, "timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lbel;->w:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 325
    :cond_d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b()Z
    .locals 2

    .prologue
    .line 271
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    .line 272
    invoke-virtual {v0}, Lcof;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcof;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static n(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 329
    invoke-static {p0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 335
    :goto_0
    return-object v0

    .line 332
    :catch_0
    move-exception v0

    .line 335
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(J)Lbel;
    .locals 1

    .prologue
    .line 154
    iput-wide p1, p0, Lbel;->b:J

    .line 155
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lbel;
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lbel;->c:Ljava/lang/String;

    .line 147
    return-object p0
.end method

.method public a(Z)Lbel;
    .locals 0

    .prologue
    .line 170
    iput-boolean p1, p0, Lbel;->h:Z

    .line 171
    return-object p0
.end method

.method protected a()Lcom/twitter/library/service/d;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    .line 59
    invoke-virtual {p0}, Lbel;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 60
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "promoted_content"

    aput-object v3, v1, v2

    const-string/jumbo v2, "log"

    aput-object v2, v1, v4

    .line 61
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lbel;->g:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 63
    const-string/jumbo v1, "impression_id"

    iget-object v2, p0, Lbel;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 65
    :cond_0
    iget-wide v2, p0, Lbel;->b:J

    cmp-long v1, v2, v6

    if-lez v1, :cond_1

    .line 66
    const-string/jumbo v1, "promoted_trend_id"

    iget-wide v2, p0, Lbel;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    .line 68
    :cond_1
    iget-boolean v1, p0, Lbel;->h:Z

    if-eqz v1, :cond_2

    .line 69
    const-string/jumbo v1, "earned"

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 71
    :cond_2
    const-string/jumbo v1, "event"

    iget-object v2, p0, Lbel;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v2}, Lcom/twitter/library/api/PromotedEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 72
    iget-object v1, p0, Lbel;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 73
    const-string/jumbo v1, "url"

    iget-object v2, p0, Lbel;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 76
    :cond_3
    iget-object v1, p0, Lbel;->j:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 77
    const-string/jumbo v1, "playlist_url"

    iget-object v2, p0, Lbel;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 80
    :cond_4
    iget-object v1, p0, Lbel;->k:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 81
    const-string/jumbo v1, "video_uuid"

    iget-object v2, p0, Lbel;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 84
    :cond_5
    iget-object v1, p0, Lbel;->l:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 85
    const-string/jumbo v1, "video_type"

    iget-object v2, p0, Lbel;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 88
    :cond_6
    iget-object v1, p0, Lbel;->m:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 89
    const-string/jumbo v1, "cta_url"

    iget-object v2, p0, Lbel;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 92
    :cond_7
    iget-object v1, p0, Lbel;->r:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 93
    const-string/jumbo v1, "play_store_id"

    iget-object v2, p0, Lbel;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 96
    :cond_8
    iget-object v1, p0, Lbel;->s:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 97
    const-string/jumbo v1, "play_store_name"

    iget-object v2, p0, Lbel;->s:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 100
    :cond_9
    iget-object v1, p0, Lbel;->t:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 101
    const-string/jumbo v1, "card_event"

    iget-object v2, p0, Lbel;->t:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 104
    :cond_a
    iget-object v1, p0, Lbel;->u:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 105
    const-string/jumbo v1, "engagement_metadata"

    iget-object v2, p0, Lbel;->u:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 108
    :cond_b
    iget-object v1, p0, Lbel;->v:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 109
    const-string/jumbo v1, "slot_details"

    iget-object v2, p0, Lbel;->v:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 112
    :cond_c
    iget-wide v2, p0, Lbel;->w:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_d

    .line 113
    const-string/jumbo v1, "epoch_ms"

    iget-wide v2, p0, Lbel;->w:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    .line 116
    :cond_d
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 20

    .prologue
    .line 122
    invoke-virtual/range {p0 .. p0}, Lbel;->R()Lcom/twitter/library/provider/t;

    move-result-object v2

    .line 123
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v3

    if-nez v3, :cond_2

    .line 124
    move-object/from16 v0, p0

    iget-object v3, v0, Lbel;->a:Lcom/twitter/library/api/PromotedEvent;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbel;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v5, v0, Lbel;->b:J

    move-object/from16 v0, p0

    iget-object v7, v0, Lbel;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lbel;->h:Z

    move-object/from16 v0, p0

    iget-object v9, v0, Lbel;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lbel;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lbel;->l:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lbel;->m:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lbel;->r:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lbel;->s:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lbel;->t:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lbel;->u:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbel;->v:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lbel;->w:J

    move-wide/from16 v18, v0

    invoke-virtual/range {v2 .. v19}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/library/api/PromotedEvent;Ljava/lang/String;JLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 132
    :cond_0
    :goto_0
    invoke-static {}, Lbel;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 133
    const-string/jumbo v2, "PromotedLog"

    invoke-static/range {p0 .. p0}, Lbel;->a(Lbel;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcqi;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :cond_1
    return-void

    .line 127
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lbel;->i:Z

    if-eqz v3, :cond_0

    .line 128
    move-object/from16 v0, p0

    iget-object v3, v0, Lbel;->a:Lcom/twitter/library/api/PromotedEvent;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbel;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v5, v0, Lbel;->b:J

    move-object/from16 v0, p0

    iget-object v7, v0, Lbel;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lbel;->h:Z

    move-object/from16 v0, p0

    iget-object v9, v0, Lbel;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lbel;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lbel;->l:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lbel;->m:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lbel;->r:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lbel;->s:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lbel;->t:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lbel;->u:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbel;->v:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lbel;->w:J

    move-wide/from16 v18, v0

    invoke-virtual/range {v2 .. v19}, Lcom/twitter/library/provider/t;->b(Lcom/twitter/library/api/PromotedEvent;Ljava/lang/String;JLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public b(J)Lbel;
    .locals 1

    .prologue
    .line 248
    iput-wide p1, p0, Lbel;->w:J

    .line 249
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lbel;
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lbel;->g:Ljava/lang/String;

    .line 163
    return-object p0
.end method

.method public b(Z)Lbel;
    .locals 0

    .prologue
    .line 179
    iput-boolean p1, p0, Lbel;->i:Z

    .line 180
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lbel;
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lbel;->j:Ljava/lang/String;

    .line 188
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lbel;
    .locals 0

    .prologue
    .line 195
    iput-object p1, p0, Lbel;->k:Ljava/lang/String;

    .line 196
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lbel;
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, Lbel;->l:Ljava/lang/String;

    .line 204
    return-object p0
.end method

.method public f(Ljava/lang/String;)Lbel;
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lbel;->m:Ljava/lang/String;

    .line 212
    return-object p0
.end method

.method protected f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    return-object v0
.end method

.method public g(Ljava/lang/String;)Lbel;
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lbel;->r:Ljava/lang/String;

    .line 220
    return-object p0
.end method

.method public h(Ljava/lang/String;)Lbel;
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lbel;->s:Ljava/lang/String;

    .line 228
    return-object p0
.end method

.method public i(Ljava/lang/String;)Lbel;
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Lbel;->t:Ljava/lang/String;

    .line 236
    return-object p0
.end method

.method public j(Ljava/lang/String;)Lbel;
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lbel;->u:Ljava/lang/String;

    .line 244
    return-object p0
.end method

.method public k(Ljava/lang/String;)Lbel;
    .locals 0

    .prologue
    .line 253
    iput-object p1, p0, Lbel;->v:Ljava/lang/String;

    .line 254
    return-object p0
.end method
