.class public abstract Lbqg;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lbqg;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a()Lbqg;
    .locals 2

    .prologue
    .line 18
    const-class v1, Lbqg;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lbqg;->a:Lbqg;

    if-eqz v0, :cond_0

    sget-object v0, Lbqg;->a:Lbqg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v1

    return-object v0

    .line 19
    :cond_0
    :try_start_1
    invoke-static {}, Lbms;->ah()Lbms;

    move-result-object v0

    invoke-virtual {v0}, Lbms;->aa()Lbqg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public abstract a(Lcom/twitter/library/client/Session;Z)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract a(Lcom/twitter/library/client/Session;)Z
.end method

.method public abstract b()Z
.end method

.method public abstract b(Lcom/twitter/library/client/Session;)Z
.end method

.method public abstract b(Lcom/twitter/library/client/Session;Z)Z
.end method

.method public abstract c()Z
.end method

.method public abstract c(Lcom/twitter/library/client/Session;)Z
.end method

.method public abstract d()Z
.end method

.method public abstract e()Z
.end method

.method public abstract f()Z
.end method

.method public abstract g()Z
.end method
