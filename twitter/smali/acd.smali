.class public Lacd;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lacg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lacg",
        "<",
        "Lcom/twitter/model/moments/viewmodels/q;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Lcom/twitter/android/moments/ui/fullscreen/bp;

.field private final c:Lbxo;

.field private final d:Lcom/twitter/android/moments/ui/maker/an;

.field private final e:Lacc;

.field private final f:Lrx/subjects/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/a",
            "<",
            "Lacg",
            "<",
            "Lcom/twitter/model/moments/viewmodels/q;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:Lrx/subjects/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/a",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/cc;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/twitter/android/moments/ui/fullscreen/as;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lacc;Lcom/twitter/android/moments/ui/fullscreen/bp;Lbxo;Lcom/twitter/android/moments/ui/maker/an;Lcom/twitter/android/moments/ui/fullscreen/as;)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {}, Lrx/subjects/a;->r()Lrx/subjects/a;

    move-result-object v0

    iput-object v0, p0, Lacd;->f:Lrx/subjects/a;

    .line 42
    invoke-static {}, Lrx/subjects/a;->r()Lrx/subjects/a;

    move-result-object v0

    iput-object v0, p0, Lacd;->g:Lrx/subjects/a;

    .line 63
    iput-object p1, p0, Lacd;->a:Landroid/content/res/Resources;

    .line 64
    iput-object p3, p0, Lacd;->b:Lcom/twitter/android/moments/ui/fullscreen/bp;

    .line 65
    iput-object p4, p0, Lacd;->c:Lbxo;

    .line 66
    iput-object p2, p0, Lacd;->e:Lacc;

    .line 67
    iput-object p5, p0, Lacd;->d:Lcom/twitter/android/moments/ui/maker/an;

    .line 68
    iput-object p6, p0, Lacd;->h:Lcom/twitter/android/moments/ui/fullscreen/as;

    .line 69
    iget-object v0, p0, Lacd;->h:Lcom/twitter/android/moments/ui/fullscreen/as;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/as;->a()Lrx/g;

    move-result-object v0

    iget-object v1, p0, Lacd;->g:Lrx/subjects/a;

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/d;)Lrx/j;

    .line 70
    return-void
.end method

.method public static a(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/content/res/Resources;Lcom/twitter/android/moments/ui/fullscreen/as;Lbxo;)Lacd;
    .locals 7

    .prologue
    .line 50
    invoke-static {p1}, Lacc;->a(Landroid/view/LayoutInflater;)Lacc;

    move-result-object v2

    .line 51
    new-instance v0, Lacd;

    .line 52
    invoke-static {p0}, Lcom/twitter/android/moments/ui/fullscreen/bp;->a(Landroid/content/Context;)Lcom/twitter/android/moments/ui/fullscreen/bp;

    move-result-object v3

    new-instance v5, Lcom/twitter/android/moments/ui/maker/an;

    invoke-direct {v5}, Lcom/twitter/android/moments/ui/maker/an;-><init>()V

    move-object v1, p2

    move-object v4, p4

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lacd;-><init>(Landroid/content/res/Resources;Lacc;Lcom/twitter/android/moments/ui/fullscreen/bp;Lbxo;Lcom/twitter/android/moments/ui/maker/an;Lcom/twitter/android/moments/ui/fullscreen/as;)V

    .line 51
    return-object v0
.end method

.method static synthetic a(Lacd;)Lrx/subjects/a;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lacd;->f:Lrx/subjects/a;

    return-object v0
.end method

.method static synthetic b(Lacd;)Lacc;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lacd;->e:Lacc;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/ui/fullscreen/cs;Lcom/twitter/model/core/Tweet;)V
    .locals 2

    .prologue
    .line 100
    if-eqz p2, :cond_0

    .line 101
    iget-object v0, p0, Lacd;->e:Lacc;

    new-instance v1, Lacd$2;

    invoke-direct {v1, p0, p1, p2}, Lacd$2;-><init>(Lacd;Lcom/twitter/android/moments/ui/fullscreen/cs;Lcom/twitter/model/core/Tweet;)V

    invoke-virtual {v0, v1}, Lacc;->a(Landroid/view/View$OnClickListener;)V

    .line 110
    :goto_0
    return-void

    .line 108
    :cond_0
    iget-object v0, p0, Lacd;->e:Lacc;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lacc;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/moments/ui/fullscreen/m;)V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lacd;->e:Lacc;

    new-instance v1, Lacd$3;

    invoke-direct {v1, p0, p1}, Lacd$3;-><init>(Lacd;Lcom/twitter/android/moments/ui/fullscreen/m;)V

    invoke-virtual {v0, v1}, Lacc;->b(Landroid/view/View$OnClickListener;)V

    .line 125
    return-void
.end method

.method public a(Lcom/twitter/model/moments/viewmodels/q;Lcom/twitter/model/core/Tweet;)V
    .locals 7

    .prologue
    .line 74
    iget-object v0, p0, Lacd;->a:Landroid/content/res/Resources;

    invoke-static {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/bf;->a(Landroid/content/res/Resources;Lcom/twitter/model/moments/viewmodels/r;)Lcom/twitter/model/moments/w;

    move-result-object v0

    .line 75
    invoke-static {p2}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    iget-object v1, p0, Lacd;->e:Lacc;

    iget-object v2, p0, Lacd;->b:Lcom/twitter/android/moments/ui/fullscreen/bp;

    iget-object v3, p0, Lacd;->e:Lacc;

    invoke-virtual {v3}, Lacc;->b()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Lcom/twitter/android/moments/ui/fullscreen/bp;->a(Lcom/twitter/model/moments/viewmodels/n;Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lacc;->a(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v1, p0, Lacd;->e:Lacc;

    iget-object v2, p2, Lcom/twitter/model/core/Tweet;->A:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lacc;->b(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v1, p0, Lacd;->e:Lacc;

    iget-object v2, p0, Lacd;->a:Landroid/content/res/Resources;

    const v3, 0x7f0a0b92

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p2, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lacc;->c(Ljava/lang/CharSequence;)V

    .line 80
    iget-object v1, p0, Lacd;->e:Lacc;

    iget-object v2, p2, Lcom/twitter/model/core/Tweet;->r:Ljava/lang/String;

    new-instance v3, Lacd$1;

    invoke-direct {v3, p0}, Lacd$1;-><init>(Lacd;)V

    invoke-virtual {v1, v2, v3}, Lacc;->a(Ljava/lang/String;Lcom/twitter/media/request/a$b;)V

    .line 87
    iget-object v1, p0, Lacd;->e:Lacc;

    invoke-virtual {v1, v0}, Lacc;->a(Lcom/twitter/model/moments/w;)V

    .line 88
    iget-object v0, p0, Lacd;->e:Lacc;

    iget-boolean v1, p2, Lcom/twitter/model/core/Tweet;->L:Z

    invoke-virtual {v0, v1}, Lacc;->a(Z)V

    .line 90
    iget-object v0, p0, Lacd;->h:Lcom/twitter/android/moments/ui/fullscreen/as;

    iget-object v1, p0, Lacd;->e:Lacc;

    invoke-virtual {v1}, Lacc;->a()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p1}, Lcom/twitter/android/moments/ui/fullscreen/as;->a(Landroid/view/ViewGroup;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    .line 91
    iget-object v0, p0, Lacd;->e:Lacc;

    iget-object v1, p0, Lacd;->c:Lbxo;

    invoke-virtual {v1, p2}, Lbxo;->a(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lacc;->a(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/q;->n()Lcom/twitter/model/moments/s;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lacd;->e:Lacc;

    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/q;->n()Lcom/twitter/model/moments/s;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/model/moments/s;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lacc;->b(Ljava/lang/String;)V

    .line 96
    :cond_0
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lacd;->e:Lacc;

    invoke-virtual {v0}, Lacc;->a()Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method public b()Lrx/g;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Lacg",
            "<",
            "Lcom/twitter/model/moments/viewmodels/q;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lacd;->d:Lcom/twitter/android/moments/ui/maker/an;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/an;->a()Lrx/f;

    move-result-object v0

    .line 131
    iget-object v1, p0, Lacd;->f:Lrx/subjects/a;

    invoke-virtual {v1}, Lrx/subjects/a;->b()Lrx/g;

    move-result-object v1

    iget-object v2, p0, Lacd;->g:Lrx/subjects/a;

    invoke-virtual {v2}, Lrx/subjects/a;->b()Lrx/g;

    move-result-object v2

    .line 132
    invoke-static {}, Lcre;->a()Lrx/functions/e;

    move-result-object v3

    .line 131
    invoke-static {v1, v2, v3}, Lrx/g;->a(Lrx/g;Lrx/g;Lrx/functions/e;)Lrx/g;

    move-result-object v1

    .line 133
    invoke-virtual {v1, v0}, Lrx/g;->a(Lrx/f;)Lrx/g;

    move-result-object v0

    .line 131
    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lacd;->h:Lcom/twitter/android/moments/ui/fullscreen/as;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/as;->b()V

    .line 139
    return-void
.end method
