.class public Lbfq;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/twitter/model/timeline/g;

.field private final h:Lcom/twitter/model/timeline/r;

.field private final i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/model/timeline/g;Lcom/twitter/model/timeline/r;ZLjava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/service/v;",
            "Lcom/twitter/model/timeline/g;",
            "Lcom/twitter/model/timeline/r;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    const-class v0, Lbfq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 39
    iput-object p3, p0, Lbfq;->g:Lcom/twitter/model/timeline/g;

    .line 40
    iput-object p4, p0, Lbfq;->h:Lcom/twitter/model/timeline/r;

    .line 41
    iput-boolean p5, p0, Lbfq;->i:Z

    .line 42
    invoke-static {p6}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbfq;->a:Ljava/util/List;

    .line 43
    invoke-static {p7}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbfq;->b:Ljava/util/List;

    .line 44
    invoke-static {p8}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbfq;->c:Ljava/util/List;

    .line 45
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lbfq;->b()Lcom/twitter/library/service/d$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method b()Lcom/twitter/library/service/d$a;
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 55
    invoke-virtual {p0}, Lbfq;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 56
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "timelines"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "feedback"

    aput-object v3, v1, v2

    .line 57
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "feedback_type"

    iget-object v2, p0, Lbfq;->g:Lcom/twitter/model/timeline/g;

    iget-object v2, v2, Lcom/twitter/model/timeline/g;->b:Ljava/lang/String;

    .line 58
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "undo"

    iget-boolean v2, p0, Lbfq;->i:Z

    .line 59
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lbfq;->h:Lcom/twitter/model/timeline/r;

    if-eqz v1, :cond_2

    .line 62
    iget-object v1, p0, Lbfq;->h:Lcom/twitter/model/timeline/r;

    iget-object v1, v1, Lcom/twitter/model/timeline/r;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 63
    const-string/jumbo v1, "injection_type"

    iget-object v2, p0, Lbfq;->h:Lcom/twitter/model/timeline/r;

    iget-object v2, v2, Lcom/twitter/model/timeline/r;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 65
    :cond_0
    iget-object v1, p0, Lbfq;->h:Lcom/twitter/model/timeline/r;

    iget-object v1, v1, Lcom/twitter/model/timeline/r;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 66
    const-string/jumbo v1, "controller_data"

    iget-object v2, p0, Lbfq;->h:Lcom/twitter/model/timeline/r;

    iget-object v2, v2, Lcom/twitter/model/timeline/r;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 68
    :cond_1
    iget-object v1, p0, Lbfq;->h:Lcom/twitter/model/timeline/r;

    iget-object v1, v1, Lcom/twitter/model/timeline/r;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 69
    const-string/jumbo v1, "source_data"

    iget-object v2, p0, Lbfq;->h:Lcom/twitter/model/timeline/r;

    iget-object v2, v2, Lcom/twitter/model/timeline/r;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 73
    :cond_2
    iget-object v1, p0, Lbfq;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 74
    const-string/jumbo v1, "tweet_ids"

    iget-object v2, p0, Lbfq;->a:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/util/Collection;)Lcom/twitter/library/service/d$a;

    .line 76
    :cond_3
    iget-object v1, p0, Lbfq;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 77
    const-string/jumbo v1, "user_ids"

    iget-object v2, p0, Lbfq;->b:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/util/Collection;)Lcom/twitter/library/service/d$a;

    .line 79
    :cond_4
    iget-object v1, p0, Lbfq;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 80
    const-string/jumbo v1, "moment_ids"

    iget-object v2, p0, Lbfq;->c:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/util/Collection;)Lcom/twitter/library/service/d$a;

    .line 83
    :cond_5
    iget-object v1, p0, Lbfq;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lbfq;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lbfq;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 89
    :cond_6
    return-object v0
.end method

.method protected f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return-object v0
.end method
