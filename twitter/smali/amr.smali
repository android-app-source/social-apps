.class public Lamr;
.super Lamg;
.source "Twttr"


# instance fields
.field private final a:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lamg;-><init>()V

    .line 21
    iput-boolean p1, p0, Lamr;->a:Z

    .line 22
    return-void
.end method


# virtual methods
.method protected a()Lanc;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lcom/twitter/android/initialization/a;

    iget-boolean v1, p0, Lamr;->a:Z

    invoke-direct {v0, v1}, Lcom/twitter/android/initialization/a;-><init>(Z)V

    return-object v0
.end method

.method protected a(Landroid/content/Context;Lcqt;)Land;
    .locals 3

    .prologue
    .line 28
    new-instance v0, Land;

    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v1

    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2, p2}, Land;-><init>(Landroid/content/Context;Lrx/f;Lrx/f;Lcqt;)V

    return-object v0
.end method

.method protected b()Land$a;
    .locals 2

    .prologue
    .line 40
    iget-boolean v0, p0, Lamr;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/android/initialization/b;

    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/initialization/b;-><init>(Lrx/f;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
