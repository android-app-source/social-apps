.class public Lafa;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/chat/ak;
.implements Ltv/periscope/android/ui/chat/k;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lafa$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Lafa$a;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lafa$a;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lafa;->a:Landroid/content/res/Resources;

    .line 31
    iput-object p2, p0, Lafa;->b:Lafa$a;

    .line 32
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)Ltv/periscope/android/ui/chat/j;
    .locals 3

    .prologue
    .line 42
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 43
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04031b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 44
    new-instance v1, Ltv/periscope/android/ui/chat/f;

    invoke-direct {v1, v0, p0}, Ltv/periscope/android/ui/chat/f;-><init>(Landroid/view/View;Ltv/periscope/android/ui/chat/k;)V

    return-object v1
.end method

.method public a()V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method public a(Ltv/periscope/android/ui/chat/j;Ltv/periscope/android/ui/chat/h;)V
    .locals 3

    .prologue
    .line 49
    check-cast p1, Ltv/periscope/android/ui/chat/f;

    .line 50
    iput-object p2, p1, Ltv/periscope/android/ui/chat/f;->c:Ltv/periscope/android/ui/chat/h;

    .line 51
    iget-object v0, p1, Ltv/periscope/android/ui/chat/f;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lafa;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a099c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ltv/periscope/android/util/ah;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    iget-object v0, p1, Ltv/periscope/android/ui/chat/f;->b:Landroid/widget/ImageView;

    iget-object v1, p0, Lafa;->a:Landroid/content/res/Resources;

    const v2, 0x7f0207d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 53
    return-void
.end method

.method public a(Ltv/periscope/android/ui/chat/k;)V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public a(Ltv/periscope/model/chat/Message;)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lafa;->b:Lafa$a;

    invoke-interface {v0}, Lafa$a;->d()V

    .line 58
    return-void
.end method

.method public b(Ltv/periscope/model/chat/Message;)V
    .locals 0

    .prologue
    .line 66
    return-void
.end method
