.class public Lbwi;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(I)Z
    .locals 1

    .prologue
    .line 19
    and-int/lit8 v0, p0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcar;)Z
    .locals 1

    .prologue
    .line 29
    const-string/jumbo v0, "realtime_ad_impression_id"

    invoke-virtual {p0, v0}, Lcar;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static a(Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 15
    if-eqz p0, :cond_0

    iget v0, p0, Lcom/twitter/model/core/Tweet;->d:I

    invoke-static {v0}, Lbwi;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/twitter/model/core/Tweet;)Z
    .locals 2

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    .line 24
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcax;->D()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "realtime_ad_impression_id"

    .line 25
    invoke-virtual {v0, v1}, Lcax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 24
    :goto_0
    return v0

    .line 25
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
