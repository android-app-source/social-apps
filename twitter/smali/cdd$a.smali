.class public final Lcdd$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcdd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcdd;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcdc;",
            ">;"
        }
    .end annotation
.end field

.field protected b:Ljava/lang/String;

.field protected c:Ljava/lang/String;

.field protected d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcdc$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 174
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcdd$a;->e:J

    .line 177
    return-void
.end method

.method public constructor <init>(Lcdd;)V
    .locals 2

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 174
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcdd$a;->e:J

    .line 180
    iget-object v0, p1, Lcdd;->c:Ljava/util/Map;

    iput-object v0, p0, Lcdd$a;->a:Ljava/util/Map;

    .line 181
    iget-object v0, p1, Lcdd;->d:Ljava/lang/String;

    iput-object v0, p0, Lcdd$a;->b:Ljava/lang/String;

    .line 182
    iget-object v0, p1, Lcdd;->e:Ljava/lang/String;

    iput-object v0, p0, Lcdd$a;->c:Ljava/lang/String;

    .line 183
    return-void
.end method

.method static synthetic a(Lcdd$a;)J
    .locals 2

    .prologue
    .line 164
    iget-wide v0, p0, Lcdd$a;->e:J

    return-wide v0
.end method


# virtual methods
.method public a(J)Lcdd$a;
    .locals 1

    .prologue
    .line 211
    iput-wide p1, p0, Lcdd$a;->e:J

    .line 212
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcdd$a;
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcdd$a;->b:Ljava/lang/String;

    .line 194
    return-object p0
.end method

.method public a(Ljava/util/Map;)Lcdd$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcdc;",
            ">;)",
            "Lcdd$a;"
        }
    .end annotation

    .prologue
    .line 187
    iput-object p1, p0, Lcdd$a;->a:Ljava/util/Map;

    .line 188
    return-object p0
.end method

.method public a(Ljava/util/Set;)Lcdd$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcdc$a;",
            ">;)",
            "Lcdd$a;"
        }
    .end annotation

    .prologue
    .line 205
    iput-object p1, p0, Lcdd$a;->d:Ljava/util/Set;

    .line 206
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcdd$a;
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lcdd$a;->c:Ljava/lang/String;

    .line 200
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0}, Lcdd$a;->e()Lcdd;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcdd;
    .locals 2

    .prologue
    .line 218
    new-instance v0, Lcdd;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcdd;-><init>(Lcdd$a;Lcdd$1;)V

    return-object v0
.end method
