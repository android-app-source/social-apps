.class Laiy$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Laiy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation


# instance fields
.field private final a:[Ljava/lang/String;

.field private final b:Laiv;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/widget/CheckBox;

.field private final e:Ljava/lang/String;


# direct methods
.method constructor <init>([Ljava/lang/String;Laiv;Ljava/util/Map;Landroid/widget/CheckBox;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Laiv;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/widget/CheckBox;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 332
    iput-object p1, p0, Laiy$b;->a:[Ljava/lang/String;

    .line 333
    iput-object p2, p0, Laiy$b;->b:Laiv;

    .line 334
    iput-object p3, p0, Laiy$b;->c:Ljava/util/Map;

    .line 335
    iput-object p4, p0, Laiy$b;->d:Landroid/widget/CheckBox;

    .line 336
    iput-object p5, p0, Laiy$b;->e:Ljava/lang/String;

    .line 337
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 342
    iget-object v0, p0, Laiy$b;->c:Ljava/util/Map;

    iget-object v1, p0, Laiy$b;->a:[Ljava/lang/String;

    aget-object v1, v1, p2

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 343
    iget-object v1, p0, Laiy$b;->b:Laiv;

    iget-object v2, p0, Laiy$b;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Laiv;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 344
    iget-object v1, p0, Laiy$b;->d:Landroid/widget/CheckBox;

    const-string/jumbo v2, "off"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 345
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 346
    return-void

    .line 344
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
