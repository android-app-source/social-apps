.class public final Lcax$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcax;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcax;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcaw;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 553
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 554
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcax$a;->a:Ljava/lang/String;

    .line 555
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcax$a;->b:Ljava/lang/String;

    .line 556
    invoke-static {}, Lcom/twitter/util/collection/i;->f()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcax$a;->c:Ljava/util/Map;

    .line 557
    invoke-static {}, Lcom/twitter/util/collection/i;->f()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcax$a;->d:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcax$a;
    .locals 0

    .prologue
    .line 563
    iput-object p1, p0, Lcax$a;->a:Ljava/lang/String;

    .line 564
    return-object p0
.end method

.method public a(Ljava/util/Map;)Lcax$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcaw;",
            ">;)",
            "Lcax$a;"
        }
    .end annotation

    .prologue
    .line 575
    iput-object p1, p0, Lcax$a;->c:Ljava/util/Map;

    .line 576
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcax$a;
    .locals 0

    .prologue
    .line 569
    iput-object p1, p0, Lcax$a;->b:Ljava/lang/String;

    .line 570
    return-object p0
.end method

.method public b(Ljava/util/Map;)Lcax$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)",
            "Lcax$a;"
        }
    .end annotation

    .prologue
    .line 581
    iput-object p1, p0, Lcax$a;->d:Ljava/util/Map;

    .line 582
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 553
    invoke-virtual {p0}, Lcax$a;->e()Lcax;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcax$a;
    .locals 0

    .prologue
    .line 587
    iput-object p1, p0, Lcax$a;->e:Ljava/lang/String;

    .line 588
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcax$a;
    .locals 0

    .prologue
    .line 593
    iput-object p1, p0, Lcax$a;->f:Ljava/lang/String;

    .line 594
    return-object p0
.end method

.method protected e()Lcax;
    .locals 8

    .prologue
    .line 600
    new-instance v0, Lcax;

    iget-object v1, p0, Lcax$a;->a:Ljava/lang/String;

    iget-object v2, p0, Lcax$a;->b:Ljava/lang/String;

    iget-object v3, p0, Lcax$a;->d:Ljava/util/Map;

    iget-object v4, p0, Lcax$a;->e:Ljava/lang/String;

    iget-object v5, p0, Lcax$a;->f:Ljava/lang/String;

    iget-object v6, p0, Lcax$a;->c:Ljava/util/Map;

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcax;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcax$1;)V

    return-object v0
.end method
