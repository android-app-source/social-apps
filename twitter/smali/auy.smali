.class public Lauy;
.super Lcbi;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        ">",
        "Lcbi",
        "<TP;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/database/model/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/model/g",
            "<TP;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/database/model/g;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/database/model/g",
            "<TP;>;)V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0}, Lcbi;-><init>()V

    .line 16
    iput-object p1, p0, Lauy;->a:Lcom/twitter/database/model/g;

    .line 17
    return-void
.end method


# virtual methods
.method public a(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TP;"
        }
    .end annotation

    .prologue
    .line 27
    invoke-virtual {p0}, Lauy;->be_()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lauy;->a:Lcom/twitter/database/model/g;

    if-nez v0, :cond_1

    .line 28
    :cond_0
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 31
    :cond_1
    iget-object v0, p0, Lauy;->a:Lcom/twitter/database/model/g;

    invoke-virtual {v0, p1}, Lcom/twitter/database/model/g;->a(I)Z

    .line 32
    iget-object v0, p0, Lauy;->a:Lcom/twitter/database/model/g;

    iget-object v0, v0, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    goto :goto_0
.end method

.method public b()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lauy;->a:Lcom/twitter/database/model/g;

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lauy;->a:Lcom/twitter/database/model/g;

    invoke-virtual {v0}, Lcom/twitter/database/model/g;->close()V

    .line 40
    :cond_0
    return-void
.end method

.method public be_()I
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lauy;->a:Lcom/twitter/database/model/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lauy;->a:Lcom/twitter/database/model/g;

    invoke-virtual {v0}, Lcom/twitter/database/model/g;->a()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Lcom/twitter/database/model/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/database/model/g",
            "<TP;>;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lauy;->a:Lcom/twitter/database/model/g;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 54
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lauy;

    if-eqz v0, :cond_1

    check-cast p1, Lauy;

    .line 55
    invoke-virtual {p1}, Lauy;->c()Lcom/twitter/database/model/g;

    move-result-object v0

    invoke-virtual {p0}, Lauy;->c()Lcom/twitter/database/model/g;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 54
    :goto_0
    return v0

    .line 55
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lauy;->a:Lcom/twitter/database/model/g;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lauy;->a:Lcom/twitter/database/model/g;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method
