.class public final Lcha$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcha;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcha;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z

.field private c:Lcom/twitter/model/timeline/j;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcha$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcha$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcha$a;)Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lcha$a;->b:Z

    return v0
.end method

.method static synthetic c(Lcha$a;)Lcom/twitter/model/timeline/j;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcha$a;->c:Lcom/twitter/model/timeline/j;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 123
    invoke-super {p0}, Lcom/twitter/util/object/i;->R_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcha$a;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/timeline/j;)Lcha$a;
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcha$a;->c:Lcom/twitter/model/timeline/j;

    .line 118
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcha$a;
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcha$a;->a:Ljava/lang/String;

    .line 106
    return-object p0
.end method

.method public a(Z)Lcha$a;
    .locals 0

    .prologue
    .line 111
    iput-boolean p1, p0, Lcha$a;->b:Z

    .line 112
    return-object p0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0}, Lcha$a;->e()Lcha;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcha;
    .locals 2

    .prologue
    .line 129
    new-instance v0, Lcha;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcha;-><init>(Lcha$a;Lcha$1;)V

    return-object v0
.end method
