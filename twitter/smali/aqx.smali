.class public final Laqx;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laqy;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laqx$d;,
        Laqx$c;,
        Laqx$a;,
        Laqx$b;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lara;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcjj;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const-class v0, Laqx;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Laqx;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Laqx$b;)V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    sget-boolean v0, Laqx;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 91
    :cond_0
    invoke-direct {p0, p1}, Laqx;->a(Laqx$b;)V

    .line 92
    return-void
.end method

.method synthetic constructor <init>(Laqx$b;Laqx$1;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Laqx;-><init>(Laqx$b;)V

    return-void
.end method

.method public static a()Laqx$b;
    .locals 2

    .prologue
    .line 95
    new-instance v0, Laqx$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Laqx$b;-><init>(Laqx$1;)V

    return-object v0
.end method

.method static synthetic a(Laqx;)Lcta;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Laqx;->d:Lcta;

    return-object v0
.end method

.method private a(Laqx$b;)V
    .locals 1

    .prologue
    .line 101
    new-instance v0, Laqx$1;

    invoke-direct {v0, p0, p1}, Laqx$1;-><init>(Laqx;Laqx$b;)V

    iput-object v0, p0, Laqx;->b:Lcta;

    .line 114
    iget-object v0, p0, Laqx;->b:Lcta;

    .line 116
    invoke-static {v0}, Lcom/twitter/app/common/abs/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 115
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx;->c:Lcta;

    .line 118
    new-instance v0, Laqx$2;

    invoke-direct {v0, p0, p1}, Laqx$2;-><init>(Laqx;Laqx$b;)V

    iput-object v0, p0, Laqx;->d:Lcta;

    .line 131
    invoke-static {}, Larb;->c()Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx;->e:Lcta;

    .line 133
    iget-object v0, p0, Laqx;->e:Lcta;

    .line 134
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx;->f:Lcta;

    .line 135
    return-void
.end method

.method static synthetic b(Laqx;)Lcta;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Laqx;->f:Lcta;

    return-object v0
.end method


# virtual methods
.method public a(Lant;)Lare;
    .locals 2

    .prologue
    .line 150
    new-instance v0, Laqx$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Laqx$a;-><init>(Laqx;Lant;Laqx$1;)V

    return-object v0
.end method

.method public b(Lant;)Larm;
    .locals 2

    .prologue
    .line 156
    new-instance v0, Laqx$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Laqx$c;-><init>(Laqx;Lant;Laqx$1;)V

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    invoke-static {}, Ldagger/internal/f;->a()Ldagger/internal/c;

    move-result-object v0

    invoke-interface {v0}, Ldagger/internal/c;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c(Lant;)Larp;
    .locals 2

    .prologue
    .line 162
    new-instance v0, Laqx$d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Laqx$d;-><init>(Laqx;Lant;Laqx$1;)V

    return-object v0
.end method

.method public c()Lcom/twitter/app/common/abs/j;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Laqx;->c:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    return-object v0
.end method
