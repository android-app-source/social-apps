.class public Lbeb;
.super Lbea;
.source "Twttr"


# instance fields
.field private h:Lcfz;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 39
    const-string/jumbo v2, "CheckPushDevice"

    new-instance v3, Lcom/twitter/library/service/v;

    invoke-direct {v3, p2}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lbea;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    iput p5, p0, Lbeb;->c:I

    .line 41
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 4

    .prologue
    .line 51
    invoke-virtual {p0}, Lbeb;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "push_destinations"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "device"

    aput-object v3, v1, v2

    .line 52
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 53
    invoke-virtual {p0, v0}, Lbeb;->a(Lcom/twitter/library/service/d$a;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(I)V
    .locals 6

    .prologue
    .line 86
    new-instance v0, Lbei;

    iget-object v1, p0, Lbeb;->p:Landroid/content/Context;

    invoke-virtual {p0}, Lbeb;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-object v3, p0, Lbeb;->a:Ljava/lang/String;

    iget-object v5, p0, Lbeb;->b:Ljava/lang/String;

    move v4, p1

    invoke-direct/range {v0 .. v5}, Lbei;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Ljava/lang/String;ILjava/lang/String;)V

    .line 87
    invoke-virtual {v0}, Lbei;->O()Lcom/twitter/library/service/u;

    .line 88
    return-void
.end method

.method protected a(J)V
    .locals 9

    .prologue
    const-wide/32 v6, 0x36ee80

    .line 91
    const-string/jumbo v0, "android_push_settings_check_in_success_interval_hours"

    const-wide/16 v2, 0x18

    .line 92
    invoke-static {v0, v2, v3}, Lcoj;->a(Ljava/lang/String;J)J

    move-result-wide v0

    mul-long/2addr v0, v6

    .line 94
    const-string/jumbo v2, "android_push_settings_check_in_failure_interval_hours"

    const-wide/16 v4, 0x6

    .line 95
    invoke-static {v2, v4, v5}, Lcoj;->a(Ljava/lang/String;J)J

    move-result-wide v2

    mul-long/2addr v2, v6

    .line 97
    iget-object v4, p0, Lbeb;->p:Landroid/content/Context;

    invoke-static {v4, p1, p2}, Lcom/twitter/library/platform/notifications/t;->a(Landroid/content/Context;J)Lcom/twitter/library/platform/notifications/t;

    move-result-object v4

    .line 98
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v6

    sub-long v0, v6, v0

    add-long/2addr v0, v2

    invoke-virtual {v4, v0, v1}, Lcom/twitter/library/platform/notifications/t;->a(J)V

    .line 99
    return-void
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcfz;",
            "Lcfy;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-super {p0, p1, p2, p3}, Lbea;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    .line 66
    invoke-virtual {p0}, Lbeb;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/v;

    iget-wide v2, v0, Lcom/twitter/library/service/v;->c:J

    .line 67
    invoke-virtual {p2}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    invoke-virtual {p0, p3}, Lbeb;->a(Lcom/twitter/library/api/i;)Lcfz;

    move-result-object v0

    iput-object v0, p0, Lbeb;->h:Lcfz;

    .line 69
    iget-object v0, p0, Lbeb;->h:Lcfz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbeb;->h:Lcfz;

    iget-boolean v0, v0, Lcfz;->a:Z

    if-eqz v0, :cond_1

    .line 70
    invoke-static {v2, v3}, Lbuz;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lbeb;->h:Lcfz;

    iget v0, v0, Lcfz;->b:I

    invoke-virtual {p0, v0}, Lbeb;->a(I)V

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    new-instance v0, Ljava/lang/Exception;

    const-string/jumbo v2, "Push API endpoint did not return the current push settings."

    invoke-direct {v0, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 75
    iput v1, p0, Lbeb;->g:I

    goto :goto_0

    .line 78
    :cond_2
    invoke-virtual {p0, p3}, Lbeb;->b(Lcom/twitter/library/api/i;)Lcfy;

    move-result-object v0

    .line 79
    if-eqz v0, :cond_3

    iget v0, v0, Lcfy;->b:I

    :goto_1
    iput v0, p0, Lbeb;->g:I

    .line 80
    invoke-virtual {p0, v2, v3}, Lbeb;->a(J)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 79
    goto :goto_1
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 34
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbeb;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    const-string/jumbo v0, "app:twitter_service:gcm_registration:checkin_request"

    return-object v0
.end method
