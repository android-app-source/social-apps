.class public Ldh;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ldh$a;
    }
.end annotation


# instance fields
.field private final a:Lcg;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:Laz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laz",
            "<",
            "Lcy;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcn;

.field private final d:Landroid/content/Context;

.field private final e:Z

.field private final f:Laz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laz",
            "<",
            "Lcy;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ldf;

.field private final h:Lcv;

.field private final i:Lcom/facebook/imagepipeline/decoder/a;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final j:Laz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laz",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/facebook/cache/disk/b;

.field private final l:Lcom/facebook/common/memory/b;

.field private final m:Lcom/facebook/imagepipeline/producers/ac;

.field private final n:Lcom/facebook/imagepipeline/bitmaps/e;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final o:Lcom/facebook/imagepipeline/memory/s;

.field private final p:Lcom/facebook/imagepipeline/decoder/b;

.field private final q:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ldx;",
            ">;"
        }
    .end annotation
.end field

.field private final r:Z

.field private final s:Lcom/facebook/cache/disk/b;


# direct methods
.method private constructor <init>(Ldh$a;)V
    .locals 3

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    invoke-static {p1}, Ldh$a;->a(Ldh$a;)Lcg;

    move-result-object v0

    iput-object v0, p0, Ldh;->a:Lcg;

    .line 89
    invoke-static {p1}, Ldh$a;->b(Ldh$a;)Laz;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v1, Lcq;

    invoke-static {p1}, Ldh$a;->c(Ldh$a;)Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-direct {v1, v0}, Lcq;-><init>(Landroid/app/ActivityManager;)V

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Ldh;->b:Laz;

    .line 94
    invoke-static {p1}, Ldh$a;->d(Ldh$a;)Lcn;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lcr;->a()Lcr;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Ldh;->c:Lcn;

    .line 98
    invoke-static {p1}, Ldh$a;->c(Ldh$a;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lax;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Ldh;->d:Landroid/content/Context;

    .line 99
    invoke-static {p1}, Ldh$a;->e(Ldh$a;)Z

    move-result v0

    iput-boolean v0, p0, Ldh;->e:Z

    .line 100
    invoke-static {p1}, Ldh$a;->f(Ldh$a;)Laz;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lcs;

    invoke-direct {v0}, Lcs;-><init>()V

    :goto_2
    iput-object v0, p0, Ldh;->f:Laz;

    .line 104
    invoke-static {p1}, Ldh$a;->g(Ldh$a;)Lcv;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-static {}, Ldb;->l()Ldb;

    move-result-object v0

    :goto_3
    iput-object v0, p0, Ldh;->h:Lcv;

    .line 108
    invoke-static {p1}, Ldh$a;->h(Ldh$a;)Lcom/facebook/imagepipeline/decoder/a;

    move-result-object v0

    iput-object v0, p0, Ldh;->i:Lcom/facebook/imagepipeline/decoder/a;

    .line 109
    invoke-static {p1}, Ldh$a;->i(Ldh$a;)Laz;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, Ldh$1;

    invoke-direct {v0, p0}, Ldh$1;-><init>(Ldh;)V

    :goto_4
    iput-object v0, p0, Ldh;->j:Laz;

    .line 118
    invoke-static {p1}, Ldh$a;->j(Ldh$a;)Lcom/facebook/cache/disk/b;

    move-result-object v0

    if-nez v0, :cond_5

    invoke-static {p1}, Ldh$a;->c(Ldh$a;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ldh;->b(Landroid/content/Context;)Lcom/facebook/cache/disk/b;

    move-result-object v0

    :goto_5
    iput-object v0, p0, Ldh;->k:Lcom/facebook/cache/disk/b;

    .line 122
    invoke-static {p1}, Ldh$a;->k(Ldh$a;)Lcom/facebook/common/memory/b;

    move-result-object v0

    if-nez v0, :cond_6

    invoke-static {}, Lcom/facebook/common/memory/c;->a()Lcom/facebook/common/memory/c;

    move-result-object v0

    :goto_6
    iput-object v0, p0, Ldh;->l:Lcom/facebook/common/memory/b;

    .line 126
    invoke-static {p1}, Ldh$a;->l(Ldh$a;)Lcom/facebook/imagepipeline/producers/ac;

    move-result-object v0

    if-nez v0, :cond_7

    new-instance v0, Lcom/facebook/imagepipeline/producers/s;

    invoke-direct {v0}, Lcom/facebook/imagepipeline/producers/s;-><init>()V

    :goto_7
    iput-object v0, p0, Ldh;->m:Lcom/facebook/imagepipeline/producers/ac;

    .line 130
    invoke-static {p1}, Ldh$a;->m(Ldh$a;)Lcom/facebook/imagepipeline/bitmaps/e;

    move-result-object v0

    iput-object v0, p0, Ldh;->n:Lcom/facebook/imagepipeline/bitmaps/e;

    .line 131
    invoke-static {p1}, Ldh$a;->n(Ldh$a;)Lcom/facebook/imagepipeline/memory/s;

    move-result-object v0

    if-nez v0, :cond_8

    new-instance v0, Lcom/facebook/imagepipeline/memory/s;

    invoke-static {}, Lcom/facebook/imagepipeline/memory/r;->i()Lcom/facebook/imagepipeline/memory/r$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/imagepipeline/memory/r$a;->a()Lcom/facebook/imagepipeline/memory/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/imagepipeline/memory/s;-><init>(Lcom/facebook/imagepipeline/memory/r;)V

    :goto_8
    iput-object v0, p0, Ldh;->o:Lcom/facebook/imagepipeline/memory/s;

    .line 135
    invoke-static {p1}, Ldh$a;->o(Ldh$a;)Lcom/facebook/imagepipeline/decoder/b;

    move-result-object v0

    if-nez v0, :cond_9

    new-instance v0, Lcom/facebook/imagepipeline/decoder/d;

    invoke-direct {v0}, Lcom/facebook/imagepipeline/decoder/d;-><init>()V

    :goto_9
    iput-object v0, p0, Ldh;->p:Lcom/facebook/imagepipeline/decoder/b;

    .line 139
    invoke-static {p1}, Ldh$a;->p(Ldh$a;)Ljava/util/Set;

    move-result-object v0

    if-nez v0, :cond_a

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    :goto_a
    iput-object v0, p0, Ldh;->q:Ljava/util/Set;

    .line 143
    invoke-static {p1}, Ldh$a;->q(Ldh$a;)Z

    move-result v0

    iput-boolean v0, p0, Ldh;->r:Z

    .line 144
    invoke-static {p1}, Ldh$a;->r(Ldh$a;)Lcom/facebook/cache/disk/b;

    move-result-object v0

    if-nez v0, :cond_b

    iget-object v0, p0, Ldh;->k:Lcom/facebook/cache/disk/b;

    :goto_b
    iput-object v0, p0, Ldh;->s:Lcom/facebook/cache/disk/b;

    .line 151
    iget-object v0, p0, Ldh;->o:Lcom/facebook/imagepipeline/memory/s;

    invoke-virtual {v0}, Lcom/facebook/imagepipeline/memory/s;->c()I

    move-result v1

    .line 152
    invoke-static {p1}, Ldh$a;->s(Ldh$a;)Ldf;

    move-result-object v0

    if-nez v0, :cond_c

    new-instance v0, Lde;

    invoke-direct {v0, v1}, Lde;-><init>(I)V

    :goto_c
    iput-object v0, p0, Ldh;->g:Ldf;

    .line 155
    return-void

    .line 89
    :cond_0
    invoke-static {p1}, Ldh$a;->b(Ldh$a;)Laz;

    move-result-object v0

    goto/16 :goto_0

    .line 94
    :cond_1
    invoke-static {p1}, Ldh$a;->d(Ldh$a;)Lcn;

    move-result-object v0

    goto/16 :goto_1

    .line 100
    :cond_2
    invoke-static {p1}, Ldh$a;->f(Ldh$a;)Laz;

    move-result-object v0

    goto/16 :goto_2

    .line 104
    :cond_3
    invoke-static {p1}, Ldh$a;->g(Ldh$a;)Lcv;

    move-result-object v0

    goto/16 :goto_3

    .line 109
    :cond_4
    invoke-static {p1}, Ldh$a;->i(Ldh$a;)Laz;

    move-result-object v0

    goto/16 :goto_4

    .line 118
    :cond_5
    invoke-static {p1}, Ldh$a;->j(Ldh$a;)Lcom/facebook/cache/disk/b;

    move-result-object v0

    goto/16 :goto_5

    .line 122
    :cond_6
    invoke-static {p1}, Ldh$a;->k(Ldh$a;)Lcom/facebook/common/memory/b;

    move-result-object v0

    goto/16 :goto_6

    .line 126
    :cond_7
    invoke-static {p1}, Ldh$a;->l(Ldh$a;)Lcom/facebook/imagepipeline/producers/ac;

    move-result-object v0

    goto/16 :goto_7

    .line 131
    :cond_8
    invoke-static {p1}, Ldh$a;->n(Ldh$a;)Lcom/facebook/imagepipeline/memory/s;

    move-result-object v0

    goto :goto_8

    .line 135
    :cond_9
    invoke-static {p1}, Ldh$a;->o(Ldh$a;)Lcom/facebook/imagepipeline/decoder/b;

    move-result-object v0

    goto :goto_9

    .line 139
    :cond_a
    invoke-static {p1}, Ldh$a;->p(Ldh$a;)Ljava/util/Set;

    move-result-object v0

    goto :goto_a

    .line 144
    :cond_b
    invoke-static {p1}, Ldh$a;->r(Ldh$a;)Lcom/facebook/cache/disk/b;

    move-result-object v0

    goto :goto_b

    .line 152
    :cond_c
    invoke-static {p1}, Ldh$a;->s(Ldh$a;)Ldf;

    move-result-object v0

    goto :goto_c
.end method

.method synthetic constructor <init>(Ldh$a;Ldh$1;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Ldh;-><init>(Ldh$a;)V

    return-void
.end method

.method public static a(Landroid/content/Context;)Ldh$a;
    .locals 2

    .prologue
    .line 253
    new-instance v0, Ldh$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Ldh$a;-><init>(Landroid/content/Context;Ldh$1;)V

    return-object v0
.end method

.method private static b(Landroid/content/Context;)Lcom/facebook/cache/disk/b;
    .locals 4

    .prologue
    .line 158
    invoke-static {}, Lcom/facebook/cache/disk/b;->j()Lcom/facebook/cache/disk/b$a;

    move-result-object v0

    new-instance v1, Ldh$2;

    invoke-direct {v1, p0}, Ldh$2;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/facebook/cache/disk/b$a;->a(Laz;)Lcom/facebook/cache/disk/b$a;

    move-result-object v0

    const-string/jumbo v1, "image_cache"

    invoke-virtual {v0, v1}, Lcom/facebook/cache/disk/b$a;->a(Ljava/lang/String;)Lcom/facebook/cache/disk/b$a;

    move-result-object v0

    const-wide/32 v2, 0x2800000

    invoke-virtual {v0, v2, v3}, Lcom/facebook/cache/disk/b$a;->a(J)Lcom/facebook/cache/disk/b$a;

    move-result-object v0

    const-wide/32 v2, 0xa00000

    invoke-virtual {v0, v2, v3}, Lcom/facebook/cache/disk/b$a;->b(J)Lcom/facebook/cache/disk/b$a;

    move-result-object v0

    const-wide/32 v2, 0x200000

    invoke-virtual {v0, v2, v3}, Lcom/facebook/cache/disk/b$a;->c(J)Lcom/facebook/cache/disk/b$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/cache/disk/b$a;->a()Lcom/facebook/cache/disk/b;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lcg;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 175
    iget-object v0, p0, Ldh;->a:Lcg;

    return-object v0
.end method

.method public b()Laz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laz",
            "<",
            "Lcy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 179
    iget-object v0, p0, Ldh;->b:Laz;

    return-object v0
.end method

.method public c()Lcn;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Ldh;->c:Lcn;

    return-object v0
.end method

.method public d()Landroid/content/Context;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Ldh;->d:Landroid/content/Context;

    return-object v0
.end method

.method public e()Laz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laz",
            "<",
            "Lcy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 191
    iget-object v0, p0, Ldh;->f:Laz;

    return-object v0
.end method

.method public f()Ldf;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Ldh;->g:Ldf;

    return-object v0
.end method

.method public g()Lcv;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Ldh;->h:Lcv;

    return-object v0
.end method

.method public h()Lcom/facebook/imagepipeline/decoder/a;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204
    iget-object v0, p0, Ldh;->i:Lcom/facebook/imagepipeline/decoder/a;

    return-object v0
.end method

.method public i()Laz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laz",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208
    iget-object v0, p0, Ldh;->j:Laz;

    return-object v0
.end method

.method public j()Lcom/facebook/cache/disk/b;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Ldh;->k:Lcom/facebook/cache/disk/b;

    return-object v0
.end method

.method public k()Lcom/facebook/common/memory/b;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Ldh;->l:Lcom/facebook/common/memory/b;

    return-object v0
.end method

.method public l()Lcom/facebook/imagepipeline/producers/ac;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Ldh;->m:Lcom/facebook/imagepipeline/producers/ac;

    return-object v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 224
    iget-boolean v0, p0, Ldh;->e:Z

    return v0
.end method

.method public n()Lcom/facebook/imagepipeline/memory/s;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Ldh;->o:Lcom/facebook/imagepipeline/memory/s;

    return-object v0
.end method

.method public o()Lcom/facebook/imagepipeline/decoder/b;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Ldh;->p:Lcom/facebook/imagepipeline/decoder/b;

    return-object v0
.end method

.method public p()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ldx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 241
    iget-object v0, p0, Ldh;->q:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 245
    iget-boolean v0, p0, Ldh;->r:Z

    return v0
.end method

.method public r()Lcom/facebook/cache/disk/b;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Ldh;->s:Lcom/facebook/cache/disk/b;

    return-object v0
.end method
