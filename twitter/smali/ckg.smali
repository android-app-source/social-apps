.class public Lckg;
.super Landroid/widget/BaseAdapter;
.source "Twttr"

# interfaces
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Lcjq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Item:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/widget/BaseAdapter;",
        "Landroid/widget/AbsListView$RecyclerListener;",
        "Lcjq",
        "<TItem;>;"
    }
.end annotation


# instance fields
.field private final a:Lcju;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcju",
            "<TItem;>;"
        }
    .end annotation
.end field

.field private final b:Lckf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lckf",
            "<TItem;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcju;Lckd;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcju",
            "<TItem;>;",
            "Lckd",
            "<TItem;>;)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 33
    iput-object p1, p0, Lckg;->a:Lcju;

    .line 34
    iget-object v0, p0, Lckg;->a:Lcju;

    new-instance v1, Lcjv;

    invoke-direct {v1, p0}, Lcjv;-><init>(Landroid/widget/BaseAdapter;)V

    invoke-interface {v0, v1}, Lcju;->a(Lcjs;)V

    .line 35
    new-instance v0, Lckf;

    invoke-direct {v0, p2}, Lckf;-><init>(Lckd;)V

    iput-object v0, p0, Lckg;->b:Lckf;

    .line 36
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TItem;",
            "Landroid/view/ViewGroup;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lckg;->b:Lckf;

    iget-object v1, p0, Lckg;->b:Lckf;

    .line 105
    invoke-virtual {v1, p1}, Lckf;->a(Ljava/lang/Object;)I

    move-result v1

    .line 104
    invoke-virtual {v0, p2, v1}, Lckf;->a(Landroid/view/ViewGroup;I)Lckb$a;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lckb$a;->b()Landroid/view/View;

    move-result-object v1

    .line 107
    sget v2, Lcjw$a;->viewbinder_viewholder_tag:I

    invoke-virtual {v1, v2, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 108
    return-object v1
.end method

.method public a()Lcju;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcju",
            "<TItem;>;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lckg;->a:Lcju;

    return-object v0
.end method

.method protected a(Landroid/view/View;Ljava/lang/Object;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "TItem;I)V"
        }
    .end annotation

    .prologue
    .line 112
    sget v0, Lcjw$a;->viewbinder_viewholder_tag:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lckb$a;

    .line 113
    iget-object v1, p0, Lckg;->b:Lckf;

    invoke-virtual {v1, v0, p2, p3}, Lckf;->a(Lckb$a;Ljava/lang/Object;I)V

    .line 114
    sget v0, Lcjw$a;->item_in_list:I

    invoke-virtual {p1, v0, p2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 115
    return-void
.end method

.method public a(Lcke;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcke",
            "<TItem;>;)V"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lckg;->b:Lckf;

    invoke-virtual {v0, p1}, Lckf;->a(Lcke;)V

    .line 40
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lckg;->a:Lcju;

    invoke-interface {v0}, Lcju;->a()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TItem;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lckg;->a:Lcju;

    invoke-interface {v0, p1}, Lcju;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lckg;->a:Lcju;

    invoke-interface {v0, p1}, Lcju;->b(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lckg;->b:Lckf;

    invoke-virtual {p0, p1}, Lckg;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lckf;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0, p1}, Lckg;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 96
    if-eqz p2, :cond_0

    .line 97
    :goto_0
    invoke-virtual {p0, p2, v0, p1}, Lckg;->a(Landroid/view/View;Ljava/lang/Object;I)V

    .line 99
    return-object p2

    .line 96
    :cond_0
    invoke-virtual {p0, v0, p3}, Lckg;->a(Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lckg;->b:Lckf;

    invoke-virtual {v0}, Lckf;->a()I

    move-result v0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lckg;->b:Lckf;

    invoke-virtual {p0, p1}, Lckg;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lckf;->b(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 85
    sget v0, Lcjw$a;->viewbinder_viewholder_tag:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lckb$a;

    .line 86
    iget-object v1, p0, Lckg;->b:Lckf;

    sget v2, Lcjw$a;->item_in_list:I

    .line 87
    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 86
    invoke-virtual {v1, v0, v2}, Lckf;->a(Lckb$a;Ljava/lang/Object;)V

    .line 88
    sget v0, Lcjw$a;->item_in_list:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 89
    return-void
.end method
