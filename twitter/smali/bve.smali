.class public Lbve;
.super Laum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laum",
        "<",
        "Lbvd$a;",
        "Lcom/twitter/util/collection/Pair",
        "<",
        "Lbec;",
        "Lcom/twitter/library/service/u;",
        ">;",
        "Lbec;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/v;

.field private final c:Lcom/twitter/util/android/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/v;Lcom/twitter/util/android/a;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Laum;-><init>()V

    .line 34
    iput-object p1, p0, Lbve;->a:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lbve;->b:Lcom/twitter/library/client/v;

    .line 36
    iput-object p3, p0, Lbve;->c:Lcom/twitter/util/android/a;

    .line 37
    return-void
.end method


# virtual methods
.method protected a(Lbvd$a;)Lbec;
    .locals 6

    .prologue
    .line 48
    new-instance v1, Lbec;

    iget-object v2, p0, Lbve;->a:Landroid/content/Context;

    iget-object v3, p0, Lbve;->b:Lcom/twitter/library/client/v;

    .line 49
    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvd$a;

    iget-wide v4, v0, Lbvd$a;->a:J

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-object v3, p0, Lbve;->c:Lcom/twitter/util/android/a;

    invoke-virtual {v3}, Lcom/twitter/util/android/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lbec;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    .line 48
    return-object v1
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Lcom/twitter/library/service/s;
    .locals 1

    .prologue
    .line 24
    check-cast p1, Lbvd$a;

    invoke-virtual {p0, p1}, Lbve;->a(Lbvd$a;)Lbec;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lbec;)Lcom/twitter/util/collection/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbec;",
            ")",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Lbec;",
            "Lcom/twitter/library/service/u;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    invoke-virtual {p1}, Lbec;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/library/service/s;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    check-cast p1, Lbec;

    invoke-virtual {p0, p1}, Lbve;->a(Lbec;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    return-object v0
.end method
