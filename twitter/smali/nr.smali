.class public Lnr;
.super Lbjb;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/av/playback/AVPlayer;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p2}, Lbjb;-><init>(Lcom/twitter/model/av/AVMedia;)V

    .line 23
    iput-object p1, p0, Lnr;->a:Lcom/twitter/library/av/playback/AVPlayer;

    .line 24
    return-void
.end method


# virtual methods
.method public processSkipAd(Lbkh;)V
    .locals 5
    .annotation runtime Lbiz;
        a = Lbkh;
    .end annotation

    .prologue
    .line 28
    new-instance v0, Lcom/twitter/library/av/m$a;

    invoke-direct {v0}, Lcom/twitter/library/av/m$a;-><init>()V

    iget-wide v2, p1, Lbkh;->b:J

    .line 29
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->c(Ljava/lang/Long;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    .line 30
    iget-object v1, p0, Lnr;->a:Lcom/twitter/library/av/playback/AVPlayer;

    const-string/jumbo v2, "video_ad_skip"

    const/4 v3, 0x0

    iget-object v4, p0, Lnr;->k:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/m$a;)V

    .line 31
    return-void
.end method
