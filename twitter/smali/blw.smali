.class public Lblw;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lblv;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:J

.field private final c:J

.field private final g:Ljava/lang/String;

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Z

.field private final j:Z

.field private final k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;JJLjava/lang/String;Ljava/util/List;ZZLjava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/client/Session;",
            "Ljava/lang/String;",
            "JJ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;ZZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39
    const-class v0, Lblw;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 40
    iput-object p3, p0, Lblw;->a:Ljava/lang/String;

    .line 41
    iput-wide p4, p0, Lblw;->b:J

    .line 42
    iput-wide p6, p0, Lblw;->c:J

    .line 43
    iput-object p8, p0, Lblw;->g:Ljava/lang/String;

    .line 44
    iput-object p9, p0, Lblw;->h:Ljava/util/List;

    .line 45
    iput-boolean p10, p0, Lblw;->i:Z

    .line 46
    iput-boolean p11, p0, Lblw;->j:Z

    .line 47
    iput-object p12, p0, Lblw;->k:Ljava/lang/String;

    .line 48
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 4

    .prologue
    .line 53
    invoke-virtual {p0}, Lblw;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 54
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "commerce"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "products"

    aput-object v3, v1, v2

    .line 55
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->b([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "product_id"

    iget-object v2, p0, Lblw;->a:Ljava/lang/String;

    .line 56
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "marketplace_id"

    iget-wide v2, p0, Lblw;->b:J

    .line 57
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "merchant_id"

    iget-wide v2, p0, Lblw;->c:J

    .line 58
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "card_url"

    iget-object v2, p0, Lblw;->g:Ljava/lang/String;

    .line 59
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 61
    iget-boolean v0, p0, Lblw;->i:Z

    if-eqz v0, :cond_1

    .line 62
    const-string/jumbo v0, "calculate_fees"

    const-string/jumbo v2, "true"

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 67
    :goto_0
    iget-boolean v0, p0, Lblw;->j:Z

    if-eqz v0, :cond_2

    .line 68
    const-string/jumbo v0, "allow_unconfirmed_email"

    const-string/jumbo v2, "true"

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 73
    :goto_1
    iget-object v0, p0, Lblw;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    const-string/jumbo v0, "shipping_address_id"

    iget-object v2, p0, Lblw;->k:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 76
    :cond_0
    iget-object v0, p0, Lblw;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 77
    const-string/jumbo v3, "variant_id"

    invoke-virtual {v1, v3, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto :goto_2

    .line 64
    :cond_1
    const-string/jumbo v0, "calculate_fees"

    const-string/jumbo v2, "false"

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto :goto_0

    .line 70
    :cond_2
    const-string/jumbo v0, "allow_unconfirmed_email"

    const-string/jumbo v2, "false"

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto :goto_1

    .line 79
    :cond_3
    iget-object v0, p0, Lblw;->p:Landroid/content/Context;

    invoke-static {v1, v0}, Lblx;->a(Lcom/twitter/library/service/d$a;Landroid/content/Context;)V

    .line 80
    invoke-virtual {v1}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lblv;)V
    .locals 3

    .prologue
    .line 92
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    invoke-virtual {p3}, Lblv;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 94
    iget-object v1, p2, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "product_info_bundle"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 98
    :goto_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/u;->a(Lcom/twitter/network/HttpOperation;)V

    .line 99
    return-void

    .line 96
    :cond_0
    invoke-static {p2, p3}, Lblx;->a(Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 18
    check-cast p3, Lblv;

    invoke-virtual {p0, p1, p2, p3}, Lblw;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lblv;)V

    return-void
.end method

.method protected b()Lblv;
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lblv;

    invoke-direct {v0}, Lblv;-><init>()V

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lblw;->b()Lblv;

    move-result-object v0

    return-object v0
.end method
