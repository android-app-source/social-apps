.class public Laiq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laip;


# instance fields
.field private final a:Lain;

.field private final b:Lrx/f;

.field private final c:Lrx/f;

.field private d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Laiu;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lrx/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/c",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lrx/j;

.field private g:Z


# direct methods
.method public constructor <init>(Lain;)V
    .locals 2

    .prologue
    .line 40
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v0

    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Laiq;-><init>(Lain;Lrx/f;Lrx/f;)V

    .line 41
    return-void
.end method

.method constructor <init>(Lain;Lrx/f;Lrx/f;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Laiq;->g:Z

    .line 46
    iput-object p1, p0, Laiq;->a:Lain;

    .line 47
    iput-object p2, p0, Laiq;->b:Lrx/f;

    .line 48
    iput-object p3, p0, Laiq;->c:Lrx/f;

    .line 49
    return-void
.end method

.method static synthetic a(Laiq;Lrx/c;)Lrx/c;
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Laiq;->e:Lrx/c;

    return-object p1
.end method

.method private b()Laiu;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 145
    iget-boolean v1, p0, Laiq;->g:Z

    if-nez v1, :cond_1

    .line 153
    :cond_0
    :goto_0
    return-object v0

    .line 149
    :cond_1
    iget-object v1, p0, Laiq;->d:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    iget-object v1, p0, Laiq;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 153
    iget-object v0, p0, Laiq;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laiu;

    goto :goto_0
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Laiq;->b()Laiu;

    move-result-object v0

    .line 98
    if-nez v0, :cond_0

    .line 103
    :goto_0
    return-void

    .line 102
    :cond_0
    invoke-interface {v0, p1}, Laiu;->a(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/TwitterUser;)I
    .locals 2

    .prologue
    .line 138
    iget v0, p1, Lcom/twitter/model/core/TwitterUser;->U:I

    .line 139
    iget-object v1, p0, Laiq;->a:Lain;

    invoke-interface {v1}, Lain;->a()I

    move-result v1

    .line 140
    invoke-static {v1, v0}, Lait;->a(II)I

    move-result v0

    return v0
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    iget-object v0, p0, Laiq;->d:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Laiq;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 64
    iput-object v1, p0, Laiq;->d:Ljava/lang/ref/WeakReference;

    .line 67
    :cond_0
    iget-object v0, p0, Laiq;->f:Lrx/j;

    if-eqz v0, :cond_1

    .line 68
    iget-object v0, p0, Laiq;->f:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 69
    iput-object v1, p0, Laiq;->f:Lrx/j;

    .line 72
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Laiq;->g:Z

    .line 73
    return-void
.end method

.method public a(Laiu;)V
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Laiq;->g:Z

    if-eqz v0, :cond_0

    .line 58
    :goto_0
    return-void

    .line 56
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Laiq;->d:Ljava/lang/ref/WeakReference;

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Laiq;->g:Z

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 77
    invoke-direct {p0}, Laiq;->b()Laiu;

    move-result-object v0

    .line 78
    if-nez v0, :cond_0

    .line 87
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0b0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 83
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0007

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 84
    new-instance v3, Laim;

    invoke-direct {v3, v1, v2}, Laim;-><init>([Ljava/lang/String;[Ljava/lang/String;)V

    .line 86
    invoke-virtual {v3}, Laim;->a()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Laim;->b()[Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Laiu;->a([Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Laiq;->a:Lain;

    invoke-interface {v0}, Lain;->a()I

    move-result v0

    invoke-direct {p0, v0}, Laiq;->b(I)V

    .line 109
    iget-object v0, p0, Laiq;->e:Lrx/c;

    if-nez v0, :cond_0

    .line 110
    iget-object v0, p0, Laiq;->a:Lain;

    invoke-interface {v0, p1, p2}, Lain;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lrx/c;

    move-result-object v0

    invoke-virtual {v0}, Lrx/c;->g()Lrx/c;

    move-result-object v0

    iput-object v0, p0, Laiq;->e:Lrx/c;

    .line 113
    :cond_0
    iget-object v0, p0, Laiq;->e:Lrx/c;

    iget-object v1, p0, Laiq;->b:Lrx/f;

    .line 114
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Laiq;->c:Lrx/f;

    .line 115
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Laiq$1;

    invoke-direct {v1, p0}, Laiq$1;-><init>(Laiq;)V

    .line 116
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Laiq;->f:Lrx/j;

    .line 129
    return-void
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Laiq;->a:Lain;

    invoke-interface {v0, p1}, Lain;->a(I)V

    .line 92
    invoke-direct {p0, p1}, Laiq;->b(I)V

    .line 93
    const/4 v0, 0x1

    return v0
.end method

.method public b(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Laiq;->a:Lain;

    invoke-interface {v0, p1, p2}, Lain;->b(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 134
    return-void
.end method
