.class public Lpm;
.super Lbjb;
.source "Twttr"


# instance fields
.field volatile a:I

.field private final b:Lcom/twitter/library/vineloops/VineLoopAggregator;

.field private final c:Lcom/twitter/library/vineloops/a;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/model/av/AVMedia;)V
    .locals 2

    .prologue
    .line 30
    invoke-static {p1}, Lcom/twitter/library/vineloops/VineLoopAggregator;->a(Landroid/content/Context;)Lcom/twitter/library/vineloops/VineLoopAggregator;

    move-result-object v0

    .line 31
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/twitter/library/vineloops/a;->a(Landroid/content/Context;Lcom/twitter/library/client/p;)Lcom/twitter/library/vineloops/a;

    move-result-object v1

    .line 30
    invoke-direct {p0, p2, v0, v1}, Lpm;-><init>(Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/vineloops/VineLoopAggregator;Lcom/twitter/library/vineloops/a;)V

    .line 32
    return-void
.end method

.method constructor <init>(Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/vineloops/VineLoopAggregator;Lcom/twitter/library/vineloops/a;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lbjb;-><init>(Lcom/twitter/model/av/AVMedia;)V

    .line 38
    iput-object p2, p0, Lpm;->b:Lcom/twitter/library/vineloops/VineLoopAggregator;

    .line 39
    iput-object p3, p0, Lpm;->c:Lcom/twitter/library/vineloops/a;

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lpm;->d:Z

    .line 41
    return-void
.end method


# virtual methods
.method public processLoopEvent(Lbjt;)V
    .locals 1
    .annotation runtime Lbiz;
        a = Lbjt;
    .end annotation

    .prologue
    .line 57
    iget v0, p0, Lpm;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lpm;->a:I

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpm;->d:Z

    .line 59
    return-void
.end method

.method public processMediaReleaseEvent(Lbjh;)V
    .locals 3
    .annotation runtime Lbiz;
        a = Lbjh;
    .end annotation

    .prologue
    .line 45
    iget v0, p0, Lpm;->a:I

    if-lez v0, :cond_1

    .line 46
    iget-object v0, p0, Lpm;->k:Lcom/twitter/model/av/AVMedia;

    invoke-interface {v0}, Lcom/twitter/model/av/AVMedia;->d()Ljava/lang/String;

    move-result-object v0

    .line 47
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    iget-object v1, p0, Lpm;->b:Lcom/twitter/library/vineloops/VineLoopAggregator;

    iget v2, p0, Lpm;->a:I

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/vineloops/VineLoopAggregator;->a(Ljava/lang/String;I)V

    .line 49
    iget-object v0, p0, Lpm;->c:Lcom/twitter/library/vineloops/a;

    invoke-virtual {v0}, Lcom/twitter/library/vineloops/a;->a()V

    .line 51
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lpm;->a:I

    .line 53
    :cond_1
    return-void
.end method
