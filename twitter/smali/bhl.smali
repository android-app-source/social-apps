.class public Lbhl;
.super Lcom/twitter/library/api/r;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/api/r",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcom/twitter/model/account/a;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    .line 43
    const-class v0, Lbhl;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/r;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 45
    if-eqz v0, :cond_0

    .line 46
    invoke-static {v0}, Lcom/twitter/util/b;->b(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbhl;->g:Ljava/lang/String;

    .line 48
    :cond_0
    const-string/jumbo v0, "retry_nux_and_login_verification_get_requests_2753"

    const-string/jumbo v1, "network_retry_active"

    invoke-static {v0, v1}, Lcoi;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    new-instance v0, Lcom/twitter/library/service/n;

    invoke-direct {v0}, Lcom/twitter/library/service/n;-><init>()V

    invoke-virtual {p0, v0}, Lbhl;->a(Lcom/twitter/library/service/e;)V

    .line 52
    :cond_1
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;ILjava/lang/String;)Lcom/twitter/library/service/s;
    .locals 1

    .prologue
    .line 109
    const-string/jumbo v0, "guest_auth_availability_check_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    new-instance v0, Lbhm;

    invoke-direct {v0, p0, p1}, Lbhm;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 111
    invoke-virtual {v0, p2}, Lbhm;->g(I)Lcom/twitter/library/service/s;

    move-result-object v0

    check-cast v0, Lbhm;

    .line 112
    packed-switch p2, :pswitch_data_0

    .line 142
    :goto_0
    return-object v0

    .line 114
    :pswitch_0
    iput-object p3, v0, Lbhm;->a:Ljava/lang/String;

    goto :goto_0

    .line 118
    :pswitch_1
    iput-object p3, v0, Lbhm;->b:Ljava/lang/String;

    goto :goto_0

    .line 127
    :cond_0
    new-instance v0, Lbhl;

    invoke-direct {v0, p0, p1}, Lbhl;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 128
    invoke-virtual {v0, p2}, Lbhl;->g(I)Lcom/twitter/library/service/s;

    move-result-object v0

    check-cast v0, Lbhl;

    .line 129
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 131
    :pswitch_2
    iput-object p3, v0, Lbhl;->b:Ljava/lang/String;

    goto :goto_0

    .line 135
    :pswitch_3
    iput-object p3, v0, Lbhl;->c:Ljava/lang/String;

    goto :goto_0

    .line 112
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 129
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 62
    invoke-virtual {p0}, Lbhl;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "i"

    .line 63
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    const-string/jumbo v2, "users"

    aput-object v2, v1, v3

    .line 64
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->b([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lbhl;->g:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    const-string/jumbo v1, "lang"

    iget-object v2, p0, Lbhl;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 68
    :cond_0
    invoke-virtual {p0}, Lbhl;->L()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 82
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No action for code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lbhl;->L()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :pswitch_0
    new-array v1, v4, [Ljava/lang/Object;

    const-string/jumbo v2, "email_available"

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "email"

    iget-object v2, p0, Lbhl;->b:Ljava/lang/String;

    .line 71
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    .line 75
    :goto_0
    return-object v0

    :pswitch_1
    new-array v1, v4, [Ljava/lang/Object;

    const-string/jumbo v2, "username_available"

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "custom"

    const-wide/16 v2, 0x1

    .line 76
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "context"

    const-string/jumbo v2, "signup"

    .line 77
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "username"

    iget-object v2, p0, Lbhl;->c:Ljava/lang/String;

    .line 78
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    goto :goto_0

    .line 68
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/account/a;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 95
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/a;

    .line 97
    iget-boolean v1, v0, Lcom/twitter/model/account/a;->a:Z

    if-nez v1, :cond_0

    .line 98
    const/16 v1, 0x190

    iget-object v0, v0, Lcom/twitter/model/account/a;->b:Ljava/lang/String;

    invoke-virtual {p2, v1, v0}, Lcom/twitter/library/service/u;->a(ILjava/lang/String;)V

    .line 101
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 29
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbhl;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/account/a;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    const-class v0, Lcom/twitter/model/account/a;

    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    const-string/jumbo v0, "app:twitter_service:account:availability_check"

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lbhl;->b()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
