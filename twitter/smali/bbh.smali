.class public Lbbh;
.super Lcom/twitter/library/api/r;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/api/r",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcom/twitter/model/core/TwitterUser;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final b:Lcom/twitter/library/network/t;

.field private c:Lcom/twitter/model/core/TwitterUser;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/network/t;)V
    .locals 2

    .prologue
    .line 35
    const-class v0, Lbbh;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/r;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 36
    iput-object p3, p0, Lbbh;->b:Lcom/twitter/library/network/t;

    .line 37
    const-string/jumbo v0, "retry_nux_and_login_verification_get_requests_2753"

    const-string/jumbo v1, "network_retry_active"

    invoke-static {v0, v1}, Lcoi;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    new-instance v0, Lcom/twitter/library/service/n;

    invoke-direct {v0}, Lcom/twitter/library/service/n;-><init>()V

    invoke-virtual {p0, v0}, Lbbh;->a(Lcom/twitter/library/service/e;)V

    .line 41
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/library/network/t;)V
    .locals 2

    .prologue
    .line 44
    const-class v0, Lbbh;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/r;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 45
    iput-object p3, p0, Lbbh;->b:Lcom/twitter/library/network/t;

    .line 46
    const-string/jumbo v0, "retry_nux_and_login_verification_get_requests_2753"

    const-string/jumbo v1, "network_retry_active"

    invoke-static {v0, v1}, Lcoi;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    new-instance v0, Lcom/twitter/library/service/n;

    invoke-direct {v0}, Lcom/twitter/library/service/n;-><init>()V

    invoke-virtual {p0, v0}, Lbbh;->a(Lcom/twitter/library/service/e;)V

    .line 50
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 55
    invoke-virtual {p0}, Lbbh;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    iget-object v1, p0, Lbbh;->b:Lcom/twitter/library/network/t;

    .line 56
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "account"

    aput-object v3, v1, v2

    const-string/jumbo v2, "verify_credentials"

    aput-object v2, v1, v4

    .line 57
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "include_user_entities"

    .line 58
    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "include_nsfw_user_flag"

    .line 59
    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    .line 55
    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v2, -0x1

    const/4 v7, 0x0

    .line 76
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/twitter/model/core/TwitterUser;

    .line 78
    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lbbh;->c:Lcom/twitter/model/core/TwitterUser;

    .line 80
    invoke-virtual {p0}, Lbbh;->S()Laut;

    move-result-object v10

    .line 81
    iget-wide v4, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v4, v5}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    .line 82
    invoke-static {v1}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const/4 v4, -0x1

    const/4 v9, 0x1

    move-wide v5, v2

    move-object v8, v7

    invoke-virtual/range {v0 .. v10}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;ZLaut;)I

    .line 84
    invoke-virtual {v10}, Laut;->a()V

    .line 86
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 29
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbbh;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    const-class v0, Lcom/twitter/model/core/TwitterUser;

    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    const-string/jumbo v0, "app:twitter_service:account:verify_credentials"

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lbbh;->b()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method

.method public g()Lcom/twitter/model/core/TwitterUser;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lbbh;->c:Lcom/twitter/model/core/TwitterUser;

    return-object v0
.end method
