.class final Lcgb$b$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcgb$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcgb$b;",
        "Lcgb$b$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 404
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcgb$1;)V
    .locals 0

    .prologue
    .line 404
    invoke-direct {p0}, Lcgb$b$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcgb$b$a;
    .locals 1

    .prologue
    .line 408
    new-instance v0, Lcgb$b$a;

    invoke-direct {v0}, Lcgb$b$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcgb$b$a;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 422
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcgb$b$a;->a(Ljava/lang/String;)Lcgb$b$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v2, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 424
    invoke-static {v0, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 423
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcgb$b$a;->a(Ljava/util/List;)Lcgb$b$a;

    .line 425
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 404
    check-cast p2, Lcgb$b$a;

    invoke-virtual {p0, p1, p2, p3}, Lcgb$b$b;->a(Lcom/twitter/util/serialization/n;Lcgb$b$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcgb$b;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 414
    iget-object v0, p2, Lcgb$b;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 415
    iget-object v0, p2, Lcgb$b;->c:Ljava/util/List;

    sget-object v1, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v2, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 416
    invoke-static {v1, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    .line 415
    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 417
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 404
    check-cast p2, Lcgb$b;

    invoke-virtual {p0, p1, p2}, Lcgb$b$b;->a(Lcom/twitter/util/serialization/o;Lcgb$b;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 404
    invoke-virtual {p0}, Lcgb$b$b;->a()Lcgb$b$a;

    move-result-object v0

    return-object v0
.end method
