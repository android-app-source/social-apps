.class public Lbij;
.super Lcom/twitter/library/api/r;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/api/r",
        "<",
        "Lcom/twitter/library/api/y;",
        ">;"
    }
.end annotation


# instance fields
.field public final b:Lcom/twitter/model/core/TwitterUser;

.field private final c:Lcgi;

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/TwitterUser;Lcgi;)V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lcom/twitter/library/service/v;

    invoke-direct {v0, p2}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    invoke-direct {p0, p1, v0, p3, p4}, Lbij;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/model/core/TwitterUser;Lcgi;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/model/core/TwitterUser;Lcgi;)V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lbij;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/r;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 63
    iput-object p3, p0, Lbij;->b:Lcom/twitter/model/core/TwitterUser;

    .line 64
    iput-object p4, p0, Lbij;->c:Lcgi;

    .line 65
    new-instance v0, Lcom/twitter/library/service/o;

    invoke-direct {v0}, Lcom/twitter/library/service/o;-><init>()V

    invoke-virtual {p0, v0}, Lbij;->a(Lcom/twitter/library/service/e;)V

    .line 66
    return-void
.end method

.method private a(Lcom/twitter/library/provider/t;Laut;JJ)V
    .locals 7

    .prologue
    .line 151
    const/16 v1, 0x2b

    move-object v0, p1

    move-wide v2, p3

    move-wide v4, p5

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/library/provider/t;->a(IJJLaut;)V

    .line 152
    const/4 v0, 0x1

    iget-object v1, p0, Lbij;->b:Lcom/twitter/model/core/TwitterUser;

    iget-object v1, v1, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/provider/t;->a(ILjava/lang/String;)I

    .line 153
    return-void
.end method

.method private a(Lcom/twitter/library/provider/t;Laut;JJZ)V
    .locals 11

    .prologue
    .line 127
    const/16 v9, 0x10

    const/16 v10, 0x2b

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide/from16 v6, p5

    move/from16 v8, p7

    invoke-direct/range {v1 .. v10}, Lbij;->a(Lcom/twitter/library/provider/t;Laut;JJZII)V

    .line 129
    return-void
.end method

.method private a(Lcom/twitter/library/provider/t;Laut;JJZII)V
    .locals 13

    .prologue
    .line 140
    if-eqz p7, :cond_0

    .line 141
    iget-object v3, p0, Lbij;->b:Lcom/twitter/model/core/TwitterUser;

    invoke-static {v3}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 142
    move-wide/from16 v0, p5

    move/from16 v2, p8

    invoke-virtual {p1, v0, v1, v2, p2}, Lcom/twitter/library/provider/t;->a(JILaut;)V

    .line 143
    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    move-object v3, p1

    move-wide/from16 v5, p3

    move/from16 v7, p9

    move-object v11, p2

    invoke-virtual/range {v3 .. v11}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;JIJLjava/lang/String;Laut;)V

    .line 147
    :goto_0
    return-void

    .line 145
    :cond_0
    move-wide/from16 v0, p5

    move/from16 v2, p8

    invoke-virtual {p1, v0, v1, v2, p2}, Lcom/twitter/library/provider/t;->b(JILaut;)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/service/d$a;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0, p3}, Lbij;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    invoke-direct {p0, p3}, Lbij;->d(I)Z

    move-result v0

    invoke-virtual {p1, p2, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 111
    :cond_0
    return-void
.end method

.method private b(Lcom/twitter/library/provider/t;Laut;JJZ)V
    .locals 11

    .prologue
    .line 133
    const/16 v9, 0x800

    const/16 v10, 0x2b

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide/from16 v6, p5

    move/from16 v8, p7

    invoke-direct/range {v1 .. v10}, Lbij;->a(Lcom/twitter/library/provider/t;Laut;JJZII)V

    .line 135
    return-void
.end method

.method private c(I)Z
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lbij;->g:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(I)Z
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lbij;->h:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(I)Lbij;
    .locals 2

    .prologue
    .line 258
    iget v0, p0, Lbij;->g:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lbij;->g:I

    .line 259
    return-object p0
.end method

.method public a(IZ)Lbij;
    .locals 5

    .prologue
    const/16 v4, 0x10

    const/16 v3, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 235
    if-eqz p2, :cond_0

    .line 236
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_1

    .line 237
    invoke-virtual {p0, v3, v1}, Lbij;->a(IZ)Lbij;

    .line 238
    invoke-virtual {p0, v4}, Lbij;->a(I)Lbij;

    .line 248
    :cond_0
    :goto_0
    iget v0, p0, Lbij;->g:I

    or-int/2addr v0, p1

    iput v0, p0, Lbij;->g:I

    .line 249
    if-eqz p2, :cond_3

    .line 250
    iget v0, p0, Lbij;->h:I

    or-int/2addr v0, p1

    iput v0, p0, Lbij;->h:I

    .line 254
    :goto_1
    return-object p0

    .line 239
    :cond_1
    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_2

    .line 240
    invoke-virtual {p0, v2, v1}, Lbij;->a(IZ)Lbij;

    .line 241
    invoke-virtual {p0, v4}, Lbij;->a(I)Lbij;

    goto :goto_0

    .line 242
    :cond_2
    and-int/lit8 v0, p1, 0x10

    if-eqz v0, :cond_0

    .line 243
    invoke-virtual {p0, v2}, Lbij;->a(I)Lbij;

    .line 244
    invoke-virtual {p0, v3}, Lbij;->a(I)Lbij;

    goto :goto_0

    .line 252
    :cond_3
    iget v0, p0, Lbij;->h:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lbij;->h:I

    goto :goto_1
.end method

.method protected a()Lcom/twitter/library/service/d;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 76
    invoke-virtual {p0}, Lbij;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 77
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "friendships"

    aput-object v3, v1, v2

    const-string/jumbo v2, "update"

    aput-object v2, v1, v4

    .line 78
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 79
    const-string/jumbo v1, "user_id"

    iget-object v2, p0, Lbij;->b:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    .line 80
    const-string/jumbo v1, "device"

    invoke-direct {p0, v0, v1, v4}, Lbij;->a(Lcom/twitter/library/service/d$a;Ljava/lang/String;I)V

    .line 81
    const-string/jumbo v1, "live"

    const/16 v2, 0x8

    invoke-direct {p0, v0, v1, v2}, Lbij;->a(Lcom/twitter/library/service/d$a;Ljava/lang/String;I)V

    .line 82
    const-string/jumbo v1, "lifeline"

    invoke-direct {p0, v0, v1, v5}, Lbij;->a(Lcom/twitter/library/service/d$a;Ljava/lang/String;I)V

    .line 83
    const-string/jumbo v1, "retweets"

    const/4 v2, 0x4

    invoke-direct {p0, v0, v1, v2}, Lbij;->a(Lcom/twitter/library/service/d$a;Ljava/lang/String;I)V

    .line 84
    const-string/jumbo v1, "unsubscribe"

    const/16 v2, 0x10

    invoke-direct {p0, v0, v1, v2}, Lbij;->a(Lcom/twitter/library/service/d$a;Ljava/lang/String;I)V

    .line 86
    iget-object v1, p0, Lbij;->c:Lcgi;

    if-eqz v1, :cond_1

    .line 87
    iget-object v1, p0, Lbij;->c:Lcgi;

    iget-object v1, v1, Lcgi;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 88
    const-string/jumbo v1, "impression_id"

    iget-object v2, p0, Lbij;->c:Lcgi;

    iget-object v2, v2, Lcgi;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 90
    :cond_0
    iget-object v1, p0, Lbij;->c:Lcgi;

    invoke-virtual {v1}, Lcgi;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 91
    const-string/jumbo v1, "earned"

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 95
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V
    .locals 19

    .prologue
    .line 158
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 159
    invoke-virtual/range {p0 .. p0}, Lbij;->R()Lcom/twitter/library/provider/t;

    move-result-object v4

    .line 160
    invoke-virtual/range {p0 .. p0}, Lbij;->S()Laut;

    move-result-object v5

    .line 161
    move-object/from16 v0, p0

    iget-object v2, v0, Lbij;->b:Lcom/twitter/model/core/TwitterUser;

    iget-wide v8, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 162
    const/16 v2, 0x10

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lbij;->c(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x10

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lbij;->d(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 164
    invoke-virtual/range {p0 .. p0}, Lbij;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    .line 165
    iget-wide v6, v2, Lcom/twitter/library/service/v;->c:J

    .line 167
    const/4 v10, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v10}, Lbij;->a(Lcom/twitter/library/provider/t;Laut;JJZ)V

    .line 168
    const/4 v10, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v10}, Lbij;->b(Lcom/twitter/library/provider/t;Laut;JJZ)V

    move-object/from16 v3, p0

    .line 169
    invoke-direct/range {v3 .. v9}, Lbij;->a(Lcom/twitter/library/provider/t;Laut;JJ)V

    .line 171
    :cond_0
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lbij;->c(I)Z

    move-result v2

    if-nez v2, :cond_1

    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lbij;->c(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 172
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lbij;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    .line 173
    iget-wide v6, v2, Lcom/twitter/library/service/v;->c:J

    .line 175
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lbij;->d(I)Z

    move-result v10

    .line 176
    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lbij;->d(I)Z

    move-result v18

    move-object/from16 v3, p0

    .line 178
    invoke-direct/range {v3 .. v10}, Lbij;->a(Lcom/twitter/library/provider/t;Laut;JJZ)V

    move-object/from16 v11, p0

    move-object v12, v4

    move-object v13, v5

    move-wide v14, v6

    move-wide/from16 v16, v8

    .line 179
    invoke-direct/range {v11 .. v18}, Lbij;->b(Lcom/twitter/library/provider/t;Laut;JJZ)V

    .line 181
    if-nez v10, :cond_2

    if-eqz v18, :cond_3

    .line 183
    :cond_2
    invoke-static {v6, v7}, Lcom/twitter/library/platform/notifications/PushRegistration;->a(J)I

    move-result v2

    .line 184
    const/16 v3, 0x200

    invoke-static {v2, v3}, Lcga;->b(II)Z

    move-result v2

    if-nez v2, :cond_3

    .line 185
    const/16 v2, 0x3e9

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/twitter/library/service/u;->a(I)V

    .line 189
    :cond_3
    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lbij;->c(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 190
    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lbij;->d(I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 191
    const/16 v2, 0x100

    invoke-virtual {v4, v8, v9, v2, v5}, Lcom/twitter/library/provider/t;->a(JILaut;)V

    .line 196
    :cond_4
    :goto_0
    const/4 v2, 0x4

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lbij;->c(I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 197
    const/4 v2, 0x4

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lbij;->d(I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 198
    const/16 v2, 0x200

    invoke-virtual {v4, v8, v9, v2, v5}, Lcom/twitter/library/provider/t;->a(JILaut;)V

    .line 203
    :cond_5
    :goto_1
    invoke-virtual {v5}, Laut;->a()V

    .line 205
    :cond_6
    return-void

    .line 193
    :cond_7
    const/16 v2, 0x100

    invoke-virtual {v4, v8, v9, v2, v5}, Lcom/twitter/library/provider/t;->b(JILaut;)V

    goto :goto_0

    .line 200
    :cond_8
    const/16 v2, 0x200

    invoke-virtual {v4, v8, v9, v2, v5}, Lcom/twitter/library/provider/t;->b(JILaut;)V

    goto :goto_1
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 38
    check-cast p3, Lcom/twitter/library/api/y;

    invoke-virtual {p0, p1, p2, p3}, Lbij;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/api/y;
    .locals 1

    .prologue
    .line 209
    const/16 v0, 0x1e

    invoke-static {v0}, Lcom/twitter/library/api/y;->a(I)Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method

.method protected b(Lcom/twitter/library/service/u;)Z
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lbij;->b:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    iget v0, p0, Lbij;->g:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d(Lcom/twitter/async/service/j;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 214
    invoke-super {p0, p1}, Lcom/twitter/library/api/r;->d(Lcom/twitter/async/service/j;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    :goto_0
    return v1

    .line 218
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 227
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v2

    const/16 v3, 0x193

    if-eq v2, v3, :cond_1

    .line 228
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    const/16 v2, 0x3e9

    if-ne v0, v2, :cond_2

    :cond_1
    move v0, v1

    :goto_1
    move v1, v0

    .line 227
    goto :goto_0

    .line 228
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    const-string/jumbo v0, "app:twitter_service:follow:update"

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lbij;->b()Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method
