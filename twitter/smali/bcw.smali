.class public Lbcw;
.super Lcom/twitter/library/service/s;
.source "Twttr"


# instance fields
.field private a:I

.field private b:J

.field private c:Ljava/lang/String;

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lbcw;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/s;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 23
    return-void
.end method


# virtual methods
.method public a(I)Lbcw;
    .locals 0

    .prologue
    .line 26
    iput p1, p0, Lbcw;->a:I

    .line 27
    return-object p0
.end method

.method public a(J)Lbcw;
    .locals 1

    .prologue
    .line 31
    iput-wide p1, p0, Lbcw;->b:J

    .line 32
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lbcw;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lbcw;->c:Ljava/lang/String;

    .line 37
    return-object p0
.end method

.method protected a_(Lcom/twitter/library/service/u;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 47
    new-instance v0, Lbcx;

    iget-object v1, p0, Lbcw;->p:Landroid/content/Context;

    invoke-virtual {p0}, Lbcw;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbcx;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V

    .line 48
    invoke-virtual {v0, v4}, Lbcx;->a(I)Lbcx;

    move-result-object v0

    iget v1, p0, Lbcw;->a:I

    .line 49
    invoke-virtual {v0, v1}, Lbcx;->c(I)Lbcx;

    move-result-object v0

    iget-wide v2, p0, Lbcw;->b:J

    .line 50
    invoke-virtual {v0, v2, v3}, Lbcx;->a(J)Lbcx;

    move-result-object v0

    iget-object v1, p0, Lbcw;->c:Ljava/lang/String;

    .line 51
    invoke-virtual {v0, v1}, Lbcx;->a(Ljava/lang/String;)Lbcx;

    move-result-object v0

    .line 52
    invoke-virtual {v0, v4}, Lbcx;->a(Z)Lbcx;

    move-result-object v0

    iget v1, p0, Lbcw;->g:I

    .line 53
    invoke-virtual {v0, v1}, Lbcx;->d(I)Lbcx;

    move-result-object v0

    .line 54
    invoke-virtual {v0, p0}, Lbcx;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/service/s;

    move-result-object v0

    check-cast v0, Lbcx;

    .line 56
    invoke-virtual {v0}, Lbcx;->O()Lcom/twitter/library/service/u;

    move-result-object v1

    .line 57
    invoke-virtual {v1}, Lcom/twitter/library/service/u;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 58
    invoke-virtual {v0}, Lbcx;->e()Z

    move-result v0

    .line 59
    new-instance v1, Lbcx;

    iget-object v2, p0, Lbcw;->p:Landroid/content/Context;

    invoke-virtual {p0}, Lbcw;->M()Lcom/twitter/library/service/v;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lbcx;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V

    const/4 v2, 0x2

    .line 60
    invoke-virtual {v1, v2}, Lbcx;->a(I)Lbcx;

    move-result-object v1

    iget v2, p0, Lbcw;->a:I

    .line 61
    invoke-virtual {v1, v2}, Lbcx;->c(I)Lbcx;

    move-result-object v1

    iget-wide v2, p0, Lbcw;->b:J

    .line 62
    invoke-virtual {v1, v2, v3}, Lbcx;->a(J)Lbcx;

    move-result-object v1

    iget-object v2, p0, Lbcw;->c:Ljava/lang/String;

    .line 63
    invoke-virtual {v1, v2}, Lbcx;->a(Ljava/lang/String;)Lbcx;

    move-result-object v1

    .line 64
    invoke-virtual {v1, v0}, Lbcx;->a(Z)Lbcx;

    move-result-object v0

    iget v1, p0, Lbcw;->g:I

    .line 65
    invoke-virtual {v0, v1}, Lbcx;->d(I)Lbcx;

    move-result-object v0

    .line 66
    invoke-virtual {v0, p0}, Lbcx;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/service/s;

    move-result-object v0

    check-cast v0, Lbcx;

    .line 67
    invoke-virtual {v0}, Lbcx;->O()Lcom/twitter/library/service/u;

    move-result-object v0

    .line 69
    :goto_0
    invoke-virtual {p1, v0}, Lcom/twitter/library/service/u;->a(Lcom/twitter/library/service/u;)V

    .line 70
    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public c(I)Lbcw;
    .locals 0

    .prologue
    .line 41
    iput p1, p0, Lbcw;->g:I

    .line 42
    return-object p0
.end method
