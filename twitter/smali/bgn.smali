.class public Lbgn;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lbgi;

.field private final b:Lbgj;

.field private final c:Lbgm;

.field private final d:Lbgx;


# direct methods
.method public constructor <init>(Landroid/content/Context;JLbwb;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "J",
            "Lbwb",
            "<",
            "Lcom/mopub/nativeads/NativeAd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Lbgi;

    invoke-direct {v0, p1, p2, p3, p4}, Lbgi;-><init>(Landroid/content/Context;JLbwb;)V

    iput-object v0, p0, Lbgn;->a:Lbgi;

    .line 45
    new-instance v0, Lbgj;

    invoke-direct {v0, p1, p2, p3}, Lbgj;-><init>(Landroid/content/Context;J)V

    iput-object v0, p0, Lbgn;->b:Lbgj;

    .line 46
    new-instance v0, Lbgm;

    invoke-direct {v0}, Lbgm;-><init>()V

    iput-object v0, p0, Lbgn;->c:Lbgm;

    .line 47
    new-instance v0, Lbgx;

    invoke-static {p2, p3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v1

    invoke-direct {v0, p1, p2, p3, v1}, Lbgx;-><init>(Landroid/content/Context;JLcom/twitter/library/provider/t;)V

    iput-object v0, p0, Lbgn;->d:Lbgx;

    .line 48
    return-void
.end method

.method private a(Lchn;Lcgx;Lchc;Lchx$b;)Lcgz;
    .locals 2

    .prologue
    .line 76
    instance-of v0, p1, Lcgr;

    if-eqz v0, :cond_0

    .line 77
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgr;

    .line 78
    iget-object v1, p0, Lbgn;->a:Lbgi;

    invoke-virtual {v1, v0, p2, p3, p4}, Lbgi;->a(Lcgr;Lcgx;Lchc;Lchx$b;)Lcgr$a;

    move-result-object v0

    .line 86
    :goto_0
    return-object v0

    .line 80
    :cond_0
    instance-of v0, p1, Lcgs;

    if-eqz v0, :cond_1

    .line 81
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgs;

    .line 82
    iget-object v1, p0, Lbgn;->b:Lbgj;

    invoke-virtual {v1, v0, p2, p3, p4}, Lbgj;->a(Lcgs;Lcgx;Lchc;Lchx$b;)Lcgz;

    move-result-object v0

    goto :goto_0

    .line 84
    :cond_1
    instance-of v0, p1, Lche;

    if-eqz v0, :cond_2

    .line 85
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lche;

    .line 86
    iget-object v1, p0, Lbgn;->c:Lbgm;

    invoke-virtual {v1, v0, p2, p3, p4}, Lbgm;->a(Lche;Lcgx;Lchc;Lchx$b;)Lche$a;

    move-result-object v0

    goto :goto_0

    .line 91
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Attempting to process an unrecognized TimelineInstruction"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(Lchx;Lcom/twitter/android/timeline/cf;)Lchd;
    .locals 6

    .prologue
    .line 58
    iget-object v0, p1, Lchx;->b:Lchf;

    iget-object v0, v0, Lchf;->b:Ljava/util/List;

    .line 59
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 58
    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 62
    iget-object v0, p0, Lbgn;->d:Lbgx;

    invoke-virtual {v0, p2}, Lbgx;->a(Lcom/twitter/android/timeline/cf;)V

    .line 65
    iget-object v0, p1, Lchx;->b:Lchf;

    iget-object v0, v0, Lchf;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchn;

    .line 66
    iget-object v3, p1, Lchx;->a:Lcgx;

    iget-object v4, p1, Lchx;->b:Lchf;

    iget-object v4, v4, Lchf;->c:Lchc;

    iget-object v5, p1, Lchx;->c:Lchx$b;

    invoke-direct {p0, v0, v3, v4, v5}, Lbgn;->a(Lchn;Lcgx;Lchc;Lchx$b;)Lcgz;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 69
    :cond_0
    new-instance v2, Lchd;

    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {v2, v0}, Lchd;-><init>(Ljava/util/List;)V

    return-object v2
.end method
