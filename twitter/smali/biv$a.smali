.class public final Lbiv$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbiv;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field volatile a:Z

.field volatile b:Z

.field private c:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/CountDownLatch;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    iput-boolean v0, p0, Lbiv$a;->a:Z

    .line 122
    iput-boolean v0, p0, Lbiv$a;->b:Z

    .line 133
    iput-object p1, p0, Lbiv$a;->c:Ljava/util/concurrent/CountDownLatch;

    .line 134
    return-void
.end method


# virtual methods
.method public a(Landroid/media/MediaCodecInfo;)V
    .locals 6
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 158
    invoke-virtual {p1}, Landroid/media/MediaCodecInfo;->isEncoder()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    :try_start_0
    const-string/jumbo v0, "video/avc"

    .line 166
    invoke-virtual {p1, v0}, Landroid/media/MediaCodecInfo;->getCapabilitiesForType(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;

    move-result-object v0

    .line 169
    iget-object v1, v0, Landroid/media/MediaCodecInfo$CodecCapabilities;->profileLevels:[Landroid/media/MediaCodecInfo$CodecProfileLevel;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 170
    iget v4, v3, Landroid/media/MediaCodecInfo$CodecProfileLevel;->profile:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 171
    const/4 v4, 0x1

    iput-boolean v4, p0, Lbiv$a;->a:Z

    .line 173
    :cond_2
    iget v3, v3, Landroid/media/MediaCodecInfo$CodecProfileLevel;->profile:I

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 174
    const/4 v3, 0x1

    iput-boolean v3, p0, Lbiv$a;->b:Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 177
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public run()V
    .locals 3
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 142
    invoke-static {}, Landroid/media/MediaCodecList;->getCodecCount()I

    move-result v1

    .line 143
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 144
    invoke-static {v0}, Landroid/media/MediaCodecList;->getCodecInfoAt(I)Landroid/media/MediaCodecInfo;

    move-result-object v2

    .line 145
    invoke-virtual {p0, v2}, Lbiv$a;->a(Landroid/media/MediaCodecInfo;)V

    .line 143
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 147
    :cond_0
    iget-object v0, p0, Lbiv$a;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 148
    return-void
.end method
