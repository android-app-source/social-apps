.class public Loj;
.super Lbiy;
.source "Twttr"


# instance fields
.field private final a:Lpg;

.field private final b:Lcom/twitter/library/av/playback/AVPlayer;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;)V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lpg;

    invoke-direct {v0}, Lpg;-><init>()V

    invoke-direct {p0, p1, v0}, Loj;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lpg;)V

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lpg;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Lbiy;-><init>()V

    .line 30
    iput-object p1, p0, Loj;->b:Lcom/twitter/library/av/playback/AVPlayer;

    .line 31
    iput-object p2, p0, Loj;->a:Lpg;

    .line 32
    return-void
.end method


# virtual methods
.method public a(Lbiw;)Z
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    return v0
.end method

.method public processTick(Lbki;)V
    .locals 4
    .annotation runtime Lbiz;
        a = Lbki;
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Loj;->a:Lpg;

    invoke-virtual {v0}, Lpg;->a()V

    .line 37
    iget-object v0, p0, Loj;->a:Lpg;

    invoke-virtual {v0}, Lpg;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x32

    rem-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p1, Lbki;->b:Lcom/twitter/library/av/playback/aa;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/aa;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 38
    :cond_0
    iget-object v0, p0, Loj;->b:Lcom/twitter/library/av/playback/AVPlayer;

    iget-object v1, p1, Lbki;->b:Lcom/twitter/library/av/playback/aa;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/playback/aa;)V

    .line 40
    :cond_1
    return-void
.end method
