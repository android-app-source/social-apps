.class public Lceg;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lceg$b;,
        Lceg$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lceg;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/twitter/model/moments/m;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lceg$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lceg$b;-><init>(Lceg$1;)V

    sput-object v0, Lceg;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lceg$a;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iget-object v0, p1, Lceg$a;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lceg;->b:Ljava/lang/String;

    .line 25
    iget-object v0, p1, Lceg$a;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lceg;->c:Ljava/lang/String;

    .line 26
    iget-object v0, p1, Lceg$a;->c:Lcom/twitter/model/moments/m;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/m;

    iput-object v0, p0, Lceg;->d:Lcom/twitter/model/moments/m;

    .line 27
    return-void
.end method

.method synthetic constructor <init>(Lceg$a;Lceg$1;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lceg;-><init>(Lceg$a;)V

    return-void
.end method
