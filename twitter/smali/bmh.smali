.class public Lbmh;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lbmg;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:J

.field private final g:Ljava/lang/String;

.field private final h:J

.field private final i:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;JJ)V
    .locals 2

    .prologue
    .line 30
    const-class v0, Lbmh;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 31
    iput-object p3, p0, Lbmh;->a:Ljava/lang/String;

    .line 32
    iput-object p4, p0, Lbmh;->b:Ljava/lang/String;

    .line 33
    iput-wide p5, p0, Lbmh;->c:J

    .line 34
    iput-object p7, p0, Lbmh;->g:Ljava/lang/String;

    .line 35
    iput-wide p8, p0, Lbmh;->h:J

    .line 36
    iput-wide p10, p0, Lbmh;->i:J

    .line 37
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 4

    .prologue
    .line 42
    invoke-virtual {p0}, Lbmh;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 43
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "offers"

    aput-object v3, v1, v2

    .line 44
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->b([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "card_url"

    iget-object v2, p0, Lbmh;->a:Ljava/lang/String;

    .line 45
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "offer_id"

    iget-object v2, p0, Lbmh;->b:Ljava/lang/String;

    .line 46
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "expiration_date"

    iget-wide v2, p0, Lbmh;->c:J

    .line 47
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "nonce"

    iget-object v2, p0, Lbmh;->g:Ljava/lang/String;

    .line 48
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "tweet_id"

    iget-wide v2, p0, Lbmh;->h:J

    .line 49
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "viewed_at"

    iget-wide v2, p0, Lbmh;->i:J

    .line 50
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lbmh;->p:Landroid/content/Context;

    invoke-static {v0, v1}, Lblx;->a(Lcom/twitter/library/service/d$a;Landroid/content/Context;)V

    .line 52
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lbmg;)V
    .locals 3

    .prologue
    .line 63
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p2, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "saveoffer_success_boolean"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 69
    :goto_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/u;->a(Lcom/twitter/network/HttpOperation;)V

    .line 70
    return-void

    .line 66
    :cond_0
    iget-object v0, p2, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "saveoffer_success_boolean"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 67
    invoke-static {p2, p3}, Lblx;->a(Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 13
    check-cast p3, Lbmg;

    invoke-virtual {p0, p1, p2, p3}, Lbmh;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lbmg;)V

    return-void
.end method

.method protected b()Lbmg;
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lbmg;

    invoke-direct {v0}, Lbmg;-><init>()V

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lbmh;->b()Lbmg;

    move-result-object v0

    return-object v0
.end method
