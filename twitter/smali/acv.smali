.class public Lacv;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lajq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lajq",
        "<",
        "Ladg;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/navigation/ah;

.field private final b:Lcom/twitter/android/moments/ui/maker/viewdelegate/n;

.field private final c:Lcom/twitter/android/moments/ui/maker/q;

.field private final d:Lzw;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/viewdelegate/n;Lcom/twitter/android/moments/ui/maker/q;Lzw;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lacv;->a:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    .line 41
    iput-object p2, p0, Lacv;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/n;

    .line 42
    iput-object p3, p0, Lacv;->c:Lcom/twitter/android/moments/ui/maker/q;

    .line 43
    iput-object p4, p0, Lacv;->d:Lzw;

    .line 44
    return-void
.end method

.method static a(Landroid/app/Activity;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/viewdelegate/n;Lcom/twitter/android/moments/ui/maker/q;J)Lacv;
    .locals 2

    .prologue
    .line 31
    .line 32
    invoke-static {p0, p4, p5}, Lzw;->a(Landroid/content/Context;J)Lzw;

    move-result-object v0

    .line 33
    new-instance v1, Lacv;

    invoke-direct {v1, p1, p2, p3, v0}, Lacv;-><init>(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/viewdelegate/n;Lcom/twitter/android/moments/ui/maker/q;Lzw;)V

    return-object v1
.end method

.method static synthetic a(Lacv;)Lzw;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lacv;->d:Lzw;

    return-object v0
.end method

.method static synthetic b(Lacv;)Lcom/twitter/android/moments/ui/maker/q;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lacv;->c:Lcom/twitter/android/moments/ui/maker/q;

    return-object v0
.end method

.method static synthetic c(Lacv;)Lcom/twitter/android/moments/ui/maker/navigation/ah;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lacv;->a:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    return-object v0
.end method


# virtual methods
.method public a(Ladg;)V
    .locals 3

    .prologue
    .line 48
    instance-of v0, p1, Ladh;

    if-eqz v0, :cond_0

    .line 49
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladh;

    .line 50
    invoke-virtual {v0}, Ladh;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 51
    invoke-static {}, Lcom/twitter/android/moments/viewmodels/l;->a()Lcom/twitter/android/moments/viewmodels/l;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/viewmodels/l;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/android/moments/viewmodels/k;

    move-result-object v1

    .line 52
    iget-object v2, p0, Lacv;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/n;

    invoke-interface {v1}, Lcom/twitter/android/moments/viewmodels/k;->a()Lcom/twitter/media/request/a$a;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/n;->a(Lcom/twitter/media/request/a$a;)V

    .line 53
    iget-object v1, p0, Lacv;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/n;

    new-instance v2, Lacv$1;

    invoke-direct {v2, p0, v0}, Lacv$1;-><init>(Lacv;Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/n;->a(Landroid/view/View$OnClickListener;)V

    .line 62
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Ladg;

    invoke-virtual {p0, p1}, Lacv;->a(Ladg;)V

    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lacv;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/n;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/n;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
