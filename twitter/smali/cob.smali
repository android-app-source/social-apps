.class public Lcob;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcoa;

.field private final b:Lcoc;

.field private volatile c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lcoa;

    invoke-direct {v0, p1}, Lcoa;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcoc;

    invoke-direct {v1, p1}, Lcoc;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0, v1}, Lcob;-><init>(Lcoa;Lcoc;)V

    .line 31
    return-void
.end method

.method constructor <init>(Lcoa;Lcoc;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcob;->a:Lcoa;

    .line 41
    iput-object p2, p0, Lcob;->b:Lcoc;

    .line 42
    invoke-virtual {p0}, Lcob;->b()I

    .line 43
    return-void
.end method

.method public static declared-synchronized a()Lcob;
    .locals 2

    .prologue
    .line 26
    const-class v1, Lcob;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcot;->ak()Lcot;

    move-result-object v0

    check-cast v0, Lcor;

    invoke-interface {v0}, Lcor;->C()Lcob;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public b()I
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcob;->c:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 51
    iget-object v0, p0, Lcob;->b:Lcoc;

    invoke-virtual {v0}, Lcoc;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcob;->c:Ljava/lang/Integer;

    .line 54
    iget-object v0, p0, Lcob;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 55
    iget-object v0, p0, Lcob;->a:Lcoa;

    invoke-virtual {v0}, Lcoa;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcob;->c:Ljava/lang/Integer;

    .line 56
    iget-object v0, p0, Lcob;->b:Lcoc;

    iget-object v1, p0, Lcob;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcoc;->a(I)V

    .line 60
    :cond_0
    iget-object v0, p0, Lcob;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method
