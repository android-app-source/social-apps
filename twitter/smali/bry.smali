.class public abstract Lbry;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final c:Lrx/f;

.field public final d:Lrx/f;


# direct methods
.method public constructor <init>(Lrx/f;Lrx/f;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lbry;->c:Lrx/f;

    .line 22
    iput-object p2, p0, Lbry;->d:Lrx/f;

    .line 23
    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation
.end method

.method public b(Ljava/lang/Object;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Lrx/c",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 27
    invoke-static {p1}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lbry;->c:Lrx/f;

    .line 28
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lbry$1;

    invoke-direct {v1, p0}, Lbry$1;-><init>(Lbry;)V

    .line 29
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lbry;->d:Lrx/f;

    .line 36
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 27
    return-object v0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)Lrx/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)",
            "Lrx/j;"
        }
    .end annotation

    .prologue
    .line 41
    invoke-static {p1}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lbry;->c:Lrx/f;

    .line 42
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lbry$2;

    invoke-direct {v1, p0, p2}, Lbry$2;-><init>(Lbry;Ljava/lang/Object;)V

    .line 43
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lbry;->d:Lrx/f;

    .line 51
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcqw;

    invoke-direct {v1}, Lcqw;-><init>()V

    .line 52
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    .line 41
    return-object v0
.end method
