.class public Lbwz;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbwz$a;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Z

.field private final e:Lbwz$a;

.field private final f:Landroid/content/res/Resources;

.field private final g:Lbwy;


# direct methods
.method public constructor <init>(Lbwz$a;Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lbwy;

    invoke-direct {v0}, Lbwy;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lbwz;-><init>(Lbwz$a;Landroid/content/res/Resources;Lbwy;)V

    .line 38
    return-void
.end method

.method constructor <init>(Lbwz$a;Landroid/content/res/Resources;Lbwy;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lbwz;->e:Lbwz$a;

    .line 44
    iput-object p2, p0, Lbwz;->f:Landroid/content/res/Resources;

    .line 45
    iput-object p3, p0, Lbwz;->g:Lbwy;

    .line 46
    return-void
.end method

.method private a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lbwz;->g:Lbwy;

    invoke-virtual {v0, p2}, Lbwy;->a(Ljava/lang/String;)Lbwy;

    move-result-object v0

    .line 141
    invoke-virtual {v0, p2}, Lbwy;->b(Ljava/lang/String;)Lbwy;

    move-result-object v0

    .line 142
    invoke-static {p1}, Lcom/twitter/ui/socialproof/a;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbwy;->a(I)Lbwy;

    .line 143
    return-void
.end method

.method private a(ILjava/lang/String;IILjava/lang/String;I)V
    .locals 8

    .prologue
    .line 151
    iget-object v0, p0, Lbwz;->f:Landroid/content/res/Resources;

    move v1, p1

    move-object v2, p2

    move-object v3, p5

    move v4, p3

    move v5, p4

    move v6, p6

    .line 154
    invoke-static/range {v0 .. v6}, Lcom/twitter/ui/socialproof/a;->a(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v7

    move v1, p1

    move-object v2, p2

    move-object v3, p5

    move v4, p3

    move v5, p4

    move v6, p6

    .line 156
    invoke-static/range {v0 .. v6}, Lcom/twitter/ui/socialproof/a;->b(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v0

    .line 158
    invoke-static {p1}, Lcom/twitter/ui/socialproof/a;->a(I)I

    move-result v1

    .line 159
    iget-object v2, p0, Lbwz;->g:Lbwy;

    invoke-virtual {v2, v7}, Lbwy;->a(Ljava/lang/String;)Lbwy;

    move-result-object v2

    .line 160
    invoke-virtual {v2, v0}, Lbwy;->b(Ljava/lang/String;)Lbwy;

    move-result-object v0

    .line 161
    invoke-virtual {v0, v1}, Lbwy;->a(I)Lbwy;

    .line 162
    return-void
.end method

.method private a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;J)Z
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 120
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->aa()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->b:J

    cmp-long v0, p3, v0

    if-eqz v0, :cond_1

    .line 121
    const/4 v7, 0x1

    .line 122
    const/16 v1, 0xd

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->e()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move v4, v3

    move v6, v3

    invoke-direct/range {v0 .. v6}, Lbwz;->a(ILjava/lang/String;IILjava/lang/String;I)V

    move v3, v7

    .line 132
    :cond_0
    :goto_0
    return v3

    .line 124
    :cond_1
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet;->c:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lbxd;->e(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 125
    iget-boolean v7, p2, Lcom/twitter/ui/view/h;->h:Z

    .line 126
    if-eqz v7, :cond_2

    .line 127
    const/16 v1, 0x2c

    move-object v0, p0

    move-object v2, v5

    move v4, v3

    move v6, v3

    invoke-direct/range {v0 .. v6}, Lbwz;->a(ILjava/lang/String;IILjava/lang/String;I)V

    move v3, v7

    goto :goto_0

    :cond_2
    move v3, v7

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lbwz;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 178
    iput p1, p0, Lbwz;->b:I

    .line 179
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;JZ)V
    .locals 7

    .prologue
    .line 50
    iget-object v0, p0, Lbwz;->g:Lbwy;

    invoke-virtual {v0}, Lbwy;->a()V

    .line 51
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet;->c:Z

    .line 53
    iget-boolean v1, p2, Lcom/twitter/ui/view/h;->a:Z

    if-eqz v1, :cond_1

    .line 54
    const/16 v1, 0x29

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lbwz;->a(ILjava/lang/String;IILjava/lang/String;I)V

    .line 114
    :cond_0
    :goto_0
    iget-object v0, p0, Lbwz;->e:Lbwz$a;

    iget-object v1, p0, Lbwz;->g:Lbwy;

    invoke-interface {v0, v1}, Lbwz$a;->setSocialProofData(Lbwy;)V

    .line 115
    return-void

    .line 55
    :cond_1
    iget-object v1, p0, Lbwz;->a:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lbwz;->b:I

    if-eqz v1, :cond_2

    .line 56
    iget-object v0, p0, Lbwz;->g:Lbwy;

    iget-object v1, p0, Lbwz;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbwy;->a(Ljava/lang/String;)Lbwy;

    move-result-object v0

    iget v1, p0, Lbwz;->b:I

    invoke-virtual {v0, v1}, Lbwy;->a(I)Lbwy;

    goto :goto_0

    .line 57
    :cond_2
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->t()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 58
    invoke-direct {p0, p1, p2, p3, p4}, Lbwz;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;J)Z

    goto :goto_0

    .line 59
    :cond_3
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->Z()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->s()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 60
    :cond_4
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    iget-object v2, v0, Lcgi;->f:Ljava/lang/String;

    .line 62
    if-eqz v2, :cond_0

    .line 63
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    invoke-virtual {v0}, Lcgi;->f()I

    move-result v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lbwz;->a(ILjava/lang/String;IILjava/lang/String;I)V

    goto :goto_0

    .line 67
    :cond_5
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->H()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-boolean v1, p2, Lcom/twitter/ui/view/h;->h:Z

    if-eqz v1, :cond_6

    if-nez v0, :cond_7

    .line 71
    :cond_6
    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->i:Ljava/lang/String;

    .line 72
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    iget v1, p1, Lcom/twitter/model/core/Tweet;->h:I

    invoke-direct {p0, v1, v0}, Lbwz;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 74
    :cond_7
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->z()Z

    move-result v1

    if-eqz v1, :cond_8

    if-nez v0, :cond_8

    .line 75
    invoke-direct {p0, p1, p2, p3, p4}, Lbwz;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;J)Z

    goto :goto_0

    .line 76
    :cond_8
    invoke-static {p1}, Lbxd;->c(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 77
    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->g:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 78
    const/16 v1, 0x23

    iget-object v2, p1, Lcom/twitter/model/core/Tweet;->g:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lbwz;->a(ILjava/lang/String;IILjava/lang/String;I)V

    goto/16 :goto_0

    .line 81
    :cond_9
    const/16 v1, 0x1d

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lbwz;->a(ILjava/lang/String;IILjava/lang/String;I)V

    goto/16 :goto_0

    .line 83
    :cond_a
    invoke-static {p1}, Lbxd;->d(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 84
    iget v1, p1, Lcom/twitter/model/core/Tweet;->f:I

    iget-object v2, p1, Lcom/twitter/model/core/Tweet;->g:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lbwz;->a(ILjava/lang/String;IILjava/lang/String;I)V

    goto/16 :goto_0

    .line 85
    :cond_b
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet;->K:Z

    if-eqz v0, :cond_c

    .line 86
    const/16 v1, 0x16

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lbwz;->a(ILjava/lang/String;IILjava/lang/String;I)V

    goto/16 :goto_0

    .line 87
    :cond_c
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet;->ab:Z

    if-eqz v0, :cond_d

    .line 88
    const/16 v1, 0x14

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lbwz;->a(ILjava/lang/String;IILjava/lang/String;I)V

    goto/16 :goto_0

    .line 89
    :cond_d
    invoke-direct {p0, p1, p2, p3, p4}, Lbwz;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->p()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->C()Z

    move-result v0

    if-eqz v0, :cond_f

    if-nez p5, :cond_f

    .line 93
    invoke-static {}, Lbpj;->b()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 94
    const/16 v0, 0x2e

    iget-object v1, p0, Lbwz;->f:Landroid/content/res/Resources;

    .line 96
    invoke-static {p1, p3, p4, v1}, Lbxo;->a(Lcom/twitter/model/core/Tweet;JLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    .line 95
    invoke-static {v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 94
    invoke-direct {p0, v0, v1}, Lbwz;->a(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 97
    :cond_e
    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/twitter/model/core/Tweet;->f:I

    const/16 v1, 0x18

    if-ne v0, v1, :cond_0

    .line 98
    const/16 v1, 0x18

    iget-object v2, p1, Lcom/twitter/model/core/Tweet;->g:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lbwz;->a(ILjava/lang/String;IILjava/lang/String;I)V

    goto/16 :goto_0

    .line 101
    :cond_f
    iget-boolean v0, p2, Lcom/twitter/ui/view/h;->c:Z

    if-nez v0, :cond_10

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->p()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-static {p1}, Lbxd;->e(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->D:Ljava/lang/String;

    if-eqz v0, :cond_10

    if-nez p5, :cond_10

    .line 103
    const/16 v1, 0x18

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "@"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p1, Lcom/twitter/model/core/Tweet;->D:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lbwz;->a(ILjava/lang/String;IILjava/lang/String;I)V

    goto/16 :goto_0

    .line 105
    :cond_10
    iget-boolean v0, p0, Lbwz;->d:Z

    if-eqz v0, :cond_11

    iget v0, p1, Lcom/twitter/model/core/Tweet;->f:I

    if-lez v0, :cond_11

    .line 106
    iget v1, p1, Lcom/twitter/model/core/Tweet;->f:I

    iget-object v2, p1, Lcom/twitter/model/core/Tweet;->g:Ljava/lang/String;

    iget v3, p1, Lcom/twitter/model/core/Tweet;->W:I

    iget v4, p1, Lcom/twitter/model/core/Tweet;->Y:I

    iget-object v5, p1, Lcom/twitter/model/core/Tweet;->Z:Ljava/lang/String;

    iget v6, p1, Lcom/twitter/model/core/Tweet;->X:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lbwz;->a(ILjava/lang/String;IILjava/lang/String;I)V

    goto/16 :goto_0

    .line 109
    :cond_11
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbsd;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbwz;->c:Ljava/lang/String;

    .line 110
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    const/16 v1, 0x2b

    iget-object v2, p0, Lbwz;->c:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lbwz;->a(ILjava/lang/String;IILjava/lang/String;I)V

    .line 112
    iget-object v0, p0, Lbwz;->g:Lbwy;

    iget-object v1, p0, Lbwz;->f:Landroid/content/res/Resources;

    sget v2, Lazw$d;->twitter_blue:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbwy;->b(I)Lbwy;

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lbwz;->a:Ljava/lang/String;

    .line 171
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 186
    iput-boolean p1, p0, Lbwz;->d:Z

    .line 187
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lbwz;->b:I

    return v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lbwz;->c:Ljava/lang/String;

    .line 183
    return-void
.end method
