.class public Laeu;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Lcom/twitter/model/json/people/JsonModule;)Lcom/twitter/model/people/b;
    .locals 2

    .prologue
    .line 213
    new-instance v0, Lcom/twitter/model/people/e$a;

    invoke-direct {v0}, Lcom/twitter/model/people/e$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/people/JsonModule;->b:Ljava/lang/String;

    .line 214
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/e$a;->b(Ljava/lang/String;)Lcom/twitter/model/people/e$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/people/JsonModule;->a:Ljava/lang/String;

    .line 215
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/e$a;->a(Ljava/lang/String;)Lcom/twitter/model/people/e$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/people/JsonModule;->d:Lcom/twitter/model/json/people/JsonModuleLayout;

    .line 216
    invoke-static {v1}, Laeu;->a(Lcom/twitter/model/json/people/JsonModuleLayout;)Lcom/twitter/model/people/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/e$a;->a(Lcom/twitter/model/people/f;)Lcom/twitter/model/people/e$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/people/JsonModule;->c:Ljava/lang/String;

    .line 217
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/e$a;->c(Ljava/lang/String;)Lcom/twitter/model/people/e$a;

    move-result-object v0

    .line 218
    invoke-virtual {v0}, Lcom/twitter/model/people/e$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/b;

    .line 213
    return-object v0
.end method

.method public static a(Lcom/twitter/model/people/b;Ljava/util/Map;Ljava/util/Map;)Lcom/twitter/model/people/b;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/people/b;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/Tweet;",
            ">;)",
            "Lcom/twitter/model/people/b;"
        }
    .end annotation

    .prologue
    .line 68
    new-instance v0, Lcom/twitter/model/people/e$a;

    invoke-direct {v0}, Lcom/twitter/model/people/e$a;-><init>()V

    .line 69
    invoke-interface {p0}, Lcom/twitter/model/people/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/e$a;->b(Ljava/lang/String;)Lcom/twitter/model/people/e$a;

    move-result-object v0

    .line 70
    invoke-interface {p0}, Lcom/twitter/model/people/b;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/e$a;->a(Ljava/lang/String;)Lcom/twitter/model/people/e$a;

    move-result-object v0

    .line 71
    invoke-interface {p0}, Lcom/twitter/model/people/b;->c()Lcom/twitter/model/people/f;

    move-result-object v1

    invoke-static {v1, p1, p2}, Laeu;->a(Lcom/twitter/model/people/f;Ljava/util/Map;Ljava/util/Map;)Lcom/twitter/model/people/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/e$a;->a(Lcom/twitter/model/people/f;)Lcom/twitter/model/people/e$a;

    move-result-object v0

    .line 72
    invoke-interface {p0}, Lcom/twitter/model/people/b;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/e$a;->c(Ljava/lang/String;)Lcom/twitter/model/people/e$a;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lcom/twitter/model/people/e$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/b;

    .line 68
    return-object v0
.end method

.method private static a(Lcom/twitter/model/people/c;Ljava/util/Map;)Lcom/twitter/model/people/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/people/c;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)",
            "Lcom/twitter/model/people/c;"
        }
    .end annotation

    .prologue
    .line 162
    if-nez p0, :cond_0

    .line 163
    const/4 v0, 0x0

    .line 165
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/model/people/c;

    iget-object v1, p0, Lcom/twitter/model/people/c;->c:Ljava/util/List;

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Laeu$4;

    invoke-direct {v2, p1}, Laeu$4;-><init>(Ljava/util/Map;)V

    invoke-static {v1, v2}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-static {v1}, Lcpt;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/model/people/c;-><init>(Ljava/util/List;)V

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/people/d;Ljava/util/Map;)Lcom/twitter/model/people/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/people/d;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)",
            "Lcom/twitter/model/people/d;"
        }
    .end annotation

    .prologue
    .line 147
    if-nez p0, :cond_0

    .line 148
    const/4 v0, 0x0

    .line 150
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/model/people/d$a;

    invoke-direct {v0}, Lcom/twitter/model/people/d$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/people/d;->c:Lcom/twitter/model/people/ModuleTitle;

    .line 151
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/d$a;->a(Lcom/twitter/model/people/ModuleTitle;)Lcom/twitter/model/people/d$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/people/d;->d:Lcom/twitter/model/people/ModuleTitle;

    .line 152
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/d$a;->b(Lcom/twitter/model/people/ModuleTitle;)Lcom/twitter/model/people/d$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/people/d;->e:Lcom/twitter/model/people/k;

    .line 153
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/d$a;->a(Lcom/twitter/model/people/k;)Lcom/twitter/model/people/d$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/people/d;->f:Lcom/twitter/model/people/c;

    .line 154
    invoke-static {v1, p1}, Laeu;->a(Lcom/twitter/model/people/c;Ljava/util/Map;)Lcom/twitter/model/people/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/d$a;->a(Lcom/twitter/model/people/c;)Lcom/twitter/model/people/d$a;

    move-result-object v0

    .line 155
    invoke-virtual {v0}, Lcom/twitter/model/people/d$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/d;

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/json/people/JsonModuleLayout;)Lcom/twitter/model/people/f;
    .locals 3

    .prologue
    .line 223
    new-instance v0, Lcom/twitter/model/people/f;

    iget-object v1, p0, Lcom/twitter/model/json/people/JsonModuleLayout;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/model/json/people/JsonModuleLayout;->b:Lcom/twitter/model/json/people/JsonModuleLayoutContent;

    invoke-static {v2}, Laeu;->a(Lcom/twitter/model/json/people/JsonModuleLayoutContent;)Lcom/twitter/model/people/g;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/people/f;-><init>(Ljava/lang/String;Lcom/twitter/model/people/g;)V

    return-object v0
.end method

.method public static a(Lcom/twitter/model/people/f;Ljava/util/Map;Ljava/util/Map;)Lcom/twitter/model/people/f;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/people/f;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/Tweet;",
            ">;)",
            "Lcom/twitter/model/people/f;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/model/people/f;->c:Lcom/twitter/model/people/g;

    .line 81
    invoke-static {v0, p1, p2}, Laeu;->a(Lcom/twitter/model/people/g;Ljava/util/Map;Ljava/util/Map;)Lcom/twitter/model/people/g$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/people/g$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/g;

    .line 82
    new-instance v1, Lcom/twitter/model/people/f;

    iget-object v2, p0, Lcom/twitter/model/people/f;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/twitter/model/people/f;-><init>(Ljava/lang/String;Lcom/twitter/model/people/g;)V

    return-object v1
.end method

.method public static a(Lcom/twitter/model/people/g;Ljava/util/Map;Ljava/util/Map;)Lcom/twitter/model/people/g$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/people/g;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/Tweet;",
            ">;)",
            "Lcom/twitter/model/people/g$a;"
        }
    .end annotation

    .prologue
    .line 89
    invoke-static {p0, p1, p2}, Laeu;->a(Lcom/twitter/model/people/h;Ljava/util/Map;Ljava/util/Map;)Lcom/twitter/model/people/g$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/people/g;->h:Ljava/lang/Iterable;

    new-instance v2, Laeu$2;

    invoke-direct {v2, p1, p2}, Laeu$2;-><init>(Ljava/util/Map;Ljava/util/Map;)V

    .line 90
    invoke-static {v1, v2}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/g$a;->c(Ljava/lang/Iterable;)Lcom/twitter/model/people/g$a;

    move-result-object v0

    .line 89
    return-object v0
.end method

.method public static a(Lcom/twitter/model/people/h;Ljava/util/Map;Ljava/util/Map;)Lcom/twitter/model/people/g$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/people/h;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/Tweet;",
            ">;)",
            "Lcom/twitter/model/people/g$a;"
        }
    .end annotation

    .prologue
    .line 105
    new-instance v0, Lcom/twitter/model/people/g$a;

    invoke-direct {v0}, Lcom/twitter/model/people/g$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/people/h;->a:Lcom/twitter/model/people/d;

    .line 106
    invoke-static {v1, p1}, Laeu;->a(Lcom/twitter/model/people/d;Ljava/util/Map;)Lcom/twitter/model/people/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/g$a;->a(Lcom/twitter/model/people/d;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/h$a;

    iget-object v1, p0, Lcom/twitter/model/people/h;->b:Ljava/lang/String;

    .line 107
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/h$a;->a(Ljava/lang/String;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/h$a;

    iget-object v1, p0, Lcom/twitter/model/people/h;->c:Lcom/twitter/model/people/k;

    .line 108
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/h$a;->a(Lcom/twitter/model/people/k;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/h$a;

    iget-object v1, p0, Lcom/twitter/model/people/h;->d:Ljava/util/List;

    .line 109
    invoke-static {v1, p1}, Laeu;->a(Ljava/lang/Iterable;Ljava/util/Map;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/h$a;->a(Ljava/lang/Iterable;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/h$a;

    iget-object v1, p0, Lcom/twitter/model/people/h;->e:Ljava/util/List;

    .line 110
    invoke-static {v1, p2}, Laeu;->b(Ljava/lang/Iterable;Ljava/util/Map;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/h$a;->b(Ljava/lang/Iterable;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/h$a;

    iget-object v1, p0, Lcom/twitter/model/people/h;->f:Lcom/twitter/model/people/i;

    .line 111
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/h$a;->a(Lcom/twitter/model/people/i;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/g$a;

    .line 105
    return-object v0
.end method

.method public static a(Lcom/twitter/model/json/people/JsonModuleLayoutContent;)Lcom/twitter/model/people/g;
    .locals 2

    .prologue
    .line 228
    new-instance v0, Lcom/twitter/model/people/g$a;

    invoke-direct {v0}, Lcom/twitter/model/people/g$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/model/json/people/JsonModuleLayoutContent;->a:Ljava/util/List;

    .line 229
    invoke-static {v1}, Laeu;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/g$a;->c(Ljava/lang/Iterable;)Lcom/twitter/model/people/g$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/model/json/people/JsonModuleLayoutContent;->b:Lcom/twitter/model/people/d;

    .line 230
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/g$a;->a(Lcom/twitter/model/people/d;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/h$a;

    iget-object v1, p0, Lcom/twitter/model/json/people/JsonModuleLayoutContent;->c:Ljava/lang/String;

    .line 231
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/h$a;->a(Ljava/lang/String;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/h$a;

    iget-object v1, p0, Lcom/twitter/model/json/people/JsonModuleLayoutContent;->d:Lcom/twitter/model/people/k;

    .line 232
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/h$a;->a(Lcom/twitter/model/people/k;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/h$a;

    iget-object v1, p0, Lcom/twitter/model/json/people/JsonModuleLayoutContent;->e:Ljava/util/List;

    .line 233
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/h$a;->a(Ljava/lang/Iterable;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/h$a;

    iget-object v1, p0, Lcom/twitter/model/json/people/JsonModuleLayoutContent;->f:Ljava/util/List;

    .line 234
    invoke-static {v1}, Laeu;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/people/h$a;->b(Ljava/lang/Iterable;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/h$a;

    iget-object v1, p0, Lcom/twitter/model/json/people/JsonModuleLayoutContent;->h:Lcom/twitter/model/people/i;

    .line 235
    invoke-virtual {v0, v1}, Lcom/twitter/model/people/h$a;->a(Lcom/twitter/model/people/i;)Lcom/twitter/model/people/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/h$a;

    .line 236
    invoke-virtual {v0}, Lcom/twitter/model/people/h$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/g;

    .line 228
    return-object v0
.end method

.method public static a(Lcom/twitter/model/json/people/JsonPeopleDiscoveryResponse;)Lcom/twitter/model/people/j;
    .locals 4

    .prologue
    .line 38
    new-instance v0, Lcom/twitter/model/people/j;

    iget-object v1, p0, Lcom/twitter/model/json/people/JsonPeopleDiscoveryResponse;->a:Ljava/util/List;

    invoke-static {v1}, Laeu;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/model/json/people/JsonPeopleDiscoveryResponse;->b:J

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/model/people/j;-><init>(Ljava/util/List;J)V

    return-object v0
.end method

.method private static a(Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/json/people/JsonModulePage;",
            ">;)",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/people/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 242
    if-nez p0, :cond_0

    .line 243
    const/4 v0, 0x0

    .line 245
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Laeu$7;

    invoke-direct {v0}, Laeu$7;-><init>()V

    invoke-static {p0, v0}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcpt;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Iterable;Ljava/util/Map;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/people/l;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/people/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    if-nez p0, :cond_0

    .line 119
    const/4 v0, 0x0

    .line 121
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Laeu$3;

    invoke-direct {v0, p1}, Laeu$3;-><init>(Ljava/util/Map;)V

    invoke-static {p0, v0}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcpt;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/json/people/JsonModule;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201
    new-instance v0, Laeu$6;

    invoke-direct {v0}, Laeu$6;-><init>()V

    invoke-static {p0, v0}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcpt;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/Tweet;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    invoke-static {}, Lcom/twitter/util/collection/i;->f()Ljava/util/Map;

    move-result-object v0

    invoke-static {p0, v0, p1}, Laeu;->a(Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/Tweet;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    new-instance v0, Laeu$1;

    invoke-direct {v0, p1, p2}, Laeu$1;-><init>(Ljava/util/Map;Ljava/util/Map;)V

    invoke-static {p0, v0}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcpt;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/Iterable;Ljava/util/Map;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/Tweet;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    if-nez p0, :cond_0

    .line 187
    const/4 v0, 0x0

    .line 189
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Laeu$5;

    invoke-direct {v0, p1}, Laeu$5;-><init>(Ljava/util/Map;)V

    invoke-static {p0, v0}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcpt;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 266
    if-nez p0, :cond_0

    .line 267
    const/4 v0, 0x0

    .line 269
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Laeu$8;

    invoke-direct {v0}, Laeu$8;-><init>()V

    invoke-static {p0, v0}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    invoke-static {}, Lcom/twitter/util/collection/i;->f()Ljava/util/Map;

    move-result-object v0

    invoke-static {p0, p1, v0}, Laeu;->a(Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
