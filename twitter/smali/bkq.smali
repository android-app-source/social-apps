.class public abstract Lbkq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbyp;


# instance fields
.field protected final a:Lcom/twitter/library/av/playback/AVDataSource;

.field private final b:Lcom/twitter/library/av/model/parser/c;


# direct methods
.method protected constructor <init>(Lcom/twitter/library/av/playback/AVDataSource;)V
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/twitter/library/av/model/parser/c;->a:Lcom/twitter/library/av/model/parser/c;

    invoke-direct {p0, p1, v0}, Lbkq;-><init>(Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/library/av/model/parser/c;)V

    .line 47
    return-void
.end method

.method protected constructor <init>(Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/library/av/model/parser/c;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lbkq;->a:Lcom/twitter/library/av/playback/AVDataSource;

    .line 52
    iput-object p2, p0, Lbkq;->b:Lcom/twitter/library/av/model/parser/c;

    .line 53
    return-void
.end method

.method public static a(ILjava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 288
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string/jumbo v1, "Network error. status code: %d reason: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 247
    const-string/jumbo v0, "Twitter-android/%s Android/%d (%s)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Lbkq;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 248
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    .line 247
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static a(Lcom/twitter/network/HttpOperation;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 275
    .line 276
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/network/HttpOperation;->m()Lcom/twitter/network/l;

    move-result-object v1

    .line 277
    :goto_0
    if-eqz v1, :cond_0

    .line 278
    iget-object v0, v1, Lcom/twitter/network/l;->c:Ljava/lang/Exception;

    .line 279
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 281
    :goto_1
    iget v1, v1, Lcom/twitter/network/l;->a:I

    invoke-static {v1, v0}, Lbkq;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 284
    :cond_0
    return-object v0

    :cond_1
    move-object v1, v0

    .line 276
    goto :goto_0

    .line 279
    :cond_2
    iget-object v0, v1, Lcom/twitter/network/l;->b:Ljava/lang/String;

    goto :goto_1
.end method

.method public static d(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 236
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lbkq;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static e(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 259
    .line 260
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 261
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 266
    :goto_0
    return-object v0

    .line 262
    :catch_0
    move-exception v0

    .line 263
    const-string/jumbo v0, ""

    goto :goto_0
.end method


# virtual methods
.method protected a(Ljava/lang/String;Ljava/util/Map;)Landroid/net/Uri$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/net/Uri$Builder;"
        }
    .end annotation

    .prologue
    .line 153
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 154
    invoke-virtual {p0, v0, p2}, Lbkq;->a(Landroid/net/Uri$Builder;Ljava/util/Map;)V

    .line 155
    return-object v0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/av/g;)Lcom/twitter/model/av/AVMediaPlaylist;
    .locals 5

    .prologue
    .line 68
    iget-object v0, p0, Lbkq;->a:Lcom/twitter/library/av/playback/AVDataSource;

    .line 69
    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->g()Ljava/lang/String;

    move-result-object v1

    .line 70
    invoke-virtual {p0, p1}, Lbkq;->a(Landroid/content/Context;)Lcom/twitter/network/j;

    move-result-object v2

    .line 72
    invoke-virtual {p0, p1}, Lbkq;->c(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v3

    .line 74
    invoke-virtual {p0, p2}, Lbkq;->a(Lcom/twitter/library/av/g;)Lcom/twitter/model/av/DynamicAdInfo;

    move-result-object v4

    .line 76
    invoke-virtual {p0, v1, v3}, Lbkq;->a(Ljava/lang/String;Ljava/util/Map;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 78
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->i()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, p1, v1, v0}, Lbkq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 80
    invoke-virtual {p0, p1, v3, v2, v0}, Lbkq;->a(Landroid/content/Context;Ljava/util/Map;Lcom/twitter/network/j;Ljava/lang/String;)Lcom/twitter/network/HttpOperation;

    move-result-object v0

    .line 81
    invoke-virtual {p0, v2, v0, v3, v4}, Lbkq;->b(Lcom/twitter/network/j;Lcom/twitter/network/HttpOperation;Ljava/util/Map;Lcom/twitter/model/av/DynamicAdInfo;)Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/library/av/g;)Lcom/twitter/model/av/DynamicAdInfo;
    .locals 1

    .prologue
    .line 86
    invoke-static {}, Lcom/twitter/library/av/v;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lbkq;->a:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-virtual {p1, v0}, Lcom/twitter/library/av/g;->a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/model/av/DynamicAdInfo;

    move-result-object v0

    .line 89
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/model/av/DynamicAdInfo;Lcom/twitter/util/network/c;)Lcom/twitter/model/av/Video;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 95
    if-eqz p1, :cond_1

    iget-object v1, p1, Lcom/twitter/model/av/DynamicAdInfo;->a:Lcom/twitter/model/av/DynamicAd;

    .line 96
    :goto_0
    if-eqz v1, :cond_0

    .line 97
    invoke-virtual {v1}, Lcom/twitter/model/av/DynamicAd;->c()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Lbkq;->a(Lcom/twitter/util/network/c;Ljava/util/List;)Lcom/twitter/util/collection/k;

    move-result-object v0

    const-string/jumbo v2, ""

    invoke-virtual {v0, v2}, Lcom/twitter/util/collection/k;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/model/av/DynamicAd;->a(Ljava/lang/String;)Lcom/twitter/model/av/Video;

    move-result-object v0

    .line 96
    :cond_0
    return-object v0

    :cond_1
    move-object v1, v0

    .line 95
    goto :goto_0
.end method

.method protected a(Landroid/content/Context;Ljava/util/Map;Lcom/twitter/network/j;Ljava/lang/String;)Lcom/twitter/network/HttpOperation;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/twitter/network/j;",
            "Ljava/lang/String;",
            ")",
            "Lcom/twitter/network/HttpOperation;"
        }
    .end annotation

    .prologue
    .line 212
    new-instance v0, Lcom/twitter/library/network/k;

    invoke-direct {v0, p1, p4}, Lcom/twitter/library/network/k;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 213
    invoke-virtual {v0, p3}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/j;)Lcom/twitter/library/network/k;

    move-result-object v0

    .line 214
    invoke-virtual {v0}, Lcom/twitter/library/network/k;->a()Lcom/twitter/network/HttpOperation;

    move-result-object v2

    .line 216
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 217
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/twitter/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/network/HttpOperation;

    goto :goto_0

    .line 220
    :cond_0
    invoke-virtual {v2}, Lcom/twitter/network/HttpOperation;->c()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a(Landroid/content/Context;)Lcom/twitter/network/j;
.end method

.method protected a(Lcom/twitter/util/network/c;Ljava/util/List;)Lcom/twitter/util/collection/k;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/network/c;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/p;",
            ">;)",
            "Lcom/twitter/util/collection/k",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226
    iget-object v0, p0, Lbkq;->b:Lcom/twitter/library/av/model/parser/c;

    invoke-virtual {v0, p2, p1}, Lcom/twitter/library/av/model/parser/c;->a(Ljava/util/List;Lcom/twitter/util/network/c;)Lcom/twitter/util/collection/k;

    move-result-object v0

    return-object v0
.end method

.method protected a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 198
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v0

    invoke-virtual {v0}, Lcrr;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "phone"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "tablet"

    goto :goto_0
.end method

.method protected a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 167
    return-object p2
.end method

.method protected abstract a(Landroid/content/Context;Ljava/util/Map;Lcom/twitter/util/network/c;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/twitter/util/network/c;",
            ")V"
        }
    .end annotation
.end method

.method protected abstract a(Landroid/net/Uri$Builder;Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri$Builder;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public b(Landroid/content/Context;)Lcom/twitter/model/av/AVMediaPlaylist;
    .locals 1

    .prologue
    .line 58
    invoke-static {p1}, Lcom/twitter/library/av/g;->a(Landroid/content/Context;)Lcom/twitter/library/av/g;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lbkq;->a(Landroid/content/Context;Lcom/twitter/library/av/g;)Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v0

    return-object v0
.end method

.method protected abstract b(Lcom/twitter/network/j;Lcom/twitter/network/HttpOperation;Ljava/util/Map;Lcom/twitter/model/av/DynamicAdInfo;)Lcom/twitter/model/av/AVMediaPlaylist;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/j;",
            "Lcom/twitter/network/HttpOperation;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/twitter/model/av/DynamicAdInfo;",
            ")",
            "Lcom/twitter/model/av/AVMediaPlaylist;"
        }
    .end annotation
.end method

.method protected c(Landroid/content/Context;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 177
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 179
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v1

    invoke-virtual {v1}, Lcrr;->e()Lcom/twitter/util/network/c;

    move-result-object v1

    .line 182
    const-string/jumbo v2, "User-Agent"

    invoke-static {p1}, Lbkq;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    const-string/jumbo v2, "Twitter-Player"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    const-string/jumbo v2, "X-CDN-DEVICE"

    invoke-virtual {p0}, Lbkq;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    const-string/jumbo v2, "Network-Type"

    iget-object v3, v1, Lcom/twitter/util/network/c;->b:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    invoke-virtual {p0, p1, v0, v1}, Lbkq;->a(Landroid/content/Context;Ljava/util/Map;Lcom/twitter/util/network/c;)V

    .line 194
    return-object v0
.end method
