.class public Lbxq;
.super Lbxn;
.source "Twttr"


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Landroid/content/res/Resources;

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/widget/TextView;Landroid/content/res/Resources;I)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lbxn;-><init>()V

    .line 21
    iput-object p1, p0, Lbxq;->a:Landroid/widget/TextView;

    .line 22
    iput-object p2, p0, Lbxq;->b:Landroid/content/res/Resources;

    .line 23
    iput p3, p0, Lbxq;->c:I

    .line 24
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/Tweet;JLbxm$a;)V
    .locals 8

    .prologue
    .line 28
    iget-object v4, p0, Lbxq;->b:Landroid/content/res/Resources;

    iget v6, p0, Lbxq;->c:I

    move-object v1, p1

    move-wide v2, p2

    move-object v5, p4

    .line 29
    invoke-static/range {v1 .. v6}, Lbxo;->a(Lcom/twitter/model/core/Tweet;JLandroid/content/res/Resources;Lbxm$a;I)Lbxi;

    move-result-object v0

    .line 31
    iget-object v1, p0, Lbxq;->a:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/twitter/ui/view/g;->a(Landroid/widget/TextView;)V

    .line 32
    invoke-virtual {p0, v0}, Lbxq;->a(Ljava/lang/CharSequence;)V

    .line 33
    return-void
.end method

.method protected a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lbxq;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 38
    iget-object v1, p0, Lbxq;->a:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 39
    return-void

    .line 38
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
