.class public Lbfg;
.super Lbao;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbao",
        "<",
        "Lcom/twitter/library/api/y;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Z

.field private c:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/twitter/library/service/v;

    invoke-direct {v0, p2}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    invoke-direct {p0, p1, v0, p3}, Lbfg;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Ljava/lang/String;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 33
    const-class v0, Lbfg;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, p2, v1}, Lbao;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;Z)V

    .line 26
    sget-object v0, Lcom/twitter/model/core/z;->b:[I

    iput-object v0, p0, Lbfg;->c:[I

    .line 34
    iput-object p3, p0, Lbfg;->a:Ljava/lang/String;

    .line 35
    iget-object v0, p0, Lbfg;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 38
    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0, p1, p2, p3}, Lbao;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V

    .line 69
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    invoke-virtual {p3}, Lcom/twitter/library/api/y;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lbfg;->b:Z

    .line 74
    :goto_0
    return-void

    .line 72
    :cond_0
    invoke-virtual {p3}, Lcom/twitter/library/api/y;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/z;

    invoke-static {v0}, Lcom/twitter/model/core/z;->a(Lcom/twitter/model/core/z;)[I

    move-result-object v0

    iput-object v0, p0, Lbfg;->c:[I

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 22
    check-cast p3, Lcom/twitter/library/api/y;

    invoke-virtual {p0, p1, p2, p3}, Lbfg;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/service/d$a;
    .locals 4

    .prologue
    .line 51
    invoke-virtual {p0}, Lbfg;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "1.1"

    .line 52
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 53
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "signup"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "sms_verify_begin"

    aput-object v3, v1, v2

    .line 54
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "raw_phone_number"

    iget-object v2, p0, Lbfg;->a:Ljava/lang/String;

    .line 55
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 51
    return-object v0
.end method

.method public e()[I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lbfg;->c:[I

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lbfg;->g()Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method

.method protected g()Lcom/twitter/library/api/y;
    .locals 1

    .prologue
    .line 60
    const/16 v0, 0x56

    invoke-static {v0}, Lcom/twitter/library/api/y;->a(I)Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method
