.class public Lbhm;
.super Lbao;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbao",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcom/twitter/model/account/a;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 45
    const-class v0, Lbhm;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/service/v;

    invoke-direct {v1, p2}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    invoke-direct {p0, p1, v0, v1, v2}, Lbao;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;Z)V

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 47
    if-eqz v0, :cond_0

    .line 48
    invoke-static {v0}, Lcom/twitter/util/b;->b(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbhm;->c:Ljava/lang/String;

    .line 50
    :cond_0
    invoke-super {p0}, Lbao;->v()Lcom/twitter/library/service/f;

    move-result-object v0

    .line 51
    if-eqz v0, :cond_2

    .line 52
    new-instance v1, Lcom/twitter/library/service/l;

    invoke-direct {v1, v2}, Lcom/twitter/library/service/l;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    .line 53
    const-string/jumbo v1, "retry_nux_and_login_verification_get_requests_2753"

    const-string/jumbo v2, "network_retry_active"

    invoke-static {v1, v2}, Lcoi;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 55
    new-instance v1, Lcom/twitter/library/service/n;

    invoke-direct {v1}, Lcom/twitter/library/service/n;-><init>()V

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    .line 57
    :cond_1
    invoke-virtual {p0, v0}, Lbhm;->a(Lcom/twitter/async/service/k;)Lcom/twitter/async/service/AsyncOperation;

    .line 59
    :cond_2
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/async/service/j;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 106
    invoke-virtual {p0}, Lbhm;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    iget-object v0, p0, Lbhm;->p:Landroid/content/Context;

    const-string/jumbo v1, "app:twitter_service:account:availability_check"

    .line 109
    invoke-virtual {p0}, Lbhm;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/service/v;->c:J

    .line 110
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/service/u;

    invoke-virtual {v4}, Lcom/twitter/library/service/u;->b()Z

    move-result v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v5, p1

    .line 107
    invoke-static/range {v0 .. v7}, Lcom/twitter/library/api/a;->a(Landroid/content/Context;Ljava/lang/String;JZLcom/twitter/async/service/j;ZLjava/lang/String;)V

    .line 115
    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/account/a;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 95
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/a;

    .line 97
    iget-boolean v1, v0, Lcom/twitter/model/account/a;->a:Z

    if-nez v1, :cond_0

    .line 98
    const/16 v1, 0x190

    iget-object v0, v0, Lcom/twitter/model/account/a;->b:Ljava/lang/String;

    invoke-virtual {p2, v1, v0}, Lcom/twitter/library/service/u;->a(ILjava/lang/String;)V

    .line 101
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 33
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lbhm;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/service/d$a;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 64
    invoke-virtual {p0}, Lbhm;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "i"

    .line 65
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    const-string/jumbo v2, "users"

    aput-object v2, v1, v3

    .line 66
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->b([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 67
    iget-object v1, p0, Lbhm;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    const-string/jumbo v1, "lang"

    iget-object v2, p0, Lbhm;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 70
    :cond_0
    invoke-virtual {p0}, Lbhm;->L()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 82
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No action for code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lbhm;->L()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :pswitch_0
    new-array v1, v4, [Ljava/lang/Object;

    const-string/jumbo v2, "email_available"

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "email"

    iget-object v2, p0, Lbhm;->a:Ljava/lang/String;

    .line 73
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 76
    :goto_0
    return-object v0

    :pswitch_1
    new-array v1, v4, [Ljava/lang/Object;

    const-string/jumbo v2, "username_available"

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "custom"

    const-wide/16 v2, 0x1

    .line 77
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "context"

    const-string/jumbo v2, "signup"

    .line 78
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "username"

    iget-object v2, p0, Lbhm;->b:Ljava/lang/String;

    .line 79
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    goto :goto_0

    .line 70
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected e()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/account/a;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    const-class v0, Lcom/twitter/model/account/a;

    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lbhm;->e()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
