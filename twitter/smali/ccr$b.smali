.class public final Lccr$b;
.super Lcck$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lccr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcck$b",
        "<",
        "Lccr;",
        "Lccr$a;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Lcck$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lccr$a;
    .locals 1

    .prologue
    .line 76
    new-instance v0, Lccr$a;

    invoke-direct {v0}, Lccr$a;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcck$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 70
    check-cast p2, Lccr$a;

    invoke-virtual {p0, p1, p2, p3}, Lccr$b;->a(Lcom/twitter/util/serialization/n;Lccr$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lccr$a;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 83
    invoke-super {p0, p1, p2, p3}, Lcck$b;->a(Lcom/twitter/util/serialization/n;Lcck$a;I)V

    .line 84
    sget-object v0, Lccp;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p2, v0}, Lccr$a;->a(Ljava/util/List;)Lccr$a;

    .line 86
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 70
    check-cast p2, Lccr$a;

    invoke-virtual {p0, p1, p2, p3}, Lccr$b;->a(Lcom/twitter/util/serialization/n;Lccr$a;I)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lcck;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    check-cast p2, Lccr;

    invoke-virtual {p0, p1, p2}, Lccr$b;->a(Lcom/twitter/util/serialization/o;Lccr;)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lccr;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    invoke-super {p0, p1, p2}, Lcck$b;->a(Lcom/twitter/util/serialization/o;Lcck;)V

    .line 93
    iget-object v0, p2, Lccr;->c:Ljava/util/List;

    sget-object v1, Lccp;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 94
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    check-cast p2, Lccr;

    invoke-virtual {p0, p1, p2}, Lccr$b;->a(Lcom/twitter/util/serialization/o;Lccr;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lccr$b;->a()Lccr$a;

    move-result-object v0

    return-object v0
.end method
