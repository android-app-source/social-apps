.class public Lbgi;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/provider/t;

.field private final b:Landroid/content/Context;

.field private final c:Lbwb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbwb",
            "<",
            "Lcom/mopub/nativeads/NativeAd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;JLbwb;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "J",
            "Lbwb",
            "<",
            "Lcom/mopub/nativeads/NativeAd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {p2, p3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    iput-object v0, p0, Lbgi;->a:Lcom/twitter/library/provider/t;

    .line 45
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbgi;->b:Landroid/content/Context;

    .line 46
    iput-object p4, p0, Lbgi;->c:Lbwb;

    .line 47
    return-void
.end method


# virtual methods
.method public a(Lcgr;Lcgx;Lchc;Lchx$b;)Lcgr$a;
    .locals 10

    .prologue
    .line 62
    iget-object v0, p1, Lcgr;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v7

    .line 63
    iget-object v0, p1, Lcgr;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchi;

    .line 64
    invoke-virtual {v0, p2, p3}, Lchi;->b(Lcgx;Lchc;)Lcom/twitter/model/timeline/y;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 67
    :cond_0
    new-instance v6, Laut;

    iget-object v0, p0, Lbgi;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {v6, v0}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 68
    iget-object v5, p4, Lchx$b;->d:Ljava/lang/String;

    .line 69
    const/4 v0, 0x0

    .line 70
    iget-boolean v1, p4, Lchx$b;->f:Z

    if-eqz v1, :cond_5

    .line 71
    iget-object v1, p0, Lbgi;->a:Lcom/twitter/library/provider/t;

    iget-wide v2, p4, Lchx$b;->b:J

    iget v4, p4, Lchx$b;->c:I

    .line 72
    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/t;->a(JILjava/lang/String;Laut;)I

    move-result v0

    move v1, v0

    .line 76
    :goto_1
    invoke-virtual {v7}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 79
    iget-object v2, p0, Lbgi;->c:Lbwb;

    if-eqz v2, :cond_1

    .line 82
    iget-object v2, p0, Lbgi;->c:Lbwb;

    invoke-static {v0, v2}, Lbwj;->a(Ljava/util/List;Lbwb;)V

    .line 85
    :cond_1
    iget-object v2, p0, Lbgi;->a:Lcom/twitter/library/provider/t;

    .line 86
    invoke-static {v0}, Lcom/twitter/library/provider/s$a;->a(Ljava/util/List;)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    iget-wide v8, p4, Lchx$b;->b:J

    .line 87
    invoke-virtual {v0, v8, v9}, Lcom/twitter/library/provider/s$a;->a(J)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    iget v3, p4, Lchx$b;->c:I

    .line 88
    invoke-virtual {v0, v3}, Lcom/twitter/library/provider/s$a;->a(I)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 89
    invoke-virtual {v0, v5}, Lcom/twitter/library/provider/s$a;->a(Ljava/lang/String;)Lcom/twitter/library/provider/s$a;

    move-result-object v3

    iget v0, p4, Lchx$b;->e:I

    const/4 v4, 0x3

    if-ne v0, v4, :cond_4

    .line 90
    invoke-virtual {v7}, Lcom/twitter/util/collection/h;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v3, v0}, Lcom/twitter/library/provider/s$a;->a(Z)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    iget-boolean v3, p4, Lchx$b;->g:Z

    .line 91
    invoke-virtual {v0, v3}, Lcom/twitter/library/provider/s$a;->c(Z)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    const/4 v3, 0x1

    .line 92
    invoke-virtual {v0, v3}, Lcom/twitter/library/provider/s$a;->d(Z)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 93
    invoke-virtual {v0, v6}, Lcom/twitter/library/provider/s$a;->a(Laut;)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    iget-boolean v3, p4, Lchx$b;->h:Z

    .line 94
    invoke-virtual {v0, v3}, Lcom/twitter/library/provider/s$a;->e(Z)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lcom/twitter/library/provider/s$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/s;

    .line 85
    invoke-virtual {v2, v0}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/library/provider/s;)I

    move-result v0

    .line 96
    if-gtz v1, :cond_2

    if-lez v0, :cond_3

    .line 97
    :cond_2
    invoke-virtual {v6}, Laut;->a()V

    .line 99
    :cond_3
    new-instance v2, Lcgr$a;

    invoke-direct {v2, v0, v1}, Lcgr$a;-><init>(II)V

    return-object v2

    .line 90
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    move v1, v0

    goto :goto_1
.end method
