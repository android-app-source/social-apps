.class public abstract Lcbz;
.super Lcbx;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcbz$b;,
        Lcbz$a;
    }
.end annotation


# instance fields
.field protected final b:Lcax;

.field private final c:Z


# direct methods
.method constructor <init>(Lcbz$a;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcbx;-><init>(Lcbx$a;)V

    .line 35
    invoke-static {p1}, Lcbz$a;->a(Lcbz$a;)Lcax;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcax;

    iput-object v0, p0, Lcbz;->b:Lcax;

    .line 36
    const-string/jumbo v0, "dm_cards_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcbz;->c:Z

    .line 37
    return-void
.end method

.method private a(Lcbz;)Z
    .locals 2

    .prologue
    .line 87
    invoke-super {p0, p1}, Lcbx;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcbz;->b:Lcax;

    iget-object v1, p1, Lcbz;->b:Lcax;

    .line 88
    invoke-virtual {v0, v1}, Lcax;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 87
    :goto_0
    return v0

    .line 88
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x5

    return v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcbz;->b:Lcax;

    invoke-virtual {v0, p1, p2}, Lcax;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 83
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcbz;

    if-eqz v0, :cond_1

    check-cast p1, Lcbz;

    invoke-direct {p0, p1}, Lcbz;->a(Lcbz;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 93
    invoke-super {p0}, Lcbx;->hashCode()I

    move-result v0

    .line 94
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcbz;->b:Lcax;

    invoke-virtual {v1}, Lcax;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    return v0
.end method

.method public j()Lcax;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcbz;->b:Lcax;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcbz;->b:Lcax;

    invoke-virtual {v0}, Lcax;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcbz;->b:Lcax;

    invoke-virtual {v0}, Lcax;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcbz;->c:Z

    return v0
.end method
