.class public Lavu;
.super Lcbr;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbr",
        "<",
        "Laxs$a;",
        "Lcom/twitter/model/core/aa;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcbr;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Laxs$a;)Lcom/twitter/model/core/aa;
    .locals 4

    .prologue
    .line 17
    invoke-interface {p1}, Laxs$a;->g()[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/topic/c;->b:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/topic/c;

    .line 18
    new-instance v1, Lcom/twitter/model/core/aa$a;

    invoke-direct {v1}, Lcom/twitter/model/core/aa$a;-><init>()V

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/twitter/model/topic/c;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 19
    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/model/core/aa$a;->b(Z)Lcom/twitter/model/core/aa$a;

    move-result-object v0

    .line 20
    invoke-interface {p1}, Laxs$a;->i()Laxp$a;

    move-result-object v1

    invoke-interface {v1}, Laxp$a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/aa$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/aa$a;

    move-result-object v0

    .line 21
    invoke-interface {p1}, Laxs$a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/aa$a;->b(Ljava/lang/String;)Lcom/twitter/model/core/aa$a;

    move-result-object v0

    .line 22
    invoke-interface {p1}, Laxs$a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/aa$a;->c(Ljava/lang/String;)Lcom/twitter/model/core/aa$a;

    move-result-object v0

    .line 23
    invoke-interface {p1}, Laxs$a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/aa$a;->a(J)Lcom/twitter/model/core/aa$a;

    move-result-object v0

    .line 24
    invoke-interface {p1}, Laxs$a;->h()Laxe$a;

    move-result-object v1

    invoke-interface {v1}, Laxe$a;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/aa$a;->b(J)Lcom/twitter/model/core/aa$a;

    move-result-object v0

    .line 25
    invoke-interface {p1}, Laxs$a;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/core/aa$a;->c(J)Lcom/twitter/model/core/aa$a;

    move-result-object v0

    .line 26
    invoke-interface {p1}, Laxs$a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/aa$a;->d(Ljava/lang/String;)Lcom/twitter/model/core/aa$a;

    move-result-object v0

    .line 27
    invoke-interface {p1}, Laxs$a;->i()Laxp$a;

    move-result-object v1

    invoke-interface {v1}, Laxp$a;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/aa$a;->e(Ljava/lang/String;)Lcom/twitter/model/core/aa$a;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lcom/twitter/model/core/aa$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/aa;

    .line 18
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    check-cast p1, Laxs$a;

    invoke-virtual {p0, p1}, Lavu;->a(Laxs$a;)Lcom/twitter/model/core/aa;

    move-result-object v0

    return-object v0
.end method
