.class public Lcfx;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lcfx;

.field public static final b:Lcfx;


# instance fields
.field public final c:Z

.field public final d:Ljava/lang/String;

.field public final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12
    new-instance v0, Lcfx;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v1, v2}, Lcfx;-><init>(ZLjava/lang/String;Z)V

    sput-object v0, Lcfx;->a:Lcfx;

    .line 13
    new-instance v0, Lcfx;

    sget-object v1, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    .line 14
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1, v3}, Lcfx;-><init>(ZLjava/lang/String;Z)V

    sput-object v0, Lcfx;->b:Lcfx;

    .line 13
    return-void
.end method

.method public constructor <init>(ZLjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-boolean p1, p0, Lcfx;->c:Z

    .line 23
    iput-object p2, p0, Lcfx;->d:Ljava/lang/String;

    .line 24
    iput-boolean p3, p0, Lcfx;->e:Z

    .line 25
    return-void
.end method


# virtual methods
.method public a(Z)Lcfx;
    .locals 3

    .prologue
    .line 29
    new-instance v0, Lcfx;

    iget-boolean v1, p0, Lcfx;->c:Z

    iget-object v2, p0, Lcfx;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1}, Lcfx;-><init>(ZLjava/lang/String;Z)V

    return-object v0
.end method

.method public b(Z)Lcfx;
    .locals 4

    .prologue
    .line 34
    if-eqz p1, :cond_0

    .line 35
    new-instance v0, Lcfx;

    iget-boolean v1, p0, Lcfx;->c:Z

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcfx;->e:Z

    invoke-direct {v0, v1, v2, v3}, Lcfx;-><init>(ZLjava/lang/String;Z)V

    move-object p0, v0

    .line 37
    :cond_0
    return-object p0
.end method
