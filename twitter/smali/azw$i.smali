.class public final Lazw$i;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lazw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "i"
.end annotation


# static fields
.field public static final days:I = 0x7f0c0005

.field public static final in_reply_to_name_and_count:I = 0x7f0c000b

.field public static final months:I = 0x7f0c0015

.field public static final ps__invited_num_followers:I = 0x7f0c0044

.field public static final ps__invited_num_followers_embolden:I = 0x7f0c0045

.field public static final ps__stats_live_viewers:I = 0x7f0c0046

.field public static final ps__stats_replay_viewers:I = 0x7f0c0047

.field public static final ps__time_format_days:I = 0x7f0c0048

.field public static final ps__time_format_hours:I = 0x7f0c0049

.field public static final ps__time_format_minutes:I = 0x7f0c004a

.field public static final ps__time_format_seconds:I = 0x7f0c004b

.field public static final reply_context_to_someone_and_n_others:I = 0x7f0c0021

.field public static final reply_context_to_someone_you_and_n_others:I = 0x7f0c0022

.field public static final reply_context_to_you_and_n_others:I = 0x7f0c0023

.field public static final social_follow_and_follow_and_others:I = 0x7f0c0027

.field public static final social_like_and_retweets_count:I = 0x7f0c0028

.field public static final social_like_count:I = 0x7f0c0029

.field public static final social_like_count_with_user:I = 0x7f0c002a

.field public static final social_like_count_with_user_accessibility_description:I = 0x7f0c002b

.field public static final social_proof_in_reply_multiple_names_and_count:I = 0x7f0c002c

.field public static final social_retweet_and_likes_count:I = 0x7f0c002d

.field public static final social_retweet_count:I = 0x7f0c002e

.field public static final tagline_view_count:I = 0x7f0c0037

.field public static final time_days:I = 0x7f0c0038

.field public static final time_days_ago:I = 0x7f0c0039

.field public static final time_hours:I = 0x7f0c003a

.field public static final time_hours_ago:I = 0x7f0c003b

.field public static final time_mins:I = 0x7f0c003c

.field public static final time_mins_ago:I = 0x7f0c003d

.field public static final time_secs:I = 0x7f0c003e

.field public static final weeks:I = 0x7f0c0042

.field public static final years:I = 0x7f0c0043
