.class public Lapj;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lapj$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcbi",
        "<",
        "Lcom/twitter/model/dms/Participant;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/support/v4/app/LoaderManager;

.field private final c:J

.field private final d:I

.field private e:Lapj$a;

.field private f:Ljava/lang/String;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;JI)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lapj;->a:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lapj;->b:Landroid/support/v4/app/LoaderManager;

    .line 37
    iput-wide p3, p0, Lapj;->c:J

    .line 38
    iput p5, p0, Lapj;->d:I

    .line 39
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 59
    iget-object v0, p0, Lapj;->b:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lapj;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 60
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 63
    iget-object v0, p0, Lapj;->b:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lapj;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 64
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lapj;->g:Z

    if-eqz v0, :cond_0

    .line 51
    invoke-direct {p0}, Lapj;->c()V

    .line 56
    :goto_0
    return-void

    .line 53
    :cond_0
    invoke-direct {p0}, Lapj;->b()V

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lapj;->g:Z

    goto :goto_0
.end method

.method public a(Landroid/support/v4/content/Loader;Lcbi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;>;",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lapj;->e:Lapj$a;

    if-eqz v0, :cond_0

    .line 94
    iget-object v1, p0, Lapj;->e:Lapj$a;

    .line 95
    invoke-static {}, Lcbi;->f()Lcbi;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbi;

    .line 94
    invoke-interface {v1, v0}, Lapj$a;->a(Lcbi;)V

    .line 97
    :cond_0
    return-void
.end method

.method public a(Lapj$a;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lapj;->e:Lapj$a;

    .line 43
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lapj;->f:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lapj;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 71
    new-instance v0, Lcom/twitter/database/model/f$a;

    invoke-direct {v0}, Lcom/twitter/database/model/f$a;-><init>()V

    const-string/jumbo v1, "conversation_participants_conversation_id"

    .line 72
    invoke-static {v1}, Laux;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lapj;->f:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v0

    const-string/jumbo v1, "conversation_participants_participant_type,conversation_participants_join_time ASC,CAST(conversation_participants_user_id AS INT)"

    .line 75
    invoke-virtual {v0, v1}, Lcom/twitter/database/model/f$a;->b(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v0

    .line 81
    :goto_0
    iget-wide v2, p0, Lapj;->c:J

    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v1

    .line 82
    new-instance v2, Lauz$a;

    iget-object v3, p0, Lapj;->a:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Lauz$a;-><init>(Landroid/content/Context;Lcom/twitter/database/model/i;)V

    const-class v1, Laww;

    .line 83
    invoke-virtual {v2, v1}, Lauz$a;->a(Ljava/lang/Class;)Lauz$a;

    move-result-object v1

    const-class v2, Lcom/twitter/model/dms/Participant;

    .line 84
    invoke-virtual {v1, v2}, Lauz$a;->b(Ljava/lang/Class;)Lauz$a;

    move-result-object v1

    sget-object v2, Lcom/twitter/database/schema/a$h;->a:Landroid/net/Uri;

    .line 85
    invoke-virtual {v1, v2}, Lauz$a;->a(Landroid/net/Uri;)Lauz$a;

    move-result-object v1

    .line 86
    invoke-virtual {v1, v0}, Lauz$a;->a(Lcom/twitter/database/model/f;)Lauz$a;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lauz$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/content/Loader;

    .line 82
    return-object v0

    .line 78
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p2, Lcbi;

    invoke-virtual {p0, p1, p2}, Lapj;->a(Landroid/support/v4/content/Loader;Lcbi;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 100
    return-void
.end method
