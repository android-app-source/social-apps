.class public Lbxj;
.super Lbxn;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/ui/widget/TextLayoutView;

.field private final b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/twitter/ui/widget/TextLayoutView;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lbxn;-><init>()V

    .line 17
    iput-object p1, p0, Lbxj;->a:Lcom/twitter/ui/widget/TextLayoutView;

    .line 18
    iput-object p2, p0, Lbxj;->b:Landroid/content/res/Resources;

    .line 19
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/r;J)V
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Lbxj;->b:Landroid/content/res/Resources;

    .line 23
    invoke-static {p1, p2, p3, v0}, Lbxo;->a(Lcom/twitter/model/core/r;JLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    .line 24
    invoke-virtual {p0, v0}, Lbxj;->a(Ljava/lang/CharSequence;)V

    .line 25
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lbxj;->a:Lcom/twitter/ui/widget/TextLayoutView;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/TextLayoutView;->setTextWithVisibility(Ljava/lang/CharSequence;)V

    .line 30
    return-void
.end method
