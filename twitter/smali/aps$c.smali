.class public Laps$c;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Laps;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# instance fields
.field private final a:Lcom/twitter/internal/android/widget/HighlightedRelativeLayout;

.field private final b:Lcom/twitter/app/dm/widget/DMAvatar;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253
    const v0, 0x7f1302ed

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/widget/DMAvatar;

    iput-object v0, p0, Laps$c;->b:Lcom/twitter/app/dm/widget/DMAvatar;

    .line 254
    const v0, 0x7f1302f6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Laps$c;->g:Landroid/widget/ImageView;

    .line 255
    const v0, 0x7f1302f5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laps$c;->c:Landroid/widget/TextView;

    .line 256
    const v0, 0x7f1302f4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laps$c;->d:Landroid/widget/TextView;

    .line 257
    const v0, 0x7f130058

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laps$c;->e:Landroid/widget/TextView;

    .line 258
    const v0, 0x7f13009a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laps$c;->f:Landroid/widget/TextView;

    .line 259
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/HighlightedRelativeLayout;

    iput-object v0, p0, Laps$c;->a:Lcom/twitter/internal/android/widget/HighlightedRelativeLayout;

    .line 260
    return-void
.end method

.method static synthetic a(Laps$c;)Lcom/twitter/internal/android/widget/HighlightedRelativeLayout;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Laps$c;->a:Lcom/twitter/internal/android/widget/HighlightedRelativeLayout;

    return-object v0
.end method

.method static synthetic b(Laps$c;)Lcom/twitter/app/dm/widget/DMAvatar;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Laps$c;->b:Lcom/twitter/app/dm/widget/DMAvatar;

    return-object v0
.end method

.method static synthetic c(Laps$c;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Laps$c;->c:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Laps$c;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Laps$c;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Laps$c;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Laps$c;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Laps$c;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Laps$c;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic g(Laps$c;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Laps$c;->g:Landroid/widget/ImageView;

    return-object v0
.end method
