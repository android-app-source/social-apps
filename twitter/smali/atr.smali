.class public Latr;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lala;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Latr$a;
    }
.end annotation


# instance fields
.field private a:J

.field private b:Z

.field private final c:Latt;

.field private final d:Latr$a;

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Latt;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Latr;->e:Ljava/util/Set;

    .line 31
    new-instance v0, Latr$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Latr$a;-><init>(Latr$1;)V

    iput-object v0, p0, Latr;->d:Latr$a;

    .line 32
    invoke-virtual {p0}, Latr;->g()V

    .line 33
    iput-object p1, p0, Latr;->c:Latt;

    .line 34
    invoke-virtual {p1}, Latt;->a()V

    .line 35
    return-void
.end method

.method private static b(Lcom/twitter/android/timeline/cf;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/timeline/cf;->a:Lcom/twitter/model/timeline/ar;

    iget-object v0, v0, Lcom/twitter/model/timeline/ar;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Latr;->b:Z

    .line 46
    iget-object v0, p0, Latr;->d:Latr$a;

    invoke-virtual {v0}, Latr$a;->a()V

    .line 47
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 38
    iget-wide v0, p0, Latr;->a:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {p0}, Latr;->g()V

    .line 40
    iput-wide p1, p0, Latr;->a:J

    .line 42
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/android/timeline/cf;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    .line 85
    if-eqz p1, :cond_0

    .line 86
    iget-object v0, p1, Lcom/twitter/android/timeline/cf;->a:Lcom/twitter/model/timeline/ar;

    iget v0, v0, Lcom/twitter/model/timeline/ar;->c:I

    packed-switch v0, :pswitch_data_0

    .line 104
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 88
    :pswitch_1
    iget-object v0, p0, Latr;->d:Latr$a;

    invoke-static {p1}, Latr;->b(Lcom/twitter/android/timeline/cf;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Latr$a;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :pswitch_2
    iget-object v0, p0, Latr;->d:Latr$a;

    invoke-static {p1}, Latr;->b(Lcom/twitter/android/timeline/cf;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Latr$a;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 96
    :pswitch_3
    iget-object v0, p0, Latr;->e:Ljava/util/Set;

    iget-object v1, p1, Lcom/twitter/android/timeline/cf;->a:Lcom/twitter/model/timeline/ar;

    iget-object v1, v1, Lcom/twitter/model/timeline/ar;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 86
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcom/twitter/android/timeline/cf;Lcom/twitter/android/timeline/cf;)V
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Latr;->d:Latr$a;

    invoke-static {p1}, Latr;->b(Lcom/twitter/android/timeline/cf;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Latr;->b(Lcom/twitter/android/timeline/cf;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Latr$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public a(Lcom/twitter/android/timeline/cf;ZI)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 108
    if-eqz p1, :cond_0

    .line 109
    iget-object v0, p1, Lcom/twitter/android/timeline/cf;->a:Lcom/twitter/model/timeline/ar;

    iget v0, v0, Lcom/twitter/model/timeline/ar;->c:I

    packed-switch v0, :pswitch_data_0

    .line 132
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 111
    :pswitch_1
    if-eqz p2, :cond_1

    if-nez p3, :cond_0

    .line 113
    :cond_1
    iget-object v0, p0, Latr;->d:Latr$a;

    invoke-virtual {v0, v1}, Latr$a;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 118
    :pswitch_2
    if-eqz p2, :cond_2

    if-nez p3, :cond_0

    .line 119
    :cond_2
    iget-object v0, p0, Latr;->d:Latr$a;

    invoke-virtual {v0, v1}, Latr$a;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 124
    :pswitch_3
    iget-object v0, p0, Latr;->e:Ljava/util/Set;

    iget-object v1, p1, Lcom/twitter/android/timeline/cf;->a:Lcom/twitter/model/timeline/ar;

    iget-object v1, v1, Lcom/twitter/model/timeline/ar;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 109
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcom/twitter/app/common/di/scope/InjectionScope;)V
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/twitter/app/common/di/scope/InjectionScope;->c:Lcom/twitter/app/common/di/scope/InjectionScope;

    if-ne p1, v0, :cond_0

    .line 149
    iget-object v0, p0, Latr;->c:Latt;

    invoke-virtual {v0}, Latt;->b()V

    .line 151
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/timeline/ar;)Z
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Latr;->e:Ljava/util/Set;

    iget-object v1, p1, Lcom/twitter/model/timeline/ar;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Latr;->b:Z

    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Latr;->d:Latr$a;

    invoke-virtual {v0}, Latr$a;->b()V

    .line 55
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Latr;->d:Latr$a;

    invoke-virtual {v0}, Latr$a;->c()V

    .line 59
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Latr;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Latr;->d:Latr$a;

    invoke-virtual {v0}, Latr$a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Latr;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Latr;->d:Latr$a;

    invoke-virtual {v0}, Latr$a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method g()V
    .locals 2

    .prologue
    .line 140
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Latr;->a:J

    .line 141
    const/4 v0, 0x0

    iput-boolean v0, p0, Latr;->b:Z

    .line 142
    iget-object v0, p0, Latr;->d:Latr$a;

    invoke-virtual {v0}, Latr$a;->a()V

    .line 143
    iget-object v0, p0, Latr;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 144
    return-void
.end method
