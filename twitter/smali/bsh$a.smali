.class public Lbsh$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbsh;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public final a:Lbsh$b;

.field public final b:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

.field public final c:Lcom/twitter/library/network/forecaster/NetworkQuality;

.field public final d:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

.field public final e:Lcom/twitter/util/connectivity/TwRadioType;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235
    const-string/jumbo v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 236
    invoke-static {}, Lcom/twitter/util/connectivity/a;->a()Lcom/twitter/util/connectivity/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/util/connectivity/a;->b()Lcom/twitter/util/connectivity/TwRadioType;

    move-result-object v1

    iput-object v1, p0, Lbsh$a;->e:Lcom/twitter/util/connectivity/TwRadioType;

    .line 239
    sget-object v1, Lbsh$1;->a:[I

    iget-object v2, p0, Lbsh$a;->e:Lcom/twitter/util/connectivity/TwRadioType;

    invoke-virtual {v2}, Lcom/twitter/util/connectivity/TwRadioType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 329
    sget-object v2, Lcom/twitter/clientapp/thriftandroid/RadioStatus;->a:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    .line 330
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->b:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    .line 334
    :goto_0
    iput-object v2, p0, Lbsh$a;->d:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    .line 335
    iput-object v1, p0, Lbsh$a;->b:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    .line 336
    invoke-static {}, Lcom/twitter/library/network/forecaster/c;->a()Lcom/twitter/library/network/forecaster/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/forecaster/c;->b()Lcom/twitter/library/network/forecaster/NetworkQuality;

    move-result-object v1

    iput-object v1, p0, Lbsh$a;->c:Lcom/twitter/library/network/forecaster/NetworkQuality;

    .line 338
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 339
    if-eqz v0, :cond_0

    .line 340
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lbsh$a;->f:Ljava/lang/String;

    .line 341
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbsh$a;->g:Ljava/lang/String;

    .line 344
    :cond_0
    new-instance v0, Lbsh$b;

    iget-object v1, p0, Lbsh$a;->d:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    invoke-direct {v0, p1, v1}, Lbsh$b;-><init>(Landroid/content/Context;Lcom/twitter/clientapp/thriftandroid/RadioStatus;)V

    iput-object v0, p0, Lbsh$a;->a:Lbsh$b;

    .line 345
    return-void

    .line 241
    :pswitch_0
    sget-object v2, Lcom/twitter/clientapp/thriftandroid/RadioStatus;->h:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    .line 242
    sget-object v1, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->c:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    goto :goto_0

    .line 246
    :pswitch_1
    sget-object v2, Lcom/twitter/clientapp/thriftandroid/RadioStatus;->c:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    .line 247
    sget-object v1, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->c:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    goto :goto_0

    .line 251
    :pswitch_2
    sget-object v2, Lcom/twitter/clientapp/thriftandroid/RadioStatus;->o:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    .line 252
    sget-object v1, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->c:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    goto :goto_0

    .line 256
    :pswitch_3
    sget-object v2, Lcom/twitter/clientapp/thriftandroid/RadioStatus;->i:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    .line 257
    sget-object v1, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->c:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    goto :goto_0

    .line 261
    :pswitch_4
    sget-object v2, Lcom/twitter/clientapp/thriftandroid/RadioStatus;->j:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    .line 262
    sget-object v1, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->c:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    goto :goto_0

    .line 266
    :pswitch_5
    sget-object v2, Lcom/twitter/clientapp/thriftandroid/RadioStatus;->k:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    .line 267
    sget-object v1, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->c:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    goto :goto_0

    .line 271
    :pswitch_6
    sget-object v2, Lcom/twitter/clientapp/thriftandroid/RadioStatus;->b:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    .line 272
    sget-object v1, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->c:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    goto :goto_0

    .line 277
    :pswitch_7
    sget-object v2, Lcom/twitter/clientapp/thriftandroid/RadioStatus;->c:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    .line 278
    sget-object v1, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->c:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    goto :goto_0

    .line 282
    :pswitch_8
    sget-object v2, Lcom/twitter/clientapp/thriftandroid/RadioStatus;->e:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    .line 283
    sget-object v1, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->c:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    goto :goto_0

    .line 287
    :pswitch_9
    sget-object v2, Lcom/twitter/clientapp/thriftandroid/RadioStatus;->g:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    .line 288
    sget-object v1, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->c:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    goto :goto_0

    .line 292
    :pswitch_a
    sget-object v2, Lcom/twitter/clientapp/thriftandroid/RadioStatus;->p:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    .line 293
    sget-object v1, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->c:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    goto :goto_0

    .line 297
    :pswitch_b
    sget-object v2, Lcom/twitter/clientapp/thriftandroid/RadioStatus;->f:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    .line 298
    sget-object v1, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->c:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    goto :goto_0

    .line 302
    :pswitch_c
    sget-object v2, Lcom/twitter/clientapp/thriftandroid/RadioStatus;->m:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    .line 303
    sget-object v1, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->c:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    goto :goto_0

    .line 307
    :pswitch_d
    sget-object v2, Lcom/twitter/clientapp/thriftandroid/RadioStatus;->n:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    .line 308
    sget-object v1, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->c:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    goto :goto_0

    .line 312
    :pswitch_e
    sget-object v2, Lcom/twitter/clientapp/thriftandroid/RadioStatus;->d:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    .line 313
    sget-object v1, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->c:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    goto :goto_0

    .line 317
    :pswitch_f
    sget-object v2, Lcom/twitter/clientapp/thriftandroid/RadioStatus;->l:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    .line 318
    sget-object v1, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->c:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    goto :goto_0

    .line 322
    :pswitch_10
    sget-object v2, Lcom/twitter/clientapp/thriftandroid/RadioStatus;->a:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    .line 323
    sget-object v1, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->b:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    goto :goto_0

    .line 330
    :cond_1
    sget-object v1, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->a:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    goto/16 :goto_0

    .line 239
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 350
    :try_start_0
    invoke-virtual {p0}, Lbsh$a;->b()Lorg/json/JSONObject;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 352
    :goto_0
    return-object v0

    .line 351
    :catch_0
    move-exception v0

    .line 352
    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public b()Lorg/json/JSONObject;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 358
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 359
    const-string/jumbo v1, "networkStatus"

    iget-object v2, p0, Lbsh$a;->b:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 360
    const-string/jumbo v1, "radioStatus"

    iget-object v2, p0, Lbsh$a;->d:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 361
    const-string/jumbo v1, "radioStatusRaw"

    iget-object v2, p0, Lbsh$a;->e:Lcom/twitter/util/connectivity/TwRadioType;

    invoke-virtual {v2}, Lcom/twitter/util/connectivity/TwRadioType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 362
    const-string/jumbo v1, "bssid"

    iget-object v2, p0, Lbsh$a;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 363
    const-string/jumbo v1, "ssid"

    iget-object v2, p0, Lbsh$a;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 364
    const-string/jumbo v1, "networkQuality"

    iget-object v2, p0, Lbsh$a;->c:Lcom/twitter/library/network/forecaster/NetworkQuality;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 365
    iget-object v1, p0, Lbsh$a;->a:Lbsh$b;

    invoke-virtual {v1}, Lbsh$b;->a()Lorg/json/JSONObject;

    move-result-object v1

    .line 366
    const-string/jumbo v2, "mobileDetails"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 367
    return-object v0
.end method

.method public c()Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus;
    .locals 4

    .prologue
    .line 372
    new-instance v0, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;

    invoke-direct {v0}, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;-><init>()V

    .line 373
    sget-object v1, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus;->b:Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$_Fields;

    iget-object v2, p0, Lbsh$a;->b:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    .line 374
    invoke-virtual {v0, v1, v2}, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;

    move-result-object v1

    sget-object v2, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus;->d:Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$_Fields;

    iget-object v3, p0, Lbsh$a;->d:Lcom/twitter/clientapp/thriftandroid/RadioStatus;

    .line 375
    invoke-virtual {v1, v2, v3}, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;

    move-result-object v1

    sget-object v2, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus;->e:Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$_Fields;

    iget-object v3, p0, Lbsh$a;->e:Lcom/twitter/util/connectivity/TwRadioType;

    .line 376
    invoke-virtual {v3}, Lcom/twitter/util/connectivity/TwRadioType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;

    move-result-object v1

    sget-object v2, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus;->h:Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$_Fields;

    iget-object v3, p0, Lbsh$a;->f:Ljava/lang/String;

    .line 377
    invoke-virtual {v1, v2, v3}, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;

    move-result-object v1

    sget-object v2, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus;->g:Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$_Fields;

    iget-object v3, p0, Lbsh$a;->g:Ljava/lang/String;

    .line 378
    invoke-virtual {v1, v2, v3}, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;

    .line 380
    iget-object v1, p0, Lbsh$a;->c:Lcom/twitter/library/network/forecaster/NetworkQuality;

    sget-object v2, Lcom/twitter/library/network/forecaster/NetworkQuality;->a:Lcom/twitter/library/network/forecaster/NetworkQuality;

    if-ne v1, v2, :cond_1

    .line 381
    sget-object v1, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus;->c:Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$_Fields;

    sget-object v2, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->a:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;

    .line 388
    :cond_0
    :goto_0
    iget-object v1, p0, Lbsh$a;->a:Lbsh$b;

    invoke-virtual {v1}, Lbsh$b;->b()Lcom/twitter/clientapp/thriftandroid/MobileDetails;

    move-result-object v1

    .line 389
    sget-object v2, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus;->i:Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$_Fields;

    invoke-virtual {v0, v2, v1}, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;

    .line 391
    invoke-virtual {v0}, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;->a()Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus;

    move-result-object v0

    return-object v0

    .line 382
    :cond_1
    iget-object v1, p0, Lbsh$a;->b:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    sget-object v2, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->b:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    if-ne v1, v2, :cond_2

    .line 383
    sget-object v1, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus;->c:Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$_Fields;

    sget-object v2, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->b:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;

    goto :goto_0

    .line 384
    :cond_2
    iget-object v1, p0, Lbsh$a;->b:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    sget-object v2, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->c:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    if-ne v1, v2, :cond_0

    .line 385
    sget-object v1, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus;->c:Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$_Fields;

    sget-object v2, Lcom/twitter/clientapp/thriftandroid/NetworkStatus;->c:Lcom/twitter/clientapp/thriftandroid/NetworkStatus;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkStatus$a;

    goto :goto_0
.end method
