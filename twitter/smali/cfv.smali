.class public Lcfv;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcfv$b;,
        Lcfv$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcfv;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Z

.field public final i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    new-instance v0, Lcfv$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcfv$b;-><init>(Lcfv$1;)V

    sput-object v0, Lcfv;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcfv$a;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, Lcfv$a;->a(Lcfv$a;)J

    move-result-wide v2

    iput-wide v2, p0, Lcfv;->b:J

    .line 34
    invoke-static {p1}, Lcfv$a;->b(Lcfv$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfv;->c:Ljava/lang/String;

    .line 35
    invoke-static {p1}, Lcfv$a;->c(Lcfv$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfv;->d:Ljava/lang/String;

    .line 36
    invoke-static {p1}, Lcfv$a;->d(Lcfv$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcfv;->e:Ljava/lang/String;

    .line 40
    invoke-static {p1}, Lcfv$a;->e(Lcfv$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcfv;->f:Ljava/lang/String;

    .line 41
    invoke-static {p1}, Lcfv$a;->f(Lcfv$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-object v1, p0, Lcfv;->g:Ljava/lang/String;

    .line 42
    invoke-static {p1}, Lcfv$a;->g(Lcfv$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcfv;->h:Z

    .line 43
    invoke-static {p1}, Lcfv$a;->h(Lcfv$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcfv;->i:Z

    .line 44
    return-void

    .line 40
    :cond_0
    invoke-static {p1}, Lcfv$a;->e(Lcfv$a;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 41
    :cond_1
    invoke-static {p1}, Lcfv$a;->f(Lcfv$a;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method synthetic constructor <init>(Lcfv$a;Lcfv$1;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcfv;-><init>(Lcfv$a;)V

    return-void
.end method
