.class public final Lamt$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lamt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:Lamv;

.field private b:Lalg;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lamt$1;)V
    .locals 0

    .prologue
    .line 1470
    invoke-direct {p0}, Lamt$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lamt$a;)Lalg;
    .locals 1

    .prologue
    .line 1470
    iget-object v0, p0, Lamt$a;->b:Lalg;

    return-object v0
.end method

.method static synthetic b(Lamt$a;)Lamv;
    .locals 1

    .prologue
    .line 1470
    iget-object v0, p0, Lamt$a;->a:Lamv;

    return-object v0
.end method


# virtual methods
.method public a(Lalg;)Lamt$a;
    .locals 1

    .prologue
    .line 1540
    .line 1541
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lalg;

    iput-object v0, p0, Lamt$a;->b:Lalg;

    .line 1542
    return-object p0
.end method

.method public a(Lamv;)Lamt$a;
    .locals 1

    .prologue
    .line 1490
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamv;

    iput-object v0, p0, Lamt$a;->a:Lamv;

    .line 1491
    return-object p0
.end method

.method public a()Lamu;
    .locals 3

    .prologue
    .line 1478
    iget-object v0, p0, Lamt$a;->a:Lamv;

    if-nez v0, :cond_0

    .line 1479
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lamv;

    .line 1480
    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1482
    :cond_0
    iget-object v0, p0, Lamt$a;->b:Lalg;

    if-nez v0, :cond_1

    .line 1483
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lalg;

    .line 1484
    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1486
    :cond_1
    new-instance v0, Lamt;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lamt;-><init>(Lamt$a;Lamt$1;)V

    return-object v0
.end method
