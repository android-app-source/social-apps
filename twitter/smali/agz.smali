.class public Lagz;
.super Lcbp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbp",
        "<",
        "Lcom/twitter/model/search/viewmodel/h;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcbp;-><init>()V

    .line 20
    iput-object p1, p0, Lagz;->a:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lagz;->b(Landroid/database/Cursor;)Lcom/twitter/model/search/viewmodel/h;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 25
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 26
    const/4 v2, 0x7

    if-eq v1, v2, :cond_0

    const/4 v2, 0x6

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/database/Cursor;)Lcom/twitter/model/search/viewmodel/h;
    .locals 13

    .prologue
    const/4 v3, 0x3

    .line 33
    const/16 v0, 0xb

    .line 34
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 35
    const/16 v0, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 37
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 38
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 39
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 40
    const/16 v0, 0xd

    .line 41
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 43
    :goto_0
    const/16 v3, 0xf

    .line 44
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 45
    const/16 v3, 0xe

    .line 46
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 47
    const/16 v3, 0x10

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 48
    const/4 v3, 0x4

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 49
    const/4 v4, 0x6

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 50
    if-eqz v4, :cond_1

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 51
    :goto_1
    new-instance v12, Lcom/twitter/model/search/viewmodel/g$a;

    invoke-direct {v12}, Lcom/twitter/model/search/viewmodel/g$a;-><init>()V

    .line 53
    invoke-virtual {v12, v6, v7}, Lcom/twitter/model/search/viewmodel/g$a;->a(J)Lcom/twitter/model/search/viewmodel/g$a;

    move-result-object v12

    .line 54
    invoke-virtual {v12, v8}, Lcom/twitter/model/search/viewmodel/g$a;->a(Ljava/lang/String;)Lcom/twitter/model/search/viewmodel/g$a;

    move-result-object v8

    .line 55
    invoke-virtual {v8, v10}, Lcom/twitter/model/search/viewmodel/g$a;->d(Ljava/lang/String;)Lcom/twitter/model/search/viewmodel/g$a;

    move-result-object v8

    .line 56
    invoke-virtual {v8, v9}, Lcom/twitter/model/search/viewmodel/g$a;->a(I)Lcom/twitter/model/search/viewmodel/g$a;

    move-result-object v8

    .line 57
    invoke-virtual {v8, v5}, Lcom/twitter/model/search/viewmodel/g$a;->b(Ljava/lang/String;)Lcom/twitter/model/search/viewmodel/g$a;

    move-result-object v8

    .line 58
    invoke-virtual {v8, v1}, Lcom/twitter/model/search/viewmodel/g$a;->c(Ljava/lang/String;)Lcom/twitter/model/search/viewmodel/g$a;

    move-result-object v1

    .line 59
    invoke-virtual {v1, v11}, Lcom/twitter/model/search/viewmodel/g$a;->e(Ljava/lang/String;)Lcom/twitter/model/search/viewmodel/g$a;

    move-result-object v1

    .line 60
    invoke-virtual {v1, v0}, Lcom/twitter/model/search/viewmodel/g$a;->a(Z)Lcom/twitter/model/search/viewmodel/g$a;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lcom/twitter/model/search/viewmodel/g$a;->q()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/model/search/viewmodel/g;

    .line 62
    new-instance v0, Lcom/twitter/model/search/viewmodel/h;

    iget-object v1, p0, Lagz;->a:Ljava/lang/String;

    invoke-direct/range {v0 .. v8}, Lcom/twitter/model/search/viewmodel/h;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;JLcom/twitter/model/search/viewmodel/g;)V

    return-object v0

    .line 41
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 50
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 15
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lagz;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
