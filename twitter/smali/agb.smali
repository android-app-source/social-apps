.class public Lagb;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbwk;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lagb$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbwk",
        "<",
        "Lcom/mopub/nativeads/NativeAd;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lrx/subjects/a",
            "<",
            "Lcom/mopub/nativeads/NativeAd;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/mopub/nativeads/MoPubNative$MoPubNativeNetworkListener;

.field private final c:Lcom/mopub/nativeads/MoPubNative;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/mopub/network/Networking;->useHttps(Z)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lafz;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lagb;->a:Ljava/util/Queue;

    .line 34
    new-instance v0, Lagb$a;

    iget-object v1, p0, Lagb;->a:Ljava/util/Queue;

    invoke-direct {v0, v1}, Lagb$a;-><init>(Ljava/util/Queue;)V

    iput-object v0, p0, Lagb;->b:Lcom/mopub/nativeads/MoPubNative$MoPubNativeNetworkListener;

    .line 41
    iget-object v0, p0, Lagb;->b:Lcom/mopub/nativeads/MoPubNative$MoPubNativeNetworkListener;

    invoke-interface {p3, p1, p2, v0}, Lafz;->a(Landroid/content/Context;Ljava/lang/String;Lcom/mopub/nativeads/MoPubNative$MoPubNativeNetworkListener;)Lcom/mopub/nativeads/MoPubNative;

    move-result-object v0

    iput-object v0, p0, Lagb;->c:Lcom/mopub/nativeads/MoPubNative;

    .line 42
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Lagb;
    .locals 3

    .prologue
    .line 124
    new-instance v0, Lagb;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Laga;

    invoke-direct {v2}, Laga;-><init>()V

    invoke-direct {v0, v1, p1, v2}, Lagb;-><init>(Landroid/content/Context;Ljava/lang/String;Lafz;)V

    return-object v0
.end method

.method static synthetic a(Lagb;)Lcom/mopub/nativeads/MoPubNative;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lagb;->c:Lcom/mopub/nativeads/MoPubNative;

    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lagb$1;

    invoke-direct {v0, p0}, Lagb$1;-><init>(Lagb;)V

    invoke-static {v0}, Lcom/twitter/util/x;->a(Lcom/twitter/util/concurrent/i;)Ljava/lang/Object;

    .line 68
    return-void
.end method


# virtual methods
.method public a()Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcom/mopub/nativeads/NativeAd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    invoke-static {}, Lrx/subjects/a;->r()Lrx/subjects/a;

    move-result-object v0

    .line 48
    iget-object v1, p0, Lagb;->a:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 50
    invoke-virtual {p0}, Lagb;->b()V

    .line 52
    return-object v0
.end method

.method b()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lagb;->c()V

    .line 57
    return-void
.end method

.method public close()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lagb;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/subjects/a;

    .line 73
    invoke-virtual {v0}, Lrx/subjects/a;->by_()V

    goto :goto_0

    .line 75
    :cond_0
    iget-object v0, p0, Lagb;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 78
    iget-object v0, p0, Lagb;->c:Lcom/mopub/nativeads/MoPubNative;

    invoke-virtual {v0}, Lcom/mopub/nativeads/MoPubNative;->destroy()V

    .line 79
    return-void
.end method
