.class public abstract Lcox;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcow;


# static fields
.field private static a:Lcom/twitter/util/object/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/b",
            "<",
            "Lcnz;",
            "Lcox;",
            ">;"
        }
    .end annotation
.end field

.field private static b:Lcnz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcnz;->b:Lcnz;

    sput-object v0, Lcox;->b:Lcnz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/twitter/util/object/d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/object/d",
            "<-",
            "Lcnz;",
            "+",
            "Lcox;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    const-class v0, Lcox;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 48
    if-eqz p0, :cond_0

    new-instance v0, Lcom/twitter/util/object/b;

    invoke-direct {v0, p0}, Lcom/twitter/util/object/b;-><init>(Lcom/twitter/util/object/d;)V

    :goto_0
    sput-object v0, Lcox;->a:Lcom/twitter/util/object/b;

    .line 49
    return-void

    .line 48
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static ax()Lcox;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcox;->b:Lcnz;

    invoke-static {v0}, Lcox;->c(Lcnz;)Lcox;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lcnz;)Lcox;
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcox;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 35
    sget-object v0, Lcox;->a:Lcom/twitter/util/object/b;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/object/b;

    invoke-virtual {v0, p0}, Lcom/twitter/util/object/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcox;

    return-object v0
.end method

.method public static d(Lcnz;)V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcox;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 40
    sget-object v0, Lcox;->a:Lcom/twitter/util/object/b;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/object/b;

    invoke-virtual {v0, p0}, Lcom/twitter/util/object/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcox;->b:Lcnz;

    invoke-virtual {p0, v0}, Lcnz;->a(Lcnz;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    sget-object v0, Lcnz;->b:Lcnz;

    sput-object v0, Lcox;->b:Lcnz;

    .line 44
    :cond_0
    return-void
.end method

.method public static e(Lcnz;)V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcox;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 53
    sput-object p0, Lcox;->b:Lcnz;

    .line 54
    return-void
.end method
