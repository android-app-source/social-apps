.class public Lceh;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lceh$b;,
        Lceh$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lceh;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lceo;

.field public final c:Lcom/twitter/model/moments/MomentPageDisplayMode;

.field public final d:Lcom/twitter/model/moments/r;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lceh$b;

    invoke-direct {v0}, Lceh$b;-><init>()V

    sput-object v0, Lceh;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>(Lceh$a;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iget-object v0, p1, Lceh$a;->a:Lceo;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lceo;

    iput-object v0, p0, Lceh;->b:Lceo;

    .line 35
    iget-object v0, p1, Lceh$a;->b:Lcom/twitter/model/moments/MomentPageDisplayMode;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/MomentPageDisplayMode;

    iput-object v0, p0, Lceh;->c:Lcom/twitter/model/moments/MomentPageDisplayMode;

    .line 36
    iget-object v0, p1, Lceh$a;->c:Lcom/twitter/model/moments/r;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/r;

    iput-object v0, p0, Lceh;->d:Lcom/twitter/model/moments/r;

    .line 37
    return-void
.end method

.method public static a(JLceo;)Lceh;
    .locals 2

    .prologue
    .line 97
    sget-object v0, Lcom/twitter/model/moments/MomentPageDisplayMode;->a:Lcom/twitter/model/moments/MomentPageDisplayMode;

    invoke-static {p0, p1, p2, v0}, Lceh;->a(JLceo;Lcom/twitter/model/moments/MomentPageDisplayMode;)Lceh;

    move-result-object v0

    return-object v0
.end method

.method private static a(JLceo;Lcom/twitter/model/moments/MomentPageDisplayMode;)Lceh;
    .locals 2

    .prologue
    .line 113
    invoke-static {p2}, Lcom/twitter/model/moments/r$a;->a(Lceo;)Lcom/twitter/model/moments/r$a;

    move-result-object v0

    .line 114
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/r$a;->a(Ljava/lang/Long;)Lcom/twitter/model/moments/r$a;

    move-result-object v0

    .line 115
    invoke-virtual {v0, p3}, Lcom/twitter/model/moments/r$a;->a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/r$a;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Lcom/twitter/model/moments/r$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/r;

    .line 117
    new-instance v1, Lceh$a;

    invoke-direct {v1}, Lceh$a;-><init>()V

    .line 118
    invoke-virtual {v1, v0}, Lceh$a;->a(Lcom/twitter/model/moments/r;)Lceh$a;

    move-result-object v0

    .line 119
    invoke-virtual {v0, p3}, Lceh$a;->a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lceh$a;

    move-result-object v0

    .line 120
    invoke-virtual {v0, p2}, Lceh$a;->a(Lceo;)Lceh$a;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Lceh$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lceh;

    .line 117
    return-object v0
.end method

.method public static b(JLceo;)Lceh;
    .locals 2

    .prologue
    .line 102
    sget-object v0, Lcom/twitter/model/moments/MomentPageDisplayMode;->c:Lcom/twitter/model/moments/MomentPageDisplayMode;

    invoke-static {p0, p1, p2, v0}, Lceh;->a(JLceo;Lcom/twitter/model/moments/MomentPageDisplayMode;)Lceh;

    move-result-object v0

    return-object v0
.end method

.method public static c(JLceo;)Lceh;
    .locals 2

    .prologue
    .line 107
    sget-object v0, Lcom/twitter/model/moments/MomentPageDisplayMode;->b:Lcom/twitter/model/moments/MomentPageDisplayMode;

    invoke-static {p0, p1, p2, v0}, Lceh;->a(JLceo;Lcom/twitter/model/moments/MomentPageDisplayMode;)Lceh;

    move-result-object v0

    return-object v0
.end method
