.class public Lapz$b;
.super Lapv$c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lapz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field private final b:Lcom/twitter/media/ui/image/UserImageView;

.field private final c:Lcom/twitter/app/dm/widget/ReceivedMessageBylineView;

.field private final d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 207
    invoke-direct {p0, p1, p2}, Lapv$c;-><init>(Landroid/view/View;Z)V

    .line 208
    iget-object v0, p0, Lapz$b;->a:Landroid/view/ViewGroup;

    const v1, 0x7f13031a

    .line 209
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Lapz$b;->b:Lcom/twitter/media/ui/image/UserImageView;

    .line 210
    iget-object v0, p0, Lapz$b;->a:Landroid/view/ViewGroup;

    const v1, 0x7f13030b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/widget/ReceivedMessageBylineView;

    iput-object v0, p0, Lapz$b;->c:Lcom/twitter/app/dm/widget/ReceivedMessageBylineView;

    .line 211
    iget-object v0, p0, Lapz$b;->a:Landroid/view/ViewGroup;

    const v1, 0x7f130324

    .line 212
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 211
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lapz$b;->d:Landroid/view/View;

    .line 213
    return-void
.end method

.method static synthetic a(Lapz$b;)Lcom/twitter/media/ui/image/UserImageView;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lapz$b;->b:Lcom/twitter/media/ui/image/UserImageView;

    return-object v0
.end method

.method static synthetic b(Lapz$b;)Lcom/twitter/app/dm/widget/ReceivedMessageBylineView;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lapz$b;->c:Lcom/twitter/app/dm/widget/ReceivedMessageBylineView;

    return-object v0
.end method

.method static synthetic c(Lapz$b;)Landroid/view/View;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lapz$b;->d:Landroid/view/View;

    return-object v0
.end method
