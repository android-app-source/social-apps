.class public Lbrg;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/facebook/imagepipeline/producers/ac;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/imagepipeline/producers/ac",
        "<",
        "Lcom/facebook/imagepipeline/producers/r;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/media/manager/i$b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/media/manager/i$b;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lbrg;->a:Landroid/content/Context;

    .line 47
    iput-object p2, p0, Lbrg;->b:Lcom/twitter/library/media/manager/i$b;

    .line 48
    return-void
.end method


# virtual methods
.method public a(Lcom/facebook/imagepipeline/producers/j;Lcom/facebook/imagepipeline/producers/ag;)Lcom/facebook/imagepipeline/producers/r;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/j",
            "<",
            "Lds;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ag;",
            ")",
            "Lcom/facebook/imagepipeline/producers/r;"
        }
    .end annotation

    .prologue
    .line 56
    new-instance v0, Lcom/facebook/imagepipeline/producers/r;

    invoke-direct {v0, p1, p2}, Lcom/facebook/imagepipeline/producers/r;-><init>(Lcom/facebook/imagepipeline/producers/j;Lcom/facebook/imagepipeline/producers/ag;)V

    return-object v0
.end method

.method public a(Lcom/facebook/imagepipeline/producers/r;I)V
    .locals 0

    .prologue
    .line 114
    return-void
.end method

.method public a(Lcom/facebook/imagepipeline/producers/r;Lcom/facebook/imagepipeline/producers/ac$a;)V
    .locals 4

    .prologue
    .line 64
    invoke-virtual {p1}, Lcom/facebook/imagepipeline/producers/r;->b()Lcom/facebook/imagepipeline/producers/ag;

    move-result-object v0

    invoke-interface {v0}, Lcom/facebook/imagepipeline/producers/ag;->d()Ljava/lang/Object;

    move-result-object v0

    .line 65
    instance-of v1, v0, Lbzb;

    if-eqz v1, :cond_0

    .line 66
    check-cast v0, Lbzb;

    .line 67
    sget-object v1, Lcom/twitter/media/request/ResourceResponse$ResourceSource;->f:Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    invoke-virtual {v0, v1}, Lbzb;->a(Lcom/twitter/media/request/ResourceResponse$ResourceSource;)V

    .line 69
    invoke-virtual {v0}, Lbzb;->b()Lcom/twitter/media/request/a;

    move-result-object v1

    .line 70
    invoke-virtual {p1}, Lcom/facebook/imagepipeline/producers/r;->e()Landroid/net/Uri;

    move-result-object v2

    .line 76
    iget-object v0, p0, Lbrg;->b:Lcom/twitter/library/media/manager/i$b;

    invoke-interface {v0}, Lcom/twitter/library/media/manager/i$b;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/i$a;

    iget-object v3, p0, Lbrg;->a:Landroid/content/Context;

    .line 77
    invoke-virtual {v0, v3}, Lcom/twitter/library/media/manager/i$a;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/i$a;

    move-result-object v0

    .line 78
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/media/manager/i$a;->a(Ljava/lang/String;)Lcom/twitter/library/media/manager/i$a;

    move-result-object v0

    .line 79
    invoke-static {}, Lcox;->ax()Lcox;

    move-result-object v2

    invoke-virtual {v2}, Lcox;->ar()Lcnz;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/media/manager/i$a;->a(Lcnz;)Lcom/twitter/library/media/manager/i$a;

    move-result-object v2

    .line 80
    invoke-virtual {v1}, Lcom/twitter/media/request/a;->u()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/network/a;

    invoke-virtual {v2, v0}, Lcom/twitter/library/media/manager/i$a;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/media/manager/i$a;

    move-result-object v0

    .line 81
    invoke-virtual {v1}, Lcom/twitter/media/request/a;->z()Lcom/twitter/media/request/ResourceRequestType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/i$a;->a(Lcom/twitter/media/request/ResourceRequestType;)Lcom/twitter/library/media/manager/i$a;

    move-result-object v0

    new-instance v1, Lbrg$1;

    invoke-direct {v1, p0, p2}, Lbrg$1;-><init>(Lbrg;Lcom/facebook/imagepipeline/producers/ac$a;)V

    .line 82
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/i$a;->a(Lcom/twitter/library/media/manager/i$c;)Lcom/twitter/library/media/manager/i$a;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Lcom/twitter/library/media/manager/i$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/i;

    .line 94
    invoke-interface {v0}, Lcom/twitter/library/media/manager/i;->a()Ljava/util/concurrent/Future;

    .line 99
    :goto_0
    return-void

    .line 96
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Expected an image request to be of type ImageRequest but was: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a(Lcom/facebook/imagepipeline/producers/r;)Z
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x1

    return v0
.end method

.method public b(Lcom/facebook/imagepipeline/producers/r;I)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/r;",
            "I)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    const/4 v0, 0x0

    return-object v0
.end method
