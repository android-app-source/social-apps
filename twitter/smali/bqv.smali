.class public Lbqv;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbqy;
.implements Lcom/google/android/gms/common/api/c$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbqv$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/geo/provider/param/a;

.field private final c:Lbqx;

.field private d:Lbqv$a;

.field private e:Z

.field private f:Z

.field private g:Lbqy;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/geo/provider/param/a;Lbql;Z)V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lbqx;

    invoke-direct {v0, p3}, Lbqx;-><init>(Lbql;)V

    invoke-direct {p0, p1, p2, v0, p4}, Lbqv;-><init>(Landroid/content/Context;Lcom/twitter/library/geo/provider/param/a;Lbqx;Z)V

    .line 62
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/twitter/library/geo/provider/param/a;Lbqx;Z)V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    sget-object v0, Lbqv$a;->a:Lbqv$a;

    iput-object v0, p0, Lbqv;->d:Lbqv$a;

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbqv;->e:Z

    .line 87
    iput-object p2, p0, Lbqv;->b:Lcom/twitter/library/geo/provider/param/a;

    .line 88
    iput-object p1, p0, Lbqv;->a:Landroid/content/Context;

    .line 89
    iput-object p3, p0, Lbqv;->c:Lbqx;

    .line 90
    invoke-direct {p0, p4}, Lbqv;->c(Z)Lbqy;

    move-result-object v0

    iput-object v0, p0, Lbqv;->g:Lbqy;

    .line 91
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lbqv;->f:Z

    invoke-direct {p0, v0}, Lbqv;->c(Z)Lbqy;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbqv;->a(Lbqy;)V

    .line 187
    return-void
.end method

.method private c(Z)Lbqy;
    .locals 4

    .prologue
    .line 105
    if-eqz p1, :cond_0

    .line 106
    iget-object v0, p0, Lbqv;->a:Landroid/content/Context;

    invoke-static {v0}, Lbqo;->a(Landroid/content/Context;)Lbqo;

    move-result-object v0

    .line 115
    :goto_0
    return-object v0

    .line 109
    :cond_0
    iget-boolean v0, p0, Lbqv;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbqv;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    new-instance v0, Lbqw;

    iget-object v1, p0, Lbqv;->a:Landroid/content/Context;

    iget-object v2, p0, Lbqv;->b:Lcom/twitter/library/geo/provider/param/a;

    iget-object v3, p0, Lbqv;->c:Lbqx;

    invoke-direct {v0, v1, v2, v3, p0}, Lbqw;-><init>(Landroid/content/Context;Lcom/twitter/library/geo/provider/param/a;Lbqx;Lcom/google/android/gms/common/api/c$c;)V

    goto :goto_0

    .line 115
    :cond_1
    new-instance v0, Lbqz;

    iget-object v1, p0, Lbqv;->a:Landroid/content/Context;

    iget-object v2, p0, Lbqv;->b:Lcom/twitter/library/geo/provider/param/a;

    iget-object v3, p0, Lbqv;->c:Lbqx;

    invoke-direct {v0, v1, v2, v3}, Lbqz;-><init>(Landroid/content/Context;Lcom/twitter/library/geo/provider/param/a;Lbqx;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lbqv$a;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lbqv;->d:Lbqv$a;

    .line 100
    return-void
.end method

.method public a(Lbqy;)V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lbqv;->g:Lbqy;

    if-ne v0, p1, :cond_0

    .line 162
    :goto_0
    return-void

    .line 156
    :cond_0
    iget-object v0, p0, Lbqv;->g:Lbqy;

    invoke-interface {v0}, Lbqy;->h()V

    .line 157
    iget-object v0, p0, Lbqv;->d:Lbqv$a;

    invoke-interface {v0}, Lbqv$a;->h()V

    .line 159
    iput-object p1, p0, Lbqv;->g:Lbqy;

    .line 161
    iget-object v0, p0, Lbqv;->d:Lbqv$a;

    invoke-interface {v0}, Lbqv$a;->i()V

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbqv;->e:Z

    .line 142
    invoke-direct {p0}, Lbqv;->a()V

    .line 143
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 168
    iget-boolean v0, p0, Lbqv;->f:Z

    if-eq v0, p1, :cond_0

    .line 169
    iput-boolean p1, p0, Lbqv;->f:Z

    .line 170
    invoke-direct {p0}, Lbqv;->a()V

    .line 172
    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 178
    iget-boolean v0, p0, Lbqv;->e:Z

    if-eq v0, p1, :cond_0

    .line 179
    iput-boolean p1, p0, Lbqv;->e:Z

    .line 180
    invoke-direct {p0}, Lbqv;->a()V

    .line 182
    :cond_0
    return-void
.end method

.method public f()Landroid/location/Location;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lbqv;->g:Lbqy;

    invoke-interface {v0}, Lbqy;->f()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lbqv;->g:Lbqy;

    invoke-interface {v0}, Lbqy;->g()V

    .line 127
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lbqv;->g:Lbqy;

    invoke-interface {v0}, Lbqy;->h()V

    .line 132
    return-void
.end method
