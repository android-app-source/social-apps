.class public Lbwg;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Lbwf;Ljava/lang/String;)Lcax;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 20
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "realtime_ad_impression_id"

    new-instance v2, Lcaw;

    invoke-direct {v2, p1, v4}, Lcaw;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "image_url"

    new-instance v2, Lcaw;

    .line 22
    invoke-interface {p0}, Lbwf;->e()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v4}, Lcaw;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "app_star_rating"

    new-instance v2, Lcaw;

    .line 23
    invoke-interface {p0}, Lbwf;->f()Ljava/lang/Double;

    move-result-object v3

    invoke-direct {v2, v3, v4}, Lcaw;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "title"

    new-instance v2, Lcaw;

    .line 24
    invoke-interface {p0}, Lbwf;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v4}, Lcaw;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "app_category"

    new-instance v2, Lcaw;

    .line 25
    invoke-interface {p0}, Lbwf;->g()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v4}, Lcaw;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "cta_key"

    new-instance v2, Lcaw;

    .line 26
    invoke-interface {p0}, Lbwf;->h()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v4}, Lcaw;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 29
    new-instance v1, Lcax$a;

    invoke-direct {v1}, Lcax$a;-><init>()V

    .line 30
    const-string/jumbo v2, "promo_image_app"

    invoke-virtual {v1, v2}, Lcax$a;->a(Ljava/lang/String;)Lcax$a;

    .line 31
    invoke-virtual {v1, v0}, Lcax$a;->a(Ljava/util/Map;)Lcax$a;

    .line 32
    invoke-virtual {v1}, Lcax$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcax;

    return-object v0
.end method
