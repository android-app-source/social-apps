.class Laqa$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Laqa;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Laqa;

.field private final b:Ljava/lang/String;

.field private c:I


# direct methods
.method constructor <init>(Laqa;)V
    .locals 2

    .prologue
    .line 444
    iput-object p1, p0, Laqa$b;->a:Laqa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 445
    const-string/jumbo v0, "."

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laqa$b;->b:Ljava/lang/String;

    .line 446
    const/4 v0, 0x0

    iput v0, p0, Laqa$b;->c:I

    .line 447
    return-void
.end method

.method private a()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 477
    new-instance v0, Landroid/text/SpannableString;

    iget-object v1, p0, Laqa$b;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 478
    iget-object v1, p0, Laqa$b;->a:Laqa;

    invoke-static {v1}, Laqa;->b(Laqa;)Landroid/text/style/CharacterStyle;

    move-result-object v1

    iget v2, p0, Laqa$b;->c:I

    invoke-interface {v0}, Landroid/text/Spannable;->length()I

    move-result v3

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 479
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/CharSequence;

    iget-object v2, p0, Laqa$b;->a:Laqa;

    invoke-static {v2}, Laqa;->c(Laqa;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    aput-object v0, v1, v2

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 451
    iget-object v0, p0, Laqa$b;->a:Laqa;

    invoke-virtual {v0}, Laqa;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqa$b;->a:Laqa;

    invoke-static {v0}, Laqa;->a(Laqa;)Lcom/twitter/app/dm/widget/SentMessageBylineView;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/ViewCompat;->isAttachedToWindow(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 455
    :cond_0
    iget-object v0, p0, Laqa$b;->a:Laqa;

    invoke-static {v0}, Laqa;->a(Laqa;)Lcom/twitter/app/dm/widget/SentMessageBylineView;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 456
    iget-object v0, p0, Laqa$b;->a:Laqa;

    invoke-static {v0}, Laqa;->a(Laqa;)Lcom/twitter/app/dm/widget/SentMessageBylineView;

    move-result-object v0

    const v1, 0x7f130027

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->setTag(ILjava/lang/Object;)V

    .line 473
    :goto_0
    return-void

    .line 460
    :cond_1
    iget v0, p0, Laqa$b;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Laqa$b;->c:I

    .line 461
    iget-object v0, p0, Laqa$b;->a:Laqa;

    invoke-static {v0}, Laqa;->a(Laqa;)Lcom/twitter/app/dm/widget/SentMessageBylineView;

    move-result-object v0

    invoke-direct {p0}, Laqa$b;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->setDraftStatusText(Ljava/lang/CharSequence;)V

    .line 468
    iget v0, p0, Laqa$b;->c:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    const/16 v0, 0x190

    :goto_1
    add-int/lit16 v0, v0, 0x190

    .line 471
    iget v1, p0, Laqa$b;->c:I

    rem-int/lit8 v1, v1, 0x3

    iput v1, p0, Laqa$b;->c:I

    .line 472
    iget-object v1, p0, Laqa$b;->a:Laqa;

    invoke-static {v1}, Laqa;->a(Laqa;)Lcom/twitter/app/dm/widget/SentMessageBylineView;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {v1, p0, v2, v3}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 468
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
