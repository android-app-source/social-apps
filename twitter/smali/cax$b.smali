.class Lcax$b;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcax;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcax;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 498
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/util/serialization/i;-><init>(I)V

    .line 499
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcax;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 518
    new-instance v2, Lcax$a;

    invoke-direct {v2}, Lcax$a;-><init>()V

    .line 520
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcax$a;->a(Ljava/lang/String;)Lcax$a;

    .line 521
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcax$a;->b(Ljava/lang/String;)Lcax$a;

    .line 522
    if-ge p2, v3, :cond_0

    .line 524
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    .line 526
    :cond_0
    if-ge p2, v3, :cond_4

    sget-object v0, Lcom/twitter/util/serialization/f;->j:Lcom/twitter/util/serialization/l;

    move-object v1, v0

    .line 528
    :goto_0
    sget-object v0, Lcaw;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v1, v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Ljava/util/Map;

    move-result-object v0

    .line 530
    if-eqz v0, :cond_1

    .line 531
    invoke-virtual {v2, v0}, Lcax$a;->a(Ljava/util/Map;)Lcax$a;

    .line 533
    :cond_1
    sget-object v0, Lcom/twitter/model/core/TwitterUser;->a:Lcom/twitter/util/serialization/b;

    .line 534
    invoke-static {p1, v1, v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Ljava/util/Map;

    move-result-object v0

    .line 533
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v2, v0}, Lcax$a;->b(Ljava/util/Map;)Lcax$a;

    .line 535
    if-ge p2, v3, :cond_2

    .line 537
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    .line 538
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    .line 539
    sget-object v0, Lcaw;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v1, v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Ljava/util/Map;

    .line 540
    sget-object v0, Lcom/twitter/model/core/TwitterUser;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v1, v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Ljava/util/Map;

    .line 542
    :cond_2
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcax$a;->c(Ljava/lang/String;)Lcax$a;

    .line 543
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcax$a;->d(Ljava/lang/String;)Lcax$a;

    .line 544
    if-ge p2, v3, :cond_3

    .line 546
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    .line 547
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    .line 549
    :cond_3
    invoke-virtual {v2}, Lcax$a;->e()Lcax;

    move-result-object v0

    return-object v0

    .line 526
    :cond_4
    sget-object v0, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    move-object v1, v0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcax;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 504
    invoke-static {p2}, Lcax;->b(Lcax;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 505
    invoke-static {p2}, Lcax;->a(Lcax;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 506
    invoke-virtual {p2}, Lcax;->i()Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v2, Lcaw;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/Map;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)V

    .line 508
    invoke-static {p2}, Lcax;->c(Lcax;)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v2, Lcom/twitter/model/core/TwitterUser;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/Map;Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)V

    .line 510
    invoke-static {p2}, Lcax;->e(Lcax;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 511
    invoke-static {p2}, Lcax;->d(Lcax;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 512
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 496
    check-cast p2, Lcax;

    invoke-virtual {p0, p1, p2}, Lcax$b;->a(Lcom/twitter/util/serialization/o;Lcax;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 496
    invoke-virtual {p0, p1, p2}, Lcax$b;->a(Lcom/twitter/util/serialization/n;I)Lcax;

    move-result-object v0

    return-object v0
.end method
