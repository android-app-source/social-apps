.class public Lahn;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lakv;
.implements Laoe;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lakv",
        "<",
        "Laii;",
        ">;",
        "Laoe;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/app/common/base/BaseFragmentActivity;

.field private final b:Lahz;

.field private final c:Lagh;

.field private final d:Lage;

.field private final e:Laht;

.field private final f:Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method public constructor <init>(Lcom/twitter/app/common/base/BaseFragmentActivity;Lahz;Lagh;Laht;Lage;Lcom/twitter/android/search/AdvancedSearchFiltersActivity$b;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Lahn$1;

    invoke-direct {v0, p0}, Lahn$1;-><init>(Lahn;)V

    iput-object v0, p0, Lahn;->f:Landroid/widget/AdapterView$OnItemClickListener;

    .line 60
    iput-object p1, p0, Lahn;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    .line 61
    iput-object p2, p0, Lahn;->b:Lahz;

    .line 62
    iput-object p3, p0, Lahn;->c:Lagh;

    .line 63
    iput-object p5, p0, Lahn;->d:Lage;

    .line 64
    iput-object p4, p0, Lahn;->e:Laht;

    .line 66
    invoke-virtual {p4, p5}, Laht;->a(Lage;)V

    .line 68
    invoke-virtual {p6, p0}, Lcom/twitter/android/search/AdvancedSearchFiltersActivity$b;->a(Lakv;)V

    .line 70
    invoke-virtual {p3}, Lagh;->b()I

    move-result v0

    invoke-direct {p0, p1, p3, v0}, Lahn;->a(Lcom/twitter/app/common/base/BaseFragmentActivity;Lagh;I)V

    .line 71
    return-void
.end method

.method static synthetic a(Lahn;)Lahz;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lahn;->b:Lahz;

    return-object v0
.end method

.method private a(Lcom/twitter/app/common/base/BaseFragmentActivity;Lagh;I)V
    .locals 7

    .prologue
    .line 85
    invoke-virtual {p2}, Lagh;->a()Ljava/util/List;

    move-result-object v2

    .line 86
    new-instance v5, Lcom/twitter/android/at;

    invoke-direct {v5, v2}, Lcom/twitter/android/at;-><init>(Ljava/util/List;)V

    .line 87
    iget-object v0, p0, Lahn;->b:Lahz;

    invoke-virtual {v0, v5}, Lahz;->a(Landroid/widget/ListAdapter;)V

    .line 88
    iget-object v0, p0, Lahn;->b:Lahz;

    iget-object v1, p0, Lahn;->f:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lahz;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 89
    new-instance v0, Lcom/twitter/android/r;

    iget-object v1, p0, Lahn;->b:Lahz;

    invoke-virtual {v1}, Lahz;->c()Landroid/support/v4/view/ViewPager;

    move-result-object v3

    iget-object v1, p0, Lahn;->b:Lahz;

    .line 90
    invoke-virtual {v1}, Lahz;->b()Lcom/twitter/internal/android/widget/HorizontalListView;

    move-result-object v4

    new-instance v6, Lcom/twitter/android/q;

    iget-object v1, p0, Lahn;->b:Lahz;

    invoke-virtual {v1}, Lahz;->d()Lcom/twitter/android/composer/ComposerDockLayout;

    move-result-object v1

    invoke-direct {v6, v1}, Lcom/twitter/android/q;-><init>(Lcom/twitter/internal/android/widget/DockLayout;)V

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/r;-><init>(Landroid/support/v4/app/FragmentActivity;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;Lcom/twitter/android/q;)V

    .line 91
    iget-object v1, p0, Lahn;->b:Lahz;

    invoke-virtual {v1, v0}, Lahz;->a(Landroid/support/v4/view/PagerAdapter;)V

    .line 92
    iget-object v1, p0, Lahn;->b:Lahz;

    invoke-virtual {v1, p3}, Lahz;->a(I)V

    .line 93
    new-instance v1, Lahn$2;

    invoke-direct {v1, p0, p2}, Lahn$2;-><init>(Lahn;Lagh;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/r;->a(Lcom/twitter/android/AbsPagesAdapter$a;)V

    .line 100
    return-void
.end method

.method static synthetic b(Lahn;)Laht;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lahn;->e:Laht;

    return-object v0
.end method

.method private b(Lage;)Z
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lahn;->d:Lage;

    invoke-virtual {v0}, Lage;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lage;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(ILaii;)V
    .locals 3

    .prologue
    .line 75
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    .line 76
    iget-object v0, p0, Lahn;->c:Lagh;

    invoke-virtual {v0, p2}, Lagh;->a(Laii;)V

    .line 77
    iget-object v0, p0, Lahn;->b:Lahz;

    invoke-virtual {v0}, Lahz;->f()I

    move-result v0

    .line 78
    iget-object v1, p0, Lahn;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    iget-object v2, p0, Lahn;->c:Lagh;

    invoke-direct {p0, v1, v2, v0}, Lahn;->a(Lcom/twitter/app/common/base/BaseFragmentActivity;Lagh;I)V

    .line 80
    :cond_0
    return-void
.end method

.method public bridge synthetic a(ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 32
    check-cast p2, Laii;

    invoke-virtual {p0, p1, p2}, Lahn;->a(ILaii;)V

    return-void
.end method

.method public a(Lage;)Z
    .locals 2

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lahn;->b(Lage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lahn;->c:Lagh;

    invoke-virtual {p1}, Lage;->h()I

    move-result v1

    invoke-virtual {v0, v1}, Lagh;->a(I)I

    move-result v0

    .line 113
    iget-object v1, p0, Lahn;->b:Lahz;

    invoke-virtual {v1, v0}, Lahz;->a(I)V

    .line 114
    const/4 v0, 0x1

    .line 116
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lahn;->b:Lahz;

    invoke-virtual {v0}, Lahz;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
