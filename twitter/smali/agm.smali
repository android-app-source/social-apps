.class public final Lagm;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lagj;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lagm$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/base/BaseFragmentActivity;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lahx;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/search/AdvancedSearchFiltersActivity$a;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laht;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lahk;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laid;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lagm;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lagm;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lagm$a;)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    sget-boolean v0, Lagm;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 62
    :cond_0
    invoke-direct {p0, p1}, Lagm;->a(Lagm$a;)V

    .line 63
    return-void
.end method

.method synthetic constructor <init>(Lagm$a;Lagm$1;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lagm;-><init>(Lagm$a;)V

    return-void
.end method

.method private a(Lagm$a;)V
    .locals 5

    .prologue
    .line 72
    .line 74
    invoke-static {p1}, Lagm$a;->a(Lagm$a;)Lant;

    move-result-object v0

    invoke-static {v0}, Lanu;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 73
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagm;->b:Lcta;

    .line 76
    iget-object v0, p0, Lagm;->b:Lcta;

    .line 78
    invoke-static {v0}, Lanv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 77
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagm;->c:Lcta;

    .line 81
    iget-object v0, p0, Lagm;->b:Lcta;

    .line 83
    invoke-static {v0}, Lanw;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 82
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagm;->d:Lcta;

    .line 85
    iget-object v0, p0, Lagm;->b:Lcta;

    .line 87
    invoke-static {v0}, Lanx;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 86
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagm;->e:Lcta;

    .line 89
    iget-object v0, p0, Lagm;->e:Lcta;

    .line 91
    invoke-static {v0}, Lahy;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 90
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagm;->f:Lcta;

    .line 93
    iget-object v0, p0, Lagm;->b:Lcta;

    .line 95
    invoke-static {v0}, Lagl;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 94
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagm;->g:Lcta;

    .line 98
    new-instance v0, Lagm$1;

    invoke-direct {v0, p0, p1}, Lagm$1;-><init>(Lagm;Lagm$a;)V

    iput-object v0, p0, Lagm;->h:Lcta;

    .line 111
    iget-object v0, p0, Lagm;->h:Lcta;

    .line 112
    invoke-static {v0}, Lahu;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagm;->i:Lcta;

    .line 114
    iget-object v0, p0, Lagm;->c:Lcta;

    iget-object v1, p0, Lagm;->d:Lcta;

    iget-object v2, p0, Lagm;->f:Lcta;

    iget-object v3, p0, Lagm;->g:Lcta;

    iget-object v4, p0, Lagm;->i:Lcta;

    .line 116
    invoke-static {v0, v1, v2, v3, v4}, Lahl;->a(Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 115
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagm;->j:Lcta;

    .line 126
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lagm;->j:Lcta;

    .line 125
    invoke-static {v0, v1}, Laie;->a(Lcsd;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 124
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lagm;->k:Lcta;

    .line 128
    return-void
.end method

.method public static d()Lagm$a;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Lagm$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lagm$a;-><init>(Lagm$1;)V

    return-object v0
.end method


# virtual methods
.method public synthetic a()Laog;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lagm;->c()Laid;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    invoke-static {}, Ldagger/internal/f;->a()Ldagger/internal/c;

    move-result-object v0

    invoke-interface {v0}, Ldagger/internal/c;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c()Laid;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lagm;->k:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laid;

    return-object v0
.end method
