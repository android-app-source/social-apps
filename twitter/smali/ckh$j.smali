.class public final Lckh$j;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lckh;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "j"
.end annotation


# static fields
.field public static final abbr_number_unit_billions:I = 0x7f0a0b7c

.field public static final abbr_number_unit_millions:I = 0x7f0a0b7a

.field public static final abbr_number_unit_thousands:I = 0x7f0a0b7b

.field public static final abc_action_bar_home_description:I = 0x7f0a0000

.field public static final abc_action_bar_home_description_format:I = 0x7f0a0001

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f0a0002

.field public static final abc_action_bar_up_description:I = 0x7f0a0003

.field public static final abc_action_menu_overflow_description:I = 0x7f0a0004

.field public static final abc_action_mode_done:I = 0x7f0a0005

.field public static final abc_activity_chooser_view_see_all:I = 0x7f0a0006

.field public static final abc_activitychooserview_choose_application:I = 0x7f0a0007

.field public static final abc_capital_off:I = 0x7f0a0008

.field public static final abc_capital_on:I = 0x7f0a0009

.field public static final abc_font_family_body_1_material:I = 0x7f0a0b7d

.field public static final abc_font_family_body_2_material:I = 0x7f0a0b7e

.field public static final abc_font_family_button_material:I = 0x7f0a0b7f

.field public static final abc_font_family_caption_material:I = 0x7f0a0b80

.field public static final abc_font_family_display_1_material:I = 0x7f0a0b81

.field public static final abc_font_family_display_2_material:I = 0x7f0a0b82

.field public static final abc_font_family_display_3_material:I = 0x7f0a0b83

.field public static final abc_font_family_display_4_material:I = 0x7f0a0b84

.field public static final abc_font_family_headline_material:I = 0x7f0a0b85

.field public static final abc_font_family_menu_material:I = 0x7f0a0b86

.field public static final abc_font_family_subhead_material:I = 0x7f0a0b87

.field public static final abc_font_family_title_material:I = 0x7f0a0b88

.field public static final abc_search_hint:I = 0x7f0a000a

.field public static final abc_searchview_description_clear:I = 0x7f0a000b

.field public static final abc_searchview_description_query:I = 0x7f0a000c

.field public static final abc_searchview_description_search:I = 0x7f0a000d

.field public static final abc_searchview_description_submit:I = 0x7f0a000e

.field public static final abc_searchview_description_voice:I = 0x7f0a000f

.field public static final abc_shareactionprovider_share_with:I = 0x7f0a0010

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f0a0011

.field public static final abc_toolbar_collapse_description:I = 0x7f0a0012

.field public static final amazon_app_store_url_format:I = 0x7f0a0b8e

.field public static final appbar_scrolling_view_behavior:I = 0x7f0a0b91

.field public static final bottom_sheet_behavior:I = 0x7f0a0b94

.field public static final character_counter_pattern:I = 0x7f0a0b96

.field public static final com_crashlytics_android_build_id:I = 0x7f0a0b98

.field public static final date_format_long:I = 0x7f0a0b71

.field public static final date_format_long_accessible:I = 0x7f0a0b9b

.field public static final date_format_short:I = 0x7f0a0b72

.field public static final date_format_short_accessible:I = 0x7f0a0b9c

.field public static final file_photo_name:I = 0x7f0a0ba5

.field public static final file_video_name:I = 0x7f0a0ba6

.field public static final foot_abbr:I = 0x7f0a0ba8

.field public static final gallery:I = 0x7f0a0ba9

.field public static final google_play_details_url_format:I = 0x7f0a0bc2

.field public static final google_play_web_details_url_format:I = 0x7f0a0bc3

.field public static final kilometer:I = 0x7f0a0434

.field public static final meter:I = 0x7f0a054d

.field public static final mile_abbr:I = 0x7f0a0bd6

.field public static final now:I = 0x7f0a0611

.field public static final recent_tweets_header_title:I = 0x7f0a074a

.field public static final search_menu_title:I = 0x7f0a0029

.field public static final status_bar_notification_info_overflow:I = 0x7f0a002a

.field public static final time_of_day_format:I = 0x7f0a0c37

.field public static final twitter_edit_text_counter_format_countdown:I = 0x7f0a0c3f

.field public static final twitter_edit_text_counter_format_normal:I = 0x7f0a0c40
