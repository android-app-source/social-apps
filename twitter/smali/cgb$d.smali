.class public final Lcgb$d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcgb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcgb$d$b;,
        Lcgb$d$a;
    }
.end annotation


# static fields
.field public static final a:Lcgb$d$b;


# instance fields
.field public final b:Lcgb$e;

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcgb$b;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcgb$c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57
    new-instance v0, Lcgb$d$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcgb$d$b;-><init>(Lcgb$1;)V

    sput-object v0, Lcgb$d;->a:Lcgb$d$b;

    return-void
.end method

.method private constructor <init>(Lcgb$d$a;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    invoke-static {p1}, Lcgb$d$a;->a(Lcgb$d$a;)Lcgb$e;

    move-result-object v0

    iput-object v0, p0, Lcgb$d;->b:Lcgb$e;

    .line 69
    invoke-static {p1}, Lcgb$d$a;->b(Lcgb$d$a;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcgb$d;->c:Ljava/util/Map;

    .line 70
    invoke-static {p1}, Lcgb$d$a;->c(Lcgb$d$a;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcgb$d;->d:Ljava/util/List;

    .line 71
    return-void
.end method

.method synthetic constructor <init>(Lcgb$d$a;Lcgb$1;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcgb$d;-><init>(Lcgb$d$a;)V

    return-void
.end method
