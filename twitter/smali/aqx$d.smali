.class final Laqx$d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Larp;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Laqx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Laqx;

.field private final b:Lant;

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/base/BaseFragmentActivity;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/support/v4/app/FragmentManager;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/d;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/Flow$Step;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laqw;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/model/onboarding/TaskContext;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laqs;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/onboarding/Subtask;",
            "Landroid/content/Intent;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcje;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcjg;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcjd;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/k;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lank;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/l;",
            ">;>;"
        }
    .end annotation
.end field

.field private s:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/l;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/i;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/b;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Larw;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Laqx;Lant;)V
    .locals 1

    .prologue
    .line 584
    iput-object p1, p0, Laqx$d;->a:Laqx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 585
    invoke-static {p2}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lant;

    iput-object v0, p0, Laqx$d;->b:Lant;

    .line 586
    invoke-direct {p0}, Laqx$d;->c()V

    .line 587
    return-void
.end method

.method synthetic constructor <init>(Laqx;Lant;Laqx$1;)V
    .locals 0

    .prologue
    .line 536
    invoke-direct {p0, p1, p2}, Laqx$d;-><init>(Laqx;Lant;)V

    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 592
    iget-object v0, p0, Laqx$d;->b:Lant;

    .line 594
    invoke-static {v0}, Lanu;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 593
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->c:Lcta;

    .line 596
    iget-object v0, p0, Laqx$d;->c:Lcta;

    .line 598
    invoke-static {v0}, Lanx;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 597
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->d:Lcta;

    .line 600
    iget-object v0, p0, Laqx$d;->c:Lcta;

    .line 602
    invoke-static {v0}, Lanv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 601
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->e:Lcta;

    .line 605
    iget-object v0, p0, Laqx$d;->e:Lcta;

    .line 607
    invoke-static {v0}, Laob;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 606
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->f:Lcta;

    .line 610
    iget-object v0, p0, Laqx$d;->c:Lcta;

    iget-object v1, p0, Laqx$d;->d:Lcta;

    iget-object v2, p0, Laqx$d;->f:Lcta;

    .line 612
    invoke-static {v0, v1, v2}, Lcom/twitter/app/onboarding/flowstep/common/e;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 611
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->g:Lcta;

    .line 618
    invoke-static {}, Laro;->c()Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->h:Lcta;

    .line 620
    iget-object v0, p0, Laqx$d;->h:Lcta;

    iget-object v1, p0, Laqx$d;->f:Lcta;

    .line 622
    invoke-static {v0, v1}, Larh;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 621
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->i:Lcta;

    .line 625
    iget-object v0, p0, Laqx$d;->c:Lcta;

    .line 627
    invoke-static {v0}, Laqv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 626
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->j:Lcta;

    .line 630
    iget-object v0, p0, Laqx$d;->a:Laqx;

    .line 632
    invoke-static {v0}, Laqx;->a(Laqx;)Lcta;

    move-result-object v0

    .line 631
    invoke-static {v0}, Laqt;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->k:Lcta;

    .line 634
    iget-object v0, p0, Laqx$d;->k:Lcta;

    .line 635
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->l:Lcta;

    .line 637
    iget-object v0, p0, Laqx$d;->l:Lcta;

    .line 639
    invoke-static {v0}, Lcjf;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 638
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->m:Lcta;

    .line 641
    iget-object v0, p0, Laqx$d;->j:Lcta;

    iget-object v1, p0, Laqx$d;->m:Lcta;

    iget-object v2, p0, Laqx$d;->a:Laqx;

    .line 646
    invoke-static {v2}, Laqx;->b(Laqx;)Lcta;

    move-result-object v2

    .line 647
    invoke-static {}, Lcjc;->c()Ldagger/internal/c;

    move-result-object v3

    .line 643
    invoke-static {v0, v1, v2, v3}, Lcjh;->a(Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 642
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->n:Lcta;

    .line 649
    iget-object v0, p0, Laqx$d;->n:Lcta;

    .line 650
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->o:Lcta;

    .line 652
    iget-object v0, p0, Laqx$d;->g:Lcta;

    .line 654
    invoke-static {v0}, Lari;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 653
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->p:Lcta;

    .line 657
    iget-object v0, p0, Laqx$d;->b:Lant;

    .line 659
    invoke-static {v0}, Laoa;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 658
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->q:Lcta;

    .line 661
    iget-object v0, p0, Laqx$d;->q:Lcta;

    .line 663
    invoke-static {v0}, Larj;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 662
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->r:Lcta;

    .line 666
    iget-object v0, p0, Laqx$d;->r:Lcta;

    .line 668
    invoke-static {v0}, Lcom/twitter/app/onboarding/flowstep/common/m;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 667
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->s:Lcta;

    .line 671
    iget-object v0, p0, Laqx$d;->p:Lcta;

    iget-object v1, p0, Laqx$d;->s:Lcta;

    .line 673
    invoke-static {v0, v1}, Lcom/twitter/app/onboarding/flowstep/common/j;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 672
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->t:Lcta;

    .line 676
    iget-object v0, p0, Laqx$d;->g:Lcta;

    iget-object v1, p0, Laqx$d;->i:Lcta;

    iget-object v2, p0, Laqx$d;->o:Lcta;

    iget-object v3, p0, Laqx$d;->t:Lcta;

    .line 678
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/app/onboarding/flowstep/common/c;->a(Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 677
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->u:Lcta;

    .line 687
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Laqx$d;->u:Lcta;

    iget-object v2, p0, Laqx$d;->s:Lcta;

    iget-object v3, p0, Laqx$d;->t:Lcta;

    .line 686
    invoke-static {v0, v1, v2, v3}, Larx;->a(Lcsd;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 685
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->v:Lcta;

    .line 692
    iget-object v0, p0, Laqx$d;->v:Lcta;

    .line 693
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Laqx$d;->w:Lcta;

    .line 694
    return-void
.end method


# virtual methods
.method public a()Laog;
    .locals 1

    .prologue
    .line 703
    iget-object v0, p0, Laqx$d;->w:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laog;

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 698
    invoke-static {}, Ldagger/internal/f;->a()Ldagger/internal/c;

    move-result-object v0

    invoke-interface {v0}, Ldagger/internal/c;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method
