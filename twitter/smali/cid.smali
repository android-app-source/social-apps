.class public Lcid;
.super Lcho;
.source "Twttr"

# interfaces
.implements Lcho$a;


# instance fields
.field public final a:Lcic;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLcom/twitter/model/timeline/r;Lchj;Lcic;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct/range {p0 .. p5}, Lcho;-><init>(Ljava/lang/String;JLcom/twitter/model/timeline/r;Lchj;)V

    .line 21
    iput-object p6, p0, Lcid;->a:Lcic;

    .line 22
    return-void
.end method


# virtual methods
.method public synthetic a(Lcgx;Lchc;)Lcom/twitter/model/timeline/y$a;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0, p1, p2}, Lcid;->d(Lcgx;Lchc;)Lcom/twitter/model/timeline/an$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lcgx;Lchc;)Lcom/twitter/model/timeline/y;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0, p1, p2}, Lcid;->c(Lcgx;Lchc;)Lcom/twitter/model/timeline/an;

    move-result-object v0

    return-object v0
.end method

.method public c(Lcgx;Lchc;)Lcom/twitter/model/timeline/an;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0, p1, p2}, Lcid;->d(Lcgx;Lchc;)Lcom/twitter/model/timeline/an$a;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Lcom/twitter/model/timeline/an$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/an;

    .line 28
    return-object v0
.end method

.method public d(Lcgx;Lchc;)Lcom/twitter/model/timeline/an$a;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 36
    iget-object v0, p0, Lcid;->a:Lcic;

    iget-object v0, v0, Lcic;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcgx;->b(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v3

    .line 38
    iget-object v0, p0, Lcid;->e:Lchj;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcid;->e:Lchj;

    iget-object v0, v0, Lchj;->b:Ljava/lang/String;

    move-object v1, v0

    .line 39
    :goto_0
    iget-object v0, p0, Lcid;->e:Lchj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcid;->e:Lchj;

    iget-object v0, v0, Lchj;->a:Ljava/util/List;

    .line 40
    invoke-virtual {p2, v0}, Lchc;->a(Ljava/util/List;)Lcom/twitter/model/timeline/k;

    move-result-object v2

    .line 42
    :cond_0
    new-instance v0, Lcom/twitter/model/timeline/an$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/an$a;-><init>()V

    .line 43
    invoke-virtual {v0, v3}, Lcom/twitter/model/timeline/an$a;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/timeline/an$a;

    move-result-object v0

    iget-object v3, p0, Lcid;->a:Lcic;

    iget-object v3, v3, Lcic;->d:Lcgi;

    .line 44
    invoke-virtual {v0, v3}, Lcom/twitter/model/timeline/an$a;->a(Lcgi;)Lcom/twitter/model/timeline/an$a;

    move-result-object v0

    iget-object v3, p0, Lcid;->a:Lcic;

    iget-object v3, v3, Lcic;->c:Ljava/lang/String;

    .line 45
    invoke-virtual {v0, v3}, Lcom/twitter/model/timeline/an$a;->e(Ljava/lang/String;)Lcom/twitter/model/timeline/an$a;

    move-result-object v0

    iget-wide v4, p0, Lcid;->c:J

    .line 46
    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/timeline/an$a;->a(J)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/an$a;

    iget-object v3, p0, Lcid;->b:Ljava/lang/String;

    .line 47
    invoke-virtual {v0, v3}, Lcom/twitter/model/timeline/an$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/an$a;

    iget-object v3, p0, Lcid;->a:Lcic;

    iget-object v3, v3, Lcic;->e:Lcom/twitter/model/core/TwitterSocialProof;

    .line 48
    invoke-virtual {v0, v3}, Lcom/twitter/model/timeline/an$a;->a(Lcom/twitter/model/core/TwitterSocialProof;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/an$a;

    .line 49
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/an$a;->a(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/an$a;

    .line 50
    invoke-virtual {v0, v2}, Lcom/twitter/model/timeline/an$a;->a(Lcom/twitter/model/timeline/k;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/an$a;

    iget-object v1, p0, Lcid;->d:Lcom/twitter/model/timeline/r;

    .line 51
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/an$a;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/an$a;

    .line 42
    return-object v0

    :cond_1
    move-object v1, v2

    .line 38
    goto :goto_0
.end method
