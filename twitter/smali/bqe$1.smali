.class final Lbqe$1;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbqe;->a(Landroid/content/Context;JLjava/lang/String;Laud$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:J


# direct methods
.method constructor <init>(J)V
    .locals 1

    .prologue
    .line 61
    iput-wide p1, p0, Lbqe$1;->a:J

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 61
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lbqe$1;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 5

    .prologue
    .line 64
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 65
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    .line 66
    iget-wide v2, p0, Lbqe$1;->a:J

    .line 67
    invoke-static {v2, v3}, Laud;->c(J)Laud$a;

    move-result-object v1

    .line 68
    if-nez v1, :cond_1

    .line 70
    const-string/jumbo v0, "fs:load:fetched_manifest"

    .line 71
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v1

    iget-wide v2, p0, Lbqe$1;->a:J

    sget-object v4, Lcom/twitter/metrics/g;->m:Lcom/twitter/metrics/g$b;

    .line 70
    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/metrics/n;->b(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;)Lcom/twitter/metrics/n;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/twitter/metrics/n;->k()V

    .line 83
    :cond_0
    :goto_0
    invoke-static {}, Lbqe;->a()Ljava/util/Map;

    move-result-object v0

    iget-wide v2, p0, Lbqe$1;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    return-void

    .line 74
    :cond_1
    invoke-virtual {v1, v0}, Laud$a;->a(Z)V

    .line 75
    if-eqz v0, :cond_0

    .line 76
    iget-wide v0, p0, Lbqe$1;->a:J

    .line 77
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    .line 76
    invoke-static {v0, v1, v2, v3}, Laud;->a(JJ)V

    .line 78
    const-string/jumbo v0, "fs:load:fetched_manifest"

    .line 79
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v1

    iget-wide v2, p0, Lbqe$1;->a:J

    sget-object v4, Lcom/twitter/metrics/g;->m:Lcom/twitter/metrics/g$b;

    .line 78
    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/metrics/n;->b(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;)Lcom/twitter/metrics/n;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lcom/twitter/metrics/n;->j()V

    goto :goto_0
.end method
