.class public Lbsq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcpk;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbsq$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/twitter/library/api/PromotedEvent;

.field public final b:Ljava/lang/String;

.field public final c:J

.field public final d:Ljava/lang/String;

.field public final e:Z

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public final m:Ljava/lang/String;

.field public final n:Ljava/lang/String;

.field public final o:J


# direct methods
.method private constructor <init>(Lbsq$a;)V
    .locals 2

    .prologue
    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    invoke-static {p1}, Lbsq$a;->a(Lbsq$a;)Lcom/twitter/library/api/PromotedEvent;

    move-result-object v0

    iput-object v0, p0, Lbsq;->a:Lcom/twitter/library/api/PromotedEvent;

    .line 159
    invoke-static {p1}, Lbsq$a;->b(Lbsq$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lbsq;->c:J

    .line 160
    invoke-static {p1}, Lbsq$a;->c(Lbsq$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbsq;->d:Ljava/lang/String;

    .line 161
    invoke-static {p1}, Lbsq$a;->d(Lbsq$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbsq;->b:Ljava/lang/String;

    .line 162
    invoke-static {p1}, Lbsq$a;->e(Lbsq$a;)Z

    move-result v0

    iput-boolean v0, p0, Lbsq;->e:Z

    .line 163
    invoke-static {p1}, Lbsq$a;->f(Lbsq$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbsq;->f:Ljava/lang/String;

    .line 164
    invoke-static {p1}, Lbsq$a;->g(Lbsq$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbsq;->g:Ljava/lang/String;

    .line 165
    invoke-static {p1}, Lbsq$a;->h(Lbsq$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbsq;->h:Ljava/lang/String;

    .line 166
    invoke-static {p1}, Lbsq$a;->i(Lbsq$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbsq;->i:Ljava/lang/String;

    .line 167
    invoke-static {p1}, Lbsq$a;->j(Lbsq$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbsq;->j:Ljava/lang/String;

    .line 168
    invoke-static {p1}, Lbsq$a;->k(Lbsq$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbsq;->k:Ljava/lang/String;

    .line 169
    invoke-static {p1}, Lbsq$a;->l(Lbsq$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbsq;->l:Ljava/lang/String;

    .line 170
    invoke-static {p1}, Lbsq$a;->m(Lbsq$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbsq;->m:Ljava/lang/String;

    .line 171
    invoke-static {p1}, Lbsq$a;->n(Lbsq$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbsq;->n:Ljava/lang/String;

    .line 172
    invoke-static {p1}, Lbsq$a;->o(Lbsq$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lbsq;->o:J

    .line 173
    return-void
.end method

.method synthetic constructor <init>(Lbsq$a;Lbsq$1;)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lbsq;-><init>(Lbsq$a;)V

    return-void
.end method

.method public static a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;
    .locals 2

    .prologue
    .line 211
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 212
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "event and pc must not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 214
    :cond_1
    new-instance v0, Lbsq$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lbsq$a;-><init>(Lbsq$1;)V

    .line 215
    invoke-virtual {v0, p0}, Lbsq$a;->a(Lcom/twitter/library/api/PromotedEvent;)Lbsq$a;

    move-result-object v0

    .line 216
    invoke-virtual {v0, p1}, Lbsq$a;->a(Lcgi;)Lbsq$a;

    move-result-object v0

    .line 214
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 177
    if-ne p0, p1, :cond_1

    .line 186
    :cond_0
    :goto_0
    return v0

    .line 180
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 181
    goto :goto_0

    .line 184
    :cond_3
    check-cast p1, Lbsq;

    .line 186
    iget-object v2, p0, Lbsq;->a:Lcom/twitter/library/api/PromotedEvent;

    iget-object v3, p1, Lbsq;->a:Lcom/twitter/library/api/PromotedEvent;

    if-ne v2, v3, :cond_4

    iget-wide v2, p0, Lbsq;->c:J

    iget-wide v4, p1, Lbsq;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lbsq;->e:Z

    iget-boolean v3, p1, Lbsq;->e:Z

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lbsq;->b:Ljava/lang/String;

    iget-object v3, p1, Lbsq;->b:Ljava/lang/String;

    .line 189
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lbsq;->d:Ljava/lang/String;

    iget-object v3, p1, Lbsq;->d:Ljava/lang/String;

    .line 190
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lbsq;->f:Ljava/lang/String;

    iget-object v3, p1, Lbsq;->f:Ljava/lang/String;

    .line 191
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lbsq;->j:Ljava/lang/String;

    iget-object v3, p1, Lbsq;->j:Ljava/lang/String;

    .line 192
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lbsq;->k:Ljava/lang/String;

    iget-object v3, p1, Lbsq;->k:Ljava/lang/String;

    .line 193
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lbsq;->i:Ljava/lang/String;

    iget-object v3, p1, Lbsq;->i:Ljava/lang/String;

    .line 194
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lbsq;->h:Ljava/lang/String;

    iget-object v3, p1, Lbsq;->h:Ljava/lang/String;

    .line 195
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lbsq;->g:Ljava/lang/String;

    iget-object v3, p1, Lbsq;->g:Ljava/lang/String;

    .line 196
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lbsq;->l:Ljava/lang/String;

    iget-object v3, p1, Lbsq;->l:Ljava/lang/String;

    .line 197
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lbsq;->m:Ljava/lang/String;

    iget-object v3, p1, Lbsq;->m:Ljava/lang/String;

    .line 198
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lbsq;->n:Ljava/lang/String;

    iget-object v3, p1, Lbsq;->n:Ljava/lang/String;

    .line 199
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-wide v2, p0, Lbsq;->o:J

    .line 200
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p1, Lbsq;->o:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 205
    iget-object v0, p0, Lbsq;->a:Lcom/twitter/library/api/PromotedEvent;

    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lbsq;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lbsq;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lbsq;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-boolean v3, p0, Lbsq;->e:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lbsq;->f:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lbsq;->g:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Lbsq;->h:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, Lbsq;->i:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-object v3, p0, Lbsq;->j:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    iget-object v3, p0, Lbsq;->k:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    iget-object v3, p0, Lbsq;->l:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    iget-object v3, p0, Lbsq;->m:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    iget-object v3, p0, Lbsq;->n:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    iget-wide v4, p0, Lbsq;->o:J

    .line 207
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 205
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;[Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
