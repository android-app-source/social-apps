.class public Lath;
.super Laog;
.source "Twttr"

# interfaces
.implements Latd;
.implements Lcom/twitter/android/widget/GalleryGridFragment$a;
.implements Lcom/twitter/android/widget/m$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lath$a;
    }
.end annotation


# static fields
.field private static final b:I

.field private static final c:I


# instance fields
.field protected final a:Landroid/support/v4/app/FragmentActivity;

.field private final d:Lcom/twitter/android/composer/TweetBox;

.field private final e:Lcom/twitter/android/composer/ComposerCountView;

.field private final f:Landroid/widget/Button;

.field private final g:Landroid/view/View;

.field private final h:Lcom/twitter/app/common/base/d;

.field private final i:Lcom/twitter/android/composer/x;

.field private final j:Landroid/widget/TextView;

.field private final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lcom/twitter/android/media/selection/c;

.field private final m:Lcom/twitter/android/widget/j;

.field private final n:Z

.field private o:Lcom/twitter/android/media/widget/InlineComposerMediaLayout;

.field private p:Landroid/view/ViewGroup;

.field private q:Lcom/twitter/android/media/selection/MediaAttachment;

.field private r:Z

.field private s:I

.field private final t:Late;

.field private u:Lcom/twitter/model/core/Tweet;

.field private v:Latg$a;

.field private w:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    invoke-static {}, Lcom/twitter/app/common/base/d;->a()I

    move-result v0

    sput v0, Lath;->b:I

    .line 77
    invoke-static {}, Lcom/twitter/app/common/base/d;->a()I

    move-result v0

    sput v0, Lath;->c:I

    return-void
.end method

.method protected constructor <init>(Lath$a;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lath$a",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/4 v12, 0x1

    const/4 v13, 0x0

    .line 191
    invoke-direct {p0}, Laog;-><init>()V

    .line 108
    iput v13, p0, Lath;->s:I

    .line 192
    iget-object v0, p1, Lath$a;->b:Landroid/view/View;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/view/View;

    .line 194
    iget-object v0, p1, Lath$a;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_4

    .line 195
    iget-object v0, p1, Lath$a;->d:Landroid/os/Bundle;

    invoke-static {v0}, Lank;->a(Landroid/os/Bundle;)Lank;

    move-result-object v0

    invoke-virtual {p0}, Lath;->al_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lank;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 196
    if-eqz v0, :cond_3

    .line 197
    const-string/jumbo v1, "sticky"

    invoke-virtual {v0, v1, v13}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lath;->w:Z

    .line 199
    const-string/jumbo v1, "launch_camera_mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lath;->s:I

    .line 200
    const-string/jumbo v1, "excluded_users"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 201
    const-string/jumbo v1, "excluded_users"

    .line 202
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    .line 201
    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iput-object v1, p0, Lath;->k:Ljava/util/List;

    move-object v10, v0

    .line 213
    :goto_0
    iget-object v0, p1, Lath$a;->c:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    iput-object v0, p0, Lath;->a:Landroid/support/v4/app/FragmentActivity;

    .line 214
    iget-object v0, p1, Lath$a;->f:Lcom/twitter/android/media/selection/c;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/selection/c;

    iput-object v0, p0, Lath;->l:Lcom/twitter/android/media/selection/c;

    .line 216
    new-instance v0, Lcom/twitter/android/widget/k;

    iget-object v1, p1, Lath$a;->c:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p1, Lath$a;->d:Landroid/os/Bundle;

    if-eqz v2, :cond_5

    move v2, v12

    :goto_1
    iget-object v3, p0, Lath;->l:Lcom/twitter/android/media/selection/c;

    const v4, 0x7f1307fa

    .line 217
    invoke-virtual {v9, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f13039f

    new-instance v8, Lath$1;

    invoke-direct {v8, p0}, Lath$1;-><init>(Lath;)V

    move-object v6, p0

    move-object v7, p0

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/widget/k;-><init>(Landroid/support/v4/app/FragmentActivity;ZLcom/twitter/android/media/selection/c;Landroid/view/View;ILcom/twitter/android/widget/GalleryGridFragment$a;Lcom/twitter/android/widget/m$a;Lcom/twitter/android/media/selection/d;)V

    iput-object v0, p0, Lath;->m:Lcom/twitter/android/widget/j;

    .line 246
    iget-object v0, p1, Lath$a;->g:Late;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Late;

    iput-object v0, p0, Lath;->t:Late;

    .line 247
    iget-object v0, p0, Lath;->t:Late;

    new-instance v1, Lath$3;

    invoke-direct {v1, p0}, Lath$3;-><init>(Lath;)V

    invoke-virtual {v0, v1}, Late;->a(Lcom/twitter/util/q;)Z

    .line 260
    invoke-static {}, Lbpj;->b()Z

    move-result v0

    iput-boolean v0, p0, Lath;->n:Z

    .line 261
    const v0, 0x7f130277

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lath;->j:Landroid/widget/TextView;

    .line 262
    new-instance v0, Lcom/twitter/android/composer/x;

    iget-object v1, p1, Lath$a;->c:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lath;->j:Landroid/widget/TextView;

    invoke-direct {v0, v1, v12, v2, v11}, Lcom/twitter/android/composer/x;-><init>(Landroid/content/res/Resources;ZLandroid/widget/TextView;Landroid/widget/TextView;)V

    iput-object v0, p0, Lath;->i:Lcom/twitter/android/composer/x;

    .line 264
    iget-object v0, p1, Lath$a;->e:Lcom/twitter/app/common/base/d;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/base/d;

    iput-object v0, p0, Lath;->h:Lcom/twitter/app/common/base/d;

    .line 265
    iget-object v0, p0, Lath;->h:Lcom/twitter/app/common/base/d;

    sget v1, Lath;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v2, v12, [Ljava/lang/Integer;

    sget v3, Lath;->b:I

    .line 266
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v13

    .line 265
    invoke-static {v1, v2}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lath$4;

    invoke-direct {v2, p0}, Lath$4;-><init>(Lath;)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/base/d;->a(Ljava/lang/Iterable;Lcom/twitter/app/common/base/d$a;)V

    .line 283
    invoke-virtual {p0, v9}, Lath;->a(Landroid/view/View;)V

    .line 285
    const v0, 0x7f130278

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/composer/TweetBox;

    iput-object v0, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    .line 286
    iget-object v0, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setSession(Lcom/twitter/library/client/Session;)V

    .line 287
    const v0, 0x7f130293

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/composer/ComposerCountView;

    iput-object v0, p0, Lath;->e:Lcom/twitter/android/composer/ComposerCountView;

    .line 288
    const v0, 0x7f1302a3

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lath;->f:Landroid/widget/Button;

    .line 289
    const v0, 0x7f13062d

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lath;->g:Landroid/view/View;

    .line 291
    iget-object v0, p0, Lath;->f:Landroid/widget/Button;

    new-instance v1, Lath$5;

    invoke-direct {v1, p0}, Lath$5;-><init>(Lath;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 300
    iget-boolean v0, p0, Lath;->w:Z

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lath;->g:Landroid/view/View;

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    .line 304
    :cond_0
    iget-object v0, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    new-instance v1, Lath$6;

    invoke-direct {v1, p0}, Lath$6;-><init>(Lath;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setTweetBoxListener(Lcom/twitter/android/composer/TweetBox$b;)V

    .line 383
    invoke-direct {p0}, Lath;->v()V

    .line 384
    invoke-direct {p0}, Lath;->u()V

    .line 385
    invoke-direct {p0, v10}, Lath;->b(Landroid/os/Bundle;)V

    .line 387
    iget-object v0, p1, Lath$a;->h:Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 388
    iget-object v0, p0, Lath;->f:Landroid/widget/Button;

    iget-object v1, p1, Lath$a;->h:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 390
    :cond_1
    return-void

    .line 204
    :cond_2
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lath;->k:Ljava/util/List;

    move-object v10, v0

    goto/16 :goto_0

    .line 207
    :cond_3
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lath;->k:Ljava/util/List;

    move-object v10, v0

    goto/16 :goto_0

    .line 211
    :cond_4
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lath;->k:Ljava/util/List;

    move-object v10, v11

    goto/16 :goto_0

    :cond_5
    move v2, v13

    .line 216
    goto/16 :goto_1
.end method

.method static synthetic a(Lath;I)I
    .locals 0

    .prologue
    .line 67
    iput p1, p0, Lath;->s:I

    return p1
.end method

.method static synthetic a(Lath;)Lcom/twitter/android/media/selection/c;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lath;->l:Lcom/twitter/android/media/selection/c;

    return-object v0
.end method

.method static synthetic a(Lath;Z)Z
    .locals 0

    .prologue
    .line 67
    iput-boolean p1, p0, Lath;->r:Z

    return p1
.end method

.method static synthetic b(Lath;)Lcom/twitter/android/widget/j;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lath;->m:Lcom/twitter/android/widget/j;

    return-object v0
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 562
    if-eqz p1, :cond_0

    .line 563
    const-string/jumbo v0, "media_attachment"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/selection/MediaAttachment;

    .line 564
    if-eqz v0, :cond_0

    .line 565
    iget-object v1, p0, Lath;->l:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v1, v0, p0}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/media/selection/a;)V

    .line 568
    :cond_0
    return-void
.end method

.method private b(Lcom/twitter/android/media/selection/MediaAttachment;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 651
    iget-object v0, p0, Lath;->q:Lcom/twitter/android/media/selection/MediaAttachment;

    if-eqz v0, :cond_0

    .line 652
    iget-object v0, p0, Lath;->q:Lcom/twitter/android/media/selection/MediaAttachment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/selection/MediaAttachment;->a(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 654
    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/twitter/android/media/selection/MediaAttachment;->b(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 655
    :cond_1
    iput-object v3, p0, Lath;->q:Lcom/twitter/android/media/selection/MediaAttachment;

    .line 656
    iget-object v0, p0, Lath;->o:Lcom/twitter/android/media/widget/InlineComposerMediaLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/InlineComposerMediaLayout;->setVisibility(I)V

    .line 657
    iget-object v0, p0, Lath;->o:Lcom/twitter/android/media/widget/InlineComposerMediaLayout;

    sget-object v1, Lcom/twitter/android/composer/ComposerType;->b:Lcom/twitter/android/composer/ComposerType;

    invoke-virtual {v0, v3, v1}, Lcom/twitter/android/media/widget/InlineComposerMediaLayout;->a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/composer/ComposerType;)Lcom/twitter/android/media/widget/AttachmentMediaView;

    .line 658
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lath;->b(Z)V

    .line 706
    :goto_0
    return-void

    .line 660
    :cond_2
    iput-object p1, p0, Lath;->q:Lcom/twitter/android/media/selection/MediaAttachment;

    .line 661
    iget-object v0, p0, Lath;->o:Lcom/twitter/android/media/widget/InlineComposerMediaLayout;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/InlineComposerMediaLayout;->setVisibility(I)V

    .line 662
    iget-object v0, p0, Lath;->o:Lcom/twitter/android/media/widget/InlineComposerMediaLayout;

    sget-object v1, Lcom/twitter/android/composer/ComposerType;->b:Lcom/twitter/android/composer/ComposerType;

    .line 663
    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/media/widget/InlineComposerMediaLayout;->a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/composer/ComposerType;)Lcom/twitter/android/media/widget/AttachmentMediaView;

    move-result-object v0

    .line 664
    if-eqz v0, :cond_3

    .line 665
    new-instance v1, Lath$2;

    invoke-direct {v1, p0}, Lath$2;-><init>(Lath;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setOnAttachmentActionListener(Lcom/twitter/android/media/widget/AttachmentMediaView$a;)V

    .line 704
    :cond_3
    invoke-direct {p0, v2}, Lath;->b(Z)V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 4

    .prologue
    .line 750
    if-eqz p1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 751
    :goto_0
    iget-object v1, p0, Lath;->p:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 752
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_1

    .line 753
    iget-object v3, p0, Lath;->p:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 754
    invoke-virtual {v3, v0}, Landroid/view/View;->setAlpha(F)V

    .line 755
    invoke-virtual {v3, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 752
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 750
    :cond_0
    const v0, 0x3e99999a    # 0.3f

    goto :goto_0

    .line 757
    :cond_1
    return-void
.end method

.method static synthetic b(Lath;Z)Z
    .locals 0

    .prologue
    .line 67
    iput-boolean p1, p0, Lath;->w:Z

    return p1
.end method

.method static synthetic c(Lath;)Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lath;->r:Z

    return v0
.end method

.method static synthetic d(Lath;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lath;->t()V

    return-void
.end method

.method static synthetic e(Lath;)I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lath;->s:I

    return v0
.end method

.method static synthetic f(Lath;)Latg$a;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lath;->v:Latg$a;

    return-object v0
.end method

.method static synthetic g(Lath;)Lcom/twitter/android/composer/ComposerCountView;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lath;->e:Lcom/twitter/android/composer/ComposerCountView;

    return-object v0
.end method

.method static synthetic h(Lath;)Lcom/twitter/android/composer/TweetBox;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    return-object v0
.end method

.method static synthetic i(Lath;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lath;->f:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic j(Lath;)Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lath;->w:Z

    return v0
.end method

.method static synthetic k(Lath;)Landroid/view/View;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lath;->g:Landroid/view/View;

    return-object v0
.end method

.method static synthetic l(Lath;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lath;->w()V

    return-void
.end method

.method static synthetic m(Lath;)Lcom/twitter/app/common/base/d;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lath;->h:Lcom/twitter/app/common/base/d;

    return-object v0
.end method

.method static synthetic n(Lath;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lath;->x()V

    return-void
.end method

.method static synthetic o(Lath;)Lcom/twitter/android/media/selection/MediaAttachment;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lath;->q:Lcom/twitter/android/media/selection/MediaAttachment;

    return-object v0
.end method

.method static synthetic s()I
    .locals 1

    .prologue
    .line 67
    sget v0, Lath;->b:I

    return v0
.end method

.method private t()V
    .locals 2

    .prologue
    .line 393
    iget-object v0, p0, Lath;->t:Late;

    invoke-virtual {v0}, Late;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    const/4 v0, 0x1

    iput-boolean v0, p0, Lath;->r:Z

    .line 395
    iget-object v0, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->a(Z)V

    .line 399
    :goto_0
    return-void

    .line 397
    :cond_0
    iget-object v0, p0, Lath;->m:Lcom/twitter/android/widget/j;

    invoke-interface {v0}, Lcom/twitter/android/widget/j;->b()V

    goto :goto_0
.end method

.method private u()V
    .locals 4

    .prologue
    .line 413
    invoke-virtual {p0}, Lath;->aN_()Landroid/view/View;

    move-result-object v1

    .line 414
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 416
    const v0, 0x7f1307fb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 417
    iget-object v2, p0, Lath;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020124

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 419
    const v0, 0x7f1304b0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/InlineComposerMediaLayout;

    iput-object v0, p0, Lath;->o:Lcom/twitter/android/media/widget/InlineComposerMediaLayout;

    .line 421
    iget-object v0, p0, Lath;->o:Lcom/twitter/android/media/widget/InlineComposerMediaLayout;

    const v2, 0x7f1304b1

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/InlineComposerMediaLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;

    new-instance v2, Lath$7;

    invoke-direct {v2, p0}, Lath$7;-><init>(Lath;)V

    .line 422
    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->setActionListener(Lcom/twitter/android/media/widget/InlineComposerMediaScrollView$a;)V

    .line 429
    const v0, 0x7f13062e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lath;->p:Landroid/view/ViewGroup;

    .line 430
    iget-object v0, p0, Lath;->p:Landroid/view/ViewGroup;

    const v1, 0x7f13062f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lath$8;

    invoke-direct {v1, p0}, Lath$8;-><init>(Lath;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 454
    invoke-static {}, Lcom/twitter/android/util/j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 455
    iget-object v0, p0, Lath;->p:Landroid/view/ViewGroup;

    const v1, 0x7f1304b5

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 456
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 457
    new-instance v1, Lath$9;

    invoke-direct {v1, p0}, Lath$9;-><init>(Lath;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 468
    :cond_0
    return-void
.end method

.method private v()V
    .locals 3

    .prologue
    .line 571
    invoke-virtual {p0}, Lath;->aF_()Ljava/lang/CharSequence;

    move-result-object v0

    .line 572
    iget-object v1, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/TweetBox;->setImeActionLabel(Ljava/lang/CharSequence;)V

    .line 574
    invoke-virtual {p0}, Lath;->n()Ljava/lang/String;

    move-result-object v0

    .line 575
    invoke-virtual {p0, v0}, Lath;->a(Ljava/lang/String;)V

    .line 577
    iget-object v0, p0, Lath;->u:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    .line 578
    iget-object v0, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    iget-object v1, p0, Lath;->u:Lcom/twitter/model/core/Tweet;

    iget-boolean v2, p0, Lath;->n:Z

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/composer/TweetBox;->a(Lcom/twitter/model/core/Tweet;Z)V

    .line 579
    iget-object v0, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {p0}, Lath;->f()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setExcludedRecipientIds(Ljava/util/Collection;)V

    .line 581
    :cond_0
    iget-boolean v0, p0, Lath;->w:Z

    if-eqz v0, :cond_1

    .line 582
    invoke-direct {p0}, Lath;->w()V

    .line 584
    :cond_1
    return-void
.end method

.method private w()V
    .locals 7

    .prologue
    .line 587
    iget-object v0, p0, Lath;->u:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    .line 588
    iget-object v0, p0, Lath;->i:Lcom/twitter/android/composer/x;

    iget-object v1, p0, Lath;->u:Lcom/twitter/model/core/Tweet;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    iget-boolean v4, p0, Lath;->n:Z

    iget-object v5, p0, Lath;->k:Ljava/util/List;

    new-instance v6, Lath$10;

    invoke-direct {v6, p0}, Lath$10;-><init>(Lath;)V

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/composer/x;->a(Lcom/twitter/model/core/Tweet;JZLjava/util/List;Lbxk$a;)V

    .line 602
    :cond_0
    return-void
.end method

.method private x()V
    .locals 1

    .prologue
    .line 645
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lath;->b(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 646
    iget-object v0, p0, Lath;->m:Lcom/twitter/android/widget/j;

    invoke-interface {v0}, Lcom/twitter/android/widget/j;->c()V

    .line 647
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lath;->a(Z)V

    .line 648
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 4

    .prologue
    .line 403
    iget-object v0, p0, Lath;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0, p1}, Lcom/twitter/android/media/camera/e;->a(Landroid/app/Activity;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p0, Lath;->l:Lcom/twitter/android/media/selection/c;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/twitter/android/media/selection/c;->a(ZI)V

    .line 410
    :goto_0
    return-void

    .line 406
    :cond_0
    iget-object v0, p0, Lath;->h:Lcom/twitter/app/common/base/d;

    sget v1, Lath;->c:I

    iget-object v2, p0, Lath;->a:Landroid/support/v4/app/FragmentActivity;

    const-string/jumbo v3, ":composition::twitter_camera"

    .line 407
    invoke-static {v2, p1, v3}, Lcom/twitter/android/media/camera/e;->a(Landroid/app/Activity;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 406
    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/base/d;->a(ILandroid/content/Intent;)V

    .line 408
    iput p1, p0, Lath;->s:I

    goto :goto_0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 533
    invoke-super {p0, p1}, Laog;->a(Landroid/os/Bundle;)V

    .line 534
    const-string/jumbo v0, "sticky"

    iget-boolean v1, p0, Lath;->w:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 535
    const-string/jumbo v0, "media_attachment"

    iget-object v1, p0, Lath;->q:Lcom/twitter/android/media/selection/MediaAttachment;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 536
    const-string/jumbo v0, "launch_camera_mode"

    iget v1, p0, Lath;->s:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 537
    const-string/jumbo v0, "excluded_users"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lath;->k:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 538
    return-void
.end method

.method public a(Latg$a;)V
    .locals 0

    .prologue
    .line 522
    iput-object p1, p0, Lath;->v:Latg$a;

    .line 523
    return-void
.end method

.method public a(Lcom/twitter/android/media/selection/b;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 726
    invoke-virtual {p1}, Lcom/twitter/android/media/selection/b;->c()Lcom/twitter/android/media/selection/MediaAttachment;

    move-result-object v0

    .line 727
    if-nez v0, :cond_0

    .line 728
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lath;->b(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 747
    :goto_0
    return-void

    .line 731
    :cond_0
    iget v1, v0, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    packed-switch v1, :pswitch_data_0

    .line 743
    iget-object v0, p0, Lath;->a:Landroid/support/v4/app/FragmentActivity;

    iget-object v1, p0, Lath;->a:Landroid/support/v4/app/FragmentActivity;

    const v2, 0x7f0a04b2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 733
    :pswitch_0
    invoke-direct {p0, v0}, Lath;->b(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 734
    iget-object v0, p0, Lath;->m:Lcom/twitter/android/widget/j;

    invoke-interface {v0}, Lcom/twitter/android/widget/j;->c()V

    .line 735
    invoke-virtual {p0, v3}, Lath;->a(Z)V

    goto :goto_0

    .line 739
    :pswitch_1
    invoke-direct {p0, v0}, Lath;->b(Lcom/twitter/android/media/selection/MediaAttachment;)V

    goto :goto_0

    .line 731
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 527
    iput-object p1, p0, Lath;->u:Lcom/twitter/model/core/Tweet;

    .line 528
    invoke-direct {p0}, Lath;->v()V

    .line 529
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 775
    iget-object v0, p0, Lath;->l:Lcom/twitter/android/media/selection/c;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p0}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;Lcom/twitter/android/media/selection/a;)V

    .line 776
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 610
    iget-object v0, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/TweetBox;->setHintText(Ljava/lang/String;)V

    .line 611
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 509
    iget-object v0, p0, Lath;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 510
    iget-object v0, p0, Lath;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 511
    iget-object v0, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    iget-object v1, p0, Lath;->k:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setExcludedRecipientIds(Ljava/util/Collection;)V

    .line 512
    invoke-direct {p0}, Lath;->w()V

    .line 513
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 471
    if-eqz p1, :cond_0

    .line 472
    iget-object v0, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->f()V

    .line 476
    :goto_0
    iget-object v0, p0, Lath;->f:Landroid/widget/Button;

    iget-object v1, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v1}, Lcom/twitter/android/composer/TweetBox;->p()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 477
    return-void

    .line 474
    :cond_0
    iget-object v0, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->g()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/media/selection/MediaAttachment;)Z
    .locals 1

    .prologue
    .line 720
    const/4 v0, 0x1

    return v0
.end method

.method protected aF_()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 641
    iget-object v0, p0, Lath;->a:Landroid/support/v4/app/FragmentActivity;

    const v1, 0x7f0a06b9

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public aK_()Z
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lath;->q:Lcom/twitter/android/media/selection/MediaAttachment;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aL_()V
    .locals 1

    .prologue
    .line 780
    iget-object v0, p0, Lath;->l:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/c;->b()V

    .line 781
    return-void
.end method

.method public aM_()Laog;
    .locals 0

    .prologue
    .line 786
    return-object p0
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 636
    iget-object v0, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/TweetBox;->setPrefillText(Ljava/lang/String;)V

    .line 637
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 710
    iget-object v0, p0, Lath;->m:Lcom/twitter/android/widget/j;

    invoke-interface {v0}, Lcom/twitter/android/widget/j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 711
    iget-object v0, p0, Lath;->m:Lcom/twitter/android/widget/j;

    invoke-interface {v0}, Lcom/twitter/android/widget/j;->c()V

    .line 712
    const/4 v0, 0x1

    .line 714
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 504
    iget-object v0, p0, Lath;->k:Ljava/util/List;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->getText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 762
    iget-object v0, p0, Lath;->q:Lcom/twitter/android/media/selection/MediaAttachment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lath;->q:Lcom/twitter/android/media/selection/MediaAttachment;

    iget v0, v0, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    if-eqz v0, :cond_1

    .line 763
    :cond_0
    const/4 v0, 0x0

    .line 765
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lath;->q:Lcom/twitter/android/media/selection/MediaAttachment;

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/MediaAttachment;->d()Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public i()V
    .locals 1

    .prologue
    .line 517
    iget-object v0, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->j()Z

    .line 518
    return-void
.end method

.method public j()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 542
    iget-object v0, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0, v2}, Lcom/twitter/android/composer/TweetBox;->a(Z)V

    .line 543
    iget-object v0, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1, v4}, Lcom/twitter/android/composer/TweetBox;->a(Ljava/lang/String;[I)V

    .line 544
    iget-object v0, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->clearFocus()V

    .line 546
    invoke-direct {p0}, Lath;->v()V

    .line 548
    iput-boolean v2, p0, Lath;->w:Z

    .line 549
    iget-object v0, p0, Lath;->g:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 550
    iget-object v0, p0, Lath;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 552
    iput-object v4, p0, Lath;->q:Lcom/twitter/android/media/selection/MediaAttachment;

    .line 553
    iget-object v0, p0, Lath;->l:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/c;->a()V

    .line 554
    iget-object v0, p0, Lath;->l:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/c;->e()V

    .line 556
    iget-object v0, p0, Lath;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 558
    invoke-direct {p0}, Lath;->x()V

    .line 559
    return-void
.end method

.method public k()V
    .locals 2

    .prologue
    .line 481
    iget-object v0, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setVisibility(I)V

    .line 482
    return-void
.end method

.method public l()[I
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Lath;->d:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->getSelection()[I

    move-result-object v0

    return-object v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 770
    iget-boolean v0, p0, Lath;->n:Z

    return v0
.end method

.method protected n()Ljava/lang/String;
    .locals 5

    .prologue
    .line 618
    iget-object v0, p0, Lath;->u:Lcom/twitter/model/core/Tweet;

    if-nez v0, :cond_0

    .line 619
    const-string/jumbo v0, ""

    .line 626
    :goto_0
    return-object v0

    .line 622
    :cond_0
    iget-boolean v0, p0, Lath;->n:Z

    if-eqz v0, :cond_1

    .line 623
    iget-object v0, p0, Lath;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0215

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 625
    :cond_1
    iget-object v0, p0, Lath;->u:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v0

    .line 626
    iget-object v1, p0, Lath;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a01f7

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
