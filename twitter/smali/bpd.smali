.class public Lbpd;
.super Lbot;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbot",
        "<",
        "Lcom/twitter/model/dms/ai;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/database/sqlite/SQLiteDatabase;JZZ)V
    .locals 0

    .prologue
    .line 17
    invoke-direct/range {p0 .. p5}, Lbot;-><init>(Landroid/database/sqlite/SQLiteDatabase;JZZ)V

    .line 18
    return-void
.end method


# virtual methods
.method a(Lcom/twitter/model/dms/ai;)V
    .locals 5

    .prologue
    .line 22
    invoke-super {p0, p1}, Lbot;->c(Lcom/twitter/model/dms/c;)V

    .line 24
    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v2, p1, Lcom/twitter/model/dms/ai;->f:Ljava/lang/String;

    aput-object v2, v1, v0

    .line 26
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 27
    const-string/jumbo v3, "avatar_url"

    invoke-virtual {p1}, Lcom/twitter/model/dms/ai;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lbpd;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string/jumbo v3, "conversations"

    sget-object v4, Lawy;->a:Ljava/lang/String;

    invoke-virtual {v0, v3, v2, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 31
    return-void
.end method

.method bridge synthetic a(Lcom/twitter/model/dms/d;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Lcom/twitter/model/dms/ai;

    invoke-virtual {p0, p1}, Lbpd;->a(Lcom/twitter/model/dms/ai;)V

    return-void
.end method

.method synthetic c(Lcom/twitter/model/dms/c;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Lcom/twitter/model/dms/ai;

    invoke-virtual {p0, p1}, Lbpd;->a(Lcom/twitter/model/dms/ai;)V

    return-void
.end method
