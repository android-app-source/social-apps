.class Lauh$a$1;
.super Lcqy;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lauh$a;->a(Lrx/i;)Lrx/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqy",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lrx/i;

.field final synthetic b:Lauh$a;

.field private c:Landroid/database/Cursor;


# direct methods
.method constructor <init>(Lauh$a;Lrx/i;Lrx/i;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lauh$a$1;->b:Lauh$a;

    iput-object p3, p0, Lauh$a$1;->a:Lrx/i;

    invoke-direct {p0, p2}, Lcqy;-><init>(Lrx/i;)V

    return-void
.end method

.method static synthetic a(Lauh$a$1;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0, p1}, Lauh$a$1;->b(Landroid/database/Cursor;)V

    return-void
.end method

.method private b(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 165
    iget-object v0, p0, Lauh$a$1;->b:Lauh$a;

    iget-object v0, v0, Lauh$a;->a:Lauh;

    invoke-static {v0}, Lauh;->c(Lauh;)Ljava/util/Set;

    move-result-object v1

    monitor-enter v1

    .line 166
    :try_start_0
    iget-object v0, p0, Lauh$a$1;->c:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lauh$a$1;->c:Landroid/database/Cursor;

    invoke-static {v0}, Lcqc;->a(Landroid/database/Cursor;)V

    .line 168
    iget-object v0, p0, Lauh$a$1;->b:Lauh$a;

    iget-object v0, v0, Lauh$a;->a:Lauh;

    invoke-static {v0}, Lauh;->c(Lauh;)Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lauh$a$1;->c:Landroid/database/Cursor;

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Lauh$a$1;->c:Landroid/database/Cursor;

    .line 171
    :cond_0
    if-eqz p1, :cond_1

    .line 172
    iget-object v0, p0, Lauh$a$1;->b:Lauh$a;

    iget-object v0, v0, Lauh$a;->a:Lauh;

    invoke-static {v0}, Lauh;->a(Lauh;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 173
    invoke-static {p1}, Lcqc;->a(Landroid/database/Cursor;)V

    .line 178
    :cond_1
    :goto_0
    monitor-exit v1

    .line 179
    return-void

    .line 175
    :cond_2
    iput-object p1, p0, Lauh$a$1;->c:Landroid/database/Cursor;

    goto :goto_0

    .line 178
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lauh$a$1;->b:Lauh$a;

    iget-object v0, v0, Lauh$a;->a:Lauh;

    invoke-static {v0}, Lauh;->a(Lauh;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lauh$a$1;->a:Lrx/i;

    invoke-virtual {v0}, Lrx/i;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    iget-object v0, p0, Lauh$a$1;->a:Lrx/i;

    invoke-virtual {v0, p1}, Lrx/i;->a(Ljava/lang/Object;)V

    .line 148
    :cond_0
    invoke-direct {p0, p1}, Lauh$a$1;->b(Landroid/database/Cursor;)V

    .line 149
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 125
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lauh$a$1;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lauh$a$1;->a:Lrx/i;

    invoke-virtual {v0}, Lrx/i;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    iget-object v0, p0, Lauh$a$1;->a:Lrx/i;

    invoke-virtual {v0, p1}, Lrx/i;->a(Ljava/lang/Throwable;)V

    .line 141
    :cond_0
    return-void
.end method

.method public by_()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lauh$a$1;->a:Lrx/i;

    invoke-virtual {v0}, Lrx/i;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 132
    iget-object v0, p0, Lauh$a$1;->a:Lrx/i;

    invoke-virtual {v0}, Lrx/i;->by_()V

    .line 134
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 153
    invoke-super {p0}, Lcqy;->c()V

    .line 156
    new-instance v0, Lauh$a$1$1;

    invoke-direct {v0, p0}, Lauh$a$1$1;-><init>(Lauh$a$1;)V

    invoke-static {v0}, Lcwy;->a(Lrx/functions/a;)Lrx/j;

    move-result-object v0

    invoke-virtual {p0, v0}, Lauh$a$1;->a(Lrx/j;)V

    .line 162
    return-void
.end method
