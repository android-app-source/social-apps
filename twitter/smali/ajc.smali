.class public Lajc;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lajc$a;
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:Z

.field public final e:J

.field public final f:J

.field public final g:J

.field public final h:J

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Lcom/twitter/library/client/Session;

.field public final l:I

.field public final m:Lcom/twitter/android/revenue/c;

.field public final n:Lcom/twitter/model/timeline/p;

.field public final o:Lbgp;

.field public final p:Lbgp;

.field public final q:Lbgp;

.field public final r:Ljava/lang/String;

.field public final s:Lbwb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbwb",
            "<",
            "Lcom/mopub/nativeads/NativeAd;",
            ">;"
        }
    .end annotation
.end field

.field public final t:Lbgw;


# direct methods
.method private constructor <init>(Lajc$a;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {p1}, Lajc$a;->a(Lajc$a;)I

    move-result v0

    iput v0, p0, Lajc;->a:I

    .line 63
    invoke-static {p1}, Lajc$a;->b(Lajc$a;)I

    move-result v0

    iput v0, p0, Lajc;->b:I

    .line 64
    invoke-static {p1}, Lajc$a;->c(Lajc$a;)I

    move-result v0

    iput v0, p0, Lajc;->c:I

    .line 65
    invoke-static {p1}, Lajc$a;->d(Lajc$a;)Z

    move-result v0

    iput-boolean v0, p0, Lajc;->d:Z

    .line 66
    invoke-static {p1}, Lajc$a;->e(Lajc$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lajc;->e:J

    .line 67
    invoke-static {p1}, Lajc$a;->f(Lajc$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lajc;->f:J

    .line 68
    invoke-static {p1}, Lajc$a;->g(Lajc$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lajc;->g:J

    .line 69
    invoke-static {p1}, Lajc$a;->h(Lajc$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lajc;->h:J

    .line 70
    invoke-static {p1}, Lajc$a;->i(Lajc$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lajc;->i:Ljava/lang/String;

    .line 71
    invoke-static {p1}, Lajc$a;->j(Lajc$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lajc;->j:Ljava/lang/String;

    .line 72
    invoke-static {p1}, Lajc$a;->k(Lajc$a;)Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lajc;->k:Lcom/twitter/library/client/Session;

    .line 73
    invoke-static {p1}, Lajc$a;->l(Lajc$a;)I

    move-result v0

    iput v0, p0, Lajc;->l:I

    .line 74
    invoke-static {p1}, Lajc$a;->m(Lajc$a;)Lcom/twitter/android/revenue/c;

    move-result-object v0

    iput-object v0, p0, Lajc;->m:Lcom/twitter/android/revenue/c;

    .line 75
    invoke-static {p1}, Lajc$a;->n(Lajc$a;)Lcom/twitter/model/timeline/p;

    move-result-object v0

    iput-object v0, p0, Lajc;->n:Lcom/twitter/model/timeline/p;

    .line 76
    invoke-static {p1}, Lajc$a;->o(Lajc$a;)Lbgp;

    move-result-object v0

    sget-object v1, Lbgk;->a:Lbgk;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgp;

    iput-object v0, p0, Lajc;->o:Lbgp;

    .line 77
    invoke-static {p1}, Lajc$a;->p(Lajc$a;)Lbgp;

    move-result-object v0

    sget-object v1, Lbgk;->a:Lbgk;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgp;

    iput-object v0, p0, Lajc;->p:Lbgp;

    .line 79
    invoke-static {p1}, Lajc$a;->q(Lajc$a;)Lbgp;

    move-result-object v0

    sget-object v1, Lbgk;->a:Lbgk;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgp;

    iput-object v0, p0, Lajc;->q:Lbgp;

    .line 80
    invoke-static {p1}, Lajc$a;->r(Lajc$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lajc;->r:Ljava/lang/String;

    .line 81
    invoke-static {p1}, Lajc$a;->s(Lajc$a;)Lbwb;

    move-result-object v0

    iput-object v0, p0, Lajc;->s:Lbwb;

    .line 82
    invoke-static {p1}, Lajc$a;->t(Lajc$a;)Lbgw;

    move-result-object v0

    iput-object v0, p0, Lajc;->t:Lbgw;

    .line 83
    return-void
.end method

.method synthetic constructor <init>(Lajc$a;Lajc$1;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lajc;-><init>(Lajc$a;)V

    return-void
.end method
