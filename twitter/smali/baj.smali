.class public Lbaj;
.super Lcom/twitter/library/api/r;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/api/r",
        "<",
        "Lcom/twitter/library/api/y;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lbaj;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/r;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 30
    iput-object p3, p0, Lbaj;->b:Ljava/lang/String;

    .line 31
    const-string/jumbo v0, "Client config fetch is never triggered by a user action."

    invoke-virtual {p0, v0}, Lbaj;->l(Ljava/lang/String;)Lcom/twitter/library/service/s;

    .line 32
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-virtual {p0}, Lbaj;->M()Lcom/twitter/library/service/v;

    move-result-object v1

    .line 38
    invoke-virtual {p0}, Lbaj;->J()Lcom/twitter/library/service/d$a;

    move-result-object v2

    const-string/jumbo v3, "mobile.twitter.com"

    .line 39
    invoke-virtual {v2, v3}, Lcom/twitter/library/service/d$a;->b(Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v2

    .line 40
    invoke-virtual {v2, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/network/t;

    if-eqz v1, :cond_0

    iget-object v0, v1, Lcom/twitter/library/service/v;->d:Lcom/twitter/model/account/OAuthToken;

    :cond_0
    invoke-direct {v3, v0}, Lcom/twitter/library/network/t;-><init>(Lcom/twitter/model/account/OAuthToken;)V

    .line 41
    invoke-virtual {v2, v3}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "i"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "config"

    aput-object v3, v1, v2

    .line 42
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->b([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "client"

    const-string/jumbo v2, "android"

    .line 43
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 44
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 45
    iget-object v1, p0, Lbaj;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 46
    const-string/jumbo v1, "carrier"

    iget-object v2, p0, Lbaj;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 49
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V
    .locals 3

    .prologue
    .line 62
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->m()Lcom/twitter/network/l;

    move-result-object v0

    iget v0, v0, Lcom/twitter/network/l;->a:I

    .line 63
    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    .line 64
    invoke-virtual {p3}, Lcom/twitter/library/api/y;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/ClientConfiguration;

    .line 65
    iget-object v1, p2, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "extra_settings"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 67
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 20
    check-cast p3, Lcom/twitter/library/api/y;

    invoke-virtual {p0, p1, p2, p3}, Lbaj;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/api/y;
    .locals 1

    .prologue
    .line 54
    const/16 v0, 0x12

    .line 55
    invoke-static {v0}, Lcom/twitter/library/api/y;->a(I)Lcom/twitter/library/api/y;

    move-result-object v0

    .line 54
    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lbaj;->b()Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method
