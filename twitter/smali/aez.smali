.class public Laez;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/ui/chat/y;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laez$a;,
        Laez$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/client/Session;

.field private final b:Ldae;

.field private final c:Lcom/twitter/android/periscope/q;

.field private d:Laba;

.field private e:Laez$a;

.field private f:Lcom/twitter/android/periscope/profile/b;


# direct methods
.method public constructor <init>(Lcom/twitter/library/client/Session;Ldae;Lcom/twitter/android/periscope/q;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Laez;->a:Lcom/twitter/library/client/Session;

    .line 51
    iput-object p2, p0, Laez;->b:Ldae;

    .line 52
    iput-object p3, p0, Laez;->c:Lcom/twitter/android/periscope/q;

    .line 53
    return-void
.end method

.method static synthetic a(Laez;)Lcom/twitter/android/periscope/profile/b;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Laez;->f:Lcom/twitter/android/periscope/profile/b;

    return-object v0
.end method

.method static synthetic b(Laez;)Lcom/twitter/android/periscope/q;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Laez;->c:Lcom/twitter/android/periscope/q;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)Ltv/periscope/android/ui/chat/j;
    .locals 8

    .prologue
    .line 66
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 67
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 68
    const v2, 0x7f04028e

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 69
    new-instance v3, Laez$b;

    iget-object v4, p0, Laez;->b:Ldae;

    invoke-direct {v3, v2, v4}, Laez$b;-><init>(Landroid/view/View;Ldae;)V

    .line 71
    iget-object v2, p0, Laez;->d:Laba;

    if-eqz v2, :cond_1

    .line 72
    iget-object v2, p0, Laez;->d:Laba;

    invoke-virtual {v2}, Laba;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    .line 73
    new-instance v4, Ljava/util/ArrayList;

    const/4 v5, 0x2

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 74
    iget-object v5, v3, Laez$b;->d:Landroid/view/ViewGroup;

    const v6, 0x7f0403b4

    .line 75
    invoke-static {v1, v5, v6}, Laay;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Laat;

    move-result-object v5

    .line 77
    invoke-static {v0, v5}, Laax;->a(Landroid/content/Context;Laat;)Laax;

    move-result-object v5

    .line 78
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    iget-object v6, v3, Laez$b;->d:Landroid/view/ViewGroup;

    const v7, 0x7f0403b5

    .line 81
    invoke-static {v1, v6, v7}, Lafg;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Laat;

    move-result-object v1

    .line 83
    iget-object v6, p0, Laez;->a:Lcom/twitter/library/client/Session;

    .line 85
    invoke-static {v0, v1, v2, v6}, Laff;->a(Landroid/content/Context;Laat;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/library/client/Session;)Laff;

    move-result-object v0

    .line 86
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    new-instance v1, Laez$1;

    invoke-direct {v1, p0, v0, v2}, Laez$1;-><init>(Laez;Laff;Lcom/twitter/model/core/TwitterUser;)V

    invoke-virtual {v5, v1}, Laax;->a(Laax$a;)V

    .line 107
    new-instance v1, Laez$2;

    invoke-direct {v1, p0}, Laez$2;-><init>(Laez;)V

    invoke-virtual {v0, v1}, Laff;->a(Laff$a;)V

    .line 118
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laas;

    .line 119
    iget-object v2, v3, Laez$b;->d:Landroid/view/ViewGroup;

    invoke-interface {v0}, Laas;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 122
    :cond_0
    new-instance v0, Laaz;

    iget-object v1, p0, Laez;->a:Lcom/twitter/library/client/Session;

    invoke-direct {v0, v4, v1}, Laaz;-><init>(Ljava/util/List;Lcom/twitter/library/client/Session;)V

    invoke-static {v3, v0}, Laez$b;->a(Laez$b;Laap;)Laap;

    .line 125
    :cond_1
    return-object v3
.end method

.method public a()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Laez;->e:Laez$a;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Laez;->e:Laez$a;

    invoke-virtual {v0}, Laez$a;->a()V

    .line 143
    :cond_0
    return-void
.end method

.method public a(Laba;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Laez;->d:Laba;

    .line 57
    return-void
.end method

.method public a(Lcom/twitter/android/periscope/profile/b;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Laez;->f:Lcom/twitter/android/periscope/profile/b;

    .line 61
    return-void
.end method

.method public a(Ltv/periscope/android/ui/chat/j;Ltv/periscope/android/ui/chat/h;)V
    .locals 3

    .prologue
    .line 130
    check-cast p1, Laez$b;

    .line 132
    invoke-virtual {p1, p2}, Laez$b;->a(Ltv/periscope/android/ui/chat/h;)V

    .line 133
    invoke-static {p1}, Laez$b;->a(Laez$b;)Laap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laez;->d:Laba;

    if-eqz v0, :cond_0

    .line 134
    invoke-static {p1}, Laez$b;->a(Laez$b;)Laap;

    move-result-object v0

    iget-object v1, p0, Laez;->d:Laba;

    invoke-interface {v0, v1}, Laap;->a(Laaq;)V

    .line 135
    new-instance v0, Laez$a;

    invoke-static {p1}, Laez$b;->a(Laez$b;)Laap;

    move-result-object v1

    iget-object v2, p0, Laez;->d:Laba;

    invoke-direct {v0, v1, v2}, Laez$a;-><init>(Laap;Laba;)V

    iput-object v0, p0, Laez;->e:Laez$a;

    .line 137
    :cond_0
    return-void
.end method
