.class Lcdy$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcdy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcdy;",
        "Lcdy$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 161
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcdy$1;)V
    .locals 0

    .prologue
    .line 161
    invoke-direct {p0}, Lcdy$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcdy$a;
    .locals 1

    .prologue
    .line 165
    new-instance v0, Lcdy$a;

    invoke-direct {v0}, Lcdy$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcdy$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 173
    sget-object v0, Lcdu;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    invoke-virtual {p2, v0}, Lcdy$a;->a(Lcdu;)Lcdy$a;

    move-result-object v1

    sget-object v0, Lcdz;->a:Lcom/twitter/util/serialization/i;

    .line 174
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/i;->b(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdz;

    invoke-virtual {v1, v0}, Lcdy$a;->a(Lcdz;)Lcdy$a;

    .line 175
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 161
    check-cast p2, Lcdy$a;

    invoke-virtual {p0, p1, p2, p3}, Lcdy$b;->a(Lcom/twitter/util/serialization/n;Lcdy$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcdy;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 180
    iget-object v0, p2, Lcdy;->b:Lcdu;

    sget-object v1, Lcdu;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcdy;->c:Lcdz;

    sget-object v2, Lcdz;->a:Lcom/twitter/util/serialization/i;

    .line 181
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 182
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    check-cast p2, Lcdy;

    invoke-virtual {p0, p1, p2}, Lcdy$b;->a(Lcom/twitter/util/serialization/o;Lcdy;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0}, Lcdy$b;->a()Lcdy$a;

    move-result-object v0

    return-object v0
.end method
