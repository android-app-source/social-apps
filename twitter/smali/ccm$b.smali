.class final Lccm$b;
.super Lccs$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lccm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lccs$b",
        "<",
        "Lccm;",
        "Lccm$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Lccs$b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lccm$1;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Lccm$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lccm$a;
    .locals 1

    .prologue
    .line 100
    new-instance v0, Lccm$a;

    invoke-direct {v0}, Lccm$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lccm$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 83
    invoke-super {p0, p1, p2, p3}, Lccs$b;->a(Lcom/twitter/util/serialization/n;Lccs$a;I)V

    .line 84
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lccm$a;->a(Ljava/lang/String;)Lccm$a;

    move-result-object v0

    .line 85
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lccm$a;->a(Z)Lccm$a;

    .line 86
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lccs$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 76
    check-cast p2, Lccm$a;

    invoke-virtual {p0, p1, p2, p3}, Lccm$b;->a(Lcom/twitter/util/serialization/n;Lccm$a;I)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 76
    check-cast p2, Lccm$a;

    invoke-virtual {p0, p1, p2, p3}, Lccm$b;->a(Lcom/twitter/util/serialization/n;Lccm$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lccm;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    invoke-super {p0, p1, p2}, Lccs$b;->a(Lcom/twitter/util/serialization/o;Lccs;)V

    .line 93
    iget-object v0, p2, Lccm;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lccm;->c:Z

    .line 94
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    .line 95
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lccs;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    check-cast p2, Lccm;

    invoke-virtual {p0, p1, p2}, Lccm$b;->a(Lcom/twitter/util/serialization/o;Lccm;)V

    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    check-cast p2, Lccm;

    invoke-virtual {p0, p1, p2}, Lccm$b;->a(Lcom/twitter/util/serialization/o;Lccm;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p0}, Lccm$b;->a()Lccm$a;

    move-result-object v0

    return-object v0
.end method
