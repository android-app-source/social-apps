.class Lcnn$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcnn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcnn;

.field private b:I

.field private c:I


# direct methods
.method constructor <init>(Lcnn;)V
    .locals 0

    .prologue
    .line 194
    iput-object p1, p0, Lcnn$a;->a:Lcnn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 7

    .prologue
    .line 226
    iget v0, p0, Lcnn$a;->b:I

    if-ne p2, v0, :cond_0

    iget v0, p0, Lcnn$a;->c:I

    if-eq p3, v0, :cond_1

    :cond_0
    const/4 v5, 0x1

    .line 229
    :goto_0
    iget-object v0, p0, Lcnn$a;->a:Lcnn;

    invoke-static {v0}, Lcnn;->b(Lcnn;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcno$c;

    .line 230
    iget-object v1, p0, Lcnn$a;->a:Lcnn;

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-interface/range {v0 .. v5}, Lcno$c;->a(Lcno;IIIZ)V

    goto :goto_1

    .line 226
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 234
    :cond_2
    if-eqz p3, :cond_3

    if-nez v5, :cond_4

    .line 254
    :cond_3
    iput p2, p0, Lcnn$a;->b:I

    .line 255
    iput p3, p0, Lcnn$a;->c:I

    .line 257
    :goto_2
    return-void

    .line 238
    :cond_4
    if-nez p2, :cond_6

    .line 239
    :try_start_0
    iget-object v0, p0, Lcnn$a;->a:Lcnn;

    invoke-virtual {v0}, Lcnn;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 254
    :cond_5
    iput p2, p0, Lcnn$a;->b:I

    .line 255
    iput p3, p0, Lcnn$a;->c:I

    goto :goto_2

    .line 241
    :cond_6
    :try_start_1
    iget v0, p0, Lcnn$a;->b:I

    if-nez v0, :cond_7

    .line 242
    iget-object v0, p0, Lcnn$a;->a:Lcnn;

    invoke-static {v0}, Lcnn;->b(Lcnn;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcno$c;

    .line 243
    iget-object v2, p0, Lcnn$a;->a:Lcnn;

    invoke-interface {v0, v2}, Lcno$c;->b(Lcno;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 254
    :catchall_0
    move-exception v0

    iput p2, p0, Lcnn$a;->b:I

    .line 255
    iput p3, p0, Lcnn$a;->c:I

    throw v0

    .line 247
    :cond_7
    add-int v0, p2, p3

    if-lt v0, p4, :cond_5

    .line 248
    :try_start_2
    iget-object v0, p0, Lcnn$a;->a:Lcnn;

    invoke-static {v0}, Lcnn;->b(Lcnn;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcno$c;

    .line 249
    iget-object v2, p0, Lcnn$a;->a:Lcnn;

    invoke-interface {v0, v2}, Lcno$c;->c(Lcno;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 4

    .prologue
    .line 201
    packed-switch p2, :pswitch_data_0

    .line 211
    const/4 v0, 0x0

    move v1, v0

    .line 216
    :goto_0
    iget-object v0, p0, Lcnn$a;->a:Lcnn;

    invoke-static {v0}, Lcnn;->b(Lcnn;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcno$c;

    .line 217
    iget-object v3, p0, Lcnn$a;->a:Lcnn;

    invoke-interface {v0, v3, v1}, Lcno$c;->a(Lcno;I)V

    goto :goto_1

    .line 203
    :pswitch_0
    const/4 v0, 0x1

    move v1, v0

    .line 204
    goto :goto_0

    .line 207
    :pswitch_1
    const/4 v0, 0x2

    move v1, v0

    .line 208
    goto :goto_0

    .line 219
    :cond_0
    return-void

    .line 201
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
