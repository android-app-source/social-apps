.class public Lbrm;
.super Laoq;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbrm$a;,
        Lbrm$c;,
        Lbrm$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laoq",
        "<",
        "Ljava/util/List",
        "<",
        "Lcdu;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:Lcom/twitter/library/provider/t;

.field private final e:Lcom/twitter/library/client/p;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbrn;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Laoq;-><init>()V

    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbrm;->a:Landroid/content/Context;

    .line 63
    iget v0, p2, Lbrn;->a:I

    iput v0, p0, Lbrm;->c:I

    .line 64
    new-instance v0, Ljava/util/LinkedHashSet;

    iget-object v1, p2, Lbrn;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbrm;->b:Ljava/util/List;

    .line 65
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iput-object v0, p0, Lbrm;->e:Lcom/twitter/library/client/p;

    .line 66
    iget-wide v0, p2, Lbrn;->c:J

    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    iput-object v0, p0, Lbrm;->d:Lcom/twitter/library/provider/t;

    .line 67
    invoke-direct {p0}, Lbrm;->a()V

    .line 68
    return-void
.end method

.method static synthetic a(Lbrm;)Lcom/twitter/library/provider/t;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lbrm;->d:Lcom/twitter/library/provider/t;

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 71
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    iget-object v1, p0, Lbrm;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 72
    new-instance v1, Lbrm$b;

    iget-object v2, p0, Lbrm;->d:Lcom/twitter/library/provider/t;

    iget-object v3, p0, Lbrm;->b:Ljava/util/List;

    invoke-direct {v1, v2, v3}, Lbrm$b;-><init>(Lcom/twitter/library/provider/t;Ljava/util/List;)V

    .line 73
    new-instance v2, Lbrm$1;

    invoke-direct {v2, p0, v0}, Lbrm$1;-><init>(Lbrm;Lcom/twitter/util/collection/h;)V

    invoke-virtual {v1, v2}, Lbrm$b;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    .line 96
    iget-object v0, p0, Lbrm;->e:Lcom/twitter/library/client/p;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 97
    return-void
.end method

.method static synthetic a(Lbrm;Ljava/util/List;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lbrm;->a(Ljava/util/List;Ljava/util/Map;)V

    return-void
.end method

.method static synthetic a(Lbrm;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lbrm;->a(Ljava/util/Map;)V

    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcdu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lbrm;->e:Lcom/twitter/library/client/p;

    new-instance v1, Lbro;

    iget-object v2, p0, Lbrm;->a:Landroid/content/Context;

    iget v3, p0, Lbrm;->c:I

    invoke-direct {v1, v2, v3, p1}, Lbro;-><init>(Landroid/content/Context;ILjava/util/List;)V

    new-instance v2, Lbrm$2;

    invoke-direct {v2, p0, p2}, Lbrm$2;-><init>(Lbrm;Ljava/util/Map;)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 117
    return-void
.end method

.method private a(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcdu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 137
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 138
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->min(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    iget-wide v0, v0, Lcdu;->n:J

    .line 139
    :goto_0
    invoke-direct {p0}, Lbrm;->c()J

    move-result-wide v2

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    const/4 v2, 0x1

    .line 140
    :goto_1
    if-nez v2, :cond_2

    .line 141
    iget-object v2, p0, Lbrm;->e:Lcom/twitter/library/client/p;

    new-instance v3, Lbrl;

    iget-object v4, p0, Lbrm;->a:Landroid/content/Context;

    iget v5, p0, Lbrm;->c:I

    invoke-direct {v3, v4, v5, v0, v1}, Lbrl;-><init>(Landroid/content/Context;IJ)V

    new-instance v0, Lbrm$3;

    invoke-direct {v0, p0, p1}, Lbrm$3;-><init>(Lbrm;Ljava/util/Map;)V

    invoke-virtual {v2, v3, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 165
    :goto_2
    return-void

    .line 138
    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0

    .line 139
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 163
    :cond_2
    invoke-direct {p0, p1}, Lbrm;->b(Ljava/util/Map;)V

    goto :goto_2
.end method

.method static synthetic b(Lbrm;)Lcom/twitter/library/client/p;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lbrm;->e:Lcom/twitter/library/client/p;

    return-object v0
.end method

.method static synthetic b(Lbrm;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lbrm;->b(Ljava/util/Map;)V

    return-void
.end method

.method private b(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcdu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 169
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 170
    iget-object v0, p0, Lbrm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 171
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 173
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbrm;->a(Ljava/lang/Object;)V

    .line 174
    return-void
.end method

.method private c()J
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 121
    const-string/jumbo v1, "photo_stickers_invalid_stickers_last_modified"

    invoke-static {v1}, Lcoj;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 124
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 126
    :try_start_0
    sget-object v2, Lced;->a:Ljava/text/DateFormat;

    invoke-virtual {v2, v1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 131
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    :goto_1
    return-wide v0

    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_1

    .line 127
    :catch_0
    move-exception v1

    goto :goto_0
.end method
