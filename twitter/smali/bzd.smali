.class public Lbzd;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Lbzc;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 23
    iget-object v0, p0, Lbzd;->a:Lbzc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbzd;->a:Lbzc;

    invoke-virtual {v0}, Lbzc;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 24
    :cond_0
    new-instance v0, Lbzc;

    invoke-direct {v0}, Lbzc;-><init>()V

    iput-object v0, p0, Lbzd;->a:Lbzc;

    .line 26
    iget-object v0, p0, Lbzd;->a:Lbzc;

    const-string/jumbo v1, "navigate"

    invoke-virtual {v0, v1}, Lbzc;->c(Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lbzd;->a:Lbzc;

    iget-object v1, p0, Lbzd;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbzc;->a(Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lbzd;->a:Lbzc;

    invoke-static {}, Lcox;->ax()Lcox;

    move-result-object v1

    invoke-virtual {v1}, Lcox;->ar()Lcnz;

    move-result-object v1

    invoke-virtual {v1}, Lcnz;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lbzc;->b(J)V

    .line 29
    iget-object v0, p0, Lbzd;->a:Lbzc;

    invoke-virtual {v0}, Lbzc;->i()V

    .line 31
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/media/request/ResourceResponse$ResourceSource;Landroid/graphics/Rect;Ljava/lang/Long;)V
    .locals 4

    .prologue
    .line 54
    iget-object v0, p0, Lbzd;->a:Lbzc;

    if-eqz v0, :cond_2

    .line 55
    iget-object v0, p0, Lbzd;->a:Lbzc;

    invoke-virtual {p1}, Lcom/twitter/media/request/ResourceResponse$ResourceSource;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbzc;->b(Ljava/lang/String;)V

    .line 57
    if-eqz p2, :cond_0

    .line 58
    iget-object v0, p0, Lbzd;->a:Lbzc;

    const-string/jumbo v1, "width"

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbzc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lbzd;->a:Lbzc;

    const-string/jumbo v1, "height"

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbzc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_0
    if-eqz p3, :cond_1

    .line 63
    iget-object v0, p0, Lbzd;->a:Lbzc;

    const-string/jumbo v1, "bytes"

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbzc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :cond_1
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v0

    invoke-virtual {v0}, Lcrr;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 67
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v0

    invoke-virtual {v0}, Lcrr;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 68
    iget-object v0, p0, Lbzd;->a:Lbzc;

    const-string/jumbo v1, "network_type"

    const-string/jumbo v2, "wifi"

    invoke-virtual {v0, v1, v2}, Lbzc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :cond_2
    :goto_0
    return-void

    .line 70
    :cond_3
    iget-object v0, p0, Lbzd;->a:Lbzc;

    const-string/jumbo v1, "network_type"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "cellular_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v3

    invoke-virtual {v3}, Lcrr;->b()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbzc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lbzd;->a:Lbzc;

    const-string/jumbo v1, "network_strength"

    .line 72
    invoke-static {}, Lcrq;->a()Lcrq;

    move-result-object v2

    invoke-virtual {v2}, Lcrq;->c()Ljava/lang/String;

    move-result-object v2

    .line 71
    invoke-virtual {v0, v1, v2}, Lbzc;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 75
    :cond_4
    iget-object v0, p0, Lbzd;->a:Lbzc;

    const-string/jumbo v1, "network_type"

    const-string/jumbo v2, "disconnected"

    invoke-virtual {v0, v1, v2}, Lbzc;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lbzd;->a:Lbzc;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lbzd;->a:Lbzc;

    invoke-virtual {v0, p1}, Lbzc;->a(Ljava/lang/String;)V

    .line 84
    :cond_0
    iput-object p1, p0, Lbzd;->b:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lbzd;->a:Lbzc;

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lbzd;->a:Lbzc;

    invoke-virtual {v0}, Lbzc;->j()V

    .line 40
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lbzd;->a:Lbzc;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lbzd;->a:Lbzc;

    invoke-virtual {v0, p1}, Lbzc;->c(Ljava/lang/String;)V

    .line 91
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lbzd;->a:Lbzc;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lbzd;->a:Lbzc;

    invoke-virtual {v0}, Lbzc;->g()V

    .line 46
    :cond_0
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lbzd;->a:Lbzc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbzd;->a:Lbzc;

    invoke-virtual {v0}, Lbzc;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
