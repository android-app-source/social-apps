.class public Lavl;
.super Lcbr;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbr",
        "<",
        "Lawu$a;",
        "Lcom/twitter/model/dms/c;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcbr;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lawu$a;)Lcom/twitter/model/dms/c;
    .locals 5

    .prologue
    .line 31
    invoke-interface {p1}, Lawu$a;->f()I

    move-result v1

    .line 32
    invoke-interface {p1}, Lawu$a;->g()[B

    move-result-object v2

    .line 35
    packed-switch v1, :pswitch_data_0

    .line 101
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unsupported conversation entry type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :pswitch_1
    new-instance v0, Lcom/twitter/model/dms/j$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/j$a;-><init>()V

    sget-object v3, Lcom/twitter/model/dms/c$c;->a:Lcom/twitter/util/serialization/l;

    .line 38
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/j$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    .line 105
    :goto_0
    invoke-virtual {v0}, Lcom/twitter/model/dms/c$b;->R_()Z

    move-result v3

    if-nez v3, :cond_1

    .line 106
    if-eqz v2, :cond_0

    const/4 v0, -0x1

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Lcom/twitter/util/serialization/k;->a([BIZ)Ljava/lang/String;

    move-result-object v0

    .line 107
    :goto_1
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Metadata of entry with type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " failed to be deserialized: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 42
    :pswitch_2
    new-instance v0, Lcom/twitter/model/dms/ak$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/ak$a;-><init>()V

    sget-object v3, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 43
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/ak$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    goto :goto_0

    .line 47
    :pswitch_3
    new-instance v0, Lcom/twitter/model/dms/ai$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/ai$a;-><init>()V

    sget-object v3, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 48
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/ai$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    goto :goto_0

    .line 52
    :pswitch_4
    new-instance v0, Lcom/twitter/model/dms/z$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/z$a;-><init>()V

    sget-object v3, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    .line 54
    invoke-static {v3}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v3

    .line 53
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/z$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    goto :goto_0

    .line 58
    :pswitch_5
    new-instance v0, Lcom/twitter/model/dms/ag$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/ag$a;-><init>()V

    sget-object v3, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 60
    invoke-static {v3}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v3

    .line 59
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/ag$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    goto :goto_0

    .line 64
    :pswitch_6
    new-instance v0, Lcom/twitter/model/dms/ae$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/ae$a;-><init>()V

    sget-object v3, Lcom/twitter/model/dms/ae$b;->a:Lcom/twitter/util/serialization/l;

    .line 65
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/ae$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    goto/16 :goto_0

    .line 69
    :pswitch_7
    new-instance v0, Lcom/twitter/model/dms/af$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/af$a;-><init>()V

    sget-object v3, Lcom/twitter/model/dms/Participant;->a:Lcom/twitter/util/serialization/l;

    .line 71
    invoke-static {v3}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v3

    .line 70
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/af$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    goto/16 :goto_0

    .line 75
    :pswitch_8
    new-instance v0, Lcom/twitter/model/dms/s$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/s$a;-><init>()V

    const/4 v3, 0x1

    .line 76
    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/s$a;->b(Z)Lcom/twitter/model/dms/s$a;

    move-result-object v0

    sget-object v3, Lcom/twitter/model/dms/a$c;->a:Lcom/twitter/util/serialization/l;

    .line 77
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/s$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    goto/16 :goto_0

    .line 81
    :pswitch_9
    new-instance v0, Lcom/twitter/model/dms/s$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/s$a;-><init>()V

    sget-object v3, Lcom/twitter/model/dms/a$c;->a:Lcom/twitter/util/serialization/l;

    .line 82
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/s$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    goto/16 :goto_0

    .line 86
    :pswitch_a
    new-instance v0, Lcom/twitter/model/dms/r$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/r$a;-><init>()V

    sget-object v3, Lcom/twitter/model/dms/r$c;->a:Lcom/twitter/util/serialization/l;

    .line 87
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/r$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    goto/16 :goto_0

    .line 91
    :pswitch_b
    new-instance v0, Lcom/twitter/model/dms/h$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/h$a;-><init>()V

    sget-object v3, Lcom/twitter/model/dms/h$c;->a:Lcom/twitter/model/dms/h$c$b;

    .line 92
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    .line 91
    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/h$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    goto/16 :goto_0

    .line 96
    :pswitch_c
    new-instance v0, Lcom/twitter/model/dms/g$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/g$a;-><init>()V

    sget-object v3, Lcom/twitter/model/dms/g$c;->a:Lcom/twitter/model/dms/g$c$b;

    .line 97
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    .line 96
    invoke-virtual {v0, v3}, Lcom/twitter/model/dms/g$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    goto/16 :goto_0

    .line 106
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 112
    :cond_1
    invoke-interface {p1}, Lawu$a;->d()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/dms/c$b;->a(J)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    .line 113
    invoke-interface {p1}, Lawu$a;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/dms/c$b;->b(J)Lcom/twitter/model/dms/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/c$b;

    .line 114
    invoke-interface {p1}, Lawu$a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/c$b;->a(Ljava/lang/String;)Lcom/twitter/model/dms/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/c$b;

    .line 115
    invoke-interface {p1}, Lawu$a;->e()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/dms/c$b;->c(J)Lcom/twitter/model/dms/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/c$b;

    .line 116
    invoke-virtual {v0}, Lcom/twitter/model/dms/c$b;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/c;

    .line 111
    return-object v0

    .line 35
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_3
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    check-cast p1, Lawu$a;

    invoke-virtual {p0, p1}, Lavl;->a(Lawu$a;)Lcom/twitter/model/dms/c;

    move-result-object v0

    return-object v0
.end method
