.class public Lbzu;
.super Lcom/twitter/metrics/m;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/metrics/o;

.field private b:Lcom/twitter/metrics/o;

.field private c:Lcom/twitter/metrics/o;

.field private d:Lcom/twitter/metrics/o;

.field private e:Lcom/twitter/metrics/o;

.field private w:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/metrics/o;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;ZI)V
    .locals 2

    .prologue
    .line 47
    invoke-direct/range {p0 .. p7}, Lcom/twitter/metrics/m;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;ZI)V

    .line 48
    const-string/jumbo v0, "MemMetric"

    invoke-virtual {p0, v0}, Lbzu;->e(Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lbzu;->w:Ljava/util/List;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Lcom/twitter/metrics/o;

    const-string/jumbo v1, "dalvik_total"

    invoke-direct {v0, v1}, Lcom/twitter/metrics/o;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbzu;->a:Lcom/twitter/metrics/o;

    .line 51
    new-instance v0, Lcom/twitter/metrics/o;

    const-string/jumbo v1, "dalvik_alloc"

    invoke-direct {v0, v1}, Lcom/twitter/metrics/o;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbzu;->b:Lcom/twitter/metrics/o;

    .line 52
    new-instance v0, Lcom/twitter/metrics/o;

    const-string/jumbo v1, "dalvik_ratio"

    invoke-direct {v0, v1}, Lcom/twitter/metrics/o;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbzu;->c:Lcom/twitter/metrics/o;

    .line 53
    new-instance v0, Lcom/twitter/metrics/o;

    const-string/jumbo v1, "native_total"

    invoke-direct {v0, v1}, Lcom/twitter/metrics/o;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbzu;->d:Lcom/twitter/metrics/o;

    .line 54
    new-instance v0, Lcom/twitter/metrics/o;

    const-string/jumbo v1, "native_alloc"

    invoke-direct {v0, v1}, Lcom/twitter/metrics/o;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbzu;->e:Lcom/twitter/metrics/o;

    .line 55
    invoke-direct {p0}, Lbzu;->D()V

    .line 57
    :cond_0
    return-void
.end method

.method private D()V
    .locals 2

    .prologue
    .line 113
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lbzu;->w:Ljava/util/List;

    .line 114
    iget-object v0, p0, Lbzu;->w:Ljava/util/List;

    iget-object v1, p0, Lbzu;->a:Lcom/twitter/metrics/o;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    iget-object v0, p0, Lbzu;->w:Ljava/util/List;

    iget-object v1, p0, Lbzu;->b:Lcom/twitter/metrics/o;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    iget-object v0, p0, Lbzu;->w:Ljava/util/List;

    iget-object v1, p0, Lbzu;->c:Lcom/twitter/metrics/o;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    iget-object v0, p0, Lbzu;->w:Ljava/util/List;

    iget-object v1, p0, Lbzu;->d:Lcom/twitter/metrics/o;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    iget-object v0, p0, Lbzu;->w:Ljava/util/List;

    iget-object v1, p0, Lbzu;->e:Lcom/twitter/metrics/o;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    return-void
.end method

.method public static a(Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/j;)Lbzu;
    .locals 8

    .prologue
    .line 36
    const-string/jumbo v0, "MemMetric"

    const-string/jumbo v1, "app:mem"

    invoke-static {v0, v1}, Lbzu;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/metrics/j;->a(Ljava/lang/String;)Lcom/twitter/metrics/f;

    move-result-object v0

    .line 37
    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lbzu;

    invoke-virtual {p1}, Lcom/twitter/metrics/j;->f()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "app:mem"

    const-string/jumbo v3, "MemMetric"

    const-string/jumbo v4, "app:mem"

    .line 39
    invoke-static {v3, v4}, Lbzu;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x1

    const/4 v7, 0x3

    move-object v3, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v7}, Lbzu;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;ZI)V

    .line 38
    invoke-virtual {p1, v0}, Lcom/twitter/metrics/j;->d(Lcom/twitter/metrics/f;)Lcom/twitter/metrics/f;

    move-result-object v0

    .line 42
    :cond_0
    check-cast v0, Lbzu;

    return-object v0
.end method


# virtual methods
.method public C()Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    .line 124
    new-instance v1, Ljava/util/HashMap;

    iget-object v0, p0, Lbzu;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 125
    iget-object v0, p0, Lbzu;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/metrics/o;

    .line 126
    const/16 v3, 0x14

    invoke-virtual {v0, v3}, Lcom/twitter/metrics/o;->a(I)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    goto :goto_0

    .line 128
    :cond_0
    return-object v1
.end method

.method protected a(Landroid/content/SharedPreferences$Editor;)V
    .locals 2

    .prologue
    .line 87
    invoke-super {p0, p1}, Lcom/twitter/metrics/m;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 88
    iget-object v0, p0, Lbzu;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/metrics/o;

    .line 89
    invoke-virtual {v0, p1}, Lcom/twitter/metrics/o;->a(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0

    .line 91
    :cond_0
    return-void
.end method

.method protected a(Landroid/content/SharedPreferences;)V
    .locals 2

    .prologue
    .line 103
    invoke-super {p0, p1}, Lcom/twitter/metrics/m;->a(Landroid/content/SharedPreferences;)V

    .line 104
    new-instance v0, Lcom/twitter/metrics/o;

    const-string/jumbo v1, "dalvik_total"

    invoke-direct {v0, v1, p1}, Lcom/twitter/metrics/o;-><init>(Ljava/lang/String;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lbzu;->a:Lcom/twitter/metrics/o;

    .line 105
    new-instance v0, Lcom/twitter/metrics/o;

    const-string/jumbo v1, "dalvik_alloc"

    invoke-direct {v0, v1, p1}, Lcom/twitter/metrics/o;-><init>(Ljava/lang/String;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lbzu;->b:Lcom/twitter/metrics/o;

    .line 106
    new-instance v0, Lcom/twitter/metrics/o;

    const-string/jumbo v1, "dalvik_ratio"

    invoke-direct {v0, v1, p1}, Lcom/twitter/metrics/o;-><init>(Ljava/lang/String;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lbzu;->c:Lcom/twitter/metrics/o;

    .line 107
    new-instance v0, Lcom/twitter/metrics/o;

    const-string/jumbo v1, "native_total"

    invoke-direct {v0, v1, p1}, Lcom/twitter/metrics/o;-><init>(Ljava/lang/String;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lbzu;->d:Lcom/twitter/metrics/o;

    .line 108
    new-instance v0, Lcom/twitter/metrics/o;

    const-string/jumbo v1, "native_alloc"

    invoke-direct {v0, v1, p1}, Lcom/twitter/metrics/o;-><init>(Ljava/lang/String;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lbzu;->e:Lcom/twitter/metrics/o;

    .line 109
    invoke-direct {p0}, Lbzu;->D()V

    .line 110
    return-void
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 61
    invoke-super {p0}, Lcom/twitter/metrics/m;->b()V

    .line 62
    invoke-virtual {p0}, Lbzu;->h()V

    .line 63
    return-void
.end method

.method protected b(Landroid/content/SharedPreferences$Editor;)V
    .locals 2

    .prologue
    .line 95
    invoke-super {p0, p1}, Lcom/twitter/metrics/m;->b(Landroid/content/SharedPreferences$Editor;)V

    .line 96
    iget-object v0, p0, Lbzu;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/metrics/o;

    .line 97
    invoke-virtual {v0, p1}, Lcom/twitter/metrics/o;->b(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0

    .line 99
    :cond_0
    return-void
.end method

.method protected br_()V
    .locals 2

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbzu;->b(Z)V

    .line 68
    iget-object v0, p0, Lbzu;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/metrics/o;

    .line 69
    invoke-virtual {v0}, Lcom/twitter/metrics/o;->a()V

    goto :goto_0

    .line 71
    :cond_0
    invoke-super {p0}, Lcom/twitter/metrics/m;->br_()V

    .line 72
    return-void
.end method

.method public h()V
    .locals 8

    .prologue
    .line 75
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v2

    .line 77
    iget-object v1, p0, Lbzu;->a:Lcom/twitter/metrics/o;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/metrics/o;->a(J)V

    .line 78
    iget-object v1, p0, Lbzu;->b:Lcom/twitter/metrics/o;

    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v1, v4, v5}, Lcom/twitter/metrics/o;->a(J)V

    .line 79
    iget-object v1, p0, Lbzu;->c:Lcom/twitter/metrics/o;

    const-wide/16 v4, 0x64

    mul-long/2addr v2, v4

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v4

    div-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Lcom/twitter/metrics/o;->a(J)V

    .line 80
    iget-object v0, p0, Lbzu;->d:Lcom/twitter/metrics/o;

    invoke-static {}, Landroid/os/Debug;->getNativeHeapSize()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/metrics/o;->a(J)V

    .line 81
    iget-object v0, p0, Lbzu;->e:Lcom/twitter/metrics/o;

    invoke-static {}, Landroid/os/Debug;->getNativeHeapAllocatedSize()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/metrics/o;->a(J)V

    .line 82
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbzu;->b(Z)V

    .line 83
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 134
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 135
    invoke-virtual {p0}, Lbzu;->C()Ljava/util/HashMap;

    move-result-object v0

    .line 136
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 137
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v4, 0x3d

    .line 138
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 139
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    .line 140
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 142
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
