.class public Lcoi;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcoi$b;,
        Lcoi$a;
    }
.end annotation


# direct methods
.method public static a(JLjava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 47
    const-string/jumbo v0, "unassigned"

    invoke-static {p0, p1, p2, v0}, Lcoj;->b(JLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    invoke-static {}, Lcoj;->a()J

    move-result-wide v0

    invoke-static {v0, v1, p0}, Lcoi;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(JLjava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 92
    invoke-static {p0, p1, p2}, Lcoi;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 93
    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static varargs a(JLjava/lang/String;[Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 116
    invoke-static {p0, p1, p2}, Lcoi;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Lcoi;->b(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 80
    invoke-static {}, Lcoj;->a()J

    move-result-wide v0

    invoke-static {v0, v1, p0, p1}, Lcoi;->a(JLjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 104
    invoke-static {}, Lcoj;->a()J

    move-result-wide v0

    invoke-static {v0, v1, p0, p1}, Lcoi;->a(JLjava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static b(JLjava/lang/String;)Z
    .locals 2

    .prologue
    .line 69
    invoke-static {p0, p1, p2}, Lcoi;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcoi;->g(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 58
    invoke-static {}, Lcoj;->a()J

    move-result-wide v0

    invoke-static {v0, v1, p0}, Lcoi;->b(JLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static varargs b(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 190
    invoke-static {p0, p1}, Lcom/twitter/util/y;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    const-string/jumbo v0, "unassigned"

    invoke-static {p0, v0}, Lcoj;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(JLjava/lang/String;)Z
    .locals 2

    .prologue
    .line 137
    const-string/jumbo v0, "unassigned"

    invoke-static {p0, p1, p2, v0}, Lcoj;->a(JLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcoi;->g(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 148
    const-string/jumbo v0, "unassigned"

    invoke-static {p0, v0}, Lcoj;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcoi;->g(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static e(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 159
    invoke-static {p0}, Lcoi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcoi;->h(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static f(Ljava/lang/String;)Lcoi$a;
    .locals 1

    .prologue
    .line 169
    invoke-static {}, Lcrt;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    new-instance v0, Lcoi$1;

    invoke-direct {v0, p0}, Lcoi$1;-><init>(Ljava/lang/String;)V

    .line 178
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcoi$b;

    invoke-direct {v0, p0}, Lcoi$b;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static g(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 186
    const-string/jumbo v0, "control"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "unassigned"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 197
    if-eqz p0, :cond_0

    const-string/jumbo v0, "unassigned"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
