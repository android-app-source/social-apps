.class public final Lchr$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lchr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lchr;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:J

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcho$a;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/lang/String;

.field e:Lchs;

.field f:Lcom/twitter/model/timeline/m;

.field g:Lcom/twitter/model/timeline/r;

.field h:Lchj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method


# virtual methods
.method public R_()Z
    .locals 4

    .prologue
    .line 223
    iget-object v0, p0, Lchr$a;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lchr$a;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lchr$a;->c:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lchr;->a:Ljava/util/Set;

    iget-object v1, p0, Lchr$a;->d:Ljava/lang/String;

    .line 224
    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 223
    :goto_0
    return v0

    .line 224
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)Lchr$a;
    .locals 1

    .prologue
    .line 181
    iput-wide p1, p0, Lchr$a;->b:J

    .line 182
    return-object p0
.end method

.method public a(Lchj;)Lchr$a;
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lchr$a;->h:Lchj;

    .line 218
    return-object p0
.end method

.method public a(Lchs;)Lchr$a;
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lchr$a;->e:Lchs;

    .line 200
    return-object p0
.end method

.method public a(Lcom/twitter/model/timeline/m;)Lchr$a;
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lchr$a;->f:Lcom/twitter/model/timeline/m;

    .line 206
    return-object p0
.end method

.method public a(Lcom/twitter/model/timeline/r;)Lchr$a;
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lchr$a;->g:Lcom/twitter/model/timeline/r;

    .line 212
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lchr$a;
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lchr$a;->a:Ljava/lang/String;

    .line 176
    return-object p0
.end method

.method public a(Ljava/util/List;)Lchr$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcho$a;",
            ">;)",
            "Lchr$a;"
        }
    .end annotation

    .prologue
    .line 187
    iput-object p1, p0, Lchr$a;->c:Ljava/util/List;

    .line 188
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lchr$a;
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lchr$a;->d:Ljava/lang/String;

    .line 194
    return-object p0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 148
    invoke-virtual {p0}, Lchr$a;->e()Lchr;

    move-result-object v0

    return-object v0
.end method

.method public e()Lchr;
    .locals 1

    .prologue
    .line 230
    new-instance v0, Lchr;

    invoke-direct {v0, p0}, Lchr;-><init>(Lchr$a;)V

    return-object v0
.end method
