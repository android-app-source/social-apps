.class public Lbiv;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbiv$a;
    }
.end annotation


# static fields
.field private static final a:[I

.field private static b:Lbiv;


# instance fields
.field private final c:Z

.field private final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x1

    new-array v0, v0, [I

    sput-object v0, Lbiv;->a:[I

    .line 28
    const/4 v0, 0x0

    sput-object v0, Lbiv;->b:Lbiv;

    return-void
.end method

.method public constructor <init>(Lbiv$a;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iget-boolean v0, p1, Lbiv$a;->a:Z

    iput-boolean v0, p0, Lbiv;->c:Z

    .line 36
    iget-boolean v0, p1, Lbiv$a;->b:Z

    iput-boolean v0, p0, Lbiv;->d:Z

    .line 37
    return-void
.end method

.method public static a()Lbiv;
    .locals 2

    .prologue
    .line 44
    sget-object v1, Lbiv;->a:[I

    monitor-enter v1

    .line 45
    :try_start_0
    sget-object v0, Lbiv;->b:Lbiv;

    if-nez v0, :cond_0

    .line 46
    const-class v0, Lbiv;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 47
    invoke-static {}, Lbiv;->d()Lbiv;

    move-result-object v0

    sput-object v0, Lbiv;->b:Lbiv;

    .line 49
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    sget-object v0, Lbiv;->b:Lbiv;

    return-object v0

    .line 49
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 58
    if-eqz p0, :cond_0

    const-string/jumbo v0, "avc1.4D401E"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    invoke-static {}, Lbiv;->a()Lbiv;

    move-result-object v0

    invoke-virtual {v0}, Lbiv;->b()Z

    move-result v0

    .line 67
    :goto_0
    return v0

    .line 62
    :cond_0
    if-eqz p0, :cond_1

    const-string/jumbo v0, "avc1.64001E"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    invoke-static {}, Lbiv;->a()Lbiv;

    move-result-object v0

    invoke-virtual {v0}, Lbiv;->c()Z

    move-result v0

    goto :goto_0

    .line 67
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static d()Lbiv;
    .locals 5

    .prologue
    .line 98
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 99
    new-instance v1, Lbiv$a;

    invoke-direct {v1, v0}, Lbiv$a;-><init>(Ljava/util/concurrent/CountDownLatch;)V

    .line 100
    invoke-static {}, Lcom/twitter/library/av/i;->a()Lcom/twitter/library/av/i;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/av/i;->b()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 102
    const-wide/16 v2, 0x5

    :try_start_0
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    :goto_0
    new-instance v0, Lbiv;

    invoke-direct {v0, v1}, Lbiv;-><init>(Lbiv$a;)V

    return-object v0

    .line 103
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public b()Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lbiv;->c:Z

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lbiv;->d:Z

    return v0
.end method
