.class public final Lbp$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# static fields
.field public static final GenericDraweeView:[I

.field public static final GenericDraweeView_actualImageScaleType:I = 0xb

.field public static final GenericDraweeView_backgroundImage:I = 0xc

.field public static final GenericDraweeView_fadeDuration:I = 0x0

.field public static final GenericDraweeView_failureImage:I = 0x6

.field public static final GenericDraweeView_failureImageScaleType:I = 0x7

.field public static final GenericDraweeView_overlayImage:I = 0xd

.field public static final GenericDraweeView_placeholderImage:I = 0x2

.field public static final GenericDraweeView_placeholderImageScaleType:I = 0x3

.field public static final GenericDraweeView_pressedStateOverlayImage:I = 0xe

.field public static final GenericDraweeView_progressBarAutoRotateInterval:I = 0xa

.field public static final GenericDraweeView_progressBarImage:I = 0x8

.field public static final GenericDraweeView_progressBarImageScaleType:I = 0x9

.field public static final GenericDraweeView_retryImage:I = 0x4

.field public static final GenericDraweeView_retryImageScaleType:I = 0x5

.field public static final GenericDraweeView_roundAsCircle:I = 0xf

.field public static final GenericDraweeView_roundBottomLeft:I = 0x14

.field public static final GenericDraweeView_roundBottomRight:I = 0x13

.field public static final GenericDraweeView_roundTopLeft:I = 0x11

.field public static final GenericDraweeView_roundTopRight:I = 0x12

.field public static final GenericDraweeView_roundWithOverlayColor:I = 0x15

.field public static final GenericDraweeView_roundedCornerRadius:I = 0x10

.field public static final GenericDraweeView_roundingBorderColor:I = 0x17

.field public static final GenericDraweeView_roundingBorderWidth:I = 0x16

.field public static final GenericDraweeView_viewAspectRatio:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const/16 v0, 0x18

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lbp$a;->GenericDraweeView:[I

    return-void

    :array_0
    .array-data 4
        0x7f010257
        0x7f010258
        0x7f010259
        0x7f01025a
        0x7f01025b
        0x7f01025c
        0x7f01025d
        0x7f01025e
        0x7f01025f
        0x7f010260
        0x7f010261
        0x7f010262
        0x7f010263
        0x7f010264
        0x7f010265
        0x7f010266
        0x7f010267
        0x7f010268
        0x7f010269
        0x7f01026a
        0x7f01026b
        0x7f01026c
        0x7f01026d
        0x7f01026e
    .end array-data
.end method
