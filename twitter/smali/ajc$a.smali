.class public final Lajc$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lajc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lajc;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Z

.field private e:J

.field private f:J

.field private g:J

.field private h:J

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private final k:Lcom/twitter/library/client/Session;

.field private l:I

.field private m:Lcom/twitter/android/revenue/c;

.field private n:Lcom/twitter/model/timeline/p;

.field private o:Lbgp;

.field private p:Lbgp;

.field private q:Lbgp;

.field private r:Ljava/lang/String;

.field private s:Lbwb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbwb",
            "<",
            "Lcom/mopub/nativeads/NativeAd;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lbgw;


# direct methods
.method public constructor <init>(Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 117
    sget-object v0, Lbgw;->a:Lbgw;

    iput-object v0, p0, Lajc$a;->t:Lbgw;

    .line 121
    iput-object p1, p0, Lajc$a;->k:Lcom/twitter/library/client/Session;

    .line 122
    return-void
.end method

.method static synthetic a(Lajc$a;)I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lajc$a;->a:I

    return v0
.end method

.method static synthetic b(Lajc$a;)I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lajc$a;->b:I

    return v0
.end method

.method static synthetic c(Lajc$a;)I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lajc$a;->c:I

    return v0
.end method

.method static synthetic d(Lajc$a;)Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lajc$a;->d:Z

    return v0
.end method

.method static synthetic e(Lajc$a;)J
    .locals 2

    .prologue
    .line 85
    iget-wide v0, p0, Lajc$a;->e:J

    return-wide v0
.end method

.method static synthetic f(Lajc$a;)J
    .locals 2

    .prologue
    .line 85
    iget-wide v0, p0, Lajc$a;->f:J

    return-wide v0
.end method

.method static synthetic g(Lajc$a;)J
    .locals 2

    .prologue
    .line 85
    iget-wide v0, p0, Lajc$a;->g:J

    return-wide v0
.end method

.method static synthetic h(Lajc$a;)J
    .locals 2

    .prologue
    .line 85
    iget-wide v0, p0, Lajc$a;->h:J

    return-wide v0
.end method

.method static synthetic i(Lajc$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lajc$a;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lajc$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lajc$a;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lajc$a;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lajc$a;->k:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method static synthetic l(Lajc$a;)I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lajc$a;->l:I

    return v0
.end method

.method static synthetic m(Lajc$a;)Lcom/twitter/android/revenue/c;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lajc$a;->m:Lcom/twitter/android/revenue/c;

    return-object v0
.end method

.method static synthetic n(Lajc$a;)Lcom/twitter/model/timeline/p;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lajc$a;->n:Lcom/twitter/model/timeline/p;

    return-object v0
.end method

.method static synthetic o(Lajc$a;)Lbgp;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lajc$a;->o:Lbgp;

    return-object v0
.end method

.method static synthetic p(Lajc$a;)Lbgp;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lajc$a;->p:Lbgp;

    return-object v0
.end method

.method static synthetic q(Lajc$a;)Lbgp;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lajc$a;->q:Lbgp;

    return-object v0
.end method

.method static synthetic r(Lajc$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lajc$a;->r:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic s(Lajc$a;)Lbwb;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lajc$a;->s:Lbwb;

    return-object v0
.end method

.method static synthetic t(Lajc$a;)Lbgw;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lajc$a;->t:Lbgw;

    return-object v0
.end method


# virtual methods
.method public a(I)Lajc$a;
    .locals 0

    .prologue
    .line 125
    iput p1, p0, Lajc$a;->a:I

    .line 126
    return-object p0
.end method

.method public a(J)Lajc$a;
    .locals 1

    .prologue
    .line 145
    iput-wide p1, p0, Lajc$a;->e:J

    .line 146
    return-object p0
.end method

.method public a(Lbgp;)Lajc$a;
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lajc$a;->o:Lbgp;

    .line 193
    return-object p0
.end method

.method public a(Lbgw;)Lajc$a;
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lajc$a;->t:Lbgw;

    .line 217
    return-object p0
.end method

.method public a(Lbwb;)Lajc$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbwb",
            "<",
            "Lcom/mopub/nativeads/NativeAd;",
            ">;)",
            "Lajc$a;"
        }
    .end annotation

    .prologue
    .line 222
    iput-object p1, p0, Lajc$a;->s:Lbwb;

    .line 223
    return-object p0
.end method

.method public a(Lcom/twitter/android/revenue/c;)Lajc$a;
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lajc$a;->m:Lcom/twitter/android/revenue/c;

    .line 181
    return-object p0
.end method

.method public a(Lcom/twitter/model/timeline/p;)Lajc$a;
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lajc$a;->n:Lcom/twitter/model/timeline/p;

    .line 187
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lajc$a;
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lajc$a;->i:Ljava/lang/String;

    .line 166
    return-object p0
.end method

.method public a(Z)Lajc$a;
    .locals 0

    .prologue
    .line 140
    iput-boolean p1, p0, Lajc$a;->d:Z

    .line 141
    return-object p0
.end method

.method public b(I)Lajc$a;
    .locals 0

    .prologue
    .line 130
    iput p1, p0, Lajc$a;->b:I

    .line 131
    return-object p0
.end method

.method public b(J)Lajc$a;
    .locals 1

    .prologue
    .line 150
    iput-wide p1, p0, Lajc$a;->f:J

    .line 151
    return-object p0
.end method

.method public b(Lbgp;)Lajc$a;
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lajc$a;->p:Lbgp;

    .line 199
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lajc$a;
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lajc$a;->j:Ljava/lang/String;

    .line 171
    return-object p0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, Lajc$a;->e()Lajc;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lajc$a;
    .locals 0

    .prologue
    .line 135
    iput p1, p0, Lajc$a;->c:I

    .line 136
    return-object p0
.end method

.method public c(J)Lajc$a;
    .locals 1

    .prologue
    .line 155
    iput-wide p1, p0, Lajc$a;->g:J

    .line 156
    return-object p0
.end method

.method public c(Lbgp;)Lajc$a;
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lajc$a;->q:Lbgp;

    .line 205
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lajc$a;
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lajc$a;->r:Ljava/lang/String;

    .line 211
    return-object p0
.end method

.method public d(I)Lajc$a;
    .locals 0

    .prologue
    .line 175
    iput p1, p0, Lajc$a;->l:I

    .line 176
    return-object p0
.end method

.method public d(J)Lajc$a;
    .locals 1

    .prologue
    .line 160
    iput-wide p1, p0, Lajc$a;->h:J

    .line 161
    return-object p0
.end method

.method public e()Lajc;
    .locals 2

    .prologue
    .line 229
    new-instance v0, Lajc;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lajc;-><init>(Lajc$a;Lajc$1;)V

    return-object v0
.end method
