.class public Lbak;
.super Lbao;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbao",
        "<",
        "Lcom/twitter/library/api/y;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:I

.field private static final b:I


# instance fields
.field private final c:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private r:Lcom/twitter/model/account/LoginResponse;

.field private s:Lcom/twitter/library/api/u;

.field private t:Lcom/twitter/model/core/TwitterUser;

.field private u:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 42
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lbak;->a:I

    .line 43
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lbak;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 61
    const-class v0, Lbak;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lbao;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 62
    iput-object p7, p0, Lbak;->c:Ljava/lang/String;

    .line 63
    iput-object p4, p0, Lbak;->h:Ljava/lang/String;

    .line 64
    iput-object p3, p0, Lbak;->k:Ljava/lang/String;

    .line 65
    iput-object p5, p0, Lbak;->i:Ljava/lang/String;

    .line 66
    iput-object p6, p0, Lbak;->j:Ljava/lang/String;

    .line 67
    iput-object p8, p0, Lbak;->l:Ljava/lang/String;

    .line 69
    new-instance v0, Lcom/twitter/library/service/o;

    const/16 v1, 0x64

    sget v2, Lbak;->a:I

    sget v3, Lbak;->b:I

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/service/o;-><init>(III)V

    .line 73
    invoke-super {p0}, Lbao;->v()Lcom/twitter/library/service/f;

    move-result-object v1

    .line 74
    if-eqz v1, :cond_0

    .line 75
    invoke-virtual {v1, v0}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    .line 76
    invoke-virtual {p0, v1}, Lbak;->a(Lcom/twitter/async/service/k;)Lcom/twitter/async/service/AsyncOperation;

    .line 80
    :goto_0
    return-void

    .line 78
    :cond_0
    invoke-virtual {p0, v0}, Lbak;->a(Lcom/twitter/async/service/k;)Lcom/twitter/async/service/AsyncOperation;

    goto :goto_0
.end method

.method private a(Lcom/twitter/async/service/j;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 243
    iget-object v0, p0, Lbak;->p:Landroid/content/Context;

    const-string/jumbo v1, "app:twitter_service:account:create"

    .line 245
    invoke-virtual {p0}, Lbak;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/service/v;->c:J

    .line 246
    invoke-virtual {p0, p1}, Lbak;->d(Lcom/twitter/async/service/j;)Z

    move-result v4

    const/4 v7, 0x0

    move-object v5, p1

    move v6, p2

    .line 243
    invoke-static/range {v0 .. v7}, Lcom/twitter/library/api/a;->a(Landroid/content/Context;Ljava/lang/String;JZLcom/twitter/async/service/j;ZLjava/lang/String;)V

    .line 249
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lbak;
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lbak;->m:Ljava/lang/String;

    .line 203
    return-object p0
.end method

.method public a(Lcom/twitter/async/service/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 172
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lbak;->a(Lcom/twitter/async/service/j;Z)V

    .line 173
    return-void
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V
    .locals 6

    .prologue
    .line 178
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->m()Lcom/twitter/network/l;

    move-result-object v0

    iget v1, v0, Lcom/twitter/network/l;->a:I

    .line 179
    invoke-virtual {p3}, Lcom/twitter/library/api/y;->b()Ljava/lang/Object;

    move-result-object v0

    .line 181
    const/16 v2, 0xc8

    if-ne v1, v2, :cond_3

    .line 183
    const-string/jumbo v1, "x-twitter-new-account-oauth-access-token"

    invoke-virtual {p1, v1}, Lcom/twitter/network/HttpOperation;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 184
    const-string/jumbo v2, "x-twitter-new-account-oauth-secret"

    invoke-virtual {p1, v2}, Lcom/twitter/network/HttpOperation;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 185
    const-string/jumbo v3, "kdt"

    invoke-virtual {p1, v3}, Lcom/twitter/network/HttpOperation;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 186
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    new-instance v4, Lcom/twitter/model/account/OAuthToken;

    invoke-direct {v4, v1, v2}, Lcom/twitter/model/account/OAuthToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    new-instance v1, Lcom/twitter/model/account/LoginResponse;

    const/4 v2, 0x1

    iget-object v5, v4, Lcom/twitter/model/account/OAuthToken;->b:Ljava/lang/String;

    iget-object v4, v4, Lcom/twitter/model/account/OAuthToken;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v5, v4, v3}, Lcom/twitter/model/account/LoginResponse;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lbak;->r:Lcom/twitter/model/account/LoginResponse;

    .line 193
    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lbak;->t:Lcom/twitter/model/core/TwitterUser;

    .line 198
    :cond_2
    :goto_1
    invoke-super {p0, p1, p2, p3}, Lbao;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V

    goto :goto_0

    .line 194
    :cond_3
    const/16 v2, 0x193

    if-ne v1, v2, :cond_2

    instance-of v1, v0, Lcom/twitter/library/api/u;

    if-eqz v1, :cond_2

    .line 196
    check-cast v0, Lcom/twitter/library/api/u;

    iput-object v0, p0, Lbak;->s:Lcom/twitter/library/api/u;

    goto :goto_1
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 36
    check-cast p3, Lcom/twitter/library/api/y;

    invoke-virtual {p0, p1, p2, p3}, Lbak;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/service/d$a;
    .locals 13

    .prologue
    const/4 v12, 0x1

    .line 95
    invoke-virtual {p0}, Lbak;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 96
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "1.1"

    .line 97
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "account"

    aput-object v3, v1, v2

    const-string/jumbo v2, "create"

    aput-object v2, v1, v12

    .line 98
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 99
    iget-object v1, p0, Lbak;->k:Ljava/lang/String;

    .line 100
    iget-object v2, p0, Lbak;->h:Ljava/lang/String;

    .line 101
    iget-object v3, p0, Lbak;->i:Ljava/lang/String;

    .line 102
    iget-object v4, p0, Lbak;->j:Ljava/lang/String;

    .line 103
    iget-object v5, p0, Lbak;->c:Ljava/lang/String;

    .line 104
    iget-object v6, p0, Lbak;->m:Ljava/lang/String;

    .line 105
    iget-object v7, p0, Lbak;->u:Ljava/lang/String;

    .line 106
    iget-object v8, p0, Lbak;->l:Ljava/lang/String;

    .line 107
    iget-object v9, p0, Lbak;->p:Landroid/content/Context;

    .line 108
    invoke-static {v9}, Lcom/twitter/library/util/q;->a(Landroid/content/Context;)Lcom/twitter/library/util/q;

    move-result-object v9

    .line 109
    iget-object v10, p0, Lbak;->p:Landroid/content/Context;

    invoke-static {v10}, Lbar;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    .line 111
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v11

    if-lez v11, :cond_0

    .line 112
    const-string/jumbo v11, "kdt"

    invoke-virtual {v0, v11, v10}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 115
    :cond_0
    if-eqz v1, :cond_1

    .line 116
    const-string/jumbo v10, "name"

    invoke-virtual {v0, v10, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 119
    :cond_1
    if-eqz v2, :cond_2

    .line 120
    const-string/jumbo v1, "screen_name"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 123
    :cond_2
    if-eqz v3, :cond_3

    .line 124
    const-string/jumbo v1, "email"

    invoke-virtual {v0, v1, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 127
    :cond_3
    if-eqz v4, :cond_4

    .line 128
    const-string/jumbo v1, "phone_number"

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 131
    :cond_4
    if-eqz v5, :cond_5

    .line 132
    const-string/jumbo v1, "password"

    invoke-virtual {v0, v1, v5}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 135
    :cond_5
    if-eqz v6, :cond_6

    .line 136
    const-string/jumbo v1, "lang"

    invoke-virtual {v0, v1, v6}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 139
    :cond_6
    invoke-static {v7}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 140
    const-string/jumbo v1, "google_auth_token"

    invoke-virtual {v0, v1, v7}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 143
    :cond_7
    invoke-interface {v9}, Lcom/twitter/library/util/u;->a()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 144
    const-string/jumbo v1, "discoverable_by_email"

    const-string/jumbo v2, "true"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 147
    :cond_8
    invoke-interface {v9}, Lcom/twitter/library/util/u;->b()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 148
    const-string/jumbo v1, "discoverable_by_mobile_phone"

    const-string/jumbo v2, "true"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 151
    :cond_9
    invoke-static {v8}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 152
    const-string/jumbo v1, "ui_metrics"

    invoke-virtual {v0, v1, v8}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 155
    :cond_a
    const-string/jumbo v1, "app_cred"

    invoke-virtual {v0, v1, v12}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 157
    return-object v0
.end method

.method public b(Lcom/twitter/async/service/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 167
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lbak;->a(Lcom/twitter/async/service/j;Z)V

    .line 168
    return-void
.end method

.method protected c(Lcom/twitter/library/service/u;)Z
    .locals 1

    .prologue
    .line 84
    const-string/jumbo v0, "google_auth_token_signal_param_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "android_autoconfirm_enabled"

    .line 85
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lbak;->u:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 87
    iget-object v0, p0, Lbak;->p:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/network/f;->a(Landroid/content/Context;)Lcom/twitter/library/network/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/network/e;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbak;->u:Ljava/lang/String;

    .line 89
    :cond_1
    invoke-super {p0, p1}, Lbao;->c(Lcom/twitter/library/service/u;)Z

    move-result v0

    return v0
.end method

.method protected d(Lcom/twitter/async/service/j;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 223
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    :goto_0
    return v1

    .line 234
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 235
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v2

    const/16 v3, 0x193

    if-eq v2, v3, :cond_1

    .line 236
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    const/16 v2, 0x19c

    if-ne v0, v2, :cond_2

    :cond_1
    move v0, v1

    :goto_1
    move v1, v0

    .line 235
    goto :goto_0

    .line 236
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected e()Lcom/twitter/library/api/y;
    .locals 1

    .prologue
    .line 162
    const/16 v0, 0x3c

    invoke-static {v0}, Lcom/twitter/library/api/y;->a(I)Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lbak;->e()Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method

.method public g()Lcom/twitter/library/api/u;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lbak;->s:Lcom/twitter/library/api/u;

    return-object v0
.end method

.method public h()Lcom/twitter/model/core/TwitterUser;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lbak;->t:Lcom/twitter/model/core/TwitterUser;

    return-object v0
.end method

.method public s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lbak;->h:Ljava/lang/String;

    return-object v0
.end method

.method public t()Lcom/twitter/model/account/LoginResponse;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lbak;->r:Lcom/twitter/model/account/LoginResponse;

    return-object v0
.end method
