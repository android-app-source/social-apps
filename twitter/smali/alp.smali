.class public Lalp;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method static a(Landroid/app/Application;)Lcom/twitter/app/common/util/b;
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcom/twitter/app/common/util/b;

    invoke-direct {v0, p0}, Lcom/twitter/app/common/util/b;-><init>(Landroid/app/Application;)V

    return-object v0
.end method

.method static a(Lcom/twitter/app/common/util/b;)Lcom/twitter/app/common/util/f;
    .locals 1

    .prologue
    .line 94
    new-instance v0, Lcom/twitter/app/common/util/f;

    invoke-direct {v0, p0}, Lcom/twitter/app/common/util/f;-><init>(Lcom/twitter/app/common/util/b;)V

    return-object v0
.end method

.method static a(Landroid/content/ContentResolver;)Lcom/twitter/util/android/a;
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lcom/twitter/util/android/a;

    invoke-direct {v0, p0}, Lcom/twitter/util/android/a;-><init>(Landroid/content/ContentResolver;)V

    return-object v0
.end method

.method static a(Lcqs;Lcom/twitter/util/connectivity/a;)Lcom/twitter/util/connectivity/b;
    .locals 1

    .prologue
    .line 151
    new-instance v0, Lcom/twitter/util/connectivity/b;

    invoke-direct {v0, p0, p1}, Lcom/twitter/util/connectivity/b;-><init>(Lcqs;Lcom/twitter/util/p;)V

    return-object v0
.end method

.method static a()Lcpd;
    .locals 1

    .prologue
    .line 73
    new-instance v0, Lcph;

    invoke-direct {v0}, Lcph;-><init>()V

    return-object v0
.end method

.method static a(Lcqq;)Lcqt;
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lcqq;->a()Lcqt;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/content/Context;)Lcrl;
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcrk;

    invoke-direct {v0, p0}, Lcrk;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method static b()Lcom/twitter/app/common/util/h;
    .locals 1

    .prologue
    .line 101
    new-instance v0, Lcom/twitter/app/common/util/h;

    invoke-direct {v0}, Lcom/twitter/app/common/util/h;-><init>()V

    return-object v0
.end method

.method static b(Landroid/app/Application;)Lcom/twitter/util/android/b;
    .locals 1

    .prologue
    .line 87
    new-instance v0, Lcom/twitter/util/android/b;

    invoke-direct {v0, p0}, Lcom/twitter/util/android/b;-><init>(Landroid/app/Application;)V

    return-object v0
.end method

.method static b(Landroid/content/Context;)Lcqq;
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcqm;

    invoke-direct {v0, p0}, Lcqm;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method static b(Lcqq;)Lcqs;
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, Lcqq;->b()Lcqs;

    move-result-object v0

    return-object v0
.end method

.method static c(Landroid/content/Context;)Lcob;
    .locals 1

    .prologue
    .line 115
    new-instance v0, Lcob;

    invoke-direct {v0, p0}, Lcob;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method static c()Lcom/twitter/util/android/f;
    .locals 1

    .prologue
    .line 122
    new-instance v0, Lcom/twitter/util/android/f;

    invoke-direct {v0}, Lcom/twitter/util/android/f;-><init>()V

    return-object v0
.end method

.method static d()Lcom/twitter/util/connectivity/a;
    .locals 1

    .prologue
    .line 143
    new-instance v0, Lcom/twitter/util/connectivity/a;

    invoke-direct {v0}, Lcom/twitter/util/connectivity/a;-><init>()V

    return-object v0
.end method

.method static d(Landroid/content/Context;)Lcrr;
    .locals 1

    .prologue
    .line 129
    new-instance v0, Lcro;

    invoke-direct {v0, p0}, Lcro;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method static e(Landroid/content/Context;)Lcrq;
    .locals 1

    .prologue
    .line 136
    new-instance v0, Lcrq;

    invoke-direct {v0, p0}, Lcrq;-><init>(Landroid/content/Context;)V

    return-object v0
.end method
