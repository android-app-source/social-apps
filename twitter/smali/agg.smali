.class public Lagg;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/twitter/android/search/c;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/twitter/android/search/c;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lagg;->a:Landroid/app/Activity;

    .line 22
    iput-object p2, p0, Lagg;->b:Lcom/twitter/android/search/c;

    .line 23
    return-void
.end method

.method public static a(Landroid/app/Activity;)Lagg;
    .locals 2

    .prologue
    .line 16
    invoke-static {p0}, Lcom/twitter/android/search/c;->a(Landroid/app/Activity;)Lcom/twitter/android/search/c;

    move-result-object v0

    .line 17
    new-instance v1, Lagg;

    invoke-direct {v1, p0, v0}, Lagg;-><init>(Landroid/app/Activity;Lcom/twitter/android/search/c;)V

    return-object v1
.end method


# virtual methods
.method public a(Lage;)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lagg;->a:Landroid/app/Activity;

    instance-of v0, v0, Lagd;

    if-eqz v0, :cond_1

    .line 33
    iget-object v0, p0, Lagg;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lagd;

    invoke-interface {v0, p1}, Lagd;->a(Lako;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 34
    invoke-virtual {p0, p1}, Lagg;->b(Lage;)V

    .line 39
    :cond_0
    :goto_0
    return-void

    .line 37
    :cond_1
    invoke-virtual {p0, p1}, Lagg;->b(Lage;)V

    goto :goto_0
.end method

.method public b(Lage;)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lagg;->b:Lcom/twitter/android/search/c;

    invoke-virtual {v0, p1}, Lcom/twitter/android/search/c;->a(Lako;)V

    .line 47
    return-void
.end method
