.class public final Lcce$b;
.super Lcbx$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcce;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbx$b",
        "<",
        "Lcce;",
        "Lcce$a;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Lcbx$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcce$a;
    .locals 1

    .prologue
    .line 81
    new-instance v0, Lcce$a;

    invoke-direct {v0}, Lcce$a;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcbx$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 75
    check-cast p2, Lcce$a;

    invoke-virtual {p0, p1, p2, p3}, Lcce$b;->a(Lcom/twitter/util/serialization/n;Lcce$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcce$a;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-super {p0, p1, p2, p3}, Lcbx$b;->a(Lcom/twitter/util/serialization/n;Lcbx$a;I)V

    .line 97
    sget-object v0, Lcom/twitter/model/geo/TwitterPlace;->a:Lcom/twitter/util/serialization/b;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->b(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/geo/TwitterPlace;

    invoke-virtual {p2, v0}, Lcce$a;->a(Lcom/twitter/model/geo/TwitterPlace;)Lcce$a;

    .line 98
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 75
    check-cast p2, Lcce$a;

    invoke-virtual {p0, p1, p2, p3}, Lcce$b;->a(Lcom/twitter/util/serialization/n;Lcce$a;I)V

    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/util/serialization/o;Lcbx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    check-cast p2, Lcce;

    invoke-virtual {p0, p1, p2}, Lcce$b;->a(Lcom/twitter/util/serialization/o;Lcce;)V

    return-void
.end method

.method public a(Lcom/twitter/util/serialization/o;Lcce;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    invoke-super {p0, p1, p2}, Lcbx$b;->a(Lcom/twitter/util/serialization/o;Lcbx;)V

    .line 89
    invoke-static {p2}, Lcce;->a(Lcce;)Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/geo/TwitterPlace;->a:Lcom/twitter/util/serialization/b;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 90
    return-void
.end method

.method public synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    check-cast p2, Lcce;

    invoke-virtual {p0, p1, p2}, Lcce$b;->a(Lcom/twitter/util/serialization/o;Lcce;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0}, Lcce$b;->a()Lcce$a;

    move-result-object v0

    return-object v0
.end method
