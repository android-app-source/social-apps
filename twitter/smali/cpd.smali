.class public abstract Lcpd;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcpd;
    .locals 1

    .prologue
    .line 13
    invoke-static {}, Lcom/twitter/util/m;->ai()Lcom/twitter/util/m$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/util/m$a;->t()Lcpd;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lcpb;)V
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lcpd;->a()Lcpd;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcpd;->a(Lcpb;)V

    .line 51
    return-void
.end method

.method public static c(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 64
    invoke-static {}, Lcpd;->a()Lcpd;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcpd;->a(Ljava/lang/Throwable;)V

    .line 65
    return-void
.end method

.method public static d(Lcpb;)V
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Lcpd;->a()Lcpd;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcpd;->b(Lcpb;)V

    .line 58
    return-void
.end method

.method public static d(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 71
    invoke-static {}, Lcpd;->a()Lcpd;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcpd;->b(Ljava/lang/Throwable;)V

    .line 72
    return-void
.end method


# virtual methods
.method public abstract a(Lcpb;)V
.end method

.method public abstract a(Ljava/lang/Throwable;)V
.end method

.method public abstract b()Lcpa;
.end method

.method public abstract b(Lcpb;)V
.end method

.method public abstract b(Ljava/lang/Throwable;)V
.end method
