.class public Lavg;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lavg$a;,
        Lavg$b;
    }
.end annotation


# instance fields
.field a:Lave;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lavg$b;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lavg$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lave;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lavg;->a:Lave;

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lavg;->b:Ljava/util/List;

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lavg;->c:Ljava/util/List;

    .line 21
    return-void
.end method


# virtual methods
.method public a()Lavg;
    .locals 3

    .prologue
    .line 37
    iget-object v0, p0, Lavg;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavg$b;

    .line 38
    iget-object v2, p0, Lavg;->a:Lave;

    invoke-interface {v0, v2}, Lavg$b;->a(Lave;)V

    goto :goto_0

    .line 40
    :cond_0
    return-object p0
.end method

.method public a(Lavg$a;)Lavg;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lavg;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    return-object p0
.end method

.method public a(Lavg$b;)Lavg;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lavg;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26
    return-object p0
.end method

.method public b()Lavg;
    .locals 3

    .prologue
    .line 45
    iget-object v0, p0, Lavg;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavg$a;

    .line 46
    iget-object v2, p0, Lavg;->a:Lave;

    invoke-interface {v0, v2}, Lavg$a;->a(Lave;)V

    goto :goto_0

    .line 48
    :cond_0
    return-object p0
.end method
