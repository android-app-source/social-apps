.class Lcok$c;
.super Lcok$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcok;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcok$d;",
        ">",
        "Lcok$b",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lrx/j;

.field private final b:Lcom/twitter/util/object/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/j",
            "<TT;>;"
        }
    .end annotation
.end field

.field private c:Lcok$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private d:Z


# direct methods
.method constructor <init>(Lcom/twitter/util/object/j;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/object/j",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 126
    invoke-direct {p0}, Lcok$b;-><init>()V

    .line 127
    iput-object p1, p0, Lcok$c;->b:Lcom/twitter/util/object/j;

    .line 128
    invoke-static {}, Lcon;->a()Lrx/c;

    move-result-object v0

    new-instance v1, Lcok$c$1;

    invoke-direct {v1, p0}, Lcok$c$1;-><init>(Lcok$c;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcok$c;->a:Lrx/j;

    .line 136
    return-void
.end method

.method static synthetic a(Lcok$c;Z)Z
    .locals 0

    .prologue
    .line 120
    iput-boolean p1, p0, Lcok$c;->d:Z

    return p1
.end method


# virtual methods
.method public declared-synchronized a()Lcok$d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 146
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcok$c;->a:Lrx/j;

    invoke-interface {v0}, Lrx/j;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Call to get() on a closed feature configuration cache."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 149
    :cond_0
    iget-boolean v0, p0, Lcok$c;->d:Z

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Lcok$c;->b:Lcom/twitter/util/object/j;

    invoke-interface {v0}, Lcom/twitter/util/object/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcok$d;

    iput-object v0, p0, Lcok$c;->c:Lcok$d;

    .line 151
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcok$c;->d:Z

    .line 153
    :cond_1
    iget-object v0, p0, Lcok$c;->c:Lcok$d;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcok$d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 120
    invoke-virtual {p0}, Lcok$c;->a()Lcok$d;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcok$c;->a:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 141
    return-void
.end method
