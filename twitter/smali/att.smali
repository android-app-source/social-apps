.class public Latt;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/p;

.field private final c:Lcom/twitter/library/client/v;

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/p;Lcom/twitter/library/client/v;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Latt;->a:Landroid/content/Context;

    .line 30
    iput-object p2, p0, Latt;->b:Lcom/twitter/library/client/p;

    .line 31
    iput-object p3, p0, Latt;->c:Lcom/twitter/library/client/v;

    .line 32
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Latt;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Latt;->d:I

    .line 36
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 39
    iget v0, p0, Latt;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Latt;->d:I

    .line 40
    iget v0, p0, Latt;->d:I

    if-nez v0, :cond_0

    .line 42
    iget-object v0, p0, Latt;->b:Lcom/twitter/library/client/p;

    new-instance v1, Lbha;

    iget-object v2, p0, Latt;->a:Landroid/content/Context;

    iget-object v3, p0, Latt;->c:Lcom/twitter/library/client/v;

    .line 43
    invoke-virtual {v3}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lbha;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 42
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 45
    :cond_0
    return-void
.end method
