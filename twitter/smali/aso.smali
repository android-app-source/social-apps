.class public Laso;
.super Laum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laum",
        "<",
        "Ljava/lang/String;",
        "Lcom/twitter/util/collection/m",
        "<",
        "Lcgl;",
        "Lcom/twitter/model/core/y;",
        ">;",
        "Lbex;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/Session;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Laum;-><init>()V

    .line 30
    iput-object p1, p0, Laso;->a:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Laso;->b:Lcom/twitter/library/client/Session;

    .line 32
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/String;)Lbex;
    .locals 4

    .prologue
    .line 48
    new-instance v1, Lbex;

    iget-object v2, p0, Laso;->a:Landroid/content/Context;

    iget-object v3, p0, Laso;->b:Lcom/twitter/library/client/Session;

    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v2, v3, v0}, Lbex;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    return-object v1
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Lcom/twitter/library/service/s;
    .locals 1

    .prologue
    .line 21
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Laso;->a(Ljava/lang/String;)Lbex;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lbex;)Lcom/twitter/util/collection/m;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbex;",
            ")",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcgl;",
            "Lcom/twitter/model/core/y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    invoke-virtual {p1}, Lbex;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    invoke-virtual {p1}, Lbex;->b()Lcgl;

    move-result-object v0

    .line 39
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/m;->a(Ljava/lang/Object;)Lcom/twitter/util/collection/m;

    move-result-object v0

    .line 41
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lbex;->g()Lcom/twitter/model/core/y;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/m;->b(Ljava/lang/Object;)Lcom/twitter/util/collection/m;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/library/service/s;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    check-cast p1, Lbex;

    invoke-virtual {p0, p1}, Laso;->a(Lbex;)Lcom/twitter/util/collection/m;

    move-result-object v0

    return-object v0
.end method
