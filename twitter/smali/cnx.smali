.class public Lcnx;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcnx$b;,
        Lcnx$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcnx;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:J

.field public final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcnx$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcnx$b;-><init>(Lcnx$1;)V

    sput-object v0, Lcnx;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcnx$a;)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {p1}, Lcnx$a;->a(Lcnx$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcnx;->b:J

    .line 25
    invoke-static {p1}, Lcnx$a;->b(Lcnx$a;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcnx;->c:Ljava/lang/String;

    .line 26
    return-void
.end method

.method synthetic constructor <init>(Lcnx$a;Lcnx$1;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcnx;-><init>(Lcnx$a;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 30
    instance-of v0, p1, Lcnx;

    if-eqz v0, :cond_0

    check-cast p1, Lcnx;

    iget-wide v0, p1, Lcnx;->b:J

    iget-wide v2, p0, Lcnx;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 35
    iget-wide v0, p0, Lcnx;->b:J

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(J)I

    move-result v0

    return v0
.end method
