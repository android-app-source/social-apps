.class public Laps;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laps$b;,
        Laps$c;,
        Laps$a;
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:Landroid/content/Context;

.field private final c:Landroid/content/res/Resources;

.field private final d:Lcom/twitter/model/dms/q;

.field private final e:Laps$b;

.field private final f:Lcom/twitter/internal/android/widget/HighlightedRelativeLayout;

.field private final g:Lcom/twitter/app/dm/widget/DMAvatar;

.field private final h:Landroid/widget/TextView;

.field private final i:Landroid/widget/TextView;

.field private final j:Landroid/widget/TextView;

.field private final k:Landroid/widget/TextView;

.field private final l:Landroid/widget/ImageView;


# direct methods
.method private constructor <init>(Laps$a;)V
    .locals 4

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Laps;->a:J

    .line 73
    invoke-static {p1}, Laps$a;->a(Laps$a;)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Laps;->b:Landroid/content/Context;

    .line 74
    iget-object v0, p0, Laps;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Laps;->c:Landroid/content/res/Resources;

    .line 75
    invoke-static {p1}, Laps$a;->b(Laps$a;)Lcom/twitter/model/dms/q;

    move-result-object v0

    iput-object v0, p0, Laps;->d:Lcom/twitter/model/dms/q;

    .line 77
    invoke-static {p1}, Laps$a;->c(Laps$a;)Laps$c;

    move-result-object v0

    .line 78
    invoke-static {v0}, Laps$c;->a(Laps$c;)Lcom/twitter/internal/android/widget/HighlightedRelativeLayout;

    move-result-object v1

    iput-object v1, p0, Laps;->f:Lcom/twitter/internal/android/widget/HighlightedRelativeLayout;

    .line 79
    invoke-static {v0}, Laps$c;->b(Laps$c;)Lcom/twitter/app/dm/widget/DMAvatar;

    move-result-object v1

    iput-object v1, p0, Laps;->g:Lcom/twitter/app/dm/widget/DMAvatar;

    .line 80
    invoke-static {v0}, Laps$c;->c(Laps$c;)Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Laps;->h:Landroid/widget/TextView;

    .line 81
    invoke-static {v0}, Laps$c;->d(Laps$c;)Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Laps;->i:Landroid/widget/TextView;

    .line 82
    invoke-static {v0}, Laps$c;->e(Laps$c;)Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Laps;->j:Landroid/widget/TextView;

    .line 83
    invoke-static {v0}, Laps$c;->f(Laps$c;)Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Laps;->k:Landroid/widget/TextView;

    .line 84
    invoke-static {v0}, Laps$c;->g(Laps$c;)Landroid/widget/ImageView;

    move-result-object v0

    iput-object v0, p0, Laps;->l:Landroid/widget/ImageView;

    .line 86
    new-instance v0, Laps$b;

    iget-object v1, p0, Laps;->d:Lcom/twitter/model/dms/q;

    iget-object v1, v1, Lcom/twitter/model/dms/q;->l:Lcom/twitter/model/dms/m;

    iget-object v2, p0, Laps;->c:Landroid/content/res/Resources;

    const v3, 0x7f0a02ee

    .line 87
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Laps$b;-><init>(Lcom/twitter/model/dms/m;Ljava/lang/String;)V

    iput-object v0, p0, Laps;->e:Laps$b;

    .line 88
    return-void
.end method

.method synthetic constructor <init>(Laps$a;Laps$1;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1}, Laps;-><init>(Laps$a;)V

    return-void
.end method

.method static synthetic a(Laps;)Lcom/twitter/model/dms/q;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Laps;->d:Lcom/twitter/model/dms/q;

    return-object v0
.end method

.method static synthetic b(Laps;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Laps;->b:Landroid/content/Context;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 102
    iget-object v0, p0, Laps;->d:Lcom/twitter/model/dms/q;

    iget-boolean v0, v0, Lcom/twitter/model/dms/q;->j:Z

    if-eqz v0, :cond_0

    .line 103
    const v1, 0x7f020832

    .line 104
    iget-object v0, p0, Laps;->c:Landroid/content/res/Resources;

    const v3, 0x7f0a02a4

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 110
    :goto_0
    if-eq v1, v2, :cond_1

    .line 111
    iget-object v2, p0, Laps;->l:Landroid/widget/ImageView;

    iget-object v3, p0, Laps;->b:Landroid/content/Context;

    invoke-static {v3, v1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 112
    iget-object v1, p0, Laps;->l:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 113
    iget-object v1, p0, Laps;->l:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v0, p0, Laps;->l:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/util/ui/a;->a(Landroid/view/View;I)V

    .line 119
    :goto_1
    return-void

    .line 107
    :cond_0
    const/4 v0, 0x0

    move v1, v2

    goto :goto_0

    .line 116
    :cond_1
    iget-object v0, p0, Laps;->l:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 117
    iget-object v0, p0, Laps;->l:Landroid/widget/ImageView;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/twitter/util/ui/a;->a(Landroid/view/View;I)V

    goto :goto_1
.end method

.method private c()V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Laps;->g:Lcom/twitter/app/dm/widget/DMAvatar;

    new-instance v1, Laps$1;

    invoke-direct {v1, p0}, Laps$1;-><init>(Laps;)V

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/widget/DMAvatar;->setOneOnOneClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    iget-object v0, p0, Laps;->g:Lcom/twitter/app/dm/widget/DMAvatar;

    iget-object v1, p0, Laps;->d:Lcom/twitter/model/dms/q;

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/widget/DMAvatar;->setConversation(Lcom/twitter/model/dms/q;)V

    .line 133
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    const v3, 0x7f110177

    const v2, 0x7f11011b

    .line 136
    iget-object v0, p0, Laps;->d:Lcom/twitter/model/dms/q;

    iget-boolean v0, v0, Lcom/twitter/model/dms/q;->f:Z

    .line 137
    iget-object v1, p0, Laps;->f:Lcom/twitter/internal/android/widget/HighlightedRelativeLayout;

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/HighlightedRelativeLayout;->setHighlighted(Z)V

    .line 138
    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Laps;->h:Landroid/widget/TextView;

    iget-object v1, p0, Laps;->b:Landroid/content/Context;

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 140
    iget-object v0, p0, Laps;->i:Landroid/widget/TextView;

    iget-object v1, p0, Laps;->b:Landroid/content/Context;

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 145
    :goto_0
    return-void

    .line 142
    :cond_0
    iget-object v0, p0, Laps;->h:Landroid/widget/TextView;

    iget-object v1, p0, Laps;->b:Landroid/content/Context;

    invoke-static {v1, v3}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 143
    iget-object v0, p0, Laps;->i:Landroid/widget/TextView;

    iget-object v1, p0, Laps;->b:Landroid/content/Context;

    invoke-static {v1, v3}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private e()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 148
    new-instance v0, Lcom/twitter/library/dm/f$a;

    invoke-direct {v0}, Lcom/twitter/library/dm/f$a;-><init>()V

    .line 149
    invoke-direct {p0}, Laps;->h()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/dm/f$a;->a(I)Lcom/twitter/library/dm/f$a;

    move-result-object v0

    iget-object v2, p0, Laps;->e:Laps$b;

    .line 150
    invoke-static {v2}, Laps$b;->e(Laps$b;)Lcci;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/dm/f$a;->a(Lcci;)Lcom/twitter/library/dm/f$a;

    move-result-object v0

    iget-object v2, p0, Laps;->e:Laps$b;

    iget-boolean v2, v2, Laps$b;->a:Z

    .line 151
    invoke-virtual {v0, v2}, Lcom/twitter/library/dm/f$a;->b(Z)Lcom/twitter/library/dm/f$a;

    move-result-object v0

    iget-object v2, p0, Laps;->c:Landroid/content/res/Resources;

    .line 152
    invoke-virtual {v0, v2}, Lcom/twitter/library/dm/f$a;->a(Landroid/content/res/Resources;)Lcom/twitter/library/dm/f$a;

    move-result-object v0

    iget-object v2, p0, Laps;->e:Laps$b;

    .line 153
    invoke-static {v2}, Laps$b;->a(Laps$b;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/dm/f$a;->a(J)Lcom/twitter/library/dm/f$a;

    move-result-object v0

    iget-object v2, p0, Laps;->e:Laps$b;

    .line 154
    invoke-static {v2}, Laps$b;->d(Laps$b;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/dm/f$a;->b(Ljava/lang/String;)Lcom/twitter/library/dm/f$a;

    move-result-object v0

    iget-object v2, p0, Laps;->e:Laps$b;

    iget-object v2, v2, Laps$b;->b:Ljava/lang/String;

    .line 155
    invoke-virtual {v0, v2}, Lcom/twitter/library/dm/f$a;->a(Ljava/lang/String;)Lcom/twitter/library/dm/f$a;

    move-result-object v0

    iget-object v2, p0, Laps;->d:Lcom/twitter/model/dms/q;

    iget-boolean v2, v2, Lcom/twitter/model/dms/q;->h:Z

    .line 156
    invoke-virtual {v0, v2}, Lcom/twitter/library/dm/f$a;->c(Z)Lcom/twitter/library/dm/f$a;

    move-result-object v0

    iget-object v2, p0, Laps;->e:Laps$b;

    .line 157
    invoke-static {v2}, Laps$b;->c(Laps$b;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/dm/f$a;->b(I)Lcom/twitter/library/dm/f$a;

    move-result-object v0

    iget-object v2, p0, Laps;->e:Laps$b;

    .line 158
    invoke-static {v2}, Laps$b;->b(Laps$b;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/dm/f$a;->c(I)Lcom/twitter/library/dm/f$a;

    move-result-object v2

    iget-object v0, p0, Laps;->e:Laps$b;

    .line 159
    invoke-static {v0}, Laps$b;->a(Laps$b;)J

    move-result-wide v4

    iget-wide v6, p0, Laps;->a:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/twitter/library/dm/f$a;->a(Z)Lcom/twitter/library/dm/f$a;

    move-result-object v0

    .line 160
    invoke-virtual {v0}, Lcom/twitter/library/dm/f$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/dm/f;

    .line 161
    invoke-virtual {v0}, Lcom/twitter/library/dm/f;->a()Ljava/lang/CharSequence;

    move-result-object v0

    .line 162
    iget-object v2, p0, Laps;->h:Landroid/widget/TextView;

    sget v3, Lcni;->a:F

    invoke-virtual {v2, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 163
    iget-object v1, p0, Laps;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    return-void

    :cond_0
    move v0, v1

    .line 159
    goto :goto_0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 169
    iget-object v0, p0, Laps;->d:Lcom/twitter/model/dms/q;

    iget-wide v0, v0, Lcom/twitter/model/dms/q;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 170
    const-string/jumbo v1, ""

    .line 171
    const-string/jumbo v0, ""

    .line 176
    :goto_0
    iget-object v2, p0, Laps;->i:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v1, p0, Laps;->i:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 178
    return-void

    .line 173
    :cond_0
    iget-object v0, p0, Laps;->c:Landroid/content/res/Resources;

    iget-object v1, p0, Laps;->d:Lcom/twitter/model/dms/q;

    iget-wide v2, v1, Lcom/twitter/model/dms/q;->g:J

    invoke-static {v0, v2, v3}, Lcom/twitter/util/aa;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v1

    .line 174
    iget-object v0, p0, Laps;->c:Landroid/content/res/Resources;

    iget-object v2, p0, Laps;->d:Lcom/twitter/model/dms/q;

    iget-wide v2, v2, Lcom/twitter/model/dms/q;->g:J

    invoke-static {v0, v2, v3}, Lcom/twitter/util/aa;->b(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private g()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 181
    iget-object v1, p0, Laps;->j:Landroid/widget/TextView;

    sget v2, Lcni;->a:F

    invoke-virtual {v1, v0, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 182
    iget-object v1, p0, Laps;->j:Landroid/widget/TextView;

    new-instance v2, Lcom/twitter/library/dm/b;

    iget-object v3, p0, Laps;->d:Lcom/twitter/model/dms/q;

    iget-object v4, p0, Laps;->b:Landroid/content/Context;

    iget-wide v6, p0, Laps;->a:J

    invoke-direct {v2, v3, v4, v6, v7}, Lcom/twitter/library/dm/b;-><init>(Lcom/twitter/model/dms/q;Landroid/content/Context;J)V

    invoke-virtual {v2}, Lcom/twitter/library/dm/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    iget-object v1, p0, Laps;->d:Lcom/twitter/model/dms/q;

    iget-object v1, v1, Lcom/twitter/model/dms/q;->d:Ljava/lang/String;

    .line 185
    if-nez v1, :cond_0

    const/16 v0, 0x8

    .line 186
    :cond_0
    iget-object v2, p0, Laps;->k:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 187
    iget-object v0, p0, Laps;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    return-void
.end method

.method private h()I
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Laps;->d:Lcom/twitter/model/dms/q;

    iget-object v0, v0, Lcom/twitter/model/dms/q;->l:Lcom/twitter/model/dms/m;

    .line 193
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/model/dms/m;->c()Lcom/twitter/model/dms/c;

    move-result-object v0

    .line 194
    :goto_0
    instance-of v1, v0, Lcom/twitter/model/dms/e;

    if-eqz v1, :cond_1

    .line 195
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e;

    .line 196
    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 198
    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->u()Lcbx;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbz;

    invoke-virtual {v0}, Lcbz;->j()Lcax;

    move-result-object v0

    .line 199
    invoke-virtual {v0}, Lcax;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200
    const/4 v0, 0x1

    .line 204
    :goto_1
    return v0

    .line 193
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 204
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Laps;->c()V

    .line 92
    invoke-direct {p0}, Laps;->f()V

    .line 93
    invoke-direct {p0}, Laps;->g()V

    .line 94
    invoke-direct {p0}, Laps;->d()V

    .line 95
    invoke-direct {p0}, Laps;->e()V

    .line 96
    invoke-direct {p0}, Laps;->b()V

    .line 97
    return-void
.end method
