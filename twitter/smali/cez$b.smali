.class public Lcez$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcez;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcez;",
        "Lcez$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcez$a;
    .locals 1

    .prologue
    .line 74
    new-instance v0, Lcez$a;

    invoke-direct {v0}, Lcez$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcez$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 80
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/model/moments/r;->a(Ljava/lang/String;)Lcom/twitter/model/moments/r;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcez$a;->a(Lcom/twitter/model/moments/r;)Lcez$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/moments/e;->a:Lcom/twitter/util/serialization/l;

    .line 81
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/e;

    invoke-virtual {v1, v0}, Lcez$a;->a(Lcom/twitter/model/moments/e;)Lcez$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/moments/w;->a:Lcom/twitter/util/serialization/l;

    .line 82
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/w;

    invoke-virtual {v1, v0}, Lcez$a;->a(Lcom/twitter/model/moments/w;)Lcez$a;

    .line 83
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 70
    check-cast p2, Lcez$a;

    invoke-virtual {p0, p1, p2, p3}, Lcez$b;->a(Lcom/twitter/util/serialization/n;Lcez$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcez;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p2, Lcez;->b:Lcom/twitter/model/moments/r;

    invoke-virtual {v0}, Lcom/twitter/model/moments/r;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcez;->c:Lcom/twitter/model/moments/e;

    sget-object v2, Lcom/twitter/model/moments/e;->a:Lcom/twitter/util/serialization/l;

    .line 89
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcez;->d:Lcom/twitter/model/moments/w;

    sget-object v2, Lcom/twitter/model/moments/w;->a:Lcom/twitter/util/serialization/l;

    .line 90
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 91
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    check-cast p2, Lcez;

    invoke-virtual {p0, p1, p2}, Lcez$b;->a(Lcom/twitter/util/serialization/o;Lcez;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lcez$b;->a()Lcez$a;

    move-result-object v0

    return-object v0
.end method
