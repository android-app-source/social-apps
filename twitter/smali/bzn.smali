.class public Lbzn;
.super Lcom/twitter/metrics/m;
.source "Twttr"


# instance fields
.field protected a:Lcom/twitter/metrics/o;

.field protected b:J

.field private c:Lbzr;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;ZI)V
    .locals 2

    .prologue
    .line 38
    invoke-direct/range {p0 .. p7}, Lcom/twitter/metrics/m;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;ZI)V

    .line 41
    iget-object v0, p0, Lbzn;->a:Lcom/twitter/metrics/o;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lcom/twitter/metrics/o;

    const-string/jumbo v1, "FramerateValueTracker"

    invoke-virtual {p0, v1}, Lbzn;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/metrics/o;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbzn;->a:Lcom/twitter/metrics/o;

    .line 44
    :cond_0
    invoke-static {}, Lbzr;->a()Lbzr;

    move-result-object v0

    iput-object v0, p0, Lbzn;->c:Lbzr;

    .line 45
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/SharedPreferences$Editor;)V
    .locals 1

    .prologue
    .line 120
    invoke-super {p0, p1}, Lcom/twitter/metrics/m;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 121
    iget-object v0, p0, Lbzn;->a:Lcom/twitter/metrics/o;

    invoke-virtual {v0, p1}, Lcom/twitter/metrics/o;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 122
    return-void
.end method

.method protected a(Landroid/content/SharedPreferences;)V
    .locals 2

    .prologue
    .line 132
    invoke-super {p0, p1}, Lcom/twitter/metrics/m;->a(Landroid/content/SharedPreferences;)V

    .line 133
    new-instance v0, Lcom/twitter/metrics/o;

    const-string/jumbo v1, "FramerateValueTracker"

    invoke-virtual {p0, v1}, Lbzn;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/twitter/metrics/o;-><init>(Ljava/lang/String;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lbzn;->a:Lcom/twitter/metrics/o;

    .line 134
    return-void
.end method

.method protected aU_()Z
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x1

    return v0
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lbzn;->c:Lbzr;

    if-nez v0, :cond_0

    .line 89
    :goto_0
    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lbzn;->c:Lbzr;

    invoke-virtual {v0}, Lbzr;->b()V

    .line 88
    invoke-super {p0}, Lcom/twitter/metrics/m;->b()V

    goto :goto_0
.end method

.method protected b(Landroid/content/SharedPreferences$Editor;)V
    .locals 1

    .prologue
    .line 126
    invoke-super {p0, p1}, Lcom/twitter/metrics/m;->b(Landroid/content/SharedPreferences$Editor;)V

    .line 127
    iget-object v0, p0, Lbzn;->a:Lcom/twitter/metrics/o;

    invoke-virtual {v0, p1}, Lcom/twitter/metrics/o;->b(Landroid/content/SharedPreferences$Editor;)V

    .line 128
    return-void
.end method

.method protected br_()V
    .locals 1

    .prologue
    .line 66
    invoke-super {p0}, Lcom/twitter/metrics/m;->br_()V

    .line 67
    iget-object v0, p0, Lbzn;->a:Lcom/twitter/metrics/o;

    invoke-virtual {v0}, Lcom/twitter/metrics/o;->a()V

    .line 68
    invoke-virtual {p0}, Lbzn;->q()V

    .line 69
    return-void
.end method

.method protected c()V
    .locals 4

    .prologue
    .line 95
    iget-object v0, p0, Lbzn;->c:Lbzr;

    if-nez v0, :cond_0

    .line 105
    :goto_0
    return-void

    .line 98
    :cond_0
    invoke-super {p0}, Lcom/twitter/metrics/m;->c()V

    .line 99
    iget-object v0, p0, Lbzn;->c:Lbzr;

    invoke-virtual {v0}, Lbzr;->d()J

    move-result-wide v0

    iput-wide v0, p0, Lbzn;->b:J

    .line 100
    invoke-virtual {p0}, Lbzn;->aU_()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lbzn;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lbzn;->a:Lcom/twitter/metrics/o;

    iget-wide v2, p0, Lbzn;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/metrics/o;->a(J)V

    .line 102
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbzn;->b(Z)V

    .line 104
    :cond_1
    iget-object v0, p0, Lbzn;->c:Lbzr;

    invoke-virtual {v0}, Lbzr;->c()V

    goto :goto_0
.end method

.method public d()Ljava/lang/Long;
    .locals 4

    .prologue
    .line 114
    iget-object v0, p0, Lbzn;->a:Lcom/twitter/metrics/o;

    iget-wide v0, v0, Lcom/twitter/metrics/o;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lbzn;->a:Lcom/twitter/metrics/o;

    invoke-virtual {v0}, Lcom/twitter/metrics/o;->b()F

    move-result v0

    float-to-long v0, v0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method protected f()Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method
