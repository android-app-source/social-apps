.class public Lbxl;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Landroid/content/res/Resources;Lcom/twitter/model/core/Tweet;JLjava/util/List;Lbxk$a;I)Lbxi;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Lcom/twitter/model/core/Tweet;",
            "J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lbxk$a;",
            "I)",
            "Lbxi;"
        }
    .end annotation

    .prologue
    .line 24
    .line 25
    invoke-static/range {p1 .. p4}, Lbxd;->b(Lcom/twitter/model/core/Tweet;JLjava/util/Collection;)Ljava/util/List;

    move-result-object v2

    .line 27
    const-wide/16 v4, 0x0

    .line 28
    invoke-static {p0, v4, v5, v2}, Lbxp;->a(Landroid/content/res/Resources;JLjava/util/List;)Ljava/lang/String;

    move-result-object v12

    .line 32
    const/4 v2, 0x0

    .line 34
    move-wide/from16 v0, p2

    invoke-static {p1, v0, v1, v2}, Lbxd;->a(Lcom/twitter/model/core/Tweet;JLjava/util/Collection;)Ljava/util/Set;

    move-result-object v2

    .line 33
    invoke-static {v2}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v2

    .line 32
    invoke-static {v2}, Lcom/twitter/util/object/h;->a([J)[J

    move-result-object v4

    .line 35
    new-instance v2, Lbxk;

    iget-wide v6, p1, Lcom/twitter/model/core/Tweet;->G:J

    iget-wide v8, p1, Lcom/twitter/model/core/Tweet;->s:J

    move-object/from16 v3, p5

    move-object/from16 v5, p4

    move-wide/from16 v10, p2

    invoke-direct/range {v2 .. v11}, Lbxk;-><init>(Lbxk$a;[JLjava/util/List;JJJ)V

    .line 37
    new-instance v3, Lbxi;

    move/from16 v0, p6

    invoke-direct {v3, v12, v2, v0}, Lbxi;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;I)V

    return-object v3
.end method
