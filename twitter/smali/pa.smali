.class public Lpa;
.super Lbjb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpa$a;,
        Lpa$b;,
        Lpa$c;
    }
.end annotation


# instance fields
.field private final a:Lpg;

.field private final b:Lpg;

.field private final c:Lcom/twitter/library/av/playback/AVPlayer;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpa$c;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Z

.field private f:J

.field private final g:Z


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;Z)V
    .locals 6

    .prologue
    .line 56
    new-instance v4, Lpg;

    invoke-direct {v4}, Lpg;-><init>()V

    new-instance v5, Lpg;

    invoke-direct {v5}, Lpg;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lpa;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;ZLpg;Lpg;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;ZLpg;Lpg;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0, p2}, Lbjb;-><init>(Lcom/twitter/model/av/AVMedia;)V

    .line 62
    iput-boolean p3, p0, Lpa;->e:Z

    .line 63
    iput-object p1, p0, Lpa;->c:Lcom/twitter/library/av/playback/AVPlayer;

    .line 64
    invoke-direct {p0}, Lpa;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lpa;->d:Ljava/util/List;

    .line 65
    invoke-static {p2}, Lcom/twitter/model/av/b;->a(Lcom/twitter/model/av/AVMedia;)Z

    move-result v0

    iput-boolean v0, p0, Lpa;->g:Z

    .line 66
    iput-object p4, p0, Lpa;->a:Lpg;

    .line 67
    iput-object p5, p0, Lpa;->b:Lpg;

    .line 68
    return-void
.end method

.method private a()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lpa$c;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v3, 0x64

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 73
    new-instance v1, Lpa$b;

    const/16 v2, 0x19

    invoke-direct {v1, v2}, Lpa$b;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    new-instance v1, Lpa$b;

    const/16 v2, 0x32

    invoke-direct {v1, v2}, Lpa$b;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    new-instance v1, Lpa$b;

    const/16 v2, 0x4b

    invoke-direct {v1, v2}, Lpa$b;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    new-instance v1, Lpa$b;

    invoke-direct {v1, v3}, Lpa$b;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    new-instance v1, Lpa$a;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lpa$a;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    new-instance v1, Lpa$a;

    invoke-direct {v1, v3}, Lpa$a;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    return-object v0
.end method

.method private a(Lbsu;)V
    .locals 4

    .prologue
    .line 142
    invoke-static {p1}, Lbsr;->a(Lbsr$a;)Ljava/lang/String;

    move-result-object v0

    .line 143
    iget-object v1, p0, Lpa;->c:Lcom/twitter/library/av/playback/AVPlayer;

    const-string/jumbo v2, "video_session"

    new-instance v3, Lcom/twitter/library/av/m$a;

    invoke-direct {v3}, Lcom/twitter/library/av/m$a;-><init>()V

    .line 144
    invoke-virtual {v3, v0}, Lcom/twitter/library/av/m$a;->d(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    .line 143
    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Ljava/lang/String;Lcom/twitter/library/av/m$a;)V

    .line 145
    return-void
.end method

.method private b(Lcom/twitter/library/av/playback/AVDataSource;)I
    .locals 1

    .prologue
    .line 110
    invoke-static {p1}, Lbsu;->a(Lcom/twitter/library/av/playback/AVDataSource;)I

    move-result v0

    return v0
.end method


# virtual methods
.method a(Lcom/twitter/library/av/playback/AVDataSource;)Lbsu;
    .locals 4

    .prologue
    .line 114
    new-instance v0, Lbsu$a;

    invoke-direct {v0}, Lbsu$a;-><init>()V

    iget-boolean v1, p0, Lpa;->g:Z

    .line 116
    invoke-virtual {v0, v1}, Lbsu$a;->a(Z)Lbsu$a;

    move-result-object v0

    iget-object v1, p0, Lpa;->a:Lpg;

    .line 117
    invoke-virtual {v1}, Lpg;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lbsu$a;->b(J)Lbsu$a;

    move-result-object v0

    iget-wide v2, p0, Lpa;->f:J

    .line 118
    invoke-virtual {v0, v2, v3}, Lbsu$a;->a(J)Lbsu$a;

    move-result-object v0

    iget-object v1, p0, Lpa;->b:Lpg;

    .line 119
    invoke-virtual {v1}, Lpg;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lbsu$a;->g(J)Lbsu$a;

    move-result-object v1

    iget-object v0, p0, Lpa;->d:Ljava/util/List;

    const/4 v2, 0x0

    .line 120
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpa$c;

    invoke-interface {v0}, Lpa$c;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lbsu$a;->c(J)Lbsu$a;

    move-result-object v1

    iget-object v0, p0, Lpa;->d:Ljava/util/List;

    const/4 v2, 0x1

    .line 121
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpa$c;

    invoke-interface {v0}, Lpa$c;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lbsu$a;->d(J)Lbsu$a;

    move-result-object v1

    iget-object v0, p0, Lpa;->d:Ljava/util/List;

    const/4 v2, 0x2

    .line 122
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpa$c;

    invoke-interface {v0}, Lpa$c;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lbsu$a;->e(J)Lbsu$a;

    move-result-object v1

    iget-object v0, p0, Lpa;->d:Ljava/util/List;

    const/4 v2, 0x3

    .line 123
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpa$c;

    invoke-interface {v0}, Lpa$c;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lbsu$a;->f(J)Lbsu$a;

    move-result-object v1

    iget-object v0, p0, Lpa;->d:Ljava/util/List;

    const/4 v2, 0x4

    .line 124
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpa$c;

    invoke-interface {v0}, Lpa$c;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lbsu$a;->h(J)Lbsu$a;

    move-result-object v1

    iget-object v0, p0, Lpa;->d:Ljava/util/List;

    const/4 v2, 0x5

    .line 125
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpa$c;

    invoke-interface {v0}, Lpa$c;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lbsu$a;->i(J)Lbsu$a;

    move-result-object v0

    .line 127
    iget-boolean v1, p0, Lpa;->e:Z

    if-eqz v1, :cond_0

    .line 128
    invoke-direct {p0, p1}, Lpa;->b(Lcom/twitter/library/av/playback/AVDataSource;)I

    move-result v1

    invoke-virtual {v0, v1}, Lbsu$a;->a(I)Lbsu$a;

    .line 131
    :cond_0
    invoke-virtual {v0}, Lbsu$a;->a()Lbsu;

    move-result-object v0

    return-object v0
.end method

.method public processRelease(Lbjh;)V
    .locals 4
    .annotation runtime Lbiz;
        a = Lbjh;
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lpa;->a:Lpg;

    invoke-virtual {v0}, Lpg;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 104
    invoke-virtual {p0}, Lpa;->d()Lcom/twitter/library/av/m;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/av/m;->b:Lcom/twitter/library/av/playback/AVDataSource;

    invoke-virtual {p0, v0}, Lpa;->a(Lcom/twitter/library/av/playback/AVDataSource;)Lbsu;

    move-result-object v0

    invoke-direct {p0, v0}, Lpa;->a(Lbsu;)V

    .line 106
    :cond_0
    return-void
.end method

.method public processTick(Lbki;)V
    .locals 4
    .annotation runtime Lbiz;
        a = Lbki;
    .end annotation

    .prologue
    .line 87
    invoke-virtual {p0}, Lpa;->b()Lcom/twitter/library/av/m;

    move-result-object v1

    .line 88
    iget-object v0, p1, Lbki;->b:Lcom/twitter/library/av/playback/aa;

    iget-wide v2, v0, Lcom/twitter/library/av/playback/aa;->c:J

    iput-wide v2, p0, Lpa;->f:J

    .line 89
    iget-object v0, p0, Lpa;->a:Lpg;

    invoke-virtual {v0}, Lpg;->a()V

    .line 90
    iget-object v0, p0, Lpa;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpa$c;

    .line 91
    invoke-interface {v0, v1}, Lpa$c;->a(Lcom/twitter/library/av/m;)V

    goto :goto_0

    .line 93
    :cond_0
    iget-object v0, v1, Lcom/twitter/library/av/m;->o:Lbyf;

    invoke-interface {v0}, Lbyf;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    iget-object v0, p0, Lpa;->b:Lpg;

    invoke-virtual {v0}, Lpg;->a()V

    .line 96
    :cond_1
    return-void
.end method
