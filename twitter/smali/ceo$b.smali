.class Lceo$b;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lceo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lceo;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/util/serialization/i;-><init>(I)V

    .line 55
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lceo;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 76
    new-instance v0, Lceo$a;

    invoke-direct {v0}, Lceo$a;-><init>()V

    .line 77
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lceo$a;->a(Ljava/lang/String;)Lceo$a;

    move-result-object v1

    const-class v0, Lcom/twitter/model/moments/MomentPageType;

    .line 78
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->b(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/MomentPageType;

    invoke-virtual {v1, v0}, Lceo$a;->a(Lcom/twitter/model/moments/MomentPageType;)Lceo$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/moments/n;->a:Lcom/twitter/util/serialization/l;

    .line 79
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/n;

    invoke-virtual {v1, v0}, Lceo$a;->a(Lcom/twitter/model/moments/n;)Lceo$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/moments/e;->a:Lcom/twitter/util/serialization/l;

    .line 80
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->b(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/e;

    invoke-virtual {v1, v0}, Lceo$a;->a(Lcom/twitter/model/moments/e;)Lceo$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/moments/w;->a:Lcom/twitter/util/serialization/l;

    .line 81
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/w;

    invoke-virtual {v1, v0}, Lceo$a;->a(Lcom/twitter/model/moments/w;)Lceo$a;

    move-result-object v1

    sget-object v0, Lcem;->a:Lcom/twitter/util/serialization/l;

    .line 82
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcem;

    invoke-virtual {v1, v0}, Lceo$a;->a(Lcem;)Lceo$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/moments/l;->a:Lcom/twitter/util/serialization/l;

    .line 83
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/l;

    invoke-virtual {v1, v0}, Lceo$a;->a(Lcom/twitter/model/moments/l;)Lceo$a;

    move-result-object v0

    .line 84
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lceo$a;->a(J)Lceo$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/model/moments/o;->a:Lcom/twitter/model/moments/o$a;

    .line 85
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/o;

    invoke-virtual {v1, v0}, Lceo$a;->a(Lcom/twitter/model/moments/o;)Lceo$a;

    move-result-object v1

    .line 86
    const/4 v0, 0x1

    if-lt p2, v0, :cond_0

    .line 87
    sget-object v0, Lcom/twitter/model/moments/s;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/s;

    invoke-virtual {v1, v0}, Lceo$a;->a(Lcom/twitter/model/moments/s;)Lceo$a;

    .line 89
    :cond_0
    invoke-virtual {v1}, Lceo$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lceo;

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lceo;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p2, Lceo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lceo;->c:Lcom/twitter/model/moments/MomentPageType;

    const-class v2, Lcom/twitter/model/moments/MomentPageType;

    .line 61
    invoke-static {v2}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lceo;->d:Lcom/twitter/model/moments/n;

    sget-object v2, Lcom/twitter/model/moments/n;->a:Lcom/twitter/util/serialization/l;

    .line 62
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lceo;->e:Lcom/twitter/model/moments/e;

    sget-object v2, Lcom/twitter/model/moments/e;->a:Lcom/twitter/util/serialization/l;

    .line 63
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lceo;->f:Lcom/twitter/model/moments/w;

    sget-object v2, Lcom/twitter/model/moments/w;->a:Lcom/twitter/util/serialization/l;

    .line 64
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lceo;->g:Lcem;

    sget-object v2, Lcem;->a:Lcom/twitter/util/serialization/l;

    .line 65
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lceo;->h:Lcom/twitter/model/moments/l;

    sget-object v2, Lcom/twitter/model/moments/l;->a:Lcom/twitter/util/serialization/l;

    .line 66
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lceo;->k:J

    .line 67
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lceo;->i:Lcom/twitter/model/moments/o;

    sget-object v2, Lcom/twitter/model/moments/o;->a:Lcom/twitter/model/moments/o$a;

    .line 68
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lceo;->j:Lcom/twitter/model/moments/s;

    sget-object v2, Lcom/twitter/model/moments/s;->a:Lcom/twitter/util/serialization/l;

    .line 69
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 70
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    check-cast p2, Lceo;

    invoke-virtual {p0, p1, p2}, Lceo$b;->a(Lcom/twitter/util/serialization/o;Lceo;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-virtual {p0, p1, p2}, Lceo$b;->a(Lcom/twitter/util/serialization/n;I)Lceo;

    move-result-object v0

    return-object v0
.end method
