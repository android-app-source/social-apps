.class abstract Lajf;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lajj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/model/core/d;",
        ">",
        "Ljava/lang/Object;",
        "Lajj;"
    }
.end annotation


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Lcom/twitter/model/core/Tweet;

.field protected final c:Lcom/twitter/model/core/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/d;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/model/core/Tweet;",
            "TT;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lajf;->a:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Lajf;->b:Lcom/twitter/model/core/Tweet;

    .line 32
    iput-object p3, p0, Lajf;->c:Lcom/twitter/model/core/d;

    .line 33
    iput-object p4, p0, Lajf;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 34
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lajf;->c()Lbsq;

    move-result-object v0

    .line 55
    if-eqz v0, :cond_0

    .line 56
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 59
    :cond_0
    invoke-direct {p0}, Lajf;->g()Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 60
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 61
    return-void
.end method

.method private g()Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 6

    .prologue
    .line 65
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 66
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 68
    iget-object v0, p0, Lajf;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lajf;->b:Lcom/twitter/model/core/Tweet;

    const/4 v3, 0x0

    invoke-static {v2, v0, v1, v3}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lajf;->b:Lcom/twitter/model/core/Tweet;

    iget-object v1, p0, Lajf;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {p0, v0, v1}, Lajf;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Ljava/lang/String;

    move-result-object v0

    .line 71
    invoke-virtual {p0}, Lajf;->d()Ljava/lang/String;

    move-result-object v1

    .line 72
    invoke-virtual {p0}, Lajf;->e()Lcom/twitter/analytics/model/ScribeItem;

    move-result-object v3

    .line 74
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 75
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 76
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lajf;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 77
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 74
    return-object v0
.end method


# virtual methods
.method abstract a(Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Ljava/lang/String;
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 49
    invoke-virtual {p0}, Lajf;->b()V

    .line 50
    invoke-direct {p0}, Lajf;->f()V

    .line 51
    return-void
.end method

.method abstract b()V
.end method

.method abstract c()Lbsq;
.end method

.method abstract d()Ljava/lang/String;
.end method

.method abstract e()Lcom/twitter/analytics/model/ScribeItem;
.end method
