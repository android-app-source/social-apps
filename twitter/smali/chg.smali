.class public Lchg;
.super Lcho;
.source "Twttr"


# instance fields
.field private final a:Lcgv;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLcgv;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 20
    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcho;-><init>(Ljava/lang/String;JLcom/twitter/model/timeline/r;Lchj;)V

    .line 21
    iput-object p4, p0, Lchg;->a:Lcgv;

    .line 22
    return-void
.end method

.method private a(Lcgx;Lcia;)Lcom/twitter/model/timeline/ah;
    .locals 4

    .prologue
    .line 93
    new-instance v0, Lcom/twitter/model/timeline/ah$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/ah$a;-><init>()V

    iget-object v1, p2, Lcia;->b:Ljava/lang/String;

    .line 94
    invoke-virtual {p1, v1}, Lcgx;->a(Ljava/lang/String;)Lcom/twitter/model/core/ac;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/ah$a;->a(Lcom/twitter/model/core/ac;)Lcom/twitter/model/timeline/ah$a;

    move-result-object v0

    iget-wide v2, p0, Lchg;->c:J

    .line 95
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/timeline/ah$a;->a(J)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah$a;

    iget-object v1, p0, Lchg;->b:Ljava/lang/String;

    .line 96
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/ah$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah$a;

    .line 97
    invoke-virtual {v0}, Lcom/twitter/model/timeline/ah$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah;

    .line 93
    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;J)Lchg;
    .locals 2

    .prologue
    .line 29
    new-instance v0, Lchg;

    iget-object v1, p0, Lchg;->a:Lcgv;

    invoke-direct {v0, p1, p2, p3, v1}, Lchg;-><init>(Ljava/lang/String;JLcgv;)V

    return-object v0
.end method

.method public a(Lcgx;Lchc;)Lcom/twitter/model/timeline/x;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 36
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v5

    .line 37
    const/4 v3, 0x1

    .line 38
    iget-object v0, p0, Lchg;->a:Lcgv;

    iget-object v0, v0, Lcgv;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    move v4, v2

    .line 39
    :goto_0
    if-ge v4, v6, :cond_4

    .line 40
    iget-object v0, p0, Lchg;->a:Lcgv;

    iget-object v0, v0, Lcgv;->a:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgt;

    .line 41
    instance-of v1, v0, Lcgw;

    if-eqz v1, :cond_0

    .line 42
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgw;

    .line 43
    iget-object v1, v0, Lcgw;->a:Lcia;

    invoke-direct {p0, p1, v1}, Lchg;->a(Lcgx;Lcia;)Lcom/twitter/model/timeline/ah;

    move-result-object v1

    .line 44
    if-eqz v1, :cond_1

    .line 45
    invoke-virtual {v5, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 47
    iget-object v1, v0, Lcgw;->b:Lcom/twitter/model/timeline/ar;

    if-eqz v1, :cond_0

    .line 48
    new-instance v1, Lcom/twitter/model/timeline/aq$a;

    invoke-direct {v1}, Lcom/twitter/model/timeline/aq$a;-><init>()V

    iget-wide v8, p0, Lchg;->c:J

    .line 49
    invoke-virtual {v1, v8, v9}, Lcom/twitter/model/timeline/aq$a;->a(J)Lcom/twitter/model/timeline/y$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/timeline/aq$a;

    iget-object v7, p0, Lchg;->b:Ljava/lang/String;

    .line 50
    invoke-virtual {v1, v7}, Lcom/twitter/model/timeline/aq$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/timeline/aq$a;

    iget-object v0, v0, Lcgw;->b:Lcom/twitter/model/timeline/ar;

    .line 51
    invoke-virtual {v1, v0}, Lcom/twitter/model/timeline/aq$a;->a(Lcom/twitter/model/timeline/ar;)Lcom/twitter/model/timeline/aq$a;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/twitter/model/timeline/aq$a;->r()Ljava/lang/Object;

    move-result-object v0

    .line 48
    invoke-virtual {v5, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 39
    :cond_0
    :goto_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 57
    :cond_1
    if-nez v4, :cond_3

    move v0, v2

    .line 75
    :goto_2
    if-eqz v0, :cond_2

    iget-object v0, p0, Lchg;->a:Lcgv;

    iget-object v0, v0, Lcgv;->b:Lcom/twitter/model/timeline/ar;

    if-eqz v0, :cond_2

    .line 76
    new-instance v0, Lcom/twitter/model/timeline/aq$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/aq$a;-><init>()V

    iget-wide v2, p0, Lchg;->c:J

    .line 77
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/timeline/aq$a;->a(J)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/aq$a;

    iget-object v1, p0, Lchg;->b:Ljava/lang/String;

    .line 78
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/aq$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/aq$a;

    iget-object v1, p0, Lchg;->a:Lcgv;

    iget-object v1, v1, Lcgv;->b:Lcom/twitter/model/timeline/ar;

    .line 79
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/aq$a;->a(Lcom/twitter/model/timeline/ar;)Lcom/twitter/model/timeline/aq$a;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lcom/twitter/model/timeline/aq$a;->r()Ljava/lang/Object;

    move-result-object v0

    .line 76
    invoke-virtual {v5, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 83
    :cond_2
    new-instance v1, Lcom/twitter/model/timeline/x$a;

    invoke-direct {v1}, Lcom/twitter/model/timeline/x$a;-><init>()V

    .line 84
    invoke-virtual {v5}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/twitter/model/timeline/x$a;->a(Ljava/util/List;)Lcom/twitter/model/timeline/x$a;

    move-result-object v0

    iget-wide v2, p0, Lchg;->c:J

    .line 85
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/timeline/x$a;->a(J)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/x$a;

    iget-object v1, p0, Lchg;->b:Ljava/lang/String;

    .line 86
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/x$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/x$a;

    .line 87
    invoke-virtual {v0}, Lcom/twitter/model/timeline/x$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/x;

    .line 83
    return-object v0

    .line 62
    :cond_3
    add-int/lit8 v1, v6, -0x1

    if-eq v4, v1, :cond_0

    .line 64
    new-instance v1, Lcom/twitter/model/timeline/as$a;

    invoke-direct {v1}, Lcom/twitter/model/timeline/as$a;-><init>()V

    iget-wide v8, p0, Lchg;->c:J

    .line 65
    invoke-virtual {v1, v8, v9}, Lcom/twitter/model/timeline/as$a;->a(J)Lcom/twitter/model/timeline/y$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/timeline/as$a;

    iget-object v7, p0, Lchg;->b:Ljava/lang/String;

    .line 66
    invoke-virtual {v1, v7}, Lcom/twitter/model/timeline/as$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/timeline/as$a;

    iget-object v0, v0, Lcgw;->a:Lcia;

    iget-object v0, v0, Lcia;->b:Ljava/lang/String;

    .line 67
    invoke-virtual {v1, v0}, Lcom/twitter/model/timeline/as$a;->e(Ljava/lang/String;)Lcom/twitter/model/timeline/as$a;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lcom/twitter/model/timeline/as$a;->r()Ljava/lang/Object;

    move-result-object v0

    .line 64
    invoke-virtual {v5, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto/16 :goto_1

    :cond_4
    move v0, v3

    goto :goto_2
.end method

.method public synthetic b(Lcgx;Lchc;)Lcom/twitter/model/timeline/y;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0, p1, p2}, Lchg;->a(Lcgx;Lchc;)Lcom/twitter/model/timeline/x;

    move-result-object v0

    return-object v0
.end method
