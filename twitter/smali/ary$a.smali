.class public final Lary$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:Lano;

.field private b:Lamu;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lary$1;)V
    .locals 0

    .prologue
    .line 631
    invoke-direct {p0}, Lary$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lary$a;)Lamu;
    .locals 1

    .prologue
    .line 631
    iget-object v0, p0, Lary$a;->b:Lamu;

    return-object v0
.end method

.method static synthetic b(Lary$a;)Lano;
    .locals 1

    .prologue
    .line 631
    iget-object v0, p0, Lary$a;->a:Lano;

    return-object v0
.end method


# virtual methods
.method public a(Lamu;)Lary$a;
    .locals 1

    .prologue
    .line 655
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamu;

    iput-object v0, p0, Lary$a;->b:Lamu;

    .line 656
    return-object p0
.end method

.method public a(Lano;)Lary$a;
    .locals 1

    .prologue
    .line 650
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lano;

    iput-object v0, p0, Lary$a;->a:Lano;

    .line 651
    return-object p0
.end method

.method public a()Larz;
    .locals 3

    .prologue
    .line 639
    iget-object v0, p0, Lary$a;->a:Lano;

    if-nez v0, :cond_0

    .line 640
    new-instance v0, Lano;

    invoke-direct {v0}, Lano;-><init>()V

    iput-object v0, p0, Lary$a;->a:Lano;

    .line 642
    :cond_0
    iget-object v0, p0, Lary$a;->b:Lamu;

    if-nez v0, :cond_1

    .line 643
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lamu;

    .line 644
    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 646
    :cond_1
    new-instance v0, Lary;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lary;-><init>(Lary$a;Lary$1;)V

    return-object v0
.end method
