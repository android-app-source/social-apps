.class public Lbsg;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;


# instance fields
.field private final a:Z

.field private final b:Z

.field private final c:Lcom/twitter/util/connectivity/TwRadioType;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {p1}, Lbsg;->a(Landroid/content/Context;)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 26
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lbsg;->a:Z

    .line 27
    iget-boolean v0, p0, Lbsg;->a:Z

    if-nez v0, :cond_2

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_1
    iput-boolean v1, p0, Lbsg;->b:Z

    .line 28
    iget-boolean v0, p0, Lbsg;->a:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lbsg;->b:Z

    if-eqz v0, :cond_3

    :cond_0
    invoke-static {p1}, Lbsg;->b(Landroid/content/Context;)Lcom/twitter/util/connectivity/TwRadioType;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lbsg;->c:Lcom/twitter/util/connectivity/TwRadioType;

    .line 29
    return-void

    :cond_1
    move v0, v2

    .line 26
    goto :goto_0

    :cond_2
    move v1, v2

    .line 27
    goto :goto_1

    .line 28
    :cond_3
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->a:Lcom/twitter/util/connectivity/TwRadioType;

    goto :goto_2
.end method

.method private static a(Landroid/content/Context;)Landroid/net/NetworkInfo;
    .locals 1

    .prologue
    .line 38
    const-string/jumbo v0, "connectivity"

    .line 39
    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 40
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    return-object v0
.end method

.method private static a(I)Lcom/twitter/util/connectivity/TwRadioType;
    .locals 1

    .prologue
    .line 72
    packed-switch p0, :pswitch_data_0

    .line 89
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->b:Lcom/twitter/util/connectivity/TwRadioType;

    :goto_0
    return-object v0

    .line 73
    :pswitch_0
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->f:Lcom/twitter/util/connectivity/TwRadioType;

    goto :goto_0

    .line 74
    :pswitch_1
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->m:Lcom/twitter/util/connectivity/TwRadioType;

    goto :goto_0

    .line 75
    :pswitch_2
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->c:Lcom/twitter/util/connectivity/TwRadioType;

    goto :goto_0

    .line 76
    :pswitch_3
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->i:Lcom/twitter/util/connectivity/TwRadioType;

    goto :goto_0

    .line 77
    :pswitch_4
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->d:Lcom/twitter/util/connectivity/TwRadioType;

    goto :goto_0

    .line 78
    :pswitch_5
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->g:Lcom/twitter/util/connectivity/TwRadioType;

    goto :goto_0

    .line 79
    :pswitch_6
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->e:Lcom/twitter/util/connectivity/TwRadioType;

    goto :goto_0

    .line 80
    :pswitch_7
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->j:Lcom/twitter/util/connectivity/TwRadioType;

    goto :goto_0

    .line 81
    :pswitch_8
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->n:Lcom/twitter/util/connectivity/TwRadioType;

    goto :goto_0

    .line 82
    :pswitch_9
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->h:Lcom/twitter/util/connectivity/TwRadioType;

    goto :goto_0

    .line 83
    :pswitch_a
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->l:Lcom/twitter/util/connectivity/TwRadioType;

    goto :goto_0

    .line 84
    :pswitch_b
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->k:Lcom/twitter/util/connectivity/TwRadioType;

    goto :goto_0

    .line 85
    :pswitch_c
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->p:Lcom/twitter/util/connectivity/TwRadioType;

    goto :goto_0

    .line 86
    :pswitch_d
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->q:Lcom/twitter/util/connectivity/TwRadioType;

    goto :goto_0

    .line 87
    :pswitch_e
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->o:Lcom/twitter/util/connectivity/TwRadioType;

    goto :goto_0

    .line 88
    :pswitch_f
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->b:Lcom/twitter/util/connectivity/TwRadioType;

    goto :goto_0

    .line 72
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_f
        :pswitch_2
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_9
        :pswitch_3
        :pswitch_7
        :pswitch_b
        :pswitch_a
        :pswitch_1
        :pswitch_8
        :pswitch_e
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method private static b(Landroid/content/Context;)Lcom/twitter/util/connectivity/TwRadioType;
    .locals 2

    .prologue
    .line 53
    invoke-static {p0}, Lbsg;->a(Landroid/content/Context;)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 54
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 55
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->s:Lcom/twitter/util/connectivity/TwRadioType;

    .line 61
    :goto_0
    return-object v0

    .line 59
    :cond_0
    const-string/jumbo v0, "phone"

    .line 60
    const-string/jumbo v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 61
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v0

    invoke-static {v0}, Lbsg;->a(I)Lcom/twitter/util/connectivity/TwRadioType;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/twitter/util/connectivity/TwRadioType;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lbsg;->c:Lcom/twitter/util/connectivity/TwRadioType;

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lbsg;->a:Z

    return v0
.end method
