.class public Lbaq;
.super Lcom/twitter/library/service/s;
.source "Twttr"


# instance fields
.field protected final a:Lcom/twitter/async/service/b;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/twitter/library/api/y;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lbap;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/service/s;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 32
    iput-object p2, p0, Lbaq;->b:Ljava/lang/String;

    .line 33
    const/16 v0, 0x36

    invoke-static {v0}, Lcom/twitter/library/api/y;->a(I)Lcom/twitter/library/api/y;

    move-result-object v0

    iput-object v0, p0, Lbaq;->c:Lcom/twitter/library/api/y;

    .line 35
    new-instance v0, Lcom/twitter/async/service/b;

    invoke-direct {v0}, Lcom/twitter/async/service/b;-><init>()V

    iput-object v0, p0, Lbaq;->a:Lcom/twitter/async/service/b;

    .line 36
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lbaq;->g:Ljava/lang/String;

    return-object v0
.end method

.method protected a_(Lcom/twitter/library/service/u;)V
    .locals 4

    .prologue
    .line 45
    iget-object v0, p0, Lbaq;->q:Lcom/twitter/library/network/ab;

    iget-object v0, v0, Lcom/twitter/library/network/ab;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "guest"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "activate"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/ab;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    .line 46
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 47
    iget-object v1, p0, Lbaq;->p:Landroid/content/Context;

    invoke-virtual {p0, v1, v0}, Lbaq;->a(Landroid/content/Context;Ljava/lang/CharSequence;)Lcom/twitter/library/network/k;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 48
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/k;

    move-result-object v0

    iget-object v1, p0, Lbaq;->c:Lcom/twitter/library/api/y;

    .line 49
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/j;)Lcom/twitter/library/network/k;

    move-result-object v0

    iget-object v1, p0, Lbaq;->a:Lcom/twitter/async/service/b;

    .line 50
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Lcom/twitter/async/service/b;)Lcom/twitter/library/network/k;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lcom/twitter/library/network/k;->a()Lcom/twitter/network/HttpOperation;

    move-result-object v1

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Bearer "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lbaq;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 54
    const-string/jumbo v2, "Authorization"

    invoke-static {v1, v2, v0}, Lcom/twitter/library/network/ab;->a(Lcom/twitter/network/HttpOperation;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    invoke-virtual {v1}, Lcom/twitter/network/HttpOperation;->c()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lbaq;->c:Lcom/twitter/library/api/y;

    invoke-virtual {v0}, Lcom/twitter/library/api/y;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lbaq;->g:Ljava/lang/String;

    .line 58
    :cond_0
    invoke-virtual {p1, v1}, Lcom/twitter/library/service/u;->a(Lcom/twitter/network/HttpOperation;)V

    .line 59
    return-void
.end method

.method public m()Lcom/twitter/async/service/b;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lbaq;->a:Lcom/twitter/async/service/b;

    return-object v0
.end method
