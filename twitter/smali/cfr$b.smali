.class public final Lcfr$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcfr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcfr;",
        "Lcfr$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcfr$a;
    .locals 1

    .prologue
    .line 107
    new-instance v0, Lcfr$a;

    invoke-direct {v0}, Lcfr$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcfr$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 113
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    invoke-virtual {p2, v0}, Lcfr$a;->a(I)Lcfr$a;

    move-result-object v0

    .line 114
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcfr$a;->b(Ljava/lang/String;)Lcfr$a;

    .line 115
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 103
    check-cast p2, Lcfr$a;

    invoke-virtual {p0, p1, p2, p3}, Lcfr$b;->a(Lcom/twitter/util/serialization/n;Lcfr$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcfr;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    iget v0, p2, Lcfr;->g:I

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcfr;->h:Ljava/lang/String;

    .line 121
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 122
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 103
    check-cast p2, Lcfr;

    invoke-virtual {p0, p1, p2}, Lcfr$b;->a(Lcom/twitter/util/serialization/o;Lcfr;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lcfr$b;->a()Lcfr$a;

    move-result-object v0

    return-object v0
.end method
