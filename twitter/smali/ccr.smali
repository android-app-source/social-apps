.class public final Lccr;
.super Lcck;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lccr$b;,
        Lccr$a;
    }
.end annotation


# static fields
.field public static final b:Lccr$b;


# instance fields
.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lccp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lccr$b;

    invoke-direct {v0}, Lccr$b;-><init>()V

    sput-object v0, Lccr;->b:Lccr$b;

    return-void
.end method

.method private constructor <init>(Lccr$a;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcck;-><init>(Lcck$a;)V

    .line 24
    invoke-static {p1}, Lccr$a;->a(Lccr$a;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lccr;->c:Ljava/util/List;

    .line 25
    return-void
.end method

.method synthetic constructor <init>(Lccr$a;Lccr$1;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lccr;-><init>(Lccr$a;)V

    return-void
.end method

.method private a(Lccr;)Z
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lccr;->c:Ljava/util/List;

    iget-object v1, p1, Lccr;->c:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    const-string/jumbo v0, "options"

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcck;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lccr;

    if-eqz v0, :cond_1

    check-cast p1, Lccr;

    .line 37
    invoke-direct {p0, p1}, Lccr;->a(Lccr;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 36
    :goto_0
    return v0

    .line 37
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 42
    invoke-super {p0}, Lcck;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lccr;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
