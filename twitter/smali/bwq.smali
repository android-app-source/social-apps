.class public Lbwq;
.super Lbwu;
.source "Twttr"


# direct methods
.method public constructor <init>(Lbwu$a;)V
    .locals 2

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lbwu;-><init>(Lbwu$a;)V

    .line 14
    const/4 v0, 0x3

    iput v0, p0, Lbwq;->b:I

    .line 16
    invoke-virtual {p0}, Lbwq;->d()Lbwu$a;

    move-result-object v1

    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lbwu$a;->setLabelOnLeft(Z)V

    .line 17
    return-void

    .line 16
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/twitter/model/core/Tweet;Lbwt;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 27
    iget-wide v2, p1, Lcom/twitter/model/core/Tweet;->s:J

    invoke-virtual {p2}, Lbwt;->a()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-object v1, p2, Lbwt;->c:Lcom/twitter/model/core/TwitterUser;

    .line 28
    invoke-static {v1}, Lbad;->a(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 29
    :goto_0
    if-eqz v1, :cond_1

    :goto_1
    return v0

    :cond_0
    move v1, v0

    .line 28
    goto :goto_0

    .line 29
    :cond_1
    const/4 v0, 0x3

    goto :goto_1
.end method

.method public a()Lcom/twitter/model/core/TweetActionType;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->o:Lcom/twitter/model/core/TweetActionType;

    return-object v0
.end method

.method protected a(Lbwt;I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 40
    if-lez p2, :cond_0

    const-string/jumbo v0, "%1$d%%"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Lcom/twitter/model/core/Tweet;Lbwt;)I
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return v0
.end method
