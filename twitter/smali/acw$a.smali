.class Lacw$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lajr;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lacw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lajr",
        "<",
        "Ladg;",
        "Lajq",
        "<",
        "Ladg;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Lcom/twitter/android/moments/ui/maker/navigation/ah;

.field private final d:Lcom/twitter/android/moments/ui/maker/q;

.field private final e:Lcom/twitter/android/util/q;

.field private final f:J


# direct methods
.method constructor <init>(Landroid/app/Activity;Landroid/view/LayoutInflater;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/q;Lcom/twitter/android/util/q;J)V
    .locals 0

    .prologue
    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    iput-object p1, p0, Lacw$a;->a:Landroid/app/Activity;

    .line 175
    iput-object p2, p0, Lacw$a;->b:Landroid/view/LayoutInflater;

    .line 176
    iput-object p3, p0, Lacw$a;->c:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    .line 177
    iput-object p4, p0, Lacw$a;->d:Lcom/twitter/android/moments/ui/maker/q;

    .line 178
    iput-object p5, p0, Lacw$a;->e:Lcom/twitter/android/util/q;

    .line 179
    iput-wide p6, p0, Lacw$a;->f:J

    .line 180
    return-void
.end method


# virtual methods
.method public a(Ladg;)I
    .locals 1

    .prologue
    .line 208
    instance-of v0, p1, Ladf;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 157
    check-cast p1, Ladg;

    invoke-virtual {p0, p1}, Lacw$a;->a(Ladg;)I

    move-result v0

    return v0
.end method

.method public b(Landroid/view/ViewGroup;Lcjs;I)Lajq;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Lcjs;",
            "I)",
            "Lajq",
            "<",
            "Ladg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    packed-switch p3, :pswitch_data_0

    .line 201
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown view type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :pswitch_0
    iget-object v0, p0, Lacw$a;->b:Landroid/view/LayoutInflater;

    .line 189
    invoke-static {v0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/m;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/m;

    move-result-object v1

    .line 191
    new-instance v0, Lacu;

    iget-object v2, p0, Lacw$a;->d:Lcom/twitter/android/moments/ui/maker/q;

    iget-object v3, p0, Lacw$a;->e:Lcom/twitter/android/util/q;

    iget-object v4, p0, Lacw$a;->c:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    invoke-direct {v0, v1, v2, v3, v4}, Lacu;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/m;Lcom/twitter/android/moments/ui/maker/q;Lcom/twitter/android/util/q;Lcom/twitter/android/moments/ui/maker/navigation/ah;)V

    .line 197
    :goto_0
    return-object v0

    .line 195
    :pswitch_1
    iget-object v0, p0, Lacw$a;->b:Landroid/view/LayoutInflater;

    .line 196
    invoke-static {v0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/n;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/n;

    move-result-object v2

    .line 197
    iget-object v0, p0, Lacw$a;->a:Landroid/app/Activity;

    iget-object v1, p0, Lacw$a;->c:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    iget-object v3, p0, Lacw$a;->d:Lcom/twitter/android/moments/ui/maker/q;

    iget-wide v4, p0, Lacw$a;->f:J

    invoke-static/range {v0 .. v5}, Lacv;->a(Landroid/app/Activity;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/viewdelegate/n;Lcom/twitter/android/moments/ui/maker/q;J)Lacv;

    move-result-object v0

    goto :goto_0

    .line 186
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
