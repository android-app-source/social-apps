.class public Lacx;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lacx$a;
    }
.end annotation


# instance fields
.field private final a:Lwy;

.field private final b:Lcom/twitter/android/moments/ui/maker/viewdelegate/p;

.field private final c:Lcom/twitter/android/moments/ui/maker/navigation/ah;

.field private final d:Lzw;


# direct methods
.method constructor <init>(Lwy;Lcom/twitter/android/moments/ui/maker/viewdelegate/p;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/viewmodels/k;Lcom/twitter/model/moments/Moment;Lzw;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lacx;->a:Lwy;

    .line 38
    iput-object p2, p0, Lacx;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/p;

    .line 39
    iput-object p3, p0, Lacx;->c:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    .line 40
    iput-object p6, p0, Lacx;->d:Lzw;

    .line 41
    invoke-direct {p0, p4, p5}, Lacx;->a(Lcom/twitter/android/moments/viewmodels/k;Lcom/twitter/model/moments/Moment;)V

    .line 42
    return-void
.end method

.method public static a(Landroid/app/Activity;Landroid/view/ViewGroup;Lwy;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/viewmodels/k;Lcom/twitter/model/moments/Moment;)Lacx;
    .locals 7

    .prologue
    .line 125
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-static {v0, p1, p0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/app/Activity;)Lcom/twitter/android/moments/ui/maker/viewdelegate/p;

    move-result-object v2

    .line 127
    iget-wide v0, p5, Lcom/twitter/model/moments/Moment;->b:J

    .line 128
    invoke-static {p0, v0, v1}, Lzw;->a(Landroid/content/Context;J)Lzw;

    move-result-object v6

    .line 129
    new-instance v0, Lacx;

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lacx;-><init>(Lwy;Lcom/twitter/android/moments/ui/maker/viewdelegate/p;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/viewmodels/k;Lcom/twitter/model/moments/Moment;Lzw;)V

    return-object v0
.end method

.method static synthetic a(Lacx;)Lcom/twitter/android/moments/ui/maker/viewdelegate/p;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lacx;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/p;

    return-object v0
.end method

.method static synthetic a(Lacx;Lcom/twitter/model/moments/Moment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lacx;->b(Lcom/twitter/model/moments/Moment;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lacx;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lacx;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/twitter/android/moments/viewmodels/k;Lcom/twitter/model/moments/Moment;)V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lacx;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/p;

    new-instance v1, Lacx$1;

    invoke-direct {v1, p0, p2}, Lacx$1;-><init>(Lacx;Lcom/twitter/model/moments/Moment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->a(Lacx$a;)V

    .line 61
    iget-object v0, p0, Lacx;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/p;

    iget-object v1, p2, Lcom/twitter/model/moments/Moment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->b(Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lacx;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/p;

    iget-object v1, p2, Lcom/twitter/model/moments/Moment;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->a(Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lacx;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/p;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->b()V

    .line 64
    if-eqz p1, :cond_0

    .line 65
    iget-object v0, p0, Lacx;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/p;

    invoke-interface {p1}, Lcom/twitter/android/moments/viewmodels/k;->a()Lcom/twitter/media/request/a$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->a(Lcom/twitter/media/request/a$a;)V

    .line 67
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Lacx;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    iget-object v1, p0, Lacx;->a:Lwy;

    new-instance v0, Lcex$a;

    invoke-direct {v0}, Lcex$a;-><init>()V

    .line 96
    invoke-virtual {v0, p1}, Lcex$a;->a(Ljava/lang/String;)Lcex$a;

    move-result-object v0

    .line 97
    invoke-virtual {v0, p2}, Lcex$a;->b(Ljava/lang/String;)Lcex$a;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Lcex$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfd;

    .line 95
    invoke-virtual {v1, v0}, Lwy;->a(Lcfd;)V

    .line 99
    iget-object v0, p0, Lacx;->a:Lwy;

    invoke-virtual {v0}, Lwy;->c()Lrx/g;

    move-result-object v0

    invoke-static {}, Lcqw;->d()Lcqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    .line 100
    iget-object v0, p0, Lacx;->c:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/maker/navigation/ah;->a()V

    .line 101
    return-void
.end method

.method static synthetic a(Lcom/twitter/model/moments/Moment;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 26
    invoke-static {p0, p1, p2}, Lacx;->c(Lcom/twitter/model/moments/Moment;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lacx;)Lzw;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lacx;->d:Lzw;

    return-object v0
.end method

.method private b(Lcom/twitter/model/moments/Moment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 71
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lacx;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/p;

    new-instance v1, Lacx$2;

    invoke-direct {v1, p0, p1, p3}, Lacx$2;-><init>(Lacx;Lcom/twitter/model/moments/Moment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->a(Landroid/content/DialogInterface$OnClickListener;)V

    .line 91
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-object v0, p0, Lacx;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/p;

    new-instance v1, Lacx$3;

    invoke-direct {v1, p0, p1}, Lacx$3;-><init>(Lacx;Lcom/twitter/model/moments/Moment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->b(Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lacx;->a:Lwy;

    invoke-virtual {v0}, Lwy;->a()Lrx/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lrx/c;->d(I)Lrx/c;

    move-result-object v0

    new-instance v1, Lacx$4;

    invoke-direct {v1, p0, p1, p2}, Lacx$4;-><init>(Lacx;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/b;)Lrx/j;

    .line 111
    return-void
.end method

.method private static c(Lcom/twitter/model/moments/Moment;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    iget-object v0, v0, Lcom/twitter/model/moments/f;->c:Lcom/twitter/model/moments/MomentVisibilityMode;

    sget-object v1, Lcom/twitter/model/moments/MomentVisibilityMode;->a:Lcom/twitter/model/moments/MomentVisibilityMode;

    if-ne v0, v1, :cond_1

    .line 118
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 116
    :goto_0
    return v0

    .line 118
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lacx;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/p;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
