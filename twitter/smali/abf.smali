.class public Labf;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/fullscreen/ap;


# static fields
.field private static final a:Landroid/view/animation/Interpolator;


# instance fields
.field private final b:Landroid/content/res/Resources;

.field private final c:Landroid/view/View;

.field private final d:Lcom/twitter/android/moments/ui/OffScreenTranslationLayout;

.field private final e:Lcom/twitter/android/moments/ui/OffScreenTranslationLayout;

.field private final f:Lcom/twitter/android/av/ExternalActionButton;

.field private final g:Landroid/view/View;

.field private final h:Landroid/view/View;

.field private final i:Lcom/twitter/android/moments/ui/DoubleTapFavoriteHud;

.field private final j:Labc;

.field private final k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    invoke-static {}, Lcom/twitter/ui/anim/h;->b()Landroid/view/animation/Interpolator;

    move-result-object v0

    sput-object v0, Labf;->a:Landroid/view/animation/Interpolator;

    return-void
.end method

.method constructor <init>(Landroid/content/res/Resources;Landroid/view/View;Lcom/twitter/android/moments/ui/OffScreenTranslationLayout;Lcom/twitter/android/moments/ui/OffScreenTranslationLayout;Lcom/twitter/android/av/ExternalActionButton;Landroid/view/View;Landroid/view/View;Lcom/twitter/android/moments/ui/DoubleTapFavoriteHud;Labc;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Labf;->b:Landroid/content/res/Resources;

    .line 67
    iput-object p2, p0, Labf;->c:Landroid/view/View;

    .line 68
    iput-object p3, p0, Labf;->d:Lcom/twitter/android/moments/ui/OffScreenTranslationLayout;

    .line 69
    iput-object p4, p0, Labf;->e:Lcom/twitter/android/moments/ui/OffScreenTranslationLayout;

    .line 70
    iput-object p5, p0, Labf;->f:Lcom/twitter/android/av/ExternalActionButton;

    .line 71
    iput-object p6, p0, Labf;->g:Landroid/view/View;

    .line 72
    iput-object p7, p0, Labf;->h:Landroid/view/View;

    .line 73
    iput-object p8, p0, Labf;->i:Lcom/twitter/android/moments/ui/DoubleTapFavoriteHud;

    .line 75
    const v0, 0x7f0f0034

    .line 76
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Labf;->k:I

    .line 79
    iput-object p9, p0, Labf;->j:Labc;

    .line 80
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/DoubleTapFavoriteHud;)Labf;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 45
    const v0, 0x7f0401e6

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 47
    const v0, 0x7f0401e5

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 49
    const v0, 0x7f1304f8

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/twitter/android/av/ExternalActionButton;

    .line 50
    const v0, 0x7f1304f7

    .line 51
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/moments/ui/OffScreenTranslationLayout;

    .line 52
    const v0, 0x7f1304f9

    .line 53
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/moments/ui/OffScreenTranslationLayout;

    .line 54
    const v0, 0x7f1304fa

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 55
    new-instance v0, Labf;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-instance v9, Labc;

    invoke-direct {v9, v2}, Labc;-><init>(Landroid/view/View;)V

    move-object v8, p2

    invoke-direct/range {v0 .. v9}, Labf;-><init>(Landroid/content/res/Resources;Landroid/view/View;Lcom/twitter/android/moments/ui/OffScreenTranslationLayout;Lcom/twitter/android/moments/ui/OffScreenTranslationLayout;Lcom/twitter/android/av/ExternalActionButton;Landroid/view/View;Landroid/view/View;Lcom/twitter/android/moments/ui/DoubleTapFavoriteHud;Labc;)V

    return-object v0
.end method

.method static synthetic a(Labf;)Landroid/view/View;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Labf;->h:Landroid/view/View;

    return-object v0
.end method

.method private a(ZZ)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 147
    if-eqz p1, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    move v2, v0

    .line 148
    :goto_0
    if-eqz p2, :cond_2

    iget v0, p0, Labf;->k:I

    .line 149
    :goto_1
    iget-object v3, p0, Labf;->h:Landroid/view/View;

    sget-object v4, Labf;->a:Landroid/view/animation/Interpolator;

    invoke-static {v3, v2, v0, v4}, Lcom/twitter/util/e;->b(Landroid/view/View;FILandroid/view/animation/Interpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v2, Labf$1;

    invoke-direct {v2, p0, p1}, Labf$1;-><init>(Labf;Z)V

    .line 150
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 159
    if-eqz p1, :cond_0

    .line 160
    iget-object v0, p0, Labf;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 162
    :cond_0
    return-void

    .line 147
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 148
    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Labf;->e:Lcom/twitter/android/moments/ui/OffScreenTranslationLayout;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/OffScreenTranslationLayout;->b()V

    .line 91
    iget-object v0, p0, Labf;->g:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    return-void
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Labf;->h:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationX(F)V

    .line 166
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Labf;->h:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 170
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Labf;->e:Lcom/twitter/android/moments/ui/OffScreenTranslationLayout;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/OffScreenTranslationLayout;->a()V

    .line 85
    iget-object v0, p0, Labf;->g:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    return-void
.end method

.method public a(Lcom/twitter/android/av/audio/e;Lcom/twitter/model/core/Tweet;)V
    .locals 1

    .prologue
    .line 115
    invoke-virtual {p0, p1, p2}, Labf;->b(Lcom/twitter/android/av/audio/e;Lcom/twitter/model/core/Tweet;)V

    .line 116
    iget-object v0, p0, Labf;->j:Labc;

    invoke-virtual {v0, p1}, Labc;->a(Lcom/twitter/android/av/audio/e;)V

    .line 117
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Labf;->a(ZZ)V

    .line 136
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Labf;->d:Lcom/twitter/android/moments/ui/OffScreenTranslationLayout;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/OffScreenTranslationLayout;->a()V

    .line 96
    return-void
.end method

.method b(Lcom/twitter/android/av/audio/e;Lcom/twitter/model/core/Tweet;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Labf;->f:Lcom/twitter/android/av/ExternalActionButton;

    invoke-static {v0, p1, p2}, Lcom/twitter/android/av/audio/c;->a(Lcom/twitter/android/av/ExternalActionButton;Lcom/twitter/android/av/audio/e;Lcom/twitter/model/core/Tweet;)V

    .line 122
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Labf;->a(ZZ)V

    .line 140
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Labf;->d:Lcom/twitter/android/moments/ui/OffScreenTranslationLayout;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/OffScreenTranslationLayout;->b()V

    .line 100
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Labf;->j:Labc;

    invoke-virtual {v0}, Labc;->a()V

    .line 104
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Labf;->j:Labc;

    invoke-virtual {v0}, Labc;->b()V

    .line 108
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Labf;->j:Labc;

    invoke-virtual {v0}, Labc;->c()V

    .line 112
    return-void
.end method

.method public g()Landroid/view/View;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Labf;->c:Landroid/view/View;

    return-object v0
.end method

.method public h()Landroid/view/View;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Labf;->h:Landroid/view/View;

    return-object v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Labf;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Labf;->i:Lcom/twitter/android/moments/ui/DoubleTapFavoriteHud;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/DoubleTapFavoriteHud;->a()V

    .line 174
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Labf;->i:Lcom/twitter/android/moments/ui/DoubleTapFavoriteHud;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/DoubleTapFavoriteHud;->b()V

    .line 178
    return-void
.end method

.method public l()V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Labf;->c:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setKeepScreenOn(Z)V

    .line 182
    return-void
.end method

.method public m()V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Labf;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setKeepScreenOn(Z)V

    .line 186
    return-void
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Labf;->b:Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/twitter/util/b;->a(Landroid/content/res/Resources;)Z

    move-result v0

    return v0
.end method
