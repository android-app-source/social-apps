.class public Lafv;
.super Lcbp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbp",
        "<",
        "Lafu$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcbp;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Lafu$a;
    .locals 4

    .prologue
    .line 19
    sget v0, Lbth;->b:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 20
    sget v1, Lbth;->d:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 21
    new-instance v2, Lafu$a;

    invoke-direct {v2}, Lafu$a;-><init>()V

    .line 22
    invoke-virtual {v2, v1}, Lafu$a;->b(I)Lafu$a;

    .line 23
    packed-switch v0, :pswitch_data_0

    .line 38
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Tried to hydrate collection with an unsupported type: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    .line 43
    :goto_0
    return-object v2

    .line 25
    :pswitch_0
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lafu$a;->a(I)Lafu$a;

    move-result-object v0

    sget-object v1, Lbtc;->a:Lbtc;

    .line 26
    invoke-virtual {v1, p1}, Lbtc;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lafu$a;->a(Lcom/twitter/model/core/Tweet$a;)Lafu$a;

    goto :goto_0

    .line 30
    :pswitch_1
    sget v0, Lbth;->c:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 31
    sget-object v1, Lcax;->a:Lcom/twitter/util/serialization/l;

    .line 32
    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcax;

    .line 33
    invoke-virtual {v2, v0}, Lafu$a;->a(Lcax;)Lafu$a;

    move-result-object v0

    const/4 v1, 0x2

    .line 34
    invoke-virtual {v0, v1}, Lafu$a;->a(I)Lafu$a;

    goto :goto_0

    .line 23
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lafv;->a(Landroid/database/Cursor;)Lafu$a;

    move-result-object v0

    return-object v0
.end method
