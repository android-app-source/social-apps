.class public Ladz;
.super Lcbp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbp",
        "<",
        "Lcam;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ladt;


# direct methods
.method public constructor <init>(Ladt;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcbp;-><init>()V

    .line 24
    iput-object p1, p0, Ladz;->a:Ladt;

    .line 25
    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Lcam;
    .locals 10

    .prologue
    .line 30
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lads;

    .line 31
    iget-object v1, p0, Ladz;->a:Ladt;

    invoke-virtual {v1, p1}, Ladt;->a(Landroid/database/Cursor;)Lbzy;

    move-result-object v8

    .line 32
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lads;->getLong(I)J

    move-result-wide v2

    .line 34
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 33
    invoke-virtual {v0, v1}, Lads;->b(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 36
    new-instance v9, Lcam;

    new-instance v1, Lcal;

    iget-wide v2, v0, Lcom/twitter/model/core/Tweet;->q:J

    iget-wide v4, v8, Lbzy;->b:J

    iget-wide v6, v8, Lbzy;->c:J

    invoke-direct/range {v1 .. v7}, Lcal;-><init>(JJJ)V

    invoke-direct {v9, v8, v1, v0}, Lcam;-><init>(Lbzy;Lcal;Lcom/twitter/model/core/Tweet;)V

    return-object v9
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Ladz;->a(Landroid/database/Cursor;)Lcam;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 42
    invoke-static {p1}, Ladt;->b(Landroid/database/Cursor;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 19
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Ladz;->b(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method
