.class public Lbww;
.super Lbwu;
.source "Twttr"


# direct methods
.method public constructor <init>(Lbwu$a;)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lbwu;-><init>(Lbwu$a;)V

    .line 11
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/model/core/Tweet;Lbwt;)I
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 23
    iget-wide v2, p1, Lcom/twitter/model/core/Tweet;->s:J

    invoke-virtual {p2}, Lbwt;->a()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    iget-boolean v2, p1, Lcom/twitter/model/core/Tweet;->F:Z

    if-eqz v2, :cond_2

    move v2, v0

    .line 24
    :goto_0
    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 25
    :cond_0
    const/4 v0, 0x2

    .line 29
    :cond_1
    :goto_1
    return v0

    :cond_2
    move v2, v1

    .line 23
    goto :goto_0

    .line 26
    :cond_3
    iget-boolean v2, p1, Lcom/twitter/model/core/Tweet;->c:Z

    if-nez v2, :cond_1

    move v0, v1

    .line 29
    goto :goto_1
.end method

.method public a()Lcom/twitter/model/core/TweetActionType;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->c:Lcom/twitter/model/core/TweetActionType;

    return-object v0
.end method

.method protected b(Lcom/twitter/model/core/Tweet;Lbwt;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p1, Lcom/twitter/model/core/Tweet;->k:I

    return v0
.end method
