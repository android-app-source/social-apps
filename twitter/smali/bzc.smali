.class public Lbzc;
.super Lcom/twitter/metrics/n;
.source "Twttr"


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 59
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "Network"

    const-string/jumbo v2, "network"

    .line 60
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "ResourceCache"

    const-string/jumbo v2, "resource_cache"

    .line 61
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "Memory"

    const-string/jumbo v2, "memory"

    .line 62
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "NetworkCache"

    const-string/jumbo v2, "network_cache"

    .line 63
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "Undefined"

    const-string/jumbo v2, "undefined"

    .line 64
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    sput-object v0, Lbzc;->a:Ljava/util/Map;

    .line 59
    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 74
    const-string/jumbo v0, "image:wait_time"

    new-instance v1, Lcom/twitter/metrics/g$a;

    const-string/jumbo v2, "photo_wait_time_sample_rate"

    const/16 v3, 0x1f4

    .line 75
    invoke-static {v2, v3}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v2

    invoke-direct {v1, v2}, Lcom/twitter/metrics/g$a;-><init>(I)V

    .line 74
    invoke-direct {p0, v0, v1, v4, v4}, Lcom/twitter/metrics/n;-><init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;Ljava/lang/String;Lcom/twitter/metrics/h;)V

    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbzc;->b:Ljava/util/Map;

    .line 71
    const-string/jumbo v0, "not_loaded"

    iput-object v0, p0, Lbzc;->e:Ljava/lang/String;

    .line 78
    invoke-static {}, Lcom/twitter/metrics/ForegroundMetricTracker;->a()Lcom/twitter/metrics/ForegroundMetricTracker;

    move-result-object v0

    sget-object v1, Lcom/twitter/metrics/ForegroundMetricTracker$BackgroundBehavior;->a:Lcom/twitter/metrics/ForegroundMetricTracker$BackgroundBehavior;

    invoke-virtual {v0, p0, v1}, Lcom/twitter/metrics/ForegroundMetricTracker;->a(Lcom/twitter/metrics/f;Lcom/twitter/metrics/ForegroundMetricTracker$BackgroundBehavior;)V

    .line 80
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/twitter/metrics/n;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lbzc;->c:Ljava/lang/String;

    const-string/jumbo v2, "undefined"

    .line 165
    invoke-static {v0, v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbzc;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbzc;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 164
    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 113
    if-nez p1, :cond_0

    .line 117
    :goto_0
    return-void

    .line 116
    :cond_0
    iput-object p1, p0, Lbzc;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 145
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    iget-object v0, p0, Lbzc;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 124
    if-nez p1, :cond_0

    .line 134
    :goto_0
    return-void

    .line 127
    :cond_0
    sget-object v0, Lbzc;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 128
    if-nez v0, :cond_1

    .line 130
    iput-object p1, p0, Lbzc;->e:Ljava/lang/String;

    goto :goto_0

    .line 132
    :cond_1
    iput-object v0, p0, Lbzc;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method protected c()V
    .locals 4

    .prologue
    .line 84
    invoke-super {p0}, Lcom/twitter/metrics/n;->c()V

    .line 88
    const-string/jumbo v0, "photo_wait_time_fling_threshold"

    const/16 v1, 0xfa

    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v0

    .line 89
    const-string/jumbo v1, "navigate"

    iget-object v2, p0, Lbzc;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lbzc;->t:J

    int-to-long v0, v0

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    .line 91
    const-string/jumbo v0, "fling"

    iput-object v0, p0, Lbzc;->d:Ljava/lang/String;

    .line 95
    :cond_0
    const-string/jumbo v0, "network"

    iget-object v1, p0, Lbzc;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "not_loaded"

    iget-object v1, p0, Lbzc;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 96
    :cond_1
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/metrics/j;->a(Lcom/twitter/metrics/g;)V

    .line 98
    :cond_2
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lbzc;->d:Ljava/lang/String;

    .line 138
    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lbzc;->b:Ljava/util/Map;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected f()Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method public g()V
    .locals 0

    .prologue
    .line 171
    invoke-virtual {p0}, Lbzc;->n()V

    .line 172
    return-void
.end method
