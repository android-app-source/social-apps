.class Lcom/twitter/library/commerce/model/h$a;
.super Lcom/twitter/library/commerce/model/j$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/commerce/model/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/commerce/model/j$a",
        "<",
        "Lcom/twitter/library/commerce/model/h;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 197
    invoke-direct {p0}, Lcom/twitter/library/commerce/model/j$a;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/library/commerce/model/h$1;)V
    .locals 0

    .prologue
    .line 197
    invoke-direct {p0}, Lcom/twitter/library/commerce/model/h$a;-><init>()V

    return-void
.end method

.method private static a()Lcom/twitter/util/serialization/l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/util/Calendar;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250
    new-instance v0, Lcom/twitter/library/commerce/model/h$a$1;

    invoke-direct {v0}, Lcom/twitter/library/commerce/model/h$a$1;-><init>()V

    return-object v0
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/library/commerce/model/h;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 226
    new-instance v1, Lcom/twitter/library/commerce/model/h;

    invoke-direct {v1}, Lcom/twitter/library/commerce/model/h;-><init>()V

    .line 227
    invoke-virtual {p0, p1, v1}, Lcom/twitter/library/commerce/model/h$a;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/commerce/model/j;)V

    .line 228
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/library/commerce/model/h;->a(Lcom/twitter/library/commerce/model/h;Ljava/lang/String;)Ljava/lang/String;

    .line 229
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/library/commerce/model/h;->b(Lcom/twitter/library/commerce/model/h;Ljava/lang/String;)Ljava/lang/String;

    .line 230
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/library/commerce/model/h;->c(Lcom/twitter/library/commerce/model/h;Ljava/lang/String;)Ljava/lang/String;

    .line 231
    invoke-static {}, Lcom/twitter/library/commerce/model/h$a;->a()Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    invoke-static {v1, v0}, Lcom/twitter/library/commerce/model/h;->a(Lcom/twitter/library/commerce/model/h;Ljava/util/Calendar;)Ljava/util/Calendar;

    .line 232
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/library/commerce/model/h;->d(Lcom/twitter/library/commerce/model/h;Ljava/lang/String;)Ljava/lang/String;

    .line 233
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/library/commerce/model/h;->e(Lcom/twitter/library/commerce/model/h;Ljava/lang/String;)Ljava/lang/String;

    .line 234
    sget-object v0, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-static {v1, v0}, Lcom/twitter/library/commerce/model/h;->a(Lcom/twitter/library/commerce/model/h;Ljava/lang/Long;)Ljava/lang/Long;

    .line 235
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/library/commerce/model/h;->f(Lcom/twitter/library/commerce/model/h;Ljava/lang/String;)Ljava/lang/String;

    .line 236
    sget-object v0, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-static {v1, v0}, Lcom/twitter/library/commerce/model/h;->b(Lcom/twitter/library/commerce/model/h;Ljava/lang/Long;)Ljava/lang/Long;

    .line 237
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/library/commerce/model/h;->g(Lcom/twitter/library/commerce/model/h;Ljava/lang/String;)Ljava/lang/String;

    .line 238
    sget-object v0, Lcom/twitter/util/serialization/f;->l:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/math/BigDecimal;

    invoke-static {v1, v0}, Lcom/twitter/library/commerce/model/h;->a(Lcom/twitter/library/commerce/model/h;Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    .line 239
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/library/commerce/model/h;->h(Lcom/twitter/library/commerce/model/h;Ljava/lang/String;)Ljava/lang/String;

    .line 240
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/library/commerce/model/h;->i(Lcom/twitter/library/commerce/model/h;Ljava/lang/String;)Ljava/lang/String;

    .line 241
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/library/commerce/model/h;->j(Lcom/twitter/library/commerce/model/h;Ljava/lang/String;)Ljava/lang/String;

    .line 242
    sget-object v0, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-static {v1, v0}, Lcom/twitter/library/commerce/model/h;->c(Lcom/twitter/library/commerce/model/h;Ljava/lang/Long;)Ljava/lang/Long;

    .line 243
    sget-object v0, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v2, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 244
    invoke-static {v0, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 243
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-static {v1, v0}, Lcom/twitter/library/commerce/model/h;->a(Lcom/twitter/library/commerce/model/h;Ljava/util/Map;)Ljava/util/Map;

    .line 245
    return-object v1
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/h;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 202
    invoke-super {p0, p1, p2}, Lcom/twitter/library/commerce/model/j$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/j;)V

    .line 203
    invoke-static {p2}, Lcom/twitter/library/commerce/model/h;->p(Lcom/twitter/library/commerce/model/h;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 204
    invoke-static {p2}, Lcom/twitter/library/commerce/model/h;->o(Lcom/twitter/library/commerce/model/h;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 205
    invoke-static {p2}, Lcom/twitter/library/commerce/model/h;->n(Lcom/twitter/library/commerce/model/h;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 206
    invoke-static {p2}, Lcom/twitter/library/commerce/model/h;->m(Lcom/twitter/library/commerce/model/h;)Ljava/util/Calendar;

    move-result-object v1

    invoke-static {}, Lcom/twitter/library/commerce/model/h$a;->a()Lcom/twitter/util/serialization/l;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 207
    invoke-static {p2}, Lcom/twitter/library/commerce/model/h;->l(Lcom/twitter/library/commerce/model/h;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 208
    invoke-static {p2}, Lcom/twitter/library/commerce/model/h;->k(Lcom/twitter/library/commerce/model/h;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 209
    invoke-static {p2}, Lcom/twitter/library/commerce/model/h;->j(Lcom/twitter/library/commerce/model/h;)Ljava/lang/Long;

    move-result-object v1

    sget-object v2, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 210
    invoke-static {p2}, Lcom/twitter/library/commerce/model/h;->i(Lcom/twitter/library/commerce/model/h;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 211
    invoke-static {p2}, Lcom/twitter/library/commerce/model/h;->h(Lcom/twitter/library/commerce/model/h;)Ljava/lang/Long;

    move-result-object v1

    sget-object v2, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 212
    invoke-static {p2}, Lcom/twitter/library/commerce/model/h;->g(Lcom/twitter/library/commerce/model/h;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 213
    invoke-static {p2}, Lcom/twitter/library/commerce/model/h;->f(Lcom/twitter/library/commerce/model/h;)Ljava/math/BigDecimal;

    move-result-object v1

    sget-object v2, Lcom/twitter/util/serialization/f;->l:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 214
    invoke-static {p2}, Lcom/twitter/library/commerce/model/h;->e(Lcom/twitter/library/commerce/model/h;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 215
    invoke-static {p2}, Lcom/twitter/library/commerce/model/h;->d(Lcom/twitter/library/commerce/model/h;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 216
    invoke-static {p2}, Lcom/twitter/library/commerce/model/h;->c(Lcom/twitter/library/commerce/model/h;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 217
    invoke-static {p2}, Lcom/twitter/library/commerce/model/h;->b(Lcom/twitter/library/commerce/model/h;)Ljava/lang/Long;

    move-result-object v1

    sget-object v2, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 218
    invoke-static {p2}, Lcom/twitter/library/commerce/model/h;->a(Lcom/twitter/library/commerce/model/h;)Ljava/util/Map;

    move-result-object v1

    sget-object v2, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    sget-object v3, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 219
    invoke-static {v2, v3}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 218
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 220
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/j;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 197
    check-cast p2, Lcom/twitter/library/commerce/model/h;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/commerce/model/h$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/h;)V

    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 197
    check-cast p2, Lcom/twitter/library/commerce/model/h;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/commerce/model/h$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/h;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 197
    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/commerce/model/h$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/library/commerce/model/h;

    move-result-object v0

    return-object v0
.end method
