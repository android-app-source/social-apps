.class public abstract Lcom/twitter/library/commerce/model/CreditCard$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/commerce/model/CreditCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/library/commerce/model/CreditCard;",
        "B:",
        "Lcom/twitter/library/commerce/model/CreditCard$a",
        "<TT;TB;>;>",
        "Lcom/twitter/util/object/i",
        "<TT;>;"
    }
.end annotation


# instance fields
.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/Integer;

.field g:Ljava/lang/Integer;

.field h:Lcom/twitter/library/commerce/model/a;

.field i:Ljava/lang/String;

.field j:Z

.field k:Z

.field l:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 492
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/commerce/model/a;)Lcom/twitter/library/commerce/model/CreditCard$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/commerce/model/a;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 530
    iput-object p1, p0, Lcom/twitter/library/commerce/model/CreditCard$a;->h:Lcom/twitter/library/commerce/model/a;

    .line 531
    return-object p0
.end method

.method public a(Ljava/lang/Integer;)Lcom/twitter/library/commerce/model/CreditCard$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 518
    iput-object p1, p0, Lcom/twitter/library/commerce/model/CreditCard$a;->f:Ljava/lang/Integer;

    .line 519
    return-object p0
.end method

.method public a(Z)Lcom/twitter/library/commerce/model/CreditCard$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TB;"
        }
    .end annotation

    .prologue
    .line 542
    iput-boolean p1, p0, Lcom/twitter/library/commerce/model/CreditCard$a;->j:Z

    .line 543
    return-object p0
.end method

.method public b(Ljava/lang/Integer;)Lcom/twitter/library/commerce/model/CreditCard$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 524
    iput-object p1, p0, Lcom/twitter/library/commerce/model/CreditCard$a;->g:Ljava/lang/Integer;

    .line 525
    return-object p0
.end method

.method public b(Z)Lcom/twitter/library/commerce/model/CreditCard$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TB;"
        }
    .end annotation

    .prologue
    .line 548
    iput-boolean p1, p0, Lcom/twitter/library/commerce/model/CreditCard$a;->k:Z

    .line 549
    return-object p0
.end method

.method public c(Z)Lcom/twitter/library/commerce/model/CreditCard$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TB;"
        }
    .end annotation

    .prologue
    .line 554
    iput-boolean p1, p0, Lcom/twitter/library/commerce/model/CreditCard$a;->l:Z

    .line 555
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/library/commerce/model/CreditCard$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 506
    iput-object p1, p0, Lcom/twitter/library/commerce/model/CreditCard$a;->d:Ljava/lang/String;

    .line 507
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/library/commerce/model/CreditCard$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 512
    iput-object p1, p0, Lcom/twitter/library/commerce/model/CreditCard$a;->e:Ljava/lang/String;

    .line 513
    return-object p0
.end method

.method public f(Ljava/lang/String;)Lcom/twitter/library/commerce/model/CreditCard$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 536
    iput-object p1, p0, Lcom/twitter/library/commerce/model/CreditCard$a;->i:Ljava/lang/String;

    .line 537
    return-object p0
.end method
