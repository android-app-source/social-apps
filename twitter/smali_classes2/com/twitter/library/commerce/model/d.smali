.class public Lcom/twitter/library/commerce/model/d;
.super Lcom/twitter/library/commerce/model/CreditCard;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/commerce/model/d$d;,
        Lcom/twitter/library/commerce/model/d$b;,
        Lcom/twitter/library/commerce/model/d$c;,
        Lcom/twitter/library/commerce/model/d$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/library/commerce/model/d;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/twitter/library/commerce/model/d$d;

    invoke-direct {v0}, Lcom/twitter/library/commerce/model/d$d;-><init>()V

    sput-object v0, Lcom/twitter/library/commerce/model/d;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/twitter/library/commerce/model/CreditCard;-><init>()V

    .line 30
    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/commerce/model/d$a;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/twitter/library/commerce/model/CreditCard;-><init>(Lcom/twitter/library/commerce/model/CreditCard$a;)V

    .line 34
    iget-object v0, p1, Lcom/twitter/library/commerce/model/d$a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/commerce/model/d;->b:Ljava/lang/String;

    .line 35
    iget-object v0, p1, Lcom/twitter/library/commerce/model/d$a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/commerce/model/d;->c:Ljava/lang/String;

    .line 36
    iget-object v0, p1, Lcom/twitter/library/commerce/model/d$a;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/commerce/model/d;->e:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/library/commerce/model/CreditCard$Type;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/d;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/d;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/commerce/model/CreditCard$Type;->e(Ljava/lang/String;)Lcom/twitter/library/commerce/model/CreditCard$Type;

    move-result-object v0

    .line 84
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/library/commerce/model/CreditCard$Type;->a:Lcom/twitter/library/commerce/model/CreditCard$Type;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/twitter/library/commerce/model/d;->c:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/d;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/d;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    .line 98
    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/d;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/d;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 100
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/twitter/library/commerce/model/d;->b:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public c()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    invoke-super {p0}, Lcom/twitter/library/commerce/model/CreditCard;->c()Ljava/util/List;

    move-result-object v0

    .line 42
    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/d;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 43
    sget v1, Lazw$k;->commerce_error_invalid_card_number_invalid:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/d;->g()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 47
    sget v1, Lazw$k;->commerce_error_invalid_card_ccv_number_empty:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    :cond_1
    :goto_0
    return-object v0

    .line 48
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/d;->d()Z

    move-result v1

    if-nez v1, :cond_1

    .line 49
    sget v1, Lazw$k;->commerce_error_invalid_card_ccv_number:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/twitter/library/commerce/model/d;->e:Ljava/lang/String;

    .line 93
    return-void
.end method

.method protected d()Z
    .locals 2

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/d;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/d;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/commerce/model/CreditCard$Type;->c(Ljava/lang/String;)I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected e()Z
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/d;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/commerce/model/CreditCard$Type;->e(Ljava/lang/String;)Lcom/twitter/library/commerce/model/CreditCard$Type;

    move-result-object v0

    .line 60
    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/d;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/commerce/model/CreditCard$Type;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 105
    if-ne p0, p1, :cond_1

    .line 116
    :cond_0
    :goto_0
    return v0

    .line 108
    :cond_1
    instance-of v2, p1, Lcom/twitter/library/commerce/model/d;

    if-nez v2, :cond_2

    move v0, v1

    .line 109
    goto :goto_0

    .line 111
    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/library/commerce/model/CreditCard;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 112
    goto :goto_0

    .line 115
    :cond_3
    check-cast p1, Lcom/twitter/library/commerce/model/d;

    .line 116
    iget-object v2, p0, Lcom/twitter/library/commerce/model/d;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/commerce/model/d;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/commerce/model/d;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/commerce/model/d;->e:Ljava/lang/String;

    .line 117
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/commerce/model/d;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/commerce/model/d;->c:Ljava/lang/String;

    .line 118
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/library/commerce/model/d;->c:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/library/commerce/model/d;->b:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/twitter/library/commerce/model/d;->e:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 123
    invoke-super {p0}, Lcom/twitter/library/commerce/model/CreditCard;->hashCode()I

    move-result v0

    .line 124
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/commerce/model/d;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 125
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/commerce/model/d;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 126
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/commerce/model/d;->e:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    return v0
.end method
