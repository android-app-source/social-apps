.class public Lcom/twitter/library/commerce/model/k;
.super Lcom/twitter/library/commerce/model/d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/commerce/model/k$b;,
        Lcom/twitter/library/commerce/model/k$a;
    }
.end annotation


# static fields
.field public static final b:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/library/commerce/model/k;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:Ljava/lang/String;

.field private e:Lcom/twitter/library/commerce/model/CreditCard$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/twitter/library/commerce/model/k$b;

    invoke-direct {v0}, Lcom/twitter/library/commerce/model/k$b;-><init>()V

    sput-object v0, Lcom/twitter/library/commerce/model/k;->b:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/twitter/library/commerce/model/d;-><init>()V

    .line 25
    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/commerce/model/k$a;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/twitter/library/commerce/model/d;-><init>(Lcom/twitter/library/commerce/model/d$a;)V

    .line 29
    iget-object v0, p1, Lcom/twitter/library/commerce/model/k$a;->n:Lcom/twitter/library/commerce/model/CreditCard$Type;

    iget-object v1, p1, Lcom/twitter/library/commerce/model/k$a;->m:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/commerce/model/k;->a(Lcom/twitter/library/commerce/model/CreditCard$Type;Ljava/lang/String;)V

    .line 30
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/library/commerce/model/CreditCard$Type;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/library/commerce/model/k;->e:Lcom/twitter/library/commerce/model/CreditCard$Type;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/twitter/library/commerce/model/k;->e:Lcom/twitter/library/commerce/model/CreditCard$Type;

    .line 59
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/library/commerce/model/CreditCard$Type;->a:Lcom/twitter/library/commerce/model/CreditCard$Type;

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/commerce/model/CreditCard$Type;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/twitter/library/commerce/model/k;->e:Lcom/twitter/library/commerce/model/CreditCard$Type;

    .line 64
    return-void
.end method

.method public a(Lcom/twitter/library/commerce/model/CreditCard$Type;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/twitter/library/commerce/model/k;->a(Lcom/twitter/library/commerce/model/CreditCard$Type;)V

    .line 41
    iput-object p2, p0, Lcom/twitter/library/commerce/model/k;->c:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/library/commerce/model/k;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 78
    return-void
.end method

.method protected d()Z
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/library/commerce/model/k;->e:Lcom/twitter/library/commerce/model/CreditCard$Type;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/k;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/library/commerce/model/k;->e:Lcom/twitter/library/commerce/model/CreditCard$Type;

    invoke-virtual {v1}, Lcom/twitter/library/commerce/model/CreditCard$Type;->c()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected e()Z
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/k;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 82
    if-ne p0, p1, :cond_1

    .line 93
    :cond_0
    :goto_0
    return v0

    .line 85
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 86
    goto :goto_0

    .line 88
    :cond_3
    invoke-super {p0, p1}, Lcom/twitter/library/commerce/model/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 89
    goto :goto_0

    .line 92
    :cond_4
    check-cast p1, Lcom/twitter/library/commerce/model/k;

    .line 93
    iget-object v2, p0, Lcom/twitter/library/commerce/model/k;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/commerce/model/k;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/twitter/library/commerce/model/k;->e:Lcom/twitter/library/commerce/model/CreditCard$Type;

    iget-object v3, p1, Lcom/twitter/library/commerce/model/k;->e:Lcom/twitter/library/commerce/model/CreditCard$Type;

    if-eq v2, v3, :cond_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 98
    invoke-super {p0}, Lcom/twitter/library/commerce/model/d;->hashCode()I

    move-result v0

    .line 99
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/commerce/model/k;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/commerce/model/k;->e:Lcom/twitter/library/commerce/model/CreditCard$Type;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    return v0
.end method
