.class public Lcom/twitter/library/commerce/model/n;
.super Lcom/twitter/library/commerce/model/CreditCard;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/commerce/model/n$b;,
        Lcom/twitter/library/commerce/model/n$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/library/commerce/model/n;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Ljava/lang/String;

.field private c:Lcom/twitter/library/commerce/model/CreditCard$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/twitter/library/commerce/model/n$b;->a:Lcom/twitter/util/serialization/l;

    sput-object v0, Lcom/twitter/library/commerce/model/n;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/twitter/library/commerce/model/CreditCard;-><init>()V

    .line 27
    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/commerce/model/n$a;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/twitter/library/commerce/model/CreditCard;-><init>(Lcom/twitter/library/commerce/model/CreditCard$a;)V

    .line 31
    iget-object v0, p1, Lcom/twitter/library/commerce/model/n$a;->b:Lcom/twitter/library/commerce/model/CreditCard$Type;

    iget-object v1, p1, Lcom/twitter/library/commerce/model/n$a;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/commerce/model/n;->a(Lcom/twitter/library/commerce/model/CreditCard$Type;Ljava/lang/String;)V

    .line 32
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/library/commerce/model/CreditCard$Type;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/twitter/library/commerce/model/n;->c:Lcom/twitter/library/commerce/model/CreditCard$Type;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/twitter/library/commerce/model/n;->c:Lcom/twitter/library/commerce/model/CreditCard$Type;

    .line 68
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/library/commerce/model/CreditCard$Type;->a:Lcom/twitter/library/commerce/model/CreditCard$Type;

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/commerce/model/CreditCard$Type;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/twitter/library/commerce/model/n;->c:Lcom/twitter/library/commerce/model/CreditCard$Type;

    .line 73
    return-void
.end method

.method public a(Lcom/twitter/library/commerce/model/CreditCard$Type;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lcom/twitter/library/commerce/model/n;->a(Lcom/twitter/library/commerce/model/CreditCard$Type;)V

    .line 55
    iput-object p2, p0, Lcom/twitter/library/commerce/model/n;->b:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/library/commerce/model/n;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    invoke-super {p0}, Lcom/twitter/library/commerce/model/CreditCard;->c()Ljava/util/List;

    move-result-object v0

    .line 40
    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/n;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 41
    sget v1, Lazw$k;->commerce_error_invalid_card_last_four_digits:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    :cond_0
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 77
    if-ne p0, p1, :cond_1

    .line 88
    :cond_0
    :goto_0
    return v0

    .line 80
    :cond_1
    instance-of v2, p1, Lcom/twitter/library/commerce/model/n;

    if-nez v2, :cond_2

    move v0, v1

    .line 81
    goto :goto_0

    .line 83
    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/library/commerce/model/CreditCard;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 84
    goto :goto_0

    .line 87
    :cond_3
    check-cast p1, Lcom/twitter/library/commerce/model/n;

    .line 88
    iget-object v2, p0, Lcom/twitter/library/commerce/model/n;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/commerce/model/n;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/commerce/model/n;->c:Lcom/twitter/library/commerce/model/CreditCard$Type;

    iget-object v3, p1, Lcom/twitter/library/commerce/model/n;->c:Lcom/twitter/library/commerce/model/CreditCard$Type;

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 93
    invoke-super {p0}, Lcom/twitter/library/commerce/model/CreditCard;->hashCode()I

    move-result v0

    .line 94
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/commerce/model/n;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/commerce/model/n;->c:Lcom/twitter/library/commerce/model/CreditCard$Type;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    return v0
.end method
