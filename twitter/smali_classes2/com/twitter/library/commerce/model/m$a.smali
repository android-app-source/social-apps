.class Lcom/twitter/library/commerce/model/m$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/commerce/model/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/library/commerce/model/m;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 178
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/library/commerce/model/m$1;)V
    .locals 0

    .prologue
    .line 178
    invoke-direct {p0}, Lcom/twitter/library/commerce/model/m$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/library/commerce/model/m;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 197
    new-instance v1, Lcom/twitter/library/commerce/model/m;

    invoke-direct {v1}, Lcom/twitter/library/commerce/model/m;-><init>()V

    .line 198
    new-instance v2, Ljava/util/ArrayList;

    sget-object v0, Lcom/twitter/library/commerce/model/CreditCard;->d:Lcom/twitter/util/serialization/l;

    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 199
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 198
    invoke-static {v1, v2}, Lcom/twitter/library/commerce/model/m;->a(Lcom/twitter/library/commerce/model/m;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 200
    new-instance v2, Ljava/util/ArrayList;

    sget-object v0, Lcom/twitter/library/commerce/model/CreditCard;->d:Lcom/twitter/util/serialization/l;

    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 201
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 200
    invoke-static {v1, v2}, Lcom/twitter/library/commerce/model/m;->b(Lcom/twitter/library/commerce/model/m;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 202
    new-instance v2, Ljava/util/ArrayList;

    sget-object v0, Lcom/twitter/library/commerce/model/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 203
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 202
    invoke-static {v1, v2}, Lcom/twitter/library/commerce/model/m;->c(Lcom/twitter/library/commerce/model/m;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 204
    new-instance v2, Ljava/util/ArrayList;

    sget-object v0, Lcom/twitter/library/commerce/model/g;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 205
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 204
    invoke-static {v1, v2}, Lcom/twitter/library/commerce/model/m;->d(Lcom/twitter/library/commerce/model/m;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 206
    new-instance v2, Ljava/util/ArrayList;

    sget-object v0, Lcom/twitter/library/commerce/model/l;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 207
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 206
    invoke-static {v1, v2}, Lcom/twitter/library/commerce/model/m;->e(Lcom/twitter/library/commerce/model/m;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 208
    sget-object v0, Lcom/twitter/library/commerce/model/a;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/commerce/model/a;

    invoke-static {v1, v0}, Lcom/twitter/library/commerce/model/m;->a(Lcom/twitter/library/commerce/model/m;Lcom/twitter/library/commerce/model/a;)Lcom/twitter/library/commerce/model/a;

    .line 209
    sget-object v0, Lcom/twitter/library/commerce/model/CreditCard;->d:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/commerce/model/CreditCard;

    invoke-static {v1, v0}, Lcom/twitter/library/commerce/model/m;->a(Lcom/twitter/library/commerce/model/m;Lcom/twitter/library/commerce/model/CreditCard;)Lcom/twitter/library/commerce/model/CreditCard;

    .line 210
    sget-object v0, Lcom/twitter/library/commerce/model/g;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/commerce/model/g;

    invoke-static {v1, v0}, Lcom/twitter/library/commerce/model/m;->a(Lcom/twitter/library/commerce/model/m;Lcom/twitter/library/commerce/model/g;)Lcom/twitter/library/commerce/model/g;

    .line 211
    sget-object v0, Lcom/twitter/library/commerce/model/l;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/commerce/model/l;

    invoke-static {v1, v0}, Lcom/twitter/library/commerce/model/m;->a(Lcom/twitter/library/commerce/model/m;Lcom/twitter/library/commerce/model/l;)Lcom/twitter/library/commerce/model/l;

    .line 212
    return-object v1
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/m;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 181
    invoke-static {p2}, Lcom/twitter/library/commerce/model/m;->i(Lcom/twitter/library/commerce/model/m;)Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/commerce/model/CreditCard;->d:Lcom/twitter/util/serialization/l;

    invoke-static {v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 182
    invoke-static {p2}, Lcom/twitter/library/commerce/model/m;->h(Lcom/twitter/library/commerce/model/m;)Ljava/util/ArrayList;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/commerce/model/CreditCard;->d:Lcom/twitter/util/serialization/l;

    .line 183
    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 182
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 184
    invoke-static {p2}, Lcom/twitter/library/commerce/model/m;->g(Lcom/twitter/library/commerce/model/m;)Ljava/util/ArrayList;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/commerce/model/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 185
    invoke-static {p2}, Lcom/twitter/library/commerce/model/m;->f(Lcom/twitter/library/commerce/model/m;)Ljava/util/ArrayList;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/commerce/model/g;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 186
    invoke-static {p2}, Lcom/twitter/library/commerce/model/m;->e(Lcom/twitter/library/commerce/model/m;)Ljava/util/ArrayList;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/commerce/model/l;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 187
    invoke-static {p2}, Lcom/twitter/library/commerce/model/m;->d(Lcom/twitter/library/commerce/model/m;)Lcom/twitter/library/commerce/model/a;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/commerce/model/a;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 188
    invoke-static {p2}, Lcom/twitter/library/commerce/model/m;->c(Lcom/twitter/library/commerce/model/m;)Lcom/twitter/library/commerce/model/CreditCard;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/commerce/model/CreditCard;->d:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 189
    invoke-static {p2}, Lcom/twitter/library/commerce/model/m;->b(Lcom/twitter/library/commerce/model/m;)Lcom/twitter/library/commerce/model/g;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/commerce/model/g;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 190
    invoke-static {p2}, Lcom/twitter/library/commerce/model/m;->a(Lcom/twitter/library/commerce/model/m;)Lcom/twitter/library/commerce/model/l;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/commerce/model/l;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 191
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    check-cast p2, Lcom/twitter/library/commerce/model/m;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/commerce/model/m$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/m;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 178
    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/commerce/model/m$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/library/commerce/model/m;

    move-result-object v0

    return-object v0
.end method
