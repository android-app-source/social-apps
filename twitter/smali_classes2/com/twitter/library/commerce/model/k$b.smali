.class public Lcom/twitter/library/commerce/model/k$b;
.super Lcom/twitter/library/commerce/model/d$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/commerce/model/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/commerce/model/d$b",
        "<",
        "Lcom/twitter/library/commerce/model/k;",
        "Lcom/twitter/library/commerce/model/k$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/library/commerce/model/k;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 130
    new-instance v0, Lcom/twitter/library/commerce/model/k$b;

    invoke-direct {v0}, Lcom/twitter/library/commerce/model/k$b;-><init>()V

    sput-object v0, Lcom/twitter/library/commerce/model/k$b;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/twitter/library/commerce/model/d$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/commerce/model/k$a;
    .locals 1

    .prologue
    .line 145
    new-instance v0, Lcom/twitter/library/commerce/model/k$a;

    invoke-direct {v0}, Lcom/twitter/library/commerce/model/k$a;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/commerce/model/CreditCard$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 128
    check-cast p2, Lcom/twitter/library/commerce/model/k$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/commerce/model/k$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/commerce/model/k$a;I)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/commerce/model/d$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 128
    check-cast p2, Lcom/twitter/library/commerce/model/k$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/commerce/model/k$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/commerce/model/k$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/commerce/model/k$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 152
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/library/commerce/model/d$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/commerce/model/d$a;I)V

    .line 153
    const-class v0, Lcom/twitter/library/commerce/model/CreditCard$Type;

    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/commerce/model/CreditCard$Type;

    invoke-virtual {p2, v0}, Lcom/twitter/library/commerce/model/k$a;->a(Lcom/twitter/library/commerce/model/CreditCard$Type;)Lcom/twitter/library/commerce/model/k$a;

    move-result-object v0

    .line 154
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/commerce/model/k$a;->g(Ljava/lang/String;)Lcom/twitter/library/commerce/model/k$a;

    .line 155
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 128
    check-cast p2, Lcom/twitter/library/commerce/model/k$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/commerce/model/k$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/commerce/model/k$a;I)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/CreditCard;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    check-cast p2, Lcom/twitter/library/commerce/model/k;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/commerce/model/k$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/k;)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    check-cast p2, Lcom/twitter/library/commerce/model/k;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/commerce/model/k$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/k;)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    invoke-super {p0, p1, p2}, Lcom/twitter/library/commerce/model/d$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/d;)V

    .line 138
    invoke-virtual {p2}, Lcom/twitter/library/commerce/model/k;->a()Lcom/twitter/library/commerce/model/CreditCard$Type;

    move-result-object v0

    const-class v1, Lcom/twitter/library/commerce/model/CreditCard$Type;

    invoke-static {v1}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 139
    invoke-virtual {p2}, Lcom/twitter/library/commerce/model/k;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 140
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    check-cast p2, Lcom/twitter/library/commerce/model/k;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/commerce/model/k$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/k;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/k$b;->a()Lcom/twitter/library/commerce/model/k$a;

    move-result-object v0

    return-object v0
.end method
