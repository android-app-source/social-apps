.class public Lcom/twitter/library/commerce/model/n$b;
.super Lcom/twitter/library/commerce/model/CreditCard$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/commerce/model/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/commerce/model/CreditCard$b",
        "<",
        "Lcom/twitter/library/commerce/model/n;",
        "Lcom/twitter/library/commerce/model/n$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/library/commerce/model/n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 124
    new-instance v0, Lcom/twitter/library/commerce/model/n$b;

    invoke-direct {v0}, Lcom/twitter/library/commerce/model/n$b;-><init>()V

    sput-object v0, Lcom/twitter/library/commerce/model/n$b;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/twitter/library/commerce/model/CreditCard$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/commerce/model/n$a;
    .locals 1

    .prologue
    .line 138
    new-instance v0, Lcom/twitter/library/commerce/model/n$a;

    invoke-direct {v0}, Lcom/twitter/library/commerce/model/n$a;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/commerce/model/CreditCard$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 123
    check-cast p2, Lcom/twitter/library/commerce/model/n$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/commerce/model/n$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/commerce/model/n$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/commerce/model/n$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 145
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/library/commerce/model/CreditCard$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/commerce/model/CreditCard$a;I)V

    .line 146
    const-class v0, Lcom/twitter/library/commerce/model/CreditCard$Type;

    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/commerce/model/CreditCard$Type;

    invoke-virtual {p2, v0}, Lcom/twitter/library/commerce/model/n$a;->a(Lcom/twitter/library/commerce/model/CreditCard$Type;)Lcom/twitter/library/commerce/model/n$a;

    move-result-object v0

    .line 147
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/commerce/model/n$a;->a(Ljava/lang/String;)Lcom/twitter/library/commerce/model/n$a;

    .line 148
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 123
    check-cast p2, Lcom/twitter/library/commerce/model/n$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/commerce/model/n$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/commerce/model/n$a;I)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/CreditCard;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    check-cast p2, Lcom/twitter/library/commerce/model/n;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/commerce/model/n$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/n;)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/n;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 130
    invoke-super {p0, p1, p2}, Lcom/twitter/library/commerce/model/CreditCard$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/CreditCard;)V

    .line 131
    invoke-virtual {p2}, Lcom/twitter/library/commerce/model/n;->a()Lcom/twitter/library/commerce/model/CreditCard$Type;

    move-result-object v0

    const-class v1, Lcom/twitter/library/commerce/model/CreditCard$Type;

    invoke-static {v1}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 132
    invoke-virtual {p2}, Lcom/twitter/library/commerce/model/n;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 133
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    check-cast p2, Lcom/twitter/library/commerce/model/n;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/commerce/model/n$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/n;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/twitter/library/commerce/model/n$b;->a()Lcom/twitter/library/commerce/model/n$a;

    move-result-object v0

    return-object v0
.end method
