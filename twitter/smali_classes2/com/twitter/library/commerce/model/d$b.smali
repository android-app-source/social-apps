.class public abstract Lcom/twitter/library/commerce/model/d$b;
.super Lcom/twitter/library/commerce/model/CreditCard$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/commerce/model/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40c
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/library/commerce/model/d;",
        "B:",
        "Lcom/twitter/library/commerce/model/d$a",
        "<TT;TB;>;>",
        "Lcom/twitter/library/commerce/model/CreditCard$b",
        "<TT;TB;>;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0}, Lcom/twitter/library/commerce/model/CreditCard$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/commerce/model/CreditCard$a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 165
    check-cast p2, Lcom/twitter/library/commerce/model/d$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/commerce/model/d$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/commerce/model/d$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/commerce/model/d$a;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/n;",
            "TB;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 181
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/library/commerce/model/CreditCard$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/commerce/model/CreditCard$a;I)V

    .line 182
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/library/commerce/model/d$a;->b(Ljava/lang/String;)Lcom/twitter/library/commerce/model/d$a;

    .line 183
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/library/commerce/model/d$a;->a(Ljava/lang/String;)Lcom/twitter/library/commerce/model/d$a;

    .line 184
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/library/commerce/model/d$a;->c(Ljava/lang/String;)Lcom/twitter/library/commerce/model/d$a;

    .line 185
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 165
    check-cast p2, Lcom/twitter/library/commerce/model/d$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/commerce/model/d$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/commerce/model/d$a;I)V

    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/CreditCard;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 165
    check-cast p2, Lcom/twitter/library/commerce/model/d;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/commerce/model/d$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/d;)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/o;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 171
    invoke-super {p0, p1, p2}, Lcom/twitter/library/commerce/model/CreditCard$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/CreditCard;)V

    .line 172
    invoke-virtual {p2}, Lcom/twitter/library/commerce/model/d;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 173
    invoke-virtual {p2}, Lcom/twitter/library/commerce/model/d;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 174
    invoke-virtual {p2}, Lcom/twitter/library/commerce/model/d;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 175
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 165
    check-cast p2, Lcom/twitter/library/commerce/model/d;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/commerce/model/d$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/d;)V

    return-void
.end method
