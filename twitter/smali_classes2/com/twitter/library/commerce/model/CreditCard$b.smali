.class public abstract Lcom/twitter/library/commerce/model/CreditCard$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/commerce/model/CreditCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40c
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/library/commerce/model/CreditCard;",
        "B:",
        "Lcom/twitter/library/commerce/model/CreditCard$a",
        "<TT;TB;>;>",
        "Lcom/twitter/util/serialization/b",
        "<TT;TB;>;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 559
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/commerce/model/CreditCard$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/n;",
            "TB;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 578
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/library/commerce/model/CreditCard$a;->d(Ljava/lang/String;)Lcom/twitter/library/commerce/model/CreditCard$a;

    move-result-object v0

    .line 579
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/commerce/model/CreditCard$a;->e(Ljava/lang/String;)Lcom/twitter/library/commerce/model/CreditCard$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/util/serialization/f;->c:Lcom/twitter/util/serialization/l;

    .line 580
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v1, v0}, Lcom/twitter/library/commerce/model/CreditCard$a;->a(Ljava/lang/Integer;)Lcom/twitter/library/commerce/model/CreditCard$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/util/serialization/f;->c:Lcom/twitter/util/serialization/l;

    .line 581
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v1, v0}, Lcom/twitter/library/commerce/model/CreditCard$a;->b(Ljava/lang/Integer;)Lcom/twitter/library/commerce/model/CreditCard$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/library/commerce/model/a;->a:Lcom/twitter/util/serialization/l;

    .line 582
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/commerce/model/a;

    invoke-virtual {v1, v0}, Lcom/twitter/library/commerce/model/CreditCard$a;->a(Lcom/twitter/library/commerce/model/a;)Lcom/twitter/library/commerce/model/CreditCard$a;

    move-result-object v0

    .line 583
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/commerce/model/CreditCard$a;->f(Ljava/lang/String;)Lcom/twitter/library/commerce/model/CreditCard$a;

    move-result-object v0

    .line 584
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/commerce/model/CreditCard$a;->a(Z)Lcom/twitter/library/commerce/model/CreditCard$a;

    move-result-object v0

    .line 585
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/commerce/model/CreditCard$a;->b(Z)Lcom/twitter/library/commerce/model/CreditCard$a;

    move-result-object v0

    .line 586
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/commerce/model/CreditCard$a;->c(Z)Lcom/twitter/library/commerce/model/CreditCard$a;

    .line 587
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 559
    check-cast p2, Lcom/twitter/library/commerce/model/CreditCard$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/commerce/model/CreditCard$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/commerce/model/CreditCard$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/CreditCard;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/o;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 564
    invoke-virtual {p2}, Lcom/twitter/library/commerce/model/CreditCard;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 565
    invoke-virtual {p2}, Lcom/twitter/library/commerce/model/CreditCard;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 566
    invoke-virtual {p2}, Lcom/twitter/library/commerce/model/CreditCard;->j()Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/twitter/util/serialization/f;->c:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 567
    invoke-virtual {p2}, Lcom/twitter/library/commerce/model/CreditCard;->k()Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/twitter/util/serialization/f;->c:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 568
    invoke-virtual {p2}, Lcom/twitter/library/commerce/model/CreditCard;->l()Lcom/twitter/library/commerce/model/a;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/commerce/model/a;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 569
    invoke-virtual {p2}, Lcom/twitter/library/commerce/model/CreditCard;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 570
    invoke-virtual {p2}, Lcom/twitter/library/commerce/model/CreditCard;->n()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 571
    invoke-virtual {p2}, Lcom/twitter/library/commerce/model/CreditCard;->o()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 572
    invoke-virtual {p2}, Lcom/twitter/library/commerce/model/CreditCard;->p()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    .line 573
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 559
    check-cast p2, Lcom/twitter/library/commerce/model/CreditCard;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/commerce/model/CreditCard$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/commerce/model/CreditCard;)V

    return-void
.end method
