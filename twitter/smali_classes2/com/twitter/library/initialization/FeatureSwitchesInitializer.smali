.class public Lcom/twitter/library/initialization/FeatureSwitchesInitializer;
.super Lanb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanb",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lanb;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;)Lcom/twitter/library/client/u;
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/twitter/library/initialization/FeatureSwitchesInitializer$2;

    invoke-direct {v0, p0}, Lcom/twitter/library/initialization/FeatureSwitchesInitializer$2;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 23
    invoke-static {p0}, Lcom/twitter/library/initialization/FeatureSwitchesInitializer;->b(Lcom/twitter/library/client/Session;)V

    return-void
.end method

.method private static b(Lcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lbqa;->a(J)V

    .line 74
    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/initialization/FeatureSwitchesInitializer;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 4

    .prologue
    .line 28
    invoke-static {p1}, Lbqa;->a(Landroid/content/Context;)V

    .line 29
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 30
    invoke-static {p1}, Lcom/twitter/library/initialization/FeatureSwitchesInitializer;->a(Landroid/content/Context;)Lcom/twitter/library/client/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/u;)V

    .line 31
    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/initialization/FeatureSwitchesInitializer;->b(Lcom/twitter/library/client/Session;)V

    .line 33
    invoke-static {}, Lcon;->a()Lrx/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/initialization/FeatureSwitchesInitializer$1;

    invoke-direct {v2, p0}, Lcom/twitter/library/initialization/FeatureSwitchesInitializer$1;-><init>(Lcom/twitter/library/initialization/FeatureSwitchesInitializer;)V

    invoke-virtual {v1, v2}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 41
    const-string/jumbo v1, "fs:first_download_req"

    .line 42
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v2

    sget-object v3, Lcom/twitter/metrics/g;->m:Lcom/twitter/metrics/g$b;

    .line 41
    invoke-static {v1, v2, v3}, Lcom/twitter/metrics/n;->b(Ljava/lang/String;Lcom/twitter/metrics/j;Lcom/twitter/metrics/g$b;)Lcom/twitter/metrics/n;

    move-result-object v1

    .line 43
    invoke-virtual {v0}, Lcom/twitter/library/client/v;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {v1}, Lcom/twitter/metrics/n;->j()V

    .line 47
    invoke-static {}, Lbpy;->a()V

    .line 51
    :goto_0
    return-void

    .line 49
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/metrics/n;->k()V

    goto :goto_0
.end method
