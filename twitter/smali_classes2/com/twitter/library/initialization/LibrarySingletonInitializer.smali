.class public Lcom/twitter/library/initialization/LibrarySingletonInitializer;
.super Lanb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanb",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lanb;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/initialization/LibrarySingletonInitializer;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 1

    .prologue
    .line 23
    invoke-static {p1}, Lcom/twitter/util/z;->a(Landroid/content/Context;)V

    .line 24
    invoke-static {}, Lcom/twitter/util/z;->b()F

    move-result v0

    invoke-static {v0}, Lcom/twitter/model/card/property/ImageSpec;->a(F)V

    .line 25
    invoke-static {}, Lcom/twitter/library/service/p;->a()V

    .line 27
    invoke-static {}, Lcom/twitter/library/network/narc/e;->a()V

    .line 30
    invoke-static {p1}, Lcom/twitter/library/client/f;->a(Landroid/content/Context;)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->a()V

    .line 32
    invoke-static {p1}, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;->a(Landroid/content/Context;)V

    .line 33
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;->a(Landroid/content/Context;Lcom/twitter/library/client/v;)V

    .line 35
    invoke-static {p1}, Lcni;->a(Landroid/content/Context;)V

    .line 36
    invoke-static {p1}, Lcom/twitter/library/client/h;->a(Landroid/content/Context;)Lcom/twitter/library/client/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/h;->a()V

    .line 37
    return-void
.end method
