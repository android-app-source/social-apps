.class public Lcom/twitter/library/initialization/PreferenceMigrationInitializer;
.super Lanb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanb",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lanb;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/util/List;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lakm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "auto_clean"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string/jumbo v1, "antispam_last_poll_timestamp"

    aput-object v1, v2, v0

    .line 47
    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "notifications_follow_only"

    aput-object v1, v3, v0

    .line 50
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 51
    const/4 v1, 0x0

    .line 52
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 53
    array-length v6, v2

    const/4 v0, 0x0

    move v14, v0

    move v0, v1

    move v1, v14

    :goto_0
    if-ge v1, v6, :cond_2

    aget-object v7, v2, v1

    .line 54
    invoke-interface {v4, v7}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 55
    const-wide/16 v8, 0x0

    invoke-interface {v4, v7, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 56
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    .line 57
    new-instance v11, Lcom/twitter/util/a;

    invoke-virtual {v0}, Lakm;->b()Lcnz;

    move-result-object v0

    invoke-virtual {v0}, Lcnz;->b()J

    move-result-wide v12

    invoke-direct {v11, p0, v12, v13}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 58
    invoke-virtual {v11}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    invoke-virtual {v0, v7, v8, v9}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;J)Lcom/twitter/util/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    goto :goto_1

    .line 60
    :cond_0
    invoke-interface {v5, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 61
    const/4 v0, 0x1

    .line 53
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 64
    :cond_2
    array-length v2, v3

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v2, :cond_5

    aget-object v6, v3, v1

    .line 65
    invoke-interface {v4, v6}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 66
    const/4 v0, 0x0

    invoke-interface {v4, v6, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    .line 67
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    .line 68
    new-instance v9, Lcom/twitter/util/a;

    invoke-virtual {v0}, Lakm;->b()Lcnz;

    move-result-object v0

    invoke-virtual {v0}, Lcnz;->b()J

    move-result-wide v10

    invoke-direct {v9, p0, v10, v11}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 69
    invoke-virtual {v9}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Z)Lcom/twitter/util/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    goto :goto_3

    .line 71
    :cond_3
    invoke-interface {v5, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 72
    const/4 v0, 0x1

    .line 64
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 75
    :cond_5
    if-eqz v0, :cond_6

    .line 76
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 78
    :cond_6
    return-void
.end method

.method private static b(Landroid/content/Context;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lakm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    .line 90
    invoke-virtual {v0}, Lakm;->d()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 92
    new-instance v3, Lcom/twitter/util/a;

    invoke-virtual {v0}, Lakm;->b()Lcnz;

    move-result-object v0

    invoke-virtual {v0}, Lcnz;->b()J

    move-result-wide v4

    invoke-direct {v3, p0, v4, v5}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 93
    invoke-static {v3, v2}, Lcqo;->a(Landroid/content/SharedPreferences;Landroid/content/SharedPreferences;)V

    goto :goto_0

    .line 95
    :cond_0
    return-void
.end method

.method private static c(Landroid/content/Context;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lakm;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 109
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    .line 110
    new-instance v2, Lcom/twitter/util/a;

    invoke-virtual {v0}, Lakm;->b()Lcnz;

    move-result-object v0

    invoke-virtual {v0}, Lcnz;->b()J

    move-result-wide v4

    invoke-direct {v2, p0, v4, v5}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 111
    const-string/jumbo v0, "launcher_icon_badge_behavior"

    invoke-virtual {v2, v0, v6}, Lcom/twitter/util/a;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_0

    .line 113
    invoke-virtual {v2}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v2

    .line 114
    const-string/jumbo v3, "badge_disabled"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    const-string/jumbo v0, "launcher_icon_badge_enabled"

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Z)Lcom/twitter/util/a$a;

    .line 117
    :cond_1
    const-string/jumbo v0, "launcher_icon_badge_behavior"

    invoke-virtual {v2, v0, v6}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/util/a$a;

    .line 118
    invoke-virtual {v2}, Lcom/twitter/util/a$a;->apply()V

    goto :goto_0

    .line 121
    :cond_2
    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/initialization/PreferenceMigrationInitializer;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 28
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Lcof;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 30
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v1

    invoke-virtual {v1}, Lakn;->c()Ljava/util/List;

    move-result-object v1

    .line 31
    const-string/jumbo v2, "is_using_account_preferences"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    .line 32
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 33
    invoke-static {p1, v1}, Lcom/twitter/library/initialization/PreferenceMigrationInitializer;->a(Landroid/content/Context;Ljava/util/List;)V

    .line 34
    invoke-static {p1, v1}, Lcom/twitter/library/initialization/PreferenceMigrationInitializer;->b(Landroid/content/Context;Ljava/util/List;)V

    .line 36
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "is_using_account_preferences"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 38
    :cond_1
    invoke-static {p1, v1}, Lcom/twitter/library/initialization/PreferenceMigrationInitializer;->c(Landroid/content/Context;Ljava/util/List;)V

    .line 39
    return-void
.end method
