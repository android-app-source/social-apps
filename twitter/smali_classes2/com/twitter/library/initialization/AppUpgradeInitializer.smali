.class public Lcom/twitter/library/initialization/AppUpgradeInitializer;
.super Lanb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanb",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 23
    const-string/jumbo v0, "masks"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "ramps"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "shaders"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/initialization/AppUpgradeInitializer;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lanb;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 44
    invoke-static {p0}, Lcom/twitter/library/initialization/AppUpgradeInitializer;->b(Landroid/content/Context;)V

    .line 45
    invoke-static {p0}, Lcom/twitter/library/initialization/AppUpgradeInitializer;->c(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method private static b(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 49
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v0

    invoke-virtual {v0}, Lakn;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    .line 50
    invoke-virtual {v0}, Lakm;->b()Lcnz;

    move-result-object v2

    invoke-virtual {v2}, Lcnz;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 51
    invoke-virtual {v0}, Lakm;->b()Lcnz;

    move-result-object v0

    invoke-virtual {v0}, Lcnz;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Lcom/twitter/library/provider/t;->c(Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 54
    :cond_1
    return-void
.end method

.method private static c(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 57
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    .line 58
    sget-object v0, Lcom/twitter/library/initialization/AppUpgradeInitializer;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 59
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v3}, Lcqc;->b(Ljava/io/File;)V

    goto :goto_0

    .line 61
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    return v0
.end method

.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/initialization/AppUpgradeInitializer;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 4

    .prologue
    .line 27
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lcof;->h()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 29
    const-string/jumbo v2, "app_v"

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 30
    invoke-virtual {v0}, Lcof;->p()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 31
    invoke-virtual {v0}, Lcof;->i()I

    move-result v0

    .line 32
    :goto_0
    if-ge v2, v0, :cond_0

    .line 33
    invoke-static {p1}, Lcom/twitter/library/initialization/AppUpgradeInitializer;->a(Landroid/content/Context;)V

    .line 34
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "app_v"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 36
    :cond_0
    return-void

    .line 31
    :cond_1
    invoke-static {p1}, Lcom/twitter/util/d;->d(Landroid/content/Context;)I

    move-result v0

    goto :goto_0
.end method
