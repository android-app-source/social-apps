.class public Lcom/twitter/library/api/geo/d;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcom/twitter/library/api/geo/e;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/model/geo/b;

.field private b:Lcom/twitter/library/api/geo/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/geo/b;)V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/twitter/library/api/geo/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 43
    iput-object p3, p0, Lcom/twitter/library/api/geo/d;->a:Lcom/twitter/model/geo/b;

    .line 44
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 4

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/twitter/library/api/geo/d;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "geo"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "reverse_geocode"

    aput-object v3, v1, v2

    .line 50
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "lat"

    iget-object v2, p0, Lcom/twitter/library/api/geo/d;->a:Lcom/twitter/model/geo/b;

    .line 51
    invoke-virtual {v2}, Lcom/twitter/model/geo/b;->a()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;D)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "long"

    iget-object v2, p0, Lcom/twitter/library/api/geo/d;->a:Lcom/twitter/model/geo/b;

    .line 52
    invoke-virtual {v2}, Lcom/twitter/model/geo/b;->b()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;D)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "granularity"

    const-string/jumbo v2, "city"

    .line 53
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    .line 49
    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/library/api/geo/e;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/library/service/b;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V

    .line 68
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/geo/e;

    iput-object v0, p0, Lcom/twitter/library/api/geo/d;->b:Lcom/twitter/library/api/geo/e;

    .line 71
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 25
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/api/geo/d;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/api/geo/f;
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/twitter/library/api/geo/f;

    invoke-direct {v0}, Lcom/twitter/library/api/geo/f;-><init>()V

    return-object v0
.end method

.method public e()Lcom/twitter/library/api/geo/e;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/library/api/geo/d;->b:Lcom/twitter/library/api/geo/e;

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/twitter/library/api/geo/d;->b()Lcom/twitter/library/api/geo/f;

    move-result-object v0

    return-object v0
.end method
