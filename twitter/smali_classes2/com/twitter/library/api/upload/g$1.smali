.class Lcom/twitter/library/api/upload/g$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/api/upload/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/api/upload/g;->a(Lcom/twitter/media/model/MediaFile;Ljava/util/List;Lcom/twitter/util/q;Lcom/twitter/library/api/upload/MediaUsage;)Lcom/twitter/library/api/upload/g$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcqt;

.field final synthetic b:J

.field final synthetic c:Lcom/twitter/library/api/upload/g$a;

.field final synthetic d:Lcom/twitter/library/api/upload/g;


# direct methods
.method constructor <init>(Lcom/twitter/library/api/upload/g;Lcqt;JLcom/twitter/library/api/upload/g$a;)V
    .locals 1

    .prologue
    .line 114
    iput-object p1, p0, Lcom/twitter/library/api/upload/g$1;->d:Lcom/twitter/library/api/upload/g;

    iput-object p2, p0, Lcom/twitter/library/api/upload/g$1;->a:Lcqt;

    iput-wide p3, p0, Lcom/twitter/library/api/upload/g$1;->b:J

    iput-object p5, p0, Lcom/twitter/library/api/upload/g$1;->c:Lcom/twitter/library/api/upload/g$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/api/upload/f;)V
    .locals 4

    .prologue
    .line 117
    iget-object v0, p0, Lcom/twitter/library/api/upload/g$1;->a:Lcqt;

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/library/api/upload/g$1;->b:J

    sub-long/2addr v0, v2

    invoke-static {p1, v0, v1}, Lcom/twitter/library/api/upload/g;->a(Lcom/twitter/library/api/upload/f;J)V

    .line 118
    iget-object v0, p0, Lcom/twitter/library/api/upload/g$1;->c:Lcom/twitter/library/api/upload/g$a;

    iput-object p1, v0, Lcom/twitter/library/api/upload/g$a;->a:Lcom/twitter/library/api/upload/f;

    .line 119
    invoke-virtual {p1}, Lcom/twitter/library/api/upload/f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/twitter/library/api/upload/g$1;->c:Lcom/twitter/library/api/upload/g$a;

    invoke-virtual {v0, p1}, Lcom/twitter/library/api/upload/g$a;->set(Ljava/lang/Object;)V

    .line 128
    :goto_0
    return-void

    .line 122
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/api/upload/f;->c()Ljava/lang/Exception;

    move-result-object v0

    if-nez v0, :cond_1

    .line 123
    iget-object v0, p0, Lcom/twitter/library/api/upload/g$1;->c:Lcom/twitter/library/api/upload/g$a;

    new-instance v1, Lcom/twitter/media/util/MediaException;

    invoke-virtual {p1}, Lcom/twitter/library/api/upload/f;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/media/util/MediaException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/g$a;->setException(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/api/upload/g$1;->c:Lcom/twitter/library/api/upload/g$a;

    invoke-virtual {p1}, Lcom/twitter/library/api/upload/f;->c()Ljava/lang/Exception;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/g$a;->setException(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
