.class public Lcom/twitter/library/api/upload/d;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Lcom/twitter/library/api/upload/internal/o;

.field private static final b:[I

.field private static final c:[I

.field private static final d:[I

.field private static final e:[I

.field private static f:Lcom/twitter/library/api/upload/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 25
    new-instance v0, Lcom/twitter/library/api/upload/internal/o;

    invoke-direct {v0}, Lcom/twitter/library/api/upload/internal/o;-><init>()V

    sput-object v0, Lcom/twitter/library/api/upload/d;->a:Lcom/twitter/library/api/upload/internal/o;

    .line 30
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/twitter/library/api/upload/d;->b:[I

    .line 31
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/twitter/library/api/upload/d;->c:[I

    .line 33
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/twitter/library/api/upload/d;->d:[I

    .line 34
    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/twitter/library/api/upload/d;->e:[I

    .line 36
    new-instance v0, Lcom/twitter/library/api/upload/d;

    invoke-direct {v0}, Lcom/twitter/library/api/upload/d;-><init>()V

    sput-object v0, Lcom/twitter/library/api/upload/d;->f:Lcom/twitter/library/api/upload/d;

    return-void

    .line 30
    :array_0
    .array-data 4
        0x50
        0x50
        0x50
    .end array-data

    .line 31
    :array_1
    .array-data 4
        0x190
        0xc8
        0xc8
    .end array-data

    .line 33
    :array_2
    .array-data 4
        0x50
        0x50
        0x50
    .end array-data

    .line 34
    :array_3
    .array-data 4
        0x5dc
        0x5dc
        0x5dc
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a()Z
    .locals 2

    .prologue
    .line 111
    const-string/jumbo v0, "android_high_upload_photo_limit"

    invoke-static {v0}, Lcoi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112
    const-string/jumbo v1, "no_upper_upload_limit"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;Lcom/twitter/media/model/MediaFile;Lcom/twitter/library/api/upload/MediaUsage;J)Lcom/twitter/library/api/upload/c;
    .locals 7

    .prologue
    .line 121
    sget-object v0, Lcom/twitter/library/api/upload/d;->f:Lcom/twitter/library/api/upload/d;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/api/upload/d;->a(Landroid/content/Context;Lcom/twitter/media/model/MediaFile;Lcom/twitter/library/api/upload/MediaUsage;J)Lcom/twitter/library/api/upload/c;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/twitter/media/model/MediaFile;Lcom/twitter/library/api/upload/MediaUsage;J)Lcom/twitter/library/api/upload/c;
    .locals 8

    .prologue
    const/high16 v4, 0x300000

    .line 58
    sget-object v0, Lcom/twitter/library/api/upload/d$1;->b:[I

    iget-object v1, p2, Lcom/twitter/media/model/MediaFile;->g:Lcom/twitter/media/model/MediaType;

    invoke-virtual {v1}, Lcom/twitter/media/model/MediaType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 98
    new-instance v0, Lcom/twitter/library/api/upload/i;

    invoke-direct {v0, p2}, Lcom/twitter/library/api/upload/i;-><init>(Lcom/twitter/media/model/MediaFile;)V

    :goto_0
    return-object v0

    .line 63
    :pswitch_0
    const/4 v0, 0x0

    .line 64
    sget-object v1, Lcom/twitter/library/api/upload/d$1;->a:[I

    invoke-virtual {p3}, Lcom/twitter/library/api/upload/MediaUsage;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 83
    invoke-static {}, Lcom/twitter/library/api/upload/d;->a()Z

    move-result v1

    .line 84
    sget-object v0, Lcom/twitter/library/api/upload/d;->a:Lcom/twitter/library/api/upload/internal/o;

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/internal/o;->a()Lcom/twitter/util/collection/Pair;

    move-result-object v2

    .line 85
    invoke-virtual {v2}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->d(Ljava/util/Collection;)[I

    move-result-object v3

    .line 86
    invoke-virtual {v2}, Lcom/twitter/util/collection/Pair;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->d(Ljava/util/Collection;)[I

    move-result-object v2

    move v0, v1

    .line 90
    :goto_1
    if-eqz v0, :cond_0

    :goto_2
    move-object v5, p2

    .line 91
    check-cast v5, Lcom/twitter/media/model/ImageFile;

    .line 92
    new-instance v0, Lcom/twitter/library/api/upload/b;

    move-object v1, p1

    move-wide v6, p4

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/api/upload/b;-><init>(Landroid/content/Context;[I[IILcom/twitter/media/model/ImageFile;J)V

    goto :goto_0

    .line 66
    :pswitch_1
    const v4, 0xaf000

    .line 67
    sget-object v3, Lcom/twitter/library/api/upload/d;->b:[I

    .line 68
    sget-object v2, Lcom/twitter/library/api/upload/d;->c:[I

    goto :goto_1

    .line 74
    :pswitch_2
    sget-object v3, Lcom/twitter/library/api/upload/d;->d:[I

    .line 75
    sget-object v2, Lcom/twitter/library/api/upload/d;->e:[I

    goto :goto_1

    .line 90
    :cond_0
    invoke-static {}, Lcrs;->a()Lcrs;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcrs;->a(I)I

    move-result v4

    goto :goto_2

    .line 58
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 64
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
