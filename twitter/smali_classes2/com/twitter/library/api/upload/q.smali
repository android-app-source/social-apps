.class public Lcom/twitter/library/api/upload/q;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/service/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/api/upload/x;

.field private final b:Lcom/twitter/util/concurrent/ObservablePromise;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/concurrent/ObservablePromise",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/upload/x;Lcom/twitter/util/concurrent/ObservablePromise;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/twitter/library/client/Session;",
            "Lcom/twitter/library/api/upload/x;",
            "Lcom/twitter/util/concurrent/ObservablePromise",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 54
    iput-object p4, p0, Lcom/twitter/library/api/upload/q;->a:Lcom/twitter/library/api/upload/x;

    .line 55
    iput-object p5, p0, Lcom/twitter/library/api/upload/q;->b:Lcom/twitter/util/concurrent/ObservablePromise;

    .line 56
    return-void
.end method

.method private b()Ljava/lang/String;
    .locals 9

    .prologue
    .line 89
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 91
    :try_start_0
    const-string/jumbo v2, "media_id"

    iget-object v3, p0, Lcom/twitter/library/api/upload/q;->a:Lcom/twitter/library/api/upload/x;

    invoke-virtual {v3}, Lcom/twitter/library/api/upload/x;->c()J

    move-result-wide v6

    invoke-virtual {v4, v2, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 92
    iget-object v2, p0, Lcom/twitter/library/api/upload/q;->a:Lcom/twitter/library/api/upload/x;

    invoke-virtual {v2}, Lcom/twitter/library/api/upload/x;->b()Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v2

    .line 93
    iget-object v3, v2, Lcom/twitter/model/drafts/DraftAttachment;->h:Lcom/twitter/model/media/MediaSource;

    invoke-virtual {v3}, Lcom/twitter/model/media/MediaSource;->d()Lcom/twitter/model/media/foundmedia/FoundMediaOrigin;

    move-result-object v3

    .line 94
    if-eqz v3, :cond_0

    .line 95
    const-string/jumbo v5, "found_media_origin"

    invoke-virtual {v3}, Lcom/twitter/model/media/foundmedia/FoundMediaOrigin;->a()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 97
    :cond_0
    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/twitter/model/drafts/DraftAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v3

    .line 98
    instance-of v2, v3, Lcom/twitter/model/core/a;

    if-eqz v2, :cond_1

    .line 99
    move-object v0, v3

    check-cast v0, Lcom/twitter/model/core/a;

    move-object v2, v0

    invoke-interface {v2}, Lcom/twitter/model/core/a;->bv_()Ljava/lang/String;

    move-result-object v2

    .line 100
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-nez v5, :cond_1

    .line 102
    :try_start_1
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 103
    const-string/jumbo v6, "text"

    invoke-virtual {v5, v6, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 104
    const-string/jumbo v2, "alt_text"

    invoke-virtual {v4, v2, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 109
    :cond_1
    :goto_0
    :try_start_2
    instance-of v2, v3, Lcom/twitter/model/media/EditableImage;

    if-eqz v2, :cond_3

    .line 110
    check-cast v3, Lcom/twitter/model/media/EditableImage;

    .line 111
    iget-object v5, v3, Lcom/twitter/model/media/EditableImage;->h:Ljava/util/List;

    .line 112
    invoke-static {v5}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 113
    iget-object v2, v3, Lcom/twitter/model/media/EditableImage;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v2, Lcom/twitter/media/model/ImageFile;

    iget-object v2, v2, Lcom/twitter/media/model/ImageFile;->f:Lcom/twitter/util/math/Size;

    invoke-virtual {v2}, Lcom/twitter/util/math/Size;->g()F

    move-result v6

    .line 114
    iget-object v7, v3, Lcom/twitter/model/media/EditableImage;->f:Lcom/twitter/util/math/c;

    .line 115
    iget v3, v3, Lcom/twitter/model/media/EditableImage;->e:I

    .line 116
    new-instance v8, Lorg/json/JSONArray;

    invoke-direct {v8}, Lorg/json/JSONArray;-><init>()V

    .line 117
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcdy;

    .line 118
    invoke-virtual {v2, v6, v7, v3}, Lcdy;->b(FLcom/twitter/util/math/c;I)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v8, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 125
    :catch_0
    move-exception v2

    .line 126
    invoke-static {v2}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 127
    const-string/jumbo v2, ""

    .line 129
    :goto_2
    return-object v2

    .line 120
    :cond_2
    :try_start_3
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 121
    const-string/jumbo v3, "stickers"

    invoke-virtual {v2, v3, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 122
    const-string/jumbo v3, "sticker_info"

    invoke-virtual {v4, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    .line 129
    :cond_3
    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 105
    :catch_1
    move-exception v2

    goto :goto_0
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 4

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/twitter/library/api/upload/q;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 75
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "media/metadata/create"

    aput-object v3, v1, v2

    .line 76
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    new-instance v1, Lcom/twitter/network/apache/entity/c;

    .line 77
    invoke-direct {p0}, Lcom/twitter/library/api/upload/q;->b()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/twitter/network/apache/a;->a:Ljava/nio/charset/Charset;

    invoke-direct {v1, v2, v3}, Lcom/twitter/network/apache/entity/c;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/apache/e;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    .line 74
    return-object v0
.end method

.method public a(Lcom/twitter/async/service/j;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-super {p0, p1}, Lcom/twitter/library/service/b;->a(Lcom/twitter/async/service/j;)V

    .line 62
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 63
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 64
    :cond_0
    iget-object v1, p0, Lcom/twitter/library/api/upload/q;->b:Lcom/twitter/util/concurrent/ObservablePromise;

    invoke-virtual {v1, v0}, Lcom/twitter/util/concurrent/ObservablePromise;->set(Ljava/lang/Object;)V

    .line 69
    :goto_0
    return-void

    .line 66
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->c()Ljava/lang/Exception;

    move-result-object v0

    .line 67
    iget-object v1, p0, Lcom/twitter/library/api/upload/q;->b:Lcom/twitter/util/concurrent/ObservablePromise;

    if-eqz v0, :cond_2

    :goto_1
    invoke-virtual {v1, v0}, Lcom/twitter/util/concurrent/ObservablePromise;->setException(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/Exception;

    const-string/jumbo v2, "cannot upload media metadata data"

    invoke-direct {v0, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return-object v0
.end method
