.class public Lcom/twitter/library/api/upload/m;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/api/upload/m$a;,
        Lcom/twitter/library/api/upload/m$b;,
        Lcom/twitter/library/api/upload/m$c;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/api/upload/g;

.field private final b:Lcom/twitter/library/api/upload/m$a;


# direct methods
.method public constructor <init>(Lcom/twitter/library/api/upload/g;Lcom/twitter/library/api/upload/m$a;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/twitter/library/api/upload/m;->a:Lcom/twitter/library/api/upload/g;

    .line 39
    iput-object p2, p0, Lcom/twitter/library/api/upload/m;->b:Lcom/twitter/library/api/upload/m$a;

    .line 40
    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;Lcom/twitter/media/model/MediaType;Lcom/twitter/library/api/upload/MediaUsage;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/twitter/media/model/MediaType;",
            "Lcom/twitter/library/api/upload/MediaUsage;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/library/api/upload/m$c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/library/api/upload/m;->b:Lcom/twitter/library/api/upload/m$a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/api/upload/m$a;->a(Landroid/net/Uri;Lcom/twitter/media/model/MediaType;)Lrx/g;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lrx/g;->c()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/api/upload/m$1;

    invoke-direct {v1, p0, p3}, Lcom/twitter/library/api/upload/m$1;-><init>(Lcom/twitter/library/api/upload/m;Lcom/twitter/library/api/upload/MediaUsage;)V

    .line 60
    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 58
    return-object v0
.end method

.method public a(Lcom/twitter/media/model/MediaFile;Lcom/twitter/library/api/upload/MediaUsage;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/media/model/MediaFile;",
            "Lcom/twitter/library/api/upload/MediaUsage;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/library/api/upload/m$c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    new-instance v0, Lcom/twitter/library/api/upload/m$b;

    iget-object v1, p0, Lcom/twitter/library/api/upload/m;->a:Lcom/twitter/library/api/upload/g;

    invoke-direct {v0, v1, p1, p2}, Lcom/twitter/library/api/upload/m$b;-><init>(Lcom/twitter/library/api/upload/g;Lcom/twitter/media/model/MediaFile;Lcom/twitter/library/api/upload/MediaUsage;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/net/Uri;Lcom/twitter/media/model/MediaType;Lcom/twitter/library/api/upload/MediaUsage;)Lrx/g;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/twitter/media/model/MediaType;",
            "Lcom/twitter/library/api/upload/MediaUsage;",
            ")",
            "Lrx/g",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/api/upload/m;->a(Landroid/net/Uri;Lcom/twitter/media/model/MediaType;Lcom/twitter/library/api/upload/MediaUsage;)Lrx/c;

    move-result-object v0

    .line 80
    new-instance v1, Lcom/twitter/library/api/upload/m$3;

    invoke-direct {v1, p0}, Lcom/twitter/library/api/upload/m$3;-><init>(Lcom/twitter/library/api/upload/m;)V

    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/api/upload/m$2;

    invoke-direct {v1, p0}, Lcom/twitter/library/api/upload/m$2;-><init>(Lcom/twitter/library/api/upload/m;)V

    .line 88
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 95
    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcre;->a(Lrx/c;Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    return-object v0
.end method
