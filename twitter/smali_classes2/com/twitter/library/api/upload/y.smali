.class public Lcom/twitter/library/api/upload/y;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/twitter/library/service/v;

.field public c:Ljava/lang/StringBuilder;

.field public d:Lcom/twitter/network/HttpOperation$RequestMethod;

.field public e:Lcom/twitter/network/j;

.field public f:I

.field public g:Lcom/twitter/network/apache/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/twitter/library/api/upload/y;->a:Landroid/content/Context;

    .line 41
    iput-object p2, p0, Lcom/twitter/library/api/upload/y;->b:Lcom/twitter/library/service/v;

    .line 42
    sget-object v0, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    iput-object v0, p0, Lcom/twitter/library/api/upload/y;->d:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 43
    const v0, 0xea60

    iput v0, p0, Lcom/twitter/library/api/upload/y;->f:I

    .line 44
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/network/j;)Lcom/twitter/library/api/upload/y;
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/twitter/library/api/upload/y;->e:Lcom/twitter/network/j;

    .line 79
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/api/upload/y;
    .locals 2

    .prologue
    .line 92
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    new-instance v0, Lcom/twitter/network/apache/entity/c;

    sget-object v1, Lcom/twitter/network/apache/a;->a:Ljava/nio/charset/Charset;

    invoke-direct {v0, p1, v1}, Lcom/twitter/network/apache/entity/c;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V

    iput-object v0, p0, Lcom/twitter/library/api/upload/y;->g:Lcom/twitter/network/apache/e;

    .line 94
    iget-object v0, p0, Lcom/twitter/library/api/upload/y;->g:Lcom/twitter/network/apache/e;

    check-cast v0, Lcom/twitter/network/apache/entity/c;

    const-string/jumbo v1, "application/x-www-form-urlencoded"

    invoke-virtual {v0, v1}, Lcom/twitter/network/apache/entity/c;->a(Ljava/lang/String;)V

    .line 96
    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/String;Landroid/net/Uri;)Lcom/twitter/library/api/upload/y;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 103
    .line 105
    if-eqz p2, :cond_0

    .line 107
    :try_start_0
    new-instance v0, Lcom/twitter/library/network/n;

    iget-object v2, p0, Lcom/twitter/library/api/upload/y;->a:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/twitter/library/network/n;-><init>(Landroid/content/Context;Lcom/twitter/network/s;)V

    .line 108
    const/16 v2, 0x8

    invoke-static {v2}, Lcom/twitter/util/y;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v2, p2}, Lcom/twitter/library/network/n;->a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 109
    invoke-virtual {v0}, Lcom/twitter/library/network/n;->d()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :goto_0
    iput-object v0, p0, Lcom/twitter/library/api/upload/y;->g:Lcom/twitter/network/apache/e;

    .line 117
    return-object p0

    .line 110
    :catch_0
    move-exception v0

    .line 111
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 112
    goto :goto_0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public a(Ljava/lang/StringBuilder;)Lcom/twitter/library/api/upload/y;
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/twitter/library/api/upload/y;->c:Ljava/lang/StringBuilder;

    .line 69
    return-object p0
.end method

.method public a(Ljava/util/ArrayList;)Lcom/twitter/library/api/upload/y;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/network/apache/message/BasicNameValuePair;",
            ">;)",
            "Lcom/twitter/library/api/upload/y;"
        }
    .end annotation

    .prologue
    .line 84
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    invoke-static {p1}, Lcom/twitter/library/util/af;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/api/upload/y;->a(Ljava/lang/String;)Lcom/twitter/library/api/upload/y;

    .line 87
    :cond_0
    return-object p0
.end method

.method public a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;)Lcom/twitter/library/service/u;
    .locals 1

    .prologue
    .line 165
    if-eqz p1, :cond_0

    .line 166
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/u;->a(Lcom/twitter/network/HttpOperation;)V

    .line 171
    :goto_0
    return-object p2

    .line 168
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/twitter/library/service/u;->a(Z)V

    goto :goto_0
.end method

.method public a()Lcom/twitter/network/HttpOperation;
    .locals 4

    .prologue
    .line 145
    iget-object v0, p0, Lcom/twitter/library/api/upload/y;->c:Ljava/lang/StringBuilder;

    if-nez v0, :cond_0

    .line 146
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 149
    :cond_0
    new-instance v0, Lcom/twitter/library/network/k;

    iget-object v1, p0, Lcom/twitter/library/api/upload/y;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/api/upload/y;->c:Ljava/lang/StringBuilder;

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/network/k;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/twitter/library/api/upload/y;->b:Lcom/twitter/library/service/v;

    iget-wide v2, v1, Lcom/twitter/library/service/v;->c:J

    .line 150
    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/network/k;->a(J)Lcom/twitter/library/network/k;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/upload/y;->d:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 151
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/k;

    move-result-object v0

    const-string/jumbo v1, "Uploads are always triggered by a user action."

    .line 152
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->b(Ljava/lang/String;)Lcom/twitter/library/network/k;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/t;

    iget-object v2, p0, Lcom/twitter/library/api/upload/y;->b:Lcom/twitter/library/service/v;

    iget-object v2, v2, Lcom/twitter/library/service/v;->d:Lcom/twitter/model/account/OAuthToken;

    invoke-direct {v1, v2}, Lcom/twitter/library/network/t;-><init>(Lcom/twitter/model/account/OAuthToken;)V

    .line 153
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/k;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/upload/y;->e:Lcom/twitter/network/j;

    .line 154
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/j;)Lcom/twitter/library/network/k;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/upload/y;->g:Lcom/twitter/network/apache/e;

    .line 155
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/apache/e;)Lcom/twitter/library/network/k;

    move-result-object v0

    .line 156
    invoke-virtual {v0}, Lcom/twitter/library/network/k;->a()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    .line 158
    iget v1, p0, Lcom/twitter/library/api/upload/y;->f:I

    if-lez v1, :cond_1

    .line 159
    iget v1, p0, Lcom/twitter/library/api/upload/y;->f:I

    invoke-virtual {v0, v1}, Lcom/twitter/network/HttpOperation;->a(I)V

    .line 161
    :cond_1
    return-object v0
.end method
