.class public Lcom/twitter/library/api/upload/internal/i;
.super Lcom/twitter/library/api/upload/internal/a;
.source "Twttr"


# instance fields
.field final d:Lcom/twitter/media/model/MediaFile;

.field e:J

.field f:I

.field g:I

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final i:Lcom/twitter/library/api/upload/MediaUsage;

.field private final j:Z

.field private final k:Landroid/os/Handler;

.field private final l:I

.field private m:J

.field private n:Lcom/twitter/library/util/l;

.field private o:Ljava/io/RandomAccessFile;

.field private p:I


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/media/model/MediaFile;Lcom/twitter/library/api/upload/e;Lcom/twitter/util/q;ILjava/util/List;Lcom/twitter/library/api/upload/MediaUsage;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/service/v;",
            "Lcom/twitter/media/model/MediaFile;",
            "Lcom/twitter/library/api/upload/e;",
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/library/api/progress/ProgressUpdatedEvent;",
            ">;I",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/twitter/library/api/upload/MediaUsage;",
            ")V"
        }
    .end annotation

    .prologue
    .line 82
    invoke-direct {p0, p1, p2, p4, p5}, Lcom/twitter/library/api/upload/internal/a;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/library/api/upload/e;Lcom/twitter/util/q;)V

    .line 59
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/library/api/upload/internal/i;->e:J

    .line 66
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/library/api/upload/internal/i;->k:Landroid/os/Handler;

    .line 72
    const/16 v0, 0x14

    iput v0, p0, Lcom/twitter/library/api/upload/internal/i;->p:I

    .line 83
    iput p6, p0, Lcom/twitter/library/api/upload/internal/i;->l:I

    .line 84
    iput-object p3, p0, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    .line 85
    iput-object p7, p0, Lcom/twitter/library/api/upload/internal/i;->h:Ljava/util/List;

    .line 86
    invoke-static {p3}, Lcom/twitter/library/api/upload/internal/i;->a(Lcom/twitter/media/model/MediaFile;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/api/upload/internal/i;->j:Z

    .line 87
    iput-object p8, p0, Lcom/twitter/library/api/upload/internal/i;->i:Lcom/twitter/library/api/upload/MediaUsage;

    .line 88
    return-void
.end method

.method private a(I)V
    .locals 6

    .prologue
    .line 229
    iget v0, p0, Lcom/twitter/library/api/upload/internal/i;->p:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/twitter/library/api/upload/internal/i;->p:I

    .line 230
    iget v0, p0, Lcom/twitter/library/api/upload/internal/i;->p:I

    if-nez v0, :cond_0

    .line 231
    new-instance v0, Lcom/twitter/library/api/upload/f;

    iget-object v1, p0, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    const/16 v2, 0x3ed

    new-instance v3, Ljava/lang/Exception;

    const-string/jumbo v4, "too many status polls"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/upload/f;-><init>(Lcom/twitter/media/model/MediaFile;ILjava/lang/Exception;)V

    .line 233
    new-instance v1, Lcom/twitter/library/api/upload/f;

    iget-object v2, p0, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    iget-wide v4, p0, Lcom/twitter/library/api/upload/internal/i;->e:J

    invoke-direct {v1, v0, v2, v4, v5}, Lcom/twitter/library/api/upload/f;-><init>(Lcom/twitter/library/service/u;Lcom/twitter/media/model/MediaFile;J)V

    invoke-virtual {p0, v1}, Lcom/twitter/library/api/upload/internal/i;->b(Lcom/twitter/library/api/upload/f;)V

    .line 251
    :goto_0
    return-void

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/library/api/upload/internal/i$3;

    invoke-direct {v1, p0}, Lcom/twitter/library/api/upload/internal/i$3;-><init>(Lcom/twitter/library/api/upload/internal/i;)V

    const/4 v2, 0x0

    .line 250
    invoke-static {p1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    .line 236
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/util/x;IJLjava/lang/String;)V
    .locals 13

    .prologue
    .line 285
    new-instance v0, Lcom/twitter/library/api/upload/internal/j;

    iget-object v1, p0, Lcom/twitter/library/api/upload/internal/i;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/api/upload/internal/i;->b:Lcom/twitter/library/service/v;

    iget-object v3, p0, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    iget-wide v4, p0, Lcom/twitter/library/api/upload/internal/i;->e:J

    iget-boolean v11, p0, Lcom/twitter/library/api/upload/internal/i;->j:Z

    move-object v6, p1

    move v7, p2

    move-wide/from16 v8, p3

    move-object/from16 v10, p5

    invoke-direct/range {v0 .. v11}, Lcom/twitter/library/api/upload/internal/j;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/media/model/MediaFile;JLcom/twitter/library/util/x;IJLjava/lang/String;Z)V

    .line 296
    new-instance v1, Lcom/twitter/library/api/upload/internal/i$4;

    invoke-direct {v1, p0}, Lcom/twitter/library/api/upload/internal/i$4;-><init>(Lcom/twitter/library/api/upload/internal/i;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/internal/j;->a(Lcom/twitter/library/api/upload/internal/m$a;)V

    .line 318
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 319
    return-void
.end method

.method private static a(Lcom/twitter/media/model/MediaFile;)Z
    .locals 2

    .prologue
    .line 330
    sget-object v0, Lcom/twitter/library/api/upload/internal/i$5;->a:[I

    iget-object v1, p0, Lcom/twitter/media/model/MediaFile;->g:Lcom/twitter/media/model/MediaType;

    invoke-virtual {v1}, Lcom/twitter/media/model/MediaType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 339
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 333
    :pswitch_0
    invoke-static {}, Lcom/twitter/library/api/upload/internal/i;->h()Z

    move-result v0

    goto :goto_0

    .line 336
    :pswitch_1
    const-string/jumbo v0, "media_async_upload_gif_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 330
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private e()V
    .locals 9

    .prologue
    .line 126
    const/16 v0, 0xfa

    const/16 v1, 0x2710

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/api/upload/internal/i;->a(II)V

    .line 128
    new-instance v0, Lcom/twitter/library/api/upload/internal/l;

    iget-object v1, p0, Lcom/twitter/library/api/upload/internal/i;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/api/upload/internal/i;->b:Lcom/twitter/library/service/v;

    iget-object v3, p0, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    iget-wide v4, p0, Lcom/twitter/library/api/upload/internal/i;->m:J

    iget-object v6, p0, Lcom/twitter/library/api/upload/internal/i;->h:Ljava/util/List;

    iget-object v7, p0, Lcom/twitter/library/api/upload/internal/i;->i:Lcom/twitter/library/api/upload/MediaUsage;

    iget-boolean v8, p0, Lcom/twitter/library/api/upload/internal/i;->j:Z

    invoke-direct/range {v0 .. v8}, Lcom/twitter/library/api/upload/internal/l;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/media/model/MediaFile;JLjava/util/List;Lcom/twitter/library/api/upload/MediaUsage;Z)V

    .line 136
    new-instance v1, Lcom/twitter/library/api/upload/internal/i$1;

    invoke-direct {v1, p0}, Lcom/twitter/library/api/upload/internal/i$1;-><init>(Lcom/twitter/library/api/upload/internal/i;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/internal/l;->a(Lcom/twitter/library/api/upload/internal/m$a;)V

    .line 170
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 171
    return-void
.end method

.method private f()V
    .locals 7

    .prologue
    .line 213
    new-instance v0, Lcom/twitter/library/api/upload/internal/k;

    iget-object v1, p0, Lcom/twitter/library/api/upload/internal/i;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/api/upload/internal/i;->b:Lcom/twitter/library/service/v;

    iget-object v3, p0, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    iget-wide v4, p0, Lcom/twitter/library/api/upload/internal/i;->e:J

    iget-boolean v6, p0, Lcom/twitter/library/api/upload/internal/i;->j:Z

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/api/upload/internal/k;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/media/model/MediaFile;JZ)V

    .line 219
    new-instance v1, Lcom/twitter/library/api/upload/internal/i$2;

    invoke-direct {v1, p0}, Lcom/twitter/library/api/upload/internal/i$2;-><init>(Lcom/twitter/library/api/upload/internal/i;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/internal/k;->a(Lcom/twitter/library/api/upload/internal/m$a;)V

    .line 225
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 226
    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i;->o:Ljava/io/RandomAccessFile;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 326
    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i;->n:Lcom/twitter/library/util/l;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 327
    return-void
.end method

.method private static h()Z
    .locals 1

    .prologue
    .line 345
    const-string/jumbo v0, "media_async_upload_video_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 346
    invoke-static {}, Lcom/twitter/media/util/d;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 345
    :goto_0
    return v0

    .line 346
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/16 v4, 0x3f0

    .line 94
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    iget-object v0, v0, Lcom/twitter/media/model/MediaFile;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/upload/internal/i;->m:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    iget-wide v0, p0, Lcom/twitter/library/api/upload/internal/i;->m:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    new-instance v1, Ljava/io/IOException;

    const-string/jumbo v2, "EditableMedia fileSize is empty"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v4, v1}, Lcom/twitter/library/api/upload/internal/i;->a(Lcom/twitter/media/model/MediaFile;ILjava/lang/Exception;)V

    .line 107
    :goto_0
    return-void

    .line 95
    :catch_0
    move-exception v0

    .line 96
    iget-object v1, p0, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    invoke-virtual {p0, v1, v4, v0}, Lcom/twitter/library/api/upload/internal/i;->a(Lcom/twitter/media/model/MediaFile;ILjava/lang/Exception;)V

    goto :goto_0

    .line 106
    :cond_0
    invoke-direct {p0}, Lcom/twitter/library/api/upload/internal/i;->e()V

    goto :goto_0
.end method

.method a(Lcdp;Lcom/twitter/async/service/j;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcdp;",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v1, 0x2710

    .line 254
    if-nez p1, :cond_1

    const/4 v0, 0x2

    .line 255
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 267
    if-eqz p1, :cond_2

    iget-object v0, p1, Lcdp;->f:Lcdo;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcdp;->f:Lcdo;

    iget-object v0, v0, Lcdo;->c:Ljava/lang/String;

    move-object v1, v0

    .line 270
    :goto_1
    invoke-virtual {p2}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 271
    if-nez v0, :cond_0

    .line 272
    new-instance v0, Lcom/twitter/library/api/upload/f;

    iget-object v2, p0, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    const/16 v3, 0x3ed

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v2, v3, v4}, Lcom/twitter/library/api/upload/f;-><init>(Lcom/twitter/media/model/MediaFile;ILjava/lang/Exception;)V

    .line 275
    :cond_0
    new-instance v1, Lcom/twitter/library/api/upload/f;

    iget-object v2, p0, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    iget-wide v4, p0, Lcom/twitter/library/api/upload/internal/i;->e:J

    invoke-direct {v1, v0, v2, v4, v5}, Lcom/twitter/library/api/upload/f;-><init>(Lcom/twitter/library/service/u;Lcom/twitter/media/model/MediaFile;J)V

    invoke-virtual {p0, v1}, Lcom/twitter/library/api/upload/internal/i;->b(Lcom/twitter/library/api/upload/f;)V

    .line 279
    :goto_2
    return-void

    .line 254
    :cond_1
    iget v0, p1, Lcdp;->a:I

    goto :goto_0

    .line 257
    :pswitch_0
    iget v0, p1, Lcdp;->e:I

    invoke-direct {p0, v0}, Lcom/twitter/library/api/upload/internal/i;->a(I)V

    goto :goto_2

    .line 261
    :pswitch_1
    invoke-virtual {p0, v1, v1}, Lcom/twitter/library/api/upload/internal/i;->a(II)V

    .line 262
    invoke-virtual {p2}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 263
    new-instance v1, Lcom/twitter/library/api/upload/f;

    iget-object v2, p0, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    iget-wide v4, p0, Lcom/twitter/library/api/upload/internal/i;->e:J

    invoke-direct {v1, v0, v2, v4, v5}, Lcom/twitter/library/api/upload/f;-><init>(Lcom/twitter/library/service/u;Lcom/twitter/media/model/MediaFile;J)V

    invoke-virtual {p0, v1}, Lcom/twitter/library/api/upload/internal/i;->b(Lcom/twitter/library/api/upload/f;)V

    goto :goto_2

    .line 267
    :cond_2
    const-string/jumbo v0, "failed"

    move-object v1, v0

    goto :goto_1

    .line 255
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/twitter/library/api/upload/internal/i;->j:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "binary_async"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "upload"

    goto :goto_0
.end method

.method protected b(Lcom/twitter/library/api/upload/f;)V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/twitter/library/api/upload/internal/i;->g()V

    .line 119
    invoke-super {p0, p1}, Lcom/twitter/library/api/upload/internal/a;->b(Lcom/twitter/library/api/upload/f;)V

    .line 120
    return-void
.end method

.method c()V
    .locals 6

    .prologue
    .line 175
    :try_start_0
    new-instance v0, Ljava/io/RandomAccessFile;

    iget-object v1, p0, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    iget-object v1, v1, Lcom/twitter/media/model/MediaFile;->e:Ljava/io/File;

    const-string/jumbo v2, "r"

    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/library/api/upload/internal/i;->o:Ljava/io/RandomAccessFile;

    .line 176
    new-instance v0, Lcom/twitter/library/util/l;

    iget-object v1, p0, Lcom/twitter/library/api/upload/internal/i;->o:Ljava/io/RandomAccessFile;

    iget-wide v2, p0, Lcom/twitter/library/api/upload/internal/i;->m:J

    iget v4, p0, Lcom/twitter/library/api/upload/internal/i;->l:I

    int-to-long v4, v4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/util/l;-><init>(Ljava/io/RandomAccessFile;JJ)V

    iput-object v0, p0, Lcom/twitter/library/api/upload/internal/i;->n:Lcom/twitter/library/util/l;

    .line 177
    iget-boolean v0, p0, Lcom/twitter/library/api/upload/internal/i;->j:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x3e8

    :goto_0
    rsub-int v0, v0, 0x1f40

    .line 180
    iget-object v1, p0, Lcom/twitter/library/api/upload/internal/i;->n:Lcom/twitter/library/util/l;

    invoke-virtual {v1}, Lcom/twitter/library/util/l;->a()I

    move-result v1

    div-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/library/api/upload/internal/i;->f:I

    .line 181
    invoke-virtual {p0}, Lcom/twitter/library/api/upload/internal/i;->d()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    :goto_1
    return-void

    .line 177
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 182
    :catch_0
    move-exception v0

    .line 183
    invoke-direct {p0}, Lcom/twitter/library/api/upload/internal/i;->g()V

    .line 184
    iget-object v1, p0, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    const/16 v2, 0x3f0

    invoke-virtual {p0, v1, v2, v0}, Lcom/twitter/library/api/upload/internal/i;->a(Lcom/twitter/media/model/MediaFile;ILjava/lang/Exception;)V

    goto :goto_1
.end method

.method d()V
    .locals 7

    .prologue
    .line 189
    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i;->n:Lcom/twitter/library/util/l;

    invoke-virtual {v0}, Lcom/twitter/library/util/l;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 190
    invoke-direct {p0}, Lcom/twitter/library/api/upload/internal/i;->f()V

    .line 210
    :goto_0
    return-void

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i;->n:Lcom/twitter/library/util/l;

    invoke-virtual {v0}, Lcom/twitter/library/util/l;->b()Lcom/twitter/library/util/x;

    move-result-object v2

    .line 195
    if-nez v2, :cond_1

    .line 198
    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    const/16 v1, 0x3f0

    new-instance v2, Ljava/io/IOException;

    const-string/jumbo v3, "RewindableInputStream is null"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/library/api/upload/internal/i;->a(Lcom/twitter/media/model/MediaFile;ILjava/lang/Exception;)V

    goto :goto_0

    .line 204
    :cond_1
    iget v3, p0, Lcom/twitter/library/api/upload/internal/i;->g:I

    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i;->n:Lcom/twitter/library/util/l;

    .line 207
    invoke-virtual {v0}, Lcom/twitter/library/util/l;->d()J

    move-result-wide v4

    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i;->n:Lcom/twitter/library/util/l;

    .line 208
    invoke-virtual {v0}, Lcom/twitter/library/util/l;->e()Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    .line 204
    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/api/upload/internal/i;->a(Lcom/twitter/library/util/x;IJLjava/lang/String;)V

    goto :goto_0
.end method
