.class Lcom/twitter/library/api/upload/internal/f$1;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/api/upload/internal/f;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/api/upload/internal/f;


# direct methods
.method constructor <init>(Lcom/twitter/library/api/upload/internal/f;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/twitter/library/api/upload/internal/f$1;->a:Lcom/twitter/library/api/upload/internal/f;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 52
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/upload/internal/f$1;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 7

    .prologue
    const/16 v4, 0x2710

    .line 55
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 56
    const-wide/16 v2, -0x1

    .line 58
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    check-cast p1, Lcom/twitter/library/api/upload/internal/e;

    invoke-virtual {p1}, Lcom/twitter/library/api/upload/internal/e;->b()J

    move-result-wide v2

    .line 60
    iget-object v1, p0, Lcom/twitter/library/api/upload/internal/f$1;->a:Lcom/twitter/library/api/upload/internal/f;

    invoke-virtual {v1, v4, v4}, Lcom/twitter/library/api/upload/internal/f;->a(II)V

    .line 61
    const-string/jumbo v1, "success"

    .line 65
    :goto_0
    iget-object v4, p0, Lcom/twitter/library/api/upload/internal/f$1;->a:Lcom/twitter/library/api/upload/internal/f;

    new-instance v5, Lcom/twitter/library/api/upload/f;

    iget-object v6, p0, Lcom/twitter/library/api/upload/internal/f$1;->a:Lcom/twitter/library/api/upload/internal/f;

    invoke-static {v6}, Lcom/twitter/library/api/upload/internal/f;->a(Lcom/twitter/library/api/upload/internal/f;)Lcom/twitter/media/model/MediaFile;

    move-result-object v6

    invoke-direct {v5, v0, v6, v2, v3}, Lcom/twitter/library/api/upload/f;-><init>(Lcom/twitter/library/service/u;Lcom/twitter/media/model/MediaFile;J)V

    invoke-virtual {v4, v5}, Lcom/twitter/library/api/upload/internal/f;->b(Lcom/twitter/library/api/upload/f;)V

    .line 66
    new-instance v0, Lcom/twitter/library/scribe/ScribeItemUploadMedia;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeItemUploadMedia;-><init>()V

    iget-object v2, p0, Lcom/twitter/library/api/upload/internal/f$1;->a:Lcom/twitter/library/api/upload/internal/f;

    .line 67
    invoke-static {v2}, Lcom/twitter/library/api/upload/internal/f;->a(Lcom/twitter/library/api/upload/internal/f;)Lcom/twitter/media/model/MediaFile;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/media/model/MediaFile;->g:Lcom/twitter/media/model/MediaType;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeItemUploadMedia;->a(Lcom/twitter/media/model/MediaType;)Lcom/twitter/library/scribe/ScribeItemUploadMedia;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/library/api/upload/internal/f$1;->a:Lcom/twitter/library/api/upload/internal/f;

    .line 68
    invoke-static {v2}, Lcom/twitter/library/api/upload/internal/f;->a(Lcom/twitter/library/api/upload/internal/f;)Lcom/twitter/media/model/MediaFile;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/media/model/MediaFile;->e:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/scribe/ScribeItemUploadMedia;->a(J)Lcom/twitter/library/scribe/ScribeItemUploadMedia;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/library/api/upload/internal/f$1;->a:Lcom/twitter/library/api/upload/internal/f;

    .line 69
    invoke-static {v2}, Lcom/twitter/library/api/upload/internal/f;->a(Lcom/twitter/library/api/upload/internal/f;)Lcom/twitter/media/model/MediaFile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/media/model/MediaFile;->a()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeItemUploadMedia;->a(Landroid/net/Uri;)Lcom/twitter/library/scribe/ScribeItemUploadMedia;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/library/api/upload/internal/f$1;->a:Lcom/twitter/library/api/upload/internal/f;

    .line 70
    invoke-static {v2}, Lcom/twitter/library/api/upload/internal/f;->b(Lcom/twitter/library/api/upload/internal/f;)Lcom/twitter/library/api/upload/MediaUsage;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeItemUploadMedia;->a(Lcom/twitter/library/api/upload/MediaUsage;)Lcom/twitter/library/scribe/ScribeItemUploadMedia;

    move-result-object v0

    .line 71
    iget-object v2, p0, Lcom/twitter/library/api/upload/internal/f$1;->a:Lcom/twitter/library/api/upload/internal/f;

    const-string/jumbo v3, "media_uploader"

    const-string/jumbo v4, "upload"

    invoke-virtual {v2, v3, v4, v1, v0}, Lcom/twitter/library/api/upload/internal/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeItemUploadMedia;)V

    .line 76
    return-void

    .line 63
    :cond_0
    const-string/jumbo v1, "failure"

    goto :goto_0
.end method
