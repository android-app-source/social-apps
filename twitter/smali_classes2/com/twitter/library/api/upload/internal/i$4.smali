.class Lcom/twitter/library/api/upload/internal/i$4;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/api/upload/internal/m$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/api/upload/internal/i;->a(Lcom/twitter/library/util/x;IJLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/api/upload/internal/i;


# direct methods
.method constructor <init>(Lcom/twitter/library/api/upload/internal/i;)V
    .locals 0

    .prologue
    .line 296
    iput-object p1, p0, Lcom/twitter/library/api/upload/internal/i$4;->a:Lcom/twitter/library/api/upload/internal/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcdp;Lcom/twitter/async/service/j;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcdp;",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 299
    invoke-virtual {p2}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 300
    if-nez v0, :cond_0

    .line 301
    new-instance v0, Lcom/twitter/library/api/upload/f;

    iget-object v1, p0, Lcom/twitter/library/api/upload/internal/i$4;->a:Lcom/twitter/library/api/upload/internal/i;

    iget-object v1, v1, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    const/16 v2, 0x3ed

    new-instance v3, Ljava/lang/Exception;

    const-string/jumbo v4, "Append request result is null"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/upload/f;-><init>(Lcom/twitter/media/model/MediaFile;ILjava/lang/Exception;)V

    .line 305
    invoke-virtual {p2, v0}, Lcom/twitter/async/service/j;->a(Ljava/lang/Object;)Lcom/twitter/async/service/j;

    .line 307
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 308
    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i$4;->a:Lcom/twitter/library/api/upload/internal/i;

    iget v1, v0, Lcom/twitter/library/api/upload/internal/i;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/twitter/library/api/upload/internal/i;->g:I

    .line 310
    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i$4;->a:Lcom/twitter/library/api/upload/internal/i;

    iget-object v1, p0, Lcom/twitter/library/api/upload/internal/i$4;->a:Lcom/twitter/library/api/upload/internal/i;

    iget v1, v1, Lcom/twitter/library/api/upload/internal/i;->f:I

    iget-object v2, p0, Lcom/twitter/library/api/upload/internal/i$4;->a:Lcom/twitter/library/api/upload/internal/i;

    iget v2, v2, Lcom/twitter/library/api/upload/internal/i;->g:I

    mul-int/2addr v1, v2

    const/16 v2, 0x2710

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/api/upload/internal/i;->a(II)V

    .line 311
    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i$4;->a:Lcom/twitter/library/api/upload/internal/i;

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/internal/i;->d()V

    .line 316
    :goto_0
    return-void

    .line 314
    :cond_1
    iget-object v1, p0, Lcom/twitter/library/api/upload/internal/i$4;->a:Lcom/twitter/library/api/upload/internal/i;

    new-instance v2, Lcom/twitter/library/api/upload/f;

    iget-object v3, p0, Lcom/twitter/library/api/upload/internal/i$4;->a:Lcom/twitter/library/api/upload/internal/i;

    iget-object v3, v3, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    iget-object v4, p0, Lcom/twitter/library/api/upload/internal/i$4;->a:Lcom/twitter/library/api/upload/internal/i;

    iget-wide v4, v4, Lcom/twitter/library/api/upload/internal/i;->e:J

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/twitter/library/api/upload/f;-><init>(Lcom/twitter/library/service/u;Lcom/twitter/media/model/MediaFile;J)V

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/upload/internal/i;->b(Lcom/twitter/library/api/upload/f;)V

    goto :goto_0
.end method
