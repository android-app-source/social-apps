.class Lcom/twitter/library/api/upload/internal/i$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/api/upload/internal/m$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/api/upload/internal/i;->e()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/api/upload/internal/i;


# direct methods
.method constructor <init>(Lcom/twitter/library/api/upload/internal/i;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/twitter/library/api/upload/internal/i$1;->a:Lcom/twitter/library/api/upload/internal/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcdp;Lcom/twitter/async/service/j;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcdp;",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v4, 0x3ed

    .line 139
    if-nez p1, :cond_1

    .line 140
    invoke-virtual {p2}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 141
    if-nez v0, :cond_0

    .line 142
    new-instance v0, Lcom/twitter/library/api/upload/f;

    iget-object v1, p0, Lcom/twitter/library/api/upload/internal/i$1;->a:Lcom/twitter/library/api/upload/internal/i;

    iget-object v1, v1, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    new-instance v2, Ljava/lang/Exception;

    const-string/jumbo v3, "no response"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v4, v2}, Lcom/twitter/library/api/upload/f;-><init>(Lcom/twitter/media/model/MediaFile;ILjava/lang/Exception;)V

    .line 145
    :cond_0
    iget-object v1, p0, Lcom/twitter/library/api/upload/internal/i$1;->a:Lcom/twitter/library/api/upload/internal/i;

    new-instance v2, Lcom/twitter/library/api/upload/f;

    iget-object v3, p0, Lcom/twitter/library/api/upload/internal/i$1;->a:Lcom/twitter/library/api/upload/internal/i;

    iget-object v3, v3, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    iget-object v4, p0, Lcom/twitter/library/api/upload/internal/i$1;->a:Lcom/twitter/library/api/upload/internal/i;

    iget-wide v4, v4, Lcom/twitter/library/api/upload/internal/i;->e:J

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/twitter/library/api/upload/f;-><init>(Lcom/twitter/library/service/u;Lcom/twitter/media/model/MediaFile;J)V

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/upload/internal/i;->b(Lcom/twitter/library/api/upload/f;)V

    .line 168
    :goto_0
    return-void

    .line 149
    :cond_1
    invoke-virtual {p2}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 150
    iget v1, p1, Lcdp;->a:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    .line 151
    iget-object v1, p1, Lcdp;->f:Lcdo;

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcdp;->f:Lcdo;

    iget-object v1, v1, Lcdo;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcdp;->f:Lcdo;

    iget-object v1, v1, Lcdo;->c:Ljava/lang/String;

    .line 154
    :goto_1
    invoke-virtual {v0, v4, v1}, Lcom/twitter/library/service/u;->a(ILjava/lang/String;)V

    .line 159
    :cond_2
    :goto_2
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 161
    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i$1;->a:Lcom/twitter/library/api/upload/internal/i;

    iget-wide v2, p1, Lcdp;->b:J

    iput-wide v2, v0, Lcom/twitter/library/api/upload/internal/i;->e:J

    .line 162
    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i$1;->a:Lcom/twitter/library/api/upload/internal/i;

    const/16 v1, 0x3e8

    const/16 v2, 0x2710

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/api/upload/internal/i;->a(II)V

    .line 163
    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i$1;->a:Lcom/twitter/library/api/upload/internal/i;

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/internal/i;->c()V

    goto :goto_0

    .line 151
    :cond_3
    const-string/jumbo v1, "Error: received failure response"

    goto :goto_1

    .line 155
    :cond_4
    iget-wide v2, p1, Lcdp;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    .line 156
    const/16 v1, 0x3ee

    const-string/jumbo v2, "Error: no media id"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/u;->a(ILjava/lang/String;)V

    goto :goto_2

    .line 166
    :cond_5
    iget-object v1, p0, Lcom/twitter/library/api/upload/internal/i$1;->a:Lcom/twitter/library/api/upload/internal/i;

    new-instance v2, Lcom/twitter/library/api/upload/f;

    iget-object v3, p0, Lcom/twitter/library/api/upload/internal/i$1;->a:Lcom/twitter/library/api/upload/internal/i;

    iget-object v3, v3, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    iget-object v4, p0, Lcom/twitter/library/api/upload/internal/i$1;->a:Lcom/twitter/library/api/upload/internal/i;

    iget-wide v4, v4, Lcom/twitter/library/api/upload/internal/i;->e:J

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/twitter/library/api/upload/f;-><init>(Lcom/twitter/library/service/u;Lcom/twitter/media/model/MediaFile;J)V

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/upload/internal/i;->b(Lcom/twitter/library/api/upload/f;)V

    goto :goto_0
.end method
