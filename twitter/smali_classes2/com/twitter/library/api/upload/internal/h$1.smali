.class Lcom/twitter/library/api/upload/internal/h$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/api/upload/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/api/upload/internal/h;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/api/upload/internal/h;


# direct methods
.method constructor <init>(Lcom/twitter/library/api/upload/internal/h;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/twitter/library/api/upload/internal/h$1;->a:Lcom/twitter/library/api/upload/internal/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/api/upload/f;)V
    .locals 5

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/h$1;->a:Lcom/twitter/library/api/upload/internal/h;

    iget v1, v0, Lcom/twitter/library/api/upload/internal/h;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/twitter/library/api/upload/internal/h;->f:I

    .line 68
    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/h$1;->a:Lcom/twitter/library/api/upload/internal/h;

    invoke-virtual {v0, p1}, Lcom/twitter/library/api/upload/internal/h;->a(Lcom/twitter/library/api/upload/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/h$1;->a:Lcom/twitter/library/api/upload/internal/h;

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/internal/h;->b()V

    .line 85
    :goto_0
    return-void

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/h$1;->a:Lcom/twitter/library/api/upload/internal/h;

    invoke-virtual {v0, p1}, Lcom/twitter/library/api/upload/internal/h;->b(Lcom/twitter/library/api/upload/f;)V

    .line 73
    invoke-virtual {p1}, Lcom/twitter/library/api/upload/f;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 74
    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/h$1;->a:Lcom/twitter/library/api/upload/internal/h;

    iget v0, v0, Lcom/twitter/library/api/upload/internal/h;->f:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    const-string/jumbo v0, "retry"

    .line 78
    :goto_1
    new-instance v1, Lcom/twitter/library/scribe/ScribeItemUploadMedia;

    invoke-direct {v1}, Lcom/twitter/library/scribe/ScribeItemUploadMedia;-><init>()V

    iget-object v2, p0, Lcom/twitter/library/api/upload/internal/h$1;->a:Lcom/twitter/library/api/upload/internal/h;

    iget-object v2, v2, Lcom/twitter/library/api/upload/internal/h;->d:Lcom/twitter/media/model/MediaFile;

    iget-object v2, v2, Lcom/twitter/media/model/MediaFile;->g:Lcom/twitter/media/model/MediaType;

    .line 79
    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeItemUploadMedia;->a(Lcom/twitter/media/model/MediaType;)Lcom/twitter/library/scribe/ScribeItemUploadMedia;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/api/upload/internal/h$1;->a:Lcom/twitter/library/api/upload/internal/h;

    iget-object v2, v2, Lcom/twitter/library/api/upload/internal/h;->d:Lcom/twitter/media/model/MediaFile;

    iget-object v2, v2, Lcom/twitter/media/model/MediaFile;->e:Ljava/io/File;

    .line 80
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeItemUploadMedia;->a(J)Lcom/twitter/library/scribe/ScribeItemUploadMedia;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/api/upload/internal/h$1;->a:Lcom/twitter/library/api/upload/internal/h;

    iget-object v2, v2, Lcom/twitter/library/api/upload/internal/h;->d:Lcom/twitter/media/model/MediaFile;

    .line 81
    invoke-virtual {v2}, Lcom/twitter/media/model/MediaFile;->a()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeItemUploadMedia;->a(Landroid/net/Uri;)Lcom/twitter/library/scribe/ScribeItemUploadMedia;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/api/upload/internal/h$1;->a:Lcom/twitter/library/api/upload/internal/h;

    iget-object v2, v2, Lcom/twitter/library/api/upload/internal/h;->e:Lcom/twitter/library/api/upload/MediaUsage;

    .line 82
    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeItemUploadMedia;->a(Lcom/twitter/library/api/upload/MediaUsage;)Lcom/twitter/library/scribe/ScribeItemUploadMedia;

    move-result-object v1

    .line 83
    iget-object v2, p0, Lcom/twitter/library/api/upload/internal/h$1;->a:Lcom/twitter/library/api/upload/internal/h;

    const-string/jumbo v3, "segmented_uploader"

    iget-object v4, p0, Lcom/twitter/library/api/upload/internal/h$1;->a:Lcom/twitter/library/api/upload/internal/h;

    iget-object v4, v4, Lcom/twitter/library/api/upload/internal/h;->g:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v0, v1}, Lcom/twitter/library/api/upload/internal/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeItemUploadMedia;)V

    goto :goto_0

    .line 74
    :cond_1
    const-string/jumbo v0, "success"

    goto :goto_1

    .line 76
    :cond_2
    const-string/jumbo v0, "failure"

    goto :goto_1
.end method
