.class public Lcom/twitter/library/api/upload/internal/g;
.super Lcom/twitter/library/api/upload/internal/a;
.source "Twttr"


# instance fields
.field d:J

.field final e:Landroid/net/Uri;

.field final f:Lcom/twitter/media/model/MediaType;

.field private final g:Landroid/os/Handler;

.field private final h:Lcom/twitter/library/api/upload/MediaUsage;

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;Landroid/net/Uri;Lcom/twitter/media/model/MediaType;Lcom/twitter/library/api/upload/MediaUsage;Lcom/twitter/library/api/upload/e;Lcom/twitter/util/q;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/service/v;",
            "Landroid/net/Uri;",
            "Lcom/twitter/media/model/MediaType;",
            "Lcom/twitter/library/api/upload/MediaUsage;",
            "Lcom/twitter/library/api/upload/e;",
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/library/api/progress/ProgressUpdatedEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p6, p7}, Lcom/twitter/library/api/upload/internal/a;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/library/api/upload/e;Lcom/twitter/util/q;)V

    .line 38
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/library/api/upload/internal/g;->d:J

    .line 42
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/library/api/upload/internal/g;->g:Landroid/os/Handler;

    .line 44
    const/16 v0, 0x14

    iput v0, p0, Lcom/twitter/library/api/upload/internal/g;->i:I

    .line 55
    iput-object p3, p0, Lcom/twitter/library/api/upload/internal/g;->e:Landroid/net/Uri;

    .line 56
    iput-object p4, p0, Lcom/twitter/library/api/upload/internal/g;->f:Lcom/twitter/media/model/MediaType;

    .line 57
    iput-object p5, p0, Lcom/twitter/library/api/upload/internal/g;->h:Lcom/twitter/library/api/upload/MediaUsage;

    .line 58
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 157
    new-instance v0, Lcom/twitter/library/scribe/ScribeItemUploadMedia;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeItemUploadMedia;-><init>()V

    iget-object v1, p0, Lcom/twitter/library/api/upload/internal/g;->f:Lcom/twitter/media/model/MediaType;

    .line 158
    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeItemUploadMedia;->a(Lcom/twitter/media/model/MediaType;)Lcom/twitter/library/scribe/ScribeItemUploadMedia;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/upload/internal/g;->e:Landroid/net/Uri;

    .line 159
    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeItemUploadMedia;->a(Landroid/net/Uri;)Lcom/twitter/library/scribe/ScribeItemUploadMedia;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/upload/internal/g;->h:Lcom/twitter/library/api/upload/MediaUsage;

    .line 160
    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeItemUploadMedia;->a(Lcom/twitter/library/api/upload/MediaUsage;)Lcom/twitter/library/scribe/ScribeItemUploadMedia;

    move-result-object v0

    .line 161
    const-string/jumbo v1, "segmented_uploader"

    const-string/jumbo v2, "url_async_upload"

    invoke-virtual {p0, v1, v2, p1, v0}, Lcom/twitter/library/api/upload/internal/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeItemUploadMedia;)V

    .line 166
    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    .line 66
    const/16 v0, 0xfa

    const/16 v1, 0x2710

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/api/upload/internal/g;->a(II)V

    .line 68
    new-instance v0, Lcom/twitter/library/api/upload/internal/l;

    iget-object v1, p0, Lcom/twitter/library/api/upload/internal/g;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/api/upload/internal/g;->b:Lcom/twitter/library/service/v;

    iget-object v3, p0, Lcom/twitter/library/api/upload/internal/g;->e:Landroid/net/Uri;

    iget-object v4, p0, Lcom/twitter/library/api/upload/internal/g;->f:Lcom/twitter/media/model/MediaType;

    iget-object v5, p0, Lcom/twitter/library/api/upload/internal/g;->h:Lcom/twitter/library/api/upload/MediaUsage;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/upload/internal/l;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Landroid/net/Uri;Lcom/twitter/media/model/MediaType;Lcom/twitter/library/api/upload/MediaUsage;)V

    .line 70
    new-instance v1, Lcom/twitter/library/api/upload/internal/g$1;

    invoke-direct {v1, p0}, Lcom/twitter/library/api/upload/internal/g$1;-><init>(Lcom/twitter/library/api/upload/internal/g;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/internal/l;->a(Lcom/twitter/library/api/upload/internal/m$a;)V

    .line 101
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 102
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/twitter/library/api/upload/internal/g;->b()V

    .line 63
    return-void
.end method

.method a(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 105
    iget v0, p0, Lcom/twitter/library/api/upload/internal/g;->i:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/twitter/library/api/upload/internal/g;->i:I

    .line 106
    iget v0, p0, Lcom/twitter/library/api/upload/internal/g;->i:I

    if-nez v0, :cond_0

    .line 107
    new-instance v0, Lcom/twitter/library/api/upload/f;

    const/16 v1, 0x3ed

    new-instance v2, Ljava/lang/Exception;

    const-string/jumbo v3, "too many status polls"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v4, v1, v2}, Lcom/twitter/library/api/upload/f;-><init>(Lcom/twitter/media/model/MediaFile;ILjava/lang/Exception;)V

    .line 109
    new-instance v1, Lcom/twitter/library/api/upload/f;

    iget-wide v2, p0, Lcom/twitter/library/api/upload/internal/g;->d:J

    invoke-direct {v1, v0, v4, v2, v3}, Lcom/twitter/library/api/upload/f;-><init>(Lcom/twitter/library/service/u;Lcom/twitter/media/model/MediaFile;J)V

    invoke-virtual {p0, v1}, Lcom/twitter/library/api/upload/internal/g;->b(Lcom/twitter/library/api/upload/f;)V

    .line 127
    :goto_0
    return-void

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/g;->g:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/library/api/upload/internal/g$2;

    invoke-direct {v1, p0}, Lcom/twitter/library/api/upload/internal/g$2;-><init>(Lcom/twitter/library/api/upload/internal/g;)V

    const/4 v2, 0x0

    .line 126
    invoke-static {p1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    .line 112
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method a(Lcdp;Lcom/twitter/async/service/j;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcdp;",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v1, 0x2710

    const/4 v4, 0x0

    .line 130
    if-nez p1, :cond_0

    const/4 v0, 0x2

    .line 131
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 144
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcdp;->f:Lcdo;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcdp;->f:Lcdo;

    iget-object v0, v0, Lcdo;->c:Ljava/lang/String;

    .line 147
    :goto_1
    new-instance v1, Lcom/twitter/library/api/upload/f;

    const/16 v2, 0x3ed

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v4, v2, v3}, Lcom/twitter/library/api/upload/f;-><init>(Lcom/twitter/media/model/MediaFile;ILjava/lang/Exception;)V

    .line 149
    new-instance v0, Lcom/twitter/library/api/upload/f;

    iget-wide v2, p0, Lcom/twitter/library/api/upload/internal/g;->d:J

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/twitter/library/api/upload/f;-><init>(Lcom/twitter/library/service/u;Lcom/twitter/media/model/MediaFile;J)V

    invoke-virtual {p0, v0}, Lcom/twitter/library/api/upload/internal/g;->b(Lcom/twitter/library/api/upload/f;)V

    .line 150
    const-string/jumbo v0, "failure"

    invoke-direct {p0, v0}, Lcom/twitter/library/api/upload/internal/g;->a(Ljava/lang/String;)V

    .line 154
    :goto_2
    return-void

    .line 130
    :cond_0
    iget v0, p1, Lcdp;->a:I

    goto :goto_0

    .line 133
    :pswitch_0
    iget v0, p1, Lcdp;->e:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/api/upload/internal/g;->a(I)V

    goto :goto_2

    .line 137
    :pswitch_1
    invoke-virtual {p0, v1, v1}, Lcom/twitter/library/api/upload/internal/g;->a(II)V

    .line 138
    invoke-virtual {p2}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 139
    new-instance v1, Lcom/twitter/library/api/upload/f;

    iget-wide v2, p0, Lcom/twitter/library/api/upload/internal/g;->d:J

    invoke-direct {v1, v0, v4, v2, v3}, Lcom/twitter/library/api/upload/f;-><init>(Lcom/twitter/library/service/u;Lcom/twitter/media/model/MediaFile;J)V

    invoke-virtual {p0, v1}, Lcom/twitter/library/api/upload/internal/g;->b(Lcom/twitter/library/api/upload/f;)V

    .line 140
    const-string/jumbo v0, "success"

    invoke-direct {p0, v0}, Lcom/twitter/library/api/upload/internal/g;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 144
    :cond_1
    const-string/jumbo v0, "failed"

    goto :goto_1

    .line 131
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
