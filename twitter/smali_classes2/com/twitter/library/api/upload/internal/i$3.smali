.class Lcom/twitter/library/api/upload/internal/i$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/api/upload/internal/i;->a(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/api/upload/internal/i;


# direct methods
.method constructor <init>(Lcom/twitter/library/api/upload/internal/i;)V
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lcom/twitter/library/api/upload/internal/i$3;->a:Lcom/twitter/library/api/upload/internal/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 239
    new-instance v1, Lcom/twitter/library/api/upload/internal/n;

    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i$3;->a:Lcom/twitter/library/api/upload/internal/i;

    iget-object v2, v0, Lcom/twitter/library/api/upload/internal/i;->a:Landroid/content/Context;

    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i$3;->a:Lcom/twitter/library/api/upload/internal/i;

    iget-object v3, v0, Lcom/twitter/library/api/upload/internal/i;->b:Lcom/twitter/library/service/v;

    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i$3;->a:Lcom/twitter/library/api/upload/internal/i;

    iget-object v0, v0, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    .line 240
    invoke-virtual {v0}, Lcom/twitter/media/model/MediaFile;->a()Landroid/net/Uri;

    move-result-object v4

    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i$3;->a:Lcom/twitter/library/api/upload/internal/i;

    iget-object v0, v0, Lcom/twitter/library/api/upload/internal/i;->d:Lcom/twitter/media/model/MediaFile;

    iget-object v5, v0, Lcom/twitter/media/model/MediaFile;->g:Lcom/twitter/media/model/MediaType;

    iget-object v0, p0, Lcom/twitter/library/api/upload/internal/i$3;->a:Lcom/twitter/library/api/upload/internal/i;

    iget-wide v6, v0, Lcom/twitter/library/api/upload/internal/i;->e:J

    invoke-direct/range {v1 .. v7}, Lcom/twitter/library/api/upload/internal/n;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Landroid/net/Uri;Lcom/twitter/media/model/MediaType;J)V

    .line 241
    new-instance v0, Lcom/twitter/library/api/upload/internal/i$3$1;

    invoke-direct {v0, p0}, Lcom/twitter/library/api/upload/internal/i$3$1;-><init>(Lcom/twitter/library/api/upload/internal/i$3;)V

    invoke-virtual {v1, v0}, Lcom/twitter/library/api/upload/internal/n;->a(Lcom/twitter/library/api/upload/internal/m$a;)V

    .line 248
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 249
    return-void
.end method
