.class public Lcom/twitter/library/api/upload/p;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method private static a(Ljava/util/List;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 192
    invoke-static {p0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    const/4 v0, 0x0

    .line 195
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ","

    invoke-static {v0, p0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/util/List;Ljava/util/List;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 208
    if-nez p1, :cond_1

    .line 239
    :cond_0
    :goto_0
    return-object v2

    .line 211
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    .line 212
    if-eqz v4, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v4, v0, :cond_0

    .line 215
    const-string/jumbo v1, ""

    .line 216
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 217
    const-string/jumbo v0, "{"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v4, :cond_2

    .line 219
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Lcom/twitter/model/drafts/DraftAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 220
    instance-of v6, v0, Lcom/twitter/model/media/EditableImage;

    if-eqz v6, :cond_0

    .line 223
    check-cast v0, Lcom/twitter/model/media/EditableImage;

    iget-object v0, v0, Lcom/twitter/model/media/EditableImage;->g:Ljava/util/List;

    .line 224
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 225
    invoke-static {v0}, Lcom/twitter/library/api/upload/p;->b(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 226
    if-eqz v0, :cond_4

    .line 227
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v6, "\""

    .line 228
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 229
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v6, "\":"

    .line 230
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 231
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    const-string/jumbo v0, ","

    .line 218
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move-object v1, v0

    goto :goto_1

    .line 236
    :cond_2
    const-string/jumbo v0, "}"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 239
    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v2

    :goto_3
    move-object v2, v0

    goto :goto_0

    :cond_3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method public static a(Lcom/twitter/library/network/ab;)Ljava/lang/StringBuilder;
    .locals 4

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/twitter/library/network/ab;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "statuses"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "update"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/ab;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    .line 55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 54
    return-object v0
.end method

.method public static a(Lcom/twitter/library/network/ab$a;Lcom/twitter/library/network/ab;Lcom/twitter/model/drafts/a;Ljava/util/List;Ljava/lang/String;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/network/ab$a;",
            "Lcom/twitter/library/network/ab;",
            "Lcom/twitter/model/drafts/a;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    .line 71
    const-string/jumbo v0, "include_entities"

    invoke-virtual {p0, v0, v6}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Z)V

    .line 72
    const-string/jumbo v0, "include_media_features"

    invoke-virtual {p0, v0, v6}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Z)V

    .line 73
    const-string/jumbo v0, "earned_read"

    invoke-virtual {p0, v0, v6}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Z)V

    .line 74
    invoke-virtual {p0}, Lcom/twitter/library/network/ab$a;->a()V

    .line 76
    iget-object v0, p2, Lcom/twitter/model/drafts/a;->i:Lcom/twitter/model/core/r;

    if-eqz v0, :cond_0

    .line 77
    const-string/jumbo v0, "attachment_url"

    iget-object v1, p2, Lcom/twitter/model/drafts/a;->i:Lcom/twitter/model/core/r;

    invoke-virtual {v1}, Lcom/twitter/model/core/r;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :cond_0
    const-string/jumbo v0, "status"

    iget-object v1, p2, Lcom/twitter/model/drafts/a;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const-string/jumbo v0, "send_error_codes"

    invoke-virtual {p0, v0, v6}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Z)V

    .line 84
    iget-wide v0, p2, Lcom/twitter/model/drafts/a;->e:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 85
    const-string/jumbo v0, "in_reply_to_status_id"

    iget-wide v2, p2, Lcom/twitter/model/drafts/a;->e:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;J)V

    .line 88
    iget-boolean v0, p2, Lcom/twitter/model/drafts/a;->f:Z

    if-eqz v0, :cond_1

    .line 89
    const-string/jumbo v0, "auto_populate_reply_metadata"

    invoke-virtual {p0, v0, v6}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Z)V

    .line 94
    :cond_1
    iget-object v0, p2, Lcom/twitter/model/drafts/a;->o:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 95
    const-string/jumbo v0, "exclude_reply_user_ids"

    iget-object v1, p2, Lcom/twitter/model/drafts/a;->o:Ljava/util/List;

    .line 96
    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p2, Lcom/twitter/model/drafts/a;->o:Ljava/util/List;

    .line 97
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    .line 95
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;[JII)I

    .line 101
    :cond_2
    iget-wide v0, p2, Lcom/twitter/model/drafts/a;->e:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_d

    iget-object v0, p2, Lcom/twitter/model/drafts/a;->h:Lcgi;

    if-eqz v0, :cond_d

    iget-object v0, p2, Lcom/twitter/model/drafts/a;->h:Lcgi;

    .line 106
    :goto_0
    if-eqz v0, :cond_3

    iget-object v1, v0, Lcgi;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 107
    const-string/jumbo v1, "impression_id"

    iget-object v2, v0, Lcgi;->c:Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    invoke-virtual {v0}, Lcgi;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 109
    const-string/jumbo v0, "earned"

    invoke-virtual {p0, v0, v6}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Z)V

    .line 113
    :cond_3
    iget-object v0, p2, Lcom/twitter/model/drafts/a;->g:Lcom/twitter/model/geo/c;

    .line 114
    if-eqz v0, :cond_5

    if-eqz p5, :cond_5

    .line 115
    const-string/jumbo v1, "place_id"

    invoke-virtual {v0}, Lcom/twitter/model/geo/c;->a()Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/model/geo/TwitterPlace;->b:Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    invoke-virtual {v0}, Lcom/twitter/model/geo/c;->b()Lcom/twitter/model/geo/b;

    move-result-object v1

    .line 117
    if-eqz v1, :cond_4

    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v2

    invoke-virtual {v2}, Lbqg;->c()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 118
    const-string/jumbo v2, "lat"

    invoke-virtual {v1}, Lcom/twitter/model/geo/b;->a()D

    move-result-wide v4

    invoke-virtual {p0, v2, v4, v5}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;D)V

    .line 119
    const-string/jumbo v2, "long"

    invoke-virtual {v1}, Lcom/twitter/model/geo/b;->b()D

    move-result-wide v4

    invoke-virtual {p0, v2, v4, v5}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;D)V

    .line 122
    :cond_4
    invoke-virtual {v0}, Lcom/twitter/model/geo/c;->c()Ljava/lang/String;

    move-result-object v0

    .line 123
    if-eqz v0, :cond_5

    .line 124
    const-string/jumbo v1, "geo_search_request_id"

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    :cond_5
    invoke-static {p3}, Lcom/twitter/library/api/upload/p;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 129
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 130
    const-string/jumbo v1, "media_ids"

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_6
    iget-object v0, p2, Lcom/twitter/model/drafts/a;->d:Ljava/util/List;

    invoke-static {v0, p3}, Lcom/twitter/library/api/upload/p;->a(Ljava/util/List;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 133
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 134
    const-string/jumbo v1, "media_tags"

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    :cond_7
    invoke-static {p4}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 138
    iget-object v0, p2, Lcom/twitter/model/drafts/a;->j:Ljava/lang/String;

    .line 139
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 140
    const-string/jumbo v1, "card_uri"

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :cond_8
    :goto_1
    const-string/jumbo v0, "include_blocked_by_and_blocking_in_requests_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 148
    const-string/jumbo v0, "include_blocking"

    invoke-virtual {p0, v0, v6}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Z)V

    .line 149
    const-string/jumbo v0, "include_blocked_by"

    invoke-virtual {p0, v0, v6}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Z)V

    .line 152
    :cond_9
    const-string/jumbo v0, "tweet_mode"

    const-string/jumbo v1, "extended"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string/jumbo v0, "include_reply_count"

    const-string/jumbo v1, "true"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string/jumbo v0, "ad_formats_convo_card_engagement_metadata_scribing_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p2, Lcom/twitter/model/drafts/a;->n:Ljava/lang/String;

    .line 156
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 157
    const-string/jumbo v0, "engagement_metadata"

    iget-object v1, p2, Lcom/twitter/model/drafts/a;->n:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_a
    invoke-virtual {p1}, Lcom/twitter/library/network/ab;->f()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 161
    const-string/jumbo v0, "nullcast"

    invoke-virtual {p0, v0, v6}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;I)V

    .line 165
    :cond_b
    invoke-virtual {p2}, Lcom/twitter/model/drafts/a;->d()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 166
    const-string/jumbo v0, "context_periscope_creator_id"

    iget-wide v2, p2, Lcom/twitter/model/drafts/a;->p:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;J)V

    .line 167
    const-string/jumbo v0, "context_periscope_is_live"

    iget-boolean v1, p2, Lcom/twitter/model/drafts/a;->q:Z

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Z)V

    .line 170
    :cond_c
    invoke-static {p0, p2}, Lcom/twitter/library/api/upload/p;->a(Lcom/twitter/library/network/ab$a;Lcom/twitter/model/drafts/a;)V

    .line 171
    return-void

    .line 101
    :cond_d
    iget-object v0, p2, Lcom/twitter/model/drafts/a;->i:Lcom/twitter/model/core/r;

    if-eqz v0, :cond_e

    iget-object v0, p2, Lcom/twitter/model/drafts/a;->i:Lcom/twitter/model/core/r;

    iget-object v0, v0, Lcom/twitter/model/core/r;->m:Lcgi;

    goto/16 :goto_0

    :cond_e
    iget-object v0, p2, Lcom/twitter/model/drafts/a;->h:Lcgi;

    goto/16 :goto_0

    .line 143
    :cond_f
    const-string/jumbo v0, "card_uri"

    invoke-virtual {p0, v0, p4}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static a(Lcom/twitter/library/network/ab$a;Lcom/twitter/model/drafts/a;)V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p1, Lcom/twitter/model/drafts/a;->l:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    :goto_0
    return-void

    .line 180
    :cond_0
    const-string/jumbo v0, ","

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->l:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 181
    const-string/jumbo v1, "semantic_annotation_ids"

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static b(Ljava/util/List;)Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/n;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 244
    .line 246
    :try_start_0
    new-instance v3, Ljava/io/StringWriter;

    const/16 v0, 0x200

    invoke-direct {v3, v0}, Ljava/io/StringWriter;-><init>(I)V

    .line 247
    sget-object v0, Lcom/twitter/model/json/common/e;->a:Lcom/fasterxml/jackson/core/JsonFactory;

    invoke-virtual {v0, v3}, Lcom/fasterxml/jackson/core/JsonFactory;->a(Ljava/io/Writer;)Lcom/fasterxml/jackson/core/JsonGenerator;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 248
    :try_start_1
    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonGenerator;->a()V

    .line 249
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/n;

    .line 250
    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 251
    const-string/jumbo v5, "type"

    const-string/jumbo v6, "user"

    invoke-virtual {v2, v5, v6}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    iget-wide v6, v0, Lcom/twitter/model/core/n;->b:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 255
    const-string/jumbo v5, "user_id"

    iget-wide v6, v0, Lcom/twitter/model/core/n;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    :cond_0
    const-string/jumbo v5, "screen_name"

    iget-object v0, v0, Lcom/twitter/model/core/n;->d:Ljava/lang/String;

    invoke-virtual {v2, v5, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 263
    :catch_0
    move-exception v0

    move-object v0, v2

    .line 266
    :goto_1
    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    move-object v0, v1

    .line 264
    :goto_2
    return-object v0

    .line 260
    :cond_1
    :try_start_2
    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonGenerator;->b()V

    .line 261
    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonGenerator;->flush()V

    .line 262
    invoke-virtual {v3}, Ljava/io/StringWriter;->getBuffer()Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 266
    invoke-static {v2}, Lcqc;->a(Ljava/io/Closeable;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_3
    invoke-static {v2}, Lcqc;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    .line 263
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_1
.end method
