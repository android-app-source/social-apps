.class public Lcom/twitter/library/api/upload/a;
.super Lcom/twitter/library/api/upload/l;
.source "Twttr"


# instance fields
.field protected a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/twitter/library/api/upload/l;-><init>(Landroid/content/Context;)V

    .line 34
    iput-object p2, p0, Lcom/twitter/library/api/upload/a;->a:Ljava/lang/String;

    .line 35
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/api/upload/w;Lcom/twitter/library/service/u;)V
    .locals 0

    .prologue
    .line 39
    return-void
.end method

.method public a(Lcom/twitter/library/api/upload/w;Lcom/twitter/library/service/u;Lcom/twitter/library/api/upload/j;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 45
    invoke-virtual {p2}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/library/api/upload/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":failure"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 53
    :goto_0
    invoke-virtual {p3}, Lcom/twitter/library/api/upload/j;->b()Ljava/util/List;

    move-result-object v1

    .line 54
    if-eqz v1, :cond_2

    const-string/jumbo v2, ","

    invoke-static {v2, v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    .line 55
    :goto_1
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p1}, Lcom/twitter/library/api/upload/w;->M()Lcom/twitter/library/service/v;

    move-result-object v3

    iget-wide v4, v3, Lcom/twitter/library/service/v;->c:J

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v6, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 56
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v2, 0x2

    .line 57
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->d(I)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 58
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 55
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 59
    return-void

    .line 47
    :cond_0
    invoke-virtual {p3}, Lcom/twitter/library/api/upload/j;->a()I

    move-result v0

    if-le v0, v6, :cond_1

    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/library/api/upload/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":retry"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 50
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/library/api/upload/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":success"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 54
    :cond_2
    const-string/jumbo v1, ""

    goto :goto_1
.end method
