.class public Lcom/twitter/library/api/upload/m$c;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/api/upload/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# instance fields
.field public final a:Lcom/twitter/library/api/upload/f;

.field public final b:Lcom/twitter/library/api/progress/ProgressUpdatedEvent;


# direct methods
.method private constructor <init>(Lcom/twitter/library/api/upload/f;Lcom/twitter/library/api/progress/ProgressUpdatedEvent;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-object p1, p0, Lcom/twitter/library/api/upload/m$c;->a:Lcom/twitter/library/api/upload/f;

    .line 114
    iput-object p2, p0, Lcom/twitter/library/api/upload/m$c;->b:Lcom/twitter/library/api/progress/ProgressUpdatedEvent;

    .line 115
    invoke-static {p1, p2}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 116
    return-void
.end method

.method public static a(Lcom/twitter/library/api/progress/ProgressUpdatedEvent;)Lcom/twitter/library/api/upload/m$c;
    .locals 2

    .prologue
    .line 109
    new-instance v0, Lcom/twitter/library/api/upload/m$c;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/twitter/library/api/upload/m$c;-><init>(Lcom/twitter/library/api/upload/f;Lcom/twitter/library/api/progress/ProgressUpdatedEvent;)V

    return-object v0
.end method

.method public static a(Lcom/twitter/library/api/upload/f;)Lcom/twitter/library/api/upload/m$c;
    .locals 2

    .prologue
    .line 104
    new-instance v0, Lcom/twitter/library/api/upload/m$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/api/upload/m$c;-><init>(Lcom/twitter/library/api/upload/f;Lcom/twitter/library/api/progress/ProgressUpdatedEvent;)V

    return-object v0
.end method
