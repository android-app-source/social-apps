.class Lcom/twitter/library/api/upload/m$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/c$a;
.implements Lrx/j;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/api/upload/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/c$a",
        "<",
        "Lcom/twitter/library/api/upload/m$c;",
        ">;",
        "Lrx/j;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/api/upload/g;

.field private final b:Lcom/twitter/media/model/MediaFile;

.field private final c:Lcom/twitter/library/api/upload/MediaUsage;

.field private d:Lcom/twitter/util/concurrent/ObservablePromise;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/concurrent/ObservablePromise",
            "<",
            "Lcom/twitter/library/api/upload/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/library/api/upload/g;Lcom/twitter/media/model/MediaFile;Lcom/twitter/library/api/upload/MediaUsage;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    iput-object p1, p0, Lcom/twitter/library/api/upload/m$b;->a:Lcom/twitter/library/api/upload/g;

    .line 129
    iput-object p2, p0, Lcom/twitter/library/api/upload/m$b;->b:Lcom/twitter/media/model/MediaFile;

    .line 130
    iput-object p3, p0, Lcom/twitter/library/api/upload/m$b;->c:Lcom/twitter/library/api/upload/MediaUsage;

    .line 131
    return-void
.end method


# virtual methods
.method public B_()V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/twitter/library/api/upload/m$b;->d:Lcom/twitter/util/concurrent/ObservablePromise;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/concurrent/ObservablePromise;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/util/concurrent/ObservablePromise;->cancel(Z)Z

    .line 167
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/api/upload/m$b;->d:Lcom/twitter/util/concurrent/ObservablePromise;

    .line 168
    return-void
.end method

.method public a(Lrx/i;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-",
            "Lcom/twitter/library/api/upload/m$c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lcom/twitter/library/api/upload/m$b;->a:Lcom/twitter/library/api/upload/g;

    iget-object v1, p0, Lcom/twitter/library/api/upload/m$b;->b:Lcom/twitter/media/model/MediaFile;

    .line 136
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/api/upload/m$b$1;

    invoke-direct {v3, p0, p1}, Lcom/twitter/library/api/upload/m$b$1;-><init>(Lcom/twitter/library/api/upload/m$b;Lrx/i;)V

    iget-object v4, p0, Lcom/twitter/library/api/upload/m$b;->c:Lcom/twitter/library/api/upload/MediaUsage;

    .line 135
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/library/api/upload/g;->a(Lcom/twitter/media/model/MediaFile;Ljava/util/List;Lcom/twitter/util/q;Lcom/twitter/library/api/upload/MediaUsage;)Lcom/twitter/library/api/upload/g$a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/upload/m$b;->d:Lcom/twitter/util/concurrent/ObservablePromise;

    .line 144
    iget-object v0, p0, Lcom/twitter/library/api/upload/m$b;->d:Lcom/twitter/util/concurrent/ObservablePromise;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/concurrent/ObservablePromise;

    new-instance v1, Lcom/twitter/library/api/upload/m$b$2;

    invoke-direct {v1, p0, p1}, Lcom/twitter/library/api/upload/m$b$2;-><init>(Lcom/twitter/library/api/upload/m$b;Lrx/i;)V

    invoke-virtual {v0, v1}, Lcom/twitter/util/concurrent/ObservablePromise;->b(Lcom/twitter/util/concurrent/d;)Lcom/twitter/util/concurrent/g;

    .line 153
    iget-object v0, p0, Lcom/twitter/library/api/upload/m$b;->d:Lcom/twitter/util/concurrent/ObservablePromise;

    new-instance v1, Lcom/twitter/library/api/upload/m$b$3;

    invoke-direct {v1, p0, p1}, Lcom/twitter/library/api/upload/m$b$3;-><init>(Lcom/twitter/library/api/upload/m$b;Lrx/i;)V

    invoke-virtual {v0, v1}, Lcom/twitter/util/concurrent/ObservablePromise;->c(Lcom/twitter/util/concurrent/d;)Lcom/twitter/util/concurrent/g;

    .line 161
    invoke-virtual {p1, p0}, Lrx/i;->a(Lrx/j;)V

    .line 162
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/twitter/library/api/upload/m$b;->d:Lcom/twitter/util/concurrent/ObservablePromise;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 119
    check-cast p1, Lrx/i;

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/upload/m$b;->a(Lrx/i;)V

    return-void
.end method
