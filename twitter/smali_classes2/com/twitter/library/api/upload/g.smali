.class public Lcom/twitter/library/api/upload/g;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/api/upload/g$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/service/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/twitter/library/service/v;

    invoke-direct {v0, p2}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/api/upload/g;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, Lcom/twitter/library/api/upload/g;->a:Landroid/content/Context;

    .line 96
    iput-object p2, p0, Lcom/twitter/library/api/upload/g;->b:Lcom/twitter/library/service/v;

    .line 97
    return-void
.end method

.method private static a(Lcom/twitter/media/model/MediaType;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/media/model/MediaType;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 206
    sget-object v0, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    if-ne p0, v0, :cond_2

    .line 207
    const-string/jumbo v0, "photos_segmented_upload_maximum_segment_size"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v0

    .line 210
    if-eqz v0, :cond_0

    invoke-static {}, Lcrt;->b()Z

    move-result v1

    if-nez v1, :cond_1

    const/16 v1, 0x400

    if-ge v0, v1, :cond_1

    .line 211
    :cond_0
    const v0, 0xc800

    .line 213
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 238
    :goto_0
    return-object v0

    .line 215
    :cond_2
    sget-object v0, Lcom/twitter/media/model/MediaType;->d:Lcom/twitter/media/model/MediaType;

    if-eq p0, v0, :cond_3

    sget-object v0, Lcom/twitter/media/model/MediaType;->f:Lcom/twitter/media/model/MediaType;

    if-eq p0, v0, :cond_3

    sget-object v0, Lcom/twitter/media/model/MediaType;->c:Lcom/twitter/media/model/MediaType;

    if-ne p0, v0, :cond_7

    .line 218
    :cond_3
    const-string/jumbo v0, "videos_segmented_upload_segment_size_wifi"

    const/high16 v1, 0x80000

    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v0

    .line 220
    const-string/jumbo v1, "videos_segmented_upload_segment_size_cellular"

    const/high16 v2, 0x40000

    invoke-static {v1, v2}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v1

    .line 222
    const-string/jumbo v2, "videos_segmented_upload_segment_size_backoff_policy_enabled"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcoj;->a(Ljava/lang/String;Z)Z

    move-result v2

    .line 224
    const-string/jumbo v3, "videos_segmented_upload_minimum_segment_size"

    const/high16 v4, 0x10000

    invoke-static {v3, v4}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v3

    .line 226
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v4

    invoke-virtual {v4}, Lcrr;->c()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 228
    :goto_1
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 229
    if-eqz v2, :cond_5

    .line 231
    :goto_2
    if-lt v0, v3, :cond_6

    .line 232
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 233
    shr-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    move v0, v1

    .line 226
    goto :goto_1

    .line 236
    :cond_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 238
    :cond_6
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    goto :goto_0

    .line 240
    :cond_7
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Failed to upload unknown media type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/media/model/MediaType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static a(Lcom/twitter/library/api/upload/f;J)V
    .locals 11

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 179
    iget-object v0, p0, Lcom/twitter/library/api/upload/f;->b:Lcom/twitter/media/model/MediaFile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/f;->b:Lcom/twitter/media/model/MediaFile;

    iget-object v0, v0, Lcom/twitter/media/model/MediaFile;->g:Lcom/twitter/media/model/MediaType;

    sget-object v1, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    if-eq v0, v1, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/api/upload/f;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "app:twitter_service:image_attachment:upload:success"

    .line 186
    :goto_1
    invoke-static {}, Lcom/twitter/library/network/forecaster/c;->a()Lcom/twitter/library/network/forecaster/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/forecaster/c;->d()Lcom/twitter/util/units/bitrate/KilobitsPerSecond;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/util/units/bitrate/KilobitsPerSecond;->intValue()I

    move-result v1

    .line 187
    iget-object v2, p0, Lcom/twitter/library/api/upload/f;->b:Lcom/twitter/media/model/MediaFile;

    iget-object v2, v2, Lcom/twitter/media/model/MediaFile;->e:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 188
    const-string/jumbo v4, "%s=%s,%s=%s,%s=%s"

    const/4 v5, 0x6

    new-array v5, v5, [Ljava/lang/Object;

    const-string/jumbo v6, "upload_kbps"

    aput-object v6, v5, v7

    .line 189
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v8

    const-string/jumbo v1, "file_size_bytes"

    aput-object v1, v5, v9

    const/4 v1, 0x3

    .line 190
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "upload_duration_ms"

    aput-object v2, v5, v1

    const/4 v1, 0x5

    .line 191
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v1

    .line 188
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 193
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 194
    new-instance v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v4, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v8, [Ljava/lang/String;

    aput-object v0, v2, v7

    invoke-virtual {v4, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 195
    invoke-virtual {v0, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;->d(I)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 196
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 194
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 183
    :cond_2
    const-string/jumbo v0, "app:twitter_service:image_attachment:upload:failure"

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/net/Uri;Lcom/twitter/media/model/MediaType;Lcom/twitter/library/api/upload/MediaUsage;Lcom/twitter/util/q;)Lcom/twitter/library/api/upload/g$a;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/twitter/media/model/MediaType;",
            "Lcom/twitter/library/api/upload/MediaUsage;",
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/library/api/progress/ProgressUpdatedEvent;",
            ">;)",
            "Lcom/twitter/library/api/upload/g$a;"
        }
    .end annotation

    .prologue
    .line 150
    new-instance v6, Lcom/twitter/library/api/upload/g$a;

    invoke-direct {v6}, Lcom/twitter/library/api/upload/g$a;-><init>()V

    .line 151
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->a()Lcqt;

    move-result-object v3

    .line 152
    invoke-interface {v3}, Lcqt;->b()J

    move-result-wide v4

    .line 153
    new-instance v1, Lcom/twitter/library/api/upload/g$2;

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/api/upload/g$2;-><init>(Lcom/twitter/library/api/upload/g;Lcqt;JLcom/twitter/library/api/upload/g$a;)V

    .line 170
    new-instance v7, Lcom/twitter/library/api/upload/internal/g;

    iget-object v8, p0, Lcom/twitter/library/api/upload/g;->a:Landroid/content/Context;

    iget-object v9, p0, Lcom/twitter/library/api/upload/g;->b:Lcom/twitter/library/service/v;

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    move-object/from16 v12, p3

    move-object v13, v1

    move-object/from16 v14, p4

    invoke-direct/range {v7 .. v14}, Lcom/twitter/library/api/upload/internal/g;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Landroid/net/Uri;Lcom/twitter/media/model/MediaType;Lcom/twitter/library/api/upload/MediaUsage;Lcom/twitter/library/api/upload/e;Lcom/twitter/util/q;)V

    .line 172
    invoke-virtual {v7}, Lcom/twitter/library/api/upload/internal/g;->a()V

    .line 174
    return-object v6
.end method

.method public a(Lcom/twitter/media/model/MediaFile;Ljava/util/List;Lcom/twitter/util/q;Lcom/twitter/library/api/upload/MediaUsage;)Lcom/twitter/library/api/upload/g$a;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/media/model/MediaFile;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/library/api/progress/ProgressUpdatedEvent;",
            ">;",
            "Lcom/twitter/library/api/upload/MediaUsage;",
            ")",
            "Lcom/twitter/library/api/upload/g$a;"
        }
    .end annotation

    .prologue
    .line 109
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v2

    invoke-virtual {v2}, Lcqq;->a()Lcqt;

    move-result-object v5

    .line 110
    invoke-interface {v5}, Lcqt;->b()J

    move-result-wide v6

    .line 112
    new-instance v8, Lcom/twitter/library/api/upload/g$a;

    invoke-direct {v8}, Lcom/twitter/library/api/upload/g$a;-><init>()V

    .line 114
    new-instance v3, Lcom/twitter/library/api/upload/g$1;

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/twitter/library/api/upload/g$1;-><init>(Lcom/twitter/library/api/upload/g;Lcqt;JLcom/twitter/library/api/upload/g$a;)V

    .line 131
    new-instance v9, Lcom/twitter/library/api/upload/internal/c;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/library/api/upload/g;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/library/api/upload/g;->b:Lcom/twitter/library/service/v;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/media/model/MediaFile;->g:Lcom/twitter/media/model/MediaType;

    .line 139
    invoke-static {v2}, Lcom/twitter/library/api/upload/g;->a(Lcom/twitter/media/model/MediaType;)Ljava/util/List;

    move-result-object v17

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    move-object v14, v3

    move-object/from16 v15, p3

    move-object/from16 v16, p4

    invoke-direct/range {v9 .. v17}, Lcom/twitter/library/api/upload/internal/c;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Lcom/twitter/media/model/MediaFile;Ljava/util/List;Lcom/twitter/library/api/upload/e;Lcom/twitter/util/q;Lcom/twitter/library/api/upload/MediaUsage;Ljava/util/List;)V

    .line 140
    invoke-virtual {v9}, Lcom/twitter/library/api/upload/internal/c;->a()V

    .line 141
    return-object v8
.end method
