.class public Lcom/twitter/library/api/moments/maker/s;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/api/moments/maker/e$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/library/api/moments/maker/e$a",
        "<",
        "Lcfm;",
        "Lcom/twitter/model/core/z;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-wide p1, p0, Lcom/twitter/library/api/moments/maker/s;->a:J

    .line 26
    iput-wide p3, p0, Lcom/twitter/library/api/moments/maker/s;->b:J

    .line 27
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    const-string/jumbo v0, "moments"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "set_cover"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/twitter/library/api/moments/maker/s;->a:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    const-string/jumbo v0, "set_cover"

    return-object v0
.end method

.method public d()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcfm;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    const-class v0, Lcfm;

    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/twitter/model/json/common/a;
    .locals 2

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/twitter/library/api/moments/maker/s;->b:J

    invoke-static {v0, v1}, Lcom/twitter/model/json/moments/maker/JsonSetMediaIdAsCoverRequest;->a(J)Lcom/twitter/model/json/moments/maker/JsonSetMediaIdAsCoverRequest;

    move-result-object v0

    return-object v0
.end method
