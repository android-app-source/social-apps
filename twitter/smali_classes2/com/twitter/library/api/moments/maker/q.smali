.class public Lcom/twitter/library/api/moments/maker/q;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Lcom/twitter/library/api/moments/maker/RecommendationType;

.field public final b:Ljava/lang/String;

.field public final c:J


# direct methods
.method constructor <init>(Lcom/twitter/library/api/moments/maker/RecommendationType;JLjava/lang/String;)V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    sget-object v0, Lcom/twitter/library/api/moments/maker/RecommendationType;->d:Lcom/twitter/library/api/moments/maker/RecommendationType;

    if-ne p1, v0, :cond_0

    .line 27
    invoke-static {p4}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    :cond_0
    iput-object p1, p0, Lcom/twitter/library/api/moments/maker/q;->a:Lcom/twitter/library/api/moments/maker/RecommendationType;

    .line 30
    iput-wide p2, p0, Lcom/twitter/library/api/moments/maker/q;->c:J

    .line 31
    iput-object p4, p0, Lcom/twitter/library/api/moments/maker/q;->b:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public static a(JLjava/lang/String;)Lcom/twitter/library/api/moments/maker/q;
    .locals 2

    .prologue
    .line 15
    new-instance v0, Lcom/twitter/library/api/moments/maker/q;

    sget-object v1, Lcom/twitter/library/api/moments/maker/RecommendationType;->d:Lcom/twitter/library/api/moments/maker/RecommendationType;

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/twitter/library/api/moments/maker/q;-><init>(Lcom/twitter/library/api/moments/maker/RecommendationType;JLjava/lang/String;)V

    return-object v0
.end method

.method public static a(Lcom/twitter/library/api/moments/maker/RecommendationType;J)Lcom/twitter/library/api/moments/maker/q;
    .locals 3

    .prologue
    .line 21
    new-instance v0, Lcom/twitter/library/api/moments/maker/q;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/twitter/library/api/moments/maker/q;-><init>(Lcom/twitter/library/api/moments/maker/RecommendationType;JLjava/lang/String;)V

    return-object v0
.end method
