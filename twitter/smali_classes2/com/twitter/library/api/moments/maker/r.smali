.class public Lcom/twitter/library/api/moments/maker/r;
.super Laum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laum",
        "<",
        "Lcom/twitter/library/api/moments/maker/q;",
        "Lcfh;",
        "Lcom/twitter/library/api/moments/maker/p;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/Session;

.field private c:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Laum;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/twitter/library/api/moments/maker/r;->a:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/twitter/library/api/moments/maker/r;->b:Lcom/twitter/library/client/Session;

    .line 29
    iput-wide p3, p0, Lcom/twitter/library/api/moments/maker/r;->c:J

    .line 30
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/api/moments/maker/p;)Lcfh;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p1}, Lcom/twitter/library/api/moments/maker/p;->e()Lcfh;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/library/api/moments/maker/q;)Lcom/twitter/library/api/moments/maker/p;
    .locals 7

    .prologue
    .line 41
    new-instance v1, Lcom/twitter/library/api/moments/maker/p;

    iget-object v2, p0, Lcom/twitter/library/api/moments/maker/r;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/library/api/moments/maker/r;->b:Lcom/twitter/library/client/Session;

    iget-wide v4, p0, Lcom/twitter/library/api/moments/maker/r;->c:J

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/api/moments/maker/p;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcom/twitter/library/api/moments/maker/q;)V

    return-object v1
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Lcom/twitter/library/service/s;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Lcom/twitter/library/api/moments/maker/q;

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/moments/maker/r;->a(Lcom/twitter/library/api/moments/maker/q;)Lcom/twitter/library/api/moments/maker/p;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/library/service/s;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Lcom/twitter/library/api/moments/maker/p;

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/moments/maker/r;->a(Lcom/twitter/library/api/moments/maker/p;)Lcfh;

    move-result-object v0

    return-object v0
.end method
