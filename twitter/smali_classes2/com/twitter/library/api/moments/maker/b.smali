.class public Lcom/twitter/library/api/moments/maker/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/api/moments/maker/e$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/library/api/moments/maker/e$a",
        "<",
        "Lcfm;",
        "Lcom/twitter/model/core/z;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:Lcom/twitter/model/json/moments/maker/JsonCurateRequest;


# direct methods
.method public constructor <init>(JLjava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<+",
            "Lcfk;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-wide p1, p0, Lcom/twitter/library/api/moments/maker/b;->a:J

    .line 31
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 32
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfk;

    .line 33
    instance-of v3, v0, Lces;

    if-eqz v3, :cond_0

    .line 34
    check-cast v0, Lces;

    invoke-static {v0}, Lcom/twitter/model/json/moments/maker/JsonCurateOperation;->a(Lces;)Lcom/twitter/model/json/moments/maker/JsonCurateOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 35
    :cond_0
    instance-of v3, v0, Lcfc;

    if-eqz v3, :cond_1

    .line 36
    check-cast v0, Lcfc;

    invoke-static {v0}, Lcom/twitter/model/json/moments/maker/JsonCurateOperation;->a(Lcfc;)Lcom/twitter/model/json/moments/maker/JsonCurateOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 37
    :cond_1
    instance-of v3, v0, Lcew;

    if-eqz v3, :cond_2

    .line 38
    check-cast v0, Lcew;

    invoke-static {v0}, Lcom/twitter/model/json/moments/maker/JsonCurateOperation;->a(Lcew;)Lcom/twitter/model/json/moments/maker/JsonCurateOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 40
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Got illegal operation type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 43
    :cond_3
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/model/json/moments/maker/JsonCurateRequest;->a(Ljava/util/List;)Lcom/twitter/model/json/moments/maker/JsonCurateRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/moments/maker/b;->b:Lcom/twitter/model/json/moments/maker/JsonCurateRequest;

    .line 44
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    const-string/jumbo v0, "moments"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "curate"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/twitter/library/api/moments/maker/b;->a:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/twitter/model/json/moments/maker/JsonCurateRequest;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/library/api/moments/maker/b;->b:Lcom/twitter/model/json/moments/maker/JsonCurateRequest;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    const-string/jumbo v0, "curate"

    return-object v0
.end method

.method public d()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcfm;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    const-class v0, Lcfm;

    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lcom/twitter/model/json/common/a;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/twitter/library/api/moments/maker/b;->b()Lcom/twitter/model/json/moments/maker/JsonCurateRequest;

    move-result-object v0

    return-object v0
.end method
