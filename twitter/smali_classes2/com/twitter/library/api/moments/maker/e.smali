.class public Lcom/twitter/library/api/moments/maker/e;
.super Lcom/twitter/library/api/r;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/api/moments/maker/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Config::",
        "Lcom/twitter/library/api/moments/maker/e$a",
        "<TRes;TErr;>;Res:",
        "Ljava/lang/Object;",
        "Err:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/twitter/library/api/r",
        "<",
        "Lcom/twitter/library/api/i",
        "<TRes;TErr;>;>;"
    }
.end annotation


# instance fields
.field private final b:Lcom/twitter/library/api/moments/maker/e$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TConfig;"
        }
    .end annotation
.end field

.field private c:Lcom/twitter/util/collection/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/m",
            "<TRes;",
            "Lcom/twitter/library/api/moments/maker/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/moments/maker/e$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/client/Session;",
            "TConfig;)V"
        }
    .end annotation

    .prologue
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/twitter/library/api/moments/maker/e;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p3}, Lcom/twitter/library/api/moments/maker/e$a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/r;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 47
    iput-object p3, p0, Lcom/twitter/library/api/moments/maker/e;->b:Lcom/twitter/library/api/moments/maker/e$a;

    .line 48
    new-instance v0, Lcom/twitter/library/service/o;

    invoke-direct {v0}, Lcom/twitter/library/service/o;-><init>()V

    invoke-virtual {p0, v0}, Lcom/twitter/library/api/moments/maker/e;->a(Lcom/twitter/library/service/e;)V

    .line 49
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 7

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/library/api/moments/maker/e;->b:Lcom/twitter/library/api/moments/maker/e$a;

    invoke-interface {v0}, Lcom/twitter/library/api/moments/maker/e$a;->a()Ljava/util/List;

    move-result-object v0

    .line 55
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    .line 56
    const/4 v1, 0x0

    .line 58
    :try_start_0
    new-instance v0, Lcom/twitter/network/apache/entity/c;

    iget-object v3, p0, Lcom/twitter/library/api/moments/maker/e;->b:Lcom/twitter/library/api/moments/maker/e$a;

    invoke-interface {v3}, Lcom/twitter/library/api/moments/maker/e$a;->e()Lcom/twitter/model/json/common/a;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/model/json/common/g;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "UTF-8"

    invoke-direct {v0, v3, v4}, Lcom/twitter/network/apache/entity/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :try_start_1
    const-string/jumbo v1, "application/json"

    invoke-virtual {v0, v1}, Lcom/twitter/network/apache/entity/c;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 63
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/library/api/moments/maker/e;->J()Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 64
    invoke-virtual {v1, v2}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 65
    invoke-virtual {v1}, Lcom/twitter/library/service/d$a;->b()Lcom/twitter/library/service/d$a;

    move-result-object v1

    const-string/jumbo v2, "v"

    const-wide/32 v4, 0x57d6f22e

    .line 66
    invoke-virtual {v1, v2, v4, v5}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v1

    const-string/jumbo v2, "X-Twitter-UTCOffset"

    .line 67
    invoke-static {}, Lcom/twitter/util/aa;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/service/d$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    sget-object v2, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 68
    invoke-virtual {v1, v2}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    const-string/jumbo v2, "include_blocking"

    const/4 v3, 0x1

    .line 69
    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v1

    const-string/jumbo v2, "max_count"

    .line 70
    invoke-static {}, Lbsd;->o()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v1, v2, v4, v5}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 71
    invoke-virtual {v1}, Lcom/twitter/library/service/d$a;->d()Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 72
    invoke-virtual {v1}, Lcom/twitter/library/service/d$a;->c()Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 73
    invoke-virtual {v1}, Lcom/twitter/library/service/d$a;->e()Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 74
    invoke-virtual {v1, v0}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/apache/e;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    .line 63
    return-object v0

    .line 60
    :catch_0
    move-exception v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    .line 61
    :goto_1
    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 60
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<TRes;TErr;>;)V"
        }
    .end annotation

    .prologue
    .line 93
    if-eqz p3, :cond_0

    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/m;->a(Ljava/lang/Object;)Lcom/twitter/util/collection/m;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/moments/maker/e;->c:Lcom/twitter/util/collection/m;

    .line 102
    :goto_0
    return-void

    .line 95
    :cond_0
    invoke-virtual {p2}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    const/16 v1, 0x1f4

    if-lt v0, v1, :cond_1

    .line 96
    new-instance v0, Lcom/twitter/library/api/moments/maker/d;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/twitter/library/api/moments/maker/d;-><init>(I)V

    invoke-static {v0}, Lcom/twitter/util/collection/m;->b(Ljava/lang/Object;)Lcom/twitter/util/collection/m;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/moments/maker/e;->c:Lcom/twitter/util/collection/m;

    goto :goto_0

    .line 97
    :cond_1
    invoke-virtual {p2}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    const/16 v1, 0x190

    if-lt v0, v1, :cond_2

    .line 98
    new-instance v0, Lcom/twitter/library/api/moments/maker/d;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/twitter/library/api/moments/maker/d;-><init>(I)V

    invoke-static {v0}, Lcom/twitter/util/collection/m;->b(Ljava/lang/Object;)Lcom/twitter/util/collection/m;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/moments/maker/e;->c:Lcom/twitter/util/collection/m;

    goto :goto_0

    .line 100
    :cond_2
    new-instance v0, Lcom/twitter/library/api/moments/maker/d;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/library/api/moments/maker/d;-><init>(I)V

    invoke-static {v0}, Lcom/twitter/util/collection/m;->b(Ljava/lang/Object;)Lcom/twitter/util/collection/m;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/moments/maker/e;->c:Lcom/twitter/util/collection/m;

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 36
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/api/moments/maker/e;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<TRes;TErr;>;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/library/api/moments/maker/e;->b:Lcom/twitter/library/api/moments/maker/e$a;

    invoke-interface {v0}, Lcom/twitter/library/api/moments/maker/e$a;->d()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "app:twitter_service:moment_maker:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/moments/maker/e;->b:Lcom/twitter/library/api/moments/maker/e$a;

    invoke-interface {v1}, Lcom/twitter/library/api/moments/maker/e$a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/twitter/library/api/moments/maker/e;->b()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method

.method public g()Lcom/twitter/util/collection/m;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/collection/m",
            "<TRes;",
            "Lcom/twitter/library/api/moments/maker/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lcom/twitter/library/api/moments/maker/e;->c:Lcom/twitter/util/collection/m;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/m;

    return-object v0
.end method
