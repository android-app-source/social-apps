.class public Lcom/twitter/library/api/moments/maker/p;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcfh;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:Lcom/twitter/library/api/moments/maker/q;

.field private c:Lcfh;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcom/twitter/library/api/moments/maker/q;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 52
    const-class v0, Lcom/twitter/library/api/moments/maker/p;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 40
    sget-object v0, Lcfh;->a:Lcfh;

    iput-object v0, p0, Lcom/twitter/library/api/moments/maker/p;->c:Lcfh;

    .line 53
    iput-wide p3, p0, Lcom/twitter/library/api/moments/maker/p;->a:J

    .line 54
    iput-object p5, p0, Lcom/twitter/library/api/moments/maker/p;->b:Lcom/twitter/library/api/moments/maker/q;

    .line 55
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 60
    invoke-virtual {p0}, Lcom/twitter/library/api/moments/maker/p;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "X-Twitter-UTCOffset"

    .line 61
    invoke-static {}, Lcom/twitter/util/aa;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    const-string/jumbo v2, "moments"

    aput-object v2, v1, v3

    .line 62
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->b([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    const-string/jumbo v2, "get_recommended_tweets"

    aput-object v2, v1, v3

    .line 63
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "momentId"

    iget-wide v2, p0, Lcom/twitter/library/api/moments/maker/p;->a:J

    .line 64
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "recommendation_types"

    iget-object v2, p0, Lcom/twitter/library/api/moments/maker/p;->b:Lcom/twitter/library/api/moments/maker/q;

    iget-object v2, v2, Lcom/twitter/library/api/moments/maker/q;->a:Lcom/twitter/library/api/moments/maker/RecommendationType;

    .line 65
    invoke-virtual {v2}, Lcom/twitter/library/api/moments/maker/RecommendationType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    iget-object v2, p0, Lcom/twitter/library/api/moments/maker/p;->b:Lcom/twitter/library/api/moments/maker/q;

    iget-wide v2, v2, Lcom/twitter/library/api/moments/maker/q;->c:J

    .line 66
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "count"

    const-wide/16 v2, 0x32

    .line 67
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->c()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->b()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lcom/twitter/library/api/moments/maker/p;->b:Lcom/twitter/library/api/moments/maker/q;

    iget-object v1, v1, Lcom/twitter/library/api/moments/maker/q;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 71
    const-string/jumbo v1, "query"

    iget-object v2, p0, Lcom/twitter/library/api/moments/maker/p;->b:Lcom/twitter/library/api/moments/maker/q;

    iget-object v2, v2, Lcom/twitter/library/api/moments/maker/q;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 73
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcfh;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-static {p3}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/i;

    invoke-virtual {v0}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcfh;->a:Lcfh;

    .line 87
    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfh;

    iput-object v0, p0, Lcom/twitter/library/api/moments/maker/p;->c:Lcfh;

    .line 90
    :cond_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/u;->a(Lcom/twitter/network/HttpOperation;)V

    .line 91
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 28
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/api/moments/maker/p;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcfh;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    const-class v0, Lcfh;

    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcfh;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/library/api/moments/maker/p;->c:Lcfh;

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/twitter/library/api/moments/maker/p;->b()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
