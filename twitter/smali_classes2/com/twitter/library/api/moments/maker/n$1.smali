.class Lcom/twitter/library/api/moments/maker/n$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/api/moments/maker/n;->a(JLcfj;)Lrx/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/d",
        "<",
        "Ljava/lang/Long;",
        "Lrx/g",
        "<",
        "Lcom/twitter/util/collection/m",
        "<",
        "Lcfm;",
        "Lcom/twitter/library/api/moments/maker/d;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcom/twitter/library/api/moments/maker/n;


# direct methods
.method constructor <init>(Lcom/twitter/library/api/moments/maker/n;J)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/twitter/library/api/moments/maker/n$1;->b:Lcom/twitter/library/api/moments/maker/n;

    iput-wide p2, p0, Lcom/twitter/library/api/moments/maker/n$1;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 96
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/moments/maker/n$1;->a(Ljava/lang/Long;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Long;)Lrx/g;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            ")",
            "Lrx/g",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcfm;",
            "Lcom/twitter/library/api/moments/maker/d;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 100
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/twitter/library/api/moments/maker/n$1;->b:Lcom/twitter/library/api/moments/maker/n;

    invoke-static {v0}, Lcom/twitter/library/api/moments/maker/n;->a(Lcom/twitter/library/api/moments/maker/n;)Lcom/twitter/library/api/moments/maker/f;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/api/moments/maker/s;

    iget-wide v2, p0, Lcom/twitter/library/api/moments/maker/n$1;->a:J

    .line 102
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/twitter/library/api/moments/maker/s;-><init>(JJ)V

    .line 101
    invoke-virtual {v0, v1}, Lcom/twitter/library/api/moments/maker/f;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 102
    invoke-static {}, Lcom/twitter/library/api/moments/maker/n;->a()Lcom/twitter/util/collection/m;

    move-result-object v1

    .line 101
    invoke-static {v0, v1}, Lcre;->a(Lrx/c;Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    .line 104
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/twitter/library/api/moments/maker/n;->a()Lcom/twitter/util/collection/m;

    move-result-object v0

    invoke-static {v0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    goto :goto_0
.end method
