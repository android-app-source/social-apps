.class public Lcom/twitter/library/api/moments/maker/l;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/api/moments/maker/n;

.field private final b:Lbsb;

.field private final c:Lcom/twitter/library/api/moments/maker/j;

.field private final d:Lcif;

.field private final e:Lcii;


# direct methods
.method public constructor <init>(Lcom/twitter/library/api/moments/maker/n;Lbsb;Lcom/twitter/library/api/moments/maker/j;Lcif;Lcii;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p4, p0, Lcom/twitter/library/api/moments/maker/l;->d:Lcif;

    .line 56
    iput-object p1, p0, Lcom/twitter/library/api/moments/maker/l;->a:Lcom/twitter/library/api/moments/maker/n;

    .line 57
    iput-object p2, p0, Lcom/twitter/library/api/moments/maker/l;->b:Lbsb;

    .line 58
    iput-object p3, p0, Lcom/twitter/library/api/moments/maker/l;->c:Lcom/twitter/library/api/moments/maker/j;

    .line 59
    iput-object p5, p0, Lcom/twitter/library/api/moments/maker/l;->e:Lcii;

    .line 60
    return-void
.end method


# virtual methods
.method public a(J)I
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 78
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 79
    iget-object v0, p0, Lcom/twitter/library/api/moments/maker/l;->c:Lcom/twitter/library/api/moments/maker/j;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/api/moments/maker/j;->a(J)Ljava/util/List;

    move-result-object v5

    .line 80
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v3

    .line 141
    :goto_0
    return v0

    .line 83
    :cond_0
    invoke-static {v5}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfd;

    .line 86
    instance-of v1, v0, Lcfk;

    if-eqz v1, :cond_1

    .line 87
    const-class v1, Lcfk;

    .line 88
    invoke-static {v5, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v2

    .line 89
    invoke-static {v2}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcfd;

    .line 90
    iget-object v6, p0, Lcom/twitter/library/api/moments/maker/l;->a:Lcom/twitter/library/api/moments/maker/n;

    invoke-virtual {v6, p1, p2, v2}, Lcom/twitter/library/api/moments/maker/n;->a(JLjava/util/List;)Lrx/g;

    move-result-object v2

    .line 125
    :goto_1
    iget-object v6, p0, Lcom/twitter/library/api/moments/maker/l;->c:Lcom/twitter/library/api/moments/maker/j;

    .line 126
    invoke-interface {v5, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v6, p1, p2, v1}, Lcom/twitter/library/api/moments/maker/j;->a(JLjava/lang/Integer;)V

    .line 127
    invoke-virtual {v2}, Lrx/g;->d()Lcwt;

    move-result-object v1

    invoke-virtual {v1}, Lcwt;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/util/collection/m;

    .line 128
    invoke-virtual {v1}, Lcom/twitter/util/collection/m;->c()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 129
    invoke-virtual {v1}, Lcom/twitter/util/collection/m;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfm;

    .line 130
    iget-object v1, p0, Lcom/twitter/library/api/moments/maker/l;->b:Lbsb;

    iget-object v0, v0, Lcfm;->a:Lcen;

    invoke-virtual {v1, v0}, Lbsb;->a(Lcen;)V

    .line 131
    iget-object v0, p0, Lcom/twitter/library/api/moments/maker/l;->c:Lcom/twitter/library/api/moments/maker/j;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/api/moments/maker/j;->b(J)V

    .line 132
    iget-object v0, p0, Lcom/twitter/library/api/moments/maker/l;->d:Lcif;

    invoke-virtual {v0, p1, p2}, Lcif;->a(J)V

    move v0, v4

    .line 133
    goto :goto_0

    .line 91
    :cond_1
    instance-of v1, v0, Lcfb;

    if-eqz v1, :cond_3

    .line 92
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 93
    const-class v1, Lcfb;

    .line 94
    invoke-static {v5, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v6

    .line 96
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcfb;

    .line 97
    invoke-interface {v1}, Lcfb;->a()Lcfa;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_2

    .line 99
    :cond_2
    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 100
    invoke-static {v6}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcfd;

    .line 101
    iget-object v6, p0, Lcom/twitter/library/api/moments/maker/l;->a:Lcom/twitter/library/api/moments/maker/n;

    invoke-virtual {v6, p1, p2, v1}, Lcom/twitter/library/api/moments/maker/n;->b(JLjava/util/List;)Lrx/g;

    move-result-object v1

    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    .line 102
    goto :goto_1

    :cond_3
    instance-of v1, v0, Lcey;

    if-eqz v1, :cond_4

    move-object v1, v0

    .line 103
    check-cast v1, Lcey;

    .line 104
    invoke-interface {v1}, Lcey;->a()Lcex;

    move-result-object v1

    .line 106
    iget-object v2, p0, Lcom/twitter/library/api/moments/maker/l;->a:Lcom/twitter/library/api/moments/maker/n;

    invoke-virtual {v2, p1, p2, v1}, Lcom/twitter/library/api/moments/maker/n;->a(JLcex;)Lrx/g;

    move-result-object v2

    move-object v1, v0

    .line 107
    goto/16 :goto_1

    :cond_4
    instance-of v1, v0, Lcff;

    if-eqz v1, :cond_6

    move-object v1, v0

    .line 108
    check-cast v1, Lcff;

    .line 110
    invoke-virtual {v1}, Lcff;->c()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 111
    iget-object v2, p0, Lcom/twitter/library/api/moments/maker/l;->a:Lcom/twitter/library/api/moments/maker/n;

    .line 112
    invoke-virtual {v1}, Lcff;->a()Lcex;

    move-result-object v6

    .line 111
    invoke-virtual {v2, p1, p2, v6}, Lcom/twitter/library/api/moments/maker/n;->a(JLcex;)Lrx/g;

    move-result-object v2

    goto/16 :goto_1

    .line 114
    :cond_5
    iget-object v2, p0, Lcom/twitter/library/api/moments/maker/l;->a:Lcom/twitter/library/api/moments/maker/n;

    .line 115
    invoke-virtual {v1}, Lcff;->b()Lcfa;

    move-result-object v6

    invoke-static {v6}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    .line 114
    invoke-virtual {v2, p1, p2, v6}, Lcom/twitter/library/api/moments/maker/n;->b(JLjava/util/List;)Lrx/g;

    move-result-object v2

    goto/16 :goto_1

    .line 117
    :cond_6
    instance-of v1, v0, Lcfj;

    if-eqz v1, :cond_7

    move-object v1, v0

    .line 118
    check-cast v1, Lcfj;

    .line 120
    iget-object v2, p0, Lcom/twitter/library/api/moments/maker/l;->a:Lcom/twitter/library/api/moments/maker/n;

    invoke-virtual {v2, p1, p2, v1}, Lcom/twitter/library/api/moments/maker/n;->a(JLcfj;)Lrx/g;

    move-result-object v2

    goto/16 :goto_1

    .line 123
    :cond_7
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Got unrecognized operation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 134
    :cond_8
    invoke-virtual {v1}, Lcom/twitter/util/collection/m;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/moments/maker/d;

    iget v1, v1, Lcom/twitter/library/api/moments/maker/d;->a:I

    if-ne v1, v3, :cond_9

    .line 135
    iget-object v1, p0, Lcom/twitter/library/api/moments/maker/l;->c:Lcom/twitter/library/api/moments/maker/j;

    invoke-virtual {v1, p1, p2}, Lcom/twitter/library/api/moments/maker/j;->b(J)V

    .line 136
    iget-object v1, p0, Lcom/twitter/library/api/moments/maker/l;->d:Lcif;

    invoke-virtual {v1, p1, p2}, Lcif;->a(J)V

    .line 137
    iget-object v1, p0, Lcom/twitter/library/api/moments/maker/l;->e:Lcii;

    new-instance v2, Lcih;

    invoke-direct {v2, v0}, Lcih;-><init>(Lcfd;)V

    invoke-virtual {v1, p1, p2, v2}, Lcii;->a(JLcih;)V

    move v0, v4

    .line 138
    goto/16 :goto_0

    .line 140
    :cond_9
    iget-object v0, p0, Lcom/twitter/library/api/moments/maker/l;->c:Lcom/twitter/library/api/moments/maker/j;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/twitter/library/api/moments/maker/j;->a(JLjava/lang/Integer;)V

    .line 141
    const/4 v0, 0x3

    goto/16 :goto_0
.end method
