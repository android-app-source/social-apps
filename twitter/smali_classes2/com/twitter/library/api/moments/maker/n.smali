.class public Lcom/twitter/library/api/moments/maker/n;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Lcom/twitter/util/collection/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcfm;",
            "Lcom/twitter/library/api/moments/maker/d;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/twitter/library/api/moments/maker/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/library/api/moments/maker/f",
            "<",
            "Lcom/twitter/library/api/moments/maker/b;",
            "Lcfm;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/library/api/moments/maker/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/library/api/moments/maker/f",
            "<",
            "Lcom/twitter/library/api/moments/maker/h;",
            "Lcfm;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/library/api/moments/maker/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/library/api/moments/maker/f",
            "<",
            "Lcom/twitter/library/api/moments/maker/i;",
            "Lcfm;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/twitter/library/api/moments/maker/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/library/api/moments/maker/f",
            "<",
            "Lcom/twitter/library/api/moments/maker/s;",
            "Lcfm;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/twitter/library/api/upload/m;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    new-instance v0, Lcom/twitter/library/api/moments/maker/d;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/library/api/moments/maker/d;-><init>(I)V

    .line 38
    invoke-static {v0}, Lcom/twitter/util/collection/m;->b(Ljava/lang/Object;)Lcom/twitter/util/collection/m;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/api/moments/maker/n;->a:Lcom/twitter/util/collection/m;

    .line 37
    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/api/moments/maker/f;Lcom/twitter/library/api/moments/maker/f;Lcom/twitter/library/api/moments/maker/f;Lcom/twitter/library/api/moments/maker/f;Lcom/twitter/library/api/upload/m;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/api/moments/maker/f",
            "<",
            "Lcom/twitter/library/api/moments/maker/b;",
            "Lcfm;",
            "Lcom/twitter/model/core/z;",
            ">;",
            "Lcom/twitter/library/api/moments/maker/f",
            "<",
            "Lcom/twitter/library/api/moments/maker/h;",
            "Lcfm;",
            "Lcom/twitter/model/core/z;",
            ">;",
            "Lcom/twitter/library/api/moments/maker/f",
            "<",
            "Lcom/twitter/library/api/moments/maker/i;",
            "Lcfm;",
            "Lcom/twitter/model/core/z;",
            ">;",
            "Lcom/twitter/library/api/moments/maker/f",
            "<",
            "Lcom/twitter/library/api/moments/maker/s;",
            "Lcfm;",
            "Lcom/twitter/model/core/z;",
            ">;",
            "Lcom/twitter/library/api/upload/m;",
            ")V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/twitter/library/api/moments/maker/n;->b:Lcom/twitter/library/api/moments/maker/f;

    .line 62
    iput-object p2, p0, Lcom/twitter/library/api/moments/maker/n;->c:Lcom/twitter/library/api/moments/maker/f;

    .line 63
    iput-object p3, p0, Lcom/twitter/library/api/moments/maker/n;->d:Lcom/twitter/library/api/moments/maker/f;

    .line 64
    iput-object p4, p0, Lcom/twitter/library/api/moments/maker/n;->e:Lcom/twitter/library/api/moments/maker/f;

    .line 65
    iput-object p5, p0, Lcom/twitter/library/api/moments/maker/n;->f:Lcom/twitter/library/api/upload/m;

    .line 66
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/api/moments/maker/n;)Lcom/twitter/library/api/moments/maker/f;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/library/api/moments/maker/n;->e:Lcom/twitter/library/api/moments/maker/f;

    return-object v0
.end method

.method static synthetic a()Lcom/twitter/util/collection/m;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/twitter/library/api/moments/maker/n;->a:Lcom/twitter/util/collection/m;

    return-object v0
.end method


# virtual methods
.method public a(JLcex;)Lrx/g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcex;",
            ")",
            "Lrx/g",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcfm;",
            "Lcom/twitter/library/api/moments/maker/d;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/library/api/moments/maker/n;->c:Lcom/twitter/library/api/moments/maker/f;

    new-instance v1, Lcom/twitter/library/api/moments/maker/h;

    invoke-direct {v1, p1, p2, p3}, Lcom/twitter/library/api/moments/maker/h;-><init>(JLcex;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/moments/maker/f;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/api/moments/maker/n;->a:Lcom/twitter/util/collection/m;

    invoke-static {v0, v1}, Lcre;->a(Lrx/c;Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(JLcfj;)Lrx/g;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcfj;",
            ")",
            "Lrx/g",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcfm;",
            "Lcom/twitter/library/api/moments/maker/d;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/library/api/moments/maker/n;->f:Lcom/twitter/library/api/upload/m;

    iget-object v1, p3, Lcfj;->a:Landroid/net/Uri;

    sget-object v2, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    sget-object v3, Lcom/twitter/library/api/upload/MediaUsage;->a:Lcom/twitter/library/api/upload/MediaUsage;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/api/upload/m;->b(Landroid/net/Uri;Lcom/twitter/media/model/MediaType;Lcom/twitter/library/api/upload/MediaUsage;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/api/moments/maker/n$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/twitter/library/api/moments/maker/n$1;-><init>(Lcom/twitter/library/api/moments/maker/n;J)V

    .line 96
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 94
    return-object v0
.end method

.method public a(JLjava/util/List;)Lrx/g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Lcfk;",
            ">;)",
            "Lrx/g",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcfm;",
            "Lcom/twitter/library/api/moments/maker/d;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/library/api/moments/maker/n;->b:Lcom/twitter/library/api/moments/maker/f;

    new-instance v1, Lcom/twitter/library/api/moments/maker/b;

    .line 72
    invoke-static {p3}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, p1, p2, v2}, Lcom/twitter/library/api/moments/maker/b;-><init>(JLjava/util/List;)V

    .line 71
    invoke-virtual {v0, v1}, Lcom/twitter/library/api/moments/maker/f;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/api/moments/maker/n;->a:Lcom/twitter/util/collection/m;

    invoke-static {v0, v1}, Lcre;->a(Lrx/c;Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public b(JLjava/util/List;)Lrx/g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Lcfa;",
            ">;)",
            "Lrx/g",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcfm;",
            "Lcom/twitter/library/api/moments/maker/d;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/library/api/moments/maker/n;->d:Lcom/twitter/library/api/moments/maker/f;

    new-instance v1, Lcom/twitter/library/api/moments/maker/i;

    .line 80
    invoke-static {p3}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, p1, p2, v2}, Lcom/twitter/library/api/moments/maker/i;-><init>(JLjava/util/List;)V

    .line 79
    invoke-virtual {v0, v1}, Lcom/twitter/library/api/moments/maker/f;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/api/moments/maker/n;->a:Lcom/twitter/util/collection/m;

    invoke-static {v0, v1}, Lcre;->a(Lrx/c;Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    return-object v0
.end method
