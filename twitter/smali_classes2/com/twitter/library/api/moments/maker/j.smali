.class public Lcom/twitter/library/api/moments/maker/j;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/database/lru/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/Long;",
            "Lcfe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/database/lru/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/Long;",
            "Lcfe;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/twitter/library/api/moments/maker/j;->a:Lcom/twitter/database/lru/l;

    .line 33
    return-void
.end method


# virtual methods
.method public a(J)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lcfd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 39
    iget-object v0, p0, Lcom/twitter/library/api/moments/maker/j;->a:Lcom/twitter/database/lru/l;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/database/lru/l;->b(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    invoke-virtual {v0}, Lrx/g;->d()Lcwt;

    move-result-object v0

    invoke-virtual {v0}, Lcwt;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfe;

    .line 40
    if-nez v0, :cond_0

    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcfe;->c:Ljava/util/List;

    goto :goto_0
.end method

.method public a(JLjava/lang/Integer;)V
    .locals 3

    .prologue
    .line 51
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 52
    iget-object v0, p0, Lcom/twitter/library/api/moments/maker/j;->a:Lcom/twitter/database/lru/l;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/api/moments/maker/j$1;

    invoke-direct {v2, p0, p3}, Lcom/twitter/library/api/moments/maker/j$1;-><init>(Lcom/twitter/library/api/moments/maker/j;Ljava/lang/Integer;)V

    invoke-interface {v0, v1, v2}, Lcom/twitter/database/lru/l;->a(Ljava/lang/Object;Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lrx/g;->d()Lcwt;

    move-result-object v0

    invoke-virtual {v0}, Lcwt;->a()Ljava/lang/Object;

    .line 59
    return-void
.end method

.method public b(J)V
    .locals 3

    .prologue
    .line 68
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 69
    iget-object v0, p0, Lcom/twitter/library/api/moments/maker/j;->a:Lcom/twitter/database/lru/l;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/api/moments/maker/j$2;

    invoke-direct {v2, p0}, Lcom/twitter/library/api/moments/maker/j$2;-><init>(Lcom/twitter/library/api/moments/maker/j;)V

    invoke-interface {v0, v1, v2}, Lcom/twitter/database/lru/l;->a(Ljava/lang/Object;Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Lrx/g;->d()Lcwt;

    move-result-object v0

    invoke-virtual {v0}, Lcwt;->a()Ljava/lang/Object;

    .line 83
    return-void
.end method

.method public c(J)V
    .locals 3

    .prologue
    .line 91
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 92
    iget-object v0, p0, Lcom/twitter/library/api/moments/maker/j;->a:Lcom/twitter/database/lru/l;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/database/lru/l;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    invoke-virtual {v0}, Lrx/g;->d()Lcwt;

    move-result-object v0

    invoke-virtual {v0}, Lcwt;->a()Ljava/lang/Object;

    .line 93
    return-void
.end method
