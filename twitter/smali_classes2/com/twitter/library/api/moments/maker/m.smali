.class public final Lcom/twitter/library/api/moments/maker/m;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lcom/twitter/library/api/moments/maker/l;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/api/moments/maker/n;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lbsb;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/api/moments/maker/j;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcif;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcii;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/twitter/library/api/moments/maker/m;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/library/api/moments/maker/m;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcta;Lcta;Lcta;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcom/twitter/library/api/moments/maker/n;",
            ">;",
            "Lcta",
            "<",
            "Lbsb;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/api/moments/maker/j;",
            ">;",
            "Lcta",
            "<",
            "Lcif;",
            ">;",
            "Lcta",
            "<",
            "Lcii;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    sget-boolean v0, Lcom/twitter/library/api/moments/maker/m;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 33
    :cond_0
    iput-object p1, p0, Lcom/twitter/library/api/moments/maker/m;->b:Lcta;

    .line 34
    sget-boolean v0, Lcom/twitter/library/api/moments/maker/m;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 35
    :cond_1
    iput-object p2, p0, Lcom/twitter/library/api/moments/maker/m;->c:Lcta;

    .line 36
    sget-boolean v0, Lcom/twitter/library/api/moments/maker/m;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 37
    :cond_2
    iput-object p3, p0, Lcom/twitter/library/api/moments/maker/m;->d:Lcta;

    .line 38
    sget-boolean v0, Lcom/twitter/library/api/moments/maker/m;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 39
    :cond_3
    iput-object p4, p0, Lcom/twitter/library/api/moments/maker/m;->e:Lcta;

    .line 40
    sget-boolean v0, Lcom/twitter/library/api/moments/maker/m;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 41
    :cond_4
    iput-object p5, p0, Lcom/twitter/library/api/moments/maker/m;->f:Lcta;

    .line 42
    return-void
.end method

.method public static a(Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcom/twitter/library/api/moments/maker/n;",
            ">;",
            "Lcta",
            "<",
            "Lbsb;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/api/moments/maker/j;",
            ">;",
            "Lcta",
            "<",
            "Lcif;",
            ">;",
            "Lcta",
            "<",
            "Lcii;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Lcom/twitter/library/api/moments/maker/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    new-instance v0, Lcom/twitter/library/api/moments/maker/m;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/moments/maker/m;-><init>(Lcta;Lcta;Lcta;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/library/api/moments/maker/l;
    .locals 6

    .prologue
    .line 46
    new-instance v0, Lcom/twitter/library/api/moments/maker/l;

    iget-object v1, p0, Lcom/twitter/library/api/moments/maker/m;->b:Lcta;

    .line 47
    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/moments/maker/n;

    iget-object v2, p0, Lcom/twitter/library/api/moments/maker/m;->c:Lcta;

    .line 48
    invoke-interface {v2}, Lcta;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbsb;

    iget-object v3, p0, Lcom/twitter/library/api/moments/maker/m;->d:Lcta;

    .line 49
    invoke-interface {v3}, Lcta;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/moments/maker/j;

    iget-object v4, p0, Lcom/twitter/library/api/moments/maker/m;->e:Lcta;

    .line 50
    invoke-interface {v4}, Lcta;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcif;

    iget-object v5, p0, Lcom/twitter/library/api/moments/maker/m;->f:Lcta;

    .line 51
    invoke-interface {v5}, Lcta;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcii;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/moments/maker/l;-><init>(Lcom/twitter/library/api/moments/maker/n;Lbsb;Lcom/twitter/library/api/moments/maker/j;Lcif;Lcii;)V

    .line 46
    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lcom/twitter/library/api/moments/maker/m;->a()Lcom/twitter/library/api/moments/maker/l;

    move-result-object v0

    return-object v0
.end method
