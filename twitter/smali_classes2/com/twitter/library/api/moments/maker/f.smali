.class public Lcom/twitter/library/api/moments/maker/f;
.super Laum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Config::",
        "Lcom/twitter/library/api/moments/maker/e$a",
        "<TRes;TErr;>;Res:",
        "Ljava/lang/Object;",
        "Err:",
        "Ljava/lang/Object;",
        ">",
        "Laum",
        "<TConfig;",
        "Lcom/twitter/util/collection/m",
        "<TRes;",
        "Lcom/twitter/library/api/moments/maker/d;",
        ">;",
        "Lcom/twitter/library/api/moments/maker/e",
        "<TConfig;TRes;TErr;>;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/Session;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Laum;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/twitter/library/api/moments/maker/f;->a:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/twitter/library/api/moments/maker/f;->b:Lcom/twitter/library/client/Session;

    .line 29
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/api/moments/maker/e$a;)Lcom/twitter/library/api/moments/maker/e;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TConfig;)",
            "Lcom/twitter/library/api/moments/maker/e",
            "<TConfig;TRes;TErr;>;"
        }
    .end annotation

    .prologue
    .line 40
    new-instance v1, Lcom/twitter/library/api/moments/maker/e;

    iget-object v2, p0, Lcom/twitter/library/api/moments/maker/f;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/library/api/moments/maker/f;->b:Lcom/twitter/library/client/Session;

    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/moments/maker/e$a;

    invoke-direct {v1, v2, v3, v0}, Lcom/twitter/library/api/moments/maker/e;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/moments/maker/e$a;)V

    return-object v1
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Lcom/twitter/library/service/s;
    .locals 1

    .prologue
    .line 18
    check-cast p1, Lcom/twitter/library/api/moments/maker/e$a;

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/moments/maker/f;->a(Lcom/twitter/library/api/moments/maker/e$a;)Lcom/twitter/library/api/moments/maker/e;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/library/api/moments/maker/e;)Lcom/twitter/util/collection/m;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/api/moments/maker/e",
            "<TConfig;TRes;TErr;>;)",
            "Lcom/twitter/util/collection/m",
            "<TRes;",
            "Lcom/twitter/library/api/moments/maker/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p1}, Lcom/twitter/library/api/moments/maker/e;->g()Lcom/twitter/util/collection/m;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/library/service/s;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    check-cast p1, Lcom/twitter/library/api/moments/maker/e;

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/moments/maker/f;->a(Lcom/twitter/library/api/moments/maker/e;)Lcom/twitter/util/collection/m;

    move-result-object v0

    return-object v0
.end method
