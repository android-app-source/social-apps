.class public Lcom/twitter/library/api/moments/maker/h;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/api/moments/maker/e$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/library/api/moments/maker/e$a",
        "<",
        "Lcfm;",
        "Lcom/twitter/model/core/z;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:Lcex;


# direct methods
.method public constructor <init>(JLcex;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-wide p1, p0, Lcom/twitter/library/api/moments/maker/h;->a:J

    .line 25
    iput-object p3, p0, Lcom/twitter/library/api/moments/maker/h;->b:Lcex;

    .line 26
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    const-string/jumbo v0, "moments"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "update"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/twitter/library/api/moments/maker/h;->a:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/twitter/model/json/moments/maker/JsonUpdateMomentRequest;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/library/api/moments/maker/h;->b:Lcex;

    invoke-static {v0}, Lcom/twitter/model/json/moments/maker/JsonUpdateMomentRequest;->a(Lcex;)Lcom/twitter/model/json/moments/maker/JsonUpdateMomentRequest;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    const-string/jumbo v0, "update"

    return-object v0
.end method

.method public d()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcfm;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    const-class v0, Lcfm;

    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lcom/twitter/model/json/common/a;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/twitter/library/api/moments/maker/h;->b()Lcom/twitter/model/json/moments/maker/JsonUpdateMomentRequest;

    move-result-object v0

    return-object v0
.end method
