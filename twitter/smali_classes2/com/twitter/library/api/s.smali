.class public Lcom/twitter/library/api/s;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/api/s$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/y;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/revenue/a;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/twitter/model/timeline/q;

.field public final d:Lcom/twitter/model/timeline/u;

.field public final e:Lcom/twitter/model/livevideo/b;

.field public final f:Z

.field public final g:I


# direct methods
.method constructor <init>(Lcom/twitter/library/api/s$a;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iget-object v0, p1, Lcom/twitter/library/api/s$a;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/library/api/s;->a:Ljava/util/List;

    .line 39
    iget-object v0, p1, Lcom/twitter/library/api/s$a;->b:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/library/api/s;->b:Ljava/util/List;

    .line 40
    iget-object v0, p1, Lcom/twitter/library/api/s$a;->c:Lcom/twitter/model/timeline/q;

    sget-object v1, Lcom/twitter/model/timeline/q;->a:Lcom/twitter/model/timeline/q;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/q;

    iput-object v0, p0, Lcom/twitter/library/api/s;->c:Lcom/twitter/model/timeline/q;

    .line 41
    iget-object v0, p1, Lcom/twitter/library/api/s$a;->d:Lcom/twitter/model/timeline/u;

    iput-object v0, p0, Lcom/twitter/library/api/s;->d:Lcom/twitter/model/timeline/u;

    .line 42
    iget-object v0, p1, Lcom/twitter/library/api/s$a;->e:Lcom/twitter/model/livevideo/b;

    iput-object v0, p0, Lcom/twitter/library/api/s;->e:Lcom/twitter/model/livevideo/b;

    .line 43
    iget-boolean v0, p1, Lcom/twitter/library/api/s$a;->f:Z

    iput-boolean v0, p0, Lcom/twitter/library/api/s;->f:Z

    .line 44
    iget v0, p1, Lcom/twitter/library/api/s$a;->g:I

    iput v0, p0, Lcom/twitter/library/api/s;->g:I

    .line 45
    return-void
.end method
