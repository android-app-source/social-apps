.class public Lcom/twitter/library/api/search/c;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/twitter/model/search/b;",
        ">;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/twitter/library/api/search/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 30
    iput-wide p3, p0, Lcom/twitter/library/api/search/c;->a:J

    .line 31
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 4

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/twitter/library/api/search/c;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->a:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 37
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "saved_searches"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "list"

    aput-object v3, v1, v2

    .line 38
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    .line 36
    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/search/b;",
            ">;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 60
    invoke-virtual {p0}, Lcom/twitter/library/api/search/c;->S()Laut;

    move-result-object v1

    .line 61
    invoke-virtual {p0}, Lcom/twitter/library/api/search/c;->R()Lcom/twitter/library/provider/t;

    move-result-object v2

    .line 62
    const/4 v3, 0x6

    invoke-virtual {v2, v0, v3, v1}, Lcom/twitter/library/provider/t;->a(Ljava/util/List;ILaut;)I

    move-result v0

    .line 63
    if-lez v0, :cond_0

    .line 64
    const/4 v0, 0x3

    .line 65
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v4

    .line 64
    invoke-virtual {v2, v0, v4, v5}, Lcom/twitter/library/provider/t;->a(IJ)J

    .line 67
    :cond_0
    invoke-virtual {v1}, Laut;->a()V

    .line 69
    :cond_1
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 25
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/api/search/c;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/api/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/search/b;",
            ">;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    const-class v0, Lcom/twitter/model/search/b;

    const-class v1, Lcom/twitter/model/core/z;

    invoke-static {v0, v1}, Lcom/twitter/library/api/j;->a(Ljava/lang/Class;Ljava/lang/Class;)Lcom/twitter/library/api/j;

    move-result-object v0

    return-object v0
.end method

.method protected b(Lcom/twitter/library/service/u;)Z
    .locals 6

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/twitter/library/api/search/c;->R()Lcom/twitter/library/provider/t;

    move-result-object v0

    .line 50
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/t;->b(I)J

    move-result-wide v0

    .line 51
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/twitter/library/api/search/c;->a:J

    add-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/twitter/library/api/search/c;->b()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
