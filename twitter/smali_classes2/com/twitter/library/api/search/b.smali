.class public Lcom/twitter/library/api/search/b;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcom/twitter/model/search/b;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 33
    const-class v0, Lcom/twitter/library/api/search/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 34
    iput-object p3, p0, Lcom/twitter/library/api/search/b;->a:Ljava/lang/String;

    .line 35
    iput-wide p4, p0, Lcom/twitter/library/api/search/b;->b:J

    .line 36
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 41
    invoke-virtual {p0}, Lcom/twitter/library/api/search/b;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 42
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 43
    invoke-virtual {p0}, Lcom/twitter/library/api/search/b;->L()I

    move-result v1

    if-nez v1, :cond_0

    .line 44
    new-array v1, v5, [Ljava/lang/Object;

    const-string/jumbo v2, "saved_searches"

    aput-object v2, v1, v3

    const-string/jumbo v2, "create"

    aput-object v2, v1, v4

    .line 45
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "query"

    iget-object v2, p0, Lcom/twitter/library/api/search/b;->a:Ljava/lang/String;

    .line 46
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    .line 49
    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string/jumbo v2, "saved_searches"

    aput-object v2, v1, v3

    const-string/jumbo v2, "destroy"

    aput-object v2, v1, v4

    iget-wide v2, p0, Lcom/twitter/library/api/search/b;->b:J

    .line 50
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/search/b;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {p0}, Lcom/twitter/library/api/search/b;->R()Lcom/twitter/library/provider/t;

    move-result-object v1

    .line 65
    invoke-virtual {p0}, Lcom/twitter/library/api/search/b;->S()Laut;

    move-result-object v2

    .line 66
    invoke-virtual {p0}, Lcom/twitter/library/api/search/b;->L()I

    move-result v0

    if-nez v0, :cond_1

    .line 67
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/search/b;

    .line 68
    const/4 v3, 0x6

    invoke-virtual {v1, v0, v3, v2}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/model/search/b;ILaut;)J

    .line 72
    :goto_0
    invoke-virtual {v2}, Laut;->a()V

    .line 74
    :cond_0
    return-void

    .line 70
    :cond_1
    iget-wide v4, p0, Lcom/twitter/library/api/search/b;->b:J

    invoke-virtual {v1, v4, v5, v2}, Lcom/twitter/library/provider/t;->b(JLaut;)I

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 24
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/api/search/b;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/api/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcom/twitter/model/search/b;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    const-class v0, Lcom/twitter/model/search/b;

    const-class v1, Lcom/twitter/model/core/z;

    invoke-static {v0, v1}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/twitter/library/api/search/b;->b()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
