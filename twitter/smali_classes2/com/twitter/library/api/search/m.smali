.class public Lcom/twitter/library/api/search/m;
.super Lbao;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbao",
        "<",
        "Lcom/twitter/library/api/y;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:I

.field private final h:Ljava/lang/String;

.field private i:Lcom/twitter/library/api/search/TwitterTypeAheadGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;IILjava/lang/String;)V
    .locals 8

    .prologue
    .line 28
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/api/search/m;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;IILjava/lang/String;Z)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;IILjava/lang/String;Z)V
    .locals 2

    .prologue
    .line 34
    const-class v0, Lcom/twitter/library/api/search/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/service/v;

    invoke-direct {v1, p2}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    invoke-direct {p0, p1, v0, v1, p7}, Lbao;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;Z)V

    .line 35
    iput-object p3, p0, Lcom/twitter/library/api/search/m;->a:Ljava/lang/String;

    .line 36
    iput p4, p0, Lcom/twitter/library/api/search/m;->b:I

    .line 37
    iput p5, p0, Lcom/twitter/library/api/search/m;->c:I

    .line 38
    iput-object p6, p0, Lcom/twitter/library/api/search/m;->h:Ljava/lang/String;

    .line 39
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    invoke-virtual {p3}, Lcom/twitter/library/api/y;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/TwitterTypeAheadGroup;

    iput-object v0, p0, Lcom/twitter/library/api/search/m;->i:Lcom/twitter/library/api/search/TwitterTypeAheadGroup;

    .line 114
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 17
    check-cast p3, Lcom/twitter/library/api/y;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/api/search/m;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/service/d$a;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 58
    invoke-virtual {p0}, Lcom/twitter/library/api/search/m;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    const-string/jumbo v2, "search"

    aput-object v2, v1, v4

    const-string/jumbo v2, "typeahead"

    aput-object v2, v1, v3

    .line 59
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 61
    const-string/jumbo v0, "prefetch"

    invoke-virtual {v1, v0, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 63
    iget v0, p0, Lcom/twitter/library/api/search/m;->b:I

    if-ne v0, v3, :cond_2

    .line 64
    iget-object v0, p0, Lcom/twitter/library/api/search/m;->a:Ljava/lang/String;

    const-string/jumbo v2, "@"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/search/m;->a:Ljava/lang/String;

    .line 70
    :goto_0
    const-string/jumbo v2, "q"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 71
    const-string/jumbo v0, "src"

    iget-object v2, p0, Lcom/twitter/library/api/search/m;->h:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 73
    iget v0, p0, Lcom/twitter/library/api/search/m;->b:I

    packed-switch v0, :pswitch_data_0

    .line 90
    const-string/jumbo v0, "result_type"

    const-string/jumbo v2, "all"

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 91
    const-string/jumbo v0, "filters"

    invoke-virtual {v1, v0, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 96
    :goto_1
    iget v0, p0, Lcom/twitter/library/api/search/m;->c:I

    if-lez v0, :cond_0

    .line 97
    const-string/jumbo v0, "count"

    iget v2, p0, Lcom/twitter/library/api/search/m;->c:I

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    .line 100
    :cond_0
    return-object v1

    .line 64
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "@"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/library/api/search/m;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 65
    :cond_2
    iget v0, p0, Lcom/twitter/library/api/search/m;->b:I

    if-ne v0, v5, :cond_4

    .line 66
    iget-object v0, p0, Lcom/twitter/library/api/search/m;->a:Ljava/lang/String;

    const-string/jumbo v2, "#"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/api/search/m;->a:Ljava/lang/String;

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "#"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/library/api/search/m;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 68
    :cond_4
    iget-object v0, p0, Lcom/twitter/library/api/search/m;->a:Ljava/lang/String;

    goto :goto_0

    .line 75
    :pswitch_0
    const-string/jumbo v0, "result_type"

    const-string/jumbo v2, "users"

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto :goto_1

    .line 79
    :pswitch_1
    const-string/jumbo v0, "result_type"

    const-string/jumbo v2, "topics"

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 81
    const-string/jumbo v0, "filters"

    invoke-virtual {v1, v0, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    goto :goto_1

    .line 85
    :pswitch_2
    const-string/jumbo v0, "result_type"

    const-string/jumbo v2, "hashtags"

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto :goto_1

    .line 73
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public e()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/twitter/library/api/search/m;->b:I

    return v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/twitter/library/api/search/m;->s()Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/library/api/search/m;->a:Ljava/lang/String;

    return-object v0
.end method

.method public h()Lcom/twitter/library/api/search/TwitterTypeAheadGroup;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/library/api/search/m;->i:Lcom/twitter/library/api/search/TwitterTypeAheadGroup;

    return-object v0
.end method

.method protected s()Lcom/twitter/library/api/y;
    .locals 1

    .prologue
    .line 105
    const/16 v0, 0x27

    invoke-static {v0}, Lcom/twitter/library/api/y;->a(I)Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method
