.class public Lcom/twitter/library/api/search/d;
.super Lbao;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbao",
        "<",
        "Lcom/twitter/library/api/y;",
        ">;"
    }
.end annotation


# instance fields
.field private A:D

.field private B:D

.field private C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:Ljava/lang/String;

.field private L:Ljava/lang/String;

.field private M:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/topic/TwitterTopic;",
            ">;"
        }
    .end annotation
.end field

.field private N:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private O:Lcom/twitter/model/topic/TwitterTopic;

.field private P:Ljava/lang/String;

.field private Q:Ljava/lang/String;

.field private R:Ljava/lang/String;

.field private final a:I

.field private final b:I

.field private final c:J

.field private final h:Z

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:Ljava/lang/String;

.field private final m:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:I

.field private v:I

.field private w:I

.field private x:J

.field private y:J

.field private z:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 15

    .prologue
    .line 135
    const/4 v13, 0x0

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move-object/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    move-object/from16 v11, p10

    move/from16 v12, p11

    invoke-direct/range {v1 .. v13}, Lcom/twitter/library/api/search/d;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;ZZ)V

    .line 137
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;ZZ)V
    .locals 3

    .prologue
    .line 142
    const-class v0, Lcom/twitter/library/api/search/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lbao;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 97
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->m:Ljava/util/Set;

    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/api/search/d;->t:Z

    .line 108
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    iput-wide v0, p0, Lcom/twitter/library/api/search/d;->A:D

    .line 109
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    iput-wide v0, p0, Lcom/twitter/library/api/search/d;->B:D

    .line 143
    iput-wide p3, p0, Lcom/twitter/library/api/search/d;->c:J

    .line 144
    iput-object p5, p0, Lcom/twitter/library/api/search/d;->i:Ljava/lang/String;

    .line 145
    iput p6, p0, Lcom/twitter/library/api/search/d;->a:I

    .line 146
    iput-object p7, p0, Lcom/twitter/library/api/search/d;->k:Ljava/lang/String;

    .line 147
    iput-object p8, p0, Lcom/twitter/library/api/search/d;->j:Ljava/lang/String;

    .line 148
    iput p9, p0, Lcom/twitter/library/api/search/d;->b:I

    .line 149
    iput-object p10, p0, Lcom/twitter/library/api/search/d;->l:Ljava/lang/String;

    .line 150
    invoke-static {p12}, Lcom/twitter/library/api/search/d;->b(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->C:Ljava/lang/String;

    .line 151
    iput-boolean p11, p0, Lcom/twitter/library/api/search/d;->h:Z

    .line 152
    return-void
.end method

.method private D()Ljava/lang/String;
    .locals 2

    .prologue
    .line 274
    const-string/jumbo v0, ","

    iget-object v1, p0, Lcom/twitter/library/api/search/d;->m:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private E()Ljava/lang/String;
    .locals 8

    .prologue
    .line 278
    iget-object v0, p0, Lcom/twitter/library/api/search/d;->K:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 279
    invoke-virtual {p0}, Lcom/twitter/library/api/search/d;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-wide v4, v0, Lcom/twitter/library/service/v;->c:J

    .line 280
    invoke-virtual {p0}, Lcom/twitter/library/api/search/d;->R()Lcom/twitter/library/provider/t;

    move-result-object v1

    const/4 v2, 0x7

    const/16 v3, 0xd

    iget-wide v6, p0, Lcom/twitter/library/api/search/d;->c:J

    .line 281
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    .line 280
    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/t;->b(IIJLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->K:Ljava/lang/String;

    .line 283
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/search/d;->K:Ljava/lang/String;

    return-object v0
.end method

.method private static b(Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 378
    const-string/jumbo v0, "tweet,user_gallery,news,media_gallery,suggestion,event,tweet_gallery,follows_tweet_gallery,nearby_tweet_gallery"

    .line 379
    const-string/jumbo v1, "search_features_summary_enabled"

    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 380
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",summary"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 382
    :cond_0
    return-object v0
.end method


# virtual methods
.method public A()Ljava/lang/String;
    .locals 1

    .prologue
    .line 576
    iget-object v0, p0, Lcom/twitter/library/api/search/d;->L:Ljava/lang/String;

    return-object v0
.end method

.method public B()Lcom/twitter/model/topic/TwitterTopic;
    .locals 2

    .prologue
    .line 585
    iget-object v0, p0, Lcom/twitter/library/api/search/d;->M:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/search/d;->M:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/search/d;->M:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/topic/TwitterTopic;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public C()Lcom/twitter/model/topic/TwitterTopic;
    .locals 1

    .prologue
    .line 589
    iget-object v0, p0, Lcom/twitter/library/api/search/d;->O:Lcom/twitter/model/topic/TwitterTopic;

    return-object v0
.end method

.method public a(I)Lcom/twitter/library/api/search/d;
    .locals 0

    .prologue
    .line 508
    iput p1, p0, Lcom/twitter/library/api/search/d;->v:I

    .line 509
    return-object p0
.end method

.method public a(IZZZ)Lcom/twitter/library/api/search/d;
    .locals 2

    .prologue
    .line 386
    if-eqz p2, :cond_2

    .line 387
    const-string/jumbo v0, "follows"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->D:Ljava/lang/String;

    .line 393
    :cond_0
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 459
    :goto_1
    :pswitch_0
    const-string/jumbo v0, "search_features_universal_event_summary_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 463
    const/16 v0, 0x8

    if-eq p1, v0, :cond_1

    const/16 v0, 0xe

    if-eq p1, v0, :cond_1

    .line 464
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/library/api/search/d;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",event_summary"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->C:Ljava/lang/String;

    .line 467
    :cond_1
    return-object p0

    .line 388
    :cond_2
    if-eqz p3, :cond_3

    .line 389
    const-string/jumbo v0, "recent"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->D:Ljava/lang/String;

    goto :goto_0

    .line 390
    :cond_3
    if-eqz p4, :cond_0

    .line 391
    const-string/jumbo v0, "realtime"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->D:Ljava/lang/String;

    goto :goto_0

    .line 395
    :pswitch_1
    const-string/jumbo v0, "tweet"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->C:Ljava/lang/String;

    goto :goto_1

    .line 399
    :pswitch_2
    const-string/jumbo v0, "user"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->C:Ljava/lang/String;

    goto :goto_1

    .line 403
    :pswitch_3
    const-string/jumbo v0, "tweet"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->C:Ljava/lang/String;

    .line 404
    const-string/jumbo v0, "images"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->E:Ljava/lang/String;

    goto :goto_1

    .line 408
    :pswitch_4
    const-string/jumbo v0, "tweet"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->C:Ljava/lang/String;

    .line 409
    const-string/jumbo v0, "videos"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->E:Ljava/lang/String;

    goto :goto_1

    .line 413
    :pswitch_5
    const-string/jumbo v0, "tweet"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->C:Ljava/lang/String;

    .line 414
    const-string/jumbo v0, "vine"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->E:Ljava/lang/String;

    goto :goto_1

    .line 418
    :pswitch_6
    const-string/jumbo v0, "tweet"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->C:Ljava/lang/String;

    .line 419
    const-string/jumbo v0, "news"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->E:Ljava/lang/String;

    goto :goto_1

    .line 423
    :pswitch_7
    const-string/jumbo v0, "tweet"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->C:Ljava/lang/String;

    .line 424
    const-string/jumbo v0, "media"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->E:Ljava/lang/String;

    goto :goto_1

    .line 428
    :pswitch_8
    const-string/jumbo v0, "event_summary"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->C:Ljava/lang/String;

    goto/16 :goto_1

    .line 432
    :pswitch_9
    const-string/jumbo v0, "tweet,media_gallery"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->C:Ljava/lang/String;

    goto/16 :goto_1

    .line 436
    :pswitch_a
    const-string/jumbo v0, "media_gallery"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->C:Ljava/lang/String;

    goto/16 :goto_1

    .line 440
    :pswitch_b
    const-string/jumbo v0, "tweet"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->C:Ljava/lang/String;

    .line 441
    const-string/jumbo v0, "periscope"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->E:Ljava/lang/String;

    .line 442
    const-string/jumbo v0, "top"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->D:Ljava/lang/String;

    goto/16 :goto_1

    .line 446
    :pswitch_c
    const-string/jumbo v0, "tweet"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->C:Ljava/lang/String;

    .line 447
    const-string/jumbo v0, "periscope"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->E:Ljava/lang/String;

    .line 448
    const-string/jumbo v0, "recent"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->D:Ljava/lang/String;

    goto/16 :goto_1

    .line 452
    :pswitch_d
    const-string/jumbo v0, "tweet"

    iput-object v0, p0, Lcom/twitter/library/api/search/d;->C:Ljava/lang/String;

    goto/16 :goto_1

    .line 393
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public a(JJ)Lcom/twitter/library/api/search/d;
    .locals 1

    .prologue
    .line 471
    iput-wide p1, p0, Lcom/twitter/library/api/search/d;->x:J

    .line 472
    iput-wide p3, p0, Lcom/twitter/library/api/search/d;->y:J

    .line 473
    return-object p0
.end method

.method public a(Landroid/location/Location;)Lcom/twitter/library/api/search/d;
    .locals 2

    .prologue
    .line 518
    if-eqz p1, :cond_0

    .line 519
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/search/d;->A:D

    .line 520
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/search/d;->B:D

    .line 522
    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/api/search/d;
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Lcom/twitter/library/api/search/d;->m:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 489
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/api/search/d;
    .locals 0

    .prologue
    .line 482
    iput-object p1, p0, Lcom/twitter/library/api/search/d;->H:Ljava/lang/String;

    .line 483
    iput-object p2, p0, Lcom/twitter/library/api/search/d;->G:Ljava/lang/String;

    .line 484
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/library/api/search/d;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/twitter/library/api/search/d;"
        }
    .end annotation

    .prologue
    .line 477
    iput-object p1, p0, Lcom/twitter/library/api/search/d;->N:Ljava/util/List;

    .line 478
    return-object p0
.end method

.method public a(Z)Lcom/twitter/library/api/search/d;
    .locals 0

    .prologue
    .line 513
    iput-boolean p1, p0, Lcom/twitter/library/api/search/d;->t:Z

    .line 514
    return-object p0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V
    .locals 19

    .prologue
    .line 294
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/network/HttpOperation;->i()Ljava/net/URI;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/library/api/search/d;->L:Ljava/lang/String;

    .line 295
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 296
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/search/d;->R()Lcom/twitter/library/provider/t;

    move-result-object v3

    .line 297
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/search/d;->S()Laut;

    move-result-object v18

    .line 298
    invoke-virtual/range {p3 .. p3}, Lcom/twitter/library/api/y;->b()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/twitter/library/api/search/g;

    .line 301
    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/library/api/search/d;->b:I

    packed-switch v2, :pswitch_data_0

    .line 312
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/twitter/library/api/search/d;->c:J

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/provider/t;->g(J)I

    .line 313
    const/4 v14, 0x0

    .line 318
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/search/d;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-wide v6, v2, Lcom/twitter/library/service/v;->c:J

    .line 319
    iget-object v2, v11, Lcom/twitter/library/api/search/g;->b:Ljava/lang/String;

    invoke-static {v2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 320
    const/4 v4, 0x7

    const/16 v5, 0xd

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/twitter/library/api/search/d;->c:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    iget-object v9, v11, Lcom/twitter/library/api/search/g;->b:Ljava/lang/String;

    invoke-virtual/range {v3 .. v9}, Lcom/twitter/library/provider/t;->a(IIJLjava/lang/String;Ljava/lang/String;)V

    .line 324
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/search/d;->N()Z

    move-result v16

    .line 326
    iget-object v2, v11, Lcom/twitter/library/api/search/g;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 327
    const/4 v2, 0x0

    .line 346
    :cond_1
    :goto_1
    if-eqz v16, :cond_2

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/api/search/d;->h:Z

    if-eqz v4, :cond_2

    .line 347
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/twitter/library/api/search/d;->c:J

    move-object/from16 v0, v18

    invoke-virtual {v3, v4, v5, v0}, Lcom/twitter/library/provider/t;->c(JLaut;)I

    .line 348
    invoke-virtual/range {v18 .. v18}, Laut;->a()V

    .line 351
    :cond_2
    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/library/api/search/d;->u:I

    .line 352
    iget v3, v11, Lcom/twitter/library/api/search/g;->e:I

    sub-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/library/api/search/d;->w:I

    .line 353
    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/library/api/search/d;->w:I

    if-gez v2, :cond_3

    .line 356
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/library/api/search/d;->w:I

    .line 359
    :cond_3
    iget-wide v2, v11, Lcom/twitter/library/api/search/g;->f:J

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/twitter/library/api/search/d;->z:J

    .line 361
    iget-object v2, v11, Lcom/twitter/library/api/search/g;->j:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/library/api/search/d;->M:Ljava/util/List;

    .line 363
    iget-object v2, v11, Lcom/twitter/library/api/search/g;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 364
    iget-object v2, v11, Lcom/twitter/library/api/search/g;->a:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/search/h;

    .line 365
    if-eqz v2, :cond_4

    iget-object v3, v2, Lcom/twitter/library/api/search/h;->k:Ljava/util/List;

    if-eqz v3, :cond_4

    iget-object v3, v2, Lcom/twitter/library/api/search/h;->k:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 366
    iget-object v2, v2, Lcom/twitter/library/api/search/h;->k:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/topic/TwitterTopic;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/library/api/search/d;->O:Lcom/twitter/model/topic/TwitterTopic;

    .line 369
    :cond_4
    iget-object v2, v11, Lcom/twitter/library/api/search/g;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/library/api/search/d;->I:Ljava/lang/String;

    .line 370
    iget-object v2, v11, Lcom/twitter/library/api/search/g;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/library/api/search/d;->H:Ljava/lang/String;

    .line 371
    iget-object v2, v11, Lcom/twitter/library/api/search/g;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/library/api/search/d;->J:Ljava/lang/String;

    .line 372
    iget-boolean v2, v11, Lcom/twitter/library/api/search/g;->c:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/twitter/library/api/search/d;->r:Z

    .line 373
    iget-boolean v2, v11, Lcom/twitter/library/api/search/g;->d:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/twitter/library/api/search/d;->s:Z

    .line 375
    :cond_5
    return-void

    .line 303
    :pswitch_0
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/twitter/library/api/search/d;->c:J

    .line 304
    invoke-virtual {v3, v2, v4, v5}, Lcom/twitter/library/provider/t;->a(ZJ)I

    move-result v2

    iget-object v4, v11, Lcom/twitter/library/api/search/g;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    sub-int v14, v2, v4

    .line 305
    goto/16 :goto_0

    .line 308
    :pswitch_1
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/twitter/library/api/search/d;->c:J

    invoke-virtual {v3, v2, v4, v5}, Lcom/twitter/library/provider/t;->a(ZJ)I

    move-result v2

    add-int/lit8 v14, v2, 0x1

    .line 309
    goto/16 :goto_0

    .line 330
    :cond_6
    if-eqz v16, :cond_7

    iget-object v2, v11, Lcom/twitter/library/api/search/g;->j:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 331
    :cond_7
    const-string/jumbo v2, "event_update_enabled"

    invoke-static {v2}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 332
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/search/d;->S()Laut;

    move-result-object v17

    .line 339
    :goto_2
    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/twitter/library/api/search/d;->c:J

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/library/api/search/d;->b:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_a

    const/4 v15, 0x1

    :goto_3
    move-object v8, v3

    move-wide v12, v6

    invoke-virtual/range {v8 .. v17}, Lcom/twitter/library/provider/t;->a(JLcom/twitter/library/api/search/g;JIZZLaut;)I

    move-result v2

    .line 341
    if-eqz v17, :cond_1

    .line 342
    invoke-virtual/range {v17 .. v17}, Laut;->a()V

    goto/16 :goto_1

    .line 334
    :cond_8
    const/16 v17, 0x0

    goto :goto_2

    .line 337
    :cond_9
    const/16 v17, 0x0

    goto :goto_2

    .line 339
    :cond_a
    const/4 v15, 0x0

    goto :goto_3

    .line 301
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 38
    check-cast p3, Lcom/twitter/library/api/y;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/api/search/d;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V

    return-void
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/library/api/search/d;
    .locals 0

    .prologue
    .line 493
    iput-object p1, p0, Lcom/twitter/library/api/search/d;->P:Ljava/lang/String;

    .line 494
    return-object p0
.end method

.method protected b()Lcom/twitter/library/service/d$a;
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 177
    invoke-virtual {p0}, Lcom/twitter/library/api/search/d;->J()Lcom/twitter/library/service/d$a;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string/jumbo v4, "search"

    aput-object v4, v3, v0

    const-string/jumbo v4, "universal"

    aput-object v4, v3, v1

    .line 178
    invoke-virtual {v2, v3}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v2

    const-string/jumbo v3, "q"

    iget-object v4, p0, Lcom/twitter/library/api/search/d;->i:Ljava/lang/String;

    .line 179
    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v2

    .line 181
    iget-object v3, p0, Lcom/twitter/library/api/search/d;->k:Ljava/lang/String;

    invoke-static {v3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 182
    const-string/jumbo v3, "query_source"

    iget-object v4, p0, Lcom/twitter/library/api/search/d;->k:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 186
    :goto_0
    iget-wide v4, p0, Lcom/twitter/library/api/search/d;->A:D

    invoke-static {v4, v5}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-nez v3, :cond_0

    iget-wide v4, p0, Lcom/twitter/library/api/search/d;->B:D

    invoke-static {v4, v5}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-nez v3, :cond_0

    .line 187
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v3

    invoke-virtual {v3}, Lbqg;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 188
    const-string/jumbo v3, "near"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "%.7f"

    new-array v6, v1, [Ljava/lang/Object;

    iget-wide v8, p0, Lcom/twitter/library/api/search/d;->A:D

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    aput-object v7, v6, v0

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x2c

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "%.7f"

    new-array v6, v1, [Ljava/lang/Object;

    iget-wide v8, p0, Lcom/twitter/library/api/search/d;->B:D

    .line 189
    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    aput-object v7, v6, v0

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 188
    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 191
    :cond_0
    iget-object v3, p0, Lcom/twitter/library/api/search/d;->D:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 192
    const-string/jumbo v3, "result_type"

    iget-object v4, p0, Lcom/twitter/library/api/search/d;->D:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 194
    :cond_1
    iget-object v3, p0, Lcom/twitter/library/api/search/d;->E:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 195
    const-string/jumbo v3, "filter"

    iget-object v4, p0, Lcom/twitter/library/api/search/d;->E:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 197
    :cond_2
    iget-object v3, p0, Lcom/twitter/library/api/search/d;->F:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 198
    const-string/jumbo v3, "timeline_type"

    iget-object v4, p0, Lcom/twitter/library/api/search/d;->F:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 200
    :cond_3
    const-string/jumbo v3, "modules"

    iget-object v4, p0, Lcom/twitter/library/api/search/d;->C:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 201
    iget-object v3, p0, Lcom/twitter/library/api/search/d;->l:Ljava/lang/String;

    if-eqz v3, :cond_4

    .line 202
    const-string/jumbo v3, "experiments"

    iget-object v4, p0, Lcom/twitter/library/api/search/d;->l:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 205
    :cond_4
    iget-wide v4, p0, Lcom/twitter/library/api/search/d;->x:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_5

    iget-wide v4, p0, Lcom/twitter/library/api/search/d;->x:J

    iget-wide v6, p0, Lcom/twitter/library/api/search/d;->y:J

    cmp-long v3, v4, v6

    if-gez v3, :cond_5

    move v0, v1

    .line 206
    :cond_5
    if-eqz v0, :cond_6

    .line 207
    const-string/jumbo v0, "since_time"

    iget-wide v4, p0, Lcom/twitter/library/api/search/d;->x:J

    invoke-virtual {v2, v0, v4, v5}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    .line 208
    const-string/jumbo v0, "until_time"

    iget-wide v4, p0, Lcom/twitter/library/api/search/d;->y:J

    invoke-virtual {v2, v0, v4, v5}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    .line 211
    :cond_6
    iget-object v0, p0, Lcom/twitter/library/api/search/d;->G:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 212
    const-string/jumbo v0, "cluster_id"

    iget-object v3, p0, Lcom/twitter/library/api/search/d;->G:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 217
    :goto_1
    iget v0, p0, Lcom/twitter/library/api/search/d;->b:I

    if-eqz v0, :cond_7

    .line 218
    invoke-direct {p0}, Lcom/twitter/library/api/search/d;->E()Ljava/lang/String;

    move-result-object v0

    .line 219
    iget v3, p0, Lcom/twitter/library/api/search/d;->b:I

    packed-switch v3, :pswitch_data_0

    .line 234
    :cond_7
    :goto_2
    iget-object v0, p0, Lcom/twitter/library/api/search/d;->H:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 235
    const-string/jumbo v0, "event_id"

    iget-object v3, p0, Lcom/twitter/library/api/search/d;->H:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 238
    :cond_8
    iget v0, p0, Lcom/twitter/library/api/search/d;->v:I

    if-lez v0, :cond_9

    .line 239
    const-string/jumbo v0, "count"

    iget v3, p0, Lcom/twitter/library/api/search/d;->v:I

    int-to-long v4, v3

    invoke-virtual {v2, v0, v4, v5}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    .line 242
    :cond_9
    iget-object v0, p0, Lcom/twitter/library/api/search/d;->N:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 243
    const-string/jumbo v0, "pt"

    const-string/jumbo v3, ","

    iget-object v4, p0, Lcom/twitter/library/api/search/d;->N:Ljava/util/List;

    invoke-static {v3, v4}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 246
    :cond_a
    iget-object v0, p0, Lcom/twitter/library/api/search/d;->m:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 247
    const-string/jumbo v0, "enabled_verticals"

    invoke-direct {p0}, Lcom/twitter/library/api/search/d;->D()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 250
    :cond_b
    iget-object v0, p0, Lcom/twitter/library/api/search/d;->P:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 251
    const-string/jumbo v0, "query_rewrite_id"

    iget-object v3, p0, Lcom/twitter/library/api/search/d;->P:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 254
    :cond_c
    iget-object v0, p0, Lcom/twitter/library/api/search/d;->Q:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 255
    const-string/jumbo v0, "data_lookup_id"

    iget-object v3, p0, Lcom/twitter/library/api/search/d;->Q:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 258
    :cond_d
    iget-object v0, p0, Lcom/twitter/library/api/search/d;->R:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 259
    const-string/jumbo v0, "safety_level"

    iget-object v3, p0, Lcom/twitter/library/api/search/d;->R:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 262
    :cond_e
    const-string/jumbo v3, "pc"

    iget-boolean v0, p0, Lcom/twitter/library/api/search/d;->t:Z

    if-eqz v0, :cond_11

    const-string/jumbo v0, "true"

    :goto_3
    invoke-virtual {v2, v3, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "include_media_features"

    .line 264
    invoke-virtual {v0, v2, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "timezone"

    .line 265
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 266
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->b()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 267
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->f()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 268
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->e()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 269
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->d()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 270
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->c()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 262
    return-object v0

    .line 184
    :cond_f
    const-string/jumbo v3, "query_source"

    const-string/jumbo v4, "unknown"

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto/16 :goto_0

    .line 214
    :cond_10
    const-string/jumbo v0, "get_clusters"

    invoke-virtual {v2, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    goto/16 :goto_1

    .line 221
    :pswitch_0
    const-string/jumbo v3, "next_cursor"

    invoke-virtual {v2, v3, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto/16 :goto_2

    .line 225
    :pswitch_1
    const-string/jumbo v3, "prev_cursor"

    invoke-virtual {v2, v3, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto/16 :goto_2

    .line 262
    :cond_11
    const-string/jumbo v0, "false"

    goto :goto_3

    .line 219
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/library/api/search/d;
    .locals 0

    .prologue
    .line 498
    iput-object p1, p0, Lcom/twitter/library/api/search/d;->Q:Ljava/lang/String;

    .line 499
    return-object p0
.end method

.method protected c(Lcom/twitter/library/service/u;)Z
    .locals 5

    .prologue
    .line 158
    iget v0, p0, Lcom/twitter/library/api/search/d;->b:I

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/api/search/d;->E()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    move v1, v0

    .line 159
    :goto_0
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/search/d;->G:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/search/d;->H:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 160
    invoke-virtual {p0}, Lcom/twitter/library/api/search/d;->N()Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "timeline"

    iget-object v2, p0, Lcom/twitter/library/api/search/d;->k:Ljava/lang/String;

    .line 161
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "stickers"

    iget-object v2, p0, Lcom/twitter/library/api/search/d;->k:Ljava/lang/String;

    .line 162
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "trend_click"

    iget-object v2, p0, Lcom/twitter/library/api/search/d;->k:Ljava/lang/String;

    .line 163
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "promoted_trend_click"

    iget-object v2, p0, Lcom/twitter/library/api/search/d;->k:Ljava/lang/String;

    .line 164
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 165
    new-instance v0, Lcom/twitter/model/search/b$a;

    invoke-direct {v0}, Lcom/twitter/model/search/b$a;-><init>()V

    iget-object v2, p0, Lcom/twitter/library/api/search/d;->j:Ljava/lang/String;

    .line 166
    invoke-virtual {v0, v2}, Lcom/twitter/model/search/b$a;->a(Ljava/lang/String;)Lcom/twitter/model/search/b$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/library/api/search/d;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/twitter/model/search/b$a;->b(Ljava/lang/String;)Lcom/twitter/model/search/b$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/search/b$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/search/b;

    .line 167
    invoke-virtual {p0}, Lcom/twitter/library/api/search/d;->S()Laut;

    move-result-object v2

    .line 168
    invoke-virtual {p0}, Lcom/twitter/library/api/search/d;->R()Lcom/twitter/library/provider/t;

    move-result-object v3

    iget v4, p0, Lcom/twitter/library/api/search/d;->a:I

    invoke-virtual {v3, v0, v4, v2}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/model/search/b;ILaut;)J

    .line 169
    invoke-virtual {v2}, Laut;->a()V

    .line 171
    :cond_1
    return v1

    .line 158
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/library/api/search/d;
    .locals 0

    .prologue
    .line 526
    iput-object p1, p0, Lcom/twitter/library/api/search/d;->R:Ljava/lang/String;

    .line 527
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/library/api/search/d;
    .locals 0

    .prologue
    .line 531
    iput-object p1, p0, Lcom/twitter/library/api/search/d;->D:Ljava/lang/String;

    .line 532
    return-object p0
.end method

.method protected e()Lcom/twitter/library/api/y;
    .locals 1

    .prologue
    .line 288
    const/16 v0, 0x18

    invoke-static {v0}, Lcom/twitter/library/api/y;->a(I)Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/twitter/library/api/search/d;->e()Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 540
    iget-boolean v0, p0, Lcom/twitter/library/api/search/d;->s:Z

    return v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 552
    iget v0, p0, Lcom/twitter/library/api/search/d;->u:I

    return v0
.end method

.method public s()I
    .locals 1

    .prologue
    .line 556
    iget v0, p0, Lcom/twitter/library/api/search/d;->w:I

    return v0
.end method

.method public t()J
    .locals 2

    .prologue
    .line 560
    iget-wide v0, p0, Lcom/twitter/library/api/search/d;->z:J

    return-wide v0
.end method

.method public x()Ljava/lang/String;
    .locals 1

    .prologue
    .line 564
    iget-object v0, p0, Lcom/twitter/library/api/search/d;->H:Ljava/lang/String;

    return-object v0
.end method

.method public y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 568
    iget-object v0, p0, Lcom/twitter/library/api/search/d;->I:Ljava/lang/String;

    return-object v0
.end method

.method public z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 572
    iget-object v0, p0, Lcom/twitter/library/api/search/d;->J:Ljava/lang/String;

    return-object v0
.end method
