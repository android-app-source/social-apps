.class public Lcom/twitter/library/api/h;
.super Lbao;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbao",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcdg;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/twitter/library/api/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lbao;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 42
    iput-object p3, p0, Lcom/twitter/library/api/h;->a:Ljava/lang/String;

    .line 43
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcdg;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 64
    invoke-super {p0, p1, p2, p3}, Lbao;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V

    .line 65
    invoke-virtual {p0}, Lcom/twitter/library/api/h;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-wide v4, v0, Lcom/twitter/library/service/v;->c:J

    .line 66
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 67
    new-instance v0, Lcom/twitter/metrics/g;

    const-string/jumbo v1, "fs:download:blocked"

    sget-object v2, Lcom/twitter/metrics/g;->m:Lcom/twitter/metrics/g$b;

    .line 68
    invoke-virtual {p0}, Lcom/twitter/library/api/h;->m()Lcom/twitter/async/service/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/async/service/b;->a()J

    move-result-wide v6

    invoke-direct {v0, v1, v2, v6, v7}, Lcom/twitter/metrics/g;-><init>(Ljava/lang/String;Lcom/twitter/metrics/g$b;J)V

    .line 69
    const-string/jumbo v1, "fs_request"

    invoke-virtual {v0, v1}, Lcom/twitter/metrics/g;->e(Ljava/lang/String;)V

    .line 70
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/metrics/j;->a(Lcom/twitter/metrics/g;)V

    .line 72
    const-string/jumbo v0, "fs:fetch:fetch_not_load"

    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v1

    sget-object v2, Lcom/twitter/metrics/g;->m:Lcom/twitter/metrics/g$b;

    invoke-static {v0, v1, v4, v5, v2}, Lcom/twitter/metrics/n;->b(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;)Lcom/twitter/metrics/n;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lcom/twitter/metrics/n;->j()V

    .line 74
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdg;

    .line 75
    invoke-virtual {v0}, Lcdg;->c()Lcdg;

    move-result-object v1

    .line 79
    invoke-static {v4, v5, v0}, Lbqa;->a(JLcdg;)V

    .line 80
    iget-object v0, p0, Lcom/twitter/library/api/h;->p:Landroid/content/Context;

    invoke-static {v0, v4, v5, v1}, Lbqa;->a(Landroid/content/Context;JLcdg;)V

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    const-string/jumbo v0, "fs:fetch:fetch_not_load"

    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v3

    sget-object v6, Lcom/twitter/metrics/g;->m:Lcom/twitter/metrics/g$b;

    invoke-static {v0, v3, v4, v5, v6}, Lcom/twitter/metrics/n;->b(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;)Lcom/twitter/metrics/n;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Lcom/twitter/metrics/n;->k()V

    .line 85
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->m()Lcom/twitter/network/l;

    move-result-object v3

    .line 88
    iget v0, v3, Lcom/twitter/network/l;->a:I

    const/16 v4, 0x193

    if-ne v0, v4, :cond_2

    .line 89
    invoke-virtual {p2}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    const/16 v4, 0xef

    if-eq v0, v4, :cond_0

    .line 90
    :cond_2
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v4

    .line 91
    if-eqz p3, :cond_3

    move v0, v1

    .line 92
    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 93
    :goto_2
    new-instance v5, Ljava/lang/RuntimeException;

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v7, "FeatureSwitchRequest failed: success=%b, networkFailure=%b, statusCode=%d, parsedObject=%s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    .line 95
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v8, v2

    iget-boolean v2, v3, Lcom/twitter/network/l;->d:Z

    .line 96
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v8, v1

    const/4 v1, 0x2

    iget v2, v3, Lcom/twitter/network/l;->a:I

    .line 97
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v8, v1

    const/4 v1, 0x3

    if-eqz v0, :cond_5

    .line 98
    invoke-virtual {p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdg;

    invoke-virtual {v0}, Lcdg;->d()Ljava/lang/String;

    move-result-object v0

    :goto_3
    aput-object v0, v8, v1

    .line 93
    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 91
    goto :goto_1

    :cond_4
    move v0, v2

    .line 92
    goto :goto_2

    .line 98
    :cond_5
    const-string/jumbo v0, "null"

    goto :goto_3
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 27
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/api/h;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/service/d$a;
    .locals 4

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/twitter/library/api/h;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "help"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "settings"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 49
    const-string/jumbo v1, "feature_switches_configs_use_feature_set_token"

    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    const-string/jumbo v1, "feature_set_token"

    iget-object v2, p0, Lcom/twitter/library/api/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 52
    :cond_0
    return-object v0
.end method

.method protected e()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcdg;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    const-class v0, Lcdg;

    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/twitter/library/api/h;->e()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
