.class public Lcom/twitter/library/api/z;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xa

    const/4 v3, 0x4

    .line 255
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v4}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/twitter/library/api/z;->a:Ljava/util/Map;

    .line 256
    sget-object v0, Lcom/twitter/library/api/z;->a:Ljava/util/Map;

    const-string/jumbo v1, "status"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    sget-object v0, Lcom/twitter/library/api/z;->a:Ljava/util/Map;

    const-string/jumbo v1, "user"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    sget-object v0, Lcom/twitter/library/api/z;->a:Ljava/util/Map;

    const-string/jumbo v1, "news"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    sget-object v0, Lcom/twitter/library/api/z;->a:Ljava/util/Map;

    const-string/jumbo v1, "suggestion"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    sget-object v0, Lcom/twitter/library/api/z;->a:Ljava/util/Map;

    const-string/jumbo v1, "user_gallery"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    sget-object v0, Lcom/twitter/library/api/z;->a:Ljava/util/Map;

    const-string/jumbo v1, "media_gallery"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    sget-object v0, Lcom/twitter/library/api/z;->a:Ljava/util/Map;

    const-string/jumbo v1, "tweet_gallery"

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    sget-object v0, Lcom/twitter/library/api/z;->a:Ljava/util/Map;

    const-string/jumbo v1, "event_summary"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    sget-object v0, Lcom/twitter/library/api/z;->a:Ljava/util/Map;

    const-string/jumbo v1, "event_update"

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    sget-object v0, Lcom/twitter/library/api/z;->a:Ljava/util/Map;

    const-string/jumbo v1, "summary"

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/twitter/library/api/z;->b:Ljava/util/Map;

    .line 268
    sget-object v0, Lcom/twitter/library/api/z;->b:Ljava/util/Map;

    const-string/jumbo v1, "favorite"

    const/16 v2, 0x24

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    sget-object v0, Lcom/twitter/library/api/z;->b:Ljava/util/Map;

    const-string/jumbo v1, "user"

    const/16 v2, 0x23

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    sget-object v0, Lcom/twitter/library/api/z;->b:Ljava/util/Map;

    const-string/jumbo v1, "megaphone"

    const/16 v2, 0x16

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    sget-object v0, Lcom/twitter/library/api/z;->b:Ljava/util/Map;

    const-string/jumbo v1, "bird"

    const/16 v2, 0x25

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    return-void
.end method

.method public static A(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/api/search/TwitterTypeAhead;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2686
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 2687
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 2688
    :goto_0
    if-eqz v0, :cond_3

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_3

    .line 2689
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    .line 2690
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 2691
    const-string/jumbo v2, "users"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2692
    invoke-static {p0}, Lcom/twitter/library/api/z;->Z(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    .line 2699
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 2693
    :cond_1
    const-string/jumbo v2, "topics"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2694
    invoke-static {p0}, Lcom/twitter/library/api/z;->ab(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    goto :goto_1

    .line 2696
    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 2701
    :cond_3
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static B(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/search/TwitterTypeAheadGroup;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2712
    .line 2715
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v2, v1

    move-object v0, v1

    .line 2716
    :goto_0
    if-eqz v3, :cond_3

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v4, :cond_3

    .line 2717
    sget-object v4, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v3}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    .line 2739
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    goto :goto_0

    .line 2719
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v3

    .line 2720
    const-string/jumbo v4, "users"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2721
    invoke-static {p0}, Lcom/twitter/library/api/z;->Z(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/List;

    move-result-object v2

    goto :goto_1

    .line 2722
    :cond_0
    const-string/jumbo v4, "topics"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2723
    invoke-static {p0}, Lcom/twitter/library/api/z;->ab(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/List;

    move-result-object v1

    goto :goto_1

    .line 2724
    :cond_1
    const-string/jumbo v4, "hashtags"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2725
    invoke-static {p0}, Lcom/twitter/library/api/z;->ac(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_1

    .line 2727
    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 2732
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 2741
    :cond_3
    new-instance v3, Lcom/twitter/library/api/search/TwitterTypeAheadGroup;

    invoke-direct {v3, v2, v1, v0}, Lcom/twitter/library/api/search/TwitterTypeAheadGroup;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-object v3

    .line 2717
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static C(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/x;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Lcom/twitter/library/api/x",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 3047
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 3048
    const/4 v1, 0x0

    .line 3049
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    .line 3050
    :goto_0
    if-eqz v1, :cond_5

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v4, :cond_5

    .line 3051
    sget-object v4, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v4, v1

    packed-switch v1, :pswitch_data_0

    .line 3080
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    .line 3053
    :pswitch_1
    const-string/jumbo v1, "ids"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 3054
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v5, v1

    .line 3055
    :goto_2
    if-eqz v5, :cond_2

    move v1, v2

    :goto_3
    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v5, v4, :cond_3

    move v4, v2

    :goto_4
    and-int/2addr v1, v4

    if-eqz v1, :cond_0

    .line 3056
    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->i:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v5, v1, :cond_1

    .line 3057
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->j()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3059
    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v5, v1

    goto :goto_2

    :cond_2
    move v1, v3

    .line 3055
    goto :goto_3

    :cond_3
    move v4, v3

    goto :goto_4

    .line 3062
    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 3067
    :pswitch_2
    const-string/jumbo v1, "next_cursor_str"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3068
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 3073
    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 3082
    :cond_5
    new-instance v1, Lcom/twitter/library/api/x;

    invoke-direct {v1, v0, v6}, Lcom/twitter/library/api/x;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object v1

    .line 3051
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static D(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/library/api/TwitterLocation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    .line 3343
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 3344
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 3345
    :goto_0
    if-eqz v0, :cond_6

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_6

    .line 3346
    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_5

    .line 3352
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v5, v6

    move-object v4, v6

    move-wide v2, v8

    move-object v1, v6

    .line 3353
    :goto_1
    if-eqz v0, :cond_3

    sget-object v10, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v10, :cond_3

    .line 3354
    sget-object v10, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v10, v0

    packed-switch v0, :pswitch_data_0

    .line 3381
    :cond_0
    :goto_2
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_1

    .line 3356
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 3357
    const-string/jumbo v10, "name"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 3358
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 3359
    :cond_1
    const-string/jumbo v10, "country"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 3360
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 3361
    :cond_2
    const-string/jumbo v10, "countryCode"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3362
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    .line 3367
    :pswitch_2
    const-string/jumbo v0, "woeid"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3368
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->j()J

    move-result-wide v2

    goto :goto_2

    .line 3374
    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    .line 3383
    :cond_3
    if-eqz v1, :cond_4

    cmp-long v0, v2, v8

    if-eqz v0, :cond_4

    .line 3384
    new-instance v0, Lcom/twitter/library/api/TwitterLocation;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/TwitterLocation;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3389
    :cond_4
    :goto_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 3386
    :cond_5
    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_4

    .line 3387
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    .line 3392
    :cond_6
    return-object v7

    .line 3354
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static E(Lcom/fasterxml/jackson/core/JsonParser;)Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 3642
    .line 3644
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v0

    move-object v0, v1

    .line 3645
    :goto_0
    if-eqz v2, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_2

    .line 3646
    sget-object v3, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 3665
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    .line 3648
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    .line 3649
    const-string/jumbo v3, "min_position"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3650
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 3651
    :cond_1
    const-string/jumbo v3, "max_position"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3652
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 3658
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 3667
    :cond_2
    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2

    .line 3646
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static F(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3841
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 3842
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 3843
    :goto_0
    if-eqz v0, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_2

    .line 3844
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_1

    .line 3845
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    const-class v2, Lcom/twitter/model/core/TwitterUser;

    invoke-static {p0, v2}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3849
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 3846
    :cond_1
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    .line 3847
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 3851
    :cond_2
    return-object v1
.end method

.method public static G(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Boolean;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4972
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 4973
    :goto_0
    if-eqz v0, :cond_1

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_1

    .line 4974
    sget-object v1, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 4989
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 4976
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 4980
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "is_device_follow"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4981
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 4991
    :goto_2
    return-object v0

    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_2

    .line 4974
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static H(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/n;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4996
    new-instance v1, Lcom/twitter/library/api/n;

    invoke-direct {v1}, Lcom/twitter/library/api/n;-><init>()V

    .line 4998
    :try_start_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 4999
    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    .line 5000
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 5019
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 5002
    :pswitch_1
    const-string/jumbo v0, "normalized_phone_number"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5003
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/library/api/n;->c:Ljava/lang/String;

    goto :goto_1

    .line 5021
    :catch_0
    move-exception v0

    .line 5023
    :cond_1
    return-object v1

    .line 5008
    :pswitch_2
    const-string/jumbo v0, "valid"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5009
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/twitter/library/api/n;->a:Z

    goto :goto_1

    .line 5010
    :cond_2
    const-string/jumbo v0, "available"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5011
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/twitter/library/api/n;->b:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 5000
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static I(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5028
    const/4 v1, 0x0

    .line 5029
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    .line 5030
    :goto_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    .line 5031
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 5047
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    .line 5034
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 5038
    :pswitch_2
    const-string/jumbo v1, "access_token"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5039
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 5049
    :cond_1
    return-object v0

    .line 5031
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static J(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5054
    const/4 v1, 0x0

    .line 5055
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    .line 5056
    :goto_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    .line 5057
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 5073
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    .line 5060
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 5064
    :pswitch_2
    const-string/jumbo v1, "guest_token"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5065
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 5075
    :cond_1
    return-object v0

    .line 5057
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static K(Lcom/fasterxml/jackson/core/JsonParser;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5079
    const/4 v1, 0x0

    .line 5080
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move v0, v1

    move-object v1, v3

    .line 5081
    :goto_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    .line 5082
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 5090
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 5094
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    .line 5084
    :pswitch_0
    const-string/jumbo v1, "is_numeric"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5085
    const/4 v0, 0x1

    goto :goto_1

    .line 5097
    :cond_1
    return v0

    .line 5082
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method public static L(Lcom/fasterxml/jackson/core/JsonParser;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5101
    const/4 v1, 0x0

    .line 5102
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move v0, v1

    move-object v1, v3

    .line 5103
    :goto_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    .line 5104
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 5112
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 5116
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    .line 5106
    :pswitch_0
    const-string/jumbo v1, "is_valid"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5107
    const/4 v0, 0x1

    goto :goto_1

    .line 5119
    :cond_1
    return v0

    .line 5104
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method public static M(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/m;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5123
    const/4 v2, 0x0

    .line 5124
    const/4 v1, 0x0

    .line 5126
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v5, v0

    move-object v0, v1

    move v1, v2

    move-object v2, v5

    .line 5127
    :goto_0
    if-eqz v2, :cond_1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_1

    .line 5128
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v3

    .line 5129
    sget-object v4, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    .line 5144
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 5147
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    .line 5132
    :pswitch_1
    const-string/jumbo v2, "pass"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5133
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->l()Z

    move-result v1

    goto :goto_1

    .line 5138
    :pswitch_2
    const-string/jumbo v2, "message"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5139
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 5149
    :cond_1
    new-instance v2, Lcom/twitter/library/api/m;

    invoke-direct {v2, v1, v0}, Lcom/twitter/library/api/m;-><init>(ZLjava/lang/String;)V

    return-object v2

    .line 5129
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static N(Lcom/fasterxml/jackson/core/JsonParser;)Lbil;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 5153
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 5154
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 5155
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 5156
    :goto_0
    if-eqz v0, :cond_e

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v3, :cond_e

    .line 5157
    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v3, :cond_6

    const-string/jumbo v3, "emails"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 5158
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v5, v0

    .line 5159
    :goto_1
    if-eqz v5, :cond_1

    move v0, v1

    :goto_2
    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v5, v3, :cond_2

    move v3, v1

    :goto_3
    and-int/2addr v0, v3

    if-eqz v0, :cond_d

    .line 5160
    sget-object v0, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v5, v0, :cond_5

    .line 5162
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 5163
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    move-object v5, v3

    move-object v3, v4

    .line 5164
    :goto_4
    if-eqz v5, :cond_3

    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v5, v8, :cond_3

    .line 5165
    sget-object v8, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v5}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v5

    aget v5, v8, v5

    packed-switch v5, :pswitch_data_0

    .line 5187
    :cond_0
    :goto_5
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v5

    goto :goto_4

    :cond_1
    move v0, v2

    .line 5159
    goto :goto_2

    :cond_2
    move v3, v2

    goto :goto_3

    .line 5167
    :pswitch_1
    const-string/jumbo v5, "email"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 5168
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v3

    goto :goto_5

    .line 5173
    :pswitch_2
    const-string/jumbo v5, "email_verified"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 5174
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->l()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_5

    .line 5180
    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_5

    .line 5189
    :cond_3
    if-eqz v3, :cond_4

    .line 5190
    new-instance v5, Lbik;

    invoke-direct {v5, v3, v0}, Lbik;-><init>(Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5195
    :cond_4
    :goto_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v5, v0

    goto :goto_1

    .line 5192
    :cond_5
    sget-object v0, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v5, v0, :cond_4

    .line 5193
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_6

    .line 5197
    :cond_6
    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v3, :cond_d

    const-string/jumbo v0, "phone_numbers"

    .line 5198
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 5199
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v5, v0

    .line 5200
    :goto_7
    if-eqz v5, :cond_8

    move v0, v1

    :goto_8
    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v5, v3, :cond_9

    move v3, v1

    :goto_9
    and-int/2addr v0, v3

    if-eqz v0, :cond_d

    .line 5201
    sget-object v0, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v5, v0, :cond_c

    .line 5203
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 5204
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    move-object v5, v3

    move-object v3, v4

    .line 5205
    :goto_a
    if-eqz v5, :cond_a

    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v5, v8, :cond_a

    .line 5206
    sget-object v8, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v5}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v5

    aget v5, v8, v5

    packed-switch v5, :pswitch_data_1

    .line 5229
    :cond_7
    :goto_b
    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v5

    goto :goto_a

    :cond_8
    move v0, v2

    .line 5200
    goto :goto_8

    :cond_9
    move v3, v2

    goto :goto_9

    .line 5208
    :pswitch_5
    const-string/jumbo v5, "phone_number"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 5209
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v3

    goto :goto_b

    .line 5214
    :pswitch_6
    const-string/jumbo v5, "phone_number_verified"

    .line 5215
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v8

    .line 5214
    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 5216
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_b

    .line 5222
    :pswitch_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_b

    .line 5231
    :cond_a
    if-eqz v3, :cond_b

    .line 5232
    new-instance v5, Lbin;

    invoke-direct {v5, v3, v0}, Lbin;-><init>(Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5237
    :cond_b
    :goto_c
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v5, v0

    goto :goto_7

    .line 5234
    :cond_c
    sget-object v0, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v5, v0, :cond_b

    .line 5235
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_c

    .line 5240
    :cond_d
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto/16 :goto_0

    .line 5242
    :cond_e
    new-instance v0, Lbil;

    invoke-direct {v0, v6, v7}, Lbil;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-object v0

    .line 5165
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch

    .line 5206
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_5
        :pswitch_4
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method

.method public static O(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lbsj;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5246
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 5248
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 5249
    :goto_0
    if-eqz v0, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_2

    .line 5250
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 5273
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 5278
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 5252
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 5253
    const-string/jumbo v2, "news_events"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5254
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 5255
    :goto_2
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    .line 5256
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_1

    .line 5262
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 5265
    :goto_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_2

    .line 5258
    :pswitch_1
    invoke-static {p0}, Lcom/twitter/library/api/z;->an(Lcom/fasterxml/jackson/core/JsonParser;)Lbsj;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 5268
    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 5281
    :cond_2
    return-object v1

    .line 5250
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 5256
    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_1
    .end packed-switch
.end method

.method public static P(Lcom/fasterxml/jackson/core/JsonParser;)Lbsj;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 5335
    .line 5338
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v1, v2

    move-object v0, v2

    .line 5339
    :goto_0
    if-eqz v3, :cond_2

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v4, :cond_2

    .line 5340
    sget-object v4, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v3}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    .line 5360
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 5365
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    goto :goto_0

    .line 5342
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v3

    .line 5343
    const-string/jumbo v4, "news_event"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 5344
    invoke-static {p0}, Lcom/twitter/library/api/z;->an(Lcom/fasterxml/jackson/core/JsonParser;)Lbsj;

    move-result-object v1

    goto :goto_1

    .line 5346
    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 5351
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v3

    .line 5352
    const-string/jumbo v4, "tweets"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 5353
    const-class v0, Lcom/twitter/model/core/ac;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->d(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 5355
    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 5368
    :cond_2
    if-eqz v1, :cond_3

    .line 5369
    iput-object v0, v1, Lbsj;->i:Ljava/util/List;

    .line 5372
    :goto_2
    return-object v1

    :cond_3
    move-object v1, v2

    goto :goto_2

    .line 5340
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static Q(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/topic/a;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 464
    .line 470
    const-wide/16 v8, -0x1

    .line 471
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v6, v7

    move-object v5, v7

    move-object v4, v7

    move-object v3, v7

    move-object v2, v7

    .line 472
    :goto_0
    if-eqz v0, :cond_6

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_6

    .line 473
    sget-object v1, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    sparse-switch v0, :sswitch_data_0

    .line 499
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 503
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 475
    :sswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 476
    const-string/jumbo v1, "location"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 477
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 478
    :cond_1
    const-string/jumbo v1, "name"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 479
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 480
    :cond_2
    const-string/jumbo v1, "score"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 481
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 482
    :cond_3
    const-string/jumbo v1, "logo_url"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 483
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 484
    :cond_4
    const-string/jumbo v1, "abbreviation"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 485
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 486
    :cond_5
    const-string/jumbo v1, "player_type"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 487
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 492
    :sswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 493
    const-string/jumbo v1, "user_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 494
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()J

    move-result-wide v8

    goto :goto_1

    .line 505
    :cond_6
    new-instance v1, Lcom/twitter/model/topic/a;

    invoke-direct/range {v1 .. v9}, Lcom/twitter/model/topic/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    return-object v1

    .line 473
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x6 -> :sswitch_1
    .end sparse-switch
.end method

.method private static R(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/model/topic/a;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 511
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 512
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 513
    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    .line 514
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    .line 515
    invoke-static {p0}, Lcom/twitter/library/api/z;->Q(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/topic/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 519
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 517
    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 521
    :cond_1
    return-object v1
.end method

.method private static S(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/topic/d;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 525
    .line 533
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v6, v7

    move-object v8, v7

    move-object v5, v7

    move-object v4, v7

    move-object v3, v7

    move-object v2, v7

    move-object v1, v7

    .line 534
    :goto_0
    if-eqz v0, :cond_9

    sget-object v9, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v9, :cond_9

    .line 535
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v9

    .line 536
    sget-object v10, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v10, v0

    packed-switch v0, :pswitch_data_0

    .line 593
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 597
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 538
    :pswitch_1
    const-string/jumbo v0, "sports_title"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 539
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 540
    :cond_1
    const-string/jumbo v0, "game_type"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 541
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 542
    :cond_2
    const-string/jumbo v0, "channel"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 543
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 544
    :cond_3
    const-string/jumbo v0, "tournament_hashtag"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 545
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v8

    goto :goto_1

    .line 550
    :pswitch_2
    const-string/jumbo v0, "game_info"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 551
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 552
    :goto_2
    if-eqz v0, :cond_0

    sget-object v9, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v9, :cond_0

    .line 553
    sget-object v9, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v9, v0

    packed-switch v0, :pswitch_data_1

    .line 575
    :cond_4
    :goto_3
    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_2

    .line 555
    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 556
    const-string/jumbo v9, "summary"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 557
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 558
    :cond_5
    const-string/jumbo v9, "status"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 559
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    .line 564
    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    .line 568
    :pswitch_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    .line 578
    :cond_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    .line 583
    :pswitch_7
    const-string/jumbo v0, "players"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 584
    invoke-static {p0}, Lcom/twitter/library/api/z;->R(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v6

    goto/16 :goto_1

    .line 585
    :cond_7
    const-string/jumbo v0, "secondary_players"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 586
    invoke-static {p0}, Lcom/twitter/library/api/z;->R(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v7

    goto/16 :goto_1

    .line 588
    :cond_8
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    .line 600
    :cond_9
    new-instance v0, Lcom/twitter/model/topic/d;

    invoke-direct/range {v0 .. v8}, Lcom/twitter/model/topic/d;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    return-object v0

    .line 536
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 553
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_6
    .end packed-switch
.end method

.method private static T(Lcom/fasterxml/jackson/core/JsonParser;)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 864
    const/4 v1, 0x0

    .line 865
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move v0, v1

    move-object v1, v3

    .line 867
    :goto_0
    if-eqz v1, :cond_d

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_d

    .line 868
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 910
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    .line 870
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    .line 871
    const-string/jumbo v2, "following"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 872
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    goto :goto_1

    .line 873
    :cond_1
    const-string/jumbo v2, "followed_by"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 874
    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    goto :goto_1

    .line 875
    :cond_2
    const-string/jumbo v2, "blocking"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 876
    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    goto :goto_1

    .line 877
    :cond_3
    const-string/jumbo v2, "can_dm"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 878
    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    goto :goto_1

    .line 879
    :cond_4
    const-string/jumbo v2, "notifications_enabled"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 880
    const/16 v1, 0x10

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    goto :goto_1

    .line 881
    :cond_5
    const-string/jumbo v2, "lifeline_following"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 882
    const/16 v1, 0x100

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    goto :goto_1

    .line 883
    :cond_6
    const-string/jumbo v2, "email_following"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 884
    const/16 v1, 0x1000

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    goto :goto_1

    .line 885
    :cond_7
    const-string/jumbo v2, "want_retweets"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 886
    const/16 v1, 0x200

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    goto/16 :goto_1

    .line 887
    :cond_8
    const-string/jumbo v2, "can_media_tag"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 888
    const/16 v1, 0x400

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    goto/16 :goto_1

    .line 889
    :cond_9
    const-string/jumbo v2, "muting"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 890
    const/16 v1, 0x2000

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    goto/16 :goto_1

    .line 891
    :cond_a
    const-string/jumbo v2, "following_requested"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 892
    const/16 v1, 0x4000

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    goto/16 :goto_1

    .line 893
    :cond_b
    const-string/jumbo v2, "blocked_by"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 894
    const v1, 0x8000

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    goto/16 :goto_1

    .line 895
    :cond_c
    const-string/jumbo v2, "live_following"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 896
    const/16 v1, 0x800

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    goto/16 :goto_1

    .line 902
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    .line 912
    :cond_d
    return v0

    .line 868
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static U(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/timeline/n;
    .locals 35
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1325
    const-string/jumbo v26, ""

    .line 1326
    const-string/jumbo v25, ""

    .line 1327
    const-string/jumbo v24, ""

    .line 1328
    const-string/jumbo v23, ""

    .line 1329
    const-string/jumbo v22, ""

    .line 1330
    const-string/jumbo v21, ""

    .line 1331
    const-string/jumbo v20, ""

    .line 1332
    const-string/jumbo v19, ""

    .line 1333
    const-string/jumbo v18, ""

    .line 1334
    const-string/jumbo v17, ""

    .line 1335
    const-string/jumbo v16, ""

    .line 1336
    const-string/jumbo v15, ""

    .line 1337
    const-string/jumbo v14, ""

    .line 1338
    const/4 v13, 0x0

    .line 1339
    const/4 v12, 0x0

    .line 1340
    const/16 v11, 0xb4

    .line 1341
    const/4 v10, 0x0

    .line 1342
    const/4 v7, 0x0

    .line 1343
    const-wide/16 v8, 0x0

    .line 1344
    const/4 v6, 0x0

    .line 1345
    const-string/jumbo v5, ""

    .line 1346
    const-string/jumbo v4, ""

    .line 1347
    const-string/jumbo v3, ""

    .line 1350
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    .line 1351
    :goto_0
    if-eqz v2, :cond_15

    sget-object v27, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    move-object/from16 v0, v27

    if-eq v2, v0, :cond_15

    .line 1352
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v27

    .line 1353
    sget-object v28, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v28, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :pswitch_0
    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    move-wide/from16 v32, v8

    move v8, v7

    move v9, v10

    move-wide/from16 v6, v32

    move v10, v11

    move-object v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move-object/from16 v19, v20

    move-object/from16 v20, v21

    move-object/from16 v21, v22

    move-object/from16 v22, v23

    move-object/from16 v23, v24

    move-object/from16 v24, v25

    move-object/from16 v25, v26

    .line 1435
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v26

    move-object/from16 v32, v26

    move-object/from16 v26, v25

    move-object/from16 v25, v24

    move-object/from16 v24, v23

    move-object/from16 v23, v22

    move-object/from16 v22, v21

    move-object/from16 v21, v20

    move-object/from16 v20, v19

    move-object/from16 v19, v18

    move-object/from16 v18, v17

    move-object/from16 v17, v16

    move-object/from16 v16, v15

    move-object v15, v14

    move-object v14, v13

    move-object v13, v12

    move-object v12, v11

    move v11, v10

    move v10, v9

    move/from16 v33, v8

    move-wide v8, v6

    move/from16 v7, v33

    move v6, v5

    move-object v5, v4

    move-object v4, v3

    move-object v3, v2

    move-object/from16 v2, v32

    goto :goto_0

    .line 1355
    :pswitch_1
    const-string/jumbo v2, "text"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1356
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v32, v3

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    move-wide/from16 v33, v8

    move v8, v7

    move v9, v10

    move-wide/from16 v6, v33

    move v10, v11

    move-object v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move-object/from16 v19, v20

    move-object/from16 v20, v21

    move-object/from16 v21, v22

    move-object/from16 v22, v23

    move-object/from16 v23, v24

    move-object/from16 v24, v25

    move-object/from16 v25, v2

    move-object/from16 v2, v32

    goto :goto_1

    .line 1357
    :cond_1
    const-string/jumbo v2, "header"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1358
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v25, v26

    move-object/from16 v32, v4

    move-object v4, v5

    move v5, v6

    move-wide/from16 v33, v8

    move v8, v7

    move v9, v10

    move-wide/from16 v6, v33

    move v10, v11

    move-object v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move-object/from16 v19, v20

    move-object/from16 v20, v21

    move-object/from16 v21, v22

    move-object/from16 v22, v23

    move-object/from16 v23, v24

    move-object/from16 v24, v2

    move-object v2, v3

    move-object/from16 v3, v32

    goto/16 :goto_1

    .line 1359
    :cond_2
    const-string/jumbo v2, "action_text"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1360
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v23, v24

    move-object/from16 v24, v25

    move-object/from16 v25, v26

    move/from16 v32, v6

    move-wide/from16 v33, v8

    move v8, v7

    move v9, v10

    move-wide/from16 v6, v33

    move v10, v11

    move-object v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move-object/from16 v19, v20

    move-object/from16 v20, v21

    move-object/from16 v21, v22

    move-object/from16 v22, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move/from16 v5, v32

    goto/16 :goto_1

    .line 1361
    :cond_3
    const-string/jumbo v2, "action_url"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1362
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v24, v25

    move-object/from16 v25, v26

    move-object/from16 v32, v5

    move v5, v6

    move-wide/from16 v33, v8

    move v8, v7

    move v9, v10

    move-wide/from16 v6, v33

    move v10, v11

    move-object v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move-object/from16 v19, v20

    move-object/from16 v20, v21

    move-object/from16 v21, v22

    move-object/from16 v22, v23

    move-object/from16 v23, v2

    move-object v2, v3

    move-object v3, v4

    move-object/from16 v4, v32

    goto/16 :goto_1

    .line 1363
    :cond_4
    const-string/jumbo v2, "trigger"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1364
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v22, v23

    move-object/from16 v23, v24

    move-object/from16 v24, v25

    move-object/from16 v25, v26

    move-wide/from16 v32, v8

    move v8, v7

    move v9, v10

    move v10, v11

    move-object v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move-object/from16 v19, v20

    move-object/from16 v20, v21

    move-object/from16 v21, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    move-wide/from16 v6, v32

    goto/16 :goto_1

    .line 1365
    :cond_5
    const-string/jumbo v2, "icon"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1366
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v21, v22

    move-object/from16 v22, v23

    move-object/from16 v23, v24

    move-object/from16 v24, v25

    move-object/from16 v25, v26

    move/from16 v32, v7

    move/from16 v33, v10

    move v10, v11

    move-object v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move-object/from16 v19, v20

    move-object/from16 v20, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    move-wide v6, v8

    move/from16 v9, v33

    move/from16 v8, v32

    goto/16 :goto_1

    .line 1367
    :cond_6
    const-string/jumbo v2, "format"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1368
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v20, v21

    move-object/from16 v21, v22

    move-object/from16 v22, v23

    move-object/from16 v23, v24

    move-object/from16 v24, v25

    move-object/from16 v25, v26

    move/from16 v32, v10

    move v10, v11

    move-object v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move-object/from16 v19, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    move-wide/from16 v33, v8

    move/from16 v9, v32

    move v8, v7

    move-wide/from16 v6, v33

    goto/16 :goto_1

    .line 1369
    :cond_7
    const-string/jumbo v2, "background_image_url"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1370
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v18, v19

    move-object/from16 v19, v20

    move-object/from16 v20, v21

    move-object/from16 v21, v22

    move-object/from16 v22, v23

    move-object/from16 v23, v24

    move-object/from16 v24, v25

    move-object/from16 v25, v26

    move-object/from16 v32, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    move-wide/from16 v33, v8

    move v8, v7

    move v9, v10

    move-wide/from16 v6, v33

    move v10, v11

    move-object/from16 v11, v32

    goto/16 :goto_1

    .line 1371
    :cond_8
    const-string/jumbo v2, "template"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1372
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v19, v20

    move-object/from16 v20, v21

    move-object/from16 v21, v22

    move-object/from16 v22, v23

    move-object/from16 v23, v24

    move-object/from16 v24, v25

    move-object/from16 v25, v26

    move/from16 v32, v11

    move-object v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    move-wide/from16 v33, v8

    move v8, v7

    move v9, v10

    move-wide/from16 v6, v33

    move/from16 v10, v32

    goto/16 :goto_1

    .line 1377
    :pswitch_2
    const-string/jumbo v2, "prompt_id"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1378
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()I

    move-result v2

    move v10, v11

    move-object v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move-object/from16 v19, v20

    move-object/from16 v20, v21

    move-object/from16 v21, v22

    move-object/from16 v22, v23

    move-object/from16 v23, v24

    move-object/from16 v24, v25

    move-object/from16 v25, v26

    move-object/from16 v32, v5

    move v5, v6

    move-wide/from16 v33, v8

    move v8, v7

    move v9, v2

    move-wide/from16 v6, v33

    move-object v2, v3

    move-object v3, v4

    move-object/from16 v4, v32

    goto/16 :goto_1

    .line 1379
    :cond_9
    const-string/jumbo v2, "persistence"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1380
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()I

    move-result v2

    move-object v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move-object/from16 v19, v20

    move-object/from16 v20, v21

    move-object/from16 v21, v22

    move-object/from16 v22, v23

    move-object/from16 v23, v24

    move-object/from16 v24, v25

    move-object/from16 v25, v26

    move/from16 v32, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    move-wide/from16 v33, v8

    move v8, v7

    move v9, v10

    move-wide/from16 v6, v33

    move/from16 v10, v32

    goto/16 :goto_1

    .line 1385
    :pswitch_3
    const-string/jumbo v2, "entities"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1386
    const-class v2, Lcom/twitter/model/core/v;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/core/v;

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move-object/from16 v19, v20

    move-object/from16 v20, v21

    move-object/from16 v21, v22

    move-object/from16 v22, v23

    move-object/from16 v23, v24

    move-object/from16 v24, v25

    move-object/from16 v25, v26

    move/from16 v32, v6

    move-wide/from16 v33, v8

    move v8, v7

    move v9, v10

    move-wide/from16 v6, v33

    move v10, v11

    move-object v11, v12

    move-object v12, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move/from16 v5, v32

    goto/16 :goto_1

    .line 1387
    :cond_a
    const-string/jumbo v2, "header_entities"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1388
    const-class v2, Lcom/twitter/model/core/v;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/core/v;

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move-object/from16 v19, v20

    move-object/from16 v20, v21

    move-object/from16 v21, v22

    move-object/from16 v22, v23

    move-object/from16 v23, v24

    move-object/from16 v24, v25

    move-object/from16 v25, v26

    move/from16 v32, v7

    move/from16 v33, v10

    move v10, v11

    move-object v11, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    move-wide v6, v8

    move/from16 v8, v32

    move/from16 v9, v33

    goto/16 :goto_1

    .line 1389
    :cond_b
    const-string/jumbo v2, "data"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1390
    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/z;->V(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v27

    .line 1391
    const-string/jumbo v2, "tweetId"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1392
    const-string/jumbo v2, "tweetId"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v28

    .line 1393
    const-wide/16 v30, 0x0

    cmp-long v2, v28, v30

    if-lez v2, :cond_c

    move-wide/from16 v8, v28

    .line 1397
    :cond_c
    const-string/jumbo v2, "insertionIndex"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1398
    const-string/jumbo v2, "insertionIndex"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 1400
    :cond_d
    const-string/jumbo v2, "tooltipTarget"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1401
    const-string/jumbo v2, "tooltipTarget"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v17, v2

    .line 1403
    :cond_e
    const-string/jumbo v2, "isAppGraphPrompt"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1404
    const-string/jumbo v2, "isAppGraphPrompt"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 1406
    :cond_f
    const-string/jumbo v2, "clientExperimentKey"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1407
    const-string/jumbo v2, "clientExperimentKey"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v5, v2

    .line 1409
    :cond_10
    const-string/jumbo v2, "clientExperimentBucket"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 1410
    const-string/jumbo v2, "clientExperimentBucket"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v4, v2

    .line 1412
    :cond_11
    const-string/jumbo v2, "displayLocation"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 1413
    const-string/jumbo v2, "displayLocation"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v3, v2

    .line 1415
    :cond_12
    const-string/jumbo v2, "email"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1416
    const-string/jumbo v2, "email"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v16, v2

    .line 1418
    :cond_13
    const-string/jumbo v2, "corrected_email"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 1419
    const-string/jumbo v2, "corrected_email"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v15, v2

    .line 1421
    :cond_14
    const-string/jumbo v2, "phone_number"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 1422
    const-string/jumbo v2, "phone_number"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    :goto_2
    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move-object/from16 v19, v20

    move-object/from16 v20, v21

    move-object/from16 v21, v22

    move-object/from16 v22, v23

    move-object/from16 v23, v24

    move-object/from16 v24, v25

    move-object/from16 v25, v26

    move-object/from16 v32, v4

    move-object v4, v5

    move v5, v6

    move-wide/from16 v33, v8

    move v8, v7

    move v9, v10

    move-wide/from16 v6, v33

    move v10, v11

    move-object v11, v12

    move-object v12, v13

    move-object v13, v2

    move-object v2, v3

    move-object/from16 v3, v32

    .line 1424
    goto/16 :goto_1

    .line 1428
    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move v5, v6

    move-wide/from16 v32, v8

    move v8, v7

    move v9, v10

    move-wide/from16 v6, v32

    move v10, v11

    move-object v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move-object/from16 v19, v20

    move-object/from16 v20, v21

    move-object/from16 v21, v22

    move-object/from16 v22, v23

    move-object/from16 v23, v24

    move-object/from16 v24, v25

    move-object/from16 v25, v26

    .line 1429
    goto/16 :goto_1

    .line 1438
    :cond_15
    new-instance v2, Lcom/twitter/model/timeline/n$a;

    invoke-direct {v2}, Lcom/twitter/model/timeline/n$a;-><init>()V

    .line 1439
    move-object/from16 v0, v26

    invoke-virtual {v2, v0}, Lcom/twitter/model/timeline/n$a;->a(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1440
    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Lcom/twitter/model/timeline/n$a;->b(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1441
    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Lcom/twitter/model/timeline/n$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1442
    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Lcom/twitter/model/timeline/n$a;->d(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1443
    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Lcom/twitter/model/timeline/n$a;->e(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1444
    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Lcom/twitter/model/timeline/n$a;->f(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1445
    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/twitter/model/timeline/n$a;->o(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1446
    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/twitter/model/timeline/n$a;->g(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1447
    invoke-virtual {v2, v11}, Lcom/twitter/model/timeline/n$a;->b(I)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1448
    invoke-virtual {v2, v10}, Lcom/twitter/model/timeline/n$a;->a(I)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1449
    invoke-virtual {v2, v13}, Lcom/twitter/model/timeline/n$a;->a(Lcom/twitter/model/core/v;)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1450
    invoke-virtual {v2, v12}, Lcom/twitter/model/timeline/n$a;->b(Lcom/twitter/model/core/v;)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1451
    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Lcom/twitter/model/timeline/n$a;->h(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1452
    invoke-virtual {v2, v8, v9}, Lcom/twitter/model/timeline/n$a;->a(J)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1453
    invoke-virtual {v2, v7}, Lcom/twitter/model/timeline/n$a;->c(I)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1454
    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/twitter/model/timeline/n$a;->i(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1455
    invoke-virtual {v2, v6}, Lcom/twitter/model/timeline/n$a;->b(Z)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1456
    invoke-virtual {v2, v5}, Lcom/twitter/model/timeline/n$a;->j(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1457
    invoke-virtual {v2, v4}, Lcom/twitter/model/timeline/n$a;->k(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1458
    invoke-virtual {v2, v3}, Lcom/twitter/model/timeline/n$a;->l(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1459
    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/twitter/model/timeline/n$a;->m(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1460
    invoke-virtual {v2, v15}, Lcom/twitter/model/timeline/n$a;->n(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1461
    invoke-virtual {v2, v14}, Lcom/twitter/model/timeline/n$a;->p(Ljava/lang/String;)Lcom/twitter/model/timeline/n$a;

    move-result-object v2

    .line 1462
    invoke-virtual {v2}, Lcom/twitter/model/timeline/n$a;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/timeline/n;

    .line 1438
    return-object v2

    :cond_16
    move-object v2, v14

    goto/16 :goto_2

    .line 1353
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static V(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1470
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1471
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 1472
    :goto_0
    if-eqz v0, :cond_a

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_a

    .line 1473
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 1508
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 1475
    :pswitch_1
    const-string/jumbo v0, "tooltip_tweet_id"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1476
    const-string/jumbo v0, "tweetId"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1477
    :cond_1
    const-string/jumbo v0, "tooltip_target"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1478
    const-string/jumbo v0, "tooltipTarget"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1479
    :cond_2
    const-string/jumbo v0, "insertion_index"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1480
    const-string/jumbo v0, "insertionIndex"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->n()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1481
    :cond_3
    const-string/jumbo v0, "app_graph_optin"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1482
    const-string/jumbo v0, "isAppGraphPrompt"

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/fasterxml/jackson/core/JsonParser;->a(Z)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1483
    :cond_4
    const-string/jumbo v0, "client_experiment_key"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1484
    const-string/jumbo v0, "clientExperimentKey"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 1485
    :cond_5
    const-string/jumbo v0, "client_experiment_bucket"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1486
    const-string/jumbo v0, "clientExperimentBucket"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 1487
    :cond_6
    const-string/jumbo v0, "display_location"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1488
    const-string/jumbo v0, "displayLocation"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 1489
    :cond_7
    const-string/jumbo v0, "email"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1490
    const-string/jumbo v0, "email"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 1491
    :cond_8
    const-string/jumbo v0, "corrected_email"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1492
    const-string/jumbo v0, "corrected_email"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 1493
    :cond_9
    const-string/jumbo v0, "phone_number"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1494
    const-string/jumbo v0, "phone_number"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 1500
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    .line 1510
    :cond_a
    return-object v1

    .line 1473
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static W(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1673
    const/4 v1, 0x0

    .line 1674
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    .line 1675
    :goto_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    .line 1676
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 1692
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    .line 1678
    :pswitch_1
    const-string/jumbo v1, "text"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1679
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1685
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 1694
    :cond_1
    return-object v0

    .line 1676
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static X(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1771
    invoke-static {p0}, Lcom/twitter/library/api/z;->u(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1772
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1774
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1775
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1776
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    .line 1777
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/twitter/library/api/z;->a(Lcom/twitter/model/core/ac;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1778
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 1783
    :goto_1
    return-object v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static Y(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/search/a;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1869
    .line 1872
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move v0, v2

    .line 1873
    :goto_0
    if-eqz v3, :cond_2

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v4, :cond_2

    .line 1874
    sget-object v4, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v3}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    .line 1893
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    goto :goto_0

    .line 1876
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v3

    .line 1877
    const-string/jumbo v4, "follow"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v2, v1

    .line 1878
    goto :goto_1

    .line 1879
    :cond_1
    const-string/jumbo v4, "nearby"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    .line 1880
    goto :goto_1

    .line 1886
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 1896
    :cond_2
    new-instance v1, Lcom/twitter/model/search/a;

    invoke-direct {v1, v2, v0}, Lcom/twitter/model/search/a;-><init>(ZZ)V

    return-object v1

    .line 1874
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private static Z(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/api/search/TwitterTypeAhead;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2745
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2746
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 2747
    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    .line 2748
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 2764
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 2750
    :pswitch_1
    invoke-static {p0}, Lcom/twitter/library/api/z;->aa(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/search/TwitterTypeAhead;

    move-result-object v0

    .line 2751
    if-eqz v0, :cond_0

    .line 2752
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2757
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 2766
    :cond_1
    return-object v1

    .line 2748
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/ArrayList;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 1699
    .line 1700
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v0

    move v0, v1

    .line 1701
    :goto_0
    if-eqz v2, :cond_1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_1

    .line 1702
    sget-object v3, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 1721
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    .line 1704
    :pswitch_1
    const-string/jumbo v2, "context_type"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1705
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/search/d;->a(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1710
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 1714
    :pswitch_3
    const-class v2, Lcom/twitter/model/core/ac;

    invoke-static {p0, v2}, Lcom/twitter/model/json/common/e;->d(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 1723
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    :cond_2
    return v0

    .line 1702
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5377
    sget-object v0, Lcom/twitter/model/json/common/e;->a:Lcom/fasterxml/jackson/core/JsonFactory;

    invoke-virtual {v0, p0}, Lcom/fasterxml/jackson/core/JsonFactory;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    .line 5378
    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 5379
    return-object v0
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;J)Lcom/twitter/library/api/search/TwitterTypeAhead;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 2887
    .line 2895
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v4, v5

    move v6, v3

    move v2, v3

    move-object v7, v5

    move-object v8, v5

    move-object v9, v5

    move-object v10, v5

    .line 2896
    :goto_0
    if-eqz v0, :cond_5

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_5

    .line 2897
    sget-object v1, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :pswitch_0
    move v0, v6

    move-object v1, v7

    move-object v6, v8

    move-object v7, v9

    move-object v8, v10

    .line 2940
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v9

    move-object v10, v8

    move-object v8, v6

    move v6, v0

    move-object v0, v9

    move-object v9, v7

    move-object v7, v1

    goto :goto_0

    .line 2899
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 2900
    const-string/jumbo v1, "topic"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2901
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    move-object v1, v7

    move-object v7, v9

    move-object v12, v8

    move-object v8, v0

    move v0, v6

    move-object v6, v12

    goto :goto_1

    .line 2902
    :cond_1
    const-string/jumbo v1, "filter"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2903
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    move-object v1, v7

    move-object v7, v0

    move v0, v6

    move-object v6, v8

    move-object v8, v10

    goto :goto_1

    .line 2904
    :cond_2
    const-string/jumbo v1, "location"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2905
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    move-object v1, v7

    move-object v8, v10

    move-object v7, v9

    move-object v12, v0

    move v0, v6

    move-object v6, v12

    goto :goto_1

    .line 2906
    :cond_3
    const-string/jumbo v1, "ttt_context"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2907
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    move-object v7, v9

    move v0, v6

    move-object v6, v8

    move-object v8, v10

    goto :goto_1

    .line 2912
    :pswitch_2
    const-string/jumbo v0, "rounded_score"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2913
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()I

    move-result v2

    move v0, v6

    move-object v1, v7

    move-object v6, v8

    move-object v7, v9

    move-object v8, v10

    goto :goto_1

    .line 2918
    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 2919
    const-string/jumbo v1, "follow"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2920
    const/4 v0, 0x1

    move-object v1, v7

    move-object v6, v8

    move-object v7, v9

    move-object v8, v10

    goto/16 :goto_1

    .line 2925
    :pswitch_4
    const-string/jumbo v0, "tokens"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2926
    invoke-static {p0}, Lcom/twitter/library/api/z;->ae(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v4

    move v0, v6

    move-object v1, v7

    move-object v6, v8

    move-object v7, v9

    move-object v8, v10

    goto/16 :goto_1

    .line 2928
    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move v0, v6

    move-object v1, v7

    move-object v6, v8

    move-object v7, v9

    move-object v8, v10

    .line 2930
    goto/16 :goto_1

    .line 2933
    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move v0, v6

    move-object v1, v7

    move-object v6, v8

    move-object v7, v9

    move-object v8, v10

    .line 2934
    goto/16 :goto_1

    .line 2943
    :cond_5
    if-eqz v10, :cond_6

    if-nez v4, :cond_7

    :cond_6
    move-object v0, v5

    .line 2946
    :goto_2
    return-object v0

    :cond_7
    new-instance v0, Lcom/twitter/library/api/search/TwitterTypeAhead;

    const/4 v1, 0x3

    new-instance v11, Lcom/twitter/model/search/b$a;

    invoke-direct {v11}, Lcom/twitter/model/search/b$a;-><init>()V

    .line 2948
    invoke-virtual {v11, v10}, Lcom/twitter/model/search/b$a;->a(Ljava/lang/String;)Lcom/twitter/model/search/b$a;

    move-result-object v11

    .line 2949
    invoke-virtual {v11, v10}, Lcom/twitter/model/search/b$a;->b(Ljava/lang/String;)Lcom/twitter/model/search/b$a;

    move-result-object v10

    .line 2950
    invoke-virtual {v10, p1, p2}, Lcom/twitter/model/search/b$a;->b(J)Lcom/twitter/model/search/b$a;

    move-result-object v10

    .line 2951
    invoke-virtual {v10, v8}, Lcom/twitter/model/search/b$a;->c(Ljava/lang/String;)Lcom/twitter/model/search/b$a;

    move-result-object v8

    .line 2952
    invoke-virtual {v8, v6}, Lcom/twitter/model/search/b$a;->a(Z)Lcom/twitter/model/search/b$a;

    move-result-object v6

    .line 2953
    invoke-virtual {v6, v9}, Lcom/twitter/model/search/b$a;->d(Ljava/lang/String;)Lcom/twitter/model/search/b$a;

    move-result-object v6

    .line 2954
    invoke-virtual {v6, v7}, Lcom/twitter/model/search/b$a;->e(Ljava/lang/String;)Lcom/twitter/model/search/b$a;

    move-result-object v6

    .line 2955
    invoke-virtual {v6}, Lcom/twitter/model/search/b$a;->q()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/model/search/b;

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/api/search/TwitterTypeAhead;-><init>(IIILjava/util/ArrayList;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/search/b;Ljava/lang/String;)V

    goto :goto_2

    .line 2897
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/api/search/g;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 2196
    .line 2202
    const/4 v3, -0x1

    .line 2205
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v6, v1

    move v4, v5

    move v10, v5

    move-object v9, v2

    move-object v8, v2

    move-object v7, v2

    move-object v1, v2

    .line 2206
    :goto_0
    if-eqz v6, :cond_9

    sget-object v11, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v6, v11, :cond_9

    .line 2207
    sget-object v11, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v6}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v6

    aget v6, v11, v6

    packed-switch v6, :pswitch_data_0

    .line 2275
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v6

    goto :goto_0

    .line 2209
    :pswitch_1
    const-string/jumbo v6, "modules"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2210
    invoke-static {p0, p1}, Lcom/twitter/library/api/z;->g(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/core/TwitterUser;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_1

    .line 2212
    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 2217
    :pswitch_2
    const-string/jumbo v6, "metadata"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2218
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v6

    .line 2219
    :goto_2
    if-eqz v6, :cond_0

    sget-object v11, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v6, v11, :cond_0

    .line 2220
    sget-object v11, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v6}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v6

    aget v6, v11, v6

    packed-switch v6, :pswitch_data_1

    .line 2264
    :cond_2
    :goto_3
    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v6

    goto :goto_2

    .line 2222
    :pswitch_4
    const-string/jumbo v6, "cursor"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2223
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 2224
    :cond_3
    const-string/jumbo v6, "takeover_type"

    .line 2225
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v11

    .line 2224
    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2226
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v7

    goto :goto_3

    .line 2227
    :cond_4
    const-string/jumbo v6, "can_subscribe"

    .line 2228
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v11

    .line 2227
    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2229
    const-string/jumbo v6, "true"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    goto :goto_3

    .line 2230
    :cond_5
    const-string/jumbo v6, "event_id"

    .line 2231
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v11

    .line 2230
    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2232
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v9

    goto :goto_3

    .line 2233
    :cond_6
    const-string/jumbo v6, "event_page_type"

    .line 2234
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v11

    .line 2233
    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2235
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v8

    goto :goto_3

    .line 2240
    :pswitch_5
    const-string/jumbo v6, "refresh_interval_in_sec"

    .line 2241
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v11

    .line 2240
    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2242
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->n()I

    move-result v3

    goto :goto_3

    .line 2247
    :pswitch_6
    const-string/jumbo v6, "has_events_response"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    move v4, v0

    .line 2248
    goto/16 :goto_3

    .line 2249
    :cond_7
    const-string/jumbo v6, "can_subscribe"

    .line 2250
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v11

    .line 2249
    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v10, v0

    .line 2251
    goto/16 :goto_3

    .line 2257
    :pswitch_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_3

    .line 2267
    :cond_8
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    .line 2278
    :cond_9
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2279
    if-nez v1, :cond_a

    .line 2280
    new-instance v0, Lcom/twitter/library/util/InvalidDataException;

    const-string/jumbo v1, "Search did not return results module"

    invoke-direct {v0, v1}, Lcom/twitter/library/util/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2283
    :cond_a
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 2284
    :cond_b
    :goto_4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2285
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/h;

    .line 2286
    iget-boolean v12, v0, Lcom/twitter/library/api/search/h;->j:Z

    if-eqz v12, :cond_c

    .line 2287
    add-int/lit8 v5, v5, 0x1

    .line 2289
    :cond_c
    iget-boolean v12, v0, Lcom/twitter/library/api/search/h;->l:Z

    if-eqz v12, :cond_b

    iget-object v12, v0, Lcom/twitter/library/api/search/h;->k:Ljava/util/List;

    if-eqz v12, :cond_b

    iget-object v12, v0, Lcom/twitter/library/api/search/h;->k:Ljava/util/List;

    .line 2290
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    if-lez v12, :cond_b

    .line 2291
    iget-object v0, v0, Lcom/twitter/library/api/search/h;->k:Ljava/util/List;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    .line 2295
    :cond_d
    new-instance v0, Lcom/twitter/library/api/search/g;

    invoke-direct/range {v0 .. v10}, Lcom/twitter/library/api/search/g;-><init>(Ljava/util/ArrayList;Ljava/lang/String;IZILjava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0

    .line 2207
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 2220
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_4
        :pswitch_3
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public static a(Lcom/twitter/model/core/z;)Lcom/twitter/library/api/u;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 3087
    .line 3093
    invoke-virtual {p0}, Lcom/twitter/model/core/z;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move-object v5, v6

    move-object v4, v6

    move-object v3, v6

    move-object v2, v6

    move-object v1, v6

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/y;

    .line 3094
    iget-object v7, v0, Lcom/twitter/model/core/y;->c:Ljava/lang/String;

    .line 3095
    if-eqz v7, :cond_1

    const-string/jumbo v9, "The user failed validation: "

    invoke-virtual {v7, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 3096
    const-string/jumbo v9, "The user failed validation: "

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 3099
    :cond_1
    const-string/jumbo v9, "name"

    iget-object v10, v0, Lcom/twitter/model/core/y;->e:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    if-nez v1, :cond_2

    move-object v1, v7

    .line 3100
    goto :goto_0

    .line 3101
    :cond_2
    const-string/jumbo v9, "screen_name"

    iget-object v10, v0, Lcom/twitter/model/core/y;->e:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    if-nez v4, :cond_3

    move-object v4, v7

    .line 3102
    goto :goto_0

    .line 3103
    :cond_3
    const-string/jumbo v9, "password"

    iget-object v10, v0, Lcom/twitter/model/core/y;->e:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    if-nez v5, :cond_4

    move-object v5, v7

    .line 3104
    goto :goto_0

    .line 3105
    :cond_4
    const-string/jumbo v9, "email"

    iget-object v10, v0, Lcom/twitter/model/core/y;->e:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    if-nez v2, :cond_5

    move-object v2, v7

    .line 3106
    goto :goto_0

    .line 3107
    :cond_5
    const-string/jumbo v9, "devices"

    iget-object v0, v0, Lcom/twitter/model/core/y;->e:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez v3, :cond_0

    move-object v3, v7

    .line 3108
    goto :goto_0

    .line 3111
    :cond_6
    new-instance v0, Lcom/twitter/library/api/u;

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/api/u;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/x;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Lcom/twitter/library/api/x",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 281
    .line 284
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    move-object v0, v3

    move-object v1, v3

    move-object v2, v3

    .line 287
    :goto_0
    if-eqz v4, :cond_2

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v5, :cond_2

    .line 288
    sget-object v5, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 317
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_0

    .line 290
    :pswitch_0
    const-string/jumbo v5, "users"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 291
    const-class v1, Lcom/twitter/model/core/TwitterUser;

    invoke-static {p0, v1}, Lcom/twitter/model/json/common/e;->d(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    goto :goto_1

    .line 293
    :cond_1
    :goto_2
    if-eqz v4, :cond_0

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v5, :cond_0

    .line 294
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_2

    .line 301
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v4

    .line 302
    const-string/jumbo v5, "next_cursor_str"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 303
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 309
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 319
    :cond_2
    if-nez v1, :cond_3

    move-object v0, v3

    .line 322
    :goto_3
    return-object v0

    :cond_3
    new-instance v0, Lcom/twitter/library/api/x;

    invoke-direct {v0, v2, v1}, Lcom/twitter/library/api/x;-><init>(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_3

    .line 288
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/Map;)Lcom/twitter/model/core/TwitterSocialProof;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)",
            "Lcom/twitter/model/core/TwitterSocialProof;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v6, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 3991
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v8

    .line 3997
    const-class v0, Lcom/twitter/model/timeline/c;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/c;

    .line 3998
    if-eqz v0, :cond_8

    .line 3999
    iget-object v7, v0, Lcom/twitter/model/timeline/c;->a:Ljava/lang/String;

    .line 4000
    iget-object v5, v0, Lcom/twitter/model/timeline/c;->b:Ljava/lang/String;

    .line 4001
    iget-object v1, v0, Lcom/twitter/model/timeline/c;->c:Ljava/util/List;

    .line 4002
    iget v4, v0, Lcom/twitter/model/timeline/c;->d:I

    .line 4003
    iget-object v9, v0, Lcom/twitter/model/timeline/c;->e:Ljava/util/List;

    if-eqz v9, :cond_1

    .line 4004
    iget-object v0, v0, Lcom/twitter/model/timeline/c;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 4005
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 4006
    if-eqz v0, :cond_0

    .line 4007
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object v0, v7

    move-object v7, v1

    move-object v1, v5

    .line 4020
    :goto_1
    if-eqz v1, :cond_3

    .line 4023
    sget-object v3, Lcom/twitter/library/api/z;->b:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v3, Lcom/twitter/library/api/z;->b:Ljava/util/Map;

    .line 4024
    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_2
    move-object v3, v1

    move v5, v4

    move v4, v0

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    .line 4056
    :goto_3
    new-instance v6, Lcom/twitter/model/core/TwitterSocialProof$a;

    invoke-direct {v6}, Lcom/twitter/model/core/TwitterSocialProof$a;-><init>()V

    .line 4057
    invoke-virtual {v6, v4}, Lcom/twitter/model/core/TwitterSocialProof$a;->a(I)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v4

    .line 4058
    invoke-virtual {v4, v3}, Lcom/twitter/model/core/TwitterSocialProof$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v3

    .line 4059
    invoke-virtual {v3, v5}, Lcom/twitter/model/core/TwitterSocialProof$a;->b(I)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v3

    .line 4060
    invoke-virtual {v3, v2}, Lcom/twitter/model/core/TwitterSocialProof$a;->b(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v2

    .line 4061
    invoke-virtual {v2, v1}, Lcom/twitter/model/core/TwitterSocialProof$a;->h(I)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v1

    .line 4062
    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterSocialProof$a;->d(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    .line 4063
    invoke-virtual {v0, v7}, Lcom/twitter/model/core/TwitterSocialProof$a;->a(Ljava/util/List;)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    .line 4064
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterSocialProof$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterSocialProof;

    .line 4056
    return-object v0

    .line 4024
    :cond_2
    const/16 v0, 0x1c

    goto :goto_2

    .line 4029
    :cond_3
    const-string/jumbo v1, "favorite"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 4030
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4031
    const/16 v0, 0x11

    move v1, v3

    move v5, v4

    move-object v3, v2

    move v4, v0

    move-object v0, v2

    .line 4033
    goto :goto_3

    .line 4035
    :cond_4
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    .line 4036
    sub-int v5, v4, v1

    .line 4037
    invoke-interface {v8, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v4

    .line 4038
    if-le v1, v10, :cond_5

    .line 4039
    invoke-interface {v8, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v1

    .line 4043
    :goto_4
    if-nez v5, :cond_6

    .line 4044
    const/16 v0, 0x10

    :goto_5
    move-object v11, v2

    move-object v2, v1

    move v1, v3

    move-object v3, v4

    move v4, v0

    move-object v0, v11

    .line 4048
    goto :goto_3

    :cond_5
    move-object v1, v2

    .line 4041
    goto :goto_4

    .line 4046
    :cond_6
    const/16 v0, 0x21

    goto :goto_5

    :cond_7
    move-object v0, v2

    move v1, v3

    move v5, v4

    move v4, v3

    move-object v3, v2

    .line 4052
    goto :goto_3

    :cond_8
    move v4, v6

    move-object v7, v2

    move-object v1, v2

    move-object v0, v2

    goto/16 :goto_1
.end method

.method private static a(Lcom/twitter/model/json/timeline/wtf/JsonSocialProof;Ljava/util/Map;)Lcom/twitter/model/core/TwitterSocialProof;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/json/timeline/wtf/JsonSocialProof;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)",
            "Lcom/twitter/model/core/TwitterSocialProof;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 4551
    if-eqz p0, :cond_5

    .line 4552
    iget-object v0, p0, Lcom/twitter/model/json/timeline/wtf/JsonSocialProof;->a:Lcom/twitter/model/json/core/b;

    iget v0, v0, Lcom/twitter/model/json/core/b;->b:I

    const/4 v3, -0x1

    if-eq v0, v3, :cond_5

    .line 4553
    new-instance v4, Lcom/twitter/model/core/TwitterSocialProof$a;

    invoke-direct {v4}, Lcom/twitter/model/core/TwitterSocialProof$a;-><init>()V

    .line 4554
    iget-object v0, p0, Lcom/twitter/model/json/timeline/wtf/JsonSocialProof;->a:Lcom/twitter/model/json/core/b;

    iget v0, v0, Lcom/twitter/model/json/core/b;->b:I

    invoke-virtual {v4, v0}, Lcom/twitter/model/core/TwitterSocialProof$a;->a(I)Lcom/twitter/model/core/TwitterSocialProof$a;

    .line 4555
    iget-object v0, p0, Lcom/twitter/model/json/timeline/wtf/JsonSocialProof;->b:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/twitter/model/core/TwitterSocialProof$a;->c(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;

    .line 4557
    iget-object v0, p0, Lcom/twitter/model/json/timeline/wtf/JsonSocialProof;->c:[Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->a([Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 4558
    new-instance v5, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/twitter/model/json/timeline/wtf/JsonSocialProof;->c:[Ljava/lang/String;

    array-length v0, v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 4559
    iget-object v6, p0, Lcom/twitter/model/json/timeline/wtf/JsonSocialProof;->c:[Ljava/lang/String;

    array-length v7, v6

    move v3, v1

    :goto_0
    if-ge v3, v7, :cond_1

    aget-object v0, v6, v3

    .line 4560
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 4561
    if-eqz v0, :cond_0

    .line 4562
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4559
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 4565
    :cond_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    .line 4566
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iget-object v6, v0, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    .line 4567
    if-le v3, v8, :cond_2

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iget-object v2, v0, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    .line 4569
    :cond_2
    const/4 v0, 0x2

    if-le v3, v0, :cond_4

    add-int/lit8 v0, v3, -0x2

    .line 4571
    :goto_1
    invoke-virtual {v4, v6}, Lcom/twitter/model/core/TwitterSocialProof$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v1

    .line 4572
    invoke-virtual {v1, v2}, Lcom/twitter/model/core/TwitterSocialProof$a;->b(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v1

    .line 4573
    invoke-virtual {v1, v0}, Lcom/twitter/model/core/TwitterSocialProof$a;->g(I)Lcom/twitter/model/core/TwitterSocialProof$a;

    .line 4575
    :cond_3
    invoke-virtual {v4}, Lcom/twitter/model/core/TwitterSocialProof$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterSocialProof;

    .line 4578
    :goto_2
    return-object v0

    :cond_4
    move v0, v1

    .line 4569
    goto :goto_1

    :cond_5
    move-object v0, v2

    .line 4578
    goto :goto_2
.end method

.method public static a(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)Lcom/twitter/model/core/ac;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/ac;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)",
            "Lcom/twitter/model/core/ac;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 4857
    invoke-virtual {p1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/twitter/model/core/ac;

    .line 4858
    if-eqz v4, :cond_1

    iget-object v0, v4, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_1

    .line 4859
    iget-object v0, v4, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 4860
    if-eqz v0, :cond_2

    .line 4861
    iput-object v0, v4, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    .line 4862
    invoke-static {p2, p1, v4}, Lcom/twitter/library/api/z;->a(Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/model/core/ac;)V

    .line 4871
    :cond_0
    invoke-static {v4, p2}, Lcom/twitter/library/api/z;->a(Lcom/twitter/model/core/ac;Ljava/util/HashMap;)Z

    move-result v0

    if-nez v0, :cond_3

    move-object v4, v6

    .line 4889
    :cond_1
    :goto_0
    return-object v4

    .line 4863
    :cond_2
    invoke-virtual {v4}, Lcom/twitter/model/core/ac;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4868
    iget-wide v0, v4, Lcom/twitter/model/core/ac;->a:J

    invoke-virtual {p2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-virtual {p1}, Ljava/util/HashMap;->size()I

    move-result v3

    iget-object v4, v4, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v4, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/api/z;->a(JIIJ)V

    move-object v4, v6

    .line 4869
    goto :goto_0

    .line 4874
    :cond_3
    iget-object v0, v4, Lcom/twitter/model/core/ac;->n:Lcom/twitter/model/core/ac;

    if-eqz v0, :cond_1

    iget-object v0, v4, Lcom/twitter/model/core/ac;->n:Lcom/twitter/model/core/ac;

    iget-object v0, v0, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_1

    .line 4875
    iget-object v0, v4, Lcom/twitter/model/core/ac;->n:Lcom/twitter/model/core/ac;

    iget-object v0, v0, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 4876
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 4877
    if-eqz v0, :cond_4

    .line 4878
    iget-object v1, v4, Lcom/twitter/model/core/ac;->n:Lcom/twitter/model/core/ac;

    iput-object v0, v1, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    .line 4884
    iget-object v0, v4, Lcom/twitter/model/core/ac;->n:Lcom/twitter/model/core/ac;

    invoke-static {v0, p2}, Lcom/twitter/library/api/z;->a(Lcom/twitter/model/core/ac;Ljava/util/HashMap;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v4, v6

    .line 4885
    goto :goto_0

    .line 4880
    :cond_4
    new-instance v0, Lcom/twitter/model/json/common/InvalidJsonFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Missing original user "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v4, Lcom/twitter/model/core/ac;->n:Lcom/twitter/model/core/ac;

    iget-object v2, v2, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/model/json/common/InvalidJsonFormatException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    move-object v4, v6

    .line 4882
    goto :goto_0
.end method

.method private static a(ILcom/fasterxml/jackson/core/JsonParser;Ljava/util/Map;)Lcom/twitter/model/moments/x;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/Moment;",
            ">;)",
            "Lcom/twitter/model/moments/x;"
        }
    .end annotation

    .prologue
    .line 4275
    if-eqz p2, :cond_0

    .line 4276
    const-class v0, Lcom/twitter/model/json/moments/JsonTimelineMomentId;

    .line 4277
    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/moments/JsonTimelineMomentId;

    .line 4278
    if-eqz v0, :cond_0

    .line 4279
    iget-wide v0, v0, Lcom/twitter/model/json/moments/JsonTimelineMomentId;->a:J

    .line 4280
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/Moment;

    .line 4281
    if-eqz v0, :cond_0

    .line 4282
    new-instance v1, Lcom/twitter/model/moments/x;

    invoke-direct {v1, p0, v0}, Lcom/twitter/model/moments/x;-><init>(ILcom/twitter/model/moments/Moment;)V

    move-object v0, v1

    .line 4286
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;)Lcom/twitter/model/timeline/f$a;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)",
            "Lcom/twitter/model/timeline/f$a;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4929
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 4931
    const-wide/16 v2, 0x0

    .line 4932
    const/4 v1, 0x0

    .line 4933
    :goto_0
    if-eqz v0, :cond_3

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v4, :cond_3

    .line 4934
    sget-object v4, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :pswitch_0
    move v0, v1

    .line 4965
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move v7, v0

    move-object v0, v1

    move v1, v7

    goto :goto_0

    .line 4936
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 4937
    const-string/jumbo v4, "target_count"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4938
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()I

    move-result v0

    goto :goto_1

    .line 4943
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 4944
    const-string/jumbo v4, "root_user_id"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 4945
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v4

    .line 4946
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 4947
    if-nez v0, :cond_1

    .line 4948
    new-instance v0, Lcom/twitter/model/json/common/InvalidJsonFormatException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Root user "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " not in users map"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/twitter/model/json/common/InvalidJsonFormatException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    :cond_1
    move v0, v1

    .line 4951
    goto :goto_1

    :cond_2
    const-string/jumbo v4, "target_tweet_id"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4952
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    move v0, v1

    goto :goto_1

    .line 4958
    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move v0, v1

    .line 4959
    goto :goto_1

    .line 4967
    :cond_3
    new-instance v0, Lcom/twitter/model/timeline/f$a;

    invoke-direct {v0, v2, v3, v1}, Lcom/twitter/model/timeline/f$a;-><init>(JI)V

    return-object v0

    .line 4934
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(ILcom/fasterxml/jackson/core/JsonParser;Ljava/util/Map;Lcom/twitter/model/timeline/ae$a;)Lcom/twitter/model/timeline/y$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/Moment;",
            ">;",
            "Lcom/twitter/model/timeline/ae$a;",
            ")",
            "Lcom/twitter/model/timeline/y$a;"
        }
    .end annotation

    .prologue
    .line 4265
    invoke-static {p0, p1, p2}, Lcom/twitter/library/api/z;->a(ILcom/fasterxml/jackson/core/JsonParser;Ljava/util/Map;)Lcom/twitter/model/moments/x;

    move-result-object v0

    .line 4266
    if-eqz v0, :cond_0

    .line 4267
    invoke-virtual {p3, v0}, Lcom/twitter/model/timeline/ae$a;->a(Lcom/twitter/model/moments/x;)Lcom/twitter/model/timeline/y$a;

    .line 4269
    :cond_0
    return-object p3
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/timeline/ac$a;)Lcom/twitter/model/timeline/y$a;
    .locals 7

    .prologue
    .line 4240
    const-class v0, Lcom/twitter/model/json/timeline/JsonTimelineMessage;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/twitter/model/json/timeline/JsonTimelineMessage;

    .line 4243
    if-nez v6, :cond_1

    .line 4258
    :cond_0
    :goto_0
    return-object p1

    .line 4246
    :cond_1
    iget-object v0, v6, Lcom/twitter/model/json/timeline/JsonTimelineMessage;->c:Lcom/twitter/model/json/timeline/JsonInlinePromptTwoActions;

    if-eqz v0, :cond_3

    iget-object v0, v6, Lcom/twitter/model/json/timeline/JsonTimelineMessage;->c:Lcom/twitter/model/json/timeline/JsonInlinePromptTwoActions;

    invoke-virtual {v0}, Lcom/twitter/model/json/timeline/JsonInlinePromptTwoActions;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4247
    iget-object v4, v6, Lcom/twitter/model/json/timeline/JsonTimelineMessage;->c:Lcom/twitter/model/json/timeline/JsonInlinePromptTwoActions;

    .line 4248
    new-instance v0, Lcom/twitter/model/timeline/ab;

    iget-object v1, v4, Lcom/twitter/model/json/timeline/JsonInlinePromptTwoActions;->a:Ljava/lang/String;

    iget-object v2, v4, Lcom/twitter/model/json/timeline/JsonInlinePromptTwoActions;->b:Ljava/lang/String;

    iget-object v3, v4, Lcom/twitter/model/json/timeline/JsonInlinePromptTwoActions;->c:Lcom/twitter/model/timeline/l;

    iget-object v4, v4, Lcom/twitter/model/json/timeline/JsonInlinePromptTwoActions;->d:Lcom/twitter/model/timeline/l;

    iget-boolean v5, v6, Lcom/twitter/model/json/timeline/JsonTimelineMessage;->b:Z

    invoke-direct/range {v0 .. v5}, Lcom/twitter/model/timeline/ab;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/timeline/l;Lcom/twitter/model/timeline/l;Z)V

    invoke-virtual {p1, v0}, Lcom/twitter/model/timeline/ac$a;->a(Lcom/twitter/model/timeline/ab;)Lcom/twitter/model/timeline/ac$a;

    .line 4255
    :cond_2
    :goto_1
    iget-object v0, v6, Lcom/twitter/model/json/timeline/JsonTimelineMessage;->a:Lcom/twitter/model/timeline/r;

    if-eqz v0, :cond_0

    .line 4256
    iget-object v0, v6, Lcom/twitter/model/json/timeline/JsonTimelineMessage;->a:Lcom/twitter/model/timeline/r;

    invoke-virtual {p1, v0}, Lcom/twitter/model/timeline/ac$a;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/timeline/y$a;

    goto :goto_0

    .line 4250
    :cond_3
    iget-object v0, v6, Lcom/twitter/model/json/timeline/JsonTimelineMessage;->d:Lcom/twitter/model/json/timeline/JsonInlinePrompt;

    if-eqz v0, :cond_2

    iget-object v0, v6, Lcom/twitter/model/json/timeline/JsonTimelineMessage;->d:Lcom/twitter/model/json/timeline/JsonInlinePrompt;

    invoke-virtual {v0}, Lcom/twitter/model/json/timeline/JsonInlinePrompt;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4251
    iget-object v4, v6, Lcom/twitter/model/json/timeline/JsonTimelineMessage;->d:Lcom/twitter/model/json/timeline/JsonInlinePrompt;

    .line 4252
    new-instance v0, Lcom/twitter/model/timeline/ab;

    iget-object v1, v4, Lcom/twitter/model/json/timeline/JsonInlinePrompt;->a:Ljava/lang/String;

    iget-object v2, v4, Lcom/twitter/model/json/timeline/JsonInlinePrompt;->b:Ljava/lang/String;

    iget-object v3, v4, Lcom/twitter/model/json/timeline/JsonInlinePrompt;->c:Lcom/twitter/model/timeline/l;

    iget-object v4, v4, Lcom/twitter/model/json/timeline/JsonInlinePrompt;->d:Lcom/twitter/model/timeline/l;

    iget-boolean v5, v6, Lcom/twitter/model/json/timeline/JsonTimelineMessage;->b:Z

    invoke-direct/range {v0 .. v5}, Lcom/twitter/model/timeline/ab;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/timeline/l;Lcom/twitter/model/timeline/l;Z)V

    invoke-virtual {p1, v0}, Lcom/twitter/model/timeline/ac$a;->a(Lcom/twitter/model/timeline/ab;)Lcom/twitter/model/timeline/ac$a;

    goto :goto_1
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/timeline/aj$a;)Lcom/twitter/model/timeline/y$a;
    .locals 25
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4292
    const/4 v5, 0x0

    .line 4293
    const/4 v6, 0x0

    .line 4294
    const/4 v7, 0x0

    .line 4295
    const/16 v22, 0x0

    .line 4296
    const/4 v8, 0x0

    .line 4297
    const/4 v3, 0x0

    .line 4298
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    move-object/from16 v24, v2

    move-object v2, v3

    move-object/from16 v3, v24

    .line 4299
    :goto_0
    if-eqz v3, :cond_7

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v4, :cond_7

    .line 4300
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v9

    .line 4301
    sget-object v4, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v3}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    move-object/from16 v24, v6

    move-object v6, v5

    move-object/from16 v5, v24

    .line 4348
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    move-object/from16 v24, v5

    move-object v5, v6

    move-object/from16 v6, v24

    .line 4349
    goto :goto_0

    .line 4303
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object/from16 v24, v6

    move-object v6, v5

    move-object/from16 v5, v24

    .line 4304
    goto :goto_1

    .line 4307
    :pswitch_2
    invoke-virtual {v9}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/model/topic/TwitterTopic;->c(Ljava/lang/String;)I

    move-result v3

    .line 4308
    const/4 v4, -0x1

    if-eq v3, v4, :cond_6

    .line 4310
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    move-object/from16 v3, v22

    move-object v4, v7

    move-object v7, v2

    move-object v2, v8

    move-object/from16 v24, v6

    move-object v6, v5

    move-object/from16 v5, v24

    .line 4311
    :goto_2
    if-eqz v7, :cond_5

    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v7, v8, :cond_5

    .line 4312
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v8

    .line 4313
    sget-object v10, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v7}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v7

    aget v7, v10, v7

    packed-switch v7, :pswitch_data_1

    .line 4337
    :cond_0
    :goto_3
    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v7

    goto :goto_2

    .line 4316
    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    .line 4320
    :pswitch_5
    const-string/jumbo v7, "name"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 4321
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v6

    goto :goto_3

    .line 4322
    :cond_1
    const-string/jumbo v7, "query"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 4323
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    .line 4324
    :cond_2
    const-string/jumbo v7, "seed_hashtag"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 4325
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 4326
    :cond_3
    const-string/jumbo v7, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 4327
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    .line 4328
    :cond_4
    const-string/jumbo v7, "event_status"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 4329
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :cond_5
    move-object v8, v2

    move-object/from16 v22, v3

    move-object v7, v4

    move-object v2, v9

    .line 4339
    goto/16 :goto_1

    .line 4340
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object/from16 v24, v6

    move-object v6, v5

    move-object/from16 v5, v24

    .line 4342
    goto/16 :goto_1

    .line 4350
    :cond_7
    if-eqz v2, :cond_8

    .line 4351
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/model/topic/TwitterTopic;->c(Ljava/lang/String;)I

    move-result v2

    .line 4352
    new-instance v3, Lcom/twitter/model/topic/TwitterTopic;

    new-instance v4, Lcom/twitter/model/topic/TwitterTopic$a;

    const/4 v9, 0x0

    invoke-direct {v4, v2, v6, v9}, Lcom/twitter/model/topic/TwitterTopic$a;-><init>(ILjava/lang/String;Z)V

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-wide/16 v12, 0x1

    const-wide/16 v14, 0x0

    const-wide/16 v16, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v23, 0x0

    invoke-direct/range {v3 .. v23}, Lcom/twitter/model/topic/TwitterTopic;-><init>(Lcom/twitter/model/topic/TwitterTopic$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLcgi;Ljava/util/ArrayList;Lcom/twitter/model/topic/b;Lcom/twitter/model/core/TwitterUser;Ljava/lang/String;Ljava/lang/String;)V

    .line 4355
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/twitter/model/timeline/aj$a;->a(Lcom/twitter/model/topic/TwitterTopic;)Lcom/twitter/model/timeline/aj$a;

    .line 4357
    :cond_8
    return-object p1

    .line 4301
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 4313
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/timeline/v$a;)Lcom/twitter/model/timeline/y$a;
    .locals 3

    .prologue
    .line 3982
    const-class v0, Lcom/twitter/model/timeline/a;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/a;

    .line 3983
    iget-object v1, v0, Lcom/twitter/model/timeline/a;->a:Lcom/twitter/model/timeline/b;

    invoke-virtual {p1, v1}, Lcom/twitter/model/timeline/v$a;->a(Lcom/twitter/model/timeline/b;)Lcom/twitter/model/timeline/v$a;

    move-result-object v1

    iget-object v2, v0, Lcom/twitter/model/timeline/a;->b:Lcom/twitter/model/timeline/h;

    .line 3984
    invoke-virtual {v1, v2}, Lcom/twitter/model/timeline/v$a;->a(Lcom/twitter/model/timeline/h;)Lcom/twitter/model/timeline/y$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/timeline/v$a;

    iget-object v0, v0, Lcom/twitter/model/timeline/a;->c:Lcom/twitter/model/timeline/r;

    .line 3985
    invoke-virtual {v1, v0}, Lcom/twitter/model/timeline/v$a;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    .line 3983
    return-object v0
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/model/timeline/ag$a;)Lcom/twitter/model/timeline/y$a;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/ac;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;",
            "Lcom/twitter/model/timeline/ag$a;",
            ")",
            "Lcom/twitter/model/timeline/y$a;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4367
    const-class v2, Lcom/twitter/model/json/timeline/JsonRecap;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/json/timeline/JsonRecap;

    .line 4369
    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/twitter/model/json/timeline/JsonRecap;->a:[Ljava/lang/String;

    if-nez v3, :cond_1

    .line 4429
    :cond_0
    :goto_0
    return-object p3

    .line 4375
    :cond_1
    iget-object v3, v2, Lcom/twitter/model/json/timeline/JsonRecap;->a:[Ljava/lang/String;

    array-length v3, v3

    .line 4376
    invoke-static {v3}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v6

    .line 4377
    iget-object v3, v2, Lcom/twitter/model/json/timeline/JsonRecap;->a:[Ljava/lang/String;

    array-length v3, v3

    invoke-static {v3}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v7

    .line 4378
    iget-object v8, v2, Lcom/twitter/model/json/timeline/JsonRecap;->b:Lcom/twitter/model/json/timeline/JsonSuggestsInfo;

    .line 4379
    iget-object v9, v2, Lcom/twitter/model/json/timeline/JsonRecap;->a:[Ljava/lang/String;

    array-length v10, v9

    const/4 v3, 0x0

    move v5, v3

    :goto_1
    if-ge v5, v10, :cond_5

    aget-object v11, v9, v5

    .line 4380
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v11, v0, v1}, Lcom/twitter/library/api/z;->a(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)Lcom/twitter/model/core/ac;

    move-result-object v12

    .line 4381
    if-eqz v12, :cond_4

    .line 4382
    new-instance v13, Lcom/twitter/model/timeline/ah$a;

    invoke-direct {v13}, Lcom/twitter/model/timeline/ah$a;-><init>()V

    .line 4383
    iget-object v3, v2, Lcom/twitter/model/json/timeline/JsonRecap;->e:Lcom/twitter/model/json/timeline/JsonRecap$JsonBanner;

    if-eqz v3, :cond_2

    iget-object v3, v2, Lcom/twitter/model/json/timeline/JsonRecap;->e:Lcom/twitter/model/json/timeline/JsonRecap$JsonBanner;

    iget-object v3, v3, Lcom/twitter/model/json/timeline/JsonRecap$JsonBanner;->a:Ljava/util/Map;

    if-eqz v3, :cond_2

    .line 4384
    iget-object v3, v2, Lcom/twitter/model/json/timeline/JsonRecap;->e:Lcom/twitter/model/json/timeline/JsonRecap$JsonBanner;

    iget-object v3, v3, Lcom/twitter/model/json/timeline/JsonRecap$JsonBanner;->a:Ljava/util/Map;

    invoke-interface {v3, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/model/timeline/c;

    .line 4385
    if-eqz v3, :cond_2

    sget-object v4, Lcom/twitter/library/api/z;->b:Ljava/util/Map;

    iget-object v14, v3, Lcom/twitter/model/timeline/c;->a:Ljava/lang/String;

    invoke-interface {v4, v14}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 4388
    sget-object v4, Lcom/twitter/library/api/z;->b:Ljava/util/Map;

    iget-object v14, v3, Lcom/twitter/model/timeline/c;->a:Ljava/lang/String;

    invoke-interface {v4, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 4389
    new-instance v14, Lcom/twitter/model/core/TwitterSocialProof$a;

    invoke-direct {v14}, Lcom/twitter/model/core/TwitterSocialProof$a;-><init>()V

    .line 4392
    invoke-virtual {v14, v4}, Lcom/twitter/model/core/TwitterSocialProof$a;->a(I)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v14

    iget-object v15, v3, Lcom/twitter/model/timeline/c;->b:Ljava/lang/String;

    .line 4393
    invoke-virtual {v14, v15}, Lcom/twitter/model/core/TwitterSocialProof$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v14

    .line 4394
    invoke-virtual {v14, v4}, Lcom/twitter/model/core/TwitterSocialProof$a;->h(I)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v4

    iget-object v14, v3, Lcom/twitter/model/timeline/c;->b:Ljava/lang/String;

    .line 4395
    invoke-virtual {v4, v14}, Lcom/twitter/model/core/TwitterSocialProof$a;->d(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v4

    iget-object v3, v3, Lcom/twitter/model/timeline/c;->c:Ljava/util/List;

    .line 4396
    invoke-virtual {v4, v3}, Lcom/twitter/model/core/TwitterSocialProof$a;->a(Ljava/util/List;)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v3

    .line 4397
    invoke-virtual {v3}, Lcom/twitter/model/core/TwitterSocialProof$a;->q()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/model/core/TwitterSocialProof;

    .line 4389
    invoke-virtual {v13, v3}, Lcom/twitter/model/timeline/ah$a;->a(Lcom/twitter/model/core/TwitterSocialProof;)Lcom/twitter/model/timeline/y$a;

    .line 4401
    :cond_2
    if-eqz v8, :cond_3

    .line 4402
    iget-object v3, v8, Lcom/twitter/model/json/timeline/JsonSuggestsInfo;->b:Ljava/util/Map;

    invoke-interface {v3, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/model/timeline/r;

    .line 4403
    invoke-virtual {v13, v3}, Lcom/twitter/model/timeline/ah$a;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/timeline/y$a;

    .line 4405
    :cond_3
    invoke-virtual {v13, v12}, Lcom/twitter/model/timeline/ah$a;->a(Lcom/twitter/model/core/ac;)Lcom/twitter/model/timeline/ah$a;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 4406
    iget-wide v12, v12, Lcom/twitter/model/core/ac;->a:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v7, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 4379
    :cond_4
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto/16 :goto_1

    .line 4409
    :cond_5
    invoke-virtual {v6}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 4410
    if-eqz v8, :cond_8

    iget-object v4, v8, Lcom/twitter/model/json/timeline/JsonSuggestsInfo;->a:Lcom/twitter/model/timeline/r;

    .line 4411
    :goto_2
    new-instance v5, Lcom/twitter/model/timeline/o$a;

    invoke-direct {v5}, Lcom/twitter/model/timeline/o$a;-><init>()V

    .line 4412
    invoke-virtual {v5, v4}, Lcom/twitter/model/timeline/o$a;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/timeline/o$a;

    move-result-object v5

    .line 4413
    invoke-virtual {v7}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-virtual {v5, v4}, Lcom/twitter/model/timeline/o$a;->a(Ljava/util/List;)Lcom/twitter/model/timeline/o$a;

    move-result-object v4

    .line 4414
    iget-object v5, v2, Lcom/twitter/model/json/timeline/JsonRecap;->c:Lcom/twitter/model/json/timeline/JsonStrings;

    .line 4415
    if-eqz v5, :cond_6

    .line 4416
    iget-object v5, v5, Lcom/twitter/model/json/timeline/JsonStrings;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/twitter/model/timeline/o$a;->a(Ljava/lang/String;)Lcom/twitter/model/timeline/o$a;

    .line 4418
    :cond_6
    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lcom/twitter/model/timeline/ag$a;->a(Ljava/util/List;)Lcom/twitter/model/timeline/ag$a;

    .line 4419
    invoke-virtual {v4}, Lcom/twitter/model/timeline/o$a;->q()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/model/timeline/o;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lcom/twitter/model/timeline/ag$a;->a(Lcom/twitter/model/timeline/o;)Lcom/twitter/model/timeline/ag$a;

    .line 4420
    iget-object v3, v2, Lcom/twitter/model/json/timeline/JsonRecap;->f:Lcom/twitter/model/timeline/m;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lcom/twitter/model/timeline/ag$a;->a(Lcom/twitter/model/timeline/m;)Lcom/twitter/model/timeline/ag$a;

    .line 4421
    iget-object v2, v2, Lcom/twitter/model/json/timeline/JsonRecap;->d:Lcom/twitter/model/timeline/k;

    .line 4422
    if-eqz v2, :cond_7

    .line 4423
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/twitter/model/timeline/ag$a;->a(Lcom/twitter/model/timeline/k;)Lcom/twitter/model/timeline/y$a;

    .line 4427
    :cond_7
    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/twitter/model/timeline/ag$a;->b(I)Lcom/twitter/model/timeline/y$a;

    goto/16 :goto_0

    .line 4410
    :cond_8
    const/4 v4, 0x0

    goto :goto_2
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/model/timeline/ah$a;)Lcom/twitter/model/timeline/y$a;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/ac;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;",
            "Lcom/twitter/model/timeline/ah$a;",
            ")",
            "Lcom/twitter/model/timeline/y$a;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 4602
    .line 4604
    const/4 v2, 0x0

    .line 4605
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v4, v0

    move v0, v2

    move-object v2, v3

    .line 4606
    :goto_0
    if-eqz v4, :cond_2

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v5, :cond_2

    .line 4607
    sget-object v5, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_0

    .line 4632
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_0

    .line 4609
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v4

    .line 4610
    const-string/jumbo v5, "id"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 4611
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1, p2}, Lcom/twitter/library/api/z;->a(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)Lcom/twitter/model/core/ac;

    move-result-object v3

    goto :goto_1

    .line 4612
    :cond_1
    const-string/jumbo v5, "sort_index"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 4613
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 4618
    :pswitch_2
    const-string/jumbo v4, "is_suggestion"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    .line 4619
    goto :goto_1

    .line 4625
    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 4635
    :cond_2
    if-eqz v3, :cond_4

    .line 4636
    invoke-virtual {p3, v3}, Lcom/twitter/model/timeline/ah$a;->a(Lcom/twitter/model/core/ac;)Lcom/twitter/model/timeline/ah$a;

    move-result-object v4

    iget-wide v6, v3, Lcom/twitter/model/core/ac;->M:J

    .line 4637
    invoke-virtual {v4, v6, v7}, Lcom/twitter/model/timeline/ah$a;->b(J)Lcom/twitter/model/timeline/y$a;

    .line 4638
    if-eqz v2, :cond_3

    .line 4639
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p3, v2, v3}, Lcom/twitter/model/timeline/ah$a;->a(J)Lcom/twitter/model/timeline/y$a;

    .line 4641
    :cond_3
    if-eqz v0, :cond_4

    .line 4642
    invoke-virtual {p3, v1}, Lcom/twitter/model/timeline/ah$a;->a(I)Lcom/twitter/model/timeline/y$a;

    .line 4645
    :cond_4
    return-object p3

    .line 4607
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/model/timeline/al$a;)Lcom/twitter/model/timeline/y$a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/ac;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;",
            "Lcom/twitter/model/timeline/al$a;",
            ")",
            "Lcom/twitter/model/timeline/y$a;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4586
    const-class v0, Lcom/twitter/model/json/timeline/JsonTweetCarousel;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/timeline/JsonTweetCarousel;

    .line 4587
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 4588
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/twitter/model/json/timeline/JsonTweetCarousel;->b:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 4589
    iget-object v1, v0, Lcom/twitter/model/json/timeline/JsonTweetCarousel;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 4590
    invoke-static {v1, p1, p2}, Lcom/twitter/library/api/z;->a(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)Lcom/twitter/model/core/ac;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 4592
    :cond_0
    new-instance v1, Lcom/twitter/model/timeline/e;

    iget-object v3, v0, Lcom/twitter/model/json/timeline/JsonTweetCarousel;->a:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {v1, v3, v0}, Lcom/twitter/model/timeline/e;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {p3, v1}, Lcom/twitter/model/timeline/al$a;->a(Lcom/twitter/model/timeline/e;)Lcom/twitter/model/timeline/al$a;

    .line 4595
    :cond_1
    return-object p3
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/model/timeline/ao$a;)Lcom/twitter/model/timeline/y$a;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/ac;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;",
            "Lcom/twitter/model/timeline/ao$a;",
            ")",
            "Lcom/twitter/model/timeline/y$a;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4436
    const-class v2, Lcom/twitter/model/json/timeline/wtf/JsonWhoToFollow;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/json/timeline/wtf/JsonWhoToFollow;

    .line 4437
    if-nez v2, :cond_1

    .line 4514
    :cond_0
    :goto_0
    return-object p3

    .line 4440
    :cond_1
    iget-object v3, v2, Lcom/twitter/model/json/timeline/wtf/JsonWhoToFollow;->c:Ljava/lang/String;

    invoke-static {v3}, Lcom/twitter/library/api/z;->b(Ljava/lang/String;)I

    move-result v8

    .line 4441
    iget-object v4, v2, Lcom/twitter/model/json/timeline/wtf/JsonWhoToFollow;->f:Lcom/twitter/model/json/timeline/JsonSuggestsInfo;

    .line 4443
    const/4 v3, -0x1

    if-eq v8, v3, :cond_0

    .line 4444
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v9

    .line 4445
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v10

    .line 4446
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v11

    .line 4447
    iget-object v3, v2, Lcom/twitter/model/json/timeline/wtf/JsonWhoToFollow;->d:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    .line 4448
    iget-object v3, v2, Lcom/twitter/model/json/timeline/wtf/JsonWhoToFollow;->b:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v12

    .line 4449
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v13

    .line 4450
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v14

    .line 4453
    if-eqz v4, :cond_2

    iget-object v3, v4, Lcom/twitter/model/json/timeline/JsonSuggestsInfo;->a:Lcom/twitter/model/timeline/r;

    move-object v7, v3

    .line 4454
    :goto_1
    if-eqz v4, :cond_3

    iget-object v3, v4, Lcom/twitter/model/json/timeline/JsonSuggestsInfo;->c:Ljava/util/Map;

    .line 4455
    invoke-static {v3}, Lcom/twitter/util/collection/i;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    move-object v5, v3

    .line 4458
    :goto_2
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 4459
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/json/timeline/wtf/JsonSocialProof;

    move-object/from16 v0, p2

    invoke-static {v4, v0}, Lcom/twitter/library/api/z;->a(Lcom/twitter/model/json/timeline/wtf/JsonSocialProof;Ljava/util/Map;)Lcom/twitter/model/core/TwitterSocialProof;

    move-result-object v4

    .line 4460
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v11, v3, v4}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    goto :goto_3

    .line 4453
    :cond_2
    const/4 v3, 0x0

    move-object v7, v3

    goto :goto_1

    .line 4455
    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v3

    move-object v5, v3

    goto :goto_2

    .line 4464
    :cond_4
    iget-object v15, v2, Lcom/twitter/model/json/timeline/wtf/JsonWhoToFollow;->a:[Ljava/lang/String;

    array-length v0, v15

    move/from16 v16, v0

    const/4 v3, 0x0

    move v6, v3

    :goto_4
    move/from16 v0, v16

    if-ge v6, v0, :cond_5

    aget-object v3, v15, v6

    .line 4465
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/model/core/TwitterUser;

    .line 4466
    invoke-virtual {v3}, Lcom/twitter/model/core/TwitterUser;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/timeline/r;

    iput-object v4, v3, Lcom/twitter/model/core/TwitterUser;->V:Lcom/twitter/model/timeline/r;

    .line 4467
    invoke-virtual {v9, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 4468
    iget-wide v0, v3, Lcom/twitter/model/core/TwitterUser;->b:J

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v13, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 4464
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_4

    .line 4472
    :cond_5
    invoke-interface {v12}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 4473
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v3, v0, v1}, Lcom/twitter/library/api/z;->a(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)Lcom/twitter/model/core/ac;

    move-result-object v3

    .line 4474
    invoke-virtual {v10, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 4475
    invoke-virtual {v3}, Lcom/twitter/model/core/ac;->a()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v14, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_5

    .line 4478
    :cond_6
    new-instance v3, Lcom/twitter/model/timeline/at$a$a;

    invoke-direct {v3}, Lcom/twitter/model/timeline/at$a$a;-><init>()V

    .line 4479
    invoke-virtual {v3, v8}, Lcom/twitter/model/timeline/at$a$a;->a(I)Lcom/twitter/model/timeline/at$a$a;

    move-result-object v4

    .line 4480
    invoke-virtual {v11}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    invoke-virtual {v4, v3}, Lcom/twitter/model/timeline/at$a$a;->a(Ljava/util/Map;)Lcom/twitter/model/timeline/at$a$a;

    move-result-object v3

    .line 4481
    invoke-virtual {v3, v7}, Lcom/twitter/model/timeline/at$a$a;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/timeline/at$a$a;

    move-result-object v3

    .line 4482
    invoke-virtual {v3, v5}, Lcom/twitter/model/timeline/at$a$a;->b(Ljava/util/Map;)Lcom/twitter/model/timeline/at$a$a;

    move-result-object v5

    .line 4483
    invoke-virtual {v14}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-virtual {v13}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-virtual {v5, v3, v4}, Lcom/twitter/model/timeline/at$a$a;->a(Ljava/util/List;Ljava/util/List;)Lcom/twitter/model/timeline/at$a$a;

    move-result-object v5

    .line 4485
    iget-object v3, v2, Lcom/twitter/model/json/timeline/wtf/JsonWhoToFollow;->e:Lcom/twitter/model/json/timeline/JsonStrings;

    .line 4486
    if-eqz v3, :cond_7

    .line 4487
    iget-object v4, v3, Lcom/twitter/model/json/timeline/JsonStrings;->a:Ljava/lang/String;

    invoke-virtual {v5, v4}, Lcom/twitter/model/timeline/at$a$a;->a(Ljava/lang/String;)Lcom/twitter/model/timeline/at$a$a;

    move-result-object v4

    iget-object v3, v3, Lcom/twitter/model/json/timeline/JsonStrings;->b:Ljava/lang/String;

    .line 4488
    invoke-virtual {v4, v3}, Lcom/twitter/model/timeline/at$a$a;->b(Ljava/lang/String;)Lcom/twitter/model/timeline/at$a$a;

    .line 4490
    :cond_7
    iget-object v3, v2, Lcom/twitter/model/json/timeline/wtf/JsonWhoToFollow;->i:Lcom/twitter/model/json/timeline/JsonHeaderContext;

    if-eqz v3, :cond_9

    .line 4491
    iget-object v3, v2, Lcom/twitter/model/json/timeline/wtf/JsonWhoToFollow;->i:Lcom/twitter/model/json/timeline/JsonHeaderContext;

    iget-object v3, v3, Lcom/twitter/model/json/timeline/JsonHeaderContext;->a:Ljava/util/List;

    .line 4492
    if-eqz v3, :cond_9

    .line 4493
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v4

    .line 4494
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/model/json/timeline/wtf/JsonHeaderAvatar;

    .line 4495
    new-instance v7, Lcom/twitter/model/timeline/i$a;

    invoke-direct {v7}, Lcom/twitter/model/timeline/i$a;-><init>()V

    iget-object v3, v3, Lcom/twitter/model/json/timeline/wtf/JsonHeaderAvatar;->a:Ljava/lang/String;

    .line 4496
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v7, v3}, Lcom/twitter/model/timeline/i$a;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/timeline/i$a;

    move-result-object v3

    .line 4497
    invoke-virtual {v3}, Lcom/twitter/model/timeline/i$a;->q()Ljava/lang/Object;

    move-result-object v3

    .line 4495
    invoke-virtual {v4, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_6

    .line 4499
    :cond_8
    new-instance v6, Lcom/twitter/model/timeline/j$a;

    invoke-direct {v6}, Lcom/twitter/model/timeline/j$a;-><init>()V

    .line 4500
    invoke-virtual {v4}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-virtual {v6, v3}, Lcom/twitter/model/timeline/j$a;->a(Ljava/util/List;)Lcom/twitter/model/timeline/j$a;

    move-result-object v3

    .line 4501
    invoke-virtual {v3}, Lcom/twitter/model/timeline/j$a;->q()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/model/timeline/j;

    .line 4499
    invoke-virtual {v5, v3}, Lcom/twitter/model/timeline/at$a$a;->a(Lcom/twitter/model/timeline/j;)Lcom/twitter/model/timeline/at$a$a;

    .line 4505
    :cond_9
    new-instance v6, Lcom/twitter/model/timeline/at;

    invoke-virtual {v9}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-virtual {v10}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-virtual {v5}, Lcom/twitter/model/timeline/at$a$a;->q()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/model/timeline/at$a;

    invoke-direct {v6, v3, v4, v5}, Lcom/twitter/model/timeline/at;-><init>(Ljava/util/List;Ljava/util/List;Lcom/twitter/model/timeline/at$a;)V

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Lcom/twitter/model/timeline/ao$a;->a(Lcom/twitter/model/timeline/at;)Lcom/twitter/model/timeline/ao$a;

    .line 4507
    iget-object v3, v2, Lcom/twitter/model/json/timeline/wtf/JsonWhoToFollow;->g:Lcom/twitter/model/timeline/k;

    .line 4508
    if-eqz v3, :cond_a

    .line 4509
    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lcom/twitter/model/timeline/ao$a;->a(Lcom/twitter/model/timeline/k;)Lcom/twitter/model/timeline/y$a;

    .line 4512
    :cond_a
    iget-object v2, v2, Lcom/twitter/model/json/timeline/wtf/JsonWhoToFollow;->h:Lcom/twitter/model/timeline/m;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/twitter/model/timeline/ao$a;->a(Lcom/twitter/model/timeline/m;)Lcom/twitter/model/timeline/ao$a;

    goto/16 :goto_0
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/model/timeline/w$a;)Lcom/twitter/model/timeline/y$a;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/ac;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;",
            "Lcom/twitter/model/timeline/w$a;",
            ")",
            "Lcom/twitter/model/timeline/y$a;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4782
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 4783
    const/4 v1, 0x0

    .line 4784
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 4785
    :goto_0
    if-eqz v0, :cond_4

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v3, :cond_4

    .line 4786
    sget-object v3, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move-object v0, v1

    .line 4814
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_0

    .line 4788
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 4789
    const-string/jumbo v3, "context"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4790
    invoke-static {p0, p2}, Lcom/twitter/library/api/z;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;)Lcom/twitter/model/timeline/f$a;

    move-result-object v0

    goto :goto_1

    .line 4792
    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    .line 4794
    goto :goto_1

    .line 4797
    :pswitch_2
    const-string/jumbo v0, "ids"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4798
    invoke-static {p0}, Lcom/twitter/library/api/z;->am(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/LinkedHashSet;

    move-result-object v0

    .line 4799
    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 4800
    invoke-static {v0, p1, p2}, Lcom/twitter/library/api/z;->a(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)Lcom/twitter/model/core/ac;

    move-result-object v0

    .line 4801
    if-eqz v0, :cond_1

    .line 4802
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    move-object v0, v1

    .line 4805
    goto :goto_1

    .line 4806
    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    .line 4808
    goto :goto_1

    .line 4816
    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 4817
    new-instance v0, Lcom/twitter/model/timeline/f;

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/timeline/f;-><init>(Lcom/twitter/model/timeline/f$a;Ljava/util/List;)V

    .line 4818
    invoke-virtual {p3, v0}, Lcom/twitter/model/timeline/w$a;->a(Lcom/twitter/model/timeline/f;)Lcom/twitter/model/timeline/w$a;

    move-result-object v1

    .line 4819
    invoke-virtual {v0}, Lcom/twitter/model/timeline/f;->a()Lcom/twitter/model/core/ac;

    move-result-object v0

    iget-wide v2, v0, Lcom/twitter/model/core/ac;->M:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/model/timeline/w$a;->b(J)Lcom/twitter/model/timeline/y$a;

    .line 4821
    :cond_5
    return-object p3

    .line 4786
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/ac;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 337
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 338
    if-eqz p0, :cond_1

    .line 339
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 340
    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    .line 341
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 359
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 344
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    .line 343
    invoke-static {v0, p1, p2}, Lcom/twitter/library/api/z;->a(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)Lcom/twitter/model/core/ac;

    move-result-object v0

    .line 345
    if-eqz v0, :cond_0

    .line 346
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 352
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 362
    :cond_1
    return-object v1

    .line 341
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x2

    const/4 v6, 0x0

    .line 3780
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 3782
    :try_start_0
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 3783
    invoke-virtual {v8}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v9

    .line 3784
    const/4 v4, 0x0

    .line 3785
    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 3786
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 3787
    const-string/jumbo v1, "errors"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "warnings"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3788
    :cond_1
    invoke-virtual {v8, v2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lorg/json/JSONArray;

    move-object v3, v0

    move v5, v6

    .line 3789
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v5, v1, :cond_0

    .line 3790
    invoke-virtual {v3, v5}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONObject;

    .line 3791
    const-string/jumbo v4, "eventName"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3792
    new-instance v10, Landroid/util/Pair;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x2

    .line 3793
    invoke-virtual {v1, v12}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v10, v11, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3792
    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3789
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_0

    .line 3795
    :cond_2
    const-string/jumbo v1, "logs"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3796
    invoke-virtual {v8, v2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lorg/json/JSONArray;

    move-object v3, v0

    move v5, v6

    .line 3797
    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v5, v1, :cond_0

    .line 3798
    invoke-virtual {v3, v5}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONArray;

    .line 3799
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lt v2, v13, :cond_4

    .line 3800
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/json/JSONObject;

    .line 3801
    const/4 v10, 0x1

    invoke-virtual {v1, v10}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 3802
    const-string/jumbo v10, "client_event"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 3803
    const-string/jumbo v4, "eventName"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3810
    :cond_3
    :goto_2
    new-instance v10, Landroid/util/Pair;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v11, "]"

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v11, 0x2

    .line 3811
    invoke-virtual {v2, v11}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v10, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3810
    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    move-object v1, v4

    .line 3797
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move-object v4, v1

    goto :goto_1

    .line 3804
    :cond_5
    const-string/jumbo v10, "perftown"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 3805
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "product"

    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v10, ":"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v10, "description"

    .line 3806
    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 3807
    :cond_6
    const-string/jumbo v10, "client_watch_error"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 3808
    const-string/jumbo v4, "error"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_2

    .line 3816
    :catch_0
    move-exception v1

    .line 3818
    :cond_7
    return-object v7
.end method

.method private static a(Ljava/util/HashMap;Ljava/util/HashMap;Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/ac;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/topic/TwitterTopic;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3893
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 3894
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 3895
    :goto_0
    if-eqz v0, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_2

    .line 3896
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_1

    .line 3897
    invoke-static {p2, p0, p1}, Lcom/twitter/library/api/z;->b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;)Lcom/twitter/model/topic/TwitterTopic;

    move-result-object v0

    .line 3898
    if-eqz v0, :cond_0

    .line 3899
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3904
    :cond_0
    :goto_1
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 3901
    :cond_1
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    .line 3902
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 3906
    :cond_2
    return-object v1
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/Map;Z)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/ac;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/topic/TwitterTopic;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/Moment;",
            ">;Z)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/y;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3941
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    if-eqz p3, :cond_1

    .line 3942
    invoke-virtual {p3}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3943
    :cond_1
    new-instance v0, Lcom/twitter/model/json/common/InvalidJsonFormatException;

    const-string/jumbo v1, "Missing tweets map or users map"

    invoke-direct {v0, v1}, Lcom/twitter/model/json/common/InvalidJsonFormatException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 3944
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    .line 3976
    :goto_0
    return-object v0

    .line 3946
    :cond_2
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v4

    .line 3947
    const/4 v1, 0x0

    .line 3948
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 3949
    :goto_1
    if-eqz v0, :cond_c

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_c

    .line 3950
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_b

    .line 3951
    invoke-static/range {p0 .. p5}, Lcom/twitter/library/api/z;->b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/Map;Z)Lcom/twitter/model/timeline/y;

    move-result-object v0

    .line 3953
    if-eqz v0, :cond_d

    .line 3954
    instance-of v2, v0, Lcom/twitter/model/timeline/ag;

    if-nez v2, :cond_3

    instance-of v2, v0, Lcom/twitter/model/timeline/al;

    if-eqz v2, :cond_7

    .line 3959
    :cond_3
    if-eqz v1, :cond_6

    iget-wide v2, v1, Lcom/twitter/model/timeline/y;->q:J

    .line 3960
    :goto_2
    iput-wide v2, v0, Lcom/twitter/model/timeline/y;->q:J

    .line 3968
    :cond_4
    :goto_3
    invoke-virtual {v4, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    :goto_4
    move-object v1, v0

    .line 3974
    :cond_5
    :goto_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_1

    .line 3960
    :cond_6
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    goto :goto_2

    .line 3961
    :cond_7
    if-eqz v1, :cond_9

    invoke-static {v0}, Lcom/twitter/model/timeline/y;->e(Lcom/twitter/model/timeline/y;)Lcgi;

    move-result-object v2

    if-nez v2, :cond_8

    instance-of v2, v0, Lcom/twitter/model/timeline/t;

    if-eqz v2, :cond_9

    .line 3963
    :cond_8
    iget-wide v2, v1, Lcom/twitter/model/timeline/y;->q:J

    iput-wide v2, v0, Lcom/twitter/model/timeline/y;->q:J

    goto :goto_3

    .line 3964
    :cond_9
    iget v2, v0, Lcom/twitter/model/timeline/y;->f:I

    if-eqz v2, :cond_4

    .line 3965
    if-eqz v1, :cond_a

    iget-wide v2, v1, Lcom/twitter/model/timeline/y;->q:J

    :goto_6
    iput-wide v2, v0, Lcom/twitter/model/timeline/y;->q:J

    goto :goto_3

    :cond_a
    const-wide/16 v2, -0x1

    goto :goto_6

    .line 3971
    :cond_b
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_5

    .line 3972
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_5

    .line 3976
    :cond_c
    invoke-virtual {v4}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    goto :goto_0

    :cond_d
    move-object v0, v1

    goto :goto_4
.end method

.method private static a(JIIJ)V
    .locals 4

    .prologue
    .line 4918
    new-instance v0, Lcpb;

    invoke-direct {v0}, Lcpb;-><init>()V

    .line 4919
    const-string/jumbo v1, "Missing user tweetId"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 4920
    const-string/jumbo v1, "Missing user usersMap size"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 4921
    const-string/jumbo v1, "Missing user tweetsMap size"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 4922
    new-instance v1, Lcom/twitter/model/json/common/InvalidJsonFormatException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Missing user "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/model/json/common/InvalidJsonFormatException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    .line 4924
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    .line 4925
    return-void
.end method

.method private static a(Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/model/core/ac;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/ac;",
            ">;",
            "Lcom/twitter/model/core/ac;",
            ")V"
        }
    .end annotation

    .prologue
    .line 4894
    invoke-static {p2}, Lcom/twitter/model/util/e;->a(Lcom/twitter/model/core/ac;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4895
    invoke-static {p2}, Lcom/twitter/model/util/e;->d(Lcom/twitter/model/core/ac;)Ljava/util/List;

    move-result-object v0

    .line 4896
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcba;

    .line 4897
    invoke-virtual {v0}, Lcba;->b()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    move-object v4, v0

    .line 4898
    check-cast v4, Lcbb;

    .line 4899
    iget-object v0, v4, Lcbb;->a:Lcom/twitter/model/core/ac;

    iget-object v0, v0, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    .line 4900
    iget-object v0, v4, Lcbb;->a:Lcom/twitter/model/core/ac;

    iget-object v0, v0, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 4901
    if-eqz v0, :cond_1

    .line 4902
    iget-object v1, v4, Lcbb;->a:Lcom/twitter/model/core/ac;

    iput-object v0, v1, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    .line 4906
    :cond_1
    iget-object v0, v4, Lcbb;->a:Lcom/twitter/model/core/ac;

    iget-object v0, v0, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v4, Lcbb;->a:Lcom/twitter/model/core/ac;

    iget-object v0, v0, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    .line 4907
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4908
    iget-object v0, v4, Lcbb;->a:Lcom/twitter/model/core/ac;

    iget-wide v0, v0, Lcom/twitter/model/core/ac;->a:J

    .line 4909
    invoke-virtual {p0}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-virtual {p1}, Ljava/util/HashMap;->size()I

    move-result v3

    iget-object v4, v4, Lcbb;->a:Lcom/twitter/model/core/ac;

    iget-object v4, v4, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v4, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 4908
    invoke-static/range {v0 .. v5}, Lcom/twitter/library/api/z;->a(JIIJ)V

    goto :goto_0

    .line 4915
    :cond_2
    return-void
.end method

.method private static a(Lcom/twitter/model/core/ac;)Z
    .locals 1

    .prologue
    .line 1788
    iget-object v0, p0, Lcom/twitter/model/core/ac;->d:Lcom/twitter/model/core/v;

    iget-object v0, v0, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-virtual {v0}, Lcom/twitter/model/core/k;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/twitter/model/core/ac;Ljava/util/HashMap;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/ac;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 4826
    iget-object v1, p0, Lcom/twitter/model/core/ac;->I:Lcom/twitter/model/core/ac;

    .line 4827
    if-eqz v1, :cond_0

    iget-object v0, v1, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    .line 4828
    iget-object v0, v1, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 4829
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 4830
    if-eqz v0, :cond_1

    .line 4831
    iput-object v0, v1, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    .line 4837
    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 4833
    :cond_1
    new-instance v0, Lcom/twitter/model/json/common/InvalidJsonFormatException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Missing user "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, v1, Lcom/twitter/model/core/ac;->F:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/model/json/common/InvalidJsonFormatException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 4834
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static aa(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/search/TwitterTypeAhead;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 2770
    new-instance v10, Lcom/twitter/model/core/TwitterUser$a;

    invoke-direct {v10}, Lcom/twitter/model/core/TwitterUser$a;-><init>()V

    .line 2775
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 2776
    const/16 v8, 0x400

    move-object v7, v2

    move v6, v4

    move v9, v4

    .line 2777
    :goto_0
    if-eqz v0, :cond_c

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_c

    .line 2778
    sget-object v1, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :pswitch_0
    move v0, v8

    move v1, v9

    .line 2848
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    move v8, v0

    move v9, v1

    move-object v0, v3

    goto :goto_0

    .line 2780
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 2781
    const-string/jumbo v1, "name"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2782
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Lcom/twitter/model/core/TwitterUser$a;->b(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move v0, v8

    move v1, v9

    goto :goto_1

    .line 2783
    :cond_1
    const-string/jumbo v1, "screen_name"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2784
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Lcom/twitter/model/core/TwitterUser$a;->g(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move v0, v8

    move v1, v9

    goto :goto_1

    .line 2785
    :cond_2
    const-string/jumbo v1, "profile_image_url_https"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2786
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Lcom/twitter/model/core/TwitterUser$a;->c(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move v0, v8

    move v1, v9

    goto :goto_1

    .line 2787
    :cond_3
    const-string/jumbo v1, "location"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2788
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Lcom/twitter/model/core/TwitterUser$a;->h(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move v0, v8

    move v1, v9

    goto :goto_1

    .line 2789
    :cond_4
    const-string/jumbo v1, "customer_service_state"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2790
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/businessprofiles/h$a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move v0, v8

    move v1, v9

    goto :goto_1

    .line 2795
    :pswitch_2
    const-string/jumbo v0, "verified"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2796
    invoke-virtual {v10, v11}, Lcom/twitter/model/core/TwitterUser$a;->c(Z)Lcom/twitter/model/core/TwitterUser$a;

    move v0, v8

    move v1, v9

    goto/16 :goto_1

    .line 2797
    :cond_5
    const-string/jumbo v0, "is_translator"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2798
    invoke-virtual {v10, v11}, Lcom/twitter/model/core/TwitterUser$a;->d(Z)Lcom/twitter/model/core/TwitterUser$a;

    move v0, v8

    move v1, v9

    goto/16 :goto_1

    .line 2799
    :cond_6
    const-string/jumbo v0, "is_lifeline_institution"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2800
    invoke-virtual {v10, v11}, Lcom/twitter/model/core/TwitterUser$a;->e(Z)Lcom/twitter/model/core/TwitterUser$a;

    move v0, v8

    move v1, v9

    goto/16 :goto_1

    .line 2801
    :cond_7
    const-string/jumbo v0, "is_dm_able"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2802
    or-int/lit8 v0, v8, 0x8

    move v1, v9

    goto/16 :goto_1

    .line 2807
    :pswitch_3
    const-string/jumbo v0, "can_media_tag"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2808
    and-int/lit16 v0, v8, -0x401

    move v1, v9

    goto/16 :goto_1

    .line 2813
    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 2814
    const-string/jumbo v1, "id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2815
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->j()J

    move-result-wide v0

    invoke-virtual {v10, v0, v1}, Lcom/twitter/model/core/TwitterUser$a;->a(J)Lcom/twitter/model/core/TwitterUser$a;

    move v0, v8

    move v1, v9

    goto/16 :goto_1

    .line 2816
    :cond_8
    const-string/jumbo v1, "rounded_score"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2817
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()I

    move-result v0

    move v1, v0

    move v0, v8

    goto/16 :goto_1

    .line 2818
    :cond_9
    const-string/jumbo v1, "rounded_graph_weight"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2819
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()I

    move-result v6

    move v0, v8

    move v1, v9

    goto/16 :goto_1

    .line 2824
    :pswitch_5
    const-string/jumbo v0, "tokens"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2825
    invoke-static {p0}, Lcom/twitter/library/api/z;->ae(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v7

    move v0, v8

    move v1, v9

    goto/16 :goto_1

    .line 2827
    :cond_a
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move v0, v8

    move v1, v9

    .line 2829
    goto/16 :goto_1

    .line 2832
    :pswitch_6
    const-string/jumbo v0, "social_context"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2833
    const-class v0, Lcom/twitter/model/json/search/JsonSearchSocialProof;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/TwitterSocialProof;

    .line 2835
    if-eqz v1, :cond_e

    .line 2836
    new-instance v0, Lcom/twitter/model/search/TwitterUserMetadata;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/model/search/TwitterUserMetadata;-><init>(Lcom/twitter/model/core/TwitterSocialProof;Ljava/lang/String;Ljava/lang/String;ZLcom/twitter/model/core/s;)V

    invoke-virtual {v10, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/search/TwitterUserMetadata;)Lcom/twitter/model/core/TwitterUser$a;

    .line 2837
    iget v0, v1, Lcom/twitter/model/core/TwitterSocialProof;->g:I

    or-int/2addr v0, v8

    :goto_2
    move v1, v9

    .line 2839
    goto/16 :goto_1

    .line 2840
    :cond_b
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move v0, v8

    move v1, v9

    .line 2842
    goto/16 :goto_1

    .line 2850
    :cond_c
    if-nez v7, :cond_d

    .line 2856
    :goto_3
    return-object v2

    .line 2854
    :cond_d
    invoke-virtual {v10, v8}, Lcom/twitter/model/core/TwitterUser$a;->i(I)Lcom/twitter/model/core/TwitterUser$a;

    .line 2856
    new-instance v3, Lcom/twitter/library/api/search/TwitterTypeAhead;

    .line 2857
    invoke-virtual {v10}, Lcom/twitter/model/core/TwitterUser$a;->q()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/model/core/TwitterUser;

    move v4, v11

    move v5, v9

    move-object v9, v2

    move-object v10, v2

    invoke-direct/range {v3 .. v10}, Lcom/twitter/library/api/search/TwitterTypeAhead;-><init>(IIILjava/util/ArrayList;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/search/b;Ljava/lang/String;)V

    move-object v2, v3

    .line 2856
    goto :goto_3

    :cond_e
    move v0, v8

    goto :goto_2

    .line 2778
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_1
        :pswitch_0
        :pswitch_6
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method private static ab(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/api/search/TwitterTypeAhead;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2861
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    .line 2862
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2863
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 2864
    :goto_0
    if-eqz v0, :cond_1

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v4, :cond_1

    .line 2865
    sget-object v4, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    .line 2881
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 2867
    :pswitch_1
    invoke-static {p0, v2, v3}, Lcom/twitter/library/api/z;->a(Lcom/fasterxml/jackson/core/JsonParser;J)Lcom/twitter/library/api/search/TwitterTypeAhead;

    move-result-object v0

    .line 2868
    if-eqz v0, :cond_0

    .line 2869
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2874
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 2883
    :cond_1
    return-object v1

    .line 2865
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static ac(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/library/api/search/TwitterTypeAhead;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2961
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2962
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 2963
    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    .line 2964
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 2980
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 2966
    :pswitch_1
    invoke-static {p0}, Lcom/twitter/library/api/z;->ad(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/search/TwitterTypeAhead;

    move-result-object v0

    .line 2967
    if-eqz v0, :cond_0

    .line 2968
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2973
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 2982
    :cond_1
    return-object v1

    .line 2964
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static ad(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/search/TwitterTypeAhead;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 2987
    .line 2989
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move v2, v3

    move-object v7, v4

    .line 2990
    :goto_0
    if-eqz v0, :cond_1

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_1

    .line 2991
    sget-object v1, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 3014
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 2993
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 2994
    const-string/jumbo v1, "hashtag"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2995
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 3000
    :pswitch_2
    const-string/jumbo v0, "rounded_score"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3001
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()I

    move-result v2

    goto :goto_1

    .line 3007
    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 3016
    :cond_1
    if-nez v7, :cond_2

    .line 3019
    :goto_2
    return-object v4

    :cond_2
    new-instance v0, Lcom/twitter/library/api/search/TwitterTypeAhead;

    const/4 v1, 0x2

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/api/search/TwitterTypeAhead;-><init>(IIILjava/util/ArrayList;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/search/b;Ljava/lang/String;)V

    move-object v4, v0

    goto :goto_2

    .line 2991
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static ae(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3025
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3027
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 3028
    :goto_0
    if-eqz v0, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_2

    .line 3029
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_1

    .line 3030
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 3031
    :goto_1
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    .line 3032
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->h:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    const-string/jumbo v0, "token"

    .line 3033
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3034
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3036
    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_1

    .line 3039
    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 3042
    :cond_2
    return-object v1
.end method

.method private static af(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/v;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 3188
    new-instance v3, Lcom/twitter/library/api/v$a;

    invoke-direct {v3}, Lcom/twitter/library/api/v$a;-><init>()V

    .line 3191
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v0

    move-object v0, v1

    .line 3192
    :goto_0
    if-eqz v2, :cond_b

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v4, :cond_b

    .line 3193
    sget-object v4, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    .line 3292
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    .line 3195
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    .line 3196
    const-string/jumbo v4, "name"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 3197
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/twitter/library/api/v$a;->a(Ljava/lang/String;)Lcom/twitter/library/api/v$a;

    goto :goto_1

    .line 3198
    :cond_1
    const-string/jumbo v4, "key"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 3199
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 3200
    :cond_2
    const-string/jumbo v4, "banner"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3201
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/twitter/library/api/v$a;->b(Ljava/lang/String;)Lcom/twitter/library/api/v$a;

    goto :goto_1

    .line 3206
    :pswitch_2
    const-string/jumbo v2, "zero_rate"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3207
    invoke-virtual {v3, v6}, Lcom/twitter/library/api/v$a;->a(Z)Lcom/twitter/library/api/v$a;

    goto :goto_1

    .line 3212
    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    .line 3213
    const-string/jumbo v4, "expire_seconds"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3214
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->j()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/api/v$a;->a(J)Lcom/twitter/library/api/v$a;

    goto :goto_1

    .line 3219
    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    .line 3220
    const-string/jumbo v4, "host_map"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 3221
    invoke-static {p0}, Lcom/twitter/library/api/z;->ag(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/twitter/library/api/v$a;->a(Ljava/util/Map;)Lcom/twitter/library/api/v$a;

    goto :goto_1

    .line 3222
    :cond_3
    const-string/jumbo v4, "display_flags"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 3223
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    .line 3224
    :goto_2
    if-eqz v2, :cond_0

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v4, :cond_0

    .line 3225
    sget-object v4, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_1

    .line 3255
    :cond_4
    :goto_3
    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_2

    .line 3227
    :pswitch_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    .line 3228
    const-string/jumbo v4, "inline_media_interstitial"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 3229
    invoke-virtual {v3, v6}, Lcom/twitter/library/api/v$a;->b(Z)Lcom/twitter/library/api/v$a;

    goto :goto_3

    .line 3230
    :cond_5
    const-string/jumbo v4, "external_links_interstitial"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 3231
    invoke-virtual {v3, v6}, Lcom/twitter/library/api/v$a;->c(Z)Lcom/twitter/library/api/v$a;

    goto :goto_3

    .line 3232
    :cond_6
    const-string/jumbo v4, "footer_text"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 3233
    invoke-virtual {v3, v6}, Lcom/twitter/library/api/v$a;->d(Z)Lcom/twitter/library/api/v$a;

    goto :goto_3

    .line 3234
    :cond_7
    const-string/jumbo v4, "banner_message"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3235
    invoke-virtual {v3, v6}, Lcom/twitter/library/api/v$a;->e(Z)Lcom/twitter/library/api/v$a;

    goto :goto_3

    .line 3240
    :pswitch_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    .line 3241
    const-string/jumbo v4, "zero_rate_videos"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3242
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Lcom/twitter/library/api/v$a;->f(Z)Lcom/twitter/library/api/v$a;

    goto :goto_3

    .line 3248
    :pswitch_8
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    .line 3257
    :cond_8
    const-string/jumbo v4, "interstitial"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 3258
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    .line 3259
    :goto_4
    if-eqz v2, :cond_0

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v4, :cond_0

    .line 3260
    sget-object v4, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_2

    .line 3277
    :cond_9
    :goto_5
    :pswitch_9
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_4

    .line 3262
    :pswitch_a
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    .line 3263
    const-string/jumbo v4, "text"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 3264
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/twitter/library/api/v$a;->c(Ljava/lang/String;)Lcom/twitter/library/api/v$a;

    goto :goto_5

    .line 3270
    :pswitch_b
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_5

    .line 3280
    :cond_a
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    .line 3285
    :pswitch_c
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    .line 3295
    :cond_b
    if-eqz v0, :cond_c

    invoke-virtual {v3, v0}, Lcom/twitter/library/api/v$a;->d(Ljava/lang/String;)Lcom/twitter/library/api/v;

    move-result-object v0

    :goto_6
    return-object v0

    :cond_c
    move-object v0, v1

    goto :goto_6

    .line 3193
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_c
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 3225
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
        :pswitch_5
        :pswitch_5
        :pswitch_8
        :pswitch_6
        :pswitch_5
        :pswitch_7
    .end packed-switch

    .line 3260
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_b
    .end packed-switch
.end method

.method private static ag(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3300
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 3301
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 3302
    :goto_0
    if-eqz v0, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_2

    .line 3303
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 3336
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 3305
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    .line 3306
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 3307
    :goto_2
    if-eqz v0, :cond_0

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v3, :cond_0

    .line 3308
    sget-object v3, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_1

    .line 3324
    :cond_1
    :goto_3
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_2

    .line 3310
    :pswitch_3
    const-string/jumbo v0, "host"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3311
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 3317
    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    .line 3329
    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 3338
    :cond_2
    return-object v1

    .line 3303
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 3308
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method private static ah(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 3530
    const-class v0, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;

    .line 3531
    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, v0, Lcom/twitter/model/json/livevideo/JsonLiveVideoEvent;->a:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static ai(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/topic/TwitterTopic;
    .locals 30
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3671
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v13

    .line 3672
    const/4 v12, 0x0

    .line 3673
    const/16 v26, 0x0

    .line 3674
    const/16 v22, 0x0

    .line 3675
    const/4 v11, 0x0

    .line 3676
    const/16 v25, 0x0

    .line 3677
    const/4 v6, 0x0

    .line 3678
    const/4 v4, -0x1

    .line 3679
    const/4 v5, -0x1

    .line 3680
    const-wide/16 v8, 0x0

    .line 3682
    const/4 v7, 0x0

    .line 3683
    const/4 v10, 0x4

    .line 3684
    const/16 v23, 0x0

    .line 3685
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    .line 3686
    :goto_0
    if-eqz v2, :cond_f

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_f

    .line 3687
    sget-object v3, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :pswitch_0
    move v2, v10

    move v3, v5

    move-object/from16 v10, v26

    move/from16 v27, v4

    move-wide v4, v8

    move-object v8, v6

    move-object/from16 v9, v22

    move/from16 v6, v27

    .line 3755
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v14

    move-object/from16 v22, v9

    move-object/from16 v26, v10

    move v10, v2

    move-object v2, v14

    move/from16 v27, v6

    move-object v6, v8

    move-wide v8, v4

    move v5, v3

    move/from16 v4, v27

    goto :goto_0

    .line 3689
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    .line 3690
    const-string/jumbo v3, "name"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3691
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    move v3, v5

    move-wide/from16 v27, v8

    move-object v8, v6

    move-object/from16 v9, v22

    move v6, v4

    move-wide/from16 v4, v27

    move/from16 v29, v10

    move-object v10, v2

    move/from16 v2, v29

    goto :goto_1

    .line 3692
    :cond_1
    const-string/jumbo v3, "description"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 3693
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    move v3, v5

    move-wide/from16 v27, v8

    move-object v8, v6

    move-object v9, v2

    move v6, v4

    move v2, v10

    move-wide/from16 v4, v27

    move-object/from16 v10, v26

    goto :goto_1

    .line 3694
    :cond_2
    const-string/jumbo v3, "user_id"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 3695
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    move v3, v5

    move v6, v4

    move-wide v4, v8

    move-object v8, v2

    move-object/from16 v9, v22

    move v2, v10

    move-object/from16 v10, v26

    goto :goto_1

    .line 3696
    :cond_3
    const-string/jumbo v3, "custom_timeline_url"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string/jumbo v3, "url"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 3699
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    .line 3700
    invoke-static {v2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    move-object v11, v2

    :cond_5
    move v2, v10

    move v3, v5

    move-object/from16 v10, v26

    move/from16 v27, v4

    move-wide v4, v8

    move-object v8, v6

    move-object/from16 v9, v22

    move/from16 v6, v27

    .line 3703
    goto/16 :goto_1

    :cond_6
    const-string/jumbo v3, "collection_type"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 3704
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v25

    move v2, v10

    move v3, v5

    move-object/from16 v10, v26

    move/from16 v27, v4

    move-wide v4, v8

    move-object v8, v6

    move-object/from16 v9, v22

    move/from16 v6, v27

    goto/16 :goto_1

    .line 3705
    :cond_7
    const-string/jumbo v3, "id"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 3706
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    move v3, v5

    move-object v12, v2

    move v2, v10

    move-object/from16 v10, v26

    move-object/from16 v27, v6

    move v6, v4

    move-wide v4, v8

    move-object/from16 v8, v27

    move-object/from16 v9, v22

    goto/16 :goto_1

    .line 3707
    :cond_8
    const-string/jumbo v3, "type"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3708
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    .line 3709
    const-string/jumbo v3, "list"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 3710
    const/4 v2, 0x5

    :goto_2
    move v3, v5

    move-object/from16 v10, v26

    move/from16 v27, v4

    move-wide v4, v8

    move-object v8, v6

    move-object/from16 v9, v22

    move/from16 v6, v27

    .line 3714
    goto/16 :goto_1

    .line 3711
    :cond_9
    const-string/jumbo v3, "curated"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 3712
    const/4 v2, 0x4

    goto :goto_2

    .line 3718
    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    .line 3719
    const-string/jumbo v3, "members"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 3720
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()I

    move-result v4

    move v2, v10

    move v3, v5

    move-object/from16 v10, v26

    move/from16 v27, v4

    move-wide v4, v8

    move-object v8, v6

    move-object/from16 v9, v22

    move/from16 v6, v27

    goto/16 :goto_1

    .line 3721
    :cond_a
    const-string/jumbo v3, "subscribers"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 3722
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()I

    move-result v5

    move v2, v10

    move v3, v5

    move-object/from16 v10, v26

    move/from16 v27, v4

    move-wide v4, v8

    move-object v8, v6

    move-object/from16 v9, v22

    move/from16 v6, v27

    goto/16 :goto_1

    .line 3723
    :cond_b
    const-string/jumbo v3, "most_recent_tweet_timestamp"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 3724
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->j()J

    move-result-wide v2

    move-object v8, v6

    move-object/from16 v9, v22

    move v6, v4

    move/from16 v27, v5

    move-wide v4, v2

    move v2, v10

    move/from16 v3, v27

    move-object/from16 v10, v26

    goto/16 :goto_1

    .line 3725
    :cond_c
    const-string/jumbo v3, "id"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3726
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    move v3, v5

    move-object v12, v2

    move v2, v10

    move-object/from16 v10, v26

    move-object/from16 v27, v6

    move v6, v4

    move-wide v4, v8

    move-object/from16 v8, v27

    move-object/from16 v9, v22

    goto/16 :goto_1

    .line 3732
    :pswitch_3
    const-string/jumbo v2, "following"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3733
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->l()Z

    move-result v2

    if-eqz v2, :cond_d

    const/4 v7, 0x1

    :goto_3
    move v2, v10

    move v3, v5

    move-object/from16 v10, v26

    move/from16 v27, v4

    move-wide v4, v8

    move-object v8, v6

    move-object/from16 v9, v22

    move/from16 v6, v27

    goto/16 :goto_1

    :cond_d
    const/4 v7, 0x2

    goto :goto_3

    .line 3739
    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move v2, v10

    move v3, v5

    move-object/from16 v10, v26

    move/from16 v27, v4

    move-wide v4, v8

    move-object v8, v6

    move-object/from16 v9, v22

    move/from16 v6, v27

    .line 3740
    goto/16 :goto_1

    .line 3743
    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    .line 3744
    const-string/jumbo v3, "owner"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 3745
    const-class v2, Lcom/twitter/model/core/TwitterUser;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/core/TwitterUser;

    move-object/from16 v23, v2

    move v3, v5

    move v2, v10

    move-object/from16 v10, v26

    move/from16 v27, v4

    move-wide v4, v8

    move-object v8, v6

    move-object/from16 v9, v22

    move/from16 v6, v27

    goto/16 :goto_1

    .line 3747
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move v2, v10

    move v3, v5

    move-object/from16 v10, v26

    move/from16 v27, v4

    move-wide v4, v8

    move-object v8, v6

    move-object/from16 v9, v22

    move/from16 v6, v27

    .line 3749
    goto/16 :goto_1

    .line 3760
    :cond_f
    if-eqz v23, :cond_10

    .line 3761
    invoke-virtual/range {v23 .. v23}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v18

    .line 3766
    :goto_4
    new-instance v3, Lcom/twitter/model/topic/c;

    const/4 v6, 0x0

    const-wide/16 v14, 0x3e8

    mul-long/2addr v8, v14

    invoke-direct/range {v3 .. v9}, Lcom/twitter/model/topic/c;-><init>(IIZIJ)V

    .line 3769
    if-nez v12, :cond_11

    move-object v9, v13

    .line 3773
    :goto_5
    new-instance v5, Lcom/twitter/model/topic/TwitterTopic;

    new-instance v6, Lcom/twitter/model/topic/TwitterTopic$a;

    const/4 v2, 0x0

    invoke-direct {v6, v10, v9, v2}, Lcom/twitter/model/topic/TwitterTopic$a;-><init>(ILjava/lang/String;Z)V

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const-wide/16 v14, 0x0

    const-wide/16 v16, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v24, 0x0

    move-object/from16 v7, v26

    move-object/from16 v8, v22

    move-object/from16 v22, v3

    invoke-direct/range {v5 .. v25}, Lcom/twitter/model/topic/TwitterTopic;-><init>(Lcom/twitter/model/topic/TwitterTopic$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLcgi;Ljava/util/ArrayList;Lcom/twitter/model/topic/b;Lcom/twitter/model/core/TwitterUser;Ljava/lang/String;Ljava/lang/String;)V

    return-object v5

    .line 3763
    :cond_10
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v18

    goto :goto_4

    :cond_11
    move-object v9, v12

    goto :goto_5

    :cond_12
    move v2, v10

    goto/16 :goto_2

    .line 3687
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_3
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static aj(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/topic/TwitterTopic;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3872
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 3873
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 3874
    :goto_0
    if-eqz v0, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_2

    .line 3875
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_1

    .line 3876
    invoke-static {p0}, Lcom/twitter/library/api/z;->ai(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/topic/TwitterTopic;

    move-result-object v0

    .line 3877
    if-eqz v0, :cond_0

    .line 3878
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3883
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 3880
    :cond_1
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    .line 3881
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 3885
    :cond_2
    return-object v1
.end method

.method private static ak(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/timeline/k;
    .locals 1

    .prologue
    .line 4539
    const-class v0, Lcom/twitter/model/timeline/k;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/k;

    return-object v0
.end method

.method private static al(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/timeline/h;
    .locals 1

    .prologue
    .line 4544
    const-class v0, Lcom/twitter/model/timeline/h;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/h;

    return-object v0
.end method

.method private static am(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/LinkedHashSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/LinkedHashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4649
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 4650
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 4651
    :goto_0
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    .line 4652
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 4666
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 4654
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 4659
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 4668
    :cond_0
    return-object v1

    .line 4652
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static an(Lcom/fasterxml/jackson/core/JsonParser;)Lbsj;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5285
    new-instance v1, Lbsj;

    invoke-direct {v1}, Lbsj;-><init>()V

    .line 5287
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 5288
    :goto_0
    if-eqz v0, :cond_8

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_8

    .line 5289
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    .line 5290
    sget-object v3, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    .line 5324
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 5328
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 5292
    :pswitch_1
    const-string/jumbo v0, "id"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5293
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lbsj;->a:Ljava/lang/String;

    goto :goto_1

    .line 5294
    :cond_1
    const-string/jumbo v0, "title"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5295
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lbsj;->b:Ljava/lang/String;

    goto :goto_1

    .line 5296
    :cond_2
    const-string/jumbo v0, "image_url"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5297
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lbsj;->c:Ljava/lang/String;

    goto :goto_1

    .line 5298
    :cond_3
    const-string/jumbo v0, "author_name"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 5299
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lbsj;->d:Ljava/lang/String;

    goto :goto_1

    .line 5300
    :cond_4
    const-string/jumbo v0, "article_description"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 5301
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lbsj;->e:Ljava/lang/String;

    goto :goto_1

    .line 5302
    :cond_5
    const-string/jumbo v0, "article_url"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 5303
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lbsj;->f:Ljava/lang/String;

    goto :goto_1

    .line 5304
    :cond_6
    const-string/jumbo v0, "start_time"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5305
    sget-object v0, Lcom/twitter/util/aa;->b:Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/twitter/util/aa;->a(Ljava/text/SimpleDateFormat;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v1, Lbsj;->g:J

    goto :goto_1

    .line 5310
    :pswitch_2
    const-string/jumbo v0, "tweet_count"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5311
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()I

    move-result v0

    int-to-long v2, v0

    iput-wide v2, v1, Lbsj;->h:J

    goto/16 :goto_1

    .line 5316
    :pswitch_3
    const-string/jumbo v0, "author_account"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 5317
    const-class v0, Lcom/twitter/model/core/TwitterUser;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iput-object v0, v1, Lbsj;->j:Lcom/twitter/model/core/TwitterUser;

    goto/16 :goto_1

    .line 5319
    :cond_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    .line 5331
    :cond_8
    return-object v1

    .line 5290
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/ArrayList;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/model/search/c;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 1730
    .line 1731
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v0

    move v0, v1

    .line 1732
    :goto_0
    if-eqz v2, :cond_3

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_3

    .line 1733
    sget-object v3, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 1761
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    .line 1735
    :pswitch_1
    const-string/jumbo v2, "suggestion_type"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1736
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v2

    .line 1737
    const-string/jumbo v3, "spelling"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1738
    const/4 v0, 0x2

    goto :goto_1

    .line 1739
    :cond_1
    const-string/jumbo v3, "related"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1740
    const/4 v0, 0x3

    goto :goto_1

    .line 1746
    :pswitch_2
    const-string/jumbo v2, "suggestions"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1747
    invoke-static {p0, p1}, Lcom/twitter/library/api/z;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/ArrayList;)V

    goto :goto_1

    .line 1749
    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 1754
    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 1763
    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    move v0, v1

    .line 1766
    :cond_5
    return v0

    .line 1733
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private static b(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v1, 0x2

    const/4 v0, -0x1

    .line 4518
    if-nez p0, :cond_0

    .line 4532
    :goto_0
    return v0

    .line 4521
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_1
    move v2, v0

    :goto_1
    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 4526
    goto :goto_0

    .line 4521
    :sswitch_0
    const-string/jumbo v2, "follow_module"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :sswitch_1
    const-string/jumbo v2, "static_follow_module"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :sswitch_2
    const-string/jumbo v2, "large_carousel"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v1

    goto :goto_1

    .line 4529
    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 4521
    :sswitch_data_0
    .sparse-switch
        -0x70172957 -> :sswitch_1
        0x32c60eba -> :sswitch_0
        0x64e149e4 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/api/t;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3405
    invoke-static {p0, p1}, Lcom/twitter/library/api/z;->h(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/api/s;

    move-result-object v0

    .line 3406
    new-instance v1, Lcom/twitter/library/api/t;

    invoke-direct {v1, v0}, Lcom/twitter/library/api/t;-><init>(Lcom/twitter/library/api/s;)V

    return-object v1
.end method

.method private static b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/Map;)Lcom/twitter/model/moments/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/Moment;",
            ">;)",
            "Lcom/twitter/model/moments/v;"
        }
    .end annotation

    .prologue
    .line 4232
    const-class v0, Lcom/twitter/model/json/moments/JsonSuggestedMomentsInjection;

    .line 4233
    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/moments/JsonSuggestedMomentsInjection;

    .line 4234
    invoke-virtual {v0, p1}, Lcom/twitter/model/json/moments/JsonSuggestedMomentsInjection;->a(Ljava/util/Map;)Lcom/twitter/model/moments/v;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/Map;Z)Lcom/twitter/model/timeline/y;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/ac;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/topic/TwitterTopic;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/Moment;",
            ">;Z)",
            "Lcom/twitter/model/timeline/y;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4075
    const/4 v14, 0x0

    .line 4076
    const/4 v13, 0x0

    .line 4077
    const/4 v12, 0x0

    .line 4078
    const/4 v9, 0x0

    .line 4079
    const-wide/16 v10, 0x0

    .line 4080
    const/4 v8, 0x0

    .line 4081
    const/4 v7, 0x0

    .line 4082
    const/4 v6, 0x0

    .line 4083
    const/4 v5, 0x0

    .line 4085
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    .line 4086
    :goto_0
    if-eqz v4, :cond_18

    sget-object v15, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v15, :cond_18

    .line 4087
    sget-object v15, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v4

    aget v4, v15, v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :pswitch_0
    move v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-wide/from16 v16, v10

    move-object v10, v9

    move-object v11, v12

    move-wide/from16 v8, v16

    move-object v12, v13

    move-object v13, v14

    .line 4180
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v14

    move-object/from16 v16, v14

    move-object v14, v13

    move-object v13, v12

    move-object v12, v11

    move-object/from16 v17, v6

    move-object v6, v5

    move v5, v4

    move-object/from16 v4, v16

    move-object/from16 v18, v10

    move-wide v10, v8

    move-object v8, v7

    move-object/from16 v9, v18

    move-object/from16 v7, v17

    goto :goto_0

    .line 4089
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v4

    .line 4090
    const-string/jumbo v15, "tweet"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 4091
    new-instance v4, Lcom/twitter/model/timeline/ah$a;

    invoke-direct {v4}, Lcom/twitter/model/timeline/ah$a;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2, v4}, Lcom/twitter/library/api/z;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/model/timeline/ah$a;)Lcom/twitter/model/timeline/y$a;

    move-result-object v4

    move-object v6, v7

    move-object v7, v8

    move/from16 v16, v5

    move-object v5, v4

    move/from16 v4, v16

    move-wide/from16 v17, v10

    move-object v10, v9

    move-object v11, v12

    move-wide/from16 v8, v17

    move-object v12, v13

    move-object v13, v14

    goto :goto_1

    .line 4093
    :cond_1
    const-string/jumbo v15, "conversation"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 4094
    new-instance v4, Lcom/twitter/model/timeline/w$a;

    invoke-direct {v4}, Lcom/twitter/model/timeline/w$a;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2, v4}, Lcom/twitter/library/api/z;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/model/timeline/w$a;)Lcom/twitter/model/timeline/y$a;

    move-result-object v4

    move-object v6, v7

    move-object v7, v8

    move/from16 v16, v5

    move-object v5, v4

    move/from16 v4, v16

    move-wide/from16 v17, v10

    move-object v10, v9

    move-object v11, v12

    move-wide/from16 v8, v17

    move-object v12, v13

    move-object v13, v14

    goto :goto_1

    .line 4096
    :cond_2
    const-string/jumbo v15, "recap"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 4097
    new-instance v4, Lcom/twitter/model/timeline/ag$a;

    invoke-direct {v4}, Lcom/twitter/model/timeline/ag$a;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2, v4}, Lcom/twitter/library/api/z;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/model/timeline/ag$a;)Lcom/twitter/model/timeline/y$a;

    move-result-object v4

    move-object v6, v7

    move-object v7, v8

    move/from16 v16, v5

    move-object v5, v4

    move/from16 v4, v16

    move-wide/from16 v17, v10

    move-object v10, v9

    move-object v11, v12

    move-wide/from16 v8, v17

    move-object v12, v13

    move-object v13, v14

    goto/16 :goto_1

    .line 4099
    :cond_3
    const-string/jumbo v15, "carousel"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 4100
    new-instance v4, Lcom/twitter/model/timeline/al$a;

    invoke-direct {v4}, Lcom/twitter/model/timeline/al$a;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2, v4}, Lcom/twitter/library/api/z;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/model/timeline/al$a;)Lcom/twitter/model/timeline/y$a;

    move-result-object v4

    move-object v6, v7

    move-object v7, v8

    move/from16 v16, v5

    move-object v5, v4

    move/from16 v4, v16

    move-wide/from16 v17, v10

    move-object v10, v9

    move-object v11, v12

    move-wide/from16 v8, v17

    move-object v12, v13

    move-object v13, v14

    goto/16 :goto_1

    .line 4102
    :cond_4
    const-string/jumbo v15, "who_to_follow"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 4103
    new-instance v4, Lcom/twitter/model/timeline/ao$a;

    invoke-direct {v4}, Lcom/twitter/model/timeline/ao$a;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2, v4}, Lcom/twitter/library/api/z;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/model/timeline/ao$a;)Lcom/twitter/model/timeline/y$a;

    move-result-object v4

    move-object v6, v7

    move-object v7, v8

    move/from16 v16, v5

    move-object v5, v4

    move/from16 v4, v16

    move-wide/from16 v17, v10

    move-object v10, v9

    move-object v11, v12

    move-wide/from16 v8, v17

    move-object v12, v13

    move-object v13, v14

    goto/16 :goto_1

    .line 4105
    :cond_5
    const-string/jumbo v15, "topic"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 4106
    new-instance v4, Lcom/twitter/model/timeline/aj$a;

    invoke-direct {v4}, Lcom/twitter/model/timeline/aj$a;-><init>()V

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/twitter/library/api/z;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/timeline/aj$a;)Lcom/twitter/model/timeline/y$a;

    move-result-object v4

    move-object v6, v7

    move-object v7, v8

    move/from16 v16, v5

    move-object v5, v4

    move/from16 v4, v16

    move-wide/from16 v17, v10

    move-object v10, v9

    move-object v11, v12

    move-wide/from16 v8, v17

    move-object v12, v13

    move-object v13, v14

    goto/16 :goto_1

    .line 4107
    :cond_6
    const-string/jumbo v15, "moment_start"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 4108
    const/4 v4, 0x1

    new-instance v6, Lcom/twitter/model/timeline/ae$a;

    invoke-direct {v6}, Lcom/twitter/model/timeline/ae$a;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-static {v4, v0, v1, v6}, Lcom/twitter/library/api/z;->a(ILcom/fasterxml/jackson/core/JsonParser;Ljava/util/Map;Lcom/twitter/model/timeline/ae$a;)Lcom/twitter/model/timeline/y$a;

    move-result-object v4

    move-object v6, v7

    move-object v7, v8

    move/from16 v16, v5

    move-object v5, v4

    move/from16 v4, v16

    move-wide/from16 v17, v10

    move-object v10, v9

    move-object v11, v12

    move-wide/from16 v8, v17

    move-object v12, v13

    move-object v13, v14

    goto/16 :goto_1

    .line 4110
    :cond_7
    const-string/jumbo v15, "moment_end"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 4111
    const/4 v4, 0x2

    new-instance v6, Lcom/twitter/model/timeline/ae$a;

    invoke-direct {v6}, Lcom/twitter/model/timeline/ae$a;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-static {v4, v0, v1, v6}, Lcom/twitter/library/api/z;->a(ILcom/fasterxml/jackson/core/JsonParser;Ljava/util/Map;Lcom/twitter/model/timeline/ae$a;)Lcom/twitter/model/timeline/y$a;

    move-result-object v4

    move-object v6, v7

    move-object v7, v8

    move/from16 v16, v5

    move-object v5, v4

    move/from16 v4, v16

    move-wide/from16 v17, v10

    move-object v10, v9

    move-object v11, v12

    move-wide/from16 v8, v17

    move-object v12, v13

    move-object v13, v14

    goto/16 :goto_1

    .line 4113
    :cond_8
    const-string/jumbo v15, "message"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 4114
    new-instance v4, Lcom/twitter/model/timeline/ac$a;

    invoke-direct {v4}, Lcom/twitter/model/timeline/ac$a;-><init>()V

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/twitter/library/api/z;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/timeline/ac$a;)Lcom/twitter/model/timeline/y$a;

    move-result-object v4

    move-object v6, v7

    move-object v7, v8

    move/from16 v16, v5

    move-object v5, v4

    move/from16 v4, v16

    move-wide/from16 v17, v10

    move-object v10, v9

    move-object v11, v12

    move-wide/from16 v8, v17

    move-object v12, v13

    move-object v13, v14

    goto/16 :goto_1

    .line 4115
    :cond_9
    const-string/jumbo v15, "ad_slot"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 4116
    new-instance v6, Lcom/twitter/model/timeline/t$a;

    invoke-direct {v6}, Lcom/twitter/model/timeline/t$a;-><init>()V

    const-class v4, Lcom/twitter/model/revenue/c;

    .line 4117
    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/revenue/c;

    invoke-virtual {v6, v4}, Lcom/twitter/model/timeline/t$a;->a(Lcom/twitter/model/revenue/c;)Lcom/twitter/model/timeline/t$a;

    move-result-object v4

    move-object v6, v7

    move-object v7, v8

    move/from16 v16, v5

    move-object v5, v4

    move/from16 v4, v16

    move-wide/from16 v17, v10

    move-object v10, v9

    move-object v11, v12

    move-wide/from16 v8, v17

    move-object v12, v13

    move-object v13, v14

    goto/16 :goto_1

    .line 4118
    :cond_a
    const-string/jumbo v15, "suggested_moments_injection"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 4119
    new-instance v4, Lcom/twitter/model/timeline/ai$a;

    invoke-direct {v4}, Lcom/twitter/model/timeline/ai$a;-><init>()V

    .line 4120
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/twitter/library/api/z;->b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/Map;)Lcom/twitter/model/moments/v;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/twitter/model/timeline/ai$a;->a(Lcom/twitter/model/moments/v;)Lcom/twitter/model/timeline/ai$a;

    move-result-object v4

    move-object v6, v7

    move-object v7, v8

    move/from16 v16, v5

    move-object v5, v4

    move/from16 v4, v16

    move-wide/from16 v17, v10

    move-object v10, v9

    move-object v11, v12

    move-wide/from16 v8, v17

    move-object v12, v13

    move-object v13, v14

    goto/16 :goto_1

    .line 4121
    :cond_b
    const-string/jumbo v15, "entity_id"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_11

    .line 4122
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 4123
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    .line 4124
    :goto_2
    if-eqz v4, :cond_10

    sget-object v15, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v15, :cond_10

    .line 4125
    sget-object v15, Lcom/fasterxml/jackson/core/JsonToken;->h:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v4, v15, :cond_d

    .line 4126
    const-string/jumbo v4, "type"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v4, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 4127
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v4

    .line 4128
    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4130
    const-string/jumbo v15, "banner"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 4131
    const/4 v5, 0x1

    .line 4146
    :cond_c
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_2

    .line 4134
    :cond_d
    sget-object v15, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v4, v15, :cond_f

    .line 4135
    const-string/jumbo v4, "ids"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v4, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 4136
    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/z;->am(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/LinkedHashSet;

    move-result-object v4

    .line 4137
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_4
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 4138
    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 4141
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    .line 4143
    :cond_f
    sget-object v15, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v4, v15, :cond_c

    .line 4144
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    .line 4148
    :cond_10
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move/from16 v16, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-wide/from16 v17, v10

    move-object v10, v9

    move-object v11, v12

    move-wide/from16 v8, v17

    move-object v12, v13

    move-object v13, v4

    move/from16 v4, v16

    .line 4149
    goto/16 :goto_1

    :cond_11
    const-string/jumbo v15, "banner"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_13

    .line 4150
    if-eqz v5, :cond_12

    .line 4151
    new-instance v4, Lcom/twitter/model/timeline/v$a;

    invoke-direct {v4}, Lcom/twitter/model/timeline/v$a;-><init>()V

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/twitter/library/api/z;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/timeline/v$a;)Lcom/twitter/model/timeline/y$a;

    move-result-object v4

    move-object v6, v7

    move-object v7, v8

    move/from16 v16, v5

    move-object v5, v4

    move/from16 v4, v16

    move-wide/from16 v17, v10

    move-object v10, v9

    move-object v11, v12

    move-wide/from16 v8, v17

    move-object v12, v13

    move-object v13, v14

    goto/16 :goto_1

    .line 4154
    :cond_12
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/twitter/library/api/z;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/Map;)Lcom/twitter/model/core/TwitterSocialProof;

    move-result-object v4

    move-object v7, v8

    move-object/from16 v16, v6

    move-object v6, v4

    move v4, v5

    move-object/from16 v5, v16

    move-wide/from16 v17, v10

    move-object v10, v9

    move-object v11, v12

    move-wide/from16 v8, v17

    move-object v12, v13

    move-object v13, v14

    goto/16 :goto_1

    .line 4156
    :cond_13
    const-string/jumbo v15, "suggests_info"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_14

    .line 4157
    const-class v4, Lcom/twitter/model/timeline/r;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/timeline/r;

    move/from16 v16, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v4

    move/from16 v4, v16

    move-wide/from16 v17, v10

    move-object v10, v9

    move-object v11, v12

    move-wide/from16 v8, v17

    move-object v12, v13

    move-object v13, v14

    goto/16 :goto_1

    .line 4158
    :cond_14
    const-string/jumbo v15, "moment"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_15

    .line 4159
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-static {v4, v0, v1}, Lcom/twitter/library/api/z;->a(ILcom/fasterxml/jackson/core/JsonParser;Ljava/util/Map;)Lcom/twitter/model/moments/x;

    move-result-object v4

    move-object v13, v14

    move-object/from16 v16, v6

    move-object v6, v7

    move-object v7, v8

    move-wide/from16 v17, v10

    move-object v10, v9

    move-object v11, v12

    move-wide/from16 v8, v17

    move-object v12, v4

    move v4, v5

    move-object/from16 v5, v16

    goto/16 :goto_1

    .line 4161
    :cond_15
    const-string/jumbo v15, "dismiss_info"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_16

    .line 4162
    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/z;->ak(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/timeline/k;

    move-result-object v4

    move-object v12, v13

    move-object v13, v14

    move-object/from16 v16, v7

    move-object v7, v8

    move-wide/from16 v17, v10

    move-object v10, v9

    move-object v11, v4

    move-wide/from16 v8, v17

    move v4, v5

    move-object v5, v6

    move-object/from16 v6, v16

    goto/16 :goto_1

    .line 4163
    :cond_16
    const-string/jumbo v15, "feedback_info"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 4164
    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/z;->al(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/timeline/h;

    move-result-object v4

    move/from16 v16, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-wide v8, v10

    move-object v10, v4

    move-object v11, v12

    move/from16 v4, v16

    move-object v12, v13

    move-object v13, v14

    goto/16 :goto_1

    .line 4166
    :cond_17
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-wide/from16 v16, v10

    move-object v10, v9

    move-object v11, v12

    move-wide/from16 v8, v16

    move-object v12, v13

    move-object v13, v14

    .line 4168
    goto/16 :goto_1

    .line 4171
    :pswitch_2
    const-string/jumbo v4, "sort_index"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v4, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 4172
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    move v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-wide/from16 v16, v10

    move-object v10, v9

    move-object v11, v12

    move-wide/from16 v8, v16

    move-object v12, v13

    move-object v13, v14

    goto/16 :goto_1

    .line 4186
    :cond_18
    if-nez v6, :cond_19

    .line 4187
    const/4 v4, 0x0

    .line 4226
    :goto_5
    return-object v4

    .line 4190
    :cond_19
    if-eqz v13, :cond_1a

    .line 4191
    invoke-virtual {v6, v13}, Lcom/twitter/model/timeline/y$a;->a(Lcom/twitter/model/moments/x;)Lcom/twitter/model/timeline/y$a;

    .line 4194
    :cond_1a
    const-wide/16 v4, 0x0

    cmp-long v4, v10, v4

    if-lez v4, :cond_1b

    .line 4195
    invoke-virtual {v6, v10, v11}, Lcom/twitter/model/timeline/y$a;->a(J)Lcom/twitter/model/timeline/y$a;

    .line 4198
    :cond_1b
    if-eqz v7, :cond_1c

    .line 4199
    invoke-virtual {v6, v7}, Lcom/twitter/model/timeline/y$a;->a(Lcom/twitter/model/core/TwitterSocialProof;)Lcom/twitter/model/timeline/y$a;

    .line 4202
    :cond_1c
    if-eqz v8, :cond_1d

    .line 4203
    invoke-virtual {v6, v8}, Lcom/twitter/model/timeline/y$a;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/timeline/y$a;

    .line 4204
    iget-object v4, v8, Lcom/twitter/model/timeline/r;->b:Ljava/lang/String;

    invoke-virtual {v6, v4}, Lcom/twitter/model/timeline/y$a;->b(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    .line 4207
    :cond_1d
    if-eqz v12, :cond_1e

    .line 4208
    invoke-virtual {v6, v12}, Lcom/twitter/model/timeline/y$a;->a(Lcom/twitter/model/timeline/k;)Lcom/twitter/model/timeline/y$a;

    .line 4211
    :cond_1e
    if-eqz v9, :cond_1f

    .line 4212
    invoke-virtual {v6, v9}, Lcom/twitter/model/timeline/y$a;->a(Lcom/twitter/model/timeline/h;)Lcom/twitter/model/timeline/y$a;

    .line 4215
    :cond_1f
    if-eqz p5, :cond_20

    instance-of v4, v6, Lcom/twitter/model/timeline/ah$a;

    if-eqz v4, :cond_20

    .line 4216
    invoke-static {v6}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/timeline/ah$a;

    .line 4217
    invoke-virtual {v4}, Lcom/twitter/model/timeline/ah$a;->e()Lcom/twitter/model/core/ac;

    move-result-object v4

    .line 4218
    if-eqz v4, :cond_20

    .line 4219
    invoke-virtual {v4}, Lcom/twitter/model/core/ac;->b()Ljava/lang/String;

    move-result-object v14

    .line 4223
    :cond_20
    invoke-static {v14}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 4224
    invoke-virtual {v6, v14}, Lcom/twitter/model/timeline/y$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    .line 4226
    :cond_21
    invoke-virtual {v6}, Lcom/twitter/model/timeline/y$a;->r()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/timeline/y;

    goto :goto_5

    .line 4087
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;)Lcom/twitter/model/topic/TwitterTopic;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/ac;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)",
            "Lcom/twitter/model/topic/TwitterTopic;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 606
    const/16 v16, -0x1

    .line 607
    const/4 v4, 0x0

    .line 608
    const-wide/16 v14, 0x0

    .line 609
    const/4 v11, 0x0

    .line 610
    const-wide/16 v12, 0x0

    .line 611
    const/4 v7, 0x0

    .line 612
    const/4 v8, 0x0

    .line 613
    const/4 v9, 0x0

    .line 614
    const/4 v10, 0x0

    .line 615
    const/4 v5, 0x0

    .line 616
    const/4 v6, 0x0

    .line 617
    const/16 v22, 0x0

    .line 618
    const/4 v3, 0x0

    .line 619
    const/16 v20, 0x0

    .line 620
    const/16 v18, 0x0

    .line 621
    const/16 v19, 0x0

    .line 623
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    move-object/from16 v23, v2

    move v2, v3

    move-object v3, v4

    move-object/from16 v4, v23

    .line 624
    :goto_0
    if-eqz v4, :cond_11

    sget-object v17, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    move-object/from16 v0, v17

    if-eq v4, v0, :cond_11

    .line 625
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v17

    .line 626
    sget-object v21, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v4

    aget v4, v21, v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :pswitch_0
    move/from16 v4, v16

    .line 713
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v16

    move-object/from16 v23, v16

    move/from16 v16, v4

    move-object/from16 v4, v23

    .line 714
    goto :goto_0

    .line 628
    :pswitch_1
    const-string/jumbo v4, "seed_hashtag"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 629
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v8

    move/from16 v4, v16

    goto :goto_1

    .line 630
    :cond_2
    const-string/jumbo v4, "image_url"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 631
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v10

    move/from16 v4, v16

    goto :goto_1

    .line 632
    :cond_3
    const-string/jumbo v4, "title"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 633
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v5

    move/from16 v4, v16

    goto :goto_1

    .line 634
    :cond_4
    const-string/jumbo v4, "subtitle"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 635
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v6

    move/from16 v4, v16

    goto :goto_1

    .line 636
    :cond_5
    const-string/jumbo v4, "query"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 637
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v7

    move/from16 v4, v16

    goto :goto_1

    .line 638
    :cond_6
    const-string/jumbo v4, "reason"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 639
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v11

    move/from16 v4, v16

    goto :goto_1

    .line 640
    :cond_7
    const-string/jumbo v4, "view_url"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 641
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v9

    move/from16 v4, v16

    goto/16 :goto_1

    .line 642
    :cond_8
    const-string/jumbo v4, "event_status"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 643
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v22

    move/from16 v4, v16

    goto/16 :goto_1

    .line 648
    :pswitch_2
    const-string/jumbo v4, "tweet_count"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 649
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->j()J

    move-result-wide v12

    move/from16 v4, v16

    goto/16 :goto_1

    .line 650
    :cond_9
    const-string/jumbo v4, "start_time"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 651
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->j()J

    move-result-wide v14

    move/from16 v4, v16

    goto/16 :goto_1

    .line 656
    :pswitch_3
    const-string/jumbo v4, "metadata"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 657
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    move-object/from16 v23, v4

    move/from16 v4, v16

    move-object/from16 v16, v23

    .line 658
    :goto_2
    if-eqz v16, :cond_1

    sget-object v17, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    .line 659
    sget-object v17, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual/range {v16 .. v16}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v16

    aget v16, v17, v16

    packed-switch v16, :pswitch_data_1

    .line 679
    :cond_a
    :goto_3
    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v16

    goto :goto_2

    .line 661
    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v16

    .line 662
    const-string/jumbo v17, "id"

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 663
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 664
    :cond_b
    const-string/jumbo v17, "type"

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_a

    .line 666
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->r()Ljava/lang/String;

    move-result-object v4

    .line 665
    invoke-static {v4}, Lcom/twitter/model/topic/TwitterTopic;->c(Ljava/lang/String;)I

    move-result v4

    goto :goto_3

    .line 672
    :pswitch_6
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    .line 681
    :cond_c
    const-string/jumbo v4, "sports_data"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 682
    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/z;->S(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/topic/d;

    move-result-object v20

    move/from16 v4, v16

    goto/16 :goto_1

    .line 683
    :cond_d
    const-string/jumbo v4, "promoted_content"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 684
    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/z;->e(Lcom/fasterxml/jackson/core/JsonParser;)Lcgi;

    move-result-object v18

    move/from16 v4, v16

    goto/16 :goto_1

    .line 686
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move/from16 v4, v16

    .line 688
    goto/16 :goto_1

    .line 691
    :pswitch_7
    const-string/jumbo v4, "tweets"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 692
    if-nez p1, :cond_f

    .line 693
    const-class v4, Lcom/twitter/model/core/ac;

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/twitter/model/json/common/e;->d(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v19

    move/from16 v4, v16

    goto/16 :goto_1

    .line 696
    :cond_f
    invoke-static/range {p0 .. p2}, Lcom/twitter/library/api/z;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;)Ljava/util/ArrayList;

    move-result-object v19

    move/from16 v4, v16

    goto/16 :goto_1

    .line 699
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move/from16 v4, v16

    .line 701
    goto/16 :goto_1

    .line 704
    :pswitch_8
    const-string/jumbo v4, "spiking"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 705
    const/4 v2, 0x1

    move/from16 v4, v16

    goto/16 :goto_1

    .line 716
    :cond_11
    const/4 v4, 0x3

    move/from16 v0, v16

    if-ne v0, v4, :cond_13

    const/4 v4, 0x1

    move/from16 v17, v4

    :goto_4
    if-nez v3, :cond_14

    const/4 v4, 0x1

    :goto_5
    and-int v4, v4, v17

    if-eqz v4, :cond_16

    .line 717
    invoke-static {v7}, Lcom/twitter/model/topic/TwitterTopic;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v17, v3

    .line 720
    :goto_6
    const/4 v3, -0x1

    move/from16 v0, v16

    if-eq v0, v3, :cond_12

    invoke-static/range {v17 .. v17}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_12

    .line 721
    invoke-static {v7}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 722
    :cond_12
    new-instance v2, Lcom/twitter/model/json/common/InvalidJsonFormatException;

    const-string/jumbo v3, "Failed parsing event; missing required data"

    invoke-direct {v2, v3}, Lcom/twitter/model/json/common/InvalidJsonFormatException;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 723
    const/4 v3, 0x0

    .line 725
    :goto_7
    return-object v3

    .line 716
    :cond_13
    const/4 v4, 0x0

    move/from16 v17, v4

    goto :goto_4

    :cond_14
    const/4 v4, 0x0

    goto :goto_5

    .line 725
    :cond_15
    new-instance v3, Lcom/twitter/model/topic/TwitterTopic;

    new-instance v4, Lcom/twitter/model/topic/TwitterTopic$a;

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v4, v0, v1, v2}, Lcom/twitter/model/topic/TwitterTopic$a;-><init>(ILjava/lang/String;Z)V

    const-wide/16 v16, 0x0

    const/16 v21, 0x0

    invoke-direct/range {v3 .. v22}, Lcom/twitter/model/topic/TwitterTopic;-><init>(Lcom/twitter/model/topic/TwitterTopic$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLcgi;Ljava/util/List;Lcom/twitter/model/topic/b;Lcom/twitter/model/core/TwitterUser;Ljava/lang/String;)V

    goto :goto_7

    :cond_16
    move-object/from16 v17, v3

    goto :goto_6

    .line 626
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_8
        :pswitch_2
    .end packed-switch

    .line 659
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_6
    .end packed-switch
.end method

.method public static b(Lcom/fasterxml/jackson/core/JsonParser;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 376
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 377
    :goto_0
    if-eqz v0, :cond_3

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_3

    .line 378
    sget-object v1, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 414
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 380
    :pswitch_1
    const-string/jumbo v0, "phone"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 381
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 382
    :goto_2
    if-eqz v0, :cond_0

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_0

    .line 383
    sget-object v1, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    .line 399
    :cond_1
    :goto_3
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_2

    .line 385
    :pswitch_3
    const-string/jumbo v0, "verified"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 386
    const/4 v0, 0x1

    .line 416
    :goto_4
    return v0

    .line 392
    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    .line 402
    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 407
    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 416
    :cond_3
    const/4 v0, 0x0

    goto :goto_4

    .line 378
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 383
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public static c(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/core/TwitterUser;)Lbdf;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3545
    invoke-static {p0, p1}, Lcom/twitter/library/api/z;->h(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/api/s;

    move-result-object v0

    .line 3546
    new-instance v1, Lbdf;

    invoke-direct {v1, v0}, Lbdf;-><init>(Lcom/twitter/library/api/s;)V

    return-object v1
.end method

.method public static c(Lcom/fasterxml/jackson/core/JsonParser;)Lcbv;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 428
    const-class v0, Lcbv;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbv;

    return-object v0
.end method

.method private static c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/ac;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/revenue/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3912
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 3913
    :cond_0
    new-instance v0, Lcom/twitter/model/json/common/InvalidJsonFormatException;

    const-string/jumbo v1, "Missing tweets map or users map"

    invoke-direct {v0, v1}, Lcom/twitter/model/json/common/InvalidJsonFormatException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 3914
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    .line 3931
    :goto_0
    return-object v0

    .line 3916
    :cond_1
    const-class v0, Lcom/twitter/model/revenue/a$b;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->d(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 3917
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Lcom/twitter/util/collection/h;->b(I)Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 3918
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/revenue/a$b;

    .line 3919
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 3920
    iget-object v1, v0, Lcom/twitter/model/revenue/a$b;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 3921
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/twitter/library/api/z;->a(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)Lcom/twitter/model/core/ac;

    move-result-object v1

    .line 3922
    if-eqz v1, :cond_3

    .line 3923
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 3926
    :cond_4
    new-instance v1, Lcom/twitter/model/revenue/a$a;

    invoke-direct {v1}, Lcom/twitter/model/revenue/a$a;-><init>()V

    invoke-virtual {v1, v4}, Lcom/twitter/model/revenue/a$a;->a(Ljava/util/List;)Lcom/twitter/model/revenue/a$a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/model/revenue/a$a;->a(Lcom/twitter/model/revenue/a$b;)Lcom/twitter/model/revenue/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/revenue/a$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/revenue/a;

    .line 3927
    if-eqz v0, :cond_2

    .line 3928
    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1

    .line 3931
    :cond_5
    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    goto :goto_0
.end method

.method private static c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/model/search/c;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 2312
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2313
    const/4 v1, 0x0

    .line 2314
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    .line 2315
    :goto_0
    if-eqz v1, :cond_8

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_8

    .line 2316
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v2, :cond_7

    .line 2317
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    .line 2318
    :goto_1
    if-eqz v1, :cond_6

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_6

    .line 2319
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 2373
    :cond_0
    :goto_2
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_1

    .line 2321
    :pswitch_1
    const-string/jumbo v1, "query"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2322
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2327
    :pswitch_2
    const-string/jumbo v1, "indices"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2328
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    .line 2329
    :goto_3
    if-eqz v1, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_0

    .line 2332
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_1

    :pswitch_3
    move v1, v3

    move v2, v3

    .line 2356
    :cond_1
    :goto_4
    if-le v2, v3, :cond_3

    if-ge v2, v1, :cond_3

    if-eqz v0, :cond_2

    .line 2357
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-gt v1, v4, :cond_3

    .line 2358
    :cond_2
    const/4 v4, 0x2

    new-array v4, v4, [I

    const/4 v6, 0x0

    aput v2, v4, v6

    const/4 v2, 0x1

    aput v1, v4, v2

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2360
    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_3

    .line 2334
    :goto_5
    if-eqz v4, :cond_1

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v6, :cond_1

    .line 2335
    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->i:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v4, v6, :cond_4

    .line 2336
    if-ne v2, v3, :cond_5

    .line 2337
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()I

    move-result v2

    .line 2342
    :cond_4
    :goto_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_5

    .line 2339
    :cond_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()I

    move-result v1

    goto :goto_6

    .line 2347
    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move v1, v3

    move v2, v3

    .line 2348
    goto :goto_4

    .line 2366
    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    .line 2375
    :cond_6
    new-instance v1, Lcom/twitter/model/search/c;

    invoke-direct {v1, v0, v5}, Lcom/twitter/model/search/c;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2377
    :cond_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto/16 :goto_0

    .line 2379
    :cond_8
    return-void

    :pswitch_6
    move v2, v3

    move-object v4, v1

    move v1, v3

    goto :goto_5

    .line 2319
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_5
    .end packed-switch

    .line 2332
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static d(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/api/w;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 3551
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v9

    .line 3558
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v4, v0

    move-object v5, v3

    move-object v8, v3

    move-object v1, v3

    move-object v2, v3

    move-object v0, v3

    .line 3559
    :goto_0
    if-eqz v4, :cond_c

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v6, :cond_c

    .line 3560
    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v4, v6, :cond_a

    .line 3561
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v4

    .line 3562
    const-string/jumbo v6, "objects"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 3563
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    move-object v6, v4

    move-object v4, v2

    move-object v2, v1

    move-object v1, v8

    .line 3564
    :goto_1
    if-eqz v6, :cond_10

    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v6, v7, :cond_10

    .line 3565
    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v6, v7, :cond_4

    .line 3566
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v6

    .line 3567
    const-string/jumbo v7, "tweets"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 3568
    invoke-static {p0, p1}, Lcom/twitter/library/api/z;->e(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/core/TwitterUser;)Ljava/util/HashMap;

    move-result-object v2

    .line 3579
    :cond_0
    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v6

    goto :goto_1

    .line 3569
    :cond_1
    const-string/jumbo v7, "users"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 3570
    invoke-static {p0}, Lcom/twitter/library/api/z;->F(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v4

    goto :goto_2

    .line 3571
    :cond_2
    const-string/jumbo v7, "timelines"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 3572
    invoke-static {p0}, Lcom/twitter/library/api/z;->aj(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v1

    goto :goto_2

    .line 3574
    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    .line 3576
    :cond_4
    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v6, v7, :cond_0

    .line 3577
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    .line 3581
    :cond_5
    const-string/jumbo v6, "response"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 3582
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    move-object v6, v0

    move-object v7, v5

    move-object v0, v4

    .line 3583
    :goto_3
    if-eqz v0, :cond_f

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v4, :cond_f

    .line 3584
    sget-object v4, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    .line 3613
    :cond_6
    :goto_4
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_3

    .line 3586
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 3587
    const-string/jumbo v4, "timeline_id"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3588
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    .line 3593
    :pswitch_2
    const-string/jumbo v0, "timeline"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 3594
    const/4 v5, 0x1

    move-object v0, p0

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/api/z;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/Map;Z)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    goto :goto_4

    .line 3597
    :cond_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_4

    .line 3602
    :pswitch_3
    const-string/jumbo v0, "position"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 3603
    invoke-static {p0}, Lcom/twitter/library/api/z;->E(Lcom/fasterxml/jackson/core/JsonParser;)Landroid/util/Pair;

    move-result-object v7

    goto :goto_4

    .line 3605
    :cond_8
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_4

    .line 3616
    :cond_9
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v4, v1

    move-object v1, v5

    move-object v5, v2

    move-object v2, v8

    .line 3621
    :goto_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v6

    move-object v8, v2

    move-object v2, v5

    move-object v5, v1

    move-object v1, v4

    move-object v4, v6

    goto/16 :goto_0

    .line 3618
    :cond_a
    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v4, v6, :cond_b

    .line 3619
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    :cond_b
    move-object v4, v1

    move-object v1, v5

    move-object v5, v2

    move-object v2, v8

    goto :goto_5

    .line 3626
    :cond_c
    if-eqz v8, :cond_e

    if-eqz v2, :cond_e

    .line 3627
    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/topic/TwitterTopic;

    .line 3628
    if-eqz v0, :cond_d

    .line 3629
    invoke-virtual {v0}, Lcom/twitter/model/topic/TwitterTopic;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/TwitterUser;

    move-object v2, v1

    move-object v1, v0

    .line 3637
    :goto_6
    new-instance v0, Lcom/twitter/library/api/w;

    invoke-virtual {v9}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    iget-object v4, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    iget-object v5, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/w;-><init>(Lcom/twitter/model/topic/TwitterTopic;Lcom/twitter/model/core/TwitterUser;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_d
    move-object v2, v3

    move-object v1, v0

    .line 3631
    goto :goto_6

    :cond_e
    move-object v2, v3

    move-object v1, v3

    .line 3635
    goto :goto_6

    :cond_f
    move-object v0, v6

    move-object v4, v1

    move-object v5, v2

    move-object v1, v7

    move-object v2, v8

    goto :goto_5

    :cond_10
    move-object v10, v5

    move-object v5, v4

    move-object v4, v2

    move-object v2, v1

    move-object v1, v10

    goto :goto_5

    .line 3584
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static d(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Long;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 439
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 440
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    .line 441
    :goto_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    .line 442
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 458
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    .line 444
    :pswitch_1
    const-string/jumbo v1, "media_id_string"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 445
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_1

    .line 451
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 460
    :cond_1
    return-object v0

    .line 442
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static e(Lcom/fasterxml/jackson/core/JsonParser;)Lcgi;
    .locals 1

    .prologue
    .line 762
    const-class v0, Lcgi;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgi;

    .line 763
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcgi;->b:Lcgi;

    goto :goto_0
.end method

.method public static e(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/core/TwitterUser;)Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Lcom/twitter/model/core/TwitterUser;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/ac;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3823
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 3824
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 3825
    :goto_0
    if-eqz v0, :cond_3

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_3

    .line 3826
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_2

    .line 3827
    const-class v0, Lcom/twitter/model/core/ac$a;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac$a;

    .line 3828
    if-eqz v0, :cond_1

    .line 3829
    invoke-virtual {v0, p1}, Lcom/twitter/model/core/ac$a;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/core/ac$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/ac$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    .line 3830
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3834
    :cond_0
    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 3829
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 3831
    :cond_2
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    .line 3832
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    .line 3836
    :cond_3
    return-object v1
.end method

.method public static f(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/ClientConfiguration;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 768
    .line 770
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v0

    move-object v0, v1

    .line 772
    :goto_0
    if-eqz v2, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_2

    .line 773
    sget-object v3, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 793
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    .line 775
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    .line 776
    const-string/jumbo v3, "access"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 777
    invoke-static {p0}, Lcom/twitter/library/api/z;->af(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/v;

    move-result-object v1

    goto :goto_1

    .line 778
    :cond_0
    const-string/jumbo v3, "twitter"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 779
    const-class v0, Lcom/twitter/model/client/UrlConfiguration;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/client/UrlConfiguration;

    goto :goto_1

    .line 781
    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 786
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 795
    :cond_2
    new-instance v2, Lcom/twitter/library/api/ClientConfiguration;

    invoke-direct {v2, v0, v1}, Lcom/twitter/library/api/ClientConfiguration;-><init>(Lcom/twitter/model/client/UrlConfiguration;Lcom/twitter/library/api/v;)V

    return-object v2

    .line 773
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static f(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/api/search/h;
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1901
    const/16 v16, 0x0

    .line 1902
    const/4 v4, 0x0

    .line 1903
    const/4 v15, 0x0

    .line 1904
    const/4 v6, 0x0

    .line 1905
    const/4 v7, 0x0

    .line 1906
    const/4 v8, 0x0

    .line 1907
    const/4 v9, 0x0

    .line 1908
    const/4 v12, 0x0

    .line 1909
    const/4 v10, 0x0

    .line 1910
    const/4 v11, 0x0

    .line 1911
    const/16 v17, 0x0

    .line 1912
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 1913
    const/4 v14, 0x0

    .line 1915
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    .line 1916
    sget-object v3, Lcom/twitter/library/api/z;->a:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 1918
    if-eqz v2, :cond_1

    .line 1919
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 1925
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    .line 1926
    :goto_0
    if-eqz v2, :cond_d

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v5, :cond_d

    .line 1927
    sget-object v5, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v5, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :pswitch_0
    move-object v2, v12

    move-object v5, v4

    move-object v4, v15

    move-object/from16 v12, v16

    .line 2064
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v15

    move-object/from16 v16, v12

    move-object v12, v2

    move-object v2, v15

    move-object v15, v4

    move-object v4, v5

    goto :goto_0

    .line 1921
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 1922
    const/4 v2, 0x0

    .line 2139
    :goto_2
    return-object v2

    .line 1929
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    .line 1930
    const-string/jumbo v5, "metadata"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1931
    packed-switch v3, :pswitch_data_1

    .line 1948
    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v2, v12

    move-object v5, v4

    move-object v4, v15

    move-object/from16 v12, v16

    .line 1949
    goto :goto_1

    .line 1937
    :pswitch_3
    const-class v2, Lcom/twitter/model/search/e;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/search/e;

    .line 1938
    if-eqz v2, :cond_16

    const-string/jumbo v5, "top"

    iget-object v0, v2, Lcom/twitter/model/search/e;->d:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 1939
    const-string/jumbo v5, "popular"

    iput-object v5, v2, Lcom/twitter/model/search/e;->d:Ljava/lang/String;

    move-object v5, v4

    move-object v4, v15

    move-object/from16 v19, v12

    move-object v12, v2

    move-object/from16 v2, v19

    goto :goto_1

    .line 1944
    :pswitch_4
    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/z;->t(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/search/TwitterUserMetadata;

    move-result-object v2

    move-object v5, v4

    move-object/from16 v12, v16

    move-object v4, v15

    .line 1945
    goto :goto_1

    .line 1952
    :cond_2
    const-string/jumbo v5, "data"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1953
    packed-switch v3, :pswitch_data_2

    .line 2031
    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v2, v12

    move-object v5, v4

    move-object v4, v15

    move-object/from16 v12, v16

    .line 2032
    goto :goto_1

    .line 1956
    :pswitch_6
    const-class v2, Lcom/twitter/model/json/core/JsonTwitterStatus;

    .line 1957
    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/json/core/JsonTwitterStatus;

    .line 1958
    if-eqz v2, :cond_0

    .line 1959
    if-eqz v16, :cond_3

    .line 1960
    move-object/from16 v0, v16

    iput-object v0, v2, Lcom/twitter/model/json/core/JsonTwitterStatus;->E:Lcom/twitter/model/search/e;

    .line 1962
    :cond_3
    invoke-virtual {v2}, Lcom/twitter/model/json/core/JsonTwitterStatus;->a()Lcom/twitter/model/core/ac$a;

    move-result-object v2

    .line 1963
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/twitter/model/core/ac$a;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/model/core/ac$a;

    .line 1964
    invoke-virtual {v2}, Lcom/twitter/model/core/ac$a;->r()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/core/ac;

    .line 1965
    if-eqz v2, :cond_4

    iget-object v4, v2, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    if-eqz v4, :cond_4

    .line 1966
    const/4 v4, 0x4

    if-ne v3, v4, :cond_4

    .line 1967
    iget-object v4, v2, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    const-string/jumbo v5, "news"

    iput-object v5, v4, Lcom/twitter/model/search/e;->d:Ljava/lang/String;

    :cond_4
    move-object v4, v15

    move-object v5, v2

    move-object v2, v12

    move-object/from16 v12, v16

    .line 1970
    goto/16 :goto_1

    .line 1974
    :pswitch_7
    const-class v2, Lcom/twitter/model/core/TwitterUser$a;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/core/TwitterUser$a;

    move-object v5, v4

    move-object v4, v2

    move-object v2, v12

    move-object/from16 v12, v16

    .line 1975
    goto/16 :goto_1

    .line 1978
    :pswitch_8
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 1979
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/twitter/library/api/z;->b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/ArrayList;)I

    move-result v5

    .line 1980
    const/4 v2, -0x1

    if-eq v5, v2, :cond_0

    .line 1982
    packed-switch v5, :pswitch_data_3

    move v3, v5

    move-object v2, v12

    move-object v5, v4

    move-object/from16 v12, v16

    move-object v4, v15

    .line 1998
    goto/16 :goto_1

    .line 1984
    :pswitch_9
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v8, v2, [Ljava/lang/String;

    .line 1985
    const/4 v2, 0x0

    .line 1986
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    move v3, v2

    :goto_3
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/search/c;

    .line 1987
    iget-object v2, v2, Lcom/twitter/model/search/c;->b:Ljava/lang/String;

    aput-object v2, v8, v3

    .line 1988
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    .line 1989
    goto :goto_3

    :cond_5
    move v3, v5

    move-object v2, v12

    move-object v5, v4

    move-object/from16 v12, v16

    move-object v4, v15

    .line 1991
    goto/16 :goto_1

    .line 1994
    :pswitch_a
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/search/c;

    move v3, v5

    move-object v7, v2

    move-object v2, v12

    move-object v5, v4

    move-object v4, v15

    move-object/from16 v12, v16

    .line 1995
    goto/16 :goto_1

    .line 2005
    :pswitch_b
    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/z;->r(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/search/d;

    move-result-object v11

    .line 2006
    if-eqz v11, :cond_0

    .line 2007
    iget-object v6, v11, Lcom/twitter/model/search/d;->e:Ljava/util/ArrayList;

    move-object v2, v12

    move-object v5, v4

    move-object v4, v15

    move-object/from16 v12, v16

    goto/16 :goto_1

    .line 2012
    :pswitch_c
    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/z;->X(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/List;

    move-result-object v6

    move-object v2, v12

    move-object v5, v4

    move-object v4, v15

    move-object/from16 v12, v16

    .line 2013
    goto/16 :goto_1

    .line 2017
    :pswitch_d
    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v2, v5}, Lcom/twitter/library/api/z;->b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;)Lcom/twitter/model/topic/TwitterTopic;

    move-result-object v2

    .line 2018
    if-nez v2, :cond_6

    .line 2019
    new-instance v2, Lcom/twitter/model/json/common/InvalidJsonFormatException;

    const-string/jumbo v3, "Search with null or empty event"

    invoke-direct {v2, v3}, Lcom/twitter/model/json/common/InvalidJsonFormatException;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 2021
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 2023
    :cond_6
    invoke-interface {v13, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2025
    const/16 v2, 0xc

    if-ne v3, v2, :cond_0

    .line 2026
    const/4 v14, 0x1

    move-object v2, v12

    move-object v5, v4

    move-object v4, v15

    move-object/from16 v12, v16

    goto/16 :goto_1

    .line 2035
    :cond_7
    const-string/jumbo v5, "filter"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2036
    const/16 v2, 0x9

    if-ne v3, v2, :cond_8

    .line 2037
    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/z;->Y(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/search/a;

    move-result-object v10

    move-object v2, v12

    move-object v5, v4

    move-object v4, v15

    move-object/from16 v12, v16

    goto/16 :goto_1

    .line 2039
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v2, v12

    move-object v5, v4

    move-object v4, v15

    move-object/from16 v12, v16

    goto/16 :goto_1

    .line 2042
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v2, v12

    move-object v5, v4

    move-object v4, v15

    move-object/from16 v12, v16

    .line 2044
    goto/16 :goto_1

    .line 2047
    :pswitch_e
    const-string/jumbo v2, "data"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 2048
    const/4 v2, 0x6

    if-ne v3, v2, :cond_a

    .line 2049
    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/z;->s(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/List;

    move-result-object v9

    move-object v2, v12

    move-object v5, v4

    move-object v4, v15

    move-object/from16 v12, v16

    goto/16 :goto_1

    .line 2050
    :cond_a
    const/16 v2, 0x9

    if-ne v3, v2, :cond_b

    .line 2051
    const-class v2, Lcom/twitter/model/core/ac;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/twitter/model/json/common/e;->d(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v6

    move-object v2, v12

    move-object v5, v4

    move-object v4, v15

    move-object/from16 v12, v16

    goto/16 :goto_1

    .line 2053
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v2, v12

    move-object v5, v4

    move-object v4, v15

    move-object/from16 v12, v16

    goto/16 :goto_1

    .line 2056
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v2, v12

    move-object v5, v4

    move-object v4, v15

    move-object/from16 v12, v16

    .line 2058
    goto/16 :goto_1

    .line 2067
    :cond_d
    const/4 v5, 0x0

    .line 2068
    packed-switch v3, :pswitch_data_4

    :cond_e
    :pswitch_f
    move/from16 v12, v17

    .line 2139
    :goto_4
    new-instance v2, Lcom/twitter/library/api/search/h;

    invoke-direct/range {v2 .. v14}, Lcom/twitter/library/api/search/h;-><init>(ILcom/twitter/model/core/ac;Lcom/twitter/model/core/TwitterUser;Ljava/util/List;Lcom/twitter/model/search/c;[Ljava/lang/String;Ljava/util/List;Lcom/twitter/model/search/a;Lcom/twitter/model/search/d;ZLjava/util/List;Z)V

    goto/16 :goto_2

    .line 2071
    :pswitch_10
    if-nez v4, :cond_f

    .line 2072
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 2074
    :cond_f
    invoke-virtual {v4}, Lcom/twitter/model/core/ac;->f()Z

    move-result v12

    goto :goto_4

    .line 2078
    :pswitch_11
    if-nez v15, :cond_10

    .line 2079
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 2080
    :cond_10
    if-eqz v12, :cond_11

    .line 2081
    invoke-virtual {v15, v12}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/search/TwitterUserMetadata;)Lcom/twitter/model/core/TwitterUser$a;

    .line 2082
    iget-object v2, v12, Lcom/twitter/model/search/TwitterUserMetadata;->a:Lcom/twitter/model/core/TwitterSocialProof;

    if-eqz v2, :cond_11

    .line 2083
    invoke-virtual {v15}, Lcom/twitter/model/core/TwitterUser$a;->g()I

    move-result v2

    iget-object v5, v12, Lcom/twitter/model/search/TwitterUserMetadata;->a:Lcom/twitter/model/core/TwitterSocialProof;

    iget v5, v5, Lcom/twitter/model/core/TwitterSocialProof;->g:I

    invoke-static {v2, v5}, Lcom/twitter/model/core/g;->a(II)I

    move-result v2

    invoke-virtual {v15, v2}, Lcom/twitter/model/core/TwitterUser$a;->i(I)Lcom/twitter/model/core/TwitterUser$a;

    .line 2087
    :cond_11
    invoke-virtual {v15}, Lcom/twitter/model/core/TwitterUser$a;->r()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/core/TwitterUser;

    .line 2088
    if-nez v2, :cond_15

    .line 2089
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 2094
    :pswitch_12
    if-nez v7, :cond_e

    .line 2095
    new-instance v2, Lcom/twitter/model/json/common/InvalidJsonFormatException;

    const-string/jumbo v3, "Search with null spelling suggestions."

    invoke-direct {v2, v3}, Lcom/twitter/model/json/common/InvalidJsonFormatException;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 2096
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 2101
    :pswitch_13
    if-eqz v8, :cond_12

    array-length v2, v8

    if-nez v2, :cond_e

    .line 2102
    :cond_12
    new-instance v2, Lcom/twitter/model/json/common/InvalidJsonFormatException;

    const-string/jumbo v3, "Search with null or empty related."

    invoke-direct {v2, v3}, Lcom/twitter/model/json/common/InvalidJsonFormatException;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 2103
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 2108
    :pswitch_14
    if-nez v11, :cond_e

    .line 2109
    new-instance v2, Lcom/twitter/model/json/common/InvalidJsonFormatException;

    const-string/jumbo v3, "Search with null or empty summary."

    invoke-direct {v2, v3}, Lcom/twitter/model/json/common/InvalidJsonFormatException;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 2110
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 2115
    :pswitch_15
    if-nez v9, :cond_e

    .line 2116
    new-instance v2, Lcom/twitter/model/json/common/InvalidJsonFormatException;

    const-string/jumbo v3, "Search with null or empty user gallery."

    invoke-direct {v2, v3}, Lcom/twitter/model/json/common/InvalidJsonFormatException;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 2117
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 2123
    :pswitch_16
    if-eqz v6, :cond_13

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 2124
    :cond_13
    new-instance v2, Lcom/twitter/model/json/common/InvalidJsonFormatException;

    const-string/jumbo v3, "Search with null or empty tweet/media gallery."

    invoke-direct {v2, v3}, Lcom/twitter/model/json/common/InvalidJsonFormatException;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 2125
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 2127
    :cond_14
    const/4 v2, 0x0

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/core/ac;

    .line 2128
    if-eqz v2, :cond_e

    iget-object v12, v2, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    if-eqz v12, :cond_e

    if-eqz v16, :cond_e

    .line 2129
    iget-object v12, v2, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    move-object/from16 v0, v16

    iget-object v15, v0, Lcom/twitter/model/search/e;->f:Ljava/lang/String;

    iput-object v15, v12, Lcom/twitter/model/search/e;->f:Ljava/lang/String;

    .line 2130
    iget-object v2, v2, Lcom/twitter/model/core/ac;->L:Lcom/twitter/model/search/e;

    move-object/from16 v0, v16

    iget-object v12, v0, Lcom/twitter/model/search/e;->g:Ljava/lang/String;

    iput-object v12, v2, Lcom/twitter/model/search/e;->g:Ljava/lang/String;

    move/from16 v12, v17

    goto/16 :goto_4

    :cond_15
    move-object v5, v2

    move/from16 v12, v17

    goto/16 :goto_4

    :cond_16
    move-object v5, v4

    move-object v4, v15

    move-object/from16 v19, v12

    move-object v12, v2

    move-object/from16 v2, v19

    goto/16 :goto_1

    .line 1927
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 1931
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 1953
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_5
        :pswitch_8
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_c
        :pswitch_5
        :pswitch_5
        :pswitch_d
        :pswitch_b
        :pswitch_d
    .end packed-switch

    .line 1982
    :pswitch_data_3
    .packed-switch 0x2
        :pswitch_a
        :pswitch_9
    .end packed-switch

    .line 2068
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_10
        :pswitch_f
        :pswitch_15
        :pswitch_16
        :pswitch_f
        :pswitch_16
        :pswitch_f
        :pswitch_14
    .end packed-switch
.end method

.method public static g(Lcom/fasterxml/jackson/core/JsonParser;)I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 806
    const/4 v2, 0x0

    .line 807
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    .line 808
    const/4 v0, 0x0

    move-object v4, v1

    move v1, v2

    move-object v2, v4

    .line 809
    :goto_0
    if-eqz v2, :cond_3

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_3

    .line 810
    sget-object v3, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 851
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    .line 812
    :pswitch_0
    const-string/jumbo v2, "relationship"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 813
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    .line 814
    :goto_2
    if-eqz v2, :cond_0

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_0

    .line 815
    sget-object v3, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_1

    .line 834
    :goto_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_2

    .line 817
    :pswitch_1
    const-string/jumbo v2, "source"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 818
    invoke-static {p0}, Lcom/twitter/library/api/z;->T(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    goto :goto_3

    .line 820
    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    .line 826
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 837
    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 843
    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 853
    :cond_3
    return v1

    .line 810
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_3
        :pswitch_0
    .end packed-switch

    .line 815
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private static g(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/core/TwitterUser;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            "Lcom/twitter/model/core/TwitterUser;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/library/api/search/h;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2145
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2146
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 2147
    :goto_0
    if-eqz v0, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_2

    .line 2148
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 2181
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 2150
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 2151
    :goto_2
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    .line 2152
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_1

    .line 2169
    :cond_1
    :goto_3
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_2

    .line 2155
    :pswitch_3
    invoke-static {p0, p1}, Lcom/twitter/library/api/z;->f(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/api/search/h;

    move-result-object v0

    .line 2156
    if-eqz v0, :cond_1

    .line 2157
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2162
    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    .line 2174
    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 2184
    :cond_2
    return-object v1

    .line 2148
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 2152
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static h(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/api/s;
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3420
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v12

    .line 3421
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v13

    .line 3422
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 3423
    const/4 v10, 0x0

    .line 3424
    const/4 v5, 0x0

    .line 3425
    const/4 v2, 0x0

    .line 3426
    const/4 v9, 0x0

    .line 3427
    const/4 v8, 0x0

    .line 3428
    const-string/jumbo v3, ""

    .line 3429
    const/4 v7, 0x0

    .line 3430
    const/4 v6, 0x0

    .line 3431
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 3434
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v15, v1

    move-object v1, v3

    move-object v3, v5

    move-object v5, v10

    move-object v10, v15

    .line 3435
    :goto_0
    if-eqz v10, :cond_17

    sget-object v11, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v10, v11, :cond_17

    .line 3436
    sget-object v11, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v10, v11, :cond_15

    .line 3437
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v10

    .line 3438
    const-string/jumbo v11, "twitter_objects"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 3439
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v10

    .line 3440
    :goto_1
    if-eqz v10, :cond_16

    sget-object v11, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v10, v11, :cond_16

    .line 3441
    sget-object v11, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v10, v11, :cond_7

    .line 3442
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v10

    .line 3443
    const-string/jumbo v11, "tweets"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 3444
    invoke-static/range {p0 .. p1}, Lcom/twitter/library/api/z;->e(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/core/TwitterUser;)Ljava/util/HashMap;

    move-result-object v2

    .line 3465
    :cond_0
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v10

    goto :goto_1

    .line 3445
    :cond_1
    const-string/jumbo v11, "users"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 3446
    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/z;->F(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v3

    goto :goto_2

    .line 3447
    :cond_2
    const-string/jumbo v11, "event_summaries"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 3448
    move-object/from16 v0, p0

    invoke-static {v2, v3, v0}, Lcom/twitter/library/api/z;->a(Ljava/util/HashMap;Ljava/util/HashMap;Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    goto :goto_2

    .line 3450
    :cond_3
    const-string/jumbo v11, "custom_timelines"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 3451
    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/z;->aj(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    goto :goto_2

    .line 3452
    :cond_4
    const-string/jumbo v11, "moments"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 3453
    const-class v5, Lcom/twitter/model/moments/Moment;

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/twitter/model/json/common/e;->e(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/util/Map;

    move-result-object v5

    invoke-static {v5}, Lcom/twitter/model/json/common/f;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v5

    goto :goto_2

    .line 3455
    :cond_5
    const-string/jumbo v11, "events"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 3456
    const-class v10, Lcom/twitter/model/livevideo/b;

    .line 3457
    move-object/from16 v0, p0

    invoke-static {v0, v10}, Lcom/twitter/model/json/common/e;->e(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/util/Map;

    move-result-object v10

    .line 3458
    invoke-interface {v14, v10}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_2

    .line 3460
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    .line 3462
    :cond_7
    sget-object v11, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v10, v11, :cond_0

    .line 3463
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    .line 3467
    :cond_8
    const-string/jumbo v11, "response"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_14

    .line 3468
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v10

    move-object v11, v9

    move-object v9, v1

    move-object v1, v10

    move-object v10, v8

    move v8, v7

    move v7, v6

    .line 3469
    :goto_3
    if-eqz v1, :cond_18

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v6, :cond_18

    .line 3470
    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->g()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 3471
    const-string/jumbo v1, "start_at_top"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 3472
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->l()Z

    move-result v7

    .line 3504
    :cond_9
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_3

    .line 3474
    :cond_a
    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->c()Z

    move-result v6

    if-eqz v6, :cond_b

    .line 3475
    const-string/jumbo v1, "polling_interval_seconds"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 3476
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()Ljava/lang/Number;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v8

    goto :goto_4

    .line 3478
    :cond_b
    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v6, :cond_11

    .line 3479
    const-string/jumbo v1, "notifications"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 3482
    const-class v1, Lcom/twitter/model/timeline/u;

    .line 3483
    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lcom/twitter/model/json/common/e;->d(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v6

    .line 3484
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_c

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    const/4 v11, 0x1

    if-ne v1, v11, :cond_d

    :cond_c
    const/4 v1, 0x1

    :goto_5
    invoke-static {v1}, Lcom/twitter/util/f;->b(Z)Z

    .line 3485
    invoke-static {v6}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/timeline/u;

    move-object v11, v1

    .line 3486
    goto :goto_4

    .line 3484
    :cond_d
    const/4 v1, 0x0

    goto :goto_5

    .line 3486
    :cond_e
    const-string/jumbo v1, "timeline"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 3487
    const/4 v6, 0x0

    move-object/from16 v1, p0

    invoke-static/range {v1 .. v6}, Lcom/twitter/library/api/z;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/Map;Z)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v12, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    goto :goto_4

    .line 3489
    :cond_f
    const-string/jumbo v1, "ads"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 3490
    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Lcom/twitter/library/api/z;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v13, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    goto/16 :goto_4

    .line 3492
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_4

    .line 3494
    :cond_11
    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v6, :cond_9

    .line 3495
    const-string/jumbo v1, "cursor"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 3496
    const-class v1, Lcom/twitter/model/timeline/q;

    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/timeline/q;

    move-object v10, v1

    goto/16 :goto_4

    .line 3498
    :cond_12
    const-string/jumbo v1, "event"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 3499
    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/z;->ah(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_4

    .line 3501
    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_4

    .line 3507
    :cond_14
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move v15, v6

    move-object v6, v9

    move-object v9, v5

    move-object v5, v8

    move-object v8, v3

    move-object v3, v1

    move v1, v15

    move-object/from16 v16, v2

    move v2, v7

    move-object/from16 v7, v16

    .line 3512
    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v10

    move v15, v1

    move-object v1, v3

    move-object v3, v8

    move-object v8, v5

    move-object v5, v9

    move-object v9, v6

    move v6, v15

    move/from16 v16, v2

    move-object v2, v7

    move/from16 v7, v16

    goto/16 :goto_0

    .line 3509
    :cond_15
    sget-object v11, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v10, v11, :cond_16

    .line 3510
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    :cond_16
    move v15, v6

    move-object v6, v9

    move-object v9, v5

    move-object v5, v8

    move-object v8, v3

    move-object v3, v1

    move v1, v15

    move-object/from16 v16, v2

    move v2, v7

    move-object/from16 v7, v16

    goto :goto_6

    .line 3515
    :cond_17
    invoke-interface {v14, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/livevideo/b;

    .line 3517
    new-instance v3, Lcom/twitter/library/api/s$a;

    invoke-direct {v3}, Lcom/twitter/library/api/s$a;-><init>()V

    .line 3518
    invoke-virtual {v12}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-virtual {v3, v2}, Lcom/twitter/library/api/s$a;->a(Ljava/util/List;)Lcom/twitter/library/api/s$a;

    move-result-object v2

    .line 3519
    invoke-virtual {v2, v8}, Lcom/twitter/library/api/s$a;->a(Lcom/twitter/model/timeline/q;)Lcom/twitter/library/api/s$a;

    move-result-object v3

    .line 3520
    invoke-virtual {v13}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-virtual {v3, v2}, Lcom/twitter/library/api/s$a;->b(Ljava/util/List;)Lcom/twitter/library/api/s$a;

    move-result-object v2

    .line 3521
    invoke-virtual {v2, v9}, Lcom/twitter/library/api/s$a;->a(Lcom/twitter/model/timeline/u;)Lcom/twitter/library/api/s$a;

    move-result-object v2

    .line 3522
    invoke-virtual {v2, v1}, Lcom/twitter/library/api/s$a;->a(Lcom/twitter/model/livevideo/b;)Lcom/twitter/library/api/s$a;

    move-result-object v1

    .line 3523
    invoke-virtual {v1, v6}, Lcom/twitter/library/api/s$a;->a(Z)Lcom/twitter/library/api/s$a;

    move-result-object v1

    .line 3524
    invoke-virtual {v1, v7}, Lcom/twitter/library/api/s$a;->a(I)Lcom/twitter/library/api/s$a;

    move-result-object v1

    .line 3525
    invoke-virtual {v1}, Lcom/twitter/library/api/s$a;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/s;

    .line 3517
    return-object v1

    :cond_18
    move v1, v7

    move-object v6, v11

    move-object v7, v2

    move v2, v8

    move-object v8, v3

    move-object v3, v9

    move-object v9, v5

    move-object v5, v10

    goto :goto_6
.end method

.method public static h(Lcom/fasterxml/jackson/core/JsonParser;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 924
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 926
    :goto_0
    if-eqz v0, :cond_1

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_1

    .line 927
    sget-object v1, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 940
    :cond_0
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 930
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 931
    const-string/jumbo v1, "saved"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 932
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->l()Z

    move-result v0

    .line 942
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 927
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static i(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 954
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 956
    :goto_0
    if-eqz v0, :cond_1

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_1

    .line 957
    sget-object v1, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 969
    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 959
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 960
    const-string/jumbo v1, "status"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 961
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    .line 971
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 957
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static j(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 982
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v0

    move-object v0, v1

    .line 984
    :goto_0
    if-eqz v2, :cond_3

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_3

    .line 985
    sget-object v3, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 1026
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    .line 987
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 991
    :pswitch_2
    const-string/jumbo v2, "promotion_destination_url"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 992
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    .line 993
    :goto_2
    if-eqz v2, :cond_0

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_0

    .line 994
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 995
    sget-object v3, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_1

    .line 1011
    :cond_1
    :goto_3
    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_2

    .line 997
    :pswitch_4
    const-string/jumbo v2, "string_value"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 998
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    .line 1028
    :goto_4
    return-object v0

    .line 1004
    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    .line 1014
    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 1019
    :pswitch_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 1028
    goto :goto_4

    .line 985
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 995
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public static k(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/geo/b;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1039
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v4

    .line 1040
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v5

    .line 1043
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v2, v0

    move-object v3, v0

    .line 1045
    :goto_0
    if-eqz v1, :cond_6

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v6, :cond_6

    .line 1046
    sget-object v6, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    :cond_0
    move-object v1, v2

    move-object v2, v3

    .line 1095
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    move-object v8, v3

    move-object v3, v2

    move-object v2, v1

    move-object v1, v8

    goto :goto_0

    .line 1048
    :pswitch_0
    const-string/jumbo v6, "places"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1049
    :cond_1
    :goto_2
    if-eqz v1, :cond_0

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v6, :cond_0

    .line 1050
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    .line 1051
    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v6, :cond_1

    .line 1052
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    .line 1053
    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->f:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v6, :cond_1

    const-string/jumbo v6, "place"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1054
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    .line 1055
    const-class v6, Lcom/twitter/model/geo/TwitterPlace;

    invoke-static {p0, v6}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_2

    .line 1059
    :cond_2
    const-string/jumbo v6, "attributions"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1060
    :cond_3
    :goto_3
    if-eqz v1, :cond_0

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v6, :cond_0

    .line 1061
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    .line 1062
    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v6, :cond_3

    .line 1063
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    .line 1064
    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->f:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v6, :cond_3

    .line 1066
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/twitter/library/api/geo/PlaceAttribution;->a(Ljava/lang/String;)Lcom/twitter/library/api/geo/PlaceAttribution;

    move-result-object v6

    .line 1067
    if-eqz v6, :cond_3

    .line 1068
    invoke-virtual {v5, v6}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_3

    .line 1074
    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v1, v2

    move-object v2, v3

    .line 1076
    goto :goto_1

    .line 1079
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    move-object v1, v2

    move-object v2, v3

    .line 1080
    goto :goto_1

    .line 1083
    :pswitch_2
    const-string/jumbo v1, "autotag_place_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1084
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v3

    .line 1086
    :cond_5
    const-string/jumbo v1, "geo_search_request_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1087
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v1

    move-object v2, v3

    goto/16 :goto_1

    .line 1098
    :cond_6
    new-instance v6, Lcom/twitter/library/api/geo/b;

    invoke-virtual {v4}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v5}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-direct {v6, v3, v0, v2, v1}, Lcom/twitter/library/api/geo/b;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V

    return-object v6

    .line 1046
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static l(Lcom/fasterxml/jackson/core/JsonParser;)Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1167
    .line 1169
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    move-object v1, v0

    move-object v2, v0

    .line 1172
    :goto_0
    if-eqz v3, :cond_2

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v4, :cond_2

    .line 1173
    sget-object v4, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v3}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1197
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    goto :goto_0

    .line 1175
    :pswitch_1
    const-string/jumbo v4, "users"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1176
    const-class v2, Lcom/twitter/model/core/TwitterUser;

    invoke-static {p0, v2}, Lcom/twitter/model/json/common/e;->d(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v2

    goto :goto_1

    .line 1178
    :cond_1
    :goto_2
    if-eqz v3, :cond_0

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v4, :cond_0

    .line 1179
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    goto :goto_2

    .line 1186
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    .line 1187
    const-string/jumbo v3, "name"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1188
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 1200
    :cond_2
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    .line 1173
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static m(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1204
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1206
    if-eqz p0, :cond_2

    .line 1207
    const/4 v1, 0x0

    .line 1208
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    .line 1209
    :goto_0
    if-eqz v1, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_2

    .line 1210
    sget-object v3, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    .line 1243
    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    .line 1213
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    .line 1214
    :goto_1
    if-eqz v1, :cond_0

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_0

    .line 1215
    sget-object v3, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_1

    .line 1235
    :cond_1
    :goto_2
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_1

    .line 1217
    :pswitch_2
    const-string/jumbo v1, "id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1218
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->j()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1223
    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1228
    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    .line 1247
    :cond_2
    return-object v2

    .line 1210
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch

    .line 1215
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static n(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcap;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1258
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 1260
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 1261
    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    .line 1262
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    .line 1263
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 1264
    invoke-static {p0}, Lcom/twitter/library/api/z;->q(Lcom/fasterxml/jackson/core/JsonParser;)Lcap;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 1266
    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 1269
    :cond_1
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static o(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcac;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1277
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 1278
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 1279
    :goto_0
    if-eqz v0, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_2

    .line 1280
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_1

    .line 1281
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 1295
    :cond_1
    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 1281
    :sswitch_0
    const-string/jumbo v3, "activity_events"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string/jumbo v3, "generic_activities"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    .line 1283
    :pswitch_0
    invoke-static {p0}, Lcom/twitter/library/api/z;->n(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    goto :goto_2

    .line 1287
    :pswitch_1
    const-class v0, Lcaj;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->d(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    goto :goto_2

    .line 1297
    :cond_2
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0

    .line 1281
    nop

    :sswitch_data_0
    .sparse-switch
        0x10962c49 -> :sswitch_0
        0x6722cb55 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static p(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/timeline/n;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1308
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 1309
    :goto_0
    if-eqz v0, :cond_3

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_3

    .line 1310
    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_2

    .line 1311
    const-string/jumbo v0, "prompt"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1312
    invoke-static {p0}, Lcom/twitter/library/api/z;->U(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/timeline/n;

    move-result-object v0

    .line 1321
    :goto_1
    return-object v0

    .line 1314
    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 1319
    :cond_1
    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 1316
    :cond_2
    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_1

    .line 1317
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    .line 1321
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static q(Lcom/fasterxml/jackson/core/JsonParser;)Lcap;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1521
    new-instance v2, Lcap$a;

    invoke-direct {v2}, Lcap$a;-><init>()V

    .line 1523
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    .line 1524
    sget-object v0, Lcom/twitter/library/api/b$a;->a:Lcom/twitter/library/api/b$a;

    .line 1525
    :goto_0
    if-eqz v1, :cond_c

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_c

    .line 1526
    sget-object v3, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    .line 1615
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    .line 1528
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    .line 1529
    const-string/jumbo v3, "action"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string/jumbo v3, "event"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1530
    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    .line 1531
    sget-object v1, Lcom/twitter/library/api/b;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/b$a;

    .line 1532
    if-eqz v0, :cond_2

    .line 1533
    iget v1, v0, Lcom/twitter/library/api/b$a;->b:I

    invoke-virtual {v2, v1}, Lcap$a;->a(I)Lcap$a;

    .line 1534
    iget v1, v0, Lcom/twitter/library/api/b$a;->c:I

    invoke-virtual {v2, v1}, Lcap$a;->c(I)Lcap$a;

    .line 1535
    iget v1, v0, Lcom/twitter/library/api/b$a;->d:I

    invoke-virtual {v2, v1}, Lcap$a;->e(I)Lcap$a;

    .line 1536
    iget v1, v0, Lcom/twitter/library/api/b$a;->e:I

    invoke-virtual {v2, v1}, Lcap$a;->g(I)Lcap$a;

    goto :goto_1

    .line 1539
    :cond_2
    const/4 v0, 0x0

    .line 1618
    :goto_2
    return-object v0

    .line 1541
    :cond_3
    const-string/jumbo v3, "created_at"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1542
    sget-object v1, Lcom/twitter/util/aa;->b:Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/twitter/util/aa;->a(Ljava/text/SimpleDateFormat;Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcap$a;->a(J)Lcap$a;

    goto :goto_1

    .line 1543
    :cond_4
    const-string/jumbo v3, "max_position"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1544
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcap$a;->b(J)Lcap$a;

    goto :goto_1

    .line 1545
    :cond_5
    const-string/jumbo v3, "min_position"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1546
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcap$a;->c(J)Lcap$a;

    goto :goto_1

    .line 1551
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    .line 1553
    const-string/jumbo v3, "sources"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1554
    const/4 v1, 0x1

    iget v3, v0, Lcom/twitter/library/api/b$a;->c:I

    if-ne v1, v3, :cond_6

    .line 1555
    const-class v1, Lcom/twitter/model/core/TwitterUser;

    invoke-static {p0, v1}, Lcom/twitter/model/json/common/e;->d(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcap$a;->a(Ljava/util/List;)Lcap$a;

    goto/16 :goto_1

    .line 1557
    :cond_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    .line 1559
    :cond_7
    const-string/jumbo v3, "targets"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1560
    iget v1, v0, Lcom/twitter/library/api/b$a;->d:I

    packed-switch v1, :pswitch_data_1

    .line 1571
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    .line 1562
    :pswitch_3
    const-class v1, Lcom/twitter/model/core/TwitterUser;

    invoke-static {p0, v1}, Lcom/twitter/model/json/common/e;->d(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcap$a;->b(Ljava/util/List;)Lcap$a;

    goto/16 :goto_1

    .line 1566
    :pswitch_4
    const-class v1, Lcom/twitter/model/core/ac;

    invoke-static {p0, v1}, Lcom/twitter/model/json/common/e;->d(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcap$a;->b(Ljava/util/List;)Lcap$a;

    goto/16 :goto_1

    .line 1575
    :cond_8
    const-string/jumbo v3, "target_objects"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1576
    iget v1, v0, Lcom/twitter/library/api/b$a;->e:I

    packed-switch v1, :pswitch_data_2

    .line 1587
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    .line 1578
    :pswitch_5
    const-class v1, Lcom/twitter/model/core/ac;

    invoke-static {p0, v1}, Lcom/twitter/model/json/common/e;->d(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcap$a;->c(Ljava/util/List;)Lcap$a;

    goto/16 :goto_1

    .line 1582
    :pswitch_6
    const-class v1, Lcom/twitter/model/core/aa;

    invoke-static {p0, v1}, Lcom/twitter/model/json/common/e;->d(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcap$a;->c(Ljava/util/List;)Lcap$a;

    goto/16 :goto_1

    .line 1592
    :cond_9
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    .line 1597
    :pswitch_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v1

    .line 1598
    const-string/jumbo v3, "sources_size"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1599
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()I

    move-result v1

    invoke-virtual {v2, v1}, Lcap$a;->b(I)Lcap$a;

    goto/16 :goto_1

    .line 1600
    :cond_a
    const-string/jumbo v3, "targets_size"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 1601
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()I

    move-result v1

    invoke-virtual {v2, v1}, Lcap$a;->d(I)Lcap$a;

    goto/16 :goto_1

    .line 1602
    :cond_b
    const-string/jumbo v3, "target_objects_size"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1603
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()I

    move-result v1

    invoke-virtual {v2, v1}, Lcap$a;->f(I)Lcap$a;

    goto/16 :goto_1

    .line 1608
    :pswitch_8
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    .line 1618
    :cond_c
    invoke-virtual {v2}, Lcap$a;->r()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcap;

    goto/16 :goto_2

    .line 1526
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_7
    .end packed-switch

    .line 1560
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 1576
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static r(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/search/d;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, -0x1

    const/4 v4, 0x0

    .line 1623
    .line 1627
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v5, v0

    move-object v1, v4

    move-object v2, v4

    move v3, v6

    move-object v0, v4

    .line 1628
    :goto_0
    if-eqz v5, :cond_4

    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v5, v7, :cond_4

    .line 1629
    sget-object v7, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v5}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v5

    aget v5, v7, v5

    packed-switch v5, :pswitch_data_0

    .line 1658
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v5

    goto :goto_0

    .line 1631
    :pswitch_1
    const-string/jumbo v5, "summary_type"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1632
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/model/search/d;->a(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1637
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v5

    .line 1638
    const-string/jumbo v7, "summary_query"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1639
    invoke-static {p0}, Lcom/twitter/library/api/z;->W(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 1640
    :cond_1
    const-string/jumbo v7, "title"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1641
    invoke-static {p0}, Lcom/twitter/library/api/z;->W(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 1642
    :cond_2
    const-string/jumbo v5, "tweets"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1643
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1644
    invoke-static {p0, v0}, Lcom/twitter/library/api/z;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/ArrayList;)I

    move-result v3

    goto :goto_1

    .line 1646
    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 1651
    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 1660
    :cond_4
    if-ne v3, v6, :cond_5

    move-object v0, v4

    .line 1669
    :goto_2
    return-object v0

    .line 1664
    :cond_5
    if-eqz v3, :cond_6

    const/4 v5, 0x1

    if-ne v3, v5, :cond_8

    :cond_6
    if-eqz v1, :cond_7

    if-nez v2, :cond_8

    :cond_7
    move-object v0, v4

    .line 1667
    goto :goto_2

    .line 1669
    :cond_8
    new-instance v4, Lcom/twitter/model/search/d;

    invoke-direct {v4, v3, v2, v1, v0}, Lcom/twitter/model/search/d;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    move-object v0, v4

    goto :goto_2

    .line 1629
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static s(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1793
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v4

    .line 1794
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 1795
    :goto_0
    if-eqz v0, :cond_5

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_5

    .line 1796
    sget-object v1, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1844
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 1798
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v1, v3

    move-object v2, v3

    .line 1801
    :goto_2
    if-eqz v0, :cond_3

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v5, :cond_3

    .line 1802
    sget-object v5, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_1

    :pswitch_2
    move-object v0, v1

    move-object v1, v2

    .line 1822
    :goto_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    move-object v6, v0

    move-object v0, v2

    move-object v2, v1

    move-object v1, v6

    goto :goto_2

    .line 1804
    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 1805
    const-string/jumbo v5, "data"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1806
    const-class v0, Lcom/twitter/model/core/TwitterUser$a;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser$a;

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_3

    .line 1807
    :cond_1
    const-string/jumbo v5, "metadata"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1808
    invoke-static {p0}, Lcom/twitter/library/api/z;->t(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/search/TwitterUserMetadata;

    move-result-object v0

    move-object v1, v2

    goto :goto_3

    .line 1810
    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    move-object v1, v2

    .line 1812
    goto :goto_3

    .line 1815
    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    move-object v1, v2

    .line 1816
    goto :goto_3

    .line 1824
    :cond_3
    if-eqz v2, :cond_0

    .line 1825
    if-eqz v1, :cond_4

    .line 1826
    invoke-virtual {v2, v1}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/search/TwitterUserMetadata;)Lcom/twitter/model/core/TwitterUser$a;

    .line 1827
    iget-object v0, v1, Lcom/twitter/model/search/TwitterUserMetadata;->a:Lcom/twitter/model/core/TwitterSocialProof;

    if-eqz v0, :cond_4

    .line 1828
    invoke-virtual {v2}, Lcom/twitter/model/core/TwitterUser$a;->g()I

    move-result v0

    iget-object v1, v1, Lcom/twitter/model/search/TwitterUserMetadata;->a:Lcom/twitter/model/core/TwitterSocialProof;

    iget v1, v1, Lcom/twitter/model/core/TwitterSocialProof;->g:I

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/twitter/model/core/TwitterUser$a;->i(I)Lcom/twitter/model/core/TwitterUser$a;

    .line 1832
    :cond_4
    invoke-virtual {v2}, Lcom/twitter/model/core/TwitterUser$a;->q()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1

    .line 1837
    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 1846
    :cond_5
    invoke-virtual {v4}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0

    .line 1796
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 1802
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static t(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/search/TwitterUserMetadata;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1852
    .line 1853
    const/4 v4, 0x0

    .line 1856
    const-class v0, Lcom/twitter/model/json/search/JsonTwitterUserMetadata;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/json/search/JsonTwitterUserMetadata;

    .line 1857
    if-eqz v0, :cond_1

    .line 1858
    const-string/jumbo v1, "top"

    iget-object v2, v0, Lcom/twitter/model/json/search/JsonTwitterUserMetadata;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 1859
    iget-object v2, v0, Lcom/twitter/model/json/search/JsonTwitterUserMetadata;->a:Ljava/lang/String;

    .line 1860
    iget-object v1, v0, Lcom/twitter/model/json/search/JsonTwitterUserMetadata;->c:Lcom/twitter/model/json/search/JsonSearchSocialProof;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/twitter/model/json/search/JsonTwitterUserMetadata;->c:Lcom/twitter/model/json/search/JsonSearchSocialProof;

    .line 1861
    invoke-virtual {v0}, Lcom/twitter/model/json/search/JsonSearchSocialProof;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterSocialProof;

    :goto_0
    move-object v1, v0

    .line 1864
    :goto_1
    new-instance v0, Lcom/twitter/model/search/TwitterUserMetadata;

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/model/search/TwitterUserMetadata;-><init>(Lcom/twitter/model/core/TwitterSocialProof;Ljava/lang/String;Ljava/lang/String;ZLcom/twitter/model/core/s;)V

    return-object v0

    :cond_0
    move-object v0, v3

    .line 1861
    goto :goto_0

    :cond_1
    move-object v2, v3

    move-object v1, v3

    goto :goto_1
.end method

.method public static u(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2388
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2389
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 2391
    :goto_0
    if-eqz v0, :cond_7

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_7

    .line 2392
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 2433
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 2394
    :pswitch_1
    const-string/jumbo v0, "media_items"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2395
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 2396
    :goto_2
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    .line 2397
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_4

    .line 2398
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 2399
    :goto_3
    if-eqz v0, :cond_5

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_5

    .line 2400
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_3

    .line 2401
    const-string/jumbo v0, "status"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2402
    const-class v0, Lcom/twitter/model/core/ac;

    .line 2403
    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    .line 2404
    if-eqz v0, :cond_1

    .line 2405
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2413
    :cond_1
    :goto_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_3

    .line 2408
    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_4

    .line 2410
    :cond_3
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_1

    .line 2411
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_4

    .line 2415
    :cond_4
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_5

    .line 2416
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    .line 2418
    :cond_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_2

    .line 2421
    :cond_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 2426
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 2436
    :cond_7
    return-object v1

    .line 2392
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static v(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/core/TwitterUser;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 2440
    .line 2445
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v2

    move-object v1, v2

    move-object v5, v2

    move-object v6, v2

    .line 2446
    :goto_0
    if-eqz v0, :cond_4

    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v7, :cond_4

    .line 2447
    sget-object v7, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v7, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :pswitch_0
    move-object v0, v1

    move-object v1, v6

    .line 2476
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v6

    move-object v8, v6

    move-object v6, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_0

    .line 2449
    :pswitch_1
    const-string/jumbo v0, "user"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2450
    const-class v0, Lcom/twitter/model/core/TwitterUser;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_1

    .line 2451
    :cond_1
    const-string/jumbo v0, "welcome_flow_reason"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2452
    const-class v0, Lcom/twitter/model/core/s;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/s;

    move-object v5, v0

    move-object v0, v1

    move-object v1, v6

    goto :goto_1

    .line 2454
    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    move-object v1, v6

    .line 2456
    goto :goto_1

    .line 2459
    :pswitch_2
    const-string/jumbo v0, "connections"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2460
    const-class v0, Lcom/twitter/model/core/TwitterUser;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->d(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    move-object v1, v6

    goto :goto_1

    .line 2462
    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    move-object v1, v6

    .line 2464
    goto :goto_1

    .line 2467
    :pswitch_3
    const-string/jumbo v0, "token"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2468
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v3

    move-object v0, v1

    move-object v1, v6

    goto :goto_1

    .line 2479
    :cond_4
    if-eqz v6, :cond_5

    .line 2480
    if-eqz v1, :cond_6

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2481
    new-instance v0, Lcom/twitter/model/core/TwitterSocialProof$a;

    invoke-direct {v0}, Lcom/twitter/model/core/TwitterSocialProof$a;-><init>()V

    const/16 v7, 0x28

    .line 2482
    invoke-virtual {v0, v7}, Lcom/twitter/model/core/TwitterSocialProof$a;->a(I)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v7

    .line 2483
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    invoke-virtual {v7, v0}, Lcom/twitter/model/core/TwitterSocialProof$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    .line 2484
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/TwitterSocialProof$a;->d(I)Lcom/twitter/model/core/TwitterSocialProof$a;

    move-result-object v0

    .line 2485
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterSocialProof$a;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/TwitterSocialProof;

    .line 2486
    new-instance v0, Lcom/twitter/model/search/TwitterUserMetadata;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/model/search/TwitterUserMetadata;-><init>(Lcom/twitter/model/core/TwitterSocialProof;Ljava/lang/String;Ljava/lang/String;ZLcom/twitter/model/core/s;)V

    iput-object v0, v6, Lcom/twitter/model/core/TwitterUser;->T:Lcom/twitter/model/search/TwitterUserMetadata;

    .line 2493
    :cond_5
    :goto_2
    return-object v6

    .line 2487
    :cond_6
    if-eqz v3, :cond_7

    .line 2488
    new-instance v0, Lcom/twitter/model/search/TwitterUserMetadata;

    move-object v1, v2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/model/search/TwitterUserMetadata;-><init>(Lcom/twitter/model/core/TwitterSocialProof;Ljava/lang/String;Ljava/lang/String;ZLcom/twitter/model/core/s;)V

    iput-object v0, v6, Lcom/twitter/model/core/TwitterUser;->T:Lcom/twitter/model/search/TwitterUserMetadata;

    goto :goto_2

    .line 2489
    :cond_7
    if-eqz v5, :cond_5

    .line 2490
    new-instance v0, Lcom/twitter/model/search/TwitterUserMetadata;

    move-object v1, v2

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/model/search/TwitterUserMetadata;-><init>(Lcom/twitter/model/core/TwitterSocialProof;Ljava/lang/String;Ljava/lang/String;ZLcom/twitter/model/core/s;)V

    iput-object v0, v6, Lcom/twitter/model/core/TwitterUser;->T:Lcom/twitter/model/search/TwitterUserMetadata;

    goto :goto_2

    .line 2447
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static w(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2497
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2500
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 2501
    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    .line 2502
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    .line 2503
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 2504
    invoke-static {p0}, Lcom/twitter/library/api/z;->v(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 2505
    if-eqz v0, :cond_0

    .line 2506
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2509
    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 2511
    :cond_1
    return-object v1
.end method

.method public static x(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2559
    const/4 v1, 0x0

    .line 2560
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    .line 2561
    :goto_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    .line 2562
    sget-object v2, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 2578
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    .line 2564
    :pswitch_1
    const-string/jumbo v1, "suggestion"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2565
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2571
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    .line 2580
    :cond_1
    return-object v0

    .line 2562
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static y(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/core/JsonParser;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2592
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2594
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 2595
    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    .line 2596
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    .line 2597
    invoke-static {p0}, Lcom/twitter/library/api/z;->x(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v0

    .line 2598
    if-eqz v0, :cond_0

    .line 2599
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2602
    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 2605
    :cond_1
    return-object v1
.end method

.method public static z(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/ActivitySummary;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2609
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 2616
    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_b

    move-object v1, v6

    move-object v2, v6

    move-object v3, v6

    move-object v4, v6

    .line 2618
    :goto_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v5

    .line 2620
    sget-object v0, Lcom/twitter/library/api/z$1;->a:[I

    invoke-virtual {v5}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v7

    aget v0, v0, v7

    packed-switch v0, :pswitch_data_0

    :cond_0
    :pswitch_0
    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    .line 2672
    :goto_1
    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v4, v5, :cond_a

    .line 2674
    :goto_2
    new-instance v4, Lcom/twitter/library/api/ActivitySummary;

    invoke-direct {v4, v3, v2, v1, v0}, Lcom/twitter/library/api/ActivitySummary;-><init>(Ljava/lang/String;Ljava/lang/String;[J[J)V

    return-object v4

    .line 2622
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 2623
    const-string/jumbo v7, "favoriters_count"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2624
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    move-object v4, v5

    move-object v10, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v1

    move-object v1, v10

    goto :goto_1

    .line 2625
    :cond_1
    const-string/jumbo v7, "retweeters_count"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2626
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()Ljava/lang/String;

    move-result-object v0

    move-object v3, v4

    move-object v4, v5

    move-object v10, v0

    move-object v0, v1

    move-object v1, v2

    move-object v2, v10

    goto :goto_1

    .line 2631
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 2632
    const-string/jumbo v7, "favoriters"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2633
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v2, v5

    .line 2634
    :goto_3
    if-eqz v2, :cond_3

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v5, :cond_3

    .line 2635
    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->i:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v2, v5, :cond_2

    .line 2636
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->j()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2638
    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_3

    .line 2640
    :cond_3
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v0

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    move-object v11, v3

    move-object v3, v4

    move-object v4, v2

    move-object v2, v11

    .line 2641
    goto :goto_1

    :cond_4
    const-string/jumbo v7, "retweeters"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2642
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v1, v5

    .line 2643
    :goto_4
    if-eqz v1, :cond_6

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v5, :cond_6

    .line 2644
    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->i:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v5, :cond_5

    .line 2645
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->j()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2647
    :cond_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_4

    .line 2649
    :cond_6
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v0

    move-object v10, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v1

    move-object v1, v10

    .line 2650
    goto/16 :goto_1

    .line 2651
    :cond_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    .line 2653
    goto/16 :goto_1

    .line 2656
    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v0

    .line 2657
    const-string/jumbo v7, "ext"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2658
    const-class v0, Lcom/twitter/model/stratostore/f;

    invoke-static {p0, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/stratostore/f;

    .line 2659
    if-eqz v0, :cond_9

    const-class v7, Lcom/twitter/model/stratostore/g;

    .line 2660
    invoke-virtual {v0, v7}, Lcom/twitter/model/stratostore/f;->a(Ljava/lang/Class;)Lcom/twitter/model/stratostore/e$b;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/stratostore/g;

    .line 2661
    :goto_5
    if-eqz v0, :cond_8

    .line 2662
    iget-wide v8, v0, Lcom/twitter/model/stratostore/g;->a:J

    .line 2665
    :cond_8
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    .line 2666
    goto/16 :goto_1

    :cond_9
    move-object v0, v6

    .line 2660
    goto :goto_5

    :cond_a
    move-object v4, v3

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    goto/16 :goto_0

    :cond_b
    move-object v0, v6

    move-object v1, v6

    move-object v2, v6

    move-object v3, v6

    goto/16 :goto_2

    .line 2620
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
