.class public Lcom/twitter/library/api/o;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/api/o$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:J

.field public final d:Ljava/lang/String;

.field public final e:Z

.field public final f:I

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public final m:Ljava/lang/String;

.field public final n:Ljava/lang/String;

.field public final o:Ljava/lang/String;

.field public final p:J


# direct methods
.method private constructor <init>(Lcom/twitter/library/api/o$a;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lcom/twitter/library/api/o$a;->a(Lcom/twitter/library/api/o$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/o;->a:Ljava/lang/String;

    .line 33
    invoke-static {p1}, Lcom/twitter/library/api/o$a;->b(Lcom/twitter/library/api/o$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/o;->b:Ljava/lang/String;

    .line 34
    invoke-static {p1}, Lcom/twitter/library/api/o$a;->c(Lcom/twitter/library/api/o$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/o;->c:J

    .line 35
    invoke-static {p1}, Lcom/twitter/library/api/o$a;->d(Lcom/twitter/library/api/o$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/o;->d:Ljava/lang/String;

    .line 36
    invoke-static {p1}, Lcom/twitter/library/api/o$a;->e(Lcom/twitter/library/api/o$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/api/o;->e:Z

    .line 37
    invoke-static {p1}, Lcom/twitter/library/api/o$a;->f(Lcom/twitter/library/api/o$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/o;->f:I

    .line 38
    invoke-static {p1}, Lcom/twitter/library/api/o$a;->g(Lcom/twitter/library/api/o$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/o;->g:Ljava/lang/String;

    .line 39
    invoke-static {p1}, Lcom/twitter/library/api/o$a;->h(Lcom/twitter/library/api/o$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/o;->h:Ljava/lang/String;

    .line 40
    invoke-static {p1}, Lcom/twitter/library/api/o$a;->i(Lcom/twitter/library/api/o$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/o;->i:Ljava/lang/String;

    .line 41
    invoke-static {p1}, Lcom/twitter/library/api/o$a;->j(Lcom/twitter/library/api/o$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/o;->j:Ljava/lang/String;

    .line 42
    invoke-static {p1}, Lcom/twitter/library/api/o$a;->k(Lcom/twitter/library/api/o$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/o;->k:Ljava/lang/String;

    .line 43
    invoke-static {p1}, Lcom/twitter/library/api/o$a;->l(Lcom/twitter/library/api/o$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/o;->l:Ljava/lang/String;

    .line 44
    invoke-static {p1}, Lcom/twitter/library/api/o$a;->m(Lcom/twitter/library/api/o$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/o;->m:Ljava/lang/String;

    .line 45
    invoke-static {p1}, Lcom/twitter/library/api/o$a;->n(Lcom/twitter/library/api/o$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/o;->n:Ljava/lang/String;

    .line 46
    invoke-static {p1}, Lcom/twitter/library/api/o$a;->o(Lcom/twitter/library/api/o$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/o;->o:Ljava/lang/String;

    .line 47
    invoke-static {p1}, Lcom/twitter/library/api/o$a;->p(Lcom/twitter/library/api/o$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/o;->p:J

    .line 48
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/library/api/o$a;Lcom/twitter/library/api/o$1;)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/twitter/library/api/o;-><init>(Lcom/twitter/library/api/o$a;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 52
    if-ne p0, p1, :cond_1

    .line 57
    :cond_0
    :goto_0
    return v0

    .line 53
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 55
    :cond_3
    check-cast p1, Lcom/twitter/library/api/o;

    .line 57
    iget-object v2, p0, Lcom/twitter/library/api/o;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/o;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/twitter/library/api/o;->e:Z

    iget-boolean v3, p1, Lcom/twitter/library/api/o;->e:Z

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/twitter/library/api/o;->f:I

    iget v3, p1, Lcom/twitter/library/api/o;->f:I

    if-ne v2, v3, :cond_4

    iget-wide v2, p0, Lcom/twitter/library/api/o;->c:J

    iget-wide v4, p1, Lcom/twitter/library/api/o;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/api/o;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/o;->a:Ljava/lang/String;

    .line 61
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/api/o;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/o;->d:Ljava/lang/String;

    .line 62
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/api/o;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/o;->g:Ljava/lang/String;

    .line 63
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/api/o;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/o;->h:Ljava/lang/String;

    .line 64
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/api/o;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/o;->i:Ljava/lang/String;

    .line 65
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/api/o;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/o;->j:Ljava/lang/String;

    .line 66
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/api/o;->k:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/o;->k:Ljava/lang/String;

    .line 67
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/api/o;->l:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/o;->l:Ljava/lang/String;

    .line 68
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/api/o;->m:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/o;->m:Ljava/lang/String;

    .line 69
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/api/o;->n:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/o;->n:Ljava/lang/String;

    .line 70
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/api/o;->o:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/o;->o:Ljava/lang/String;

    .line 71
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-wide v2, p0, Lcom/twitter/library/api/o;->p:J

    .line 72
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p1, Lcom/twitter/library/api/o;->p:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 77
    iget-object v0, p0, Lcom/twitter/library/api/o;->a:Ljava/lang/String;

    const/16 v1, 0xf

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/library/api/o;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/twitter/library/api/o;->c:J

    .line 79
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/twitter/library/api/o;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/twitter/library/api/o;->g:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/twitter/library/api/o;->h:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/twitter/library/api/o;->i:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/twitter/library/api/o;->j:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/twitter/library/api/o;->k:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-object v3, p0, Lcom/twitter/library/api/o;->l:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    iget-object v3, p0, Lcom/twitter/library/api/o;->m:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    iget-object v3, p0, Lcom/twitter/library/api/o;->n:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    iget-boolean v3, p0, Lcom/twitter/library/api/o;->e:Z

    .line 89
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xc

    iget v3, p0, Lcom/twitter/library/api/o;->f:I

    .line 90
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xd

    iget-object v3, p0, Lcom/twitter/library/api/o;->o:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    iget-wide v4, p0, Lcom/twitter/library/api/o;->p:J

    .line 92
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 77
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;[Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
