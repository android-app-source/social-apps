.class public final Lcom/twitter/library/api/s$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/api/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/library/api/s;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/y;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/revenue/a;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/twitter/model/timeline/q;

.field d:Lcom/twitter/model/timeline/u;

.field e:Lcom/twitter/model/livevideo/b;

.field f:Z

.field g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 49
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/s$a;->a:Ljava/util/List;

    .line 51
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/s$a;->b:Ljava/util/List;

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/library/api/s$a;->g:I

    return-void
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/twitter/library/api/s$a;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/s$a;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Lcom/twitter/library/api/s$a;
    .locals 0

    .prologue
    .line 103
    iput p1, p0, Lcom/twitter/library/api/s$a;->g:I

    .line 104
    return-object p0
.end method

.method public a(Lcom/twitter/model/livevideo/b;)Lcom/twitter/library/api/s$a;
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/twitter/library/api/s$a;->e:Lcom/twitter/model/livevideo/b;

    .line 88
    return-object p0
.end method

.method public a(Lcom/twitter/model/timeline/q;)Lcom/twitter/library/api/s$a;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/twitter/library/api/s$a;->c:Lcom/twitter/model/timeline/q;

    .line 76
    return-object p0
.end method

.method public a(Lcom/twitter/model/timeline/u;)Lcom/twitter/library/api/s$a;
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/twitter/library/api/s$a;->d:Lcom/twitter/model/timeline/u;

    .line 82
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/library/api/s$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/timeline/y;",
            ">;)",
            "Lcom/twitter/library/api/s$a;"
        }
    .end annotation

    .prologue
    .line 63
    iput-object p1, p0, Lcom/twitter/library/api/s$a;->a:Ljava/util/List;

    .line 64
    return-object p0
.end method

.method public a(Z)Lcom/twitter/library/api/s$a;
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/twitter/library/api/s$a;->f:Z

    .line 94
    return-object p0
.end method

.method public b(Ljava/util/List;)Lcom/twitter/library/api/s$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/revenue/a;",
            ">;)",
            "Lcom/twitter/library/api/s$a;"
        }
    .end annotation

    .prologue
    .line 69
    iput-object p1, p0, Lcom/twitter/library/api/s$a;->b:Ljava/util/List;

    .line 70
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/twitter/library/api/s$a;->e()Lcom/twitter/library/api/s;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/library/api/s;
    .locals 1

    .prologue
    .line 115
    new-instance v0, Lcom/twitter/library/api/s;

    invoke-direct {v0, p0}, Lcom/twitter/library/api/s;-><init>(Lcom/twitter/library/api/s$a;)V

    return-object v0
.end method
