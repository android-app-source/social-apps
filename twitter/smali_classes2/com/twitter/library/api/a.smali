.class public abstract Lcom/twitter/library/api/a;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Landroid/content/Context;Ljava/lang/String;JZLcom/twitter/library/service/u;Z)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 4

    .prologue
    .line 54
    if-eqz p4, :cond_1

    .line 55
    const-string/jumbo v0, "success"

    .line 62
    :goto_0
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1, p2, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 64
    invoke-virtual {p5}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v1

    .line 65
    invoke-virtual {p5}, Lcom/twitter/library/service/u;->f()Lcom/twitter/network/HttpOperation;

    move-result-object v2

    .line 66
    if-eqz v1, :cond_0

    .line 67
    invoke-static {v0, v1}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/network/l;)V

    .line 68
    invoke-virtual {v2}, Lcom/twitter/network/HttpOperation;->i()Ljava/net/URI;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/model/ScribeLog;Ljava/lang/String;Lcom/twitter/network/l;)V

    .line 71
    :cond_0
    return-object v0

    .line 56
    :cond_1
    if-eqz p6, :cond_2

    .line 57
    const-string/jumbo v0, "retry"

    goto :goto_0

    .line 59
    :cond_2
    const-string/jumbo v0, "failure"

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;JZLcom/twitter/async/service/j;ZLjava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "JZ",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 34
    if-eqz p1, :cond_0

    if-eqz p5, :cond_0

    invoke-virtual {p5}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 45
    :cond_0
    :goto_0
    return-void

    .line 37
    :cond_1
    invoke-virtual {p5}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/library/service/u;

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    move v6, p6

    invoke-static/range {v0 .. v6}, Lcom/twitter/library/api/a;->a(Landroid/content/Context;Ljava/lang/String;JZLcom/twitter/library/service/u;Z)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 40
    invoke-virtual {p5}, Lcom/twitter/async/service/j;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    .line 41
    invoke-static {p7}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 42
    invoke-virtual {v0, p7}, Lcom/twitter/analytics/feature/model/ClientEventLog;->k(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 44
    :cond_2
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0
.end method
