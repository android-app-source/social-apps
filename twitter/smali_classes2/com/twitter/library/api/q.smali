.class public Lcom/twitter/library/api/q;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/y;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/twitter/library/api/q;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 40
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Z)Lcom/twitter/library/api/q;
    .locals 3

    .prologue
    .line 45
    new-instance v0, Lcom/twitter/library/api/q;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/api/q;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const-string/jumbo v1, "format"

    .line 46
    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/api/q;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v0

    const-string/jumbo v1, "has_unknown_phone_number"

    .line 47
    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/s;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/s;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/q;

    .line 48
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 49
    if-eqz v1, :cond_0

    .line 50
    const-string/jumbo v2, "lang"

    .line 51
    invoke-static {v1}, Lcom/twitter/util/b;->b(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 50
    invoke-virtual {v0, v2, v1}, Lcom/twitter/library/api/q;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/s;

    .line 53
    :cond_0
    return-object v0
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 59
    iget-object v0, p0, Lcom/twitter/library/api/q;->p:Landroid/content/Context;

    .line 60
    iget-object v1, p0, Lcom/twitter/library/api/q;->o:Landroid/os/Bundle;

    .line 61
    const-string/jumbo v2, "phone"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 62
    const-string/jumbo v3, "format"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 63
    const-string/jumbo v4, "lang"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 64
    invoke-virtual {p0}, Lcom/twitter/library/api/q;->M()Lcom/twitter/library/service/v;

    move-result-object v5

    iget-wide v6, v5, Lcom/twitter/library/service/v;->c:J

    .line 66
    invoke-virtual {p0}, Lcom/twitter/library/api/q;->J()Lcom/twitter/library/service/d$a;

    move-result-object v5

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const-string/jumbo v9, "prompts"

    aput-object v9, v8, v10

    const-string/jumbo v9, "suggest"

    aput-object v9, v8, v11

    .line 67
    invoke-virtual {v5, v8}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v5

    const-string/jumbo v8, "format"

    .line 68
    invoke-virtual {v5, v8, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v3

    const-string/jumbo v5, "client_namespace"

    const-string/jumbo v8, "native"

    .line 69
    invoke-virtual {v3, v5, v8}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v3

    const-string/jumbo v5, "force_user_language"

    .line 70
    invoke-virtual {v3, v5, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v3

    const-string/jumbo v4, "has_unknown_phone_number"

    .line 71
    invoke-virtual {v3, v4, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v2

    const-string/jumbo v3, "notifications_device"

    .line 72
    invoke-static {v0}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;)Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v2

    const-string/jumbo v3, "notifications_twitter"

    .line 73
    invoke-static {v6, v7}, Lcom/twitter/library/platform/notifications/PushRegistration;->a(J)I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v2

    const-string/jumbo v3, "notifications_app"

    .line 75
    invoke-static {v0, v6, v7}, Lcom/twitter/library/platform/notifications/PushRegistration;->b(Landroid/content/Context;J)Z

    move-result v4

    .line 74
    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v2

    .line 77
    invoke-static {v0}, Lcom/twitter/util/u;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    const-string/jumbo v0, "no_play_store"

    invoke-virtual {v2, v0, v11}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 81
    :cond_0
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 82
    iget-object v0, p0, Lcom/twitter/library/api/q;->p:Landroid/content/Context;

    const-string/jumbo v3, "debug_prefs"

    .line 83
    invoke-virtual {v0, v3, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 85
    const-string/jumbo v0, "force_campaign"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 86
    if-nez v0, :cond_1

    const-string/jumbo v1, "pb_force_campaign_enabled"

    .line 87
    invoke-interface {v3, v1, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 88
    const-string/jumbo v0, "pb_force_campaign_id"

    const/4 v1, 0x0

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 91
    :cond_1
    if-eqz v0, :cond_2

    .line 92
    const-string/jumbo v1, "pb_force_campaign_cookie"

    const-string/jumbo v4, ""

    .line 93
    invoke-interface {v3, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 94
    const-string/jumbo v4, "pb_force_campaign_sticky"

    .line 95
    invoke-interface {v3, v4, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 96
    const-string/jumbo v5, "force_campaign"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "targeting_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 97
    const-string/jumbo v0, "force_fatigue_on_override"

    invoke-virtual {v2, v0, v11}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 98
    const-string/jumbo v0, "force_cookie"

    invoke-virtual {v2, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 99
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "pb_force_campaign_enabled"

    .line 100
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 99
    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 101
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 104
    :cond_2
    invoke-virtual {v2}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V
    .locals 4

    .prologue
    .line 115
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    invoke-virtual {p3}, Lcom/twitter/library/api/y;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/n;

    .line 117
    iget-object v1, p2, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "prompt"

    sget-object v3, Lcom/twitter/model/timeline/n;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v1, v2, v0, v3}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    .line 119
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 29
    check-cast p3, Lcom/twitter/library/api/y;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/api/q;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/api/y;
    .locals 1

    .prologue
    .line 109
    const/16 v0, 0x29

    invoke-static {v0}, Lcom/twitter/library/api/y;->a(I)Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/twitter/library/api/q;->b()Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method
