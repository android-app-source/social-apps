.class public Lcom/twitter/library/api/p;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/y;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/twitter/library/service/v;

    invoke-direct {v0, p2}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/api/p;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/twitter/library/api/p;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 26
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;II)Lcom/twitter/library/api/p;
    .locals 4

    .prologue
    .line 31
    new-instance v0, Lcom/twitter/library/api/p;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/api/p;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 32
    invoke-virtual {v0, p2}, Lcom/twitter/library/api/p;->g(I)Lcom/twitter/library/service/s;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    .line 33
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/s;->b(Ljava/lang/String;J)Lcom/twitter/library/service/s;

    move-result-object v0

    const-string/jumbo v1, "prompt_id"

    .line 34
    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/s;->a(Ljava/lang/String;I)Lcom/twitter/library/service/s;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/p;

    .line 31
    return-object v0
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 8

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/twitter/library/api/p;->L()I

    move-result v0

    .line 41
    iget-object v1, p0, Lcom/twitter/library/api/p;->o:Landroid/os/Bundle;

    .line 42
    const-string/jumbo v2, "user_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 43
    const-string/jumbo v4, "prompt_id"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 45
    invoke-virtual {p0}, Lcom/twitter/library/api/p;->J()Lcom/twitter/library/service/d$a;

    move-result-object v4

    sget-object v5, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 46
    invoke-virtual {v4, v5}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "prompts"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "record_event"

    aput-object v7, v5, v6

    .line 47
    invoke-virtual {v4, v5}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v4

    const-string/jumbo v5, "user_id"

    .line 48
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v2

    const-string/jumbo v3, "prompt_id"

    .line 49
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 51
    packed-switch v0, :pswitch_data_0

    .line 65
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Invalid action "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 53
    :pswitch_0
    const-string/jumbo v0, "action"

    const-string/jumbo v2, "acted_on"

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 68
    :goto_0
    invoke-virtual {v1}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0

    .line 57
    :pswitch_1
    const-string/jumbo v0, "action"

    const-string/jumbo v2, "dismissed"

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto :goto_0

    .line 61
    :pswitch_2
    const-string/jumbo v0, "action"

    const-string/jumbo v2, "shown"

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto :goto_0

    .line 51
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected b()Lcom/twitter/library/api/y;
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/twitter/library/api/p;->b()Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method
