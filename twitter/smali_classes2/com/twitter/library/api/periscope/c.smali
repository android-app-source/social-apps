.class public Lcom/twitter/library/api/periscope/c;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Lcgk;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/model/json/periscope/JsonPeriscopeAuthConsentState;

.field private final b:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Z)V
    .locals 2

    .prologue
    .line 33
    const-string/jumbo v0, "UpdatePeriscopeConsent"

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 34
    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/periscope/c;->b:J

    .line 35
    new-instance v0, Lcom/twitter/model/json/periscope/JsonPeriscopeAuthConsentState;

    invoke-direct {v0}, Lcom/twitter/model/json/periscope/JsonPeriscopeAuthConsentState;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/api/periscope/c;->a:Lcom/twitter/model/json/periscope/JsonPeriscopeAuthConsentState;

    .line 36
    iget-object v0, p0, Lcom/twitter/library/api/periscope/c;->a:Lcom/twitter/model/json/periscope/JsonPeriscopeAuthConsentState;

    iput-boolean p3, v0, Lcom/twitter/model/json/periscope/JsonPeriscopeAuthConsentState;->a:Z

    .line 37
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 9

    .prologue
    .line 42
    const/4 v1, 0x0

    .line 44
    :try_start_0
    new-instance v0, Lcom/twitter/network/apache/entity/c;

    iget-object v2, p0, Lcom/twitter/library/api/periscope/c;->a:Lcom/twitter/model/json/periscope/JsonPeriscopeAuthConsentState;

    invoke-static {v2}, Lcom/twitter/model/json/common/g;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "UTF-8"

    invoke-direct {v0, v2, v3}, Lcom/twitter/network/apache/entity/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    :try_start_1
    const-string/jumbo v1, "application/json"

    invoke-virtual {v0, v1}, Lcom/twitter/network/apache/entity/c;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 49
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/library/api/periscope/c;->J()Lcom/twitter/library/service/d$a;

    move-result-object v1

    sget-object v2, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 50
    invoke-virtual {v1, v2}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "strato/column/User/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lcom/twitter/library/api/periscope/c;->b:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "/periscope/authConsent"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 51
    invoke-virtual {v1, v2}, Lcom/twitter/library/service/d$a;->b([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 52
    invoke-virtual {v1, v0}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/apache/e;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    .line 49
    return-object v0

    .line 46
    :catch_0
    move-exception v0

    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    .line 47
    :goto_1
    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 46
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method protected b()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Lcgk;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    const-class v0, Lcgk;

    invoke-static {v0}, Lcom/twitter/library/api/k;->a(Ljava/lang/Class;)Lcom/twitter/library/api/k;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/twitter/library/api/periscope/c;->b()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
