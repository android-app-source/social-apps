.class public abstract Lcom/twitter/library/api/r;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/library/service/c;",
        ">",
        "Lcom/twitter/library/service/b",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/library/service/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/library/service/f",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/twitter/library/service/v;

    invoke-direct {v0, p3}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/api/r;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 35
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;Lcom/twitter/async/service/k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/twitter/library/client/Session;",
            "Lcom/twitter/async/service/k",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Lcom/twitter/library/service/v;

    invoke-direct {v0, p3}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/api/r;->a:Ljava/util/Map;

    .line 50
    new-instance v0, Lcom/twitter/library/service/f;

    invoke-direct {v0}, Lcom/twitter/library/service/f;-><init>()V

    .line 51
    invoke-virtual {v0, p4}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/r;->b:Lcom/twitter/library/service/f;

    .line 52
    iget-object v0, p0, Lcom/twitter/library/api/r;->b:Lcom/twitter/library/service/f;

    invoke-virtual {p0, v0}, Lcom/twitter/library/api/r;->a(Lcom/twitter/async/service/k;)Lcom/twitter/async/service/AsyncOperation;

    .line 53
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V
    .locals 3

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/api/r;->a:Ljava/util/Map;

    .line 60
    new-instance v0, Lcom/twitter/library/service/f;

    invoke-direct {v0}, Lcom/twitter/library/service/f;-><init>()V

    new-instance v1, Lcom/twitter/library/service/l;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/twitter/library/service/l;-><init>(I)V

    .line 61
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/service/g;

    invoke-direct {v1, p1}, Lcom/twitter/library/service/g;-><init>(Landroid/content/Context;)V

    .line 62
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/r;->b:Lcom/twitter/library/service/f;

    .line 63
    iget-object v0, p0, Lcom/twitter/library/api/r;->b:Lcom/twitter/library/service/f;

    invoke-virtual {p0, v0}, Lcom/twitter/library/api/r;->a(Lcom/twitter/async/service/k;)Lcom/twitter/async/service/AsyncOperation;

    .line 64
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/async/service/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/twitter/library/api/r;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 89
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/library/api/r;->a(Lcom/twitter/async/service/j;Z)V

    .line 91
    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/async/service/j;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lcom/twitter/library/api/r;->p:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/library/api/r;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/library/api/r;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/service/v;->c:J

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/r;->d(Lcom/twitter/async/service/j;)Z

    move-result v4

    iget-object v5, p0, Lcom/twitter/library/api/r;->a:Ljava/util/Map;

    .line 102
    invoke-static {v5}, Lmg;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v7

    move-object v5, p1

    move v6, p2

    .line 101
    invoke-static/range {v0 .. v7}, Lcom/twitter/library/api/a;->a(Landroid/content/Context;Ljava/lang/String;JZLcom/twitter/async/service/j;ZLjava/lang/String;)V

    .line 103
    return-void
.end method

.method protected a(Lcom/twitter/library/service/e;)V
    .locals 1

    .prologue
    .line 67
    if-eqz p1, :cond_0

    .line 68
    iget-object v0, p0, Lcom/twitter/library/api/r;->b:Lcom/twitter/library/service/f;

    invoke-virtual {v0, p1}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    .line 70
    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/twitter/library/api/r;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    return-void
.end method

.method public b(Lcom/twitter/async/service/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 80
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/twitter/library/api/r;->a(Lcom/twitter/async/service/j;Z)V

    .line 81
    return-void
.end method

.method protected d(Lcom/twitter/async/service/j;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 111
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    return v0
.end method

.method public abstract e()Ljava/lang/String;
.end method
