.class public final Lcom/twitter/library/api/dm/q$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/api/dm/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/library/api/dm/q;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/twitter/library/client/Session;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Lcom/twitter/model/core/r;

.field private g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/twitter/model/drafts/DraftAttachment;

.field private i:Ljava/lang/String;

.field private j:Lccl;

.field private k:Lcdu;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 212
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/api/dm/q$a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/twitter/library/api/dm/q$a;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/library/api/dm/q$a;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/twitter/library/api/dm/q$a;->b:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/library/api/dm/q$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/twitter/library/api/dm/q$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/library/api/dm/q$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/twitter/library/api/dm/q$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/library/api/dm/q$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/twitter/library/api/dm/q$a;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/library/api/dm/q$a;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/twitter/library/api/dm/q$a;->g:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/library/api/dm/q$a;)Lcom/twitter/model/drafts/DraftAttachment;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/twitter/library/api/dm/q$a;->h:Lcom/twitter/model/drafts/DraftAttachment;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/library/api/dm/q$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/twitter/library/api/dm/q$a;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/library/api/dm/q$a;)Lccl;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/twitter/library/api/dm/q$a;->j:Lccl;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/library/api/dm/q$a;)Lcdu;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/twitter/library/api/dm/q$a;->k:Lcdu;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/library/api/dm/q$a;)Lcom/twitter/model/core/r;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/twitter/library/api/dm/q$a;->f:Lcom/twitter/model/core/r;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 293
    invoke-super {p0}, Lcom/twitter/util/object/i;->R_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/dm/q$a;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/dm/q$a;->b:Lcom/twitter/library/client/Session;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/dm/q$a;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)Lcom/twitter/library/api/dm/q$a;
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lcom/twitter/library/api/dm/q$a;->a:Landroid/content/Context;

    .line 228
    return-object p0
.end method

.method public a(Lccl;)Lcom/twitter/library/api/dm/q$a;
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lcom/twitter/library/api/dm/q$a;->j:Lccl;

    .line 282
    return-object p0
.end method

.method public a(Lcdu;)Lcom/twitter/library/api/dm/q$a;
    .locals 0

    .prologue
    .line 287
    iput-object p1, p0, Lcom/twitter/library/api/dm/q$a;->k:Lcdu;

    .line 288
    return-object p0
.end method

.method public a(Lcom/twitter/library/client/Session;)Lcom/twitter/library/api/dm/q$a;
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lcom/twitter/library/api/dm/q$a;->b:Lcom/twitter/library/client/Session;

    .line 240
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/r;)Lcom/twitter/library/api/dm/q$a;
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lcom/twitter/library/api/dm/q$a;->f:Lcom/twitter/model/core/r;

    .line 264
    return-object p0
.end method

.method public a(Lcom/twitter/model/drafts/DraftAttachment;)Lcom/twitter/library/api/dm/q$a;
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Lcom/twitter/library/api/dm/q$a;->h:Lcom/twitter/model/drafts/DraftAttachment;

    .line 276
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/api/dm/q$a;
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Lcom/twitter/library/api/dm/q$a;->i:Ljava/lang/String;

    .line 234
    return-object p0
.end method

.method public a(Ljava/util/Set;)Lcom/twitter/library/api/dm/q$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/twitter/library/api/dm/q$a;"
        }
    .end annotation

    .prologue
    .line 269
    iput-object p1, p0, Lcom/twitter/library/api/dm/q$a;->g:Ljava/util/Set;

    .line 270
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/library/api/dm/q$a;
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/twitter/library/api/dm/q$a;->c:Ljava/lang/String;

    .line 246
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/q$a;->e()Lcom/twitter/library/api/dm/q;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/library/api/dm/q$a;
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, Lcom/twitter/library/api/dm/q$a;->d:Ljava/lang/String;

    .line 252
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/library/api/dm/q$a;
    .locals 0

    .prologue
    .line 257
    iput-object p1, p0, Lcom/twitter/library/api/dm/q$a;->e:Ljava/lang/String;

    .line 258
    return-object p0
.end method

.method protected e()Lcom/twitter/library/api/dm/q;
    .locals 2

    .prologue
    .line 299
    new-instance v0, Lcom/twitter/library/api/dm/q;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/api/dm/q;-><init>(Lcom/twitter/library/api/dm/q$a;Lcom/twitter/library/api/dm/q$1;)V

    return-object v0
.end method
