.class public Lcom/twitter/library/api/dm/j;
.super Lcom/twitter/library/api/dm/c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/api/dm/c",
        "<",
        "Lcom/twitter/library/api/dm/d;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/twitter/library/api/dm/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/api/dm/j;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/twitter/library/api/dm/j;->a:Ljava/lang/String;

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/dm/c;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/twitter/library/api/dm/j;->a:Ljava/lang/String;

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/dm/c;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 36
    return-void
.end method


# virtual methods
.method a(Lcom/twitter/model/dms/k;Lcom/twitter/library/provider/t;Laut;)V
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x1

    invoke-virtual {p2, p1, v0, p3}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/model/dms/p;ZLaut;)V

    .line 120
    return-void
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/dm/d;)V
    .locals 12

    .prologue
    const-wide/16 v6, 0x0

    const/4 v3, 0x0

    .line 81
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {p3}, Lcom/twitter/library/api/dm/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/k;

    .line 83
    if-eqz v0, :cond_0

    .line 84
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/j;->S()Laut;

    move-result-object v9

    .line 85
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/j;->M()Lcom/twitter/library/service/v;

    move-result-object v1

    iget-wide v4, v1, Lcom/twitter/library/service/v;->c:J

    .line 86
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/j;->R()Lcom/twitter/library/provider/t;

    move-result-object v1

    .line 88
    invoke-virtual {v0}, Lcom/twitter/model/dms/k;->f()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 105
    :goto_0
    invoke-virtual {v9}, Laut;->a()V

    .line 108
    const/16 v2, 0xc

    iget-object v8, v0, Lcom/twitter/model/dms/k;->a:Ljava/lang/String;

    invoke-virtual/range {v1 .. v8}, Lcom/twitter/library/provider/t;->a(IIJJLjava/lang/String;)V

    .line 111
    invoke-static {v4, v5}, Lcom/twitter/library/database/dm/a;->b(J)Lcom/twitter/library/database/dm/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/database/dm/a;->a(Lcom/twitter/model/dms/p;)V

    .line 114
    :cond_0
    return-void

    .line 90
    :pswitch_0
    invoke-virtual {p0, v0, v1, v9}, Lcom/twitter/library/api/dm/j;->a(Lcom/twitter/model/dms/k;Lcom/twitter/library/provider/t;Laut;)V

    goto :goto_0

    .line 95
    :pswitch_1
    invoke-virtual {v1, v0, v9}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/model/dms/k;Laut;)V

    .line 96
    const/16 v2, 0xd

    .line 97
    invoke-virtual {v0}, Lcom/twitter/model/dms/k;->a()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    .line 96
    invoke-virtual/range {v1 .. v8}, Lcom/twitter/library/provider/t;->a(IIJJLjava/lang/String;)V

    goto :goto_0

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 26
    check-cast p3, Lcom/twitter/library/api/dm/d;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/api/dm/j;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/dm/d;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/service/d$a;
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 41
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/j;->R()Lcom/twitter/library/provider/t;

    move-result-object v0

    .line 42
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/j;->M()Lcom/twitter/library/service/v;

    move-result-object v1

    iget-wide v2, v1, Lcom/twitter/library/service/v;->c:J

    .line 43
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/j;->J()Lcom/twitter/library/service/d$a;

    move-result-object v1

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const-string/jumbo v5, "dm"

    aput-object v5, v4, v7

    const-string/jumbo v5, "user_updates"

    aput-object v5, v4, v6

    .line 44
    invoke-virtual {v1, v4}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 45
    invoke-virtual {v1}, Lcom/twitter/library/service/d$a;->b()Lcom/twitter/library/service/d$a;

    move-result-object v1

    const-string/jumbo v4, "dm_users"

    .line 46
    invoke-virtual {v1, v4, v6}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v1

    const-string/jumbo v4, "include_groups"

    .line 47
    invoke-virtual {v1, v4, v6}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 48
    invoke-virtual {v1}, Lcom/twitter/library/service/d$a;->c()Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 49
    invoke-virtual {v1}, Lcom/twitter/library/service/d$a;->d()Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 51
    iget-object v4, p0, Lcom/twitter/library/api/dm/j;->p:Landroid/content/Context;

    invoke-static {v4}, Lcom/twitter/library/dm/e;->a(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 52
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/model/dms/k;)V

    .line 53
    iget-object v4, p0, Lcom/twitter/library/api/dm/j;->p:Landroid/content/Context;

    invoke-static {v4}, Lcom/twitter/library/dm/e;->b(Landroid/content/Context;)V

    .line 57
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->k()I

    move-result v4

    const/16 v5, 0x7d0

    if-ge v4, v5, :cond_1

    .line 58
    const/16 v4, 0xc

    invoke-virtual {v0, v4, v7, v2, v3}, Lcom/twitter/library/provider/t;->a(IIJ)Ljava/lang/String;

    move-result-object v4

    .line 59
    invoke-static {v4}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 60
    const-string/jumbo v5, "inbox_min_id"

    const/16 v6, 0xd

    invoke-virtual {v0, v6, v7, v2, v3}, Lcom/twitter/library/provider/t;->a(IIJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v5, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v2, "cursor"

    .line 62
    invoke-virtual {v0, v2, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 65
    :cond_1
    return-object v1
.end method

.method protected e()Lcom/twitter/library/api/dm/d;
    .locals 1

    .prologue
    .line 75
    new-instance v0, Lcom/twitter/library/api/dm/d;

    invoke-direct {v0}, Lcom/twitter/library/api/dm/d;-><init>()V

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/j;->e()Lcom/twitter/library/api/dm/d;

    move-result-object v0

    return-object v0
.end method

.method h()Z
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x1

    return v0
.end method
