.class public Lcom/twitter/library/api/dm/f;
.super Lcom/twitter/library/api/dm/j;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/String;

.field private c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/library/dm/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/api/dm/j;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 43
    invoke-static {p3}, Lcom/twitter/util/object/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/dm/f;->a:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/api/dm/j;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V

    .line 49
    invoke-static {p3}, Lcom/twitter/util/object/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/dm/f;->a:Ljava/lang/String;

    .line 50
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/api/dm/f;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/library/api/dm/f;->c:Ljava/lang/ref/WeakReference;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/library/dm/g;)V
    .locals 1

    .prologue
    .line 71
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/api/dm/f;->c:Ljava/lang/ref/WeakReference;

    .line 72
    return-void
.end method

.method a(Lcom/twitter/model/dms/k;Lcom/twitter/library/provider/t;Laut;)V
    .locals 6

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/library/api/dm/f;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    .line 81
    invoke-virtual {p1}, Lcom/twitter/model/dms/k;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    .line 82
    invoke-virtual {v0}, Lcom/twitter/model/dms/d;->o()I

    move-result v2

    if-nez v2, :cond_1

    .line 83
    invoke-virtual {v0}, Lcom/twitter/model/dms/d;->l()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/model/dms/d;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 84
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v2

    invoke-virtual {v0, v2}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v2, Lcom/twitter/library/api/dm/f$1;

    invoke-direct {v2, p0}, Lcom/twitter/library/api/dm/f$1;-><init>(Lcom/twitter/library/api/dm/f;)V

    .line 85
    invoke-virtual {v0, v2}, Lrx/c;->b(Lrx/i;)Lrx/j;

    goto :goto_0

    .line 94
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/model/dms/d;->o()I

    move-result v2

    const/16 v3, 0xa

    if-ne v2, v3, :cond_0

    .line 95
    iget-object v2, p0, Lcom/twitter/library/api/dm/f;->c:Ljava/lang/ref/WeakReference;

    invoke-static {v2}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v2

    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v3

    invoke-virtual {v2, v3}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/api/dm/f$2;

    invoke-direct {v3, p0, v0, p2}, Lcom/twitter/library/api/dm/f$2;-><init>(Lcom/twitter/library/api/dm/f;Lcom/twitter/model/dms/d;Lcom/twitter/library/provider/t;)V

    invoke-virtual {v2, v3}, Lrx/c;->b(Lrx/i;)Lrx/j;

    goto :goto_0

    .line 115
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/library/api/dm/j;->a(Lcom/twitter/model/dms/k;Lcom/twitter/library/provider/t;Laut;)V

    .line 116
    return-void
.end method

.method protected b()Lcom/twitter/library/service/d$a;
    .locals 3

    .prologue
    .line 55
    invoke-super {p0}, Lcom/twitter/library/api/dm/j;->b()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->c()Lcom/twitter/library/service/d$a;

    move-result-object v1

    .line 58
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    .line 59
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/model/account/UserSettings;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 60
    :goto_0
    invoke-static {v0}, Lcom/twitter/library/dm/d;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/dm/f;->a:Ljava/lang/String;

    .line 61
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    const-string/jumbo v0, "active_conversation_id"

    iget-object v2, p0, Lcom/twitter/library/api/dm/f;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 64
    :cond_0
    return-object v1

    .line 59
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
