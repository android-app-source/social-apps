.class Lcom/twitter/library/api/dm/f$2;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/api/dm/f;->a(Lcom/twitter/model/dms/k;Lcom/twitter/library/provider/t;Laut;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Ljava/lang/ref/WeakReference",
        "<",
        "Lcom/twitter/library/dm/g;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/model/dms/d;

.field final synthetic b:Lcom/twitter/library/provider/t;

.field final synthetic c:Lcom/twitter/library/api/dm/f;


# direct methods
.method constructor <init>(Lcom/twitter/library/api/dm/f;Lcom/twitter/model/dms/d;Lcom/twitter/library/provider/t;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/twitter/library/api/dm/f$2;->c:Lcom/twitter/library/api/dm/f;

    iput-object p2, p0, Lcom/twitter/library/api/dm/f$2;->a:Lcom/twitter/model/dms/d;

    iput-object p3, p0, Lcom/twitter/library/api/dm/f$2;->b:Lcom/twitter/library/provider/t;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 96
    check-cast p1, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/dm/f$2;->a(Ljava/lang/ref/WeakReference;)V

    return-void
.end method

.method public a(Ljava/lang/ref/WeakReference;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/library/dm/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 99
    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/dm/g;

    .line 100
    if-eqz v0, :cond_1

    .line 101
    iget-object v1, p0, Lcom/twitter/library/api/dm/f$2;->a:Lcom/twitter/model/dms/d;

    check-cast v1, Lcom/twitter/model/dms/ae;

    .line 103
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 104
    invoke-virtual {v1}, Lcom/twitter/model/dms/ae;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/Participant;

    .line 105
    iget-wide v4, v1, Lcom/twitter/model/dms/Participant;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 107
    :cond_0
    iget-object v3, p0, Lcom/twitter/library/api/dm/f$2;->b:Lcom/twitter/library/provider/t;

    .line 108
    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v3, v1}, Lcom/twitter/library/provider/t;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    .line 107
    invoke-interface {v0, v1}, Lcom/twitter/library/dm/g;->a(Ljava/util/Collection;)V

    .line 110
    :cond_1
    return-void
.end method
