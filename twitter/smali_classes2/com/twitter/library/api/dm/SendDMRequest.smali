.class public abstract Lcom/twitter/library/api/dm/SendDMRequest;
.super Lcom/twitter/library/api/upload/w;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/api/dm/SendDMRequest$UploadMessageMediaException;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/api/upload/w;",
        "Lcom/twitter/util/q",
        "<",
        "Lcom/twitter/library/api/progress/ProgressUpdatedEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final i:I

.field private static final j:I

.field private static final k:I


# instance fields
.field public a:Lcom/twitter/model/dms/ad;

.field protected final b:Lcom/twitter/library/provider/t;

.field protected final c:Laut;

.field private final l:Lcom/twitter/library/client/Session;

.field private m:Z

.field private n:Ljava/lang/String;

.field private r:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lcom/twitter/model/dms/r;

.field private t:Lcom/twitter/model/drafts/DraftAttachment;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 107
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/twitter/library/api/dm/SendDMRequest;->i:I

    .line 108
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/twitter/library/api/dm/SendDMRequest;->j:I

    .line 109
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/twitter/library/api/dm/SendDMRequest;->k:I

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V
    .locals 5

    .prologue
    .line 136
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/api/upload/w;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 137
    new-instance v0, Lcom/twitter/library/service/f;

    invoke-direct {v0}, Lcom/twitter/library/service/f;-><init>()V

    new-instance v1, Lcom/twitter/library/service/g;

    invoke-direct {v1, p1}, Lcom/twitter/library/service/g;-><init>(Landroid/content/Context;)V

    .line 138
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/service/o;

    sget v2, Lcom/twitter/library/api/dm/SendDMRequest;->i:I

    sget v3, Lcom/twitter/library/api/dm/SendDMRequest;->j:I

    sget v4, Lcom/twitter/library/api/dm/SendDMRequest;->k:I

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/library/service/o;-><init>(III)V

    .line 139
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/f;->a(Lcom/twitter/async/service/k;)Lcom/twitter/library/service/f;

    move-result-object v0

    .line 141
    invoke-virtual {p0, v0}, Lcom/twitter/library/api/dm/SendDMRequest;->a(Lcom/twitter/async/service/k;)Lcom/twitter/async/service/AsyncOperation;

    .line 142
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/SendDMRequest;->R()Lcom/twitter/library/provider/t;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->b:Lcom/twitter/library/provider/t;

    .line 143
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/SendDMRequest;->S()Laut;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->c:Laut;

    .line 144
    iput-object p3, p0, Lcom/twitter/library/api/dm/SendDMRequest;->l:Lcom/twitter/library/client/Session;

    .line 145
    return-void
.end method

.method private a(Lcom/twitter/model/drafts/DraftAttachment;)Lcom/twitter/library/api/upload/f;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/concurrent/ExecutionException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x3

    const/4 v3, 0x1

    .line 548
    new-instance v2, Lcom/twitter/library/api/progress/a;

    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->d:Ljava/lang/String;

    invoke-direct {v2, v3, v0, v5, v3}, Lcom/twitter/library/api/progress/a;-><init>(ILjava/lang/String;IZ)V

    .line 550
    invoke-static {}, Lcom/twitter/library/api/progress/c;->a()Lcom/twitter/library/api/progress/c;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/library/api/dm/SendDMRequest;->d:Ljava/lang/String;

    invoke-virtual {v0, p0, v3}, Lcom/twitter/library/api/progress/c;->a(Lcom/twitter/util/q;Ljava/lang/String;)V

    .line 551
    new-instance v3, Lcom/twitter/library/api/upload/g;

    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->p:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/library/api/dm/SendDMRequest;->M()Lcom/twitter/library/service/v;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lcom/twitter/library/api/upload/g;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V

    .line 554
    iget-object v0, p1, Lcom/twitter/model/drafts/DraftAttachment;->f:Landroid/net/Uri;

    invoke-static {v0}, Lcom/twitter/util/ac;->e(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 555
    iget-object v0, p1, Lcom/twitter/model/drafts/DraftAttachment;->f:Landroid/net/Uri;

    iget-object v1, p1, Lcom/twitter/model/drafts/DraftAttachment;->g:Lcom/twitter/media/model/MediaType;

    sget-object v4, Lcom/twitter/library/api/upload/MediaUsage;->d:Lcom/twitter/library/api/upload/MediaUsage;

    invoke-virtual {v3, v0, v1, v4, v2}, Lcom/twitter/library/api/upload/g;->a(Landroid/net/Uri;Lcom/twitter/media/model/MediaType;Lcom/twitter/library/api/upload/MediaUsage;Lcom/twitter/util/q;)Lcom/twitter/library/api/upload/g$a;

    move-result-object v0

    .line 570
    :goto_0
    invoke-virtual {v0}, Lcom/twitter/library/api/upload/g$a;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/upload/f;

    :goto_1
    return-object v0

    .line 558
    :cond_0
    invoke-virtual {p1, v5}, Lcom/twitter/model/drafts/DraftAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 559
    if-eqz v0, :cond_1

    iget-object v4, p0, Lcom/twitter/library/api/dm/SendDMRequest;->p:Landroid/content/Context;

    .line 560
    invoke-static {v4, v0}, Lbrt;->a(Landroid/content/Context;Lcom/twitter/model/media/EditableMedia;)Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    .line 561
    :goto_2
    if-nez v0, :cond_2

    .line 562
    new-instance v0, Lcom/twitter/library/api/upload/f;

    const/4 v2, 0x0

    new-instance v3, Lcom/twitter/media/util/MediaException;

    const-string/jumbo v4, "media is null or failed to prepare"

    invoke-direct {v3, v4}, Lcom/twitter/media/util/MediaException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/upload/f;-><init>(Lcom/twitter/media/model/MediaFile;ILjava/lang/Exception;)V

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 560
    goto :goto_2

    .line 566
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    sget-object v4, Lcom/twitter/library/api/upload/MediaUsage;->d:Lcom/twitter/library/api/upload/MediaUsage;

    .line 564
    invoke-virtual {v3, v0, v1, v2, v4}, Lcom/twitter/library/api/upload/g;->a(Lcom/twitter/media/model/MediaFile;Ljava/util/List;Lcom/twitter/util/q;Lcom/twitter/library/api/upload/MediaUsage;)Lcom/twitter/library/api/upload/g$a;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/twitter/model/dms/r;Lcci;JLjava/lang/String;Lcom/twitter/library/api/dm/p;Lcch;)Lcom/twitter/network/HttpOperation;
    .locals 9

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/twitter/library/api/dm/SendDMRequest;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "dm"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "new"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/ab;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    .line 244
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 246
    iget-object v1, p0, Lcom/twitter/library/api/dm/SendDMRequest;->p:Landroid/content/Context;

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/api/dm/SendDMRequest;->a(Landroid/content/Context;Ljava/lang/CharSequence;)Lcom/twitter/library/network/k;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 247
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/k;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/t;

    .line 248
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/SendDMRequest;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/library/service/v;->d:Lcom/twitter/model/account/OAuthToken;

    invoke-direct {v1, v2}, Lcom/twitter/library/network/t;-><init>(Lcom/twitter/model/account/OAuthToken;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/k;

    move-result-object v0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    move-object/from16 v7, p7

    .line 249
    invoke-direct/range {v1 .. v7}, Lcom/twitter/library/api/dm/SendDMRequest;->a(Lcom/twitter/model/dms/r;Lcci;JLjava/lang/String;Lcch;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Ljava/util/List;)Lcom/twitter/library/network/k;

    move-result-object v0

    .line 250
    invoke-virtual {v0, p6}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/j;)Lcom/twitter/library/network/k;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/dm/SendDMRequest;->g:Lcom/twitter/async/service/b;

    .line 251
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Lcom/twitter/async/service/b;)Lcom/twitter/library/network/k;

    move-result-object v0

    const/4 v1, 0x0

    .line 252
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Z)Lcom/twitter/library/network/k;

    move-result-object v0

    .line 254
    invoke-virtual {v0}, Lcom/twitter/library/network/k;->a()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->c()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;J)Ljava/lang/Iterable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 618
    new-instance v0, Lbta;

    invoke-virtual {p0}, Lcom/twitter/library/api/dm/SendDMRequest;->R()Lcom/twitter/library/provider/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v1

    invoke-direct {v0, v1}, Lbta;-><init>(Lcom/twitter/database/model/i;)V

    .line 619
    invoke-virtual {v0, p1}, Lbta;->a(Ljava/lang/String;)Lcbi;

    move-result-object v0

    .line 620
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/api/dm/SendDMRequest$1;

    invoke-direct {v2, p0}, Lcom/twitter/library/api/dm/SendDMRequest$1;-><init>(Lcom/twitter/library/api/dm/SendDMRequest;)V

    .line 621
    invoke-static {v0, v2}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Iterable;)Lcom/twitter/util/collection/o;

    move-result-object v1

    .line 629
    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 632
    invoke-virtual {v1}, Lcom/twitter/util/collection/o;->h()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    .line 633
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/o;->d(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 636
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    return-object v0
.end method

.method private a()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 505
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 506
    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->p:Landroid/content/Context;

    const-string/jumbo v1, "debug_prefs"

    .line 507
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 508
    const-string/jumbo v1, "dm_staging_enabled"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 509
    const-string/jumbo v1, "dm_staging_host"

    iget-object v2, p0, Lcom/twitter/library/api/dm/SendDMRequest;->q:Lcom/twitter/library/network/ab;

    iget-object v2, v2, Lcom/twitter/library/network/ab;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 513
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->q:Lcom/twitter/library/network/ab;

    iget-object v0, v0, Lcom/twitter/library/network/ab;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method private a(Lcom/twitter/model/dms/r;Lcci;JLjava/lang/String;Lcch;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/dms/r;",
            "Lcci;",
            "J",
            "Ljava/lang/String;",
            "Lcch;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/network/apache/message/BasicNameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    const/16 v1, 0x8

    new-array v1, v1, [Lcom/twitter/network/apache/message/BasicNameValuePair;

    const/4 v2, 0x0

    new-instance v3, Lcom/twitter/network/apache/message/BasicNameValuePair;

    const-string/jumbo v4, "text"

    .line 262
    invoke-virtual {p1}, Lcom/twitter/model/dms/r;->r()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lcom/twitter/network/apache/message/BasicNameValuePair;

    const-string/jumbo v4, "request_id"

    .line 263
    invoke-virtual {p1}, Lcom/twitter/model/dms/r;->v()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, Lcom/twitter/network/apache/message/BasicNameValuePair;

    const-string/jumbo v4, "include_cards"

    const-string/jumbo v5, "true"

    invoke-direct {v3, v4, v5}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x3

    new-instance v3, Lcom/twitter/network/apache/message/BasicNameValuePair;

    const-string/jumbo v4, "cards_platform"

    const-string/jumbo v5, "Android-12"

    invoke-direct {v3, v4, v5}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x4

    new-instance v3, Lcom/twitter/network/apache/message/BasicNameValuePair;

    const-string/jumbo v4, "dm_users"

    const-string/jumbo v5, "true"

    invoke-direct {v3, v4, v5}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x5

    new-instance v3, Lcom/twitter/network/apache/message/BasicNameValuePair;

    const-string/jumbo v4, "ext"

    const-string/jumbo v5, ","

    sget-object v6, Lcom/twitter/library/api/dm/c;->b:[Ljava/lang/String;

    .line 268
    invoke-static {v5, v6}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-instance v3, Lcom/twitter/network/apache/message/BasicNameValuePair;

    const-string/jumbo v4, "tweet_mode"

    const-string/jumbo v5, "extended"

    invoke-direct {v3, v4, v5}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-instance v3, Lcom/twitter/network/apache/message/BasicNameValuePair;

    const-string/jumbo v4, "include_reply_count"

    const-string/jumbo v5, "true"

    invoke-direct {v3, v4, v5}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    .line 261
    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->b([Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 273
    invoke-virtual {p1}, Lcom/twitter/model/dms/r;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 274
    new-instance v0, Lcom/twitter/network/apache/message/BasicNameValuePair;

    const-string/jumbo v1, "card_uri"

    invoke-virtual {p1}, Lcom/twitter/model/dms/r;->i()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 277
    :cond_0
    if-eqz p6, :cond_1

    .line 278
    new-instance v0, Lcom/twitter/network/apache/message/BasicNameValuePair;

    const-string/jumbo v1, "sticker_id"

    iget-wide v4, p6, Lcch;->c:J

    .line 279
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 282
    :cond_1
    if-eqz p5, :cond_2

    .line 283
    new-instance v0, Lcom/twitter/network/apache/message/BasicNameValuePair;

    const-string/jumbo v1, "media_id"

    invoke-direct {v0, v1, p5}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 286
    :cond_2
    iget-object v0, p1, Lcom/twitter/model/dms/r;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/dm/e;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 287
    new-instance v0, Lcom/twitter/network/apache/message/BasicNameValuePair;

    const-string/jumbo v1, "conversation_id"

    iget-object v3, p1, Lcom/twitter/model/dms/r;->f:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 296
    :goto_0
    if-eqz p2, :cond_3

    iget-wide v0, p2, Lcci;->c:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-lez v0, :cond_3

    .line 297
    new-instance v0, Lcom/twitter/network/apache/message/BasicNameValuePair;

    const-string/jumbo v1, "tweet_id"

    iget-wide v4, p2, Lcci;->c:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 298
    iget-object v0, p2, Lcci;->d:Lcom/twitter/model/core/r;

    iget-object v0, v0, Lcom/twitter/model/core/r;->m:Lcgi;

    .line 299
    if-eqz v0, :cond_3

    .line 300
    new-instance v1, Lcom/twitter/network/apache/message/BasicNameValuePair;

    const-string/jumbo v3, "impression_id"

    iget-object v0, v0, Lcgi;->c:Ljava/lang/String;

    invoke-direct {v1, v3, v0}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 304
    :cond_3
    invoke-virtual {p1}, Lcom/twitter/model/dms/r;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/r$c;

    iget-object v0, v0, Lcom/twitter/model/dms/r$c;->p:Lccl;

    .line 305
    if-eqz v0, :cond_5

    .line 306
    invoke-virtual {v0}, Lccl;->a()Ljava/lang/String;

    move-result-object v3

    const/4 v1, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_4
    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 328
    :cond_5
    :goto_2
    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0

    .line 290
    :cond_6
    iget-object v0, p1, Lcom/twitter/model/dms/r;->f:Ljava/lang/String;

    invoke-direct {p0, v0, p3, p4}, Lcom/twitter/library/api/dm/SendDMRequest;->a(Ljava/lang/String;J)Ljava/lang/Iterable;

    move-result-object v1

    .line 291
    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_7

    const/4 v0, 0x1

    :goto_3
    const-string/jumbo v3, "Must have non-empty participant ids to create a new conversation"

    invoke-static {v0, v3}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 293
    new-instance v0, Lcom/twitter/network/apache/message/BasicNameValuePair;

    const-string/jumbo v3, "recipient_ids"

    const-string/jumbo v4, ","

    invoke-static {v4, v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 291
    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    .line 306
    :sswitch_0
    const-string/jumbo v4, "options"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string/jumbo v4, "text_input"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_2
    const-string/jumbo v4, "unknown"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_3
    const-string/jumbo v4, "location"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v1, 0x3

    goto :goto_1

    .line 308
    :pswitch_0
    check-cast v0, Lccq;

    .line 309
    const/4 v1, 0x2

    new-array v1, v1, [Lcom/twitter/network/apache/message/BasicNameValuePair;

    const/4 v3, 0x0

    new-instance v4, Lcom/twitter/network/apache/message/BasicNameValuePair;

    const-string/jumbo v5, "quick_reply_response[options][id]"

    .line 311
    invoke-virtual {v0}, Lccq;->b()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v1, v3

    const/4 v3, 0x1

    new-instance v4, Lcom/twitter/network/apache/message/BasicNameValuePair;

    const-string/jumbo v5, "quick_reply_response[options][selected_id]"

    .line 313
    invoke-virtual {v0}, Lccq;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v5, v0}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v1, v3

    .line 309
    invoke-virtual {v2, v1}, Lcom/twitter/util/collection/h;->b([Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto/16 :goto_2

    .line 317
    :pswitch_1
    new-instance v1, Lcom/twitter/network/apache/message/BasicNameValuePair;

    const-string/jumbo v3, "quick_reply_response[text_input][id]"

    .line 318
    invoke-virtual {v0}, Lccl;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v3, v0}, Lcom/twitter/network/apache/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    invoke-virtual {v2, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto/16 :goto_2

    .line 306
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7dc155c8 -> :sswitch_1
        -0x4a797962 -> :sswitch_0
        -0x10fa53b6 -> :sswitch_2
        0x714f9fb5 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(ILaut;)V
    .locals 2

    .prologue
    .line 472
    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->s:Lcom/twitter/model/dms/r;

    if-eqz v0, :cond_0

    .line 473
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 474
    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->b:Lcom/twitter/library/provider/t;

    iget-object v1, p0, Lcom/twitter/library/api/dm/SendDMRequest;->s:Lcom/twitter/model/dms/r;

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/model/dms/r;ILaut;)V

    .line 475
    invoke-virtual {p2}, Laut;->a()V

    .line 477
    :cond_0
    return-void
.end method

.method private a(JLcom/twitter/model/drafts/DraftAttachment;Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 11

    .prologue
    const/4 v4, 0x6

    const/4 v8, 0x3

    const/4 v5, 0x1

    const/4 v7, 0x2

    .line 704
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v3, p1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v6, v8, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v9, "app:twitter_service:direct_messages"

    aput-object v9, v6, v2

    if-eqz p5, :cond_2

    const-string/jumbo v2, "retry_dm"

    :goto_0
    aput-object v2, v6, v5

    if-eqz p6, :cond_3

    const-string/jumbo v2, "cancel"

    :goto_1
    aput-object v2, v6, v7

    .line 705
    invoke-virtual {v3, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-string/jumbo v3, "has_media"

    .line 709
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->k(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 710
    invoke-virtual {v2, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(I)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 711
    invoke-virtual {v2, p4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 712
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v3

    invoke-virtual {v3}, Lcrr;->g()Z

    move-result v3

    if-eqz v3, :cond_4

    const-string/jumbo v3, "connected"

    :goto_2
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 714
    iget-boolean v3, p0, Lcom/twitter/library/api/dm/SendDMRequest;->m:Z

    if-eqz v3, :cond_0

    .line 715
    invoke-virtual {v2, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;->d(I)Lcom/twitter/analytics/model/ScribeLog;

    .line 717
    :cond_0
    if-eqz p7, :cond_1

    .line 718
    move-object/from16 v0, p7

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->m(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 720
    :cond_1
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 722
    iget-object v2, p3, Lcom/twitter/model/drafts/DraftAttachment;->g:Lcom/twitter/media/model/MediaType;

    iget-object v6, v2, Lcom/twitter/media/model/MediaType;->extension:Ljava/lang/String;

    if-eqz p6, :cond_5

    :goto_3
    if-eqz p6, :cond_6

    :goto_4
    const/4 v9, 0x0

    move-object v3, p0

    move-wide v4, p1

    invoke-direct/range {v3 .. v9}, Lcom/twitter/library/api/dm/SendDMRequest;->a(JLjava/lang/String;IILcom/twitter/network/l;)V

    .line 727
    return-void

    .line 704
    :cond_2
    const-string/jumbo v2, "send_dm"

    goto :goto_0

    :cond_3
    const-string/jumbo v2, "failure"

    goto :goto_1

    .line 712
    :cond_4
    const-string/jumbo v3, "disconnected"

    goto :goto_2

    :cond_5
    move v7, v5

    .line 722
    goto :goto_3

    :cond_6
    move v8, v4

    goto :goto_4
.end method

.method private a(JLjava/lang/String;IILcom/twitter/network/l;)V
    .locals 7

    .prologue
    const/4 v5, 0x2

    .line 658
    packed-switch p4, :pswitch_data_0

    .line 673
    const-string/jumbo v0, "unknown"

    .line 678
    :goto_0
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1, p1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "app:twitter_service:dm_with_media"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    aput-object v0, v2, v5

    .line 679
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-string/jumbo v1, "has_media"

    .line 680
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->k(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 681
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v1

    invoke-virtual {v1}, Lcrr;->g()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string/jumbo v1, "connected"

    :goto_1
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 683
    iget-boolean v1, p0, Lcom/twitter/library/api/dm/SendDMRequest;->m:Z

    if-eqz v1, :cond_0

    .line 684
    invoke-virtual {v0, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->d(I)Lcom/twitter/analytics/model/ScribeLog;

    .line 686
    :cond_0
    const/4 v1, -0x1

    if-eq p5, v1, :cond_1

    .line 687
    invoke-virtual {v0, p5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(I)Lcom/twitter/analytics/model/ScribeLog;

    .line 689
    :cond_1
    if-eqz p6, :cond_2

    .line 690
    invoke-static {v0, p6}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/network/l;)V

    .line 692
    :cond_2
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 693
    return-void

    .line 660
    :pswitch_0
    const-string/jumbo v0, "success"

    goto :goto_0

    .line 664
    :pswitch_1
    const-string/jumbo v0, "error"

    goto :goto_0

    .line 668
    :pswitch_2
    const-string/jumbo v0, "cancel"

    goto :goto_0

    .line 681
    :cond_3
    const-string/jumbo v1, "disconnected"

    goto :goto_1

    .line 658
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/network/l;Z)V
    .locals 2

    .prologue
    .line 382
    if-nez p2, :cond_0

    .line 394
    :goto_0
    return-void

    .line 386
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->m:Z

    if-eqz v0, :cond_1

    .line 387
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->d(I)Lcom/twitter/analytics/model/ScribeLog;

    .line 390
    :cond_1
    if-eqz p3, :cond_2

    const-string/jumbo v0, "has_media"

    :goto_1
    invoke-virtual {p1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->k(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 391
    invoke-static {p2}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/network/l;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(I)Lcom/twitter/analytics/model/ScribeLog;

    .line 392
    invoke-static {p1, p2}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/network/l;)V

    .line 393
    invoke-static {p1}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 390
    :cond_2
    const-string/jumbo v0, "no_media"

    goto :goto_1
.end method

.method private a(Lcom/twitter/async/service/j;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 480
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/SendDMRequest;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501
    :goto_0
    return-void

    .line 484
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 493
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    const/16 v1, 0x193

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v4, 0x1

    .line 495
    :goto_1
    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->p:Landroid/content/Context;

    const-string/jumbo v1, "app:twitter_service:direct_messages:create"

    .line 497
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/SendDMRequest;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/service/v;->c:J

    const/4 v7, 0x0

    move-object v5, p1

    move v6, p2

    .line 495
    invoke-static/range {v0 .. v7}, Lcom/twitter/library/api/a;->a(Landroid/content/Context;Ljava/lang/String;JZLcom/twitter/async/service/j;ZLjava/lang/String;)V

    goto :goto_0

    .line 493
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method


# virtual methods
.method a(Lcom/twitter/model/drafts/DraftAttachment;Z)Lcom/twitter/library/api/upload/f;
    .locals 10
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 519
    .line 521
    const/4 v7, 0x0

    .line 524
    :try_start_0
    invoke-direct {p0, p1}, Lcom/twitter/library/api/dm/SendDMRequest;->a(Lcom/twitter/model/drafts/DraftAttachment;)Lcom/twitter/library/api/upload/f;
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 532
    :goto_0
    invoke-static {}, Lcom/twitter/library/api/progress/c;->a()Lcom/twitter/library/api/progress/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/api/dm/SendDMRequest;->d:Ljava/lang/String;

    invoke-virtual {v1, p0, v2}, Lcom/twitter/library/api/progress/c;->b(Lcom/twitter/util/q;Ljava/lang/String;)V

    .line 534
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/f;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 535
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/SendDMRequest;->M()Lcom/twitter/library/service/v;

    move-result-object v1

    iget-wide v2, v1, Lcom/twitter/library/service/v;->c:J

    const-string/jumbo v5, "Upload media failed"

    move-object v1, p0

    move-object v4, p1

    move v6, p2

    invoke-direct/range {v1 .. v8}, Lcom/twitter/library/api/dm/SendDMRequest;->a(JLcom/twitter/model/drafts/DraftAttachment;Ljava/lang/String;ZZLjava/lang/String;)V

    .line 539
    :cond_1
    return-object v0

    .line 525
    :catch_0
    move-exception v0

    .line 526
    :goto_1
    const/4 v7, 0x1

    .line 527
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    move-object v9, v0

    move-object v0, v8

    move-object v8, v9

    .line 530
    goto :goto_0

    .line 528
    :catch_1
    move-exception v0

    .line 529
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getMessage()Ljava/lang/String;

    move-result-object v0

    move-object v9, v0

    move-object v0, v8

    move-object v8, v9

    goto :goto_0

    .line 525
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method a(Lcom/twitter/library/api/upload/x;Z)Lcom/twitter/library/service/u;
    .locals 10
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 576
    new-instance v5, Lcom/twitter/util/concurrent/ObservablePromise;

    invoke-direct {v5}, Lcom/twitter/util/concurrent/ObservablePromise;-><init>()V

    .line 577
    new-instance v0, Lcom/twitter/library/api/upload/q;

    iget-object v1, p0, Lcom/twitter/library/api/dm/SendDMRequest;->p:Landroid/content/Context;

    const-string/jumbo v2, "set_metadata"

    iget-object v3, p0, Lcom/twitter/library/api/dm/SendDMRequest;->l:Lcom/twitter/library/client/Session;

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/upload/q;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/upload/x;Lcom/twitter/util/concurrent/ObservablePromise;)V

    .line 579
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 583
    const/4 v7, 0x0

    .line 586
    :try_start_0
    invoke-virtual {v5}, Lcom/twitter/util/concurrent/ObservablePromise;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 594
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 596
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/SendDMRequest;->M()Lcom/twitter/library/service/v;

    move-result-object v1

    iget-wide v2, v1, Lcom/twitter/library/service/v;->c:J

    .line 597
    invoke-virtual {p1}, Lcom/twitter/library/api/upload/x;->b()Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v4

    const-string/jumbo v5, "Upload media metadata failed"

    move-object v1, p0

    move v6, p2

    .line 595
    invoke-direct/range {v1 .. v8}, Lcom/twitter/library/api/dm/SendDMRequest;->a(JLcom/twitter/model/drafts/DraftAttachment;Ljava/lang/String;ZZLjava/lang/String;)V

    .line 604
    :cond_1
    return-object v0

    .line 587
    :catch_0
    move-exception v0

    .line 588
    :goto_1
    const/4 v7, 0x1

    .line 589
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    move-object v9, v0

    move-object v0, v8

    move-object v8, v9

    .line 592
    goto :goto_0

    .line 590
    :catch_1
    move-exception v0

    .line 591
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getMessage()Ljava/lang/String;

    move-result-object v0

    move-object v9, v0

    move-object v0, v8

    move-object v8, v9

    goto :goto_0

    .line 587
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method a(Lcom/twitter/library/service/u;Lcom/twitter/model/drafts/DraftAttachment;Z)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/twitter/library/api/dm/SendDMRequest$UploadMessageMediaException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 350
    if-nez p2, :cond_0

    .line 351
    const/4 v0, 0x0

    .line 377
    :goto_0
    return-object v0

    .line 354
    :cond_0
    invoke-virtual {p0, p2, p3}, Lcom/twitter/library/api/dm/SendDMRequest;->a(Lcom/twitter/model/drafts/DraftAttachment;Z)Lcom/twitter/library/api/upload/f;

    move-result-object v0

    .line 355
    if-nez v0, :cond_1

    .line 356
    invoke-virtual {p1, v6}, Lcom/twitter/library/service/u;->a(Z)V

    .line 357
    new-instance v0, Lcom/twitter/library/api/dm/SendDMRequest$UploadMessageMediaException;

    const-string/jumbo v1, "Upload media failed"

    invoke-direct {v0, v1}, Lcom/twitter/library/api/dm/SendDMRequest$UploadMessageMediaException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 358
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/library/api/upload/f;->b()Z

    move-result v1

    if-nez v1, :cond_2

    .line 359
    invoke-virtual {p1, v0}, Lcom/twitter/library/service/u;->a(Lcom/twitter/library/service/u;)V

    .line 360
    new-instance v0, Lcom/twitter/library/api/dm/SendDMRequest$UploadMessageMediaException;

    const-string/jumbo v1, "Upload media failed"

    invoke-direct {v0, v1}, Lcom/twitter/library/api/dm/SendDMRequest$UploadMessageMediaException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 362
    :cond_2
    invoke-virtual {v0}, Lcom/twitter/library/api/upload/f;->a()J

    move-result-wide v0

    .line 363
    new-instance v2, Lcom/twitter/library/api/upload/x;

    invoke-direct {v2, p2}, Lcom/twitter/library/api/upload/x;-><init>(Lcom/twitter/model/drafts/DraftAttachment;)V

    .line 364
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v4

    invoke-virtual {v2, v0, v1, v4, v5}, Lcom/twitter/library/api/upload/x;->a(JJ)V

    .line 365
    invoke-virtual {v2}, Lcom/twitter/library/api/upload/x;->f()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 367
    invoke-virtual {p0, v2, p3}, Lcom/twitter/library/api/dm/SendDMRequest;->a(Lcom/twitter/library/api/upload/x;Z)Lcom/twitter/library/service/u;

    move-result-object v2

    .line 368
    if-nez v2, :cond_3

    .line 369
    invoke-virtual {p1, v6}, Lcom/twitter/library/service/u;->a(Z)V

    .line 370
    new-instance v0, Lcom/twitter/library/api/dm/SendDMRequest$UploadMessageMediaException;

    const-string/jumbo v1, "Upload media metadata failed"

    invoke-direct {v0, v1}, Lcom/twitter/library/api/dm/SendDMRequest$UploadMessageMediaException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 371
    :cond_3
    invoke-virtual {v2}, Lcom/twitter/library/service/u;->b()Z

    move-result v3

    if-nez v3, :cond_4

    .line 372
    invoke-virtual {p1, v2}, Lcom/twitter/library/service/u;->a(Lcom/twitter/library/service/u;)V

    .line 373
    new-instance v0, Lcom/twitter/library/api/dm/SendDMRequest$UploadMessageMediaException;

    const-string/jumbo v1, "Upload media metadata failed"

    invoke-direct {v0, v1}, Lcom/twitter/library/api/dm/SendDMRequest$UploadMessageMediaException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 377
    :cond_4
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/twitter/async/service/j;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x2

    const/4 v1, 0x0

    .line 398
    invoke-direct {p0, p1, v1}, Lcom/twitter/library/api/dm/SendDMRequest;->a(Lcom/twitter/async/service/j;Z)V

    .line 400
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 401
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->f()Lcom/twitter/network/HttpOperation;

    move-result-object v3

    .line 402
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/SendDMRequest;->S()Laut;

    move-result-object v4

    .line 403
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 404
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v0

    .line 405
    if-eqz v0, :cond_1

    iget v0, v0, Lcom/twitter/network/l;->a:I

    .line 406
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 448
    :pswitch_0
    invoke-direct {p0, v6, v4}, Lcom/twitter/library/api/dm/SendDMRequest;->a(ILaut;)V

    .line 468
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 405
    goto :goto_0

    .line 408
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/SendDMRequest;->M()Lcom/twitter/library/service/v;

    move-result-object v5

    .line 410
    invoke-virtual {v3}, Lcom/twitter/network/HttpOperation;->r()Lcom/twitter/network/j;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/dm/p;

    invoke-virtual {v0}, Lcom/twitter/library/api/dm/p;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/ad;

    iput-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->a:Lcom/twitter/model/dms/ad;

    .line 411
    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->a:Lcom/twitter/model/dms/ad;

    invoke-virtual {v0}, Lcom/twitter/model/dms/ad;->g()Lcom/twitter/model/dms/s;

    move-result-object v3

    .line 413
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/SendDMRequest;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-wide v0, v3, Lcom/twitter/model/dms/s;->e:J

    :goto_2
    iput-wide v0, v3, Lcom/twitter/model/dms/s;->h:J

    .line 414
    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->b:Lcom/twitter/library/provider/t;

    iget-object v1, p0, Lcom/twitter/library/api/dm/SendDMRequest;->s:Lcom/twitter/model/dms/r;

    iget-object v6, p0, Lcom/twitter/library/api/dm/SendDMRequest;->a:Lcom/twitter/model/dms/ad;

    invoke-virtual {v0, v1, v3, v6, v4}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/model/dms/r;Lcom/twitter/model/dms/s;Lcom/twitter/model/dms/p;Laut;)V

    .line 415
    invoke-virtual {v4}, Laut;->a()V

    .line 417
    iget-object v0, v3, Lcom/twitter/model/dms/s;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->n:Ljava/lang/String;

    .line 418
    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->t:Lcom/twitter/model/drafts/DraftAttachment;

    if-eqz v0, :cond_2

    .line 419
    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->t:Lcom/twitter/model/drafts/DraftAttachment;

    invoke-virtual {v0, v2}, Lcom/twitter/model/drafts/DraftAttachment;->b(Lcom/twitter/model/drafts/DraftAttachment;)Lrx/g;

    .line 422
    :cond_2
    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->a:Lcom/twitter/model/dms/ad;

    iget-object v0, v0, Lcom/twitter/model/dms/ad;->a:Ljava/util/List;

    .line 423
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 425
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 426
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/y;

    .line 427
    iget-wide v6, v0, Lcom/twitter/model/dms/y;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_3

    .line 413
    :cond_3
    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->s:Lcom/twitter/model/dms/r;

    iget-wide v0, v0, Lcom/twitter/model/dms/r;->e:J

    goto :goto_2

    .line 429
    :cond_4
    iget-object v2, p0, Lcom/twitter/library/api/dm/SendDMRequest;->b:Lcom/twitter/library/provider/t;

    iget-object v3, p0, Lcom/twitter/library/api/dm/SendDMRequest;->n:Ljava/lang/String;

    .line 430
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v0

    .line 429
    invoke-virtual {v2, v3, v0, v4}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;[JLaut;)V

    .line 433
    :cond_5
    new-instance v0, Lcom/twitter/library/api/dm/f;

    iget-object v1, p0, Lcom/twitter/library/api/dm/SendDMRequest;->p:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/api/dm/SendDMRequest;->n:Ljava/lang/String;

    invoke-direct {v0, v1, v5, v2}, Lcom/twitter/library/api/dm/f;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;Ljava/lang/String;)V

    .line 434
    invoke-virtual {v0, p0}, Lcom/twitter/library/api/dm/f;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/service/s;

    move-result-object v0

    .line 433
    invoke-virtual {p0, v0}, Lcom/twitter/library/api/dm/SendDMRequest;->b(Lcom/twitter/async/service/AsyncOperation;)V

    goto/16 :goto_1

    .line 438
    :pswitch_2
    invoke-static {}, Lcom/twitter/library/dm/d;->j()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 440
    invoke-direct {p0, v1, v4}, Lcom/twitter/library/api/dm/SendDMRequest;->a(ILaut;)V

    goto/16 :goto_1

    .line 442
    :cond_6
    invoke-direct {p0, v6, v4}, Lcom/twitter/library/api/dm/SendDMRequest;->a(ILaut;)V

    goto/16 :goto_1

    .line 453
    :cond_7
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/SendDMRequest;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_8

    .line 454
    invoke-direct {p0, v6, v4}, Lcom/twitter/library/api/dm/SendDMRequest;->a(ILaut;)V

    .line 457
    :cond_8
    if-eqz v3, :cond_a

    invoke-virtual {v3}, Lcom/twitter/network/HttpOperation;->r()Lcom/twitter/network/j;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/dm/p;

    invoke-virtual {v0}, Lcom/twitter/library/api/dm/p;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/z;

    .line 458
    :goto_4
    invoke-static {v0}, Lcom/twitter/model/core/z;->a(Lcom/twitter/model/core/z;)[I

    move-result-object v0

    .line 459
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->a([I)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->r:Ljava/util/Set;

    .line 460
    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->r:Ljava/util/Set;

    const/16 v1, 0x96

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->r:Ljava/util/Set;

    const/16 v1, 0x15d

    .line 461
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 462
    :cond_9
    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->s:Lcom/twitter/model/dms/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->s:Lcom/twitter/model/dms/r;

    iget-object v0, v0, Lcom/twitter/model/dms/r;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/dm/e;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 463
    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->b:Lcom/twitter/library/provider/t;

    iget-object v1, p0, Lcom/twitter/library/api/dm/SendDMRequest;->s:Lcom/twitter/model/dms/r;

    iget-object v1, v1, Lcom/twitter/model/dms/r;->f:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, v4}, Lcom/twitter/library/provider/t;->d(Ljava/lang/String;ZLaut;)V

    .line 464
    invoke-virtual {v4}, Laut;->a()V

    goto/16 :goto_1

    :cond_a
    move-object v0, v2

    .line 457
    goto :goto_4

    .line 406
    :pswitch_data_0
    .packed-switch 0xc8
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected a(Lcom/twitter/model/dms/r;Lcom/twitter/model/drafts/DraftAttachment;Lcom/twitter/library/service/u;Lcci;Lcch;)V
    .locals 20

    .prologue
    .line 183
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/library/api/dm/SendDMRequest;->s:Lcom/twitter/model/dms/r;

    .line 184
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/library/api/dm/SendDMRequest;->t:Lcom/twitter/model/drafts/DraftAttachment;

    .line 186
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/dm/SendDMRequest;->e()Z

    move-result v13

    .line 190
    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2, v13}, Lcom/twitter/library/api/dm/SendDMRequest;->a(Lcom/twitter/library/service/u;Lcom/twitter/model/drafts/DraftAttachment;Z)Ljava/lang/String;
    :try_end_0
    .catch Lcom/twitter/library/api/dm/SendDMRequest$UploadMessageMediaException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 195
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/dm/SendDMRequest;->M()Lcom/twitter/library/service/v;

    move-result-object v4

    .line 196
    iget-wide v8, v4, Lcom/twitter/library/service/v;->c:J

    .line 197
    new-instance v11, Lcom/twitter/library/api/dm/p;

    invoke-direct {v11}, Lcom/twitter/library/api/dm/p;-><init>()V

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p4

    move-object/from16 v12, p5

    .line 198
    invoke-direct/range {v5 .. v12}, Lcom/twitter/library/api/dm/SendDMRequest;->a(Lcom/twitter/model/dms/r;Lcci;JLjava/lang/String;Lcom/twitter/library/api/dm/p;Lcch;)Lcom/twitter/network/HttpOperation;

    move-result-object v6

    .line 201
    invoke-virtual {v6}, Lcom/twitter/network/HttpOperation;->m()Lcom/twitter/network/l;

    move-result-object v19

    .line 204
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/dm/SendDMRequest;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 205
    const-string/jumbo v4, "cancel"

    .line 211
    :goto_0
    new-instance v7, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v7, v8, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v5, 0x3

    new-array v12, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v14, "app:twitter_service:direct_messages"

    aput-object v14, v12, v5

    const/4 v14, 0x1

    if-eqz v13, :cond_4

    const-string/jumbo v5, "retry_dm"

    :goto_1
    aput-object v5, v12, v14

    const/4 v5, 0x2

    aput-object v4, v12, v5

    .line 212
    invoke-virtual {v7, v12}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v4

    check-cast v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 215
    if-eqz v10, :cond_5

    const/4 v5, 0x1

    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v4, v1, v5}, Lcom/twitter/library/api/dm/SendDMRequest;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Lcom/twitter/network/l;Z)V

    .line 216
    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Lcom/twitter/library/service/u;->a(Lcom/twitter/network/HttpOperation;)V

    .line 218
    if-eqz p2, :cond_0

    .line 219
    invoke-virtual {v6}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v4

    .line 220
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/twitter/model/drafts/DraftAttachment;->g:Lcom/twitter/media/model/MediaType;

    iget-object v0, v5, Lcom/twitter/media/model/MediaType;->extension:Ljava/lang/String;

    move-object/from16 v16, v0

    if-eqz v4, :cond_6

    const/16 v17, 0x0

    :goto_3
    if-eqz v4, :cond_7

    const/16 v18, -0x1

    :goto_4
    move-object/from16 v13, p0

    move-wide v14, v8

    invoke-direct/range {v13 .. v19}, Lcom/twitter/library/api/dm/SendDMRequest;->a(JLjava/lang/String;IILcom/twitter/network/l;)V

    .line 228
    :cond_0
    invoke-virtual/range {p3 .. p3}, Lcom/twitter/library/service/u;->i()Lcom/twitter/library/service/r;

    move-result-object v4

    if-nez v4, :cond_1

    .line 229
    invoke-virtual {v11}, Lcom/twitter/library/api/dm/p;->a()Lcom/twitter/library/service/a;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/twitter/library/service/u;->a(Lcom/twitter/library/service/r;)V

    .line 231
    :cond_1
    :goto_5
    return-void

    .line 206
    :cond_2
    move-object/from16 v0, v19

    iget v4, v0, Lcom/twitter/network/l;->a:I

    const/16 v5, 0xc8

    if-ne v4, v5, :cond_3

    .line 207
    const-string/jumbo v4, "success"

    goto :goto_0

    .line 209
    :cond_3
    const-string/jumbo v4, "failure"

    goto :goto_0

    .line 211
    :cond_4
    const-string/jumbo v5, "send_dm"

    goto :goto_1

    .line 215
    :cond_5
    const/4 v5, 0x0

    goto :goto_2

    .line 220
    :cond_6
    const/16 v17, 0x1

    goto :goto_3

    .line 223
    :cond_7
    invoke-static/range {v19 .. v19}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/network/l;)I

    move-result v18

    goto :goto_4

    .line 191
    :catch_0
    move-exception v4

    goto :goto_5
.end method

.method protected a(Z)V
    .locals 0

    .prologue
    .line 163
    iput-boolean p1, p0, Lcom/twitter/library/api/dm/SendDMRequest;->m:Z

    .line 164
    return-void
.end method

.method public b(Lcom/twitter/async/service/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 235
    iput-boolean v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->m:Z

    .line 236
    invoke-direct {p0, p1, v0}, Lcom/twitter/library/api/dm/SendDMRequest;->a(Lcom/twitter/async/service/j;Z)V

    .line 237
    return-void
.end method

.method public abstract e()Z
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->n:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->r:Ljava/util/Set;

    return-object v0
.end method

.method public h()J
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->s:Lcom/twitter/model/dms/r;

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->s:Lcom/twitter/model/dms/r;

    iget-wide v0, v0, Lcom/twitter/model/dms/r;->e:J

    goto :goto_0
.end method

.method public onEvent(Lcom/twitter/library/api/progress/ProgressUpdatedEvent;)V
    .locals 4

    .prologue
    .line 609
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->d:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/library/api/progress/ProgressUpdatedEvent;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->s:Lcom/twitter/model/dms/r;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/api/dm/SendDMRequest;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 610
    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->b:Lcom/twitter/library/provider/t;

    iget-object v1, p0, Lcom/twitter/library/api/dm/SendDMRequest;->s:Lcom/twitter/model/dms/r;

    iget v2, p1, Lcom/twitter/library/api/progress/ProgressUpdatedEvent;->c:I

    iget-object v3, p0, Lcom/twitter/library/api/dm/SendDMRequest;->c:Laut;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/provider/t;->b(Lcom/twitter/model/dms/r;ILaut;)V

    .line 611
    iget-object v0, p0, Lcom/twitter/library/api/dm/SendDMRequest;->c:Laut;

    invoke-virtual {v0}, Laut;->a()V

    .line 613
    :cond_0
    return-void
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 97
    check-cast p1, Lcom/twitter/library/api/progress/ProgressUpdatedEvent;

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/dm/SendDMRequest;->onEvent(Lcom/twitter/library/api/progress/ProgressUpdatedEvent;)V

    return-void
.end method
