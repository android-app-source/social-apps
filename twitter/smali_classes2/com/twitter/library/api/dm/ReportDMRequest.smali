.class public Lcom/twitter/library/api/dm/ReportDMRequest;
.super Lcom/twitter/library/api/dm/c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/api/dm/ReportDMRequest$Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/api/dm/c",
        "<",
        "Lcom/twitter/library/api/i;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final c:Lcom/twitter/library/api/dm/ReportDMRequest$Type;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcom/twitter/library/api/dm/ReportDMRequest$Type;)V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/twitter/library/api/dm/ReportDMRequest;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/dm/c;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 31
    iput-wide p3, p0, Lcom/twitter/library/api/dm/ReportDMRequest;->a:J

    .line 32
    invoke-static {p5}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/dm/ReportDMRequest$Type;

    iput-object v0, p0, Lcom/twitter/library/api/dm/ReportDMRequest;->c:Lcom/twitter/library/api/dm/ReportDMRequest$Type;

    .line 33
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 6

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/ReportDMRequest;->R()Lcom/twitter/library/provider/t;

    move-result-object v0

    .line 54
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/ReportDMRequest;->S()Laut;

    move-result-object v1

    .line 55
    sget-object v2, Lcom/twitter/library/api/dm/ReportDMRequest$Type;->c:Lcom/twitter/library/api/dm/ReportDMRequest$Type;

    iget-object v3, p0, Lcom/twitter/library/api/dm/ReportDMRequest;->c:Lcom/twitter/library/api/dm/ReportDMRequest$Type;

    if-ne v2, v3, :cond_1

    .line 56
    iget-wide v2, p0, Lcom/twitter/library/api/dm/ReportDMRequest;->a:J

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/twitter/library/provider/t;->c(JZLaut;)V

    .line 57
    invoke-virtual {v1}, Laut;->a()V

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 60
    iget-wide v2, p0, Lcom/twitter/library/api/dm/ReportDMRequest;->a:J

    invoke-virtual {v0, v2, v3, v1}, Lcom/twitter/library/provider/t;->e(JLaut;)V

    .line 61
    invoke-virtual {v1}, Laut;->a()V

    .line 65
    new-instance v0, Lcom/twitter/library/api/dm/m;

    iget-object v1, p0, Lcom/twitter/library/api/dm/ReportDMRequest;->p:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/library/api/dm/ReportDMRequest;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-wide v4, p0, Lcom/twitter/library/api/dm/ReportDMRequest;->a:J

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/twitter/library/api/dm/m;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;J)V

    invoke-virtual {v0}, Lcom/twitter/library/api/dm/m;->O()Lcom/twitter/library/service/u;

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 20
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/api/dm/ReportDMRequest;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/service/d$a;
    .locals 4

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/ReportDMRequest;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 39
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "direct_messages"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "report_spam"

    aput-object v3, v1, v2

    .line 40
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "dm_id"

    iget-wide v2, p0, Lcom/twitter/library/api/dm/ReportDMRequest;->a:J

    .line 41
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "report_as"

    iget-object v2, p0, Lcom/twitter/library/api/dm/ReportDMRequest;->c:Lcom/twitter/library/api/dm/ReportDMRequest$Type;

    .line 42
    invoke-virtual {v2}, Lcom/twitter/library/api/dm/ReportDMRequest$Type;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 38
    return-object v0
.end method

.method protected e()Lcom/twitter/library/api/i;
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/ReportDMRequest;->e()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
