.class public Lcom/twitter/library/api/dm/u;
.super Lcom/twitter/library/api/dm/c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/api/dm/c",
        "<",
        "Lcom/twitter/library/api/i;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final c:Z

.field private final g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 45
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/dm/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;ZI)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;ZI)V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/twitter/library/api/dm/u;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/dm/c;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 51
    invoke-static {p3}, Lcom/twitter/util/object/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/dm/u;->a:Ljava/lang/String;

    .line 52
    iput-boolean p4, p0, Lcom/twitter/library/api/dm/u;->c:Z

    .line 53
    iput p5, p0, Lcom/twitter/library/api/dm/u;->g:I

    .line 54
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 4

    .prologue
    .line 75
    invoke-virtual {p2}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/u;->S()Laut;

    move-result-object v0

    .line 77
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/u;->R()Lcom/twitter/library/provider/t;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/api/dm/u;->a:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/twitter/library/api/dm/u;->c:Z

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;ZLaut;)V

    .line 78
    invoke-virtual {v0}, Laut;->a()V

    .line 80
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 24
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/api/dm/u;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/service/d$a;
    .locals 5

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/twitter/library/api/dm/u;->c:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "disable_notifications"

    .line 60
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/u;->J()Lcom/twitter/library/service/d$a;

    move-result-object v1

    sget-object v2, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 61
    invoke-virtual {v1, v2}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v1

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string/jumbo v4, "dm"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "conversation"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/twitter/library/api/dm/u;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object v0, v2, v3

    .line 62
    invoke-virtual {v1, v2}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "request_id"

    .line 63
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "duration"

    iget v2, p0, Lcom/twitter/library/api/dm/u;->g:I

    int-to-long v2, v2

    .line 64
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 60
    return-object v0

    .line 59
    :cond_0
    const-string/jumbo v0, "enable_notifications"

    goto :goto_0
.end method

.method protected e()Lcom/twitter/library/api/i;
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/u;->e()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
