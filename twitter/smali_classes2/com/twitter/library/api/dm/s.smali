.class public Lcom/twitter/library/api/dm/s;
.super Lcom/twitter/library/api/dm/SendDMRequest;
.source "Twttr"


# instance fields
.field private final i:J

.field private final j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JZ)V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/twitter/library/api/dm/s;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/dm/SendDMRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 33
    iput-wide p3, p0, Lcom/twitter/library/api/dm/s;->i:J

    .line 34
    if-eqz p5, :cond_0

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/api/dm/s;->j:I

    .line 39
    :goto_0
    invoke-virtual {p0, p5}, Lcom/twitter/library/api/dm/s;->a(Z)V

    .line 40
    return-void

    .line 37
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/library/api/dm/s;->j:I

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/u;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 49
    iget-object v0, p0, Lcom/twitter/library/api/dm/s;->b:Lcom/twitter/library/provider/t;

    iget-wide v4, p0, Lcom/twitter/library/api/dm/s;->i:J

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/provider/t;->j(J)Landroid/database/Cursor;

    move-result-object v6

    .line 51
    if-eqz v6, :cond_1

    .line 52
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    const/4 v0, 0x5

    .line 54
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/dms/r$c;->a:Lcom/twitter/util/serialization/l;

    .line 53
    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/r$c;

    .line 56
    new-instance v1, Lcom/twitter/model/dms/r$a;

    invoke-direct {v1}, Lcom/twitter/model/dms/r$a;-><init>()V

    .line 57
    invoke-virtual {v1, v0}, Lcom/twitter/model/dms/r$a;->a(Ljava/lang/Object;)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/r$a;

    const/4 v1, 0x0

    .line 58
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/dms/r$a;->b(J)Lcom/twitter/model/dms/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/r$a;

    const/4 v1, 0x2

    .line 59
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/dms/r$a;->a(J)Lcom/twitter/model/dms/c$b;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/r$a;

    const/4 v1, 0x1

    .line 60
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/dms/r$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/d$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/r$a;

    .line 61
    invoke-virtual {v0}, Lcom/twitter/model/dms/r$a;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/r;

    .line 63
    iget-object v0, p0, Lcom/twitter/library/api/dm/s;->b:Lcom/twitter/library/provider/t;

    iget v2, p0, Lcom/twitter/library/api/dm/s;->j:I

    iget-object v4, p0, Lcom/twitter/library/api/dm/s;->c:Laut;

    invoke-virtual {v0, v1, v2, v4}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/model/dms/r;ILaut;)V

    .line 64
    iget-object v0, p0, Lcom/twitter/library/api/dm/s;->c:Laut;

    invoke-virtual {v0}, Laut;->a()V

    .line 66
    invoke-virtual {v1}, Lcom/twitter/model/dms/r;->h()Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v2

    .line 67
    invoke-virtual {v1}, Lcom/twitter/model/dms/r;->A()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/twitter/model/dms/r;->u()Lcbx;

    move-result-object v0

    check-cast v0, Lcci;

    move-object v4, v0

    .line 68
    :goto_0
    invoke-virtual {v1}, Lcom/twitter/model/dms/r;->F()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 69
    invoke-virtual {v1}, Lcom/twitter/model/dms/r;->u()Lcbx;

    move-result-object v0

    check-cast v0, Lcch;

    move-object v5, v0

    :goto_1
    move-object v0, p0

    move-object v3, p1

    .line 70
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/api/dm/s;->a(Lcom/twitter/model/dms/r;Lcom/twitter/model/drafts/DraftAttachment;Lcom/twitter/library/service/u;Lcci;Lcch;)V

    .line 72
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 74
    :cond_1
    return-void

    :cond_2
    move-object v4, v3

    .line 67
    goto :goto_0

    :cond_3
    move-object v5, v3

    .line 69
    goto :goto_1
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    return v0
.end method
