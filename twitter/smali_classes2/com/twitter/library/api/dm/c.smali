.class public abstract Lcom/twitter/library/api/dm/c;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/library/api/i;",
        ">",
        "Lcom/twitter/library/service/b",
        "<TT;>;"
    }
.end annotation


# static fields
.field static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "stickerInfo"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "mediaRestrictions"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "altText"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/library/api/dm/c;->b:[Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 35
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/api/dm/c;->f(Z)V

    .line 36
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 40
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/api/dm/c;->f(Z)V

    .line 41
    return-void
.end method


# virtual methods
.method protected final a()Lcom/twitter/library/service/d;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 53
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/c;->b()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 54
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/c;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    const-string/jumbo v1, "ext"

    sget-object v2, Lcom/twitter/library/api/dm/c;->b:[Ljava/lang/String;

    invoke-static {v2}, Lcom/twitter/util/collection/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/util/Collection;)Lcom/twitter/library/service/d$a;

    .line 58
    :cond_0
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 59
    iget-object v1, p0, Lcom/twitter/library/api/dm/c;->p:Landroid/content/Context;

    const-string/jumbo v2, "debug_prefs"

    .line 60
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 61
    const-string/jumbo v2, "dm_staging_enabled"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 62
    const-string/jumbo v2, "dm_staging_host"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 63
    if-eqz v1, :cond_1

    .line 64
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 65
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/service/d$a;->c(Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v2

    .line 66
    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/library/service/d$a;->b(Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 70
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected abstract b()Lcom/twitter/library/service/d$a;
.end method

.method h()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method
