.class public Lcom/twitter/library/api/dm/q;
.super Lcom/twitter/library/api/dm/SendDMRequest;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/api/dm/q$a;
    }
.end annotation


# instance fields
.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:J

.field private final l:Ljava/lang/String;

.field private final m:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lcom/twitter/library/client/Session;

.field private final r:Lcbx;

.field private final s:Lcci;

.field private final t:Lcom/twitter/model/drafts/DraftAttachment;

.field private final u:Ljava/lang/String;

.field private final v:Lccl;

.field private w:Lcom/twitter/model/dms/r;

.field private x:Lcom/twitter/library/service/u;


# direct methods
.method private constructor <init>(Lcom/twitter/library/api/dm/q$a;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-static {p1}, Lcom/twitter/library/api/dm/q$a;->a(Lcom/twitter/library/api/dm/q$a;)Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/twitter/library/api/dm/q;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/twitter/library/api/dm/q$a;->b(Lcom/twitter/library/api/dm/q$a;)Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {p0, v0, v2, v3}, Lcom/twitter/library/api/dm/SendDMRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 56
    invoke-static {p1}, Lcom/twitter/library/api/dm/q$a;->c(Lcom/twitter/library/api/dm/q$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/dm/q;->i:Ljava/lang/String;

    .line 57
    invoke-static {p1}, Lcom/twitter/library/api/dm/q$a;->d(Lcom/twitter/library/api/dm/q$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/dm/q;->j:Ljava/lang/String;

    .line 58
    invoke-static {p1}, Lcom/twitter/library/api/dm/q$a;->b(Lcom/twitter/library/api/dm/q$a;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/library/api/dm/q;->k:J

    .line 59
    invoke-static {p1}, Lcom/twitter/library/api/dm/q$a;->e(Lcom/twitter/library/api/dm/q$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/dm/q;->l:Ljava/lang/String;

    .line 60
    invoke-static {p1}, Lcom/twitter/library/api/dm/q$a;->f(Lcom/twitter/library/api/dm/q$a;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/dm/q;->m:Ljava/util/Set;

    .line 61
    invoke-static {p1}, Lcom/twitter/library/api/dm/q$a;->b(Lcom/twitter/library/api/dm/q$a;)Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/dm/q;->n:Lcom/twitter/library/client/Session;

    .line 62
    invoke-static {p1}, Lcom/twitter/library/api/dm/q$a;->g(Lcom/twitter/library/api/dm/q$a;)Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/dm/q;->t:Lcom/twitter/model/drafts/DraftAttachment;

    .line 63
    invoke-static {p1}, Lcom/twitter/library/api/dm/q$a;->h(Lcom/twitter/library/api/dm/q$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/dm/q;->u:Ljava/lang/String;

    .line 64
    invoke-static {p1}, Lcom/twitter/library/api/dm/q$a;->i(Lcom/twitter/library/api/dm/q$a;)Lccl;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/dm/q;->v:Lccl;

    .line 66
    iget-object v0, p0, Lcom/twitter/library/api/dm/q;->t:Lcom/twitter/model/drafts/DraftAttachment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/dm/q;->t:Lcom/twitter/model/drafts/DraftAttachment;

    const/4 v2, 0x3

    .line 67
    invoke-virtual {v0, v2}, Lcom/twitter/model/drafts/DraftAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 69
    :goto_0
    invoke-static {p1}, Lcom/twitter/library/api/dm/q$a;->j(Lcom/twitter/library/api/dm/q$a;)Lcdu;

    move-result-object v2

    .line 70
    if-eqz v2, :cond_1

    .line 71
    new-instance v0, Lcch$a;

    invoke-direct {v0}, Lcch$a;-><init>()V

    iget-wide v2, v2, Lcdu;->h:J

    invoke-virtual {v0, v2, v3}, Lcch$a;->a(J)Lcch$a;

    move-result-object v0

    invoke-virtual {v0}, Lcch$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbx;

    iput-object v0, p0, Lcom/twitter/library/api/dm/q;->r:Lcbx;

    .line 72
    iput-object v1, p0, Lcom/twitter/library/api/dm/q;->s:Lcci;

    .line 93
    :goto_1
    return-void

    :cond_0
    move-object v0, v1

    .line 67
    goto :goto_0

    .line 73
    :cond_1
    if-eqz v0, :cond_2

    .line 74
    iput-object v1, p0, Lcom/twitter/library/api/dm/q;->s:Lcci;

    .line 75
    new-instance v1, Lccf$a;

    invoke-direct {v1}, Lccf$a;-><init>()V

    new-instance v2, Lcom/twitter/model/core/MediaEntity$a;

    invoke-direct {v2}, Lcom/twitter/model/core/MediaEntity$a;-><init>()V

    iget-object v3, v0, Lcom/twitter/model/media/EditableMedia;->k:Lcom/twitter/media/model/MediaFile;

    .line 77
    invoke-virtual {v3}, Lcom/twitter/media/model/MediaFile;->a()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/model/core/MediaEntity$a;->a(Ljava/lang/String;)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v2

    iget-object v3, v0, Lcom/twitter/model/media/EditableMedia;->k:Lcom/twitter/media/model/MediaFile;

    iget-object v3, v3, Lcom/twitter/media/model/MediaFile;->f:Lcom/twitter/util/math/Size;

    .line 78
    invoke-virtual {v3}, Lcom/twitter/util/math/Size;->a()I

    move-result v3

    iget-object v4, v0, Lcom/twitter/model/media/EditableMedia;->k:Lcom/twitter/media/model/MediaFile;

    iget-object v4, v4, Lcom/twitter/media/model/MediaFile;->f:Lcom/twitter/util/math/Size;

    .line 79
    invoke-virtual {v4}, Lcom/twitter/util/math/Size;->b()I

    move-result v4

    .line 78
    invoke-static {v3, v4}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/model/core/MediaEntity$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v2

    .line 80
    invoke-static {v0}, Lcom/twitter/library/api/dm/q;->a(Lcom/twitter/model/media/EditableMedia;)Lcom/twitter/model/core/MediaEntity$Type;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/model/core/MediaEntity$a;->a(Lcom/twitter/model/core/MediaEntity$Type;)Lcom/twitter/model/core/MediaEntity$a;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Lcom/twitter/model/core/MediaEntity$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    .line 76
    invoke-virtual {v1, v0}, Lccf$a;->a(Lcom/twitter/model/core/MediaEntity;)Lccf$a;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Lccf$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbx;

    iput-object v0, p0, Lcom/twitter/library/api/dm/q;->r:Lcbx;

    goto :goto_1

    .line 83
    :cond_2
    invoke-static {p1}, Lcom/twitter/library/api/dm/q$a;->k(Lcom/twitter/library/api/dm/q$a;)Lcom/twitter/model/core/r;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 84
    new-instance v0, Lcci$a;

    invoke-direct {v0}, Lcci$a;-><init>()V

    .line 85
    invoke-static {p1}, Lcom/twitter/library/api/dm/q$a;->k(Lcom/twitter/library/api/dm/q$a;)Lcom/twitter/model/core/r;

    move-result-object v1

    iget-wide v2, v1, Lcom/twitter/model/core/r;->e:J

    invoke-virtual {v0, v2, v3}, Lcci$a;->a(J)Lcci$a;

    move-result-object v0

    .line 86
    invoke-static {p1}, Lcom/twitter/library/api/dm/q$a;->k(Lcom/twitter/library/api/dm/q$a;)Lcom/twitter/model/core/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcci$a;->a(Lcom/twitter/model/core/r;)Lcci$a;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lcci$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcci;

    iput-object v0, p0, Lcom/twitter/library/api/dm/q;->s:Lcci;

    .line 88
    iget-object v0, p0, Lcom/twitter/library/api/dm/q;->s:Lcci;

    iput-object v0, p0, Lcom/twitter/library/api/dm/q;->r:Lcbx;

    goto :goto_1

    .line 90
    :cond_3
    iput-object v1, p0, Lcom/twitter/library/api/dm/q;->s:Lcci;

    .line 91
    iput-object v1, p0, Lcom/twitter/library/api/dm/q;->r:Lcbx;

    goto :goto_1
.end method

.method synthetic constructor <init>(Lcom/twitter/library/api/dm/q$a;Lcom/twitter/library/api/dm/q$1;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/twitter/library/api/dm/q;-><init>(Lcom/twitter/library/api/dm/q$a;)V

    return-void
.end method

.method private static a(Lcom/twitter/model/media/EditableMedia;)Lcom/twitter/model/core/MediaEntity$Type;
    .locals 2

    .prologue
    .line 195
    sget-object v0, Lcom/twitter/library/api/dm/q$2;->a:[I

    invoke-virtual {p0}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/media/model/MediaType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 207
    sget-object v0, Lcom/twitter/model/core/MediaEntity$Type;->a:Lcom/twitter/model/core/MediaEntity$Type;

    :goto_0
    return-object v0

    .line 197
    :pswitch_0
    sget-object v0, Lcom/twitter/model/core/MediaEntity$Type;->b:Lcom/twitter/model/core/MediaEntity$Type;

    goto :goto_0

    .line 200
    :pswitch_1
    sget-object v0, Lcom/twitter/model/core/MediaEntity$Type;->c:Lcom/twitter/model/core/MediaEntity$Type;

    goto :goto_0

    .line 204
    :pswitch_2
    sget-object v0, Lcom/twitter/model/core/MediaEntity$Type;->d:Lcom/twitter/model/core/MediaEntity$Type;

    goto :goto_0

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/twitter/library/api/dm/q;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/twitter/library/api/dm/q;->s()V

    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 142
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/q;->R()Lcom/twitter/library/provider/t;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/twitter/library/provider/t;->c(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 143
    if-eqz v1, :cond_1

    .line 145
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-lez v2, :cond_0

    const/4 v0, 0x1

    .line 147
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 150
    :cond_1
    return v0

    .line 147
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 155
    new-instance v1, Lcom/twitter/library/dm/c;

    iget-object v2, p0, Lcom/twitter/library/api/dm/q;->p:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/library/api/dm/q;->n:Lcom/twitter/library/client/Session;

    iget-object v0, p0, Lcom/twitter/library/api/dm/q;->m:Ljava/util/Set;

    .line 156
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v0

    const/4 v4, 0x0

    new-array v4, v4, [J

    invoke-static {v0, v4}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    invoke-direct {v1, v2, v3, p1, v0}, Lcom/twitter/library/dm/c;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;[J)V

    invoke-virtual {v1}, Lcom/twitter/library/dm/c;->b()V

    .line 157
    return-void
.end method

.method private s()V
    .locals 12

    .prologue
    .line 113
    iget-object v0, p0, Lcom/twitter/library/api/dm/q;->i:Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/library/api/dm/q;->k:J

    iget-object v1, p0, Lcom/twitter/library/api/dm/q;->m:Ljava/util/Set;

    .line 114
    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v1

    invoke-static {v2, v3, v1}, Lcom/twitter/library/dm/e;->a(J[J)Ljava/lang/String;

    move-result-object v1

    .line 113
    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 116
    iget-object v0, p0, Lcom/twitter/library/api/dm/q;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-direct {p0, v2}, Lcom/twitter/library/api/dm/q;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 117
    :cond_0
    invoke-direct {p0, v2}, Lcom/twitter/library/api/dm/q;->b(Ljava/lang/String;)V

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/api/dm/q;->w:Lcom/twitter/model/dms/r;

    if-nez v0, :cond_2

    .line 121
    iget-object v1, p0, Lcom/twitter/library/api/dm/q;->b:Lcom/twitter/library/provider/t;

    iget-object v3, p0, Lcom/twitter/library/api/dm/q;->j:Ljava/lang/String;

    iget-wide v4, p0, Lcom/twitter/library/api/dm/q;->k:J

    iget-object v6, p0, Lcom/twitter/library/api/dm/q;->l:Ljava/lang/String;

    iget-object v7, p0, Lcom/twitter/library/api/dm/q;->t:Lcom/twitter/model/drafts/DraftAttachment;

    iget-object v8, p0, Lcom/twitter/library/api/dm/q;->r:Lcbx;

    iget-object v9, p0, Lcom/twitter/library/api/dm/q;->c:Laut;

    iget-object v10, p0, Lcom/twitter/library/api/dm/q;->u:Ljava/lang/String;

    iget-object v11, p0, Lcom/twitter/library/api/dm/q;->v:Lccl;

    invoke-virtual/range {v1 .. v11}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/twitter/model/drafts/DraftAttachment;Lcbx;Laut;Ljava/lang/String;Lccl;)Lcom/twitter/model/dms/r;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/dm/q;->w:Lcom/twitter/model/dms/r;

    .line 125
    :cond_2
    iget-object v0, p0, Lcom/twitter/library/api/dm/q;->c:Laut;

    invoke-virtual {v0}, Laut;->a()V

    .line 126
    return-void
.end method


# virtual methods
.method a()V
    .locals 6

    .prologue
    .line 136
    iget-object v0, p0, Lcom/twitter/library/api/dm/q;->w:Lcom/twitter/model/dms/r;

    invoke-virtual {v0}, Lcom/twitter/model/dms/r;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/dm/q;->w:Lcom/twitter/model/dms/r;

    .line 137
    invoke-virtual {v0}, Lcom/twitter/model/dms/r;->u()Lcbx;

    move-result-object v0

    check-cast v0, Lcch;

    move-object v5, v0

    .line 138
    :goto_0
    iget-object v1, p0, Lcom/twitter/library/api/dm/q;->w:Lcom/twitter/model/dms/r;

    iget-object v2, p0, Lcom/twitter/library/api/dm/q;->t:Lcom/twitter/model/drafts/DraftAttachment;

    iget-object v3, p0, Lcom/twitter/library/api/dm/q;->x:Lcom/twitter/library/service/u;

    iget-object v4, p0, Lcom/twitter/library/api/dm/q;->s:Lcci;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/api/dm/q;->a(Lcom/twitter/model/dms/r;Lcom/twitter/model/drafts/DraftAttachment;Lcom/twitter/library/service/u;Lcci;Lcch;)V

    .line 139
    return-void

    .line 137
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/service/u;)V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/twitter/library/api/dm/q;->w:Lcom/twitter/model/dms/r;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string/jumbo v1, "Attempting to send a null message."

    invoke-static {v0, v1}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 131
    iput-object p1, p0, Lcom/twitter/library/api/dm/q;->x:Lcom/twitter/library/service/u;

    .line 132
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/q;->a()V

    .line 133
    return-void

    .line 130
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ay_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/twitter/library/api/dm/q;->j:Ljava/lang/String;

    return-object v0
.end method

.method protected b(Lcom/twitter/library/service/u;)Z
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/twitter/library/api/dm/q;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/dm/q;->m:Ljava/util/Set;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lcom/twitter/library/api/dm/q$1;

    invoke-direct {v0, p0}, Lcom/twitter/library/api/dm/q$1;-><init>(Lcom/twitter/library/api/dm/q;)V

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x0

    return v0
.end method
