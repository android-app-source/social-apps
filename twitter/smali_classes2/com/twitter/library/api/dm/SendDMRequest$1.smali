.class Lcom/twitter/library/api/dm/SendDMRequest$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcpp;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/api/dm/SendDMRequest;->a(Ljava/lang/String;J)Ljava/lang/Iterable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcpp",
        "<",
        "Lcom/twitter/model/dms/Participant;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/api/dm/SendDMRequest;


# direct methods
.method constructor <init>(Lcom/twitter/library/api/dm/SendDMRequest;)V
    .locals 0

    .prologue
    .line 622
    iput-object p1, p0, Lcom/twitter/library/api/dm/SendDMRequest$1;->a:Lcom/twitter/library/api/dm/SendDMRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/dms/Participant;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 626
    if-eqz p1, :cond_0

    iget-wide v0, p1, Lcom/twitter/model/dms/Participant;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 622
    check-cast p1, Lcom/twitter/model/dms/Participant;

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/dm/SendDMRequest$1;->a(Lcom/twitter/model/dms/Participant;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
