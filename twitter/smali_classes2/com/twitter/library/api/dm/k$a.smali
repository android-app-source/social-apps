.class Lcom/twitter/library/api/dm/k$a;
.super Lcom/twitter/library/api/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/api/dm/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/api/i",
        "<",
        "Lcom/twitter/model/dms/am;",
        "Lcom/twitter/model/core/z;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/twitter/library/api/i;-><init>()V

    return-void
.end method


# virtual methods
.method protected synthetic a(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-virtual {p0, p1}, Lcom/twitter/library/api/dm/k$a;->b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/dms/am;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic a(Lcom/fasterxml/jackson/core/JsonParser;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/api/dm/k$a;->b(Lcom/fasterxml/jackson/core/JsonParser;I)Lcom/twitter/model/core/z;

    move-result-object v0

    return-object v0
.end method

.method protected b(Lcom/fasterxml/jackson/core/JsonParser;I)Lcom/twitter/model/core/z;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    const-class v0, Lcom/twitter/model/core/z;

    invoke-static {p1, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/z;

    return-object v0
.end method

.method protected b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/dms/am;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    new-instance v0, Lcom/twitter/model/json/dms/q;

    invoke-direct {v0}, Lcom/twitter/model/json/dms/q;-><init>()V

    invoke-virtual {v0, p1}, Lcom/twitter/model/json/dms/q;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/dms/am;

    move-result-object v0

    return-object v0
.end method
