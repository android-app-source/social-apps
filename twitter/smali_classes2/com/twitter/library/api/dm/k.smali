.class public Lcom/twitter/library/api/dm/k;
.super Lcom/twitter/library/api/dm/c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/api/dm/k$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/api/dm/c",
        "<",
        "Lcom/twitter/library/api/dm/k$a;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final c:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private h:Lcom/twitter/network/apache/entity/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/twitter/library/api/dm/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/api/dm/k;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/twitter/library/api/dm/k;->a:Ljava/lang/String;

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/dm/c;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 43
    invoke-static {p3}, Lcom/twitter/util/object/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/dm/k;->c:Ljava/lang/String;

    .line 44
    iput-object p4, p0, Lcom/twitter/library/api/dm/k;->g:Ljava/lang/String;

    .line 45
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/dm/k$a;)V
    .locals 4

    .prologue
    .line 78
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/twitter/model/dms/u;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {p3}, Lcom/twitter/library/api/dm/k$a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/am;

    .line 80
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/model/dms/am;->f()I

    move-result v1

    const/4 v2, 0x6

    if-ne v1, v2, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/k;->S()Laut;

    move-result-object v1

    .line 82
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/k;->R()Lcom/twitter/library/provider/t;

    move-result-object v2

    .line 84
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3, v1}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/model/dms/p;ZLaut;)V

    .line 85
    invoke-virtual {v1}, Laut;->a()V

    .line 88
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 34
    check-cast p3, Lcom/twitter/library/api/dm/k$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/api/dm/k;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/dm/k$a;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/service/d$a;
    .locals 4

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/k;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 51
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "dm"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "welcome_messages"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "add_to_conversation"

    aput-object v3, v1, v2

    .line 52
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->b()Lcom/twitter/library/service/d$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/dm/k;->h:Lcom/twitter/network/apache/entity/c;

    .line 54
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/apache/e;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 50
    return-object v0
.end method

.method protected b(Lcom/twitter/library/service/u;)Z
    .locals 3

    .prologue
    .line 60
    :try_start_0
    new-instance v0, Lcom/twitter/model/json/dms/JsonWelcomeMessageRequestData;

    invoke-direct {v0}, Lcom/twitter/model/json/dms/JsonWelcomeMessageRequestData;-><init>()V

    .line 61
    iget-object v1, p0, Lcom/twitter/library/api/dm/k;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/model/json/dms/JsonWelcomeMessageRequestData;->a:Ljava/lang/String;

    .line 62
    iget-object v1, p0, Lcom/twitter/library/api/dm/k;->g:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/model/json/dms/JsonWelcomeMessageRequestData;->b:Ljava/lang/String;

    .line 63
    new-instance v1, Lcom/twitter/network/apache/entity/c;

    invoke-static {v0}, Lcom/twitter/model/json/common/g;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "UTF-8"

    invoke-direct {v1, v0, v2}, Lcom/twitter/network/apache/entity/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/twitter/library/api/dm/k;->h:Lcom/twitter/network/apache/entity/c;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    const/4 v0, 0x1

    .line 66
    :goto_0
    return v0

    .line 65
    :catch_0
    move-exception v0

    .line 66
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected e()Lcom/twitter/library/api/dm/k$a;
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/twitter/library/api/dm/k$a;

    invoke-direct {v0}, Lcom/twitter/library/api/dm/k$a;-><init>()V

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/k;->e()Lcom/twitter/library/api/dm/k$a;

    move-result-object v0

    return-object v0
.end method
