.class public Lcom/twitter/library/api/dm/x;
.super Lcom/twitter/library/api/dm/c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/api/dm/c",
        "<",
        "Lcom/twitter/library/api/i;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/twitter/library/api/dm/x;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/dm/c;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 35
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/async/service/j;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 78
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->f()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/x;->R()Lcom/twitter/library/provider/t;

    move-result-object v1

    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/api/dm/x;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-wide v4, v0, Lcom/twitter/library/service/v;->c:J

    const-string/jumbo v6, "server"

    iget-object v7, p0, Lcom/twitter/library/api/dm/x;->a:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Lcom/twitter/library/provider/t;->a(IIJLjava/lang/String;Ljava/lang/String;Laut;)Z

    .line 86
    :cond_0
    return-void
.end method

.method protected b()Lcom/twitter/library/service/d$a;
    .locals 4

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/x;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->b:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 41
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "dm"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "update_last_seen_event_id"

    aput-object v3, v1, v2

    .line 42
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "last_seen_event_id"

    iget-object v2, p0, Lcom/twitter/library/api/dm/x;->a:Ljava/lang/String;

    .line 43
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "request_id"

    .line 44
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 40
    return-object v0
.end method

.method protected b(Lcom/twitter/library/service/u;)Z
    .locals 9

    .prologue
    const/16 v2, 0xe

    const/4 v3, 0x0

    .line 54
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/x;->S()Laut;

    move-result-object v8

    .line 55
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/x;->R()Lcom/twitter/library/provider/t;

    move-result-object v1

    .line 57
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/twitter/library/provider/t;->b(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/dm/x;->a:Ljava/lang/String;

    .line 60
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/x;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-wide v4, v0, Lcom/twitter/library/service/v;->c:J

    const-string/jumbo v6, "server"

    .line 59
    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/t;->b(IIJLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/x;->M()Lcom/twitter/library/service/v;

    move-result-object v4

    iget-wide v4, v4, Lcom/twitter/library/service/v;->c:J

    const-string/jumbo v6, "local"

    iget-object v7, p0, Lcom/twitter/library/api/dm/x;->a:Ljava/lang/String;

    .line 62
    invoke-virtual/range {v1 .. v8}, Lcom/twitter/library/provider/t;->a(IIJLjava/lang/String;Ljava/lang/String;Laut;)Z

    move-result v2

    .line 65
    if-nez v0, :cond_2

    const-wide/16 v0, 0x0

    .line 69
    :goto_0
    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/twitter/library/api/dm/x;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v0, v4, v0

    if-lez v0, :cond_1

    .line 70
    :cond_0
    invoke-virtual {v8}, Laut;->a()V

    .line 71
    const/4 v3, 0x1

    .line 73
    :cond_1
    return v3

    .line 65
    :cond_2
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method protected e()Lcom/twitter/library/api/i;
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/x;->e()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method
