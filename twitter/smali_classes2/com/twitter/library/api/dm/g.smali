.class public Lcom/twitter/library/api/dm/g;
.super Lcom/twitter/library/api/dm/c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/api/dm/c",
        "<",
        "Lcom/twitter/library/api/dm/d;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/twitter/library/api/dm/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/api/dm/g;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/twitter/library/api/dm/g;->a:Ljava/lang/String;

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/dm/c;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/api/dm/g;->c:Z

    .line 35
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/dm/d;)V
    .locals 12

    .prologue
    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 87
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    invoke-virtual {p3}, Lcom/twitter/library/api/dm/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/k;

    .line 89
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/g;->S()Laut;

    move-result-object v2

    .line 90
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/g;->R()Lcom/twitter/library/provider/t;

    move-result-object v1

    .line 92
    invoke-virtual {v1, v0, v3, v2}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/model/dms/p;ZLaut;)V

    .line 93
    invoke-virtual {v2}, Laut;->a()V

    .line 94
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/g;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-wide v4, v2, Lcom/twitter/library/service/v;->c:J

    .line 95
    const/16 v2, 0xd

    const-wide/16 v6, 0x0

    .line 96
    invoke-virtual {v0}, Lcom/twitter/model/dms/k;->a()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    .line 95
    invoke-virtual/range {v1 .. v8}, Lcom/twitter/library/provider/t;->a(IIJJLjava/lang/String;)V

    .line 97
    invoke-static {v4, v5}, Lcom/twitter/library/database/dm/a;->b(J)Lcom/twitter/library/database/dm/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/database/dm/a;->a(Lcom/twitter/model/dms/p;)V

    .line 98
    iget v0, v0, Lcom/twitter/model/dms/k;->b:I

    if-ne v9, v0, :cond_0

    move v3, v9

    :cond_0
    iput-boolean v3, p0, Lcom/twitter/library/api/dm/g;->c:Z

    .line 102
    :goto_0
    return-void

    .line 100
    :cond_1
    iput-boolean v9, p0, Lcom/twitter/library/api/dm/g;->c:Z

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 26
    check-cast p3, Lcom/twitter/library/api/dm/d;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/api/dm/g;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/dm/d;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/service/d$a;
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v6, 0x0

    const/4 v3, 0x1

    .line 57
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/g;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string/jumbo v2, "dm"

    aput-object v2, v1, v6

    const-string/jumbo v2, "user_inbox"

    aput-object v2, v1, v3

    .line 58
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->b()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "dm_users"

    .line 60
    invoke-virtual {v0, v1, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "include_groups"

    .line 61
    invoke-virtual {v0, v1, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->c()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->d()Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 65
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/g;->R()Lcom/twitter/library/provider/t;

    move-result-object v1

    const/16 v2, 0xd

    .line 66
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/g;->M()Lcom/twitter/library/service/v;

    move-result-object v3

    iget-wide v4, v3, Lcom/twitter/library/service/v;->c:J

    invoke-virtual {v1, v2, v6, v4, v5}, Lcom/twitter/library/provider/t;->a(IIJ)Ljava/lang/String;

    move-result-object v1

    .line 65
    invoke-static {v1, v8, v9}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 67
    cmp-long v1, v2, v8

    if-eqz v1, :cond_0

    .line 68
    const-string/jumbo v1, "max_id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    .line 71
    :cond_0
    return-object v0
.end method

.method protected b(Lcom/twitter/library/service/u;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 43
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/g;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    invoke-virtual {p1, v1}, Lcom/twitter/library/service/u;->a(Z)V

    .line 45
    const/4 v0, 0x0

    .line 50
    :goto_0
    return v0

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/dm/g;->p:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/dm/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 47
    new-instance v2, Lcom/twitter/library/api/dm/j;

    iget-object v3, p0, Lcom/twitter/library/api/dm/g;->p:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/library/api/dm/g;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/v;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/api/dm/j;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V

    invoke-virtual {v2}, Lcom/twitter/library/api/dm/j;->O()Lcom/twitter/library/service/u;

    move v0, v1

    .line 48
    goto :goto_0

    :cond_1
    move v0, v1

    .line 50
    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/twitter/library/api/dm/g;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/twitter/library/api/dm/g;->g()Lcom/twitter/library/api/dm/d;

    move-result-object v0

    return-object v0
.end method

.method protected g()Lcom/twitter/library/api/dm/d;
    .locals 1

    .prologue
    .line 81
    new-instance v0, Lcom/twitter/library/api/dm/d;

    invoke-direct {v0}, Lcom/twitter/library/api/dm/d;-><init>()V

    return-object v0
.end method

.method h()Z
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x1

    return v0
.end method
