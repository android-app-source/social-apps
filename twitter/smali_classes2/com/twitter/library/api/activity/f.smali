.class public Lcom/twitter/library/api/activity/f;
.super Lbge;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbge",
        "<",
        "Lcom/twitter/library/api/i",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/twitter/model/core/ac;",
        ">;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JI)V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/twitter/library/api/activity/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lbge;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 40
    iput-wide p3, p0, Lcom/twitter/library/api/activity/f;->a:J

    .line 41
    iput p5, p0, Lcom/twitter/library/api/activity/f;->b:I

    .line 42
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/i",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 76
    invoke-super/range {p0 .. p3}, Lbge;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V

    .line 77
    if-eqz p3, :cond_1

    invoke-virtual/range {p3 .. p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 78
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/activity/f;->F()J

    move-result-wide v20

    .line 79
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/activity/f;->S()Laut;

    move-result-object v19

    .line 81
    invoke-virtual/range {p3 .. p3}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v18, v4

    check-cast v18, Ljava/util/List;

    .line 82
    new-instance v4, Lcom/twitter/library/api/activity/f$1;

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v4, v0, v1, v2}, Lcom/twitter/library/api/activity/f$1;-><init>(Lcom/twitter/library/api/activity/f;J)V

    move-object/from16 v0, v18

    invoke-static {v0, v4}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Lcpv;)Ljava/util/List;

    move-result-object v5

    .line 90
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/activity/f;->R()Lcom/twitter/library/provider/t;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/twitter/library/api/activity/f;->a:J

    const/4 v8, 0x5

    const-wide/16 v9, -0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {v4 .. v17}, Lcom/twitter/library/provider/t;->a(Ljava/util/Collection;JIJZZZLjava/lang/String;ZLaut;Z)Ljava/util/Collection;

    move-result-object v4

    .line 95
    new-instance v8, Lbsk;

    .line 96
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/activity/f;->R()Lcom/twitter/library/provider/t;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v6

    invoke-direct {v8, v6}, Lbsk;-><init>(Lcom/twitter/database/model/i;)V

    .line 97
    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/library/api/activity/f;->b:I

    move-object/from16 v0, v19

    invoke-virtual {v8, v6, v4, v0}, Lbsk;->a(ILjava/lang/Iterable;Laut;)V

    .line 100
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/activity/f;->D()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/api/activity/f;->b:I

    move-object/from16 v0, v19

    invoke-virtual {v8, v6, v7, v4, v0}, Lbsk;->a(JILaut;)Z

    .line 101
    const-wide/16 v6, 0x0

    cmp-long v4, v20, v6

    if-lez v4, :cond_0

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 104
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v6

    if-ne v4, v6, :cond_0

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/activity/f;->E()I

    move-result v6

    if-ne v4, v6, :cond_0

    .line 106
    invoke-static {v5}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/core/ac;

    .line 107
    invoke-virtual {v4}, Lcom/twitter/model/core/ac;->a()J

    move-result-wide v6

    iget-wide v10, v4, Lcom/twitter/model/core/ac;->g:J

    move-object/from16 v0, p0

    iget v12, v0, Lcom/twitter/library/api/activity/f;->b:I

    move-object v5, v8

    move-wide/from16 v8, v20

    move-object/from16 v13, v19

    invoke-virtual/range {v5 .. v13}, Lbsk;->a(JJJILaut;)V

    .line 112
    :cond_0
    invoke-virtual/range {v19 .. v19}, Laut;->a()V

    .line 114
    :cond_1
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 31
    check-cast p3, Lcom/twitter/library/api/i;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/api/activity/f;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/i;)V

    return-void
.end method

.method protected e()Lcom/twitter/library/api/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/i",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    const-class v0, Lcom/twitter/model/core/ac;

    invoke-static {v0}, Lcom/twitter/library/api/j;->a(Ljava/lang/Class;)Lcom/twitter/library/api/j;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/twitter/library/api/activity/f;->e()Lcom/twitter/library/api/i;

    move-result-object v0

    return-object v0
.end method

.method protected g()Lcom/twitter/library/service/d$a;
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    .line 53
    .line 54
    invoke-virtual {p0}, Lcom/twitter/library/api/activity/f;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "statuses"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "mentions_timeline"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 55
    iget v1, p0, Lcom/twitter/library/api/activity/f;->b:I

    packed-switch v1, :pswitch_data_0

    .line 68
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "This Request does not support ActivityType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/twitter/library/api/activity/f;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :pswitch_0
    const-string/jumbo v1, "filters"

    const-string/jumbo v2, "filtered"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 65
    :goto_0
    :pswitch_1
    return-object v0

    .line 61
    :pswitch_2
    const-string/jumbo v1, "filters"

    const-string/jumbo v2, "following"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    goto :goto_0

    .line 55
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
