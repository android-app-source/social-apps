.class public Lcom/twitter/library/api/activity/b;
.super Lcom/twitter/library/service/j;
.source "Twttr"


# instance fields
.field private final a:J

.field private final b:I

.field private final c:Z

.field private final g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JZIZ)V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/twitter/library/api/activity/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/j;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 28
    iput-wide p3, p0, Lcom/twitter/library/api/activity/b;->a:J

    .line 29
    iput-boolean p5, p0, Lcom/twitter/library/api/activity/b;->g:Z

    .line 30
    iput p6, p0, Lcom/twitter/library/api/activity/b;->b:I

    .line 31
    iput-boolean p7, p0, Lcom/twitter/library/api/activity/b;->c:Z

    .line 32
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 11

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/twitter/library/api/activity/b;->h()Lcom/twitter/library/service/v;

    move-result-object v6

    .line 37
    invoke-virtual {p0}, Lcom/twitter/library/api/activity/b;->s()Lcom/twitter/library/provider/t;

    move-result-object v0

    .line 38
    invoke-virtual {p0}, Lcom/twitter/library/api/activity/b;->t()Laut;

    move-result-object v4

    .line 39
    iget-boolean v1, p0, Lcom/twitter/library/api/activity/b;->g:Z

    if-eqz v1, :cond_1

    .line 41
    iget v1, p0, Lcom/twitter/library/api/activity/b;->b:I

    iget-boolean v2, p0, Lcom/twitter/library/api/activity/b;->c:Z

    invoke-virtual {v0, v1, v4, v2}, Lcom/twitter/library/provider/t;->a(ILaut;Z)V

    .line 45
    :goto_0
    invoke-static {}, Lcom/twitter/library/provider/j;->c()Lcom/twitter/library/provider/j;

    move-result-object v5

    .line 46
    new-instance v1, Lbsk;

    .line 47
    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v0

    invoke-direct {v1, v0}, Lbsk;-><init>(Lcom/twitter/database/model/i;)V

    .line 48
    iget v0, p0, Lcom/twitter/library/api/activity/b;->b:I

    invoke-virtual {v1, v0}, Lbsk;->a(I)I

    move-result v9

    .line 49
    iget-wide v6, v6, Lcom/twitter/library/service/v;->c:J

    const-string/jumbo v8, "unread_interactions"

    move-object v10, v4

    invoke-virtual/range {v5 .. v10}, Lcom/twitter/library/provider/j;->a(JLjava/lang/String;ILaut;)I

    .line 50
    iget-boolean v0, p0, Lcom/twitter/library/api/activity/b;->c:Z

    if-eqz v0, :cond_0

    .line 51
    invoke-virtual {v4}, Laut;->a()V

    .line 53
    :cond_0
    return-void

    .line 43
    :cond_1
    iget v1, p0, Lcom/twitter/library/api/activity/b;->b:I

    iget-wide v2, p0, Lcom/twitter/library/api/activity/b;->a:J

    iget-boolean v5, p0, Lcom/twitter/library/api/activity/b;->c:Z

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/provider/t;->a(IJLaut;Z)I

    goto :goto_0
.end method
