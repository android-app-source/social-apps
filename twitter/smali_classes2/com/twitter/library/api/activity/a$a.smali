.class public final Lcom/twitter/library/api/activity/a$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/api/activity/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/library/api/activity/a;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Z

.field private b:Z

.field private c:I

.field private d:J

.field private e:J

.field private f:J

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcac;",
            ">;"
        }
    .end annotation
.end field

.field private h:Laut;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/api/activity/a$a;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/twitter/library/api/activity/a$a;->a:Z

    return v0
.end method

.method static synthetic b(Lcom/twitter/library/api/activity/a$a;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/twitter/library/api/activity/a$a;->b:Z

    return v0
.end method

.method static synthetic c(Lcom/twitter/library/api/activity/a$a;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/twitter/library/api/activity/a$a;->c:I

    return v0
.end method

.method static synthetic d(Lcom/twitter/library/api/activity/a$a;)J
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/twitter/library/api/activity/a$a;->d:J

    return-wide v0
.end method

.method static synthetic e(Lcom/twitter/library/api/activity/a$a;)J
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/twitter/library/api/activity/a$a;->e:J

    return-wide v0
.end method

.method static synthetic f(Lcom/twitter/library/api/activity/a$a;)J
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/twitter/library/api/activity/a$a;->f:J

    return-wide v0
.end method

.method static synthetic g(Lcom/twitter/library/api/activity/a$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/library/api/activity/a$a;->g:Ljava/util/List;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/library/api/activity/a$a;)Laut;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/library/api/activity/a$a;->h:Laut;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/twitter/library/api/activity/a$a;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Lcom/twitter/library/api/activity/a$a;
    .locals 0

    .prologue
    .line 66
    iput p1, p0, Lcom/twitter/library/api/activity/a$a;->c:I

    .line 67
    return-object p0
.end method

.method public a(J)Lcom/twitter/library/api/activity/a$a;
    .locals 1

    .prologue
    .line 72
    iput-wide p1, p0, Lcom/twitter/library/api/activity/a$a;->d:J

    .line 73
    return-object p0
.end method

.method public a(Laut;)Lcom/twitter/library/api/activity/a$a;
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/twitter/library/api/activity/a$a;->h:Laut;

    .line 97
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/library/api/activity/a$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Lcac;",
            "L::Ljava/util/List",
            "<TA;>;>(T",
            "L;",
            ")",
            "Lcom/twitter/library/api/activity/a$a;"
        }
    .end annotation

    .prologue
    .line 90
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/library/api/activity/a$a;->g:Ljava/util/List;

    .line 91
    return-object p0
.end method

.method public a(Z)Lcom/twitter/library/api/activity/a$a;
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/twitter/library/api/activity/a$a;->a:Z

    .line 55
    return-object p0
.end method

.method public b(J)Lcom/twitter/library/api/activity/a$a;
    .locals 1

    .prologue
    .line 78
    iput-wide p1, p0, Lcom/twitter/library/api/activity/a$a;->e:J

    .line 79
    return-object p0
.end method

.method public b(Z)Lcom/twitter/library/api/activity/a$a;
    .locals 0

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/twitter/library/api/activity/a$a;->b:Z

    .line 61
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/twitter/library/api/activity/a$a;->e()Lcom/twitter/library/api/activity/a;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Lcom/twitter/library/api/activity/a$a;
    .locals 1

    .prologue
    .line 84
    iput-wide p1, p0, Lcom/twitter/library/api/activity/a$a;->f:J

    .line 85
    return-object p0
.end method

.method protected e()Lcom/twitter/library/api/activity/a;
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lcom/twitter/library/api/activity/a;

    invoke-direct {v0, p0}, Lcom/twitter/library/api/activity/a;-><init>(Lcom/twitter/library/api/activity/a$a;)V

    return-object v0
.end method
