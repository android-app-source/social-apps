.class public Lcom/twitter/library/api/activity/a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/api/activity/a$a;
    }
.end annotation


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:I

.field public final d:J

.field public final e:J

.field public final f:J

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcac;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Laut;


# direct methods
.method public constructor <init>(Lcom/twitter/library/api/activity/a$a;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1}, Lcom/twitter/library/api/activity/a$a;->a(Lcom/twitter/library/api/activity/a$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/api/activity/a;->a:Z

    .line 30
    invoke-static {p1}, Lcom/twitter/library/api/activity/a$a;->b(Lcom/twitter/library/api/activity/a$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/api/activity/a;->b:Z

    .line 31
    invoke-static {p1}, Lcom/twitter/library/api/activity/a$a;->c(Lcom/twitter/library/api/activity/a$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/activity/a;->c:I

    .line 32
    invoke-static {p1}, Lcom/twitter/library/api/activity/a$a;->d(Lcom/twitter/library/api/activity/a$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/activity/a;->d:J

    .line 33
    invoke-static {p1}, Lcom/twitter/library/api/activity/a$a;->e(Lcom/twitter/library/api/activity/a$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/activity/a;->e:J

    .line 34
    invoke-static {p1}, Lcom/twitter/library/api/activity/a$a;->f(Lcom/twitter/library/api/activity/a$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/activity/a;->f:J

    .line 35
    invoke-static {p1}, Lcom/twitter/library/api/activity/a$a;->g(Lcom/twitter/library/api/activity/a$a;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/library/api/activity/a;->g:Ljava/util/List;

    .line 36
    invoke-static {p1}, Lcom/twitter/library/api/activity/a$a;->h(Lcom/twitter/library/api/activity/a$a;)Laut;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/activity/a;->h:Laut;

    .line 37
    return-void
.end method
