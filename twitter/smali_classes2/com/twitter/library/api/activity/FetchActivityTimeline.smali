.class public Lcom/twitter/library/api/activity/FetchActivityTimeline;
.super Lbge;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/api/activity/FetchActivityTimeline$ActivityParsedObjectCastException;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbge",
        "<",
        "Lcom/twitter/library/api/y;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lcpv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcpv",
            "<",
            "Lcac;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final h:I

.field private final i:Z

.field private final j:I

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcac;",
            ">;"
        }
    .end annotation
.end field

.field private l:I

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 49
    const-class v0, Lcom/twitter/library/api/activity/FetchActivityTimeline;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->a:Ljava/lang/String;

    .line 52
    const/4 v0, 0x2

    .line 53
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->b:Ljava/util/List;

    .line 54
    new-instance v0, Lcom/twitter/library/api/activity/FetchActivityTimeline$1;

    invoke-direct {v0}, Lcom/twitter/library/api/activity/FetchActivityTimeline$1;-><init>()V

    sput-object v0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->c:Lcpv;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/v;IZ)V
    .locals 2

    .prologue
    .line 85
    sget-object v0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->a:Ljava/lang/String;

    invoke-direct {p0, p1, v0, p2}, Lbge;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/v;)V

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->m:Z

    .line 86
    iput p3, p0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->h:I

    .line 87
    iput-boolean p4, p0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->i:Z

    .line 88
    const-string/jumbo v0, "notifications_tab_include_generic_activities_enabled"

    const/4 v1, 0x0

    .line 89
    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    :goto_0
    iput v0, p0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->j:I

    .line 91
    return-void

    .line 89
    :cond_0
    const/4 v0, 0x7

    goto :goto_0
.end method

.method public static a(Lcom/twitter/library/provider/t;Lbsk;Lcom/twitter/library/api/activity/a;)Ljava/util/List;
    .locals 11
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/provider/t;",
            "Lbsk;",
            "Lcom/twitter/library/api/activity/a;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 323
    iget-wide v0, p2, Lcom/twitter/library/api/activity/a;->e:J

    iget v2, p2, Lcom/twitter/library/api/activity/a;->c:I

    iget-object v3, p2, Lcom/twitter/library/api/activity/a;->h:Laut;

    invoke-virtual {p1, v0, v1, v2, v3}, Lbsk;->a(JILaut;)Z

    move-result v0

    .line 324
    iget-object v1, p2, Lcom/twitter/library/api/activity/a;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 327
    iget-boolean v1, p2, Lcom/twitter/library/api/activity/a;->a:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 328
    iget v0, p2, Lcom/twitter/library/api/activity/a;->c:I

    iget-object v1, p2, Lcom/twitter/library/api/activity/a;->h:Laut;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/provider/t;->a(ILaut;)V

    .line 330
    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    .line 341
    :goto_0
    return-object v0

    .line 333
    :cond_1
    invoke-virtual {p0, p2}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/library/api/activity/a;)Ljava/util/List;

    move-result-object v10

    .line 334
    invoke-static {v10}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcac;

    .line 335
    if-eqz v0, :cond_2

    iget-boolean v1, p2, Lcom/twitter/library/api/activity/a;->b:Z

    if-eqz v1, :cond_2

    .line 338
    invoke-virtual {v0}, Lcac;->d()J

    move-result-wide v2

    .line 339
    iget-wide v4, p2, Lcom/twitter/library/api/activity/a;->f:J

    iget v8, p2, Lcom/twitter/library/api/activity/a;->c:I

    iget-object v9, p2, Lcom/twitter/library/api/activity/a;->h:Laut;

    move-object v1, p1

    move-wide v6, v2

    invoke-virtual/range {v1 .. v9}, Lbsk;->a(JJJILaut;)V

    :cond_2
    move-object v0, v10

    .line 341
    goto :goto_0
.end method

.method static synthetic s()Ljava/util/List;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->b:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/twitter/library/api/activity/FetchActivityTimeline;
    .locals 1

    .prologue
    .line 94
    const-string/jumbo v0, "scribe_event"

    invoke-virtual {p0, v0, p1}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/activity/FetchActivityTimeline;

    return-object v0
.end method

.method public a(Z)Lcom/twitter/library/api/activity/FetchActivityTimeline;
    .locals 0

    .prologue
    .line 98
    iput-boolean p1, p0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->m:Z

    .line 99
    return-object p0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V
    .locals 16

    .prologue
    .line 157
    const/4 v3, 0x0

    .line 158
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 160
    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->h:I

    invoke-static {v2}, Lcab$a;->a(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 162
    invoke-virtual/range {p3 .. p3}, Lcom/twitter/library/api/y;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    new-instance v3, Lcom/twitter/library/api/activity/FetchActivityTimeline$2;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/twitter/library/api/activity/FetchActivityTimeline$2;-><init>(Lcom/twitter/library/api/activity/FetchActivityTimeline;)V

    invoke-static {v2, v3}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Lcpv;)Ljava/util/List;

    move-result-object v2

    .line 174
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->M()Lcom/twitter/library/service/v;

    move-result-object v3

    .line 175
    iget-wide v12, v3, Lcom/twitter/library/service/v;->c:J

    .line 177
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->F()J

    move-result-wide v6

    .line 178
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->E()I

    move-result v3

    int-to-long v4, v3

    .line 179
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->D()J

    move-result-wide v8

    .line 181
    const-wide/16 v10, 0x0

    cmp-long v3, v8, v10

    if-lez v3, :cond_4

    const/4 v3, 0x1

    move v10, v3

    .line 185
    :goto_1
    const-wide/16 v14, 0x0

    cmp-long v3, v6, v14

    if-lez v3, :cond_6

    .line 186
    new-instance v3, Lcom/twitter/library/api/activity/FetchActivityTimeline$3;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v6, v7}, Lcom/twitter/library/api/activity/FetchActivityTimeline$3;-><init>(Lcom/twitter/library/api/activity/FetchActivityTimeline;J)V

    invoke-static {v2, v3}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Lcpv;)Ljava/util/List;

    move-result-object v3

    .line 194
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v11

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v11, v2, :cond_5

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v14, v2

    cmp-long v2, v14, v4

    if-nez v2, :cond_5

    const/4 v2, 0x1

    :goto_2
    move v4, v2

    .line 200
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->R()Lcom/twitter/library/provider/t;

    move-result-object v2

    .line 201
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->S()Laut;

    move-result-object v5

    .line 203
    new-instance v11, Lbsk;

    .line 204
    invoke-virtual {v2}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v14

    invoke-direct {v11, v14}, Lbsk;-><init>(Lcom/twitter/database/model/i;)V

    new-instance v14, Lcom/twitter/library/api/activity/a$a;

    invoke-direct {v14}, Lcom/twitter/library/api/activity/a$a;-><init>()V

    .line 206
    invoke-virtual {v14, v4}, Lcom/twitter/library/api/activity/a$a;->b(Z)Lcom/twitter/library/api/activity/a$a;

    move-result-object v4

    .line 207
    invoke-virtual {v4, v10}, Lcom/twitter/library/api/activity/a$a;->a(Z)Lcom/twitter/library/api/activity/a$a;

    move-result-object v4

    move-object/from16 v0, p0

    iget v14, v0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->h:I

    .line 208
    invoke-virtual {v4, v14}, Lcom/twitter/library/api/activity/a$a;->a(I)Lcom/twitter/library/api/activity/a$a;

    move-result-object v4

    .line 209
    invoke-virtual {v4, v12, v13}, Lcom/twitter/library/api/activity/a$a;->a(J)Lcom/twitter/library/api/activity/a$a;

    move-result-object v4

    .line 210
    invoke-virtual {v4, v8, v9}, Lcom/twitter/library/api/activity/a$a;->b(J)Lcom/twitter/library/api/activity/a$a;

    move-result-object v4

    .line 211
    invoke-virtual {v4, v6, v7}, Lcom/twitter/library/api/activity/a$a;->c(J)Lcom/twitter/library/api/activity/a$a;

    move-result-object v4

    .line 212
    invoke-virtual {v4, v3}, Lcom/twitter/library/api/activity/a$a;->a(Ljava/util/List;)Lcom/twitter/library/api/activity/a$a;

    move-result-object v3

    .line 213
    invoke-virtual {v3, v5}, Lcom/twitter/library/api/activity/a$a;->a(Laut;)Lcom/twitter/library/api/activity/a$a;

    move-result-object v3

    .line 214
    invoke-virtual {v3}, Lcom/twitter/library/api/activity/a$a;->q()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/activity/a;

    .line 203
    invoke-static {v2, v11, v3}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->a(Lcom/twitter/library/provider/t;Lbsk;Lcom/twitter/library/api/activity/a;)Ljava/util/List;

    move-result-object v11

    .line 215
    invoke-virtual {v5}, Laut;->a()V

    .line 216
    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->k:Ljava/util/List;

    .line 217
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v9

    .line 219
    const-wide/16 v4, 0x0

    .line 220
    const/4 v3, 0x0

    .line 221
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->m:Z

    if-eqz v6, :cond_1

    .line 222
    new-instance v3, Lcom/twitter/library/api/activity/e;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->p:Landroid/content/Context;

    .line 223
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->M()Lcom/twitter/library/service/v;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/api/activity/e;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V

    .line 224
    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Lcom/twitter/library/api/activity/e;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/service/s;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/activity/e;

    .line 225
    invoke-virtual {v3}, Lcom/twitter/library/api/activity/e;->O()Lcom/twitter/library/service/u;

    move-result-object v3

    .line 226
    invoke-virtual {v3}, Lcom/twitter/library/service/u;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 227
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->p:Landroid/content/Context;

    iget-object v3, v3, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v5, "act_read_pos"

    .line 228
    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 227
    invoke-static {v4, v12, v13, v6, v7}, Lcom/twitter/library/client/a;->a(Landroid/content/Context;JJ)V

    .line 232
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->p:Landroid/content/Context;

    invoke-static {v3, v12, v13}, Lcom/twitter/library/client/a;->a(Landroid/content/Context;J)J

    move-result-wide v4

    .line 233
    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->h:I

    .line 234
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->S()Laut;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/twitter/library/provider/t;->a(IJLaut;Z)I

    move-result v3

    .line 235
    if-lez v3, :cond_7

    const/4 v3, 0x1

    .line 238
    :cond_1
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->S()Laut;

    move-result-object v8

    .line 239
    if-nez v10, :cond_9

    if-lez v9, :cond_9

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->m:Z

    if-eqz v6, :cond_9

    .line 240
    const/4 v7, 0x1

    .line 241
    const/4 v3, 0x0

    .line 242
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v6, v3

    :cond_2
    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcac;

    .line 243
    instance-of v11, v3, Lcap;

    if-eqz v11, :cond_2

    .line 246
    invoke-static {v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcap;

    .line 247
    iget-wide v14, v3, Lcap;->a:J

    cmp-long v11, v14, v4

    if-lez v11, :cond_2

    .line 250
    iget v3, v3, Lcap;->d:I

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    move v3, v6

    :goto_6
    move v6, v3

    .line 280
    goto :goto_5

    .line 172
    :cond_3
    invoke-virtual/range {p3 .. p3}, Lcom/twitter/library/api/y;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    goto/16 :goto_0

    .line 181
    :cond_4
    const/4 v3, 0x0

    move v10, v3

    goto/16 :goto_1

    .line 194
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 197
    :cond_6
    const/4 v3, 0x0

    move v4, v3

    move-object v3, v2

    goto/16 :goto_3

    .line 235
    :cond_7
    const/4 v3, 0x0

    goto :goto_4

    .line 253
    :pswitch_1
    or-int/lit8 v3, v6, 0x1

    .line 254
    goto :goto_6

    .line 257
    :pswitch_2
    or-int/lit8 v3, v6, 0x2

    .line 258
    goto :goto_6

    .line 261
    :pswitch_3
    or-int/lit8 v3, v6, 0x4

    .line 262
    goto :goto_6

    .line 265
    :pswitch_4
    or-int/lit8 v3, v6, 0x8

    .line 266
    goto :goto_6

    .line 269
    :pswitch_5
    or-int/lit8 v3, v6, 0x10

    .line 270
    goto :goto_6

    .line 273
    :pswitch_6
    or-int/lit16 v3, v6, 0x200

    .line 274
    goto :goto_6

    .line 281
    :cond_8
    move-object/from16 v0, p0

    iput v6, v0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->l:I

    .line 282
    invoke-static {}, Lcom/twitter/library/provider/j;->c()Lcom/twitter/library/provider/j;

    move-result-object v3

    invoke-virtual {v3, v12, v13, v6, v8}, Lcom/twitter/library/provider/j;->a(JILaut;)I

    move v3, v7

    .line 285
    :cond_9
    if-eqz v3, :cond_a

    .line 286
    new-instance v3, Lbsk;

    invoke-virtual {v2}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v2

    invoke-direct {v3, v2}, Lbsk;-><init>(Lcom/twitter/database/model/i;)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->h:I

    invoke-virtual {v3, v2}, Lbsk;->a(I)I

    move-result v7

    .line 288
    invoke-static {}, Lcom/twitter/library/provider/j;->c()Lcom/twitter/library/provider/j;

    move-result-object v3

    const-string/jumbo v6, "unread_interactions"

    move-wide v4, v12

    invoke-virtual/range {v3 .. v8}, Lcom/twitter/library/provider/j;->a(JLjava/lang/String;ILaut;)I

    .line 292
    :cond_a
    invoke-virtual {v8}, Laut;->a()V

    .line 293
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v3, "scribe_item_count"

    invoke-virtual {v2, v3, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move v2, v9

    .line 316
    :goto_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->o:Landroid/os/Bundle;

    const-string/jumbo v4, "count"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 317
    return-void

    .line 295
    :cond_b
    invoke-virtual/range {p3 .. p3}, Lcom/twitter/library/api/y;->b()Ljava/lang/Object;

    move-result-object v4

    .line 297
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->o:Landroid/os/Bundle;

    const-string/jumbo v6, "custom_errors"

    .line 298
    invoke-static {v4}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/core/z;

    invoke-static {v2}, Lcom/twitter/model/core/z;->a(Lcom/twitter/model/core/z;)[I

    move-result-object v2

    .line 297
    invoke-virtual {v5, v6, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move v2, v3

    .line 314
    goto :goto_7

    .line 299
    :catch_0
    move-exception v2

    .line 301
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/network/HttpOperation;->m()Lcom/twitter/network/l;

    move-result-object v5

    .line 302
    if-eqz v5, :cond_e

    .line 303
    new-instance v6, Lcpb;

    new-instance v7, Lcom/twitter/library/api/activity/FetchActivityTimeline$ActivityParsedObjectCastException;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v2}, Lcom/twitter/library/api/activity/FetchActivityTimeline$ActivityParsedObjectCastException;-><init>(Lcom/twitter/library/api/activity/FetchActivityTimeline;Ljava/lang/Throwable;)V

    invoke-direct {v6, v7}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    const-string/jumbo v2, "status_code"

    iget v7, v5, Lcom/twitter/network/l;->a:I

    .line 304
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v2, v7}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v2

    const-string/jumbo v6, "error_code"

    iget v7, v5, Lcom/twitter/network/l;->j:I

    .line 305
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v6

    .line 306
    iget-object v2, v5, Lcom/twitter/network/l;->b:Ljava/lang/String;

    invoke-static {v2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 307
    const-string/jumbo v2, "reason_phrase"

    iget-object v5, v5, Lcom/twitter/network/l;->b:Ljava/lang/String;

    invoke-virtual {v6, v2, v5}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 309
    :cond_c
    instance-of v2, v4, Ljava/util/List;

    if-eqz v2, :cond_d

    .line 310
    const-string/jumbo v5, "result_size"

    invoke-static {v4}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v5, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 312
    :cond_d
    invoke-static {v6}, Lcpd;->c(Lcpb;)V

    :cond_e
    move v2, v3

    goto :goto_7

    .line 250
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 45
    check-cast p3, Lcom/twitter/library/api/y;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V

    return-void
.end method

.method public e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->k:Ljava/util/List;

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->h()Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method

.method protected g()Lcom/twitter/library/service/d$a;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 113
    invoke-virtual {p0}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->F()J

    move-result-wide v0

    .line 115
    invoke-virtual {p0}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->J()Lcom/twitter/library/service/d$a;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, "activity"

    aput-object v5, v3, v4

    const-string/jumbo v4, "about_me"

    aput-object v4, v3, v6

    .line 116
    invoke-virtual {v2, v3}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v2

    .line 118
    iget v3, p0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->h:I

    packed-switch v3, :pswitch_data_0

    .line 145
    :cond_0
    :goto_0
    const-string/jumbo v3, "model_version"

    iget v4, p0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->j:I

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    .line 146
    const-string/jumbo v3, "send_error_codes"

    invoke-virtual {v2, v3, v6}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 148
    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 149
    const-string/jumbo v0, "latest_results"

    invoke-virtual {v2, v0, v6}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 151
    :cond_1
    return-object v2

    .line 121
    :pswitch_0
    iget-boolean v3, p0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->i:Z

    if-eqz v3, :cond_0

    .line 122
    const-string/jumbo v3, "filters"

    const-string/jumbo v4, "filtered"

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto :goto_0

    .line 129
    :pswitch_1
    const-string/jumbo v3, "filters"

    const-string/jumbo v4, "following"

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto :goto_0

    .line 134
    :pswitch_2
    const-string/jumbo v3, "filters"

    const-string/jumbo v4, "verified"

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto :goto_0

    .line 118
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected h()Lcom/twitter/library/api/y;
    .locals 2

    .prologue
    .line 347
    iget v0, p0, Lcom/twitter/library/api/activity/FetchActivityTimeline;->j:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    const/16 v0, 0x1c

    :goto_0
    invoke-static {v0}, Lcom/twitter/library/api/y;->a(I)Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0

    :cond_0
    const/16 v0, 0x1b

    goto :goto_0
.end method
