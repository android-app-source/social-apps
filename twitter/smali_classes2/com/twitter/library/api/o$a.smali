.class public Lcom/twitter/library/api/o$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/api/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:J

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/api/o$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/library/api/o$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/library/api/o$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/library/api/o$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/library/api/o$a;)J
    .locals 2

    .prologue
    .line 95
    iget-wide v0, p0, Lcom/twitter/library/api/o$a;->c:J

    return-wide v0
.end method

.method static synthetic d(Lcom/twitter/library/api/o$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/library/api/o$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/library/api/o$a;)Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/twitter/library/api/o$a;->e:Z

    return v0
.end method

.method static synthetic f(Lcom/twitter/library/api/o$a;)I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/twitter/library/api/o$a;->f:I

    return v0
.end method

.method static synthetic g(Lcom/twitter/library/api/o$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/library/api/o$a;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/library/api/o$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/library/api/o$a;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/library/api/o$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/library/api/o$a;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/library/api/o$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/library/api/o$a;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/library/api/o$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/library/api/o$a;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/library/api/o$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/library/api/o$a;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic m(Lcom/twitter/library/api/o$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/library/api/o$a;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/library/api/o$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/library/api/o$a;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Lcom/twitter/library/api/o$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/library/api/o$a;->o:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p(Lcom/twitter/library/api/o$a;)J
    .locals 2

    .prologue
    .line 95
    iget-wide v0, p0, Lcom/twitter/library/api/o$a;->p:J

    return-wide v0
.end method


# virtual methods
.method public a(I)Lcom/twitter/library/api/o$a;
    .locals 0

    .prologue
    .line 139
    iput p1, p0, Lcom/twitter/library/api/o$a;->f:I

    .line 140
    return-object p0
.end method

.method public a(J)Lcom/twitter/library/api/o$a;
    .locals 1

    .prologue
    .line 124
    iput-wide p1, p0, Lcom/twitter/library/api/o$a;->c:J

    .line 125
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/api/o$a;
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/twitter/library/api/o$a;->a:Ljava/lang/String;

    .line 115
    return-object p0
.end method

.method public a(Z)Lcom/twitter/library/api/o$a;
    .locals 0

    .prologue
    .line 134
    iput-boolean p1, p0, Lcom/twitter/library/api/o$a;->e:Z

    .line 135
    return-object p0
.end method

.method public a()Lcom/twitter/library/api/o;
    .locals 2

    .prologue
    .line 194
    new-instance v0, Lcom/twitter/library/api/o;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/api/o;-><init>(Lcom/twitter/library/api/o$a;Lcom/twitter/library/api/o$1;)V

    return-object v0
.end method

.method public b(J)Lcom/twitter/library/api/o$a;
    .locals 1

    .prologue
    .line 189
    iput-wide p1, p0, Lcom/twitter/library/api/o$a;->p:J

    .line 190
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/library/api/o$a;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/twitter/library/api/o$a;->b:Ljava/lang/String;

    .line 120
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/library/api/o$a;
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/twitter/library/api/o$a;->d:Ljava/lang/String;

    .line 130
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/library/api/o$a;
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/twitter/library/api/o$a;->g:Ljava/lang/String;

    .line 145
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/library/api/o$a;
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/twitter/library/api/o$a;->h:Ljava/lang/String;

    .line 150
    return-object p0
.end method

.method public f(Ljava/lang/String;)Lcom/twitter/library/api/o$a;
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/twitter/library/api/o$a;->i:Ljava/lang/String;

    .line 155
    return-object p0
.end method

.method public g(Ljava/lang/String;)Lcom/twitter/library/api/o$a;
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/twitter/library/api/o$a;->j:Ljava/lang/String;

    .line 160
    return-object p0
.end method

.method public h(Ljava/lang/String;)Lcom/twitter/library/api/o$a;
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/twitter/library/api/o$a;->k:Ljava/lang/String;

    .line 165
    return-object p0
.end method

.method public i(Ljava/lang/String;)Lcom/twitter/library/api/o$a;
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/twitter/library/api/o$a;->l:Ljava/lang/String;

    .line 170
    return-object p0
.end method

.method public j(Ljava/lang/String;)Lcom/twitter/library/api/o$a;
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/twitter/library/api/o$a;->m:Ljava/lang/String;

    .line 175
    return-object p0
.end method

.method public k(Ljava/lang/String;)Lcom/twitter/library/api/o$a;
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/twitter/library/api/o$a;->n:Ljava/lang/String;

    .line 180
    return-object p0
.end method

.method public l(Ljava/lang/String;)Lcom/twitter/library/api/o$a;
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/twitter/library/api/o$a;->o:Ljava/lang/String;

    .line 185
    return-object p0
.end method
