.class public Lcom/twitter/library/client/b;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lcom/twitter/library/client/b;


# instance fields
.field private b:Lcom/twitter/library/api/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    new-instance v0, Lcom/twitter/library/client/b;

    invoke-direct {v0}, Lcom/twitter/library/client/b;-><init>()V

    sput-object v0, Lcom/twitter/library/client/b;->a:Lcom/twitter/library/client/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    return-void
.end method


# virtual methods
.method public declared-synchronized a()Lcom/twitter/library/api/c;
    .locals 1

    .prologue
    .line 24
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/b;->b:Lcom/twitter/library/api/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/twitter/library/api/c;)V
    .locals 1

    .prologue
    .line 31
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/twitter/library/client/b;->b:Lcom/twitter/library/api/c;

    .line 32
    const-class v0, Lcom/twitter/library/client/b;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    monitor-exit p0

    return-void

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
