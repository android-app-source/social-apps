.class public final Lcom/twitter/library/client/m$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/client/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field final a:Landroid/net/Uri;

.field final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/app/common/base/BaseFragment;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/twitter/app/common/base/b;

.field d:Ljava/lang/CharSequence;

.field e:Ljava/lang/String;

.field f:Ljava/lang/Object;

.field g:I

.field h:Z

.field i:I

.field j:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/app/common/base/BaseFragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    iput-object p1, p0, Lcom/twitter/library/client/m$a;->a:Landroid/net/Uri;

    .line 148
    invoke-virtual {p1}, Landroid/net/Uri;->hashCode()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/client/m$a;->i:I

    .line 149
    iput-object p2, p0, Lcom/twitter/library/client/m$a;->b:Ljava/lang/Class;

    .line 150
    return-void
.end method


# virtual methods
.method public a(I)Lcom/twitter/library/client/m$a;
    .locals 0

    .prologue
    .line 184
    iput p1, p0, Lcom/twitter/library/client/m$a;->g:I

    .line 185
    return-object p0
.end method

.method public a(Lcom/twitter/app/common/base/b;)Lcom/twitter/library/client/m$a;
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/twitter/library/client/m$a;->c:Lcom/twitter/app/common/base/b;

    .line 158
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Lcom/twitter/library/client/m$a;
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/twitter/library/client/m$a;->d:Ljava/lang/CharSequence;

    .line 167
    return-object p0
.end method

.method public a(Ljava/lang/Object;)Lcom/twitter/library/client/m$a;
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/twitter/library/client/m$a;->f:Ljava/lang/Object;

    .line 210
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/client/m$a;
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/twitter/library/client/m$a;->e:Ljava/lang/String;

    .line 176
    return-object p0
.end method

.method public a(Z)Lcom/twitter/library/client/m$a;
    .locals 0

    .prologue
    .line 203
    iput-boolean p1, p0, Lcom/twitter/library/client/m$a;->h:Z

    .line 204
    return-object p0
.end method

.method public a()Lcom/twitter/library/client/m;
    .locals 1

    .prologue
    .line 224
    new-instance v0, Lcom/twitter/library/client/m;

    invoke-direct {v0, p0}, Lcom/twitter/library/client/m;-><init>(Lcom/twitter/library/client/m$a;)V

    return-object v0
.end method

.method public b(I)Lcom/twitter/library/client/m$a;
    .locals 0

    .prologue
    .line 194
    iput p1, p0, Lcom/twitter/library/client/m$a;->i:I

    .line 195
    return-object p0
.end method

.method public b(Ljava/lang/CharSequence;)Lcom/twitter/library/client/m$a;
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/twitter/library/client/m$a;->j:Ljava/lang/CharSequence;

    .line 219
    return-object p0
.end method
