.class Lcom/twitter/library/client/v$g;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/client/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "g"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/client/v;


# direct methods
.method private constructor <init>(Lcom/twitter/library/client/v;)V
    .locals 0

    .prologue
    .line 1039
    iput-object p1, p0, Lcom/twitter/library/client/v$g;->a:Lcom/twitter/library/client/v;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/library/client/v;Lcom/twitter/library/client/v$1;)V
    .locals 0

    .prologue
    .line 1039
    invoke-direct {p0, p1}, Lcom/twitter/library/client/v$g;-><init>(Lcom/twitter/library/client/v;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 1039
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/v$g;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 5

    .prologue
    .line 1043
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    .line 1044
    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 1045
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v1

    .line 1046
    iget-object v2, p0, Lcom/twitter/library/client/v$g;->a:Lcom/twitter/library/client/v;

    invoke-static {v2}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/v;)Ljava/util/Map;

    move-result-object v2

    iget-object v1, v1, Lcom/twitter/library/service/v;->a:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/client/Session;

    .line 1047
    if-nez v1, :cond_1

    .line 1085
    :cond_0
    :goto_0
    return-void

    .line 1051
    :cond_1
    check-cast p1, Lbak;

    .line 1052
    iget-object v2, p0, Lcom/twitter/library/client/v$g;->a:Lcom/twitter/library/client/v;

    .line 1053
    invoke-static {v2}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/v;)Ljava/util/HashMap;

    move-result-object v2

    iget-object v3, p1, Lbak;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/v$h;

    .line 1054
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1056
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/v$g;->a:Lcom/twitter/library/client/v;

    .line 1057
    invoke-virtual {p1}, Lbak;->t()Lcom/twitter/model/account/LoginResponse;

    move-result-object v3

    invoke-virtual {p1}, Lbak;->h()Lcom/twitter/model/core/TwitterUser;

    move-result-object v4

    .line 1056
    invoke-static {v0, v1, v3, v4}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/v;Lcom/twitter/library/client/Session;Lcom/twitter/model/account/LoginResponse;Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;

    move-result-object v0

    .line 1058
    if-eqz v2, :cond_0

    .line 1059
    invoke-interface {v2, v1, v0}, Lcom/twitter/library/client/v$h;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1061
    :catch_0
    move-exception v0

    .line 1062
    iget-object v0, p0, Lcom/twitter/library/client/v$g;->a:Lcom/twitter/library/client/v;

    invoke-virtual {p1}, Lbak;->s()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/v;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    .line 1063
    if-eqz v2, :cond_0

    .line 1064
    const/4 v0, 0x2

    const/16 v3, 0x190

    .line 1067
    invoke-virtual {p1}, Lbak;->g()Lcom/twitter/library/api/u;

    move-result-object v4

    .line 1064
    invoke-interface {v2, v1, v0, v3, v4}, Lcom/twitter/library/client/v$h;->a(Lcom/twitter/library/client/Session;IILcom/twitter/library/api/u;)V

    goto :goto_0

    .line 1071
    :cond_2
    iget-object v3, p0, Lcom/twitter/library/client/v$g;->a:Lcom/twitter/library/client/v;

    invoke-virtual {p1}, Lbak;->s()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v1, v4}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/v;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    .line 1072
    if-eqz v2, :cond_0

    .line 1073
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    .line 1074
    const/16 v3, 0x19c

    if-ne v0, v3, :cond_3

    .line 1076
    invoke-virtual {p1}, Lbak;->g()Lcom/twitter/library/api/u;

    move-result-object v0

    .line 1075
    invoke-interface {v2, v1, v0}, Lcom/twitter/library/client/v$h;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/u;)V

    goto :goto_0

    .line 1078
    :cond_3
    const/4 v3, 0x1

    .line 1081
    invoke-virtual {p1}, Lbak;->g()Lcom/twitter/library/api/u;

    move-result-object v4

    .line 1078
    invoke-interface {v2, v1, v3, v0, v4}, Lcom/twitter/library/client/v$h;->a(Lcom/twitter/library/client/Session;IILcom/twitter/library/api/u;)V

    goto :goto_0
.end method
