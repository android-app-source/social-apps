.class Lcom/twitter/library/client/c$a;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/client/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/client/c;


# direct methods
.method private constructor <init>(Lcom/twitter/library/client/c;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/twitter/library/client/c$a;->a:Lcom/twitter/library/client/c;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/library/client/c;Lcom/twitter/library/client/c$1;)V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/twitter/library/client/c$a;-><init>(Lcom/twitter/library/client/c;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 122
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/c$a;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 10

    .prologue
    .line 125
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    .line 126
    iget-object v1, p0, Lcom/twitter/library/client/c$a;->a:Lcom/twitter/library/client/c;

    invoke-static {v1}, Lcom/twitter/library/client/c;->b(Lcom/twitter/library/client/c;)Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 127
    new-instance v2, Lcom/twitter/util/a;

    iget-object v3, p0, Lcom/twitter/library/client/c$a;->a:Lcom/twitter/library/client/c;

    invoke-static {v3}, Lcom/twitter/library/client/c;->a(Lcom/twitter/library/client/c;)Landroid/content/Context;

    move-result-object v3

    .line 128
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v3, v4, v5}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 129
    if-eqz v0, :cond_0

    .line 130
    instance-of v0, p1, Lbhh;

    if-eqz v0, :cond_1

    .line 133
    sget-object v0, Lcom/twitter/library/util/af;->a:Ljava/util/Random;

    const/16 v1, 0x258

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 135
    invoke-virtual {v2}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v1

    const-string/jumbo v2, "app_graph_timestamp"

    .line 136
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v4

    int-to-long v6, v0

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    add-long/2addr v4, v6

    invoke-virtual {v1, v2, v4, v5}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;J)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 138
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    const-string/jumbo v0, ""

    .line 141
    instance-of v2, p1, Lbhj;

    if-eqz v2, :cond_3

    .line 142
    check-cast p1, Lbhj;

    invoke-virtual {p1}, Lbhj;->g()Ljava/lang/String;

    move-result-object v0

    .line 146
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/twitter/library/client/c$a;->a:Lcom/twitter/library/client/c;

    invoke-virtual {v2, v0}, Lcom/twitter/library/client/c;->b(Ljava/lang/String;)V

    .line 147
    const-string/jumbo v2, "optin"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/twitter/library/client/c$a;->a:Lcom/twitter/library/client/c;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/twitter/library/client/c;->a(Lcom/twitter/library/client/c;J)V

    goto :goto_0

    .line 143
    :cond_3
    instance-of v2, p1, Lbhk;

    if-eqz v2, :cond_2

    .line 144
    check-cast p1, Lbhk;

    invoke-virtual {p1}, Lbhk;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
