.class public Lcom/twitter/library/client/k;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/client/k$a;,
        Lcom/twitter/library/client/k$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/q",
        "<",
        "Lcom/twitter/library/client/ChromeScribeEvent;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic c:Z

.field private static d:Lcom/twitter/library/client/k;


# instance fields
.field protected a:Z

.field protected b:Lcom/twitter/library/client/k$b;

.field private final e:Lcqt;

.field private f:Landroid/support/customtabs/CustomTabsClient;

.field private g:Landroid/support/customtabs/CustomTabsServiceConnection;

.field private h:Lcom/twitter/library/client/BrowserDataSource;

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/twitter/library/client/k;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/library/client/k;->c:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->a()Lcqt;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/k;->e:Lcqt;

    .line 144
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/client/k;Landroid/support/customtabs/CustomTabsClient;)Landroid/support/customtabs/CustomTabsClient;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/twitter/library/client/k;->f:Landroid/support/customtabs/CustomTabsClient;

    return-object p1
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/library/client/k;
    .locals 4

    .prologue
    .line 73
    const-class v1, Lcom/twitter/library/client/k;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/client/k;->d:Lcom/twitter/library/client/k;

    if-nez v0, :cond_0

    .line 74
    const-class v0, Lcom/twitter/library/client/k;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 75
    new-instance v0, Lcom/twitter/library/client/k;

    invoke-direct {v0}, Lcom/twitter/library/client/k;-><init>()V

    sput-object v0, Lcom/twitter/library/client/k;->d:Lcom/twitter/library/client/k;

    .line 78
    :cond_0
    sget-object v0, Lcom/twitter/library/client/k;->d:Lcom/twitter/library/client/k;

    invoke-virtual {v0}, Lcom/twitter/library/client/k;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 79
    const-string/jumbo v0, "CustomTabs"

    const-string/jumbo v2, "CustomTabsManager requires initialization. Creating connection..."

    invoke-static {v0, v2}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    sget-object v0, Lcom/twitter/library/client/k;->d:Lcom/twitter/library/client/k;

    invoke-virtual {v0, p0}, Lcom/twitter/library/client/k;->c(Landroid/content/Context;)V

    .line 87
    :cond_1
    :goto_0
    sget-object v0, Lcom/twitter/library/client/k;->d:Lcom/twitter/library/client/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 81
    :cond_2
    :try_start_1
    sget-object v0, Lcom/twitter/library/client/k;->d:Lcom/twitter/library/client/k;

    iget-object v0, v0, Lcom/twitter/library/client/k;->f:Landroid/support/customtabs/CustomTabsClient;

    if-eqz v0, :cond_1

    .line 82
    sget-object v0, Lcom/twitter/library/client/k;->d:Lcom/twitter/library/client/k;

    iget-object v0, v0, Lcom/twitter/library/client/k;->f:Landroid/support/customtabs/CustomTabsClient;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/support/customtabs/CustomTabsClient;->warmup(J)Z

    move-result v0

    if-nez v0, :cond_1

    .line 83
    const-string/jumbo v0, "CustomTabs"

    const-string/jumbo v2, "Client warmup failed when retrieving the CustomTabsManager instance"

    invoke-static {v0, v2}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/twitter/library/client/k;)Lcqt;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/library/client/k;->e:Lcqt;

    return-object v0
.end method

.method public static a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 121
    invoke-static {}, Lcom/twitter/library/client/k;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 122
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v1

    invoke-virtual {v1}, Lcqq;->b()Lcqs;

    move-result-object v1

    const-string/jumbo v2, "in_app_browser"

    invoke-interface {v1, v2, v0}, Lcqs;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 121
    :goto_0
    return v0

    .line 122
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/library/client/k;Z)Z
    .locals 0

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/twitter/library/client/k;->i:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/library/client/k;)Landroid/support/customtabs/CustomTabsClient;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/library/client/k;->f:Landroid/support/customtabs/CustomTabsClient;

    return-object v0
.end method

.method public static b()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 129
    invoke-static {}, Lcom/twitter/library/client/k;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 130
    invoke-static {}, Lcom/twitter/library/network/forecaster/c;->a()Lcom/twitter/library/network/forecaster/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/forecaster/c;->f()Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "cct_warmup_enabled"

    .line 131
    invoke-static {v1, v0}, Lcoj;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "cct_warmup_v2_5166"

    .line 132
    invoke-static {v1}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 129
    :cond_0
    return v0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 109
    invoke-static {}, Lcom/twitter/library/client/k;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    invoke-static {}, Lcom/twitter/library/util/f;->a()Lcom/twitter/library/util/f;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/library/util/f;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 109
    :goto_0
    return v0

    .line 110
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;Landroid/content/Context;)Landroid/support/customtabs/CustomTabsSession;
    .locals 4

    .prologue
    .line 372
    const/4 v0, 0x0

    .line 373
    invoke-virtual {p0}, Lcom/twitter/library/client/k;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 374
    sget-boolean v0, Lcom/twitter/library/client/k;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/client/k;->f:Landroid/support/customtabs/CustomTabsClient;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 375
    :cond_0
    new-instance v0, Lcom/twitter/library/client/k$a;

    invoke-direct {v0, p1, p2}, Lcom/twitter/library/client/k$a;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 376
    iget-object v1, p0, Lcom/twitter/library/client/k;->f:Landroid/support/customtabs/CustomTabsClient;

    invoke-virtual {v1, v0}, Landroid/support/customtabs/CustomTabsClient;->newSession(Landroid/support/customtabs/CustomTabsCallback;)Landroid/support/customtabs/CustomTabsSession;

    move-result-object v0

    .line 377
    if-nez v0, :cond_1

    .line 380
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 381
    const-string/jumbo v2, "url"

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 382
    new-instance v2, Lcom/twitter/library/client/ChromeScribeEvent;

    const-string/jumbo v3, "chrome::::error"

    invoke-direct {v2, v3, v1, p2}, Lcom/twitter/library/client/ChromeScribeEvent;-><init>(Ljava/lang/String;Ljava/util/Map;Landroid/content/Context;)V

    invoke-virtual {p0, v2}, Lcom/twitter/library/client/k;->onEvent(Lcom/twitter/library/client/ChromeScribeEvent;)V

    .line 383
    const-string/jumbo v1, "CustomTabs"

    const-string/jumbo v2, "Failed to create a session with the client..."

    invoke-static {v1, v2}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    invoke-direct {p0}, Lcom/twitter/library/client/k;->i()V

    .line 388
    :cond_1
    return-object v0
.end method

.method private static h()Z
    .locals 2

    .prologue
    .line 139
    const-string/jumbo v0, "chrome_custom_tabs_android_enabled"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private i()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 395
    iput-boolean v1, p0, Lcom/twitter/library/client/k;->i:Z

    .line 396
    iput-object v0, p0, Lcom/twitter/library/client/k;->f:Landroid/support/customtabs/CustomTabsClient;

    .line 397
    iput-object v0, p0, Lcom/twitter/library/client/k;->g:Landroid/support/customtabs/CustomTabsServiceConnection;

    .line 398
    iput-object v0, p0, Lcom/twitter/library/client/k;->b:Lcom/twitter/library/client/k$b;

    .line 399
    iput-object v0, p0, Lcom/twitter/library/client/k;->h:Lcom/twitter/library/client/BrowserDataSource;

    .line 400
    iput-boolean v1, p0, Lcom/twitter/library/client/k;->a:Z

    .line 401
    const-string/jumbo v0, "CustomTabs"

    const-string/jumbo v1, "CustomTabsManager cleared"

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;Ljava/lang/String;Landroid/support/customtabs/CustomTabsIntent;Lcom/twitter/library/client/BrowserDataSource;)V
    .locals 1

    .prologue
    .line 218
    iput-object p4, p0, Lcom/twitter/library/client/k;->h:Lcom/twitter/library/client/BrowserDataSource;

    .line 219
    invoke-virtual {p0, p2}, Lcom/twitter/library/client/k;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/client/k;->a:Z

    .line 220
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p3, p1, v0}, Landroid/support/customtabs/CustomTabsIntent;->launchUrl(Landroid/app/Activity;Landroid/net/Uri;)V

    .line 221
    iget-boolean v0, p0, Lcom/twitter/library/client/k;->a:Z

    if-eqz v0, :cond_0

    .line 222
    invoke-virtual {p0}, Lcom/twitter/library/client/k;->e()V

    .line 224
    :cond_0
    return-void
.end method

.method protected declared-synchronized a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 279
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/k;->b:Lcom/twitter/library/client/k$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/client/k;->b:Lcom/twitter/library/client/k$b;

    invoke-virtual {v0, p1}, Lcom/twitter/library/client/k$b;->a(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 154
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/library/client/k;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/twitter/library/client/k;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 181
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 160
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/twitter/library/client/k;->b:Lcom/twitter/library/client/k$b;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/library/client/k;->b:Lcom/twitter/library/client/k$b;

    iget-object v2, p0, Lcom/twitter/library/client/k;->e:Lcqt;

    invoke-interface {v2}, Lcqt;->b()J

    move-result-wide v2

    invoke-virtual {v1, p1, v2, v3}, Lcom/twitter/library/client/k$b;->a(Ljava/lang/String;J)Z

    move-result v1

    if-nez v1, :cond_2

    .line 161
    const/4 v0, 0x1

    goto :goto_0

    .line 163
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/client/k;->c(Ljava/lang/String;Landroid/content/Context;)Landroid/support/customtabs/CustomTabsSession;

    move-result-object v1

    .line 165
    if-eqz v1, :cond_0

    .line 166
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/support/customtabs/CustomTabsSession;->mayLaunchUrl(Landroid/net/Uri;Landroid/os/Bundle;Ljava/util/List;)Z

    move-result v0

    .line 167
    if-nez v0, :cond_3

    .line 168
    const-string/jumbo v1, "CustomTabs"

    const-string/jumbo v2, "URL warm up failed despite the existence of a CustomTabs session"

    invoke-static {v1, v2}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 154
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 170
    :cond_3
    :try_start_2
    new-instance v2, Lcom/twitter/library/client/k$b;

    iget-object v3, p0, Lcom/twitter/library/client/k;->e:Lcqt;

    invoke-interface {v3}, Lcqt;->b()J

    move-result-wide v4

    invoke-direct {v2, v1, p1, v4, v5}, Lcom/twitter/library/client/k$b;-><init>(Landroid/support/customtabs/CustomTabsSession;Ljava/lang/String;J)V

    iput-object v2, p0, Lcom/twitter/library/client/k;->b:Lcom/twitter/library/client/k$b;

    .line 171
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 172
    const-string/jumbo v2, "url"

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    const-string/jumbo v2, "is_wifi"

    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v3

    invoke-virtual {v3}, Lcrr;->c()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    new-instance v2, Lcom/twitter/library/client/ChromeScribeEvent;

    const-string/jumbo v3, "chrome::::warm_url"

    invoke-direct {v2, v3, v1, p2}, Lcom/twitter/library/client/ChromeScribeEvent;-><init>(Ljava/lang/String;Ljava/util/Map;Landroid/content/Context;)V

    invoke-virtual {p0, v2}, Lcom/twitter/library/client/k;->onEvent(Lcom/twitter/library/client/ChromeScribeEvent;)V

    .line 178
    const-string/jumbo v1, "CustomTabs"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "URL warmed up: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public declared-synchronized b(Ljava/lang/String;Landroid/content/Context;)Landroid/support/customtabs/CustomTabsIntent$Builder;
    .locals 2

    .prologue
    .line 191
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/twitter/library/client/k;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 192
    sget-boolean v0, Lcom/twitter/library/client/k;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/client/k;->b:Lcom/twitter/library/client/k$b;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 193
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/twitter/library/client/k;->b:Lcom/twitter/library/client/k$b;

    invoke-virtual {v0}, Lcom/twitter/library/client/k$b;->a()Landroid/support/customtabs/CustomTabsSession;

    move-result-object v0

    .line 198
    :goto_0
    new-instance v1, Landroid/support/customtabs/CustomTabsIntent$Builder;

    invoke-direct {v1, v0}, Landroid/support/customtabs/CustomTabsIntent$Builder;-><init>(Landroid/support/customtabs/CustomTabsSession;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v1

    .line 195
    :cond_1
    :try_start_2
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/client/k;->c(Ljava/lang/String;Landroid/content/Context;)Landroid/support/customtabs/CustomTabsSession;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto :goto_0
.end method

.method protected declared-synchronized c(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 293
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/twitter/library/util/f;->a()Lcom/twitter/library/util/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/library/util/f;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 294
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/client/k;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 295
    const-string/jumbo v1, "CustomTabs"

    const-string/jumbo v2, "Chrome connection not established. Will create..."

    invoke-static {v1, v2}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    iget-object v1, p0, Lcom/twitter/library/client/k;->e:Lcqt;

    invoke-interface {v1}, Lcqt;->b()J

    move-result-wide v2

    .line 297
    invoke-direct {p0}, Lcom/twitter/library/client/k;->i()V

    .line 298
    new-instance v1, Lcom/twitter/library/client/k$1;

    invoke-direct {v1, p0, v2, v3}, Lcom/twitter/library/client/k$1;-><init>(Lcom/twitter/library/client/k;J)V

    iput-object v1, p0, Lcom/twitter/library/client/k;->g:Landroid/support/customtabs/CustomTabsServiceConnection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 334
    :try_start_1
    iget-object v1, p0, Lcom/twitter/library/client/k;->g:Landroid/support/customtabs/CustomTabsServiceConnection;

    invoke-static {p1, v0, v1}, Landroid/support/customtabs/CustomTabsClient;->bindCustomTabsService(Landroid/content/Context;Ljava/lang/String;Landroid/support/customtabs/CustomTabsServiceConnection;)Z

    move-result v0

    .line 335
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/twitter/library/client/k;->i:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 341
    :goto_0
    if-nez v0, :cond_0

    .line 342
    :try_start_2
    const-string/jumbo v0, "CustomTabs"

    const-string/jumbo v1, "Service binding failed"

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    invoke-direct {p0}, Lcom/twitter/library/client/k;->i()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 348
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 336
    :catch_0
    move-exception v0

    .line 337
    :try_start_3
    const-string/jumbo v1, "CustomTabs"

    const-string/jumbo v2, "Binding to Chrome service caused exception"

    invoke-static {v1, v2, v0}, Lcqj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 338
    const/4 v0, 0x0

    goto :goto_0

    .line 346
    :cond_1
    const-string/jumbo v0, "CustomTabs"

    const-string/jumbo v1, "Connection abandoned. Already connected."

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 293
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/twitter/library/client/k;->g:Landroid/support/customtabs/CustomTabsServiceConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/client/k;->f:Landroid/support/customtabs/CustomTabsClient;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 272
    iget-boolean v0, p0, Lcom/twitter/library/client/k;->a:Z

    return v0
.end method

.method protected declared-synchronized e()V
    .locals 1

    .prologue
    .line 286
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/twitter/library/client/k;->b:Lcom/twitter/library/client/k$b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 287
    monitor-exit p0

    return-void

    .line 286
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected f()Z
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/twitter/library/client/k;->g:Landroid/support/customtabs/CustomTabsServiceConnection;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/client/k;->i:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/client/k;->f:Landroid/support/customtabs/CustomTabsClient;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected g()Lbss;
    .locals 1

    .prologue
    .line 362
    new-instance v0, Lbss;

    invoke-direct {v0}, Lbss;-><init>()V

    return-object v0
.end method

.method public onEvent(Lcom/twitter/library/client/ChromeScribeEvent;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 231
    iget-object v2, p1, Lcom/twitter/library/client/ChromeScribeEvent;->b:Ljava/util/Map;

    .line 232
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 233
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/twitter/library/client/ChromeScribeEvent;->a:Ljava/lang/String;

    aput-object v4, v1, v3

    .line 234
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 236
    iget-object v1, p0, Lcom/twitter/library/client/k;->h:Lcom/twitter/library/client/BrowserDataSource;

    if-eqz v1, :cond_0

    .line 237
    const-string/jumbo v1, "is_promoted"

    iget-object v3, p0, Lcom/twitter/library/client/k;->h:Lcom/twitter/library/client/BrowserDataSource;

    invoke-interface {v3}, Lcom/twitter/library/client/BrowserDataSource;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    iget-object v1, p1, Lcom/twitter/library/client/ChromeScribeEvent;->c:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/library/client/k;->h:Lcom/twitter/library/client/BrowserDataSource;

    .line 239
    invoke-interface {v3}, Lcom/twitter/library/client/BrowserDataSource;->d()Lcom/twitter/library/scribe/ScribeItemsProvider;

    move-result-object v3

    .line 238
    invoke-static {v0, v1, v3, v6, v6}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/library/scribe/ScribeItemsProvider;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)V

    .line 242
    iget-object v1, p0, Lcom/twitter/library/client/k;->h:Lcom/twitter/library/client/BrowserDataSource;

    invoke-interface {v1}, Lcom/twitter/library/client/BrowserDataSource;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/client/k;->h:Lcom/twitter/library/client/BrowserDataSource;

    invoke-interface {v1}, Lcom/twitter/library/client/BrowserDataSource;->b()Lcgi;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "dwell_time"

    .line 243
    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 244
    const-string/jumbo v1, "dwell_time"

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 245
    invoke-virtual {p0}, Lcom/twitter/library/client/k;->g()Lbss;

    move-result-object v3

    .line 246
    sget-object v1, Lcom/twitter/library/client/BrowserTimingHelper$Dwell;->a:Lcom/twitter/library/client/BrowserTimingHelper$Dwell;

    .line 248
    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/client/BrowserTimingHelper$Dwell;->d()J

    move-result-wide v6

    cmp-long v6, v6, v4

    if-gtz v6, :cond_0

    .line 249
    invoke-virtual {v1}, Lcom/twitter/library/client/BrowserTimingHelper$Dwell;->a()Lcom/twitter/library/api/PromotedEvent;

    move-result-object v6

    iget-object v7, p0, Lcom/twitter/library/client/k;->h:Lcom/twitter/library/client/BrowserDataSource;

    .line 250
    invoke-interface {v7}, Lcom/twitter/library/client/BrowserDataSource;->b()Lcgi;

    move-result-object v7

    .line 249
    invoke-static {v6, v7}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v6

    .line 250
    invoke-virtual {v6}, Lbsq$a;->a()Lbsq;

    move-result-object v6

    .line 249
    invoke-virtual {v3, v6}, Lbss;->a(Lbsq;)V

    .line 251
    invoke-virtual {v1}, Lcom/twitter/library/client/BrowserTimingHelper$Dwell;->b()Lcom/twitter/library/client/BrowserTimingHelper$Dwell;

    move-result-object v1

    goto :goto_0

    .line 256
    :cond_0
    invoke-static {v2}, Lmg;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 257
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 258
    return-void
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 53
    check-cast p1, Lcom/twitter/library/client/ChromeScribeEvent;

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/k;->onEvent(Lcom/twitter/library/client/ChromeScribeEvent;)V

    return-void
.end method
