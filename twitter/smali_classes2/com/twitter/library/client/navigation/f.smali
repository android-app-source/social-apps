.class public Lcom/twitter/library/client/navigation/f;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcmz;
.implements Lcna;


# instance fields
.field private final a:Lcmz$c;

.field private final b:Lcmz$a;

.field private c:Lcna;

.field private d:Z


# direct methods
.method constructor <init>(Lcmz$a;Lcmz$c;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p2, p0, Lcom/twitter/library/client/navigation/f;->a:Lcmz$c;

    .line 40
    iget-object v0, p0, Lcom/twitter/library/client/navigation/f;->a:Lcmz$c;

    invoke-interface {v0, p0}, Lcmz$c;->a(Lcna;)V

    .line 42
    iput-object p1, p0, Lcom/twitter/library/client/navigation/f;->b:Lcmz$a;

    .line 43
    iget-object v0, p0, Lcom/twitter/library/client/navigation/f;->b:Lcmz$a;

    invoke-interface {v0, p0}, Lcmz$a;->a(Lcna;)V

    .line 44
    return-void
.end method

.method public static a(Landroid/content/Context;Lakn;Lcmz$d;Lcmz$b;Lcmy;)Lcom/twitter/library/client/navigation/f;
    .locals 3

    .prologue
    .line 30
    new-instance v0, Lcom/twitter/library/client/navigation/b;

    invoke-direct {v0, p3}, Lcom/twitter/library/client/navigation/b;-><init>(Lcmz$b;)V

    .line 31
    new-instance v1, Lcom/twitter/library/client/navigation/d;

    invoke-direct {v1, p0, p1, p2, p4}, Lcom/twitter/library/client/navigation/d;-><init>(Landroid/content/Context;Lakn;Lcmz$d;Lcmy;)V

    .line 33
    new-instance v2, Lcom/twitter/library/client/navigation/f;

    invoke-direct {v2, v0, v1}, Lcom/twitter/library/client/navigation/f;-><init>(Lcmz$a;Lcmz$c;)V

    return-object v2
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/library/client/navigation/f;->b:Lcmz$a;

    invoke-interface {v0}, Lcmz$a;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcmx;)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/twitter/library/client/navigation/f;->c:Lcna;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/twitter/library/client/navigation/f;->c:Lcna;

    invoke-interface {v0, p1}, Lcna;->a(Lcmx;)V

    .line 68
    :cond_0
    return-void
.end method

.method public a(Lcna;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/twitter/library/client/navigation/f;->c:Lcna;

    .line 61
    return-void
.end method

.method public a(Lcom/twitter/model/account/UserAccount;)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/library/client/navigation/f;->c:Lcna;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/twitter/library/client/navigation/f;->c:Lcna;

    invoke-interface {v0, p1}, Lcna;->a(Lcom/twitter/model/account/UserAccount;)V

    .line 75
    :cond_0
    return-void
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/library/client/navigation/f;->a:Lcmz$c;

    invoke-interface {v0}, Lcmz$c;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 79
    iget-object v0, p0, Lcom/twitter/library/client/navigation/f;->b:Lcmz$a;

    invoke-interface {v0}, Lcmz$a;->a()Landroid/view/View;

    move-result-object v0

    sget v1, Lazw$g;->other_accounts:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 80
    invoke-static {v0}, Landroid/support/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;)V

    .line 82
    iget-boolean v1, p0, Lcom/twitter/library/client/navigation/f;->d:Z

    if-nez v1, :cond_1

    .line 83
    iget-object v1, p0, Lcom/twitter/library/client/navigation/f;->a:Lcmz$c;

    invoke-interface {v1}, Lcmz$c;->c()V

    .line 84
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/client/navigation/f;->d:Z

    .line 92
    :goto_0
    iget-object v0, p0, Lcom/twitter/library/client/navigation/f;->c:Lcna;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/twitter/library/client/navigation/f;->c:Lcna;

    invoke-interface {v0}, Lcna;->c()Z

    .line 96
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/library/client/navigation/f;->d:Z

    return v0

    .line 87
    :cond_1
    iget-object v1, p0, Lcom/twitter/library/client/navigation/f;->a:Lcmz$c;

    invoke-interface {v1}, Lcmz$c;->b()V

    .line 88
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 89
    iput-boolean v2, p0, Lcom/twitter/library/client/navigation/f;->d:Z

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/twitter/library/client/navigation/f;->c:Lcna;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/twitter/library/client/navigation/f;->c:Lcna;

    invoke-interface {v0}, Lcna;->d()V

    .line 104
    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/twitter/library/client/navigation/f;->a:Lcmz$c;

    invoke-interface {v0}, Lcmz$c;->b()V

    .line 109
    return-void
.end method
