.class Lcom/twitter/library/client/navigation/g$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/client/navigation/g;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/twitter/model/core/TwitterUser;

.field final synthetic c:Lcom/twitter/model/account/UserSettings;

.field final synthetic d:Lcom/twitter/library/client/navigation/g;


# direct methods
.method constructor <init>(Lcom/twitter/library/client/navigation/g;Landroid/view/View;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/twitter/library/client/navigation/g$1;->d:Lcom/twitter/library/client/navigation/g;

    iput-object p2, p0, Lcom/twitter/library/client/navigation/g$1;->a:Landroid/view/View;

    iput-object p3, p0, Lcom/twitter/library/client/navigation/g$1;->b:Lcom/twitter/model/core/TwitterUser;

    iput-object p4, p0, Lcom/twitter/library/client/navigation/g$1;->c:Lcom/twitter/model/account/UserSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 225
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g$1;->a:Landroid/view/View;

    sget v1, Lazw$g;->name:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 226
    iget-object v1, p0, Lcom/twitter/library/client/navigation/g$1;->b:Lcom/twitter/model/core/TwitterUser;

    iget-object v1, v1, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 227
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g$1;->a:Landroid/view/View;

    sget v1, Lazw$g;->username:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 228
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/client/navigation/g$1;->b:Lcom/twitter/model/core/TwitterUser;

    iget-object v2, v2, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g$1;->c:Lcom/twitter/model/account/UserSettings;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/client/navigation/g$1;->c:Lcom/twitter/model/account/UserSettings;

    iget-boolean v4, v0, Lcom/twitter/model/account/UserSettings;->j:Z

    .line 235
    :goto_0
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g$1;->a:Landroid/view/View;

    sget v1, Lazw$g;->badge_container:I

    .line 236
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 237
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g$1;->d:Lcom/twitter/library/client/navigation/g;

    iget-object v0, v0, Lcom/twitter/library/client/navigation/g;->b:Landroid/app/Activity;

    iget-object v2, p0, Lcom/twitter/library/client/navigation/g$1;->b:Lcom/twitter/model/core/TwitterUser;

    iget-boolean v2, v2, Lcom/twitter/model/core/TwitterUser;->m:Z

    iget-object v3, p0, Lcom/twitter/library/client/navigation/g$1;->b:Lcom/twitter/model/core/TwitterUser;

    iget-object v3, v3, Lcom/twitter/model/core/TwitterUser;->P:Lcom/twitter/model/profile/TranslatorType;

    sget v5, Lazw$d;->white:I

    sget v6, Lazw$d;->white:I

    sget v7, Lazw$d;->white:I

    sget v8, Lazw$d;->white:I

    invoke-static/range {v0 .. v8}, Lcom/twitter/library/util/w;->a(Landroid/content/Context;Landroid/view/ViewGroup;ZLcom/twitter/model/profile/TranslatorType;ZIIII)V

    .line 239
    return-void

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g$1;->b:Lcom/twitter/model/core/TwitterUser;

    iget-boolean v4, v0, Lcom/twitter/model/core/TwitterUser;->l:Z

    goto :goto_0
.end method
