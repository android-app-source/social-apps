.class public Lcom/twitter/library/client/navigation/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcmz$c;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lakn;

.field private final c:Lcmz$d;

.field private final d:Lcmy;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lakn;Lcmz$d;Lcmy;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/twitter/library/client/navigation/d;->a:Landroid/content/Context;

    .line 34
    iput-object p2, p0, Lcom/twitter/library/client/navigation/d;->b:Lakn;

    .line 35
    iput-object p3, p0, Lcom/twitter/library/client/navigation/d;->c:Lcmz$d;

    .line 36
    iput-object p4, p0, Lcom/twitter/library/client/navigation/d;->d:Lcmy;

    .line 37
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/library/client/navigation/d;->c:Lcmz$d;

    invoke-interface {v0}, Lcmz$d;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcna;)V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/twitter/library/client/navigation/d;->c:Lcmz$d;

    invoke-interface {v0, p1}, Lcmz$d;->a(Lcna;)V

    .line 111
    return-void
.end method

.method public b()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 47
    new-instance v5, Lcbl$a;

    invoke-direct {v5}, Lcbl$a;-><init>()V

    .line 50
    iget-object v0, p0, Lcom/twitter/library/client/navigation/d;->d:Lcmy;

    invoke-virtual {v0}, Lcmy;->b()Landroid/support/v7/util/SortedList;

    move-result-object v6

    .line 51
    invoke-virtual {v6}, Landroid/support/v7/util/SortedList;->size()I

    move-result v7

    move v4, v1

    move v2, v1

    move v3, v1

    .line 54
    :goto_0
    if-ge v4, v7, :cond_4

    .line 55
    invoke-virtual {v6, v4}, Landroid/support/v7/util/SortedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmx;

    .line 56
    invoke-virtual {v0}, Lcmx;->b()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 57
    if-lez v4, :cond_0

    invoke-virtual {v0}, Lcmx;->d()I

    move-result v8

    if-eq v3, v8, :cond_0

    if-nez v2, :cond_0

    .line 59
    new-instance v2, Lcmy$e;

    invoke-direct {v2}, Lcmy$e;-><init>()V

    invoke-virtual {v5, v2}, Lcbl$a;->a(Ljava/lang/Object;)Lcbl$a;

    .line 61
    :cond_0
    invoke-virtual {v0}, Lcmx;->d()I

    move-result v2

    .line 62
    new-instance v3, Lcmy$h;

    invoke-direct {v3, v0}, Lcmy$h;-><init>(Lcmx;)V

    invoke-virtual {v5, v3}, Lcbl$a;->a(Ljava/lang/Object;)Lcbl$a;

    .line 65
    invoke-virtual {v0}, Lcmx;->i()Ljava/util/List;

    move-result-object v3

    .line 66
    if-eqz v3, :cond_3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_3

    invoke-virtual {v0}, Lcmx;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 67
    new-instance v0, Lcmy$g;

    invoke-direct {v0}, Lcmy$g;-><init>()V

    invoke-virtual {v5, v0}, Lcbl$a;->a(Ljava/lang/Object;)Lcbl$a;

    .line 68
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmm;

    .line 69
    instance-of v8, v0, Lcmx;

    if-eqz v8, :cond_1

    .line 70
    new-instance v8, Lcmy$d;

    .line 71
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmx;

    invoke-direct {v8, v0}, Lcmy$d;-><init>(Lcmx;)V

    .line 70
    invoke-virtual {v5, v8}, Lcbl$a;->a(Ljava/lang/Object;)Lcbl$a;

    goto :goto_1

    .line 74
    :cond_2
    new-instance v0, Lcmy$f;

    invoke-direct {v0}, Lcmy$f;-><init>()V

    invoke-virtual {v5, v0}, Lcbl$a;->a(Ljava/lang/Object;)Lcbl$a;

    .line 75
    const/4 v0, 0x1

    .line 54
    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    move v2, v0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 77
    goto :goto_2

    .line 81
    :cond_4
    iget-object v0, p0, Lcom/twitter/library/client/navigation/d;->c:Lcmz$d;

    invoke-virtual {v5}, Lcbl$a;->a()Lcbl;

    move-result-object v1

    invoke-interface {v0, v1}, Lcmz$d;->a(Lcbl;)V

    .line 82
    return-void

    :cond_5
    move v0, v2

    move v2, v3

    goto :goto_2
.end method

.method public c()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 86
    iget-object v0, p0, Lcom/twitter/library/client/navigation/d;->b:Lakn;

    invoke-virtual {v0}, Lakn;->c()Ljava/util/List;

    move-result-object v0

    .line 87
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    .line 86
    invoke-static {v0, v1}, Lcom/twitter/library/util/b;->a(Ljava/util/List;Lcom/twitter/library/client/v;)Ljava/util/List;

    move-result-object v1

    .line 90
    new-instance v2, Lcbl$a;

    invoke-direct {v2}, Lcbl$a;-><init>()V

    .line 91
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/UserAccount;

    .line 92
    new-instance v4, Lcmy$a;

    invoke-direct {v4, v0}, Lcmy$a;-><init>(Lcom/twitter/model/account/UserAccount;)V

    invoke-virtual {v2, v4}, Lcbl$a;->a(Ljava/lang/Object;)Lcbl$a;

    goto :goto_0

    .line 96
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 97
    new-instance v0, Lcmy$e;

    invoke-direct {v0}, Lcmy$e;-><init>()V

    invoke-virtual {v2, v0}, Lcbl$a;->a(Ljava/lang/Object;)Lcbl$a;

    .line 100
    :cond_1
    new-instance v0, Lcmy$h;

    new-instance v1, Lcmx;

    iget-object v3, p0, Lcom/twitter/library/client/navigation/d;->a:Landroid/content/Context;

    sget v4, Lazw$g;->new_account:I

    invoke-direct {v1, v3, v4, v5, v5}, Lcmx;-><init>(Landroid/content/Context;III)V

    sget v3, Lazw$k;->accounts_dialog_new_account:I

    .line 101
    invoke-virtual {v1, v3}, Lcmx;->a(I)Lcmx;

    move-result-object v1

    invoke-direct {v0, v1}, Lcmy$h;-><init>(Lcmx;)V

    .line 100
    invoke-virtual {v2, v0}, Lcbl$a;->a(Ljava/lang/Object;)Lcbl$a;

    .line 102
    new-instance v0, Lcmy$h;

    new-instance v1, Lcmx;

    iget-object v3, p0, Lcom/twitter/library/client/navigation/d;->a:Landroid/content/Context;

    sget v4, Lazw$g;->add_account:I

    invoke-direct {v1, v3, v4, v5, v5}, Lcmx;-><init>(Landroid/content/Context;III)V

    sget v3, Lazw$k;->accounts_dialog_add_account:I

    .line 103
    invoke-virtual {v1, v3}, Lcmx;->a(I)Lcmx;

    move-result-object v1

    invoke-direct {v0, v1}, Lcmy$h;-><init>(Lcmx;)V

    .line 102
    invoke-virtual {v2, v0}, Lcbl$a;->a(Ljava/lang/Object;)Lcbl$a;

    .line 105
    iget-object v0, p0, Lcom/twitter/library/client/navigation/d;->c:Lcmz$d;

    invoke-virtual {v2}, Lcbl$a;->a()Lcbl;

    move-result-object v1

    invoke-interface {v0, v1}, Lcmz$d;->a(Lcbl;)V

    .line 106
    return-void
.end method
