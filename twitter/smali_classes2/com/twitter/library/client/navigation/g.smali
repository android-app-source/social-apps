.class public Lcom/twitter/library/client/navigation/g;
.super Lcom/twitter/library/client/navigation/h;
.source "Twttr"

# interfaces
.implements Landroid/support/design/widget/NavigationView$OnNavigationItemSelectedListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcna;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/client/navigation/g$a;,
        Lcom/twitter/library/client/navigation/g$b;,
        Lcom/twitter/library/client/navigation/g$c;
    }
.end annotation


# instance fields
.field private final e:Lcom/twitter/library/client/navigation/ModernDrawerView;

.field private final f:Landroid/support/v4/widget/DrawerLayout;

.field private final g:Ljava/util/AbstractMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/AbstractMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/twitter/library/client/navigation/g$c;",
            ">;"
        }
    .end annotation
.end field

.field private h:Landroid/widget/BaseAdapter;

.field private i:Lcmr$a;


# direct methods
.method public constructor <init>(Lcom/twitter/library/client/navigation/ModernDrawerView;Lcom/twitter/internal/android/widget/ToolBar;ILandroid/app/Activity;)V
    .locals 2

    .prologue
    .line 79
    invoke-direct {p0, p2, p3, p4}, Lcom/twitter/library/client/navigation/h;-><init>(Lcom/twitter/internal/android/widget/ToolBar;ILandroid/app/Activity;)V

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/client/navigation/g;->g:Ljava/util/AbstractMap;

    .line 80
    iput-object p1, p0, Lcom/twitter/library/client/navigation/g;->e:Lcom/twitter/library/client/navigation/ModernDrawerView;

    .line 81
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->e:Lcom/twitter/library/client/navigation/ModernDrawerView;

    invoke-virtual {v0}, Lcom/twitter/library/client/navigation/ModernDrawerView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    iput-object v0, p0, Lcom/twitter/library/client/navigation/g;->f:Landroid/support/v4/widget/DrawerLayout;

    .line 82
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->e:Lcom/twitter/library/client/navigation/ModernDrawerView;

    invoke-virtual {v0, p0}, Lcom/twitter/library/client/navigation/ModernDrawerView;->setOnDrawerClickListener(Lcna;)V

    .line 83
    new-instance v0, Lcom/twitter/library/client/navigation/g$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/client/navigation/g$a;-><init>(Lcom/twitter/library/client/navigation/g;Lcom/twitter/library/client/navigation/g$1;)V

    invoke-virtual {p0, v0}, Lcom/twitter/library/client/navigation/g;->a(Landroid/support/v4/widget/DrawerLayout$SimpleDrawerListener;)V

    .line 84
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->a:Lcom/twitter/internal/android/widget/ToolBar;

    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayOptions(I)V

    .line 87
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/client/navigation/g;)Landroid/widget/BaseAdapter;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->h:Landroid/widget/BaseAdapter;

    return-object v0
.end method

.method private a(Lcmm;)Z
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->i:Lcmr$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->i:Lcmr$a;

    invoke-interface {v0, p1}, Lcmr$a;->a(Lcmm;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/library/client/navigation/g;)Lcom/twitter/library/client/navigation/ModernDrawerView;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->e:Lcom/twitter/library/client/navigation/ModernDrawerView;

    return-object v0
.end method

.method private d(I)Lcom/twitter/library/client/m;
    .locals 4

    .prologue
    .line 288
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->h:Landroid/widget/BaseAdapter;

    if-eqz v0, :cond_1

    .line 289
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->h:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v3

    .line 290
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    .line 291
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->h:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 292
    instance-of v0, v1, Lcom/twitter/library/client/m;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lcom/twitter/library/client/m;

    invoke-virtual {v0}, Lcom/twitter/library/client/m;->c()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 293
    check-cast v1, Lcom/twitter/library/client/m;

    .line 297
    :goto_1
    return-object v1

    .line 290
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 297
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a(I)V
    .locals 6
    .param p1    # I
        .annotation build Landroid/support/annotation/MenuRes;
        .end annotation
    .end param

    .prologue
    .line 91
    new-instance v2, Lcom/twitter/library/client/navigation/g$b;

    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->b:Landroid/app/Activity;

    invoke-direct {v2, p0, v0, p1}, Lcom/twitter/library/client/navigation/g$b;-><init>(Lcom/twitter/library/client/navigation/g;Landroid/content/Context;I)V

    .line 92
    invoke-virtual {v2}, Lcom/twitter/library/client/navigation/g$b;->a()V

    .line 95
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->a:Lcom/twitter/internal/android/widget/ToolBar;

    iget-object v1, v2, Lcom/twitter/library/client/navigation/g$b;->d:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(Ljava/util/Collection;)V

    .line 98
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->h:Landroid/widget/BaseAdapter;

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, v2, Lcom/twitter/library/client/navigation/g$b;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 100
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/twitter/library/client/navigation/g;->d(I)Lcom/twitter/library/client/m;

    move-result-object v4

    .line 103
    iget-object v5, p0, Lcom/twitter/library/client/navigation/g;->g:Ljava/util/AbstractMap;

    if-eqz v4, :cond_0

    new-instance v1, Lcom/twitter/library/client/navigation/g$c;

    invoke-direct {v1, p0, v4}, Lcom/twitter/library/client/navigation/g$c;-><init>(Lcom/twitter/library/client/navigation/g;Lcom/twitter/library/client/m;)V

    :goto_1
    invoke-virtual {v5, v0, v1}, Ljava/util/AbstractMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->e:Lcom/twitter/library/client/navigation/ModernDrawerView;

    iget-object v1, v2, Lcom/twitter/library/client/navigation/g$b;->e:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/navigation/ModernDrawerView;->a(Ljava/util/List;)V

    .line 108
    return-void
.end method

.method public a(Landroid/support/v4/widget/DrawerLayout$SimpleDrawerListener;)V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->f:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/DrawerLayout;->addDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    .line 124
    return-void
.end method

.method public a(Landroid/widget/BaseAdapter;)V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->h:Landroid/widget/BaseAdapter;

    if-eq v0, p1, :cond_0

    .line 118
    iput-object p1, p0, Lcom/twitter/library/client/navigation/g;->h:Landroid/widget/BaseAdapter;

    .line 120
    :cond_0
    return-void
.end method

.method public a(Lcmr$a;)V
    .locals 0

    .prologue
    .line 112
    invoke-super {p0, p1}, Lcom/twitter/library/client/navigation/h;->a(Lcmr$a;)V

    .line 113
    iput-object p1, p0, Lcom/twitter/library/client/navigation/g;->i:Lcmr$a;

    .line 114
    return-void
.end method

.method public a(Lcmx;)V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0, p1}, Lcom/twitter/library/client/navigation/g;->a(Lcmm;)Z

    .line 154
    return-void
.end method

.method public a(Lcom/twitter/media/request/ImageResponse;)V
    .locals 2

    .prologue
    .line 255
    iget-object v1, p0, Lcom/twitter/library/client/navigation/g;->a:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setDrawerIcon(Landroid/graphics/Bitmap;)V

    .line 256
    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/media/request/ResourceResponse;)V
    .locals 0

    .prologue
    .line 62
    check-cast p1, Lcom/twitter/media/request/ImageResponse;

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/navigation/g;->a(Lcom/twitter/media/request/ImageResponse;)V

    return-void
.end method

.method public a(Lcom/twitter/model/account/UserAccount;)V
    .locals 2

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/twitter/library/client/navigation/g;->e()Z

    .line 159
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->i:Lcmr$a;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->i:Lcmr$a;

    iget-object v1, p1, Lcom/twitter/model/account/UserAccount;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcmr$a;->a_(Ljava/lang/String;)V

    .line 162
    :cond_0
    return-void
.end method

.method a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 3

    .prologue
    .line 249
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/twitter/library/media/manager/g;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    move-result-object v1

    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    const/4 v2, -0x1

    .line 250
    invoke-static {v0, v2}, Lcom/twitter/media/manager/UserImageRequest;->a(Ljava/lang/String;I)Lcom/twitter/media/request/a$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/media/request/b$b;)Lcom/twitter/media/request/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/a$a;

    .line 249
    invoke-virtual {v1, v0}, Lcom/twitter/library/media/manager/g;->a(Lcom/twitter/media/request/a$a;)Lcom/twitter/util/concurrent/g;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/navigation/g;->c:Ljava/util/concurrent/Future;

    .line 251
    return-void
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 188
    if-eqz p1, :cond_5

    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->d:Lcom/twitter/model/core/TwitterUser;

    invoke-static {p1, v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 190
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->g:Ljava/util/AbstractMap;

    invoke-virtual {v0}, Ljava/util/AbstractMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 191
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 192
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 193
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/navigation/g$c;

    .line 194
    invoke-direct {p0, v1}, Lcom/twitter/library/client/navigation/g;->d(I)Lcom/twitter/library/client/m;

    move-result-object v4

    .line 195
    if-nez v4, :cond_1

    if-nez v0, :cond_2

    :cond_1
    if-nez v0, :cond_0

    if-eqz v4, :cond_0

    .line 196
    :cond_2
    iget-object v5, p0, Lcom/twitter/library/client/navigation/g;->g:Ljava/util/AbstractMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v4, :cond_3

    new-instance v0, Lcom/twitter/library/client/navigation/g$c;

    invoke-direct {v0, p0, v4}, Lcom/twitter/library/client/navigation/g$c;-><init>(Lcom/twitter/library/client/navigation/g;Lcom/twitter/library/client/m;)V

    :goto_1
    invoke-virtual {v5, v1, v0}, Ljava/util/AbstractMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    move-object v0, v2

    goto :goto_1

    .line 200
    :cond_4
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->e:Lcom/twitter/library/client/navigation/ModernDrawerView;

    invoke-virtual {v0}, Lcom/twitter/library/client/navigation/ModernDrawerView;->getHeaderView()Landroid/view/View;

    move-result-object v3

    .line 201
    if-eqz v3, :cond_5

    .line 202
    sget v0, Lazw$g;->my_profile:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    .line 203
    invoke-virtual {v0, p1}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;)Z

    .line 204
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v0

    invoke-virtual {v0}, Lakn;->b()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    .line 205
    invoke-virtual {p1}, Lcom/twitter/model/core/TwitterUser;->g()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 206
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->a:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->setDrawerIcon(Landroid/graphics/Bitmap;)V

    .line 211
    :goto_2
    sget v0, Lazw$g;->banner_image:I

    .line 212
    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/BackgroundImageView;

    .line 213
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    iget v1, p1, Lcom/twitter/model/core/TwitterUser;->i:I

    if-eqz v1, :cond_7

    iget v1, p1, Lcom/twitter/model/core/TwitterUser;->i:I

    .line 214
    :goto_3
    invoke-direct {v2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 215
    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/image/BackgroundImageView;->setDefaultDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 216
    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->F:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    sget-object v2, Lcom/twitter/media/util/HeaderImageVariant;->j:Lcom/twitter/media/request/a$c;

    .line 217
    invoke-virtual {v1, v2}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/media/request/a$c;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    .line 216
    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/BackgroundImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 222
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->b:Landroid/app/Activity;

    new-instance v1, Lcom/twitter/library/client/navigation/g$1;

    invoke-direct {v1, p0, v3, p1, p2}, Lcom/twitter/library/client/navigation/g$1;-><init>(Lcom/twitter/library/client/navigation/g;Landroid/view/View;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 244
    :cond_5
    invoke-super {p0, p1, p2}, Lcom/twitter/library/client/navigation/h;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V

    .line 245
    return-void

    .line 208
    :cond_6
    invoke-virtual {p0, p1}, Lcom/twitter/library/client/navigation/g;->a(Lcom/twitter/model/core/TwitterUser;)V

    goto :goto_2

    .line 213
    :cond_7
    iget-object v1, p0, Lcom/twitter/library/client/navigation/g;->b:Landroid/app/Activity;

    sget v4, Lazw$d;->twitter_blue:I

    .line 214
    invoke-static {v1, v4}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    goto :goto_3
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->f:Landroid/support/v4/widget/DrawerLayout;

    const v1, 0x800003

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(I)Z

    move-result v0

    return v0
.end method

.method public b(I)Lcmm;
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->e:Lcom/twitter/library/client/navigation/ModernDrawerView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/client/navigation/ModernDrawerView;->a(I)Lcmx;

    move-result-object v0

    .line 262
    if-eqz v0, :cond_1

    .line 271
    :cond_0
    :goto_0
    return-object v0

    .line 266
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->g:Ljava/util/AbstractMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/AbstractMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/navigation/g$c;

    .line 267
    if-nez v0, :cond_0

    .line 271
    invoke-super {p0, p1}, Lcom/twitter/library/client/navigation/h;->b(I)Lcmm;

    move-result-object v0

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->f:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/twitter/library/client/navigation/g;->e:Lcom/twitter/library/client/navigation/ModernDrawerView;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->openDrawer(Landroid/view/View;)V

    .line 134
    const/4 v0, 0x1

    return v0
.end method

.method public c()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 166
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->i:Lcmr$a;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->i:Lcmr$a;

    new-instance v1, Lcmx;

    iget-object v2, p0, Lcom/twitter/library/client/navigation/g;->b:Landroid/app/Activity;

    sget v3, Lazw$g;->accounts:I

    invoke-direct {v1, v2, v3, v4, v4}, Lcmx;-><init>(Landroid/content/Context;III)V

    invoke-interface {v0, v1}, Lcmr$a;->a(Lcmm;)Z

    .line 169
    :cond_0
    return v4
.end method

.method public d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 174
    new-instance v0, Lcmx;

    iget-object v1, p0, Lcom/twitter/library/client/navigation/g;->b:Landroid/app/Activity;

    sget v2, Lazw$g;->my_profile:I

    invoke-direct {v0, v1, v2, v3, v3}, Lcmx;-><init>(Landroid/content/Context;III)V

    invoke-direct {p0, v0}, Lcom/twitter/library/client/navigation/g;->a(Lcmm;)Z

    .line 175
    return-void
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->f:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/twitter/library/client/navigation/g;->e:Lcom/twitter/library/client/navigation/ModernDrawerView;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    .line 140
    const/4 v0, 0x1

    return v0
.end method

.method public f()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/media/ui/image/BadgeableUserImageView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 276
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v3

    .line 277
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g;->e:Lcom/twitter/library/client/navigation/ModernDrawerView;

    invoke-virtual {v0}, Lcom/twitter/library/client/navigation/ModernDrawerView;->getHeaderView()Landroid/view/View;

    move-result-object v0

    sget v1, Lazw$g;->other_accounts:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 279
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v2, v1, :cond_0

    .line 280
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/media/ui/image/BadgeableUserImageView;

    invoke-virtual {v3, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 279
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 283
    :cond_0
    invoke-virtual {v3}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/client/navigation/g;->b(I)Lcmm;

    move-result-object v0

    .line 146
    if-eqz v0, :cond_0

    .line 147
    invoke-direct {p0, v0}, Lcom/twitter/library/client/navigation/g;->a(Lcmm;)Z

    .line 149
    :cond_0
    return-void
.end method

.method public onNavigationItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 179
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/client/navigation/g;->b(I)Lcmm;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/library/client/navigation/g;->a(Lcmm;)Z

    move-result v0

    return v0
.end method
