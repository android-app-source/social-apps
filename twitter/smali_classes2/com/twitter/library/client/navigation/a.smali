.class public Lcom/twitter/library/client/navigation/a;
.super Lcjr;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/client/navigation/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcjr",
        "<",
        "Lcmy$c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Landroid/content/res/ColorStateList;

.field private final c:Landroid/content/res/ColorStateList;

.field private final d:I

.field private final e:I

.field private final f:Z

.field private g:Lcna;

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcmq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;IILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcjr;-><init>(Landroid/content/Context;)V

    .line 60
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/navigation/a;->a:Landroid/view/LayoutInflater;

    .line 61
    iput p2, p0, Lcom/twitter/library/client/navigation/a;->d:I

    .line 62
    iput p3, p0, Lcom/twitter/library/client/navigation/a;->e:I

    .line 63
    iput-object p4, p0, Lcom/twitter/library/client/navigation/a;->b:Landroid/content/res/ColorStateList;

    .line 64
    iput-object p5, p0, Lcom/twitter/library/client/navigation/a;->c:Landroid/content/res/ColorStateList;

    .line 65
    const-string/jumbo v0, "android_avatar_badging_5571"

    invoke-static {v0}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/client/navigation/a;->f:Z

    .line 66
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/navigation/a;->h:Ljava/util/Map;

    .line 67
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/client/navigation/a;)Lcna;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/library/client/navigation/a;->g:Lcna;

    return-object v0
.end method

.method private static a(Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 286
    if-eqz p0, :cond_1

    .line 287
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 288
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 290
    if-eqz p1, :cond_1

    .line 293
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 294
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 296
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 297
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 300
    :cond_1
    return-void
.end method

.method private a(Lcom/twitter/media/ui/image/BadgeableUserImageView;Lcom/twitter/model/account/UserAccount;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 264
    iget-object v0, p0, Lcom/twitter/library/client/navigation/a;->h:Ljava/util/Map;

    iget-object v1, p2, Lcom/twitter/model/account/UserAccount;->b:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmq;

    .line 265
    if-nez v0, :cond_0

    .line 266
    iget-object v0, p2, Lcom/twitter/model/account/UserAccount;->b:Lcom/twitter/model/core/TwitterUser;

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {p3, v0, v1}, Lcom/twitter/library/media/widget/b;->a(Landroid/content/Context;J)Lcmq;

    move-result-object v0

    .line 267
    iget-object v1, p0, Lcom/twitter/library/client/navigation/a;->h:Ljava/util/Map;

    iget-object v2, p2, Lcom/twitter/model/account/UserAccount;->b:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    :cond_0
    new-instance v1, Lcmo;

    invoke-direct {v1, p1}, Lcmo;-><init>(Lcom/twitter/ui/widget/b;)V

    invoke-virtual {v0, v1}, Lcmq;->a(Lcmp;)V

    .line 271
    sget v1, Lazw$g;->drawer_account_item_presenter_tag:I

    invoke-virtual {p1, v1, v0}, Lcom/twitter/media/ui/image/BadgeableUserImageView;->setTag(ILjava/lang/Object;)V

    .line 272
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 275
    iget-object v0, p0, Lcom/twitter/library/client/navigation/a;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmq;

    .line 276
    invoke-virtual {v0}, Lcmq;->a()V

    goto :goto_0

    .line 278
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/client/navigation/a;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 279
    return-void
.end method

.method static synthetic b(Lcom/twitter/library/client/navigation/a;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/twitter/library/client/navigation/a;->b()V

    return-void
.end method


# virtual methods
.method protected a(Lcmy$c;)I
    .locals 1

    .prologue
    .line 91
    instance-of v0, p1, Lcmy$g;

    if-eqz v0, :cond_0

    .line 92
    const/4 v0, 0x4

    .line 102
    :goto_0
    return v0

    .line 93
    :cond_0
    instance-of v0, p1, Lcmy$f;

    if-eqz v0, :cond_1

    .line 94
    const/4 v0, 0x5

    goto :goto_0

    .line 95
    :cond_1
    instance-of v0, p1, Lcmy$e;

    if-eqz v0, :cond_2

    .line 96
    const/4 v0, 0x1

    goto :goto_0

    .line 97
    :cond_2
    instance-of v0, p1, Lcmy$a;

    if-eqz v0, :cond_3

    .line 98
    const/4 v0, 0x2

    goto :goto_0

    .line 99
    :cond_3
    instance-of v0, p1, Lcmy$d;

    if-eqz v0, :cond_4

    .line 100
    const/4 v0, 0x3

    goto :goto_0

    .line 102
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 36
    check-cast p1, Lcmy$c;

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/navigation/a;->a(Lcmy$c;)I

    move-result v0

    return v0
.end method

.method protected a(Landroid/content/Context;Lcmy$c;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 125
    invoke-virtual {p0, p2}, Lcom/twitter/library/client/navigation/a;->a(Lcmy$c;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 166
    iget-object v0, p0, Lcom/twitter/library/client/navigation/a;->a:Landroid/view/LayoutInflater;

    sget v1, Lazw$h;->drawer_menu_item:I

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 167
    new-instance v0, Lcom/twitter/library/client/navigation/a$3;

    invoke-direct {v0, p0}, Lcom/twitter/library/client/navigation/a$3;-><init>(Lcom/twitter/library/client/navigation/a;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    sget v0, Lazw$g;->title:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 178
    iget-object v2, p0, Lcom/twitter/library/client/navigation/a;->b:Landroid/content/res/ColorStateList;

    if-eqz v2, :cond_0

    .line 179
    iget-object v2, p0, Lcom/twitter/library/client/navigation/a;->b:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_0
    move-object v0, v1

    .line 183
    :goto_0
    return-object v0

    .line 127
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/library/client/navigation/a;->a:Landroid/view/LayoutInflater;

    sget v1, Lazw$h;->design_navigation_item_shadow:I

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 131
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/library/client/navigation/a;->a:Landroid/view/LayoutInflater;

    sget v1, Lazw$h;->design_navigation_item_separator:I

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 132
    iget v1, p0, Lcom/twitter/library/client/navigation/a;->e:I

    invoke-virtual {v0, v3, v3, v3, v1}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    .line 136
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/library/client/navigation/a;->a:Landroid/view/LayoutInflater;

    sget v1, Lazw$h;->design_navigation_item_separator:I

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 137
    iget v1, p0, Lcom/twitter/library/client/navigation/a;->e:I

    iget v2, p0, Lcom/twitter/library/client/navigation/a;->e:I

    invoke-virtual {v0, v3, v1, v3, v2}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    .line 141
    :pswitch_3
    iget-object v0, p0, Lcom/twitter/library/client/navigation/a;->a:Landroid/view/LayoutInflater;

    sget v1, Lazw$h;->drawer_account_item:I

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 142
    new-instance v1, Lcom/twitter/library/client/navigation/a$1;

    invoke-direct {v1, p0}, Lcom/twitter/library/client/navigation/a$1;-><init>(Lcom/twitter/library/client/navigation/a;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 153
    :pswitch_4
    iget-object v0, p0, Lcom/twitter/library/client/navigation/a;->a:Landroid/view/LayoutInflater;

    sget v1, Lazw$h;->secondary_drawer_menu_item:I

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 154
    new-instance v1, Lcom/twitter/library/client/navigation/a$2;

    invoke-direct {v1, p0}, Lcom/twitter/library/client/navigation/a$2;-><init>(Lcom/twitter/library/client/navigation/a;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 125
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 36
    check-cast p2, Lcmy$c;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/client/navigation/a;->a(Landroid/content/Context;Lcmy$c;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/view/View;Landroid/content/Context;Lcmy$c;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 188
    invoke-virtual {p1, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 190
    invoke-virtual {p0, p3}, Lcom/twitter/library/client/navigation/a;->a(Lcmy$c;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 258
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 193
    :pswitch_1
    check-cast p3, Lcmy$h;

    .line 194
    iget-object v2, p3, Lcmy$h;->a:Lcmx;

    .line 195
    sget v0, Lazw$g;->drawer_item_tag:I

    invoke-virtual {p1, v0, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 198
    sget v0, Lazw$g;->title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 199
    invoke-virtual {v2}, Lcmx;->g()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    invoke-static {v0, v5, v5, v5, v5}, Lcom/twitter/util/ui/j;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 205
    invoke-virtual {v2}, Lcmx;->c()Landroid/view/View;

    move-result-object v3

    .line 206
    sget v1, Lazw$g;->icon_view:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-static {v1, v3}, Lcom/twitter/library/client/navigation/a;->a(Landroid/view/ViewGroup;Landroid/view/View;)V

    .line 209
    if-nez v3, :cond_2

    invoke-virtual {v2}, Lcmx;->h()I

    move-result v1

    if-eqz v1, :cond_2

    .line 210
    invoke-virtual {p0}, Lcom/twitter/library/client/navigation/a;->j()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v2}, Lcmx;->h()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 211
    if-eqz v1, :cond_2

    .line 212
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v3

    .line 213
    if-nez v3, :cond_3

    :goto_1
    invoke-static {v1}, Landroid/support/v4/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 214
    iget v3, p0, Lcom/twitter/library/client/navigation/a;->d:I

    iget v4, p0, Lcom/twitter/library/client/navigation/a;->d:I

    invoke-virtual {v1, v6, v6, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 215
    iget-object v3, p0, Lcom/twitter/library/client/navigation/a;->c:Landroid/content/res/ColorStateList;

    if-eqz v3, :cond_1

    .line 216
    iget-object v3, p0, Lcom/twitter/library/client/navigation/a;->c:Landroid/content/res/ColorStateList;

    invoke-static {v1, v3}, Landroid/support/v4/graphics/drawable/DrawableCompat;->setTintList(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 218
    :cond_1
    invoke-static {v0, v1, v5, v5, v5}, Lcom/twitter/util/ui/j;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 223
    :cond_2
    sget v0, Lazw$g;->action_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v2}, Lcmx;->e()Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/client/navigation/a;->a(Landroid/view/ViewGroup;Landroid/view/View;)V

    goto :goto_0

    .line 213
    :cond_3
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_1

    .line 227
    :pswitch_2
    check-cast p3, Lcmy$a;

    iget-object v2, p3, Lcmy$a;->a:Lcom/twitter/model/account/UserAccount;

    .line 228
    sget v0, Lazw$g;->drawer_item_tag:I

    invoke-virtual {p1, v0, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 230
    sget v0, Lazw$g;->user_image:I

    .line 231
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/BadgeableUserImageView;

    .line 232
    iget-object v1, v2, Lcom/twitter/model/account/UserAccount;->b:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/BadgeableUserImageView;->a(Lcom/twitter/model/core/TwitterUser;)Z

    .line 235
    sget v1, Lazw$g;->drawer_account_item_presenter_tag:I

    .line 236
    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcmq;

    .line 237
    if-eqz v1, :cond_4

    .line 238
    invoke-virtual {v1, v5}, Lcmq;->a(Lcmp;)V

    .line 241
    :cond_4
    iget-object v1, v2, Lcom/twitter/model/account/UserAccount;->b:Lcom/twitter/model/core/TwitterUser;

    if-eqz v1, :cond_0

    .line 242
    sget v1, Lazw$g;->account_name:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 243
    iget-object v3, v2, Lcom/twitter/model/account/UserAccount;->b:Lcom/twitter/model/core/TwitterUser;

    iget-object v3, v3, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 245
    sget v1, Lazw$g;->username:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 246
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "@"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/twitter/model/account/UserAccount;->b:Lcom/twitter/model/core/TwitterUser;

    iget-object v4, v4, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    iget-boolean v1, p0, Lcom/twitter/library/client/navigation/a;->f:Z

    if-eqz v1, :cond_0

    .line 249
    invoke-direct {p0, v0, v2, p2}, Lcom/twitter/library/client/navigation/a;->a(Lcom/twitter/media/ui/image/BadgeableUserImageView;Lcom/twitter/model/account/UserAccount;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 190
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 36
    check-cast p3, Lcmy$c;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/client/navigation/a;->a(Landroid/view/View;Landroid/content/Context;Lcmy$c;)V

    return-void
.end method

.method public a(Lcna;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/twitter/library/client/navigation/a;->g:Lcna;

    .line 77
    return-void
.end method

.method protected a(Landroid/content/Context;Lcmy$c;)Z
    .locals 1

    .prologue
    .line 86
    instance-of v0, p2, Lcmy$e;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 36
    check-cast p2, Lcmy$c;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/client/navigation/a;->a(Landroid/content/Context;Lcmy$c;)Z

    move-result v0

    return v0
.end method

.method protected ac_()Lcjt;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcjt",
            "<",
            "Lcmy$c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, Lcom/twitter/library/client/navigation/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/client/navigation/a$a;-><init>(Lcom/twitter/library/client/navigation/a;Lcom/twitter/library/client/navigation/a$1;)V

    return-object v0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 112
    invoke-virtual {p0, p1}, Lcom/twitter/library/client/navigation/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmy$c;

    .line 113
    instance-of v1, v0, Lcmy$h;

    if-eqz v1, :cond_0

    .line 114
    check-cast v0, Lcmy$h;

    iget-object v0, v0, Lcmy$h;->a:Lcmx;

    invoke-virtual {v0}, Lcmx;->a()I

    move-result v0

    int-to-long v0, v0

    .line 116
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x6

    return v0
.end method
