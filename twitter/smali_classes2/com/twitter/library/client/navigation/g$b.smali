.class Lcom/twitter/library/client/navigation/g$b;
.super Lazt;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/client/navigation/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "b"
.end annotation


# instance fields
.field final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lazv;",
            ">;"
        }
    .end annotation
.end field

.field final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcmx;",
            ">;"
        }
    .end annotation
.end field

.field final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic g:Lcom/twitter/library/client/navigation/g;


# direct methods
.method constructor <init>(Lcom/twitter/library/client/navigation/g;Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 428
    iput-object p1, p0, Lcom/twitter/library/client/navigation/g$b;->g:Lcom/twitter/library/client/navigation/g;

    .line 429
    invoke-direct {p0, p2, p3}, Lazt;-><init>(Landroid/content/Context;I)V

    .line 422
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/navigation/g$b;->d:Ljava/util/List;

    .line 424
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/navigation/g$b;->e:Ljava/util/List;

    .line 425
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/client/navigation/g$b;->f:Ljava/util/Set;

    .line 430
    return-void
.end method


# virtual methods
.method protected b()V
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 434
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g$b;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/library/client/navigation/g$b;->c:Landroid/util/AttributeSet;

    sget-object v2, Lazw$l;->ToolBarItem:[I

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 435
    sget v0, Lazw$l;->ToolBarItem_component:I

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 436
    sget v1, Lazw$l;->ToolBarItem_android_id:I

    invoke-virtual {v2, v1, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 437
    const-string/jumbo v1, "drawer"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 438
    sget v0, Lazw$l;->ToolBarItem_drawerTitle:I

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 439
    if-nez v0, :cond_0

    .line 440
    sget v0, Lazw$l;->ToolBarItem_android_title:I

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 442
    :cond_0
    sget v1, Lazw$l;->ToolBarItem_drawerIcon:I

    invoke-virtual {v2, v1, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 443
    if-nez v1, :cond_1

    .line 444
    sget v1, Lazw$l;->ToolBarItem_android_icon:I

    invoke-virtual {v2, v1, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 447
    :cond_1
    sget v4, Lazw$l;->ToolBarItem_groupId:I

    const v5, 0x7fffffff

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    .line 448
    sget v5, Lazw$l;->ToolBarItem_order:I

    const/16 v6, 0x1f4

    invoke-virtual {v2, v5, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v5

    .line 449
    sget v6, Lazw$l;->ToolBarItem_android_visible:I

    const/4 v7, 0x1

    invoke-virtual {v2, v6, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v6

    .line 450
    sget v7, Lazw$l;->ToolBarItem_actionLayout:I

    invoke-virtual {v2, v7, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    .line 451
    sget v8, Lazw$l;->ToolBarItem_iconLayoutResId:I

    invoke-virtual {v2, v8, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v8

    .line 453
    new-instance v9, Lcmx;

    iget-object v10, p0, Lcom/twitter/library/client/navigation/g$b;->b:Landroid/content/Context;

    invoke-direct {v9, v10, v3, v4, v5}, Lcmx;-><init>(Landroid/content/Context;III)V

    .line 454
    invoke-virtual {v9, v0}, Lcmx;->a(Ljava/lang/CharSequence;)Lcmx;

    move-result-object v0

    .line 455
    invoke-virtual {v0, v1}, Lcmx;->b(I)Lcmx;

    move-result-object v0

    .line 456
    invoke-virtual {v0, v8}, Lcmx;->c(I)Lcmx;

    move-result-object v0

    .line 457
    invoke-virtual {v0, v6}, Lcmx;->a(Z)Lcmx;

    move-result-object v0

    .line 458
    invoke-virtual {v0, v7}, Lcmx;->d(I)Lcmx;

    move-result-object v0

    .line 461
    sget v1, Lazw$g;->accounts:I

    if-eq v3, v1, :cond_2

    .line 462
    iget-object v1, p0, Lcom/twitter/library/client/navigation/g$b;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 472
    :cond_2
    :goto_0
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 473
    return-void

    .line 464
    :cond_3
    const-string/jumbo v1, "tabs"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 468
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g$b;->f:Ljava/util/Set;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 470
    :cond_4
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g$b;->d:Ljava/util/List;

    new-instance v1, Lazv;

    iget-object v3, p0, Lcom/twitter/library/client/navigation/g$b;->g:Lcom/twitter/library/client/navigation/g;

    iget-object v3, v3, Lcom/twitter/library/client/navigation/g;->a:Lcom/twitter/internal/android/widget/ToolBar;

    iget-object v4, p0, Lcom/twitter/library/client/navigation/g$b;->b:Landroid/content/Context;

    iget-object v5, p0, Lcom/twitter/library/client/navigation/g$b;->c:Landroid/util/AttributeSet;

    invoke-direct {v1, v3, v4, v5}, Lazv;-><init>(Lcom/twitter/internal/android/widget/ToolBar;Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
