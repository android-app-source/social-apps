.class public Lcom/twitter/library/client/navigation/g$c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcmm;
.implements Lcom/twitter/ui/widget/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/client/navigation/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/client/navigation/g;

.field private final b:Lcom/twitter/library/client/m;


# direct methods
.method public constructor <init>(Lcom/twitter/library/client/navigation/g;Lcom/twitter/library/client/m;)V
    .locals 0

    .prologue
    .line 307
    iput-object p1, p0, Lcom/twitter/library/client/navigation/g$c;->a:Lcom/twitter/library/client/navigation/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 308
    iput-object p2, p0, Lcom/twitter/library/client/navigation/g$c;->b:Lcom/twitter/library/client/m;

    .line 309
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g$c;->b:Lcom/twitter/library/client/m;

    invoke-virtual {v0}, Lcom/twitter/library/client/m;->c()I

    move-result v0

    return v0
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcmm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 394
    return-void
.end method

.method public c()Landroid/view/View;
    .locals 1

    .prologue
    .line 376
    const/4 v0, 0x0

    return-object v0
.end method

.method public e()Landroid/view/View;
    .locals 1

    .prologue
    .line 388
    const/4 v0, 0x0

    return-object v0
.end method

.method public e(Z)Lcmm;
    .locals 0

    .prologue
    .line 347
    return-object p0
.end method

.method public f(Z)Lcmm;
    .locals 0

    .prologue
    .line 314
    return-object p0
.end method

.method public g(I)Lcmm;
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 358
    return-object p0
.end method

.method public n()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 330
    const/4 v0, 0x0

    return-object v0
.end method

.method public setBadgeMode(I)V
    .locals 2

    .prologue
    .line 404
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    .line 405
    :goto_0
    iget-object v1, p0, Lcom/twitter/library/client/navigation/g$c;->b:Lcom/twitter/library/client/m;

    iget-boolean v1, v1, Lcom/twitter/library/client/m;->h:Z

    if-eq v0, v1, :cond_0

    .line 406
    iget-object v1, p0, Lcom/twitter/library/client/navigation/g$c;->b:Lcom/twitter/library/client/m;

    iput-boolean v0, v1, Lcom/twitter/library/client/m;->h:Z

    .line 407
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g$c;->a:Lcom/twitter/library/client/navigation/g;

    invoke-static {v0}, Lcom/twitter/library/client/navigation/g;->a(Lcom/twitter/library/client/navigation/g;)Landroid/widget/BaseAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 409
    :cond_0
    return-void

    .line 404
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBadgeNumber(I)V
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g$c;->b:Lcom/twitter/library/client/m;

    iget v0, v0, Lcom/twitter/library/client/m;->i:I

    if-eq v0, p1, :cond_0

    .line 414
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g$c;->b:Lcom/twitter/library/client/m;

    iput p1, v0, Lcom/twitter/library/client/m;->i:I

    .line 415
    iget-object v0, p0, Lcom/twitter/library/client/navigation/g$c;->a:Lcom/twitter/library/client/navigation/g;

    invoke-static {v0}, Lcom/twitter/library/client/navigation/g;->a(Lcom/twitter/library/client/navigation/g;)Landroid/widget/BaseAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 417
    :cond_0
    return-void
.end method
