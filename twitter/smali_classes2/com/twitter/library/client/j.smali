.class public Lcom/twitter/library/client/j;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    .line 30
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/j;->a:Landroid/content/Context;

    .line 31
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 36
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v1

    .line 37
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v4

    .line 38
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    .line 39
    if-nez v0, :cond_1

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    iget-object v0, v0, Lcom/twitter/library/service/v;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/twitter/library/client/v;->c(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v5

    .line 43
    if-eqz v5, :cond_0

    .line 46
    invoke-virtual {v1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 47
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->h()Lcom/twitter/library/api/RateLimit;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/api/RateLimit;)V

    .line 48
    invoke-virtual {p0, p1, v1, v5}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/service/s;Lcom/twitter/async/service/j;Lcom/twitter/library/client/Session;)V

    .line 51
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v6

    .line 52
    if-eqz v6, :cond_2

    iget v1, v6, Lcom/twitter/network/l;->a:I

    const/16 v7, 0x191

    if-ne v1, v7, :cond_2

    iget v1, v6, Lcom/twitter/network/l;->j:I

    const/16 v7, 0x59

    if-ne v1, v7, :cond_2

    move v1, v2

    .line 55
    :goto_1
    if-eqz v1, :cond_0

    .line 56
    iget v1, v6, Lcom/twitter/network/l;->j:I

    .line 57
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    .line 58
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    invoke-direct {v1, v8, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v8, "api:::unauthorized:error"

    aput-object v8, v2, v3

    .line 59
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 60
    invoke-virtual {v1, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 62
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->f()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->i()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    .line 61
    invoke-static {v1, v0, v6}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/model/ScribeLog;Ljava/lang/String;Lcom/twitter/network/l;)V

    .line 63
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 64
    invoke-virtual {v4, v5, p1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/service/s;)Ljava/lang/String;

    goto :goto_0

    :cond_2
    move v1, v3

    .line 52
    goto :goto_1
.end method

.method public a(Lcom/twitter/library/service/s;Lcom/twitter/async/service/j;Lcom/twitter/library/client/Session;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/service/s;",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;",
            "Lcom/twitter/library/client/Session;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v8, -0x1

    .line 70
    iget-object v1, p1, Lcom/twitter/library/service/s;->o:Landroid/os/Bundle;

    .line 71
    invoke-virtual {p2}, Lcom/twitter/async/service/j;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 72
    iget-object v0, v0, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    .line 73
    const-string/jumbo v3, "scribe_item_count"

    invoke-virtual {v0, v3, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 75
    const-string/jumbo v0, "scribe_log"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 76
    if-eqz v0, :cond_2

    .line 86
    :goto_1
    if-eqz v0, :cond_0

    .line 87
    if-le v3, v8, :cond_1

    .line 88
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(I)Lcom/twitter/analytics/model/ScribeLog;

    .line 90
    :cond_1
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 79
    :cond_2
    const-string/jumbo v0, "scribe_event"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 80
    if-eqz v0, :cond_3

    .line 81
    new-instance v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct {v4, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    goto :goto_1

    .line 83
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 93
    :cond_4
    return-void
.end method
