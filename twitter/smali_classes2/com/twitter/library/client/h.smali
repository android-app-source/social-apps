.class public Lcom/twitter/library/client/h;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/library/client/h;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/twitter/library/client/v;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/twitter/library/client/h;->b:Landroid/content/Context;

    .line 51
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/h;->c:Lcom/twitter/library/client/v;

    .line 52
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/library/client/h;
    .locals 3

    .prologue
    .line 37
    const-class v1, Lcom/twitter/library/client/h;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/client/h;->a:Lcom/twitter/library/client/h;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/twitter/library/client/h;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/library/client/h;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/library/client/h;->a:Lcom/twitter/library/client/h;

    .line 39
    const-class v0, Lcom/twitter/library/client/h;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 41
    :cond_0
    sget-object v0, Lcom/twitter/library/client/h;->a:Lcom/twitter/library/client/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected a(Landroid/content/Context;JLjava/lang/String;)Lcom/twitter/util/a;
    .locals 2

    .prologue
    .line 114
    new-instance v0, Lcom/twitter/util/a;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 55
    invoke-static {}, Lcon;->a()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/h$1;

    invoke-direct {v1, p0}, Lcom/twitter/library/client/h$1;-><init>(Lcom/twitter/library/client/h;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 61
    return-void
.end method

.method public b()V
    .locals 6

    .prologue
    .line 65
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 66
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 67
    new-instance v1, Lcom/twitter/util/a;

    iget-object v2, p0, Lcom/twitter/library/client/h;->b:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    const-string/jumbo v3, "decider"

    invoke-direct {v1, v2, v4, v5, v3}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    .line 69
    const-string/jumbo v2, "cache_dirty"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/twitter/util/a;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 70
    new-instance v2, Laut;

    iget-object v3, p0, Lcom/twitter/library/client/h;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v2, v3}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 71
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/twitter/library/provider/t;->c(Laut;)V

    .line 72
    invoke-virtual {v2}, Laut;->a()V

    .line 73
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/twitter/library/scribe/ScribeDatabaseHelper;->a(J)Lcom/twitter/library/scribe/ScribeDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/scribe/ScribeDatabaseHelper;->c()V

    .line 74
    invoke-virtual {v1}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "cache_dirty"

    .line 75
    invoke-virtual {v0, v1}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 78
    :cond_0
    return-void
.end method

.method public c()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 84
    iget-object v0, p0, Lcom/twitter/library/client/h;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/library/client/h;->c:Lcom/twitter/library/client/v;

    .line 86
    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const-string/jumbo v1, "decider"

    .line 84
    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/twitter/library/client/h;->a(Landroid/content/Context;JLjava/lang/String;)Lcom/twitter/util/a;

    move-result-object v0

    .line 91
    const-string/jumbo v1, "cache_version"

    invoke-virtual {v0, v1, v4}, Lcom/twitter/util/a;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 93
    const-string/jumbo v2, "cache_version"

    .line 94
    invoke-static {v2, v4}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v2

    .line 95
    if-eq v1, v4, :cond_1

    .line 96
    if-ge v1, v2, :cond_0

    .line 97
    invoke-virtual {v0}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "cache_version"

    .line 98
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;I)Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "cache_dirty"

    const/4 v2, 0x1

    .line 99
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Z)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 109
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/library/client/h;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/c;->a(Landroid/content/Context;)Lcom/twitter/library/client/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/c;->a()V

    .line 110
    return-void

    .line 104
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "cache_version"

    .line 105
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;I)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    goto :goto_0
.end method
