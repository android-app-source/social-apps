.class public Lcom/twitter/library/client/q;
.super Lcom/twitter/library/client/p;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/client/q$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/q$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/library/client/q$a",
            "<",
            "Landroid/os/Bundle;",
            "Lcom/twitter/library/service/s;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/library/client/j;

.field private final d:Lcom/twitter/async/service/i;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/twitter/library/client/p;-><init>()V

    .line 39
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/q;->a:Landroid/content/Context;

    .line 40
    new-instance v0, Lcom/twitter/library/client/j;

    iget-object v1, p0, Lcom/twitter/library/client/q;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/twitter/library/client/j;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/library/client/q;->c:Lcom/twitter/library/client/j;

    .line 41
    new-instance v0, Lcom/twitter/library/client/q$a;

    invoke-direct {v0}, Lcom/twitter/library/client/q$a;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/client/q;->b:Lcom/twitter/library/client/q$a;

    .line 42
    new-instance v0, Lcom/twitter/async/service/i;

    invoke-direct {v0}, Lcom/twitter/async/service/i;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/client/q;->d:Lcom/twitter/async/service/i;

    .line 43
    return-void
.end method

.method private b(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/twitter/library/client/q;->c:Lcom/twitter/library/client/j;

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/s;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    .line 115
    if-eqz p2, :cond_0

    .line 116
    invoke-virtual {p1, p2, p3}, Lcom/twitter/library/service/s;->a(Lcom/twitter/async/service/AsyncOperation$b;Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)Lcom/twitter/async/service/AsyncOperation;

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/client/q;->b:Lcom/twitter/library/client/q$a;

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/s;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    .line 119
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/AsyncOperation",
            "<**>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/twitter/library/client/q;->d:Lcom/twitter/async/service/i;

    .line 71
    invoke-virtual {v0}, Lcom/twitter/async/service/i;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 72
    iget-object v1, p0, Lcom/twitter/library/client/q;->a:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/twitter/library/client/q;->a:Landroid/content/Context;

    const-class v4, Lcom/twitter/async/service/AsyncService;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 75
    :cond_0
    invoke-virtual {v0, p1}, Lcom/twitter/async/service/i;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/service/s;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 63
    const/4 v0, 0x0

    sget-object v1, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->h:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/library/client/q;->b(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)V

    .line 64
    invoke-virtual {p0, p1}, Lcom/twitter/library/client/q;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->h:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/library/client/q;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/client/q;->b(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)V

    .line 100
    invoke-virtual {p0, p1}, Lcom/twitter/library/client/q;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/twitter/library/client/q;->d:Lcom/twitter/async/service/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/async/service/i;->a(Z)V

    .line 148
    return-void
.end method

.method public a(Lcom/twitter/async/service/e;)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/library/client/q;->d:Lcom/twitter/async/service/i;

    invoke-virtual {v0, p1}, Lcom/twitter/async/service/i;->a(Lcom/twitter/async/service/e;)V

    .line 58
    return-void
.end method

.method public a(Lcom/twitter/library/service/t;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/library/client/q;->b:Lcom/twitter/library/client/q$a;

    invoke-virtual {v0, p1}, Lcom/twitter/library/client/q$a;->a(Lcom/twitter/async/service/AsyncOperation$b;)V

    .line 48
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/twitter/library/client/q;->d:Lcom/twitter/async/service/i;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/async/service/i;->a(Ljava/lang/String;Z)V

    .line 158
    return-void
.end method

.method public a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z
    .locals 2

    .prologue
    .line 80
    if-eqz p3, :cond_0

    new-instance v0, Lcom/twitter/library/client/t;

    invoke-direct {v0, p2, p3}, Lcom/twitter/library/client/t;-><init>(ILcom/twitter/library/client/s;)V

    .line 82
    :goto_0
    sget-object v1, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->h:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/library/client/q;->b(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)V

    .line 83
    invoke-virtual {p0, p1}, Lcom/twitter/library/client/q;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 84
    const/4 v0, 0x1

    return v0

    .line 80
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/twitter/library/client/q;->d:Lcom/twitter/async/service/i;

    invoke-virtual {v0, p1}, Lcom/twitter/async/service/i;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b(Lcom/twitter/library/service/t;)V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/library/client/q;->b:Lcom/twitter/library/client/q$a;

    invoke-virtual {v0, p1}, Lcom/twitter/library/client/q$a;->b(Lcom/twitter/async/service/AsyncOperation$b;)V

    .line 53
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/library/client/q;->a(Ljava/lang/String;Z)V

    .line 153
    return-void
.end method
