.class public Lcom/twitter/library/client/a;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static declared-synchronized a(Landroid/content/Context;J)J
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    .line 21
    const-class v2, Lcom/twitter/library/client/a;

    monitor-enter v2

    :try_start_0
    invoke-static {p0, p1, p2}, Lcom/twitter/library/client/a;->b(Landroid/content/Context;J)Lcom/twitter/util/a;

    move-result-object v3

    .line 22
    const-string/jumbo v0, "read_"

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v0, v4, v5}, Lcom/twitter/util/a;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 23
    cmp-long v4, v0, v6

    if-nez v4, :cond_0

    .line 25
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    .line 26
    invoke-virtual {v3}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v3

    const-string/jumbo v4, "read_"

    invoke-virtual {v3, v4, v0, v1}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;J)Lcom/twitter/util/a$a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/util/a$a;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 28
    :cond_0
    monitor-exit v2

    return-wide v0

    .line 21
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;JJ)V
    .locals 3

    .prologue
    .line 36
    const-class v1, Lcom/twitter/library/client/a;

    monitor-enter v1

    :try_start_0
    invoke-static {p0, p1, p2}, Lcom/twitter/library/client/a;->b(Landroid/content/Context;J)Lcom/twitter/util/a;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v2, "read_"

    invoke-virtual {v0, v2, p3, p4}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;J)Lcom/twitter/util/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    monitor-exit v1

    return-void

    .line 36
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static b(Landroid/content/Context;J)Lcom/twitter/util/a;
    .locals 3

    .prologue
    .line 57
    new-instance v0, Lcom/twitter/util/a;

    const-string/jumbo v1, "activity"

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    return-object v0
.end method

.method public static declared-synchronized b(Landroid/content/Context;JJ)Z
    .locals 7

    .prologue
    .line 46
    const-class v1, Lcom/twitter/library/client/a;

    monitor-enter v1

    :try_start_0
    invoke-static {p0, p1, p2}, Lcom/twitter/library/client/a;->b(Landroid/content/Context;J)Lcom/twitter/util/a;

    move-result-object v0

    .line 47
    const-string/jumbo v2, "read_"

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v2, v4, v5}, Lcom/twitter/util/a;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 48
    cmp-long v0, p3, v2

    if-lez v0, :cond_0

    .line 49
    invoke-static {p0, p1, p2, p3, p4}, Lcom/twitter/library/client/a;->a(Landroid/content/Context;JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    const/4 v0, 0x1

    .line 52
    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
