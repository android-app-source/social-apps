.class Lcom/twitter/library/client/v$c;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/client/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/client/v;


# direct methods
.method private constructor <init>(Lcom/twitter/library/client/v;)V
    .locals 0

    .prologue
    .line 923
    iput-object p1, p0, Lcom/twitter/library/client/v$c;->a:Lcom/twitter/library/client/v;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/library/client/v;Lcom/twitter/library/client/v$1;)V
    .locals 0

    .prologue
    .line 923
    invoke-direct {p0, p1}, Lcom/twitter/library/client/v$c;-><init>(Lcom/twitter/library/client/v;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 923
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/v$c;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 7

    .prologue
    .line 980
    iget-object v0, p0, Lcom/twitter/library/client/v$c;->a:Lcom/twitter/library/client/v;

    invoke-static {v0}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/v;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/library/service/v;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/Session;

    .line 981
    if-nez v2, :cond_1

    .line 990
    :cond_0
    :goto_0
    return-void

    .line 985
    :cond_1
    instance-of v0, p1, Lbas;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 986
    check-cast v0, Lbas;

    .line 987
    invoke-virtual {v0}, Lbas;->s()[I

    move-result-object v3

    .line 988
    invoke-virtual {v0}, Lbas;->t()Lcom/twitter/model/account/LoginResponse;

    move-result-object v4

    invoke-virtual {v0}, Lbas;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lbas;->h()Lcom/twitter/model/core/TwitterUser;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    .line 987
    invoke-virtual/range {v0 .. v6}, Lcom/twitter/library/client/v$c;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/client/Session;[ILcom/twitter/model/account/LoginResponse;Ljava/lang/String;Lcom/twitter/model/core/TwitterUser;)V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/service/s;Lcom/twitter/library/client/Session;[ILcom/twitter/model/account/LoginResponse;Ljava/lang/String;Lcom/twitter/model/core/TwitterUser;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 928
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    .line 929
    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 930
    iget-object v1, p0, Lcom/twitter/library/client/v$c;->a:Lcom/twitter/library/client/v;

    .line 931
    invoke-static {v1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/v;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/library/service/s;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/client/v$d;

    .line 933
    const-string/jumbo v2, "login_challenge_enabled"

    invoke-static {v2}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz p4, :cond_1

    iget v2, p4, Lcom/twitter/model/account/LoginResponse;->d:I

    if-ne v2, v3, :cond_1

    iget-object v2, p4, Lcom/twitter/model/account/LoginResponse;->b:Lcom/twitter/model/account/LoginVerificationRequiredResponse;

    iget v2, v2, Lcom/twitter/model/account/LoginVerificationRequiredResponse;->e:I

    if-ne v2, v3, :cond_1

    .line 938
    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p2, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    .line 939
    if-eqz v1, :cond_0

    .line 940
    iget-object v0, p4, Lcom/twitter/model/account/LoginResponse;->b:Lcom/twitter/model/account/LoginVerificationRequiredResponse;

    invoke-interface {v1, p2, v0}, Lcom/twitter/library/client/v$d;->b(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/LoginVerificationRequiredResponse;)V

    .line 976
    :cond_0
    :goto_0
    return-void

    .line 943
    :cond_1
    if-eqz p4, :cond_3

    iget v2, p4, Lcom/twitter/model/account/LoginResponse;->d:I

    if-ne v2, v3, :cond_3

    iget-object v2, p4, Lcom/twitter/model/account/LoginResponse;->b:Lcom/twitter/model/account/LoginVerificationRequiredResponse;

    iget v2, v2, Lcom/twitter/model/account/LoginVerificationRequiredResponse;->e:I

    if-eq v2, v4, :cond_2

    iget-object v2, p4, Lcom/twitter/model/account/LoginResponse;->b:Lcom/twitter/model/account/LoginVerificationRequiredResponse;

    iget v2, v2, Lcom/twitter/model/account/LoginVerificationRequiredResponse;->e:I

    if-nez v2, :cond_3

    .line 948
    :cond_2
    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p2, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    .line 949
    if-eqz v1, :cond_0

    .line 950
    iget-object v0, p4, Lcom/twitter/model/account/LoginResponse;->b:Lcom/twitter/model/account/LoginVerificationRequiredResponse;

    invoke-interface {v1, p2, v0}, Lcom/twitter/library/client/v$d;->a(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/LoginVerificationRequiredResponse;)V

    goto :goto_0

    .line 954
    :cond_3
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 956
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/v$c;->a:Lcom/twitter/library/client/v;

    invoke-static {v0, p2, p4, p6}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/v;Lcom/twitter/library/client/Session;Lcom/twitter/model/account/LoginResponse;Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;

    move-result-object v0

    .line 958
    if-eqz v1, :cond_0

    .line 959
    invoke-interface {v1, p2, v0}, Lcom/twitter/library/client/v$d;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 961
    :catch_0
    move-exception v0

    .line 962
    iget-object v0, p0, Lcom/twitter/library/client/v$c;->a:Lcom/twitter/library/client/v;

    invoke-static {v0, p2, p5}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/v;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    .line 963
    if-eqz v1, :cond_0

    .line 964
    invoke-interface {v1, p2, v3, p3}, Lcom/twitter/library/client/v$d;->a(Lcom/twitter/library/client/Session;I[I)V

    goto :goto_0

    .line 968
    :cond_4
    if-eqz v1, :cond_0

    .line 969
    instance-of v0, p1, Lbas;

    if-eqz v0, :cond_5

    .line 970
    invoke-interface {v1, p2, v4, p3}, Lcom/twitter/library/client/v$d;->a(Lcom/twitter/library/client/Session;I[I)V

    .line 972
    :cond_5
    iget-object v0, p0, Lcom/twitter/library/client/v$c;->a:Lcom/twitter/library/client/v;

    invoke-static {v0, p2, p5}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/v;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    goto :goto_0
.end method
