.class Lcom/twitter/library/client/v$e;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/client/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "e"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/client/v;

.field private final b:I


# direct methods
.method constructor <init>(Lcom/twitter/library/client/v;I)V
    .locals 0

    .prologue
    .line 1093
    iput-object p1, p0, Lcom/twitter/library/client/v$e;->a:Lcom/twitter/library/client/v;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    .line 1094
    iput p2, p0, Lcom/twitter/library/client/v$e;->b:I

    .line 1095
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 1088
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/v$e;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1101
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    .line 1102
    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 1103
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v1

    iget-object v5, v1, Lcom/twitter/library/service/v;->a:Ljava/lang/String;

    .line 1104
    iget-object v1, p0, Lcom/twitter/library/client/v$e;->a:Lcom/twitter/library/client/v;

    invoke-static {v1}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/v;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/client/Session;

    .line 1105
    if-nez v1, :cond_1

    .line 1157
    :cond_0
    :goto_0
    return-void

    .line 1108
    :cond_1
    iget v2, p0, Lcom/twitter/library/client/v$e;->b:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1147
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/library/client/v$e;->a:Lcom/twitter/library/client/v;

    invoke-static {v0, v5}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/v;Ljava/lang/String;)Z

    move-result v0

    .line 1148
    sget-object v2, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    .line 1149
    iget-object v2, p0, Lcom/twitter/library/client/v$e;->a:Lcom/twitter/library/client/v;

    invoke-static {v2, v1, v0}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/v;Lcom/twitter/library/client/Session;Z)V

    .line 1150
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->a()V

    goto :goto_0

    .line 1110
    :pswitch_1
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct {v2, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v6, v3, [Ljava/lang/String;

    const-string/jumbo v7, "api::verify_credentials:unauthorized:check"

    aput-object v7, v6, v4

    .line 1111
    invoke-virtual {v2, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 1110
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 1112
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v2

    .line 1115
    if-eqz v2, :cond_3

    iget v6, v2, Lcom/twitter/network/l;->a:I

    const/16 v7, 0x191

    if-ne v6, v7, :cond_2

    iget v2, v2, Lcom/twitter/network/l;->j:I

    const/16 v6, 0x59

    if-ne v2, v6, :cond_2

    move v2, v3

    .line 1120
    :goto_1
    if-eqz v2, :cond_0

    .line 1122
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v0

    iget v0, v0, Lcom/twitter/network/l;->j:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 1123
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct {v0, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v6, "api::verify_credentials:unauthorized:logout"

    aput-object v6, v3, v4

    .line 1124
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 1125
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 1126
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1128
    const-string/jumbo v0, "SessionManager"

    const-string/jumbo v2, "Invalid credentials. The auth token has expired."

    invoke-static {v0, v2}, Lcqj;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1129
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/b;->b(Ljava/lang/String;)Lakm;

    move-result-object v0

    .line 1130
    if-eqz v0, :cond_0

    .line 1134
    iget-object v2, p0, Lcom/twitter/library/client/v$e;->a:Lcom/twitter/library/client/v;

    invoke-static {v2, v5}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/v;Ljava/lang/String;)Z

    move-result v2

    .line 1137
    invoke-virtual {v0, v8}, Lakm;->b(Ljava/lang/String;)V

    .line 1138
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v0

    invoke-virtual {v0, v8}, Lakn;->a(Ljava/lang/String;)V

    .line 1139
    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    .line 1140
    iget-object v0, p0, Lcom/twitter/library/client/v$e;->a:Lcom/twitter/library/client/v;

    invoke-static {v0, v1}, Lcom/twitter/library/client/v;->c(Lcom/twitter/library/client/v;Lcom/twitter/library/client/Session;)V

    .line 1141
    iget-object v0, p0, Lcom/twitter/library/client/v$e;->a:Lcom/twitter/library/client/v;

    invoke-static {v0, v1, v2}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/v;Lcom/twitter/library/client/Session;Z)V

    .line 1142
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->a()V

    goto/16 :goto_0

    :cond_2
    move v2, v4

    .line 1115
    goto :goto_1

    :cond_3
    move v2, v4

    goto :goto_1

    .line 1108
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
