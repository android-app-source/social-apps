.class public final Lcom/twitter/library/client/m;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/client/m$a;
    }
.end annotation


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/app/common/base/BaseFragment;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/CharSequence;

.field public final d:I

.field public final e:Ljava/lang/String;

.field public final f:I

.field public final g:Ljava/lang/Object;

.field public h:Z

.field public i:I

.field public j:I

.field public final k:Ljava/lang/CharSequence;

.field private l:Lcom/twitter/app/common/base/b;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/app/common/base/BaseFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/library/client/m$a;)V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iget-object v0, p1, Lcom/twitter/library/client/m$a;->a:Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/library/client/m;->a:Landroid/net/Uri;

    .line 43
    iget-object v0, p1, Lcom/twitter/library/client/m$a;->b:Ljava/lang/Class;

    iput-object v0, p0, Lcom/twitter/library/client/m;->b:Ljava/lang/Class;

    .line 44
    iget-object v0, p1, Lcom/twitter/library/client/m$a;->c:Lcom/twitter/app/common/base/b;

    sget-object v1, Lcom/twitter/app/common/base/b;->b:Lcom/twitter/app/common/base/b;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/base/b;

    iput-object v0, p0, Lcom/twitter/library/client/m;->l:Lcom/twitter/app/common/base/b;

    .line 45
    iget-object v0, p1, Lcom/twitter/library/client/m$a;->d:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/twitter/library/client/m;->c:Ljava/lang/CharSequence;

    .line 46
    iget v0, p1, Lcom/twitter/library/client/m$a;->i:I

    iput v0, p0, Lcom/twitter/library/client/m;->d:I

    .line 47
    iget-object v0, p1, Lcom/twitter/library/client/m$a;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/client/m;->e:Ljava/lang/String;

    .line 48
    iget v0, p1, Lcom/twitter/library/client/m$a;->g:I

    iput v0, p0, Lcom/twitter/library/client/m;->f:I

    .line 49
    iget-boolean v0, p1, Lcom/twitter/library/client/m$a;->h:Z

    iput-boolean v0, p0, Lcom/twitter/library/client/m;->h:Z

    .line 50
    iget-object v0, p1, Lcom/twitter/library/client/m$a;->f:Ljava/lang/Object;

    iput-object v0, p0, Lcom/twitter/library/client/m;->g:Ljava/lang/Object;

    .line 51
    iget-object v0, p1, Lcom/twitter/library/client/m$a;->j:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/twitter/library/client/m;->k:Ljava/lang/CharSequence;

    .line 52
    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/app/FragmentManager;)Lcom/twitter/app/common/base/BaseFragment;
    .locals 2

    .prologue
    .line 61
    const/4 v0, 0x0

    .line 62
    iget-object v1, p0, Lcom/twitter/library/client/m;->n:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    .line 63
    iget-object v0, p0, Lcom/twitter/library/client/m;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/base/BaseFragment;

    .line 64
    if-nez v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/twitter/library/client/m;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/base/BaseFragment;

    .line 66
    if-eqz v0, :cond_0

    .line 67
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/twitter/library/client/m;->n:Ljava/lang/ref/WeakReference;

    .line 71
    :cond_0
    return-object v0
.end method

.method public a()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/library/client/m;->l:Lcom/twitter/app/common/base/b;

    return-object v0
.end method

.method public a(Lcom/twitter/app/common/base/BaseFragment;)V
    .locals 1

    .prologue
    .line 55
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/client/m;->n:Ljava/lang/ref/WeakReference;

    .line 56
    invoke-virtual {p1}, Lcom/twitter/app/common/base/BaseFragment;->getTag()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/m;->m:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/library/client/m;->m:Ljava/lang/String;

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/twitter/library/client/m;->d:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 94
    if-ne p0, p1, :cond_1

    .line 97
    :cond_0
    :goto_0
    return v0

    .line 95
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 96
    :cond_3
    check-cast p1, Lcom/twitter/library/client/m;

    .line 97
    iget v2, p0, Lcom/twitter/library/client/m;->d:I

    iget v3, p1, Lcom/twitter/library/client/m;->d:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/twitter/library/client/m;->f:I

    iget v3, p1, Lcom/twitter/library/client/m;->f:I

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcom/twitter/library/client/m;->h:Z

    iget-boolean v3, p1, Lcom/twitter/library/client/m;->h:Z

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/twitter/library/client/m;->i:I

    iget v3, p1, Lcom/twitter/library/client/m;->i:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/twitter/library/client/m;->j:I

    iget v3, p1, Lcom/twitter/library/client/m;->j:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/twitter/library/client/m;->a:Landroid/net/Uri;

    iget-object v3, p1, Lcom/twitter/library/client/m;->a:Landroid/net/Uri;

    .line 102
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/client/m;->b:Ljava/lang/Class;

    iget-object v3, p1, Lcom/twitter/library/client/m;->b:Ljava/lang/Class;

    .line 103
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/client/m;->c:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/twitter/library/client/m;->c:Ljava/lang/CharSequence;

    .line 104
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/client/m;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/client/m;->e:Ljava/lang/String;

    .line 105
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/client/m;->l:Lcom/twitter/app/common/base/b;

    iget-object v3, p1, Lcom/twitter/library/client/m;->l:Lcom/twitter/app/common/base/b;

    .line 106
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/client/m;->m:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/client/m;->m:Ljava/lang/String;

    .line 107
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/client/m;->n:Ljava/lang/ref/WeakReference;

    iget-object v3, p1, Lcom/twitter/library/client/m;->n:Ljava/lang/ref/WeakReference;

    .line 108
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 113
    iget-object v0, p0, Lcom/twitter/library/client/m;->a:Landroid/net/Uri;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/library/client/m;->b:Ljava/lang/Class;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/library/client/m;->c:Ljava/lang/CharSequence;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/twitter/library/client/m;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/twitter/library/client/m;->e:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget v3, p0, Lcom/twitter/library/client/m;->f:I

    .line 114
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/twitter/library/client/m;->h:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget v3, p0, Lcom/twitter/library/client/m;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/twitter/library/client/m;->l:Lcom/twitter/app/common/base/b;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-object v3, p0, Lcom/twitter/library/client/m;->m:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    iget-object v3, p0, Lcom/twitter/library/client/m;->n:Ljava/lang/ref/WeakReference;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    iget v3, p0, Lcom/twitter/library/client/m;->j:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 113
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;[Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
