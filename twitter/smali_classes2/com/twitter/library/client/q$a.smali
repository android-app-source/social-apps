.class Lcom/twitter/library/client/q$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/async/service/AsyncOperation$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/client/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "S:",
        "Lcom/twitter/async/service/AsyncOperation",
        "<TT;*>;>",
        "Ljava/lang/Object;",
        "Lcom/twitter/async/service/AsyncOperation$b",
        "<TT;TS;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/async/service/AsyncOperation$b",
            "<TT;TS;>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/client/q$a;->a:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/async/service/AsyncOperation$b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/AsyncOperation$b",
            "<TT;TS;>;)V"
        }
    .end annotation

    .prologue
    .line 201
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/client/q$a;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/twitter/library/client/q$a;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 204
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)V"
        }
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lcom/twitter/library/client/q$a;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/async/service/AsyncOperation$b;

    .line 191
    invoke-interface {v0, p1}, Lcom/twitter/async/service/AsyncOperation$b;->a(Lcom/twitter/async/service/AsyncOperation;)V

    goto :goto_0

    .line 193
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Object;Lcom/twitter/async/service/AsyncOperation;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TS;)V"
        }
    .end annotation

    .prologue
    .line 183
    iget-object v0, p0, Lcom/twitter/library/client/q$a;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/async/service/AsyncOperation$b;

    .line 184
    invoke-interface {v0, p1, p2}, Lcom/twitter/async/service/AsyncOperation$b;->a(Ljava/lang/Object;Lcom/twitter/async/service/AsyncOperation;)V

    goto :goto_0

    .line 186
    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/async/service/AsyncOperation$b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/AsyncOperation$b",
            "<TT;TS;>;)V"
        }
    .end annotation

    .prologue
    .line 212
    if-eqz p1, :cond_0

    .line 213
    iget-object v0, p0, Lcom/twitter/library/client/q$a;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 215
    :cond_0
    return-void
.end method

.method public final b(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)V"
        }
    .end annotation

    .prologue
    .line 176
    iget-object v0, p0, Lcom/twitter/library/client/q$a;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/async/service/AsyncOperation$b;

    .line 177
    invoke-interface {v0, p1}, Lcom/twitter/async/service/AsyncOperation$b;->b(Lcom/twitter/async/service/AsyncOperation;)V

    goto :goto_0

    .line 179
    :cond_0
    return-void
.end method
