.class public Lcom/twitter/library/client/c;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/client/c$a;
    }
.end annotation


# static fields
.field private static a:Lcom/twitter/library/client/c;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/twitter/library/client/v;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/twitter/library/client/c;->b:Landroid/content/Context;

    .line 56
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/c;->c:Lcom/twitter/library/client/v;

    .line 57
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/client/c;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/library/client/c;->b:Landroid/content/Context;

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/library/client/c;
    .locals 3

    .prologue
    .line 42
    const-class v1, Lcom/twitter/library/client/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/client/c;->a:Lcom/twitter/library/client/c;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lcom/twitter/library/client/c;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/library/client/c;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/library/client/c;->a:Lcom/twitter/library/client/c;

    .line 44
    const-class v0, Lcom/twitter/library/client/c;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 46
    :cond_0
    sget-object v0, Lcom/twitter/library/client/c;->a:Lcom/twitter/library/client/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 42
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(J)V
    .locals 5

    .prologue
    .line 99
    new-instance v0, Lcom/twitter/util/a;

    iget-object v1, p0, Lcom/twitter/library/client/c;->b:Landroid/content/Context;

    invoke-direct {v0, v1, p1, p2}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 100
    const-string/jumbo v1, "app_graph_timestamp"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/util/a;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/util/aa;->d(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    :goto_0
    return-void

    .line 103
    :cond_0
    new-instance v0, Lcom/twitter/library/client/c$1;

    invoke-direct {v0, p0}, Lcom/twitter/library/client/c$1;-><init>(Lcom/twitter/library/client/c;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 116
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/c$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/library/client/c;J)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/client/c;->a(J)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/library/client/c;)Lcom/twitter/library/client/v;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/library/client/c;->c:Lcom/twitter/library/client/v;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 60
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 61
    new-instance v1, Lcom/twitter/util/a;

    iget-object v2, p0, Lcom/twitter/library/client/c;->b:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v1, v2, v4, v5}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 62
    const-string/jumbo v2, "app_graph_status"

    invoke-virtual {v1, v2}, Lcom/twitter/util/a;->contains(Ljava/lang/String;)Z

    move-result v2

    .line 63
    const-string/jumbo v3, "app_graph_status"

    const-string/jumbo v4, "undetermined"

    invoke-virtual {v1, v3, v4}, Lcom/twitter/util/a;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 65
    sget-object v4, Lcom/twitter/library/client/b;->a:Lcom/twitter/library/client/b;

    invoke-virtual {v4}, Lcom/twitter/library/client/b;->a()Lcom/twitter/library/api/c;

    move-result-object v4

    .line 66
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "app_graph_enabled"

    .line 67
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v2, :cond_0

    const-string/jumbo v0, "optin"

    .line 68
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "app_graph_timestamp"

    const-wide/16 v2, 0x0

    .line 70
    invoke-virtual {v1, v0, v2, v3}, Lcom/twitter/util/a;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 69
    invoke-static {v0, v1}, Lcom/twitter/util/aa;->d(J)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    if-eqz v4, :cond_1

    .line 71
    invoke-virtual {v4}, Lcom/twitter/library/api/c;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 72
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/client/c;->a(Ljava/lang/String;)V

    .line 74
    :cond_2
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 77
    iget-object v0, p0, Lcom/twitter/library/client/c;->c:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 78
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    new-instance v2, Lbhj;

    iget-object v3, p0, Lcom/twitter/library/client/c;->b:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lbhj;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    new-instance v0, Lcom/twitter/library/client/c$a;

    invoke-direct {v0, p0, v4}, Lcom/twitter/library/client/c$a;-><init>(Lcom/twitter/library/client/c;Lcom/twitter/library/client/c$1;)V

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 87
    :goto_0
    return-void

    .line 83
    :cond_0
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    new-instance v2, Lbhk;

    iget-object v3, p0, Lcom/twitter/library/client/c;->b:Landroid/content/Context;

    invoke-direct {v2, v3, v0, p1}, Lbhk;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    new-instance v0, Lcom/twitter/library/client/c$a;

    invoke-direct {v0, p0, v4}, Lcom/twitter/library/client/c$a;-><init>(Lcom/twitter/library/client/c;Lcom/twitter/library/client/c$1;)V

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/library/client/c;->c:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 91
    new-instance v1, Lcom/twitter/util/a;

    iget-object v2, p0, Lcom/twitter/library/client/c;->b:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v1, v2, v4, v5}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 92
    invoke-virtual {v1}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "app_graph_status"

    .line 93
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    invoke-virtual {v0, v1, p1}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 96
    return-void

    .line 93
    :cond_0
    const-string/jumbo p1, "undetermined"

    goto :goto_0
.end method
