.class Lcom/twitter/library/client/v$a;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/client/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/client/v;


# direct methods
.method constructor <init>(Lcom/twitter/library/client/v;)V
    .locals 0

    .prologue
    .line 994
    iput-object p1, p0, Lcom/twitter/library/client/v$a;->a:Lcom/twitter/library/client/v;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    .line 995
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 993
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/v$a;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 999
    move-object v2, p1

    check-cast v2, Lbah;

    .line 1000
    invoke-virtual {v2}, Lbah;->h()[I

    move-result-object v4

    .line 1001
    invoke-virtual {v2}, Lbah;->t()Z

    move-result v7

    .line 1002
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    .line 1003
    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/twitter/library/service/u;

    .line 1004
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    .line 1005
    iget-object v1, p0, Lcom/twitter/library/client/v$a;->a:Lcom/twitter/library/client/v;

    invoke-static {v1}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/v;)Ljava/util/Map;

    move-result-object v1

    iget-object v0, v0, Lcom/twitter/library/service/v;->a:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/client/Session;

    .line 1006
    if-nez v1, :cond_1

    .line 1036
    :cond_0
    :goto_0
    return-void

    .line 1009
    :cond_1
    invoke-virtual {v3}, Lcom/twitter/library/service/u;->b()Z

    move-result v8

    .line 1010
    iget-object v0, p0, Lcom/twitter/library/client/v$a;->a:Lcom/twitter/library/client/v;

    .line 1011
    invoke-static {v0}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/v;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v9, v2, Lbah;->d:Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/v$b;

    .line 1012
    if-eqz v8, :cond_4

    .line 1014
    :try_start_0
    iget-object v8, p0, Lcom/twitter/library/client/v$a;->a:Lcom/twitter/library/client/v;

    .line 1015
    invoke-virtual {v2}, Lbah;->s()Lcom/twitter/model/account/LoginResponse;

    move-result-object v9

    .line 1016
    invoke-virtual {v2}, Lbah;->g()Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    .line 1014
    invoke-static {v8, v1, v9, v2}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/v;Lcom/twitter/library/client/Session;Lcom/twitter/model/account/LoginResponse;Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;

    move-result-object v8

    .line 1017
    if-eqz v0, :cond_0

    .line 1018
    if-nez v7, :cond_3

    move v2, v6

    :goto_1
    invoke-interface {v0, v1, v8, v2}, Lcom/twitter/library/client/v$b;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1020
    :catch_0
    move-exception v2

    .line 1021
    sget-object v2, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    .line 1022
    if-eqz v0, :cond_0

    .line 1023
    const/4 v2, 0x2

    .line 1025
    invoke-virtual {v3}, Lcom/twitter/library/service/u;->d()I

    move-result v3

    if-nez v7, :cond_2

    move v5, v6

    .line 1023
    :cond_2
    invoke-interface/range {v0 .. v5}, Lcom/twitter/library/client/v$b;->a(Lcom/twitter/library/client/Session;II[IZ)V

    goto :goto_0

    :cond_3
    move v2, v5

    .line 1018
    goto :goto_1

    .line 1029
    :cond_4
    sget-object v2, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    .line 1030
    if-eqz v0, :cond_0

    .line 1032
    invoke-virtual {v3}, Lcom/twitter/library/service/u;->d()I

    move-result v3

    if-nez v7, :cond_5

    move v5, v6

    :cond_5
    move v2, v6

    .line 1031
    invoke-interface/range {v0 .. v5}, Lcom/twitter/library/client/v$b;->a(Lcom/twitter/library/client/Session;II[IZ)V

    goto :goto_0
.end method
