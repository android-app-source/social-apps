.class public Lcom/twitter/library/dm/d;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a()Z
    .locals 4

    .prologue
    .line 28
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    const-string/jumbo v2, "dm_longterm_holdback_android_4930"

    const-string/jumbo v3, "control"

    invoke-static {v0, v1, v2, v3}, Lcoi;->a(JLjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static a(Z)Z
    .locals 1

    .prologue
    .line 114
    invoke-static {}, Lcom/twitter/library/dm/d;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()Z
    .locals 4

    .prologue
    .line 40
    invoke-static {}, Lcom/twitter/library/dm/d;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    const-string/jumbo v2, "dm_dedup_groups_android_5358"

    const-string/jumbo v3, "enabled"

    invoke-static {v0, v1, v2, v3}, Lcoi;->a(JLjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 40
    :goto_0
    return v0

    .line 41
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c()Z
    .locals 1

    .prologue
    .line 65
    invoke-static {}, Lcom/twitter/library/dm/d;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "dm_better_mute_controls_enabled"

    .line 66
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 65
    :goto_0
    return v0

    .line 66
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d()I
    .locals 2

    .prologue
    .line 79
    const-string/jumbo v0, "dm_notification_mute_duration"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static e()Z
    .locals 1

    .prologue
    .line 91
    const-string/jumbo v0, "dm_cards_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static f()Z
    .locals 1

    .prologue
    .line 100
    const-string/jumbo v0, "dm_shruggie_shortcut_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static g()Z
    .locals 4

    .prologue
    .line 108
    invoke-static {}, Lcom/twitter/library/dm/d;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/twitter/library/dm/d;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    const-string/jumbo v2, "cards_in_dms_android_4938"

    const-string/jumbo v3, "enabled"

    invoke-static {v0, v1, v2, v3}, Lcoi;->a(JLjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 108
    :goto_0
    return v0

    .line 109
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h()Z
    .locals 1

    .prologue
    .line 118
    invoke-static {}, Lcom/twitter/library/dm/d;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i()Z
    .locals 1

    .prologue
    .line 128
    const-string/jumbo v0, "b2c_feedback_submitted_dm_event_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static j()Z
    .locals 1

    .prologue
    .line 135
    const-string/jumbo v0, "dm_auto_retry_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static k()I
    .locals 2

    .prologue
    .line 139
    const-string/jumbo v0, "dm_max_group_size"

    const/16 v1, 0x14

    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static l()Z
    .locals 1

    .prologue
    .line 143
    invoke-static {}, Lcom/twitter/library/dm/d;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m()Z
    .locals 4

    .prologue
    .line 147
    invoke-static {}, Lcom/twitter/library/dm/d;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    const-string/jumbo v2, "dm_standalone_stickers_android_5469"

    const-string/jumbo v3, "enabled"

    invoke-static {v0, v1, v2, v3}, Lcoi;->a(JLjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 147
    :goto_0
    return v0

    .line 148
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n()Z
    .locals 1

    .prologue
    .line 153
    invoke-static {}, Lcom/twitter/library/dm/d;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "dm_stickers_as_messages_tooltip_enabled"

    .line 154
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 153
    :goto_0
    return v0

    .line 154
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static o()Z
    .locals 1

    .prologue
    .line 158
    const-string/jumbo v0, "dm_stickers_as_messages_read_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static p()I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 162
    invoke-static {}, Lcob;->a()Lcob;

    move-result-object v1

    invoke-virtual {v1}, Lcob;->b()I

    move-result v1

    const/16 v2, 0x7dd

    if-lt v1, v2, :cond_0

    .line 163
    const-string/jumbo v1, "dm_videos_and_gifs_max_autoplay_items"

    invoke-static {v1, v0}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v0

    .line 166
    :cond_0
    return v0
.end method

.method public static q()Z
    .locals 1

    .prologue
    .line 171
    invoke-static {}, Lcom/twitter/library/dm/d;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "dm_card_preview_android_enabled"

    .line 172
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    invoke-static {}, Lcom/twitter/library/dm/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 171
    :goto_0
    return v0

    .line 173
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static r()Z
    .locals 1

    .prologue
    .line 177
    invoke-static {}, Lcom/twitter/library/dm/d;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s()Z
    .locals 1

    .prologue
    .line 185
    const-string/jumbo v0, "b2c_add_welcome_message_request_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static t()Z
    .locals 1

    .prologue
    .line 193
    const-string/jumbo v0, "dm_quick_reply_options_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static u()Z
    .locals 1

    .prologue
    .line 197
    const-string/jumbo v0, "dm_quick_reply_text_input_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static v()Z
    .locals 1

    .prologue
    .line 205
    const-string/jumbo v0, "dm_photos_alt_text_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static w()Z
    .locals 1

    .prologue
    .line 209
    const-string/jumbo v0, "dm_quick_share_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static x()Z
    .locals 1

    .prologue
    .line 217
    const-string/jumbo v0, "dm_quick_reply_location_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static y()Z
    .locals 1

    .prologue
    .line 241
    const-string/jumbo v0, "dm_location_sharing_overflow_menu_button_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    invoke-static {}, Lcof;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 241
    :goto_0
    return v0

    .line 242
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
