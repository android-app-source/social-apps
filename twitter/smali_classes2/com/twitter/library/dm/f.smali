.class public Lcom/twitter/library/dm/f;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/dm/f$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field private final b:Landroid/content/res/Resources;

.field private final c:J

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Z

.field private final g:Z

.field private final h:I

.field private final i:I

.field private final j:Z

.field private final k:Z

.field private final l:Z

.field private final m:Lcci;

.field private final n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-string/jumbo v0, "\\n+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/dm/f;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/dm/f$a;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {p1}, Lcom/twitter/library/dm/f$a;->a(Lcom/twitter/library/dm/f$a;)Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    .line 45
    invoke-static {p1}, Lcom/twitter/library/dm/f$a;->b(Lcom/twitter/library/dm/f$a;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/library/dm/f;->c:J

    .line 46
    invoke-static {p1}, Lcom/twitter/library/dm/f$a;->c(Lcom/twitter/library/dm/f$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    .line 47
    invoke-static {p1}, Lcom/twitter/library/dm/f$a;->d(Lcom/twitter/library/dm/f$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    .line 48
    invoke-static {p1}, Lcom/twitter/library/dm/f$a;->e(Lcom/twitter/library/dm/f$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/dm/f;->f:Z

    .line 49
    invoke-static {p1}, Lcom/twitter/library/dm/f$a;->f(Lcom/twitter/library/dm/f$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/dm/f;->i:I

    .line 50
    iget-object v0, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->c(Ljava/lang/CharSequence;)I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/dm/f;->g:Z

    .line 51
    invoke-static {p1}, Lcom/twitter/library/dm/f$a;->g(Lcom/twitter/library/dm/f$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/dm/f;->h:I

    .line 52
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/twitter/library/dm/f;->j:Z

    .line 53
    invoke-static {p1}, Lcom/twitter/library/dm/f$a;->h(Lcom/twitter/library/dm/f$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/dm/f;->k:Z

    .line 54
    invoke-static {p1}, Lcom/twitter/library/dm/f$a;->i(Lcom/twitter/library/dm/f$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/dm/f;->l:Z

    .line 55
    invoke-static {p1}, Lcom/twitter/library/dm/f$a;->j(Lcom/twitter/library/dm/f$a;)Lcci;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/dm/f;->m:Lcci;

    .line 56
    invoke-static {p1}, Lcom/twitter/library/dm/f$a;->k(Lcom/twitter/library/dm/f$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/dm/f;->n:I

    .line 57
    return-void

    :cond_0
    move v0, v2

    .line 50
    goto :goto_0

    :cond_1
    move v1, v2

    .line 52
    goto :goto_1
.end method

.method private a(Z)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 179
    if-eqz p1, :cond_0

    .line 180
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_user_sent_a_video:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 182
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_sent_a_video:I

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private b()Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 103
    sget-object v0, Lcom/twitter/library/dm/f;->a:Ljava/util/regex/Pattern;

    iget-object v1, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 104
    invoke-direct {p0}, Lcom/twitter/library/dm/f;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v2, Lazw$k;->dm_user_conversation_preview:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private b(Z)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 223
    if-eqz p1, :cond_0

    .line 224
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_user_sent_a_gif:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 226
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_sent_a_gif:I

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private c(Z)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 250
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->k:Z

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_you_sent_a_photo:I

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 256
    :goto_0
    return-object v0

    .line 253
    :cond_0
    if-eqz p1, :cond_1

    .line 254
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_user_sent_a_photo:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 256
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_sent_a_photo:I

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->j:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->k:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Ljava/lang/CharSequence;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 114
    iget-object v0, p0, Lcom/twitter/library/dm/f;->m:Lcci;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcci;

    iget-object v0, v0, Lcci;->d:Lcom/twitter/model/core/r;

    iget-object v3, v0, Lcom/twitter/model/core/r;->d:Ljava/lang/String;

    .line 115
    iget-wide v4, p0, Lcom/twitter/library/dm/f;->c:J

    iget-object v0, p0, Lcom/twitter/library/dm/f;->m:Lcci;

    iget-object v0, v0, Lcci;->d:Lcom/twitter/model/core/r;

    iget-wide v6, v0, Lcom/twitter/model/core/r;->b:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    move v0, v1

    .line 116
    :goto_0
    iget-boolean v4, p0, Lcom/twitter/library/dm/f;->g:Z

    if-eqz v4, :cond_4

    .line 117
    iget-boolean v4, p0, Lcom/twitter/library/dm/f;->k:Z

    if-eqz v4, :cond_2

    .line 118
    if-eqz v0, :cond_1

    .line 119
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v3, Lazw$k;->dm_you_shared_your_own_tweet_with_message:I

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 148
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 115
    goto :goto_0

    .line 122
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v4, Lazw$k;->dm_you_shared_someones_tweet_with_message:I

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v3, v5, v2

    iget-object v2, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    aput-object v2, v5, v1

    invoke-static {v0, v4, v5}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    .line 126
    :cond_2
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->f:Z

    if-eqz v0, :cond_3

    .line 127
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v4, Lazw$k;->dm_shared_someones_tweet_in_a_group_with_message:I

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    aput-object v6, v5, v2

    aput-object v3, v5, v1

    iget-object v1, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    aput-object v1, v5, v8

    invoke-static {v0, v4, v5}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    .line 131
    :cond_3
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v4, Lazw$k;->dm_shared_someones_tweet_with_message:I

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v3, v5, v2

    iget-object v2, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    aput-object v2, v5, v1

    invoke-static {v0, v4, v5}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    .line 136
    :cond_4
    iget-boolean v4, p0, Lcom/twitter/library/dm/f;->k:Z

    if-eqz v4, :cond_6

    .line 137
    if-eqz v0, :cond_5

    .line 138
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_you_shared_your_own_tweet:I

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    .line 140
    :cond_5
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v4, Lazw$k;->dm_you_shared_someones_tweet:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v2

    invoke-static {v0, v4, v1}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    .line 144
    :cond_6
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->f:Z

    if-eqz v0, :cond_7

    .line 145
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v4, Lazw$k;->dm_shared_someones_tweet_in_a_group:I

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    aput-object v6, v5, v2

    aput-object v3, v5, v1

    invoke-static {v0, v4, v5}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    .line 148
    :cond_7
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v4, Lazw$k;->dm_shared_someones_tweet:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v2

    invoke-static {v0, v4, v1}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1
.end method

.method private d(Z)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 289
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->k:Z

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_you_sent_a_card:I

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 295
    :goto_0
    return-object v0

    .line 292
    :cond_0
    if-eqz p1, :cond_1

    .line 293
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_user_sent_a_card:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 295
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_sent_a_card:I

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private e()Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 157
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->k:Z

    if-eqz v0, :cond_1

    .line 158
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->g:Z

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_you_sent_a_video_with_message:I

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 172
    :goto_0
    return-object v0

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_you_sent_a_video:I

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 164
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->g:Z

    if-eqz v0, :cond_3

    .line 165
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->j:Z

    if-eqz v0, :cond_2

    .line 166
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_user_sent_a_video_with_message:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    aput-object v3, v2, v5

    .line 167
    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 169
    :cond_2
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_sent_a_video_with_message:I

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 172
    :cond_3
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->j:Z

    invoke-direct {p0, v0}, Lcom/twitter/library/dm/f;->a(Z)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private e(Z)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 318
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->k:Z

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_you_sent_a_moment:I

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 323
    :goto_0
    return-object v0

    .line 320
    :cond_0
    if-eqz p1, :cond_1

    .line 321
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_user_sent_a_moment:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 323
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_sent_a_moment:I

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private f()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 188
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->k:Z

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_you_sent_a_sticker:I

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 194
    :goto_0
    return-object v0

    .line 191
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->j:Z

    if-eqz v0, :cond_1

    .line 192
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_user_sent_a_sticker:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_sent_a_sticker:I

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private g()Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 201
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->k:Z

    if-eqz v0, :cond_1

    .line 202
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->g:Z

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_you_sent_a_gif_with_message:I

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 216
    :goto_0
    return-object v0

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_you_sent_a_gif:I

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 208
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->g:Z

    if-eqz v0, :cond_3

    .line 209
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->j:Z

    if-eqz v0, :cond_2

    .line 210
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_user_sent_a_gif_with_message:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    aput-object v3, v2, v5

    .line 211
    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 213
    :cond_2
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_sent_a_gif_with_message:I

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 216
    :cond_3
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->j:Z

    invoke-direct {p0, v0}, Lcom/twitter/library/dm/f;->b(Z)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private h()Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 232
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->g:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->k:Z

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_you_sent_a_photo_with_message:I

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 243
    :goto_0
    return-object v0

    .line 235
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->g:Z

    if-eqz v0, :cond_2

    .line 236
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->j:Z

    if-eqz v0, :cond_1

    .line 237
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_user_sent_a_photo_with_message:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    aput-object v3, v2, v5

    .line 238
    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 240
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_sent_a_photo_with_message:I

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 243
    :cond_2
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->j:Z

    invoke-direct {p0, v0}, Lcom/twitter/library/dm/f;->c(Z)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private i()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 263
    iget v0, p0, Lcom/twitter/library/dm/f;->n:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 264
    invoke-direct {p0}, Lcom/twitter/library/dm/f;->k()Ljava/lang/CharSequence;

    move-result-object v0

    .line 266
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/twitter/library/dm/f;->j()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private j()Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 271
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->g:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->k:Z

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_you_sent_a_card_with_message:I

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 282
    :goto_0
    return-object v0

    .line 274
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->g:Z

    if-eqz v0, :cond_2

    .line 275
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->j:Z

    if-eqz v0, :cond_1

    .line 276
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_user_sent_a_card_with_message:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    aput-object v3, v2, v5

    .line 277
    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 279
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_sent_a_card_with_message:I

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 282
    :cond_2
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->j:Z

    invoke-direct {p0, v0}, Lcom/twitter/library/dm/f;->d(Z)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private k()Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 302
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->g:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->k:Z

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_you_sent_a_moment_with_message:I

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 312
    :goto_0
    return-object v0

    .line 304
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->g:Z

    if-eqz v0, :cond_2

    .line 305
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->j:Z

    if-eqz v0, :cond_1

    .line 306
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_user_sent_a_moment_with_message:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    aput-object v3, v2, v5

    .line 307
    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 309
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_sent_a_moment_with_message:I

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 312
    :cond_2
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->j:Z

    invoke-direct {p0, v0}, Lcom/twitter/library/dm/f;->e(Z)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private l()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 329
    iget-object v0, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_italicized_added_you:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 330
    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private m()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 335
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_italicized_cs_feedback_submitted_text:I

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private n()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 340
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_italicized_cs_feedback_dismissed_text:I

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private o()Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 345
    iget-object v0, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    .line 346
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 347
    iget-object v0, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_italicized_group_name_removed:I

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 360
    :goto_0
    return-object v0

    .line 351
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->k:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_italicized_you_removed_group_name:I

    new-array v2, v5, [Ljava/lang/Object;

    .line 352
    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v1, Lazw$k;->dm_italicized_user_removed_group_name:I

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    aput-object v3, v2, v5

    .line 353
    invoke-static {v0, v1, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 356
    :cond_2
    iget-object v1, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 357
    iget-object v1, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v2, Lazw$k;->dm_italicized_group_name_changed:I

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v1, v2, v3}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 360
    :cond_3
    iget-boolean v1, p0, Lcom/twitter/library/dm/f;->k:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v2, Lazw$k;->dm_italicized_you_changed_group_name:I

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    .line 361
    invoke-static {v1, v2, v3}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v2, Lazw$k;->dm_italicized_user_changed_group_name:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    aput-object v4, v3, v5

    aput-object v0, v3, v6

    .line 362
    invoke-static {v1, v2, v3}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private p()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 368
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->k:Z

    if-eqz v0, :cond_1

    .line 369
    iget-object v1, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->l:Z

    if-eqz v0, :cond_0

    sget v0, Lazw$k;->dm_italicized_you_removed_group_photo:I

    :goto_0
    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 373
    :goto_1
    return-object v0

    .line 369
    :cond_0
    sget v0, Lazw$k;->dm_italicized_you_updated_group_photo:I

    goto :goto_0

    .line 373
    :cond_1
    iget-object v1, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->l:Z

    if-eqz v0, :cond_2

    sget v0, Lazw$k;->dm_italicized_user_removed_group_photo:I

    :goto_2
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v0, v2}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    :cond_2
    sget v0, Lazw$k;->dm_italicized_user_updated_group_photo:I

    goto :goto_2
.end method

.method private q()Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 381
    iget-object v0, p0, Lcom/twitter/library/dm/f;->e:Ljava/lang/String;

    .line 382
    iget-object v1, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 383
    iget-object v1, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v2, Lazw$k;->dm_italicized_participant_added_by_deleted_user:I

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v1, v2, v3}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 388
    :goto_0
    return-object v0

    .line 385
    :cond_0
    iget-boolean v1, p0, Lcom/twitter/library/dm/f;->k:Z

    if-eqz v1, :cond_1

    .line 386
    iget-object v1, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v2, Lazw$k;->dm_italicized_participant_added_by_you:I

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v1, v2, v3}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 388
    :cond_1
    iget-object v1, p0, Lcom/twitter/library/dm/f;->b:Landroid/content/res/Resources;

    sget v2, Lazw$k;->dm_italicized_participant_added_by_user:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/library/dm/f;->d:Ljava/lang/String;

    aput-object v4, v3, v5

    aput-object v0, v3, v6

    invoke-static {v1, v2, v3}, Lcom/twitter/util/d;->a(Landroid/content/res/Resources;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 61
    iget v0, p0, Lcom/twitter/library/dm/f;->i:I

    sparse-switch v0, :sswitch_data_0

    .line 81
    iget v0, p0, Lcom/twitter/library/dm/f;->h:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 82
    invoke-direct {p0}, Lcom/twitter/library/dm/f;->i()Ljava/lang/CharSequence;

    move-result-object v0

    .line 96
    :goto_0
    return-object v0

    .line 63
    :sswitch_0
    invoke-direct {p0}, Lcom/twitter/library/dm/f;->o()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 66
    :sswitch_1
    invoke-direct {p0}, Lcom/twitter/library/dm/f;->p()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 69
    :sswitch_2
    invoke-direct {p0}, Lcom/twitter/library/dm/f;->q()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 72
    :sswitch_3
    invoke-direct {p0}, Lcom/twitter/library/dm/f;->m()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 75
    :sswitch_4
    invoke-direct {p0}, Lcom/twitter/library/dm/f;->n()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 78
    :sswitch_5
    invoke-direct {p0}, Lcom/twitter/library/dm/f;->l()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 83
    :cond_0
    iget v0, p0, Lcom/twitter/library/dm/f;->h:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 84
    invoke-direct {p0}, Lcom/twitter/library/dm/f;->d()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 85
    :cond_1
    iget v0, p0, Lcom/twitter/library/dm/f;->h:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 86
    invoke-direct {p0}, Lcom/twitter/library/dm/f;->h()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 87
    :cond_2
    iget v0, p0, Lcom/twitter/library/dm/f;->h:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 88
    invoke-direct {p0}, Lcom/twitter/library/dm/f;->e()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 89
    :cond_3
    iget v0, p0, Lcom/twitter/library/dm/f;->h:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 90
    invoke-direct {p0}, Lcom/twitter/library/dm/f;->g()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 91
    :cond_4
    iget v0, p0, Lcom/twitter/library/dm/f;->h:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_5

    .line 92
    invoke-direct {p0}, Lcom/twitter/library/dm/f;->f()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 93
    :cond_5
    iget-boolean v0, p0, Lcom/twitter/library/dm/f;->g:Z

    if-eqz v0, :cond_6

    .line 94
    invoke-direct {p0}, Lcom/twitter/library/dm/f;->b()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 96
    :cond_6
    const-string/jumbo v0, ""

    goto :goto_0

    .line 61
    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0xa -> :sswitch_2
        0x11 -> :sswitch_5
        0x14 -> :sswitch_1
        0x15 -> :sswitch_3
        0x16 -> :sswitch_4
    .end sparse-switch
.end method
