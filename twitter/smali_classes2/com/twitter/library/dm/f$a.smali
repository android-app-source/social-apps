.class public final Lcom/twitter/library/dm/f$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/dm/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/library/dm/f;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/res/Resources;

.field private b:J

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:I

.field private g:I

.field private h:Z

.field private i:Z

.field private j:Lcci;

.field private k:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 404
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 408
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/library/dm/f$a;->d:Ljava/lang/String;

    .line 415
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/dm/f$a;->k:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/dm/f$a;)Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/twitter/library/dm/f$a;->a:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/library/dm/f$a;)J
    .locals 2

    .prologue
    .line 404
    iget-wide v0, p0, Lcom/twitter/library/dm/f$a;->b:J

    return-wide v0
.end method

.method static synthetic c(Lcom/twitter/library/dm/f$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/twitter/library/dm/f$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/library/dm/f$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/twitter/library/dm/f$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/library/dm/f$a;)Z
    .locals 1

    .prologue
    .line 404
    iget-boolean v0, p0, Lcom/twitter/library/dm/f$a;->e:Z

    return v0
.end method

.method static synthetic f(Lcom/twitter/library/dm/f$a;)I
    .locals 1

    .prologue
    .line 404
    iget v0, p0, Lcom/twitter/library/dm/f$a;->f:I

    return v0
.end method

.method static synthetic g(Lcom/twitter/library/dm/f$a;)I
    .locals 1

    .prologue
    .line 404
    iget v0, p0, Lcom/twitter/library/dm/f$a;->g:I

    return v0
.end method

.method static synthetic h(Lcom/twitter/library/dm/f$a;)Z
    .locals 1

    .prologue
    .line 404
    iget-boolean v0, p0, Lcom/twitter/library/dm/f$a;->h:Z

    return v0
.end method

.method static synthetic i(Lcom/twitter/library/dm/f$a;)Z
    .locals 1

    .prologue
    .line 404
    iget-boolean v0, p0, Lcom/twitter/library/dm/f$a;->i:Z

    return v0
.end method

.method static synthetic j(Lcom/twitter/library/dm/f$a;)Lcci;
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/twitter/library/dm/f$a;->j:Lcci;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/library/dm/f$a;)I
    .locals 1

    .prologue
    .line 404
    iget v0, p0, Lcom/twitter/library/dm/f$a;->k:I

    return v0
.end method


# virtual methods
.method public R_()Z
    .locals 2

    .prologue
    .line 485
    iget-object v0, p0, Lcom/twitter/library/dm/f$a;->a:Landroid/content/res/Resources;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/library/dm/f$a;->g:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/dm/f$a;->j:Lcci;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Lcom/twitter/library/dm/f$a;
    .locals 0

    .prologue
    .line 437
    iput p1, p0, Lcom/twitter/library/dm/f$a;->k:I

    .line 438
    return-object p0
.end method

.method public a(J)Lcom/twitter/library/dm/f$a;
    .locals 1

    .prologue
    .line 473
    iput-wide p1, p0, Lcom/twitter/library/dm/f$a;->b:J

    .line 474
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/dm/f$a;

    return-object v0
.end method

.method public a(Landroid/content/res/Resources;)Lcom/twitter/library/dm/f$a;
    .locals 1

    .prologue
    .line 479
    iput-object p1, p0, Lcom/twitter/library/dm/f$a;->a:Landroid/content/res/Resources;

    .line 480
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/dm/f$a;

    return-object v0
.end method

.method public a(Lcci;)Lcom/twitter/library/dm/f$a;
    .locals 0

    .prologue
    .line 431
    iput-object p1, p0, Lcom/twitter/library/dm/f$a;->j:Lcci;

    .line 432
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/dm/f$a;
    .locals 1

    .prologue
    .line 461
    iput-object p1, p0, Lcom/twitter/library/dm/f$a;->d:Ljava/lang/String;

    .line 462
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/dm/f$a;

    return-object v0
.end method

.method public a(Z)Lcom/twitter/library/dm/f$a;
    .locals 0

    .prologue
    .line 419
    iput-boolean p1, p0, Lcom/twitter/library/dm/f$a;->h:Z

    .line 420
    return-object p0
.end method

.method public b(I)Lcom/twitter/library/dm/f$a;
    .locals 1

    .prologue
    .line 443
    iput p1, p0, Lcom/twitter/library/dm/f$a;->g:I

    .line 444
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/dm/f$a;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/library/dm/f$a;
    .locals 1

    .prologue
    .line 467
    iput-object p1, p0, Lcom/twitter/library/dm/f$a;->c:Ljava/lang/String;

    .line 468
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/dm/f$a;

    return-object v0
.end method

.method public b(Z)Lcom/twitter/library/dm/f$a;
    .locals 0

    .prologue
    .line 425
    iput-boolean p1, p0, Lcom/twitter/library/dm/f$a;->i:Z

    .line 426
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 404
    invoke-virtual {p0}, Lcom/twitter/library/dm/f$a;->e()Lcom/twitter/library/dm/f;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lcom/twitter/library/dm/f$a;
    .locals 1

    .prologue
    .line 449
    iput p1, p0, Lcom/twitter/library/dm/f$a;->f:I

    .line 450
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/dm/f$a;

    return-object v0
.end method

.method public c(Z)Lcom/twitter/library/dm/f$a;
    .locals 1

    .prologue
    .line 455
    iput-boolean p1, p0, Lcom/twitter/library/dm/f$a;->e:Z

    .line 456
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/dm/f$a;

    return-object v0
.end method

.method protected e()Lcom/twitter/library/dm/f;
    .locals 1

    .prologue
    .line 491
    new-instance v0, Lcom/twitter/library/dm/f;

    invoke-direct {v0, p0}, Lcom/twitter/library/dm/f;-><init>(Lcom/twitter/library/dm/f$a;)V

    return-object v0
.end method
