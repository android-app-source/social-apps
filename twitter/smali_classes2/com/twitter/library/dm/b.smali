.class public Lcom/twitter/library/dm/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/object/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/object/j",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/model/dms/q;

.field private final b:Landroid/content/Context;

.field private final c:J


# direct methods
.method public constructor <init>(Lcom/twitter/model/dms/q;Landroid/content/Context;J)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/twitter/library/dm/b;->a:Lcom/twitter/model/dms/q;

    .line 28
    iput-object p2, p0, Lcom/twitter/library/dm/b;->b:Landroid/content/Context;

    .line 29
    iput-wide p3, p0, Lcom/twitter/library/dm/b;->c:J

    .line 30
    return-void
.end method

.method private static a(Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 51
    iget-object v0, p0, Lcom/twitter/library/dm/b;->a:Lcom/twitter/model/dms/q;

    iget-object v0, v0, Lcom/twitter/model/dms/q;->i:Ljava/util/List;

    iget-wide v2, p0, Lcom/twitter/library/dm/b;->c:J

    .line 52
    invoke-static {v0, v2, v3}, Lcom/twitter/library/dm/e;->a(Ljava/util/List;J)Ljava/util/List;

    move-result-object v1

    .line 53
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 54
    packed-switch v2, :pswitch_data_0

    .line 73
    iget-object v3, p0, Lcom/twitter/library/dm/b;->b:Landroid/content/Context;

    sget v4, Lazw$k;->dm_conversation_title_many:I

    new-array v5, v5, [Ljava/lang/Object;

    .line 74
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    invoke-static {v0}, Lcom/twitter/library/dm/b;->a(Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    add-int/lit8 v0, v2, -0x1

    .line 75
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v7

    .line 73
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 56
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/library/dm/b;->b:Landroid/content/Context;

    sget v1, Lazw$k;->media_tag_you:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 59
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/library/dm/b;->a:Lcom/twitter/model/dms/q;

    iget-boolean v0, v0, Lcom/twitter/model/dms/q;->h:Z

    if-eqz v0, :cond_0

    .line 60
    iget-object v2, p0, Lcom/twitter/library/dm/b;->b:Landroid/content/Context;

    sget v3, Lazw$k;->dm_conversation_title_two:I

    new-array v4, v7, [Ljava/lang/Object;

    .line 61
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    invoke-static {v0}, Lcom/twitter/library/dm/b;->a(Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    .line 60
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 63
    :cond_0
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    invoke-static {v0}, Lcom/twitter/library/dm/b;->a(Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 67
    :pswitch_2
    iget-object v2, p0, Lcom/twitter/library/dm/b;->b:Landroid/content/Context;

    sget v3, Lazw$k;->dm_conversation_title_three:I

    new-array v4, v5, [Ljava/lang/Object;

    .line 68
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    invoke-static {v0}, Lcom/twitter/library/dm/b;->a(Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    .line 69
    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    invoke-static {v0}, Lcom/twitter/library/dm/b;->a(Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .line 67
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 54
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/library/dm/b;->a:Lcom/twitter/model/dms/q;

    iget-object v0, v0, Lcom/twitter/model/dms/q;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/dm/b;->a:Lcom/twitter/model/dms/q;

    iget-object v0, v0, Lcom/twitter/model/dms/q;->c:Ljava/lang/String;

    :goto_0
    return-object v0

    .line 37
    :cond_0
    invoke-direct {p0}, Lcom/twitter/library/dm/b;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/twitter/library/dm/b;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
