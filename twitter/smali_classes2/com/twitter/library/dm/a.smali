.class public Lcom/twitter/library/dm/a;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/database/schema/TwitterSchema;

.field private final b:Landroid/database/sqlite/SQLiteDatabase;

.field private final c:Z


# direct methods
.method public constructor <init>(Lcom/twitter/database/schema/TwitterSchema;Landroid/database/sqlite/SQLiteDatabase;Z)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/twitter/library/dm/a;->a:Lcom/twitter/database/schema/TwitterSchema;

    .line 29
    iput-object p2, p0, Lcom/twitter/library/dm/a;->b:Landroid/database/sqlite/SQLiteDatabase;

    .line 30
    iput-boolean p3, p0, Lcom/twitter/library/dm/a;->c:Z

    .line 31
    return-void
.end method

.method private a(Lcom/twitter/model/dms/Participant;Lcom/twitter/model/dms/l;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 110
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 111
    const-string/jumbo v3, "conversation_id"

    iget-object v4, p2, Lcom/twitter/model/dms/l;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string/jumbo v3, "user_id"

    iget-wide v4, p1, Lcom/twitter/model/dms/Participant;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 113
    const-string/jumbo v3, "last_read_event_id"

    iget-wide v4, p1, Lcom/twitter/model/dms/Participant;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 114
    const-string/jumbo v3, "join_conversation_event_id"

    iget-wide v4, p1, Lcom/twitter/model/dms/Participant;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 115
    iget v3, p2, Lcom/twitter/model/dms/l;->b:I

    if-ne v1, v3, :cond_1

    .line 116
    const-string/jumbo v3, "join_time"

    iget-wide v4, p1, Lcom/twitter/model/dms/Participant;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 117
    const-string/jumbo v3, "participant_type"

    iget-wide v4, p1, Lcom/twitter/model/dms/Participant;->b:J

    iget-wide v6, p2, Lcom/twitter/model/dms/l;->c:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 118
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 117
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 126
    :goto_1
    iget-object v1, p0, Lcom/twitter/library/dm/a;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string/jumbo v3, "conversation_participants"

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/twitter/library/dm/a;->c:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x5

    :goto_2
    invoke-virtual {v1, v3, v4, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 128
    return-void

    :cond_0
    move v0, v1

    .line 117
    goto :goto_0

    .line 123
    :cond_1
    const-string/jumbo v3, "join_time"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 124
    const-string/jumbo v0, "participant_type"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 126
    :cond_2
    const/4 v0, 0x4

    goto :goto_2
.end method


# virtual methods
.method public a(Lcom/twitter/model/dms/l;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/library/dm/a;->a:Lcom/twitter/database/schema/TwitterSchema;

    invoke-static {v0}, Lcom/twitter/database/hydrator/c;->a(Lcom/twitter/database/model/i;)Lcom/twitter/database/hydrator/c;

    move-result-object v0

    const-class v1, Lawy;

    new-instance v2, Lcom/twitter/database/model/f$a;

    invoke-direct {v2}, Lcom/twitter/database/model/f$a;-><init>()V

    sget-object v3, Lawy;->a:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p1, Lcom/twitter/model/dms/l;->a:Ljava/lang/String;

    aput-object v6, v4, v5

    .line 57
    invoke-virtual {v2, v3, v4}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v2

    .line 58
    invoke-virtual {v2}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v2

    const-class v3, Lcom/twitter/model/dms/i;

    .line 54
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/database/hydrator/c;->a(Ljava/lang/Class;Lcom/twitter/database/model/f;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/i;

    .line 62
    if-eqz v0, :cond_2

    .line 64
    iget-wide v2, v0, Lcom/twitter/model/dms/i;->e:J

    iget-wide v4, p1, Lcom/twitter/model/dms/l;->h:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    .line 65
    iget-wide v2, v0, Lcom/twitter/model/dms/i;->h:J

    iget-wide v4, p1, Lcom/twitter/model/dms/l;->k:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 66
    iget-wide v2, v0, Lcom/twitter/model/dms/i;->g:J

    iget-wide v8, p1, Lcom/twitter/model/dms/l;->i:J

    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    .line 67
    iget-wide v2, v0, Lcom/twitter/model/dms/i;->f:J

    iget-wide v10, p1, Lcom/twitter/model/dms/l;->h:J

    invoke-static {v2, v3, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 68
    iget-object p2, v0, Lcom/twitter/model/dms/i;->o:Ljava/lang/String;

    move-wide v0, v2

    move-wide v2, v4

    move-wide v4, v6

    move-wide v6, v8

    .line 78
    :cond_0
    :goto_0
    iget-boolean v8, p0, Lcom/twitter/library/dm/a;->c:Z

    if-eqz v8, :cond_1

    .line 79
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-object v10, p1, Lcom/twitter/model/dms/l;->a:Ljava/lang/String;

    aput-object v10, v8, v9

    .line 80
    iget-object v9, p0, Lcom/twitter/library/dm/a;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string/jumbo v10, "conversation_participants"

    const-string/jumbo v11, "conversation_id=?"

    invoke-virtual {v9, v10, v11, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 82
    iget-object v9, p0, Lcom/twitter/library/dm/a;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string/jumbo v10, "conversations"

    sget-object v11, Lawy;->a:Ljava/lang/String;

    invoke-virtual {v9, v10, v11, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 85
    :cond_1
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 86
    const-string/jumbo v9, "conversation_id"

    iget-object v10, p1, Lcom/twitter/model/dms/l;->a:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const-string/jumbo v9, "type"

    iget v10, p1, Lcom/twitter/model/dms/l;->b:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 88
    const-string/jumbo v9, "title"

    iget-object v10, p1, Lcom/twitter/model/dms/l;->e:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string/jumbo v9, "avatar_url"

    iget-object v10, p1, Lcom/twitter/model/dms/l;->f:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string/jumbo v9, "is_muted"

    iget-boolean v10, p1, Lcom/twitter/model/dms/l;->g:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 91
    const-string/jumbo v9, "is_hidden"

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 92
    const-string/jumbo v9, "sort_event_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v8, v9, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 93
    const-string/jumbo v4, "last_read_event_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v8, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 94
    const-string/jumbo v4, "sort_timestamp"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v8, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 95
    const-string/jumbo v2, "last_readable_event_id"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v8, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 96
    const-string/jumbo v0, "min_event_id"

    iget-wide v2, p1, Lcom/twitter/model/dms/l;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 97
    const-string/jumbo v0, "has_more"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 98
    const-string/jumbo v0, "read_only"

    iget-boolean v1, p1, Lcom/twitter/model/dms/l;->l:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 99
    const-string/jumbo v0, "local_conversation_id"

    invoke-virtual {v8, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    iget-object v1, p0, Lcom/twitter/library/dm/a;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string/jumbo v2, "conversations"

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/twitter/library/dm/a;->c:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x5

    :goto_1
    invoke-virtual {v1, v2, v3, v8, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 103
    iget-object v0, p1, Lcom/twitter/model/dms/l;->d:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/Participant;

    .line 104
    invoke-direct {p0, v0, p1}, Lcom/twitter/library/dm/a;->a(Lcom/twitter/model/dms/Participant;Lcom/twitter/model/dms/l;)V

    goto :goto_2

    .line 70
    :cond_2
    iget-wide v6, p1, Lcom/twitter/model/dms/l;->i:J

    .line 71
    iget-wide v4, p1, Lcom/twitter/model/dms/l;->h:J

    .line 72
    iget-wide v2, p1, Lcom/twitter/model/dms/l;->k:J

    .line 73
    iget-wide v0, p1, Lcom/twitter/model/dms/l;->h:J

    .line 74
    iget-object v8, p1, Lcom/twitter/model/dms/l;->a:Ljava/lang/String;

    invoke-static {v8}, Lcom/twitter/library/dm/e;->c(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object p2, p1, Lcom/twitter/model/dms/l;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 100
    :cond_3
    const/4 v0, 0x4

    goto :goto_1

    .line 106
    :cond_4
    return-void
.end method
