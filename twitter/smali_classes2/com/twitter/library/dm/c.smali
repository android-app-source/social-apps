.class public Lcom/twitter/library/dm/c;
.super Lcom/twitter/library/service/j;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;[J)V
    .locals 8

    .prologue
    .line 27
    const-class v0, Lcom/twitter/library/dm/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/j;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 29
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    .line 30
    array-length v0, p4

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/twitter/util/collection/o;->a(I)Lcom/twitter/util/collection/o;

    move-result-object v1

    .line 31
    array-length v4, p4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    aget-wide v6, p4, v0

    .line 32
    new-instance v5, Lcom/twitter/model/dms/Participant$a;

    invoke-direct {v5}, Lcom/twitter/model/dms/Participant$a;-><init>()V

    .line 33
    invoke-virtual {v5, v6, v7}, Lcom/twitter/model/dms/Participant$a;->a(J)Lcom/twitter/model/dms/Participant$a;

    move-result-object v5

    .line 34
    invoke-virtual {v5, v2, v3}, Lcom/twitter/model/dms/Participant$a;->b(J)Lcom/twitter/model/dms/Participant$a;

    move-result-object v5

    .line 35
    invoke-virtual {v5, p3}, Lcom/twitter/model/dms/Participant$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/Participant$a;

    move-result-object v5

    .line 36
    invoke-virtual {v5}, Lcom/twitter/model/dms/Participant$a;->q()Ljava/lang/Object;

    move-result-object v5

    .line 32
    invoke-virtual {v1, v5}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 31
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 39
    :cond_0
    new-instance v0, Lcom/twitter/model/dms/Participant$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/Participant$a;-><init>()V

    .line 40
    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/dms/Participant$a;->a(J)Lcom/twitter/model/dms/Participant$a;

    move-result-object v0

    .line 41
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/dms/Participant$a;->b(J)Lcom/twitter/model/dms/Participant$a;

    move-result-object v0

    .line 42
    invoke-virtual {v0, p3}, Lcom/twitter/model/dms/Participant$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/Participant$a;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lcom/twitter/model/dms/Participant$a;->q()Ljava/lang/Object;

    move-result-object v0

    .line 39
    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 44
    invoke-virtual {v1}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Lcom/twitter/library/dm/c;->b:Ljava/util/Set;

    .line 46
    iput-object p3, p0, Lcom/twitter/library/dm/c;->a:Ljava/lang/String;

    .line 47
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/twitter/library/dm/c;->b()V

    .line 52
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 55
    iget-object v0, p0, Lcom/twitter/library/dm/c;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v2, 0x2

    if-le v0, v2, :cond_0

    const/4 v0, 0x1

    .line 57
    :goto_0
    new-instance v2, Lcom/twitter/model/dms/l$a;

    invoke-direct {v2}, Lcom/twitter/model/dms/l$a;-><init>()V

    .line 58
    invoke-virtual {v2, v0}, Lcom/twitter/model/dms/l$a;->a(I)Lcom/twitter/model/dms/l$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/library/dm/c;->b:Ljava/util/Set;

    .line 59
    invoke-virtual {v0, v2}, Lcom/twitter/model/dms/l$a;->a(Ljava/util/Collection;)Lcom/twitter/model/dms/l$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/library/dm/c;->a:Ljava/lang/String;

    .line 60
    invoke-virtual {v0, v2}, Lcom/twitter/model/dms/l$a;->c(Ljava/lang/String;)Lcom/twitter/model/dms/l$a;

    move-result-object v0

    const-wide/16 v2, 0x0

    .line 61
    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/dms/l$a;->c(J)Lcom/twitter/model/dms/l$a;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lcom/twitter/model/dms/l$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/l;

    .line 63
    invoke-virtual {p0}, Lcom/twitter/library/dm/c;->s()Lcom/twitter/library/provider/t;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/model/dms/l;Z)V

    .line 64
    return-void

    :cond_0
    move v0, v1

    .line 55
    goto :goto_0
.end method
