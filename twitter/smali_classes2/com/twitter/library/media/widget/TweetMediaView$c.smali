.class public Lcom/twitter/library/media/widget/TweetMediaView$c;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/media/widget/TweetMediaView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;

.field public final b:Lcom/twitter/util/math/Size;


# direct methods
.method public constructor <init>(Lcax;)V
    .locals 2

    .prologue
    .line 804
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 805
    iput-object p1, p0, Lcom/twitter/library/media/widget/TweetMediaView$c;->a:Ljava/lang/Object;

    .line 806
    invoke-virtual {p1}, Lcax;->q()Lcom/twitter/model/card/property/ImageSpec;

    move-result-object v0

    .line 807
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    iget v1, v1, Lcom/twitter/model/card/property/Vector2F;->x:F

    iget-object v0, v0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    iget v0, v0, Lcom/twitter/model/card/property/Vector2F;->y:F

    invoke-static {v1, v0}, Lcom/twitter/util/math/Size;->a(FF)Lcom/twitter/util/math/Size;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/twitter/library/media/widget/TweetMediaView$c;->b:Lcom/twitter/util/math/Size;

    .line 808
    return-void

    .line 807
    :cond_0
    sget-object v0, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/twitter/model/core/MediaEntity;)V
    .locals 1

    .prologue
    .line 799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 800
    iput-object p1, p0, Lcom/twitter/library/media/widget/TweetMediaView$c;->a:Ljava/lang/Object;

    .line 801
    iget-object v0, p1, Lcom/twitter/model/core/MediaEntity;->o:Lcom/twitter/util/math/Size;

    iput-object v0, p0, Lcom/twitter/library/media/widget/TweetMediaView$c;->b:Lcom/twitter/util/math/Size;

    .line 802
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/media/EditableMedia;)V
    .locals 1

    .prologue
    .line 794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 795
    iput-object p1, p0, Lcom/twitter/library/media/widget/TweetMediaView$c;->a:Ljava/lang/Object;

    .line 796
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->e()Lcom/twitter/util/math/Size;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/media/widget/TweetMediaView$c;->b:Lcom/twitter/util/math/Size;

    .line 797
    return-void
.end method

.method public static a(Lcax;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcax;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/media/widget/TweetMediaView$c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 882
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcax;->q()Lcom/twitter/model/card/property/ImageSpec;

    move-result-object v0

    if-nez v0, :cond_1

    .line 883
    :cond_0
    const/4 v0, 0x0

    .line 885
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/twitter/library/media/widget/TweetMediaView$c;

    invoke-direct {v0, p0}, Lcom/twitter/library/media/widget/TweetMediaView$c;-><init>(Lcax;)V

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/media/EditableMedia;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/media/widget/TweetMediaView$c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 856
    if-nez p0, :cond_0

    .line 857
    const/4 v0, 0x0

    .line 864
    :goto_0
    return-object v0

    .line 860
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 861
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableMedia;

    .line 862
    new-instance v3, Lcom/twitter/library/media/widget/TweetMediaView$c;

    invoke-direct {v3, v0}, Lcom/twitter/library/media/widget/TweetMediaView$c;-><init>(Lcom/twitter/model/media/EditableMedia;)V

    invoke-virtual {v1, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1

    .line 864
    :cond_1
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    goto :goto_0
.end method

.method public static b(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/MediaEntity;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/media/widget/TweetMediaView$c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 869
    if-nez p0, :cond_0

    .line 870
    const/4 v0, 0x0

    .line 877
    :goto_0
    return-object v0

    .line 873
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 874
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    .line 875
    new-instance v3, Lcom/twitter/library/media/widget/TweetMediaView$c;

    invoke-direct {v3, v0}, Lcom/twitter/library/media/widget/TweetMediaView$c;-><init>(Lcom/twitter/model/core/MediaEntity;)V

    invoke-virtual {v1, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1

    .line 877
    :cond_1
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;)Lcom/twitter/media/request/a$a;
    .locals 1

    .prologue
    .line 813
    iget-object v0, p0, Lcom/twitter/library/media/widget/TweetMediaView$c;->a:Ljava/lang/Object;

    instance-of v0, v0, Lcom/twitter/model/core/MediaEntity;

    if-eqz v0, :cond_0

    .line 814
    iget-object v0, p0, Lcom/twitter/library/media/widget/TweetMediaView$c;->a:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    invoke-static {v0}, Lcom/twitter/media/util/k;->a(Lcom/twitter/model/core/MediaEntity;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 823
    :goto_0
    return-object v0

    .line 815
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/media/widget/TweetMediaView$c;->a:Ljava/lang/Object;

    instance-of v0, v0, Lcom/twitter/model/media/EditableMedia;

    if-eqz v0, :cond_1

    .line 816
    iget-object v0, p0, Lcom/twitter/library/media/widget/TweetMediaView$c;->a:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/model/media/EditableMedia;

    .line 817
    invoke-static {p1, v0}, Lbru;->a(Landroid/content/Context;Lcom/twitter/model/media/EditableMedia;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    goto :goto_0

    .line 818
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/media/widget/TweetMediaView$c;->a:Ljava/lang/Object;

    instance-of v0, v0, Lcax;

    if-eqz v0, :cond_2

    .line 819
    iget-object v0, p0, Lcom/twitter/library/media/widget/TweetMediaView$c;->a:Ljava/lang/Object;

    check-cast v0, Lcax;

    invoke-static {v0}, Lcom/twitter/media/util/k;->a(Lcax;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    goto :goto_0

    .line 821
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 827
    iget-object v0, p0, Lcom/twitter/library/media/widget/TweetMediaView$c;->a:Ljava/lang/Object;

    instance-of v0, v0, Lcom/twitter/model/core/MediaEntity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/media/widget/TweetMediaView$c;->a:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/model/core/MediaEntity;

    iget-object v0, v0, Lcom/twitter/model/core/MediaEntity;->n:Lcom/twitter/model/core/MediaEntity$Type;

    invoke-static {v0}, Lcom/twitter/model/util/c;->a(Lcom/twitter/model/core/MediaEntity$Type;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lcom/twitter/library/media/widget/TweetMediaView$c;->a:Ljava/lang/Object;

    instance-of v0, v0, Lcax;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/media/widget/TweetMediaView$c;->a:Ljava/lang/Object;

    check-cast v0, Lcax;

    invoke-virtual {v0}, Lcax;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 836
    iget-object v0, p0, Lcom/twitter/library/media/widget/TweetMediaView$c;->a:Ljava/lang/Object;

    instance-of v0, v0, Lcom/twitter/model/core/a;

    if-eqz v0, :cond_0

    .line 837
    iget-object v0, p0, Lcom/twitter/library/media/widget/TweetMediaView$c;->a:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/model/core/a;

    invoke-interface {v0}, Lcom/twitter/model/core/a;->bv_()Ljava/lang/String;

    move-result-object v0

    .line 839
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 844
    instance-of v0, p1, Lcom/twitter/library/media/widget/TweetMediaView$c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/media/widget/TweetMediaView$c;->a:Ljava/lang/Object;

    check-cast p1, Lcom/twitter/library/media/widget/TweetMediaView$c;

    iget-object v1, p1, Lcom/twitter/library/media/widget/TweetMediaView$c;->a:Ljava/lang/Object;

    .line 845
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 844
    :goto_0
    return v0

    .line 845
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 850
    iget-object v0, p0, Lcom/twitter/library/media/widget/TweetMediaView$c;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
