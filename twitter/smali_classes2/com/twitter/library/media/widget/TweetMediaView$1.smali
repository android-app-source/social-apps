.class Lcom/twitter/library/media/widget/TweetMediaView$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/media/request/a$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/media/widget/TweetMediaView;->setMediaItems(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/media/widget/TweetMediaView$c;

.field final synthetic b:Lcom/twitter/media/request/a$a;

.field final synthetic c:Lcom/twitter/library/media/widget/TweetMediaView;


# direct methods
.method constructor <init>(Lcom/twitter/library/media/widget/TweetMediaView;Lcom/twitter/library/media/widget/TweetMediaView$c;Lcom/twitter/media/request/a$a;)V
    .locals 0

    .prologue
    .line 493
    iput-object p1, p0, Lcom/twitter/library/media/widget/TweetMediaView$1;->c:Lcom/twitter/library/media/widget/TweetMediaView;

    iput-object p2, p0, Lcom/twitter/library/media/widget/TweetMediaView$1;->a:Lcom/twitter/library/media/widget/TweetMediaView$c;

    iput-object p3, p0, Lcom/twitter/library/media/widget/TweetMediaView$1;->b:Lcom/twitter/media/request/a$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/media/request/ImageResponse;)V
    .locals 6

    .prologue
    .line 496
    iget-object v0, p0, Lcom/twitter/library/media/widget/TweetMediaView$1;->a:Lcom/twitter/library/media/widget/TweetMediaView$c;

    iget-object v0, v0, Lcom/twitter/library/media/widget/TweetMediaView$c;->b:Lcom/twitter/util/math/Size;

    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->g()F

    move-result v0

    .line 497
    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->a()Lcom/twitter/media/model/MediaFile;

    move-result-object v1

    .line 498
    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->e()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/twitter/media/model/MediaFile;->f:Lcom/twitter/util/math/Size;

    .line 499
    invoke-virtual {v2}, Lcom/twitter/util/math/Size;->g()F

    move-result v2

    sub-float v2, v0, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-double v2, v2

    const-wide v4, 0x3fd3333333333333L    # 0.3

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    .line 500
    new-instance v2, Lcpb;

    invoke-direct {v2}, Lcpb;-><init>()V

    const-string/jumbo v3, "RequestedUrl"

    iget-object v4, p0, Lcom/twitter/library/media/widget/TweetMediaView$1;->b:Lcom/twitter/media/request/a$a;

    iget-object v4, v4, Lcom/twitter/media/request/a$a;->p:Ljava/lang/String;

    .line 501
    invoke-virtual {v2, v3, v4}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v2

    const-string/jumbo v3, "ExpectedAspectRatio"

    .line 502
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    const-string/jumbo v2, "ReceivedAspectRatio"

    iget-object v1, v1, Lcom/twitter/media/model/MediaFile;->f:Lcom/twitter/util/math/Size;

    .line 503
    invoke-virtual {v1}, Lcom/twitter/util/math/Size;->g()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    const-string/jumbo v1, "TweetMediaViewSize"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/twitter/library/media/widget/TweetMediaView$1;->c:Lcom/twitter/library/media/widget/TweetMediaView;

    .line 504
    invoke-virtual {v3}, Lcom/twitter/library/media/widget/TweetMediaView;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/library/media/widget/TweetMediaView$1;->c:Lcom/twitter/library/media/widget/TweetMediaView;

    invoke-virtual {v3}, Lcom/twitter/library/media/widget/TweetMediaView;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    const-string/jumbo v1, "ItemType"

    iget-object v2, p0, Lcom/twitter/library/media/widget/TweetMediaView$1;->a:Lcom/twitter/library/media/widget/TweetMediaView$c;

    iget-object v2, v2, Lcom/twitter/library/media/widget/TweetMediaView$c;->a:Ljava/lang/Object;

    .line 505
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    const-string/jumbo v1, "Source"

    .line 506
    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->f()Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/media/request/ResourceResponse$ResourceSource;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    const-string/jumbo v1, "ViewHierarchy"

    iget-object v2, p0, Lcom/twitter/library/media/widget/TweetMediaView$1;->c:Lcom/twitter/library/media/widget/TweetMediaView;

    const/4 v3, 0x1

    .line 507
    invoke-static {v2, v3}, Lcom/twitter/util/ui/k;->a(Landroid/view/View;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    new-instance v1, Ljava/util/zip/DataFormatException;

    const-string/jumbo v2, "Received bad image data"

    invoke-direct {v1, v2}, Ljava/util/zip/DataFormatException;-><init>(Ljava/lang/String;)V

    .line 509
    invoke-virtual {v0, v1}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v0

    .line 511
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    .line 513
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/media/request/ResourceResponse;)V
    .locals 0

    .prologue
    .line 493
    check-cast p1, Lcom/twitter/media/request/ImageResponse;

    invoke-virtual {p0, p1}, Lcom/twitter/library/media/widget/TweetMediaView$1;->a(Lcom/twitter/media/request/ImageResponse;)V

    return-void
.end method
