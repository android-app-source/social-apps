.class public Lcom/twitter/library/media/widget/a;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 19
    new-instance v0, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-direct {v0, p1, v1, v1}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;-><init>(Landroid/content/Context;ZZ)V

    return-object v0
.end method

.method private b(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;
    .locals 2

    .prologue
    .line 24
    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->Z()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    new-instance v0, Lcom/twitter/library/media/widget/PromotedAdaptiveTweetMediaView;

    invoke-direct {v0, p1, p2}, Lcom/twitter/library/media/widget/PromotedAdaptiveTweetMediaView;-><init>(Landroid/content/Context;Lcom/twitter/model/core/Tweet;)V

    .line 27
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->U()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-direct {v1, p1, v0}, Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;-><init>(Landroid/content/Context;Z)V

    move-object v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne p3, v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/media/widget/a;->b(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/media/widget/a;->a(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/media/widget/AdaptiveTweetMediaView;

    move-result-object v0

    goto :goto_0
.end method
