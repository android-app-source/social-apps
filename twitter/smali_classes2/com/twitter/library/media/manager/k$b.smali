.class public Lcom/twitter/library/media/manager/k$b;
.super Landroid/os/Handler;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/media/manager/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/media/manager/k$b$b;,
        Lcom/twitter/library/media/manager/k$b$c;,
        Lcom/twitter/library/media/manager/k$b$d;,
        Lcom/twitter/library/media/manager/k$b$a;,
        Lcom/twitter/library/media/manager/k$b$e;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<REQ:",
        "Lcom/twitter/media/request/b;",
        "RES:",
        "Ljava/lang/Object;",
        "RESP:",
        "Lcom/twitter/media/request/ResourceResponse",
        "<TREQ;TRES;>;>",
        "Landroid/os/Handler;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/os/Handler;

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/library/media/manager/k",
            "<TREQ;TRES;TRESP;>;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/library/media/manager/i$b;

.field private final e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/media/manager/k$b$a",
            "<TREQ;TRESP;>;>;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/media/manager/k$b$a",
            "<TREQ;TRESP;>;>;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/media/manager/k$b$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/twitter/library/media/manager/k;Lcom/twitter/library/media/manager/i$b;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/os/Looper;",
            "Lcom/twitter/library/media/manager/k",
            "<TREQ;TRES;TRESP;>;",
            "Lcom/twitter/library/media/manager/i$b;",
            ")V"
        }
    .end annotation

    .prologue
    const/16 v2, 0x14

    .line 460
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 439
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/library/media/manager/k$b;->b:Landroid/os/Handler;

    .line 442
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/media/manager/k$b;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 446
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/library/media/manager/k$b;->f:Ljava/util/Map;

    .line 450
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/library/media/manager/k$b;->g:Ljava/util/Map;

    .line 455
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/library/media/manager/k$b;->h:Ljava/util/Map;

    .line 461
    iput-object p1, p0, Lcom/twitter/library/media/manager/k$b;->a:Landroid/content/Context;

    .line 462
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/media/manager/k$b;->c:Ljava/lang/ref/WeakReference;

    .line 463
    iput-object p4, p0, Lcom/twitter/library/media/manager/k$b;->d:Lcom/twitter/library/media/manager/i$b;

    .line 464
    return-void
.end method

.method private a(Lcom/twitter/library/media/manager/k$b$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/media/manager/k$b$a",
            "<TREQ;TRESP;>;)V"
        }
    .end annotation

    .prologue
    .line 848
    invoke-virtual {p0}, Lcom/twitter/library/media/manager/k$b;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/f;->a(Landroid/os/Looper;)V

    .line 849
    iget-object v0, p1, Lcom/twitter/library/media/manager/k$b$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$b$e;

    .line 850
    iget-object v0, v0, Lcom/twitter/library/media/manager/k$b$e;->d:Lcom/twitter/library/media/manager/h;

    .line 851
    invoke-virtual {v0}, Lcom/twitter/library/media/manager/h;->j()V

    .line 852
    invoke-virtual {v0}, Lcom/twitter/library/media/manager/h;->bp_()V

    goto :goto_0

    .line 854
    :cond_0
    return-void
.end method

.method private a(Lcom/twitter/library/media/manager/k$b$a;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/media/manager/k$b$a",
            "<TREQ;TRESP;>;TRES;)V"
        }
    .end annotation

    .prologue
    .line 811
    invoke-virtual {p0}, Lcom/twitter/library/media/manager/k$b;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/f;->a(Landroid/os/Looper;)V

    .line 813
    invoke-direct {p0, p1}, Lcom/twitter/library/media/manager/k$b;->a(Lcom/twitter/library/media/manager/k$b$a;)V

    .line 815
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k;

    .line 816
    if-eqz v0, :cond_0

    .line 817
    iget-object v1, p1, Lcom/twitter/library/media/manager/k$b$a;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/media/manager/k$b$e;

    .line 818
    iget-object v1, v1, Lcom/twitter/library/media/manager/k$b$e;->c:Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/k;->a(Lcom/twitter/media/request/ResourceResponse$ResourceSource;)V

    goto :goto_0

    .line 822
    :cond_0
    iget-object v1, p0, Lcom/twitter/library/media/manager/k$b;->b:Landroid/os/Handler;

    new-instance v2, Lcom/twitter/library/media/manager/k$b$5;

    invoke-direct {v2, p0, v0, p1, p2}, Lcom/twitter/library/media/manager/k$b$5;-><init>(Lcom/twitter/library/media/manager/k$b;Lcom/twitter/library/media/manager/k;Lcom/twitter/library/media/manager/k$b$a;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 844
    return-void
.end method

.method private a(Lcom/twitter/library/media/manager/k$b$a;Ljava/lang/String;Lcom/twitter/network/l;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/media/manager/k$b$a",
            "<TREQ;TRESP;>;",
            "Ljava/lang/String;",
            "Lcom/twitter/network/l;",
            ")V"
        }
    .end annotation

    .prologue
    .line 779
    invoke-virtual {p0}, Lcom/twitter/library/media/manager/k$b;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/f;->a(Landroid/os/Looper;)V

    .line 780
    if-eqz p3, :cond_0

    iget v0, p3, Lcom/twitter/network/l;->a:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b;->h:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 782
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    const-wide/32 v2, 0xea60

    add-long/2addr v0, v2

    .line 783
    iget-object v2, p0, Lcom/twitter/library/media/manager/k$b;->h:Ljava/util/Map;

    new-instance v3, Lcom/twitter/library/media/manager/k$b$b;

    invoke-direct {v3, v0, v1, p3}, Lcom/twitter/library/media/manager/k$b$b;-><init>(JLcom/twitter/network/l;)V

    invoke-interface {v2, p2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 785
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/media/manager/k$b;->a(Lcom/twitter/library/media/manager/k$b$a;Ljava/lang/Object;)V

    .line 786
    return-void
.end method

.method private a(Lcom/twitter/library/media/manager/k$b$a;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/media/manager/k$b$a",
            "<TREQ;TRESP;>;",
            "Ljava/lang/String;",
            "TRES;)V"
        }
    .end annotation

    .prologue
    .line 791
    invoke-virtual {p0}, Lcom/twitter/library/media/manager/k$b;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/f;->a(Landroid/os/Looper;)V

    .line 792
    if-eqz p3, :cond_2

    .line 793
    const/4 v1, 0x0

    .line 794
    iget-object v0, p1, Lcom/twitter/library/media/manager/k$b$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$b$e;

    .line 795
    iget-object v0, v0, Lcom/twitter/library/media/manager/k$b$e;->a:Lcom/twitter/media/request/b;

    invoke-virtual {v0}, Lcom/twitter/media/request/b;->y()Z

    move-result v0

    if-nez v0, :cond_0

    .line 796
    const/4 v0, 0x1

    move v1, v0

    .line 800
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k;

    .line 801
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 802
    invoke-virtual {v0, p2, p3}, Lcom/twitter/library/media/manager/k;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 805
    :cond_2
    invoke-direct {p0, p1, p3}, Lcom/twitter/library/media/manager/k$b;->a(Lcom/twitter/library/media/manager/k$b$a;Ljava/lang/Object;)V

    .line 806
    return-void
.end method

.method private a(Lcom/twitter/library/media/manager/k$b$e;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/media/manager/k$b$e",
            "<TREQ;TRESP;>;)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 655
    invoke-virtual {p0}, Lcom/twitter/library/media/manager/k$b;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/f;->a(Landroid/os/Looper;)V

    .line 656
    iget-object v9, p1, Lcom/twitter/library/media/manager/k$b$e;->a:Lcom/twitter/media/request/b;

    .line 657
    invoke-virtual {v9}, Lcom/twitter/media/request/b;->a()Ljava/lang/String;

    move-result-object v6

    .line 658
    invoke-static {v9}, Lcom/twitter/library/media/manager/k$b;->b(Lcom/twitter/media/request/b;)Ljava/lang/String;

    move-result-object v7

    .line 659
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b;->f:Ljava/util/Map;

    invoke-interface {v0, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$b$a;

    .line 660
    if-nez v0, :cond_4

    .line 664
    invoke-direct {p0}, Lcom/twitter/library/media/manager/k$b;->b()V

    .line 665
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k;

    .line 666
    new-instance v2, Lcom/twitter/library/media/manager/h;

    const-string/jumbo v1, "fetch_blocking"

    invoke-direct {v2, v1}, Lcom/twitter/library/media/manager/h;-><init>(Ljava/lang/String;)V

    .line 668
    new-instance v3, Lcom/twitter/library/media/manager/h;

    const-string/jumbo v1, "fetch_runtime"

    invoke-direct {v3, v1}, Lcom/twitter/library/media/manager/h;-><init>(Ljava/lang/String;)V

    .line 670
    new-instance v10, Lcom/twitter/util/t;

    invoke-direct {v10}, Lcom/twitter/util/t;-><init>()V

    .line 671
    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/twitter/library/media/manager/k$b;->h:Ljava/util/Map;

    invoke-interface {v1, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v9}, Lcom/twitter/media/request/b;->x()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 672
    invoke-static {v6}, Lcom/twitter/util/ac;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 673
    invoke-virtual {v2}, Lcom/twitter/library/media/manager/h;->i()V

    .line 677
    iget-object v5, v0, Lcom/twitter/library/media/manager/k;->c:Lcpy;

    .line 678
    invoke-virtual {v9}, Lcom/twitter/media/request/b;->z()Lcom/twitter/media/request/ResourceRequestType;

    move-result-object v4

    .line 679
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b;->d:Lcom/twitter/library/media/manager/i$b;

    invoke-interface {v0}, Lcom/twitter/library/media/manager/i$b;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/i$a;

    iget-object v1, p0, Lcom/twitter/library/media/manager/k$b;->a:Landroid/content/Context;

    .line 680
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/i$a;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/i$a;

    move-result-object v0

    .line 681
    invoke-virtual {v0, v6}, Lcom/twitter/library/media/manager/i$a;->a(Ljava/lang/String;)Lcom/twitter/library/media/manager/i$a;

    move-result-object v0

    .line 682
    invoke-static {}, Lcox;->ax()Lcox;

    move-result-object v1

    invoke-virtual {v1}, Lcox;->ar()Lcnz;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/i$a;->a(Lcnz;)Lcom/twitter/library/media/manager/i$a;

    move-result-object v1

    .line 683
    invoke-virtual {v9}, Lcom/twitter/media/request/b;->u()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/network/a;

    invoke-virtual {v1, v0}, Lcom/twitter/library/media/manager/i$a;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/media/manager/i$a;

    move-result-object v0

    .line 684
    invoke-virtual {v0, v4}, Lcom/twitter/library/media/manager/i$a;->a(Lcom/twitter/media/request/ResourceRequestType;)Lcom/twitter/library/media/manager/i$a;

    move-result-object v0

    .line 685
    invoke-virtual {v9}, Lcom/twitter/media/request/b;->B()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/i$a;->b(Ljava/lang/String;)Lcom/twitter/library/media/manager/i$a;

    move-result-object v0

    .line 686
    invoke-virtual {v0, v10}, Lcom/twitter/library/media/manager/i$a;->a(Lcom/twitter/util/q;)Lcom/twitter/library/media/manager/i$a;

    move-result-object v11

    new-instance v0, Lcom/twitter/library/media/manager/k$b$4;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/media/manager/k$b$4;-><init>(Lcom/twitter/library/media/manager/k$b;Lcom/twitter/library/media/manager/h;Lcom/twitter/library/media/manager/h;Lcom/twitter/media/request/ResourceRequestType;Lcpy;Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    invoke-virtual {v11, v0}, Lcom/twitter/library/media/manager/i$a;->a(Lcom/twitter/library/media/manager/i$c;)Lcom/twitter/library/media/manager/i$a;

    move-result-object v0

    .line 719
    invoke-virtual {v0}, Lcom/twitter/library/media/manager/i$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/i;

    .line 720
    invoke-interface {v0}, Lcom/twitter/library/media/manager/i;->a()Ljava/util/concurrent/Future;

    move-result-object v0

    .line 722
    :goto_0
    new-instance v1, Lcom/twitter/library/media/manager/k$b$a;

    invoke-direct {v1, v0, v10}, Lcom/twitter/library/media/manager/k$b$a;-><init>(Ljava/util/concurrent/Future;Lcom/twitter/util/t;)V

    .line 725
    :goto_1
    invoke-virtual {v1, p1}, Lcom/twitter/library/media/manager/k$b$a;->a(Lcom/twitter/library/media/manager/k$b$e;)V

    .line 726
    invoke-virtual {v9}, Lcom/twitter/media/request/b;->v()Lcom/twitter/util/q;

    move-result-object v0

    .line 727
    if-eqz v0, :cond_0

    .line 728
    iget-object v2, v1, Lcom/twitter/library/media/manager/k$b$a;->b:Lcom/twitter/util/t;

    invoke-virtual {v2, v0}, Lcom/twitter/util/t;->a(Lcom/twitter/util/q;)V

    .line 731
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/library/media/manager/k$b$a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 732
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b;->f:Ljava/util/Map;

    invoke-interface {v0, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 737
    :goto_2
    return-void

    .line 734
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b;->h:Ljava/util/Map;

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$b$b;

    .line 735
    if-eqz v0, :cond_2

    iget-object v8, v0, Lcom/twitter/library/media/manager/k$b$b;->b:Lcom/twitter/network/l;

    :cond_2
    invoke-direct {p0, v1, v7, v8}, Lcom/twitter/library/media/manager/k$b;->a(Lcom/twitter/library/media/manager/k$b$a;Ljava/lang/String;Lcom/twitter/network/l;)V

    goto :goto_2

    :cond_3
    move-object v0, v8

    goto :goto_0

    :cond_4
    move-object v1, v0

    goto :goto_1
.end method

.method private a(Lcom/twitter/library/media/manager/k$b$e;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/media/manager/k$b$e",
            "<TREQ;TRESP;>;Z)V"
        }
    .end annotation

    .prologue
    .line 613
    invoke-virtual {p0}, Lcom/twitter/library/media/manager/k$b;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/f;->a(Landroid/os/Looper;)V

    .line 614
    iget-object v3, p1, Lcom/twitter/library/media/manager/k$b$e;->a:Lcom/twitter/media/request/b;

    .line 615
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/media/manager/k;

    .line 616
    if-nez v2, :cond_0

    .line 651
    :goto_0
    return-void

    .line 619
    :cond_0
    invoke-virtual {v3}, Lcom/twitter/media/request/b;->r()Ljava/lang/String;

    move-result-object v4

    .line 620
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b;->g:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$b$a;

    .line 621
    if-nez v0, :cond_1

    .line 623
    new-instance v0, Lcom/twitter/library/media/manager/k$b$3;

    move-object v1, p0

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/media/manager/k$b$3;-><init>(Lcom/twitter/library/media/manager/k$b;Lcom/twitter/library/media/manager/k;Lcom/twitter/media/request/b;Ljava/lang/String;Z)V

    .line 642
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 643
    new-instance v1, Lcom/twitter/library/media/manager/k$b$a;

    new-instance v2, Lcom/twitter/util/t;

    invoke-direct {v2}, Lcom/twitter/util/t;-><init>()V

    invoke-direct {v1, v0, v2}, Lcom/twitter/library/media/manager/k$b$a;-><init>(Ljava/util/concurrent/Future;Lcom/twitter/util/t;)V

    move-object v0, v1

    .line 645
    :cond_1
    invoke-virtual {v0, p1}, Lcom/twitter/library/media/manager/k$b$a;->a(Lcom/twitter/library/media/manager/k$b$e;)V

    .line 646
    invoke-virtual {v0}, Lcom/twitter/library/media/manager/k$b$a;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 647
    iget-object v1, p0, Lcom/twitter/library/media/manager/k$b;->g:Ljava/util/Map;

    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 649
    :cond_2
    const/4 v1, 0x0

    invoke-direct {p0, v0, v4, v1}, Lcom/twitter/library/media/manager/k$b;->a(Lcom/twitter/library/media/manager/k$b$a;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static b(Lcom/twitter/media/request/b;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 866
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/media/request/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/media/request/b;->z()Lcom/twitter/media/request/ResourceRequestType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 6

    .prologue
    .line 741
    invoke-virtual {p0}, Lcom/twitter/library/media/manager/k$b;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/f;->a(Landroid/os/Looper;)V

    .line 742
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    .line 743
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 744
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 745
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 746
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$b$b;

    iget-wide v4, v0, Lcom/twitter/library/media/manager/k$b$b;->a:J

    cmp-long v0, v4, v2

    if-gez v0, :cond_0

    .line 748
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 754
    :cond_0
    return-void
.end method

.method private b(Lcom/twitter/library/media/manager/k$b$e;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/media/manager/k$b$e",
            "<TREQ;TRESP;>;)V"
        }
    .end annotation

    .prologue
    .line 758
    invoke-virtual {p0}, Lcom/twitter/library/media/manager/k$b;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/f;->a(Landroid/os/Looper;)V

    .line 759
    iget-object v0, p1, Lcom/twitter/library/media/manager/k$b$e;->a:Lcom/twitter/media/request/b;

    invoke-static {v0}, Lcom/twitter/library/media/manager/k$b;->b(Lcom/twitter/media/request/b;)Ljava/lang/String;

    move-result-object v1

    .line 760
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b;->f:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$b$a;

    .line 761
    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/twitter/library/media/manager/k$b$a;->b(Lcom/twitter/library/media/manager/k$b$e;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 762
    invoke-virtual {v0}, Lcom/twitter/library/media/manager/k$b$a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 763
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b;->f:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 774
    :cond_0
    :goto_0
    return-void

    .line 766
    :cond_1
    iget-object v0, p1, Lcom/twitter/library/media/manager/k$b$e;->a:Lcom/twitter/media/request/b;

    invoke-virtual {v0}, Lcom/twitter/media/request/b;->r()Ljava/lang/String;

    move-result-object v1

    .line 767
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b;->g:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$b$a;

    .line 768
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/twitter/library/media/manager/k$b$a;->b(Lcom/twitter/library/media/manager/k$b$e;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 769
    invoke-virtual {v0}, Lcom/twitter/library/media/manager/k$b$a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 770
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b;->g:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/media/request/b;)Lcom/twitter/util/concurrent/g;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TREQ;)",
            "Lcom/twitter/util/concurrent/g",
            "<TRESP;>;"
        }
    .end annotation

    .prologue
    .line 468
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k;

    .line 469
    iget-object v1, p0, Lcom/twitter/library/media/manager/k$b;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_0

    if-nez v0, :cond_1

    .line 470
    :cond_0
    invoke-static {}, Lcom/twitter/util/concurrent/ObservablePromise;->b()Lcom/twitter/util/concurrent/ObservablePromise;

    move-result-object v0

    .line 507
    :goto_0
    return-object v0

    .line 473
    :cond_1
    invoke-virtual {v0, p1}, Lcom/twitter/library/media/manager/k;->e(Lcom/twitter/media/request/b;)Ljava/lang/Object;

    move-result-object v2

    .line 474
    invoke-virtual {p1}, Lcom/twitter/media/request/b;->t()Z

    move-result v1

    if-eqz v1, :cond_2

    if-nez v2, :cond_2

    invoke-virtual {p1}, Lcom/twitter/media/request/b;->w()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 475
    :cond_2
    if-eqz v2, :cond_4

    sget-object v1, Lcom/twitter/media/request/ResourceResponse$ResourceSource;->b:Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    .line 477
    :goto_1
    invoke-virtual {v0, p1, v2, v1}, Lcom/twitter/library/media/manager/k;->a(Lcom/twitter/media/request/b;Ljava/lang/Object;Lcom/twitter/media/request/ResourceResponse$ResourceSource;)Lcom/twitter/media/request/ResourceResponse;

    move-result-object v3

    .line 478
    invoke-virtual {p1}, Lcom/twitter/media/request/b;->C()Lcom/twitter/media/request/b$b;

    move-result-object v4

    .line 479
    invoke-static {v3}, Lcom/twitter/util/concurrent/ObservablePromise;->a(Ljava/lang/Object;)Lcom/twitter/util/concurrent/ObservablePromise;

    move-result-object v2

    .line 480
    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/k;->a(Lcom/twitter/media/request/ResourceResponse$ResourceSource;)V

    .line 481
    if-eqz v4, :cond_3

    .line 482
    invoke-static {}, Lcom/twitter/util/d;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 483
    invoke-interface {v4, v3}, Lcom/twitter/media/request/b$b;->a(Lcom/twitter/media/request/ResourceResponse;)V

    :cond_3
    :goto_2
    move-object v0, v2

    .line 493
    goto :goto_0

    .line 475
    :cond_4
    sget-object v1, Lcom/twitter/media/request/ResourceResponse$ResourceSource;->a:Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    goto :goto_1

    .line 485
    :cond_5
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b;->b:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/library/media/manager/k$b$1;

    invoke-direct {v1, p0, v4, v3}, Lcom/twitter/library/media/manager/k$b$1;-><init>(Lcom/twitter/library/media/manager/k$b;Lcom/twitter/media/request/b$b;Lcom/twitter/media/request/ResourceResponse;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    .line 495
    :cond_6
    new-instance v1, Lcom/twitter/library/media/manager/k$b$e;

    invoke-direct {v1, p1}, Lcom/twitter/library/media/manager/k$b$e;-><init>(Lcom/twitter/media/request/b;)V

    .line 496
    new-instance v0, Lcom/twitter/library/media/manager/k$b$2;

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/media/manager/k$b$2;-><init>(Lcom/twitter/library/media/manager/k$b;Lcom/twitter/library/media/manager/k$b$e;)V

    .line 504
    iput-object v0, v1, Lcom/twitter/library/media/manager/k$b$e;->b:Lcom/twitter/util/concurrent/h;

    .line 505
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v1}, Lcom/twitter/library/media/manager/k$b;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public a()Ljava/util/concurrent/Future;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 512
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 513
    new-instance v0, Lcom/twitter/util/concurrent/h;

    invoke-direct {v0}, Lcom/twitter/util/concurrent/h;-><init>()V

    .line 514
    const/4 v1, 0x5

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/media/manager/k$b;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 517
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/twitter/util/concurrent/h;->d()Lcom/twitter/util/concurrent/h;

    move-result-object v0

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    .line 524
    invoke-virtual {p0}, Lcom/twitter/library/media/manager/k$b;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/f;->a(Landroid/os/Looper;)V

    .line 525
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 606
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Unknown message!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 527
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$b$e;

    .line 528
    iget-object v1, p0, Lcom/twitter/library/media/manager/k$b;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/media/manager/k;

    .line 529
    if-eqz v1, :cond_1

    .line 530
    iget-object v2, v0, Lcom/twitter/library/media/manager/k$b$e;->a:Lcom/twitter/media/request/b;

    .line 531
    invoke-virtual {v1, v2}, Lcom/twitter/library/media/manager/k;->e(Lcom/twitter/media/request/b;)Ljava/lang/Object;

    move-result-object v3

    .line 532
    if-eqz v3, :cond_2

    .line 533
    sget-object v1, Lcom/twitter/media/request/ResourceResponse$ResourceSource;->b:Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    iput-object v1, v0, Lcom/twitter/library/media/manager/k$b$e;->c:Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    .line 534
    new-instance v1, Lcom/twitter/util/t;

    invoke-direct {v1}, Lcom/twitter/util/t;-><init>()V

    .line 537
    invoke-virtual {v2}, Lcom/twitter/media/request/b;->v()Lcom/twitter/util/q;

    move-result-object v2

    .line 538
    if-eqz v2, :cond_0

    .line 539
    invoke-virtual {v1, v2}, Lcom/twitter/util/t;->a(Lcom/twitter/util/q;)V

    .line 541
    :cond_0
    new-instance v2, Lcom/twitter/library/media/manager/k$b$a;

    invoke-direct {v2, v0, v1}, Lcom/twitter/library/media/manager/k$b$a;-><init>(Lcom/twitter/library/media/manager/k$b$e;Lcom/twitter/util/t;)V

    invoke-direct {p0, v2, v3}, Lcom/twitter/library/media/manager/k$b;->a(Lcom/twitter/library/media/manager/k$b$a;Ljava/lang/Object;)V

    .line 609
    :cond_1
    :goto_0
    return-void

    .line 543
    :cond_2
    invoke-virtual {v1, v2}, Lcom/twitter/library/media/manager/k;->c(Lcom/twitter/media/request/b;)Landroid/util/Pair;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 545
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/twitter/library/media/manager/k$b;->a(Lcom/twitter/library/media/manager/k$b$e;Z)V

    goto :goto_0

    .line 547
    :cond_3
    invoke-direct {p0, v0}, Lcom/twitter/library/media/manager/k$b;->a(Lcom/twitter/library/media/manager/k$b$e;)V

    goto :goto_0

    .line 553
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/library/media/manager/k$b$c;

    .line 554
    iget-object v1, v0, Lcom/twitter/library/media/manager/k$b$c;->a:Ljava/lang/String;

    .line 555
    iget-object v2, v0, Lcom/twitter/library/media/manager/k$b$c;->b:Lcom/twitter/network/l;

    .line 556
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b;->f:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$b$a;

    .line 557
    if-eqz v0, :cond_1

    .line 558
    if-eqz v2, :cond_4

    iget v3, v2, Lcom/twitter/network/l;->a:I

    const/16 v4, 0xc8

    if-ne v3, v4, :cond_4

    .line 560
    sget-object v1, Lcom/twitter/media/request/ResourceResponse$ResourceSource;->f:Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/k$b$a;->a(Lcom/twitter/media/request/ResourceResponse$ResourceSource;)V

    .line 561
    iget-object v0, v0, Lcom/twitter/library/media/manager/k$b$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$b$e;

    .line 562
    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/media/manager/k$b;->a(Lcom/twitter/library/media/manager/k$b$e;Z)V

    goto :goto_1

    .line 565
    :cond_4
    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/library/media/manager/k$b;->a(Lcom/twitter/library/media/manager/k$b$a;Ljava/lang/String;Lcom/twitter/network/l;)V

    goto :goto_0

    .line 571
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$b$d;

    .line 572
    iget-object v2, v0, Lcom/twitter/library/media/manager/k$b$d;->a:Ljava/lang/String;

    .line 573
    iget-object v1, p0, Lcom/twitter/library/media/manager/k$b;->g:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/media/manager/k$b$a;

    .line 574
    if-eqz v1, :cond_1

    .line 575
    iget-object v3, v0, Lcom/twitter/library/media/manager/k$b$d;->c:Ljava/lang/Object;

    if-nez v3, :cond_5

    iget-boolean v3, v0, Lcom/twitter/library/media/manager/k$b$d;->d:Z

    if-nez v3, :cond_6

    .line 576
    :cond_5
    iget-object v3, v0, Lcom/twitter/library/media/manager/k$b$d;->b:Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    invoke-virtual {v1, v3}, Lcom/twitter/library/media/manager/k$b$a;->a(Lcom/twitter/media/request/ResourceResponse$ResourceSource;)V

    .line 577
    iget-object v0, v0, Lcom/twitter/library/media/manager/k$b$d;->c:Ljava/lang/Object;

    invoke-direct {p0, v1, v2, v0}, Lcom/twitter/library/media/manager/k$b;->a(Lcom/twitter/library/media/manager/k$b$a;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 580
    :cond_6
    iget-object v0, v1, Lcom/twitter/library/media/manager/k$b$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$b$e;

    .line 581
    invoke-direct {p0, v0}, Lcom/twitter/library/media/manager/k$b;->a(Lcom/twitter/library/media/manager/k$b$e;)V

    goto :goto_2

    .line 588
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$b$e;

    .line 589
    invoke-direct {p0, v0}, Lcom/twitter/library/media/manager/k$b;->b(Lcom/twitter/library/media/manager/k$b$e;)V

    goto/16 :goto_0

    .line 593
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/util/concurrent/h;

    .line 594
    iget-object v1, p0, Lcom/twitter/library/media/manager/k$b;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/media/manager/k$b$a;

    .line 595
    invoke-virtual {v1}, Lcom/twitter/library/media/manager/k$b$a;->c()V

    goto :goto_3

    .line 597
    :cond_7
    iget-object v1, p0, Lcom/twitter/library/media/manager/k$b;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 598
    iget-object v1, p0, Lcom/twitter/library/media/manager/k$b;->f:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/media/manager/k$b$a;

    .line 599
    invoke-virtual {v1}, Lcom/twitter/library/media/manager/k$b$a;->c()V

    goto :goto_4

    .line 601
    :cond_8
    iget-object v1, p0, Lcom/twitter/library/media/manager/k$b;->f:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 602
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/util/concurrent/h;->set(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 525
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
