.class Lcom/twitter/library/media/manager/k$b$3;
.super Lcom/twitter/library/media/manager/k$d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/media/manager/k$b;->a(Lcom/twitter/library/media/manager/k$b$e;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/media/manager/k$d",
        "<TREQ;TRES;TRESP;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Z

.field final synthetic c:Lcom/twitter/library/media/manager/k$b;


# direct methods
.method constructor <init>(Lcom/twitter/library/media/manager/k$b;Lcom/twitter/library/media/manager/k;Lcom/twitter/media/request/b;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 625
    iput-object p1, p0, Lcom/twitter/library/media/manager/k$b$3;->c:Lcom/twitter/library/media/manager/k$b;

    iput-object p4, p0, Lcom/twitter/library/media/manager/k$b$3;->a:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/twitter/library/media/manager/k$b$3;->b:Z

    invoke-direct {p0, p2, p3}, Lcom/twitter/library/media/manager/k$d;-><init>(Lcom/twitter/library/media/manager/k;Lcom/twitter/media/request/b;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/async/service/j;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/twitter/media/request/ResourceResponse$ResourceSource;",
            "TRES;>;>;)V"
        }
    .end annotation

    .prologue
    .line 629
    invoke-super {p0, p1}, Lcom/twitter/library/media/manager/k$d;->a(Lcom/twitter/async/service/j;)V

    .line 630
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 631
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/media/manager/k$b$3;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 640
    :cond_0
    :goto_0
    return-void

    .line 634
    :cond_1
    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    .line 635
    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    .line 636
    new-instance v0, Lcom/twitter/library/media/manager/k$b$d;

    iget-object v1, p0, Lcom/twitter/library/media/manager/k$b$3;->a:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/twitter/library/media/manager/k$b$3;->b:Z

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/media/manager/k$b$d;-><init>(Ljava/lang/String;Lcom/twitter/media/request/ResourceResponse$ResourceSource;Ljava/lang/Object;ZLcom/twitter/library/media/manager/k$1;)V

    .line 638
    iget-object v1, p0, Lcom/twitter/library/media/manager/k$b$3;->c:Lcom/twitter/library/media/manager/k$b;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/media/manager/k$b;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 639
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method
