.class public Lcom/twitter/library/media/manager/m;
.super Lcom/twitter/library/media/manager/k;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/media/manager/m$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/media/manager/k",
        "<",
        "Lcom/twitter/library/media/manager/n;",
        "Lcom/twitter/media/model/VideoFile;",
        "Lcom/twitter/media/request/ResourceResponse",
        "<",
        "Lcom/twitter/library/media/manager/n;",
        "Lcom/twitter/media/model/VideoFile;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/twitter/library/media/manager/m$a;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/twitter/library/media/manager/k;-><init>(Lcom/twitter/library/media/manager/k$a;)V

    .line 15
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/media/manager/n;Ljava/io/File;)Lcom/twitter/media/model/VideoFile;
    .locals 1

    .prologue
    .line 33
    invoke-static {p2}, Lcom/twitter/media/model/VideoFile;->a(Ljava/io/File;)Lcom/twitter/media/model/VideoFile;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/library/media/manager/n;Lcom/twitter/media/model/VideoFile;Lcom/twitter/media/request/ResourceResponse$ResourceSource;)Lcom/twitter/media/request/ResourceResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/media/manager/n;",
            "Lcom/twitter/media/model/VideoFile;",
            "Lcom/twitter/media/request/ResourceResponse$ResourceSource;",
            ")",
            "Lcom/twitter/media/request/ResourceResponse",
            "<",
            "Lcom/twitter/library/media/manager/n;",
            "Lcom/twitter/media/model/VideoFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 21
    new-instance v0, Lcom/twitter/media/request/ResourceResponse;

    invoke-direct {v0, p1, p2, p3}, Lcom/twitter/media/request/ResourceResponse;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/twitter/media/request/ResourceResponse$ResourceSource;)V

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/media/request/b;Ljava/lang/Object;Lcom/twitter/media/request/ResourceResponse$ResourceSource;)Lcom/twitter/media/request/ResourceResponse;
    .locals 1

    .prologue
    .line 12
    check-cast p1, Lcom/twitter/library/media/manager/n;

    check-cast p2, Lcom/twitter/media/model/VideoFile;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/media/manager/m;->a(Lcom/twitter/library/media/manager/n;Lcom/twitter/media/model/VideoFile;Lcom/twitter/media/request/ResourceResponse$ResourceSource;)Lcom/twitter/media/request/ResourceResponse;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/media/model/VideoFile;)Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x1

    return v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 12
    check-cast p1, Lcom/twitter/media/model/VideoFile;

    invoke-virtual {p0, p1}, Lcom/twitter/library/media/manager/m;->a(Lcom/twitter/media/model/VideoFile;)Z

    move-result v0

    return v0
.end method

.method protected synthetic b(Lcom/twitter/media/request/b;Ljava/io/File;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    check-cast p1, Lcom/twitter/library/media/manager/n;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/media/manager/m;->a(Lcom/twitter/library/media/manager/n;Ljava/io/File;)Lcom/twitter/media/model/VideoFile;

    move-result-object v0

    return-object v0
.end method
