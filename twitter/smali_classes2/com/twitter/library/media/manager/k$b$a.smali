.class Lcom/twitter/library/media/manager/k$b$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/media/manager/k$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<REQ:",
        "Lcom/twitter/media/request/b;",
        "RESP:",
        "Lcom/twitter/media/request/ResourceResponse;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/media/manager/k$b$e",
            "<TREQ;TRESP;>;>;"
        }
    .end annotation
.end field

.field public final b:Lcom/twitter/util/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/t",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/library/media/manager/k$b$e;Lcom/twitter/util/t;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/media/manager/k$b$e",
            "<TREQ;TRESP;>;",
            "Lcom/twitter/util/t",
            "<",
            "Ljava/lang/Double;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 892
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/library/media/manager/k$b$a;->a:Ljava/util/List;

    .line 893
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 894
    iput-object p2, p0, Lcom/twitter/library/media/manager/k$b$a;->b:Lcom/twitter/util/t;

    .line 895
    return-void
.end method

.method constructor <init>(Ljava/util/concurrent/Future;Lcom/twitter/util/t;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;",
            "Lcom/twitter/util/t",
            "<",
            "Ljava/lang/Double;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 899
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 900
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/library/media/manager/k$b$a;->a:Ljava/util/List;

    .line 901
    iput-object p1, p0, Lcom/twitter/library/media/manager/k$b$a;->c:Ljava/util/concurrent/Future;

    .line 902
    iput-object p2, p0, Lcom/twitter/library/media/manager/k$b$a;->b:Lcom/twitter/util/t;

    .line 903
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/media/manager/k$b$e;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/media/manager/k$b$e",
            "<TREQ;TRESP;>;)V"
        }
    .end annotation

    .prologue
    .line 910
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 911
    return-void
.end method

.method public a(Lcom/twitter/media/request/ResourceResponse$ResourceSource;)V
    .locals 4

    .prologue
    .line 925
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$b$e;

    .line 926
    iget-object v2, v0, Lcom/twitter/library/media/manager/k$b$e;->c:Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    sget-object v3, Lcom/twitter/media/request/ResourceResponse$ResourceSource;->a:Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    if-ne v2, v3, :cond_0

    .line 927
    iput-object p1, v0, Lcom/twitter/library/media/manager/k$b$e;->c:Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    goto :goto_0

    .line 930
    :cond_1
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 906
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$a;->c:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 933
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 934
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/media/manager/k$b$a;->c:Ljava/util/concurrent/Future;

    .line 935
    return-void
.end method

.method public b(Lcom/twitter/library/media/manager/k$b$e;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/media/manager/k$b$e",
            "<TREQ;TRESP;>;)Z"
        }
    .end annotation

    .prologue
    .line 914
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 915
    iget-object v0, p1, Lcom/twitter/library/media/manager/k$b$e;->b:Lcom/twitter/util/concurrent/h;

    invoke-virtual {v0}, Lcom/twitter/util/concurrent/h;->e()Z

    .line 916
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 917
    invoke-virtual {p0}, Lcom/twitter/library/media/manager/k$b$a;->c()V

    .line 919
    :cond_0
    const/4 v0, 0x1

    .line 921
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 938
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$b$e;

    .line 939
    iget-object v0, v0, Lcom/twitter/library/media/manager/k$b$e;->b:Lcom/twitter/util/concurrent/h;

    invoke-virtual {v0}, Lcom/twitter/util/concurrent/h;->e()Z

    goto :goto_0

    .line 941
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 942
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$a;->c:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_1

    .line 943
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$a;->c:Ljava/util/concurrent/Future;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 944
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/media/manager/k$b$a;->c:Ljava/util/concurrent/Future;

    .line 946
    :cond_1
    return-void
.end method
