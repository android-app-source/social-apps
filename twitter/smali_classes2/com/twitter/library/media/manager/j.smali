.class public Lcom/twitter/library/media/manager/j;
.super Lcom/twitter/library/service/s;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/media/manager/i;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/media/manager/j$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/twitter/library/network/a;

.field private c:J

.field private g:Z

.field private h:Lcom/twitter/library/media/manager/j;

.field private final i:Lcom/twitter/library/media/manager/i$c;

.field private j:Z

.field private final k:Lcom/twitter/media/request/ResourceRequestType;

.field private final l:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ljava/lang/String;

.field private final n:Lcom/twitter/async/service/b;


# direct methods
.method private constructor <init>(Lcom/twitter/library/media/manager/j$a;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 68
    iget-object v0, p1, Lcom/twitter/library/media/manager/j$a;->b:Landroid/content/Context;

    iget-object v1, p1, Lcom/twitter/library/media/manager/j$a;->a:Ljava/lang/String;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    iget-object v3, p1, Lcom/twitter/library/media/manager/j$a;->c:Lcnz;

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/v;->a(Lcnz;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/library/service/s;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 46
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/library/media/manager/j;->c:J

    .line 47
    iput-boolean v4, p0, Lcom/twitter/library/media/manager/j;->g:Z

    .line 53
    iput-boolean v4, p0, Lcom/twitter/library/media/manager/j;->j:Z

    .line 70
    iget-object v0, p1, Lcom/twitter/library/media/manager/j$a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/media/manager/j;->a:Ljava/lang/String;

    .line 71
    iget-object v0, p1, Lcom/twitter/library/media/manager/j$a;->d:Lcom/twitter/library/network/a;

    iput-object v0, p0, Lcom/twitter/library/media/manager/j;->b:Lcom/twitter/library/network/a;

    .line 72
    iget-object v0, p1, Lcom/twitter/library/media/manager/j$a;->e:Lcom/twitter/media/request/ResourceRequestType;

    iput-object v0, p0, Lcom/twitter/library/media/manager/j;->k:Lcom/twitter/media/request/ResourceRequestType;

    .line 73
    iget-object v0, p1, Lcom/twitter/library/media/manager/j$a;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/media/manager/j;->m:Ljava/lang/String;

    .line 74
    iget-object v0, p1, Lcom/twitter/library/media/manager/j$a;->g:Lcom/twitter/util/q;

    iput-object v0, p0, Lcom/twitter/library/media/manager/j;->l:Lcom/twitter/util/q;

    .line 75
    iget-object v0, p1, Lcom/twitter/library/media/manager/j$a;->h:Lcom/twitter/library/media/manager/i$c;

    iput-object v0, p0, Lcom/twitter/library/media/manager/j;->i:Lcom/twitter/library/media/manager/i$c;

    .line 77
    new-instance v0, Lcom/twitter/async/service/b;

    invoke-direct {v0}, Lcom/twitter/async/service/b;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/media/manager/j;->n:Lcom/twitter/async/service/b;

    .line 79
    iget-object v0, p0, Lcom/twitter/library/media/manager/j;->k:Lcom/twitter/media/request/ResourceRequestType;

    sget-object v1, Lcom/twitter/media/request/ResourceRequestType;->a:Lcom/twitter/media/request/ResourceRequestType;

    if-ne v0, v1, :cond_1

    .line 82
    sget-object v0, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->b:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    invoke-virtual {p0, v0}, Lcom/twitter/library/media/manager/j;->a(Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)V

    .line 83
    new-instance v0, Lcom/twitter/library/service/n;

    invoke-direct {v0}, Lcom/twitter/library/service/n;-><init>()V

    invoke-virtual {p0, v0}, Lcom/twitter/library/media/manager/j;->a(Lcom/twitter/async/service/k;)Lcom/twitter/async/service/AsyncOperation;

    .line 92
    :cond_0
    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/media/manager/j;->c:J

    .line 93
    return-void

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/media/manager/j;->k:Lcom/twitter/media/request/ResourceRequestType;

    sget-object v1, Lcom/twitter/media/request/ResourceRequestType;->b:Lcom/twitter/media/request/ResourceRequestType;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/library/media/manager/j;->k:Lcom/twitter/media/request/ResourceRequestType;

    sget-object v1, Lcom/twitter/media/request/ResourceRequestType;->c:Lcom/twitter/media/request/ResourceRequestType;

    if-ne v0, v1, :cond_0

    .line 89
    :cond_2
    sget-object v0, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->f:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    invoke-virtual {p0, v0}, Lcom/twitter/library/media/manager/j;->a(Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)V

    .line 90
    const v0, 0x7fffffff

    invoke-virtual {p0, v0}, Lcom/twitter/library/media/manager/j;->b(I)Lcom/twitter/async/service/AsyncOperation;

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/twitter/library/media/manager/j$a;Lcom/twitter/library/media/manager/j$1;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/twitter/library/media/manager/j;-><init>(Lcom/twitter/library/media/manager/j$a;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/media/manager/j;)Lcom/twitter/library/media/manager/i$c;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/twitter/library/media/manager/j;->i:Lcom/twitter/library/media/manager/i$c;

    return-object v0
.end method

.method private d(Lcom/twitter/async/service/j;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v12, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 187
    iget-boolean v0, p0, Lcom/twitter/library/media/manager/j;->g:Z

    if-nez v0, :cond_0

    const-string/jumbo v0, "resource_fetch_scribe_sample"

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeService;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v4

    .line 189
    :goto_0
    if-nez v0, :cond_1

    .line 222
    :goto_1
    return-void

    :cond_0
    move v0, v3

    .line 187
    goto :goto_0

    .line 193
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v6, p0, Lcom/twitter/library/media/manager/j;->c:J

    sub-long v6, v0, v6

    .line 194
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 195
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    .line 196
    if-eqz v1, :cond_3

    const-string/jumbo v1, "success"

    .line 197
    :goto_2
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v5, v2, -0x1

    .line 199
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/library/media/manager/j;->M()Lcom/twitter/library/service/v;

    move-result-object v8

    iget-wide v8, v8, Lcom/twitter/library/service/v;->c:J

    invoke-direct {v2, v8, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v8, v12, [Ljava/lang/String;

    const-string/jumbo v9, "app:twitter_service:media:downloaded"

    aput-object v9, v8, v3

    aput-object v1, v8, v4

    .line 200
    invoke-virtual {v2, v8}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 202
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v2

    invoke-virtual {v2}, Lcrr;->d()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 203
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v2

    invoke-virtual {v2}, Lcrr;->c()Z

    move-result v2

    if-eqz v2, :cond_4

    const-string/jumbo v2, "wifi"

    .line 206
    :goto_3
    invoke-static {v1}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 207
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v8

    .line 208
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->f()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    .line 209
    if-eqz v8, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->i()Ljava/net/URI;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 210
    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->i()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v8}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/model/ScribeLog;Ljava/lang/String;Lcom/twitter/network/l;)V

    .line 213
    :cond_2
    int-to-long v10, v5

    invoke-virtual {v1, v10, v11}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    .line 214
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 215
    if-eqz v8, :cond_6

    iget-object v0, v8, Lcom/twitter/network/l;->c:Ljava/lang/Exception;

    if-eqz v0, :cond_6

    iget-object v0, v8, Lcom/twitter/network/l;->c:Ljava/lang/Exception;

    .line 216
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 217
    :goto_4
    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string/jumbo v9, "total_duration_ms:%d,is_canceled:%b,content_length:%d,exception:%s,executed:%b"

    const/4 v2, 0x5

    new-array v10, v2, [Ljava/lang/Object;

    .line 219
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v10, v3

    invoke-virtual {p0}, Lcom/twitter/library/media/manager/j;->isCancelled()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v10, v4

    if-eqz v8, :cond_7

    iget v2, v8, Lcom/twitter/network/l;->k:I

    :goto_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v10, v12

    const/4 v2, 0x3

    aput-object v0, v10, v2

    const/4 v0, 0x4

    iget-boolean v2, p0, Lcom/twitter/library/media/manager/j;->j:Z

    .line 220
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v10, v0

    .line 217
    invoke-static {v5, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 221
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_1

    .line 196
    :cond_3
    const-string/jumbo v1, "failure"

    goto/16 :goto_2

    .line 203
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "cellular_"

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 204
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v8

    invoke-virtual {v8}, Lcrr;->b()I

    move-result v8

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    :cond_5
    const-string/jumbo v2, "disconnected"

    goto/16 :goto_3

    .line 216
    :cond_6
    const-string/jumbo v0, "none"

    goto :goto_4

    :cond_7
    move v2, v3

    .line 219
    goto :goto_5
.end method


# virtual methods
.method public a()Ljava/util/concurrent/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 237
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 238
    return-object p0
.end method

.method public a(Lcom/twitter/async/service/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 181
    invoke-direct {p0, p1}, Lcom/twitter/library/media/manager/j;->d(Lcom/twitter/async/service/j;)V

    .line 182
    iget-object v0, p0, Lcom/twitter/library/media/manager/j;->i:Lcom/twitter/library/media/manager/i$c;

    invoke-virtual {v0, p1}, Lcom/twitter/library/media/manager/i$c;->a(Lcom/twitter/async/service/j;)V

    .line 183
    return-void
.end method

.method protected a_(Lcom/twitter/library/service/u;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 147
    iput-boolean v0, p0, Lcom/twitter/library/media/manager/j;->j:Z

    .line 148
    iget-object v1, p0, Lcom/twitter/library/media/manager/j;->p:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/media/manager/j;->a:Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, Lcom/twitter/library/media/manager/j;->a(Landroid/content/Context;Ljava/lang/CharSequence;)Lcom/twitter/library/network/k;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/media/manager/j;->b:Lcom/twitter/library/network/a;

    .line 149
    invoke-virtual {v1, v2}, Lcom/twitter/library/network/k;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/k;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/media/manager/j$1;

    invoke-direct {v2, p0}, Lcom/twitter/library/media/manager/j$1;-><init>(Lcom/twitter/library/media/manager/j;)V

    .line 150
    invoke-virtual {v1, v2}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/j;)Lcom/twitter/library/network/k;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/media/manager/j;->n:Lcom/twitter/async/service/b;

    .line 165
    invoke-virtual {v1, v2}, Lcom/twitter/library/network/k;->a(Lcom/twitter/async/service/b;)Lcom/twitter/library/network/k;

    move-result-object v1

    const v2, 0xafc8

    .line 166
    invoke-virtual {v1, v2}, Lcom/twitter/library/network/k;->a(I)Lcom/twitter/library/network/k;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/media/manager/j;->l:Lcom/twitter/util/q;

    .line 167
    invoke-virtual {v1, v2}, Lcom/twitter/library/network/k;->a(Lcom/twitter/util/q;)Lcom/twitter/library/network/k;

    move-result-object v1

    .line 169
    invoke-virtual {v1, v0}, Lcom/twitter/library/network/k;->d(Z)Lcom/twitter/library/network/k;

    move-result-object v1

    .line 170
    invoke-virtual {p0}, Lcom/twitter/library/media/manager/j;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 171
    iget-object v2, p0, Lcom/twitter/library/media/manager/j;->k:Lcom/twitter/media/request/ResourceRequestType;

    sget-object v3, Lcom/twitter/media/request/ResourceRequestType;->b:Lcom/twitter/media/request/ResourceRequestType;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/twitter/library/media/manager/j;->k:Lcom/twitter/media/request/ResourceRequestType;

    sget-object v3, Lcom/twitter/media/request/ResourceRequestType;->c:Lcom/twitter/media/request/ResourceRequestType;

    if-ne v2, v3, :cond_2

    .line 174
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/library/media/manager/j;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/service/v;->c:J

    new-instance v4, Lcom/twitter/library/network/d;

    iget-object v5, p0, Lcom/twitter/library/media/manager/j;->m:Ljava/lang/String;

    invoke-direct {v4, v0, v5}, Lcom/twitter/library/network/d;-><init>(ZLjava/lang/String;)V

    .line 173
    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/network/k;->a(JLcom/twitter/library/network/c;)Lcom/twitter/library/network/k;

    .line 176
    :cond_1
    invoke-virtual {v1}, Lcom/twitter/library/network/k;->a()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->c()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/u;->a(Lcom/twitter/network/HttpOperation;)V

    .line 177
    return-void

    .line 171
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Lcom/twitter/library/service/u;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 113
    iget-object v0, p0, Lcom/twitter/library/media/manager/j;->i:Lcom/twitter/library/media/manager/i$c;

    invoke-virtual {v0, p1}, Lcom/twitter/library/media/manager/i$c;->a(Lcom/twitter/library/service/u;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    invoke-virtual {p0, v4}, Lcom/twitter/library/media/manager/j;->cancel(Z)Z

    move v0, v1

    .line 142
    :goto_0
    return v0

    .line 119
    :cond_0
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v0

    invoke-virtual {v0}, Lcrr;->c()Z

    move-result v0

    .line 120
    iget-object v2, p0, Lcom/twitter/library/media/manager/j;->k:Lcom/twitter/media/request/ResourceRequestType;

    sget-object v3, Lcom/twitter/media/request/ResourceRequestType;->b:Lcom/twitter/media/request/ResourceRequestType;

    if-ne v2, v3, :cond_1

    if-nez v0, :cond_1

    .line 121
    invoke-virtual {p0, v4}, Lcom/twitter/library/media/manager/j;->cancel(Z)Z

    move v0, v1

    .line 122
    goto :goto_0

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/media/manager/j;->h:Lcom/twitter/library/media/manager/j;

    if-eqz v0, :cond_3

    .line 128
    iget-object v0, p0, Lcom/twitter/library/media/manager/j;->h:Lcom/twitter/library/media/manager/j;

    invoke-virtual {v0}, Lcom/twitter/library/media/manager/j;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    .line 129
    if-eqz v0, :cond_2

    .line 130
    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 131
    if-eqz v0, :cond_2

    .line 132
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->f()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    .line 133
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 134
    invoke-virtual {p1, v0}, Lcom/twitter/library/service/u;->a(Lcom/twitter/network/HttpOperation;)V

    .line 135
    iput-boolean v4, p0, Lcom/twitter/library/media/manager/j;->g:Z

    move v0, v1

    .line 136
    goto :goto_0

    .line 140
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/media/manager/j;->h:Lcom/twitter/library/media/manager/j;

    .line 142
    :cond_3
    invoke-super {p0, p1}, Lcom/twitter/library/service/s;->b(Lcom/twitter/library/service/u;)Z

    move-result v0

    goto :goto_0
.end method

.method public c(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 104
    instance-of v0, p1, Lcom/twitter/library/media/manager/j;

    if-eqz v0, :cond_0

    .line 106
    check-cast p1, Lcom/twitter/library/media/manager/j;

    iput-object p1, p0, Lcom/twitter/library/media/manager/j;->h:Lcom/twitter/library/media/manager/j;

    .line 108
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public m()Lcom/twitter/async/service/b;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/twitter/library/media/manager/j;->n:Lcom/twitter/async/service/b;

    return-object v0
.end method

.method protected o()Ljava/lang/String;
    .locals 2

    .prologue
    .line 232
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "resource_fetch_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/media/manager/j;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/media/manager/j;->k:Lcom/twitter/media/request/ResourceRequestType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
