.class public abstract Lcom/twitter/library/media/manager/k;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/media/manager/k$c;,
        Lcom/twitter/library/media/manager/k$d;,
        Lcom/twitter/library/media/manager/k$b;,
        Lcom/twitter/library/media/manager/k$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<REQ:",
        "Lcom/twitter/media/request/b;",
        "RES:",
        "Ljava/lang/Object;",
        "RESP:",
        "Lcom/twitter/media/request/ResourceResponse",
        "<TREQ;TRES;>;>",
        "Ljava/lang/Object;",
        "Ljava/io/Closeable;"
    }
.end annotation


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Lcom/twitter/util/collection/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/g",
            "<",
            "Ljava/lang/String;",
            "TRES;>;"
        }
    .end annotation
.end field

.field protected final c:Lcpy;

.field protected final d:Lcpy;

.field protected final e:Landroid/os/Looper;

.field protected f:Lcom/twitter/library/media/manager/k$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/library/media/manager/k$b",
            "<TREQ;TRES;TRESP;>;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/media/request/ResourceResponse$ResourceSource;",
            "Lcom/twitter/metrics/c;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/twitter/library/media/manager/i$b;


# direct methods
.method protected constructor <init>(Lcom/twitter/library/media/manager/k$a;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/media/manager/k$a",
            "<TRES;**>;)V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iget-object v0, p1, Lcom/twitter/library/media/manager/k$a;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/media/manager/k;->a:Landroid/content/Context;

    .line 91
    iget-object v0, p1, Lcom/twitter/library/media/manager/k$a;->f:Lcom/twitter/util/collection/g;

    iput-object v0, p0, Lcom/twitter/library/media/manager/k;->b:Lcom/twitter/util/collection/g;

    .line 92
    iget-object v0, p1, Lcom/twitter/library/media/manager/k$a;->g:Lcpy;

    iput-object v0, p0, Lcom/twitter/library/media/manager/k;->c:Lcpy;

    .line 93
    iget-object v0, p1, Lcom/twitter/library/media/manager/k$a;->h:Lcpy;

    iput-object v0, p0, Lcom/twitter/library/media/manager/k;->d:Lcpy;

    .line 94
    iget-object v0, p1, Lcom/twitter/library/media/manager/k$a;->e:Landroid/os/Looper;

    iput-object v0, p0, Lcom/twitter/library/media/manager/k;->e:Landroid/os/Looper;

    .line 95
    iget-object v0, p1, Lcom/twitter/library/media/manager/k$a;->i:Lcom/twitter/library/media/manager/i$b;

    iput-object v0, p0, Lcom/twitter/library/media/manager/k;->h:Lcom/twitter/library/media/manager/i$b;

    .line 98
    invoke-static {}, Lcom/twitter/media/request/ResourceResponse$ResourceSource;->values()[Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    move-result-object v7

    .line 99
    new-instance v0, Ljava/util/HashMap;

    array-length v1, v7

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/library/media/manager/k;->g:Ljava/util/Map;

    .line 100
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v1

    .line 101
    array-length v8, v7

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v8, :cond_0

    aget-object v9, v7, v6

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "media:fetcher:source:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p1, Lcom/twitter/library/media/manager/k$a;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v9}, Lcom/twitter/media/request/ResourceResponse$ResourceSource;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 104
    const-wide/16 v2, 0x0

    sget-object v4, Lcom/twitter/metrics/g;->n:Lcom/twitter/metrics/g$b;

    const/4 v5, 0x3

    invoke-static/range {v0 .. v5}, Lcom/twitter/metrics/c;->a(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;I)Lcom/twitter/metrics/c;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lcom/twitter/metrics/c;->i()V

    .line 107
    iget-object v2, p0, Lcom/twitter/library/media/manager/k;->g:Ljava/util/Map;

    invoke-interface {v2, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 109
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract a(Lcom/twitter/media/request/b;Ljava/lang/Object;Lcom/twitter/media/request/ResourceResponse$ResourceSource;)Lcom/twitter/media/request/ResourceResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TREQ;TRES;",
            "Lcom/twitter/media/request/ResourceResponse$ResourceSource;",
            ")TRESP;"
        }
    .end annotation
.end method

.method public a()Lcom/twitter/util/collection/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/collection/g",
            "<",
            "Ljava/lang/String;",
            "TRES;>;"
        }
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->b:Lcom/twitter/util/collection/g;

    return-object v0
.end method

.method public a(Lcom/twitter/media/request/b;)Ljava/io/File;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TREQ;)",
            "Ljava/io/File;"
        }
    .end annotation

    .prologue
    .line 328
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 329
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->d:Lcpy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->d:Lcpy;

    invoke-virtual {p1}, Lcom/twitter/media/request/b;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcpy;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/media/request/b;Ljava/io/File;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TREQ;",
            "Ljava/io/File;",
            ")TRES;"
        }
    .end annotation

    .prologue
    .line 248
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TRES;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 142
    iget-object v1, p0, Lcom/twitter/library/media/manager/k;->b:Lcom/twitter/util/collection/g;

    if-nez v1, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-object v0

    .line 145
    :cond_1
    iget-object v1, p0, Lcom/twitter/library/media/manager/k;->b:Lcom/twitter/util/collection/g;

    invoke-interface {v1, p1}, Lcom/twitter/util/collection/g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 146
    if-eqz v1, :cond_0

    .line 149
    invoke-virtual {p0, v1}, Lcom/twitter/library/media/manager/k;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 150
    goto :goto_0

    .line 154
    :cond_2
    iget-object v1, p0, Lcom/twitter/library/media/manager/k;->b:Lcom/twitter/util/collection/g;

    invoke-interface {v1, p1}, Lcom/twitter/util/collection/g;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    new-instance v1, Lcpb;

    invoke-direct {v1}, Lcpb;-><init>()V

    const-string/jumbo v2, "Resource Request Key"

    .line 159
    invoke-virtual {v1, v2, p1}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v1

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "Cached resource was unexpectedly invalidated by external code. For bitmaps, do not call Bitmap.recycle() on images returned by MediaManager."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 160
    invoke-virtual {v1, v2}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v1

    .line 164
    invoke-static {v1}, Lcpd;->c(Lcpb;)V

    goto :goto_0
.end method

.method a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TRES;)TRES;"
        }
    .end annotation

    .prologue
    .line 267
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->b:Lcom/twitter/util/collection/g;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->b:Lcom/twitter/util/collection/g;

    .line 268
    invoke-interface {v0, p1, p2}, Lcom/twitter/util/collection/g;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 267
    :goto_0
    return-object v0

    .line 268
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/util/Collection;)Ljava/util/concurrent/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 222
    if-eqz p1, :cond_0

    .line 223
    new-instance v0, Lcom/twitter/library/media/manager/k$c;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/media/manager/k$c;-><init>(Lcom/twitter/library/media/manager/k;Ljava/util/Collection;)V

    .line 224
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 227
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Lcom/twitter/media/request/ResourceResponse$ResourceSource;)V
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/metrics/c;

    invoke-virtual {v0}, Lcom/twitter/metrics/c;->h()V

    .line 363
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/io/File;)V
    .locals 1

    .prologue
    .line 273
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 275
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->c:Lcpy;

    invoke-virtual {v0, p1, p2}, Lcpy;->a(Ljava/lang/String;Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 279
    :goto_0
    return-void

    .line 276
    :catch_0
    move-exception v0

    .line 277
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method a(Lcom/twitter/media/request/b;Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TREQ;TRES;)Z"
        }
    .end annotation

    .prologue
    .line 334
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 335
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->d:Lcpy;

    if-eqz v0, :cond_0

    .line 337
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->d:Lcpy;

    invoke-virtual {p1}, Lcom/twitter/media/request/b;->r()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/media/manager/k$1;

    invoke-direct {v2, p0, p1}, Lcom/twitter/library/media/manager/k$1;-><init>(Lcom/twitter/library/media/manager/k;Lcom/twitter/media/request/b;)V

    invoke-virtual {v0, v1, p2, v2}, Lcpy;->a(Ljava/lang/String;Ljava/lang/Object;Lcpy$b;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 348
    :goto_0
    return v0

    .line 344
    :catch_0
    move-exception v0

    .line 345
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 348
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/media/request/b;Ljava/lang/Object;Ljava/io/OutputStream;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TREQ;TRES;",
            "Ljava/io/OutputStream;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 262
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract a(Ljava/lang/Object;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRES;)Z"
        }
    .end annotation
.end method

.method protected declared-synchronized b()Lcom/twitter/library/media/manager/k$b;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/media/manager/k$b",
            "<TREQ;TRES;TRESP;>;"
        }
    .end annotation

    .prologue
    .line 353
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->f:Lcom/twitter/library/media/manager/k$b;

    if-nez v0, :cond_0

    .line 355
    new-instance v0, Lcom/twitter/library/media/manager/k$b;

    iget-object v1, p0, Lcom/twitter/library/media/manager/k;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/media/manager/k;->e:Landroid/os/Looper;

    iget-object v3, p0, Lcom/twitter/library/media/manager/k;->h:Lcom/twitter/library/media/manager/i$b;

    invoke-direct {v0, v1, v2, p0, v3}, Lcom/twitter/library/media/manager/k$b;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/twitter/library/media/manager/k;Lcom/twitter/library/media/manager/i$b;)V

    iput-object v0, p0, Lcom/twitter/library/media/manager/k;->f:Lcom/twitter/library/media/manager/k$b;

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->f:Lcom/twitter/library/media/manager/k$b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 353
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Lcom/twitter/media/request/b;)Ljava/io/File;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TREQ;)",
            "Ljava/io/File;"
        }
    .end annotation

    .prologue
    .line 321
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 322
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->c:Lcpy;

    invoke-virtual {p1}, Lcom/twitter/media/request/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcpy;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method protected b(Lcom/twitter/media/request/b;Ljava/io/File;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TREQ;",
            "Ljava/io/File;",
            ")TRES;"
        }
    .end annotation

    .prologue
    .line 237
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 238
    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/media/manager/k;->a(Lcom/twitter/media/request/b;Ljava/io/File;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 205
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->b:Lcom/twitter/util/collection/g;

    if-eqz v0, :cond_1

    .line 206
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->b:Lcom/twitter/util/collection/g;

    invoke-interface {v0}, Lcom/twitter/util/collection/g;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 207
    invoke-virtual {v0, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 208
    iget-object v2, p0, Lcom/twitter/library/media/manager/k;->b:Lcom/twitter/util/collection/g;

    invoke-interface {v2, v0}, Lcom/twitter/util/collection/g;->b(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 212
    :cond_1
    return-void
.end method

.method c(Lcom/twitter/media/request/b;)Landroid/util/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TREQ;)",
            "Landroid/util/Pair",
            "<",
            "Lcom/twitter/media/request/ResourceResponse$ResourceSource;",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 284
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 285
    invoke-virtual {p0, p1}, Lcom/twitter/library/media/manager/k;->a(Lcom/twitter/media/request/b;)Ljava/io/File;

    move-result-object v0

    .line 286
    if-eqz v0, :cond_0

    .line 287
    sget-object v1, Lcom/twitter/media/request/ResourceResponse$ResourceSource;->c:Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 303
    :goto_0
    return-object v0

    .line 289
    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/library/media/manager/k;->b(Lcom/twitter/media/request/b;)Ljava/io/File;

    move-result-object v0

    .line 290
    if-eqz v0, :cond_1

    .line 291
    sget-object v1, Lcom/twitter/media/request/ResourceResponse$ResourceSource;->d:Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    .line 293
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->a:Landroid/content/Context;

    invoke-virtual {p1, v0}, Lcom/twitter/media/request/b;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 294
    if-eqz v0, :cond_2

    .line 295
    sget-object v1, Lcom/twitter/media/request/ResourceResponse$ResourceSource;->e:Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    .line 297
    :cond_2
    invoke-virtual {p1}, Lcom/twitter/media/request/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/ac;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 298
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/twitter/media/request/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/ac;->c(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/File;

    move-result-object v0

    .line 299
    if-eqz v0, :cond_3

    .line 300
    sget-object v1, Lcom/twitter/media/request/ResourceResponse$ResourceSource;->e:Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    .line 303
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->f:Lcom/twitter/library/media/manager/k$b;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->f:Lcom/twitter/library/media/manager/k$b;

    invoke-virtual {v0}, Lcom/twitter/library/media/manager/k$b;->a()Ljava/util/concurrent/Future;

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->c:Lcpy;

    invoke-virtual {v0}, Lcpy;->close()V

    .line 117
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->d:Lcpy;

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->d:Lcpy;

    invoke-virtual {v0}, Lcpy;->close()V

    .line 120
    :cond_1
    return-void
.end method

.method public d(Lcom/twitter/media/request/b;)Lcom/twitter/util/concurrent/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TREQ;)",
            "Lcom/twitter/util/concurrent/g",
            "<TRESP;>;"
        }
    .end annotation

    .prologue
    .line 193
    if-eqz p1, :cond_0

    .line 194
    invoke-virtual {p0}, Lcom/twitter/library/media/manager/k;->b()Lcom/twitter/library/media/manager/k$b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/library/media/manager/k$b;->a(Lcom/twitter/media/request/b;)Lcom/twitter/util/concurrent/g;

    move-result-object v0

    .line 196
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/twitter/util/concurrent/ObservablePromise;->a(Ljava/lang/Object;)Lcom/twitter/util/concurrent/ObservablePromise;

    move-result-object v0

    goto :goto_0
.end method

.method public e(Lcom/twitter/media/request/b;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TREQ;)TRES;"
        }
    .end annotation

    .prologue
    .line 134
    invoke-virtual {p1}, Lcom/twitter/media/request/b;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    invoke-virtual {p1}, Lcom/twitter/media/request/b;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/media/manager/k;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 137
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method f(Lcom/twitter/media/request/b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TREQ;)V"
        }
    .end annotation

    .prologue
    .line 308
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 309
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->d:Lcpy;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->d:Lcpy;

    invoke-virtual {p1}, Lcom/twitter/media/request/b;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcpy;->d(Ljava/lang/String;)Z

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->b:Lcom/twitter/util/collection/g;

    if-eqz v0, :cond_1

    .line 313
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->b:Lcom/twitter/util/collection/g;

    invoke-virtual {p1}, Lcom/twitter/media/request/b;->r()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/util/collection/g;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/media/manager/k;->c:Lcpy;

    invoke-virtual {p1}, Lcom/twitter/media/request/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcpy;->d(Ljava/lang/String;)Z

    .line 316
    return-void
.end method
