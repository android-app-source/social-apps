.class Lcom/twitter/library/media/manager/k$b$4;
.super Lcom/twitter/library/media/manager/i$c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/media/manager/k$b;->a(Lcom/twitter/library/media/manager/k$b$e;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/media/manager/h;

.field final synthetic b:Lcom/twitter/library/media/manager/h;

.field final synthetic c:Lcom/twitter/media/request/ResourceRequestType;

.field final synthetic d:Lcpy;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Lcom/twitter/library/media/manager/k$b;


# direct methods
.method constructor <init>(Lcom/twitter/library/media/manager/k$b;Lcom/twitter/library/media/manager/h;Lcom/twitter/library/media/manager/h;Lcom/twitter/media/request/ResourceRequestType;Lcpy;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 687
    iput-object p1, p0, Lcom/twitter/library/media/manager/k$b$4;->g:Lcom/twitter/library/media/manager/k$b;

    iput-object p2, p0, Lcom/twitter/library/media/manager/k$b$4;->a:Lcom/twitter/library/media/manager/h;

    iput-object p3, p0, Lcom/twitter/library/media/manager/k$b$4;->b:Lcom/twitter/library/media/manager/h;

    iput-object p4, p0, Lcom/twitter/library/media/manager/k$b$4;->c:Lcom/twitter/media/request/ResourceRequestType;

    iput-object p5, p0, Lcom/twitter/library/media/manager/k$b$4;->d:Lcpy;

    iput-object p6, p0, Lcom/twitter/library/media/manager/k$b$4;->e:Ljava/lang/String;

    iput-object p7, p0, Lcom/twitter/library/media/manager/k$b$4;->f:Ljava/lang/String;

    invoke-direct {p0}, Lcom/twitter/library/media/manager/i$c;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/async/service/j;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 700
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$4;->b:Lcom/twitter/library/media/manager/h;

    invoke-virtual {v0}, Lcom/twitter/library/media/manager/h;->j()V

    .line 701
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$4;->b:Lcom/twitter/library/media/manager/h;

    invoke-virtual {v0}, Lcom/twitter/library/media/manager/h;->bp_()V

    .line 706
    invoke-virtual {p1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 707
    if-eqz v0, :cond_0

    .line 708
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v0

    .line 709
    :goto_0
    new-instance v2, Lcom/twitter/library/media/manager/k$b$c;

    iget-object v3, p0, Lcom/twitter/library/media/manager/k$b$4;->f:Ljava/lang/String;

    invoke-direct {v2, v3, v0, v1}, Lcom/twitter/library/media/manager/k$b$c;-><init>(Ljava/lang/String;Lcom/twitter/network/l;Lcom/twitter/library/media/manager/k$1;)V

    .line 710
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$4;->g:Lcom/twitter/library/media/manager/k$b;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/media/manager/k$b;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 711
    return-void

    :cond_0
    move-object v0, v1

    .line 708
    goto :goto_0
.end method

.method public a(Ljava/io/InputStream;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 716
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$4;->d:Lcpy;

    iget-object v1, p0, Lcom/twitter/library/media/manager/k$b$4;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcpy;->a(Ljava/lang/String;Ljava/io/InputStream;)Z

    .line 717
    return-void
.end method

.method public a(Lcom/twitter/library/service/u;)Z
    .locals 2

    .prologue
    .line 690
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$4;->a:Lcom/twitter/library/media/manager/h;

    invoke-virtual {v0}, Lcom/twitter/library/media/manager/h;->j()V

    .line 691
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$4;->a:Lcom/twitter/library/media/manager/h;

    invoke-virtual {v0}, Lcom/twitter/library/media/manager/h;->bp_()V

    .line 692
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$4;->b:Lcom/twitter/library/media/manager/h;

    invoke-virtual {v0}, Lcom/twitter/library/media/manager/h;->i()V

    .line 694
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$4;->c:Lcom/twitter/media/request/ResourceRequestType;

    sget-object v1, Lcom/twitter/media/request/ResourceRequestType;->a:Lcom/twitter/media/request/ResourceRequestType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$4;->d:Lcpy;

    iget-object v1, p0, Lcom/twitter/library/media/manager/k$b$4;->e:Ljava/lang/String;

    .line 695
    invoke-virtual {v0, v1}, Lcpy;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 694
    :goto_0
    return v0

    .line 695
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
