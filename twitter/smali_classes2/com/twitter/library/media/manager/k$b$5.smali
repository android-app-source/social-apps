.class Lcom/twitter/library/media/manager/k$b$5;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/media/manager/k$b;->a(Lcom/twitter/library/media/manager/k$b$a;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/media/manager/k;

.field final synthetic b:Lcom/twitter/library/media/manager/k$b$a;

.field final synthetic c:Ljava/lang/Object;

.field final synthetic d:Lcom/twitter/library/media/manager/k$b;


# direct methods
.method constructor <init>(Lcom/twitter/library/media/manager/k$b;Lcom/twitter/library/media/manager/k;Lcom/twitter/library/media/manager/k$b$a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 822
    iput-object p1, p0, Lcom/twitter/library/media/manager/k$b$5;->d:Lcom/twitter/library/media/manager/k$b;

    iput-object p2, p0, Lcom/twitter/library/media/manager/k$b$5;->a:Lcom/twitter/library/media/manager/k;

    iput-object p3, p0, Lcom/twitter/library/media/manager/k$b$5;->b:Lcom/twitter/library/media/manager/k$b$a;

    iput-object p4, p0, Lcom/twitter/library/media/manager/k$b$5;->c:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 825
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$5;->a:Lcom/twitter/library/media/manager/k;

    if-eqz v0, :cond_1

    .line 829
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$5;->b:Lcom/twitter/library/media/manager/k$b$a;

    iget-object v0, v0, Lcom/twitter/library/media/manager/k$b$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$b$e;

    .line 830
    iget-object v2, v0, Lcom/twitter/library/media/manager/k$b$e;->a:Lcom/twitter/media/request/b;

    .line 831
    iget-object v3, p0, Lcom/twitter/library/media/manager/k$b$5;->a:Lcom/twitter/library/media/manager/k;

    iget-object v4, p0, Lcom/twitter/library/media/manager/k$b$5;->c:Ljava/lang/Object;

    iget-object v5, v0, Lcom/twitter/library/media/manager/k$b$e;->c:Lcom/twitter/media/request/ResourceResponse$ResourceSource;

    invoke-virtual {v3, v2, v4, v5}, Lcom/twitter/library/media/manager/k;->a(Lcom/twitter/media/request/b;Ljava/lang/Object;Lcom/twitter/media/request/ResourceResponse$ResourceSource;)Lcom/twitter/media/request/ResourceResponse;

    move-result-object v3

    .line 832
    iget-object v4, v0, Lcom/twitter/library/media/manager/k$b$e;->b:Lcom/twitter/util/concurrent/h;

    invoke-virtual {v4, v3}, Lcom/twitter/util/concurrent/h;->set(Ljava/lang/Object;)V

    .line 833
    iget-object v0, v0, Lcom/twitter/library/media/manager/k$b$e;->b:Lcom/twitter/util/concurrent/h;

    invoke-virtual {v0}, Lcom/twitter/util/concurrent/h;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 834
    invoke-virtual {v2}, Lcom/twitter/media/request/b;->C()Lcom/twitter/media/request/b$b;

    move-result-object v0

    .line 835
    if-eqz v0, :cond_0

    .line 836
    invoke-interface {v0, v3}, Lcom/twitter/media/request/b$b;->a(Lcom/twitter/media/request/ResourceResponse;)V

    goto :goto_0

    .line 841
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$b$5;->b:Lcom/twitter/library/media/manager/k$b$a;

    invoke-virtual {v0}, Lcom/twitter/library/media/manager/k$b$a;->b()V

    .line 842
    return-void
.end method
