.class public abstract Lcom/twitter/library/media/manager/i$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/media/manager/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/library/media/manager/i;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:Landroid/content/Context;

.field c:Lcnz;

.field d:Lcom/twitter/library/network/a;

.field e:Lcom/twitter/media/request/ResourceRequestType;

.field f:Ljava/lang/String;

.field g:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field h:Lcom/twitter/library/media/manager/i$c;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/library/media/manager/i$a;->b:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/media/manager/i$a;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/media/manager/i$a;->e:Lcom/twitter/media/request/ResourceRequestType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/media/manager/i$a;->h:Lcom/twitter/library/media/manager/i$c;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)Lcom/twitter/library/media/manager/i$a;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/twitter/library/media/manager/i$a;->b:Landroid/content/Context;

    .line 39
    return-object p0
.end method

.method public a(Lcnz;)Lcom/twitter/library/media/manager/i$a;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/twitter/library/media/manager/i$a;->c:Lcnz;

    .line 51
    return-object p0
.end method

.method public a(Lcom/twitter/library/media/manager/i$c;)Lcom/twitter/library/media/manager/i$a;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/twitter/library/media/manager/i$a;->h:Lcom/twitter/library/media/manager/i$c;

    .line 80
    return-object p0
.end method

.method public a(Lcom/twitter/library/network/a;)Lcom/twitter/library/media/manager/i$a;
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/twitter/library/media/manager/i$a;->d:Lcom/twitter/library/network/a;

    .line 57
    return-object p0
.end method

.method public a(Lcom/twitter/media/request/ResourceRequestType;)Lcom/twitter/library/media/manager/i$a;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/twitter/library/media/manager/i$a;->e:Lcom/twitter/media/request/ResourceRequestType;

    .line 63
    return-object p0
.end method

.method public a(Lcom/twitter/util/q;)Lcom/twitter/library/media/manager/i$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/q",
            "<",
            "Ljava/lang/Double;",
            ">;)",
            "Lcom/twitter/library/media/manager/i$a;"
        }
    .end annotation

    .prologue
    .line 73
    iput-object p1, p0, Lcom/twitter/library/media/manager/i$a;->g:Lcom/twitter/util/q;

    .line 74
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/media/manager/i$a;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/twitter/library/media/manager/i$a;->a:Ljava/lang/String;

    .line 45
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/library/media/manager/i$a;
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/twitter/library/media/manager/i$a;->f:Ljava/lang/String;

    .line 68
    return-object p0
.end method
