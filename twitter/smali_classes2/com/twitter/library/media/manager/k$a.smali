.class public abstract Lcom/twitter/library/media/manager/k$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/media/manager/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RES:",
        "Ljava/lang/Object;",
        "FETCHER:",
        "Lcom/twitter/library/media/manager/k",
        "<*TRES;*>;B:",
        "Lcom/twitter/library/media/manager/k$a;",
        ">",
        "Lcom/twitter/util/object/i",
        "<TFETCHER;>;"
    }
.end annotation


# instance fields
.field c:Ljava/lang/String;

.field d:Landroid/content/Context;

.field e:Landroid/os/Looper;

.field f:Lcom/twitter/util/collection/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/g",
            "<",
            "Ljava/lang/String;",
            "TRES;>;"
        }
    .end annotation
.end field

.field g:Lcpy;

.field h:Lcpy;

.field i:Lcom/twitter/library/media/manager/i$b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 365
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Lcom/twitter/library/media/manager/k$a;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/media/manager/k$a;->d:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/media/manager/k$a;->e:Landroid/os/Looper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/media/manager/k$a;->g:Lcpy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/media/manager/k$a;->i:Lcom/twitter/library/media/manager/i$b;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)Lcom/twitter/library/media/manager/k$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 383
    iput-object p1, p0, Lcom/twitter/library/media/manager/k$a;->d:Landroid/content/Context;

    .line 384
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$a;

    return-object v0
.end method

.method public a(Landroid/os/Looper;)Lcom/twitter/library/media/manager/k$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Looper;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 389
    iput-object p1, p0, Lcom/twitter/library/media/manager/k$a;->e:Landroid/os/Looper;

    .line 390
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$a;

    return-object v0
.end method

.method public a(Lcom/twitter/library/media/manager/i$b;)Lcom/twitter/library/media/manager/k$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/media/manager/i$b;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 414
    iput-object p1, p0, Lcom/twitter/library/media/manager/k$a;->i:Lcom/twitter/library/media/manager/i$b;

    .line 415
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$a;

    return-object v0
.end method

.method public a(Lcom/twitter/util/collection/g;)Lcom/twitter/library/media/manager/k$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/collection/g",
            "<",
            "Ljava/lang/String;",
            "TRES;>;)TB;"
        }
    .end annotation

    .prologue
    .line 395
    iput-object p1, p0, Lcom/twitter/library/media/manager/k$a;->f:Lcom/twitter/util/collection/g;

    .line 396
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$a;

    return-object v0
.end method

.method public a(Lcpy;)Lcom/twitter/library/media/manager/k$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcpy;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 401
    iput-object p1, p0, Lcom/twitter/library/media/manager/k$a;->g:Lcpy;

    .line 402
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$a;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/media/manager/k$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 377
    iput-object p1, p0, Lcom/twitter/library/media/manager/k$a;->c:Ljava/lang/String;

    .line 378
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$a;

    return-object v0
.end method

.method public b(Lcpy;)Lcom/twitter/library/media/manager/k$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcpy;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 407
    iput-object p1, p0, Lcom/twitter/library/media/manager/k$a;->h:Lcpy;

    .line 408
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/media/manager/k$a;

    return-object v0
.end method
