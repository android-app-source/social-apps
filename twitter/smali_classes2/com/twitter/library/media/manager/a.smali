.class public Lcom/twitter/library/media/manager/a;
.super Lcom/twitter/library/media/manager/k;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/media/manager/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/media/manager/k",
        "<",
        "Lcom/twitter/library/media/manager/b;",
        "Lbyu;",
        "Lcom/twitter/library/media/manager/c;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/twitter/library/media/manager/a$a;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/twitter/library/media/manager/k;-><init>(Lcom/twitter/library/media/manager/k$a;)V

    .line 22
    return-void
.end method

.method private static a(Lcom/twitter/media/model/MediaFile;Ljava/lang/String;)Lbyu;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    invoke-static {p0, p1}, Lbyr;->a(Lcom/twitter/media/model/MediaFile;Ljava/lang/String;)Lbyu;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a(Lcom/twitter/library/media/manager/b;Ljava/io/File;)Lbyu;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 44
    :try_start_0
    iget-object v1, p1, Lcom/twitter/library/media/manager/b;->a:Lcom/twitter/media/model/MediaFile;

    .line 45
    if-nez v1, :cond_0

    .line 46
    sget-object v1, Lcom/twitter/media/model/MediaType;->a:Lcom/twitter/media/model/MediaType;

    invoke-static {p2, v1}, Lcom/twitter/media/model/MediaFile;->a(Ljava/io/File;Lcom/twitter/media/model/MediaType;)Lcom/twitter/media/model/MediaFile;

    move-result-object v1

    .line 47
    if-nez v1, :cond_0

    .line 57
    :goto_0
    return-object v0

    .line 51
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/media/manager/b;->r()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/twitter/library/media/manager/a;->a(Lcom/twitter/media/model/MediaFile;Ljava/lang/String;)Lbyu;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 52
    :catch_0
    move-exception v1

    .line 53
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v2

    invoke-virtual {v2}, Lcof;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 54
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 56
    :cond_1
    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/media/manager/b;Lbyu;Lcom/twitter/media/request/ResourceResponse$ResourceSource;)Lcom/twitter/library/media/manager/c;
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/twitter/library/media/manager/c;

    invoke-direct {v0, p1, p2, p3}, Lcom/twitter/library/media/manager/c;-><init>(Lcom/twitter/library/media/manager/b;Lbyu;Lcom/twitter/media/request/ResourceResponse$ResourceSource;)V

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/media/request/b;Ljava/lang/Object;Lcom/twitter/media/request/ResourceResponse$ResourceSource;)Lcom/twitter/media/request/ResourceResponse;
    .locals 1

    .prologue
    .line 19
    check-cast p1, Lcom/twitter/library/media/manager/b;

    check-cast p2, Lbyu;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/media/manager/a;->a(Lcom/twitter/library/media/manager/b;Lbyu;Lcom/twitter/media/request/ResourceResponse$ResourceSource;)Lcom/twitter/library/media/manager/c;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/media/request/b;Ljava/io/File;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    check-cast p1, Lcom/twitter/library/media/manager/b;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/media/manager/a;->a(Lcom/twitter/library/media/manager/b;Ljava/io/File;)Lbyu;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lbyu;)Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x1

    return v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 19
    check-cast p1, Lbyu;

    invoke-virtual {p0, p1}, Lcom/twitter/library/media/manager/a;->a(Lbyu;)Z

    move-result v0

    return v0
.end method
