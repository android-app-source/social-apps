.class public Lcom/twitter/library/media/manager/e;
.super Lcom/twitter/library/media/manager/k;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/media/manager/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/media/manager/k",
        "<",
        "Lcom/twitter/media/request/a;",
        "Landroid/graphics/Bitmap;",
        "Lcom/twitter/media/request/ImageResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/media/model/MediaFile;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/media/request/ImageResponse$Error;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/twitter/util/math/Size;

.field private final j:I


# direct methods
.method public constructor <init>(Lcom/twitter/library/media/manager/e$a;)V
    .locals 3

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/twitter/library/media/manager/k;-><init>(Lcom/twitter/library/media/manager/k$a;)V

    .line 41
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/media/manager/e;->h:Ljava/util/Map;

    .line 47
    iget-object v0, p1, Lcom/twitter/library/media/manager/e$a;->a:Lcom/twitter/util/math/Size;

    iput-object v0, p0, Lcom/twitter/library/media/manager/e;->i:Lcom/twitter/util/math/Size;

    .line 48
    iget v0, p1, Lcom/twitter/library/media/manager/e$a;->b:I

    iput v0, p0, Lcom/twitter/library/media/manager/e;->j:I

    .line 49
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 50
    iput-object v1, p0, Lcom/twitter/library/media/manager/e;->g:Ljava/util/Map;

    .line 52
    iget-object v0, p1, Lcom/twitter/library/media/manager/e$a;->f:Lcom/twitter/util/collection/g;

    .line 53
    if-eqz v0, :cond_0

    instance-of v2, v0, Lcom/twitter/util/collection/e;

    if-eqz v2, :cond_0

    .line 54
    check-cast v0, Lcom/twitter/util/collection/e;

    new-instance v2, Lcom/twitter/library/media/manager/e$1;

    invoke-direct {v2, p0, v1}, Lcom/twitter/library/media/manager/e$1;-><init>(Lcom/twitter/library/media/manager/e;Ljava/util/Map;)V

    invoke-virtual {v0, v2}, Lcom/twitter/util/collection/e;->a(Lcom/twitter/util/collection/e$b;)V

    .line 62
    :cond_0
    return-void
.end method

.method private c(Lcom/twitter/media/request/a;Ljava/io/File;)Lcom/twitter/media/model/MediaFile;
    .locals 3

    .prologue
    .line 171
    iget-object v0, p0, Lcom/twitter/library/media/manager/e;->g:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/twitter/media/request/a;->r()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaFile;

    .line 172
    if-nez v0, :cond_1

    .line 173
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->d()Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    .line 174
    if-nez v0, :cond_3

    if-eqz p2, :cond_3

    .line 175
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->m()Lcom/twitter/media/model/MediaType;

    move-result-object v0

    sget-object v1, Lcom/twitter/media/model/MediaType;->e:Lcom/twitter/media/model/MediaType;

    if-ne v0, v1, :cond_2

    .line 176
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->e()Lcom/twitter/util/math/Size;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/twitter/media/model/SvgFile;->a(Ljava/io/File;Lcom/twitter/util/math/Size;)Lcom/twitter/media/model/SvgFile;

    move-result-object v0

    .line 183
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 184
    iget-object v1, p0, Lcom/twitter/library/media/manager/e;->g:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/twitter/media/request/a;->r()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    :cond_1
    return-object v0

    .line 178
    :cond_2
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->m()Lcom/twitter/media/model/MediaType;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/twitter/media/model/MediaFile;->a(Ljava/io/File;Lcom/twitter/media/model/MediaType;)Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    goto :goto_0

    .line 180
    :cond_3
    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/media/request/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/ac;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 181
    iget-object v0, p0, Lcom/twitter/library/media/manager/e;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/twitter/media/request/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/media/request/a;->m()Lcom/twitter/media/model/MediaType;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/twitter/media/model/MediaFile;->a(Landroid/content/Context;Landroid/net/Uri;Lcom/twitter/media/model/MediaType;)Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/twitter/media/request/a;Ljava/io/File;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 82
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 83
    invoke-static {p2}, Lcom/twitter/media/decoder/ImageDecoder;->a(Ljava/io/File;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/media/request/a;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/decoder/ImageDecoder;->a(Ljava/lang/String;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/media/decoder/ImageDecoder;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_0

    .line 86
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/twitter/library/media/manager/e;->c(Lcom/twitter/media/request/a;Ljava/io/File;)Lcom/twitter/media/model/MediaFile;

    .line 88
    :cond_0
    return-object v0
.end method

.method protected a(Lcom/twitter/media/request/a;Landroid/graphics/Bitmap;Lcom/twitter/media/request/ResourceResponse$ResourceSource;)Lcom/twitter/media/request/ImageResponse;
    .locals 3

    .prologue
    .line 195
    new-instance v1, Lcom/twitter/media/request/ImageResponse$a;

    invoke-direct {v1, p1}, Lcom/twitter/media/request/ImageResponse$a;-><init>(Lcom/twitter/media/request/a;)V

    iget-object v0, p0, Lcom/twitter/library/media/manager/e;->g:Ljava/util/Map;

    .line 196
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->r()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaFile;

    invoke-virtual {v1, v0}, Lcom/twitter/media/request/ImageResponse$a;->a(Lcom/twitter/media/model/MediaFile;)Lcom/twitter/media/request/ImageResponse$a;

    move-result-object v0

    .line 197
    invoke-virtual {v0, p2}, Lcom/twitter/media/request/ImageResponse$a;->a(Landroid/graphics/Bitmap;)Lcom/twitter/media/request/ImageResponse$a;

    move-result-object v0

    .line 198
    invoke-virtual {v0, p3}, Lcom/twitter/media/request/ImageResponse$a;->a(Lcom/twitter/media/request/ResourceResponse$ResourceSource;)Lcom/twitter/media/request/ImageResponse$a;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/library/media/manager/e;->h:Ljava/util/Map;

    .line 199
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->r()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/twitter/media/request/ImageResponse$Error;->b:Lcom/twitter/media/request/ImageResponse$Error;

    invoke-static {v0, v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/ImageResponse$Error;

    invoke-virtual {v1, v0}, Lcom/twitter/media/request/ImageResponse$a;->a(Lcom/twitter/media/request/ImageResponse$Error;)Lcom/twitter/media/request/ImageResponse$a;

    move-result-object v1

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    .line 200
    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/media/request/ImageResponse$a;->a(Z)Lcom/twitter/media/request/ImageResponse$a;

    move-result-object v0

    .line 201
    invoke-virtual {v0}, Lcom/twitter/media/request/ImageResponse$a;->a()Lcom/twitter/media/request/ImageResponse;

    move-result-object v0

    .line 195
    return-object v0

    .line 199
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/media/request/b;Ljava/lang/Object;Lcom/twitter/media/request/ResourceResponse$ResourceSource;)Lcom/twitter/media/request/ResourceResponse;
    .locals 1

    .prologue
    .line 39
    check-cast p1, Lcom/twitter/media/request/a;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/media/manager/e;->a(Lcom/twitter/media/request/a;Landroid/graphics/Bitmap;Lcom/twitter/media/request/ResourceResponse$ResourceSource;)Lcom/twitter/media/request/ImageResponse;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/media/request/a;)Lcom/twitter/util/concurrent/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/media/request/a;",
            ")",
            "Lcom/twitter/util/concurrent/g",
            "<",
            "Lcom/twitter/media/request/ImageResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/media/request/a;->e()Lcom/twitter/util/math/Size;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Request with an empty size."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 75
    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/library/media/manager/k;->d(Lcom/twitter/media/request/b;)Lcom/twitter/util/concurrent/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/twitter/media/request/b;)Ljava/io/File;
    .locals 1

    .prologue
    .line 39
    check-cast p1, Lcom/twitter/media/request/a;

    invoke-virtual {p0, p1}, Lcom/twitter/library/media/manager/e;->d(Lcom/twitter/media/request/a;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/media/request/b;Ljava/io/File;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    check-cast p1, Lcom/twitter/media/request/a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/media/manager/e;->a(Lcom/twitter/media/request/a;Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/graphics/Bitmap;)Z
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/media/request/a;Landroid/graphics/Bitmap;Ljava/io/OutputStream;)Z
    .locals 2

    .prologue
    .line 207
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/media/model/ImageFormat;->a(Ljava/lang/String;)Lcom/twitter/media/model/ImageFormat;

    move-result-object v0

    .line 208
    sget-object v1, Lcom/twitter/media/model/ImageFormat;->b:Lcom/twitter/media/model/ImageFormat;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/twitter/media/model/ImageFormat;->c:Lcom/twitter/media/model/ImageFormat;

    if-ne v0, v1, :cond_1

    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    .line 211
    :goto_0
    const/16 v1, 0x5a

    invoke-virtual {p2, v0, v1, p3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v0

    return v0

    .line 208
    :cond_1
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    goto :goto_0
.end method

.method protected bridge synthetic a(Lcom/twitter/media/request/b;Ljava/lang/Object;Ljava/io/OutputStream;)Z
    .locals 1

    .prologue
    .line 39
    check-cast p1, Lcom/twitter/media/request/a;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/media/manager/e;->a(Lcom/twitter/media/request/a;Landroid/graphics/Bitmap;Ljava/io/OutputStream;)Z

    move-result v0

    return v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 39
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/twitter/library/media/manager/e;->a(Landroid/graphics/Bitmap;)Z

    move-result v0

    return v0
.end method

.method protected b(Lcom/twitter/media/request/a;Ljava/io/File;)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 107
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/media/manager/e;->c(Lcom/twitter/media/request/a;Ljava/io/File;)Lcom/twitter/media/model/MediaFile;

    move-result-object v2

    .line 108
    if-nez v2, :cond_0

    .line 166
    :goto_0
    return-object v1

    .line 113
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->j()Lbzi;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 116
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/media/manager/e;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 117
    if-eqz v0, :cond_7

    .line 119
    :try_start_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 126
    :goto_1
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->i()Lcom/twitter/util/math/c;

    move-result-object v3

    .line 128
    if-nez v0, :cond_6

    .line 129
    invoke-static {v2}, Lcom/twitter/media/decoder/ImageDecoder;->a(Lcom/twitter/media/model/MediaFile;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v1

    .line 130
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->e()Lcom/twitter/util/math/Size;

    move-result-object v4

    .line 131
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/twitter/media/decoder/c;->a:Lcom/twitter/media/decoder/c;

    .line 130
    :goto_2
    invoke-virtual {v1, v4, v0}, Lcom/twitter/media/decoder/ImageDecoder;->a(Lcom/twitter/util/math/Size;Lcom/twitter/media/decoder/c;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    .line 132
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->g()Lcom/twitter/media/decoder/ImageDecoder$ScaleType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/decoder/ImageDecoder;->a(Lcom/twitter/media/decoder/ImageDecoder$ScaleType;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    .line 133
    invoke-virtual {v0, v3}, Lcom/twitter/media/decoder/ImageDecoder;->a(Lcom/twitter/util/math/c;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/media/manager/e;->i:Lcom/twitter/util/math/Size;

    .line 134
    invoke-virtual {v0, v1}, Lcom/twitter/media/decoder/ImageDecoder;->d(Lcom/twitter/util/math/Size;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/media/manager/e;->j:I

    .line 135
    invoke-virtual {v0, v1}, Lcom/twitter/media/decoder/ImageDecoder;->d(I)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    .line 136
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->h()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/decoder/ImageDecoder;->e(I)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    .line 137
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->k()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/decoder/ImageDecoder;->a(Z)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    .line 138
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/decoder/ImageDecoder;->a(Ljava/lang/String;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v0

    .line 139
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->l()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/decoder/ImageDecoder;->a(Landroid/graphics/Bitmap$Config;)Lcom/twitter/media/decoder/ImageDecoder;

    move-result-object v1

    .line 141
    instance-of v0, v1, Lcom/twitter/media/decoder/f;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 142
    check-cast v0, Lcom/twitter/media/decoder/f;

    invoke-virtual {p1}, Lcom/twitter/media/request/a;->o()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/twitter/media/decoder/f;->f(I)Lcom/twitter/media/decoder/ImageDecoder;

    .line 145
    :cond_1
    invoke-virtual {v1}, Lcom/twitter/media/decoder/ImageDecoder;->b()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 148
    :goto_3
    if-eqz v1, :cond_4

    .line 149
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->j()Lbzi;

    move-result-object v0

    .line 150
    if-eqz v0, :cond_5

    .line 151
    iget-object v4, v2, Lcom/twitter/media/model/MediaFile;->f:Lcom/twitter/util/math/Size;

    invoke-virtual {p1}, Lcom/twitter/media/request/a;->e()Lcom/twitter/util/math/Size;

    move-result-object v5

    invoke-virtual {p1}, Lcom/twitter/media/request/a;->h()I

    move-result v6

    invoke-interface {v0, v4, v5, v3, v6}, Lbzi;->a(Lcom/twitter/util/math/Size;Lcom/twitter/util/math/Size;Lcom/twitter/util/math/c;I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 152
    invoke-interface {v0, v1}, Lbzi;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 153
    if-eq v0, v1, :cond_5

    if-eqz v0, :cond_5

    .line 154
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 163
    :goto_4
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->p()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v1

    invoke-virtual {v1}, Lcqq;->c()Lcqr;

    move-result-object v1

    iget-object v3, v2, Lcom/twitter/media/model/MediaFile;->e:Ljava/io/File;

    invoke-interface {v1, v3}, Lcqr;->a(Ljava/io/File;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 164
    invoke-virtual {v2}, Lcom/twitter/media/model/MediaFile;->b()Z

    :cond_2
    move-object v1, v0

    .line 166
    goto/16 :goto_0

    .line 131
    :cond_3
    const v0, 0x3f4ccccd    # 0.8f

    const/high16 v5, 0x3fc00000    # 1.5f

    invoke-static {v0, v5}, Lcom/twitter/media/decoder/c;->a(FF)Lcom/twitter/media/decoder/c;

    move-result-object v0

    goto/16 :goto_2

    .line 160
    :cond_4
    iget-object v0, p0, Lcom/twitter/library/media/manager/e;->g:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/twitter/media/request/a;->r()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    move-object v0, v1

    goto :goto_4

    .line 120
    :catch_0
    move-exception v0

    goto/16 :goto_0

    :cond_6
    move-object v1, v0

    goto :goto_3

    :cond_7
    move-object v0, v1

    goto/16 :goto_1
.end method

.method b(Lcom/twitter/media/request/a;)Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/media/request/a;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/twitter/media/request/ResourceResponse$ResourceSource;",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/twitter/library/media/manager/k;->c(Lcom/twitter/media/request/b;)Landroid/util/Pair;

    move-result-object v0

    .line 95
    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v1, :cond_1

    .line 96
    :cond_0
    iget-object v1, p0, Lcom/twitter/library/media/manager/e;->h:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/twitter/media/request/a;->r()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/twitter/media/request/ImageResponse$Error;->a:Lcom/twitter/media/request/ImageResponse$Error;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    :cond_1
    return-object v0
.end method

.method public synthetic b(Lcom/twitter/media/request/b;)Ljava/io/File;
    .locals 1

    .prologue
    .line 39
    check-cast p1, Lcom/twitter/media/request/a;

    invoke-virtual {p0, p1}, Lcom/twitter/library/media/manager/e;->c(Lcom/twitter/media/request/a;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic b(Lcom/twitter/media/request/b;Ljava/io/File;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    check-cast p1, Lcom/twitter/media/request/a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/media/manager/e;->b(Lcom/twitter/media/request/a;Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method synthetic c(Lcom/twitter/media/request/b;)Landroid/util/Pair;
    .locals 1

    .prologue
    .line 39
    check-cast p1, Lcom/twitter/media/request/a;

    invoke-virtual {p0, p1}, Lcom/twitter/library/media/manager/e;->b(Lcom/twitter/media/request/a;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public c(Lcom/twitter/media/request/a;)Ljava/io/File;
    .locals 3

    .prologue
    .line 218
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 219
    invoke-virtual {p1}, Lcom/twitter/media/request/a;->b()Ljava/util/List;

    move-result-object v0

    .line 220
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 221
    iget-object v2, p0, Lcom/twitter/library/media/manager/e;->c:Lcpy;

    invoke-virtual {v2, v0}, Lcpy;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 222
    if-eqz v0, :cond_0

    .line 226
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic d(Lcom/twitter/media/request/b;)Lcom/twitter/util/concurrent/g;
    .locals 1

    .prologue
    .line 39
    check-cast p1, Lcom/twitter/media/request/a;

    invoke-virtual {p0, p1}, Lcom/twitter/library/media/manager/e;->a(Lcom/twitter/media/request/a;)Lcom/twitter/util/concurrent/g;

    move-result-object v0

    return-object v0
.end method

.method public d(Lcom/twitter/media/request/a;)Ljava/io/File;
    .locals 2

    .prologue
    .line 233
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 234
    const/4 v0, 0x0

    .line 235
    iget-object v1, p0, Lcom/twitter/library/media/manager/e;->d:Lcpy;

    if-eqz v1, :cond_0

    .line 236
    iget-object v0, p0, Lcom/twitter/library/media/manager/e;->d:Lcpy;

    invoke-virtual {p1}, Lcom/twitter/media/request/a;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcpy;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 238
    :cond_0
    return-object v0
.end method
