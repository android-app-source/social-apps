.class final Lcom/twitter/library/metrics/c$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/metrics/c;->a(Lcom/twitter/metrics/j;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/q",
        "<",
        "Lcom/twitter/library/network/DataUsageEvent;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/metrics/c;

.field final synthetic b:Lcom/twitter/metrics/c;


# direct methods
.method constructor <init>(Lcom/twitter/metrics/c;Lcom/twitter/metrics/c;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/twitter/library/metrics/c$2;->a:Lcom/twitter/metrics/c;

    iput-object p2, p0, Lcom/twitter/library/metrics/c$2;->b:Lcom/twitter/metrics/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEvent(Lcom/twitter/library/network/DataUsageEvent;)V
    .locals 4

    .prologue
    .line 149
    invoke-static {}, Lcom/twitter/util/android/b;->a()Lcom/twitter/util/android/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/android/b;->b()Z

    move-result v0

    .line 150
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/metrics/c$2;->a:Lcom/twitter/metrics/c;

    .line 151
    :goto_0
    iget-wide v2, p1, Lcom/twitter/library/network/DataUsageEvent;->f:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/metrics/c;->a(J)V

    .line 152
    return-void

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/metrics/c$2;->b:Lcom/twitter/metrics/c;

    goto :goto_0
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 146
    check-cast p1, Lcom/twitter/library/network/DataUsageEvent;

    invoke-virtual {p0, p1}, Lcom/twitter/library/metrics/c$2;->onEvent(Lcom/twitter/library/network/DataUsageEvent;)V

    return-void
.end method
