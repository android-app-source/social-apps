.class public Lcom/twitter/library/metrics/c;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static b:Lcom/twitter/metrics/g;

.field private static volatile c:Lcom/twitter/library/metrics/c;


# instance fields
.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 56
    const-string/jumbo v0, "api:foreground:::rxbytes"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "api:background:::rxbytes"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "api::scribe::size"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/metrics/c;->a:Ljava/util/Set;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v0

    .line 163
    invoke-static {}, Lcom/twitter/util/u;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/metrics/j;->b(Z)V

    .line 167
    :cond_0
    new-instance v1, Lcom/twitter/library/metrics/e;

    invoke-direct {v1, p1}, Lcom/twitter/library/metrics/e;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/twitter/metrics/j;->a(Lcom/twitter/metrics/k;)V

    .line 168
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/twitter/library/metrics/c;
    .locals 2

    .prologue
    .line 76
    sget-object v0, Lcom/twitter/library/metrics/c;->c:Lcom/twitter/library/metrics/c;

    if-nez v0, :cond_1

    .line 77
    const-class v1, Lcom/twitter/library/metrics/c;

    monitor-enter v1

    .line 78
    :try_start_0
    sget-object v0, Lcom/twitter/library/metrics/c;->c:Lcom/twitter/library/metrics/c;

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Lcom/twitter/library/metrics/c;

    invoke-direct {v0, p0}, Lcom/twitter/library/metrics/c;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/library/metrics/c;->c:Lcom/twitter/library/metrics/c;

    .line 80
    const-class v0, Lcom/twitter/library/metrics/c;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 82
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    :cond_1
    sget-object v0, Lcom/twitter/library/metrics/c;->c:Lcom/twitter/library/metrics/c;

    return-object v0

    .line 82
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a()Lcom/twitter/metrics/g;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/twitter/library/metrics/c;->b:Lcom/twitter/metrics/g;

    return-object v0
.end method

.method public static a(Lcom/twitter/metrics/g;)V
    .locals 1

    .prologue
    .line 70
    sput-object p0, Lcom/twitter/library/metrics/c;->b:Lcom/twitter/metrics/g;

    .line 71
    const-class v0, Lcom/twitter/library/metrics/c;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 72
    return-void
.end method

.method private static a(Lcom/twitter/metrics/j;J)V
    .locals 7

    .prologue
    const/4 v5, 0x3

    .line 140
    const-string/jumbo v0, "api:background:::rxbytes"

    sget-object v4, Lcom/twitter/metrics/g;->n:Lcom/twitter/metrics/g$b;

    move-object v1, p0

    move-wide v2, p1

    invoke-static/range {v0 .. v5}, Lcom/twitter/metrics/c;->a(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;I)Lcom/twitter/metrics/c;

    move-result-object v6

    .line 142
    invoke-virtual {v6}, Lcom/twitter/metrics/c;->i()V

    .line 143
    const-string/jumbo v0, "api:foreground:::rxbytes"

    sget-object v4, Lcom/twitter/metrics/g;->n:Lcom/twitter/metrics/g$b;

    move-object v1, p0

    move-wide v2, p1

    invoke-static/range {v0 .. v5}, Lcom/twitter/metrics/c;->a(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;I)Lcom/twitter/metrics/c;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Lcom/twitter/metrics/c;->i()V

    .line 146
    invoke-static {}, Lcom/twitter/library/network/b;->a()Lcom/twitter/library/network/b;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/metrics/c$2;

    invoke-direct {v2, v0, v6}, Lcom/twitter/library/metrics/c$2;-><init>(Lcom/twitter/metrics/c;Lcom/twitter/metrics/c;)V

    invoke-virtual {v1, v2}, Lcom/twitter/library/network/b;->a(Lcom/twitter/util/q;)Z

    .line 154
    return-void
.end method

.method public static c()V
    .locals 1

    .prologue
    .line 171
    invoke-static {}, Lcom/twitter/library/metrics/c;->e()V

    .line 172
    new-instance v0, Lcom/twitter/library/metrics/c$3;

    invoke-direct {v0}, Lcom/twitter/library/metrics/c$3;-><init>()V

    invoke-static {v0}, Lcoj;->a(Lcoj$a;)V

    .line 178
    return-void
.end method

.method static synthetic d()V
    .locals 0

    .prologue
    .line 31
    invoke-static {}, Lcom/twitter/library/metrics/c;->e()V

    return-void
.end method

.method private static e()V
    .locals 8

    .prologue
    .line 181
    invoke-static {}, Lcom/twitter/metrics/i;->a()Lcom/twitter/metrics/i$b;

    move-result-object v6

    .line 182
    new-instance v0, Lcom/twitter/metrics/i$b;

    iget v1, v6, Lcom/twitter/metrics/i$b;->a:I

    const-string/jumbo v2, "perftown_low_priority_sampling_rate"

    iget v3, v6, Lcom/twitter/metrics/i$b;->b:I

    .line 184
    invoke-static {v2, v3}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v2

    const-string/jumbo v3, "perftown_high_priority_sampling_rate"

    iget v4, v6, Lcom/twitter/metrics/i$b;->c:I

    .line 186
    invoke-static {v3, v4}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v3

    const-string/jumbo v4, "metrics_periodic_reporting_interval_short"

    iget v5, v6, Lcom/twitter/metrics/i$b;->d:I

    .line 188
    invoke-static {v4, v5}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v4

    const-string/jumbo v5, "metrics_periodic_reporting_interval_regular"

    iget v7, v6, Lcom/twitter/metrics/i$b;->e:I

    .line 190
    invoke-static {v5, v7}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v5

    const-string/jumbo v7, "metrics_periodic_reporting_interval_long"

    iget v6, v6, Lcom/twitter/metrics/i$b;->f:I

    .line 192
    invoke-static {v7, v6}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/twitter/metrics/i$b;-><init>(IIIIII)V

    .line 182
    invoke-static {v0}, Lcom/twitter/metrics/i;->a(Lcom/twitter/metrics/i$b;)V

    .line 194
    return-void
.end method


# virtual methods
.method public b()V
    .locals 6

    .prologue
    .line 94
    monitor-enter p0

    .line 95
    :try_start_0
    iget-boolean v0, p0, Lcom/twitter/library/metrics/c;->d:Z

    if-eqz v0, :cond_0

    .line 96
    monitor-exit p0

    .line 136
    :goto_0
    return-void

    .line 98
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/metrics/c;->d:Z

    .line 99
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v1

    .line 102
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 103
    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 106
    invoke-static {}, Lcof;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 107
    invoke-static {v1, v2, v3}, Lcom/twitter/library/metrics/c;->a(Lcom/twitter/metrics/j;J)V

    .line 110
    :cond_1
    const-string/jumbo v0, "api::scribe::size"

    const-wide/16 v2, 0x0

    sget-object v4, Lcom/twitter/metrics/g;->n:Lcom/twitter/metrics/g$b;

    const/4 v5, 0x3

    invoke-static/range {v0 .. v5}, Lcom/twitter/metrics/c;->a(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;I)Lcom/twitter/metrics/c;

    move-result-object v0

    .line 112
    invoke-virtual {v0}, Lcom/twitter/metrics/c;->i()V

    .line 113
    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeService;->a(Lcom/twitter/metrics/c;)V

    .line 115
    const-string/jumbo v0, "fs:first_download_req"

    sget-object v2, Lcom/twitter/metrics/g;->m:Lcom/twitter/metrics/g$b;

    invoke-static {v0, v1, v2}, Lcom/twitter/metrics/n;->b(Ljava/lang/String;Lcom/twitter/metrics/j;Lcom/twitter/metrics/g$b;)Lcom/twitter/metrics/n;

    move-result-object v0

    .line 117
    invoke-virtual {v0}, Lcom/twitter/metrics/n;->i()V

    .line 119
    invoke-static {v1}, Lcom/twitter/library/metrics/a;->a(Lcom/twitter/metrics/j;)V

    .line 121
    invoke-static {}, Lcom/twitter/util/android/b;->a()Lcom/twitter/util/android/b;

    move-result-object v0

    new-instance v2, Lcom/twitter/library/metrics/c$1;

    invoke-direct {v2, p0, v1}, Lcom/twitter/library/metrics/c$1;-><init>(Lcom/twitter/library/metrics/c;Lcom/twitter/metrics/j;)V

    invoke-virtual {v0, v2}, Lcom/twitter/util/android/b;->a(Lcom/twitter/util/android/b$a;)V

    goto :goto_0

    .line 99
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
