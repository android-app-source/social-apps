.class public Lcom/twitter/library/metrics/e;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/metrics/k;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/metrics/e;->a:Landroid/content/Context;

    .line 21
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/metrics/g;)V
    .locals 3

    .prologue
    .line 25
    invoke-virtual {p1}, Lcom/twitter/metrics/g;->v()Lcom/twitter/metrics/g$b;

    move-result-object v0

    .line 27
    sget-object v1, Lcom/twitter/metrics/g;->l:Lcom/twitter/metrics/g$b;

    if-eq v0, v1, :cond_0

    .line 28
    sget-object v1, Lcom/twitter/util/y;->a:Ljava/security/SecureRandom;

    const/16 v2, 0x2710

    invoke-virtual {v1, v2}, Ljava/security/SecureRandom;->nextInt(I)I

    move-result v1

    .line 29
    invoke-interface {v0}, Lcom/twitter/metrics/g$b;->a()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 30
    invoke-static {p1}, Lcom/twitter/library/metrics/d;->a(Lcom/twitter/metrics/g;)Lcom/twitter/library/scribe/performance/PerformanceScribeLog;

    move-result-object v0

    .line 32
    iget-object v1, p0, Lcom/twitter/library/metrics/e;->a:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/analytics/model/ScribeLog;Z)V

    .line 35
    :cond_0
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 44
    instance-of v0, p1, Lcom/twitter/library/metrics/e;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/twitter/library/metrics/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
