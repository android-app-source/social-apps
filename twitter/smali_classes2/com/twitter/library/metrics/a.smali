.class public Lcom/twitter/library/metrics/a;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/library/metrics/a;


# instance fields
.field private b:Z

.field private c:Lcom/twitter/metrics/a;

.field private d:Lcom/twitter/metrics/l;

.field private e:Lcom/twitter/metrics/a;

.field private f:Lcom/twitter/metrics/l;

.field private g:Lcom/twitter/metrics/a;

.field private h:Lcom/twitter/metrics/l;

.field private i:Lcom/twitter/metrics/a;

.field private j:Lcom/twitter/metrics/l;

.field private k:Lcom/twitter/metrics/a;

.field private l:Lcom/twitter/metrics/l;


# direct methods
.method private constructor <init>(Lcom/twitter/metrics/j;)V
    .locals 3

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    const-string/jumbo v0, "app::::oome_count"

    sget-object v1, Lcom/twitter/metrics/g;->n:Lcom/twitter/metrics/g$b;

    const/4 v2, 0x3

    invoke-static {p1, v0, v1, v2}, Lbrk;->a(Lcom/twitter/metrics/j;Ljava/lang/String;Lcom/twitter/metrics/g$b;I)Lbrk;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lbrk;->i()V

    .line 74
    new-instance v1, Lcom/twitter/library/metrics/a$1;

    invoke-direct {v1, p0, v0}, Lcom/twitter/library/metrics/a$1;-><init>(Lcom/twitter/library/metrics/a;Lbrk;)V

    invoke-static {v1}, Lcpg;->a(Lcpg$b;)V

    .line 81
    invoke-direct {p0, p1}, Lcom/twitter/library/metrics/a;->b(Lcom/twitter/metrics/j;)V

    .line 82
    return-void
.end method

.method public static a()Lcom/twitter/library/metrics/a;
    .locals 2

    .prologue
    .line 52
    sget-object v0, Lcom/twitter/library/metrics/a;->a:Lcom/twitter/library/metrics/a;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "MediaMetrics.initialize() must be called first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_0
    sget-object v0, Lcom/twitter/library/metrics/a;->a:Lcom/twitter/library/metrics/a;

    return-object v0
.end method

.method public static a(Lcom/twitter/metrics/j;)V
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/twitter/library/metrics/a;->a:Lcom/twitter/library/metrics/a;

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Lcom/twitter/library/metrics/a;

    invoke-direct {v0, p0}, Lcom/twitter/library/metrics/a;-><init>(Lcom/twitter/metrics/j;)V

    sput-object v0, Lcom/twitter/library/metrics/a;->a:Lcom/twitter/library/metrics/a;

    .line 61
    const-class v0, Lcom/twitter/library/metrics/a;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 63
    :cond_0
    return-void
.end method

.method private b(Lcom/twitter/metrics/j;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 85
    sget-object v0, Lcom/twitter/metrics/g;->m:Lcom/twitter/metrics/g$b;

    invoke-static {v0, p1}, Lbzu;->a(Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/j;)Lbzu;

    move-result-object v0

    invoke-virtual {v0}, Lbzu;->i()V

    .line 87
    const-string/jumbo v0, "memory_metric_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/metrics/a;->b:Z

    .line 89
    sget-object v0, Lcom/twitter/metrics/g;->n:Lcom/twitter/metrics/g$b;

    .line 92
    const-string/jumbo v1, "app::dalvik:heap:avg"

    invoke-static {v1, v0, p1, v2, v3}, Lcom/twitter/metrics/a;->a(Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/j;ZI)Lcom/twitter/metrics/a;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/metrics/a;->c:Lcom/twitter/metrics/a;

    .line 94
    iget-object v1, p0, Lcom/twitter/library/metrics/a;->c:Lcom/twitter/metrics/a;

    invoke-virtual {v1}, Lcom/twitter/metrics/a;->i()V

    .line 95
    const-string/jumbo v1, "app::dalvik:heap:peak"

    invoke-static {v1, v0, p1, v2, v3}, Lcom/twitter/metrics/l;->a(Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/j;ZI)Lcom/twitter/metrics/l;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/metrics/a;->d:Lcom/twitter/metrics/l;

    .line 97
    iget-object v1, p0, Lcom/twitter/library/metrics/a;->d:Lcom/twitter/metrics/l;

    invoke-virtual {v1}, Lcom/twitter/metrics/l;->i()V

    .line 98
    const-string/jumbo v1, "app::dalvik:heap_allocated:avg"

    invoke-static {v1, v0, p1, v2, v3}, Lcom/twitter/metrics/a;->a(Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/j;ZI)Lcom/twitter/metrics/a;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/metrics/a;->e:Lcom/twitter/metrics/a;

    .line 100
    iget-object v1, p0, Lcom/twitter/library/metrics/a;->e:Lcom/twitter/metrics/a;

    invoke-virtual {v1}, Lcom/twitter/metrics/a;->i()V

    .line 101
    const-string/jumbo v1, "app::dalvik:heap_allocated:peak"

    invoke-static {v1, v0, p1, v2, v3}, Lcom/twitter/metrics/l;->a(Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/j;ZI)Lcom/twitter/metrics/l;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/metrics/a;->f:Lcom/twitter/metrics/l;

    .line 103
    iget-object v1, p0, Lcom/twitter/library/metrics/a;->f:Lcom/twitter/metrics/l;

    invoke-virtual {v1}, Lcom/twitter/metrics/l;->i()V

    .line 104
    const-string/jumbo v1, "app::dalvik:heap_ratio:avg"

    invoke-static {v1, v0, p1, v2, v3}, Lcom/twitter/metrics/a;->a(Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/j;ZI)Lcom/twitter/metrics/a;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/metrics/a;->g:Lcom/twitter/metrics/a;

    .line 106
    iget-object v1, p0, Lcom/twitter/library/metrics/a;->g:Lcom/twitter/metrics/a;

    invoke-virtual {v1}, Lcom/twitter/metrics/a;->i()V

    .line 107
    const-string/jumbo v1, "app::dalvik:heap_ratio:peak"

    invoke-static {v1, v0, p1, v2, v3}, Lcom/twitter/metrics/l;->a(Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/j;ZI)Lcom/twitter/metrics/l;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/metrics/a;->h:Lcom/twitter/metrics/l;

    .line 109
    iget-object v1, p0, Lcom/twitter/library/metrics/a;->h:Lcom/twitter/metrics/l;

    invoke-virtual {v1}, Lcom/twitter/metrics/l;->i()V

    .line 110
    const-string/jumbo v1, "app::native:heap:avg"

    invoke-static {v1, v0, p1, v2, v3}, Lcom/twitter/metrics/a;->a(Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/j;ZI)Lcom/twitter/metrics/a;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/metrics/a;->i:Lcom/twitter/metrics/a;

    .line 112
    iget-object v1, p0, Lcom/twitter/library/metrics/a;->i:Lcom/twitter/metrics/a;

    invoke-virtual {v1}, Lcom/twitter/metrics/a;->i()V

    .line 113
    const-string/jumbo v1, "app::native:heap:peak"

    invoke-static {v1, v0, p1, v2, v3}, Lcom/twitter/metrics/l;->a(Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/j;ZI)Lcom/twitter/metrics/l;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/metrics/a;->j:Lcom/twitter/metrics/l;

    .line 115
    iget-object v1, p0, Lcom/twitter/library/metrics/a;->j:Lcom/twitter/metrics/l;

    invoke-virtual {v1}, Lcom/twitter/metrics/l;->i()V

    .line 116
    const-string/jumbo v1, "app::native:heap_allocated:avg"

    invoke-static {v1, v0, p1, v2, v3}, Lcom/twitter/metrics/a;->a(Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/j;ZI)Lcom/twitter/metrics/a;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/metrics/a;->k:Lcom/twitter/metrics/a;

    .line 118
    iget-object v1, p0, Lcom/twitter/library/metrics/a;->k:Lcom/twitter/metrics/a;

    invoke-virtual {v1}, Lcom/twitter/metrics/a;->i()V

    .line 119
    const-string/jumbo v1, "app::native:heap_allocated:peak"

    invoke-static {v1, v0, p1, v2, v3}, Lcom/twitter/metrics/l;->a(Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/j;ZI)Lcom/twitter/metrics/l;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/metrics/a;->l:Lcom/twitter/metrics/l;

    .line 121
    iget-object v0, p0, Lcom/twitter/library/metrics/a;->l:Lcom/twitter/metrics/l;

    invoke-virtual {v0}, Lcom/twitter/metrics/l;->i()V

    .line 123
    :cond_1
    return-void
.end method


# virtual methods
.method public b()V
    .locals 8

    .prologue
    .line 126
    sget-object v0, Lcom/twitter/metrics/g;->l:Lcom/twitter/metrics/g$b;

    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v1

    invoke-static {v0, v1}, Lbzu;->a(Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/j;)Lbzu;

    move-result-object v0

    invoke-virtual {v0}, Lbzu;->h()V

    .line 127
    const-string/jumbo v0, "memory_metric_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 128
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/library/metrics/a;->b:Z

    if-nez v0, :cond_1

    .line 129
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/library/metrics/a;->b(Lcom/twitter/metrics/j;)V

    .line 132
    :cond_1
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    .line 133
    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v2

    .line 134
    iget-object v1, p0, Lcom/twitter/library/metrics/a;->c:Lcom/twitter/metrics/a;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/metrics/a;->a(J)V

    .line 135
    iget-object v1, p0, Lcom/twitter/library/metrics/a;->d:Lcom/twitter/metrics/l;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/metrics/l;->a(J)V

    .line 137
    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 138
    iget-object v1, p0, Lcom/twitter/library/metrics/a;->e:Lcom/twitter/metrics/a;

    invoke-virtual {v1, v4, v5}, Lcom/twitter/metrics/a;->a(J)V

    .line 139
    iget-object v1, p0, Lcom/twitter/library/metrics/a;->f:Lcom/twitter/metrics/l;

    invoke-virtual {v1, v4, v5}, Lcom/twitter/metrics/l;->a(J)V

    .line 141
    const-wide/16 v4, 0x64

    mul-long/2addr v2, v4

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v0

    div-long v0, v2, v0

    .line 142
    iget-object v2, p0, Lcom/twitter/library/metrics/a;->g:Lcom/twitter/metrics/a;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/metrics/a;->a(J)V

    .line 143
    iget-object v2, p0, Lcom/twitter/library/metrics/a;->h:Lcom/twitter/metrics/l;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/metrics/l;->a(J)V

    .line 145
    invoke-static {}, Landroid/os/Debug;->getNativeHeapSize()J

    move-result-wide v0

    .line 146
    iget-object v2, p0, Lcom/twitter/library/metrics/a;->i:Lcom/twitter/metrics/a;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/metrics/a;->a(J)V

    .line 147
    iget-object v2, p0, Lcom/twitter/library/metrics/a;->j:Lcom/twitter/metrics/l;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/metrics/l;->a(J)V

    .line 149
    invoke-static {}, Landroid/os/Debug;->getNativeHeapAllocatedSize()J

    move-result-wide v0

    .line 150
    iget-object v2, p0, Lcom/twitter/library/metrics/a;->k:Lcom/twitter/metrics/a;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/metrics/a;->a(J)V

    .line 151
    iget-object v2, p0, Lcom/twitter/library/metrics/a;->l:Lcom/twitter/metrics/l;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/metrics/l;->a(J)V

    .line 153
    :cond_2
    return-void
.end method
