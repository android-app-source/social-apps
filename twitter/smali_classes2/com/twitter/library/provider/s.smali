.class public Lcom/twitter/library/provider/s;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/provider/s$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/twitter/library/api/t;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/twitter/model/timeline/y;",
            ">;"
        }
    .end annotation
.end field

.field public final c:J

.field public final d:J

.field public final e:I

.field public final f:Ljava/lang/String;

.field public final g:Z

.field public final h:Z

.field public final i:Z

.field public final j:Ljava/lang/String;

.field public final k:Z

.field public final l:Laut;

.field public final m:Ljava/lang/String;

.field public final n:Ljava/lang/String;

.field public final o:Z

.field public final p:Z


# direct methods
.method private constructor <init>(Lcom/twitter/library/provider/s$a;)V
    .locals 2

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iget-object v0, p1, Lcom/twitter/library/provider/s$a;->b:Lcom/twitter/library/api/t;

    iput-object v0, p0, Lcom/twitter/library/provider/s;->a:Lcom/twitter/library/api/t;

    .line 110
    iget-object v0, p1, Lcom/twitter/library/provider/s$a;->a:Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/library/provider/s;->b:Ljava/util/List;

    .line 111
    iget-wide v0, p1, Lcom/twitter/library/provider/s$a;->d:J

    iput-wide v0, p0, Lcom/twitter/library/provider/s;->d:J

    .line 112
    iget-wide v0, p1, Lcom/twitter/library/provider/s$a;->c:J

    iput-wide v0, p0, Lcom/twitter/library/provider/s;->c:J

    .line 113
    iget v0, p1, Lcom/twitter/library/provider/s$a;->e:I

    iput v0, p0, Lcom/twitter/library/provider/s;->e:I

    .line 114
    iget-object v0, p1, Lcom/twitter/library/provider/s$a;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/provider/s;->f:Ljava/lang/String;

    .line 115
    iget-boolean v0, p1, Lcom/twitter/library/provider/s$a;->g:Z

    iput-boolean v0, p0, Lcom/twitter/library/provider/s;->g:Z

    .line 116
    iget-boolean v0, p1, Lcom/twitter/library/provider/s$a;->h:Z

    iput-boolean v0, p0, Lcom/twitter/library/provider/s;->h:Z

    .line 117
    iget-boolean v0, p1, Lcom/twitter/library/provider/s$a;->i:Z

    iput-boolean v0, p0, Lcom/twitter/library/provider/s;->i:Z

    .line 118
    iget-object v0, p1, Lcom/twitter/library/provider/s$a;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/provider/s;->j:Ljava/lang/String;

    .line 119
    iget-boolean v0, p1, Lcom/twitter/library/provider/s$a;->k:Z

    iput-boolean v0, p0, Lcom/twitter/library/provider/s;->k:Z

    .line 120
    iget-object v0, p1, Lcom/twitter/library/provider/s$a;->l:Laut;

    iput-object v0, p0, Lcom/twitter/library/provider/s;->l:Laut;

    .line 121
    iget-object v0, p1, Lcom/twitter/library/provider/s$a;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/provider/s;->m:Ljava/lang/String;

    .line 122
    iget-object v0, p1, Lcom/twitter/library/provider/s$a;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/provider/s;->n:Ljava/lang/String;

    .line 123
    iget-boolean v0, p1, Lcom/twitter/library/provider/s$a;->o:Z

    iput-boolean v0, p0, Lcom/twitter/library/provider/s;->o:Z

    .line 124
    iget-boolean v0, p1, Lcom/twitter/library/provider/s$a;->p:Z

    iput-boolean v0, p0, Lcom/twitter/library/provider/s;->p:Z

    .line 125
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/library/provider/s$a;Lcom/twitter/library/provider/s$1;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/twitter/library/provider/s;-><init>(Lcom/twitter/library/provider/s$a;)V

    return-void
.end method
