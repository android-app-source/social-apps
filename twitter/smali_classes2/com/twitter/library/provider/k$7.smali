.class Lcom/twitter/library/provider/k$7;
.super Laus$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/provider/k;->b()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic b:Lcom/twitter/library/provider/k;


# direct methods
.method constructor <init>(Lcom/twitter/library/provider/k;I)V
    .locals 0

    .prologue
    .line 473
    iput-object p1, p0, Lcom/twitter/library/provider/k$7;->b:Lcom/twitter/library/provider/k;

    invoke-direct {p0, p2}, Laus$a;-><init>(I)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/database/model/j;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    .line 477
    const-class v0, Lawf;

    new-instance v1, Lcom/twitter/database/model/ColumnDefinition$a;

    const-string/jumbo v2, "account_id"

    sget-object v3, Lcom/twitter/database/model/ColumnDefinition$Type;->e:Lcom/twitter/database/model/ColumnDefinition$Type;

    invoke-direct {v1, v2, v3}, Lcom/twitter/database/model/ColumnDefinition$a;-><init>(Ljava/lang/String;Lcom/twitter/database/model/ColumnDefinition$Type;)V

    .line 479
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Lcom/twitter/database/model/ColumnDefinition$a;->a(Ljava/lang/Object;Z)Lcom/twitter/database/model/ColumnDefinition$a;

    move-result-object v1

    .line 480
    invoke-virtual {v1}, Lcom/twitter/database/model/ColumnDefinition$a;->a()Lcom/twitter/database/model/ColumnDefinition;

    move-result-object v1

    .line 477
    invoke-interface {p1, v0, v1}, Lcom/twitter/database/model/j;->a(Ljava/lang/Class;Lcom/twitter/database/model/ColumnDefinition;)V

    .line 481
    iget-object v0, p0, Lcom/twitter/library/provider/k$7;->b:Lcom/twitter/library/provider/k;

    const-string/jumbo v1, "activity_states"

    const-string/jumbo v2, "account_id"

    invoke-virtual {v0, p2, v1, v2}, Lcom/twitter/library/provider/k;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    iget-object v0, p0, Lcom/twitter/library/provider/k$7;->b:Lcom/twitter/library/provider/k;

    invoke-static {v0}, Lcom/twitter/library/provider/k;->g(Lcom/twitter/library/provider/k;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 483
    const-class v0, Laxb;

    new-instance v1, Lcom/twitter/database/model/ColumnDefinition$a;

    const-string/jumbo v2, "account_id"

    sget-object v3, Lcom/twitter/database/model/ColumnDefinition$Type;->e:Lcom/twitter/database/model/ColumnDefinition$Type;

    invoke-direct {v1, v2, v3}, Lcom/twitter/database/model/ColumnDefinition$a;-><init>(Ljava/lang/String;Lcom/twitter/database/model/ColumnDefinition$Type;)V

    .line 485
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Lcom/twitter/database/model/ColumnDefinition$a;->a(Ljava/lang/Object;Z)Lcom/twitter/database/model/ColumnDefinition$a;

    move-result-object v1

    .line 486
    invoke-virtual {v1}, Lcom/twitter/database/model/ColumnDefinition$a;->a()Lcom/twitter/database/model/ColumnDefinition;

    move-result-object v1

    .line 483
    invoke-interface {p1, v0, v1}, Lcom/twitter/database/model/j;->a(Ljava/lang/Class;Lcom/twitter/database/model/ColumnDefinition;)V

    .line 487
    iget-object v0, p0, Lcom/twitter/library/provider/k$7;->b:Lcom/twitter/library/provider/k;

    invoke-static {v0, v4}, Lcom/twitter/library/provider/k;->c(Lcom/twitter/library/provider/k;Z)Z

    .line 489
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/provider/k$7;->b:Lcom/twitter/library/provider/k;

    const-string/jumbo v1, "account_settings"

    const-string/jumbo v2, "account_id"

    invoke-virtual {v0, p2, v1, v2}, Lcom/twitter/library/provider/k;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    return-void
.end method
