.class public final Lcom/twitter/library/provider/s$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/provider/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/library/provider/s;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/twitter/model/timeline/y;",
            ">;"
        }
    .end annotation
.end field

.field final b:Lcom/twitter/library/api/t;

.field final c:J

.field d:J

.field e:I

.field f:Ljava/lang/String;

.field g:Z

.field h:Z

.field i:Z

.field j:Ljava/lang/String;

.field k:Z

.field l:Laut;

.field m:Ljava/lang/String;

.field n:Ljava/lang/String;

.field o:Z

.field p:Z


# direct methods
.method private constructor <init>(Lcom/twitter/library/api/t;)V
    .locals 2

    .prologue
    .line 192
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 135
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/library/provider/s$a;->e:I

    .line 193
    iput-object p1, p0, Lcom/twitter/library/provider/s$a;->b:Lcom/twitter/library/api/t;

    .line 194
    invoke-virtual {p1}, Lcom/twitter/library/api/t;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/provider/s$a;->c:J

    .line 195
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/provider/s$a;->a:Ljava/util/List;

    .line 196
    return-void
.end method

.method private constructor <init>(Ljava/util/List;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/twitter/model/timeline/y;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 186
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 135
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/library/provider/s$a;->e:I

    .line 187
    iput-object p1, p0, Lcom/twitter/library/provider/s$a;->a:Ljava/util/List;

    .line 188
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/provider/s$a;->b:Lcom/twitter/library/api/t;

    .line 189
    iput-wide p2, p0, Lcom/twitter/library/provider/s$a;->c:J

    .line 190
    return-void
.end method

.method public static a(Lcom/twitter/library/api/t;)Lcom/twitter/library/provider/s$a;
    .locals 1

    .prologue
    .line 161
    new-instance v0, Lcom/twitter/library/provider/s$a;

    invoke-direct {v0, p0}, Lcom/twitter/library/provider/s$a;-><init>(Lcom/twitter/library/api/t;)V

    return-object v0
.end method

.method public static a(Lcom/twitter/model/timeline/s;)Lcom/twitter/library/provider/s$a;
    .locals 4

    .prologue
    .line 171
    new-instance v0, Lcom/twitter/library/provider/s$a;

    iget-object v1, p0, Lcom/twitter/model/timeline/s;->b:Ljava/util/List;

    iget-wide v2, p0, Lcom/twitter/model/timeline/s;->a:J

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/provider/s$a;-><init>(Ljava/util/List;J)V

    return-object v0
.end method

.method public static a(Ljava/util/List;)Lcom/twitter/library/provider/s$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/twitter/model/timeline/y;",
            ">;)",
            "Lcom/twitter/library/provider/s$a;"
        }
    .end annotation

    .prologue
    .line 181
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 182
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y;

    iget-wide v0, v0, Lcom/twitter/model/timeline/y;->g:J

    .line 183
    :goto_0
    new-instance v2, Lcom/twitter/library/provider/s$a;

    invoke-direct {v2, p0, v0, v1}, Lcom/twitter/library/provider/s$a;-><init>(Ljava/util/List;J)V

    return-object v2

    .line 182
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public R_()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 278
    iget-object v1, p0, Lcom/twitter/library/provider/s$a;->b:Lcom/twitter/library/api/t;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/provider/s$a;->a:Ljava/util/List;

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/twitter/library/provider/s$a;->b:Lcom/twitter/library/api/t;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/library/provider/s$a;->a:Ljava/util/List;

    if-eqz v1, :cond_2

    .line 289
    :cond_1
    :goto_0
    return v0

    .line 284
    :cond_2
    iget v1, p0, Lcom/twitter/library/provider/s$a;->e:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    iget-wide v2, p0, Lcom/twitter/library/provider/s$a;->d:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 289
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(I)Lcom/twitter/library/provider/s$a;
    .locals 0

    .prologue
    .line 206
    iput p1, p0, Lcom/twitter/library/provider/s$a;->e:I

    .line 207
    return-object p0
.end method

.method public a(J)Lcom/twitter/library/provider/s$a;
    .locals 1

    .prologue
    .line 200
    iput-wide p1, p0, Lcom/twitter/library/provider/s$a;->d:J

    .line 201
    return-object p0
.end method

.method public a(Laut;)Lcom/twitter/library/provider/s$a;
    .locals 0

    .prologue
    .line 248
    iput-object p1, p0, Lcom/twitter/library/provider/s$a;->l:Laut;

    .line 249
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/provider/s$a;
    .locals 0

    .prologue
    .line 212
    iput-object p1, p0, Lcom/twitter/library/provider/s$a;->f:Ljava/lang/String;

    .line 213
    return-object p0
.end method

.method public a(Z)Lcom/twitter/library/provider/s$a;
    .locals 0

    .prologue
    .line 218
    iput-boolean p1, p0, Lcom/twitter/library/provider/s$a;->g:Z

    .line 219
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/library/provider/s$a;
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lcom/twitter/library/provider/s$a;->j:Ljava/lang/String;

    .line 237
    return-object p0
.end method

.method public b(Z)Lcom/twitter/library/provider/s$a;
    .locals 0

    .prologue
    .line 224
    iput-boolean p1, p0, Lcom/twitter/library/provider/s$a;->h:Z

    .line 225
    return-object p0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/twitter/library/provider/s$a;->e()Lcom/twitter/library/provider/s;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/library/provider/s$a;
    .locals 0

    .prologue
    .line 254
    iput-object p1, p0, Lcom/twitter/library/provider/s$a;->m:Ljava/lang/String;

    .line 255
    return-object p0
.end method

.method public c(Z)Lcom/twitter/library/provider/s$a;
    .locals 0

    .prologue
    .line 230
    iput-boolean p1, p0, Lcom/twitter/library/provider/s$a;->i:Z

    .line 231
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/library/provider/s$a;
    .locals 0

    .prologue
    .line 260
    iput-object p1, p0, Lcom/twitter/library/provider/s$a;->n:Ljava/lang/String;

    .line 261
    return-object p0
.end method

.method public d(Z)Lcom/twitter/library/provider/s$a;
    .locals 0

    .prologue
    .line 242
    iput-boolean p1, p0, Lcom/twitter/library/provider/s$a;->k:Z

    .line 243
    return-object p0
.end method

.method public e(Z)Lcom/twitter/library/provider/s$a;
    .locals 0

    .prologue
    .line 266
    iput-boolean p1, p0, Lcom/twitter/library/provider/s$a;->o:Z

    .line 267
    return-object p0
.end method

.method public e()Lcom/twitter/library/provider/s;
    .locals 2

    .prologue
    .line 295
    new-instance v0, Lcom/twitter/library/provider/s;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/provider/s;-><init>(Lcom/twitter/library/provider/s$a;Lcom/twitter/library/provider/s$1;)V

    return-object v0
.end method

.method public f(Z)Lcom/twitter/library/provider/s$a;
    .locals 0

    .prologue
    .line 272
    iput-boolean p1, p0, Lcom/twitter/library/provider/s$a;->p:Z

    .line 273
    return-object p0
.end method
