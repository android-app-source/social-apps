.class Lcom/twitter/library/provider/i$15;
.super Laus$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/provider/i;->b()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic b:Lcom/twitter/library/provider/i;


# direct methods
.method constructor <init>(Lcom/twitter/library/provider/i;I)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lcom/twitter/library/provider/i$15;->b:Lcom/twitter/library/provider/i;

    invoke-direct {p0, p2}, Laus$a;-><init>(I)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/database/model/j;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4

    .prologue
    .line 269
    const-class v0, Lcom/twitter/database/schema/DraftsSchema$a;

    new-instance v1, Lcom/twitter/database/model/ColumnDefinition$a;

    const-string/jumbo v2, "periscope_creator_id"

    sget-object v3, Lcom/twitter/database/model/ColumnDefinition$Type;->e:Lcom/twitter/database/model/ColumnDefinition$Type;

    invoke-direct {v1, v2, v3}, Lcom/twitter/database/model/ColumnDefinition$a;-><init>(Ljava/lang/String;Lcom/twitter/database/model/ColumnDefinition$Type;)V

    const-wide/16 v2, -0x1

    .line 272
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/database/model/ColumnDefinition$a;->a(Ljava/lang/Object;)Lcom/twitter/database/model/ColumnDefinition$a;

    move-result-object v1

    .line 273
    invoke-virtual {v1}, Lcom/twitter/database/model/ColumnDefinition$a;->a()Lcom/twitter/database/model/ColumnDefinition;

    move-result-object v1

    .line 269
    invoke-interface {p1, v0, v1}, Lcom/twitter/database/model/j;->a(Ljava/lang/Class;Lcom/twitter/database/model/ColumnDefinition;)V

    .line 274
    const-class v0, Lcom/twitter/database/schema/DraftsSchema$a;

    new-instance v1, Lcom/twitter/database/model/ColumnDefinition$a;

    const-string/jumbo v2, "periscope_is_live"

    sget-object v3, Lcom/twitter/database/model/ColumnDefinition$Type;->h:Lcom/twitter/database/model/ColumnDefinition$Type;

    invoke-direct {v1, v2, v3}, Lcom/twitter/database/model/ColumnDefinition$a;-><init>(Ljava/lang/String;Lcom/twitter/database/model/ColumnDefinition$Type;)V

    const/4 v2, 0x0

    .line 277
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/database/model/ColumnDefinition$a;->a(Ljava/lang/Object;)Lcom/twitter/database/model/ColumnDefinition$a;

    move-result-object v1

    .line 278
    invoke-virtual {v1}, Lcom/twitter/database/model/ColumnDefinition$a;->a()Lcom/twitter/database/model/ColumnDefinition;

    move-result-object v1

    .line 274
    invoke-interface {p1, v0, v1}, Lcom/twitter/database/model/j;->a(Ljava/lang/Class;Lcom/twitter/database/model/ColumnDefinition;)V

    .line 279
    return-void
.end method
