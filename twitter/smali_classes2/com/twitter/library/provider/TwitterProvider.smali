.class public Lcom/twitter/library/provider/TwitterProvider;
.super Landroid/content/ContentProvider;
.source "Twttr"


# static fields
.field private static final a:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0x352

    .line 351
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    .line 354
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "users"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 355
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "users/id/#"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 357
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups"

    const/16 v3, 0x3c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 359
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/#"

    const/16 v3, 0x46

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 361
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/following/#"

    const/16 v3, 0x48

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 363
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/followers/#"

    const/16 v3, 0x49

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 365
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/verified_followers/#"

    const/16 v3, 0x63

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 367
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/blocked/#"

    const/16 v3, 0x4a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 369
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/members/#"

    const/16 v3, 0x4b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 371
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/subscribers/#"

    const/16 v3, 0x4c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 373
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/people/#"

    const/16 v3, 0x4d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 375
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/search"

    const/16 v3, 0x4e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 377
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/category_users/#"

    const/16 v3, 0x4f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 379
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/contacts/#"

    const/16 v3, 0x50

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 381
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/activity_sources/#"

    const/16 v3, 0x51

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 383
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/favorited/#"

    const/16 v3, 0x52

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 385
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/retweeted/#"

    const/16 v3, 0x53

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 387
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/media_tagged/#"

    const/16 v3, 0x60

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 389
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/activity_targets/#"

    const/16 v3, 0x54

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 391
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/dm_contacts/#"

    const/16 v3, 0x55

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 393
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/device_following/#"

    const/16 v3, 0x56

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 395
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/device_and_live_following/#"

    const/16 v3, 0x57

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 397
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/incoming_friendships/#"

    const/16 v3, 0x58

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 399
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/social_proof_favorited/#"

    const/16 v3, 0x59

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 401
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/find_people/#"

    const/16 v3, 0x62

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 403
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/similar_to/#"

    const/16 v3, 0x5a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 405
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/follow_recommendations/#"

    const/16 v3, 0x5b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 407
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/mutual_follows/#"

    const/16 v3, 0x5e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 409
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/geo_wtf/#"

    const/16 v3, 0x65

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 411
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/blocked_imported/#"

    const/16 v3, 0x66

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 413
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/muted/#"

    const/16 v3, 0x67

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 415
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/muted_automated/#"

    const/16 v3, 0x68

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 417
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/contact_forward_recommendation/#"

    const/16 v3, 0x69

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 419
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/reply_context_consume/#"

    const/16 v3, 0x6b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 421
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "user_groups_view/reply_context_compose/#"

    const/16 v3, 0x6a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 424
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "statuses"

    const/16 v3, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 425
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "statuses/id/#"

    const/16 v3, 0x15

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 427
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "drafts"

    const/16 v3, 0x28

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 428
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "drafts/id/#"

    const/16 v3, 0x29

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 430
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups"

    const/16 v3, 0x6e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 431
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups/#"

    const/16 v3, 0x6f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 433
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_view"

    const/16 v3, 0x78

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 434
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_view/#"

    const/16 v3, 0x79

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 436
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_view/ref_id/#"

    const/16 v3, 0x7a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 438
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_view/id/#"

    const/16 v3, 0x8e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 440
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_view/timeline/#"

    const/16 v3, 0x7b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 442
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_view/home/#"

    const/16 v3, 0x7d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 444
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_view/mentions/#"

    const/16 v3, 0x80

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 446
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_view/mentions_filtered/#"

    const/16 v3, 0x81

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 448
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_view/mentions_following/#"

    const/16 v3, 0x83

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 450
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_view/list/#"

    const/16 v3, 0x84

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 452
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_view/activity_targets/#"

    const/16 v3, 0x8a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 454
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_view/activity_target_objects/#"

    const/16 v3, 0x8b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 456
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_view/activity/#"

    const/16 v3, 0x89

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 458
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_view/custom_timeline_users"

    const/16 v3, 0x61

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 460
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_retweets_view/rt_timeline/#"

    const/16 v3, 0x87

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 462
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_retweets_view/rt_timeline_unlimited/#"

    const/16 v3, 0x92

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 464
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_view/media/#"

    const/16 v3, 0x8c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 466
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_retweets_view/rt_media/#"

    const/16 v3, 0x8d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 468
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_retweets_view/activity/#"

    const/16 v3, 0x8f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 470
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_retweets_view/#"

    const/16 v3, 0x90

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 472
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_retweets_view/ref_id/#"

    const/16 v3, 0x91

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 474
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "status_groups_view/moments/#"

    const/16 v3, 0x93

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 480
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "conversation/*"

    const/16 v3, 0xc9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 485
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "dm_inbox"

    const/16 v3, 0xca

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 490
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "dm_inbox/*"

    const/16 v3, 0xcb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 495
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "conversation_participants"

    const/16 v3, 0xcc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 501
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "conversation_participants/*"

    const/16 v3, 0xcd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 507
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "conversation_participants_users"

    const/16 v3, 0xce

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 513
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "conversation_participants_users/*"

    const/16 v3, 0xcf

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 519
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "dm_unread_count"

    const/16 v3, 0xd0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 521
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "lists_view"

    const/16 v3, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 522
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "lists_view/#"

    const/16 v3, 0x12d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 523
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "lists_view/id/#"

    const/16 v3, 0x12e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 525
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "timeline_view"

    const/16 v3, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 526
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "timeline_view/home/#"

    const/16 v3, 0x191

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 527
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "timeline_view/urt_home/#"

    const/16 v3, 0x1a1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 528
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "timeline_view/user/#"

    const/16 v3, 0x192

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 530
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "timeline_view/trendsplus/#"

    const/16 v3, 0x195

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 531
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "timeline_view/user_without_replies/#"

    const/16 v3, 0x1a2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 533
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "timeline_view/conversation/#"

    const/16 v3, 0x19e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 535
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "timeline_view/custom/#"

    const/16 v3, 0x194

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 537
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "timeline_view/place/#"

    const/16 v3, 0x193

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 539
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "timeline_view/live_video_event/#"

    const/16 v3, 0x19d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 541
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "timeline_view/list/#"

    const/16 v3, 0x19f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 542
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "timeline_view/urt_favorites/#"

    const/16 v3, 0x1a0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 544
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "timeline_view/urt_trends/#"

    const/16 v3, 0x1a5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 545
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "timeline_view/urt_moment_likes/#"

    const/16 v3, 0x1a6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 547
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "timeline_view/urt_search/#"

    const/16 v3, 0x1a7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 548
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "timeline_view/guide/#"

    const/16 v3, 0x1a8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 549
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "timeline_view/media/#"

    const/16 v3, 0x198

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 550
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "timeline_view/mentions/#"

    const/16 v3, 0x199

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 551
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "timeline_view/mentions_filtered/#"

    const/16 v3, 0x19a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 553
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "timeline_view/mentions_following/#"

    const/16 v3, 0x19c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 556
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "dismiss_info_view/timeline_id/#"

    const/16 v3, 0x1a4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 559
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "search_queries"

    const/16 v3, 0x208

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 560
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "search_queries/#"

    const/16 v3, 0x209

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 562
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "search_results_view"

    const/16 v3, 0x20a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 564
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "notifications_tab_view"

    const/16 v3, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 566
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "activities"

    const/16 v3, 0x2bc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 568
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "stories_view"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 569
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "stories_view"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 570
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "stories/tweets/*"

    const/16 v3, 0x353

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 572
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "categories_view"

    const/16 v3, 0x384

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 574
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "notifications"

    const/16 v3, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 576
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "order_history"

    const/16 v3, 0x578

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 577
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "news"

    const/16 v3, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 578
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "news/#"

    const/16 v3, 0x5dd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 579
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "news/tweets/#"

    const/16 v3, 0x5de

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 580
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "news/users/#"

    const/16 v3, 0x5df

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 583
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "moments_guide_categories"

    const/16 v3, 0x647

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 585
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "moments_sectioned_guide/#/#"

    const/16 v3, 0x648

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 587
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "moments_pages_view/#"

    const/16 v3, 0x641

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 588
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "moments/#"

    const/16 v3, 0x642

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 589
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "moments_pivot_guide/#"

    const/16 v3, 0x645

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 593
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "ads_account_permissions"

    const/16 v3, 0x6a4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 596
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "business_profiles"

    const/16 v3, 0x708

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 597
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "business_profiles/id/#"

    const/16 v3, 0x709

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 598
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "business_profiles/user_id/#"

    const/16 v3, 0x70a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 601
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "dm_card_state"

    const/16 v3, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 604
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "ads_view"

    const/16 v3, 0x834

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 606
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "carousel_view"

    const/16 v3, 0x898

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 608
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    const-string/jumbo v2, "campaigns_metadata_view"

    const/16 v3, 0x8fc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 609
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;
    .locals 17

    .prologue
    .line 1585
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 1586
    if-eqz p3, :cond_0

    .line 1587
    const-string/jumbo v1, "("

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1589
    :cond_0
    const-string/jumbo v1, "timeline_owner_id"

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x3d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1590
    invoke-static/range {p4 .. p4}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1591
    const-string/jumbo v1, " AND "

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1593
    :cond_1
    const-string/jumbo v1, "newer"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1594
    if-eqz v1, :cond_2

    .line 1595
    const-string/jumbo v2, " AND "

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "timeline_updated_at"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3e

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1598
    :cond_2
    invoke-static/range {p6 .. p6}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    move-object/from16 v6, p6

    .line 1605
    :goto_0
    :try_start_0
    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1611
    :goto_1
    if-gtz v1, :cond_5

    .line 1612
    const/16 v1, 0x190

    move/from16 v16, v1

    .line 1615
    :goto_2
    invoke-static {}, Lcqj;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1616
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 1617
    const-string/jumbo v2, "timeline_view"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1618
    const-string/jumbo v8, "TwitterProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "QUERY: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v2, Lbue;->a:[Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", POST-GROUPING LIMIT: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1623
    :cond_3
    const-string/jumbo v8, "timeline_view"

    sget-object v9, Lbue;->a:[Ljava/lang/String;

    .line 1624
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v15, 0x0

    move-object/from16 v7, p1

    move-object/from16 v11, p5

    move-object v14, v6

    .line 1623
    invoke-virtual/range {v7 .. v15}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1626
    new-instance v2, Lcom/twitter/library/provider/r;

    invoke-direct {v2, v1}, Lcom/twitter/library/provider/r;-><init>(Landroid/database/Cursor;)V

    .line 1627
    move/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/twitter/library/provider/r;->a(I)V

    .line 1628
    invoke-virtual {v2}, Lcom/twitter/library/provider/r;->b()V

    .line 1629
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/provider/TwitterProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lcom/twitter/database/schema/a$x;->a:Landroid/net/Uri;

    invoke-virtual {v2, v1, v3}, Lcom/twitter/library/provider/r;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 1630
    return-object v2

    .line 1601
    :cond_4
    const-string/jumbo v6, "status_groups_preview_draft_id DESC, timeline_updated_at DESC, _id ASC"

    goto/16 :goto_0

    .line 1606
    :catch_0
    move-exception v1

    .line 1607
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_5
    move/from16 v16, v1

    goto :goto_2
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1645
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Delete not supported: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1650
    sget-object v0, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1802
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1652
    :sswitch_0
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.users"

    .line 1799
    :goto_0
    return-object v0

    .line 1655
    :sswitch_1
    const-string/jumbo v0, "vnd.android.cursor.item/vnd.twitter.android.users"

    goto :goto_0

    .line 1685
    :sswitch_2
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.users.groups"

    goto :goto_0

    .line 1688
    :sswitch_3
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.statuses"

    goto :goto_0

    .line 1691
    :sswitch_4
    const-string/jumbo v0, "vnd.android.cursor.item/vnd.twitter.android.statuses"

    goto :goto_0

    .line 1694
    :sswitch_5
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.drafts"

    goto :goto_0

    .line 1697
    :sswitch_6
    const-string/jumbo v0, "vnd.android.cursor.item/vnd.twitter.android.drafts"

    goto :goto_0

    .line 1704
    :sswitch_7
    const-string/jumbo v0, "vnd.android.cursor.item/vnd.twitter.android.statuses"

    goto :goto_0

    .line 1719
    :sswitch_8
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.statuses"

    goto :goto_0

    .line 1722
    :sswitch_9
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.statuses.groups"

    goto :goto_0

    .line 1725
    :sswitch_a
    const-string/jumbo v0, "vnd.android.cursor.item/vnd.twitter.android.statuses.groups"

    goto :goto_0

    .line 1728
    :sswitch_b
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.lists"

    goto :goto_0

    .line 1732
    :sswitch_c
    const-string/jumbo v0, "vnd.android.cursor.item/vnd.twitter.android.lists"

    goto :goto_0

    .line 1746
    :sswitch_d
    const-string/jumbo v0, "vnd.android.cursor.item/vnd.twitter.android.timeline"

    goto :goto_0

    .line 1749
    :sswitch_e
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.search.queries"

    goto :goto_0

    .line 1752
    :sswitch_f
    const-string/jumbo v0, "vnd.android.cursor.item/vnd.twitter.android.search.queries"

    goto :goto_0

    .line 1755
    :sswitch_10
    const-string/jumbo v0, "vnd.android.cursor.item/vnd.twitter.android.search.results"

    goto :goto_0

    .line 1758
    :sswitch_11
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.notificationstab"

    goto :goto_0

    .line 1761
    :sswitch_12
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.activities"

    goto :goto_0

    .line 1764
    :sswitch_13
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.stories"

    goto :goto_0

    .line 1767
    :sswitch_14
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.statuses"

    goto :goto_0

    .line 1770
    :sswitch_15
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.search.queries"

    goto :goto_0

    .line 1773
    :sswitch_16
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.notifications"

    goto :goto_0

    .line 1776
    :sswitch_17
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.orderhistory"

    goto :goto_0

    .line 1780
    :sswitch_18
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.news.news"

    goto :goto_0

    .line 1783
    :sswitch_19
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.statuses"

    goto :goto_0

    .line 1786
    :sswitch_1a
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.adsaccountpermissions"

    goto :goto_0

    .line 1789
    :sswitch_1b
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.businessprofiles"

    goto :goto_0

    .line 1793
    :sswitch_1c
    const-string/jumbo v0, "vnd.android.cursor.item/vnd.twitter.android.businessprofiles"

    goto :goto_0

    .line 1796
    :sswitch_1d
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.ads"

    goto :goto_0

    .line 1799
    :sswitch_1e
    const-string/jumbo v0, "vnd.android.cursor.dir/vnd.twitter.android.carousel"

    goto :goto_0

    .line 1650
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x14 -> :sswitch_3
        0x15 -> :sswitch_4
        0x28 -> :sswitch_5
        0x29 -> :sswitch_6
        0x3c -> :sswitch_2
        0x46 -> :sswitch_2
        0x48 -> :sswitch_2
        0x49 -> :sswitch_2
        0x4a -> :sswitch_2
        0x4b -> :sswitch_2
        0x4c -> :sswitch_2
        0x4d -> :sswitch_2
        0x4e -> :sswitch_2
        0x51 -> :sswitch_2
        0x52 -> :sswitch_2
        0x53 -> :sswitch_2
        0x54 -> :sswitch_2
        0x55 -> :sswitch_2
        0x56 -> :sswitch_2
        0x57 -> :sswitch_2
        0x58 -> :sswitch_2
        0x5a -> :sswitch_2
        0x5b -> :sswitch_2
        0x60 -> :sswitch_2
        0x61 -> :sswitch_2
        0x62 -> :sswitch_2
        0x63 -> :sswitch_2
        0x66 -> :sswitch_2
        0x67 -> :sswitch_2
        0x69 -> :sswitch_2
        0x6a -> :sswitch_2
        0x6b -> :sswitch_2
        0x6e -> :sswitch_9
        0x6f -> :sswitch_a
        0x78 -> :sswitch_8
        0x79 -> :sswitch_7
        0x7a -> :sswitch_7
        0x7b -> :sswitch_8
        0x7d -> :sswitch_8
        0x80 -> :sswitch_8
        0x81 -> :sswitch_8
        0x83 -> :sswitch_8
        0x84 -> :sswitch_8
        0x87 -> :sswitch_8
        0x8a -> :sswitch_8
        0x8b -> :sswitch_8
        0x8c -> :sswitch_8
        0x8e -> :sswitch_7
        0x90 -> :sswitch_7
        0x91 -> :sswitch_7
        0x92 -> :sswitch_8
        0x93 -> :sswitch_8
        0x12c -> :sswitch_b
        0x12d -> :sswitch_c
        0x12e -> :sswitch_c
        0x190 -> :sswitch_d
        0x191 -> :sswitch_d
        0x192 -> :sswitch_d
        0x193 -> :sswitch_d
        0x194 -> :sswitch_d
        0x19d -> :sswitch_d
        0x19e -> :sswitch_d
        0x1a1 -> :sswitch_d
        0x1a2 -> :sswitch_d
        0x1a5 -> :sswitch_d
        0x1a6 -> :sswitch_d
        0x1a8 -> :sswitch_d
        0x208 -> :sswitch_e
        0x209 -> :sswitch_f
        0x20a -> :sswitch_10
        0x258 -> :sswitch_11
        0x2bc -> :sswitch_12
        0x352 -> :sswitch_13
        0x353 -> :sswitch_14
        0x384 -> :sswitch_15
        0x3e8 -> :sswitch_16
        0x578 -> :sswitch_17
        0x5dc -> :sswitch_18
        0x5dd -> :sswitch_18
        0x5de -> :sswitch_19
        0x6a4 -> :sswitch_1a
        0x708 -> :sswitch_1b
        0x709 -> :sswitch_1c
        0x70a -> :sswitch_1c
        0x834 -> :sswitch_1d
        0x898 -> :sswitch_1e
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 1635
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Insert not supported "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 613
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 13

    .prologue
    .line 619
    const/4 v2, 0x0

    .line 620
    const/4 v5, 0x0

    .line 621
    const-string/jumbo v0, "limit"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 622
    const-string/jumbo v0, "ownerId"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 624
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 625
    const-string/jumbo v3, "TwitterProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "QUERY uri: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, " -> "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v6, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    sget-object v3, Lcom/twitter/library/provider/TwitterProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    .line 629
    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-wide/16 v8, 0x0

    .line 630
    :goto_0
    const/16 v1, 0x29

    if-eq v3, v1, :cond_0

    const/16 v1, 0x28

    if-ne v3, v1, :cond_2

    .line 631
    :cond_0
    invoke-static {v8, v9}, Lcom/twitter/library/provider/h;->a(J)Lcom/twitter/library/provider/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/provider/h;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 636
    :goto_1
    sparse-switch v3, :sswitch_data_0

    .line 1567
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 629
    :cond_1
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    goto :goto_0

    .line 633
    :cond_2
    invoke-static {v8, v9}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/provider/t;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    goto :goto_1

    .line 639
    :sswitch_0
    const-string/jumbo v2, "users"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 640
    const/4 v2, 0x2

    if-ne v3, v2, :cond_3

    .line 641
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 643
    :cond_3
    const-string/jumbo v2, "profile_created DESC"

    move-object v8, v7

    move-object v9, v5

    .line 1572
    :goto_2
    invoke-static/range {p5 .. p5}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_11

    move-object/from16 v10, p5

    .line 1576
    :goto_3
    const-string/jumbo v11, "TwitterProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "QUERY: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, v0

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v7, p5

    invoke-virtual/range {v2 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v11, v2}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1577
    const/4 v6, 0x0

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object v5, v9

    move-object v7, v10

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1578
    const-string/jumbo v1, "TwitterProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "QUERY results: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1579
    invoke-virtual {p0}, Lcom/twitter/library/provider/TwitterProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 1580
    :goto_4
    return-object v0

    .line 647
    :sswitch_1
    const-string/jumbo v2, "user_groups"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 648
    const-string/jumbo v2, "rank ASC"

    .line 649
    if-nez v7, :cond_12

    .line 650
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto :goto_2

    .line 655
    :sswitch_2
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 656
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 657
    const-string/jumbo v2, "_id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 658
    goto/16 :goto_2

    .line 661
    :sswitch_3
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 662
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 664
    const-string/jumbo v2, "_id ASC"

    .line 665
    if-nez v7, :cond_12

    .line 666
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 671
    :sswitch_4
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 672
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 674
    const-string/jumbo v2, "_id ASC"

    .line 675
    if-nez v7, :cond_12

    .line 676
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 681
    :sswitch_5
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 682
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x1d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 684
    const-string/jumbo v2, "_id ASC"

    .line 685
    if-nez v7, :cond_12

    .line 686
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 691
    :sswitch_6
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 693
    const-string/jumbo v2, "user_groups_type=2"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 694
    const-string/jumbo v2, "_id ASC"

    .line 695
    if-nez v7, :cond_12

    .line 696
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 701
    :sswitch_7
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 703
    const-string/jumbo v2, "user_groups_type=4"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 704
    const-string/jumbo v2, "_id ASC"

    .line 705
    if-nez v7, :cond_12

    .line 706
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 711
    :sswitch_8
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 713
    const-string/jumbo v2, "user_groups_type=5"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 714
    const-string/jumbo v2, "_id ASC"

    .line 715
    if-nez v7, :cond_12

    .line 716
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 721
    :sswitch_9
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 723
    const-string/jumbo v2, "user_groups_type=3"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 724
    const-string/jumbo v2, "_id ASC"

    .line 725
    if-nez v7, :cond_12

    .line 726
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 731
    :sswitch_a
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 732
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " IN ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x1d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xf

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 739
    const-string/jumbo v5, "users_user_id"

    .line 740
    const-string/jumbo v2, "_id ASC"

    .line 741
    if-nez v7, :cond_13

    .line 742
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 747
    :sswitch_b
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 748
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 749
    const-string/jumbo v5, "users_user_id"

    .line 750
    const-string/jumbo v2, "_id ASC"

    .line 751
    if-nez v7, :cond_13

    .line 752
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 757
    :sswitch_c
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 758
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 760
    const-string/jumbo v5, "users_user_id"

    .line 761
    const-string/jumbo v2, "_id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 762
    goto/16 :goto_2

    .line 765
    :sswitch_d
    const-string/jumbo v2, "status_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 766
    const-string/jumbo v5, "username"

    .line 767
    const-string/jumbo v2, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    .line 768
    if-nez v7, :cond_13

    .line 769
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 774
    :sswitch_e
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 775
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 777
    const-string/jumbo v5, "users_user_id"

    .line 778
    const-string/jumbo v2, "_id ASC"

    .line 779
    if-nez v7, :cond_13

    .line 780
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 785
    :sswitch_f
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 786
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 788
    const-string/jumbo v5, "users_user_id"

    .line 789
    const-string/jumbo v2, "_id ASC"

    .line 790
    if-nez v7, :cond_13

    .line 791
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 796
    :sswitch_10
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 797
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "users_friendship"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x26

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x29

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 800
    const-string/jumbo v5, "users_user_id"

    .line 801
    const-string/jumbo v2, "users_friendship DESC, LOWER(users_name) ASC"

    .line 802
    if-nez v7, :cond_13

    .line 803
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 808
    :sswitch_11
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 809
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 811
    const-string/jumbo v2, "_id ASC"

    .line 812
    if-nez v7, :cond_12

    .line 813
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 818
    :sswitch_12
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 819
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 822
    const-string/jumbo v2, "_id ASC"

    .line 823
    if-nez v7, :cond_12

    .line 824
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 829
    :sswitch_13
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 830
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 832
    const-string/jumbo v5, "users_user_id"

    .line 833
    const-string/jumbo v2, "_id ASC"

    .line 834
    if-nez v7, :cond_13

    .line 835
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 840
    :sswitch_14
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 841
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 843
    const-string/jumbo v5, "users_user_id"

    .line 844
    const-string/jumbo v2, "_id ASC"

    .line 845
    if-nez v7, :cond_13

    .line 846
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 851
    :sswitch_15
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 852
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x19

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 854
    const-string/jumbo v5, "users_user_id"

    .line 855
    const-string/jumbo v2, "_id ASC"

    .line 856
    if-nez v7, :cond_13

    .line 857
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 862
    :sswitch_16
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 863
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x12

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 865
    const-string/jumbo v5, "users_user_id"

    .line 866
    const-string/jumbo v2, "_id ASC"

    .line 867
    if-nez v7, :cond_13

    .line 868
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 873
    :sswitch_17
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 874
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 876
    const-string/jumbo v5, "users_user_id"

    .line 877
    const-string/jumbo v2, "_id ASC"

    .line 878
    if-nez v7, :cond_13

    .line 879
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 884
    :sswitch_18
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 885
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x27

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND NOT ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "users_friendship"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x26

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 888
    const-string/jumbo v5, "users_user_id"

    .line 889
    const-string/jumbo v2, "_id ASC"

    .line 890
    if-nez v7, :cond_13

    .line 891
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 896
    :sswitch_19
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 897
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 899
    const-string/jumbo v2, "_id ASC"

    .line 900
    if-nez v7, :cond_12

    .line 901
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 906
    :sswitch_1a
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 907
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 909
    const-string/jumbo v2, "_id ASC"

    .line 910
    if-nez v7, :cond_12

    .line 911
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 916
    :sswitch_1b
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 917
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "users_friendship"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "&("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x7c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x29

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x7c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 921
    const-string/jumbo v5, "users_user_id"

    .line 922
    const-string/jumbo v2, "LOWER(users_name) ASC"

    .line 923
    if-nez v7, :cond_13

    .line 924
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 929
    :sswitch_1c
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 930
    const-string/jumbo v2, "user_groups_type=33"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 931
    const-string/jumbo v2, "_id ASC"

    .line 932
    if-nez v7, :cond_12

    .line 933
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 938
    :sswitch_1d
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 940
    const-string/jumbo v2, "user_groups_type=37"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 941
    const-string/jumbo v2, "_id ASC"

    .line 942
    if-nez v7, :cond_12

    .line 943
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 948
    :sswitch_1e
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 950
    const-string/jumbo v2, "user_groups_type=26"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 951
    const-string/jumbo v2, "_id ASC"

    .line 952
    if-nez v7, :cond_12

    .line 953
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 958
    :sswitch_1f
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 960
    const-string/jumbo v2, "user_groups_type=38"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 961
    const-string/jumbo v2, "_id ASC"

    .line 962
    if-nez v7, :cond_12

    .line 963
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 968
    :sswitch_20
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 969
    const-string/jumbo v2, "user_groups_type=41 AND NOT (users_friendship&4=4)"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 972
    const-string/jumbo v2, "_id ASC"

    .line 973
    if-nez v7, :cond_12

    .line 974
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 979
    :sswitch_21
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 980
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND NOT ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "users_friendship"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x26

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 984
    const-string/jumbo v5, "users_user_id"

    .line 985
    const-string/jumbo v2, "_id ASC"

    .line 986
    if-nez v7, :cond_13

    .line 987
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 992
    :sswitch_22
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 993
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2a

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND NOT ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "users_friendship"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x26

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 997
    const-string/jumbo v5, "users_user_id"

    .line 998
    const-string/jumbo v2, "_id ASC"

    .line 999
    if-nez v7, :cond_13

    .line 1000
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 1006
    :sswitch_23
    const-string/jumbo v2, "statuses"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1007
    const/16 v2, 0x15

    if-ne v3, v2, :cond_4

    .line 1008
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1010
    :cond_4
    const-string/jumbo v2, "created DESC"

    .line 1011
    if-nez v7, :cond_12

    .line 1012
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 1018
    :sswitch_24
    const-string/jumbo v2, "drafts"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1019
    const/16 v2, 0x29

    if-ne v3, v2, :cond_5

    .line 1020
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1022
    :cond_5
    const-string/jumbo v2, "updated_at DESC"

    .line 1023
    if-nez v7, :cond_12

    .line 1024
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 1030
    :sswitch_25
    const-string/jumbo v2, "status_groups"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1031
    const/16 v2, 0x6f

    if-ne v3, v2, :cond_6

    .line 1032
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1034
    :cond_6
    const-string/jumbo v2, "type DESC"

    .line 1035
    if-nez v7, :cond_12

    .line 1036
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 1041
    :sswitch_26
    const-string/jumbo v2, "status_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1042
    const-string/jumbo v2, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    .line 1043
    if-nez v7, :cond_12

    .line 1044
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 1049
    :sswitch_27
    const-string/jumbo v2, "status_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1050
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1051
    const-string/jumbo v2, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    .line 1052
    if-nez v7, :cond_12

    .line 1053
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 1058
    :sswitch_28
    const-string/jumbo v2, "status_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1059
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status_groups_ref_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1061
    const-string/jumbo v7, "1"

    .line 1062
    const-string/jumbo v2, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1063
    goto/16 :goto_2

    .line 1066
    :sswitch_29
    const-string/jumbo v2, "status_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1067
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status_groups_g_status_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1069
    const-string/jumbo v7, "1"

    .line 1070
    const-string/jumbo v2, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1071
    goto/16 :goto_2

    .line 1074
    :sswitch_2a
    const-string/jumbo v2, "status_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1075
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "status_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1077
    const-string/jumbo v2, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    .line 1078
    if-nez v7, :cond_12

    .line 1079
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 1085
    :sswitch_2b
    const-string/jumbo v2, "status_groups_retweets_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1086
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "status_groups_owner_id="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, " AND "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "status_groups_type"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v4, 0x3d

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1088
    const-string/jumbo v2, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    .line 1089
    const/16 v4, 0x87

    if-ne v4, v3, :cond_12

    if-nez v7, :cond_12

    .line 1090
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 1095
    :sswitch_2c
    const-string/jumbo v2, "status_groups_retweets_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1096
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status_groups_g_status_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1098
    const-string/jumbo v7, "1"

    .line 1099
    const-string/jumbo v2, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1100
    goto/16 :goto_2

    .line 1103
    :sswitch_2d
    const-string/jumbo v2, "status_groups_retweets_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1104
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status_groups_ref_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1106
    const-string/jumbo v7, "1"

    .line 1107
    const-string/jumbo v2, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1108
    goto/16 :goto_2

    .line 1111
    :sswitch_2e
    const-string/jumbo v2, "status_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1112
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "status_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1114
    const-string/jumbo v2, "newer"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1115
    if-eqz v2, :cond_7

    .line 1116
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, " AND status_groups_updated_at>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1118
    :cond_7
    const-string/jumbo v2, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    .line 1119
    if-nez v7, :cond_12

    .line 1120
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 1125
    :sswitch_2f
    const-string/jumbo v2, "status_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1126
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "status_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1128
    const-string/jumbo v2, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1129
    goto/16 :goto_2

    .line 1132
    :sswitch_30
    const-string/jumbo v2, "status_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1133
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "status_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x17

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1135
    const-string/jumbo v2, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1136
    goto/16 :goto_2

    .line 1139
    :sswitch_31
    const-string/jumbo v2, "status_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1140
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "status_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x18

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1142
    const-string/jumbo v2, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1143
    goto/16 :goto_2

    .line 1146
    :sswitch_32
    const-string/jumbo v3, "status_groups_retweets_view"

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1147
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "status_groups_owner_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "status_groups_type"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x3d

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x9

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1149
    if-nez v7, :cond_12

    .line 1150
    const/16 v3, 0x190

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 1155
    :sswitch_33
    const-string/jumbo v2, "status_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1156
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "status_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1158
    const-string/jumbo v2, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1159
    goto/16 :goto_2

    .line 1162
    :sswitch_34
    const-string/jumbo v2, "status_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1163
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "status_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xe

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1165
    const-string/jumbo v2, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1166
    goto/16 :goto_2

    .line 1169
    :sswitch_35
    const-string/jumbo v2, "status_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1170
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1171
    const-string/jumbo v2, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1172
    goto/16 :goto_2

    .line 1175
    :sswitch_36
    const-string/jumbo v2, "status_groups_retweets_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1176
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1177
    const-string/jumbo v2, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1178
    goto/16 :goto_2

    .line 1181
    :sswitch_37
    const-string/jumbo v2, "status_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1182
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "status_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " )"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1184
    const-string/jumbo v2, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1185
    goto/16 :goto_2

    .line 1188
    :sswitch_38
    const-string/jumbo v2, "status_groups_retweets_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1189
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status_groups_owner_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "status_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1191
    const-string/jumbo v2, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1192
    goto/16 :goto_2

    .line 1195
    :sswitch_39
    const-string/jumbo v2, "status_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1196
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status_groups_type=33 AND status_groups_tag="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1197
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1196
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1198
    const-string/jumbo v2, "status_groups_preview_draft_id DESC, status_groups_updated_at DESC, _id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1199
    goto/16 :goto_2

    .line 1202
    :sswitch_3a
    const/4 v4, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;

    move-result-object v0

    goto/16 :goto_4

    .line 1205
    :sswitch_3b
    const/4 v0, 0x0

    invoke-static {v0}, Lbpv;->b(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    .line 1206
    const-string/jumbo v4, "timeline_type=0 AND (timeline_data_type_group=0 OR timeline_data_type_group IS NULL)"

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;

    move-result-object v0

    goto/16 :goto_4

    .line 1210
    :sswitch_3c
    const/16 v0, 0x11

    invoke-static {v0}, Lbpv;->b(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    .line 1211
    const-string/jumbo v4, "timeline_type=17 AND (timeline_data_type_group=0 OR timeline_data_type_group IS NULL)"

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;

    move-result-object v0

    goto/16 :goto_4

    .line 1215
    :sswitch_3d
    const-string/jumbo v4, "timeline_type=1 AND (timeline_data_type_group=1 OR timeline_data_type_group IS NULL)"

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;

    move-result-object v0

    goto/16 :goto_4

    .line 1219
    :sswitch_3e
    const-string/jumbo v4, "timeline_type=18 AND (timeline_data_type_group=1 OR timeline_data_type_group IS NULL)"

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;

    move-result-object v0

    goto/16 :goto_4

    .line 1223
    :sswitch_3f
    const-string/jumbo v4, "timeline_type=6 AND (timeline_data_type_group=28 OR timeline_data_type_group IS NULL)"

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;

    move-result-object v0

    goto/16 :goto_4

    .line 1227
    :sswitch_40
    const-string/jumbo v4, "timeline_type=5 AND (timeline_data_type_group=27 OR timeline_data_type_group IS NULL)"

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;

    move-result-object v0

    goto/16 :goto_4

    .line 1231
    :sswitch_41
    const-string/jumbo v4, "timeline_type=14 AND (timeline_data_type_group=37 OR timeline_data_type_group IS NULL)"

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;

    move-result-object v0

    goto/16 :goto_4

    .line 1235
    :sswitch_42
    const-string/jumbo v4, "timeline_type=3 AND (timeline_data_type_group=30 OR timeline_data_type_group IS NULL)"

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;

    move-result-object v0

    goto/16 :goto_4

    .line 1239
    :sswitch_43
    const-string/jumbo v4, "timeline_type=13 AND (timeline_data_type_group=3 OR timeline_data_type_group IS NULL)"

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;

    move-result-object v0

    goto/16 :goto_4

    .line 1243
    :sswitch_44
    const-string/jumbo v4, "timeline_type=15 AND (timeline_data_type_group=9 OR timeline_data_type_group IS NULL)"

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;

    move-result-object v0

    goto/16 :goto_4

    .line 1247
    :sswitch_45
    const-string/jumbo v4, "timeline_type=16 AND (timeline_data_type_group=2 OR timeline_data_type_group IS NULL)"

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;

    move-result-object v0

    goto/16 :goto_4

    .line 1251
    :sswitch_46
    const-string/jumbo v4, "timeline_type=2 AND (timeline_data_type_group=17 OR timeline_data_type_group IS NULL)"

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;

    move-result-object v0

    goto/16 :goto_4

    .line 1255
    :sswitch_47
    const-string/jumbo v4, "timeline_type=9 AND (timeline_data_type_group=5 OR timeline_data_type_group IS NULL)"

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;

    move-result-object v0

    goto/16 :goto_4

    .line 1259
    :sswitch_48
    const-string/jumbo v4, "timeline_type=10 AND (timeline_data_type_group=23 OR timeline_data_type_group IS NULL)"

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;

    move-result-object v0

    goto/16 :goto_4

    .line 1263
    :sswitch_49
    const-string/jumbo v4, "timeline_type=12 AND (timeline_data_type_group=24 OR timeline_data_type_group IS NULL)"

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;

    move-result-object v0

    goto/16 :goto_4

    .line 1267
    :sswitch_4a
    const-string/jumbo v4, "timeline_type=19 AND (timeline_data_type_group=38 OR timeline_data_type_group IS NULL)"

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;

    move-result-object v0

    goto/16 :goto_4

    .line 1271
    :sswitch_4b
    const-string/jumbo v4, "timeline_type=20"

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;

    move-result-object v0

    goto/16 :goto_4

    .line 1275
    :sswitch_4c
    const-string/jumbo v4, "timeline_type=21"

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;

    move-result-object v0

    goto/16 :goto_4

    .line 1279
    :sswitch_4d
    const-string/jumbo v4, "timeline_type=22"

    move-object v0, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/provider/TwitterProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/provider/r;

    move-result-object v0

    goto/16 :goto_4

    .line 1283
    :sswitch_4e
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    .line 1284
    const-string/jumbo v4, "dismiss_info_view"

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1285
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "dismiss_info_timeline_id="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    move-object v8, v7

    move-object v9, v5

    .line 1286
    goto/16 :goto_2

    .line 1289
    :sswitch_4f
    const-string/jumbo v2, "lists_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1290
    const-string/jumbo v2, "topics_ev_query ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1291
    goto/16 :goto_2

    .line 1294
    :sswitch_50
    const-string/jumbo v2, "lists_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1295
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1296
    const-string/jumbo v2, "topics_ev_query ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1297
    goto/16 :goto_2

    .line 1300
    :sswitch_51
    const-string/jumbo v2, "lists_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1301
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "topics_ev_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1302
    const-string/jumbo v2, "topics_ev_query ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1303
    goto/16 :goto_2

    .line 1306
    :sswitch_52
    const-string/jumbo v2, "conversation"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1307
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    .line 1308
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v6, "conversation_entries_conversation_id"

    .line 1309
    invoke-static {v6, v2}, Laux;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v4

    const/4 v2, 0x1

    const-string/jumbo v4, "conversation_entries_entry_type"

    sget-object v6, Lcbu$a;->c:Ljava/util/Set;

    .line 1310
    invoke-static {v4, v6}, Laux;->a(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    .line 1308
    invoke-static {v3}, Laux;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1312
    const-string/jumbo v2, "conversation_entries_sort_entry_id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1313
    goto/16 :goto_2

    .line 1316
    :sswitch_53
    const-string/jumbo v2, "dm_inbox"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1317
    const-string/jumbo v2, "conversations_sort_event_id DESC"

    move-object v8, v7

    move-object v9, v5

    .line 1318
    goto/16 :goto_2

    .line 1321
    :sswitch_54
    const-string/jumbo v2, "dm_inbox"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1322
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    .line 1324
    const-string/jumbo v2, "conversations_conversation_id"

    invoke-static {v2, v3}, Laux;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1327
    invoke-static {v3}, Lcom/twitter/library/dm/e;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1328
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v2, v4, v6

    const/4 v2, 0x1

    const-string/jumbo v6, "conversations_local_conversation_id"

    .line 1330
    invoke-static {v6, v3}, Laux;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v2

    .line 1328
    invoke-static {v4}, Laux;->b([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1336
    :cond_8
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1337
    const-string/jumbo v2, "conversations_sort_event_id DESC"

    .line 1338
    const-string/jumbo v7, "1"

    move-object v8, v7

    move-object v9, v5

    .line 1339
    goto/16 :goto_2

    .line 1342
    :sswitch_55
    const-string/jumbo v2, "conversation_participants"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1343
    const-string/jumbo v2, "participant_type,join_time ASC,user_id"

    move-object v8, v7

    move-object v9, v5

    .line 1344
    goto/16 :goto_2

    .line 1347
    :sswitch_56
    const-string/jumbo v2, "conversation_participants"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1348
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    .line 1349
    const-string/jumbo v3, "conversation_id"

    invoke-static {v3, v2}, Laux;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1350
    const-string/jumbo v2, "participant_type,join_time ASC,user_id"

    move-object v8, v7

    move-object v9, v5

    .line 1351
    goto/16 :goto_2

    .line 1354
    :sswitch_57
    const-string/jumbo v2, "conversation_participants_users"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1355
    const-string/jumbo v2, "conversation_participants_participant_type,conversation_participants_join_time ASC,CAST(conversation_participants_user_id AS INT)"

    move-object v8, v7

    move-object v9, v5

    .line 1356
    goto/16 :goto_2

    .line 1359
    :sswitch_58
    const-string/jumbo v2, "conversation_participants_users"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1360
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    .line 1361
    const-string/jumbo v3, "conversation_participants_conversation_id"

    invoke-static {v3, v2}, Laux;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1363
    const-string/jumbo v2, "conversation_participants_participant_type,conversation_participants_join_time ASC,CAST(conversation_participants_user_id AS INT)"

    move-object v8, v7

    move-object v9, v5

    .line 1364
    goto/16 :goto_2

    .line 1367
    :sswitch_59
    const-string/jumbo v0, "SELECT COUNT(*) FROM conversations WHERE last_readable_event_id > last_read_event_id AND last_readable_event_id > (SELECT COALESCE((SELECT CAST(next as int) AS last_seen_event_id FROM cursors WHERE kind=14 AND type=0 ORDER BY last_seen_event_id DESC LIMIT 1), 0));"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1368
    invoke-virtual {p0}, Lcom/twitter/library/provider/TwitterProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto/16 :goto_4

    .line 1372
    :sswitch_5a
    const-string/jumbo v2, "search_queries"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1373
    const-string/jumbo v2, "like"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1374
    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_9

    .line 1375
    const-string/jumbo v3, "name LIKE "

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1376
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1378
    :cond_9
    const-string/jumbo v2, "name ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1379
    goto/16 :goto_2

    .line 1382
    :sswitch_5b
    const-string/jumbo v2, "search_queries"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1383
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1384
    const-string/jumbo v2, "name ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1385
    goto/16 :goto_2

    .line 1388
    :sswitch_5c
    const-string/jumbo v2, "search_results_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1389
    const-string/jumbo v2, "polled=0"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1390
    const-string/jumbo v2, "type_id ASC, _id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1391
    goto/16 :goto_2

    .line 1394
    :sswitch_5d
    const-string/jumbo v2, "notifications_tab_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1395
    const-string/jumbo v2, "notifications_tab_sort_id DESC"

    move-object v8, v7

    move-object v9, v5

    .line 1396
    goto/16 :goto_2

    .line 1399
    :sswitch_5e
    const-string/jumbo v2, "activities"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1400
    const-string/jumbo v2, "max_position DESC"

    move-object v8, v7

    move-object v9, v5

    .line 1401
    goto/16 :goto_2

    .line 1404
    :sswitch_5f
    const-string/jumbo v2, "stories_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1405
    invoke-static/range {p5 .. p5}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    move-object/from16 v2, p5

    :goto_5
    move-object v8, v7

    move-object v9, v5

    .line 1406
    goto/16 :goto_2

    .line 1405
    :cond_a
    const-string/jumbo v2, "story_order DESC, _id ASC"

    goto :goto_5

    .line 1409
    :sswitch_60
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 1410
    const-string/jumbo v2, "search_id"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1414
    const-string/jumbo v2, "stories_view"

    sget-object v3, Lbuc;->a:[Ljava/lang/String;

    const-string/jumbo v4, "story_id=? AND data_type=?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    const/4 v6, 0x4

    .line 1416
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 1417
    invoke-static/range {p5 .. p5}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string/jumbo v8, "story_order DESC, _id ASC"

    .line 1414
    :goto_6
    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1419
    invoke-virtual {p0}, Lcom/twitter/library/provider/TwitterProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-interface {v10, v0, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 1422
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1423
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1424
    const-string/jumbo v2, " AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "status_groups_g_status_id"

    .line 1425
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " NOT IN ("

    .line 1426
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1427
    const/4 v2, 0x1

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1428
    :goto_7
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1429
    const/16 v2, 0x2c

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    .line 1430
    invoke-interface {v10, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto :goto_7

    :cond_b
    move-object/from16 v8, p5

    .line 1417
    goto :goto_6

    .line 1432
    :cond_c
    const/16 v2, 0x29

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1437
    :cond_d
    const-string/jumbo v2, "search_results_view"

    sget-object v3, Lbtv;->a:[Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "search_id=?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v9, v5, v0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 1439
    invoke-static/range {p5 .. p5}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    const-string/jumbo v8, "type_id ASC, _id ASC"

    .line 1437
    :goto_8
    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1440
    invoke-virtual {p0}, Lcom/twitter/library/provider/TwitterProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-interface {v1, v0, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 1443
    new-instance v0, Landroid/database/MergeCursor;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/database/Cursor;

    const/4 v3, 0x0

    aput-object v10, v2, v3

    const/4 v3, 0x1

    aput-object v1, v2, v3

    invoke-direct {v0, v2}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    goto/16 :goto_4

    :cond_e
    move-object/from16 v8, p5

    .line 1439
    goto :goto_8

    .line 1446
    :sswitch_61
    const-string/jumbo v2, "categories_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1447
    new-instance v9, Lcom/twitter/library/provider/a;

    const/4 v6, 0x0

    .line 1449
    invoke-static/range {p5 .. p5}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_f

    const-string/jumbo v7, "user_groups_view_user_groups_rank ASC "

    :goto_9
    const/4 v8, 0x0

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    .line 1448
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {v9, v0}, Lcom/twitter/library/provider/a;-><init>(Landroid/database/Cursor;)V

    .line 1452
    invoke-virtual {v9}, Lcom/twitter/library/provider/a;->b()V

    .line 1453
    invoke-virtual {p0}, Lcom/twitter/library/provider/TwitterProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v9, v0, p1}, Lcom/twitter/library/provider/a;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    move-object v0, v9

    .line 1454
    goto/16 :goto_4

    :cond_f
    move-object/from16 v7, p5

    .line 1449
    goto :goto_9

    .line 1457
    :sswitch_62
    const-string/jumbo v2, "notifications"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1458
    const-string/jumbo v2, "notif_id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1459
    goto/16 :goto_2

    .line 1462
    :sswitch_63
    const-string/jumbo v2, "order_history"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1463
    const-string/jumbo v2, "ordered_at DESC"

    move-object v8, v7

    move-object v9, v5

    .line 1464
    goto/16 :goto_2

    .line 1467
    :sswitch_64
    const-string/jumbo v2, "news"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1468
    const-string/jumbo v2, "_id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1469
    goto/16 :goto_2

    .line 1472
    :sswitch_65
    const-string/jumbo v2, "news"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1473
    const-string/jumbo v2, "topic_id="

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1474
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 1475
    const-string/jumbo v2, "_id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1476
    goto/16 :goto_2

    .line 1479
    :sswitch_66
    const-string/jumbo v2, "status_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1480
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "status_groups_tag="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "status_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1482
    const-string/jumbo v2, "_id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1483
    goto/16 :goto_2

    .line 1486
    :sswitch_67
    const-string/jumbo v2, "user_groups_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1487
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_groups_tag="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1488
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_groups_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x3d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x23

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1487
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1491
    const-string/jumbo v2, "_id ASC"

    .line 1493
    const-string/jumbo v7, "1"

    move-object v8, v7

    move-object v9, v5

    .line 1494
    goto/16 :goto_2

    .line 1497
    :sswitch_68
    const-string/jumbo v2, "moments_guide_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1498
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    .line 1500
    if-eqz v3, :cond_10

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    const/4 v4, 0x3

    if-ne v2, v4, :cond_10

    .line 1501
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "moments_sections_section_group_type="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v2, 0x1

    .line 1502
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, " AND "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "moments_sections_section_group_id"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v2, 0x2

    .line 1503
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1501
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1504
    const-string/jumbo v2, "moments_guide_section_id ASC, _id"

    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 1506
    :cond_10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Modern guide query without group type or group id"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1511
    :sswitch_69
    const-string/jumbo v2, "moments_pages_view"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1512
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "moments_pages_moment_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1513
    const-string/jumbo v2, "_id ASC"

    move-object v8, v7

    move-object v9, v5

    .line 1514
    goto/16 :goto_2

    .line 1517
    :sswitch_6a
    const-string/jumbo v3, "moments"

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1518
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    move-object v8, v7

    move-object v9, v5

    .line 1519
    goto/16 :goto_2

    .line 1522
    :sswitch_6b
    const-string/jumbo v3, "moments_guide_view"

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1523
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "moments_sections_section_group_type=1 AND moments_sections_section_group_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1525
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1523
    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    move-object v8, v7

    move-object v9, v5

    .line 1526
    goto/16 :goto_2

    .line 1529
    :sswitch_6c
    const-string/jumbo v3, "moments_guide_categories"

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    move-object v8, v7

    move-object v9, v5

    .line 1530
    goto/16 :goto_2

    .line 1533
    :sswitch_6d
    const-string/jumbo v3, "ads_account_permissions"

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    move-object v8, v7

    move-object v9, v5

    .line 1534
    goto/16 :goto_2

    .line 1537
    :sswitch_6e
    const-string/jumbo v3, "business_profiles"

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    move-object v8, v7

    move-object v9, v5

    .line 1538
    goto/16 :goto_2

    .line 1541
    :sswitch_6f
    const-string/jumbo v3, "business_profiles"

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1542
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    move-object v8, v7

    move-object v9, v5

    .line 1543
    goto/16 :goto_2

    .line 1546
    :sswitch_70
    const-string/jumbo v3, "business_profiles"

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1547
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "user_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    move-object v8, v7

    move-object v9, v5

    .line 1548
    goto/16 :goto_2

    .line 1551
    :sswitch_71
    const-string/jumbo v3, "dm_card_state"

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    move-object v8, v7

    move-object v9, v5

    .line 1552
    goto/16 :goto_2

    .line 1555
    :sswitch_72
    const-string/jumbo v3, "ads_view"

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    move-object v8, v7

    move-object v9, v5

    .line 1556
    goto/16 :goto_2

    .line 1559
    :sswitch_73
    const-string/jumbo v3, "carousel_view"

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    move-object v8, v7

    move-object v9, v5

    .line 1560
    goto/16 :goto_2

    .line 1563
    :sswitch_74
    const-string/jumbo v3, "campaigns_metadata_view"

    invoke-virtual {v0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    move-object v8, v7

    move-object v9, v5

    .line 1564
    goto/16 :goto_2

    :cond_11
    move-object v10, v2

    goto/16 :goto_3

    :cond_12
    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    :cond_13
    move-object v8, v7

    move-object v9, v5

    goto/16 :goto_2

    .line 636
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x14 -> :sswitch_23
        0x15 -> :sswitch_23
        0x28 -> :sswitch_24
        0x29 -> :sswitch_24
        0x3c -> :sswitch_1
        0x46 -> :sswitch_2
        0x48 -> :sswitch_3
        0x49 -> :sswitch_4
        0x4a -> :sswitch_6
        0x4b -> :sswitch_7
        0x4c -> :sswitch_8
        0x4d -> :sswitch_a
        0x4e -> :sswitch_9
        0x4f -> :sswitch_b
        0x50 -> :sswitch_c
        0x51 -> :sswitch_e
        0x52 -> :sswitch_13
        0x53 -> :sswitch_14
        0x54 -> :sswitch_f
        0x55 -> :sswitch_10
        0x56 -> :sswitch_11
        0x57 -> :sswitch_12
        0x58 -> :sswitch_16
        0x59 -> :sswitch_17
        0x5a -> :sswitch_19
        0x5b -> :sswitch_1a
        0x5e -> :sswitch_1b
        0x60 -> :sswitch_15
        0x61 -> :sswitch_d
        0x62 -> :sswitch_18
        0x63 -> :sswitch_5
        0x65 -> :sswitch_1c
        0x66 -> :sswitch_1d
        0x67 -> :sswitch_1e
        0x68 -> :sswitch_1f
        0x69 -> :sswitch_20
        0x6a -> :sswitch_22
        0x6b -> :sswitch_21
        0x6e -> :sswitch_25
        0x6f -> :sswitch_25
        0x78 -> :sswitch_26
        0x79 -> :sswitch_27
        0x7a -> :sswitch_28
        0x7b -> :sswitch_2a
        0x7d -> :sswitch_2e
        0x80 -> :sswitch_2f
        0x81 -> :sswitch_30
        0x83 -> :sswitch_31
        0x84 -> :sswitch_32
        0x87 -> :sswitch_2b
        0x89 -> :sswitch_35
        0x8a -> :sswitch_33
        0x8b -> :sswitch_34
        0x8c -> :sswitch_37
        0x8d -> :sswitch_38
        0x8e -> :sswitch_29
        0x8f -> :sswitch_36
        0x90 -> :sswitch_2c
        0x91 -> :sswitch_2d
        0x92 -> :sswitch_2b
        0x93 -> :sswitch_39
        0xc9 -> :sswitch_52
        0xca -> :sswitch_53
        0xcb -> :sswitch_54
        0xcc -> :sswitch_55
        0xcd -> :sswitch_56
        0xce -> :sswitch_57
        0xcf -> :sswitch_58
        0xd0 -> :sswitch_59
        0x12c -> :sswitch_4f
        0x12d -> :sswitch_50
        0x12e -> :sswitch_51
        0x190 -> :sswitch_3a
        0x191 -> :sswitch_3b
        0x192 -> :sswitch_3d
        0x193 -> :sswitch_42
        0x194 -> :sswitch_40
        0x195 -> :sswitch_3f
        0x198 -> :sswitch_46
        0x199 -> :sswitch_47
        0x19a -> :sswitch_48
        0x19c -> :sswitch_49
        0x19d -> :sswitch_43
        0x19e -> :sswitch_41
        0x19f -> :sswitch_44
        0x1a0 -> :sswitch_45
        0x1a1 -> :sswitch_3c
        0x1a2 -> :sswitch_3e
        0x1a4 -> :sswitch_4e
        0x1a5 -> :sswitch_4a
        0x1a6 -> :sswitch_4b
        0x1a7 -> :sswitch_4c
        0x1a8 -> :sswitch_4d
        0x208 -> :sswitch_5a
        0x209 -> :sswitch_5b
        0x20a -> :sswitch_5c
        0x258 -> :sswitch_5d
        0x2bc -> :sswitch_5e
        0x352 -> :sswitch_5f
        0x353 -> :sswitch_60
        0x384 -> :sswitch_61
        0x3e8 -> :sswitch_62
        0x578 -> :sswitch_63
        0x5dc -> :sswitch_64
        0x5dd -> :sswitch_65
        0x5de -> :sswitch_66
        0x5df -> :sswitch_67
        0x641 -> :sswitch_69
        0x642 -> :sswitch_6a
        0x645 -> :sswitch_6b
        0x647 -> :sswitch_6c
        0x648 -> :sswitch_68
        0x6a4 -> :sswitch_6d
        0x708 -> :sswitch_6e
        0x709 -> :sswitch_6f
        0x70a -> :sswitch_70
        0x7d0 -> :sswitch_71
        0x834 -> :sswitch_72
        0x898 -> :sswitch_73
        0x8fc -> :sswitch_74
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1640
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Update not supported: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
