.class Lcom/twitter/library/provider/k$15;
.super Laus$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/provider/k;->b()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic b:Lcom/twitter/library/provider/k;


# direct methods
.method constructor <init>(Lcom/twitter/library/provider/k;I)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/twitter/library/provider/k$15;->b:Lcom/twitter/library/provider/k;

    invoke-direct {p0, p2}, Laus$a;-><init>(I)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/database/model/j;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 26

    .prologue
    .line 138
    const-class v2, Laxb;

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Lcom/twitter/database/model/j;->a(Ljava/lang/Class;)V

    .line 139
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/k$15;->b:Lcom/twitter/library/provider/k;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/twitter/library/provider/k;->c(Lcom/twitter/library/provider/k;Z)Z

    .line 140
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/k$15;->b:Lcom/twitter/library/provider/k;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/twitter/library/provider/k;->d(Lcom/twitter/library/provider/k;Z)Z

    .line 141
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/k$15;->b:Lcom/twitter/library/provider/k;

    invoke-static {v2}, Lcom/twitter/library/provider/k;->a(Lcom/twitter/library/provider/k;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 142
    new-instance v6, Landroid/content/ContentValues;

    const/4 v2, 0x5

    invoke-direct {v6, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 144
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/k$15;->b:Lcom/twitter/library/provider/k;

    invoke-static {v2}, Lcom/twitter/library/provider/k;->b(Lcom/twitter/library/provider/k;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 146
    const-string/jumbo v2, "interval"

    const-string/jumbo v4, "polling_interval"

    const/16 v5, 0x5a0

    .line 148
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 147
    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 146
    invoke-virtual {v6, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 151
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "account_name"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "notif_id"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "notif_tweet"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string/jumbo v5, "notif_mention"

    aput-object v5, v2, v4

    const/4 v4, 0x4

    const-string/jumbo v5, "notif_message"

    aput-object v5, v2, v4

    move-object v10, v2

    .line 171
    :goto_0
    const-string/jumbo v2, "vibrate"

    const-string/jumbo v4, "vibrate"

    sget-boolean v5, Lcom/twitter/library/provider/GlobalDatabaseProvider$a;->b:Z

    .line 172
    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 171
    invoke-virtual {v6, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 173
    const-string/jumbo v2, "ringtone"

    const-string/jumbo v4, "ringtone"

    sget-object v5, Lcom/twitter/library/provider/GlobalDatabaseProvider$a;->a:Ljava/lang/String;

    .line 174
    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 173
    invoke-virtual {v6, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const-string/jumbo v2, "light"

    const-string/jumbo v4, "use_led"

    sget-boolean v5, Lcom/twitter/library/provider/GlobalDatabaseProvider$a;->c:Z

    .line 176
    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 175
    invoke-virtual {v6, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 179
    const-string/jumbo v2, "push_flags"

    sget v3, Lcga;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 180
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/k$15;->b:Lcom/twitter/library/provider/k;

    invoke-virtual {v2}, Lcom/twitter/library/provider/k;->c()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lakm;

    .line 181
    invoke-virtual {v2}, Lakm;->b()Lcnz;

    move-result-object v2

    invoke-virtual {v2}, Lcnz;->b()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/k$15;->b:Lcom/twitter/library/provider/k;

    .line 182
    invoke-static {v2}, Lcom/twitter/library/provider/k;->c(Lcom/twitter/library/provider/k;)Z

    move-result v7

    const/4 v8, 0x0

    move-object/from16 v3, p2

    .line 181
    invoke-static/range {v3 .. v8}, Lcom/twitter/library/provider/j;->a(Landroid/database/sqlite/SQLiteDatabase;JLandroid/content/ContentValues;ZLaut;)I

    goto :goto_1

    .line 160
    :cond_0
    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "account_name"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "notif_id"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "notif_tweet"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string/jumbo v5, "notif_mention"

    aput-object v5, v2, v4

    const/4 v4, 0x4

    const-string/jumbo v5, "notif_message"

    aput-object v5, v2, v4

    const/4 v4, 0x5

    const-string/jumbo v5, "interval"

    aput-object v5, v2, v4

    move-object v10, v2

    goto/16 :goto_0

    .line 186
    :cond_1
    const-string/jumbo v3, "activity_states"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p2

    move-object v4, v10

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 187
    if-eqz v22, :cond_5

    .line 189
    :try_start_0
    new-instance v23, Landroid/content/ContentValues;

    const/4 v2, 0x5

    move-object/from16 v0, v23

    invoke-direct {v0, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 190
    :cond_2
    :goto_2
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 191
    const/4 v2, 0x0

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 192
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/provider/k$15;->b:Lcom/twitter/library/provider/k;

    invoke-static {v3}, Lcom/twitter/library/provider/k;->d(Lcom/twitter/library/provider/k;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lakm;

    .line 193
    if-eqz v2, :cond_2

    .line 196
    invoke-virtual {v2}, Lakm;->b()Lcnz;

    move-result-object v2

    invoke-virtual {v2}, Lcnz;->b()J

    move-result-wide v24

    .line 197
    new-instance v2, Lcom/twitter/library/provider/n$a;

    invoke-direct {v2}, Lcom/twitter/library/provider/n$a;-><init>()V

    sget v3, Lcga;->a:I

    .line 198
    invoke-virtual {v2, v3}, Lcom/twitter/library/provider/n$a;->a(I)Lcom/twitter/library/provider/n$a;

    move-result-object v2

    const/4 v3, 0x2

    .line 199
    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/twitter/library/provider/n$a;->k(I)Lcom/twitter/library/provider/n$a;

    move-result-object v2

    const/4 v3, 0x3

    .line 200
    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/twitter/library/provider/n$a;->b(I)Lcom/twitter/library/provider/n$a;

    move-result-object v2

    const/4 v3, 0x4

    .line 201
    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/twitter/library/provider/n$a;->l(I)Lcom/twitter/library/provider/n$a;

    move-result-object v2

    .line 202
    invoke-virtual {v2}, Lcom/twitter/library/provider/n$a;->a()Lcom/twitter/library/provider/n;

    move-result-object v21

    .line 203
    const-string/jumbo v2, "notif_id"

    const/4 v3, 0x1

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 204
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/k$15;->b:Lcom/twitter/library/provider/k;

    .line 205
    invoke-static {v2}, Lcom/twitter/library/provider/k;->a(Lcom/twitter/library/provider/k;)Landroid/content/Context;

    move-result-object v2

    move-wide/from16 v0, v24

    invoke-static {v2, v0, v1}, Lbaw;->a(Landroid/content/Context;J)Z

    move-result v20

    .line 206
    move-object/from16 v0, v21

    iget v2, v0, Lcom/twitter/library/provider/n;->m:I

    move-object/from16 v0, v21

    iget v3, v0, Lcom/twitter/library/provider/n;->e:I

    move-object/from16 v0, v21

    iget v4, v0, Lcom/twitter/library/provider/n;->f:I

    move-object/from16 v0, v21

    iget v5, v0, Lcom/twitter/library/provider/n;->g:I

    move-object/from16 v0, v21

    iget v6, v0, Lcom/twitter/library/provider/n;->h:I

    move-object/from16 v0, v21

    iget v7, v0, Lcom/twitter/library/provider/n;->n:I

    move-object/from16 v0, v21

    iget v8, v0, Lcom/twitter/library/provider/n;->i:I

    move-object/from16 v0, v21

    iget v9, v0, Lcom/twitter/library/provider/n;->o:I

    move-object/from16 v0, v21

    iget v10, v0, Lcom/twitter/library/provider/n;->p:I

    move-object/from16 v0, v21

    iget v11, v0, Lcom/twitter/library/provider/n;->q:I

    move-object/from16 v0, v21

    iget v12, v0, Lcom/twitter/library/provider/n;->r:I

    move-object/from16 v0, v21

    iget v13, v0, Lcom/twitter/library/provider/n;->s:I

    move-object/from16 v0, v21

    iget v14, v0, Lcom/twitter/library/provider/n;->j:I

    move-object/from16 v0, v21

    iget v15, v0, Lcom/twitter/library/provider/n;->k:I

    move-object/from16 v0, v21

    iget v0, v0, Lcom/twitter/library/provider/n;->l:I

    move/from16 v16, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/twitter/library/provider/n;->t:I

    move/from16 v17, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/twitter/library/provider/n;->u:I

    move/from16 v18, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/twitter/library/provider/n;->v:I

    move/from16 v19, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/twitter/library/provider/n;->w:I

    move/from16 v21, v0

    invoke-static/range {v2 .. v21}, Lcom/twitter/library/provider/NotificationSetting;->a(IIIIIIIIIIIIIIIIIIZI)I

    move-result v2

    .line 215
    const-string/jumbo v3, "push_flags"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 218
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/k$15;->b:Lcom/twitter/library/provider/k;

    invoke-static {v2}, Lcom/twitter/library/provider/k;->b(Lcom/twitter/library/provider/k;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 219
    const-string/jumbo v2, "interval"

    const/4 v3, 0x5

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 221
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/k$15;->b:Lcom/twitter/library/provider/k;

    .line 222
    invoke-static {v2}, Lcom/twitter/library/provider/k;->c(Lcom/twitter/library/provider/k;)Z

    move-result v7

    const/4 v8, 0x0

    move-object/from16 v3, p2

    move-wide/from16 v4, v24

    move-object/from16 v6, v23

    .line 221
    invoke-static/range {v3 .. v8}, Lcom/twitter/library/provider/j;->a(Landroid/database/sqlite/SQLiteDatabase;JLandroid/content/ContentValues;ZLaut;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_2

    .line 225
    :catchall_0
    move-exception v2

    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_4
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 228
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/k$15;->b:Lcom/twitter/library/provider/k;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/twitter/library/provider/k;->b(Lcom/twitter/library/provider/k;Z)Z

    .line 229
    return-void
.end method
