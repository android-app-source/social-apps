.class public Lcom/twitter/library/provider/a;
.super Lcom/twitter/library/provider/l;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/provider/l",
        "<",
        "Lcom/twitter/model/search/b;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/twitter/library/provider/l;-><init>(Landroid/database/Cursor;)V

    .line 20
    return-void
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x3

    .line 24
    iget-object v6, p0, Lcom/twitter/library/provider/a;->e:Landroid/database/Cursor;

    .line 25
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 26
    const-wide/16 v4, 0x0

    .line 28
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/twitter/library/provider/a;->c:Ljava/util/List;

    .line 30
    :goto_0
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 34
    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 36
    if-eqz v0, :cond_0

    .line 37
    iget-object v1, p0, Lcom/twitter/library/provider/a;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    :cond_0
    new-instance v0, Lcom/twitter/model/search/b$a;

    invoke-direct {v0}, Lcom/twitter/model/search/b$a;-><init>()V

    const/4 v1, 0x1

    .line 41
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/search/b$a;->a(Ljava/lang/String;)Lcom/twitter/model/search/b$a;

    move-result-object v0

    const/4 v1, 0x2

    .line 42
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/search/b$a;->b(Ljava/lang/String;)Lcom/twitter/model/search/b$a;

    move-result-object v0

    .line 43
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/search/b$a;->a(J)Lcom/twitter/model/search/b$a;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lcom/twitter/model/search/b$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/search/b;

    move-object v1, v0

    .line 48
    :goto_1
    new-instance v0, Lcom/twitter/model/core/TwitterUser$a;

    invoke-direct {v0}, Lcom/twitter/model/core/TwitterUser$a;-><init>()V

    const/4 v4, 0x4

    .line 49
    invoke-interface {v6, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/model/core/TwitterUser$a;->a(J)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    const/4 v4, 0x6

    .line 50
    invoke-interface {v6, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->g(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    const/4 v4, 0x7

    .line 51
    invoke-interface {v6, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->c(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 53
    if-eqz v1, :cond_1

    .line 54
    iget-object v4, v1, Lcom/twitter/model/search/b;->j:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_4

    .line 60
    :goto_2
    return-void

    .line 58
    :cond_2
    iput-object v0, p0, Lcom/twitter/library/provider/a;->c:Ljava/util/List;

    goto :goto_2

    :cond_3
    move-object v1, v0

    move-wide v2, v4

    goto :goto_1

    :cond_4
    move-object v0, v1

    move-wide v4, v2

    goto :goto_0
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 82
    new-instance v1, Landroid/os/Bundle;

    invoke-super {p0}, Lcom/twitter/library/provider/l;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 83
    invoke-virtual {p0}, Lcom/twitter/library/provider/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/search/b;

    .line 84
    const-string/jumbo v2, "name"

    iget-object v3, v0, Lcom/twitter/model/search/b;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string/jumbo v2, "query"

    iget-object v3, v0, Lcom/twitter/model/search/b;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string/jumbo v2, "users"

    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, v0, Lcom/twitter/model/search/b;->j:Ljava/util/List;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 87
    return-object v1
.end method

.method public getLong(I)J
    .locals 2

    .prologue
    .line 64
    if-nez p1, :cond_0

    .line 65
    iget-object v0, p0, Lcom/twitter/library/provider/a;->c:Ljava/util/List;

    iget v1, p0, Lcom/twitter/library/provider/a;->d:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/search/b;

    iget-wide v0, v0, Lcom/twitter/model/search/b;->h:J

    .line 67
    :goto_0
    return-wide v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/library/provider/l;->getLong(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 72
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/twitter/library/provider/a;->c:Ljava/util/List;

    iget v1, p0, Lcom/twitter/library/provider/a;->d:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/search/b;

    iget-object v0, v0, Lcom/twitter/model/search/b;->b:Ljava/lang/String;

    .line 77
    :goto_0
    return-object v0

    .line 74
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 75
    iget-object v0, p0, Lcom/twitter/library/provider/a;->c:Ljava/util/List;

    iget v1, p0, Lcom/twitter/library/provider/a;->d:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/search/b;

    iget-object v0, v0, Lcom/twitter/model/search/b;->c:Ljava/lang/String;

    goto :goto_0

    .line 77
    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/library/provider/l;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
