.class public final Lcom/twitter/library/provider/o;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/provider/o$a;
    }
.end annotation


# static fields
.field private static a:Lcom/twitter/library/provider/o;


# instance fields
.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/library/provider/o$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/o;->b:Ljava/util/Set;

    .line 64
    return-void
.end method

.method private static a()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/library/provider/o$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    invoke-static {}, Lcom/twitter/library/provider/o;->b()Lcom/twitter/library/provider/o;

    move-result-object v0

    iget-object v1, v0, Lcom/twitter/library/provider/o;->b:Ljava/util/Set;

    .line 122
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 123
    monitor-enter v1

    .line 124
    :try_start_0
    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 125
    monitor-exit v1

    .line 126
    return-object v0

    .line 125
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(J)V
    .locals 2

    .prologue
    .line 70
    invoke-static {}, Lcom/twitter/library/provider/o;->a()Ljava/util/Set;

    move-result-object v0

    .line 71
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/o$a;

    .line 72
    invoke-interface {v0, p0, p1}, Lcom/twitter/library/provider/o$a;->e(J)V

    goto :goto_0

    .line 74
    :cond_0
    return-void
.end method

.method public static a(Lcom/twitter/library/provider/o$a;)V
    .locals 1

    .prologue
    .line 110
    invoke-static {}, Lcom/twitter/library/provider/o;->b()Lcom/twitter/library/provider/o;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/provider/o;->b:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 111
    return-void
.end method

.method public static a(Lcom/twitter/model/core/Tweet;)V
    .locals 2

    .prologue
    .line 80
    invoke-static {}, Lcom/twitter/library/provider/o;->a()Ljava/util/Set;

    move-result-object v0

    .line 81
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/o$a;

    .line 82
    invoke-interface {v0, p0}, Lcom/twitter/library/provider/o$a;->b(Lcom/twitter/model/core/Tweet;)V

    goto :goto_0

    .line 84
    :cond_0
    return-void
.end method

.method public static a(JLcom/twitter/model/core/Tweet;Ljava/lang/Runnable;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 95
    .line 96
    invoke-static {}, Lcom/twitter/library/provider/o;->a()Ljava/util/Set;

    move-result-object v0

    .line 97
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/o$a;

    .line 99
    invoke-interface {v0, p0, p1, p2, p3}, Lcom/twitter/library/provider/o$a;->a(JLcom/twitter/model/core/Tweet;Ljava/lang/Runnable;)Z

    move-result v0

    .line 101
    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 102
    goto :goto_0

    :cond_1
    move v0, v2

    .line 101
    goto :goto_1

    .line 103
    :cond_2
    return v1
.end method

.method private static declared-synchronized b()Lcom/twitter/library/provider/o;
    .locals 2

    .prologue
    .line 130
    const-class v1, Lcom/twitter/library/provider/o;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/provider/o;->a:Lcom/twitter/library/provider/o;

    if-nez v0, :cond_0

    .line 131
    new-instance v0, Lcom/twitter/library/provider/o;

    invoke-direct {v0}, Lcom/twitter/library/provider/o;-><init>()V

    sput-object v0, Lcom/twitter/library/provider/o;->a:Lcom/twitter/library/provider/o;

    .line 132
    const-class v0, Lcom/twitter/library/provider/o;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 134
    :cond_0
    sget-object v0, Lcom/twitter/library/provider/o;->a:Lcom/twitter/library/provider/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(Lcom/twitter/library/provider/o$a;)V
    .locals 1

    .prologue
    .line 117
    invoke-static {}, Lcom/twitter/library/provider/o;->b()Lcom/twitter/library/provider/o;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/provider/o;->b:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 118
    return-void
.end method
