.class public Lcom/twitter/library/provider/n;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/provider/n$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/library/provider/n;


# instance fields
.field public final b:Z

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:I

.field public final k:I

.field public final l:I

.field public final m:I

.field public final n:I

.field public final o:I

.field public final p:I

.field public final q:I

.field public final r:I

.field public final s:I

.field public final t:I

.field public final u:I

.field public final v:I

.field public final w:I

.field public final x:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 10
    new-instance v0, Lcom/twitter/library/provider/n$a;

    invoke-direct {v0}, Lcom/twitter/library/provider/n$a;-><init>()V

    sget v1, Lcga;->a:I

    .line 11
    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/n$a;->a(I)Lcom/twitter/library/provider/n$a;

    move-result-object v0

    sget-boolean v1, Lcom/twitter/library/provider/GlobalDatabaseProvider$a;->b:Z

    .line 12
    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/n$a;->a(Z)Lcom/twitter/library/provider/n$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/provider/GlobalDatabaseProvider$a;->a:Ljava/lang/String;

    .line 13
    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/n$a;->a(Ljava/lang/String;)Lcom/twitter/library/provider/n$a;

    move-result-object v0

    sget-boolean v1, Lcom/twitter/library/provider/GlobalDatabaseProvider$a;->c:Z

    .line 14
    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/n$a;->b(Z)Lcom/twitter/library/provider/n$a;

    move-result-object v0

    .line 15
    invoke-virtual {v0}, Lcom/twitter/library/provider/n$a;->a()Lcom/twitter/library/provider/n;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/provider/n;->a:Lcom/twitter/library/provider/n;

    .line 10
    return-void
.end method

.method private constructor <init>(Lcom/twitter/library/provider/n$a;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iget-boolean v0, p1, Lcom/twitter/library/provider/n$a;->a:Z

    iput-boolean v0, p0, Lcom/twitter/library/provider/n;->b:Z

    .line 44
    iget-object v0, p1, Lcom/twitter/library/provider/n$a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/provider/n;->c:Ljava/lang/String;

    .line 45
    iget-boolean v0, p1, Lcom/twitter/library/provider/n$a;->c:Z

    iput-boolean v0, p0, Lcom/twitter/library/provider/n;->d:Z

    .line 46
    iget v0, p1, Lcom/twitter/library/provider/n$a;->d:I

    iput v0, p0, Lcom/twitter/library/provider/n;->e:I

    .line 47
    iget v0, p1, Lcom/twitter/library/provider/n$a;->e:I

    iput v0, p0, Lcom/twitter/library/provider/n;->f:I

    .line 48
    iget v0, p1, Lcom/twitter/library/provider/n$a;->f:I

    iput v0, p0, Lcom/twitter/library/provider/n;->g:I

    .line 49
    iget v0, p1, Lcom/twitter/library/provider/n$a;->g:I

    iput v0, p0, Lcom/twitter/library/provider/n;->h:I

    .line 50
    iget v0, p1, Lcom/twitter/library/provider/n$a;->h:I

    iput v0, p0, Lcom/twitter/library/provider/n;->i:I

    .line 51
    iget v0, p1, Lcom/twitter/library/provider/n$a;->i:I

    iput v0, p0, Lcom/twitter/library/provider/n;->j:I

    .line 52
    iget v0, p1, Lcom/twitter/library/provider/n$a;->j:I

    iput v0, p0, Lcom/twitter/library/provider/n;->k:I

    .line 53
    iget v0, p1, Lcom/twitter/library/provider/n$a;->k:I

    iput v0, p0, Lcom/twitter/library/provider/n;->l:I

    .line 54
    iget v0, p1, Lcom/twitter/library/provider/n$a;->l:I

    iput v0, p0, Lcom/twitter/library/provider/n;->m:I

    .line 55
    iget v0, p1, Lcom/twitter/library/provider/n$a;->m:I

    iput v0, p0, Lcom/twitter/library/provider/n;->n:I

    .line 56
    iget v0, p1, Lcom/twitter/library/provider/n$a;->n:I

    iput v0, p0, Lcom/twitter/library/provider/n;->o:I

    .line 57
    iget v0, p1, Lcom/twitter/library/provider/n$a;->o:I

    iput v0, p0, Lcom/twitter/library/provider/n;->p:I

    .line 58
    iget v0, p1, Lcom/twitter/library/provider/n$a;->p:I

    iput v0, p0, Lcom/twitter/library/provider/n;->q:I

    .line 59
    iget v0, p1, Lcom/twitter/library/provider/n$a;->q:I

    iput v0, p0, Lcom/twitter/library/provider/n;->r:I

    .line 60
    iget v0, p1, Lcom/twitter/library/provider/n$a;->r:I

    iput v0, p0, Lcom/twitter/library/provider/n;->s:I

    .line 61
    iget v0, p1, Lcom/twitter/library/provider/n$a;->s:I

    iput v0, p0, Lcom/twitter/library/provider/n;->t:I

    .line 62
    iget v0, p1, Lcom/twitter/library/provider/n$a;->t:I

    iput v0, p0, Lcom/twitter/library/provider/n;->u:I

    .line 63
    iget v0, p1, Lcom/twitter/library/provider/n$a;->u:I

    iput v0, p0, Lcom/twitter/library/provider/n;->v:I

    .line 64
    iget v0, p1, Lcom/twitter/library/provider/n$a;->v:I

    iput v0, p0, Lcom/twitter/library/provider/n;->w:I

    .line 65
    iget-boolean v0, p1, Lcom/twitter/library/provider/n$a;->w:Z

    iput-boolean v0, p0, Lcom/twitter/library/provider/n;->x:Z

    .line 66
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/library/provider/n$a;Lcom/twitter/library/provider/n$1;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/twitter/library/provider/n;-><init>(Lcom/twitter/library/provider/n$a;)V

    return-void
.end method
