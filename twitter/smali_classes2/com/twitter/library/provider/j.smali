.class public Lcom/twitter/library/provider/j;
.super Laur;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/provider/j$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;


# instance fields
.field private final c:Landroid/content/Context;

.field private d:Lcom/twitter/database/schema/GlobalSchema;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-string/jumbo v0, "account_id"

    .line 45
    invoke-static {v0}, Laux;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/provider/j;->a:Ljava/lang/String;

    .line 46
    const-string/jumbo v0, "account_id"

    .line 47
    invoke-static {v0}, Laux;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/provider/j;->b:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 75
    const-string/jumbo v0, "global.db"

    const/16 v1, 0x24

    invoke-direct {p0, p1, v0, v1}, Laur;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 76
    iput-object p1, p0, Lcom/twitter/library/provider/j;->c:Landroid/content/Context;

    .line 77
    return-void
.end method

.method static a(Landroid/database/sqlite/SQLiteDatabase;JLandroid/content/ContentValues;ZLaut;)I
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 228
    .line 229
    const-string/jumbo v0, "account_settings"

    sget-object v1, Lcom/twitter/library/provider/j;->b:Ljava/lang/String;

    new-array v2, v7, [Ljava/lang/String;

    .line 230
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    .line 229
    invoke-virtual {p0, v0, p3, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    add-int v1, v6, v0

    .line 231
    if-nez v1, :cond_7

    .line 232
    const-string/jumbo v0, "account_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 233
    const-string/jumbo v0, "push_flags"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 234
    const-string/jumbo v0, "push_flags"

    sget v2, Lcga;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 236
    :cond_0
    const-string/jumbo v0, "interval"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 238
    if-eqz p4, :cond_6

    const/16 v0, 0x5a0

    .line 240
    :goto_0
    const-string/jumbo v2, "interval"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p3, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 242
    :cond_1
    const-string/jumbo v0, "vibrate"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 243
    const-string/jumbo v0, "vibrate"

    sget-boolean v2, Lcom/twitter/library/provider/GlobalDatabaseProvider$a;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 245
    :cond_2
    const-string/jumbo v0, "light"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 246
    const-string/jumbo v0, "light"

    sget-boolean v2, Lcom/twitter/library/provider/GlobalDatabaseProvider$a;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 248
    :cond_3
    const-string/jumbo v0, "ringtone"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 249
    const-string/jumbo v0, "ringtone"

    sget-object v2, Lcom/twitter/library/provider/GlobalDatabaseProvider$a;->a:Ljava/lang/String;

    invoke-virtual {p3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    :cond_4
    const-string/jumbo v0, "account_settings"

    const-string/jumbo v2, "account_id"

    invoke-virtual {p0, v0, v2, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_7

    .line 252
    add-int/lit8 v0, v1, 0x1

    .line 255
    :goto_1
    if-lez v0, :cond_5

    if-eqz p5, :cond_5

    .line 256
    new-array v1, v7, [Landroid/net/Uri;

    sget-object v2, Lcom/twitter/library/provider/GlobalDatabaseProvider;->c:Landroid/net/Uri;

    aput-object v2, v1, v6

    invoke-virtual {p5, v1}, Laut;->a([Landroid/net/Uri;)V

    .line 258
    :cond_5
    return v0

    .line 238
    :cond_6
    const/16 v0, 0xf

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method public static declared-synchronized c()Lcom/twitter/library/provider/j;
    .locals 2

    .prologue
    .line 81
    const-class v1, Lcom/twitter/library/provider/j;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lbms;->ah()Lbms;

    move-result-object v0

    invoke-virtual {v0}, Lbms;->J()Lcom/twitter/library/provider/j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(J)I
    .locals 7

    .prologue
    .line 153
    sget v1, Lcga;->a:I

    .line 154
    invoke-virtual {p0}, Lcom/twitter/library/provider/j;->d()Lcom/twitter/database/schema/GlobalSchema;

    move-result-object v0

    const-class v2, Laxb;

    invoke-interface {v0, v2}, Lcom/twitter/database/schema/GlobalSchema;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Laxb;

    invoke-interface {v0}, Laxb;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    .line 155
    sget-object v2, Lcom/twitter/library/provider/j;->b:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-interface {v0, v2, v3}, Lcom/twitter/database/model/l;->a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/twitter/database/model/g;

    move-result-object v2

    .line 157
    :try_start_0
    invoke-virtual {v2}, Lcom/twitter/database/model/g;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, v2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Laxb$a;

    invoke-interface {v0}, Laxb$a;->f()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 161
    :goto_0
    invoke-virtual {v2}, Lcom/twitter/database/model/g;->close()V

    .line 163
    return v0

    .line 161
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/twitter/database/model/g;->close()V

    throw v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public a(JILaut;)I
    .locals 7

    .prologue
    .line 405
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    .line 406
    const/4 v0, 0x0

    .line 408
    :goto_0
    return v0

    :cond_0
    const-string/jumbo v4, "mention"

    const-string/jumbo v0, "mention"

    .line 409
    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/library/provider/j;->a(JLjava/lang/String;)I

    move-result v0

    or-int v5, v0, p3

    move-object v1, p0

    move-wide v2, p1

    move-object v6, p4

    .line 408
    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/j;->a(JLjava/lang/String;ILaut;)I

    move-result v0

    goto :goto_0
.end method

.method public a(JIZLaut;)I
    .locals 7

    .prologue
    .line 129
    new-instance v4, Landroid/content/ContentValues;

    const/4 v0, 0x1

    invoke-direct {v4, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 130
    const-string/jumbo v0, "push_flags"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object v1, p0

    move-wide v2, p1

    move v5, p4

    move-object v6, p5

    .line 131
    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/j;->a(JLandroid/content/ContentValues;ZLaut;)I

    move-result v0

    return v0
.end method

.method public a(JLandroid/content/ContentValues;ZLaut;)I
    .locals 7

    .prologue
    .line 200
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    .line 201
    const/4 v0, 0x0

    .line 213
    :goto_0
    return v0

    .line 205
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 206
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    .line 208
    :try_start_0
    invoke-static/range {v1 .. v6}, Lcom/twitter/library/provider/j;->a(Landroid/database/sqlite/SQLiteDatabase;JLandroid/content/ContentValues;ZLaut;)I

    move-result v0

    .line 209
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public a(JLcom/twitter/library/provider/n;ZLaut;)I
    .locals 23

    .prologue
    .line 136
    move-object/from16 v0, p3

    iget v2, v0, Lcom/twitter/library/provider/n;->m:I

    move-object/from16 v0, p3

    iget v3, v0, Lcom/twitter/library/provider/n;->e:I

    move-object/from16 v0, p3

    iget v4, v0, Lcom/twitter/library/provider/n;->f:I

    move-object/from16 v0, p3

    iget v5, v0, Lcom/twitter/library/provider/n;->g:I

    move-object/from16 v0, p3

    iget v6, v0, Lcom/twitter/library/provider/n;->h:I

    move-object/from16 v0, p3

    iget v7, v0, Lcom/twitter/library/provider/n;->n:I

    move-object/from16 v0, p3

    iget v8, v0, Lcom/twitter/library/provider/n;->i:I

    move-object/from16 v0, p3

    iget v9, v0, Lcom/twitter/library/provider/n;->o:I

    move-object/from16 v0, p3

    iget v10, v0, Lcom/twitter/library/provider/n;->p:I

    move-object/from16 v0, p3

    iget v11, v0, Lcom/twitter/library/provider/n;->q:I

    move-object/from16 v0, p3

    iget v12, v0, Lcom/twitter/library/provider/n;->r:I

    move-object/from16 v0, p3

    iget v13, v0, Lcom/twitter/library/provider/n;->s:I

    move-object/from16 v0, p3

    iget v14, v0, Lcom/twitter/library/provider/n;->j:I

    move-object/from16 v0, p3

    iget v15, v0, Lcom/twitter/library/provider/n;->k:I

    move-object/from16 v0, p3

    iget v0, v0, Lcom/twitter/library/provider/n;->l:I

    move/from16 v16, v0

    move-object/from16 v0, p3

    iget v0, v0, Lcom/twitter/library/provider/n;->t:I

    move/from16 v17, v0

    move-object/from16 v0, p3

    iget v0, v0, Lcom/twitter/library/provider/n;->u:I

    move/from16 v18, v0

    move-object/from16 v0, p3

    iget v0, v0, Lcom/twitter/library/provider/n;->v:I

    move/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p3

    iget v0, v0, Lcom/twitter/library/provider/n;->w:I

    move/from16 v21, v0

    invoke-static/range {v2 .. v21}, Lcom/twitter/library/provider/NotificationSetting;->a(IIIIIIIIIIIIIIIIIIZI)I

    move-result v2

    .line 143
    new-instance v6, Landroid/content/ContentValues;

    const/4 v3, 0x4

    invoke-direct {v6, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 144
    const-string/jumbo v3, "push_flags"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 145
    const-string/jumbo v2, "vibrate"

    move-object/from16 v0, p3

    iget-boolean v3, v0, Lcom/twitter/library/provider/n;->b:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 146
    const-string/jumbo v2, "ringtone"

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/twitter/library/provider/n;->c:Ljava/lang/String;

    invoke-virtual {v6, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string/jumbo v2, "light"

    move-object/from16 v0, p3

    iget-boolean v3, v0, Lcom/twitter/library/provider/n;->d:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    move/from16 v7, p4

    move-object/from16 v8, p5

    .line 148
    invoke-virtual/range {v3 .. v8}, Lcom/twitter/library/provider/j;->a(JLandroid/content/ContentValues;ZLaut;)I

    move-result v2

    return v2
.end method

.method public a(JLjava/lang/String;)I
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v10, 0x0

    const/4 v5, 0x0

    .line 375
    .line 377
    const-string/jumbo v0, "tweet"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v9, v4

    .line 388
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/library/provider/j;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string/jumbo v1, "activity_states"

    sget-object v2, Lcom/twitter/library/provider/j$a;->a:[Ljava/lang/String;

    sget-object v3, Lcom/twitter/library/provider/j;->a:Ljava/lang/String;

    new-array v4, v4, [Ljava/lang/String;

    .line 389
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v10

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    .line 388
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 391
    if-eqz v1, :cond_5

    .line 393
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 394
    invoke-interface {v1, v9}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 397
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 400
    :goto_2
    return v0

    .line 379
    :cond_0
    const-string/jumbo v0, "mention"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 380
    const/4 v0, 0x2

    move v9, v0

    goto :goto_0

    .line 381
    :cond_1
    const-string/jumbo v0, "unread_interactions"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 382
    const/4 v0, 0x3

    move v9, v0

    goto :goto_0

    .line 383
    :cond_2
    const-string/jumbo v0, "message"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 384
    const/4 v0, 0x4

    move v9, v0

    goto :goto_0

    .line 386
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid activity type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 397
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_4
    move v0, v10

    goto :goto_1

    :cond_5
    move v0, v10

    goto :goto_2
.end method

.method public a(JLjava/lang/String;ILaut;)I
    .locals 9

    .prologue
    .line 337
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_1

    .line 338
    const/4 v0, 0x0

    .line 363
    :cond_0
    :goto_0
    return v0

    .line 340
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 341
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 343
    const/4 v0, 0x0

    .line 344
    invoke-virtual {p0}, Lcom/twitter/library/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 345
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 346
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 348
    :try_start_0
    const-string/jumbo v4, "activity_states"

    sget-object v5, Lcom/twitter/library/provider/j;->a:Ljava/lang/String;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-virtual {v2, v4, v1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    .line 350
    if-nez v0, :cond_2

    .line 351
    const-string/jumbo v4, "account_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 352
    const-string/jumbo v4, "activity_states"

    const-string/jumbo v5, "account_id"

    invoke-virtual {v2, v4, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_2

    .line 353
    add-int/lit8 v0, v0, 0x1

    .line 356
    :cond_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 358
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 360
    if-lez v0, :cond_0

    if-eqz p5, :cond_0

    .line 361
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/net/Uri;

    const/4 v2, 0x0

    sget-object v4, Lcom/twitter/library/provider/GlobalDatabaseProvider;->b:Landroid/net/Uri;

    invoke-static {v4, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p5, v1}, Laut;->a([Landroid/net/Uri;)V

    goto :goto_0

    .line 358
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public a(JZ)Lcom/twitter/library/provider/n;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 168
    invoke-virtual {p0}, Lcom/twitter/library/provider/j;->d()Lcom/twitter/database/schema/GlobalSchema;

    move-result-object v0

    const-class v3, Laxb;

    invoke-interface {v0, v3}, Lcom/twitter/database/schema/GlobalSchema;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Laxb;

    invoke-interface {v0}, Laxb;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    .line 169
    sget-object v3, Lcom/twitter/library/provider/j;->b:Ljava/lang/String;

    new-array v4, v1, [Ljava/lang/Object;

    .line 170
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    .line 169
    invoke-interface {v0, v3, v4}, Lcom/twitter/database/model/l;->a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/twitter/database/model/g;

    move-result-object v3

    .line 172
    :try_start_0
    invoke-virtual {v3}, Lcom/twitter/database/model/g;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 173
    new-instance v4, Lcom/twitter/library/provider/n$a;

    invoke-direct {v4}, Lcom/twitter/library/provider/n$a;-><init>()V

    .line 174
    iget-object v0, v3, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Laxb$a;

    invoke-interface {v0}, Laxb$a;->f()I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/twitter/library/provider/n$a;->a(I)Lcom/twitter/library/provider/n$a;

    .line 175
    if-eqz p3, :cond_0

    iget-object v0, v3, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Laxb$a;

    invoke-interface {v0}, Laxb$a;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Lcom/twitter/library/provider/n$a;->a(Z)Lcom/twitter/library/provider/n$a;

    .line 176
    iget-object v0, v3, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Laxb$a;

    invoke-interface {v0}, Laxb$a;->h()Ljava/lang/String;

    move-result-object v0

    .line 177
    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v4, v0}, Lcom/twitter/library/provider/n$a;->a(Ljava/lang/String;)Lcom/twitter/library/provider/n$a;

    .line 178
    iget-object v0, v3, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Laxb$a;

    invoke-interface {v0}, Laxb$a;->j()Z

    move-result v0

    invoke-virtual {v4, v0}, Lcom/twitter/library/provider/n$a;->b(Z)Lcom/twitter/library/provider/n$a;

    .line 179
    invoke-virtual {v4}, Lcom/twitter/library/provider/n$a;->a()Lcom/twitter/library/provider/n;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 182
    invoke-virtual {v3}, Lcom/twitter/database/model/g;->close()V

    .line 184
    :goto_2
    return-object v0

    :cond_0
    move v0, v2

    .line 175
    goto :goto_0

    .line 177
    :cond_1
    :try_start_1
    sget-object v0, Lcom/twitter/library/provider/GlobalDatabaseProvider$a;->a:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 182
    :cond_2
    invoke-virtual {v3}, Lcom/twitter/database/model/g;->close()V

    .line 184
    const/4 v0, 0x0

    goto :goto_2

    .line 182
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/twitter/database/model/g;->close()V

    throw v0
.end method

.method public b(J)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v7, -0x1

    .line 267
    const-wide/16 v2, 0x0

    cmp-long v0, p1, v2

    if-gtz v0, :cond_0

    .line 323
    :goto_0
    return v7

    .line 271
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/provider/j;->d()Lcom/twitter/database/schema/GlobalSchema;

    move-result-object v0

    const-class v2, Laxb;

    invoke-interface {v0, v2}, Lcom/twitter/database/schema/GlobalSchema;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Laxb;

    invoke-interface {v0}, Laxb;->f()Lcom/twitter/database/model/l;

    move-result-object v2

    .line 272
    sget-object v0, Lcom/twitter/library/provider/j;->b:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 273
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-interface {v2, v0, v3}, Lcom/twitter/database/model/l;->a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/twitter/database/model/g;

    move-result-object v3

    .line 275
    :try_start_0
    invoke-virtual {v3}, Lcom/twitter/database/model/g;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 276
    iget-object v0, v3, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Laxb$a;

    invoke-interface {v0}, Laxb$a;->e()Ljava/lang/Integer;

    move-result-object v0

    .line 277
    if-eqz v0, :cond_1

    .line 278
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 282
    invoke-virtual {v3}, Lcom/twitter/database/model/g;->close()V

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Lcom/twitter/database/model/g;->close()V

    .line 287
    new-instance v0, Lcom/twitter/database/model/f$a;

    invoke-direct {v0}, Lcom/twitter/database/model/f$a;-><init>()V

    const-string/jumbo v3, "notif_id"

    .line 289
    invoke-static {v3}, Laux;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v0

    const-string/jumbo v3, "notif_id ASC"

    .line 290
    invoke-virtual {v0, v3}, Lcom/twitter/database/model/f$a;->b(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v0

    .line 291
    invoke-virtual {v0}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v0

    .line 288
    invoke-interface {v2, v0}, Lcom/twitter/database/model/l;->a(Lcom/twitter/database/model/f;)Lcom/twitter/database/model/g;

    move-result-object v2

    .line 294
    :try_start_1
    invoke-virtual {v2}, Lcom/twitter/database/model/g;->a()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 310
    :goto_1
    invoke-virtual {v2}, Lcom/twitter/database/model/g;->close()V

    .line 312
    if-eq v0, v7, :cond_2

    .line 314
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 315
    const-string/jumbo v1, "notif_id"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v4, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 316
    iget-object v1, p0, Lcom/twitter/library/provider/j;->c:Landroid/content/Context;

    .line 317
    invoke-static {v1}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;)Z

    move-result v5

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    .line 316
    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/j;->a(JLandroid/content/ContentValues;ZLaut;)I

    move-result v1

    if-nez v1, :cond_2

    .line 319
    const-string/jumbo v0, "GlobalDatabaseHelper"

    const-string/jumbo v1, "Failed to save notification id"

    invoke-static {v0, v1}, Lcqj;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v7

    :cond_2
    move v7, v0

    .line 323
    goto/16 :goto_0

    .line 282
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/twitter/database/model/g;->close()V

    throw v0

    .line 304
    :cond_3
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 298
    :cond_4
    invoke-virtual {v2}, Lcom/twitter/database/model/g;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 299
    iget-object v0, v2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Laxb$a;

    invoke-interface {v0}, Laxb$a;->e()Ljava/lang/Integer;

    move-result-object v0

    .line 300
    if-eqz v0, :cond_4

    .line 301
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v3

    sub-int/2addr v3, v1

    const/16 v4, 0x3e8

    if-le v3, v4, :cond_3

    .line 307
    :cond_5
    add-int/lit16 v0, v1, 0x3e8

    goto :goto_1

    .line 310
    :catchall_1
    move-exception v0

    invoke-virtual {v2}, Lcom/twitter/database/model/g;->close()V

    throw v0
.end method

.method public b(JZ)I
    .locals 5

    .prologue
    .line 454
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 456
    invoke-virtual {p0}, Lcom/twitter/library/provider/j;->d()Lcom/twitter/database/schema/GlobalSchema;

    move-result-object v0

    const-class v1, Laxb;

    invoke-interface {v0, v1}, Lcom/twitter/database/schema/GlobalSchema;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Laxb;

    invoke-interface {v0}, Laxb;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    .line 457
    sget-object v1, Lcom/twitter/library/provider/j;->b:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 458
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/twitter/database/model/l;->a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/twitter/database/model/g;

    move-result-object v1

    .line 460
    :try_start_0
    invoke-virtual {v1}, Lcom/twitter/database/model/g;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461
    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Laxb$a;

    invoke-interface {v0}, Laxb$a;->i()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 464
    invoke-virtual {v1}, Lcom/twitter/database/model/g;->close()V

    .line 470
    :goto_0
    return v0

    .line 464
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/database/model/g;->close()V

    .line 467
    :cond_1
    if-eqz p3, :cond_2

    .line 468
    const/16 v0, 0x5a0

    goto :goto_0

    .line 464
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/twitter/database/model/g;->close()V

    throw v0

    .line 470
    :cond_2
    const/16 v0, 0xf

    goto :goto_0
.end method

.method public synthetic bq_()Lcom/twitter/database/model/i;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/twitter/library/provider/j;->d()Lcom/twitter/database/schema/GlobalSchema;

    move-result-object v0

    return-object v0
.end method

.method public c(J)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 417
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-gtz v1, :cond_0

    .line 430
    :goto_0
    return v0

    .line 421
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 422
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 424
    :try_start_0
    const-string/jumbo v0, "account_settings"

    sget-object v2, Lcom/twitter/library/provider/j;->b:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 425
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 424
    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 426
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 428
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public d(J)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 437
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-gtz v1, :cond_0

    .line 450
    :goto_0
    return v0

    .line 441
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/provider/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 442
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 444
    :try_start_0
    const-string/jumbo v0, "activity_states"

    sget-object v2, Lcom/twitter/library/provider/j;->a:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 445
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 444
    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 446
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 448
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public d()Lcom/twitter/database/schema/GlobalSchema;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/library/provider/j;->d:Lcom/twitter/database/schema/GlobalSchema;

    if-nez v0, :cond_0

    .line 90
    new-instance v0, Lcom/twitter/library/provider/j$1;

    invoke-direct {v0, p0}, Lcom/twitter/library/provider/j$1;-><init>(Lcom/twitter/library/provider/j;)V

    invoke-static {v0}, Lcom/twitter/util/f;->a(Lcom/twitter/util/concurrent/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/database/schema/GlobalSchema;

    iput-object v0, p0, Lcom/twitter/library/provider/j;->d:Lcom/twitter/database/schema/GlobalSchema;

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/provider/j;->d:Lcom/twitter/database/schema/GlobalSchema;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/database/schema/GlobalSchema;

    return-object v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 104
    const-class v0, Lcom/twitter/database/schema/GlobalSchema;

    new-instance v1, Lava;

    invoke-direct {v1, p1}, Lava;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {v0, v1}, Lcom/twitter/database/model/i$a;->a(Ljava/lang/Class;Lcom/twitter/database/model/a;)Lcom/twitter/database/model/i;

    move-result-object v0

    .line 105
    invoke-interface {v0}, Lcom/twitter/database/model/i;->g()V

    .line 106
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 8

    .prologue
    .line 115
    new-instance v3, Landroid/util/LongSparseArray;

    invoke-direct {v3}, Landroid/util/LongSparseArray;-><init>()V

    .line 116
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 117
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v0

    invoke-virtual {v0}, Lakn;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    .line 118
    invoke-virtual {v0}, Lakm;->b()Lcnz;

    move-result-object v2

    invoke-virtual {v2}, Lcnz;->b()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7, v0}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 119
    invoke-virtual {v0}, Lakm;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 121
    :cond_0
    const-class v0, Lcom/twitter/database/schema/GlobalSchema;

    new-instance v1, Lava;

    invoke-direct {v1, p1}, Lava;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {v0, v1}, Lcom/twitter/database/model/j$a;->a(Ljava/lang/Class;Lcom/twitter/database/model/a;)Lcom/twitter/database/model/j;

    move-result-object v1

    .line 123
    new-instance v0, Lcom/twitter/library/provider/k;

    iget-object v5, p0, Lcom/twitter/library/provider/j;->c:Landroid/content/Context;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/provider/k;-><init>(Lcom/twitter/database/model/j;Landroid/database/sqlite/SQLiteDatabase;Landroid/util/LongSparseArray;Ljava/util/Map;Landroid/content/Context;)V

    .line 124
    invoke-virtual {v0, p2, p3}, Lcom/twitter/library/provider/k;->a(II)V

    .line 125
    return-void
.end method
