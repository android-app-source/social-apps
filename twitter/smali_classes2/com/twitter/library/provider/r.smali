.class public Lcom/twitter/library/provider/r;
.super Lcom/twitter/library/provider/l;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/provider/l",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final i:Ljava/util/BitSet;

.field private final j:Ljava/util/BitSet;

.field private final k:Ljava/util/BitSet;

.field private final l:Ljava/util/BitSet;

.field private final m:Landroid/util/SparseIntArray;

.field private final n:Landroid/util/SparseIntArray;

.field private o:I

.field private p:I

.field private q:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/twitter/library/provider/l;-><init>(Landroid/database/Cursor;)V

    .line 48
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/provider/r;->i:Ljava/util/BitSet;

    .line 49
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/provider/r;->j:Ljava/util/BitSet;

    .line 50
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/provider/r;->k:Ljava/util/BitSet;

    .line 51
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/provider/r;->l:Ljava/util/BitSet;

    .line 52
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/provider/r;->m:Landroid/util/SparseIntArray;

    .line 53
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/provider/r;->n:Landroid/util/SparseIntArray;

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/provider/r;->o:I

    .line 67
    return-void
.end method

.method private static a(II)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 242
    const/16 v0, 0xc

    if-ne p0, v0, :cond_1

    move v0, v1

    .line 243
    :goto_0
    invoke-static {p1}, Lcom/twitter/model/timeline/z$a;->g(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 244
    invoke-static {p1}, Lcom/twitter/model/timeline/z$a;->h(I)Z

    move-result v3

    if-nez v3, :cond_0

    if-nez v0, :cond_0

    .line 245
    invoke-static {p1}, Lcom/twitter/model/timeline/z$a;->r(I)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v1

    .line 246
    :goto_1
    if-eqz p0, :cond_3

    if-eqz v0, :cond_3

    :goto_2
    return v1

    :cond_1
    move v0, v2

    .line 242
    goto :goto_0

    :cond_2
    move v0, v2

    .line 245
    goto :goto_1

    :cond_3
    move v1, v2

    .line 246
    goto :goto_2
.end method

.method private static a(ILjava/lang/String;ILjava/lang/String;I)Z
    .locals 1

    .prologue
    .line 256
    if-eqz p0, :cond_0

    invoke-static {p1, p2, p3, p4}, Lcom/twitter/library/provider/r;->a(Ljava/lang/String;ILjava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;ILjava/lang/String;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 217
    invoke-static {p0, p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 221
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p1}, Lcom/twitter/library/provider/r;->b(I)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p3}, Lcom/twitter/library/provider/r;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(I)Z
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x7

    if-eq p0, v0, :cond_0

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/16 v0, 0xf

    if-eq p0, v0, :cond_0

    const/16 v0, 0xc

    if-eq p0, v0, :cond_0

    const/16 v0, 0xb

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 22

    .prologue
    .line 71
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/provider/r;->i:Ljava/util/BitSet;

    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    .line 72
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/provider/r;->j:Ljava/util/BitSet;

    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    .line 73
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/provider/r;->k:Ljava/util/BitSet;

    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    .line 74
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/provider/r;->l:Ljava/util/BitSet;

    invoke-virtual {v1}, Ljava/util/BitSet;->clear()V

    .line 75
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/provider/r;->m:Landroid/util/SparseIntArray;

    invoke-virtual {v1}, Landroid/util/SparseIntArray;->clear()V

    .line 76
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/provider/r;->n:Landroid/util/SparseIntArray;

    invoke-virtual {v1}, Landroid/util/SparseIntArray;->clear()V

    .line 77
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/twitter/library/provider/r;->p:I

    .line 79
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/twitter/library/provider/r;->e:Landroid/database/Cursor;

    .line 80
    if-nez v14, :cond_0

    .line 81
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/library/provider/r;->c:Ljava/util/List;

    .line 211
    :goto_0
    return-void

    .line 85
    :cond_0
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 87
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 88
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/provider/r;->i:Ljava/util/BitSet;

    move-object/from16 v16, v0

    .line 89
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/provider/r;->j:Ljava/util/BitSet;

    move-object/from16 v17, v0

    .line 90
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/provider/r;->k:Ljava/util/BitSet;

    move-object/from16 v18, v0

    .line 91
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/provider/r;->l:Ljava/util/BitSet;

    move-object/from16 v19, v0

    .line 92
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/provider/r;->m:Landroid/util/SparseIntArray;

    move-object/from16 v20, v0

    .line 93
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/provider/r;->n:Landroid/util/SparseIntArray;

    move-object/from16 v21, v0

    .line 95
    const/4 v8, 0x0

    .line 96
    const/4 v7, 0x0

    .line 97
    const/4 v6, 0x0

    .line 99
    const/4 v5, 0x0

    .line 100
    const/4 v4, 0x0

    .line 101
    const/4 v3, 0x0

    .line 102
    const/4 v2, -0x1

    .line 103
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v1

    move-object v9, v7

    move v11, v8

    move v7, v6

    .line 106
    :goto_1
    sget v6, Lbue;->ar:I

    invoke-interface {v14, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 107
    sget v6, Lbue;->d:I

    invoke-interface {v14, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 108
    sget v6, Lbue;->e:I

    invoke-interface {v14, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 110
    sget v6, Lbue;->g:I

    invoke-interface {v14, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 112
    const/16 v13, 0xb

    if-ne v8, v13, :cond_1

    .line 113
    move-object/from16 v0, p0

    iget v13, v0, Lcom/twitter/library/provider/r;->p:I

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/twitter/library/provider/r;->p:I

    .line 115
    :cond_1
    const/high16 v13, 0x20000

    and-int/2addr v13, v6

    if-eqz v13, :cond_2

    .line 116
    move-object/from16 v0, p0

    iget v13, v0, Lcom/twitter/library/provider/r;->q:I

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/twitter/library/provider/r;->q:I

    .line 119
    :cond_2
    const/4 v13, 0x1

    .line 124
    invoke-static {v1, v10, v12, v9, v11}, Lcom/twitter/library/provider/r;->a(ILjava/lang/String;ILjava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 126
    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 131
    :cond_3
    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_d

    const/4 v9, 0x1

    .line 132
    :goto_2
    if-eqz v9, :cond_e

    .line 134
    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 138
    if-eqz v1, :cond_4

    .line 139
    add-int/lit8 v11, v1, -0x1

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/util/BitSet;->set(I)V

    .line 154
    :cond_4
    :goto_3
    const/16 v11, 0xd

    if-ne v12, v11, :cond_5

    .line 155
    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 160
    :cond_5
    if-nez v9, :cond_6

    if-eq v7, v8, :cond_8

    .line 161
    :cond_6
    invoke-static {v7, v5}, Lcom/twitter/library/provider/r;->a(II)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 165
    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 167
    :cond_7
    invoke-static {v8, v6}, Lcom/twitter/library/provider/r;->a(II)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 171
    move-object/from16 v0, v20

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    move v4, v1

    .line 178
    :cond_8
    invoke-static {v8, v6}, Lcom/twitter/library/provider/r;->a(II)Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-interface {v14}, Landroid/database/Cursor;->isFirst()Z

    move-result v2

    if-nez v2, :cond_11

    .line 179
    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    if-eqz v1, :cond_f

    const/4 v1, 0x1

    .line 182
    :goto_4
    if-eqz v1, :cond_9

    .line 183
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    :cond_9
    add-int/lit8 v3, v3, 0x1

    .line 191
    add-int/lit8 v2, v3, -0x1

    .line 192
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 193
    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/provider/r;->o:I

    if-lez v5, :cond_10

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v5

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/provider/r;->o:I

    if-lt v5, v7, :cond_10

    .line 202
    :cond_a
    if-lez v1, :cond_b

    .line 203
    add-int/lit8 v1, v1, -0x1

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 206
    :cond_b
    invoke-static {v8, v6}, Lcom/twitter/library/provider/r;->a(II)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 207
    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 210
    :cond_c
    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/twitter/library/provider/r;->c:Ljava/util/List;

    goto/16 :goto_0

    .line 131
    :cond_d
    const/4 v9, 0x0

    goto/16 :goto_2

    .line 141
    :cond_e
    const/4 v11, 0x2

    if-ne v12, v11, :cond_4

    .line 145
    if-eqz v1, :cond_4

    .line 146
    add-int/lit8 v11, v1, -0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/BitSet;->set(I)V

    goto/16 :goto_3

    .line 179
    :cond_f
    const/4 v1, 0x0

    goto :goto_4

    .line 196
    :cond_10
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_a

    move v5, v6

    move v7, v8

    move-object v9, v10

    move v11, v12

    goto/16 :goto_1

    :cond_11
    move v1, v13

    goto :goto_4
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 317
    if-gez p1, :cond_0

    .line 318
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "groupedLimit must be >= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 320
    :cond_0
    iput p1, p0, Lcom/twitter/library/provider/r;->o:I

    .line 321
    return-void
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 288
    iget-object v0, p0, Lcom/twitter/library/provider/r;->e:Landroid/database/Cursor;

    .line 289
    invoke-virtual {p0}, Lcom/twitter/library/provider/r;->getPosition()I

    move-result v1

    .line 290
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 291
    if-eqz v0, :cond_0

    if-ltz v1, :cond_0

    .line 292
    const-string/jumbo v0, "requires_top_divider"

    iget-object v3, p0, Lcom/twitter/library/provider/r;->i:Ljava/util/BitSet;

    invoke-virtual {v3, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 293
    const-string/jumbo v0, "entity_start"

    iget-object v3, p0, Lcom/twitter/library/provider/r;->k:Ljava/util/BitSet;

    invoke-virtual {v3, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 294
    const-string/jumbo v0, "entity_end"

    iget-object v3, p0, Lcom/twitter/library/provider/r;->l:Ljava/util/BitSet;

    invoke-virtual {v3, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 295
    const-string/jumbo v0, "should_hide_borders"

    iget-object v3, p0, Lcom/twitter/library/provider/r;->j:Ljava/util/BitSet;

    invoke-virtual {v3, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 296
    iget-object v0, p0, Lcom/twitter/library/provider/r;->m:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 297
    iget-object v3, p0, Lcom/twitter/library/provider/r;->n:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 298
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 299
    const-string/jumbo v3, "data_type_source_start"

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 300
    const-string/jumbo v0, "data_type_source_end"

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 303
    :cond_0
    const-string/jumbo v0, "ad_slots_count"

    iget v1, p0, Lcom/twitter/library/provider/r;->p:I

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 304
    const-string/jumbo v0, "realtime_ad_count"

    iget v1, p0, Lcom/twitter/library/provider/r;->q:I

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 305
    return-object v2
.end method

.method public isFirst()Z
    .locals 3

    .prologue
    .line 263
    iget-object v0, p0, Lcom/twitter/library/provider/r;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264
    :cond_0
    invoke-super {p0}, Lcom/twitter/library/provider/l;->isFirst()Z

    move-result v0

    .line 267
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/provider/r;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v1, p0, Lcom/twitter/library/provider/r;->c:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public isLast()Z
    .locals 3

    .prologue
    .line 273
    iget-object v0, p0, Lcom/twitter/library/provider/r;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/r;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 274
    :cond_0
    invoke-super {p0}, Lcom/twitter/library/provider/l;->isLast()Z

    move-result v0

    .line 277
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/provider/r;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v1, p0, Lcom/twitter/library/provider/r;->c:Ljava/util/List;

    iget-object v2, p0, Lcom/twitter/library/provider/r;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public moveToPosition(I)Z
    .locals 2

    .prologue
    .line 282
    iget-object v1, p0, Lcom/twitter/library/provider/r;->e:Landroid/database/Cursor;

    .line 283
    if-eqz v1, :cond_0

    invoke-super {p0, p1}, Lcom/twitter/library/provider/l;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/provider/r;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
