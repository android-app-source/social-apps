.class public Lcom/twitter/library/provider/g;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/provider/t;


# direct methods
.method private constructor <init>(Lcom/twitter/library/provider/t;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/twitter/library/provider/g;->a:Lcom/twitter/library/provider/t;

    .line 28
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Lcom/twitter/android/timeline/bk;)I
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 183
    invoke-virtual {p1}, Lcom/twitter/android/timeline/bk;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v2

    .line 186
    iget-object v0, v2, Lcom/twitter/android/timeline/bg;->j:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    const-string/jumbo v1, "owner_id=? AND type=? AND timeline_tag=? AND entity_group_id=? AND dismissed=1"

    .line 191
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    iget-wide v4, v2, Lcom/twitter/android/timeline/bg;->i:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v6

    iget v3, v2, Lcom/twitter/android/timeline/bg;->g:I

    .line 192
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v7

    iget-object v2, v2, Lcom/twitter/android/timeline/bg;->j:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v8

    .line 193
    invoke-virtual {p1}, Lcom/twitter/android/timeline/bk;->h()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v9

    .line 203
    :goto_0
    const-string/jumbo v2, "timeline"

    invoke-virtual {p0, v2, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0

    .line 195
    :cond_0
    const-string/jumbo v1, "owner_id=? AND type=? AND timeline_tag IS NULL AND entity_group_id=? AND dismissed=1"

    .line 200
    new-array v0, v9, [Ljava/lang/String;

    iget-wide v4, v2, Lcom/twitter/android/timeline/bg;->i:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v6

    iget v2, v2, Lcom/twitter/android/timeline/bg;->g:I

    .line 201
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    invoke-virtual {p1}, Lcom/twitter/android/timeline/bk;->h()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v8

    goto :goto_0
.end method

.method private static a(Lcom/twitter/android/timeline/bg;Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 131
    const-string/jumbo v0, "dismissed"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 134
    iget-object v0, p0, Lcom/twitter/android/timeline/bg;->j:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 135
    const-string/jumbo v1, "owner_id=? AND type=? AND timeline_tag=? AND entity_group_id=? AND entity_id!=entity_group_id"

    .line 137
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/android/timeline/bg;->i:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    iget v2, p0, Lcom/twitter/android/timeline/bg;->g:I

    .line 138
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    iget-object v2, p0, Lcom/twitter/android/timeline/bg;->j:Ljava/lang/String;

    aput-object v2, v0, v6

    aput-object p1, v0, v7

    .line 146
    :goto_0
    const-string/jumbo v2, "timeline"

    invoke-virtual {p3, v2, p4, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 149
    return v0

    .line 140
    :cond_0
    const-string/jumbo v1, "owner_id=? AND type=? AND timeline_tag IS NULL AND entity_group_id=? AND entity_id!=entity_group_id"

    .line 143
    new-array v0, v7, [Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/android/timeline/bg;->i:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    iget v2, p0, Lcom/twitter/android/timeline/bg;->g:I

    .line 144
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    aput-object p1, v0, v6

    goto :goto_0
.end method

.method private static a(Lcom/twitter/android/timeline/bg;Ljava/lang/String;Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 159
    const-string/jumbo v0, "dismissed"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 162
    iget-object v0, p0, Lcom/twitter/android/timeline/bg;->j:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    const-string/jumbo v1, "owner_id=? AND type=? AND timeline_tag=? AND entity_group_id=? AND entity_id=?"

    .line 165
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/android/timeline/bg;->i:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    iget v2, p0, Lcom/twitter/android/timeline/bg;->g:I

    .line 166
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    iget-object v2, p0, Lcom/twitter/android/timeline/bg;->j:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    aput-object p1, v0, v7

    aput-object p2, v0, v8

    .line 175
    :goto_0
    const-string/jumbo v2, "timeline"

    invoke-virtual {p4, v2, p5, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 178
    return v0

    .line 169
    :cond_0
    const-string/jumbo v1, "owner_id=? AND type=? AND timeline_tag IS NULL AND entity_group_id=? AND entity_id=?"

    .line 172
    new-array v0, v8, [Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/android/timeline/bg;->i:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    iget v2, p0, Lcom/twitter/android/timeline/bg;->g:I

    .line 173
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    aput-object p1, v0, v6

    aput-object p2, v0, v7

    goto :goto_0
.end method

.method public static a(Lcom/twitter/library/provider/t;)Lcom/twitter/library/provider/g;
    .locals 1

    .prologue
    .line 210
    new-instance v0, Lcom/twitter/library/provider/g;

    invoke-direct {v0, p0}, Lcom/twitter/library/provider/g;-><init>(Lcom/twitter/library/provider/t;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/timeline/bk;ZIILaut;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v8, 0x2

    const/4 v7, 0x0

    .line 62
    .line 63
    const/4 v6, 0x0

    .line 64
    iget-object v0, p0, Lcom/twitter/library/provider/g;->a:Lcom/twitter/library/provider/t;

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 65
    invoke-virtual {p1}, Lcom/twitter/android/timeline/bk;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v0

    .line 66
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 68
    :try_start_0
    new-instance v5, Landroid/content/ContentValues;

    const/4 v1, 0x2

    invoke-direct {v5, v1}, Landroid/content/ContentValues;-><init>(I)V

    .line 69
    const-string/jumbo v1, "dismiss_reason"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 70
    if-ne p4, v8, :cond_3

    .line 71
    if-eqz p2, :cond_6

    .line 73
    invoke-static {v4, p1}, Lcom/twitter/library/provider/g;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/twitter/android/timeline/bk;)I

    move-result v1

    add-int/2addr v1, v7

    move v9, v1

    .line 83
    :goto_0
    invoke-virtual {p1}, Lcom/twitter/android/timeline/bk;->h()Ljava/lang/String;

    move-result-object v1

    .line 84
    if-eqz p2, :cond_2

    :goto_1
    move-object v2, v1

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/provider/g;->a(Lcom/twitter/android/timeline/bg;Ljava/lang/String;Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I

    move-result v2

    add-int/2addr v2, v9

    .line 92
    if-eqz p2, :cond_0

    move v7, v8

    :cond_0
    invoke-static {v0, v1, v7, v4, v5}, Lcom/twitter/library/provider/g;->a(Lcom/twitter/android/timeline/bg;Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I

    move-result v0

    add-int/2addr v0, v2

    .line 94
    if-lez v0, :cond_5

    .line 110
    :goto_2
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 114
    if-lez v0, :cond_1

    if-eqz p5, :cond_1

    .line 116
    invoke-static {p5}, Lawc;->a(Laut;)V

    .line 118
    :cond_1
    return-object v1

    :cond_2
    move v3, v7

    .line 84
    goto :goto_1

    .line 102
    :cond_3
    :try_start_1
    invoke-virtual {p1}, Lcom/twitter/android/timeline/bk;->f()Ljava/lang/String;

    move-result-object v2

    .line 103
    invoke-virtual {p1}, Lcom/twitter/android/timeline/bk;->h()Ljava/lang/String;

    move-result-object v1

    if-eqz p2, :cond_4

    :goto_3
    invoke-static/range {v0 .. v5}, Lcom/twitter/library/provider/g;->a(Lcom/twitter/android/timeline/bg;Ljava/lang/String;Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 106
    if-lez v0, :cond_5

    move-object v1, v2

    .line 107
    goto :goto_2

    :cond_4
    move v3, v7

    .line 103
    goto :goto_3

    .line 112
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    :cond_5
    move-object v1, v6

    goto :goto_2

    :cond_6
    move v9, v7

    goto :goto_0
.end method
