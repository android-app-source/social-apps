.class Lcom/twitter/library/provider/b$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/provider/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/library/provider/b;",
        "Lcom/twitter/library/provider/b$a;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/provider/b$a;
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lcom/twitter/library/provider/b$a;

    invoke-direct {v0}, Lcom/twitter/library/provider/b$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/provider/b$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 71
    sget-object v0, Lcom/twitter/model/dms/q;->a:Lcom/twitter/util/serialization/b;

    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/b;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/q;

    invoke-virtual {p2, v0}, Lcom/twitter/library/provider/b$a;->a(Lcom/twitter/model/dms/q;)Lcom/twitter/library/provider/b$a;

    move-result-object v1

    const-class v0, Lcom/twitter/library/database/dm/ShareHistoryTable$Type;

    .line 72
    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 73
    invoke-virtual {v0, p1}, Lcom/twitter/util/serialization/l;->d(Lcom/twitter/util/serialization/n;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/database/dm/ShareHistoryTable$Type;

    .line 72
    invoke-virtual {v1, v0}, Lcom/twitter/library/provider/b$a;->a(Lcom/twitter/library/database/dm/ShareHistoryTable$Type;)Lcom/twitter/library/provider/c$a;

    .line 74
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 54
    check-cast p2, Lcom/twitter/library/provider/b$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/provider/b$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/provider/b$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/provider/b;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p2, Lcom/twitter/library/provider/b;->b:Lcom/twitter/model/dms/q;

    sget-object v1, Lcom/twitter/model/dms/q;->a:Lcom/twitter/util/serialization/b;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/provider/b;->d:Lcom/twitter/library/database/dm/ShareHistoryTable$Type;

    const-class v2, Lcom/twitter/library/database/dm/ShareHistoryTable$Type;

    .line 59
    invoke-static {v2}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 60
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    check-cast p2, Lcom/twitter/library/provider/b;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/provider/b$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/provider/b;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/twitter/library/provider/b$b;->a()Lcom/twitter/library/provider/b$a;

    move-result-object v0

    return-object v0
.end method
