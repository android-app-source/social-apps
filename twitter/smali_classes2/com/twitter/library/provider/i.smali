.class public Lcom/twitter/library/provider/i;
.super Laus;
.source "Twttr"


# instance fields
.field private final b:Laus$a;

.field private c:Z

.field private d:Z


# direct methods
.method protected constructor <init>(Lcom/twitter/database/model/j;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Laus;-><init>(Lcom/twitter/database/model/j;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 38
    new-instance v0, Lcom/twitter/library/provider/i$1;

    const/4 v1, -0x1

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/provider/i$1;-><init>(Lcom/twitter/library/provider/i;I)V

    iput-object v0, p0, Lcom/twitter/library/provider/i;->b:Laus$a;

    .line 50
    return-void
.end method

.method static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8

    .prologue
    .line 311
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 313
    :try_start_0
    const-string/jumbo v1, "drafts"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v3, "_id"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string/jumbo v3, "media"

    aput-object v3, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 315
    if-nez v1, :cond_0

    .line 337
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 339
    :goto_0
    return-void

    .line 319
    :cond_0
    :try_start_1
    new-instance v2, Landroid/content/ContentValues;

    const/4 v0, 0x1

    invoke-direct {v2, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 320
    :cond_1
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 321
    const/4 v0, 0x1

    .line 322
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v3, Lcom/twitter/model/media/EditableMedia;->j:Lcom/twitter/util/serialization/l;

    invoke-static {v3}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v3

    .line 321
    invoke-static {v0, v3}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 323
    if-eqz v0, :cond_1

    .line 324
    sget-object v3, Lcom/twitter/model/drafts/DraftAttachment;->b:Lcpp;

    invoke-static {v0, v3}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;Lcpp;)Ljava/util/List;

    move-result-object v0

    .line 326
    const-string/jumbo v3, "media"

    sget-object v4, Lcom/twitter/model/drafts/DraftAttachment;->a:Lcom/twitter/util/serialization/l;

    .line 327
    invoke-static {v4}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v4

    .line 326
    invoke-static {v0, v4}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 328
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 329
    const-string/jumbo v0, "drafts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "_id="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p0, v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 333
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 337
    :catchall_1
    move-exception v0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 333
    :cond_2
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 335
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 337
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/library/provider/i;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/twitter/library/provider/i;->d:Z

    return v0
.end method

.method static synthetic a(Lcom/twitter/library/provider/i;Z)Z
    .locals 0

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/twitter/library/provider/i;->c:Z

    return p1
.end method

.method static b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8

    .prologue
    .line 342
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 344
    :try_start_0
    const-string/jumbo v1, "drafts"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v3, "_id"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string/jumbo v3, "media"

    aput-object v3, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 346
    if-eqz v1, :cond_2

    .line 348
    :try_start_1
    new-instance v2, Landroid/content/ContentValues;

    const/4 v0, 0x1

    invoke-direct {v2, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 349
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 350
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 351
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/media/model/legacyeditablemedia/a;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 352
    if-eqz v0, :cond_0

    .line 353
    const-string/jumbo v3, "media"

    sget-object v6, Lcom/twitter/model/media/EditableMedia;->j:Lcom/twitter/util/serialization/l;

    .line 354
    invoke-static {v6}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v6

    .line 353
    invoke-static {v0, v6}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 355
    const-string/jumbo v0, "drafts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "_id="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p0, v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 359
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 364
    :catchall_1
    move-exception v0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 359
    :cond_1
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 362
    :cond_2
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 364
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 366
    return-void
.end method

.method static b(Lcom/twitter/database/model/j;)V
    .locals 6

    .prologue
    .line 293
    const-class v0, Lcom/twitter/database/schema/DraftsSchema;

    invoke-static {v0}, Lcom/twitter/database/model/i$a;->a(Ljava/lang/Class;)Lcom/twitter/database/model/i;

    move-result-object v0

    .line 294
    const-string/jumbo v1, ", "

    const-class v2, Lcom/twitter/database/schema/DraftsSchema$a;

    .line 295
    invoke-interface {v0, v2}, Lcom/twitter/database/model/i;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Lcom/twitter/database/schema/DraftsSchema$a;

    invoke-interface {v0}, Lcom/twitter/database/schema/DraftsSchema$a;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    check-cast v0, Lcom/twitter/database/internal/i;

    invoke-virtual {v0}, Lcom/twitter/database/internal/i;->a()[Ljava/lang/String;

    move-result-object v0

    .line 294
    invoke-static {v1, v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 296
    const-string/jumbo v1, "drafts_old"

    .line 297
    invoke-interface {p0}, Lcom/twitter/database/model/j;->a()Lcom/twitter/database/model/o;

    move-result-object v1

    .line 299
    const/4 v2, 0x1

    :try_start_0
    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "ALTER TABLE drafts RENAME TO drafts_old;"

    aput-object v4, v2, v3

    invoke-interface {p0, v2}, Lcom/twitter/database/model/j;->c([Ljava/lang/String;)V

    .line 300
    const-class v2, Lcom/twitter/database/schema/DraftsSchema$a;

    invoke-interface {p0, v2}, Lcom/twitter/database/model/j;->a(Ljava/lang/Class;)V

    .line 301
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "INSERT INTO drafts SELECT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, " FROM "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, "drafts_old"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v4, 0x3b

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {p0, v2}, Lcom/twitter/database/model/j;->c([Ljava/lang/String;)V

    .line 303
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "DROP TABLE drafts_old;"

    aput-object v3, v0, v2

    invoke-interface {p0, v0}, Lcom/twitter/database/model/j;->c([Ljava/lang/String;)V

    .line 304
    invoke-interface {v1}, Lcom/twitter/database/model/o;->a()Lcom/twitter/database/model/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 306
    invoke-interface {v1}, Lcom/twitter/database/model/o;->close()V

    .line 308
    return-void

    .line 306
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Lcom/twitter/database/model/o;->close()V

    throw v0
.end method

.method static synthetic b(Lcom/twitter/library/provider/i;Z)Z
    .locals 0

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/twitter/library/provider/i;->d:Z

    return p1
.end method

.method static c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8

    .prologue
    .line 369
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 371
    :try_start_0
    const-string/jumbo v1, "drafts"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v3, "_id"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string/jumbo v3, "media_entities"

    aput-object v3, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 373
    if-eqz v1, :cond_2

    .line 375
    :try_start_1
    new-instance v2, Landroid/content/ContentValues;

    const/4 v0, 0x1

    invoke-direct {v2, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 376
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 377
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 378
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/media/model/legacyentities/a;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 379
    if-eqz v0, :cond_0

    .line 380
    sget-object v3, Lcom/twitter/model/media/MediaSource;->b:Lcom/twitter/model/media/MediaSource;

    invoke-static {v0, v3}, Lcom/twitter/library/media/model/legacyentities/a;->a(Ljava/util/List;Lcom/twitter/model/media/MediaSource;)Ljava/util/List;

    move-result-object v0

    .line 382
    const-string/jumbo v3, "media"

    sget-object v6, Lcom/twitter/model/media/EditableMedia;->j:Lcom/twitter/util/serialization/l;

    .line 383
    invoke-static {v6}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v6

    .line 382
    invoke-static {v0, v6}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 384
    const-string/jumbo v0, "drafts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "_id="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p0, v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 388
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 393
    :catchall_1
    move-exception v0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 388
    :cond_1
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 391
    :cond_2
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 393
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 395
    return-void
.end method

.method static d(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8

    .prologue
    .line 398
    .line 399
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 401
    :try_start_0
    const-string/jumbo v1, "drafts"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v3, "_id"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string/jumbo v3, "flags"

    aput-object v3, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 403
    if-eqz v1, :cond_2

    .line 405
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    const/4 v2, 0x2

    invoke-direct {v0, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 406
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 407
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 408
    const/4 v4, 0x1

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 409
    and-int/lit8 v5, v4, 0x1

    if-lez v5, :cond_0

    .line 410
    const-string/jumbo v5, "flags"

    and-int/lit8 v4, v4, -0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 411
    const-string/jumbo v4, "sending_state"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 412
    const-string/jumbo v4, "drafts"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v4, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 416
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 421
    :catchall_1
    move-exception v0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 416
    :cond_1
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 419
    :cond_2
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 421
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 423
    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 63
    const/16 v0, 0x1e

    return v0
.end method

.method protected a(Lcom/twitter/database/model/j;)Lcom/twitter/database/model/i;
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/twitter/library/provider/i;->c:Z

    if-eqz v0, :cond_0

    .line 56
    invoke-static {p1}, Lcom/twitter/library/provider/i;->b(Lcom/twitter/database/model/j;)V

    .line 58
    :cond_0
    invoke-super {p0, p1}, Laus;->a(Lcom/twitter/database/model/j;)Lcom/twitter/database/model/i;

    move-result-object v0

    return-object v0
.end method

.method protected b()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Laus$a;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v8, 0x9

    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    .line 69
    new-instance v0, Lcom/twitter/library/provider/i$12;

    invoke-direct {v0, p0, v4}, Lcom/twitter/library/provider/i$12;-><init>(Lcom/twitter/library/provider/i;I)V

    const/16 v1, 0x1c

    new-array v1, v1, [Laus$a;

    const/4 v2, 0x0

    new-instance v3, Lcom/twitter/library/provider/i$16;

    invoke-direct {v3, p0, v5}, Lcom/twitter/library/provider/i$16;-><init>(Lcom/twitter/library/provider/i;I)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lcom/twitter/library/provider/i$17;

    invoke-direct {v3, p0, v6}, Lcom/twitter/library/provider/i$17;-><init>(Lcom/twitter/library/provider/i;I)V

    aput-object v3, v1, v2

    new-instance v2, Lcom/twitter/library/provider/i$18;

    invoke-direct {v2, p0, v7}, Lcom/twitter/library/provider/i$18;-><init>(Lcom/twitter/library/provider/i;I)V

    aput-object v2, v1, v4

    sget-object v2, Lcom/twitter/library/provider/i;->a:Laus$a;

    aput-object v2, v1, v5

    sget-object v2, Lcom/twitter/library/provider/i;->a:Laus$a;

    aput-object v2, v1, v6

    sget-object v2, Lcom/twitter/library/provider/i;->a:Laus$a;

    aput-object v2, v1, v7

    const/4 v2, 0x6

    new-instance v3, Lcom/twitter/library/provider/i$19;

    invoke-direct {v3, p0, v8}, Lcom/twitter/library/provider/i$19;-><init>(Lcom/twitter/library/provider/i;I)V

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-instance v3, Lcom/twitter/library/provider/i$20;

    const/16 v4, 0xa

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/i$20;-><init>(Lcom/twitter/library/provider/i;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-instance v3, Lcom/twitter/library/provider/i$21;

    const/16 v4, 0xb

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/i$21;-><init>(Lcom/twitter/library/provider/i;I)V

    aput-object v3, v1, v2

    iget-object v2, p0, Lcom/twitter/library/provider/i;->b:Laus$a;

    aput-object v2, v1, v8

    const/16 v2, 0xa

    iget-object v3, p0, Lcom/twitter/library/provider/i;->b:Laus$a;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    new-instance v3, Lcom/twitter/library/provider/i$22;

    const/16 v4, 0xe

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/i$22;-><init>(Lcom/twitter/library/provider/i;I)V

    aput-object v3, v1, v2

    const/16 v2, 0xc

    new-instance v3, Lcom/twitter/library/provider/i$2;

    const/16 v4, 0xf

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/i$2;-><init>(Lcom/twitter/library/provider/i;I)V

    aput-object v3, v1, v2

    const/16 v2, 0xd

    sget-object v3, Lcom/twitter/library/provider/i;->a:Laus$a;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    new-instance v3, Lcom/twitter/library/provider/i$3;

    const/16 v4, 0x11

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/i$3;-><init>(Lcom/twitter/library/provider/i;I)V

    aput-object v3, v1, v2

    const/16 v2, 0xf

    new-instance v3, Lcom/twitter/library/provider/i$4;

    const/16 v4, 0x12

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/i$4;-><init>(Lcom/twitter/library/provider/i;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x10

    sget-object v3, Lcom/twitter/library/provider/i;->a:Laus$a;

    aput-object v3, v1, v2

    const/16 v2, 0x11

    new-instance v3, Lcom/twitter/library/provider/i$5;

    const/16 v4, 0x14

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/i$5;-><init>(Lcom/twitter/library/provider/i;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x12

    new-instance v3, Lcom/twitter/library/provider/i$6;

    const/16 v4, 0x15

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/i$6;-><init>(Lcom/twitter/library/provider/i;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x13

    iget-object v3, p0, Lcom/twitter/library/provider/i;->b:Laus$a;

    aput-object v3, v1, v2

    const/16 v2, 0x14

    new-instance v3, Lcom/twitter/library/provider/i$7;

    const/16 v4, 0x17

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/i$7;-><init>(Lcom/twitter/library/provider/i;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x15

    new-instance v3, Lcom/twitter/library/provider/i$8;

    const/16 v4, 0x18

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/i$8;-><init>(Lcom/twitter/library/provider/i;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x16

    new-instance v3, Lcom/twitter/library/provider/i$9;

    const/16 v4, 0x19

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/i$9;-><init>(Lcom/twitter/library/provider/i;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x17

    new-instance v3, Lcom/twitter/library/provider/i$10;

    const/16 v4, 0x1a

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/i$10;-><init>(Lcom/twitter/library/provider/i;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x18

    new-instance v3, Lcom/twitter/library/provider/i$11;

    const/16 v4, 0x1b

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/i$11;-><init>(Lcom/twitter/library/provider/i;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x19

    new-instance v3, Lcom/twitter/library/provider/i$13;

    const/16 v4, 0x1c

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/i$13;-><init>(Lcom/twitter/library/provider/i;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    new-instance v3, Lcom/twitter/library/provider/i$14;

    const/16 v4, 0x1d

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/i$14;-><init>(Lcom/twitter/library/provider/i;I)V

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    new-instance v3, Lcom/twitter/library/provider/i$15;

    const/16 v4, 0x1e

    invoke-direct {v3, p0, v4}, Lcom/twitter/library/provider/i$15;-><init>(Lcom/twitter/library/provider/i;I)V

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
