.class public Lcom/twitter/library/provider/h;
.super Laur;
.source "Twttr"


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;


# instance fields
.field private d:Lcom/twitter/database/schema/DraftsSchema;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 51
    const-string/jumbo v0, "_id"

    invoke-static {v0}, Laux;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/provider/h;->a:Ljava/lang/String;

    .line 53
    const-string/jumbo v0, "sending_state"

    invoke-static {v0}, Laux;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/provider/h;->b:Ljava/lang/String;

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "sending_state!=1 AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/provider/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/provider/h;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;J)V
    .locals 2

    .prologue
    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "drafts"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".db"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x1e

    invoke-direct {p0, p1, v0, v1}, Laur;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 77
    invoke-virtual {p0}, Lcom/twitter/library/provider/h;->b()V

    .line 78
    return-void
.end method

.method public static declared-synchronized a(J)Lcom/twitter/library/provider/h;
    .locals 2

    .prologue
    .line 71
    const-class v1, Lcom/twitter/library/provider/h;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcnz;

    invoke-direct {v0, p0, p1}, Lcnz;-><init>(J)V

    invoke-static {v0}, Lbnt;->b(Lcnz;)Lbnt;

    move-result-object v0

    invoke-virtual {v0}, Lbnt;->ai()Lcom/twitter/library/provider/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static c(J)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/c",
            "<[J>;"
        }
    .end annotation

    .prologue
    .line 188
    new-instance v0, Lcom/twitter/library/provider/h$2;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/provider/h$2;-><init>(J)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    .line 200
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 188
    return-object v0
.end method


# virtual methods
.method public a(I)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 173
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 175
    if-ne p1, v4, :cond_0

    .line 176
    sget-object v0, Lcom/twitter/library/provider/h;->b:Ljava/lang/String;

    .line 182
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/library/provider/h;->c()Lcom/twitter/database/schema/DraftsSchema;

    move-result-object v2

    const-class v3, Lcom/twitter/database/schema/DraftsSchema$b;

    invoke-interface {v2, v3}, Lcom/twitter/database/schema/DraftsSchema;->b(Ljava/lang/Class;)Lcom/twitter/database/model/l;

    move-result-object v2

    .line 183
    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-interface {v2, v0, v3}, Lcom/twitter/database/model/l;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    move-result v0

    :goto_1
    return v0

    .line 177
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 178
    const-string/jumbo v0, "sending_state!=?"

    goto :goto_0

    :cond_1
    move v0, v1

    .line 180
    goto :goto_1
.end method

.method public a(Lcom/twitter/model/drafts/a;ILaut;)J
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 123
    .line 124
    invoke-virtual {p0}, Lcom/twitter/library/provider/h;->c()Lcom/twitter/database/schema/DraftsSchema;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/database/hydrator/d;->a(Lcom/twitter/database/model/i;)Lcom/twitter/database/hydrator/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/database/hydrator/d;->a(Ljava/lang/Object;)Lcom/twitter/database/model/h;

    move-result-object v2

    .line 125
    if-nez v2, :cond_1

    .line 126
    const-wide/16 v0, -0x1

    .line 143
    :cond_0
    :goto_0
    return-wide v0

    .line 129
    :cond_1
    iget-object v0, v2, Lcom/twitter/database/model/h;->d:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/database/schema/DraftsSchema$c$a;

    invoke-interface {v0, p2}, Lcom/twitter/database/schema/DraftsSchema$c$a;->a(I)Lcom/twitter/database/schema/DraftsSchema$c$a;

    .line 131
    iget-wide v0, p1, Lcom/twitter/model/drafts/a;->b:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-lez v0, :cond_3

    .line 132
    iget-wide v0, p1, Lcom/twitter/model/drafts/a;->b:J

    .line 133
    const-string/jumbo v3, "_id=?"

    new-array v4, v9, [Ljava/lang/String;

    iget-wide v6, p1, Lcom/twitter/model/drafts/a;->b:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v2, v3, v4}, Lcom/twitter/database/model/h;->a(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_2

    .line 134
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "failed to update draft tweet"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 140
    :cond_2
    :goto_1
    if-eqz p3, :cond_0

    .line 141
    new-array v2, v9, [Landroid/net/Uri;

    sget-object v3, Lcom/twitter/database/schema/a$m;->a:Landroid/net/Uri;

    aput-object v3, v2, v8

    invoke-virtual {p3, v2}, Laut;->a([Landroid/net/Uri;)V

    goto :goto_0

    .line 137
    :cond_3
    iget-object v0, v2, Lcom/twitter/database/model/h;->d:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/database/schema/DraftsSchema$c$a;

    iget-wide v4, p1, Lcom/twitter/model/drafts/a;->e:J

    invoke-interface {v0, v4, v5}, Lcom/twitter/database/schema/DraftsSchema$c$a;->a(J)Lcom/twitter/database/schema/DraftsSchema$c$a;

    .line 138
    invoke-virtual {v2}, Lcom/twitter/database/model/h;->b()J

    move-result-wide v0

    goto :goto_1
.end method

.method public a(JILaut;)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 228
    .line 229
    invoke-virtual {p0}, Lcom/twitter/library/provider/h;->c()Lcom/twitter/database/schema/DraftsSchema;

    move-result-object v0

    .line 230
    const-class v3, Lcom/twitter/database/schema/DraftsSchema$b;

    invoke-interface {v0, v3}, Lcom/twitter/database/model/i;->b(Ljava/lang/Class;)Lcom/twitter/database/model/l;

    move-result-object v3

    .line 231
    const-class v4, Lcom/twitter/database/schema/DraftsSchema$c;

    .line 232
    invoke-interface {v0, v4}, Lcom/twitter/database/model/i;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/database/model/m;->b()Lcom/twitter/database/model/h;

    move-result-object v4

    .line 233
    invoke-virtual {p0}, Lcom/twitter/library/provider/h;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 234
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 236
    :try_start_0
    sget-object v0, Lcom/twitter/library/provider/h;->a:Ljava/lang/String;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-interface {v3, v0, v6}, Lcom/twitter/database/model/l;->a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/twitter/database/model/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    .line 238
    :try_start_1
    invoke-virtual {v3}, Lcom/twitter/database/model/g;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 239
    iget-object v0, v3, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/database/schema/DraftsSchema$b$a;

    invoke-interface {v0}, Lcom/twitter/database/schema/DraftsSchema$b$a;->b()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 240
    if-ne v0, p3, :cond_1

    move v0, v1

    .line 252
    :cond_0
    :goto_0
    :try_start_2
    invoke-virtual {v3}, Lcom/twitter/database/model/g;->close()V

    .line 254
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 256
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 258
    return v0

    .line 243
    :cond_1
    :try_start_3
    iget-object v0, v4, Lcom/twitter/database/model/h;->d:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/database/schema/DraftsSchema$c$a;

    invoke-interface {v0, p3}, Lcom/twitter/database/schema/DraftsSchema$c$a;->a(I)Lcom/twitter/database/schema/DraftsSchema$c$a;

    .line 244
    const-string/jumbo v0, "_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v0, v6}, Lcom/twitter/database/model/h;->a(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 245
    if-lez v0, :cond_2

    move v0, v1

    .line 246
    :goto_1
    if-eqz p4, :cond_0

    .line 247
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/net/Uri;

    const/4 v2, 0x0

    sget-object v4, Lcom/twitter/database/schema/a$m;->a:Landroid/net/Uri;

    aput-object v4, v1, v2

    invoke-virtual {p4, v1}, Laut;->a([Landroid/net/Uri;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 252
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-virtual {v3}, Lcom/twitter/database/model/g;->close()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 256
    :catchall_1
    move-exception v0

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    :cond_2
    move v0, v2

    .line 245
    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public a(JLaut;Z)Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 148
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 150
    if-eqz p4, :cond_1

    .line 151
    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/provider/h;->b(J)Lcom/twitter/model/drafts/a;

    move-result-object v1

    .line 152
    if-nez v1, :cond_0

    .line 168
    :goto_0
    return v0

    .line 155
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/model/drafts/a;->c()V

    .line 158
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/provider/h;->c()Lcom/twitter/database/schema/DraftsSchema;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/database/hydrator/d;->a(Lcom/twitter/database/model/i;)Lcom/twitter/database/hydrator/d;

    move-result-object v1

    const-class v3, Lcom/twitter/database/schema/DraftsSchema$c;

    new-instance v4, Lcom/twitter/database/model/f$a;

    invoke-direct {v4}, Lcom/twitter/database/model/f$a;-><init>()V

    const-string/jumbo v5, "_id"

    .line 161
    invoke-static {v5}, Laux;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-array v6, v2, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v0

    invoke-virtual {v4, v5, v6}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v4

    .line 162
    invoke-virtual {v4}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v4

    .line 159
    invoke-virtual {v1, v3, v4}, Lcom/twitter/database/hydrator/d;->a(Ljava/lang/Class;Lcom/twitter/database/model/f;)I

    move-result v1

    if-ne v1, v2, :cond_3

    move v1, v2

    .line 165
    :goto_1
    if-eqz v1, :cond_2

    if-eqz p3, :cond_2

    .line 166
    new-array v2, v2, [Landroid/net/Uri;

    sget-object v3, Lcom/twitter/database/schema/a$m;->a:Landroid/net/Uri;

    aput-object v3, v2, v0

    invoke-virtual {p3, v2}, Laut;->a([Landroid/net/Uri;)V

    :cond_2
    move v0, v1

    .line 168
    goto :goto_0

    :cond_3
    move v1, v0

    .line 159
    goto :goto_1
.end method

.method public b(J)Lcom/twitter/model/drafts/a;
    .locals 7

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/twitter/library/provider/h;->c()Lcom/twitter/database/schema/DraftsSchema;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/database/hydrator/c;->a(Lcom/twitter/database/model/i;)Lcom/twitter/database/hydrator/c;

    move-result-object v0

    const-class v1, Lcom/twitter/database/schema/DraftsSchema$a;

    new-instance v2, Lcom/twitter/database/model/f$a;

    invoke-direct {v2}, Lcom/twitter/database/model/f$a;-><init>()V

    const-string/jumbo v3, "_id"

    .line 116
    invoke-static {v3}, Laux;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v2

    .line 117
    invoke-virtual {v2}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v2

    const-class v3, Lcom/twitter/model/drafts/a;

    .line 113
    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/database/hydrator/c;->a(Ljava/lang/Class;Lcom/twitter/database/model/f;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/a;

    return-object v0
.end method

.method public synthetic bq_()Lcom/twitter/database/model/i;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/twitter/library/provider/h;->c()Lcom/twitter/database/schema/DraftsSchema;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/twitter/database/schema/DraftsSchema;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/library/provider/h;->d:Lcom/twitter/database/schema/DraftsSchema;

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Lcom/twitter/library/provider/h$1;

    invoke-direct {v0, p0}, Lcom/twitter/library/provider/h$1;-><init>(Lcom/twitter/library/provider/h;)V

    invoke-static {v0}, Lcom/twitter/util/f;->a(Lcom/twitter/util/concurrent/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/database/schema/DraftsSchema;

    iput-object v0, p0, Lcom/twitter/library/provider/h;->d:Lcom/twitter/database/schema/DraftsSchema;

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/provider/h;->d:Lcom/twitter/database/schema/DraftsSchema;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/database/schema/DraftsSchema;

    return-object v0
.end method

.method public d()[J
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 208
    invoke-virtual {p0}, Lcom/twitter/library/provider/h;->c()Lcom/twitter/database/schema/DraftsSchema;

    move-result-object v1

    const-class v2, Lcom/twitter/database/schema/DraftsSchema$b;

    invoke-interface {v1, v2}, Lcom/twitter/database/schema/DraftsSchema;->b(Ljava/lang/Class;)Lcom/twitter/database/model/l;

    move-result-object v1

    .line 209
    sget-object v2, Lcom/twitter/library/provider/h;->b:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-interface {v1, v2, v3}, Lcom/twitter/database/model/l;->a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/twitter/database/model/g;

    move-result-object v2

    .line 210
    invoke-virtual {v2}, Lcom/twitter/database/model/g;->a()I

    move-result v1

    new-array v3, v1, [J

    move v1, v0

    .line 213
    :goto_0
    :try_start_0
    invoke-virtual {v2}, Lcom/twitter/database/model/g;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, v2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/database/schema/DraftsSchema$b$a;

    invoke-interface {v0}, Lcom/twitter/database/schema/DraftsSchema$b$a;->a()J

    move-result-wide v4

    aput-wide v4, v3, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 215
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 218
    :cond_0
    invoke-virtual {v2}, Lcom/twitter/database/model/g;->close()V

    .line 220
    return-object v3

    .line 218
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/twitter/database/model/g;->close()V

    throw v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 100
    const-class v0, Lcom/twitter/database/schema/DraftsSchema;

    new-instance v1, Lava;

    invoke-direct {v1, p1}, Lava;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {v0, v1}, Lcom/twitter/database/model/i$a;->a(Ljava/lang/Class;Lcom/twitter/database/model/a;)Lcom/twitter/database/model/i;

    move-result-object v0

    .line 101
    invoke-interface {v0}, Lcom/twitter/database/model/i;->g()V

    .line 102
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    .line 106
    const-class v0, Lcom/twitter/database/schema/DraftsSchema;

    new-instance v1, Lava;

    invoke-direct {v1, p1}, Lava;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {v0, v1}, Lcom/twitter/database/model/j$a;->a(Ljava/lang/Class;Lcom/twitter/database/model/a;)Lcom/twitter/database/model/j;

    move-result-object v0

    .line 108
    new-instance v1, Lcom/twitter/library/provider/i;

    invoke-direct {v1, v0, p1}, Lcom/twitter/library/provider/i;-><init>(Lcom/twitter/database/model/j;Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-virtual {v1, p2, p3}, Lcom/twitter/library/provider/i;->a(II)V

    .line 109
    return-void
.end method
