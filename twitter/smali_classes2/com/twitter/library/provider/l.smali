.class public abstract Lcom/twitter/library/provider/l;
.super Landroid/database/CursorWrapper;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/provider/l$a;,
        Lcom/twitter/library/provider/l$c;,
        Lcom/twitter/library/provider/l$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/database/CursorWrapper;"
    }
.end annotation


# instance fields
.field protected final a:Lcom/twitter/library/provider/l$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/library/provider/l",
            "<TT;>.b;"
        }
    .end annotation
.end field

.field protected final b:I

.field protected c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field protected d:I

.field protected e:Landroid/database/Cursor;

.field volatile f:Z

.field volatile g:Z

.field volatile h:Z

.field private final i:Landroid/database/ContentObservable;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 34
    const/16 v0, 0x190

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/provider/l;-><init>(Landroid/database/Cursor;I)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;I)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 20
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/library/provider/l;->d:I

    .line 39
    if-gtz p2, :cond_0

    .line 40
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Limit must be greater than 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_0
    new-instance v0, Lcom/twitter/library/provider/l$b;

    invoke-direct {v0, p0}, Lcom/twitter/library/provider/l$b;-><init>(Lcom/twitter/library/provider/l;)V

    iput-object v0, p0, Lcom/twitter/library/provider/l;->a:Lcom/twitter/library/provider/l$b;

    .line 43
    new-instance v0, Landroid/database/ContentObservable;

    invoke-direct {v0}, Landroid/database/ContentObservable;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/provider/l;->i:Landroid/database/ContentObservable;

    .line 44
    iput-object p1, p0, Lcom/twitter/library/provider/l;->e:Landroid/database/Cursor;

    .line 45
    iput p2, p0, Lcom/twitter/library/provider/l;->b:I

    .line 46
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 47
    new-instance v0, Lcom/twitter/library/provider/l$c;

    invoke-direct {v0, p0}, Lcom/twitter/library/provider/l$c;-><init>(Lcom/twitter/library/provider/l;)V

    invoke-interface {p1, v0}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 48
    new-instance v0, Lcom/twitter/library/provider/l$a;

    invoke-direct {v0, p0}, Lcom/twitter/library/provider/l$a;-><init>(Lcom/twitter/library/provider/l;)V

    invoke-interface {p1, v0}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 50
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/provider/l;)Landroid/database/ContentObservable;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/twitter/library/provider/l;->i:Landroid/database/ContentObservable;

    return-object v0
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/provider/l;->f:Z

    .line 57
    invoke-virtual {p0}, Lcom/twitter/library/provider/l;->a()V

    .line 58
    iput-boolean v1, p0, Lcom/twitter/library/provider/l;->f:Z

    .line 59
    iget-boolean v0, p0, Lcom/twitter/library/provider/l;->g:Z

    if-eqz v0, :cond_0

    .line 60
    iput-boolean v1, p0, Lcom/twitter/library/provider/l;->g:Z

    .line 61
    iget-object v0, p0, Lcom/twitter/library/provider/l;->i:Landroid/database/ContentObservable;

    iget-boolean v1, p0, Lcom/twitter/library/provider/l;->h:Z

    invoke-virtual {v0, v1}, Landroid/database/ContentObservable;->dispatchChange(Z)V

    .line 63
    :cond_0
    return-void
.end method

.method public c()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/twitter/library/provider/l;->c:Ljava/util/List;

    if-nez v0, :cond_0

    .line 138
    const/4 v0, 0x0

    .line 140
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/provider/l;->c:Ljava/util/List;

    iget v1, p0, Lcom/twitter/library/provider/l;->d:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/library/provider/l;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/twitter/library/provider/l;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 72
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/twitter/library/provider/l;->d:I

    return v0
.end method

.method public moveToFirst()Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/provider/l;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToLast()Z
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/twitter/library/provider/l;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/library/provider/l;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToNext()Z
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/twitter/library/provider/l;->d:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/library/provider/l;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToPosition(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 103
    invoke-virtual {p0}, Lcom/twitter/library/provider/l;->getCount()I

    move-result v1

    .line 104
    if-gt p1, v2, :cond_0

    .line 105
    iput v2, p0, Lcom/twitter/library/provider/l;->d:I

    .line 112
    :goto_0
    return v0

    .line 107
    :cond_0
    if-lt p1, v1, :cond_1

    .line 108
    iput v1, p0, Lcom/twitter/library/provider/l;->d:I

    goto :goto_0

    .line 111
    :cond_1
    iput p1, p0, Lcom/twitter/library/provider/l;->d:I

    .line 112
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public moveToPrevious()Z
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/twitter/library/provider/l;->d:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/library/provider/l;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/twitter/library/provider/l;->i:Landroid/database/ContentObservable;

    invoke-virtual {v0, p1}, Landroid/database/ContentObservable;->registerObserver(Landroid/database/ContentObserver;)V

    .line 129
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/twitter/library/provider/l;->a:Lcom/twitter/library/provider/l$b;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/l$b;->registerObserver(Ljava/lang/Object;)V

    .line 119
    return-void
.end method

.method public requery()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 151
    iget-object v2, p0, Lcom/twitter/library/provider/l;->a:Lcom/twitter/library/provider/l$b;

    .line 152
    invoke-virtual {v2, v0}, Lcom/twitter/library/provider/l$b;->a(Z)V

    .line 154
    iget-object v3, p0, Lcom/twitter/library/provider/l;->e:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/twitter/library/provider/l;->e:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->requery()Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    .line 155
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/provider/l;->b()V

    .line 156
    invoke-virtual {v2, v1}, Lcom/twitter/library/provider/l$b;->a(Z)V

    .line 157
    invoke-virtual {v2}, Lcom/twitter/library/provider/l$b;->notifyChanged()V

    .line 158
    return v0
.end method

.method public unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/twitter/library/provider/l;->i:Landroid/database/ContentObservable;

    invoke-virtual {v0, p1}, Landroid/database/ContentObservable;->unregisterObserver(Ljava/lang/Object;)V

    .line 134
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/twitter/library/provider/l;->a:Lcom/twitter/library/provider/l$b;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/l$b;->unregisterObserver(Ljava/lang/Object;)V

    .line 124
    return-void
.end method
