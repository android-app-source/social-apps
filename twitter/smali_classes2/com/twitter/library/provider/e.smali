.class public Lcom/twitter/library/provider/e;
.super Lcom/evernote/android/job/Job;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/evernote/android/job/Job;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 46
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 47
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v0

    invoke-virtual {v0}, Lakn;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    .line 48
    invoke-static {v0}, Lcom/twitter/library/util/b;->c(Lakm;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    .line 49
    if-eqz v2, :cond_0

    .line 50
    iget-wide v4, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v4, v5}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v3

    .line 51
    iget-wide v4, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/provider/t;->i(J)V

    .line 54
    iget-boolean v4, v2, Lcom/twitter/model/core/TwitterUser;->m:Z

    if-eqz v4, :cond_1

    .line 55
    iget-wide v4, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v3, v4, v5}, Lcom/twitter/android/util/ak;->a(Lcom/twitter/library/provider/t;J)V

    .line 57
    :cond_1
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v2, v6

    const/4 v6, 0x1

    aput-object v8, v2, v6

    const/4 v6, 0x2

    aput-object v8, v2, v6

    const/4 v6, 0x3

    const-string/jumbo v7, "database"

    aput-object v7, v2, v6

    const/4 v6, 0x4

    const-string/jumbo v7, "clean_up"

    aput-object v7, v2, v6

    invoke-direct {v3, v4, v5, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    invoke-static {v3}, Lcpm;->a(Lcpk;)V

    .line 58
    if-eqz p2, :cond_0

    .line 59
    new-instance v2, Lcom/twitter/util/a;

    invoke-virtual {v0}, Lakm;->b()Lcnz;

    move-result-object v0

    invoke-virtual {v0}, Lcnz;->b()J

    move-result-wide v4

    invoke-direct {v2, p0, v4, v5}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    invoke-virtual {v2}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v2, "auto_clean"

    .line 60
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;J)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    goto :goto_0

    .line 65
    :cond_2
    return-void
.end method

.method public static l()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 68
    invoke-static {}, Lcom/evernote/android/job/d;->a()Lcom/evernote/android/job/d;

    move-result-object v0

    const-string/jumbo v1, "DatabaseCleanUpJob"

    invoke-virtual {v0, v1}, Lcom/evernote/android/job/d;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    new-instance v0, Lcom/evernote/android/job/JobRequest$a;

    const-string/jumbo v1, "DatabaseCleanUpJob"

    invoke-direct {v0, v1}, Lcom/evernote/android/job/JobRequest$a;-><init>(Ljava/lang/String;)V

    const-wide/32 v2, 0x1b77400

    .line 70
    invoke-virtual {v0, v2, v3}, Lcom/evernote/android/job/JobRequest$a;->b(J)Lcom/evernote/android/job/JobRequest$a;

    move-result-object v0

    .line 71
    invoke-virtual {v0, v4}, Lcom/evernote/android/job/JobRequest$a;->b(Z)Lcom/evernote/android/job/JobRequest$a;

    move-result-object v0

    .line 72
    invoke-virtual {v0, v4}, Lcom/evernote/android/job/JobRequest$a;->c(Z)Lcom/evernote/android/job/JobRequest$a;

    move-result-object v0

    .line 73
    invoke-virtual {v0, v4}, Lcom/evernote/android/job/JobRequest$a;->e(Z)Lcom/evernote/android/job/JobRequest$a;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest$a;->a()Lcom/evernote/android/job/JobRequest;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->t()I

    .line 77
    :cond_0
    return-void
.end method


# virtual methods
.method protected a(Lcom/evernote/android/job/Job$a;)Lcom/evernote/android/job/Job$Result;
    .locals 4

    .prologue
    .line 38
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    .line 39
    invoke-virtual {p0}, Lcom/twitter/library/provider/e;->f()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "jobs"

    invoke-virtual {v0}, Lcof;->a()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Lcof;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Lcom/twitter/library/provider/e;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 40
    sget-object v0, Lcom/evernote/android/job/Job$Result;->a:Lcom/evernote/android/job/Job$Result;

    return-object v0

    .line 39
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
