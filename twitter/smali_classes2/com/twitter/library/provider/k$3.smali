.class Lcom/twitter/library/provider/k$3;
.super Laus$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/provider/k;->b()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic b:Lcom/twitter/library/provider/k;


# direct methods
.method constructor <init>(Lcom/twitter/library/provider/k;I)V
    .locals 0

    .prologue
    .line 309
    iput-object p1, p0, Lcom/twitter/library/provider/k$3;->b:Lcom/twitter/library/provider/k;

    invoke-direct {p0, p2}, Laus$a;-><init>(I)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/database/model/j;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 37

    .prologue
    .line 313
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/k$3;->b:Lcom/twitter/library/provider/k;

    invoke-static {v2}, Lcom/twitter/library/provider/k;->f(Lcom/twitter/library/provider/k;)Z

    move-result v2

    if-nez v2, :cond_12

    .line 314
    const-class v2, Laxb;

    new-instance v3, Lcom/twitter/database/model/ColumnDefinition$a;

    const-string/jumbo v4, "push_flags"

    sget-object v5, Lcom/twitter/database/model/ColumnDefinition$Type;->a:Lcom/twitter/database/model/ColumnDefinition$Type;

    invoke-direct {v3, v4, v5}, Lcom/twitter/database/model/ColumnDefinition$a;-><init>(Ljava/lang/String;Lcom/twitter/database/model/ColumnDefinition$Type;)V

    sget v4, Lcga;->a:I

    .line 317
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/twitter/database/model/ColumnDefinition$a;->a(Ljava/lang/Object;)Lcom/twitter/database/model/ColumnDefinition$a;

    move-result-object v3

    .line 318
    invoke-virtual {v3}, Lcom/twitter/database/model/ColumnDefinition$a;->a()Lcom/twitter/database/model/ColumnDefinition;

    move-result-object v3

    .line 314
    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3}, Lcom/twitter/database/model/j;->a(Ljava/lang/Class;Lcom/twitter/database/model/ColumnDefinition;)V

    .line 319
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/k$3;->b:Lcom/twitter/library/provider/k;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/twitter/library/provider/k;->d(Lcom/twitter/library/provider/k;Z)Z

    .line 322
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/k$3;->b:Lcom/twitter/library/provider/k;

    invoke-static {v2}, Lcom/twitter/library/provider/k;->g(Lcom/twitter/library/provider/k;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 323
    const-class v2, Laxb;

    new-instance v3, Lcom/twitter/database/model/ColumnDefinition$a;

    const-string/jumbo v4, "account_id"

    sget-object v5, Lcom/twitter/database/model/ColumnDefinition$Type;->e:Lcom/twitter/database/model/ColumnDefinition$Type;

    invoke-direct {v3, v4, v5}, Lcom/twitter/database/model/ColumnDefinition$a;-><init>(Ljava/lang/String;Lcom/twitter/database/model/ColumnDefinition$Type;)V

    const-wide/16 v4, 0x0

    .line 326
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/twitter/database/model/ColumnDefinition$a;->a(Ljava/lang/Object;Z)Lcom/twitter/database/model/ColumnDefinition$a;

    move-result-object v3

    .line 327
    invoke-virtual {v3}, Lcom/twitter/database/model/ColumnDefinition$a;->a()Lcom/twitter/database/model/ColumnDefinition;

    move-result-object v3

    .line 323
    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3}, Lcom/twitter/database/model/j;->a(Ljava/lang/Class;Lcom/twitter/database/model/ColumnDefinition;)V

    .line 328
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/k$3;->b:Lcom/twitter/library/provider/k;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/twitter/library/provider/k;->c(Lcom/twitter/library/provider/k;Z)Z

    .line 330
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/k$3;->b:Lcom/twitter/library/provider/k;

    const-string/jumbo v3, "account_settings"

    const-string/jumbo v4, "account_id"

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v3, v4}, Lcom/twitter/library/provider/k;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    new-instance v22, Ljava/util/HashMap;

    invoke-direct/range {v22 .. v22}, Ljava/util/HashMap;-><init>()V

    .line 334
    const-string/jumbo v3, "account_settings"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v23

    .line 335
    if-eqz v23, :cond_11

    .line 337
    :try_start_0
    const-string/jumbo v2, "account_name"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 338
    const-string/jumbo v2, "notif_mention"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    .line 340
    const-string/jumbo v2, "notif_message"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    .line 342
    const-string/jumbo v2, "notif_tweet"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    .line 343
    const-string/jumbo v2, "notif_experimental"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    .line 345
    const-string/jumbo v2, "notif_lifeline_alerts"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v29

    .line 347
    const-string/jumbo v2, "notif_recommendations"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    .line 349
    const-string/jumbo v2, "notif_news"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v31

    .line 350
    const-string/jumbo v2, "notif_vit_notable_event"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    .line 352
    const-string/jumbo v2, "notif_offer_redemption"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v33

    .line 354
    const-string/jumbo v2, "notif_highlights"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    .line 356
    const-string/jumbo v2, "notif_moments"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v35

    .line 358
    :cond_1
    :goto_0
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 359
    invoke-interface/range {v23 .. v24}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v36

    .line 360
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/k$3;->b:Lcom/twitter/library/provider/k;

    invoke-static {v2}, Lcom/twitter/library/provider/k;->d(Lcom/twitter/library/provider/k;)Ljava/util/Map;

    move-result-object v2

    move-object/from16 v0, v36

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lakm;

    .line 361
    if-eqz v2, :cond_1

    .line 364
    new-instance v3, Lcom/twitter/library/provider/n$a;

    invoke-direct {v3}, Lcom/twitter/library/provider/n$a;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/provider/k$3;->b:Lcom/twitter/library/provider/k;

    .line 365
    invoke-static {v4}, Lcom/twitter/library/provider/k;->e(Lcom/twitter/library/provider/k;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/twitter/library/provider/n$a;->a(I)Lcom/twitter/library/provider/n$a;

    move-result-object v6

    .line 369
    const/4 v3, -0x1

    move/from16 v0, v25

    if-eq v0, v3, :cond_d

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_d

    .line 370
    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 371
    invoke-virtual {v6, v3}, Lcom/twitter/library/provider/n$a;->b(I)Lcom/twitter/library/provider/n$a;

    move v5, v3

    .line 376
    :goto_1
    const/4 v3, -0x1

    move/from16 v0, v26

    if-eq v0, v3, :cond_2

    move-object/from16 v0, v23

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 377
    move-object/from16 v0, v23

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v6, v3}, Lcom/twitter/library/provider/n$a;->l(I)Lcom/twitter/library/provider/n$a;

    .line 379
    :cond_2
    const/4 v3, -0x1

    move/from16 v0, v27

    if-eq v0, v3, :cond_3

    move-object/from16 v0, v23

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 380
    move-object/from16 v0, v23

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v6, v3}, Lcom/twitter/library/provider/n$a;->k(I)Lcom/twitter/library/provider/n$a;

    .line 382
    :cond_3
    const/4 v3, -0x1

    move/from16 v0, v28

    if-eq v0, v3, :cond_4

    move-object/from16 v0, v23

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_4

    .line 383
    move-object/from16 v0, v23

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v6, v3}, Lcom/twitter/library/provider/n$a;->m(I)Lcom/twitter/library/provider/n$a;

    .line 385
    :cond_4
    const/4 v3, -0x1

    move/from16 v0, v29

    if-eq v0, v3, :cond_5

    move-object/from16 v0, v23

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_5

    .line 386
    move-object/from16 v0, v23

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v6, v3}, Lcom/twitter/library/provider/n$a;->n(I)Lcom/twitter/library/provider/n$a;

    .line 388
    :cond_5
    const/4 v3, -0x1

    move/from16 v0, v30

    if-eq v0, v3, :cond_6

    move-object/from16 v0, v23

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_6

    .line 389
    move-object/from16 v0, v23

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v6, v3}, Lcom/twitter/library/provider/n$a;->o(I)Lcom/twitter/library/provider/n$a;

    .line 391
    :cond_6
    const/4 v3, -0x1

    move/from16 v0, v31

    if-eq v0, v3, :cond_7

    move-object/from16 v0, v23

    move/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_7

    .line 392
    move-object/from16 v0, v23

    move/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v6, v3}, Lcom/twitter/library/provider/n$a;->p(I)Lcom/twitter/library/provider/n$a;

    .line 394
    :cond_7
    const/4 v3, -0x1

    move/from16 v0, v32

    if-eq v0, v3, :cond_8

    move-object/from16 v0, v23

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_8

    .line 395
    move-object/from16 v0, v23

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v6, v3}, Lcom/twitter/library/provider/n$a;->q(I)Lcom/twitter/library/provider/n$a;

    .line 397
    :cond_8
    const/4 v3, -0x1

    move/from16 v0, v33

    if-eq v0, v3, :cond_9

    move-object/from16 v0, v23

    move/from16 v1, v33

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_9

    .line 398
    move-object/from16 v0, v23

    move/from16 v1, v33

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v6, v3}, Lcom/twitter/library/provider/n$a;->r(I)Lcom/twitter/library/provider/n$a;

    .line 400
    :cond_9
    const/4 v3, -0x1

    move/from16 v0, v34

    if-eq v0, v3, :cond_a

    move-object/from16 v0, v23

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_a

    .line 401
    move-object/from16 v0, v23

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v6, v3}, Lcom/twitter/library/provider/n$a;->s(I)Lcom/twitter/library/provider/n$a;

    .line 403
    :cond_a
    const/4 v3, -0x1

    move/from16 v0, v35

    if-eq v0, v3, :cond_b

    move-object/from16 v0, v23

    move/from16 v1, v35

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_b

    .line 404
    move-object/from16 v0, v23

    move/from16 v1, v35

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v6, v3}, Lcom/twitter/library/provider/n$a;->t(I)Lcom/twitter/library/provider/n$a;

    .line 406
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/provider/k$3;->b:Lcom/twitter/library/provider/k;

    invoke-static {v3}, Lcom/twitter/library/provider/k;->h(Lcom/twitter/library/provider/k;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 407
    if-eqz v2, :cond_e

    .line 408
    invoke-static {v2}, Lcom/twitter/library/util/b;->c(Lakm;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v3

    move-object v4, v3

    .line 409
    :goto_2
    and-int/lit8 v3, v5, 0x40

    if-eqz v3, :cond_f

    const/4 v3, 0x1

    .line 412
    :goto_3
    if-eqz v3, :cond_c

    if-eqz v4, :cond_c

    iget-boolean v3, v4, Lcom/twitter/model/core/TwitterUser;->m:Z

    if-eqz v3, :cond_c

    .line 413
    const/4 v3, 0x1

    invoke-virtual {v6, v3}, Lcom/twitter/library/provider/n$a;->j(I)Lcom/twitter/library/provider/n$a;

    .line 416
    :cond_c
    invoke-virtual {v6}, Lcom/twitter/library/provider/n$a;->a()Lcom/twitter/library/provider/n;

    move-result-object v21

    .line 417
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/provider/k$3;->b:Lcom/twitter/library/provider/k;

    .line 418
    invoke-static {v3}, Lcom/twitter/library/provider/k;->a(Lcom/twitter/library/provider/k;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2}, Lakm;->b()Lcnz;

    move-result-object v2

    invoke-virtual {v2}, Lcnz;->b()J

    move-result-wide v4

    .line 417
    invoke-static {v3, v4, v5}, Lbaw;->a(Landroid/content/Context;J)Z

    move-result v20

    .line 419
    move-object/from16 v0, v21

    iget v2, v0, Lcom/twitter/library/provider/n;->m:I

    move-object/from16 v0, v21

    iget v3, v0, Lcom/twitter/library/provider/n;->e:I

    move-object/from16 v0, v21

    iget v4, v0, Lcom/twitter/library/provider/n;->f:I

    move-object/from16 v0, v21

    iget v5, v0, Lcom/twitter/library/provider/n;->g:I

    move-object/from16 v0, v21

    iget v6, v0, Lcom/twitter/library/provider/n;->h:I

    move-object/from16 v0, v21

    iget v7, v0, Lcom/twitter/library/provider/n;->n:I

    move-object/from16 v0, v21

    iget v8, v0, Lcom/twitter/library/provider/n;->i:I

    move-object/from16 v0, v21

    iget v9, v0, Lcom/twitter/library/provider/n;->o:I

    move-object/from16 v0, v21

    iget v10, v0, Lcom/twitter/library/provider/n;->p:I

    move-object/from16 v0, v21

    iget v11, v0, Lcom/twitter/library/provider/n;->q:I

    move-object/from16 v0, v21

    iget v12, v0, Lcom/twitter/library/provider/n;->r:I

    move-object/from16 v0, v21

    iget v13, v0, Lcom/twitter/library/provider/n;->s:I

    move-object/from16 v0, v21

    iget v14, v0, Lcom/twitter/library/provider/n;->j:I

    move-object/from16 v0, v21

    iget v15, v0, Lcom/twitter/library/provider/n;->k:I

    move-object/from16 v0, v21

    iget v0, v0, Lcom/twitter/library/provider/n;->l:I

    move/from16 v16, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/twitter/library/provider/n;->t:I

    move/from16 v17, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/twitter/library/provider/n;->u:I

    move/from16 v18, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/twitter/library/provider/n;->v:I

    move/from16 v19, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/twitter/library/provider/n;->w:I

    move/from16 v21, v0

    invoke-static/range {v2 .. v21}, Lcom/twitter/library/provider/NotificationSetting;->a(IIIIIIIIIIIIIIIIIIZI)I

    move-result v2

    .line 429
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v22

    move-object/from16 v1, v36

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 432
    :catchall_0
    move-exception v2

    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    throw v2

    .line 373
    :cond_d
    const/16 v3, 0x2755

    move v5, v3

    goto/16 :goto_1

    .line 408
    :cond_e
    const/4 v3, 0x0

    move-object v4, v3

    goto/16 :goto_2

    .line 409
    :cond_f
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 432
    :cond_10
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 435
    :cond_11
    new-instance v6, Landroid/content/ContentValues;

    const/4 v2, 0x1

    invoke-direct {v6, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 436
    invoke-interface/range {v22 .. v22}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 437
    const-string/jumbo v4, "push_flags"

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v6, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 438
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 439
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/provider/k$3;->b:Lcom/twitter/library/provider/k;

    invoke-static {v3}, Lcom/twitter/library/provider/k;->d(Lcom/twitter/library/provider/k;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lakm;

    .line 440
    invoke-virtual {v2}, Lakm;->b()Lcnz;

    move-result-object v2

    invoke-virtual {v2}, Lcnz;->b()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/k$3;->b:Lcom/twitter/library/provider/k;

    .line 441
    invoke-static {v2}, Lcom/twitter/library/provider/k;->c(Lcom/twitter/library/provider/k;)Z

    move-result v7

    const/4 v8, 0x0

    move-object/from16 v3, p2

    .line 440
    invoke-static/range {v3 .. v8}, Lcom/twitter/library/provider/j;->a(Landroid/database/sqlite/SQLiteDatabase;JLandroid/content/ContentValues;ZLaut;)I

    goto :goto_4

    .line 444
    :cond_12
    return-void
.end method
