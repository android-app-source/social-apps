.class public final Lcom/twitter/library/provider/n$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/provider/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field a:Z

.field b:Ljava/lang/String;

.field c:Z

.field d:I

.field e:I

.field f:I

.field g:I

.field h:I

.field i:I

.field j:I

.field k:I

.field l:I

.field m:I

.field n:I

.field o:I

.field p:I

.field q:I

.field r:I

.field s:I

.field t:I

.field u:I

.field v:I

.field w:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    sget-boolean v0, Lcom/twitter/library/provider/GlobalDatabaseProvider$a;->b:Z

    iput-boolean v0, p0, Lcom/twitter/library/provider/n$a;->a:Z

    .line 70
    sget-object v0, Lcom/twitter/library/provider/GlobalDatabaseProvider$a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/provider/n$a;->b:Ljava/lang/String;

    .line 72
    sget-boolean v0, Lcom/twitter/library/provider/GlobalDatabaseProvider$a;->c:Z

    iput-boolean v0, p0, Lcom/twitter/library/provider/n$a;->c:Z

    .line 73
    iput v1, p0, Lcom/twitter/library/provider/n$a;->d:I

    .line 74
    iput v1, p0, Lcom/twitter/library/provider/n$a;->e:I

    .line 75
    iput v1, p0, Lcom/twitter/library/provider/n$a;->f:I

    .line 76
    iput v1, p0, Lcom/twitter/library/provider/n$a;->g:I

    .line 77
    iput v1, p0, Lcom/twitter/library/provider/n$a;->h:I

    .line 78
    iput v1, p0, Lcom/twitter/library/provider/n$a;->i:I

    .line 79
    iput v1, p0, Lcom/twitter/library/provider/n$a;->j:I

    .line 80
    iput v1, p0, Lcom/twitter/library/provider/n$a;->k:I

    .line 81
    iput v1, p0, Lcom/twitter/library/provider/n$a;->l:I

    .line 82
    iput v1, p0, Lcom/twitter/library/provider/n$a;->m:I

    .line 83
    iput v1, p0, Lcom/twitter/library/provider/n$a;->n:I

    .line 84
    iput v1, p0, Lcom/twitter/library/provider/n$a;->o:I

    .line 85
    iput v1, p0, Lcom/twitter/library/provider/n$a;->p:I

    .line 86
    iput v1, p0, Lcom/twitter/library/provider/n$a;->q:I

    .line 87
    iput v1, p0, Lcom/twitter/library/provider/n$a;->r:I

    .line 88
    iput v1, p0, Lcom/twitter/library/provider/n$a;->s:I

    .line 89
    iput v1, p0, Lcom/twitter/library/provider/n$a;->t:I

    .line 90
    iput v1, p0, Lcom/twitter/library/provider/n$a;->u:I

    .line 91
    iput v1, p0, Lcom/twitter/library/provider/n$a;->v:I

    .line 92
    iput-boolean v1, p0, Lcom/twitter/library/provider/n$a;->w:Z

    .line 95
    return-void
.end method


# virtual methods
.method public a(I)Lcom/twitter/library/provider/n$a;
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->a:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->d:I

    .line 144
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->c:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->f:I

    .line 145
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->b:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->e:I

    .line 146
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->d:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->g:I

    .line 147
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->h:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->h:I

    .line 148
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->l:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->i:I

    .line 149
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->m:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->j:I

    .line 150
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->o:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->k:I

    .line 151
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->g:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->l:I

    .line 152
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->e:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->m:I

    .line 153
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->i:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->n:I

    .line 154
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->f:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->o:I

    .line 155
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->j:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->p:I

    .line 156
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->k:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->q:I

    .line 157
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->n:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->r:I

    .line 158
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->p:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->s:I

    .line 159
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->q:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->t:I

    .line 160
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->r:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->u:I

    .line 161
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->s:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->v:I

    .line 162
    const/16 v0, 0x400

    invoke-static {p1, v0}, Lcga;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/provider/n$a;->w:Z

    .line 163
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/twitter/library/provider/n$a;->b:Ljava/lang/String;

    .line 132
    return-object p0
.end method

.method public a(Z)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 125
    iput-boolean p1, p0, Lcom/twitter/library/provider/n$a;->a:Z

    .line 126
    return-object p0
.end method

.method public a()Lcom/twitter/library/provider/n;
    .locals 2

    .prologue
    .line 301
    new-instance v0, Lcom/twitter/library/provider/n;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/provider/n;-><init>(Lcom/twitter/library/provider/n$a;Lcom/twitter/library/provider/n$1;)V

    return-object v0
.end method

.method public b(I)Lcom/twitter/library/provider/n$a;
    .locals 1

    .prologue
    .line 168
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->a:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->d:I

    .line 169
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->c:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->f:I

    .line 170
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->b:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->e:I

    .line 171
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->d:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->g:I

    .line 172
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->h:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->h:I

    .line 173
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->l:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->i:I

    .line 174
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->m:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->j:I

    .line 175
    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->o:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/n$a;->k:I

    .line 176
    return-object p0
.end method

.method public b(Z)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 137
    iput-boolean p1, p0, Lcom/twitter/library/provider/n$a;->c:Z

    .line 138
    return-object p0
.end method

.method public c(I)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 181
    iput p1, p0, Lcom/twitter/library/provider/n$a;->d:I

    .line 182
    return-object p0
.end method

.method public c(Z)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 295
    iput-boolean p1, p0, Lcom/twitter/library/provider/n$a;->w:Z

    .line 296
    return-object p0
.end method

.method public d(I)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 187
    iput p1, p0, Lcom/twitter/library/provider/n$a;->e:I

    .line 188
    return-object p0
.end method

.method public e(I)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 193
    iput p1, p0, Lcom/twitter/library/provider/n$a;->f:I

    .line 194
    return-object p0
.end method

.method public f(I)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 199
    iput p1, p0, Lcom/twitter/library/provider/n$a;->g:I

    .line 200
    return-object p0
.end method

.method public g(I)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 205
    iput p1, p0, Lcom/twitter/library/provider/n$a;->h:I

    .line 206
    return-object p0
.end method

.method public h(I)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 211
    iput p1, p0, Lcom/twitter/library/provider/n$a;->i:I

    .line 212
    return-object p0
.end method

.method public i(I)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 217
    iput p1, p0, Lcom/twitter/library/provider/n$a;->j:I

    .line 218
    return-object p0
.end method

.method public j(I)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 223
    iput p1, p0, Lcom/twitter/library/provider/n$a;->k:I

    .line 224
    return-object p0
.end method

.method public k(I)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 229
    iput p1, p0, Lcom/twitter/library/provider/n$a;->l:I

    .line 230
    return-object p0
.end method

.method public l(I)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 235
    iput p1, p0, Lcom/twitter/library/provider/n$a;->m:I

    .line 236
    return-object p0
.end method

.method public m(I)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 241
    iput p1, p0, Lcom/twitter/library/provider/n$a;->n:I

    .line 242
    return-object p0
.end method

.method public n(I)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 247
    iput p1, p0, Lcom/twitter/library/provider/n$a;->o:I

    .line 248
    return-object p0
.end method

.method public o(I)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 253
    iput p1, p0, Lcom/twitter/library/provider/n$a;->p:I

    .line 254
    return-object p0
.end method

.method public p(I)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 259
    iput p1, p0, Lcom/twitter/library/provider/n$a;->q:I

    .line 260
    return-object p0
.end method

.method public q(I)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 265
    iput p1, p0, Lcom/twitter/library/provider/n$a;->r:I

    .line 266
    return-object p0
.end method

.method public r(I)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 271
    iput p1, p0, Lcom/twitter/library/provider/n$a;->s:I

    .line 272
    return-object p0
.end method

.method public s(I)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 277
    iput p1, p0, Lcom/twitter/library/provider/n$a;->t:I

    .line 278
    return-object p0
.end method

.method public t(I)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 283
    iput p1, p0, Lcom/twitter/library/provider/n$a;->u:I

    .line 284
    return-object p0
.end method

.method public u(I)Lcom/twitter/library/provider/n$a;
    .locals 0

    .prologue
    .line 289
    iput p1, p0, Lcom/twitter/library/provider/n$a;->v:I

    .line 290
    return-object p0
.end method
