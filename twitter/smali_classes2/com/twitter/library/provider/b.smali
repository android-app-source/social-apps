.class public Lcom/twitter/library/provider/b;
.super Lcom/twitter/library/provider/c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/provider/b$a;,
        Lcom/twitter/library/provider/b$b;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/library/provider/b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcom/twitter/model/dms/q;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/twitter/library/provider/b$b;

    invoke-direct {v0}, Lcom/twitter/library/provider/b$b;-><init>()V

    sput-object v0, Lcom/twitter/library/provider/b;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/library/provider/b$a;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/twitter/library/provider/c;-><init>(Lcom/twitter/library/provider/c$a;)V

    .line 28
    iget-object v0, p1, Lcom/twitter/library/provider/b$a;->a:Lcom/twitter/model/dms/q;

    iput-object v0, p0, Lcom/twitter/library/provider/b;->b:Lcom/twitter/model/dms/q;

    .line 29
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/library/provider/b$a;Lcom/twitter/library/provider/b$1;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/twitter/library/provider/b;-><init>(Lcom/twitter/library/provider/b$a;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/library/provider/b;->b:Lcom/twitter/model/dms/q;

    iget-object v0, v0, Lcom/twitter/model/dms/q;->i:Ljava/util/List;

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    return v0
.end method

.method public c()Lcom/twitter/model/dms/q;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/library/provider/b;->b:Lcom/twitter/model/dms/q;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/library/provider/b;->b:Lcom/twitter/model/dms/q;

    iget-object v0, v0, Lcom/twitter/model/dms/q;->b:Ljava/lang/String;

    return-object v0
.end method
