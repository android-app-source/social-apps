.class public Lcom/twitter/library/av/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/av/g$a;


# static fields
.field private static a:I


# instance fields
.field private final b:Lcom/twitter/library/av/g;

.field private c:I

.field private d:J

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/model/av/DynamicAdId;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/16 v0, 0xc

    sput v0, Lcom/twitter/library/av/b;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 46
    invoke-static {p1}, Lcom/twitter/library/av/g;->a(Landroid/content/Context;)Lcom/twitter/library/av/g;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/library/av/b;-><init>(Lcom/twitter/library/av/g;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/av/g;)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    sget v0, Lcom/twitter/library/av/b;->a:I

    iput v0, p0, Lcom/twitter/library/av/b;->c:I

    .line 42
    const-wide/16 v0, 0x7530

    iput-wide v0, p0, Lcom/twitter/library/av/b;->d:J

    .line 43
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/library/av/b;->e:Ljava/util/Map;

    .line 50
    iput-object p1, p0, Lcom/twitter/library/av/b;->b:Lcom/twitter/library/av/g;

    .line 51
    iget-object v0, p0, Lcom/twitter/library/av/b;->b:Lcom/twitter/library/av/g;

    invoke-virtual {v0, p0}, Lcom/twitter/library/av/g;->a(Lcom/twitter/library/av/g$a;)Z

    .line 52
    return-void
.end method

.method private a(ILcom/twitter/library/av/p;Ljava/util/Map;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/twitter/library/av/p;",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/model/av/DynamicAdId;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/av/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 163
    invoke-interface {p2}, Lcom/twitter/library/av/p;->e()I

    move-result v0

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    .line 172
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    invoke-interface {p2, p1}, Lcom/twitter/library/av/p;->a(I)V

    .line 167
    invoke-interface {p2}, Lcom/twitter/library/av/p;->b()Lcom/twitter/library/av/a;

    move-result-object v0

    .line 168
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/twitter/library/av/a;->a:Lcom/twitter/model/av/DynamicAdId;

    invoke-direct {p0, v1}, Lcom/twitter/library/av/b;->a(Lcom/twitter/model/av/DynamicAdId;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {p2}, Lcom/twitter/library/av/p;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 169
    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    iget-object v0, v0, Lcom/twitter/library/av/a;->a:Lcom/twitter/model/av/DynamicAdId;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/av/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 192
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/a;

    .line 193
    iget-object v2, p0, Lcom/twitter/library/av/b;->e:Ljava/util/Map;

    iget-object v0, v0, Lcom/twitter/library/av/a;->a:Lcom/twitter/model/av/DynamicAdId;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 195
    :cond_0
    return-void
.end method

.method private a(Lcom/twitter/model/av/DynamicAdId;)Z
    .locals 4

    .prologue
    .line 175
    iget-object v0, p0, Lcom/twitter/library/av/b;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 176
    if-eqz v0, :cond_0

    .line 177
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v2, v0

    iget-wide v2, p0, Lcom/twitter/library/av/b;->d:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    .line 176
    :goto_0
    return v0

    .line 177
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 6

    .prologue
    .line 181
    iget-object v0, p0, Lcom/twitter/library/av/b;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 182
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 184
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 185
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    iget-wide v4, p0, Lcom/twitter/library/av/b;->d:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 186
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 189
    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/library/av/b;->b:Lcom/twitter/library/av/g;

    invoke-virtual {v0, p0}, Lcom/twitter/library/av/g;->b(Lcom/twitter/library/av/g$a;)Z

    .line 60
    return-void
.end method

.method public a(Lcom/twitter/library/av/p;I)V
    .locals 5

    .prologue
    .line 110
    invoke-interface {p1}, Lcom/twitter/library/av/p;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    :goto_0
    return-void

    .line 116
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 117
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 118
    invoke-interface {p1}, Lcom/twitter/library/av/p;->a()I

    move-result v2

    .line 121
    iget-object v0, p0, Lcom/twitter/library/av/b;->b:Lcom/twitter/library/av/g;

    invoke-virtual {v0}, Lcom/twitter/library/av/g;->a()Ljava/util/List;

    move-result-object v0

    .line 122
    invoke-direct {p0, v0}, Lcom/twitter/library/av/b;->a(Ljava/util/List;)V

    .line 126
    iget-object v0, p0, Lcom/twitter/library/av/b;->e:Ljava/util/Map;

    invoke-direct {p0, p2, p1, v0, v1}, Lcom/twitter/library/av/b;->a(ILcom/twitter/library/av/p;Ljava/util/Map;Ljava/util/List;)V

    .line 127
    const/4 v0, 0x1

    :goto_1
    iget v3, p0, Lcom/twitter/library/av/b;->c:I

    if-gt v0, v3, :cond_1

    .line 128
    add-int v3, p2, v0

    iget-object v4, p0, Lcom/twitter/library/av/b;->e:Ljava/util/Map;

    invoke-direct {p0, v3, p1, v4, v1}, Lcom/twitter/library/av/b;->a(ILcom/twitter/library/av/p;Ljava/util/Map;Ljava/util/List;)V

    .line 129
    sub-int v3, p2, v0

    iget-object v4, p0, Lcom/twitter/library/av/b;->e:Ljava/util/Map;

    invoke-direct {p0, v3, p1, v4, v1}, Lcom/twitter/library/av/b;->a(ILcom/twitter/library/av/p;Ljava/util/Map;Ljava/util/List;)V

    .line 127
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 133
    :cond_1
    invoke-interface {p1, v2}, Lcom/twitter/library/av/p;->a(I)V

    .line 135
    invoke-direct {p0}, Lcom/twitter/library/av/b;->c()V

    .line 139
    iget-object v0, p0, Lcom/twitter/library/av/b;->b:Lcom/twitter/library/av/g;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/g;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/av/DynamicAdId;Lcom/twitter/model/av/DynamicAdInfo;)V
    .locals 0

    .prologue
    .line 203
    return-void
.end method

.method public bo_()V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/twitter/library/av/b;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 200
    return-void
.end method
