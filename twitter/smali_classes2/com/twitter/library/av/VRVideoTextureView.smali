.class public Lcom/twitter/library/av/VRVideoTextureView;
.super Lcom/twitter/library/av/SmoothPlaybackVideoTextureView;
.source "Twttr"


# instance fields
.field private i:Ltv/periscope/android/graphics/b;

.field private j:Landroid/view/Surface;

.field private k:Landroid/graphics/SurfaceTexture;

.field private l:Landroid/graphics/SurfaceTexture;

.field private m:Ltv/periscope/android/graphics/i;

.field private n:Ltv/periscope/android/graphics/g;

.field private o:Ltv/periscope/android/graphics/n;

.field private p:Ltv/periscope/android/util/Size;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/n$c;Lcom/twitter/library/av/z;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/library/av/SmoothPlaybackVideoTextureView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/n$c;Lcom/twitter/library/av/z;)V

    .line 37
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/av/VRVideoTextureView;)Landroid/graphics/SurfaceTexture;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->l:Landroid/graphics/SurfaceTexture;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/library/av/VRVideoTextureView;Ltv/periscope/android/graphics/i;)Ltv/periscope/android/graphics/i;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lcom/twitter/library/av/VRVideoTextureView;->m:Ltv/periscope/android/graphics/i;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/library/av/VRVideoTextureView;Ltv/periscope/android/graphics/n;)Ltv/periscope/android/graphics/n;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lcom/twitter/library/av/VRVideoTextureView;->o:Ltv/periscope/android/graphics/n;

    return-object p1
.end method

.method static synthetic b(Lcom/twitter/library/av/VRVideoTextureView;)Ltv/periscope/android/graphics/n;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->o:Ltv/periscope/android/graphics/n;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/library/av/VRVideoTextureView;)Ltv/periscope/android/util/Size;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->p:Ltv/periscope/android/util/Size;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/library/av/VRVideoTextureView;)Ltv/periscope/android/graphics/i;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->m:Ltv/periscope/android/graphics/i;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->k:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->l:Landroid/graphics/SurfaceTexture;

    if-nez v0, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->n:Ltv/periscope/android/graphics/g;

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Ltv/periscope/android/graphics/b;

    invoke-direct {v0}, Ltv/periscope/android/graphics/b;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->i:Ltv/periscope/android/graphics/b;

    .line 87
    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, Lcom/twitter/library/av/VRVideoTextureView;->k:Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->j:Landroid/view/Surface;

    .line 88
    iget-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->i:Ltv/periscope/android/graphics/b;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/library/av/VRVideoTextureView;->j:Landroid/view/Surface;

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/graphics/b;->a(Ltv/periscope/android/graphics/b;Landroid/view/Surface;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 90
    iget-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->l:Landroid/graphics/SurfaceTexture;

    invoke-super {p0, v0}, Lcom/twitter/library/av/SmoothPlaybackVideoTextureView;->setSurfaceTexture(Landroid/graphics/SurfaceTexture;)V

    .line 91
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "unable to create OpenGL context"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 95
    :cond_2
    iget-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->i:Ltv/periscope/android/graphics/b;

    new-instance v1, Lcom/twitter/library/av/VRVideoTextureView$1;

    invoke-direct {v1, p0}, Lcom/twitter/library/av/VRVideoTextureView$1;-><init>(Lcom/twitter/library/av/VRVideoTextureView;)V

    invoke-virtual {v0, v1}, Ltv/periscope/android/graphics/b;->a(Ltv/periscope/android/graphics/b$c;)Z

    .line 110
    new-instance v0, Ltv/periscope/android/graphics/g;

    iget-object v1, p0, Lcom/twitter/library/av/VRVideoTextureView;->i:Ltv/periscope/android/graphics/b;

    new-instance v2, Lcom/twitter/library/av/VRVideoTextureView$2;

    invoke-direct {v2, p0}, Lcom/twitter/library/av/VRVideoTextureView$2;-><init>(Lcom/twitter/library/av/VRVideoTextureView;)V

    invoke-direct {v0, v1, v2}, Ltv/periscope/android/graphics/g;-><init>(Ltv/periscope/android/graphics/b;Ltv/periscope/android/graphics/g$b;)V

    iput-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->n:Ltv/periscope/android/graphics/g;

    .line 123
    iget-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->n:Ltv/periscope/android/graphics/g;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/g;->a()V

    goto :goto_0
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 128
    iget-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->n:Ltv/periscope/android/graphics/g;

    if-eqz v0, :cond_1

    .line 129
    iget-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->n:Ltv/periscope/android/graphics/g;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/g;->b()V

    .line 130
    iget-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->i:Ltv/periscope/android/graphics/b;

    new-instance v1, Lcom/twitter/library/av/VRVideoTextureView$3;

    invoke-direct {v1, p0}, Lcom/twitter/library/av/VRVideoTextureView$3;-><init>(Lcom/twitter/library/av/VRVideoTextureView;)V

    invoke-virtual {v0, v1}, Ltv/periscope/android/graphics/b;->a(Ltv/periscope/android/graphics/b$c;)Z

    .line 144
    iget-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->j:Landroid/view/Surface;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->j:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 146
    iput-object v2, p0, Lcom/twitter/library/av/VRVideoTextureView;->j:Landroid/view/Surface;

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->i:Ltv/periscope/android/graphics/b;

    invoke-virtual {v0}, Ltv/periscope/android/graphics/b;->b()V

    .line 149
    iput-object v2, p0, Lcom/twitter/library/av/VRVideoTextureView;->i:Ltv/periscope/android/graphics/b;

    .line 150
    iput-object v2, p0, Lcom/twitter/library/av/VRVideoTextureView;->n:Ltv/periscope/android/graphics/g;

    .line 152
    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 50
    invoke-static {p2, p3}, Ltv/periscope/android/util/Size;->a(II)Ltv/periscope/android/util/Size;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->p:Ltv/periscope/android/util/Size;

    .line 51
    iget-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->k:Landroid/graphics/SurfaceTexture;

    if-eq v0, p1, :cond_0

    .line 52
    iput-object p1, p0, Lcom/twitter/library/av/VRVideoTextureView;->k:Landroid/graphics/SurfaceTexture;

    .line 53
    invoke-direct {p0}, Lcom/twitter/library/av/VRVideoTextureView;->e()V

    .line 54
    invoke-direct {p0}, Lcom/twitter/library/av/VRVideoTextureView;->d()V

    .line 56
    :cond_0
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/twitter/library/av/VRVideoTextureView;->e()V

    .line 62
    iget-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->l:Landroid/graphics/SurfaceTexture;

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 67
    invoke-static {p2, p3}, Ltv/periscope/android/util/Size;->a(II)Ltv/periscope/android/util/Size;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->p:Ltv/periscope/android/util/Size;

    .line 68
    return-void
.end method

.method public setSurfaceTexture(Landroid/graphics/SurfaceTexture;)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/library/av/VRVideoTextureView;->l:Landroid/graphics/SurfaceTexture;

    if-eq v0, p1, :cond_0

    .line 42
    iput-object p1, p0, Lcom/twitter/library/av/VRVideoTextureView;->l:Landroid/graphics/SurfaceTexture;

    .line 43
    invoke-direct {p0}, Lcom/twitter/library/av/VRVideoTextureView;->e()V

    .line 44
    invoke-direct {p0}, Lcom/twitter/library/av/VRVideoTextureView;->d()V

    .line 46
    :cond_0
    return-void
.end method

.method public setTransform(Landroid/graphics/Matrix;)V
    .locals 0

    .prologue
    .line 78
    return-void
.end method
