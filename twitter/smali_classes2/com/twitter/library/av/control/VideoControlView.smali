.class public Lcom/twitter/library/av/control/VideoControlView;
.super Landroid/widget/RelativeLayout;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/library/av/control/a$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/control/VideoControlView$a;
    }
.end annotation


# instance fields
.field a:Lcom/twitter/library/av/playback/AVPlayer;

.field private final b:Landroid/view/View;

.field private final c:Landroid/view/View;

.field private final d:Landroid/view/View;

.field private final e:Landroid/widget/ImageButton;

.field private final f:Landroid/widget/ImageButton;

.field private final g:Landroid/view/View;

.field private h:Landroid/widget/TextView;

.field private i:Z

.field private j:Z

.field private final k:Lcom/twitter/library/av/control/a;

.field private l:Lcom/twitter/library/av/control/VideoControlView$a;

.field private final m:Z

.field private final n:Lcom/twitter/library/av/control/b;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayer;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 62
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 40
    iput-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->j:Z

    .line 63
    invoke-virtual {p0, v0}, Lcom/twitter/library/av/control/VideoControlView;->setWillNotDraw(Z)V

    .line 64
    invoke-virtual {p0, v1}, Lcom/twitter/library/av/control/VideoControlView;->setFocusable(Z)V

    .line 65
    invoke-virtual {p0, v1}, Lcom/twitter/library/av/control/VideoControlView;->setFocusableInTouchMode(Z)V

    .line 66
    invoke-static {}, Lcom/twitter/library/av/control/c;->b()Lcom/twitter/library/av/control/b;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->n:Lcom/twitter/library/av/control/b;

    .line 67
    invoke-static {}, Lcom/twitter/android/av/ai;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->m:Z

    .line 69
    const-string/jumbo v0, "layout_inflater"

    .line 70
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 72
    iget-boolean v1, p0, Lcom/twitter/library/av/control/VideoControlView;->m:Z

    if-eqz v1, :cond_0

    .line 73
    sget v1, Lazw$h;->av_media_view_count_controller:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->c:Landroid/view/View;

    .line 74
    iget-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->c:Landroid/view/View;

    sget v2, Lazw$g;->view_count:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->h:Landroid/widget/TextView;

    .line 78
    :goto_0
    iget-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->c:Landroid/view/View;

    sget v2, Lazw$g;->av_media_controller_controls:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->b:Landroid/view/View;

    .line 79
    new-instance v1, Lcom/twitter/library/av/control/a;

    iget-object v2, p0, Lcom/twitter/library/av/control/VideoControlView;->c:Landroid/view/View;

    invoke-direct {v1, v2, p0}, Lcom/twitter/library/av/control/a;-><init>(Landroid/view/View;Lcom/twitter/library/av/control/a$a;)V

    iput-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->k:Lcom/twitter/library/av/control/a;

    .line 81
    iget-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->c:Landroid/view/View;

    sget v2, Lazw$g;->pause:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->e:Landroid/widget/ImageButton;

    .line 82
    iget-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->e:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 83
    iget-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->e:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    iget-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->c:Landroid/view/View;

    sget v2, Lazw$g;->fullscreen:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->f:Landroid/widget/ImageButton;

    .line 86
    iget-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->f:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iget-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->c:Landroid/view/View;

    sget v2, Lazw$g;->skip:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->g:Landroid/view/View;

    .line 89
    iget-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->g:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    sget v1, Lazw$h;->av_error_msg:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->d:Landroid/view/View;

    .line 92
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->d:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/control/VideoControlView;->addView(Landroid/view/View;)V

    .line 94
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->c:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/control/VideoControlView;->addView(Landroid/view/View;)V

    .line 95
    invoke-virtual {p0, p2}, Lcom/twitter/library/av/control/VideoControlView;->a(Lcom/twitter/library/av/playback/AVPlayer;)V

    .line 96
    return-void

    .line 76
    :cond_0
    sget v1, Lazw$h;->av_media_controller:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->c:Landroid/view/View;

    goto :goto_0
.end method

.method constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/av/control/VideoControlView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayer;Z)V

    .line 52
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/av/control/VideoControlView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->d:Landroid/view/View;

    return-object v0
.end method

.method private b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 248
    new-instance v0, Lcom/twitter/library/av/control/VideoControlView$1;

    invoke-direct {v0, p0, p2, p1}, Lcom/twitter/library/av/control/VideoControlView$1;-><init>(Lcom/twitter/library/av/control/VideoControlView;Ljava/lang/String;Landroid/content/Context;)V

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/twitter/library/av/control/VideoControlView;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 117
    invoke-virtual {p0}, Lcom/twitter/library/av/control/VideoControlView;->g()V

    .line 119
    :cond_0
    return-void
.end method

.method private k()V
    .locals 5

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->e()Lcom/twitter/library/av/playback/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->p()J

    move-result-wide v0

    .line 128
    invoke-static {v0, v1}, Lcom/twitter/android/av/ai;->a(J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 129
    iget-object v2, p0, Lcom/twitter/library/av/control/VideoControlView;->h:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/library/av/control/VideoControlView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/library/av/control/VideoControlView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v3, v4, v0, v1}, Lcom/twitter/android/av/ai;->a(Landroid/content/res/Resources;Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    :goto_0
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->h:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 135
    :cond_0
    return-void

    .line 131
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->h:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private l()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 138
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->y()Z

    move-result v0

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->j:Z

    .line 139
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    iput-boolean v1, p0, Lcom/twitter/library/av/control/VideoControlView;->j:Z

    .line 141
    invoke-virtual {p0}, Lcom/twitter/library/av/control/VideoControlView;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    invoke-direct {p0}, Lcom/twitter/library/av/control/VideoControlView;->m()V

    .line 143
    invoke-direct {p0}, Lcom/twitter/library/av/control/VideoControlView;->p()V

    .line 149
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->j:Z

    if-eqz v0, :cond_1

    .line 150
    iget-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->i:Z

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/control/VideoControlView;->a(Z)V

    .line 152
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 138
    goto :goto_0
.end method

.method private m()V
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    .line 217
    :goto_0
    if-eqz v0, :cond_0

    .line 218
    invoke-interface {v0}, Lcom/twitter/model/av/AVMedia;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->i:Z

    .line 219
    invoke-virtual {p0}, Lcom/twitter/library/av/control/VideoControlView;->g()V

    .line 221
    :cond_0
    return-void

    .line 216
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 309
    iget-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->i:Z

    if-eqz v0, :cond_1

    .line 310
    invoke-direct {p0}, Lcom/twitter/library/av/control/VideoControlView;->q()V

    .line 311
    iget-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->m:Z

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->h:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 317
    :cond_0
    :goto_0
    return-void

    .line 315
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->k:Lcom/twitter/library/av/control/a;

    invoke-virtual {v0}, Lcom/twitter/library/av/control/a;->c()V

    goto :goto_0
.end method

.method private o()V
    .locals 2

    .prologue
    .line 349
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    if-nez v0, :cond_0

    .line 358
    :goto_0
    return-void

    .line 354
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->k:Lcom/twitter/library/av/control/a;

    invoke-virtual {v0}, Lcom/twitter/library/av/control/a;->a()V

    .line 355
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->b(Z)V

    .line 356
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->j:Z

    .line 357
    invoke-virtual {p0}, Lcom/twitter/library/av/control/VideoControlView;->g()V

    goto :goto_0
.end method

.method private p()V
    .locals 3

    .prologue
    .line 364
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    if-nez v0, :cond_0

    .line 386
    :goto_0
    return-void

    .line 368
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->j:Z

    if-eqz v0, :cond_2

    .line 369
    iget-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->m:Z

    if-eqz v0, :cond_1

    .line 370
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->e:Landroid/widget/ImageButton;

    sget v1, Lazw$f;->ic_video_view_count_replay_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 374
    :goto_1
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->e:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/twitter/library/av/control/VideoControlView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lazw$k;->replay:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 372
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->e:Landroid/widget/ImageButton;

    sget v1, Lazw$f;->ic_video_replay_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_1

    .line 375
    :cond_2
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 376
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->e:Landroid/widget/ImageButton;

    sget v1, Lazw$f;->ic_video_pause_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 377
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->e:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/twitter/library/av/control/VideoControlView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lazw$k;->pause:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 379
    :cond_3
    iget-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->m:Z

    if-eqz v0, :cond_4

    .line 380
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->e:Landroid/widget/ImageButton;

    sget v1, Lazw$f;->ic_view_count_video_play_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 384
    :goto_2
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->e:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/twitter/library/av/control/VideoControlView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lazw$k;->play:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 382
    :cond_4
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->e:Landroid/widget/ImageButton;

    sget v1, Lazw$f;->ic_video_play_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_2
.end method

.method private q()V
    .locals 2

    .prologue
    .line 425
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->e:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 426
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->g:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 427
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->k:Lcom/twitter/library/av/control/a;

    invoke-virtual {v0}, Lcom/twitter/library/av/control/a;->d()V

    .line 428
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/twitter/library/av/control/VideoControlView;->requestLayout()V

    .line 156
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 230
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/av/control/VideoControlView;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v0

    .line 233
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 234
    iget-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->k:Lcom/twitter/library/av/control/a;

    invoke-virtual {v1, v0}, Lcom/twitter/library/av/control/a;->a(Ljava/lang/Runnable;)V

    .line 238
    :goto_0
    return-void

    .line 236
    :cond_0
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayer;)V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    if-ne p1, v0, :cond_0

    .line 112
    :goto_0
    return-void

    .line 106
    :cond_0
    iput-object p1, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    .line 107
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->k:Lcom/twitter/library/av/control/a;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/control/a;->a(Lcom/twitter/library/av/playback/AVPlayer;)V

    .line 109
    invoke-direct {p0}, Lcom/twitter/library/av/control/VideoControlView;->j()V

    .line 110
    invoke-direct {p0}, Lcom/twitter/library/av/control/VideoControlView;->l()V

    .line 111
    invoke-direct {p0}, Lcom/twitter/library/av/control/VideoControlView;->k()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/av/playback/aa;)V
    .locals 2

    .prologue
    .line 411
    iget-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->n:Lcom/twitter/library/av/control/b;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/control/b;->a(Lcom/twitter/library/av/playback/aa;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->g:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 413
    iget-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->m:Z

    if-eqz v0, :cond_0

    .line 414
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->h:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 417
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->k:Lcom/twitter/library/av/control/a;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/control/a;->a(Lcom/twitter/library/av/playback/aa;)V

    .line 418
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 181
    iput-boolean p1, p0, Lcom/twitter/library/av/control/VideoControlView;->i:Z

    .line 182
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->j:Z

    .line 183
    invoke-direct {p0}, Lcom/twitter/library/av/control/VideoControlView;->p()V

    .line 184
    return-void
.end method

.method public a(ZJ)V
    .locals 2

    .prologue
    .line 269
    if-eqz p1, :cond_0

    .line 270
    iget-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->j:Z

    if-eqz v0, :cond_0

    .line 271
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->j:Z

    .line 272
    invoke-virtual {p0}, Lcom/twitter/library/av/control/VideoControlView;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    invoke-direct {p0}, Lcom/twitter/library/av/control/VideoControlView;->m()V

    .line 277
    :cond_0
    invoke-direct {p0}, Lcom/twitter/library/av/control/VideoControlView;->p()V

    .line 279
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->l:Lcom/twitter/library/av/control/VideoControlView$a;

    if-eqz v0, :cond_1

    .line 280
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->l:Lcom/twitter/library/av/control/VideoControlView$a;

    invoke-interface {v0, p1, p2, p3}, Lcom/twitter/library/av/control/VideoControlView$a;->a(ZJ)V

    .line 282
    :cond_1
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->j:Z

    .line 209
    invoke-virtual {p0}, Lcom/twitter/library/av/control/VideoControlView;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    invoke-direct {p0}, Lcom/twitter/library/av/control/VideoControlView;->p()V

    .line 211
    invoke-direct {p0}, Lcom/twitter/library/av/control/VideoControlView;->m()V

    .line 213
    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 192
    iput-boolean p1, p0, Lcom/twitter/library/av/control/VideoControlView;->i:Z

    .line 194
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->k:Lcom/twitter/library/av/control/a;

    iget-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->A()Lcom/twitter/library/av/playback/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/control/a;->b(Lcom/twitter/library/av/playback/aa;)V

    .line 197
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    .line 323
    :goto_0
    if-eqz v0, :cond_0

    .line 324
    invoke-interface {v0}, Lcom/twitter/model/av/AVMedia;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->i:Z

    .line 325
    invoke-direct {p0}, Lcom/twitter/library/av/control/VideoControlView;->n()V

    .line 327
    :cond_0
    return-void

    .line 321
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Z)V
    .locals 3

    .prologue
    .line 477
    if-eqz p1, :cond_0

    .line 478
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->f:Landroid/widget/ImageButton;

    sget v1, Lazw$f;->ic_video_collapse_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 479
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->f:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/twitter/library/av/control/VideoControlView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lazw$k;->av_player_button_collapse:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 484
    :goto_0
    return-void

    .line 481
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->f:Landroid/widget/ImageButton;

    sget v1, Lazw$f;->ic_video_expand_btn:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 482
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->f:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/twitter/library/av/control/VideoControlView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lazw$k;->av_player_button_fullscreen:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->l:Lcom/twitter/library/av/control/VideoControlView$a;

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->l:Lcom/twitter/library/av/control/VideoControlView$a;

    invoke-interface {v0}, Lcom/twitter/library/av/control/VideoControlView$a;->z()V

    .line 296
    :cond_0
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->b:Landroid/view/View;

    invoke-static {v0}, Lcom/twitter/util/e;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    .line 289
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->e:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 303
    invoke-direct {p0}, Lcom/twitter/library/av/control/VideoControlView;->n()V

    .line 304
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->b:Landroid/view/View;

    invoke-static {v0}, Lcom/twitter/util/e;->c(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    .line 305
    invoke-direct {p0}, Lcom/twitter/library/av/control/VideoControlView;->p()V

    .line 306
    return-void
.end method

.method public getControlBarView()Landroid/view/View;
    .locals 1

    .prologue
    .line 435
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->b:Landroid/view/View;

    return-object v0
.end method

.method h()V
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    if-nez v0, :cond_0

    .line 346
    :goto_0
    return-void

    .line 337
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->j:Z

    if-eqz v0, :cond_1

    .line 338
    invoke-direct {p0}, Lcom/twitter/library/av/control/VideoControlView;->o()V

    .line 345
    :goto_1
    invoke-direct {p0}, Lcom/twitter/library/av/control/VideoControlView;->p()V

    goto :goto_0

    .line 339
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 340
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->s()V

    goto :goto_1

    .line 342
    :cond_2
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->b(Z)V

    goto :goto_1
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 160
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 161
    invoke-direct {p0}, Lcom/twitter/library/av/control/VideoControlView;->l()V

    .line 162
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 442
    iget-boolean v0, p0, Lcom/twitter/library/av/control/VideoControlView;->j:Z

    .line 443
    iget-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->e:Landroid/widget/ImageButton;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 444
    invoke-virtual {p0}, Lcom/twitter/library/av/control/VideoControlView;->h()V

    .line 445
    iget-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->l:Lcom/twitter/library/av/control/VideoControlView$a;

    if-eqz v1, :cond_0

    .line 446
    if-eqz v0, :cond_1

    .line 447
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->l:Lcom/twitter/library/av/control/VideoControlView$a;

    invoke-interface {v0}, Lcom/twitter/library/av/control/VideoControlView$a;->w()V

    .line 461
    :cond_0
    :goto_0
    return-void

    .line 449
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->l:Lcom/twitter/library/av/control/VideoControlView$a;

    invoke-interface {v0}, Lcom/twitter/library/av/control/VideoControlView$a;->y()V

    goto :goto_0

    .line 452
    :cond_2
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->g:Landroid/view/View;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 453
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    if-eqz v0, :cond_0

    .line 454
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->D()V

    goto :goto_0

    .line 456
    :cond_3
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->f:Landroid/widget/ImageButton;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 457
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->l:Lcom/twitter/library/av/control/VideoControlView$a;

    if-eqz v0, :cond_0

    .line 458
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->l:Lcom/twitter/library/av/control/VideoControlView$a;

    invoke-interface {v0}, Lcom/twitter/library/av/control/VideoControlView$a;->x()V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 166
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 169
    invoke-virtual {p0}, Lcom/twitter/library/av/control/VideoControlView;->getRight()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/library/av/control/VideoControlView;->getLeft()I

    move-result v1

    sub-int/2addr v0, v1

    .line 171
    invoke-virtual {p0}, Lcom/twitter/library/av/control/VideoControlView;->getBottom()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/library/av/control/VideoControlView;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    .line 172
    iget-object v2, p0, Lcom/twitter/library/av/control/VideoControlView;->c:Landroid/view/View;

    invoke-virtual {v2, v3, v3, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 173
    iget-object v2, p0, Lcom/twitter/library/av/control/VideoControlView;->d:Landroid/view/View;

    invoke-virtual {v2, v3, v3, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 174
    return-void
.end method

.method public setIsFullScreenToggleAllowed(Z)V
    .locals 2

    .prologue
    .line 200
    iget-object v1, p0, Lcom/twitter/library/av/control/VideoControlView;->f:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 201
    return-void

    .line 200
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setListener(Lcom/twitter/library/av/control/VideoControlView$a;)V
    .locals 0

    .prologue
    .line 469
    iput-object p1, p0, Lcom/twitter/library/av/control/VideoControlView;->l:Lcom/twitter/library/av/control/VideoControlView$a;

    .line 470
    return-void
.end method

.method protected setVideoControlsBackgroundTransparency(I)V
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/twitter/library/av/control/VideoControlView;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 401
    if-eqz v0, :cond_0

    .line 402
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 404
    :cond_0
    return-void
.end method
