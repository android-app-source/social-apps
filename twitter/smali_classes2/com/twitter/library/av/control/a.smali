.class public Lcom/twitter/library/av/control/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/control/a$a;
    }
.end annotation


# instance fields
.field final a:Lcom/twitter/library/av/control/a$a;

.field b:J

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/SeekBar;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private i:Z

.field private j:Z

.field private k:Lcom/twitter/library/av/playback/AVPlayer;

.field private final l:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/twitter/library/av/control/a$a;)V
    .locals 6

    .prologue
    .line 47
    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    new-instance v5, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v5, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/av/control/a;-><init>(Landroid/view/View;Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/library/av/control/a$a;Landroid/content/Context;Landroid/os/Handler;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/view/View;Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/library/av/control/a$a;)V
    .locals 6

    .prologue
    .line 55
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    new-instance v5, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v5, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/av/control/a;-><init>(Landroid/view/View;Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/library/av/control/a$a;Landroid/content/Context;Landroid/os/Handler;)V

    .line 56
    return-void
.end method

.method constructor <init>(Landroid/view/View;Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/library/av/control/a$a;Landroid/content/Context;Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/library/av/control/a;->b:J

    .line 68
    iput-object p5, p0, Lcom/twitter/library/av/control/a;->l:Landroid/os/Handler;

    .line 69
    iput-object p3, p0, Lcom/twitter/library/av/control/a;->a:Lcom/twitter/library/av/control/a$a;

    .line 70
    sget v0, Lazw$g;->mediacontroller_progress:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/twitter/library/av/control/a;->f:Landroid/widget/SeekBar;

    .line 71
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->f:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 72
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->f:Landroid/widget/SeekBar;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 73
    sget v0, Lazw$g;->time_current:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/library/av/control/a;->d:Landroid/widget/TextView;

    .line 74
    sget v0, Lazw$g;->time:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/library/av/control/a;->c:Landroid/widget/TextView;

    .line 75
    sget v0, Lazw$g;->countdown:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/library/av/control/a;->e:Landroid/widget/TextView;

    .line 76
    sget v0, Lazw$k;->av_preroll_countdown_text:I

    invoke-virtual {p4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/control/a;->g:Ljava/lang/String;

    .line 77
    sget v0, Lazw$k;->av_time_duration_text:I

    invoke-virtual {p4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/control/a;->h:Ljava/lang/String;

    .line 79
    invoke-virtual {p0, p2}, Lcom/twitter/library/av/control/a;->a(Lcom/twitter/library/av/playback/AVPlayer;)V

    .line 80
    return-void
.end method


# virtual methods
.method protected a(J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 244
    long-to-int v0, p1

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/twitter/util/aa;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 245
    invoke-static {}, Lcom/twitter/android/av/ai;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 246
    iget-object v1, p0, Lcom/twitter/library/av/control/a;->h:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 248
    :cond_0
    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->f:Landroid/widget/SeekBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 201
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayer;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->k:Lcom/twitter/library/av/playback/AVPlayer;

    if-ne p1, v0, :cond_0

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    iput-object p1, p0, Lcom/twitter/library/av/control/a;->k:Lcom/twitter/library/av/playback/AVPlayer;

    .line 90
    sget-object v0, Lcom/twitter/library/av/playback/aa;->a:Lcom/twitter/library/av/playback/aa;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/control/a;->b(Lcom/twitter/library/av/playback/aa;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/av/playback/aa;)V
    .locals 1

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/twitter/library/av/control/a;->i:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/av/control/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/control/a;->k:Lcom/twitter/library/av/playback/AVPlayer;

    if-eqz v0, :cond_0

    .line 163
    invoke-virtual {p0, p1}, Lcom/twitter/library/av/control/a;->b(Lcom/twitter/library/av/playback/aa;)V

    .line 167
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->a:Lcom/twitter/library/av/control/a$a;

    invoke-interface {v0}, Lcom/twitter/library/av/control/a$a;->c()V

    .line 169
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->l:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 99
    return-void
.end method

.method public b(Lcom/twitter/library/av/playback/aa;)V
    .locals 5

    .prologue
    const-wide/16 v0, 0x0

    .line 176
    iget-boolean v2, p0, Lcom/twitter/library/av/control/a;->i:Z

    if-eqz v2, :cond_1

    .line 194
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    iget-object v2, p0, Lcom/twitter/library/av/control/a;->f:Landroid/widget/SeekBar;

    if-eqz v2, :cond_3

    .line 181
    iget-wide v2, p1, Lcom/twitter/library/av/playback/aa;->c:J

    cmp-long v2, v2, v0

    if-lez v2, :cond_2

    const-wide/16 v0, 0x3e8

    iget-wide v2, p1, Lcom/twitter/library/av/playback/aa;->b:J

    mul-long/2addr v0, v2

    iget-wide v2, p1, Lcom/twitter/library/av/playback/aa;->c:J

    div-long/2addr v0, v2

    .line 182
    :cond_2
    iget-object v2, p0, Lcom/twitter/library/av/control/a;->f:Landroid/widget/SeekBar;

    long-to-int v0, v0

    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 185
    :cond_3
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->c:Landroid/widget/TextView;

    iget-wide v2, p1, Lcom/twitter/library/av/playback/aa;->c:J

    invoke-static {v2, v3}, Lcom/twitter/util/aa;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->d:Landroid/widget/TextView;

    iget-wide v2, p1, Lcom/twitter/library/av/playback/aa;->b:J

    invoke-virtual {p0, v2, v3}, Lcom/twitter/library/av/control/a;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 189
    iget-wide v0, p1, Lcom/twitter/library/av/playback/aa;->c:J

    iget-wide v2, p1, Lcom/twitter/library/av/playback/aa;->b:J

    sub-long/2addr v0, v2

    .line 190
    invoke-static {v0, v1}, Lcom/twitter/util/aa;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 192
    iget-object v1, p0, Lcom/twitter/library/av/control/a;->e:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/twitter/library/av/control/a;->g:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method b()Z
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->f:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 215
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 216
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 217
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->f:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 218
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 221
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 228
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 229
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 230
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->f:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 232
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->e:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 236
    :cond_0
    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 6

    .prologue
    .line 116
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->k:Lcom/twitter/library/av/playback/AVPlayer;

    if-eqz v0, :cond_0

    if-nez p3, :cond_1

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->k:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->A()Lcom/twitter/library/av/playback/aa;

    move-result-object v0

    .line 121
    iget-object v1, p0, Lcom/twitter/library/av/control/a;->k:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v1

    .line 122
    if-eqz v1, :cond_0

    .line 125
    iget-wide v2, v0, Lcom/twitter/library/av/playback/aa;->c:J

    int-to-long v4, p2

    mul-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 126
    iget-wide v0, v0, Lcom/twitter/library/av/playback/aa;->b:J

    cmp-long v0, v2, v0

    if-gez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/library/av/control/a;->j:Z

    .line 128
    iput-wide v2, p0, Lcom/twitter/library/av/control/a;->b:J

    .line 129
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->d:Landroid/widget/TextView;

    invoke-virtual {p0, v2, v3}, Lcom/twitter/library/av/control/a;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 126
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->k:Lcom/twitter/library/av/playback/AVPlayer;

    if-nez v0, :cond_0

    .line 112
    :goto_0
    return-void

    .line 107
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/av/control/a;->i:Z

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/av/control/a;->j:Z

    .line 110
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->k:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->L()V

    .line 111
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->a:Lcom/twitter/library/av/control/a$a;

    invoke-interface {v0}, Lcom/twitter/library/av/control/a$a;->d()V

    goto :goto_0
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    .line 136
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->k:Lcom/twitter/library/av/playback/AVPlayer;

    if-nez v0, :cond_1

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    iget-wide v0, p0, Lcom/twitter/library/av/control/a;->b:J

    .line 141
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/twitter/library/av/control/a;->i:Z

    .line 142
    iget-wide v2, p0, Lcom/twitter/library/av/control/a;->b:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_2

    .line 143
    iget-object v2, p0, Lcom/twitter/library/av/control/a;->k:Lcom/twitter/library/av/playback/AVPlayer;

    iget-wide v4, p0, Lcom/twitter/library/av/control/a;->b:J

    long-to-int v3, v4

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Lcom/twitter/library/av/playback/AVPlayer;->a(J)V

    .line 144
    iput-wide v6, p0, Lcom/twitter/library/av/control/a;->b:J

    .line 147
    :cond_2
    iget-object v2, p0, Lcom/twitter/library/av/control/a;->k:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v2}, Lcom/twitter/library/av/playback/AVPlayer;->A()Lcom/twitter/library/av/playback/aa;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/twitter/library/av/control/a;->b(Lcom/twitter/library/av/playback/aa;)V

    .line 149
    iget-object v2, p0, Lcom/twitter/library/av/control/a;->k:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v2}, Lcom/twitter/library/av/playback/AVPlayer;->M()V

    .line 150
    iget-object v2, p0, Lcom/twitter/library/av/control/a;->a:Lcom/twitter/library/av/control/a$a;

    iget-boolean v3, p0, Lcom/twitter/library/av/control/a;->j:Z

    invoke-interface {v2, v3, v0, v1}, Lcom/twitter/library/av/control/a$a;->a(ZJ)V

    .line 151
    iget-boolean v0, p0, Lcom/twitter/library/av/control/a;->j:Z

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/twitter/library/av/control/a;->k:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->B()V

    goto :goto_0
.end method
