.class public Lcom/twitter/library/av/LiveVideoPlayerView;
.super Lcom/twitter/library/av/VideoPlayerView;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/n$a;


# instance fields
.field private final i:Lcom/twitter/util/n;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/VideoPlayerView$Mode;)V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/twitter/library/av/w;

    invoke-direct {v0}, Lcom/twitter/library/av/w;-><init>()V

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/twitter/library/av/VideoPlayerView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/ag;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    .line 31
    new-instance v0, Lcom/twitter/util/n;

    invoke-direct {v0, p1}, Lcom/twitter/util/n;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/library/av/LiveVideoPlayerView;->i:Lcom/twitter/util/n;

    .line 32
    return-void
.end method

.method private s()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/library/av/LiveVideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    if-nez v0, :cond_0

    .line 57
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/LiveVideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    new-instance v0, Lbjs;

    iget-object v1, p0, Lcom/twitter/library/av/LiveVideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v1

    invoke-direct {v0, v1}, Lbjs;-><init>(Lcom/twitter/model/av/AVMedia;)V

    .line 56
    :goto_1
    iget-object v1, p0, Lcom/twitter/library/av/LiveVideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->h()Lbix;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbix;->a(Lbiw;)V

    goto :goto_0

    .line 54
    :cond_1
    new-instance v0, Lbjr;

    iget-object v1, p0, Lcom/twitter/library/av/LiveVideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v1

    invoke-direct {v0, v1}, Lbjr;-><init>(Lcom/twitter/model/av/AVMedia;)V

    goto :goto_1
.end method

.method private t()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 60
    iget-object v1, p0, Lcom/twitter/library/av/LiveVideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v1

    if-nez v1, :cond_0

    .line 73
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-object v1, p0, Lcom/twitter/library/av/LiveVideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 66
    new-instance v1, Lbjv;

    iget-object v2, p0, Lcom/twitter/library/av/LiveVideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v2}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lbjv;-><init>(Lcom/twitter/model/av/AVMedia;Z)V

    move-object v0, v1

    .line 72
    :goto_1
    iget-object v1, p0, Lcom/twitter/library/av/LiveVideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->h()Lbix;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbix;->a(Lbiw;)V

    goto :goto_0

    .line 69
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/av/LiveVideoPlayerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 70
    :goto_2
    new-instance v1, Lbjv;

    iget-object v2, p0, Lcom/twitter/library/av/LiveVideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v2}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lbjv;-><init>(Lcom/twitter/model/av/AVMedia;Z)V

    move-object v0, v1

    goto :goto_1

    .line 69
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public a(IIZZ)V
    .locals 0

    .prologue
    .line 36
    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/library/av/VideoPlayerView;->a(IIZZ)V

    .line 37
    invoke-direct {p0}, Lcom/twitter/library/av/LiveVideoPlayerView;->s()V

    .line 38
    invoke-direct {p0}, Lcom/twitter/library/av/LiveVideoPlayerView;->t()V

    .line 39
    return-void
.end method

.method public e_(I)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/twitter/library/av/LiveVideoPlayerView;->t()V

    .line 44
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Lcom/twitter/library/av/VideoPlayerView;->onAttachedToWindow()V

    .line 86
    iget-object v0, p0, Lcom/twitter/library/av/LiveVideoPlayerView;->i:Lcom/twitter/util/n;

    invoke-virtual {v0}, Lcom/twitter/util/n;->a()V

    .line 87
    iget-object v0, p0, Lcom/twitter/library/av/LiveVideoPlayerView;->i:Lcom/twitter/util/n;

    invoke-virtual {v0, p0}, Lcom/twitter/util/n;->a(Lcom/twitter/util/n$a;)V

    .line 88
    invoke-direct {p0}, Lcom/twitter/library/av/LiveVideoPlayerView;->s()V

    .line 89
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 77
    invoke-super {p0}, Lcom/twitter/library/av/VideoPlayerView;->onDetachedFromWindow()V

    .line 78
    iget-object v0, p0, Lcom/twitter/library/av/LiveVideoPlayerView;->i:Lcom/twitter/util/n;

    invoke-virtual {v0}, Lcom/twitter/util/n;->b()V

    .line 79
    iget-object v0, p0, Lcom/twitter/library/av/LiveVideoPlayerView;->i:Lcom/twitter/util/n;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/util/n;->a(Lcom/twitter/util/n$a;)V

    .line 80
    invoke-direct {p0}, Lcom/twitter/library/av/LiveVideoPlayerView;->s()V

    .line 81
    return-void
.end method
