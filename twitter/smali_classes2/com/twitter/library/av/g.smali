.class public Lcom/twitter/library/av/g;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/g$b;,
        Lcom/twitter/library/av/g$a;
    }
.end annotation


# static fields
.field private static a:Lcom/twitter/library/av/g;


# instance fields
.field private final b:Lcom/twitter/library/av/t;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/model/av/DynamicAdId;",
            "Lcom/twitter/model/av/DynamicAdInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/library/av/g$a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/model/av/DynamicAdId;",
            "Lcom/twitter/model/av/DynamicAdInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/library/av/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 128
    new-instance v2, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    new-instance v3, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    new-instance v5, Lcom/twitter/library/av/g$b;

    invoke-direct {v5}, Lcom/twitter/library/av/g$b;-><init>()V

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/av/g;-><init>(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;Lcom/twitter/library/av/c;)V

    .line 131
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;Lcom/twitter/library/av/c;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/model/av/DynamicAdId;",
            "Lcom/twitter/model/av/DynamicAdInfo;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/model/av/DynamicAdId;",
            "Lcom/twitter/model/av/DynamicAdInfo;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/library/av/a;",
            ">;",
            "Lcom/twitter/library/av/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 139
    new-instance v5, Lcom/twitter/library/av/u;

    invoke-direct {v5}, Lcom/twitter/library/av/u;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/av/g;-><init>(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;Lcom/twitter/library/av/u;Lcom/twitter/library/av/c;)V

    .line 141
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;Lcom/twitter/library/av/u;Lcom/twitter/library/av/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/model/av/DynamicAdId;",
            "Lcom/twitter/model/av/DynamicAdInfo;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/model/av/DynamicAdId;",
            "Lcom/twitter/model/av/DynamicAdInfo;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/library/av/a;",
            ">;",
            "Lcom/twitter/library/av/u;",
            "Lcom/twitter/library/av/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 146
    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    .line 50
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/av/g;->d:Ljava/util/List;

    .line 147
    iput-object p2, p0, Lcom/twitter/library/av/g;->c:Ljava/util/Map;

    .line 148
    iput-object p3, p0, Lcom/twitter/library/av/g;->e:Ljava/util/Map;

    .line 149
    iput-object p4, p0, Lcom/twitter/library/av/g;->f:Ljava/util/Set;

    .line 151
    invoke-static {p0, p2, p3}, Lcom/twitter/library/av/g;->a(Lcom/twitter/library/av/g;Ljava/util/Map;Ljava/util/Map;)Lcom/twitter/library/av/t$a;

    move-result-object v0

    .line 150
    invoke-virtual {p5, p1, v0, p6}, Lcom/twitter/library/av/u;->a(Landroid/content/Context;Lcom/twitter/library/av/t$a;Lcom/twitter/library/av/c;)Lcom/twitter/library/av/t;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/g;->b:Lcom/twitter/library/av/t;

    .line 152
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/library/av/g;
    .locals 2

    .prologue
    .line 67
    const-class v1, Lcom/twitter/library/av/g;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/av/g;->a:Lcom/twitter/library/av/g;

    if-nez v0, :cond_0

    .line 68
    new-instance v0, Lcom/twitter/library/av/g;

    invoke-direct {v0, p0}, Lcom/twitter/library/av/g;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/library/av/g;->a:Lcom/twitter/library/av/g;

    .line 69
    const-class v0, Lcom/twitter/library/av/g;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 72
    :cond_0
    sget-object v0, Lcom/twitter/library/av/g;->a:Lcom/twitter/library/av/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected static a(Lcom/twitter/library/av/g;Ljava/util/Map;Ljava/util/Map;)Lcom/twitter/library/av/t$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/av/g;",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/model/av/DynamicAdId;",
            "Lcom/twitter/model/av/DynamicAdInfo;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/model/av/DynamicAdId;",
            "Lcom/twitter/model/av/DynamicAdInfo;",
            ">;)",
            "Lcom/twitter/library/av/t$a;"
        }
    .end annotation

    .prologue
    .line 175
    new-instance v0, Lcom/twitter/library/av/g$1;

    invoke-direct {v0, p2, p1, p0}, Lcom/twitter/library/av/g$1;-><init>(Ljava/util/Map;Ljava/util/Map;Lcom/twitter/library/av/g;)V

    return-object v0
.end method

.method private a(Lcom/twitter/model/av/DynamicAdId;)Lcom/twitter/model/av/DynamicAdInfo;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p1, Lcom/twitter/model/av/DynamicAdId;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/g;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/av/DynamicAdInfo;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/g;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/av/DynamicAdInfo;

    goto :goto_0
.end method

.method private a(Lcom/twitter/model/av/AVMediaPlaylist;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 270
    invoke-static {p1}, Lcom/twitter/model/av/f;->a(Lcom/twitter/model/av/AVMediaPlaylist;)Lcom/twitter/model/av/DynamicAd;

    move-result-object v0

    .line 271
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/model/av/DynamicAd;->a:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/model/av/DynamicAdInfo;
    .locals 2

    .prologue
    .line 156
    invoke-interface {p1}, Lcom/twitter/library/av/playback/AVDataSource;->m()Lcom/twitter/library/av/playback/ah;

    move-result-object v0

    .line 157
    invoke-interface {v0}, Lcom/twitter/library/av/playback/ah;->b()Lcom/twitter/model/av/DynamicAdId;

    move-result-object v0

    .line 158
    invoke-virtual {v0}, Lcom/twitter/model/av/DynamicAdId;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 159
    invoke-direct {p0, v0}, Lcom/twitter/library/av/g;->a(Lcom/twitter/model/av/DynamicAdId;)Lcom/twitter/model/av/DynamicAdInfo;

    move-result-object v0

    .line 162
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/av/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309
    iget-object v0, p0, Lcom/twitter/library/av/g;->b:Lcom/twitter/library/av/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/t;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/av/playback/u;Lcom/twitter/model/av/AVMedia;Lcom/twitter/model/av/AVMediaPlaylist;)V
    .locals 3

    .prologue
    .line 232
    invoke-virtual {p1}, Lcom/twitter/library/av/playback/u;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 234
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/twitter/model/av/AVMediaPlaylist;->k()Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    .line 236
    :goto_0
    if-ne v0, p2, :cond_3

    invoke-virtual {p0, v0, p3}, Lcom/twitter/library/av/g;->a(Lcom/twitter/model/av/AVMedia;Lcom/twitter/model/av/AVMediaPlaylist;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 239
    iget-object v0, p0, Lcom/twitter/library/av/g;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 241
    iget-object v0, p0, Lcom/twitter/library/av/g;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 242
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/g$a;

    .line 243
    if-eqz v0, :cond_0

    .line 244
    invoke-interface {v0}, Lcom/twitter/library/av/g$a;->bo_()V

    goto :goto_1

    .line 234
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 248
    :cond_2
    monitor-enter p0

    .line 250
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/av/g;->b:Lcom/twitter/library/av/t;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/twitter/library/av/g;->f:Ljava/util/Set;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 251
    invoke-static {p3}, Lcom/twitter/model/av/f;->a(Lcom/twitter/model/av/AVMediaPlaylist;)Lcom/twitter/model/av/DynamicAd;

    move-result-object v2

    .line 250
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/av/t;->a(Ljava/util/List;Lcom/twitter/model/av/DynamicAd;)V

    .line 252
    monitor-exit p0

    .line 255
    :cond_3
    return-void

    .line 252
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(Lcom/twitter/model/av/DynamicAdId;Lcom/twitter/model/av/DynamicAdInfo;)V
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/twitter/library/av/g;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 206
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/g$a;

    .line 207
    if-eqz v0, :cond_0

    .line 208
    invoke-interface {v0, p1, p2}, Lcom/twitter/library/av/g$a;->a(Lcom/twitter/model/av/DynamicAdId;Lcom/twitter/model/av/DynamicAdInfo;)V

    goto :goto_0

    .line 211
    :cond_1
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/av/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 281
    monitor-enter p0

    .line 282
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/twitter/library/av/g;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 285
    iget-object v0, p0, Lcom/twitter/library/av/g;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 286
    iget-object v0, p0, Lcom/twitter/library/av/g;->b:Lcom/twitter/library/av/t;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/twitter/library/av/g;->f:Ljava/util/Set;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/t;->a(Ljava/util/List;)V

    .line 288
    :cond_0
    monitor-exit p0

    .line 289
    return-void

    .line 288
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/twitter/library/av/g$a;)Z
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/library/av/g;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 96
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 97
    const/4 v0, 0x0

    .line 101
    :goto_0
    return v0

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/av/g;->d:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/av/AVMedia;Lcom/twitter/model/av/AVMediaPlaylist;)Z
    .locals 2

    .prologue
    .line 265
    invoke-direct {p0, p2}, Lcom/twitter/library/av/g;->a(Lcom/twitter/model/av/AVMediaPlaylist;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/twitter/model/av/AVMedia;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b(Lcom/twitter/library/av/g$a;)Z
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/twitter/library/av/g;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 112
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/g$a;

    .line 114
    if-ne p1, v0, :cond_0

    .line 115
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 116
    const/4 v0, 0x1

    .line 119
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
