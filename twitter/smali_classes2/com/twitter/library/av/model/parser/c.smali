.class public Lcom/twitter/library/av/model/parser/c;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/model/parser/c$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/library/av/model/parser/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lcom/twitter/library/av/model/parser/c;->a()Lcom/twitter/library/av/model/parser/c;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/av/model/parser/c;->a:Lcom/twitter/library/av/model/parser/c;

    .line 26
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a()Lcom/twitter/library/av/model/parser/c;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 37
    new-instance v0, Lcom/twitter/library/av/model/parser/c;

    invoke-direct {v0}, Lcom/twitter/library/av/model/parser/c;-><init>()V

    return-object v0
.end method

.method private a(Lcom/twitter/model/core/p;)Z
    .locals 1

    .prologue
    .line 76
    if-eqz p1, :cond_0

    const-string/jumbo v0, "video_variant_v1_android_5413"

    invoke-static {v0}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    invoke-static {p1}, Lcom/twitter/library/av/model/parser/c;->d(Lcom/twitter/model/core/p;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 76
    :goto_0
    return v0

    .line 77
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/twitter/model/core/p;)Lcom/twitter/model/core/p;
    .locals 6

    .prologue
    .line 83
    :try_start_0
    new-instance v5, Ljava/net/URI;

    iget-object v0, p1, Lcom/twitter/model/core/p;->c:Ljava/lang/String;

    invoke-direct {v5, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 84
    invoke-virtual {v5}, Ljava/net/URI;->getQuery()Ljava/lang/String;

    move-result-object v0

    .line 85
    if-nez v0, :cond_0

    .line 86
    const-string/jumbo v4, "useV1Variant=true"

    .line 90
    :goto_0
    new-instance v0, Ljava/net/URI;

    .line 91
    invoke-virtual {v5}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5}, Ljava/net/URI;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 92
    invoke-virtual {v5}, Ljava/net/URI;->getFragment()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    new-instance v1, Lcom/twitter/model/core/p;

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p1, Lcom/twitter/model/core/p;->d:Ljava/lang/String;

    iget v3, p1, Lcom/twitter/model/core/p;->b:I

    iget-object v4, p1, Lcom/twitter/model/core/p;->e:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/twitter/model/core/p;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    move-object p1, v1

    .line 96
    :goto_1
    return-object p1

    .line 88
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "useV1Variant=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 95
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private static c(Lcom/twitter/model/core/p;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 101
    if-nez p0, :cond_1

    .line 111
    :cond_0
    :goto_0
    return v0

    .line 105
    :cond_1
    iget-object v1, p0, Lcom/twitter/model/core/p;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/model/core/p;->d:Ljava/lang/String;

    .line 106
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/twitter/model/util/c;->a:Ljava/util/List;

    iget-object v2, p0, Lcom/twitter/model/core/p;->d:Ljava/lang/String;

    .line 107
    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 111
    iget-object v0, p0, Lcom/twitter/model/core/p;->e:Ljava/lang/String;

    invoke-static {v0}, Lbiv;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private static d(Lcom/twitter/model/core/p;)Z
    .locals 2

    .prologue
    .line 115
    const-string/jumbo v0, "application/x-mpegURL"

    iget-object v1, p0, Lcom/twitter/model/core/p;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Ljava/util/List;Lcom/twitter/util/network/c;)Lcom/twitter/util/collection/k;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/p;",
            ">;",
            "Lcom/twitter/util/network/c;",
            ")",
            "Lcom/twitter/util/collection/k",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    invoke-static {}, Lcom/twitter/util/collection/k;->a()Lcom/twitter/util/collection/k;

    move-result-object v0

    .line 72
    :goto_0
    return-object v0

    .line 55
    :cond_0
    iget-object v0, p2, Lcom/twitter/util/network/c;->a:Lcom/twitter/util/network/DownloadQuality;

    sget-object v1, Lcom/twitter/util/network/DownloadQuality;->a:Lcom/twitter/util/network/DownloadQuality;

    if-eq v0, v1, :cond_1

    iget-object v0, p2, Lcom/twitter/util/network/c;->a:Lcom/twitter/util/network/DownloadQuality;

    .line 57
    :goto_1
    invoke-virtual {v0}, Lcom/twitter/util/network/DownloadQuality;->c()F

    move-result v0

    const/high16 v1, 0x44800000    # 1024.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v0, v1

    .line 58
    new-instance v2, Lcom/twitter/library/av/model/parser/c$a;

    invoke-direct {v2, v0}, Lcom/twitter/library/av/model/parser/c$a;-><init>(F)V

    .line 61
    const/4 v1, 0x0

    .line 62
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/p;

    .line 63
    invoke-static {v0}, Lcom/twitter/library/av/model/parser/c;->c(Lcom/twitter/model/core/p;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 64
    invoke-virtual {v2, v1, v0}, Lcom/twitter/library/av/model/parser/c$a;->a(Lcom/twitter/model/core/p;Lcom/twitter/model/core/p;)I

    move-result v4

    if-gez v4, :cond_5

    :goto_3
    move-object v1, v0

    .line 67
    goto :goto_2

    .line 55
    :cond_1
    sget-object v0, Lcom/twitter/util/network/DownloadQuality;->e:Lcom/twitter/util/network/DownloadQuality;

    goto :goto_1

    .line 69
    :cond_2
    invoke-direct {p0, v1}, Lcom/twitter/library/av/model/parser/c;->a(Lcom/twitter/model/core/p;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 70
    invoke-direct {p0, v1}, Lcom/twitter/library/av/model/parser/c;->b(Lcom/twitter/model/core/p;)Lcom/twitter/model/core/p;

    move-result-object v1

    .line 72
    :cond_3
    if-nez v1, :cond_4

    invoke-static {}, Lcom/twitter/util/collection/k;->a()Lcom/twitter/util/collection/k;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-object v0, v1, Lcom/twitter/model/core/p;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/collection/k;->a(Ljava/lang/Object;)Lcom/twitter/util/collection/k;

    move-result-object v0

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_3
.end method
