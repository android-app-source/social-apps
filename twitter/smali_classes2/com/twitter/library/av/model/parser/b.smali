.class public abstract Lcom/twitter/library/av/model/parser/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/network/j;


# instance fields
.field public a:[Lcom/twitter/model/av/Video;

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILjava/io/InputStream;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 21
    const/16 v0, 0xc8

    if-ne p1, v0, :cond_1

    .line 23
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/twitter/library/av/model/parser/b;->a(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 31
    :cond_0
    :goto_0
    return-void

    .line 24
    :catch_0
    move-exception v0

    .line 25
    iput-object v1, p0, Lcom/twitter/library/av/model/parser/b;->a:[Lcom/twitter/model/av/Video;

    goto :goto_0

    .line 27
    :cond_1
    const/16 v0, 0x193

    if-ne p1, v0, :cond_0

    .line 28
    iput-object v1, p0, Lcom/twitter/library/av/model/parser/b;->a:[Lcom/twitter/model/av/Video;

    .line 29
    sget v0, Lazw$k;->av_playback_forbidden:I

    iput v0, p0, Lcom/twitter/library/av/model/parser/b;->b:I

    goto :goto_0
.end method

.method public a(Lcom/twitter/network/l;)V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/av/model/parser/b;->a:[Lcom/twitter/model/av/Video;

    .line 38
    return-void
.end method

.method protected abstract a(Ljava/io/InputStream;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method
