.class public Lcom/twitter/library/av/model/PeriscopePlaylist;
.super Lcom/twitter/model/av/AVMediaPlaylist;
.source "Twttr"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/library/av/model/PeriscopePlaylist;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Z

.field private final i:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    new-instance v0, Lcom/twitter/library/av/model/PeriscopePlaylist$1;

    invoke-direct {v0}, Lcom/twitter/library/av/model/PeriscopePlaylist$1;-><init>()V

    sput-object v0, Lcom/twitter/library/av/model/PeriscopePlaylist;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 45
    invoke-direct {p0, p1}, Lcom/twitter/model/av/AVMediaPlaylist;-><init>(Landroid/os/Parcel;)V

    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->d:Ljava/lang/String;

    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->e:Ljava/lang/String;

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->f:Ljava/lang/String;

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->g:Ljava/lang/String;

    .line 50
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->h:Z

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->i:J

    .line 52
    return-void

    .line 50
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/twitter/model/av/AVMediaPlaylist;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->d:Ljava/lang/String;

    .line 37
    iput-object p2, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->e:Ljava/lang/String;

    .line 38
    iput-object p3, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->f:Ljava/lang/String;

    .line 39
    iput-object p4, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->g:Ljava/lang/String;

    .line 40
    iput-boolean p5, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->h:Z

    .line 41
    iput-wide p6, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->i:J

    .line 42
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->g:Ljava/lang/String;

    return-object v0
.end method

.method public c()Lcom/twitter/model/av/AVMedia;
    .locals 8

    .prologue
    .line 68
    new-instance v1, Lcom/twitter/library/av/model/PeriscopeMedia;

    iget-object v2, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->e:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->f:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->h:Z

    iget-wide v6, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->i:J

    invoke-direct/range {v1 .. v7}, Lcom/twitter/library/av/model/PeriscopeMedia;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V

    return-object v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 73
    if-ne p0, p1, :cond_1

    .line 74
    const/4 v0, 0x1

    .line 100
    :cond_0
    :goto_0
    return v0

    .line 76
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 79
    invoke-super {p0, p1}, Lcom/twitter/model/av/AVMediaPlaylist;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    check-cast p1, Lcom/twitter/library/av/model/PeriscopePlaylist;

    .line 85
    iget-boolean v1, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->h:Z

    iget-boolean v2, p1, Lcom/twitter/library/av/model/PeriscopePlaylist;->h:Z

    if-ne v1, v2, :cond_0

    .line 88
    iget-wide v2, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->i:J

    iget-wide v4, p1, Lcom/twitter/library/av/model/PeriscopePlaylist;->i:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 91
    iget-object v1, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/library/av/model/PeriscopePlaylist;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    iget-object v1, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/library/av/model/PeriscopePlaylist;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 97
    iget-object v1, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->g:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/library/av/model/PeriscopePlaylist;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    iget-object v0, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->f:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/library/av/model/PeriscopePlaylist;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 105
    invoke-super {p0}, Lcom/twitter/model/av/AVMediaPlaylist;->hashCode()I

    move-result v0

    .line 106
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 108
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 109
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 110
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    .line 111
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->i:J

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    .line 112
    return v0

    .line 110
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 117
    invoke-super {p0, p1, p2}, Lcom/twitter/model/av/AVMediaPlaylist;->a(Landroid/os/Parcel;I)V

    .line 118
    iget-object v0, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 122
    iget-boolean v0, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 123
    iget-wide v0, p0, Lcom/twitter/library/av/model/PeriscopePlaylist;->i:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 124
    return-void

    .line 122
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
