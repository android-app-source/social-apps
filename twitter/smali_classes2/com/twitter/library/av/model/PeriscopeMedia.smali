.class public Lcom/twitter/library/av/model/PeriscopeMedia;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/av/AVMedia;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/library/av/model/PeriscopeMedia;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Z

.field private final e:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/twitter/library/av/model/PeriscopeMedia$1;

    invoke-direct {v0}, Lcom/twitter/library/av/model/PeriscopeMedia$1;-><init>()V

    sput-object v0, Lcom/twitter/library/av/model/PeriscopeMedia;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->a:Ljava/lang/String;

    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->b:Ljava/lang/String;

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->c:Ljava/lang/String;

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->d:Z

    .line 50
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->e:J

    .line 51
    return-void

    .line 49
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->a:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->b:Ljava/lang/String;

    .line 40
    iput-object p3, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->c:Ljava/lang/String;

    .line 41
    iput-boolean p4, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->d:Z

    .line 42
    iput-wide p5, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->e:J

    .line 43
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->e:J

    return-wide v0
.end method

.method public a(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    const/4 v0, 0x0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    const-string/jumbo v0, "video"

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->b:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 132
    if-ne p0, p1, :cond_1

    .line 133
    const/4 v0, 0x1

    .line 153
    :cond_0
    :goto_0
    return v0

    .line 135
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 139
    check-cast p1, Lcom/twitter/library/av/model/PeriscopeMedia;

    .line 141
    iget-boolean v1, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->d:Z

    iget-boolean v2, p1, Lcom/twitter/library/av/model/PeriscopeMedia;->d:Z

    if-ne v1, v2, :cond_0

    .line 144
    iget-wide v2, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->e:J

    iget-wide v4, p1, Lcom/twitter/library/av/model/PeriscopeMedia;->e:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 147
    iget-object v1, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/library/av/model/PeriscopeMedia;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    iget-object v1, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/library/av/model/PeriscopeMedia;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 153
    iget-object v0, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/library/av/model/PeriscopeMedia;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public g()Lcom/twitter/model/av/a;
    .locals 2

    .prologue
    .line 88
    new-instance v0, Lcom/twitter/library/av/model/c;

    iget-object v1, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/twitter/library/av/model/c;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x3

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 158
    iget-object v0, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 159
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 160
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 161
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    .line 162
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->e:J

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    .line 163
    return v0

    .line 161
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()J
    .locals 2

    .prologue
    .line 102
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 126
    iget-boolean v0, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 127
    iget-wide v0, p0, Lcom/twitter/library/av/model/PeriscopeMedia;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 128
    return-void

    .line 126
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
