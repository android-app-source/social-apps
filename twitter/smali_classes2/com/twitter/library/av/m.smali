.class public Lcom/twitter/library/av/m;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/m$a;
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/twitter/library/av/playback/AVDataSource;

.field public final c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field public final d:Ljava/lang/String;

.field public final e:Lcom/twitter/model/av/AVMediaPlaylist;

.field public final f:Lcom/twitter/model/av/AVMedia;

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final h:I

.field public final i:Z

.field public final j:I

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/Boolean;

.field public final m:Ljava/lang/String;

.field public final n:Ljava/lang/Long;

.field public final o:Lbyf;

.field public final p:Ljava/lang/Long;

.field public final q:Landroid/os/Bundle;

.field public final r:Ljava/lang/Long;

.field public final s:Ljava/lang/String;

.field public final t:Ljava/lang/String;

.field public final u:Lbyd;


# direct methods
.method private constructor <init>(Lcom/twitter/library/av/m$a;)V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iget-object v0, p1, Lcom/twitter/library/av/m$a;->a:Landroid/content/Context;

    iput-object v0, p0, Lcom/twitter/library/av/m;->a:Landroid/content/Context;

    .line 90
    iget-object v0, p1, Lcom/twitter/library/av/m$a;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/library/av/m;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 91
    iget-object v0, p1, Lcom/twitter/library/av/m$a;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/av/m;->d:Ljava/lang/String;

    .line 92
    iget-object v0, p1, Lcom/twitter/library/av/m$a;->d:Lcom/twitter/model/av/AVMediaPlaylist;

    iput-object v0, p0, Lcom/twitter/library/av/m;->e:Lcom/twitter/model/av/AVMediaPlaylist;

    .line 93
    iget-object v0, p1, Lcom/twitter/library/av/m$a;->e:Lcom/twitter/model/av/AVMedia;

    iput-object v0, p0, Lcom/twitter/library/av/m;->f:Lcom/twitter/model/av/AVMedia;

    .line 94
    iget-object v0, p1, Lcom/twitter/library/av/m$a;->f:Ljava/util/Map;

    iput-object v0, p0, Lcom/twitter/library/av/m;->g:Ljava/util/Map;

    .line 95
    iget v0, p1, Lcom/twitter/library/av/m$a;->g:I

    iput v0, p0, Lcom/twitter/library/av/m;->h:I

    .line 96
    iget-boolean v0, p1, Lcom/twitter/library/av/m$a;->h:Z

    iput-boolean v0, p0, Lcom/twitter/library/av/m;->i:Z

    .line 97
    iget v0, p1, Lcom/twitter/library/av/m$a;->i:I

    iput v0, p0, Lcom/twitter/library/av/m;->j:I

    .line 98
    iget-object v0, p1, Lcom/twitter/library/av/m$a;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/av/m;->k:Ljava/lang/String;

    .line 99
    iget-object v0, p1, Lcom/twitter/library/av/m$a;->k:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/twitter/library/av/m;->l:Ljava/lang/Boolean;

    .line 100
    iget-object v0, p1, Lcom/twitter/library/av/m$a;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/av/m;->m:Ljava/lang/String;

    .line 101
    iget-object v0, p1, Lcom/twitter/library/av/m$a;->m:Ljava/lang/Long;

    iput-object v0, p0, Lcom/twitter/library/av/m;->n:Ljava/lang/Long;

    .line 102
    iget-object v0, p1, Lcom/twitter/library/av/m$a;->n:Lbyf;

    iput-object v0, p0, Lcom/twitter/library/av/m;->o:Lbyf;

    .line 103
    iget-object v0, p1, Lcom/twitter/library/av/m$a;->o:Ljava/lang/Long;

    iput-object v0, p0, Lcom/twitter/library/av/m;->p:Ljava/lang/Long;

    .line 104
    iget-object v0, p1, Lcom/twitter/library/av/m$a;->p:Landroid/os/Bundle;

    iput-object v0, p0, Lcom/twitter/library/av/m;->q:Landroid/os/Bundle;

    .line 105
    iget-object v0, p1, Lcom/twitter/library/av/m$a;->q:Ljava/lang/Long;

    iput-object v0, p0, Lcom/twitter/library/av/m;->r:Ljava/lang/Long;

    .line 106
    iget-object v0, p1, Lcom/twitter/library/av/m$a;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/av/m;->t:Ljava/lang/String;

    .line 107
    iget-object v0, p1, Lcom/twitter/library/av/m$a;->s:Lbyd;

    iput-object v0, p0, Lcom/twitter/library/av/m;->u:Lbyd;

    .line 108
    iget-object v0, p1, Lcom/twitter/library/av/m$a;->u:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/av/m;->s:Ljava/lang/String;

    .line 109
    iget-object v0, p1, Lcom/twitter/library/av/m$a;->t:Lcom/twitter/library/av/playback/AVDataSource;

    iput-object v0, p0, Lcom/twitter/library/av/m;->b:Lcom/twitter/library/av/playback/AVDataSource;

    .line 110
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/library/av/m$a;Lcom/twitter/library/av/m$1;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/twitter/library/av/m;-><init>(Lcom/twitter/library/av/m$a;)V

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 2

    .prologue
    .line 116
    iget v0, p0, Lcom/twitter/library/av/m;->j:I

    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 121
    if-ne p0, p1, :cond_1

    .line 130
    :cond_0
    :goto_0
    return v0

    .line 124
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 125
    goto :goto_0

    .line 128
    :cond_3
    check-cast p1, Lcom/twitter/library/av/m;

    .line 130
    iget v2, p0, Lcom/twitter/library/av/m;->h:I

    iget v3, p1, Lcom/twitter/library/av/m;->h:I

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcom/twitter/library/av/m;->i:Z

    iget-boolean v3, p1, Lcom/twitter/library/av/m;->i:Z

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/twitter/library/av/m;->o:Lbyf;

    iget-object v3, p1, Lcom/twitter/library/av/m;->o:Lbyf;

    .line 132
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/twitter/library/av/m;->j:I

    iget v3, p1, Lcom/twitter/library/av/m;->j:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/twitter/library/av/m;->a:Landroid/content/Context;

    iget-object v3, p1, Lcom/twitter/library/av/m;->a:Landroid/content/Context;

    .line 134
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/av/m;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v3, p1, Lcom/twitter/library/av/m;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 135
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/av/m;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/av/m;->d:Ljava/lang/String;

    .line 136
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/av/m;->e:Lcom/twitter/model/av/AVMediaPlaylist;

    iget-object v3, p1, Lcom/twitter/library/av/m;->e:Lcom/twitter/model/av/AVMediaPlaylist;

    .line 137
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/av/m;->f:Lcom/twitter/model/av/AVMedia;

    iget-object v3, p1, Lcom/twitter/library/av/m;->f:Lcom/twitter/model/av/AVMedia;

    .line 138
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/av/m;->g:Ljava/util/Map;

    iget-object v3, p1, Lcom/twitter/library/av/m;->g:Ljava/util/Map;

    .line 139
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/av/m;->k:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/av/m;->k:Ljava/lang/String;

    .line 140
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/av/m;->l:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/twitter/library/av/m;->l:Ljava/lang/Boolean;

    .line 141
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/av/m;->m:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/av/m;->m:Ljava/lang/String;

    .line 142
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/av/m;->n:Ljava/lang/Long;

    iget-object v3, p1, Lcom/twitter/library/av/m;->n:Ljava/lang/Long;

    .line 143
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/av/m;->p:Ljava/lang/Long;

    iget-object v3, p1, Lcom/twitter/library/av/m;->p:Ljava/lang/Long;

    .line 144
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/av/m;->r:Ljava/lang/Long;

    iget-object v3, p1, Lcom/twitter/library/av/m;->r:Ljava/lang/Long;

    .line 145
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/av/m;->q:Landroid/os/Bundle;

    iget-object v3, p1, Lcom/twitter/library/av/m;->q:Landroid/os/Bundle;

    .line 146
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/av/m;->t:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/av/m;->t:Ljava/lang/String;

    .line 147
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/av/m;->u:Lbyd;

    iget-object v3, p1, Lcom/twitter/library/av/m;->u:Lbyd;

    .line 148
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/av/m;->s:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/av/m;->s:Ljava/lang/String;

    .line 149
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/av/m;->b:Lcom/twitter/library/av/playback/AVDataSource;

    iget-object v3, p1, Lcom/twitter/library/av/m;->b:Lcom/twitter/library/av/playback/AVDataSource;

    .line 150
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 155
    iget-object v0, p0, Lcom/twitter/library/av/m;->a:Landroid/content/Context;

    const/16 v1, 0x14

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/library/av/m;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/library/av/m;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/twitter/library/av/m;->e:Lcom/twitter/model/av/AVMediaPlaylist;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/twitter/library/av/m;->f:Lcom/twitter/model/av/AVMedia;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/twitter/library/av/m;->g:Ljava/util/Map;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget v3, p0, Lcom/twitter/library/av/m;->h:I

    .line 161
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/twitter/library/av/m;->i:Z

    .line 162
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/twitter/library/av/m;->k:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-object v3, p0, Lcom/twitter/library/av/m;->l:Ljava/lang/Boolean;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    iget-object v3, p0, Lcom/twitter/library/av/m;->m:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    iget-object v3, p0, Lcom/twitter/library/av/m;->n:Ljava/lang/Long;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    iget-object v3, p0, Lcom/twitter/library/av/m;->o:Lbyf;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    iget-object v3, p0, Lcom/twitter/library/av/m;->p:Ljava/lang/Long;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    iget-object v3, p0, Lcom/twitter/library/av/m;->r:Ljava/lang/Long;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    iget-object v3, p0, Lcom/twitter/library/av/m;->q:Landroid/os/Bundle;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    iget v3, p0, Lcom/twitter/library/av/m;->j:I

    .line 171
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x10

    iget-object v3, p0, Lcom/twitter/library/av/m;->t:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0x11

    iget-object v3, p0, Lcom/twitter/library/av/m;->u:Lbyd;

    aput-object v3, v1, v2

    const/16 v2, 0x12

    iget-object v3, p0, Lcom/twitter/library/av/m;->s:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0x13

    iget-object v3, p0, Lcom/twitter/library/av/m;->b:Lcom/twitter/library/av/playback/AVDataSource;

    aput-object v3, v1, v2

    .line 155
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;[Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
