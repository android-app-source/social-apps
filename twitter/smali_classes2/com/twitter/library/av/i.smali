.class public Lcom/twitter/library/av/i;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/library/av/i;


# instance fields
.field private final b:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/twitter/library/av/i;

    invoke-direct {v0}, Lcom/twitter/library/av/i;-><init>()V

    sput-object v0, Lcom/twitter/library/av/i;->a:Lcom/twitter/library/av/i;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Landroid/os/HandlerThread;

    const-class v1, Lcom/twitter/library/av/i;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 40
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 41
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/twitter/library/av/i;->b:Landroid/os/Handler;

    .line 42
    return-void
.end method

.method public static a()Lcom/twitter/library/av/i;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/twitter/library/av/i;->a:Lcom/twitter/library/av/i;

    return-object v0
.end method


# virtual methods
.method public b()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/library/av/i;->b:Landroid/os/Handler;

    return-object v0
.end method
