.class public Lcom/twitter/library/av/ae;
.super Lcom/twitter/library/av/ai;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/av/VideoTextureView;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/n$c;)V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/twitter/library/av/z;

    invoke-direct {v0}, Lcom/twitter/library/av/z;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/library/av/ae;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/n$c;Lcom/twitter/library/av/z;)V

    .line 28
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/n$c;Lcom/twitter/library/av/z;)V
    .locals 2

    .prologue
    const v1, 0x3f800008    # 1.000001f

    .line 39
    invoke-direct {p0}, Lcom/twitter/library/av/ai;-><init>()V

    .line 40
    invoke-direct {p0}, Lcom/twitter/library/av/ae;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    const-string/jumbo v0, "video_threesixty_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {p2}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->h()Lcom/twitter/library/av/playback/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    new-instance v0, Lcom/twitter/library/av/VRVideoTextureView;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/twitter/library/av/VRVideoTextureView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/n$c;Lcom/twitter/library/av/z;)V

    iput-object v0, p0, Lcom/twitter/library/av/ae;->a:Lcom/twitter/library/av/VideoTextureView;

    .line 52
    :goto_0
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 53
    invoke-virtual {v0, v1, v1}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 54
    iget-object v1, p0, Lcom/twitter/library/av/ae;->a:Lcom/twitter/library/av/VideoTextureView;

    invoke-virtual {v1, v0}, Lcom/twitter/library/av/VideoTextureView;->setTransform(Landroid/graphics/Matrix;)V

    .line 55
    return-void

    .line 45
    :cond_0
    new-instance v0, Lcom/twitter/library/av/SmoothPlaybackVideoTextureView;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/twitter/library/av/SmoothPlaybackVideoTextureView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/n$c;Lcom/twitter/library/av/z;)V

    iput-object v0, p0, Lcom/twitter/library/av/ae;->a:Lcom/twitter/library/av/VideoTextureView;

    goto :goto_0

    .line 48
    :cond_1
    new-instance v0, Lcom/twitter/library/av/CompatVideoTextureView;

    invoke-direct {v0, p1, p2, p4}, Lcom/twitter/library/av/CompatVideoTextureView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/z;)V

    iput-object v0, p0, Lcom/twitter/library/av/ae;->a:Lcom/twitter/library/av/VideoTextureView;

    goto :goto_0
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 62
    invoke-static {}, Lblb;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/library/av/ae;->a:Lcom/twitter/library/av/VideoTextureView;

    return-object v0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/library/av/ae;->a:Lcom/twitter/library/av/VideoTextureView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/VideoTextureView;->setKeepScreenOn(Z)V

    .line 88
    return-void
.end method

.method protected b()Lcom/twitter/library/av/aa;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/twitter/library/av/ae;->a:Lcom/twitter/library/av/VideoTextureView;

    return-object v0
.end method
