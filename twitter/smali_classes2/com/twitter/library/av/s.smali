.class public Lcom/twitter/library/av/s;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/av/p;


# instance fields
.field private a:I

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/twitter/library/av/r;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/twitter/library/av/r;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/av/s;->a:I

    .line 25
    iput-object p1, p0, Lcom/twitter/library/av/s;->b:Ljava/util/List;

    .line 26
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/twitter/library/av/s;->a:I

    return v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 47
    iput p1, p0, Lcom/twitter/library/av/s;->a:I

    .line 48
    return-void
.end method

.method public b()Lcom/twitter/library/av/a;
    .locals 4

    .prologue
    .line 41
    new-instance v0, Lcom/twitter/library/av/a;

    .line 42
    invoke-virtual {p0}, Lcom/twitter/library/av/s;->f()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/twitter/library/av/s;->g()Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v1

    .line 41
    invoke-static {v2, v3, v1}, Lcom/twitter/model/av/DynamicAdId;->a(JLcgi;)Lcom/twitter/model/av/DynamicAdId;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/av/a;-><init>(Lcom/twitter/model/av/DynamicAdId;)V

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/twitter/library/av/s;->g()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ah()Z

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x1

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/library/av/s;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/twitter/library/av/s;->g()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->t:J

    return-wide v0
.end method

.method g()Lcom/twitter/model/core/Tweet;
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/library/av/s;->b:Ljava/util/List;

    iget v1, p0, Lcom/twitter/library/av/s;->a:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/r;

    invoke-interface {v0}, Lcom/twitter/library/av/r;->a()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    return-object v0
.end method
