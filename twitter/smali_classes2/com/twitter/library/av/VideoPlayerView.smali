.class public Lcom/twitter/library/av/VideoPlayerView;
.super Landroid/view/ViewGroup;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnScrollChangedListener;
.implements Lcom/twitter/library/av/k;
.implements Lcom/twitter/media/ui/image/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/VideoPlayerView$a;,
        Lcom/twitter/library/av/VideoPlayerView$Mode;
    }
.end annotation


# instance fields
.field protected final a:Lcom/twitter/library/av/playback/AVPlayer;

.field protected final b:Lcom/twitter/library/av/control/e;

.field protected final c:Lcom/twitter/library/av/VideoPlayerView$Mode;

.field protected final d:Landroid/graphics/Point;

.field e:Z

.field f:Lcom/twitter/library/av/control/e;

.field h:Lcom/twitter/library/av/af;

.field private final i:Lcom/twitter/library/av/ag;

.field private final j:Landroid/os/Handler;

.field private final k:Lcom/twitter/library/av/VideoViewContainer;

.field private final l:Lcom/twitter/library/av/ac;

.field private m:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/library/av/k;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

.field private final o:Landroid/graphics/Rect;

.field private final p:Lcom/twitter/library/av/ScaleType;

.field private q:Ljava/lang/Runnable;

.field private final r:Lcom/twitter/library/av/ak;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/VideoPlayerView$Mode;)V
    .locals 1

    .prologue
    .line 88
    new-instance v0, Lcom/twitter/library/av/ag;

    invoke-direct {v0}, Lcom/twitter/library/av/ag;-><init>()V

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/twitter/library/av/VideoPlayerView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/ag;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/VideoViewContainer;Lcom/twitter/library/av/ag;Lcom/twitter/library/av/VideoPlayerView$Mode;)V
    .locals 8

    .prologue
    .line 116
    new-instance v6, Lcom/twitter/library/av/ak;

    invoke-direct {v6}, Lcom/twitter/library/av/ak;-><init>()V

    new-instance v7, Lcom/twitter/library/av/ac;

    .line 118
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {v7, v0}, Lcom/twitter/library/av/ac;-><init>(Landroid/content/res/Resources;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 116
    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/av/VideoPlayerView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/VideoViewContainer;Lcom/twitter/library/av/ag;Lcom/twitter/library/av/VideoPlayerView$Mode;Lcom/twitter/library/av/ak;Lcom/twitter/library/av/ac;)V

    .line 119
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/VideoViewContainer;Lcom/twitter/library/av/ag;Lcom/twitter/library/av/VideoPlayerView$Mode;Lcom/twitter/library/av/ak;Lcom/twitter/library/av/ac;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 143
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 63
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v2, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->d:Landroid/graphics/Point;

    .line 70
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->j:Landroid/os/Handler;

    .line 73
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->m:Ljava/lang/ref/WeakReference;

    .line 76
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->o:Landroid/graphics/Rect;

    .line 144
    sget v0, Lazw$g;->video_player_view:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/VideoPlayerView;->setId(I)V

    .line 145
    invoke-virtual {p0, v2}, Lcom/twitter/library/av/VideoPlayerView;->setWillNotDraw(Z)V

    .line 146
    iput-object p6, p0, Lcom/twitter/library/av/VideoPlayerView;->r:Lcom/twitter/library/av/ak;

    .line 147
    iput-object p2, p0, Lcom/twitter/library/av/VideoPlayerView;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 148
    invoke-virtual {p2}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    .line 149
    iput-object p5, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    .line 150
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/VideoPlayerView;->a(Lcom/twitter/library/av/VideoPlayerView$Mode;)Lcom/twitter/library/av/ScaleType;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->p:Lcom/twitter/library/av/ScaleType;

    .line 151
    iput-object p3, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    .line 152
    iput-object p7, p0, Lcom/twitter/library/av/VideoPlayerView;->l:Lcom/twitter/library/av/ac;

    .line 154
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->K()Lcom/twitter/library/av/j;

    move-result-object v0

    .line 155
    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->getEmbeddedChromeMode()I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/twitter/library/av/j;->a(Landroid/content/Context;I)Lcom/twitter/library/av/control/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    .line 156
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-interface {v0, v1}, Lcom/twitter/library/av/control/e;->a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V

    .line 158
    iput-object p4, p0, Lcom/twitter/library/av/VideoPlayerView;->i:Lcom/twitter/library/av/ag;

    .line 159
    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    invoke-direct {p0}, Lcom/twitter/library/av/VideoPlayerView;->s()Lcom/twitter/library/av/af;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    .line 163
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->a:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->g:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->j:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->m:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, v1, :cond_0

    .line 165
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    invoke-interface {v0}, Lcom/twitter/library/av/af;->a()V

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    invoke-direct {p0, v0}, Lcom/twitter/library/av/VideoPlayerView;->b(Lcom/twitter/library/av/VideoPlayerView$Mode;)Z

    move-result v0

    .line 173
    if-nez v0, :cond_1

    .line 174
    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    invoke-virtual {p0, v1}, Lcom/twitter/library/av/VideoPlayerView;->addView(Landroid/view/View;)V

    .line 176
    :cond_1
    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    if-eqz v1, :cond_2

    .line 177
    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    invoke-interface {v1}, Lcom/twitter/library/av/af;->getView()Landroid/view/View;

    move-result-object v1

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v2}, Lcom/twitter/library/av/VideoPlayerView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 180
    :cond_2
    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    invoke-interface {v1}, Lcom/twitter/library/av/control/e;->getView()Landroid/view/View;

    move-result-object v1

    .line 181
    if-eqz v1, :cond_3

    .line 182
    invoke-virtual {p0, v1}, Lcom/twitter/library/av/VideoPlayerView;->addView(Landroid/view/View;)V

    .line 185
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->c()V

    .line 187
    if-eqz v0, :cond_4

    .line 188
    new-instance v0, Lcom/twitter/library/av/VideoPlayerView$1;

    invoke-direct {v0, p0}, Lcom/twitter/library/av/VideoPlayerView$1;-><init>(Lcom/twitter/library/av/VideoPlayerView;)V

    .line 194
    invoke-virtual {p0, v0}, Lcom/twitter/library/av/VideoPlayerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    :cond_4
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->m:Ljava/lang/ref/WeakReference;

    .line 198
    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/VideoPlayerView;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/VideoPlayerView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 199
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/ag;Lcom/twitter/library/av/VideoPlayerView$Mode;)V
    .locals 6

    .prologue
    .line 99
    new-instance v0, Lcom/twitter/library/av/ah;

    invoke-direct {v0}, Lcom/twitter/library/av/ah;-><init>()V

    .line 101
    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/av/ah;->a(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;)Lcom/twitter/library/av/VideoViewContainer;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    .line 99
    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/av/VideoPlayerView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/VideoViewContainer;Lcom/twitter/library/av/ag;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    .line 104
    return-void
.end method

.method private b(Lcom/twitter/library/av/VideoPlayerView$Mode;)Z
    .locals 1

    .prologue
    .line 208
    sget-object v0, Lcom/twitter/library/av/VideoPlayerView$Mode;->b:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, p1, :cond_0

    sget-object v0, Lcom/twitter/library/av/VideoPlayerView$Mode;->d:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, p1, :cond_0

    sget-object v0, Lcom/twitter/library/av/VideoPlayerView$Mode;->f:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, p1, :cond_0

    sget-object v0, Lcom/twitter/library/av/VideoPlayerView$Mode;->h:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, p1, :cond_0

    sget-object v0, Lcom/twitter/library/av/VideoPlayerView$Mode;->i:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, p1, :cond_0

    sget-object v0, Lcom/twitter/library/av/VideoPlayerView$Mode;->k:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private s()Lcom/twitter/library/av/af;
    .locals 4

    .prologue
    .line 415
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->i:Lcom/twitter/library/av/ag;

    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/ag;->a(Landroid/content/Context;)Lcom/twitter/library/av/af;

    move-result-object v0

    .line 416
    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->p:Lcom/twitter/library/av/ScaleType;

    invoke-interface {v0, v1}, Lcom/twitter/library/av/af;->setScaleType(Lcom/twitter/library/av/ScaleType;)V

    .line 417
    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->e()Lcom/twitter/library/av/playback/u;

    move-result-object v1

    .line 418
    invoke-virtual {v1}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v1

    .line 419
    invoke-interface {v1}, Lcom/twitter/library/av/playback/AVDataSource;->b()Lcom/twitter/model/card/property/ImageSpec;

    move-result-object v1

    .line 420
    if-eqz v1, :cond_0

    .line 421
    invoke-interface {v0, v1}, Lcom/twitter/library/av/af;->setImageSpec(Lcom/twitter/model/card/property/ImageSpec;)V

    .line 424
    :cond_0
    iget-object v2, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v3, Lcom/twitter/library/av/VideoPlayerView$Mode;->a:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v3, Lcom/twitter/library/av/VideoPlayerView$Mode;->j:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v2, v3, :cond_1

    if-nez v1, :cond_2

    .line 426
    :cond_1
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 427
    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lazw$d;->placeholder_bg:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 426
    invoke-interface {v0, v1}, Lcom/twitter/library/av/af;->setPlaceholderDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 430
    :cond_2
    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v2, Lcom/twitter/library/av/VideoPlayerView$Mode;->i:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-ne v1, v2, :cond_3

    .line 431
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 432
    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lazw$d;->gray_opacity_30:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 431
    invoke-interface {v0, v1}, Lcom/twitter/library/av/af;->setPlaceholderDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 434
    :cond_3
    return-object v0
.end method

.method private t()V
    .locals 2

    .prologue
    .line 898
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    if-eqz v0, :cond_0

    .line 899
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    invoke-interface {v0}, Lcom/twitter/library/av/af;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/e;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    .line 901
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    if-eqz v0, :cond_1

    .line 902
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/VideoViewContainer;->setVisibility(I)V

    .line 904
    :cond_1
    return-void
.end method


# virtual methods
.method protected a(IIII)Landroid/graphics/Rect;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 374
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->p:Lcom/twitter/library/av/ScaleType;

    sget-object v1, Lcom/twitter/library/av/ScaleType;->b:Lcom/twitter/library/av/ScaleType;

    if-ne v0, v1, :cond_0

    .line 375
    new-instance v0, Landroid/graphics/Rect;

    sub-int v1, p3, p1

    sub-int v2, p4, p2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 377
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    sub-int v1, p3, p1

    sub-int v2, p4, p2

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/twitter/library/av/VideoViewContainer;->a(IIII)Landroid/graphics/Rect;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/av/VideoPlayerView$Mode;)Lcom/twitter/library/av/ScaleType;
    .locals 2

    .prologue
    .line 220
    sget-object v0, Lcom/twitter/library/av/VideoPlayerView$3;->a:[I

    invoke-virtual {p1}, Lcom/twitter/library/av/VideoPlayerView$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 230
    sget-object v0, Lcom/twitter/library/av/ScaleType;->a:Lcom/twitter/library/av/ScaleType;

    :goto_0
    return-object v0

    .line 226
    :pswitch_0
    sget-object v0, Lcom/twitter/library/av/ScaleType;->b:Lcom/twitter/library/av/ScaleType;

    goto :goto_0

    .line 220
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 954
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->e()Lcom/twitter/library/av/playback/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 972
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 958
    :pswitch_1
    sget v0, Lazw$k;->media_type_video:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 961
    :pswitch_2
    sget v0, Lazw$k;->media_type_gif:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 964
    :pswitch_3
    sget v0, Lazw$k;->media_type_vine:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 954
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(D)V
    .locals 3

    .prologue
    .line 647
    sget-object v0, Lbyo;->c:Lbyf;

    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->N()Lbyf;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 648
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/av/VideoViewContainer;->a(D)V

    .line 650
    :cond_0
    return-void
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 512
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->d:Landroid/graphics/Point;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Point;->set(II)V

    .line 513
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/av/VideoViewContainer;->a(II)V

    .line 514
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->l()V

    .line 515
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    if-eqz v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->l()V

    .line 518
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/k;

    .line 519
    if-eqz v0, :cond_1

    .line 520
    invoke-interface {v0, p1, p2}, Lcom/twitter/library/av/k;->a(II)V

    .line 522
    :cond_1
    return-void
.end method

.method public a(IIZZ)V
    .locals 2

    .prologue
    .line 527
    if-lez p2, :cond_0

    if-lez p1, :cond_0

    .line 528
    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/av/VideoPlayerView;->a(II)V

    .line 530
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    .line 531
    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    invoke-interface {v1, v0}, Lcom/twitter/library/av/control/e;->a(Lcom/twitter/model/av/AVMedia;)V

    .line 532
    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    if-eqz v1, :cond_1

    .line 533
    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    invoke-interface {v1, v0}, Lcom/twitter/library/av/control/e;->a(Lcom/twitter/model/av/AVMedia;)V

    .line 535
    :cond_1
    if-eqz p4, :cond_2

    .line 536
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->h()V

    .line 537
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    if-eqz v0, :cond_2

    .line 538
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->h()V

    .line 541
    :cond_2
    if-eqz p3, :cond_3

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->G()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 542
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a(Z)V

    .line 545
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_4

    .line 551
    invoke-direct {p0}, Lcom/twitter/library/av/VideoPlayerView;->t()V

    .line 554
    :cond_4
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/k;

    .line 555
    if-eqz v0, :cond_5

    .line 556
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/twitter/library/av/k;->a(IIZZ)V

    .line 558
    :cond_5
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 678
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->l:Lcom/twitter/library/av/ac;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/av/ac;->a(ILjava/lang/String;)Lcom/twitter/library/av/ad;

    move-result-object v1

    .line 679
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Lcom/twitter/library/av/control/e;->a(Landroid/content/Context;Lcom/twitter/library/av/ad;)V

    .line 680
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    if-eqz v0, :cond_0

    .line 681
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Lcom/twitter/library/av/control/e;->a(Landroid/content/Context;Lcom/twitter/library/av/ad;)V

    .line 684
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/k;

    .line 685
    if-eqz v0, :cond_1

    .line 686
    iget-object v1, v1, Lcom/twitter/library/av/ad;->b:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lcom/twitter/library/av/k;->a(ILjava/lang/String;)V

    .line 688
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/library/av/o;)V
    .locals 2

    .prologue
    .line 630
    sget-object v0, Lbyo;->c:Lbyf;

    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->N()Lbyf;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 631
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/VideoViewContainer;->a(Lcom/twitter/library/av/o;)V

    .line 633
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V
    .locals 2

    .prologue
    .line 706
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->j:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->q:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 707
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    invoke-interface {v0, p1}, Lcom/twitter/library/av/control/e;->a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V

    .line 708
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    if-eqz v0, :cond_0

    .line 709
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    invoke-interface {v0, p1}, Lcom/twitter/library/av/control/e;->a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V

    .line 712
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    invoke-virtual {v0}, Lcom/twitter/library/av/VideoViewContainer;->requestLayout()V

    .line 713
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    invoke-virtual {v0}, Lcom/twitter/library/av/VideoViewContainer;->invalidate()V

    .line 714
    sget-object v0, Lbyo;->c:Lbyf;

    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->N()Lbyf;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 715
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/VideoViewContainer;->setKeepScreenOn(Z)V

    .line 718
    :cond_1
    sget-object v0, Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;->d:Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;

    if-eq p1, v0, :cond_2

    .line 719
    invoke-direct {p0}, Lcom/twitter/library/av/VideoPlayerView;->t()V

    .line 722
    :cond_2
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/k;

    .line 723
    if-eqz v0, :cond_3

    .line 724
    invoke-interface {v0, p1}, Lcom/twitter/library/av/k;->a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V

    .line 726
    :cond_3
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/aa;)V
    .locals 1

    .prologue
    .line 601
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    invoke-interface {v0, p1}, Lcom/twitter/library/av/control/e;->a(Lcom/twitter/library/av/playback/aa;)V

    .line 602
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    invoke-interface {v0, p1}, Lcom/twitter/library/av/control/e;->a(Lcom/twitter/library/av/playback/aa;)V

    .line 606
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/k;

    .line 607
    if-eqz v0, :cond_1

    .line 608
    invoke-interface {v0, p1}, Lcom/twitter/library/av/k;->a(Lcom/twitter/library/av/playback/aa;)V

    .line 610
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/model/av/c;)V
    .locals 3

    .prologue
    .line 692
    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/twitter/library/av/VideoPlayerView$a;->a(Landroid/content/res/Resources;Lcom/twitter/model/av/c;)Lcom/twitter/library/av/ad;

    move-result-object v0

    .line 693
    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/twitter/library/av/control/e;->a(Landroid/content/Context;Lcom/twitter/library/av/ad;)V

    .line 694
    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    if-eqz v1, :cond_0

    .line 695
    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/twitter/library/av/control/e;->a(Landroid/content/Context;Lcom/twitter/library/av/ad;)V

    .line 698
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/k;

    .line 699
    if-eqz v0, :cond_1

    .line 700
    invoke-interface {v0, p1}, Lcom/twitter/library/av/k;->a(Lcom/twitter/model/av/c;)V

    .line 702
    :cond_1
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer/text/Cue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 637
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    invoke-interface {v0, p1}, Lcom/twitter/library/av/control/e;->a(Ljava/util/List;)V

    .line 639
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    if-eqz v0, :cond_0

    .line 640
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    invoke-interface {v0, p1}, Lcom/twitter/library/av/control/e;->a(Ljava/util/List;)V

    .line 642
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 575
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    invoke-interface {v0, p1}, Lcom/twitter/library/av/control/e;->b_(Z)V

    .line 576
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    if-eqz v0, :cond_0

    .line 577
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    invoke-interface {v0, p1}, Lcom/twitter/library/av/control/e;->b_(Z)V

    .line 580
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/k;

    .line 581
    if-eqz v0, :cond_1

    .line 582
    invoke-interface {v0, p1}, Lcom/twitter/library/av/k;->a(Z)V

    .line 584
    :cond_1
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 338
    iget-boolean v0, p0, Lcom/twitter/library/av/VideoPlayerView;->e:Z

    if-eqz v0, :cond_1

    .line 339
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->f()Z

    move-result v0

    .line 340
    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    if-eqz v1, :cond_0

    .line 341
    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    invoke-interface {v1}, Lcom/twitter/library/av/control/e;->f()Z

    .line 346
    :cond_0
    :goto_0
    return v0

    .line 344
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(II)V
    .locals 4

    .prologue
    .line 822
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->j:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->q:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 823
    const/16 v0, 0x2bd

    if-ne p1, v0, :cond_2

    .line 824
    new-instance v0, Lcom/twitter/library/av/VideoPlayerView$2;

    invoke-direct {v0, p0}, Lcom/twitter/library/av/VideoPlayerView$2;-><init>(Lcom/twitter/library/av/VideoPlayerView;)V

    iput-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->q:Ljava/lang/Runnable;

    .line 833
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->j:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->q:Ljava/lang/Runnable;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 846
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/k;

    .line 847
    if-eqz v0, :cond_1

    .line 848
    invoke-interface {v0, p1, p2}, Lcom/twitter/library/av/k;->b(II)V

    .line 850
    :cond_1
    return-void

    .line 834
    :cond_2
    const/16 v0, 0x2be

    if-ne p1, v0, :cond_3

    .line 835
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->d()V

    .line 836
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    if-eqz v0, :cond_0

    .line 837
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->d()V

    goto :goto_0

    .line 839
    :cond_3
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 840
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->b()V

    .line 841
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    if-eqz v0, :cond_4

    .line 842
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->b()V

    .line 844
    :cond_4
    invoke-direct {p0}, Lcom/twitter/library/av/VideoPlayerView;->t()V

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 881
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->g:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->f:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0, p0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a(Lcom/twitter/library/av/k;)Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 237
    return-void
.end method

.method protected d()Z
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->a:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->e:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->g:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->f:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->i:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->j:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->m:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 399
    const/4 v0, 0x4

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 408
    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->i()V

    .line 410
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 562
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->e()V

    .line 563
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    if-eqz v0, :cond_0

    .line 564
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->e()V

    .line 567
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/k;

    .line 568
    if-eqz v0, :cond_1

    .line 569
    invoke-interface {v0}, Lcom/twitter/library/av/k;->e()V

    .line 571
    :cond_1
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 654
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    .line 655
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/twitter/model/av/AVMedia;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_0
    const/4 v0, 0x1

    .line 656
    :goto_0
    iget-object v2, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    invoke-interface {v2, v0}, Lcom/twitter/library/av/control/e;->a_(Z)V

    .line 657
    iget-object v2, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    if-eqz v2, :cond_1

    .line 658
    iget-object v2, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    invoke-interface {v2, v0}, Lcom/twitter/library/av/control/e;->a_(Z)V

    .line 660
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/VideoViewContainer;->setKeepScreenOn(Z)V

    .line 662
    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 663
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    if-nez v0, :cond_2

    .line 664
    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->m()V

    .line 666
    :cond_2
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    invoke-interface {v0}, Lcom/twitter/library/av/af;->getView()Landroid/view/View;

    move-result-object v0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 667
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    invoke-interface {v0}, Lcom/twitter/library/av/af;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 670
    :cond_3
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/k;

    .line 671
    if-eqz v0, :cond_4

    .line 672
    invoke-interface {v0}, Lcom/twitter/library/av/k;->f()V

    .line 674
    :cond_4
    return-void

    :cond_5
    move v0, v1

    .line 655
    goto :goto_0
.end method

.method protected g()Z
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->a:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->e:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->i:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->j:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->l:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->m:Lcom/twitter/library/av/VideoPlayerView$Mode;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getChromeView()Lcom/twitter/library/av/control/e;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    return-object v0
.end method

.method public getCurrentMediaSource()Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 254
    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v1

    .line 255
    :goto_0
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/twitter/model/av/AVMedia;->b()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0

    :cond_1
    move-object v1, v0

    .line 254
    goto :goto_0
.end method

.method protected getEmbeddedChromeMode()I
    .locals 6

    .prologue
    const/4 v0, 0x3

    const/4 v1, 0x0

    .line 260
    iget-object v2, p0, Lcom/twitter/library/av/VideoPlayerView;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v2}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->h()Lcom/twitter/library/av/playback/u;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v2

    .line 261
    invoke-interface {v2}, Lcom/twitter/library/av/playback/AVDataSource;->d()I

    move-result v3

    .line 262
    sget-object v4, Lcom/twitter/library/av/VideoPlayerView$3;->a:[I

    iget-object v5, p0, Lcom/twitter/library/av/VideoPlayerView;->c:Lcom/twitter/library/av/VideoPlayerView$Mode;

    invoke-virtual {v5}, Lcom/twitter/library/av/VideoPlayerView$Mode;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    move v0, v1

    .line 291
    :cond_0
    :goto_0
    return v0

    .line 264
    :pswitch_1
    invoke-static {v2}, Lcom/twitter/android/av/ai;->a(Lcom/twitter/library/av/playback/AVDataSource;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 265
    const/16 v0, 0x9

    goto :goto_0

    .line 270
    :pswitch_2
    const/4 v0, 0x7

    goto :goto_0

    .line 273
    :pswitch_3
    const/4 v1, 0x2

    if-eq v1, v3, :cond_1

    if-ne v0, v3, :cond_2

    .line 274
    :cond_1
    const/4 v0, 0x4

    goto :goto_0

    .line 277
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_4
    move v0, v1

    .line 280
    goto :goto_0

    .line 285
    :pswitch_5
    const/4 v0, 0x5

    goto :goto_0

    .line 288
    :pswitch_6
    const/16 v0, 0x8

    goto :goto_0

    .line 262
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_5
        :pswitch_3
        :pswitch_5
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public getThumbnailDrawable()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 914
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    if-nez v0, :cond_0

    .line 915
    const-string/jumbo v0, "Getting the thumbnail before mVideoThumbnailPresenter is set is not yet implemented"

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    .line 917
    invoke-static {}, Lrx/c;->d()Lrx/c;

    move-result-object v0

    .line 919
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    invoke-interface {v0}, Lcom/twitter/library/av/af;->getThumbnailBitmap()Lrx/c;

    move-result-object v0

    goto :goto_0
.end method

.method public getVideoSize()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->d:Landroid/graphics/Point;

    return-object v0
.end method

.method public getVisibilityPercentage()F
    .locals 2

    .prologue
    .line 890
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->r:Lcom/twitter/library/av/ak;

    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->o:Landroid/graphics/Rect;

    invoke-virtual {v0, p0, v1}, Lcom/twitter/library/av/ak;->a(Landroid/view/View;Landroid/graphics/Rect;)F

    move-result v0

    return v0
.end method

.method public h()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 757
    iget-boolean v0, p0, Lcom/twitter/library/av/VideoPlayerView;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    invoke-virtual {v0}, Lcom/twitter/library/av/VideoViewContainer;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 800
    :cond_0
    :goto_0
    return-void

    .line 765
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->j()V

    .line 766
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    if-eqz v0, :cond_2

    .line 767
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->j()V

    .line 770
    :cond_2
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    invoke-virtual {v0}, Lcom/twitter/library/av/VideoViewContainer;->c()V

    .line 772
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    invoke-virtual {v0}, Lcom/twitter/library/av/VideoViewContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_3

    .line 773
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    invoke-virtual {p0, v0, v3}, Lcom/twitter/library/av/VideoPlayerView;->addView(Landroid/view/View;I)V

    .line 776
    :cond_3
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->d:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->d:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    if-lez v0, :cond_4

    .line 777
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->d:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/twitter/library/av/VideoPlayerView;->d:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/av/VideoViewContainer;->a(II)V

    .line 781
    :cond_4
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    if-eqz v0, :cond_6

    .line 782
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->z()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 783
    :cond_5
    invoke-direct {p0}, Lcom/twitter/library/av/VideoPlayerView;->t()V

    .line 789
    :cond_6
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/av/VideoPlayerView;->e:Z

    .line 790
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/VideoViewContainer;->setKeepScreenOn(Z)V

    .line 792
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/k;

    .line 793
    if-eqz v0, :cond_7

    .line 794
    invoke-interface {v0}, Lcom/twitter/library/av/k;->h()V

    .line 797
    :cond_7
    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 798
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->getVisibilityPercentage()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a(F)V

    goto :goto_0

    .line 785
    :cond_8
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    invoke-interface {v0}, Lcom/twitter/library/av/af;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public i()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 804
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->i()V

    .line 805
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    if-eqz v0, :cond_0

    .line 806
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->i()V

    .line 808
    :cond_0
    iput-boolean v1, p0, Lcom/twitter/library/av/VideoPlayerView;->e:Z

    .line 811
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    invoke-virtual {v0}, Lcom/twitter/library/av/VideoViewContainer;->a()Z

    .line 812
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/VideoViewContainer;->setKeepScreenOn(Z)V

    .line 814
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/k;

    .line 815
    if-eqz v0, :cond_1

    .line 816
    invoke-interface {v0}, Lcom/twitter/library/av/k;->i()V

    .line 818
    :cond_1
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    .line 730
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/VideoViewContainer;->setKeepScreenOn(Z)V

    .line 731
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->a()V

    .line 732
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    if-eqz v0, :cond_0

    .line 733
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->a()V

    .line 736
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/k;

    .line 737
    if-eqz v0, :cond_1

    .line 738
    invoke-interface {v0}, Lcom/twitter/library/av/k;->j()V

    .line 740
    :cond_1
    return-void
.end method

.method public k()V
    .locals 2

    .prologue
    .line 744
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    if-eqz v0, :cond_0

    .line 745
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    invoke-interface {v0}, Lcom/twitter/library/av/af;->getView()Landroid/view/View;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 746
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    invoke-interface {v0}, Lcom/twitter/library/av/af;->getView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 749
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->g()V

    .line 750
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    if-eqz v0, :cond_1

    .line 751
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    invoke-interface {v0}, Lcom/twitter/library/av/control/e;->g()V

    .line 753
    :cond_1
    return-void
.end method

.method public l()V
    .locals 2

    .prologue
    .line 614
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    if-eqz v0, :cond_0

    .line 615
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    invoke-interface {v0}, Lcom/twitter/library/av/af;->getView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 617
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    if-eqz v0, :cond_1

    .line 618
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/VideoViewContainer;->setVisibility(I)V

    .line 621
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/k;

    .line 622
    if-eqz v0, :cond_2

    .line 623
    invoke-interface {v0}, Lcom/twitter/library/av/k;->l()V

    .line 625
    :cond_2
    return-void
.end method

.method public m()V
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    if-nez v0, :cond_0

    .line 446
    invoke-direct {p0}, Lcom/twitter/library/av/VideoPlayerView;->s()Lcom/twitter/library/av/af;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    .line 448
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    invoke-interface {v0}, Lcom/twitter/library/av/af;->a()V

    .line 449
    return-void
.end method

.method public n()Lcom/twitter/library/av/playback/u;
    .locals 2

    .prologue
    .line 460
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->y()Z

    move-result v0

    .line 461
    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v1, v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a(Z)V

    .line 462
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->h()Lcom/twitter/library/av/playback/u;

    move-result-object v0

    return-object v0
.end method

.method public o()Lcom/twitter/library/av/playback/u;
    .locals 2

    .prologue
    .line 474
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->m()V

    .line 475
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    invoke-interface {v0}, Lcom/twitter/library/av/af;->getView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 478
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->h()Lcom/twitter/library/av/playback/u;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 383
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 384
    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 385
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 389
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 390
    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 392
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->G()Z

    move-result v0

    if-nez v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->c(Z)V

    .line 395
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 351
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/twitter/library/av/VideoPlayerView;->a(IIII)Landroid/graphics/Rect;

    move-result-object v0

    .line 352
    iget-object v1, p0, Lcom/twitter/library/av/VideoPlayerView;->k:Lcom/twitter/library/av/VideoViewContainer;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, v0, Landroid/graphics/Rect;->top:I

    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/twitter/library/av/VideoViewContainer;->layout(IIII)V

    .line 355
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->b:Lcom/twitter/library/av/control/e;

    sub-int v1, p4, p2

    sub-int v2, p5, p3

    invoke-interface {v0, v5, v5, v1, v2}, Lcom/twitter/library/av/control/e;->layout(IIII)V

    .line 357
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    invoke-interface {v0}, Lcom/twitter/library/av/af;->getView()Landroid/view/View;

    move-result-object v0

    sub-int v1, p4, p2

    sub-int v2, p5, p3

    invoke-virtual {v0, v5, v5, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 360
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 322
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 323
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 322
    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/av/VideoPlayerView;->setMeasuredDimension(II)V

    .line 324
    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/av/VideoPlayerView;->measureChildren(II)V

    .line 325
    return-void
.end method

.method public onScrollChanged()V
    .locals 2

    .prologue
    .line 854
    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 855
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->getVisibilityPercentage()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a(F)V

    .line 857
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2

    .prologue
    .line 861
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onWindowFocusChanged(Z)V

    .line 862
    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 865
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {p0}, Lcom/twitter/library/av/VideoPlayerView;->getVisibilityPercentage()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a(F)V

    .line 867
    :cond_0
    return-void
.end method

.method public p()Lcom/twitter/library/av/playback/u;
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->m()V

    .line 489
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->h()Lcom/twitter/library/av/playback/u;

    move-result-object v0

    return-object v0
.end method

.method public q()Lcom/twitter/library/av/playback/u;
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->n()V

    .line 502
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->h()Lcom/twitter/library/av/playback/u;

    move-result-object v0

    return-object v0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 926
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    invoke-interface {v0}, Lcom/twitter/library/av/af;->getView()Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/library/av/VideoThumbnailView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->h:Lcom/twitter/library/av/af;

    .line 927
    invoke-interface {v0}, Lcom/twitter/library/av/af;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/VideoThumbnailView;

    invoke-virtual {v0}, Lcom/twitter/library/av/VideoThumbnailView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 926
    :goto_0
    return v0

    .line 927
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAVPlayerListener(Lcom/twitter/library/av/k;)V
    .locals 1

    .prologue
    .line 317
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->m:Ljava/lang/ref/WeakReference;

    .line 318
    return-void
.end method

.method public setExternalChromeView(Lcom/twitter/library/av/control/e;)V
    .locals 1

    .prologue
    .line 304
    iput-object p1, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    .line 305
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->f:Lcom/twitter/library/av/control/e;

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/twitter/library/av/VideoPlayerView;->n:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->g()V

    .line 308
    :cond_0
    return-void
.end method
