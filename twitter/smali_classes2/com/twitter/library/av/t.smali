.class public Lcom/twitter/library/av/t;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/t$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/av/h;

.field private final c:Lcom/twitter/library/client/v;

.field private final d:Lcom/twitter/library/av/t$a;

.field private final e:Lcom/twitter/library/av/c;

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/model/av/DynamicAdId;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/av/h;Lcom/twitter/library/client/v;Lcom/twitter/library/av/t$a;Lcom/twitter/library/av/c;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/av/h;",
            "Lcom/twitter/library/client/v;",
            "Lcom/twitter/library/av/t$a;",
            "Lcom/twitter/library/av/c;",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/model/av/DynamicAdId;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/t;->a:Landroid/content/Context;

    .line 52
    iput-object p2, p0, Lcom/twitter/library/av/t;->b:Lcom/twitter/library/av/h;

    .line 53
    iput-object p3, p0, Lcom/twitter/library/av/t;->c:Lcom/twitter/library/client/v;

    .line 54
    iput-object p4, p0, Lcom/twitter/library/av/t;->d:Lcom/twitter/library/av/t$a;

    .line 55
    iput-object p5, p0, Lcom/twitter/library/av/t;->e:Lcom/twitter/library/av/c;

    .line 56
    iput-object p6, p0, Lcom/twitter/library/av/t;->f:Ljava/util/Set;

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/av/t$a;Lcom/twitter/library/av/c;)V
    .locals 7

    .prologue
    .line 42
    new-instance v2, Lcom/twitter/library/av/h;

    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/twitter/library/av/h;-><init>(Lcom/twitter/library/client/p;)V

    .line 43
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v3

    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    .line 42
    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/av/t;-><init>(Landroid/content/Context;Lcom/twitter/library/av/h;Lcom/twitter/library/client/v;Lcom/twitter/library/av/t$a;Lcom/twitter/library/av/c;Ljava/util/Set;)V

    .line 44
    return-void
.end method

.method protected static c(Lcom/twitter/library/service/s;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 160
    .line 161
    invoke-virtual {p0}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    .line 162
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 163
    :goto_0
    if-eqz v0, :cond_2

    .line 164
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v0

    .line 167
    :goto_1
    if-eqz v0, :cond_1

    .line 169
    const-string/jumbo v1, "Network error. status code: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v0, v0, Lcom/twitter/network/l;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 172
    :goto_2
    return-object v0

    :cond_0
    move-object v0, v1

    .line 162
    goto :goto_0

    .line 172
    :cond_1
    const-string/jumbo v0, ""

    goto :goto_2

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method private d(Lcom/twitter/library/service/s;)V
    .locals 4

    .prologue
    .line 114
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 115
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, ":::dynamic_video_ads:dynamic_preroll_request_error"

    aput-object v3, v1, v2

    .line 116
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 117
    new-instance v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 118
    invoke-static {p1}, Lcom/twitter/library/av/t;->c(Lcom/twitter/library/service/s;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->J:Ljava/lang/String;

    .line 119
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 120
    iget-object v1, p0, Lcom/twitter/library/av/t;->e:Lcom/twitter/library/av/c;

    iget-object v2, p0, Lcom/twitter/library/av/t;->a:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, Lcom/twitter/library/av/c;->a(Landroid/content/Context;Lcom/twitter/analytics/model/ScribeLog;)V

    .line 121
    return-void
.end method

.method private e(Lcom/twitter/library/service/s;)V
    .locals 4

    .prologue
    .line 128
    instance-of v0, p1, Lbbl;

    if-eqz v0, :cond_1

    .line 129
    check-cast p1, Lbbl;

    .line 130
    invoke-virtual {p1}, Lbbl;->b()Ljava/util/Map;

    move-result-object v0

    .line 131
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 132
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 133
    iget-object v3, p0, Lcom/twitter/library/av/t;->d:Lcom/twitter/library/av/t$a;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/av/DynamicAdId;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/av/DynamicAdInfo;

    invoke-interface {v3, v1, v0}, Lcom/twitter/library/av/t$a;->a(Lcom/twitter/model/av/DynamicAdId;Lcom/twitter/model/av/DynamicAdInfo;)V

    goto :goto_0

    .line 137
    :cond_1
    return-void
.end method

.method private f(Lcom/twitter/library/service/s;)V
    .locals 3

    .prologue
    .line 145
    instance-of v0, p1, Lbbl;

    if-eqz v0, :cond_1

    .line 147
    check-cast p1, Lbbl;

    .line 148
    invoke-virtual {p1}, Lbbl;->e()Ljava/util/List;

    move-result-object v0

    .line 149
    if-eqz v0, :cond_1

    .line 150
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/a;

    .line 151
    if-eqz v0, :cond_0

    .line 152
    iget-object v2, p0, Lcom/twitter/library/av/t;->f:Ljava/util/Set;

    iget-object v0, v0, Lcom/twitter/library/av/a;->a:Lcom/twitter/model/av/DynamicAdId;

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 157
    :cond_1
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/av/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Lcom/twitter/library/av/t;->b:Lcom/twitter/library/av/h;

    invoke-virtual {v0}, Lcom/twitter/library/av/h;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/library/av/t;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 1

    .prologue
    .line 99
    invoke-super {p0, p1}, Lcom/twitter/library/service/t;->a(Lcom/twitter/library/service/s;)V

    .line 100
    invoke-direct {p0, p1}, Lcom/twitter/library/av/t;->f(Lcom/twitter/library/service/s;)V

    .line 102
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    invoke-direct {p0, p1}, Lcom/twitter/library/av/t;->d(Lcom/twitter/library/service/s;)V

    .line 107
    :goto_0
    return-void

    .line 105
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/library/av/t;->e(Lcom/twitter/library/service/s;)V

    goto :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/av/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/library/av/t;->a(Ljava/util/List;Lcom/twitter/model/av/DynamicAd;)V

    .line 64
    return-void
.end method

.method public a(Ljava/util/List;Lcom/twitter/model/av/DynamicAd;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/av/a;",
            ">;",
            "Lcom/twitter/model/av/DynamicAd;",
            ")V"
        }
    .end annotation

    .prologue
    .line 72
    new-instance v1, Lbbl$a;

    iget-object v0, p0, Lcom/twitter/library/av/t;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/av/t;->c:Lcom/twitter/library/client/v;

    .line 73
    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lbbl$a;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 75
    invoke-virtual {v1, p2}, Lbbl$a;->a(Lcom/twitter/model/av/DynamicAd;)V

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/a;

    .line 82
    iget-object v3, p0, Lcom/twitter/library/av/t;->d:Lcom/twitter/library/av/t$a;

    invoke-interface {v3, v0}, Lcom/twitter/library/av/t$a;->a(Lcom/twitter/library/av/a;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/twitter/library/av/t;->f:Ljava/util/Set;

    iget-object v4, v0, Lcom/twitter/library/av/a;->a:Lcom/twitter/model/av/DynamicAdId;

    .line 83
    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 87
    invoke-virtual {v1, v0}, Lbbl$a;->a(Lcom/twitter/library/av/a;)Lbbl$a;

    .line 89
    iget-object v3, p0, Lcom/twitter/library/av/t;->f:Ljava/util/Set;

    iget-object v0, v0, Lcom/twitter/library/av/a;->a:Lcom/twitter/model/av/DynamicAdId;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 92
    :cond_1
    invoke-virtual {v1}, Lbbl$a;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbbl;

    .line 93
    iget-object v2, p0, Lcom/twitter/library/av/t;->b:Lcom/twitter/library/av/h;

    invoke-virtual {v2, v0, p0}, Lcom/twitter/library/av/h;->a(Lbbl;Lcom/twitter/library/service/t;)V

    goto :goto_1

    .line 95
    :cond_2
    return-void
.end method
