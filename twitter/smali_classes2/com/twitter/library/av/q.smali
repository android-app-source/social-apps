.class public Lcom/twitter/library/av/q;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/av/p;


# instance fields
.field private final a:Landroid/database/Cursor;

.field private final b:I

.field private final c:I

.field private final d:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 30
    const/4 v0, 0x1

    const/16 v1, 0x1c

    const/16 v2, 0x1b

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/twitter/library/av/q;-><init>(Landroid/database/Cursor;III)V

    .line 31
    return-void
.end method

.method constructor <init>(Landroid/database/Cursor;III)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/twitter/library/av/q;->a:Landroid/database/Cursor;

    .line 37
    iput p2, p0, Lcom/twitter/library/av/q;->b:I

    .line 38
    iput p3, p0, Lcom/twitter/library/av/q;->c:I

    .line 39
    iput p4, p0, Lcom/twitter/library/av/q;->d:I

    .line 40
    return-void
.end method

.method private a(ILjava/lang/String;)Z
    .locals 3

    .prologue
    .line 103
    iget-object v0, p0, Lcom/twitter/library/av/q;->a:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v0

    .line 105
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 106
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ") was not equal to "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 107
    invoke-direct {p0}, Lcom/twitter/library/av/q;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    .line 106
    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 108
    const/4 v0, 0x0

    .line 111
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private h()Ljava/lang/String;
    .locals 3

    .prologue
    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/library/av/q;->a:Landroid/database/Cursor;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ","

    iget-object v2, p0, Lcom/twitter/library/av/q;->a:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/library/av/q;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    return v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/library/av/q;->a:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 67
    return-void
.end method

.method public b()Lcom/twitter/library/av/a;
    .locals 4

    .prologue
    .line 60
    new-instance v0, Lcom/twitter/library/av/a;

    invoke-virtual {p0}, Lcom/twitter/library/av/q;->f()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/twitter/library/av/q;->g()Lcgi;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lcom/twitter/model/av/DynamicAdId;->a(JLcgi;)Lcom/twitter/model/av/DynamicAdId;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/av/a;-><init>(Lcom/twitter/model/av/DynamicAdId;)V

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/library/av/q;->a:Landroid/database/Cursor;

    iget v1, p0, Lcom/twitter/library/av/q;->d:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 72
    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 83
    iget-object v1, p0, Lcom/twitter/library/av/q;->a:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 84
    new-instance v1, Ljava/lang/AssertionError;

    const-string/jumbo v2, "Cursor is closed"

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 97
    :cond_0
    :goto_0
    return v0

    .line 88
    :cond_1
    iget-object v1, p0, Lcom/twitter/library/av/q;->a:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getColumnCount()I

    move-result v1

    .line 89
    iget v2, p0, Lcom/twitter/library/av/q;->d:I

    iget v3, p0, Lcom/twitter/library/av/q;->b:I

    iget v4, p0, Lcom/twitter/library/av/q;->c:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 91
    if-gt v1, v2, :cond_2

    .line 92
    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Column count "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, " less than required column count "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 93
    invoke-direct {p0}, Lcom/twitter/library/av/q;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    .line 92
    invoke-static {v3}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 97
    :cond_2
    iget v1, p0, Lcom/twitter/library/av/q;->d:I

    const-string/jumbo v2, "statuses_flags"

    invoke-direct {p0, v1, v2}, Lcom/twitter/library/av/q;->a(ILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/twitter/library/av/q;->b:I

    const-string/jumbo v2, "status_groups_g_status_id"

    .line 98
    invoke-direct {p0, v1, v2}, Lcom/twitter/library/av/q;->a(ILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/twitter/library/av/q;->c:I

    const-string/jumbo v2, "status_groups_pc"

    .line 99
    invoke-direct {p0, v1, v2}, Lcom/twitter/library/av/q;->a(ILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/twitter/library/av/q;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    return v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 51
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/av/q;->a:Landroid/database/Cursor;

    iget v1, p0, Lcom/twitter/library/av/q;->b:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 53
    :goto_0
    return-wide v0

    .line 52
    :catch_0
    move-exception v0

    .line 53
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method g()Lcgi;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 131
    iget-object v0, p0, Lcom/twitter/library/av/q;->a:Landroid/database/Cursor;

    iget v2, p0, Lcom/twitter/library/av/q;->c:I

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 133
    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    move-object v1, v0

    .line 135
    :goto_1
    return-object v1

    .line 133
    :cond_0
    :try_start_0
    sget-object v2, Lcgi;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v2}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgi;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 134
    :catch_0
    move-exception v0

    goto :goto_1
.end method
