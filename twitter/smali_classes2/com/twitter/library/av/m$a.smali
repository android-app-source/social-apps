.class public Lcom/twitter/library/av/m$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/av/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public a:Landroid/content/Context;

.field public b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field public c:Ljava/lang/String;

.field public d:Lcom/twitter/model/av/AVMediaPlaylist;

.field public e:Lcom/twitter/model/av/AVMedia;

.field public f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:I

.field public h:Z

.field public i:I

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/Boolean;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/Long;

.field public n:Lbyf;

.field public o:Ljava/lang/Long;

.field public p:Landroid/os/Bundle;

.field public q:Ljava/lang/Long;

.field public r:Ljava/lang/String;

.field public s:Lbyd;

.field public t:Lcom/twitter/library/av/playback/AVDataSource;

.field public u:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/twitter/library/av/m;)Lcom/twitter/library/av/m$a;
    .locals 1

    .prologue
    .line 320
    new-instance v0, Lcom/twitter/library/av/m$a;

    invoke-direct {v0}, Lcom/twitter/library/av/m$a;-><init>()V

    invoke-static {p0, v0}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/library/av/m;Lcom/twitter/library/av/m$a;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    return-object v0
.end method

.method static a(Lcom/twitter/library/av/m;Lcom/twitter/library/av/m$a;)Lcom/twitter/library/av/m$a;
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 330
    iget-object v0, p0, Lcom/twitter/library/av/m;->a:Landroid/content/Context;

    invoke-virtual {p1, v0}, Lcom/twitter/library/av/m$a;->a(Landroid/content/Context;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/m;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 331
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/m;->d:Ljava/lang/String;

    .line 332
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/m;->e:Lcom/twitter/model/av/AVMediaPlaylist;

    .line 333
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/model/av/AVMediaPlaylist;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/m;->f:Lcom/twitter/model/av/AVMedia;

    .line 334
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/model/av/AVMedia;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/m;->g:Ljava/util/Map;

    .line 335
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Ljava/util/Map;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/av/m;->h:I

    .line 336
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(I)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/library/av/m;->i:Z

    .line 337
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Z)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/av/m;->j:I

    .line 338
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->b(I)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/m;->k:Ljava/lang/String;

    .line 339
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->b(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/m;->l:Ljava/lang/Boolean;

    .line 340
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Ljava/lang/Boolean;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/m;->m:Ljava/lang/String;

    .line 341
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->c(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/m;->n:Ljava/lang/Long;

    .line 342
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Ljava/lang/Long;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/m;->o:Lbyf;

    .line 343
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Lbyf;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/m;->p:Ljava/lang/Long;

    .line 344
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->b(Ljava/lang/Long;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/m;->q:Landroid/os/Bundle;

    .line 345
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Landroid/os/Bundle;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/m;->r:Ljava/lang/Long;

    .line 346
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->c(Ljava/lang/Long;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/m;->t:Ljava/lang/String;

    .line 347
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->d(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/m;->u:Lbyd;

    .line 348
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Lbyd;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/m;->s:Ljava/lang/String;

    .line 349
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->e(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/m;->b:Lcom/twitter/library/av/playback/AVDataSource;

    .line 350
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    .line 330
    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/twitter/library/av/m$a;
    .locals 0

    .prologue
    .line 235
    iput p1, p0, Lcom/twitter/library/av/m$a;->g:I

    .line 236
    return-object p0
.end method

.method public a(Landroid/content/Context;)Lcom/twitter/library/av/m$a;
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lcom/twitter/library/av/m$a;->a:Landroid/content/Context;

    .line 206
    return-object p0
.end method

.method public a(Landroid/os/Bundle;)Lcom/twitter/library/av/m$a;
    .locals 0

    .prologue
    .line 282
    iput-object p1, p0, Lcom/twitter/library/av/m$a;->p:Landroid/os/Bundle;

    .line 283
    return-object p0
.end method

.method public a(Lbyd;)Lcom/twitter/library/av/m$a;
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Lcom/twitter/library/av/m$a;->s:Lbyd;

    .line 298
    return-object p0
.end method

.method public a(Lbyf;)Lcom/twitter/library/av/m$a;
    .locals 0

    .prologue
    .line 272
    iput-object p1, p0, Lcom/twitter/library/av/m$a;->n:Lbyf;

    .line 273
    return-object p0
.end method

.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/av/m$a;
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lcom/twitter/library/av/m$a;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 211
    return-object p0
.end method

.method public a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/library/av/m$a;
    .locals 0

    .prologue
    .line 309
    iput-object p1, p0, Lcom/twitter/library/av/m$a;->t:Lcom/twitter/library/av/playback/AVDataSource;

    .line 310
    return-object p0
.end method

.method public a(Lcom/twitter/model/av/AVMedia;)Lcom/twitter/library/av/m$a;
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/twitter/library/av/m$a;->e:Lcom/twitter/model/av/AVMedia;

    .line 226
    return-object p0
.end method

.method public a(Lcom/twitter/model/av/AVMediaPlaylist;)Lcom/twitter/library/av/m$a;
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lcom/twitter/library/av/m$a;->d:Lcom/twitter/model/av/AVMediaPlaylist;

    .line 221
    return-object p0
.end method

.method public a(Ljava/lang/Boolean;)Lcom/twitter/library/av/m$a;
    .locals 0

    .prologue
    .line 257
    iput-object p1, p0, Lcom/twitter/library/av/m$a;->k:Ljava/lang/Boolean;

    .line 258
    return-object p0
.end method

.method public a(Ljava/lang/Long;)Lcom/twitter/library/av/m$a;
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lcom/twitter/library/av/m$a;->m:Ljava/lang/Long;

    .line 268
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/av/m$a;
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lcom/twitter/library/av/m$a;->c:Ljava/lang/String;

    .line 216
    return-object p0
.end method

.method public a(Ljava/util/Map;)Lcom/twitter/library/av/m$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/twitter/library/av/m$a;"
        }
    .end annotation

    .prologue
    .line 230
    iput-object p1, p0, Lcom/twitter/library/av/m$a;->f:Ljava/util/Map;

    .line 231
    return-object p0
.end method

.method public a(Z)Lcom/twitter/library/av/m$a;
    .locals 0

    .prologue
    .line 240
    iput-boolean p1, p0, Lcom/twitter/library/av/m$a;->h:Z

    .line 241
    return-object p0
.end method

.method public a()Lcom/twitter/library/av/m;
    .locals 2

    .prologue
    .line 354
    new-instance v0, Lcom/twitter/library/av/m;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/av/m;-><init>(Lcom/twitter/library/av/m$a;Lcom/twitter/library/av/m$1;)V

    return-object v0
.end method

.method public b(I)Lcom/twitter/library/av/m$a;
    .locals 1

    .prologue
    .line 245
    if-ltz p1, :cond_0

    const/16 v0, 0x64

    if-gt p1, v0, :cond_0

    .line 246
    iput p1, p0, Lcom/twitter/library/av/m$a;->i:I

    .line 248
    :cond_0
    return-object p0
.end method

.method public b(Ljava/lang/Long;)Lcom/twitter/library/av/m$a;
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/twitter/library/av/m$a;->o:Ljava/lang/Long;

    .line 278
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/library/av/m$a;
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, Lcom/twitter/library/av/m$a;->j:Ljava/lang/String;

    .line 253
    return-object p0
.end method

.method public c(Ljava/lang/Long;)Lcom/twitter/library/av/m$a;
    .locals 0

    .prologue
    .line 287
    iput-object p1, p0, Lcom/twitter/library/av/m$a;->q:Ljava/lang/Long;

    .line 288
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/library/av/m$a;
    .locals 0

    .prologue
    .line 262
    iput-object p1, p0, Lcom/twitter/library/av/m$a;->l:Ljava/lang/String;

    .line 263
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/library/av/m$a;
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Lcom/twitter/library/av/m$a;->r:Ljava/lang/String;

    .line 293
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/library/av/m$a;
    .locals 0

    .prologue
    .line 303
    iput-object p1, p0, Lcom/twitter/library/av/m$a;->u:Ljava/lang/String;

    .line 304
    return-object p0
.end method
