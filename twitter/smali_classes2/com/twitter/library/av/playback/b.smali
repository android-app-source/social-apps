.class public abstract Lcom/twitter/library/av/playback/b;
.super Lcom/twitter/library/av/playback/af;
.source "Twttr"

# interfaces
.implements Lcom/google/android/exoplayer/ExoPlayer$Listener;
.implements Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer$EventListener;
.implements Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$EventListener;
.implements Lcom/twitter/library/av/playback/e$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/playback/b$a;
    }
.end annotation


# instance fields
.field private volatile A:J

.field private B:I

.field private final o:Lcom/google/android/exoplayer/ExoPlayer;

.field private final p:Landroid/os/Handler;

.field private final q:Lbrh;

.field private r:Lcom/google/android/exoplayer/TrackRenderer;

.field private s:Lcom/google/android/exoplayer/TrackRenderer;

.field private t:Lcom/google/android/exoplayer/TrackRenderer;

.field private u:I

.field private v:I

.field private w:I

.field private x:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/Surface;",
            ">;"
        }
    .end annotation
.end field

.field private y:Ljava/lang/Exception;

.field private z:Z


# direct methods
.method constructor <init>(Lcom/twitter/library/av/playback/l;Lcom/google/android/exoplayer/ExoPlayer;)V
    .locals 3

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/twitter/library/av/playback/af;-><init>(Lcom/twitter/library/av/playback/l;)V

    .line 50
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/library/av/playback/b;->u:I

    .line 53
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/av/playback/b;->x:Ljava/lang/ref/WeakReference;

    .line 56
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/library/av/playback/b;->A:J

    .line 62
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/library/av/playback/b;->p:Landroid/os/Handler;

    .line 63
    iput-object p2, p0, Lcom/twitter/library/av/playback/b;->o:Lcom/google/android/exoplayer/ExoPlayer;

    .line 64
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->o:Lcom/google/android/exoplayer/ExoPlayer;

    new-instance v1, Lcom/twitter/library/av/playback/b$a;

    iget-object v2, p0, Lcom/twitter/library/av/playback/b;->p:Landroid/os/Handler;

    invoke-direct {v1, p0, v2}, Lcom/twitter/library/av/playback/b$a;-><init>(Lcom/google/android/exoplayer/ExoPlayer$Listener;Landroid/os/Handler;)V

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/ExoPlayer;->addListener(Lcom/google/android/exoplayer/ExoPlayer$Listener;)V

    .line 65
    iget-object v0, p1, Lcom/twitter/library/av/playback/l;->a:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/b;->a(Lcom/twitter/model/av/AVMedia;)Lbrh;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/playback/b;->q:Lbrh;

    .line 66
    return-void
.end method

.method static a()I
    .locals 1

    .prologue
    .line 90
    const/16 v0, 0x3e8

    return v0
.end method

.method static synthetic a(Lcom/twitter/library/av/playback/b;)Lcom/google/android/exoplayer/ExoPlayer;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->o:Lcom/google/android/exoplayer/ExoPlayer;

    return-object v0
.end method


# virtual methods
.method protected a(Lcom/twitter/model/av/AVMedia;)Lbrh;
    .locals 2

    .prologue
    .line 164
    new-instance v0, Lbrh;

    iget-object v1, p0, Lcom/twitter/library/av/playback/b;->c:Lbix;

    invoke-direct {v0, v1}, Lbrh;-><init>(Lbix;)V

    return-object v0
.end method

.method public a(F)V
    .locals 4

    .prologue
    .line 257
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->s:Lcom/google/android/exoplayer/TrackRenderer;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->o:Lcom/google/android/exoplayer/ExoPlayer;

    iget-object v1, p0, Lcom/twitter/library/av/playback/b;->s:Lcom/google/android/exoplayer/TrackRenderer;

    const/4 v2, 0x1

    .line 259
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    .line 258
    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/exoplayer/ExoPlayer;->sendMessage(Lcom/google/android/exoplayer/ExoPlayer$ExoPlayerComponent;ILjava/lang/Object;)V

    .line 261
    :cond_0
    return-void
.end method

.method protected a(J)V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->o:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer/ExoPlayer;->seekTo(J)V

    .line 212
    return-void
.end method

.method protected a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->e:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {p0, p1, v0}, Lcom/twitter/library/av/playback/b;->a(Landroid/content/Context;Lcom/twitter/model/av/AVMedia;)V

    .line 145
    return-void
.end method

.method abstract a(Landroid/content/Context;Lcom/twitter/model/av/AVMedia;)V
.end method

.method public a(Landroid/view/Surface;)V
    .locals 3

    .prologue
    .line 232
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->x:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 243
    :goto_0
    return-void

    .line 236
    :cond_0
    const-string/jumbo v0, "AVMediaExoPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setSurface(Surface)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->r:Lcom/google/android/exoplayer/TrackRenderer;

    if-eqz v0, :cond_1

    .line 238
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->x:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 239
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->o:Lcom/google/android/exoplayer/ExoPlayer;

    iget-object v1, p0, Lcom/twitter/library/av/playback/b;->r:Lcom/google/android/exoplayer/TrackRenderer;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2, p1}, Lcom/google/android/exoplayer/ExoPlayer;->sendMessage(Lcom/google/android/exoplayer/ExoPlayer$ExoPlayerComponent;ILjava/lang/Object;)V

    goto :goto_0

    .line 241
    :cond_1
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/av/playback/b;->x:Ljava/lang/ref/WeakReference;

    goto :goto_0
.end method

.method public a(Lcom/google/android/exoplayer/TrackRenderer;)V
    .locals 0

    .prologue
    .line 394
    iput-object p1, p0, Lcom/twitter/library/av/playback/b;->r:Lcom/google/android/exoplayer/TrackRenderer;

    .line 395
    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 484
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/b;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 485
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/b;->x()V

    .line 487
    :cond_0
    new-instance v0, Lcom/google/android/exoplayer/ExoPlaybackException;

    const-string/jumbo v1, "Allocation Failure"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/ExoPlaybackException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/b;->onPlayerError(Lcom/google/android/exoplayer/ExoPlaybackException;)V

    .line 488
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 265
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/b;->b()Lcom/twitter/library/av/playback/AVMediaPlayer$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVMediaPlayer$a;->c()Lcom/twitter/library/av/playback/o;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/o;->a()V

    .line 266
    invoke-super {p0, p1}, Lcom/twitter/library/av/playback/af;->a(Z)V

    .line 267
    return-void
.end method

.method protected a(ZLjava/lang/Exception;)V
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/library/av/playback/b;->a(ZLjava/lang/Exception;I)V

    .line 174
    return-void
.end method

.method protected a(ZLjava/lang/Exception;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 185
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/b;->b()Lcom/twitter/library/av/playback/AVMediaPlayer$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVMediaPlayer$a;->c()Lcom/twitter/library/av/playback/o;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/twitter/library/av/playback/o;->a(ZLjava/lang/Exception;)V

    .line 186
    const-string/jumbo v0, "Unable to open content %s %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/library/av/playback/b;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 187
    const-string/jumbo v1, "AVMediaExoPlayer"

    invoke-static {v1, v0, p2}, Lcqj;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 189
    iput v4, p0, Lcom/twitter/library/av/playback/b;->u:I

    .line 190
    iput-object p2, p0, Lcom/twitter/library/av/playback/b;->y:Ljava/lang/Exception;

    .line 192
    iput p3, p0, Lcom/twitter/library/av/playback/b;->B:I

    .line 194
    invoke-virtual {p0, p1, p3, v0}, Lcom/twitter/library/av/playback/b;->a(ZILjava/lang/String;)V

    .line 195
    return-void
.end method

.method public b()Lcom/twitter/library/av/playback/AVMediaPlayer$a;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->b:Lcom/twitter/library/av/playback/AVMediaPlayer$a;

    return-object v0
.end method

.method public b(Lcom/google/android/exoplayer/TrackRenderer;)V
    .locals 0

    .prologue
    .line 398
    iput-object p1, p0, Lcom/twitter/library/av/playback/b;->s:Lcom/google/android/exoplayer/TrackRenderer;

    .line 399
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/twitter/library/av/playback/b;->v:I

    return v0
.end method

.method public c(Lcom/google/android/exoplayer/TrackRenderer;)V
    .locals 0

    .prologue
    .line 402
    iput-object p1, p0, Lcom/twitter/library/av/playback/b;->t:Lcom/google/android/exoplayer/TrackRenderer;

    .line 403
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/twitter/library/av/playback/b;->w:I

    return v0
.end method

.method protected e()J
    .locals 4

    .prologue
    .line 114
    iget-wide v0, p0, Lcom/twitter/library/av/playback/b;->A:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->o:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0}, Lcom/google/android/exoplayer/ExoPlayer;->getDuration()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/av/playback/b;->A:J

    .line 118
    :cond_0
    iget-wide v0, p0, Lcom/twitter/library/av/playback/b;->A:J

    return-wide v0
.end method

.method protected f()J
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->o:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0}, Lcom/google/android/exoplayer/ExoPlayer;->getCurrentPosition()J

    move-result-wide v0

    return-wide v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/twitter/library/av/playback/b;->B:I

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->y:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->y:Ljava/lang/Exception;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected i()Z
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->o:Lcom/google/android/exoplayer/ExoPlayer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected j()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 149
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->t:Lcom/google/android/exoplayer/TrackRenderer;

    if-eqz v0, :cond_1

    .line 150
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/exoplayer/TrackRenderer;

    iget-object v1, p0, Lcom/twitter/library/av/playback/b;->r:Lcom/google/android/exoplayer/TrackRenderer;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/twitter/library/av/playback/b;->s:Lcom/google/android/exoplayer/TrackRenderer;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/twitter/library/av/playback/b;->t:Lcom/google/android/exoplayer/TrackRenderer;

    aput-object v1, v0, v4

    .line 155
    :goto_0
    iget-object v1, p0, Lcom/twitter/library/av/playback/b;->o:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer/ExoPlayer;->prepare([Lcom/google/android/exoplayer/TrackRenderer;)V

    .line 156
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/b;->n()Landroid/view/Surface;

    move-result-object v0

    .line 157
    if-eqz v0, :cond_0

    .line 158
    iget-object v1, p0, Lcom/twitter/library/av/playback/b;->o:Lcom/google/android/exoplayer/ExoPlayer;

    iget-object v2, p0, Lcom/twitter/library/av/playback/b;->r:Lcom/google/android/exoplayer/TrackRenderer;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/exoplayer/ExoPlayer;->sendMessage(Lcom/google/android/exoplayer/ExoPlayer$ExoPlayerComponent;ILjava/lang/Object;)V

    .line 160
    :cond_0
    return-void

    .line 152
    :cond_1
    new-array v0, v4, [Lcom/google/android/exoplayer/TrackRenderer;

    iget-object v1, p0, Lcom/twitter/library/av/playback/b;->r:Lcom/google/android/exoplayer/TrackRenderer;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/twitter/library/av/playback/b;->s:Lcom/google/android/exoplayer/TrackRenderer;

    aput-object v1, v0, v3

    goto :goto_0
.end method

.method protected k()V
    .locals 2

    .prologue
    .line 199
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->f:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/b;->b(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    .line 200
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->o:Lcom/google/android/exoplayer/ExoPlayer;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/ExoPlayer;->setPlayWhenReady(Z)V

    .line 201
    return-void
.end method

.method protected l()V
    .locals 2

    .prologue
    .line 205
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->g:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/b;->b(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    .line 206
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->o:Lcom/google/android/exoplayer/ExoPlayer;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/ExoPlayer;->setPlayWhenReady(Z)V

    .line 207
    return-void
.end method

.method protected m()V
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->o:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0, p0}, Lcom/google/android/exoplayer/ExoPlayer;->removeListener(Lcom/google/android/exoplayer/ExoPlayer$Listener;)V

    .line 217
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->n:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/library/av/playback/b$1;

    invoke-direct {v1, p0}, Lcom/twitter/library/av/playback/b$1;-><init>(Lcom/twitter/library/av/playback/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 223
    return-void
.end method

.method public n()Landroid/view/Surface;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->x:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Surface;

    return-object v0
.end method

.method protected o()V
    .locals 0

    .prologue
    .line 371
    return-void
.end method

.method public onAudioTrackInitializationError(Lcom/google/android/exoplayer/audio/AudioTrack$InitializationException;)V
    .locals 1

    .prologue
    .line 470
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/twitter/library/av/playback/b;->a(ZLjava/lang/Exception;)V

    .line 471
    return-void
.end method

.method public onAudioTrackUnderrun(IJJ)V
    .locals 0

    .prologue
    .line 480
    return-void
.end method

.method public onAudioTrackWriteError(Lcom/google/android/exoplayer/audio/AudioTrack$WriteException;)V
    .locals 1

    .prologue
    .line 475
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/twitter/library/av/playback/b;->a(ZLjava/lang/Exception;)V

    .line 476
    return-void
.end method

.method public onCryptoError(Landroid/media/MediaCodec$CryptoException;)V
    .locals 1

    .prologue
    .line 461
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/twitter/library/av/playback/b;->a(ZLjava/lang/Exception;)V

    .line 462
    return-void
.end method

.method public onDecoderInitializationError(Lcom/google/android/exoplayer/MediaCodecTrackRenderer$DecoderInitializationException;)V
    .locals 1

    .prologue
    .line 456
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/twitter/library/av/playback/b;->a(ZLjava/lang/Exception;)V

    .line 457
    return-void
.end method

.method public onDecoderInitialized(Ljava/lang/String;JJ)V
    .locals 0

    .prologue
    .line 466
    return-void
.end method

.method public onDrawnToSurface(Landroid/view/Surface;)V
    .locals 3

    .prologue
    .line 443
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/b;->J()Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->f:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_1

    .line 444
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->g:Lcom/twitter/library/av/k;

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->g:Lcom/twitter/library/av/k;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/twitter/library/av/k;->b(II)V

    .line 450
    :cond_0
    :goto_0
    return-void

    .line 448
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/av/playback/b;->z:Z

    goto :goto_0
.end method

.method public onDroppedFrames(IJ)V
    .locals 0

    .prologue
    .line 426
    return-void
.end method

.method public onPlayWhenReadyCommitted()V
    .locals 0

    .prologue
    .line 382
    return-void
.end method

.method public onPlayerError(Lcom/google/android/exoplayer/ExoPlaybackException;)V
    .locals 1

    .prologue
    .line 386
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/twitter/library/av/playback/b;->a(ZLjava/lang/Exception;)V

    .line 387
    return-void
.end method

.method public onPlayerStateChanged(ZI)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x3

    const/4 v1, 0x0

    .line 273
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/b;->b()Lcom/twitter/library/av/playback/AVMediaPlayer$a;

    move-result-object v2

    invoke-interface {v2}, Lcom/twitter/library/av/playback/AVMediaPlayer$a;->c()Lcom/twitter/library/av/playback/o;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Lcom/twitter/library/av/playback/o;->a(ZI)V

    .line 274
    iget v2, p0, Lcom/twitter/library/av/playback/b;->u:I

    .line 275
    iput p2, p0, Lcom/twitter/library/av/playback/b;->u:I

    .line 281
    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    if-eq p2, v6, :cond_0

    const/4 v3, 0x4

    if-ne p2, v3, :cond_1

    .line 283
    :cond_0
    sget-object v3, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->e:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v3}, Lcom/twitter/library/av/playback/b;->a(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    .line 284
    iget-object v3, p0, Lcom/twitter/library/av/playback/b;->g:Lcom/twitter/library/av/k;

    if-eqz v3, :cond_1

    .line 285
    iget-object v3, p0, Lcom/twitter/library/av/playback/b;->g:Lcom/twitter/library/av/k;

    iget v4, p0, Lcom/twitter/library/av/playback/b;->v:I

    iget v5, p0, Lcom/twitter/library/av/playback/b;->w:I

    invoke-interface {v3, v4, v5, v1, v1}, Lcom/twitter/library/av/k;->a(IIZZ)V

    .line 293
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/b;->B()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 368
    :cond_2
    :goto_0
    return-void

    .line 297
    :cond_3
    packed-switch p2, :pswitch_data_0

    .line 361
    :cond_4
    :goto_1
    if-ne v2, v6, :cond_2

    if-eq p2, v6, :cond_2

    .line 362
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->g:Lcom/twitter/library/av/k;

    if-eqz v0, :cond_5

    .line 363
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->g:Lcom/twitter/library/av/k;

    const/16 v2, 0x2be

    invoke-interface {v0, v2, v1}, Lcom/twitter/library/av/k;->b(II)V

    .line 366
    :cond_5
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->c:Lbix;

    new-instance v1, Lbjn;

    iget-object v2, p0, Lcom/twitter/library/av/playback/b;->e:Lcom/twitter/model/av/AVMedia;

    invoke-direct {v1, v2}, Lbjn;-><init>(Lcom/twitter/model/av/AVMedia;)V

    invoke-virtual {v0, v1}, Lbix;->a(Lbiw;)V

    goto :goto_0

    .line 299
    :pswitch_0
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->c:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/b;->a(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    goto :goto_1

    .line 303
    :pswitch_1
    sget-object v3, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->h:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v3}, Lcom/twitter/library/av/playback/b;->a(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    .line 304
    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/b;->c(Z)V

    .line 305
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->h:Landroid/media/MediaPlayer$OnCompletionListener;

    if-eqz v0, :cond_4

    .line 306
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->h:Landroid/media/MediaPlayer$OnCompletionListener;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/media/MediaPlayer$OnCompletionListener;->onCompletion(Landroid/media/MediaPlayer;)V

    goto :goto_1

    .line 311
    :pswitch_2
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->d:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/b;->a(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    goto :goto_1

    .line 315
    :pswitch_3
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/b;->K()Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    move-result-object v3

    sget-object v4, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->f:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-eq v3, v4, :cond_6

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/b;->J()Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    move-result-object v3

    sget-object v4, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->f:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-ne v3, v4, :cond_7

    .line 316
    :cond_6
    iget-object v3, p0, Lcom/twitter/library/av/playback/b;->o:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v3, v0}, Lcom/google/android/exoplayer/ExoPlayer;->setPlayWhenReady(Z)V

    .line 318
    :cond_7
    if-eq v2, v6, :cond_9

    .line 320
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/b;->J()Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v3, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->a:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-eq v0, v3, :cond_9

    .line 321
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->g:Lcom/twitter/library/av/k;

    if-eqz v0, :cond_8

    .line 322
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->g:Lcom/twitter/library/av/k;

    const/16 v3, 0x2bd

    invoke-interface {v0, v3, v1}, Lcom/twitter/library/av/k;->b(II)V

    .line 324
    :cond_8
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->c:Lbix;

    new-instance v3, Lbjo;

    iget-object v4, p0, Lcom/twitter/library/av/playback/b;->e:Lcom/twitter/model/av/AVMedia;

    invoke-direct {v3, v4}, Lbjo;-><init>(Lcom/twitter/model/av/AVMedia;)V

    invoke-virtual {v0, v3}, Lbix;->a(Lbiw;)V

    .line 326
    :cond_9
    invoke-virtual {p0, v1}, Lcom/twitter/library/av/playback/b;->c(Z)V

    goto :goto_1

    .line 330
    :pswitch_4
    if-eqz p1, :cond_c

    .line 331
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->f:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/b;->a(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    .line 332
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->k:Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/b;->a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V

    .line 335
    iget-boolean v0, p0, Lcom/twitter/library/av/playback/b;->z:Z

    if-eqz v0, :cond_a

    .line 336
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->g:Lcom/twitter/library/av/k;

    invoke-interface {v0, v6, v1}, Lcom/twitter/library/av/k;->b(II)V

    .line 337
    iput-boolean v1, p0, Lcom/twitter/library/av/playback/b;->z:Z

    .line 339
    :cond_a
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->g:Lcom/twitter/library/av/k;

    if-eqz v0, :cond_b

    .line 340
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->g:Lcom/twitter/library/av/k;

    iget-object v3, p0, Lcom/twitter/library/av/playback/b;->k:Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;

    invoke-interface {v0, v3}, Lcom/twitter/library/av/k;->a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V

    .line 344
    :cond_b
    sget-object v0, Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;->b:Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;

    iput-object v0, p0, Lcom/twitter/library/av/playback/b;->k:Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;

    .line 345
    invoke-virtual {p0, v1}, Lcom/twitter/library/av/playback/b;->b(Z)V

    goto/16 :goto_1

    .line 346
    :cond_c
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/b;->K()Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    move-result-object v3

    sget-object v4, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->f:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-ne v3, v4, :cond_4

    .line 348
    iget-object v3, p0, Lcom/twitter/library/av/playback/b;->k:Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;

    sget-object v4, Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;->a:Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;

    if-ne v3, v4, :cond_d

    :goto_2
    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/b;->b(Z)V

    goto/16 :goto_1

    :cond_d
    move v0, v1

    goto :goto_2

    .line 297
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method public onVideoSizeChanged(IIIF)V
    .locals 1

    .prologue
    .line 430
    iput p1, p0, Lcom/twitter/library/av/playback/b;->v:I

    .line 431
    iput p2, p0, Lcom/twitter/library/av/playback/b;->w:I

    .line 433
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->g:Lcom/twitter/library/av/k;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/av/playback/b;->v:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/twitter/library/av/playback/b;->w:I

    if-lez v0, :cond_0

    .line 434
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->g:Lcom/twitter/library/av/k;

    invoke-interface {v0, p1, p2}, Lcom/twitter/library/av/k;->a(II)V

    .line 436
    :cond_0
    return-void
.end method

.method protected p()V
    .locals 4

    .prologue
    .line 375
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->o:Lcom/google/android/exoplayer/ExoPlayer;

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->o:Lcom/google/android/exoplayer/ExoPlayer;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v2, v3}, Lcom/google/android/exoplayer/ExoPlayer;->seekTo(J)V

    .line 378
    :cond_0
    return-void
.end method

.method protected q()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->p:Landroid/os/Handler;

    return-object v0
.end method

.method protected r()Lcom/google/android/exoplayer/ExoPlayer;
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->o:Lcom/google/android/exoplayer/ExoPlayer;

    return-object v0
.end method

.method protected s()Lbrh;
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->q:Lbrh;

    return-object v0
.end method

.method public t()Lcom/google/android/exoplayer/TrackRenderer;
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/twitter/library/av/playback/b;->s:Lcom/google/android/exoplayer/TrackRenderer;

    return-object v0
.end method
