.class public Lcom/twitter/library/av/playback/TweetAVDataSource$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/av/playback/ax;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/av/playback/TweetAVDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field protected final a:Lcom/twitter/model/core/Tweet;

.field private final b:Lbis;


# direct methods
.method public constructor <init>(Lcom/twitter/model/core/Tweet;)V
    .locals 1

    .prologue
    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    iput-object p1, p0, Lcom/twitter/library/av/playback/TweetAVDataSource$a;->a:Lcom/twitter/model/core/Tweet;

    .line 189
    new-instance v0, Lbit;

    invoke-direct {v0, p1}, Lbit;-><init>(Lcom/twitter/model/core/Tweet;)V

    iput-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource$a;->b:Lbis;

    .line 190
    return-void
.end method


# virtual methods
.method public a()Lbis;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource$a;->b:Lbis;

    return-object v0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource$a;->a:Lcom/twitter/model/core/Tweet;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/twitter/library/scribe/b;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcgi;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource$a;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource$a;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->V()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
