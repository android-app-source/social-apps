.class public Lcom/twitter/library/av/playback/y;
.super Lcom/twitter/library/av/playback/aw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/playback/y$a;
    }
.end annotation


# instance fields
.field final a:Lcom/twitter/library/av/playback/AVPlayer;

.field private final e:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Landroid/os/Handler;Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/twitter/library/av/k;->g:Lcom/twitter/library/av/k;

    invoke-direct {p0, v0, p2}, Lcom/twitter/library/av/playback/aw;-><init>(Lcom/twitter/library/av/k;Landroid/os/Handler;)V

    .line 31
    iput-object p1, p0, Lcom/twitter/library/av/playback/y;->a:Lcom/twitter/library/av/playback/AVPlayer;

    .line 32
    iput-object p3, p0, Lcom/twitter/library/av/playback/y;->e:Landroid/content/res/Resources;

    .line 33
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/av/playback/y;II)V
    .locals 0

    .prologue
    .line 20
    invoke-super {p0, p1, p2}, Lcom/twitter/library/av/playback/aw;->a(II)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/av/playback/y;IIZZ)V
    .locals 0

    .prologue
    .line 20
    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/library/av/playback/aw;->a(IIZZ)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/av/playback/y;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 20
    invoke-super {p0, p1, p2}, Lcom/twitter/library/av/playback/aw;->a(ILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/library/av/k;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/twitter/library/av/playback/y;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->o()Lcom/twitter/library/av/k;

    move-result-object v0

    .line 39
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/library/av/k;->g:Lcom/twitter/library/av/k;

    goto :goto_0
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/twitter/library/av/playback/y$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/library/av/playback/y$1;-><init>(Lcom/twitter/library/av/playback/y;II)V

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/y;->a(Ljava/lang/Runnable;)V

    .line 51
    return-void
.end method

.method public a(IIZZ)V
    .locals 6

    .prologue
    .line 56
    new-instance v0, Lcom/twitter/library/av/playback/y$2;

    move-object v1, p0

    move v2, p4

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/av/playback/y$2;-><init>(Lcom/twitter/library/av/playback/y;ZIIZ)V

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/y;->a(Ljava/lang/Runnable;)V

    .line 81
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lcom/twitter/library/av/playback/y$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/library/av/playback/y$3;-><init>(Lcom/twitter/library/av/playback/y;ILjava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/y;->a(Ljava/lang/Runnable;)V

    .line 97
    return-void
.end method

.method public a(Lcom/twitter/model/av/c;)V
    .locals 1

    .prologue
    .line 101
    new-instance v0, Lcom/twitter/library/av/playback/y$4;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/av/playback/y$4;-><init>(Lcom/twitter/library/av/playback/y;Lcom/twitter/model/av/c;)V

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/y;->a(Ljava/lang/Runnable;)V

    .line 108
    invoke-super {p0, p1}, Lcom/twitter/library/av/playback/aw;->a(Lcom/twitter/model/av/c;)V

    .line 109
    return-void
.end method
