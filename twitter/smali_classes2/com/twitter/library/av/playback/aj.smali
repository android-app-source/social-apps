.class public Lcom/twitter/library/av/playback/aj;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:J

.field public final b:J

.field public final c:J

.field public final d:J


# direct methods
.method constructor <init>()V
    .locals 6

    .prologue
    const-wide/32 v4, 0xf4240

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string/jumbo v0, "android_standard_quality_autoplay_hls_bitrate_limit"

    const-wide/32 v2, 0x927c0

    invoke-static {v0, v2, v3}, Lcoj;->a(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/av/playback/aj;->a:J

    .line 24
    const-string/jumbo v0, "android_standard_quality_manualplay_hls_bitrate_limit"

    invoke-static {v0, v4, v5}, Lcoj;->a(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/av/playback/aj;->b:J

    .line 27
    const-string/jumbo v0, "android_high_quality_autoplay_hls_bitrate_limit"

    invoke-static {v0, v4, v5}, Lcoj;->a(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/av/playback/aj;->c:J

    .line 30
    const-string/jumbo v0, "android_high_quality_manualplay_hls_bitrate_limit"

    const-wide v2, 0x7fffffffffffffffL

    invoke-static {v0, v2, v3}, Lcoj;->a(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/av/playback/aj;->d:J

    .line 33
    return-void
.end method
