.class public Lcom/twitter/library/av/playback/c;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/card/CardContext;)Lcom/twitter/library/av/playback/CardAVDataSource;
    .locals 4

    .prologue
    .line 13
    invoke-static {p1}, Lcom/twitter/library/card/CardContext;->a(Lcom/twitter/library/card/CardContext;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 14
    new-instance v1, Lcom/twitter/library/av/playback/CardAVDataSource$b;

    invoke-direct {v1}, Lcom/twitter/library/av/playback/CardAVDataSource$b;-><init>()V

    .line 17
    if-eqz v0, :cond_0

    .line 18
    iget-wide v2, v0, Lcom/twitter/model/core/Tweet;->t:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/av/playback/CardAVDataSource$b;->a(Ljava/lang/String;)Lcom/twitter/library/av/playback/CardAVDataSource$b;

    move-result-object v1

    .line 19
    invoke-virtual {p1}, Lcom/twitter/library/card/CardContext;->a()Lcax;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/av/playback/CardAVDataSource$b;->a(Lcax;)Lcom/twitter/library/av/playback/CardAVDataSource$b;

    move-result-object v1

    .line 20
    invoke-virtual {v1, v0}, Lcom/twitter/library/av/playback/CardAVDataSource$b;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/av/playback/CardAVDataSource$b;

    move-result-object v0

    .line 21
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/CardAVDataSource$b;->e()Lcom/twitter/library/av/playback/CardAVDataSource;

    move-result-object v0

    .line 23
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/card/CardContext;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/av/playback/CardAVDataSource$b;->a(Ljava/lang/String;)Lcom/twitter/library/av/playback/CardAVDataSource$b;

    move-result-object v0

    .line 24
    invoke-virtual {p1}, Lcom/twitter/library/card/CardContext;->a()Lcax;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/CardAVDataSource$b;->a(Lcax;)Lcom/twitter/library/av/playback/CardAVDataSource$b;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/CardAVDataSource$b;->e()Lcom/twitter/library/av/playback/CardAVDataSource;

    move-result-object v0

    goto :goto_0
.end method
