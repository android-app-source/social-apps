.class public Lcom/twitter/library/av/playback/AVPlayer;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Lbrq$a;
.implements Lcom/twitter/library/av/g$a;
.implements Lcom/twitter/util/android/b$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/playback/AVPlayer$f;,
        Lcom/twitter/library/av/playback/AVPlayer$b;,
        Lcom/twitter/library/av/playback/AVPlayer$c;,
        Lcom/twitter/library/av/playback/AVPlayer$a;,
        Lcom/twitter/library/av/playback/AVPlayer$e;,
        Lcom/twitter/library/av/playback/AVPlayer$d;,
        Lcom/twitter/library/av/playback/AVPlayer$g;,
        Lcom/twitter/library/av/playback/AVPlayer$PlayerPauseType;,
        Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;
    }
.end annotation


# static fields
.field private static l:Lcom/twitter/library/av/j;

.field private static m:Lcom/twitter/library/av/playback/g;


# instance fields
.field private final A:Lcom/twitter/library/av/playback/m;

.field private B:Lrx/j;

.field private C:Z

.field private D:Lcom/twitter/library/client/v;

.field private E:Lcom/twitter/library/av/playback/AVPlayer$g;

.field private final F:Lcom/twitter/library/av/playback/AVPlayer$c;

.field private final G:Lcom/twitter/library/av/n;

.field private final H:Landroid/os/Bundle;

.field private I:Z

.field private J:F

.field private K:Z

.field private L:Lcom/twitter/library/av/model/parser/c;

.field volatile a:Lcom/twitter/model/av/AVMediaPlaylist;

.field b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/Surface;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/twitter/util/math/Size;

.field final d:Lcom/twitter/library/av/g;

.field e:Z

.field final f:Lcom/twitter/library/av/playback/t;

.field g:Lcom/twitter/library/av/playback/AVPlayer$PlayerPauseType;

.field volatile h:Lbyf;

.field i:J

.field j:Z

.field k:Z

.field private final n:Lcom/twitter/library/av/playback/s;

.field private final o:Lcom/twitter/library/av/playback/f;

.field private final p:[I

.field private final q:Lcom/twitter/library/av/playback/y;

.field private final r:Lbix;

.field private final s:Landroid/content/BroadcastReceiver;

.field private final t:Lbrq;

.field private u:Z

.field private v:I

.field private w:F

.field private final x:Landroid/content/Context;

.field private final y:Lcom/twitter/library/av/playback/u;

.field private final z:Lcom/twitter/library/av/playback/q;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 126
    sget-object v0, Lcom/twitter/library/av/playback/g;->a:Lcom/twitter/library/av/playback/g;

    sput-object v0, Lcom/twitter/library/av/playback/AVPlayer;->m:Lcom/twitter/library/av/playback/g;

    return-void
.end method

.method protected constructor <init>(Lcom/twitter/library/av/playback/q;Lcom/twitter/library/av/playback/u;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 180
    invoke-static {}, Lcom/twitter/library/av/playback/m;->a()Lcom/twitter/library/av/playback/m;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/library/av/playback/AVPlayer;-><init>(Lcom/twitter/library/av/playback/q;Lcom/twitter/library/av/playback/u;Landroid/content/Context;Lcom/twitter/library/av/playback/m;)V

    .line 181
    return-void
.end method

.method protected constructor <init>(Lcom/twitter/library/av/playback/q;Lcom/twitter/library/av/playback/u;Landroid/content/Context;Lcom/twitter/library/av/playback/m;)V
    .locals 6

    .prologue
    .line 187
    new-instance v5, Lcom/twitter/library/av/playback/t;

    invoke-direct {v5}, Lcom/twitter/library/av/playback/t;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/av/playback/AVPlayer;-><init>(Lcom/twitter/library/av/playback/q;Lcom/twitter/library/av/playback/u;Landroid/content/Context;Lcom/twitter/library/av/playback/m;Lcom/twitter/library/av/playback/t;)V

    .line 189
    return-void
.end method

.method protected constructor <init>(Lcom/twitter/library/av/playback/q;Lcom/twitter/library/av/playback/u;Landroid/content/Context;Lcom/twitter/library/av/playback/m;Landroid/os/Handler;Lcom/twitter/library/av/playback/AVPlayer$c;Lcom/twitter/library/av/n;Lcom/twitter/library/av/playback/f;Lcom/twitter/library/av/playback/t;Lcom/twitter/library/av/playback/s;Lcom/twitter/library/av/g;Lbrr;Lcom/twitter/library/av/playback/y$a;Lbix$a;)V
    .locals 4

    .prologue
    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    new-instance v1, Ljava/lang/ref/WeakReference;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->b:Ljava/lang/ref/WeakReference;

    .line 134
    sget-object v1, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    iput-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->c:Lcom/twitter/util/math/Size;

    .line 136
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->e:Z

    .line 138
    sget-object v1, Lcom/twitter/library/av/playback/AVPlayer$PlayerPauseType;->b:Lcom/twitter/library/av/playback/AVPlayer$PlayerPauseType;

    iput-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->g:Lcom/twitter/library/av/playback/AVPlayer$PlayerPauseType;

    .line 140
    sget-object v1, Lbyo;->a:Lbyf;

    iput-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->h:Lbyf;

    .line 147
    const/4 v1, 0x1

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->p:[I

    .line 152
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->u:Z

    .line 153
    const/16 v1, 0x64

    iput v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->v:I

    .line 154
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->w:F

    .line 170
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->H:Landroid/os/Bundle;

    .line 171
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->I:Z

    .line 175
    sget-object v1, Lcom/twitter/library/av/model/parser/c;->a:Lcom/twitter/library/av/model/parser/c;

    iput-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->L:Lcom/twitter/library/av/model/parser/c;

    .line 242
    invoke-virtual {p3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->x:Landroid/content/Context;

    .line 244
    iput-object p1, p0, Lcom/twitter/library/av/playback/AVPlayer;->z:Lcom/twitter/library/av/playback/q;

    .line 245
    iput-object p4, p0, Lcom/twitter/library/av/playback/AVPlayer;->A:Lcom/twitter/library/av/playback/m;

    .line 246
    iput-object p6, p0, Lcom/twitter/library/av/playback/AVPlayer;->F:Lcom/twitter/library/av/playback/AVPlayer$c;

    .line 247
    iput-object p7, p0, Lcom/twitter/library/av/playback/AVPlayer;->G:Lcom/twitter/library/av/n;

    .line 248
    iput-object p2, p0, Lcom/twitter/library/av/playback/AVPlayer;->y:Lcom/twitter/library/av/playback/u;

    .line 249
    iput-object p10, p0, Lcom/twitter/library/av/playback/AVPlayer;->n:Lcom/twitter/library/av/playback/s;

    .line 250
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->x:Landroid/content/Context;

    move-object/from16 v0, p12

    invoke-virtual {v0, v1}, Lbrr;->a(Landroid/content/Context;)Lbrq;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->t:Lbrq;

    .line 251
    iput-object p11, p0, Lcom/twitter/library/av/playback/AVPlayer;->d:Lcom/twitter/library/av/g;

    .line 252
    iput-object p8, p0, Lcom/twitter/library/av/playback/AVPlayer;->o:Lcom/twitter/library/av/playback/f;

    .line 253
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    move-object/from16 v0, p13

    invoke-virtual {v0, p0, p5, v1}, Lcom/twitter/library/av/playback/y$a;->a(Lcom/twitter/library/av/playback/AVPlayer;Landroid/os/Handler;Landroid/content/res/Resources;)Lcom/twitter/library/av/playback/y;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->q:Lcom/twitter/library/av/playback/y;

    .line 254
    iput-object p9, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    .line 255
    new-instance v1, Lcom/twitter/library/av/playback/z;

    invoke-direct {v1, p0}, Lcom/twitter/library/av/playback/z;-><init>(Lcom/twitter/library/av/playback/AVPlayer;)V

    move-object/from16 v0, p14

    invoke-virtual {v0, v1}, Lbix$a;->a(Lcom/twitter/library/av/playback/z;)Lbix;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->r:Lbix;

    .line 256
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    iget-object v2, p0, Lcom/twitter/library/av/playback/AVPlayer;->q:Lcom/twitter/library/av/playback/y;

    invoke-virtual {v1, p0, v2}, Lcom/twitter/library/av/playback/t;->a(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/library/av/k;)V

    .line 257
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->d:Lcom/twitter/library/av/g;

    invoke-virtual {v1, p0}, Lcom/twitter/library/av/g;->a(Lcom/twitter/library/av/g$a;)Z

    .line 258
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 259
    const-string/jumbo v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 260
    const-string/jumbo v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 261
    new-instance v2, Lcom/twitter/library/av/playback/AVPlayer$f;

    iget-object v3, p0, Lcom/twitter/library/av/playback/AVPlayer;->q:Lcom/twitter/library/av/playback/y;

    invoke-direct {v2, p0, v3}, Lcom/twitter/library/av/playback/AVPlayer$f;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/library/av/playback/y;)V

    iput-object v2, p0, Lcom/twitter/library/av/playback/AVPlayer;->s:Landroid/content/BroadcastReceiver;

    .line 262
    iget-object v2, p0, Lcom/twitter/library/av/playback/AVPlayer;->x:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/library/av/playback/AVPlayer;->s:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 265
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->r:Lbix;

    invoke-static {}, Lbko;->a()Lbko;

    move-result-object v2

    invoke-virtual {v2, p0}, Lbko;->a(Lcom/twitter/library/av/playback/AVPlayer;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbix;->a(Ljava/util/Collection;)Lbix;

    .line 267
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->n:Lcom/twitter/library/av/playback/s;

    .line 268
    invoke-virtual {v1}, Lcom/twitter/library/av/playback/s;->c()Lrx/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/av/playback/AVPlayer$1;

    invoke-direct {v2, p0}, Lcom/twitter/library/av/playback/AVPlayer$1;-><init>(Lcom/twitter/library/av/playback/AVPlayer;)V

    .line 269
    invoke-virtual {v1, v2}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 275
    return-void
.end method

.method protected constructor <init>(Lcom/twitter/library/av/playback/q;Lcom/twitter/library/av/playback/u;Landroid/content/Context;Lcom/twitter/library/av/playback/m;Lcom/twitter/library/av/playback/t;)V
    .locals 17

    .prologue
    .line 196
    new-instance v7, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v7, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v8, Lcom/twitter/library/av/playback/AVPlayer$c;

    invoke-direct {v8}, Lcom/twitter/library/av/playback/AVPlayer$c;-><init>()V

    new-instance v9, Lcom/twitter/library/av/n;

    invoke-direct {v9}, Lcom/twitter/library/av/n;-><init>()V

    sget-object v2, Lcom/twitter/library/av/playback/AVPlayer;->m:Lcom/twitter/library/av/playback/g;

    .line 198
    move-object/from16 v0, p3

    move-object/from16 v1, p2

    invoke-interface {v2, v0, v1}, Lcom/twitter/library/av/playback/g;->a(Landroid/content/Context;Lcom/twitter/library/av/playback/u;)Lcom/twitter/library/av/playback/f;

    move-result-object v10

    new-instance v12, Lcom/twitter/library/av/playback/s;

    invoke-direct {v12}, Lcom/twitter/library/av/playback/s;-><init>()V

    .line 199
    invoke-static/range {p3 .. p3}, Lcom/twitter/library/av/g;->a(Landroid/content/Context;)Lcom/twitter/library/av/g;

    move-result-object v13

    new-instance v14, Lbrr;

    invoke-direct {v14}, Lbrr;-><init>()V

    new-instance v15, Lcom/twitter/library/av/playback/y$a;

    invoke-direct {v15}, Lcom/twitter/library/av/playback/y$a;-><init>()V

    new-instance v16, Lbix$a;

    invoke-direct/range {v16 .. v16}, Lbix$a;-><init>()V

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v11, p5

    .line 196
    invoke-direct/range {v2 .. v16}, Lcom/twitter/library/av/playback/AVPlayer;-><init>(Lcom/twitter/library/av/playback/q;Lcom/twitter/library/av/playback/u;Landroid/content/Context;Lcom/twitter/library/av/playback/m;Landroid/os/Handler;Lcom/twitter/library/av/playback/AVPlayer$c;Lcom/twitter/library/av/n;Lcom/twitter/library/av/playback/f;Lcom/twitter/library/av/playback/t;Lcom/twitter/library/av/playback/s;Lcom/twitter/library/av/g;Lbrr;Lcom/twitter/library/av/playback/y$a;Lbix$a;)V

    .line 202
    return-void
.end method

.method private X()I
    .locals 1

    .prologue
    .line 440
    iget-boolean v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->K:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->v:I

    goto :goto_0
.end method

.method private Y()Z
    .locals 1

    .prologue
    .line 460
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->O()Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Z()V
    .locals 4

    .prologue
    .line 832
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->q:Lcom/twitter/library/av/playback/y;

    sget v1, Lazw$k;->media_error_audio_focus_rejected:I

    iget-object v2, p0, Lcom/twitter/library/av/playback/AVPlayer;->x:Landroid/content/Context;

    sget v3, Lazw$k;->media_error_audio_focus_rejected:I

    .line 834
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 832
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/av/playback/y;->a(ILjava/lang/String;)V

    .line 835
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/av/playback/AVPlayer;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->x:Landroid/content/Context;

    return-object v0
.end method

.method private a(Lcom/twitter/model/av/DynamicAd;)Lcom/twitter/util/collection/k;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/av/DynamicAd;",
            ")",
            "Lcom/twitter/util/collection/k",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1445
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/model/av/DynamicAd;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1446
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v0

    invoke-virtual {v0}, Lcrr;->e()Lcom/twitter/util/network/c;

    move-result-object v0

    .line 1447
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->L:Lcom/twitter/library/av/model/parser/c;

    .line 1448
    invoke-virtual {p1}, Lcom/twitter/model/av/DynamicAd;->c()Ljava/util/List;

    move-result-object v2

    .line 1447
    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/av/model/parser/c;->a(Ljava/util/List;Lcom/twitter/util/network/c;)Lcom/twitter/util/collection/k;

    move-result-object v0

    .line 1451
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/k;->a()Lcom/twitter/util/collection/k;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/library/av/playback/AVPlayer;Lrx/j;)Lrx/j;
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/twitter/library/av/playback/AVPlayer;->B:Lrx/j;

    return-object p1
.end method

.method public static a(Lcom/twitter/library/av/j;)V
    .locals 1

    .prologue
    .line 1244
    sput-object p0, Lcom/twitter/library/av/playback/AVPlayer;->l:Lcom/twitter/library/av/j;

    .line 1245
    const-class v0, Lcom/twitter/library/av/playback/AVPlayer;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 1246
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/library/av/playback/AVPlayerAttachment;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V

    return-void
.end method

.method private a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V
    .locals 1

    .prologue
    .line 346
    if-eqz p1, :cond_0

    .line 347
    invoke-virtual {p1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->b()Lbyf;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lbyf;)V

    .line 349
    :cond_0
    return-void
.end method

.method public static a(Lcom/twitter/library/av/playback/g;)V
    .locals 1

    .prologue
    .line 1259
    sput-object p0, Lcom/twitter/library/av/playback/AVPlayer;->m:Lcom/twitter/library/av/playback/g;

    .line 1260
    const-class v0, Lcom/twitter/library/av/playback/AVPlayer;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 1261
    return-void
.end method

.method private aa()Z
    .locals 2

    .prologue
    .line 841
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->O()Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v0

    .line 842
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/t;->j()Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/model/av/AVMediaPlaylist;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ab()Z
    .locals 2

    .prologue
    .line 849
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->O()Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v0

    .line 850
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/t;->j()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/model/av/AVMediaPlaylist;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ac()Z
    .locals 1

    .prologue
    .line 863
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVMediaPlayer;->w()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ad()I
    .locals 4

    .prologue
    .line 1097
    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->W()F

    move-result v2

    float-to-double v2, v2

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method private ae()V
    .locals 4

    .prologue
    .line 1132
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->O()Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v0

    .line 1133
    if-nez v0, :cond_1

    .line 1149
    :cond_0
    :goto_0
    return-void

    .line 1137
    :cond_1
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->q:Lcom/twitter/library/av/playback/y;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/y;->e()V

    .line 1138
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v1

    .line 1139
    if-eqz v1, :cond_2

    .line 1140
    iget-object v2, p0, Lcom/twitter/library/av/playback/AVPlayer;->r:Lbix;

    new-instance v3, Lbkm;

    invoke-direct {v3, v1}, Lbkm;-><init>(Lcom/twitter/model/av/AVMedia;)V

    invoke-virtual {v2, v3}, Lbix;->a(Lbiw;)V

    .line 1143
    :cond_2
    invoke-direct {p0, v0}, Lcom/twitter/library/av/playback/AVPlayer;->c(Lcom/twitter/model/av/AVMediaPlaylist;)Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1144
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->q:Lcom/twitter/library/av/playback/y;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/y;->f()V

    .line 1145
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->t:Lbrq;

    if-eqz v0, :cond_0

    .line 1146
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->t:Lbrq;

    invoke-virtual {v0, p0}, Lbrq;->b(Lbrq$a;)V

    goto :goto_0
.end method

.method private af()Z
    .locals 1

    .prologue
    .line 1346
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->h:Lbyf;

    invoke-interface {v0}, Lbyf;->a()Z

    move-result v0

    return v0
.end method

.method private ag()Z
    .locals 1

    .prologue
    .line 1429
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->U()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/av/playback/AVPlayer;->ah()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ah()Z
    .locals 2

    .prologue
    .line 1438
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->y:Lcom/twitter/library/av/playback/u;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->d()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/content/Context;)Lcom/twitter/model/av/AVMediaPlaylist;
    .locals 1

    .prologue
    .line 490
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->y:Lcom/twitter/library/av/playback/u;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->h()Lbyp;

    move-result-object v0

    .line 491
    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lbyp;->b(Landroid/content/Context;)Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/library/av/playback/AVPlayer;)Lrx/j;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->B:Lrx/j;

    return-object v0
.end method

.method private c(Lcom/twitter/model/av/AVMediaPlaylist;)Lcom/twitter/library/av/playback/AVMediaPlayer;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1161
    invoke-virtual {p0, v4}, Lcom/twitter/library/av/playback/AVPlayer;->c(Z)V

    .line 1163
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/playback/t;->c(Lcom/twitter/model/av/AVMediaPlaylist;)Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    .line 1164
    if-eqz v0, :cond_1

    .line 1166
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v1

    .line 1167
    if-eqz v1, :cond_0

    .line 1168
    iget-object v2, p0, Lcom/twitter/library/av/playback/AVPlayer;->r:Lbix;

    new-instance v3, Lbju;

    invoke-direct {v3, v1}, Lbju;-><init>(Lcom/twitter/model/av/AVMedia;)V

    invoke-virtual {v2, v3}, Lbix;->a(Lbiw;)V

    .line 1170
    :cond_0
    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/playback/AVMediaPlayer;)V

    .line 1172
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->q:Lcom/twitter/library/av/playback/y;

    const/16 v2, 0x2bd

    invoke-virtual {v1, v2, v4}, Lcom/twitter/library/av/playback/y;->b(II)V

    .line 1173
    invoke-interface {v0, v4}, Lcom/twitter/library/av/playback/AVMediaPlayer;->b(Z)V

    .line 1174
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->E()V

    .line 1177
    :cond_1
    return-object v0
.end method


# virtual methods
.method public A()Lcom/twitter/library/av/playback/aa;
    .locals 1

    .prologue
    .line 973
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->k()Lcom/twitter/library/av/playback/aa;

    move-result-object v0

    return-object v0
.end method

.method public B()V
    .locals 3

    .prologue
    .line 980
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    .line 981
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/t;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 982
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->r:Lbix;

    new-instance v2, Lbkf;

    invoke-direct {v2, v0}, Lbkf;-><init>(Lcom/twitter/model/av/AVMedia;)V

    invoke-virtual {v1, v2}, Lbix;->a(Lbiw;)V

    .line 984
    :cond_0
    return-void
.end method

.method C()Lcom/twitter/library/av/m;
    .locals 6

    .prologue
    .line 1075
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->x:Landroid/content/Context;

    .line 1076
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->O()Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v2

    .line 1077
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v3, v0, Landroid/content/res/Configuration;->orientation:I

    .line 1078
    iget-boolean v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->K:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->w:F

    const/4 v4, 0x0

    cmpl-float v0, v0, v4

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1079
    :goto_0
    new-instance v4, Lcom/twitter/library/av/m$a;

    invoke-direct {v4}, Lcom/twitter/library/av/m$a;-><init>()V

    .line 1081
    invoke-virtual {v4, v1}, Lcom/twitter/library/av/m$a;->a(Landroid/content/Context;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    iget-object v5, p0, Lcom/twitter/library/av/playback/AVPlayer;->y:Lcom/twitter/library/av/playback/u;

    .line 1082
    invoke-virtual {v5}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    .line 1083
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->R()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    .line 1084
    invoke-virtual {v1, v2}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/model/av/AVMediaPlaylist;)Lcom/twitter/library/av/m$a;

    move-result-object v5

    if-eqz v2, :cond_2

    .line 1085
    invoke-virtual {v2}, Lcom/twitter/model/av/AVMediaPlaylist;->b()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v5, v1}, Lcom/twitter/library/av/m$a;->c(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    .line 1086
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/model/av/AVMedia;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/av/playback/AVPlayer;->y:Lcom/twitter/library/av/playback/u;

    .line 1087
    invoke-virtual {v2}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v2

    invoke-interface {v2}, Lcom/twitter/library/av/playback/AVDataSource;->i()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/av/m$a;->a(Ljava/util/Map;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    .line 1088
    invoke-virtual {v1, v3}, Lcom/twitter/library/av/m$a;->a(I)Lcom/twitter/library/av/m$a;

    move-result-object v1

    .line 1089
    invoke-virtual {v1, v0}, Lcom/twitter/library/av/m$a;->a(Z)Lcom/twitter/library/av/m$a;

    move-result-object v0

    .line 1090
    invoke-direct {p0}, Lcom/twitter/library/av/playback/AVPlayer;->ad()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->b(I)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->h:Lbyf;

    .line 1091
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Lbyf;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    .line 1092
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->j()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Landroid/os/Bundle;)Lcom/twitter/library/av/m$a;

    .line 1093
    invoke-virtual {v4}, Lcom/twitter/library/av/m$a;->a()Lcom/twitter/library/av/m;

    move-result-object v0

    return-object v0

    .line 1078
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1085
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public D()V
    .locals 5

    .prologue
    .line 1112
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->i()J

    move-result-wide v0

    .line 1113
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 1114
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v2

    .line 1115
    if-eqz v2, :cond_0

    .line 1116
    iget-object v3, p0, Lcom/twitter/library/av/playback/AVPlayer;->r:Lbix;

    new-instance v4, Lbkh;

    invoke-direct {v4, v2, v0, v1}, Lbkh;-><init>(Lcom/twitter/model/av/AVMedia;J)V

    invoke-virtual {v3, v4}, Lbix;->a(Lbiw;)V

    .line 1118
    :cond_0
    invoke-direct {p0}, Lcom/twitter/library/av/playback/AVPlayer;->ae()V

    .line 1120
    :cond_1
    return-void
.end method

.method E()V
    .locals 4

    .prologue
    .line 1184
    invoke-static {}, Lcom/twitter/library/av/v;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1185
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    .line 1186
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->O()Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v1

    .line 1187
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 1188
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Player can\'t be started without setting a media and a playlist"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 1190
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 1191
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1192
    throw v0

    .line 1196
    :cond_1
    iget-object v2, p0, Lcom/twitter/library/av/playback/AVPlayer;->d:Lcom/twitter/library/av/g;

    iget-object v3, p0, Lcom/twitter/library/av/playback/AVPlayer;->y:Lcom/twitter/library/av/playback/u;

    invoke-virtual {v2, v3, v0, v1}, Lcom/twitter/library/av/g;->a(Lcom/twitter/library/av/playback/u;Lcom/twitter/model/av/AVMedia;Lcom/twitter/model/av/AVMediaPlaylist;)V

    .line 1198
    :cond_2
    return-void
.end method

.method public F()Lcom/twitter/model/av/AVMedia;
    .locals 1

    .prologue
    .line 1205
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->d()Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    return-object v0
.end method

.method public G()Z
    .locals 1

    .prologue
    .line 1209
    iget-boolean v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->u:Z

    return v0
.end method

.method public H()Lcom/twitter/library/av/n;
    .locals 1

    .prologue
    .line 1218
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->G:Lcom/twitter/library/av/n;

    return-object v0
.end method

.method public I()V
    .locals 1

    .prologue
    .line 1222
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->G:Lcom/twitter/library/av/n;

    invoke-virtual {v0}, Lcom/twitter/library/av/n;->a()V

    .line 1223
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->b:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 1224
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 1226
    :cond_0
    return-void
.end method

.method public J()Z
    .locals 1

    .prologue
    .line 1232
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->h()Z

    move-result v0

    return v0
.end method

.method public K()Lcom/twitter/library/av/j;
    .locals 1

    .prologue
    .line 1236
    sget-object v0, Lcom/twitter/library/av/playback/AVPlayer;->l:Lcom/twitter/library/av/j;

    return-object v0
.end method

.method public L()V
    .locals 1

    .prologue
    .line 1274
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVMediaPlayer;->D()V

    .line 1275
    return-void
.end method

.method public M()V
    .locals 1

    .prologue
    .line 1281
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVMediaPlayer;->F()V

    .line 1282
    return-void
.end method

.method public N()Lbyf;
    .locals 1

    .prologue
    .line 1286
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->h:Lbyf;

    return-object v0
.end method

.method public O()Lcom/twitter/model/av/AVMediaPlaylist;
    .locals 2

    .prologue
    .line 1294
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->p:[I

    monitor-enter v1

    .line 1295
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->a:Lcom/twitter/model/av/AVMediaPlaylist;

    monitor-exit v1

    return-object v0

    .line 1296
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public P()V
    .locals 1

    .prologue
    .line 1324
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1325
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->s()V

    .line 1327
    :cond_0
    return-void
.end method

.method public Q()Lcom/twitter/util/math/Size;
    .locals 1

    .prologue
    .line 1339
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->c:Lcom/twitter/util/math/Size;

    return-object v0
.end method

.method public R()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 1357
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->n:Lcom/twitter/library/av/playback/s;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/s;->a()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    .line 1358
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->f()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method S()Lcom/twitter/library/av/playback/m;
    .locals 1

    .prologue
    .line 1384
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->A:Lcom/twitter/library/av/playback/m;

    return-object v0
.end method

.method public T()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1389
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->x:Landroid/content/Context;

    return-object v0
.end method

.method public U()Z
    .locals 1

    .prologue
    .line 1421
    iget-boolean v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->K:Z

    return v0
.end method

.method public V()Z
    .locals 2

    .prologue
    .line 1496
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->W()F

    move-result v0

    const v1, 0x3f7fbe77    # 0.999f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public W()F
    .locals 1

    .prologue
    .line 1503
    iget v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->J:F

    return v0
.end method

.method a(Z)J
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 573
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->s()V

    .line 576
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->g()J

    move-result-wide v0

    .line 578
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->o()Lcom/twitter/library/av/k;

    move-result-object v2

    .line 579
    if-eqz v2, :cond_1

    .line 580
    invoke-interface {v2}, Lcom/twitter/library/av/k;->l()V

    .line 582
    :cond_1
    return-wide v0
.end method

.method protected a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/m$a;)Lcom/twitter/library/av/m$a;
    .locals 5

    .prologue
    .line 1047
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->O()Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v1

    .line 1048
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v2, v0, Landroid/content/res/Configuration;->orientation:I

    .line 1049
    iget-boolean v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->K:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->w:F

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1051
    :goto_0
    invoke-virtual {p5, p1}, Lcom/twitter/library/av/m$a;->a(Landroid/content/Context;)Lcom/twitter/library/av/m$a;

    move-result-object v3

    .line 1052
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->R()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/av/m$a;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/library/av/playback/AVPlayer;->y:Lcom/twitter/library/av/playback/u;

    .line 1053
    invoke-virtual {v4}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/library/av/m$a;

    move-result-object v3

    .line 1054
    invoke-virtual {v3, p2}, Lcom/twitter/library/av/m$a;->a(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v3

    .line 1055
    invoke-virtual {v3, v1}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/model/av/AVMediaPlaylist;)Lcom/twitter/library/av/m$a;

    move-result-object v3

    if-eqz v1, :cond_2

    .line 1056
    invoke-virtual {v1}, Lcom/twitter/model/av/AVMediaPlaylist;->b()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v3, v1}, Lcom/twitter/library/av/m$a;->c(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    .line 1057
    invoke-virtual {v1, p4}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/model/av/AVMedia;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/library/av/playback/AVPlayer;->y:Lcom/twitter/library/av/playback/u;

    .line 1058
    invoke-virtual {v3}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v3

    invoke-interface {v3}, Lcom/twitter/library/av/playback/AVDataSource;->i()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/twitter/library/av/m$a;->a(Ljava/util/Map;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    .line 1059
    invoke-virtual {v1, v2}, Lcom/twitter/library/av/m$a;->a(I)Lcom/twitter/library/av/m$a;

    move-result-object v1

    .line 1060
    invoke-virtual {v1, v0}, Lcom/twitter/library/av/m$a;->a(Z)Lcom/twitter/library/av/m$a;

    move-result-object v0

    .line 1061
    invoke-direct {p0}, Lcom/twitter/library/av/playback/AVPlayer;->ad()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->b(I)Lcom/twitter/library/av/m$a;

    move-result-object v0

    .line 1062
    invoke-virtual {v0, p3}, Lcom/twitter/library/av/m$a;->b(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->h:Lbyf;

    .line 1063
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Lbyf;)Lcom/twitter/library/av/m$a;

    move-result-object v0

    .line 1064
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->j()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/m$a;->a(Landroid/os/Bundle;)Lcom/twitter/library/av/m$a;

    .line 1065
    return-object p5

    .line 1049
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1056
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected a(Landroid/content/Context;)Lcom/twitter/model/av/AVMediaPlaylist;
    .locals 1

    .prologue
    .line 453
    invoke-direct {p0}, Lcom/twitter/library/av/playback/AVPlayer;->Y()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->O()Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/library/av/playback/AVPlayer;->b(Landroid/content/Context;)Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 409
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/twitter/library/av/playback/AVMediaPlayer;->b(Z)V

    .line 410
    return-void
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 1400
    iput p1, p0, Lcom/twitter/library/av/playback/AVPlayer;->w:F

    .line 1401
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/playback/AVMediaPlayer;)V

    .line 1402
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 966
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/twitter/library/av/playback/AVMediaPlayer;->b(J)V

    .line 967
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 1311
    invoke-virtual {p1}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1312
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->s()V

    .line 1314
    :cond_0
    return-void
.end method

.method public a(Landroid/view/Surface;)V
    .locals 1

    .prologue
    .line 725
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->b:Ljava/lang/ref/WeakReference;

    .line 726
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/twitter/library/av/playback/AVMediaPlayer;->a(Landroid/view/Surface;)V

    .line 727
    return-void
.end method

.method a(Lbyf;)V
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 1368
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->h:Lbyf;

    if-eq p1, v0, :cond_2

    .line 1369
    iput-object p1, p0, Lcom/twitter/library/av/playback/AVPlayer;->h:Lbyf;

    .line 1370
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    .line 1371
    if-eqz v0, :cond_0

    .line 1372
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->r:Lbix;

    new-instance v2, Lbjz;

    invoke-direct {v2, v0, p1}, Lbjz;-><init>(Lcom/twitter/model/av/AVMedia;Lbyf;)V

    invoke-virtual {v1, v2}, Lbix;->a(Lbiw;)V

    .line 1374
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1375
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVMediaPlayer;->w()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->u:Z

    .line 1377
    :cond_1
    invoke-interface {p1}, Lbyf;->b()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/AVPlayer;->d(Z)V

    .line 1378
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/playback/AVMediaPlayer;)V

    .line 1380
    :cond_2
    return-void
.end method

.method public a(Lcom/twitter/library/av/k;)V
    .locals 5

    .prologue
    .line 360
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    .line 361
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->O()Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v1

    .line 362
    iget-object v2, p0, Lcom/twitter/library/av/playback/AVPlayer;->c:Lcom/twitter/util/math/Size;

    invoke-virtual {v2}, Lcom/twitter/util/math/Size;->e()Z

    move-result v2

    if-nez v2, :cond_0

    .line 363
    iget-object v2, p0, Lcom/twitter/library/av/playback/AVPlayer;->c:Lcom/twitter/util/math/Size;

    invoke-virtual {v2}, Lcom/twitter/util/math/Size;->a()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/library/av/playback/AVPlayer;->c:Lcom/twitter/util/math/Size;

    invoke-virtual {v3}, Lcom/twitter/util/math/Size;->b()I

    move-result v3

    invoke-interface {p1, v2, v3}, Lcom/twitter/library/av/k;->a(II)V

    .line 365
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->z()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 366
    :cond_1
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->c:Lcom/twitter/util/math/Size;

    invoke-virtual {v1}, Lcom/twitter/util/math/Size;->a()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/library/av/playback/AVPlayer;->c:Lcom/twitter/util/math/Size;

    invoke-virtual {v2}, Lcom/twitter/util/math/Size;->b()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->y()Z

    move-result v3

    const/4 v4, 0x0

    invoke-interface {p1, v1, v2, v3, v4}, Lcom/twitter/library/av/k;->a(IIZZ)V

    .line 380
    :cond_2
    :goto_0
    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVMediaPlayer;->v()Z

    move-result v1

    if-nez v1, :cond_3

    .line 381
    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVMediaPlayer;->H()Lcom/twitter/library/av/playback/aa;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/twitter/library/av/k;->a(Lcom/twitter/library/av/playback/aa;)V

    .line 383
    :cond_3
    return-void

    .line 367
    :cond_4
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->y()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 368
    invoke-interface {p1}, Lcom/twitter/library/av/k;->f()V

    goto :goto_0

    .line 369
    :cond_5
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->w()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 370
    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVMediaPlayer;->v()Z

    move-result v1

    if-nez v1, :cond_2

    .line 372
    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVMediaPlayer;->g()I

    move-result v1

    .line 373
    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVMediaPlayer;->h()Ljava/lang/String;

    move-result-object v2

    .line 371
    invoke-interface {p1, v1, v2}, Lcom/twitter/library/av/k;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 375
    :cond_6
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/twitter/model/av/AVMediaPlaylist;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 377
    invoke-virtual {v1}, Lcom/twitter/model/av/AVMediaPlaylist;->f()Lcom/twitter/model/av/c;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/twitter/library/av/k;->a(Lcom/twitter/model/av/c;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/av/m;)V
    .locals 2

    .prologue
    .line 1017
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->o:Lcom/twitter/library/av/playback/f;

    const-string/jumbo v1, "AVPlayer.EVENT_LOG_ANALYTICS_EVENT"

    invoke-interface {v0, v1, p1}, Lcom/twitter/library/av/playback/f;->a(Ljava/lang/String;Lcom/twitter/library/av/m;)V

    .line 1020
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->o:Lcom/twitter/library/av/playback/f;

    const-string/jumbo v1, "AVPlayer.EVENT_PROMOTED_LOGGING_EVENT"

    invoke-interface {v0, v1, p1}, Lcom/twitter/library/av/playback/f;->a(Ljava/lang/String;Lcom/twitter/library/av/m;)V

    .line 1021
    return-void
.end method

.method a(Lcom/twitter/library/av/playback/AVMediaPlayer;)V
    .locals 3

    .prologue
    .line 431
    invoke-direct {p0}, Lcom/twitter/library/av/playback/AVPlayer;->X()I

    move-result v0

    int-to-float v0, v0

    .line 432
    const/16 v1, 0x64

    iget v2, p0, Lcom/twitter/library/av/playback/AVPlayer;->w:F

    mul-float/2addr v0, v2

    .line 433
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 432
    invoke-static {v1, v0}, Lbrq;->a(II)F

    move-result v0

    invoke-interface {p1, v0}, Lcom/twitter/library/av/playback/AVMediaPlayer;->a(F)V

    .line 434
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayer$PlayerPauseType;)V
    .locals 3

    .prologue
    .line 884
    iput-object p1, p0, Lcom/twitter/library/av/playback/AVPlayer;->g:Lcom/twitter/library/av/playback/AVPlayer$PlayerPauseType;

    .line 885
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->u:Z

    .line 886
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->t:Lbrq;

    invoke-virtual {v0, p0}, Lbrq;->b(Lbrq$a;)V

    .line 887
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 888
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    .line 889
    if-eqz v0, :cond_0

    .line 890
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->r:Lbix;

    new-instance v2, Lbjw;

    invoke-direct {v2, v0}, Lbjw;-><init>(Lcom/twitter/model/av/AVMedia;)V

    invoke-virtual {v1, v2}, Lbix;->a(Lbiw;)V

    .line 892
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVMediaPlayer;->x()V

    .line 898
    :goto_0
    return-void

    .line 896
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->q:Lcom/twitter/library/av/playback/y;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/y;->j()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayer$b;)V
    .locals 3

    .prologue
    .line 506
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->O()Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v1

    .line 507
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/twitter/model/av/AVMediaPlaylist;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 508
    :goto_0
    iget-object v2, p0, Lcom/twitter/library/av/playback/AVPlayer;->B:Lrx/j;

    if-nez v2, :cond_2

    if-nez v1, :cond_2

    .line 509
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->F:Lcom/twitter/library/av/playback/AVPlayer$c;

    invoke-virtual {v0, p0}, Lcom/twitter/library/av/playback/AVPlayer$c;->a(Lcom/twitter/library/av/playback/AVPlayer;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/av/playback/AVPlayer$e;

    invoke-direct {v1, p0, p1}, Lcom/twitter/library/av/playback/AVPlayer$e;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/library/av/playback/AVPlayer$b;)V

    .line 510
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->B:Lrx/j;

    .line 514
    :cond_0
    :goto_1
    return-void

    .line 507
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 511
    :cond_2
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 512
    invoke-virtual {v1}, Lcom/twitter/model/av/AVMediaPlaylist;->f()Lcom/twitter/model/av/c;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/twitter/library/av/playback/AVPlayer$b;->a(Lcom/twitter/model/av/c;)V

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/av/playback/aa;)V
    .locals 1

    .prologue
    .line 1105
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->q:Lcom/twitter/library/av/playback/y;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/playback/y;->a(Lcom/twitter/library/av/playback/aa;)V

    .line 1106
    return-void
.end method

.method public a(Lcom/twitter/library/client/v;)V
    .locals 2

    .prologue
    .line 394
    iput-object p1, p0, Lcom/twitter/library/av/playback/AVPlayer;->D:Lcom/twitter/library/client/v;

    .line 396
    new-instance v0, Lcom/twitter/library/av/playback/AVPlayer$g;

    invoke-direct {v0, p0}, Lcom/twitter/library/av/playback/AVPlayer$g;-><init>(Lcom/twitter/library/av/playback/AVPlayer;)V

    iput-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->E:Lcom/twitter/library/av/playback/AVPlayer$g;

    .line 397
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->D:Lcom/twitter/library/client/v;

    if-eqz v0, :cond_0

    .line 398
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->D:Lcom/twitter/library/client/v;

    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->E:Lcom/twitter/library/av/playback/AVPlayer$g;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/u;)V

    .line 400
    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/model/av/AVMediaPlaylist;)V
    .locals 3

    .prologue
    .line 543
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/model/av/AVMediaPlaylist;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 552
    :cond_0
    :goto_0
    return-void

    .line 547
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->e:Z

    .line 548
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/playback/t;->a(Lcom/twitter/model/av/AVMediaPlaylist;)Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    .line 549
    if-eqz v0, :cond_0

    .line 550
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->r:Lbix;

    new-instance v2, Lbju;

    invoke-direct {v2, v0}, Lbju;-><init>(Lcom/twitter/model/av/AVMedia;)V

    invoke-virtual {v1, v2}, Lbix;->a(Lbiw;)V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/model/av/AVMediaPlaylist;Landroid/content/res/Resources;)V
    .locals 2

    .prologue
    .line 471
    .line 472
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/model/av/AVMediaPlaylist;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 473
    invoke-virtual {p0, p1}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/model/av/AVMediaPlaylist;)V

    .line 480
    :goto_0
    return-void

    .line 475
    :cond_0
    if-eqz p1, :cond_1

    .line 476
    invoke-virtual {p1}, Lcom/twitter/model/av/AVMediaPlaylist;->f()Lcom/twitter/model/av/c;

    move-result-object v0

    .line 478
    :goto_1
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->q:Lcom/twitter/library/av/playback/y;

    invoke-virtual {v1, v0}, Lcom/twitter/library/av/playback/y;->a(Lcom/twitter/model/av/c;)V

    goto :goto_0

    .line 476
    :cond_1
    sget v0, Lazw$k;->av_playlist_download_failed:I

    .line 477
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 476
    invoke-static {v0}, Lcom/twitter/model/av/c;->a(Ljava/lang/String;)Lcom/twitter/model/av/c;

    move-result-object v0

    goto :goto_1
.end method

.method public a(Lcom/twitter/model/av/DynamicAdId;Lcom/twitter/model/av/DynamicAdInfo;)V
    .locals 2

    .prologue
    .line 1457
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->y:Lcom/twitter/library/av/playback/u;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->m()Lcom/twitter/library/av/playback/ah;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/twitter/library/av/playback/ah;->a(Lcom/twitter/model/av/DynamicAdId;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1478
    :cond_0
    :goto_0
    return-void

    .line 1461
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->O()Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v0

    .line 1466
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/model/av/AVMediaPlaylist;->k()Lcom/twitter/model/av/AVMedia;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/model/av/AVMediaPlaylist;->i()Lcom/twitter/model/av/DynamicAdInfo;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1472
    iget-object v1, p2, Lcom/twitter/model/av/DynamicAdInfo;->a:Lcom/twitter/model/av/DynamicAd;

    invoke-direct {p0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/model/av/DynamicAd;)Lcom/twitter/util/collection/k;

    move-result-object v1

    .line 1473
    invoke-virtual {v0, p2, v1}, Lcom/twitter/model/av/AVMediaPlaylist;->a(Lcom/twitter/model/av/DynamicAdInfo;Lcom/twitter/util/collection/k;)Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/AVPlayer;->b(Lcom/twitter/model/av/AVMediaPlaylist;)V

    .line 1475
    invoke-virtual {v1}, Lcom/twitter/util/collection/k;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1476
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->O()Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/t;->b(Lcom/twitter/model/av/AVMediaPlaylist;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/twitter/library/av/m$a;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 987
    invoke-virtual {p0, p1, v0, v0, p2}, Lcom/twitter/library/av/playback/AVPlayer;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/m$a;)V

    .line 988
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;)V
    .locals 1

    .prologue
    .line 991
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/m$a;)V

    .line 992
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/m$a;)V
    .locals 6

    .prologue
    .line 1001
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->x:Landroid/content/Context;

    .line 1003
    if-nez p3, :cond_1

    .line 1004
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v4

    .line 1007
    :goto_0
    if-eqz p4, :cond_0

    move-object v5, p4

    :goto_1
    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    .line 1009
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/av/playback/AVPlayer;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/m$a;)Lcom/twitter/library/av/m$a;

    .line 1010
    invoke-virtual {v5}, Lcom/twitter/library/av/m$a;->a()Lcom/twitter/library/av/m;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/m;)V

    .line 1011
    return-void

    .line 1007
    :cond_0
    new-instance v5, Lcom/twitter/library/av/m$a;

    invoke-direct {v5}, Lcom/twitter/library/av/m$a;-><init>()V

    goto :goto_1

    :cond_1
    move-object v4, p3

    goto :goto_0
.end method

.method protected a(ZZ)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 623
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->O()Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v0

    .line 624
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->q()V

    .line 626
    iget-boolean v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->C:Z

    if-eqz v1, :cond_0

    .line 627
    invoke-static {}, Lcom/twitter/util/android/b;->a()Lcom/twitter/util/android/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/twitter/util/android/b;->b(Lcom/twitter/util/android/b$a;)Z

    .line 628
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->C:Z

    .line 631
    :cond_0
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->t:Lbrq;

    if-eqz v1, :cond_1

    .line 632
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->t:Lbrq;

    invoke-virtual {v1, p0}, Lbrq;->b(Lbrq$a;)V

    .line 635
    :cond_1
    iget-boolean v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->e:Z

    if-nez v1, :cond_3

    .line 637
    if-eqz v0, :cond_2

    .line 638
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v1

    .line 639
    if-eqz v1, :cond_2

    .line 640
    iget-object v4, p0, Lcom/twitter/library/av/playback/AVPlayer;->r:Lbix;

    new-instance v5, Lbjp;

    invoke-direct {v5, v1, v0}, Lbjp;-><init>(Lcom/twitter/model/av/AVMedia;Lcom/twitter/model/av/AVMediaPlaylist;)V

    invoke-virtual {v4, v5}, Lbix;->a(Lbiw;)V

    .line 643
    :cond_2
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->r:Lbix;

    new-instance v1, Lbji;

    invoke-direct {v1}, Lbji;-><init>()V

    invoke-virtual {v0, v1}, Lbix;->a(Lbiw;)V

    .line 649
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->y()Z

    move-result v4

    .line 650
    invoke-virtual {p0, p2}, Lcom/twitter/library/av/playback/AVPlayer;->a(Z)J

    move-result-wide v0

    .line 651
    if-nez v4, :cond_9

    :goto_0
    iput-wide v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->i:J

    .line 653
    if-eqz p1, :cond_4

    .line 654
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->I()V

    .line 657
    :cond_4
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->D:Lcom/twitter/library/client/v;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->E:Lcom/twitter/library/av/playback/AVPlayer$g;

    if-eqz v0, :cond_5

    .line 658
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->D:Lcom/twitter/library/client/v;

    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->E:Lcom/twitter/library/av/playback/AVPlayer$g;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/u;)V

    .line 659
    iput-object v7, p0, Lcom/twitter/library/av/playback/AVPlayer;->E:Lcom/twitter/library/av/playback/AVPlayer$g;

    .line 662
    :cond_5
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->f()V

    .line 663
    iput-boolean v6, p0, Lcom/twitter/library/av/playback/AVPlayer;->u:Z

    .line 665
    if-eqz p2, :cond_8

    .line 666
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->H:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    .line 667
    sget-object v0, Lbyo;->a:Lbyf;

    iput-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->h:Lbyf;

    .line 668
    iput-wide v2, p0, Lcom/twitter/library/av/playback/AVPlayer;->i:J

    .line 669
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->p:[I

    monitor-enter v1

    .line 670
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->a:Lcom/twitter/model/av/AVMediaPlaylist;

    .line 671
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 673
    iget-boolean v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->I:Z

    if-nez v0, :cond_7

    .line 675
    :try_start_1
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->x:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->s:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    .line 685
    :cond_6
    iput-boolean v6, p0, Lcom/twitter/library/av/playback/AVPlayer;->I:Z

    .line 687
    :cond_7
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->d:Lcom/twitter/library/av/g;

    invoke-virtual {v0, p0}, Lcom/twitter/library/av/g;->b(Lcom/twitter/library/av/g$a;)Z

    .line 690
    :cond_8
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->l()V

    .line 693
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->r:Lbix;

    invoke-virtual {v0}, Lbix;->a()V

    .line 695
    iput-boolean v6, p0, Lcom/twitter/library/av/playback/AVPlayer;->e:Z

    .line 696
    return-void

    :cond_9
    move-wide v0, v2

    .line 651
    goto :goto_0

    .line 671
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 676
    :catch_0
    move-exception v0

    .line 678
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 679
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 681
    throw v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVMediaPlayer;->x()V

    .line 405
    return-void
.end method

.method public b(F)V
    .locals 3

    .prologue
    .line 1486
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 1487
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "visibility percentage must be 0 - 1.0! received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 1489
    :cond_1
    iput p1, p0, Lcom/twitter/library/av/playback/AVPlayer;->J:F

    .line 1490
    return-void
.end method

.method public b(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1318
    return-void
.end method

.method public b(Lcom/twitter/model/av/AVMediaPlaylist;)V
    .locals 2

    .prologue
    .line 1300
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->a:Lcom/twitter/model/av/AVMediaPlaylist;

    if-ne p1, v0, :cond_0

    .line 1306
    :goto_0
    return-void

    .line 1303
    :cond_0
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->p:[I

    monitor-enter v1

    .line 1304
    :try_start_0
    iput-object p1, p0, Lcom/twitter/library/av/playback/AVPlayer;->a:Lcom/twitter/model/av/AVMediaPlaylist;

    .line 1305
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Z)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 760
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->e()Lcom/twitter/library/av/playback/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->d()I

    move-result v0

    const/4 v4, 0x7

    if-ne v0, v4, :cond_9

    move v0, v1

    .line 764
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->w()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->x()Z

    move-result v4

    if-eqz v4, :cond_1

    if-eqz v0, :cond_1

    .line 766
    :cond_0
    iget-object v4, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v4}, Lcom/twitter/library/av/playback/t;->g()J

    .line 772
    if-eqz v0, :cond_1

    .line 773
    invoke-virtual {p0, v3}, Lcom/twitter/library/av/playback/AVPlayer;->b(Lcom/twitter/model/av/AVMediaPlaylist;)V

    .line 777
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->k:Z

    if-nez v0, :cond_2

    .line 778
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->r:Lbix;

    new-instance v4, Lbkg;

    invoke-direct {v4}, Lbkg;-><init>()V

    invoke-virtual {v0, v4}, Lbix;->a(Lbiw;)V

    .line 779
    iput-boolean v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->k:Z

    .line 782
    :cond_2
    invoke-direct {p0}, Lcom/twitter/library/av/playback/AVPlayer;->ac()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 783
    iget-boolean v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->C:Z

    if-nez v0, :cond_3

    .line 784
    invoke-static {}, Lcom/twitter/util/android/b;->a()Lcom/twitter/util/android/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/util/android/b;->a(Lcom/twitter/util/android/b$a;)V

    .line 785
    iput-boolean v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->C:Z

    .line 790
    :cond_3
    if-eqz p1, :cond_b

    .line 791
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    if-eqz v0, :cond_a

    new-instance v0, Lbkd;

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v3

    invoke-direct {v0, v3}, Lbkd;-><init>(Lcom/twitter/model/av/AVMedia;)V

    :goto_1
    move-object v3, v0

    .line 799
    :cond_4
    :goto_2
    if-eqz v3, :cond_5

    .line 800
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->r:Lbix;

    invoke-virtual {v0, v3}, Lbix;->a(Lbiw;)V

    .line 803
    :cond_5
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->z:Lcom/twitter/library/av/playback/q;

    iget-object v3, p0, Lcom/twitter/library/av/playback/AVPlayer;->y:Lcom/twitter/library/av/playback/u;

    invoke-virtual {v0, v3}, Lcom/twitter/library/av/playback/q;->a(Lcom/twitter/library/av/playback/u;)V

    .line 805
    invoke-direct {p0}, Lcom/twitter/library/av/playback/AVPlayer;->ag()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->t:Lbrq;

    invoke-virtual {v0, p0}, Lbrq;->a(Lbrq$a;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 806
    :cond_6
    iput-boolean v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->u:Z

    .line 807
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/twitter/library/av/playback/AVMediaPlayer;->b(Z)V

    .line 822
    :cond_7
    :goto_3
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->w()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->x()Z

    move-result v0

    if-nez v0, :cond_8

    .line 823
    iput-boolean p1, p0, Lcom/twitter/library/av/playback/AVPlayer;->j:Z

    .line 824
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->q:Lcom/twitter/library/av/playback/y;

    const/16 v1, 0x2bd

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/av/playback/y;->b(II)V

    .line 826
    :cond_8
    return-void

    :cond_9
    move v0, v2

    .line 760
    goto/16 :goto_0

    :cond_a
    move-object v0, v3

    .line 791
    goto :goto_1

    .line 792
    :cond_b
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVMediaPlayer;->y()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 793
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    if-eqz v0, :cond_4

    new-instance v3, Lbke;

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    invoke-direct {v3, v0}, Lbke;-><init>(Lcom/twitter/model/av/AVMedia;)V

    goto :goto_2

    .line 795
    :cond_c
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->E()V

    .line 796
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    if-eqz v0, :cond_4

    new-instance v3, Lbjx;

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    invoke-direct {v3, v0}, Lbjx;-><init>(Lcom/twitter/model/av/AVMedia;)V

    goto :goto_2

    .line 809
    :cond_d
    invoke-direct {p0}, Lcom/twitter/library/av/playback/AVPlayer;->Z()V

    goto :goto_3

    .line 811
    :cond_e
    invoke-direct {p0}, Lcom/twitter/library/av/playback/AVPlayer;->ab()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 812
    invoke-virtual {p0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->c(Z)V

    .line 813
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->O()Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/model/av/AVMediaPlaylist;)V

    goto :goto_3

    .line 814
    :cond_f
    invoke-direct {p0}, Lcom/twitter/library/av/playback/AVPlayer;->aa()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 815
    invoke-virtual {p0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->c(Z)V

    .line 816
    invoke-virtual {p0, v3}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/playback/AVPlayer$b;)V

    goto :goto_3
.end method

.method public b(Landroid/view/Surface;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 736
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eq p1, v0, :cond_0

    .line 737
    const/4 v0, 0x0

    .line 742
    :goto_0
    return v0

    .line 740
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->b:Ljava/lang/ref/WeakReference;

    .line 741
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/twitter/library/av/playback/AVMediaPlayer;->a(Landroid/view/Surface;)V

    .line 742
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bo_()V
    .locals 0

    .prologue
    .line 1442
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 414
    const/16 v0, 0x32

    iput v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->v:I

    .line 415
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/playback/AVMediaPlayer;)V

    .line 416
    return-void
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 1213
    iput-boolean p1, p0, Lcom/twitter/library/av/playback/AVPlayer;->u:Z

    .line 1214
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 420
    const/16 v0, 0x64

    iput v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->v:I

    .line 421
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/playback/AVMediaPlayer;)V

    .line 422
    return-void
.end method

.method public d(Z)V
    .locals 1

    .prologue
    .line 1408
    iput-boolean p1, p0, Lcom/twitter/library/av/playback/AVPlayer;->K:Z

    .line 1409
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/playback/AVMediaPlayer;)V

    .line 1410
    if-eqz p1, :cond_1

    .line 1411
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->t:Lbrq;

    invoke-virtual {v0, p0}, Lbrq;->b(Lbrq$a;)V

    .line 1415
    :cond_0
    :goto_0
    return-void

    .line 1412
    :cond_1
    invoke-direct {p0}, Lcom/twitter/library/av/playback/AVPlayer;->ah()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1413
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->t:Lbrq;

    invoke-virtual {v0, p0}, Lbrq;->a(Lbrq$a;)Z

    goto :goto_0
.end method

.method public e()Lcom/twitter/library/av/playback/u;
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->y:Lcom/twitter/library/av/playback/u;

    return-object v0
.end method

.method public f()Lcom/twitter/library/av/playback/s;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->n:Lcom/twitter/library/av/playback/s;

    return-object v0
.end method

.method public g()Lcom/twitter/library/av/playback/AVMediaPlayer;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    return-object v0
.end method

.method public h()Lbix;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->r:Lbix;

    return-object v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->h:Lbyf;

    invoke-interface {v0}, Lbyf;->e()Z

    move-result v0

    return v0
.end method

.method public j()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->H:Landroid/os/Bundle;

    return-object v0
.end method

.method public k()Lbyf;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->n:Lcom/twitter/library/av/playback/s;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/s;->a()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    .line 336
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->b()Lbyf;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()V
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->B:Lrx/j;

    if-eqz v0, :cond_0

    .line 521
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->B:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 522
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->B:Lrx/j;

    .line 524
    :cond_0
    return-void
.end method

.method public m()V
    .locals 2

    .prologue
    .line 531
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 532
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->O()Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->x:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/model/av/AVMediaPlaylist;Landroid/content/res/Resources;)V

    .line 534
    :cond_0
    return-void
.end method

.method public n()J
    .locals 2

    .prologue
    .line 560
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Z)J

    move-result-wide v0

    return-wide v0
.end method

.method protected o()Lcom/twitter/library/av/k;
    .locals 1

    .prologue
    .line 594
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->n:Lcom/twitter/library/av/playback/s;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/s;->a()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    .line 595
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->d()Lcom/twitter/library/av/k;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2

    .prologue
    .line 1124
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-direct {p0}, Lcom/twitter/library/av/playback/AVPlayer;->af()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/t;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1129
    :goto_0
    return-void

    .line 1128
    :cond_0
    invoke-direct {p0}, Lcom/twitter/library/av/playback/AVPlayer;->ae()V

    goto :goto_0
.end method

.method public p()Landroid/view/Surface;
    .locals 1

    .prologue
    .line 603
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Surface;

    return-object v0
.end method

.method protected q()V
    .locals 2

    .prologue
    .line 707
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->n:Lcom/twitter/library/av/playback/s;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/s;->b()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 708
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->d()Lcom/twitter/library/av/k;

    move-result-object v0

    .line 709
    if-eqz v0, :cond_0

    .line 710
    invoke-interface {v0}, Lcom/twitter/library/av/k;->k()V

    goto :goto_0

    .line 713
    :cond_1
    return-void
.end method

.method public r()Lcom/twitter/library/av/playback/AVPlayer$PlayerPauseType;
    .locals 1

    .prologue
    .line 868
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->g:Lcom/twitter/library/av/playback/AVPlayer$PlayerPauseType;

    return-object v0
.end method

.method public s()V
    .locals 1

    .prologue
    .line 875
    sget-object v0, Lcom/twitter/library/av/playback/AVPlayer$PlayerPauseType;->b:Lcom/twitter/library/av/playback/AVPlayer$PlayerPauseType;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/playback/AVPlayer$PlayerPauseType;)V

    .line 876
    return-void
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 904
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->j()Z

    move-result v0

    return v0
.end method

.method public u()Z
    .locals 2

    .prologue
    .line 913
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    .line 914
    invoke-direct {p0}, Lcom/twitter/library/av/playback/AVPlayer;->aa()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer;->B:Lrx/j;

    if-nez v1, :cond_1

    :cond_0
    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVMediaPlayer;->z()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Z
    .locals 1

    .prologue
    .line 921
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVMediaPlayer;->w()Z

    move-result v0

    return v0
.end method

.method public w()Z
    .locals 1

    .prologue
    .line 931
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVMediaPlayer;->B()Z

    move-result v0

    return v0
.end method

.method public x()Z
    .locals 1

    .prologue
    .line 941
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/AVPlayer;->O()Lcom/twitter/model/av/AVMediaPlaylist;

    move-result-object v0

    .line 942
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/model/av/AVMediaPlaylist;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public y()Z
    .locals 1

    .prologue
    .line 950
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVMediaPlayer;->C()Z

    move-result v0

    return v0
.end method

.method public z()Z
    .locals 1

    .prologue
    .line 957
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer;->f:Lcom/twitter/library/av/playback/t;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/t;->c()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVMediaPlayer;->y()Z

    move-result v0

    return v0
.end method
