.class Lcom/twitter/library/av/playback/AVPlayer$f;
.super Landroid/content/BroadcastReceiver;
.source "Twttr"


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/av/playback/AVPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "f"
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/av/playback/AVPlayer;

.field private final b:Lcom/twitter/library/av/playback/y;


# direct methods
.method constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/library/av/playback/y;)V
    .locals 0

    .prologue
    .line 1702
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 1703
    iput-object p1, p0, Lcom/twitter/library/av/playback/AVPlayer$f;->a:Lcom/twitter/library/av/playback/AVPlayer;

    .line 1704
    iput-object p2, p0, Lcom/twitter/library/av/playback/AVPlayer$f;->b:Lcom/twitter/library/av/playback/y;

    .line 1705
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1709
    const-string/jumbo v0, "android.intent.action.SCREEN_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1710
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer$f;->b:Lcom/twitter/library/av/playback/y;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/y;->a(Z)V

    .line 1711
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer$f;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1712
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer$f;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->s()V

    .line 1714
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer$f;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0, v2}, Lcom/twitter/library/av/playback/AVPlayer;->a(Z)J

    .line 1718
    :cond_1
    :goto_0
    return-void

    .line 1715
    :cond_2
    const-string/jumbo v0, "android.intent.action.SCREEN_ON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1716
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer$f;->b:Lcom/twitter/library/av/playback/y;

    invoke-virtual {v0, v2}, Lcom/twitter/library/av/playback/y;->a(Z)V

    goto :goto_0
.end method
