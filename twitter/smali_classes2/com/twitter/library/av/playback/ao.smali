.class public Lcom/twitter/library/av/playback/ao;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcoi$a;

.field private final b:J

.field private final c:J

.field private final d:F


# direct methods
.method constructor <init>(F)V
    .locals 4

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string/jumbo v0, "video_hls_initial_quality_regulator_5487"

    invoke-static {v0}, Lcoi;->f(Ljava/lang/String;)Lcoi$a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/playback/ao;->a:Lcoi$a;

    .line 23
    const-string/jumbo v0, "video_hls_initial_quality_start_range"

    const-wide/32 v2, 0x7a120

    invoke-static {v0, v2, v3}, Lcoj;->a(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/av/playback/ao;->b:J

    .line 25
    const-string/jumbo v0, "video_hls_initial_quality_end_range"

    const-wide/32 v2, 0xdbba0

    invoke-static {v0, v2, v3}, Lcoj;->a(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/av/playback/ao;->c:J

    .line 27
    iput p1, p0, Lcom/twitter/library/av/playback/ao;->d:F

    .line 28
    return-void
.end method

.method private a([Lcom/google/android/exoplayer/hls/Variant;)I
    .locals 6

    .prologue
    .line 77
    array-length v1, p1

    .line 79
    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 80
    aget-object v2, p1, v0

    iget-object v2, v2, Lcom/google/android/exoplayer/hls/Variant;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget v2, v2, Lcom/google/android/exoplayer/chunk/Format;->bitrate:I

    int-to-long v2, v2

    iget-wide v4, p0, Lcom/twitter/library/av/playback/ao;->b:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    aget-object v2, p1, v0

    iget-object v2, v2, Lcom/google/android/exoplayer/hls/Variant;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget v2, v2, Lcom/google/android/exoplayer/chunk/Format;->bitrate:I

    int-to-long v2, v2

    iget-wide v4, p0, Lcom/twitter/library/av/playback/ao;->c:J

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    .line 86
    :goto_1
    return v0

    .line 79
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 86
    :cond_1
    add-int/lit8 v0, v1, -0x1

    goto :goto_1
.end method

.method private a(IJ[Lcom/google/android/exoplayer/hls/Variant;[JJ)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 68
    const-wide/16 v2, -0x1

    cmp-long v0, p2, v2

    if-nez v0, :cond_2

    move v0, v1

    .line 71
    :goto_0
    aget-wide v2, p5, p1

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    aget-object v2, p4, p1

    iget-object v2, v2, Lcom/google/android/exoplayer/hls/Variant;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget v2, v2, Lcom/google/android/exoplayer/chunk/Format;->bitrate:I

    int-to-long v2, v2

    cmp-long v2, v2, p6

    if-gtz v2, :cond_1

    if-eqz v0, :cond_0

    aget-object v2, p4, p1

    iget-object v2, v2, Lcom/google/android/exoplayer/hls/Variant;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget v2, v2, Lcom/google/android/exoplayer/chunk/Format;->bitrate:I

    if-gt v2, v0, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    return v1

    .line 68
    :cond_2
    long-to-float v0, p2

    iget v2, p0, Lcom/twitter/library/av/playback/ao;->d:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    goto :goto_0
.end method


# virtual methods
.method public a(J[Lcom/google/android/exoplayer/hls/Variant;[JJ)I
    .locals 11

    .prologue
    .line 40
    array-length v9, p3

    .line 41
    const/4 v0, 0x1

    if-gt v9, v0, :cond_1

    .line 42
    const/4 v1, 0x0

    .line 63
    :cond_0
    :goto_0
    return v1

    .line 46
    :cond_1
    invoke-direct {p0, p3}, Lcom/twitter/library/av/playback/ao;->a([Lcom/google/android/exoplayer/hls/Variant;)I

    move-result v8

    move v1, v8

    .line 48
    :goto_1
    add-int/lit8 v0, v9, -0x1

    if-gt v1, v0, :cond_2

    move-object v0, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-wide/from16 v6, p5

    .line 50
    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/av/playback/ao;->a(IJ[Lcom/google/android/exoplayer/hls/Variant;[JJ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 56
    :cond_2
    add-int/lit8 v1, v8, -0x1

    :goto_2
    if-ltz v1, :cond_3

    move-object v0, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-wide/from16 v6, p5

    .line 58
    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/av/playback/ao;->a(IJ[Lcom/google/android/exoplayer/hls/Variant;[JJ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 63
    :cond_3
    array-length v0, p3

    add-int/lit8 v1, v0, -0x1

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/library/av/playback/ao;->a:Lcoi$a;

    invoke-virtual {v0}, Lcoi$a;->close()V

    .line 32
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/library/av/playback/ao;->a:Lcoi$a;

    invoke-virtual {v0}, Lcoi$a;->c()Z

    move-result v0

    return v0
.end method
