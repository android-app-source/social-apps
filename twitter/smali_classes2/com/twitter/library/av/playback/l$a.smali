.class public final Lcom/twitter/library/av/playback/l$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/av/playback/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/library/av/playback/l;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/twitter/model/av/AVMedia;

.field public b:Lbix;

.field public c:Landroid/content/Context;

.field public d:Lcom/twitter/library/av/playback/AVMediaPlayer$a;

.field public e:Landroid/os/Handler;

.field public f:Ljava/lang/String;

.field public g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 47
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/library/av/playback/l$a;->g:I

    return-void
.end method


# virtual methods
.method public R_()Z
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/twitter/library/av/playback/l$a;->a:Lcom/twitter/model/av/AVMedia;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/playback/l$a;->b:Lbix;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/playback/l$a;->c:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/playback/l$a;->d:Lcom/twitter/library/av/playback/AVMediaPlayer$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/playback/l$a;->e:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/av/playback/l$a;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Lcom/twitter/library/av/playback/l$a;
    .locals 0

    .prologue
    .line 81
    iput p1, p0, Lcom/twitter/library/av/playback/l$a;->g:I

    .line 82
    return-object p0
.end method

.method public a(Landroid/content/Context;)Lcom/twitter/library/av/playback/l$a;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/twitter/library/av/playback/l$a;->c:Landroid/content/Context;

    .line 64
    return-object p0
.end method

.method public a(Landroid/os/Handler;)Lcom/twitter/library/av/playback/l$a;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/twitter/library/av/playback/l$a;->e:Landroid/os/Handler;

    .line 76
    return-object p0
.end method

.method public a(Lbix;)Lcom/twitter/library/av/playback/l$a;
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/twitter/library/av/playback/l$a;->b:Lbix;

    .line 58
    return-object p0
.end method

.method public a(Lcom/twitter/library/av/playback/AVMediaPlayer$a;)Lcom/twitter/library/av/playback/l$a;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/twitter/library/av/playback/l$a;->d:Lcom/twitter/library/av/playback/AVMediaPlayer$a;

    .line 70
    return-object p0
.end method

.method public a(Lcom/twitter/model/av/AVMedia;)Lcom/twitter/library/av/playback/l$a;
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/twitter/library/av/playback/l$a;->a:Lcom/twitter/model/av/AVMedia;

    .line 52
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/av/playback/l$a;
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/twitter/library/av/playback/l$a;->f:Ljava/lang/String;

    .line 88
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/l$a;->e()Lcom/twitter/library/av/playback/l;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/library/av/playback/l;
    .locals 1

    .prologue
    .line 100
    new-instance v0, Lcom/twitter/library/av/playback/l;

    invoke-direct {v0, p0}, Lcom/twitter/library/av/playback/l;-><init>(Lcom/twitter/library/av/playback/l$a;)V

    return-object v0
.end method
