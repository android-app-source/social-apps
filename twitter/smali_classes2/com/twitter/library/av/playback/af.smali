.class public abstract Lcom/twitter/library/av/playback/af;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/av/playback/AVMediaPlayer;


# instance fields
.field protected final b:Lcom/twitter/library/av/playback/AVMediaPlayer$a;

.field protected final c:Lbix;

.field protected final d:Ljava/lang/String;

.field protected final e:Lcom/twitter/model/av/AVMedia;

.field protected final f:Ljava/lang/String;

.field protected g:Lcom/twitter/library/av/k;

.field protected h:Landroid/media/MediaPlayer$OnCompletionListener;

.field protected volatile i:Lcom/twitter/library/av/playback/n;

.field protected j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected k:Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;

.field l:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

.field m:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

.field final n:Landroid/os/Handler;

.field private o:Z

.field private p:Z


# direct methods
.method protected constructor <init>(Lcom/twitter/library/av/playback/l;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    sget-object v0, Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;->d:Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;

    iput-object v0, p0, Lcom/twitter/library/av/playback/af;->k:Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;

    .line 36
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->c:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    iput-object v0, p0, Lcom/twitter/library/av/playback/af;->l:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    .line 40
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->c:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    iput-object v0, p0, Lcom/twitter/library/av/playback/af;->m:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    .line 50
    iget-object v0, p1, Lcom/twitter/library/av/playback/l;->d:Lcom/twitter/library/av/playback/AVMediaPlayer$a;

    iput-object v0, p0, Lcom/twitter/library/av/playback/af;->b:Lcom/twitter/library/av/playback/AVMediaPlayer$a;

    .line 51
    new-instance v0, Lcom/twitter/library/av/playback/n;

    invoke-direct {v0, p0}, Lcom/twitter/library/av/playback/n;-><init>(Lcom/twitter/library/av/playback/AVMediaPlayer;)V

    iput-object v0, p0, Lcom/twitter/library/av/playback/af;->i:Lcom/twitter/library/av/playback/n;

    .line 52
    iget-object v0, p1, Lcom/twitter/library/av/playback/l;->e:Landroid/os/Handler;

    iput-object v0, p0, Lcom/twitter/library/av/playback/af;->n:Landroid/os/Handler;

    .line 53
    iget-object v0, p1, Lcom/twitter/library/av/playback/l;->b:Lbix;

    iput-object v0, p0, Lcom/twitter/library/av/playback/af;->c:Lbix;

    .line 54
    iget-object v0, p1, Lcom/twitter/library/av/playback/l;->a:Lcom/twitter/model/av/AVMedia;

    iput-object v0, p0, Lcom/twitter/library/av/playback/af;->e:Lcom/twitter/model/av/AVMedia;

    .line 55
    iget-object v0, p0, Lcom/twitter/library/av/playback/af;->e:Lcom/twitter/model/av/AVMedia;

    invoke-interface {v0}, Lcom/twitter/model/av/AVMedia;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/playback/af;->d:Ljava/lang/String;

    .line 56
    iget-object v0, p1, Lcom/twitter/library/av/playback/l;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/av/playback/af;->f:Ljava/lang/String;

    .line 57
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 350
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->c:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/af;->a(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    .line 351
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->c:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/af;->b(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    .line 352
    return-void
.end method


# virtual methods
.method public A()Z
    .locals 2

    .prologue
    .line 260
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->J()Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    move-result-object v0

    .line 261
    sget-object v1, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->e:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->d:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public B()Z
    .locals 2

    .prologue
    .line 266
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->J()Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->a:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public C()Z
    .locals 2

    .prologue
    .line 275
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->J()Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->h:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public D()V
    .locals 1

    .prologue
    .line 307
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/af;->c(Z)V

    .line 308
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/av/playback/af;->o:Z

    .line 309
    return-void
.end method

.method public E()Z
    .locals 1

    .prologue
    .line 317
    iget-boolean v0, p0, Lcom/twitter/library/av/playback/af;->o:Z

    return v0
.end method

.method public F()V
    .locals 2

    .prologue
    .line 325
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/av/playback/af;->o:Z

    .line 326
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->M()Lcom/twitter/library/av/playback/n;

    move-result-object v0

    .line 327
    if-eqz v0, :cond_0

    .line 328
    sget-object v1, Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;->c:Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/n;->a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V

    .line 329
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/n;->a()V

    .line 331
    :cond_0
    return-void
.end method

.method public G()Z
    .locals 2

    .prologue
    .line 343
    iget-boolean v0, p0, Lcom/twitter/library/av/playback/af;->p:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->J()Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->a:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public H()Lcom/twitter/library/av/playback/aa;
    .locals 6

    .prologue
    .line 433
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 434
    new-instance v0, Lcom/twitter/library/av/playback/aa;

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->f()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->e()J

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/twitter/library/av/playback/aa;-><init>(JJ)V

    .line 439
    :goto_0
    return-object v0

    .line 436
    :cond_0
    new-instance v0, Lcom/twitter/library/av/playback/aa;

    const-wide/16 v2, 0x0

    const-wide/16 v4, -0x1

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/twitter/library/av/playback/aa;-><init>(JJ)V

    goto :goto_0
.end method

.method public I()V
    .locals 3

    .prologue
    .line 478
    sget-object v0, Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;->b:Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/af;->a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V

    .line 479
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->M()Lcom/twitter/library/av/playback/n;

    move-result-object v0

    .line 480
    if-eqz v0, :cond_0

    .line 481
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/n;->f()V

    .line 483
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/playback/af;->c:Lbix;

    new-instance v1, Lbjt;

    iget-object v2, p0, Lcom/twitter/library/av/playback/af;->e:Lcom/twitter/model/av/AVMedia;

    invoke-direct {v1, v2}, Lbjt;-><init>(Lcom/twitter/model/av/AVMedia;)V

    invoke-virtual {v0, v1}, Lbix;->a(Lbiw;)V

    .line 484
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->p()V

    .line 485
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->f:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/af;->a(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    .line 486
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->f:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/af;->b(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    .line 487
    return-void
.end method

.method protected declared-synchronized J()Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;
    .locals 1

    .prologue
    .line 195
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/av/playback/af;->l:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized K()Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;
    .locals 1

    .prologue
    .line 210
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/av/playback/af;->m:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public L()Z
    .locals 2

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->J()Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    move-result-object v0

    .line 242
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->a:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->c:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->d:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected declared-synchronized M()Lcom/twitter/library/av/playback/n;
    .locals 1

    .prologue
    .line 495
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/av/playback/af;->i:Lcom/twitter/library/av/playback/n;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract a(J)V
.end method

.method protected abstract a(Landroid/content/Context;)V
.end method

.method public a(Landroid/content/Context;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 335
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/av/playback/af;->p:Z

    .line 336
    iput-object p2, p0, Lcom/twitter/library/av/playback/af;->j:Ljava/util/Map;

    .line 337
    invoke-direct {p0}, Lcom/twitter/library/av/playback/af;->a()V

    .line 338
    invoke-virtual {p0, p1}, Lcom/twitter/library/av/playback/af;->a(Landroid/content/Context;)V

    .line 339
    return-void
.end method

.method public a(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 0

    .prologue
    .line 362
    iput-object p1, p0, Lcom/twitter/library/av/playback/af;->h:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 363
    return-void
.end method

.method public a(Lcom/twitter/library/av/k;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/twitter/library/av/playback/af;->g:Lcom/twitter/library/av/k;

    .line 173
    return-void
.end method

.method protected declared-synchronized a(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V
    .locals 1

    .prologue
    .line 188
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/twitter/library/av/playback/af;->l:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    monitor-exit p0

    return-void

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V
    .locals 1

    .prologue
    .line 448
    monitor-enter p0

    .line 449
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/av/playback/af;->i:Lcom/twitter/library/av/playback/n;

    if-nez v0, :cond_0

    .line 450
    new-instance v0, Lcom/twitter/library/av/playback/n;

    invoke-direct {v0, p0}, Lcom/twitter/library/av/playback/n;-><init>(Lcom/twitter/library/av/playback/AVMediaPlayer;)V

    iput-object v0, p0, Lcom/twitter/library/av/playback/af;->i:Lcom/twitter/library/av/playback/n;

    .line 452
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/playback/af;->i:Lcom/twitter/library/av/playback/n;

    .line 453
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 455
    invoke-virtual {v0, p1}, Lcom/twitter/library/av/playback/n;->a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V

    .line 456
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/n;->a()V

    .line 457
    return-void

    .line 453
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 368
    monitor-enter p0

    .line 369
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/av/playback/af;->i:Lcom/twitter/library/av/playback/n;

    .line 370
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/library/av/playback/af;->i:Lcom/twitter/library/av/playback/n;

    .line 371
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373
    if-eqz v0, :cond_0

    .line 375
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/n;->d()V

    .line 378
    :cond_0
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->c:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/af;->a(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    .line 379
    if-eqz p1, :cond_1

    .line 380
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->c:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/af;->b(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    .line 382
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->m()V

    .line 383
    return-void

    .line 371
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected a(ZILjava/lang/String;)V
    .locals 1

    .prologue
    .line 155
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->a:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/af;->a(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    .line 156
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->a:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/af;->b(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    .line 157
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/af;->c(Z)V

    .line 158
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/playback/af;->g:Lcom/twitter/library/av/k;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/twitter/library/av/playback/af;->g:Lcom/twitter/library/av/k;

    invoke-interface {v0, p2, p3}, Lcom/twitter/library/av/k;->a(ILjava/lang/String;)V

    .line 161
    :cond_0
    return-void
.end method

.method public b()Lcom/twitter/library/av/playback/AVMediaPlayer$a;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/library/av/playback/af;->b:Lcom/twitter/library/av/playback/AVMediaPlayer$a;

    return-object v0
.end method

.method public b(J)V
    .locals 3

    .prologue
    .line 285
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/af;->c(Z)V

    .line 287
    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/av/playback/af;->a(J)V

    .line 292
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->e()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->J()Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->f:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-eq v0, v1, :cond_0

    .line 293
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->h:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/af;->a(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    .line 294
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->h:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/af;->b(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    .line 295
    iget-object v0, p0, Lcom/twitter/library/av/playback/af;->h:Landroid/media/MediaPlayer$OnCompletionListener;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/twitter/library/av/playback/af;->h:Landroid/media/MediaPlayer$OnCompletionListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/media/MediaPlayer$OnCompletionListener;->onCompletion(Landroid/media/MediaPlayer;)V

    .line 300
    :cond_0
    return-void
.end method

.method protected declared-synchronized b(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V
    .locals 1

    .prologue
    .line 203
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/twitter/library/av/playback/af;->m:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    monitor-exit p0

    return-void

    .line 203
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 393
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->J()Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    move-result-object v0

    .line 396
    sget-object v1, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->g:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->f:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_4

    .line 398
    :cond_0
    sget-object v0, Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;->b:Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;

    .line 405
    :goto_0
    iput-object v0, p0, Lcom/twitter/library/av/playback/af;->k:Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;

    .line 407
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->L()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->u()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 408
    :cond_1
    if-eqz p1, :cond_2

    .line 409
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/av/playback/af;->b(J)V

    .line 412
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->k()V

    .line 413
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->f:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/af;->a(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    .line 414
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->o()V

    .line 416
    :cond_3
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->f:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/af;->b(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    .line 417
    return-void

    .line 399
    :cond_4
    if-eqz p1, :cond_5

    .line 400
    sget-object v0, Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;->a:Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;

    goto :goto_0

    .line 402
    :cond_5
    sget-object v0, Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;->d:Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;

    goto :goto_0
.end method

.method protected c(Z)V
    .locals 3

    .prologue
    .line 464
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->M()Lcom/twitter/library/av/playback/n;

    move-result-object v0

    .line 465
    if-nez v0, :cond_1

    .line 474
    :cond_0
    :goto_0
    return-void

    .line 469
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/n;->c()V

    .line 471
    if-eqz p1, :cond_0

    .line 472
    iget-object v0, p0, Lcom/twitter/library/av/playback/af;->c:Lbix;

    new-instance v1, Lbkb;

    iget-object v2, p0, Lcom/twitter/library/av/playback/af;->e:Lcom/twitter/model/av/AVMedia;

    invoke-direct {v1, v2}, Lbkb;-><init>(Lcom/twitter/model/av/AVMedia;)V

    invoke-virtual {v0, v1}, Lbix;->a(Lbiw;)V

    goto :goto_0
.end method

.method protected abstract e()J
.end method

.method protected abstract f()J
.end method

.method protected abstract i()Z
.end method

.method protected abstract k()V
.end method

.method protected abstract l()V
.end method

.method protected abstract m()V
.end method

.method protected abstract o()V
.end method

.method protected abstract p()V
.end method

.method protected u()Z
    .locals 1

    .prologue
    .line 424
    const/4 v0, 0x0

    return v0
.end method

.method public v()Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return v0
.end method

.method public w()Z
    .locals 2

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->J()Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->f:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public x()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 218
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->z()Z

    move-result v2

    .line 219
    iget-object v0, p0, Lcom/twitter/library/av/playback/af;->m:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    sget-object v3, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->f:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-ne v0, v3, :cond_4

    iget-object v0, p0, Lcom/twitter/library/av/playback/af;->l:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    sget-object v3, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->f:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-eq v0, v3, :cond_4

    const/4 v0, 0x1

    .line 220
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->i()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->w()Z

    move-result v3

    if-nez v3, :cond_0

    if-nez v2, :cond_0

    if-eqz v0, :cond_3

    .line 221
    :cond_0
    if-nez v2, :cond_1

    .line 222
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->g:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/af;->a(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    .line 224
    :cond_1
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->g:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/af;->b(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    .line 225
    invoke-virtual {p0, v1}, Lcom/twitter/library/av/playback/af;->c(Z)V

    .line 227
    if-nez v2, :cond_2

    .line 228
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->l()V

    .line 230
    :cond_2
    iget-object v0, p0, Lcom/twitter/library/av/playback/af;->g:Lcom/twitter/library/av/k;

    if-eqz v0, :cond_3

    .line 231
    iget-object v0, p0, Lcom/twitter/library/av/playback/af;->g:Lcom/twitter/library/av/k;

    invoke-interface {v0}, Lcom/twitter/library/av/k;->j()V

    .line 234
    :cond_3
    return-void

    :cond_4
    move v0, v1

    .line 219
    goto :goto_0
.end method

.method public y()Z
    .locals 2

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->J()Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->g:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public z()Z
    .locals 2

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/af;->J()Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->d:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
