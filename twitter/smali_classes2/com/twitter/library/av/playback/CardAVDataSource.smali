.class public Lcom/twitter/library/av/playback/CardAVDataSource;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/av/playback/AVDataSource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/playback/CardAVDataSource$a;,
        Lcom/twitter/library/av/playback/CardAVDataSource$b;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/library/av/playback/CardAVDataSource;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lcax;

.field private c:Ljava/lang/String;

.field private d:Lcom/twitter/model/core/Tweet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/twitter/library/av/playback/CardAVDataSource$1;

    invoke-direct {v0}, Lcom/twitter/library/av/playback/CardAVDataSource$1;-><init>()V

    sput-object v0, Lcom/twitter/library/av/playback/CardAVDataSource;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->c:Ljava/lang/String;

    .line 65
    sget-object v0, Lcax;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcax;

    iput-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->b:Lcax;

    .line 66
    const-class v0, Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    iput-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->d:Lcom/twitter/model/core/Tweet;

    .line 67
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/twitter/library/av/playback/CardAVDataSource$1;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/twitter/library/av/playback/CardAVDataSource;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/av/playback/CardAVDataSource$b;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iget-object v0, p1, Lcom/twitter/library/av/playback/CardAVDataSource$b;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->c:Ljava/lang/String;

    .line 59
    iget-object v0, p1, Lcom/twitter/library/av/playback/CardAVDataSource$b;->a:Lcax;

    iput-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->b:Lcax;

    .line 60
    iget-object v0, p1, Lcom/twitter/library/av/playback/CardAVDataSource$b;->c:Lcom/twitter/model/core/Tweet;

    iput-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->d:Lcom/twitter/model/core/Tweet;

    .line 61
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/av/playback/CardAVDataSource;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->c:Ljava/lang/String;

    return-object v0
.end method

.method public b()Lcom/twitter/model/card/property/ImageSpec;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->b:Lcax;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ae;->e(Lcax;)Lcom/twitter/model/card/property/ImageSpec;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->d:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->b:Lcax;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ae;->a(Lcax;)I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 163
    if-ne p0, p1, :cond_1

    .line 178
    :cond_0
    :goto_0
    return v0

    .line 166
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 167
    goto :goto_0

    .line 169
    :cond_3
    check-cast p1, Lcom/twitter/library/av/playback/CardAVDataSource;

    .line 171
    iget-object v2, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/av/playback/CardAVDataSource;->c:Ljava/lang/String;

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->b:Lcax;

    iget-object v3, p1, Lcom/twitter/library/av/playback/CardAVDataSource;->b:Lcax;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 172
    iget-object v2, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->d:Lcom/twitter/model/core/Tweet;

    if-nez v2, :cond_4

    .line 173
    iget-object v2, p1, Lcom/twitter/library/av/playback/CardAVDataSource;->d:Lcom/twitter/model/core/Tweet;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 175
    :cond_4
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->d:Lcom/twitter/model/core/Tweet;

    invoke-virtual {p1}, Lcom/twitter/library/av/playback/CardAVDataSource;->c()Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/Tweet;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_5
    move v0, v1

    .line 178
    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->b:Lcax;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ae;->d(Lcax;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Lbyp;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->b:Lcax;

    invoke-static {p0, v0}, Lcom/twitter/library/av/playback/ae;->a(Lcom/twitter/library/av/playback/AVDataSource;Lcax;)Lbyp;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public i()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->b:Lcax;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ae;->f(Lcax;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public j()Lcom/twitter/model/av/Partner;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->b:Lcax;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ae;->b(Lcax;)Lcom/twitter/model/av/Partner;

    move-result-object v0

    return-object v0
.end method

.method public k()Lcom/twitter/library/av/playback/ax;
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->d:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    .line 203
    new-instance v0, Lcom/twitter/library/av/playback/TweetAVDataSource$a;

    iget-object v1, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->d:Lcom/twitter/model/core/Tweet;

    invoke-direct {v0, v1}, Lcom/twitter/library/av/playback/TweetAVDataSource$a;-><init>(Lcom/twitter/model/core/Tweet;)V

    .line 205
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/library/av/playback/CardAVDataSource$a;

    iget-object v1, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->b:Lcax;

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/av/playback/CardAVDataSource$a;-><init>(Lcom/twitter/library/av/playback/CardAVDataSource;Lcax;)V

    goto :goto_0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    const/4 v0, 0x0

    return-object v0
.end method

.method public m()Lcom/twitter/library/av/playback/ah;
    .locals 1

    .prologue
    .line 218
    sget-object v0, Lcom/twitter/library/av/playback/ah;->a:Lcom/twitter/library/av/playback/ah;

    return-object v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->d:Lcom/twitter/model/core/Tweet;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()F
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->b:Lcax;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ae;->c(Lcax;)F

    move-result v0

    return v0
.end method

.method public p()J
    .locals 2

    .prologue
    .line 107
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public q()Lcax;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->b:Lcax;

    return-object v0
.end method

.method public r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->b:Lcax;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ae;->g(Lcax;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->b:Lcax;

    sget-object v1, Lcax;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 158
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource;->d:Lcom/twitter/model/core/Tweet;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 159
    return-void
.end method
