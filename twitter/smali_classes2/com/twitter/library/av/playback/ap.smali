.class public Lcom/twitter/library/av/playback/ap;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/google/android/exoplayer/hls/HlsChunkSource$Delegate;
.implements Lcom/twitter/library/av/playback/al$a;


# instance fields
.field private a:J

.field private b:Z

.field private final c:Lcom/twitter/library/av/playback/ao;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/twitter/library/av/playback/ap;->a:J

    .line 26
    new-instance v0, Lcom/twitter/library/av/playback/ao;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-direct {v0, v1}, Lcom/twitter/library/av/playback/ao;-><init>(F)V

    iput-object v0, p0, Lcom/twitter/library/av/playback/ap;->c:Lcom/twitter/library/av/playback/ao;

    .line 27
    return-void
.end method

.method private static a(J[Lcom/google/android/exoplayer/hls/Variant;[JJI)I
    .locals 6

    .prologue
    .line 76
    .line 77
    const/4 v0, 0x0

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_2

    .line 78
    aget-wide v2, p3, v0

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    aget-object v1, p2, v0

    iget-object v1, v1, Lcom/google/android/exoplayer/hls/Variant;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget v1, v1, Lcom/google/android/exoplayer/chunk/Format;->bitrate:I

    int-to-long v2, v1

    cmp-long v1, v2, p4

    if-gtz v1, :cond_1

    .line 79
    aget-object v1, p2, v0

    iget-object v1, v1, Lcom/google/android/exoplayer/hls/Variant;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget v1, v1, Lcom/google/android/exoplayer/chunk/Format;->bitrate:I

    int-to-long v2, v1

    cmp-long v1, v2, p0

    if-gtz v1, :cond_0

    .line 85
    :goto_1
    return v0

    :cond_0
    move p6, v0

    .line 77
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, p6

    .line 85
    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/library/av/playback/ap;->c:Lcom/twitter/library/av/playback/ao;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/ao;->a()V

    .line 31
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 35
    iput-wide p1, p0, Lcom/twitter/library/av/playback/ap;->a:J

    .line 36
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/twitter/library/av/playback/ap;->b:Z

    .line 41
    return-void
.end method

.method public getVariantIndexForBandwidth(J[Lcom/google/android/exoplayer/hls/Variant;[JZ)I
    .locals 9

    .prologue
    .line 45
    if-eqz p5, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/av/playback/ap;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/playback/ap;->c:Lcom/twitter/library/av/playback/ao;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/ao;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    iget-object v1, p0, Lcom/twitter/library/av/playback/ap;->c:Lcom/twitter/library/av/playback/ao;

    iget-wide v6, p0, Lcom/twitter/library/av/playback/ap;->a:J

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v1 .. v7}, Lcom/twitter/library/av/playback/ao;->a(J[Lcom/google/android/exoplayer/hls/Variant;[JJ)I

    move-result v0

    .line 71
    :goto_0
    return v0

    .line 50
    :cond_0
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 52
    const-wide/16 v0, 0x0

    const-wide v4, 0x7fffffffffffffffL

    const/4 v6, -0x1

    move-object v2, p3

    move-object v3, p4

    invoke-static/range {v0 .. v6}, Lcom/twitter/library/av/playback/ap;->a(J[Lcom/google/android/exoplayer/hls/Variant;[JJI)I

    move-result v0

    goto :goto_0

    .line 57
    :cond_1
    long-to-float v0, p1

    const v1, 0x3f4ccccd    # 0.8f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 58
    int-to-long v0, v0

    iget-wide v4, p0, Lcom/twitter/library/av/playback/ap;->a:J

    const/4 v6, -0x1

    move-object v2, p3

    move-object v3, p4

    invoke-static/range {v0 .. v6}, Lcom/twitter/library/av/playback/ap;->a(J[Lcom/google/android/exoplayer/hls/Variant;[JJI)I

    move-result v1

    .line 61
    const/4 v0, -0x1

    if-ne v1, v0, :cond_2

    .line 64
    const-wide/16 v0, 0x0

    const-wide v4, 0x7fffffffffffffffL

    const/4 v6, -0x1

    move-object v2, p3

    move-object v3, p4

    invoke-static/range {v0 .. v6}, Lcom/twitter/library/av/playback/ap;->a(J[Lcom/google/android/exoplayer/hls/Variant;[JJI)I

    move-result v1

    .line 70
    :cond_2
    const/4 v0, -0x1

    if-eq v1, v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    move v0, v1

    .line 71
    goto :goto_0

    .line 70
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
