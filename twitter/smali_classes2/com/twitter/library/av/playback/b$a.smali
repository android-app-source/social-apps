.class public Lcom/twitter/library/av/playback/b$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/google/android/exoplayer/ExoPlayer$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/av/playback/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/google/android/exoplayer/ExoPlayer$Listener;

.field private final b:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/google/android/exoplayer/ExoPlayer$Listener;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 498
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 499
    iput-object p1, p0, Lcom/twitter/library/av/playback/b$a;->a:Lcom/google/android/exoplayer/ExoPlayer$Listener;

    .line 500
    iput-object p2, p0, Lcom/twitter/library/av/playback/b$a;->b:Landroid/os/Handler;

    .line 501
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/av/playback/b$a;)Lcom/google/android/exoplayer/ExoPlayer$Listener;
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lcom/twitter/library/av/playback/b$a;->a:Lcom/google/android/exoplayer/ExoPlayer$Listener;

    return-object v0
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 544
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/playback/b$a;->b:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 545
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 549
    :goto_0
    return-void

    .line 547
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/playback/b$a;->b:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method


# virtual methods
.method public onPlayWhenReadyCommitted()V
    .locals 1

    .prologue
    .line 515
    new-instance v0, Lcom/twitter/library/av/playback/b$a$2;

    invoke-direct {v0, p0}, Lcom/twitter/library/av/playback/b$a$2;-><init>(Lcom/twitter/library/av/playback/b$a;)V

    invoke-direct {p0, v0}, Lcom/twitter/library/av/playback/b$a;->a(Ljava/lang/Runnable;)V

    .line 521
    return-void
.end method

.method public onPlayerError(Lcom/google/android/exoplayer/ExoPlaybackException;)V
    .locals 1

    .prologue
    .line 525
    new-instance v0, Lcom/twitter/library/av/playback/b$a$3;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/av/playback/b$a$3;-><init>(Lcom/twitter/library/av/playback/b$a;Lcom/google/android/exoplayer/ExoPlaybackException;)V

    invoke-direct {p0, v0}, Lcom/twitter/library/av/playback/b$a;->a(Ljava/lang/Runnable;)V

    .line 531
    return-void
.end method

.method public onPlayerSeekComplete()V
    .locals 1

    .prologue
    .line 535
    new-instance v0, Lcom/twitter/library/av/playback/b$a$4;

    invoke-direct {v0, p0}, Lcom/twitter/library/av/playback/b$a$4;-><init>(Lcom/twitter/library/av/playback/b$a;)V

    invoke-direct {p0, v0}, Lcom/twitter/library/av/playback/b$a;->a(Ljava/lang/Runnable;)V

    .line 541
    return-void
.end method

.method public onPlayerStateChanged(ZI)V
    .locals 1

    .prologue
    .line 505
    new-instance v0, Lcom/twitter/library/av/playback/b$a$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/library/av/playback/b$a$1;-><init>(Lcom/twitter/library/av/playback/b$a;ZI)V

    invoke-direct {p0, v0}, Lcom/twitter/library/av/playback/b$a;->a(Ljava/lang/Runnable;)V

    .line 511
    return-void
.end method
