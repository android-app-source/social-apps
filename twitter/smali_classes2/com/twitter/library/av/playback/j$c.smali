.class Lcom/twitter/library/av/playback/j$c;
.super Lcom/twitter/library/av/playback/ar;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/av/playback/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/av/playback/j;


# direct methods
.method constructor <init>(Lcom/twitter/library/av/playback/j;)V
    .locals 0

    .prologue
    .line 274
    iput-object p1, p0, Lcom/twitter/library/av/playback/j$c;->a:Lcom/twitter/library/av/playback/j;

    invoke-direct {p0}, Lcom/twitter/library/av/playback/ar;-><init>()V

    return-void
.end method


# virtual methods
.method public onDownstreamFormatChanged(ILcom/google/android/exoplayer/chunk/Format;IJ)V
    .locals 4

    .prologue
    .line 284
    iget-object v0, p0, Lcom/twitter/library/av/playback/j$c;->a:Lcom/twitter/library/av/playback/j;

    iget-object v0, v0, Lcom/twitter/library/av/playback/j;->c:Lbix;

    new-instance v1, Lbjm;

    iget-object v2, p0, Lcom/twitter/library/av/playback/j$c;->a:Lcom/twitter/library/av/playback/j;

    iget-object v2, v2, Lcom/twitter/library/av/playback/j;->e:Lcom/twitter/model/av/AVMedia;

    iget v3, p2, Lcom/google/android/exoplayer/chunk/Format;->bitrate:I

    invoke-direct {v1, v2, v3}, Lbjm;-><init>(Lcom/twitter/model/av/AVMedia;I)V

    invoke-virtual {v0, v1}, Lbix;->a(Lbiw;)V

    .line 285
    invoke-super/range {p0 .. p5}, Lcom/twitter/library/av/playback/ar;->onDownstreamFormatChanged(ILcom/google/android/exoplayer/chunk/Format;IJ)V

    .line 286
    return-void
.end method

.method public onLoadError(ILjava/io/IOException;)V
    .locals 3

    .prologue
    .line 279
    const-string/jumbo v0, "AVMediaExoPlayerHls"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onLoadError() called with: sourceId = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "], e = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    return-void
.end method

.method public onProgramDateTime(Ljava/util/Date;J)V
    .locals 4

    .prologue
    .line 290
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/library/av/playback/ar;->onProgramDateTime(Ljava/util/Date;J)V

    .line 291
    iget-object v0, p0, Lcom/twitter/library/av/playback/j$c;->a:Lcom/twitter/library/av/playback/j;

    iget-object v0, v0, Lcom/twitter/library/av/playback/j;->c:Lbix;

    new-instance v1, Lcom/twitter/library/av/playback/j$f;

    iget-object v2, p0, Lcom/twitter/library/av/playback/j$c;->a:Lcom/twitter/library/av/playback/j;

    iget-object v2, v2, Lcom/twitter/library/av/playback/j;->e:Lcom/twitter/model/av/AVMedia;

    invoke-direct {v1, v2, p1, p2, p3}, Lcom/twitter/library/av/playback/j$f;-><init>(Lcom/twitter/model/av/AVMedia;Ljava/util/Date;J)V

    invoke-virtual {v0, v1}, Lbix;->a(Lbiw;)V

    .line 292
    return-void
.end method
