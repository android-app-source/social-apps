.class public abstract Lcom/twitter/library/av/playback/q;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/playback/q$a;
    }
.end annotation


# static fields
.field private static final d:Lcom/twitter/library/av/playback/q;


# instance fields
.field final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/av/playback/q$a;",
            ">;"
        }
    .end annotation
.end field

.field final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/library/av/playback/q$a;",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/library/av/playback/AVPlayerAttachment;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/twitter/library/av/playback/AVPlayer$a;

    invoke-direct {v0}, Lcom/twitter/library/av/playback/AVPlayer$a;-><init>()V

    sput-object v0, Lcom/twitter/library/av/playback/q;->d:Lcom/twitter/library/av/playback/q;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/av/playback/q;->a:Ljava/util/Set;

    .line 36
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/av/playback/q;->b:Ljava/util/Map;

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/av/playback/q;->c:Ljava/util/Map;

    return-void
.end method

.method private a(Lcom/twitter/library/av/playback/u;Lbyf;Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Z)Lcom/twitter/library/av/playback/AVPlayerAttachment;
    .locals 5

    .prologue
    .line 85
    if-nez p1, :cond_0

    .line 86
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 90
    :cond_0
    invoke-static {p1}, Lcom/twitter/library/av/playback/q;->c(Lcom/twitter/library/av/playback/u;)Ljava/lang/String;

    move-result-object v3

    .line 92
    iget-object v0, p0, Lcom/twitter/library/av/playback/q;->b:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 93
    iget-object v0, p0, Lcom/twitter/library/av/playback/q;->b:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/playback/q$a;

    .line 94
    iget-object v1, v0, Lcom/twitter/library/av/playback/q$a;->a:Lcom/twitter/library/av/playback/AVPlayer;

    .line 95
    if-eqz p5, :cond_1

    .line 96
    iget-object v2, v0, Lcom/twitter/library/av/playback/q$a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    .line 106
    :cond_1
    :goto_0
    new-instance v2, Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-direct {v2, v1, p2, p4}, Lcom/twitter/library/av/playback/AVPlayerAttachment;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lbyf;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 108
    iget-object v3, p0, Lcom/twitter/library/av/playback/q;->c:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 109
    if-eqz v0, :cond_4

    .line 110
    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 111
    invoke-virtual {v1}, Lcom/twitter/library/av/playback/AVPlayer;->f()Lcom/twitter/library/av/playback/s;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/library/av/playback/s;->a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)Lcom/twitter/library/av/playback/s;

    .line 116
    :goto_1
    return-object v2

    .line 99
    :cond_2
    invoke-virtual {p0, p1, p3}, Lcom/twitter/library/av/playback/q;->a(Lcom/twitter/library/av/playback/u;Landroid/content/Context;)Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v2

    .line 100
    if-eqz p5, :cond_3

    const/4 v0, 0x1

    .line 101
    :goto_2
    new-instance v1, Lcom/twitter/library/av/playback/q$a;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/av/playback/q$a;-><init>(Lcom/twitter/library/av/playback/AVPlayer;I)V

    .line 102
    iget-object v0, p0, Lcom/twitter/library/av/playback/q;->c:Ljava/util/Map;

    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-direct {v4}, Ljava/util/LinkedHashSet;-><init>()V

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    invoke-direct {p0, v3, v1}, Lcom/twitter/library/av/playback/q;->b(Ljava/lang/String;Lcom/twitter/library/av/playback/q$a;)V

    move-object v0, v1

    move-object v1, v2

    goto :goto_0

    .line 100
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 113
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Tried to attach, but set of attachments was not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static a()Lcom/twitter/library/av/playback/q;
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/twitter/library/av/playback/q;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 44
    sget-object v0, Lcom/twitter/library/av/playback/q;->d:Lcom/twitter/library/av/playback/q;

    return-object v0
.end method

.method private a(Ljava/lang/String;Lcom/twitter/library/av/playback/q$a;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 156
    iget-object v0, p2, Lcom/twitter/library/av/playback/q$a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-ge v0, v1, :cond_1

    move v0, v1

    .line 157
    :goto_0
    invoke-virtual {p0, p1}, Lcom/twitter/library/av/playback/q;->d(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    .line 158
    invoke-virtual {p0, p1}, Lcom/twitter/library/av/playback/q;->e(Ljava/lang/String;)V

    .line 159
    iget-object v0, p2, Lcom/twitter/library/av/playback/q$a;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/av/playback/q;->a(Lcom/twitter/library/av/playback/AVPlayer;Z)V

    .line 160
    invoke-virtual {p0, p1}, Lcom/twitter/library/av/playback/q;->f(Ljava/lang/String;)V

    .line 162
    :cond_0
    return-void

    .line 156
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Lcom/twitter/library/av/playback/q$a;)V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/twitter/library/av/playback/q;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    return-void
.end method

.method protected static c(Lcom/twitter/library/av/playback/u;)Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 396
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private g(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/twitter/library/av/playback/q;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 325
    return-void
.end method


# virtual methods
.method protected abstract a(Lcom/twitter/library/av/playback/u;Landroid/content/Context;)Lcom/twitter/library/av/playback/AVPlayer;
.end method

.method public a(Lcom/twitter/library/av/playback/u;Lbyf;Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/av/playback/AVPlayerAttachment;
    .locals 6

    .prologue
    .line 59
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/av/playback/q;->a(Lcom/twitter/library/av/playback/u;Lbyf;Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Z)Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a(Lcom/twitter/library/av/playback/AVPlayer;Z)V
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V
    .locals 4

    .prologue
    .line 128
    invoke-virtual {p1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->j()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 130
    invoke-virtual {p1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->h()Lcom/twitter/library/av/playback/u;

    move-result-object v3

    .line 131
    iget-object v0, p0, Lcom/twitter/library/av/playback/q;->b:Ljava/util/Map;

    invoke-static {v3}, Lcom/twitter/library/av/playback/q;->c(Lcom/twitter/library/av/playback/u;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/playback/q$a;

    .line 132
    iget-object v1, p0, Lcom/twitter/library/av/playback/q;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 134
    if-eqz v0, :cond_1

    .line 135
    const/4 v2, 0x0

    .line 136
    if-eqz v1, :cond_2

    .line 137
    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    .line 138
    iget-object v2, v0, Lcom/twitter/library/av/playback/q$a;->a:Lcom/twitter/library/av/playback/AVPlayer;

    .line 139
    invoke-virtual {v2}, Lcom/twitter/library/av/playback/AVPlayer;->f()Lcom/twitter/library/av/playback/s;

    move-result-object v2

    .line 140
    invoke-virtual {v2, p1}, Lcom/twitter/library/av/playback/s;->b(Lcom/twitter/library/av/playback/AVPlayerAttachment;)Lcom/twitter/library/av/playback/s;

    .line 142
    :goto_0
    if-eqz v1, :cond_0

    .line 143
    iget-object v1, v0, Lcom/twitter/library/av/playback/q$a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndDecrement()I

    .line 146
    :cond_0
    invoke-static {v3}, Lcom/twitter/library/av/playback/q;->c(Lcom/twitter/library/av/playback/u;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/twitter/library/av/playback/q;->a(Ljava/lang/String;Lcom/twitter/library/av/playback/q$a;)V

    .line 148
    :cond_1
    return-void

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/av/playback/u;)V
    .locals 1

    .prologue
    .line 224
    invoke-static {p1}, Lcom/twitter/library/av/playback/q;->c(Lcom/twitter/library/av/playback/u;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/q;->a(Ljava/lang/String;)V

    .line 225
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 213
    invoke-virtual {p0, p1}, Lcom/twitter/library/av/playback/q;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 214
    invoke-direct {p0, p1}, Lcom/twitter/library/av/playback/q;->g(Ljava/lang/String;)V

    .line 216
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Lbyf;)Z
    .locals 3

    .prologue
    .line 356
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/q;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/playback/q$a;

    .line 357
    iget-object v0, v0, Lcom/twitter/library/av/playback/q$a;->a:Lcom/twitter/library/av/playback/AVPlayer;

    .line 359
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->e()Lcom/twitter/library/av/playback/u;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v2

    invoke-interface {v2}, Lcom/twitter/library/av/playback/AVDataSource;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 360
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->k()Lbyf;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    const/4 v0, 0x1

    .line 364
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 273
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/q;->d()Ljava/util/Set;

    move-result-object v0

    .line 274
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/playback/q$a;

    .line 275
    if-eqz v0, :cond_0

    .line 276
    iget-object v0, v0, Lcom/twitter/library/av/playback/q$a;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->e()Lcom/twitter/library/av/playback/u;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/av/playback/q;->c(Lcom/twitter/library/av/playback/u;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/q;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 279
    :cond_1
    return-void
.end method

.method public b(Lcom/twitter/library/av/playback/u;)V
    .locals 1

    .prologue
    .line 302
    invoke-static {p1}, Lcom/twitter/library/av/playback/q;->c(Lcom/twitter/library/av/playback/u;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/q;->c(Ljava/lang/String;)V

    .line 303
    return-void
.end method

.method protected b(Ljava/lang/String;)V
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 237
    invoke-virtual {p0, p1}, Lcom/twitter/library/av/playback/q;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238
    iget-object v0, p0, Lcom/twitter/library/av/playback/q;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/playback/q$a;

    .line 240
    if-eqz v0, :cond_1

    .line 242
    iget-object v2, v0, Lcom/twitter/library/av/playback/q$a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    .line 243
    if-ge v2, v1, :cond_2

    .line 244
    :goto_0
    iget-object v2, v0, Lcom/twitter/library/av/playback/q$a;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v2}, Lcom/twitter/library/av/playback/AVPlayer;->P()V

    .line 245
    if-eqz v1, :cond_0

    .line 246
    iget-object v0, v0, Lcom/twitter/library/av/playback/q$a;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/av/playback/q;->a(Lcom/twitter/library/av/playback/AVPlayer;Z)V

    .line 247
    invoke-virtual {p0, p1}, Lcom/twitter/library/av/playback/q;->f(Ljava/lang/String;)V

    .line 249
    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/library/av/playback/q;->e(Ljava/lang/String;)V

    .line 252
    :cond_1
    return-void

    .line 243
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 311
    iget-object v0, p0, Lcom/twitter/library/av/playback/q;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/playback/q$a;

    .line 312
    iget-object v0, v0, Lcom/twitter/library/av/playback/q$a;->a:Lcom/twitter/library/av/playback/AVPlayer;

    .line 313
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->n()J

    goto :goto_0

    .line 315
    :cond_0
    return-void
.end method

.method protected c(Ljava/lang/String;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 288
    invoke-virtual {p0, p1}, Lcom/twitter/library/av/playback/q;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/twitter/library/av/playback/q;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/playback/q$a;

    .line 290
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/library/av/playback/q$a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-nez v0, :cond_0

    .line 291
    invoke-virtual {p0, p1}, Lcom/twitter/library/av/playback/q;->b(Ljava/lang/String;)V

    .line 294
    :cond_0
    return-void
.end method

.method d()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/library/av/playback/q$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 332
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 333
    iget-object v0, p0, Lcom/twitter/library/av/playback/q;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 334
    iget-object v3, p0, Lcom/twitter/library/av/playback/q;->b:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 335
    iget-object v3, p0, Lcom/twitter/library/av/playback/q;->b:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 338
    :cond_1
    return-object v1
.end method

.method public d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/twitter/library/av/playback/q;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/twitter/library/av/playback/q;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 373
    return-void
.end method

.method f(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/twitter/library/av/playback/q;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 381
    return-void
.end method
