.class public Lcom/twitter/library/av/playback/j;
.super Lcom/twitter/library/av/playback/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/playback/j$b;,
        Lcom/twitter/library/av/playback/j$f;,
        Lcom/twitter/library/av/playback/j$d;,
        Lcom/twitter/library/av/playback/j$e;,
        Lcom/twitter/library/av/playback/j$c;,
        Lcom/twitter/library/av/playback/j$a;
    }
.end annotation


# instance fields
.field private final o:Lcom/twitter/library/av/playback/ExoPlayerHelper;

.field private final p:Landroid/content/Context;

.field private final q:Z

.field private final r:Z

.field private final s:Lcom/twitter/library/av/playback/ap;

.field private final t:Lcom/twitter/library/av/playback/al;


# direct methods
.method constructor <init>(Lcom/twitter/library/av/playback/l;Lcom/twitter/library/av/playback/ExoPlayerHelper;Lcom/google/android/exoplayer/ExoPlayer;ZZLcom/twitter/library/av/playback/am;Lcom/twitter/library/av/playback/aq;)V
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 103
    invoke-direct {p0, p1, p3}, Lcom/twitter/library/av/playback/b;-><init>(Lcom/twitter/library/av/playback/l;Lcom/google/android/exoplayer/ExoPlayer;)V

    .line 104
    iput-object p2, p0, Lcom/twitter/library/av/playback/j;->o:Lcom/twitter/library/av/playback/ExoPlayerHelper;

    .line 105
    iput-boolean p4, p0, Lcom/twitter/library/av/playback/j;->q:Z

    .line 106
    iget-object v0, p1, Lcom/twitter/library/av/playback/l;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/playback/j;->p:Landroid/content/Context;

    .line 107
    iput-boolean p5, p0, Lcom/twitter/library/av/playback/j;->r:Z

    .line 109
    iget-object v0, p1, Lcom/twitter/library/av/playback/l;->a:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {p7, v0}, Lcom/twitter/library/av/playback/aq;->a(Lcom/twitter/model/av/AVMedia;)Lcom/twitter/library/av/playback/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/playback/j;->s:Lcom/twitter/library/av/playback/ap;

    .line 110
    iget-object v0, p0, Lcom/twitter/library/av/playback/j;->s:Lcom/twitter/library/av/playback/ap;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/twitter/library/av/playback/j;->p:Landroid/content/Context;

    iget-object v1, p1, Lcom/twitter/library/av/playback/l;->a:Lcom/twitter/model/av/AVMedia;

    iget-object v2, p0, Lcom/twitter/library/av/playback/j;->s:Lcom/twitter/library/av/playback/ap;

    invoke-virtual {p6, v0, v1, v2}, Lcom/twitter/library/av/playback/am;->a(Landroid/content/Context;Lcom/twitter/model/av/AVMedia;Lcom/twitter/library/av/playback/al$a;)Lcom/twitter/library/av/playback/al;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/playback/j;->t:Lcom/twitter/library/av/playback/al;

    .line 113
    iget-object v0, p0, Lcom/twitter/library/av/playback/j;->c:Lbix;

    iget-object v1, p0, Lcom/twitter/library/av/playback/j;->t:Lcom/twitter/library/av/playback/al;

    invoke-virtual {v0, v1}, Lbix;->a(Lbiy;)Lbix;

    .line 117
    :goto_0
    return-void

    .line 115
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/av/playback/j;->t:Lcom/twitter/library/av/playback/al;

    goto :goto_0
.end method


# virtual methods
.method a(Landroid/content/Context;Lcom/twitter/model/av/AVMedia;)V
    .locals 7

    .prologue
    .line 133
    const-string/jumbo v0, "AVMediaExoPlayerHls"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "openMedia() called with: userAgent = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/av/playback/j;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "], avMedia = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "] for mUrl = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/av/playback/j;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 136
    iget-object v0, p0, Lcom/twitter/library/av/playback/j;->o:Lcom/twitter/library/av/playback/ExoPlayerHelper;

    iget-object v2, p0, Lcom/twitter/library/av/playback/j;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/library/av/playback/j;->d:Ljava/lang/String;

    new-instance v4, Lcom/google/android/exoplayer/hls/HlsPlaylistParser;

    invoke-direct {v4}, Lcom/google/android/exoplayer/hls/HlsPlaylistParser;-><init>()V

    new-instance v5, Lcom/twitter/library/av/playback/j$1;

    invoke-direct {v5, p0, v1}, Lcom/twitter/library/av/playback/j$1;-><init>(Lcom/twitter/library/av/playback/j;Landroid/content/Context;)V

    .line 153
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/j;->s()Lbrh;

    move-result-object v6

    .line 136
    invoke-virtual/range {v0 .. v6}, Lcom/twitter/library/av/playback/ExoPlayerHelper;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/exoplayer/upstream/UriLoadable$Parser;Lcom/google/android/exoplayer/util/ManifestFetcher$ManifestCallback;Lcom/google/android/exoplayer/upstream/TransferListener;)V

    .line 154
    return-void
.end method

.method a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/exoplayer/hls/HlsPlaylist;)V
    .locals 15

    .prologue
    .line 184
    move-object/from16 v0, p3

    instance-of v2, v0, Lcom/google/android/exoplayer/hls/HlsMasterPlaylist;

    if-eqz v2, :cond_0

    move-object/from16 v2, p3

    .line 185
    check-cast v2, Lcom/google/android/exoplayer/hls/HlsMasterPlaylist;

    .line 188
    :try_start_0
    iget-object v2, v2, Lcom/google/android/exoplayer/hls/HlsMasterPlaylist;->variants:Ljava/util/List;

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 189
    move-object/from16 v0, p1

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/exoplayer/chunk/VideoFormatSelectorUtil;->selectVideoFormatsForDefaultDisplay(Landroid/content/Context;Ljava/util/List;[Ljava/lang/String;Z)[I
    :try_end_0
    .catch Lcom/google/android/exoplayer/MediaCodecUtil$DecoderQueryException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 194
    array-length v2, v2

    if-nez v2, :cond_0

    .line 195
    const/4 v2, 0x1

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string/jumbo v4, "No variants selected."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/twitter/library/av/playback/j;->a(ZLjava/lang/Exception;)V

    .line 246
    :goto_0
    return-void

    .line 190
    :catch_0
    move-exception v2

    .line 191
    const/4 v3, 0x1

    invoke-virtual {p0, v3, v2}, Lcom/twitter/library/av/playback/j;->a(ZLjava/lang/Exception;)V

    goto :goto_0

    .line 200
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/j;->q()Landroid/os/Handler;

    move-result-object v12

    .line 201
    new-instance v7, Lbrj;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/exoplayer/upstream/TransferListener;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/j;->s()Lbrh;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {v7, v2}, Lbrj;-><init>([Lcom/google/android/exoplayer/upstream/TransferListener;)V

    .line 202
    new-instance v9, Lcom/twitter/library/av/playback/e;

    const/16 v2, 0x2000

    const/16 v3, 0xc9

    invoke-direct {v9, v2, v3}, Lcom/twitter/library/av/playback/e;-><init>(II)V

    .line 205
    new-instance v2, Lcom/twitter/library/av/playback/e;

    const/16 v3, 0x2000

    const/16 v4, 0x29

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/av/playback/e;-><init>(II)V

    .line 208
    invoke-virtual {v9, p0}, Lcom/twitter/library/av/playback/e;->a(Lcom/twitter/library/av/playback/e$a;)V

    .line 209
    invoke-virtual {v2, p0}, Lcom/twitter/library/av/playback/e;->a(Lcom/twitter/library/av/playback/e$a;)V

    .line 211
    iget-object v2, p0, Lcom/twitter/library/av/playback/j;->o:Lcom/twitter/library/av/playback/ExoPlayerHelper;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v2, v0, v7, v1}, Lcom/twitter/library/av/playback/ExoPlayerHelper;->a(Landroid/content/Context;Lcom/google/android/exoplayer/upstream/TransferListener;Ljava/lang/String;)Lcom/google/android/exoplayer/upstream/DataSource;

    move-result-object v4

    .line 213
    new-instance v8, Lcom/google/android/exoplayer/hls/PtsTimestampAdjusterProvider;

    invoke-direct {v8}, Lcom/google/android/exoplayer/hls/PtsTimestampAdjusterProvider;-><init>()V

    .line 215
    new-instance v2, Lcom/google/android/exoplayer/hls/HlsChunkSource;

    const/4 v3, 0x1

    .line 217
    invoke-static/range {p1 .. p1}, Lcom/google/android/exoplayer/hls/DefaultHlsTrackSelector;->newDefaultInstance(Landroid/content/Context;)Lcom/google/android/exoplayer/hls/DefaultHlsTrackSelector;

    move-result-object v6

    move-object/from16 v5, p3

    invoke-direct/range {v2 .. v8}, Lcom/google/android/exoplayer/hls/HlsChunkSource;-><init>(ZLcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/hls/HlsPlaylist;Lcom/google/android/exoplayer/hls/HlsTrackSelector;Lcom/google/android/exoplayer/upstream/BandwidthMeter;Lcom/google/android/exoplayer/hls/PtsTimestampAdjusterProvider;)V

    .line 219
    iget-object v3, p0, Lcom/twitter/library/av/playback/j;->s:Lcom/twitter/library/av/playback/ap;

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer/hls/HlsChunkSource;->setDelegate(Lcom/google/android/exoplayer/hls/HlsChunkSource$Delegate;)V

    .line 220
    new-instance v5, Lcom/google/android/exoplayer/DefaultLoadControl;

    invoke-direct {v5, v9}, Lcom/google/android/exoplayer/DefaultLoadControl;-><init>(Lcom/google/android/exoplayer/upstream/Allocator;)V

    .line 221
    new-instance v3, Lcom/google/android/exoplayer/hls/HlsSampleSource;

    const/high16 v6, 0x190000

    new-instance v8, Lcom/twitter/library/av/playback/j$c;

    invoke-direct {v8, p0}, Lcom/twitter/library/av/playback/j$c;-><init>(Lcom/twitter/library/av/playback/j;)V

    const/4 v9, 0x0

    const/16 v10, 0x8

    move-object v4, v2

    move-object v7, v12

    invoke-direct/range {v3 .. v10}, Lcom/google/android/exoplayer/hls/HlsSampleSource;-><init>(Lcom/google/android/exoplayer/hls/HlsChunkSource;Lcom/google/android/exoplayer/LoadControl;ILandroid/os/Handler;Lcom/google/android/exoplayer/hls/HlsSampleSource$EventListener;II)V

    .line 227
    new-instance v5, Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer;

    sget-object v8, Lcom/google/android/exoplayer/MediaCodecSelector;->DEFAULT:Lcom/google/android/exoplayer/MediaCodecSelector;

    const/4 v9, 0x1

    const-wide/16 v10, 0x1388

    new-instance v13, Lcom/twitter/library/av/playback/j$e;

    invoke-direct {v13, p0}, Lcom/twitter/library/av/playback/j$e;-><init>(Lcom/twitter/library/av/playback/j;)V

    const/4 v14, -0x1

    move-object/from16 v6, p1

    move-object v7, v3

    invoke-direct/range {v5 .. v14}, Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer;-><init>(Landroid/content/Context;Lcom/google/android/exoplayer/SampleSource;Lcom/google/android/exoplayer/MediaCodecSelector;IJLandroid/os/Handler;Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$EventListener;I)V

    .line 232
    new-instance v7, Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer;

    sget-object v9, Lcom/google/android/exoplayer/MediaCodecSelector;->DEFAULT:Lcom/google/android/exoplayer/MediaCodecSelector;

    const/4 v10, 0x0

    const/4 v11, 0x1

    move-object v8, v3

    move-object v13, p0

    invoke-direct/range {v7 .. v13}, Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer;-><init>(Lcom/google/android/exoplayer/SampleSource;Lcom/google/android/exoplayer/MediaCodecSelector;Lcom/google/android/exoplayer/drm/DrmSessionManager;ZLandroid/os/Handler;Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer$EventListener;)V

    .line 235
    iget-boolean v2, p0, Lcom/twitter/library/av/playback/j;->q:Z

    if-eqz v2, :cond_1

    .line 238
    new-instance v2, Lcom/google/android/exoplayer/text/eia608/Eia608TrackRenderer;

    new-instance v4, Lcom/twitter/library/av/playback/j$d;

    invoke-direct {v4, p0}, Lcom/twitter/library/av/playback/j$d;-><init>(Lcom/twitter/library/av/playback/j;)V

    .line 239
    invoke-virtual {v12}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-direct {v2, v3, v4, v6}, Lcom/google/android/exoplayer/text/eia608/Eia608TrackRenderer;-><init>(Lcom/google/android/exoplayer/SampleSource;Lcom/google/android/exoplayer/text/TextRenderer;Landroid/os/Looper;)V

    .line 240
    invoke-virtual {p0, v2}, Lcom/twitter/library/av/playback/j;->c(Lcom/google/android/exoplayer/TrackRenderer;)V

    .line 243
    :cond_1
    invoke-virtual {p0, v7}, Lcom/twitter/library/av/playback/j;->b(Lcom/google/android/exoplayer/TrackRenderer;)V

    .line 244
    invoke-virtual {p0, v5}, Lcom/twitter/library/av/playback/j;->a(Lcom/google/android/exoplayer/TrackRenderer;)V

    .line 245
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/j;->j()V

    goto/16 :goto_0
.end method

.method protected k()V
    .locals 2

    .prologue
    .line 169
    invoke-super {p0}, Lcom/twitter/library/av/playback/b;->k()V

    .line 172
    iget-boolean v0, p0, Lcom/twitter/library/av/playback/j;->r:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/j;->J()Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->c:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/j;->K()Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->c:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_1

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/playback/j;->p:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/library/av/playback/j;->e:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/av/playback/j;->a(Landroid/content/Context;Lcom/twitter/model/av/AVMedia;)V

    .line 175
    :cond_1
    return-void
.end method

.method protected l()V
    .locals 1

    .prologue
    .line 158
    invoke-super {p0}, Lcom/twitter/library/av/playback/b;->l()V

    .line 161
    iget-boolean v0, p0, Lcom/twitter/library/av/playback/j;->r:Z

    if-eqz v0, :cond_0

    .line 162
    sget-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;->c:Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/j;->b(Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;)V

    .line 163
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/j;->r()Lcom/google/android/exoplayer/ExoPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/exoplayer/ExoPlayer;->stop()V

    .line 165
    :cond_0
    return-void
.end method

.method protected m()V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/twitter/library/av/playback/j;->t:Lcom/twitter/library/av/playback/al;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/twitter/library/av/playback/j;->t:Lcom/twitter/library/av/playback/al;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/al;->a()V

    .line 123
    iget-object v0, p0, Lcom/twitter/library/av/playback/j;->c:Lbix;

    iget-object v1, p0, Lcom/twitter/library/av/playback/j;->t:Lcom/twitter/library/av/playback/al;

    invoke-virtual {v0, v1}, Lbix;->b(Lbiy;)Lbix;

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/playback/j;->s:Lcom/twitter/library/av/playback/ap;

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Lcom/twitter/library/av/playback/j;->s:Lcom/twitter/library/av/playback/ap;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/ap;->a()V

    .line 128
    :cond_1
    invoke-super {p0}, Lcom/twitter/library/av/playback/b;->m()V

    .line 129
    return-void
.end method

.method public onPlayerError(Lcom/google/android/exoplayer/ExoPlaybackException;)V
    .locals 4

    .prologue
    .line 250
    const-string/jumbo v0, "AVMediaExoPlayerHls"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onPlayerError() called with: error = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    const-string/jumbo v0, "live_video_force_skip_forward_android_enabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 252
    invoke-virtual {p1}, Lcom/google/android/exoplayer/ExoPlaybackException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/exoplayer/BehindLiveWindowException;

    if-eqz v0, :cond_1

    .line 254
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/j;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/j;->x()V

    .line 258
    iget-object v0, p0, Lcom/twitter/library/av/playback/j;->c:Lbix;

    new-instance v1, Lcom/twitter/library/av/playback/j$b;

    iget-object v2, p0, Lcom/twitter/library/av/playback/j;->e:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {p1}, Lcom/google/android/exoplayer/ExoPlaybackException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/av/playback/j$b;-><init>(Lcom/twitter/model/av/AVMedia;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Lbix;->a(Lbiw;)V

    .line 259
    iget-object v0, p0, Lcom/twitter/library/av/playback/j;->p:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/library/av/playback/j;->e:Lcom/twitter/model/av/AVMedia;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/av/playback/j;->a(Landroid/content/Context;Lcom/twitter/model/av/AVMedia;)V

    .line 267
    :cond_0
    :goto_0
    return-void

    .line 262
    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/library/av/playback/b;->onPlayerError(Lcom/google/android/exoplayer/ExoPlaybackException;)V

    goto :goto_0

    .line 265
    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/library/av/playback/b;->onPlayerError(Lcom/google/android/exoplayer/ExoPlaybackException;)V

    goto :goto_0
.end method

.method public onPlayerSeekComplete()V
    .locals 0

    .prologue
    .line 272
    return-void
.end method

.method protected u()Z
    .locals 1

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/twitter/library/av/playback/j;->r:Z

    return v0
.end method
