.class public Lcom/twitter/library/av/playback/m;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/library/av/playback/m;


# instance fields
.field private final b:I


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 49
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/twitter/library/av/playback/m;-><init>(I)V

    .line 50
    return-void
.end method

.method protected constructor <init>(I)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput p1, p0, Lcom/twitter/library/av/playback/m;->b:I

    .line 57
    return-void
.end method

.method public static declared-synchronized a()Lcom/twitter/library/av/playback/m;
    .locals 2

    .prologue
    .line 29
    const-class v1, Lcom/twitter/library/av/playback/m;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/av/playback/m;->a:Lcom/twitter/library/av/playback/m;

    if-nez v0, :cond_0

    .line 30
    const-class v0, Lcom/twitter/library/av/playback/m;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 31
    new-instance v0, Lcom/twitter/library/av/playback/m;

    invoke-direct {v0}, Lcom/twitter/library/av/playback/m;-><init>()V

    sput-object v0, Lcom/twitter/library/av/playback/m;->a:Lcom/twitter/library/av/playback/m;

    .line 33
    :cond_0
    sget-object v0, Lcom/twitter/library/av/playback/m;->a:Lcom/twitter/library/av/playback/m;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(Lcom/twitter/library/av/playback/l;)Lcom/twitter/library/av/playback/AVMediaPlayer;
    .locals 3

    .prologue
    .line 69
    new-instance v0, Lcom/twitter/library/av/playback/m$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/av/playback/m$1;-><init>(Lcom/twitter/library/av/playback/m;Lcom/twitter/library/av/playback/l;)V

    .line 95
    iget-object v1, p1, Lcom/twitter/library/av/playback/l;->e:Landroid/os/Handler;

    iget v2, p0, Lcom/twitter/library/av/playback/m;->b:I

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/av/playback/at;->a(Landroid/os/Handler;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/playback/AVMediaPlayer;

    return-object v0
.end method
