.class public Lcom/twitter/library/av/playback/AVPlayer$e;
.super Lcqy;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/av/playback/AVPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqy",
        "<",
        "Lcom/twitter/model/av/AVMediaPlaylist;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/library/av/playback/AVPlayer;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/twitter/library/av/playback/AVPlayer$b;


# direct methods
.method constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/library/av/playback/AVPlayer$b;)V
    .locals 1

    .prologue
    .line 1608
    invoke-direct {p0}, Lcqy;-><init>()V

    .line 1609
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer$e;->a:Ljava/lang/ref/WeakReference;

    .line 1610
    iput-object p2, p0, Lcom/twitter/library/av/playback/AVPlayer$e;->b:Lcom/twitter/library/av/playback/AVPlayer$b;

    .line 1611
    new-instance v0, Lcom/twitter/library/av/playback/AVPlayer$e$1;

    invoke-direct {v0, p0}, Lcom/twitter/library/av/playback/AVPlayer$e$1;-><init>(Lcom/twitter/library/av/playback/AVPlayer$e;)V

    invoke-static {v0}, Lcwy;->a(Lrx/functions/a;)Lrx/j;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/AVPlayer$e;->a(Lrx/j;)V

    .line 1620
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/av/playback/AVPlayer$e;)Lcom/twitter/library/av/playback/AVPlayer$b;
    .locals 1

    .prologue
    .line 1598
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer$e;->b:Lcom/twitter/library/av/playback/AVPlayer$b;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/library/av/playback/AVPlayer$e;Lcom/twitter/library/av/playback/AVPlayer$b;)Lcom/twitter/library/av/playback/AVPlayer$b;
    .locals 0

    .prologue
    .line 1598
    iput-object p1, p0, Lcom/twitter/library/av/playback/AVPlayer$e;->b:Lcom/twitter/library/av/playback/AVPlayer$b;

    return-object p1
.end method


# virtual methods
.method public a(Lcom/twitter/model/av/AVMediaPlaylist;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1624
    iget-object v0, p0, Lcom/twitter/library/av/playback/AVPlayer$e;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/playback/AVPlayer;

    .line 1625
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/twitter/model/av/AVMediaPlaylist;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    .line 1626
    :goto_0
    if-eqz v0, :cond_0

    .line 1627
    invoke-virtual {v0, p1}, Lcom/twitter/library/av/playback/AVPlayer;->b(Lcom/twitter/model/av/AVMediaPlaylist;)V

    .line 1628
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->T()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/model/av/AVMediaPlaylist;Landroid/content/res/Resources;)V

    .line 1631
    :cond_0
    iget-object v2, p0, Lcom/twitter/library/av/playback/AVPlayer$e;->b:Lcom/twitter/library/av/playback/AVPlayer$b;

    if-eqz v2, :cond_1

    .line 1632
    if-eqz v1, :cond_4

    .line 1633
    iget-object v1, p0, Lcom/twitter/library/av/playback/AVPlayer$e;->b:Lcom/twitter/library/av/playback/AVPlayer$b;

    invoke-interface {v1}, Lcom/twitter/library/av/playback/AVPlayer$b;->n()V

    .line 1638
    :goto_1
    iput-object v3, p0, Lcom/twitter/library/av/playback/AVPlayer$e;->b:Lcom/twitter/library/av/playback/AVPlayer$b;

    .line 1641
    :cond_1
    if-eqz v0, :cond_2

    invoke-static {v0}, Lcom/twitter/library/av/playback/AVPlayer;->b(Lcom/twitter/library/av/playback/AVPlayer;)Lrx/j;

    move-result-object v1

    if-ne v1, p0, :cond_2

    .line 1642
    invoke-static {v0, v3}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/playback/AVPlayer;Lrx/j;)Lrx/j;

    .line 1644
    :cond_2
    return-void

    .line 1625
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 1635
    :cond_4
    iget-object v2, p0, Lcom/twitter/library/av/playback/AVPlayer$e;->b:Lcom/twitter/library/av/playback/AVPlayer$b;

    if-eqz p1, :cond_5

    .line 1636
    invoke-virtual {p1}, Lcom/twitter/model/av/AVMediaPlaylist;->f()Lcom/twitter/model/av/c;

    move-result-object v1

    .line 1635
    :goto_2
    invoke-interface {v2, v1}, Lcom/twitter/library/av/playback/AVPlayer$b;->a(Lcom/twitter/model/av/c;)V

    goto :goto_1

    .line 1636
    :cond_5
    sget-object v1, Lcom/twitter/model/av/c;->a:Lcom/twitter/model/av/c;

    goto :goto_2
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1598
    check-cast p1, Lcom/twitter/model/av/AVMediaPlaylist;

    invoke-virtual {p0, p1}, Lcom/twitter/library/av/playback/AVPlayer$e;->a(Lcom/twitter/model/av/AVMediaPlaylist;)V

    return-void
.end method
