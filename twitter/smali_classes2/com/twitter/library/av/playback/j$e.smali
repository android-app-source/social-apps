.class Lcom/twitter/library/av/playback/j$e;
.super Lcom/twitter/library/av/playback/as;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/av/playback/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "e"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/av/playback/j;


# direct methods
.method constructor <init>(Lcom/twitter/library/av/playback/j;)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lcom/twitter/library/av/playback/j$e;->a:Lcom/twitter/library/av/playback/j;

    invoke-direct {p0}, Lcom/twitter/library/av/playback/as;-><init>()V

    return-void
.end method


# virtual methods
.method public onCryptoError(Landroid/media/MediaCodec$CryptoException;)V
    .locals 2

    .prologue
    .line 303
    iget-object v0, p0, Lcom/twitter/library/av/playback/j$e;->a:Lcom/twitter/library/av/playback/j;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/av/playback/j;->a(ZLjava/lang/Exception;)V

    .line 304
    return-void
.end method

.method public onDecoderInitializationError(Lcom/google/android/exoplayer/MediaCodecTrackRenderer$DecoderInitializationException;)V
    .locals 2

    .prologue
    .line 298
    iget-object v0, p0, Lcom/twitter/library/av/playback/j$e;->a:Lcom/twitter/library/av/playback/j;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/av/playback/j;->a(ZLjava/lang/Exception;)V

    .line 299
    return-void
.end method

.method public onDrawnToSurface(Landroid/view/Surface;)V
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/twitter/library/av/playback/j$e;->a:Lcom/twitter/library/av/playback/j;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/playback/j;->onDrawnToSurface(Landroid/view/Surface;)V

    .line 315
    return-void
.end method

.method public onVideoSizeChanged(IIIF)V
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/twitter/library/av/playback/j$e;->a:Lcom/twitter/library/av/playback/j;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/twitter/library/av/playback/j;->onVideoSizeChanged(IIIF)V

    .line 310
    return-void
.end method
