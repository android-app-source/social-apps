.class public Lcom/twitter/library/av/playback/r;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Lbyf;

.field private final b:Lcom/twitter/library/av/playback/q;

.field private c:Landroid/content/Context;

.field private d:Lcom/twitter/library/av/playback/u;

.field private e:Z

.field private f:Z

.field private g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/q;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    sget-object v0, Lbyo;->c:Lbyf;

    iput-object v0, p0, Lcom/twitter/library/av/playback/r;->a:Lbyf;

    .line 24
    iput-boolean v1, p0, Lcom/twitter/library/av/playback/r;->e:Z

    .line 25
    iput-boolean v1, p0, Lcom/twitter/library/av/playback/r;->f:Z

    .line 29
    iput-object p1, p0, Lcom/twitter/library/av/playback/r;->b:Lcom/twitter/library/av/playback/q;

    .line 30
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/library/av/playback/AVPlayerAttachment;
    .locals 5

    .prologue
    .line 70
    iget-object v0, p0, Lcom/twitter/library/av/playback/r;->d:Lcom/twitter/library/av/playback/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/playback/r;->c:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 71
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Obtaining an AVPlayer for playback requires  a delegate, event handler and context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/av/playback/r;->b:Lcom/twitter/library/av/playback/q;

    iget-object v1, p0, Lcom/twitter/library/av/playback/r;->d:Lcom/twitter/library/av/playback/u;

    iget-object v2, p0, Lcom/twitter/library/av/playback/r;->a:Lbyf;

    iget-object v3, p0, Lcom/twitter/library/av/playback/r;->c:Landroid/content/Context;

    .line 75
    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/library/av/playback/r;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/library/av/playback/q;->a(Lcom/twitter/library/av/playback/u;Lbyf;Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/library/av/playback/r;->e:Z

    invoke-virtual {v1, v2}, Lcom/twitter/library/av/playback/AVPlayer;->c(Z)V

    .line 78
    iget-boolean v1, p0, Lcom/twitter/library/av/playback/r;->f:Z

    if-eqz v1, :cond_2

    .line 79
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->i()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 81
    :cond_2
    return-object v0
.end method

.method public a(Landroid/content/Context;)Lcom/twitter/library/av/playback/r;
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/twitter/library/av/playback/r;->c:Landroid/content/Context;

    .line 35
    return-object p0
.end method

.method public a(Lbyf;)Lcom/twitter/library/av/playback/r;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/twitter/library/av/playback/r;->a:Lbyf;

    .line 53
    return-object p0
.end method

.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/av/playback/r;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/twitter/library/av/playback/r;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 47
    return-object p0
.end method

.method public a(Lcom/twitter/library/av/playback/u;)Lcom/twitter/library/av/playback/r;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/twitter/library/av/playback/r;->d:Lcom/twitter/library/av/playback/u;

    .line 41
    return-object p0
.end method

.method public a(Z)Lcom/twitter/library/av/playback/r;
    .locals 0

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/twitter/library/av/playback/r;->f:Z

    .line 59
    return-object p0
.end method

.method public b(Z)Lcom/twitter/library/av/playback/r;
    .locals 0

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/twitter/library/av/playback/r;->e:Z

    .line 65
    return-object p0
.end method
