.class public Lcom/twitter/library/av/playback/PeriscopeDataSource;
.super Lcom/twitter/library/av/playback/TweetAVDataSource;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/playback/PeriscopeDataSource$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/library/av/playback/PeriscopeDataSource;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

.field private final d:Lcom/twitter/model/card/property/Vector2F;

.field private final e:Ljava/lang/String;

.field private final f:J

.field private final g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/twitter/library/av/playback/PeriscopeDataSource$1;

    invoke-direct {v0}, Lcom/twitter/library/av/playback/PeriscopeDataSource$1;-><init>()V

    sput-object v0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/twitter/library/av/playback/TweetAVDataSource;-><init>(Landroid/os/Parcel;)V

    .line 46
    const-class v0, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    iput-object v0, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->e:Ljava/lang/String;

    .line 48
    sget-object v0, Lcom/twitter/model/card/property/Vector2F;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/card/property/Vector2F;

    .line 49
    if-nez v0, :cond_0

    sget-object v0, Lcom/twitter/model/card/property/Vector2F;->b:Lcom/twitter/model/card/property/Vector2F;

    :cond_0
    iput-object v0, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->d:Lcom/twitter/model/card/property/Vector2F;

    .line 50
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->f:J

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->g:Z

    .line 52
    return-void

    .line 51
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/api/periscope/PeriscopeCapiModel;Lcom/twitter/model/card/property/Vector2F;Ljava/lang/String;JZ)V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/av/playback/TweetAVDataSource;-><init>(Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 57
    iput-object p2, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    .line 58
    iput-object p3, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->d:Lcom/twitter/model/card/property/Vector2F;

    .line 59
    iput-object p4, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->e:Ljava/lang/String;

    .line 60
    iput-wide p5, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->f:J

    .line 61
    iput-boolean p7, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->g:Z

    .line 62
    return-void
.end method


# virtual methods
.method public b()Lcom/twitter/model/card/property/ImageSpec;
    .locals 3

    .prologue
    .line 67
    new-instance v0, Lcom/twitter/model/card/property/ImageSpec;

    invoke-direct {v0}, Lcom/twitter/model/card/property/ImageSpec;-><init>()V

    .line 68
    iget-object v1, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->d:Lcom/twitter/model/card/property/Vector2F;

    iput-object v1, v0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    .line 69
    iget-object v1, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v1}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->f()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 70
    iget-object v1, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v1}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/model/card/property/ImageSpec;->b:Ljava/lang/String;

    .line 75
    :cond_0
    :goto_0
    return-object v0

    .line 71
    :cond_1
    iget-object v1, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v1

    invoke-virtual {v1}, Lcax;->K()Lcar;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 72
    const-string/jumbo v1, "full_size_thumbnail_url"

    iget-object v2, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->b:Lcom/twitter/model/core/Tweet;

    .line 73
    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v2

    invoke-virtual {v2}, Lcax;->K()Lcar;

    move-result-object v2

    .line 72
    invoke-static {v1, v2}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/model/card/property/ImageSpec;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 119
    if-ne p0, p1, :cond_1

    .line 120
    const/4 v0, 0x1

    .line 143
    :cond_0
    :goto_0
    return v0

    .line 122
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 125
    invoke-super {p0, p1}, Lcom/twitter/library/av/playback/TweetAVDataSource;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 129
    check-cast p1, Lcom/twitter/library/av/playback/PeriscopeDataSource;

    .line 131
    iget-wide v2, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->f:J

    iget-wide v4, p1, Lcom/twitter/library/av/playback/PeriscopeDataSource;->f:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 134
    iget-object v1, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    iget-object v2, p1, Lcom/twitter/library/av/playback/PeriscopeDataSource;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    iget-object v1, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->d:Lcom/twitter/model/card/property/Vector2F;

    iget-object v2, p1, Lcom/twitter/library/av/playback/PeriscopeDataSource;->d:Lcom/twitter/model/card/property/Vector2F;

    invoke-virtual {v1, v2}, Lcom/twitter/model/card/property/Vector2F;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 140
    iget-boolean v1, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->g:Z

    iget-boolean v2, p1, Lcom/twitter/library/av/playback/PeriscopeDataSource;->g:Z

    if-ne v1, v2, :cond_0

    .line 143
    iget-object v0, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->e:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/library/av/playback/PeriscopeDataSource;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->g:Z

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 93
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "getPlaylistUrl not supported for PeriscopeDataSource. Use getPlaylistFactory"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public synthetic h()Lbyp;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/PeriscopeDataSource;->r()Lbkq;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 148
    invoke-super {p0}, Lcom/twitter/library/av/playback/TweetAVDataSource;->hashCode()I

    move-result v0

    .line 149
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {v1}, Lcom/twitter/library/api/periscope/PeriscopeCapiModel;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 150
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->d:Lcom/twitter/model/card/property/Vector2F;

    invoke-virtual {v1}, Lcom/twitter/model/card/property/Vector2F;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 151
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 152
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->f:J

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    .line 154
    return v0

    .line 153
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Lcom/twitter/library/av/playback/ax;
    .locals 3

    .prologue
    .line 159
    new-instance v0, Lcom/twitter/library/av/playback/PeriscopeDataSource$a;

    iget-object v1, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->b:Lcom/twitter/model/core/Tweet;

    iget-object v2, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/av/playback/PeriscopeDataSource$a;-><init>(Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    return-object v0
.end method

.method public o()F
    .locals 1

    .prologue
    .line 99
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public q()Lcom/twitter/library/api/periscope/PeriscopeCapiModel;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    return-object v0
.end method

.method public r()Lbkq;
    .locals 8

    .prologue
    .line 86
    new-instance v1, Lbky;

    iget-object v3, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    iget-object v4, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->e:Ljava/lang/String;

    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v5

    iget-wide v6, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->f:J

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lbky;-><init>(Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/library/api/periscope/PeriscopeCapiModel;Ljava/lang/String;Lcrr;J)V

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 109
    invoke-super {p0, p1, p2}, Lcom/twitter/library/av/playback/TweetAVDataSource;->writeToParcel(Landroid/os/Parcel;I)V

    .line 110
    iget-object v0, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->c:Lcom/twitter/library/api/periscope/PeriscopeCapiModel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 111
    iget-object v0, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->d:Lcom/twitter/model/card/property/Vector2F;

    sget-object v1, Lcom/twitter/model/card/property/Vector2F;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 113
    iget-wide v0, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->f:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 114
    iget-boolean v0, p0, Lcom/twitter/library/av/playback/PeriscopeDataSource;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 115
    return-void

    .line 114
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
