.class public Lcom/twitter/library/av/playback/CardAVDataSource$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/av/playback/ax;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/av/playback/CardAVDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field protected final a:Lcax;

.field final synthetic b:Lcom/twitter/library/av/playback/CardAVDataSource;

.field private final d:Lbis;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/CardAVDataSource;Lcax;)V
    .locals 1

    .prologue
    .line 262
    iput-object p1, p0, Lcom/twitter/library/av/playback/CardAVDataSource$a;->b:Lcom/twitter/library/av/playback/CardAVDataSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 263
    iput-object p2, p0, Lcom/twitter/library/av/playback/CardAVDataSource$a;->a:Lcax;

    .line 264
    new-instance v0, Lbir;

    invoke-direct {v0, p2}, Lbir;-><init>(Lcax;)V

    iput-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource$a;->d:Lbis;

    .line 265
    return-void
.end method


# virtual methods
.method public a()Lbis;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource$a;->d:Lbis;

    return-object v0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 4

    .prologue
    .line 271
    iget-object v0, p0, Lcom/twitter/library/av/playback/CardAVDataSource$a;->b:Lcom/twitter/library/av/playback/CardAVDataSource;

    invoke-static {v0}, Lcom/twitter/library/av/playback/CardAVDataSource;->a(Lcom/twitter/library/av/playback/CardAVDataSource;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/library/av/playback/CardAVDataSource$a;->a:Lcax;

    const/4 v3, 0x0

    invoke-static {p1, v0, v1, v2, v3}, Lcom/twitter/library/scribe/b;->a(Landroid/content/Context;JLcax;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcgi;
    .locals 1

    .prologue
    .line 283
    const/4 v0, 0x0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x0

    return-object v0
.end method
