.class Lcom/twitter/library/av/playback/m$1;
.super Lcom/twitter/library/av/playback/at;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/av/playback/m;->a(Lcom/twitter/library/av/playback/l;)Lcom/twitter/library/av/playback/AVMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/av/playback/at",
        "<",
        "Lcom/twitter/library/av/playback/AVMediaPlayer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/av/playback/l;

.field final synthetic b:Lcom/twitter/library/av/playback/m;


# direct methods
.method constructor <init>(Lcom/twitter/library/av/playback/m;Lcom/twitter/library/av/playback/l;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/twitter/library/av/playback/m$1;->b:Lcom/twitter/library/av/playback/m;

    iput-object p2, p0, Lcom/twitter/library/av/playback/m$1;->a:Lcom/twitter/library/av/playback/l;

    invoke-direct {p0}, Lcom/twitter/library/av/playback/at;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/library/av/playback/AVMediaPlayer;
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 72
    iget-object v0, p0, Lcom/twitter/library/av/playback/m$1;->a:Lcom/twitter/library/av/playback/l;

    iget-object v0, v0, Lcom/twitter/library/av/playback/l;->a:Lcom/twitter/model/av/AVMedia;

    .line 74
    invoke-interface {v0}, Lcom/twitter/model/av/AVMedia;->h()I

    move-result v1

    .line 75
    if-ne v3, v1, :cond_0

    .line 76
    new-instance v0, Lcom/twitter/library/av/playback/i;

    iget-object v1, p0, Lcom/twitter/library/av/playback/m$1;->a:Lcom/twitter/library/av/playback/l;

    new-instance v2, Lcom/twitter/library/av/playback/ExoPlayerHelper;

    invoke-direct {v2}, Lcom/twitter/library/av/playback/ExoPlayerHelper;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/av/playback/i;-><init>(Lcom/twitter/library/av/playback/l;Lcom/twitter/library/av/playback/ExoPlayerHelper;)V

    .line 91
    :goto_0
    return-object v0

    .line 77
    :cond_0
    const/4 v2, 0x3

    if-ne v2, v1, :cond_1

    .line 78
    new-instance v1, Lbrj;

    new-array v2, v3, [Lcom/google/android/exoplayer/upstream/TransferListener;

    const/4 v3, 0x0

    new-instance v4, Lbrh;

    iget-object v5, p0, Lcom/twitter/library/av/playback/m$1;->a:Lcom/twitter/library/av/playback/l;

    iget-object v5, v5, Lcom/twitter/library/av/playback/l;->b:Lbix;

    invoke-direct {v4, v5}, Lbrh;-><init>(Lbix;)V

    aput-object v4, v2, v3

    invoke-direct {v1, v2}, Lbrj;-><init>([Lcom/google/android/exoplayer/upstream/TransferListener;)V

    .line 80
    new-instance v2, Lczd;

    iget-object v3, p0, Lcom/twitter/library/av/playback/m$1;->a:Lcom/twitter/library/av/playback/l;

    iget-object v3, v3, Lcom/twitter/library/av/playback/l;->c:Landroid/content/Context;

    iget-object v4, p0, Lcom/twitter/library/av/playback/m$1;->a:Lcom/twitter/library/av/playback/l;

    iget-object v4, v4, Lcom/twitter/library/av/playback/l;->c:Landroid/content/Context;

    .line 81
    invoke-static {v4}, Lcom/twitter/library/network/ab;->a(Landroid/content/Context;)Lcom/twitter/library/network/ab;

    move-result-object v4

    iget-object v4, v4, Lcom/twitter/library/network/ab;->c:Lcom/twitter/library/network/ae;

    invoke-virtual {v4}, Lcom/twitter/library/network/ae;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lcom/twitter/model/av/AVMedia;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v4, v0, v1}, Lczd;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/exoplayer/upstream/BandwidthMeter;)V

    .line 83
    new-instance v0, Lcom/twitter/library/av/playback/ai;

    iget-object v1, p0, Lcom/twitter/library/av/playback/m$1;->a:Lcom/twitter/library/av/playback/l;

    new-instance v3, Lczi;

    invoke-direct {v3, v2}, Lczi;-><init>(Lczi$f;)V

    invoke-direct {v0, v1, v3}, Lcom/twitter/library/av/playback/ai;-><init>(Lcom/twitter/library/av/playback/l;Lczi;)V

    goto :goto_0

    .line 84
    :cond_1
    const/4 v0, 0x2

    if-ne v0, v1, :cond_2

    .line 85
    new-instance v0, Lcom/twitter/library/av/playback/j$a;

    iget-object v1, p0, Lcom/twitter/library/av/playback/m$1;->a:Lcom/twitter/library/av/playback/l;

    new-instance v2, Lcom/twitter/library/av/playback/ExoPlayerHelper;

    invoke-direct {v2}, Lcom/twitter/library/av/playback/ExoPlayerHelper;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/av/playback/j$a;-><init>(Lcom/twitter/library/av/playback/l;Lcom/twitter/library/av/playback/ExoPlayerHelper;)V

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/j$a;->a()Lcom/twitter/library/av/playback/j;

    move-result-object v0

    goto :goto_0

    .line 86
    :cond_2
    const/4 v0, 0x4

    if-ne v0, v1, :cond_3

    .line 87
    new-instance v0, Lcom/twitter/library/av/playback/h;

    iget-object v1, p0, Lcom/twitter/library/av/playback/m$1;->a:Lcom/twitter/library/av/playback/l;

    invoke-direct {v0, v1}, Lcom/twitter/library/av/playback/h;-><init>(Lcom/twitter/library/av/playback/l;)V

    goto :goto_0

    .line 89
    :cond_3
    new-instance v0, Lcom/twitter/library/av/playback/k;

    iget-object v1, p0, Lcom/twitter/library/av/playback/m$1;->a:Lcom/twitter/library/av/playback/l;

    invoke-direct {v0, v1}, Lcom/twitter/library/av/playback/k;-><init>(Lcom/twitter/library/av/playback/l;)V

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/m$1;->a()Lcom/twitter/library/av/playback/AVMediaPlayer;

    move-result-object v0

    return-object v0
.end method
