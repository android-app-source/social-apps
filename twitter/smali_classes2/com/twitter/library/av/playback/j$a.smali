.class public Lcom/twitter/library/av/playback/j$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/av/playback/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/av/playback/ExoPlayerHelper;

.field private final b:Lcom/twitter/library/av/playback/l;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/l;Lcom/twitter/library/av/playback/ExoPlayerHelper;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p2, p0, Lcom/twitter/library/av/playback/j$a;->a:Lcom/twitter/library/av/playback/ExoPlayerHelper;

    .line 81
    iput-object p1, p0, Lcom/twitter/library/av/playback/j$a;->b:Lcom/twitter/library/av/playback/l;

    .line 82
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/library/av/playback/j;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 86
    const-string/jumbo v0, "android_media_playback_enable_hls_subtitle_support"

    invoke-static {v0, v5}, Lcoj;->a(Ljava/lang/String;Z)Z

    move-result v4

    .line 88
    if-eqz v4, :cond_1

    const/4 v0, 0x3

    .line 89
    :goto_0
    invoke-static {}, Lcom/twitter/library/av/playback/b;->a()I

    move-result v1

    const/16 v2, 0x7d0

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer/ExoPlayer$Factory;->newInstance(III)Lcom/google/android/exoplayer/ExoPlayer;

    move-result-object v3

    .line 91
    new-instance v0, Lcom/twitter/library/av/playback/j;

    iget-object v1, p0, Lcom/twitter/library/av/playback/j$a;->b:Lcom/twitter/library/av/playback/l;

    iget-object v2, p0, Lcom/twitter/library/av/playback/j$a;->a:Lcom/twitter/library/av/playback/ExoPlayerHelper;

    iget-object v6, p0, Lcom/twitter/library/av/playback/j$a;->b:Lcom/twitter/library/av/playback/l;

    iget v6, v6, Lcom/twitter/library/av/playback/l;->g:I

    const/4 v7, 0x7

    if-ne v6, v7, :cond_0

    const/4 v5, 0x1

    :cond_0
    new-instance v6, Lcom/twitter/library/av/playback/am;

    invoke-direct {v6}, Lcom/twitter/library/av/playback/am;-><init>()V

    new-instance v7, Lcom/twitter/library/av/playback/aq;

    invoke-direct {v7}, Lcom/twitter/library/av/playback/aq;-><init>()V

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/av/playback/j;-><init>(Lcom/twitter/library/av/playback/l;Lcom/twitter/library/av/playback/ExoPlayerHelper;Lcom/google/android/exoplayer/ExoPlayer;ZZLcom/twitter/library/av/playback/am;Lcom/twitter/library/av/playback/aq;)V

    return-object v0

    .line 88
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method
