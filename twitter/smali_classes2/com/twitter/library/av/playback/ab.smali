.class public Lcom/twitter/library/av/playback/ab;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Lcom/twitter/model/core/Tweet;)I
    .locals 1

    .prologue
    .line 76
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->c(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    const/4 v0, 0x1

    .line 95
    :goto_0
    return v0

    .line 78
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ag()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    const/4 v0, 0x2

    goto :goto_0

    .line 80
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->J()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 81
    const/16 v0, 0x8

    goto :goto_0

    .line 82
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->K()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 83
    const/4 v0, 0x0

    goto :goto_0

    .line 84
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->L()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 85
    const/4 v0, 0x3

    goto :goto_0

    .line 86
    :cond_4
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ai()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 87
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/av/playback/ae;->a(Lcax;)I

    move-result v0

    goto :goto_0

    .line 88
    :cond_5
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->w(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 89
    const/4 v0, 0x4

    goto :goto_0

    .line 90
    :cond_6
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->aj()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 91
    const/4 v0, 0x6

    goto :goto_0

    .line 92
    :cond_7
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ak()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 93
    const/4 v0, 0x7

    goto :goto_0

    .line 95
    :cond_8
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;)J
    .locals 4

    .prologue
    .line 686
    if-eqz p1, :cond_0

    iget-wide v0, p1, Lcom/twitter/model/core/MediaEntity;->k:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 687
    iget-wide v0, p1, Lcom/twitter/model/core/MediaEntity;->k:J

    .line 689
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->s:J

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/av/playback/AVDataSource;)Lbyp;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 152
    if-eqz p0, :cond_0

    .line 153
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/Tweet;)I

    move-result v1

    .line 154
    packed-switch v1, :pswitch_data_0

    .line 193
    :cond_0
    :goto_0
    :pswitch_0
    return-object v0

    .line 156
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->P()Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/util/c;->e(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v1

    .line 157
    invoke-static {p0}, Lcom/twitter/media/util/q;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "gif_caching_enabled"

    .line 158
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 159
    :cond_1
    new-instance v0, Lbkr;

    invoke-direct {v0, p1, v1}, Lbkr;-><init>(Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/model/core/MediaEntity;)V

    goto :goto_0

    .line 161
    :cond_2
    new-instance v0, Lbkx;

    invoke-direct {v0, p1, v1}, Lbkx;-><init>(Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/model/core/MediaEntity;)V

    goto :goto_0

    .line 166
    :pswitch_2
    new-instance v0, Lbkx;

    .line 167
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->P()Ljava/lang/Iterable;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/model/util/c;->d(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lbkx;-><init>(Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/model/core/MediaEntity;)V

    goto :goto_0

    .line 170
    :pswitch_3
    new-instance v0, Lbkw;

    .line 171
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->P()Ljava/lang/Iterable;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/model/util/c;->d(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lbkw;-><init>(Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/model/core/MediaEntity;)V

    goto :goto_0

    .line 174
    :pswitch_4
    new-instance v0, Lbku;

    invoke-direct {v0, p1}, Lbku;-><init>(Lcom/twitter/library/av/playback/AVDataSource;)V

    goto :goto_0

    .line 177
    :pswitch_5
    new-instance v0, Lbkz;

    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->f(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->q(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/model/core/Tweet;->b:J

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lbkz;-><init>(Lcom/twitter/library/av/playback/AVDataSource;Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_0

    .line 181
    :pswitch_6
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/twitter/library/av/playback/ae;->a(Lcom/twitter/library/av/playback/AVDataSource;Lcax;)Lbyp;

    move-result-object v0

    goto :goto_0

    .line 184
    :pswitch_7
    new-instance v0, Lbkt;

    invoke-direct {v0, p1}, Lbkt;-><init>(Lcom/twitter/library/av/playback/AVDataSource;)V

    goto :goto_0

    .line 154
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/twitter/model/core/Tweet;I)Lcom/twitter/util/math/Size;
    .locals 2

    .prologue
    .line 452
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 453
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->m(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/card/property/ImageSpec;

    move-result-object v0

    .line 454
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    invoke-virtual {v0}, Lcom/twitter/model/card/property/Vector2F;->b()Lcom/twitter/util/math/Size;

    move-result-object v0

    .line 455
    :goto_1
    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->e()Z

    move-result v1

    if-nez v1, :cond_2

    .line 459
    :goto_2
    return-object v0

    .line 452
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 454
    :cond_1
    sget-object v0, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    goto :goto_1

    .line 458
    :cond_2
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->n(Lcom/twitter/model/core/Tweet;)F

    move-result v0

    .line 459
    int-to-float v1, p1

    mul-float/2addr v0, v1

    int-to-float v1, p1

    invoke-static {v0, v1}, Lcom/twitter/util/math/Size;->a(FF)Lcom/twitter/util/math/Size;

    move-result-object v0

    goto :goto_2
.end method

.method private static a(F)Ljava/lang/String;
    .locals 2

    .prologue
    .line 539
    const/4 v0, 0x0

    cmpl-float v0, p0, v0

    if-lez v0, :cond_0

    const/high16 v0, 0x447a0000    # 1000.0f

    mul-float/2addr v0, p0

    float-to-long v0, v0

    .line 540
    invoke-static {v0, v1}, Lcom/twitter/util/aa;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 539
    :goto_0
    return-object v0

    .line 540
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/model/core/MediaEntity;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 256
    if-eqz p0, :cond_0

    iget-object v1, p0, Lcom/twitter/model/core/MediaEntity;->p:Lcom/twitter/model/core/o;

    if-nez v1, :cond_1

    .line 263
    :cond_0
    :goto_0
    return-object v0

    .line 260
    :cond_1
    invoke-static {p0}, Lcom/twitter/model/util/c;->d(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 261
    invoke-static {p0}, Lcom/twitter/model/util/c;->e(Lcom/twitter/model/core/MediaEntity;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 263
    :cond_2
    iget-object v1, p0, Lcom/twitter/model/core/MediaEntity;->p:Lcom/twitter/model/core/o;

    iget-object v1, v1, Lcom/twitter/model/core/o;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/twitter/model/core/MediaEntity;->p:Lcom/twitter/model/core/o;

    iget-object v0, v0, Lcom/twitter/model/core/o;->d:Ljava/util/List;

    const/4 v1, 0x0

    .line 264
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/p;

    iget-object v0, v0, Lcom/twitter/model/core/p;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public static b(Lcom/twitter/model/core/MediaEntity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 534
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->c(Lcom/twitter/model/core/MediaEntity;)F

    move-result v0

    invoke-static {v0}, Lcom/twitter/library/av/playback/ab;->a(F)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/twitter/model/core/Tweet;)Z
    .locals 2

    .prologue
    .line 102
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/Tweet;)I

    move-result v0

    .line 103
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Lcom/twitter/model/core/MediaEntity;)F
    .locals 1

    .prologue
    .line 544
    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/twitter/model/util/c;->c(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 545
    iget-object v0, p0, Lcom/twitter/model/core/MediaEntity;->p:Lcom/twitter/model/core/o;

    iget v0, v0, Lcom/twitter/model/core/o;->c:F

    .line 547
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 110
    invoke-static {p0}, Lbwr;->c(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->al()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 119
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->I()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->c(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/twitter/model/core/Tweet;)Z
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->L()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ag()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 204
    if-eqz p0, :cond_0

    .line 205
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/Tweet;)I

    move-result v1

    .line 206
    packed-switch v1, :pswitch_data_0

    .line 235
    :cond_0
    :goto_0
    :pswitch_0
    return-object v0

    .line 208
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->P()Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/util/c;->e(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    .line 209
    invoke-static {v0}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/MediaEntity;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 214
    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->P()Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/util/c;->d(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    .line 215
    invoke-static {v0}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/MediaEntity;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 218
    :pswitch_3
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->i(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 221
    :pswitch_4
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->h(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 224
    :pswitch_5
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->g(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 227
    :pswitch_6
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/av/playback/ae;->d(Lcax;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 206
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method static g(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    .line 241
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcax;->m()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static h(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    .line 251
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcax;->n()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static i(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 270
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    .line 271
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcax;->j()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(Lcom/twitter/model/core/Tweet;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/Tweet;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 321
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/Tweet;)I

    move-result v0

    .line 322
    packed-switch v0, :pswitch_data_0

    .line 327
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    :goto_0
    return-object v0

    .line 324
    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/av/playback/ae;->f(Lcax;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    .line 322
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method public static k(Lcom/twitter/model/core/Tweet;)J
    .locals 3

    .prologue
    const-wide/16 v0, -0x1

    .line 343
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/Tweet;)I

    move-result v2

    .line 344
    packed-switch v2, :pswitch_data_0

    .line 361
    :cond_0
    :goto_0
    :pswitch_0
    return-wide v0

    .line 349
    :pswitch_1
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->s(Lcom/twitter/model/core/Tweet;)J

    move-result-wide v0

    goto :goto_0

    .line 355
    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v2

    .line 356
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcax;->h()Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    .line 357
    :goto_1
    if-eqz v2, :cond_0

    iget-wide v0, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    goto :goto_0

    .line 356
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 344
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static l(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/av/Partner;
    .locals 1

    .prologue
    .line 375
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/Tweet;)I

    move-result v0

    .line 376
    packed-switch v0, :pswitch_data_0

    .line 381
    sget-object v0, Lcom/twitter/model/av/Partner;->a:Lcom/twitter/model/av/Partner;

    :goto_0
    return-object v0

    .line 378
    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/av/playback/ae;->b(Lcax;)Lcom/twitter/model/av/Partner;

    move-result-object v0

    goto :goto_0

    .line 376
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method public static m(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/card/property/ImageSpec;
    .locals 4

    .prologue
    .line 395
    const/4 v0, 0x0

    .line 396
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/Tweet;)I

    move-result v1

    .line 397
    packed-switch v1, :pswitch_data_0

    .line 432
    :cond_0
    :goto_0
    :pswitch_0
    return-object v0

    .line 402
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v1

    .line 403
    if-eqz v1, :cond_0

    .line 405
    invoke-virtual {v1}, Lcax;->q()Lcom/twitter/model/card/property/ImageSpec;

    move-result-object v0

    goto :goto_0

    .line 414
    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->P()Ljava/lang/Iterable;

    move-result-object v2

    .line 415
    if-eqz v1, :cond_1

    const/4 v3, 0x7

    if-eq v1, v3, :cond_1

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 416
    :cond_1
    invoke-static {v2}, Lcom/twitter/model/util/c;->d(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v1

    .line 421
    :goto_1
    if-eqz v1, :cond_0

    .line 422
    new-instance v0, Lcom/twitter/model/card/property/ImageSpec;

    invoke-direct {v0}, Lcom/twitter/model/card/property/ImageSpec;-><init>()V

    .line 423
    iget-object v2, v1, Lcom/twitter/model/core/MediaEntity;->m:Ljava/lang/String;

    iput-object v2, v0, Lcom/twitter/model/card/property/ImageSpec;->b:Ljava/lang/String;

    .line 424
    new-instance v2, Lcom/twitter/model/card/property/Vector2F;

    iget-object v3, v1, Lcom/twitter/model/core/MediaEntity;->o:Lcom/twitter/util/math/Size;

    invoke-virtual {v3}, Lcom/twitter/util/math/Size;->a()I

    move-result v3

    int-to-float v3, v3

    iget-object v1, v1, Lcom/twitter/model/core/MediaEntity;->o:Lcom/twitter/util/math/Size;

    invoke-virtual {v1}, Lcom/twitter/util/math/Size;->b()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v2, v3, v1}, Lcom/twitter/model/card/property/Vector2F;-><init>(FF)V

    iput-object v2, v0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    goto :goto_0

    .line 418
    :cond_2
    invoke-static {v2}, Lcom/twitter/model/util/c;->e(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v1

    goto :goto_1

    .line 397
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static n(Lcom/twitter/model/core/Tweet;)F
    .locals 3

    .prologue
    const v0, 0x3fe38e39

    .line 471
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/Tweet;)I

    move-result v1

    .line 472
    packed-switch v1, :pswitch_data_0

    .line 502
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 478
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/av/playback/ae;->c(Lcax;)F

    move-result v0

    goto :goto_0

    .line 481
    :pswitch_2
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    .line 487
    :pswitch_3
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->P()Ljava/lang/Iterable;

    move-result-object v1

    .line 488
    invoke-static {v1}, Lcom/twitter/model/util/c;->c(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v1

    .line 490
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/twitter/model/core/MediaEntity;->o:Lcom/twitter/util/math/Size;

    invoke-virtual {v2}, Lcom/twitter/util/math/Size;->e()Z

    move-result v2

    if-nez v2, :cond_0

    .line 491
    iget-object v0, v1, Lcom/twitter/model/core/MediaEntity;->o:Lcom/twitter/util/math/Size;

    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->g()F

    move-result v0

    goto :goto_0

    .line 472
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public static o(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 516
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->P()Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/util/c;->d(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    .line 517
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v1

    .line 518
    invoke-static {v0}, Lcom/twitter/library/av/playback/ab;->c(Lcom/twitter/model/core/MediaEntity;)F

    move-result v0

    .line 519
    const/4 v2, 0x0

    cmpg-float v2, v0, v2

    if-gtz v2, :cond_0

    if-eqz v1, :cond_0

    .line 521
    const-string/jumbo v2, "content_duration_seconds"

    invoke-virtual {v1, v2}, Lcax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 523
    if-eqz v1, :cond_1

    .line 524
    :try_start_0
    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 529
    :cond_0
    :goto_0
    invoke-static {v0}, Lcom/twitter/library/av/playback/ab;->a(F)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 524
    :cond_1
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_0

    .line 525
    :catch_0
    move-exception v1

    .line 526
    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static p(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 552
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/Tweet;)I

    move-result v0

    .line 553
    packed-switch v0, :pswitch_data_0

    .line 588
    :cond_0
    :pswitch_0
    const-string/jumbo v0, ""

    :cond_1
    :goto_0
    return-object v0

    .line 555
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->P()Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/util/c;->e(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    .line 556
    if-eqz v0, :cond_2

    .line 557
    iget-object v0, v0, Lcom/twitter/model/core/MediaEntity;->F:Ljava/lang/String;

    goto :goto_0

    .line 564
    :cond_2
    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->P()Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/util/c;->d(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    .line 565
    if-eqz v0, :cond_3

    .line 566
    iget-wide v0, v0, Lcom/twitter/model/core/MediaEntity;->c:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 571
    :cond_3
    :pswitch_3
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    .line 572
    if-eqz v0, :cond_4

    .line 573
    invoke-virtual {v0}, Lcax;->o()Ljava/lang/String;

    move-result-object v0

    .line 574
    if-nez v0, :cond_1

    .line 581
    :cond_4
    :pswitch_4
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->q(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v0

    .line 582
    if-eqz v0, :cond_0

    goto :goto_0

    .line 553
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static q(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 599
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->Q()Ljava/lang/Iterable;

    move-result-object v0

    .line 601
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad;

    .line 602
    iget-object v0, v0, Lcom/twitter/model/core/ad;->F:Ljava/lang/String;

    .line 603
    invoke-static {v0}, Lcom/twitter/library/util/af;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 607
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static r(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 612
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/Tweet;)I

    move-result v0

    .line 613
    packed-switch v0, :pswitch_data_0

    .line 641
    :pswitch_0
    const-string/jumbo v0, ""

    :cond_0
    :goto_0
    return-object v0

    .line 618
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->P()Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/util/c;->c(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    .line 619
    if-eqz v0, :cond_1

    .line 620
    iget-object v0, v0, Lcom/twitter/model/core/MediaEntity;->F:Ljava/lang/String;

    goto :goto_0

    .line 625
    :cond_1
    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v1

    .line 626
    const/4 v0, 0x0

    .line 627
    if-eqz v1, :cond_2

    .line 628
    invoke-virtual {v1}, Lcax;->j()Ljava/lang/String;

    move-result-object v0

    .line 630
    :cond_2
    if-nez v0, :cond_0

    .line 631
    const-string/jumbo v0, ""

    goto :goto_0

    .line 638
    :pswitch_3
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->p(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 613
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static s(Lcom/twitter/model/core/Tweet;)J
    .locals 2

    .prologue
    .line 647
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/Tweet;)I

    move-result v0

    .line 648
    packed-switch v0, :pswitch_data_0

    .line 674
    :pswitch_0
    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    .line 650
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->P()Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/util/c;->e(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    .line 651
    invoke-static {p0, v0}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;)J

    move-result-wide v0

    goto :goto_0

    .line 656
    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->P()Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/util/c;->d(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    .line 657
    invoke-static {p0, v0}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;)J

    move-result-wide v0

    goto :goto_0

    .line 660
    :pswitch_3
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    .line 661
    if-eqz v0, :cond_0

    .line 662
    invoke-virtual {v0}, Lcax;->h()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 663
    if-eqz v0, :cond_0

    .line 664
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v0

    goto :goto_0

    .line 671
    :cond_0
    :pswitch_4
    iget-wide v0, p0, Lcom/twitter/model/core/Tweet;->s:J

    goto :goto_0

    .line 648
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static t(Lcom/twitter/model/core/Tweet;)I
    .locals 1

    .prologue
    .line 695
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/Tweet;)I

    move-result v0

    .line 696
    packed-switch v0, :pswitch_data_0

    .line 714
    :pswitch_0
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 698
    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 702
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 705
    :pswitch_3
    const/4 v0, 0x5

    goto :goto_0

    .line 708
    :pswitch_4
    const/4 v0, 0x2

    goto :goto_0

    .line 711
    :pswitch_5
    const/4 v0, 0x4

    goto :goto_0

    .line 696
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public static u(Lcom/twitter/model/core/Tweet;)Lcom/twitter/media/request/a$a;
    .locals 1

    .prologue
    .line 721
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->I()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 722
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->P()Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/util/c;->c(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    .line 723
    if-eqz v0, :cond_3

    .line 724
    invoke-static {v0}, Lcom/twitter/media/util/k;->a(Lcom/twitter/model/core/MediaEntity;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 734
    :goto_0
    return-object v0

    .line 726
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ag()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->c(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->aj()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ai()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    .line 728
    if-eqz v0, :cond_3

    .line 729
    invoke-static {v0}, Lcom/twitter/media/util/k;->a(Lcax;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    goto :goto_0

    .line 731
    :cond_2
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->w(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 732
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/media/util/k;->a(Lcax;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    goto :goto_0

    .line 734
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static v(Lcom/twitter/model/core/Tweet;)J
    .locals 2

    .prologue
    .line 744
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->P()Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/util/c;->d(Ljava/lang/Iterable;)Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    .line 745
    if-eqz v0, :cond_0

    .line 746
    iget-object v1, v0, Lcom/twitter/model/core/MediaEntity;->A:Lcom/twitter/model/core/m;

    if-eqz v1, :cond_0

    .line 747
    iget-object v0, v0, Lcom/twitter/model/core/MediaEntity;->A:Lcom/twitter/model/core/m;

    iget-wide v0, v0, Lcom/twitter/model/core/m;->b:J

    .line 750
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private static w(Lcom/twitter/model/core/Tweet;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 123
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v1

    .line 124
    if-nez v1, :cond_1

    .line 129
    :cond_0
    :goto_0
    return v0

    .line 127
    :cond_1
    const-string/jumbo v2, "appplayer"

    invoke-virtual {v1}, Lcax;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 128
    const-string/jumbo v3, "promo_video_convo"

    invoke-virtual {v1}, Lcax;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 129
    if-nez v2, :cond_2

    if-eqz v1, :cond_0

    :cond_2
    invoke-static {p0}, Lcom/twitter/library/av/playback/ab;->g(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
