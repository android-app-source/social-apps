.class public interface abstract Lcom/twitter/library/av/playback/AVMediaPlayer;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/playback/AVMediaPlayer$a;,
        Lcom/twitter/library/av/playback/AVMediaPlayer$PlayerState;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/library/av/playback/AVMediaPlayer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/twitter/library/av/playback/AVMediaPlayer$1;

    invoke-direct {v0}, Lcom/twitter/library/av/playback/AVMediaPlayer$1;-><init>()V

    sput-object v0, Lcom/twitter/library/av/playback/AVMediaPlayer;->a:Lcom/twitter/library/av/playback/AVMediaPlayer;

    return-void
.end method


# virtual methods
.method public abstract A()Z
.end method

.method public abstract B()Z
.end method

.method public abstract C()Z
.end method

.method public abstract D()V
.end method

.method public abstract E()Z
.end method

.method public abstract F()V
.end method

.method public abstract G()Z
.end method

.method public abstract H()Lcom/twitter/library/av/playback/aa;
.end method

.method public abstract I()V
.end method

.method public abstract a(F)V
.end method

.method public abstract a(Landroid/content/Context;Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Landroid/media/MediaPlayer$OnCompletionListener;)V
.end method

.method public abstract a(Landroid/view/Surface;)V
.end method

.method public abstract a(Lcom/twitter/library/av/k;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b()Lcom/twitter/library/av/playback/AVMediaPlayer$a;
.end method

.method public abstract b(J)V
.end method

.method public abstract b(Z)V
.end method

.method public abstract g()I
.end method

.method public abstract h()Ljava/lang/String;
.end method

.method public abstract v()Z
.end method

.method public abstract w()Z
.end method

.method public abstract x()V
.end method

.method public abstract y()Z
.end method

.method public abstract z()Z
.end method
