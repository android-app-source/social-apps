.class public Lcom/twitter/library/av/playback/livevideo/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/av/playback/ax;


# instance fields
.field private final a:Lcom/twitter/model/livevideo/b;

.field private final b:Lcom/twitter/model/core/Tweet;


# direct methods
.method constructor <init>(Lcom/twitter/model/livevideo/b;Lcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/twitter/library/av/playback/livevideo/a;->a:Lcom/twitter/model/livevideo/b;

    .line 27
    iput-object p2, p0, Lcom/twitter/library/av/playback/livevideo/a;->b:Lcom/twitter/model/core/Tweet;

    .line 28
    return-void
.end method


# virtual methods
.method public a()Lbis;
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/library/av/playback/livevideo/a;->a:Lcom/twitter/model/livevideo/b;

    iget-object v0, v0, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    if-nez v0, :cond_0

    sget-object v0, Lbis;->a:Lbis;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/library/av/playback/livevideo/b;

    iget-object v1, p0, Lcom/twitter/library/av/playback/livevideo/a;->a:Lcom/twitter/model/livevideo/b;

    invoke-direct {v0, v1}, Lcom/twitter/library/av/playback/livevideo/b;-><init>(Lcom/twitter/model/livevideo/b;)V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 4

    .prologue
    .line 34
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 35
    iget-object v1, p0, Lcom/twitter/library/av/playback/livevideo/a;->a:Lcom/twitter/model/livevideo/b;

    iget-wide v2, v1, Lcom/twitter/model/livevideo/b;->b:J

    iput-wide v2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 36
    const/16 v1, 0x1c

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 37
    return-object v0
.end method

.method public b()Lcgi;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/library/av/playback/livevideo/a;->b:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/playback/livevideo/a;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    return-object v0
.end method
