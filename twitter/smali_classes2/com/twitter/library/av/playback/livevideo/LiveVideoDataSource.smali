.class public Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/av/playback/AVDataSource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Lcom/twitter/model/livevideo/b;

.field private final e:Lcom/twitter/model/core/Tweet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource$1;

    invoke-direct {v0}, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource$1;-><init>()V

    sput-object v0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 54
    const-class v0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->c:Ljava/lang/String;

    .line 83
    sget-object v0, Lcom/twitter/model/livevideo/b;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/livevideo/b;

    iput-object v0, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->d:Lcom/twitter/model/livevideo/b;

    .line 84
    const-class v0, Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    iput-object v0, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->e:Lcom/twitter/model/core/Tweet;

    .line 85
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/livevideo/b;)V
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;-><init>(Lcom/twitter/model/livevideo/b;Lcom/twitter/model/core/Tweet;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/livevideo/b;Lcom/twitter/model/core/Tweet;)V
    .locals 2

    .prologue
    .line 67
    iget-wide v0, p1, Lcom/twitter/model/livevideo/b;->b:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;-><init>(Ljava/lang/String;Lcom/twitter/model/livevideo/b;Lcom/twitter/model/core/Tweet;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/twitter/model/livevideo/b;Lcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p2, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->d:Lcom/twitter/model/livevideo/b;

    .line 76
    iput-object p1, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->c:Ljava/lang/String;

    .line 77
    iput-object p3, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->e:Lcom/twitter/model/core/Tweet;

    .line 78
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->c:Ljava/lang/String;

    return-object v0
.end method

.method public b()Lcom/twitter/model/card/property/ImageSpec;
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->d:Lcom/twitter/model/livevideo/b;

    iget-object v0, v0, Lcom/twitter/model/livevideo/b;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    const/4 v0, 0x0

    .line 198
    :goto_0
    return-object v0

    .line 187
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->d:Lcom/twitter/model/livevideo/b;

    iget-object v1, v1, Lcom/twitter/model/livevideo/b;->k:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 188
    new-instance v1, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource$2;

    invoke-direct {v1, p0}, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource$2;-><init>(Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 198
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/card/property/ImageSpec;

    goto :goto_0
.end method

.method public c()Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->e:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x7

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 218
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 230
    if-ne p0, p1, :cond_1

    .line 231
    const/4 v0, 0x1

    .line 248
    :cond_0
    :goto_0
    return v0

    .line 234
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 238
    check-cast p1, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;

    .line 240
    iget-object v1, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 244
    iget-object v1, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->d:Lcom/twitter/model/livevideo/b;

    iget-object v2, p1, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->d:Lcom/twitter/model/livevideo/b;

    invoke-virtual {v1, v2}, Lcom/twitter/model/livevideo/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 248
    iget-object v0, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->e:Lcom/twitter/model/core/Tweet;

    iget-object v1, p1, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->e:Lcom/twitter/model/core/Tweet;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->d:Lcom/twitter/model/livevideo/b;

    iget-object v0, v0, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->d:Lcom/twitter/model/livevideo/b;

    iget-object v0, v0, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    iget-object v0, v0, Lcom/twitter/model/livevideo/a;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public h()Lbyp;
    .locals 7

    .prologue
    .line 132
    iget-object v0, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->d:Lcom/twitter/model/livevideo/b;

    iget-object v0, v0, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    if-nez v0, :cond_0

    .line 133
    const/4 v1, 0x0

    .line 135
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lbkv;

    iget-object v0, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->d:Lcom/twitter/model/livevideo/b;

    iget-object v3, v0, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    iget-object v0, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->d:Lcom/twitter/model/livevideo/b;

    iget-wide v4, v0, Lcom/twitter/model/livevideo/b;->c:J

    new-instance v6, Lcom/twitter/library/av/playback/livevideo/c;

    invoke-direct {v6}, Lcom/twitter/library/av/playback/livevideo/c;-><init>()V

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lbkv;-><init>(Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/model/livevideo/a;JLbkv$a;)V

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 253
    iget-object v0, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->d:Lcom/twitter/model/livevideo/b;

    iget-object v2, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->e:Lcom/twitter/model/core/Tweet;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public i()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public j()Lcom/twitter/model/av/Partner;
    .locals 1

    .prologue
    .line 149
    sget-object v0, Lcom/twitter/model/av/Partner;->a:Lcom/twitter/model/av/Partner;

    return-object v0
.end method

.method public k()Lcom/twitter/library/av/playback/ax;
    .locals 3

    .prologue
    .line 161
    new-instance v0, Lcom/twitter/library/av/playback/livevideo/a;

    iget-object v1, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->d:Lcom/twitter/model/livevideo/b;

    iget-object v2, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->e:Lcom/twitter/model/core/Tweet;

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/av/playback/livevideo/a;-><init>(Lcom/twitter/model/livevideo/b;Lcom/twitter/model/core/Tweet;)V

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    return-object v0
.end method

.method public m()Lcom/twitter/library/av/playback/ah;
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->e:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/twitter/library/av/v;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 174
    :cond_0
    sget-object v0, Lcom/twitter/library/av/playback/ah;->a:Lcom/twitter/library/av/playback/ah;

    .line 177
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource$a;

    iget-object v1, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->e:Lcom/twitter/model/core/Tweet;

    invoke-direct {v0, v1}, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource$a;-><init>(Lcom/twitter/model/core/Tweet;)V

    goto :goto_0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x0

    return v0
.end method

.method public o()F
    .locals 1

    .prologue
    .line 208
    const v0, 0x3fe38e39

    return v0
.end method

.method public p()J
    .locals 2

    .prologue
    .line 213
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 224
    iget-object v0, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->d:Lcom/twitter/model/livevideo/b;

    sget-object v1, Lcom/twitter/model/livevideo/b;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 225
    iget-object v0, p0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;->e:Lcom/twitter/model/core/Tweet;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 226
    return-void
.end method
