.class public Lcom/twitter/library/av/playback/ae;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Lcax;)I
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcax;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    const/4 v0, 0x5

    .line 36
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(Lcom/twitter/library/av/playback/AVDataSource;Lcax;)Lbyp;
    .locals 1

    .prologue
    .line 99
    invoke-static {p1}, Lcom/twitter/library/av/playback/ae;->a(Lcax;)I

    move-result v0

    .line 100
    packed-switch v0, :pswitch_data_0

    .line 105
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 102
    :pswitch_0
    new-instance v0, Lbkp;

    invoke-direct {v0, p0}, Lbkp;-><init>(Lcom/twitter/library/av/playback/AVDataSource;)V

    goto :goto_0

    .line 100
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Lcax;)Lcom/twitter/model/av/Partner;
    .locals 2

    .prologue
    .line 47
    invoke-static {p0}, Lcom/twitter/library/av/playback/ae;->a(Lcax;)I

    move-result v0

    .line 48
    packed-switch v0, :pswitch_data_0

    .line 54
    sget-object v0, Lcom/twitter/model/av/Partner;->a:Lcom/twitter/model/av/Partner;

    :goto_0
    return-object v0

    .line 50
    :pswitch_0
    const-string/jumbo v0, "partner"

    invoke-virtual {p0, v0}, Lcax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 51
    if-eqz v1, :cond_0

    new-instance v0, Lcom/twitter/model/av/Partner;

    invoke-direct {v0, v1}, Lcom/twitter/model/av/Partner;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/twitter/model/av/Partner;->a:Lcom/twitter/model/av/Partner;

    goto :goto_0

    .line 48
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method public static c(Lcax;)F
    .locals 1

    .prologue
    .line 69
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public static d(Lcax;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    invoke-static {p0}, Lcom/twitter/library/av/playback/ae;->a(Lcax;)I

    move-result v0

    .line 80
    packed-switch v0, :pswitch_data_0

    .line 85
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 82
    :pswitch_0
    const-string/jumbo v0, "source"

    invoke-virtual {p0, v0}, Lcax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method public static e(Lcax;)Lcom/twitter/model/card/property/ImageSpec;
    .locals 1

    .prologue
    .line 120
    invoke-virtual {p0}, Lcax;->q()Lcom/twitter/model/card/property/ImageSpec;

    move-result-object v0

    return-object v0
.end method

.method public static f(Lcax;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcax;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 131
    invoke-static {p0}, Lcom/twitter/library/av/playback/ae;->a(Lcax;)I

    move-result v0

    .line 132
    packed-switch v0, :pswitch_data_0

    .line 162
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    .line 134
    :pswitch_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 136
    invoke-static {p0}, Lcom/twitter/library/av/playback/ae;->d(Lcax;)Ljava/lang/String;

    move-result-object v3

    .line 137
    invoke-static {p0}, Lcom/twitter/library/av/playback/ae;->e(Lcax;)Lcom/twitter/model/card/property/ImageSpec;

    move-result-object v4

    .line 138
    invoke-static {p0}, Lcom/twitter/library/av/playback/ae;->b(Lcax;)Lcom/twitter/model/av/Partner;

    move-result-object v5

    .line 139
    if-eqz p0, :cond_6

    const-string/jumbo v2, "title"

    .line 140
    invoke-virtual {p0, v2}, Lcax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 141
    :goto_1
    if-eqz p0, :cond_1

    const-string/jumbo v1, "artist_name"

    .line 142
    invoke-virtual {p0, v1}, Lcax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 144
    :cond_1
    if-eqz v3, :cond_2

    .line 145
    const-string/jumbo v6, "playlist_url"

    invoke-interface {v0, v6, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    :cond_2
    if-eqz v4, :cond_3

    iget-object v3, v4, Lcom/twitter/model/card/property/ImageSpec;->b:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 148
    const-string/jumbo v3, "image_url"

    iget-object v4, v4, Lcom/twitter/model/card/property/ImageSpec;->b:Ljava/lang/String;

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    :cond_3
    if-eqz v2, :cond_4

    .line 151
    const-string/jumbo v3, "card_title"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    :cond_4
    if-eqz v1, :cond_5

    .line 154
    const-string/jumbo v2, "artist_name"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    :cond_5
    sget-object v1, Lcom/twitter/model/av/Partner;->a:Lcom/twitter/model/av/Partner;

    if-eq v5, v1, :cond_0

    .line 157
    const-string/jumbo v1, "integration_partner"

    invoke-virtual {v5}, Lcom/twitter/model/av/Partner;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_6
    move-object v2, v1

    .line 140
    goto :goto_1

    .line 132
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method public static g(Lcax;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    const-string/jumbo v0, "app_id"

    invoke-virtual {p0, v0}, Lcax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static h(Lcax;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public static i(Lcax;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public static j(Lcax;)J
    .locals 2

    .prologue
    .line 211
    const-wide/16 v0, -0x1

    return-wide v0
.end method
