.class public Lcom/twitter/library/av/playback/ak;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Landroid/content/SharedPreferences;)I
    .locals 2

    .prologue
    .line 27
    const-string/jumbo v0, "video_quality"

    invoke-static {}, Lcom/twitter/library/av/playback/ak;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 28
    invoke-static {v0}, Lcom/twitter/library/av/playback/ak;->b(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    invoke-static {}, Lcom/twitter/library/av/playback/ak;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    invoke-static {p0}, Lcom/twitter/library/av/playback/ak;->b(Ljava/lang/String;)I

    move-result v0

    .line 33
    packed-switch v0, :pswitch_data_0

    .line 41
    const-string/jumbo v0, "settings::video_quality::cellular_wifi"

    :goto_0
    return-object v0

    .line 35
    :pswitch_0
    const-string/jumbo v0, "settings::video_quality::off"

    goto :goto_0

    .line 38
    :pswitch_1
    const-string/jumbo v0, "settings::video_quality::wifi"

    goto :goto_0

    .line 33
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Z)Z
    .locals 2

    .prologue
    .line 61
    if-eqz p0, :cond_0

    .line 62
    const-string/jumbo v0, "video_hls_bitrate_limit_android_5264"

    invoke-static {v0}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v0

    .line 65
    :goto_0
    return v0

    .line 64
    :cond_0
    invoke-static {}, Lcom/twitter/library/av/playback/ak;->b()Ljava/lang/String;

    move-result-object v0

    .line 65
    const-string/jumbo v1, "unassigned"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "control"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 47
    const/4 v1, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 55
    :goto_1
    return v0

    .line 47
    :sswitch_0
    const-string/jumbo v2, "never"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v2, "wifi_only"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 49
    :pswitch_0
    const/4 v0, 0x3

    goto :goto_1

    .line 52
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_1

    .line 47
    nop

    :sswitch_data_0
    .sparse-switch
        0x63dca8c -> :sswitch_0
        0x53824e96 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    const-string/jumbo v0, "video_hls_bitrate_limit_android_5264"

    invoke-static {v0}, Lcoi;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
