.class Lcom/twitter/library/av/playback/TweetAVDataSource$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/av/playback/ah;


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/av/playback/TweetAVDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation


# instance fields
.field private final b:Lcom/twitter/model/core/Tweet;


# direct methods
.method constructor <init>(Lcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 223
    iput-object p1, p0, Lcom/twitter/library/av/playback/TweetAVDataSource$b;->b:Lcom/twitter/model/core/Tweet;

    .line 224
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource$b;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ah()Z

    move-result v0

    return v0
.end method

.method public a(Lcom/twitter/model/av/DynamicAdId;)Z
    .locals 6

    .prologue
    .line 233
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource$b;->b:Lcom/twitter/model/core/Tweet;

    .line 234
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource$b;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    iget-object v0, v0, Lcgi;->c:Ljava/lang/String;

    .line 235
    :goto_0
    iget-wide v2, p1, Lcom/twitter/model/av/DynamicAdId;->b:J

    iget-object v1, p0, Lcom/twitter/library/av/playback/TweetAVDataSource$b;->b:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v1, Lcom/twitter/model/core/Tweet;->t:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    iget-object v1, p1, Lcom/twitter/model/av/DynamicAdId;->c:Ljava/lang/String;

    .line 236
    invoke-static {v1, v0}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 235
    :goto_1
    return v0

    .line 234
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 236
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public b()Lcom/twitter/model/av/DynamicAdId;
    .locals 3

    .prologue
    .line 242
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource$b;->b:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->t:J

    iget-object v2, p0, Lcom/twitter/library/av/playback/TweetAVDataSource$b;->b:Lcom/twitter/model/core/Tweet;

    .line 243
    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v2

    .line 242
    invoke-static {v0, v1, v2}, Lcom/twitter/model/av/DynamicAdId;->a(JLcgi;)Lcom/twitter/model/av/DynamicAdId;

    move-result-object v0

    return-object v0
.end method
