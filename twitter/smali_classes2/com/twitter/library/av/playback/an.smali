.class public Lcom/twitter/library/av/playback/an;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/av/playback/aj;

.field private final b:Z


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/aj;Z)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/twitter/library/av/playback/an;->a:Lcom/twitter/library/av/playback/aj;

    .line 16
    iput-boolean p2, p0, Lcom/twitter/library/av/playback/an;->b:Z

    .line 17
    return-void
.end method


# virtual methods
.method public a(ZZ)J
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/twitter/library/av/playback/an;->b:Z

    if-nez v0, :cond_0

    .line 22
    const-wide v0, 0x7fffffffffffffffL

    .line 35
    :goto_0
    return-wide v0

    .line 25
    :cond_0
    if-eqz p1, :cond_2

    .line 26
    if-eqz p2, :cond_1

    .line 27
    iget-object v0, p0, Lcom/twitter/library/av/playback/an;->a:Lcom/twitter/library/av/playback/aj;

    iget-wide v0, v0, Lcom/twitter/library/av/playback/aj;->c:J

    goto :goto_0

    .line 29
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/av/playback/an;->a:Lcom/twitter/library/av/playback/aj;

    iget-wide v0, v0, Lcom/twitter/library/av/playback/aj;->a:J

    goto :goto_0

    .line 32
    :cond_2
    if-eqz p2, :cond_3

    .line 33
    iget-object v0, p0, Lcom/twitter/library/av/playback/an;->a:Lcom/twitter/library/av/playback/aj;

    iget-wide v0, v0, Lcom/twitter/library/av/playback/aj;->d:J

    goto :goto_0

    .line 35
    :cond_3
    iget-object v0, p0, Lcom/twitter/library/av/playback/an;->a:Lcom/twitter/library/av/playback/aj;

    iget-wide v0, v0, Lcom/twitter/library/av/playback/aj;->b:J

    goto :goto_0
.end method
