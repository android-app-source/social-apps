.class public Lcom/twitter/library/av/playback/ai;
.super Lcom/twitter/library/av/playback/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/playback/ai$a;,
        Lcom/twitter/library/av/playback/ai$b;
    }
.end annotation


# instance fields
.field private final o:Lczi;

.field private p:Lczn;

.field private q:Lczi$e;

.field private r:Lczi$b;


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/l;Lczi;)V
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p2}, Lczi;->b()Lcom/google/android/exoplayer/ExoPlayer;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/av/playback/b;-><init>(Lcom/twitter/library/av/playback/l;Lcom/google/android/exoplayer/ExoPlayer;)V

    .line 44
    iput-object p2, p0, Lcom/twitter/library/av/playback/ai;->o:Lczi;

    .line 45
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/av/playback/ai;)Lczi$e;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai;->q:Lczi$e;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/library/av/playback/ai;)Lczn;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai;->p:Lczn;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/library/av/playback/ai;)Lczi$b;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai;->r:Lczi$b;

    return-object v0
.end method


# virtual methods
.method public N()Lczi;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai;->o:Lczi;

    return-object v0
.end method

.method public O()V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai;->p:Lczn;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai;->p:Lczn;

    invoke-virtual {v0}, Lczn;->a()V

    .line 115
    :cond_0
    return-void
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai;->o:Lczi;

    invoke-virtual {v0, p1}, Lczi;->a(F)V

    .line 120
    return-void
.end method

.method a(Landroid/content/Context;Lcom/twitter/model/av/AVMedia;)V
    .locals 4

    .prologue
    .line 62
    new-instance v0, Lcom/twitter/library/av/playback/ai$b;

    invoke-direct {v0, p0}, Lcom/twitter/library/av/playback/ai$b;-><init>(Lcom/twitter/library/av/playback/ai;)V

    .line 63
    iget-object v1, p0, Lcom/twitter/library/av/playback/ai;->o:Lczi;

    invoke-virtual {v1, v0}, Lczi;->a(Lczi$e;)V

    .line 64
    iget-object v1, p0, Lcom/twitter/library/av/playback/ai;->o:Lczi;

    invoke-virtual {v1, v0}, Lczi;->a(Lczi$c;)V

    .line 65
    iget-object v1, p0, Lcom/twitter/library/av/playback/ai;->o:Lczi;

    invoke-virtual {v1, v0}, Lczi;->a(Lczi$d;)V

    .line 66
    iget-object v1, p0, Lcom/twitter/library/av/playback/ai;->o:Lczi;

    invoke-virtual {v1, v0}, Lczi;->a(Lczi$b;)V

    .line 68
    new-instance v0, Lcom/twitter/library/av/playback/ai$1;

    invoke-direct {v0, p0}, Lcom/twitter/library/av/playback/ai$1;-><init>(Lcom/twitter/library/av/playback/ai;)V

    .line 82
    new-instance v1, Lczn;

    invoke-direct {v1, v0}, Lczn;-><init>(Lczn$a;)V

    iput-object v1, p0, Lcom/twitter/library/av/playback/ai;->p:Lczn;

    .line 84
    instance-of v0, p2, Lcom/twitter/library/av/model/PeriscopeMedia;

    if-eqz v0, :cond_0

    invoke-interface {p2}, Lcom/twitter/model/av/AVMedia;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    check-cast p2, Lcom/twitter/library/av/model/PeriscopeMedia;

    .line 86
    invoke-virtual {p2}, Lcom/twitter/library/av/model/PeriscopeMedia;->a()J

    move-result-wide v0

    .line 87
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 88
    iget-object v2, p0, Lcom/twitter/library/av/playback/ai;->o:Lczi;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Lczi;->a(J)V

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai;->o:Lczi;

    invoke-virtual {v0}, Lczi;->c()V

    .line 92
    return-void
.end method

.method public a(Landroid/view/Surface;)V
    .locals 1

    .prologue
    .line 101
    invoke-super {p0, p1}, Lcom/twitter/library/av/playback/b;->a(Landroid/view/Surface;)V

    .line 102
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai;->o:Lczi;

    invoke-virtual {v0, p1}, Lczi;->a(Landroid/view/Surface;)V

    .line 103
    return-void
.end method

.method public a(Lczi$b;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/twitter/library/av/playback/ai;->r:Lczi$b;

    .line 58
    return-void
.end method

.method public a(Lczi$e;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/twitter/library/av/playback/ai;->q:Lczi$e;

    .line 54
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 107
    invoke-super {p0, p1}, Lcom/twitter/library/av/playback/b;->a(Z)V

    .line 108
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai;->o:Lczi;

    invoke-virtual {v0}, Lczi;->e()V

    .line 109
    return-void
.end method

.method public onPlayerSeekComplete()V
    .locals 0

    .prologue
    .line 97
    return-void
.end method
