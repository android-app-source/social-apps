.class public Lcom/twitter/library/av/playback/v;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            "Lbrc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lbrd;

    invoke-direct {v0}, Lbrd;-><init>()V

    invoke-direct {p0, v0}, Lcom/twitter/library/av/playback/v;-><init>(Lcom/twitter/util/object/d;)V

    .line 20
    return-void
.end method

.method constructor <init>(Lcom/twitter/util/object/d;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            "Lbrc;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/twitter/library/av/playback/v;->a:Lcom/twitter/util/object/d;

    .line 26
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/library/av/playback/u;
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/twitter/library/av/playback/u;

    invoke-direct {v0, p1}, Lcom/twitter/library/av/playback/u;-><init>(Lcom/twitter/library/av/playback/AVDataSource;)V

    return-object v0
.end method

.method public a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/av/playback/u;
    .locals 2

    .prologue
    .line 30
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ak()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 31
    iget-object v0, p0, Lcom/twitter/library/av/playback/v;->a:Lcom/twitter/util/object/d;

    invoke-interface {v0, p1}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrc;

    invoke-virtual {v0}, Lbrc;->p()Lcom/twitter/model/livevideo/b;

    move-result-object v1

    .line 33
    if-nez v1, :cond_0

    .line 34
    sget-object v0, Lcom/twitter/library/av/playback/AVDataSource;->a:Lcom/twitter/library/av/playback/AVDataSource;

    .line 38
    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/v;->a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/library/av/playback/u;

    move-result-object v0

    .line 40
    :goto_1
    return-object v0

    .line 36
    :cond_0
    new-instance v0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;

    invoke-direct {v0, v1}, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;-><init>(Lcom/twitter/model/livevideo/b;)V

    goto :goto_0

    .line 40
    :cond_1
    new-instance v0, Lcom/twitter/library/av/playback/TweetAVDataSource;

    invoke-direct {v0, p1}, Lcom/twitter/library/av/playback/TweetAVDataSource;-><init>(Lcom/twitter/model/core/Tweet;)V

    invoke-virtual {p0, v0}, Lcom/twitter/library/av/playback/v;->a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/library/av/playback/u;

    move-result-object v0

    goto :goto_1
.end method
