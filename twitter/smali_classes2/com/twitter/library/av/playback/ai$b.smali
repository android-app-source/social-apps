.class Lcom/twitter/library/av/playback/ai$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lczi$b;
.implements Lczi$c;
.implements Lczi$d;
.implements Lczi$e;


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/av/playback/ai;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/av/playback/ai;


# direct methods
.method constructor <init>(Lcom/twitter/library/av/playback/ai;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/twitter/library/av/playback/ai$b;->a:Lcom/twitter/library/av/playback/ai;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IIIF)V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai$b;->a:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/twitter/library/av/playback/ai;->onVideoSizeChanged(IIIF)V

    .line 191
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai$b;->a:Lcom/twitter/library/av/playback/ai;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ai;->a(Lcom/twitter/library/av/playback/ai;)Lczi$e;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai$b;->a:Lcom/twitter/library/av/playback/ai;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ai;->a(Lcom/twitter/library/av/playback/ai;)Lczi$e;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lczi$e;->a(IIIF)V

    .line 194
    :cond_0
    return-void
.end method

.method public a(IJ)V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai$b;->a:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/library/av/playback/ai;->onDroppedFrames(IJ)V

    .line 138
    return-void
.end method

.method public a(IJIILcom/google/android/exoplayer/chunk/Format;JJ)V
    .locals 0

    .prologue
    .line 150
    return-void
.end method

.method public a(IJIILcom/google/android/exoplayer/chunk/Format;JJJJ)V
    .locals 0

    .prologue
    .line 156
    return-void
.end method

.method public a(IJJ)V
    .locals 0

    .prologue
    .line 143
    return-void
.end method

.method public a(ILcom/google/android/exoplayer/TimeRange;)V
    .locals 0

    .prologue
    .line 166
    return-void
.end method

.method public a(ILjava/io/IOException;)V
    .locals 0

    .prologue
    .line 231
    return-void
.end method

.method public a(Landroid/media/MediaCodec$CryptoException;)V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai$b;->a:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/playback/ai;->onCryptoError(Landroid/media/MediaCodec$CryptoException;)V

    .line 226
    return-void
.end method

.method public a(Landroid/view/Surface;)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai$b;->a:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/playback/ai;->onDrawnToSurface(Landroid/view/Surface;)V

    .line 171
    return-void
.end method

.method public a(Lcom/google/android/exoplayer/MediaCodecTrackRenderer$DecoderInitializationException;)V
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai$b;->a:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/playback/ai;->onDecoderInitializationError(Lcom/google/android/exoplayer/MediaCodecTrackRenderer$DecoderInitializationException;)V

    .line 221
    return-void
.end method

.method public a(Lcom/google/android/exoplayer/audio/AudioTrack$InitializationException;)V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai$b;->a:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/playback/ai;->onAudioTrackInitializationError(Lcom/google/android/exoplayer/audio/AudioTrack$InitializationException;)V

    .line 211
    return-void
.end method

.method public a(Lcom/google/android/exoplayer/audio/AudioTrack$WriteException;)V
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai$b;->a:Lcom/twitter/library/av/playback/ai;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/playback/ai;->onAudioTrackWriteError(Lcom/google/android/exoplayer/audio/AudioTrack$WriteException;)V

    .line 216
    return-void
.end method

.method public a(Lcom/google/android/exoplayer/chunk/Format;IJ)V
    .locals 0

    .prologue
    .line 128
    return-void
.end method

.method public a(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai$b;->a:Lcom/twitter/library/av/playback/ai;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ai;->a(Lcom/twitter/library/av/playback/ai;)Lczi$e;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai$b;->a:Lcom/twitter/library/av/playback/ai;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ai;->a(Lcom/twitter/library/av/playback/ai;)Lczi$e;

    move-result-object v0

    invoke-interface {v0, p1}, Lczi$e;->a(Ljava/lang/Exception;)V

    .line 185
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;JJ)V
    .locals 6

    .prologue
    .line 160
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai$b;->a:Lcom/twitter/library/av/playback/ai;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/av/playback/ai;->onDecoderInitialized(Ljava/lang/String;JJ)V

    .line 161
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer/metadata/id3/Id3Frame;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 240
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai$b;->a:Lcom/twitter/library/av/playback/ai;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ai;->b(Lcom/twitter/library/av/playback/ai;)Lczn;

    move-result-object v0

    invoke-virtual {v0, p1}, Lczn;->a(Ljava/util/List;)V

    .line 241
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai$b;->a:Lcom/twitter/library/av/playback/ai;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ai;->c(Lcom/twitter/library/av/playback/ai;)Lczi$b;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai$b;->a:Lcom/twitter/library/av/playback/ai;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ai;->c(Lcom/twitter/library/av/playback/ai;)Lczi$b;

    move-result-object v0

    invoke-interface {v0, p1}, Lczi$b;->a(Ljava/util/List;)V

    .line 244
    :cond_0
    return-void
.end method

.method public a(ZI)V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai$b;->a:Lcom/twitter/library/av/playback/ai;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ai;->a(Lcom/twitter/library/av/playback/ai;)Lczi$e;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai$b;->a:Lcom/twitter/library/av/playback/ai;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ai;->a(Lcom/twitter/library/av/playback/ai;)Lczi$e;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lczi$e;->a(ZI)V

    .line 178
    :cond_0
    return-void
.end method

.method public b(IJJ)V
    .locals 0

    .prologue
    .line 249
    return-void
.end method

.method public b(Lcom/google/android/exoplayer/chunk/Format;IJ)V
    .locals 0

    .prologue
    .line 133
    return-void
.end method

.method public b(Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 206
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai$b;->a:Lcom/twitter/library/av/playback/ai;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ai;->a(Lcom/twitter/library/av/playback/ai;)Lczi$e;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/twitter/library/av/playback/ai$b;->a:Lcom/twitter/library/av/playback/ai;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ai;->a(Lcom/twitter/library/av/playback/ai;)Lczi$e;

    move-result-object v0

    invoke-interface {v0}, Lczi$e;->c()V

    .line 201
    :cond_0
    return-void
.end method

.method public c(Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 236
    return-void
.end method
