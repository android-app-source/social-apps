.class public Lcom/twitter/library/av/playback/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/google/android/exoplayer/upstream/BandwidthMeter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/playback/a$a;
    }
.end annotation


# static fields
.field private static a:Lcom/twitter/library/av/playback/a;


# instance fields
.field private b:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

.field private final c:Lcom/twitter/library/av/playback/a$a;

.field private d:J

.field private e:J


# direct methods
.method constructor <init>(Lcom/twitter/library/av/playback/a$a;J)V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/twitter/library/av/playback/a;->c:Lcom/twitter/library/av/playback/a$a;

    .line 57
    invoke-virtual {p1}, Lcom/twitter/library/av/playback/a$a;->a()Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/playback/a;->b:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    .line 58
    iput-wide p2, p0, Lcom/twitter/library/av/playback/a;->e:J

    .line 59
    return-void
.end method

.method public static declared-synchronized a()Lcom/twitter/library/av/playback/a;
    .locals 6

    .prologue
    .line 40
    const-class v1, Lcom/twitter/library/av/playback/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/av/playback/a;->a:Lcom/twitter/library/av/playback/a;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/twitter/library/av/playback/a;

    new-instance v2, Lcom/twitter/library/av/playback/a$a;

    new-instance v3, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v2, v3}, Lcom/twitter/library/av/playback/a$a;-><init>(Landroid/os/Handler;)V

    const-wide/32 v4, 0x493e0

    invoke-direct {v0, v2, v4, v5}, Lcom/twitter/library/av/playback/a;-><init>(Lcom/twitter/library/av/playback/a$a;J)V

    sput-object v0, Lcom/twitter/library/av/playback/a;->a:Lcom/twitter/library/av/playback/a;

    .line 43
    const-class v0, Lcom/twitter/library/av/playback/a;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 46
    :cond_0
    sget-object v0, Lcom/twitter/library/av/playback/a;->a:Lcom/twitter/library/av/playback/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private b()Z
    .locals 4

    .prologue
    .line 94
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/library/av/playback/a;->d:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/twitter/library/av/playback/a;->e:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized getBitrateEstimate()J
    .locals 4

    .prologue
    .line 63
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/twitter/library/av/playback/a;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/av/playback/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/twitter/library/av/playback/a;->b:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/BandwidthMeter;->getBitrateEstimate()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 67
    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onBytesTransferred(I)V
    .locals 1

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/av/playback/a;->b:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer/upstream/BandwidthMeter;->onBytesTransferred(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    monitor-exit p0

    return-void

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onTransferEnd()V
    .locals 1

    .prologue
    .line 87
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/av/playback/a;->b:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/BandwidthMeter;->onTransferEnd()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    monitor-exit p0

    return-void

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onTransferStart()V
    .locals 2

    .prologue
    .line 72
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/twitter/library/av/playback/a;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/twitter/library/av/playback/a;->c:Lcom/twitter/library/av/playback/a$a;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/a$a;->a()Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/playback/a;->b:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    .line 76
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/av/playback/a;->d:J

    .line 77
    iget-object v0, p0, Lcom/twitter/library/av/playback/a;->b:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/BandwidthMeter;->onTransferStart()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    monitor-exit p0

    return-void

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
