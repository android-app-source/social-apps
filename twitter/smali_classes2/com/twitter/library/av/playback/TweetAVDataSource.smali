.class public Lcom/twitter/library/av/playback/TweetAVDataSource;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/av/playback/AVDataSource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/playback/TweetAVDataSource$b;,
        Lcom/twitter/library/av/playback/TweetAVDataSource$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/library/av/playback/TweetAVDataSource;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected final b:Lcom/twitter/model/core/Tweet;

.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/twitter/library/av/playback/TweetAVDataSource$1;

    invoke-direct {v0}, Lcom/twitter/library/av/playback/TweetAVDataSource$1;-><init>()V

    sput-object v0, Lcom/twitter/library/av/playback/TweetAVDataSource;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const-class v0, Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    iput-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    .line 58
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->c:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/core/Tweet;)V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/av/playback/TweetAVDataSource;-><init>(Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    .line 53
    iput-object p2, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->c:Ljava/lang/String;

    .line 54
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->t:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/twitter/model/card/property/ImageSpec;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ab;->m(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/card/property/ImageSpec;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/Tweet;)I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lcom/twitter/media/util/q;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 163
    if-ne p0, p1, :cond_1

    .line 168
    :cond_0
    :goto_0
    return v0

    .line 164
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 166
    :cond_3
    check-cast p1, Lcom/twitter/library/av/playback/TweetAVDataSource;

    .line 168
    iget-object v2, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    iget-object v3, p1, Lcom/twitter/library/av/playback/TweetAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/av/playback/TweetAVDataSource;->c:Ljava/lang/String;

    .line 169
    invoke-static {v2, v3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ab;->f(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public h()Lbyp;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    invoke-static {v0, p0}, Lcom/twitter/library/av/playback/ab;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/av/playback/AVDataSource;)Lbyp;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public i()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ab;->j(Lcom/twitter/model/core/Tweet;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public j()Lcom/twitter/model/av/Partner;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ab;->l(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/av/Partner;

    move-result-object v0

    return-object v0
.end method

.method public k()Lcom/twitter/library/av/playback/ax;
    .locals 2

    .prologue
    .line 142
    new-instance v0, Lcom/twitter/library/av/playback/TweetAVDataSource$a;

    iget-object v1, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    invoke-direct {v0, v1}, Lcom/twitter/library/av/playback/TweetAVDataSource$a;-><init>(Lcom/twitter/model/core/Tweet;)V

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ab;->o(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Lcom/twitter/library/av/playback/ah;
    .locals 2

    .prologue
    .line 153
    new-instance v0, Lcom/twitter/library/av/playback/TweetAVDataSource$b;

    iget-object v1, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    invoke-direct {v0, v1}, Lcom/twitter/library/av/playback/TweetAVDataSource$b;-><init>(Lcom/twitter/model/core/Tweet;)V

    return-object v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method public o()F
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ab;->n(Lcom/twitter/model/core/Tweet;)F

    move-result v0

    return v0
.end method

.method public p()J
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lcom/twitter/library/av/playback/ab;->v(Lcom/twitter/model/core/Tweet;)J

    move-result-wide v0

    return-wide v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 180
    iget-object v0, p0, Lcom/twitter/library/av/playback/TweetAVDataSource;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 181
    return-void
.end method
