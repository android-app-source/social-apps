.class public Lcom/twitter/library/av/playback/k;
.super Lcom/twitter/library/av/playback/b;
.source "Twttr"


# direct methods
.method public constructor <init>(Lcom/twitter/library/av/playback/l;)V
    .locals 3

    .prologue
    .line 36
    const/4 v0, 0x2

    .line 38
    invoke-static {}, Lcom/twitter/library/av/playback/k;->a()I

    move-result v1

    const/16 v2, 0x7d0

    .line 36
    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer/ExoPlayer$Factory;->newInstance(III)Lcom/google/android/exoplayer/ExoPlayer;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/av/playback/k;-><init>(Lcom/twitter/library/av/playback/l;Lcom/google/android/exoplayer/ExoPlayer;)V

    .line 40
    return-void
.end method

.method constructor <init>(Lcom/twitter/library/av/playback/l;Lcom/google/android/exoplayer/ExoPlayer;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/av/playback/b;-><init>(Lcom/twitter/library/av/playback/l;Lcom/google/android/exoplayer/ExoPlayer;)V

    .line 33
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/model/av/AVMedia;)Lbrh;
    .locals 2

    .prologue
    .line 45
    invoke-interface {p1}, Lcom/twitter/model/av/AVMedia;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    new-instance v0, Lbri;

    iget-object v1, p0, Lcom/twitter/library/av/playback/k;->c:Lbix;

    invoke-direct {v0, v1}, Lbri;-><init>(Lbix;)V

    .line 48
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/library/av/playback/b;->a(Lcom/twitter/model/av/AVMedia;)Lbrh;

    move-result-object v0

    goto :goto_0
.end method

.method a(Landroid/content/Context;Lcom/twitter/model/av/AVMedia;)V
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 54
    iget-object v0, p0, Lcom/twitter/library/av/playback/k;->e:Lcom/twitter/model/av/AVMedia;

    invoke-interface {v0}, Lcom/twitter/model/av/AVMedia;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 55
    new-instance v3, Lcom/twitter/library/av/playback/e;

    const/16 v0, 0x2000

    const/16 v2, 0xc9

    invoke-direct {v3, v0, v2}, Lcom/twitter/library/av/playback/e;-><init>(II)V

    .line 56
    invoke-virtual {v3, p0}, Lcom/twitter/library/av/playback/e;->a(Lcom/twitter/library/av/playback/e$a;)V

    .line 58
    new-instance v0, Lbrj;

    new-array v2, v9, [Lcom/google/android/exoplayer/upstream/TransferListener;

    invoke-virtual {p0}, Lcom/twitter/library/av/playback/k;->s()Lbrh;

    move-result-object v4

    aput-object v4, v2, v10

    invoke-direct {v0, v2}, Lbrj;-><init>([Lcom/google/android/exoplayer/upstream/TransferListener;)V

    .line 59
    new-instance v2, Lcom/google/android/exoplayer/upstream/DefaultUriDataSource;

    iget-object v4, p0, Lcom/twitter/library/av/playback/k;->f:Ljava/lang/String;

    invoke-direct {v2, p1, v0, v4}, Lcom/google/android/exoplayer/upstream/DefaultUriDataSource;-><init>(Landroid/content/Context;Lcom/google/android/exoplayer/upstream/TransferListener;Ljava/lang/String;)V

    .line 60
    new-instance v6, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;

    invoke-direct {v6}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;-><init>()V

    .line 61
    new-instance v0, Lcom/google/android/exoplayer/extractor/ExtractorSampleSource;

    const/high16 v4, 0x190000

    new-array v5, v9, [Lcom/google/android/exoplayer/extractor/Extractor;

    aput-object v6, v5, v10

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer/extractor/ExtractorSampleSource;-><init>(Landroid/net/Uri;Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/upstream/Allocator;I[Lcom/google/android/exoplayer/extractor/Extractor;)V

    .line 63
    new-instance v1, Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer;

    sget-object v4, Lcom/google/android/exoplayer/MediaCodecSelector;->DEFAULT:Lcom/google/android/exoplayer/MediaCodecSelector;

    const-wide/16 v6, 0x0

    .line 70
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/k;->q()Landroid/os/Handler;

    move-result-object v8

    move-object v2, p1

    move-object v3, v0

    move v5, v9

    move-object v9, p0

    invoke-direct/range {v1 .. v10}, Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer;-><init>(Landroid/content/Context;Lcom/google/android/exoplayer/SampleSource;Lcom/google/android/exoplayer/MediaCodecSelector;IJLandroid/os/Handler;Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$EventListener;I)V

    .line 73
    new-instance v2, Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer;

    sget-object v3, Lcom/google/android/exoplayer/MediaCodecSelector;->DEFAULT:Lcom/google/android/exoplayer/MediaCodecSelector;

    .line 74
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/k;->q()Landroid/os/Handler;

    move-result-object v4

    invoke-direct {v2, v0, v3, v4, p0}, Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer;-><init>(Lcom/google/android/exoplayer/SampleSource;Lcom/google/android/exoplayer/MediaCodecSelector;Landroid/os/Handler;Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer$EventListener;)V

    .line 76
    invoke-virtual {p0, v2}, Lcom/twitter/library/av/playback/k;->b(Lcom/google/android/exoplayer/TrackRenderer;)V

    .line 77
    invoke-virtual {p0, v1}, Lcom/twitter/library/av/playback/k;->a(Lcom/google/android/exoplayer/TrackRenderer;)V

    .line 78
    invoke-virtual {p0}, Lcom/twitter/library/av/playback/k;->j()V

    .line 79
    return-void
.end method

.method public onPlayerSeekComplete()V
    .locals 0

    .prologue
    .line 84
    return-void
.end method
