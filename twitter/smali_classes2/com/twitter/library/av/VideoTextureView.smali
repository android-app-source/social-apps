.class public abstract Lcom/twitter/library/av/VideoTextureView;
.super Landroid/view/TextureView;
.source "Twttr"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;
.implements Lcom/twitter/library/av/aa;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/av/VideoTextureView$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/reflect/Field;


# instance fields
.field protected final b:Lcom/twitter/library/av/z;

.field protected c:I

.field protected d:I

.field e:F
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field final f:Lcom/twitter/library/av/playback/AVPlayerAttachment;

.field final g:Lcom/twitter/library/av/n;

.field h:Lcom/twitter/library/av/n$b;

.field private final i:Lcom/twitter/library/av/VideoTextureView$a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    const/4 v0, 0x0

    .line 39
    :try_start_0
    const-class v1, Landroid/view/TextureView;

    const-string/jumbo v2, "mLayer"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 40
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    :goto_0
    sput-object v0, Lcom/twitter/library/av/VideoTextureView;->a:Ljava/lang/reflect/Field;

    .line 45
    return-void

    .line 41
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/z;)V
    .locals 1

    .prologue
    .line 71
    new-instance v0, Lcom/twitter/library/av/VideoTextureView$a;

    invoke-direct {v0}, Lcom/twitter/library/av/VideoTextureView$a;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/library/av/VideoTextureView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/z;Lcom/twitter/library/av/VideoTextureView$a;)V

    .line 72
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/z;Lcom/twitter/library/av/VideoTextureView$a;)V
    .locals 2

    .prologue
    .line 81
    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/av/VideoTextureView;->e:F

    .line 82
    iput-object p2, p0, Lcom/twitter/library/av/VideoTextureView;->f:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 83
    invoke-virtual {p2}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->H()Lcom/twitter/library/av/n;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/av/VideoTextureView;->g:Lcom/twitter/library/av/n;

    .line 84
    iput-object p3, p0, Lcom/twitter/library/av/VideoTextureView;->b:Lcom/twitter/library/av/z;

    .line 85
    iput-object p4, p0, Lcom/twitter/library/av/VideoTextureView;->i:Lcom/twitter/library/av/VideoTextureView$a;

    .line 86
    invoke-direct {p0}, Lcom/twitter/library/av/VideoTextureView;->d()V

    .line 88
    sget-object v0, Lcom/twitter/library/av/VideoTextureView;->a:Ljava/lang/reflect/Field;

    if-nez v0, :cond_0

    .line 89
    new-instance v0, Ljava/lang/NoSuchFieldException;

    const-string/jumbo v1, "Could not find TextureView.mLayer"

    invoke-direct {v0, v1}, Ljava/lang/NoSuchFieldException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 91
    :cond_0
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/twitter/library/av/VideoTextureView;->getDefaultVideoSize()Lcom/twitter/util/math/Size;

    move-result-object v0

    .line 144
    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->a()I

    move-result v1

    iput v1, p0, Lcom/twitter/library/av/VideoTextureView;->c:I

    .line 145
    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->b()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/av/VideoTextureView;->d:I

    .line 146
    invoke-virtual {p0, p0}, Lcom/twitter/library/av/VideoTextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 147
    return-void
.end method

.method private getDefaultVideoSize()Lcom/twitter/util/math/Size;
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/twitter/library/av/VideoTextureView;->f:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->h()Lcom/twitter/library/av/playback/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v0

    .line 152
    invoke-interface {v0}, Lcom/twitter/library/av/playback/AVDataSource;->b()Lcom/twitter/model/card/property/ImageSpec;

    move-result-object v0

    .line 153
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, v0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    iget v1, v1, Lcom/twitter/model/card/property/Vector2F;->x:F

    iget-object v0, v0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    iget v0, v0, Lcom/twitter/model/card/property/Vector2F;->y:F

    invoke-static {v1, v0}, Lcom/twitter/util/math/Size;->a(FF)Lcom/twitter/util/math/Size;

    move-result-object v0

    .line 156
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/twitter/library/av/VideoTextureView;->h:Lcom/twitter/library/av/n$b;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/twitter/library/av/VideoTextureView;->f:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/av/VideoTextureView;->h:Lcom/twitter/library/av/n$b;

    iget-object v1, v1, Lcom/twitter/library/av/n$b;->b:Landroid/view/Surface;

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->a(Landroid/view/Surface;)V

    .line 171
    :cond_0
    return-void
.end method

.method public a(D)V
    .locals 3

    .prologue
    .line 110
    iget v0, p0, Lcom/twitter/library/av/VideoTextureView;->d:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/av/VideoTextureView;->c:I

    if-nez v0, :cond_1

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    const-wide v0, 0x4046800000000000L    # 45.0

    add-double/2addr v0, p1

    double-to-int v0, v0

    div-int/lit8 v0, v0, 0x5a

    .line 116
    int-to-float v0, v0

    const/high16 v1, 0x42b40000    # 90.0f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/twitter/library/av/VideoTextureView;->e:F

    .line 118
    invoke-virtual {p0}, Lcom/twitter/library/av/VideoTextureView;->b()V

    goto :goto_0
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 129
    iput p1, p0, Lcom/twitter/library/av/VideoTextureView;->c:I

    .line 130
    iput p2, p0, Lcom/twitter/library/av/VideoTextureView;->d:I

    .line 132
    iget v0, p0, Lcom/twitter/library/av/VideoTextureView;->c:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/av/VideoTextureView;->d:I

    if-eqz v0, :cond_0

    .line 133
    invoke-virtual {p0}, Lcom/twitter/library/av/VideoTextureView;->requestLayout()V

    .line 136
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/av/VideoTextureView;->b()V

    .line 137
    return-void
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/twitter/library/av/VideoTextureView;->i:Lcom/twitter/library/av/VideoTextureView$a;

    invoke-virtual {v0, p0}, Lcom/twitter/library/av/VideoTextureView$a;->a(Lcom/twitter/library/av/VideoTextureView;)V

    .line 101
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 226
    invoke-virtual {p0}, Lcom/twitter/library/av/VideoTextureView;->getHardwareLayer()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/av/VideoTextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getAVPlayer()Lcom/twitter/library/av/playback/AVPlayer;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lcom/twitter/library/av/VideoTextureView;->f:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    return-object v0
.end method

.method public getHardwareLayer()Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 212
    :try_start_0
    sget-object v1, Lcom/twitter/library/av/VideoTextureView;->a:Ljava/lang/reflect/Field;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/twitter/library/av/VideoTextureView;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v1, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 214
    :cond_0
    :goto_0
    return-object v0

    .line 213
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public getVideoHeight()I
    .locals 1

    .prologue
    .line 206
    iget v0, p0, Lcom/twitter/library/av/VideoTextureView;->d:I

    return v0
.end method

.method public getVideoWidth()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lcom/twitter/library/av/VideoTextureView;->c:I

    return v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 175
    invoke-super {p0, p1}, Landroid/view/TextureView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 176
    const-class v0, Lcom/twitter/library/av/VideoTextureView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 177
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 181
    invoke-super {p0, p1}, Landroid/view/TextureView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 182
    const-class v0, Lcom/twitter/library/av/VideoTextureView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 183
    return-void
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/twitter/library/av/VideoTextureView;->b()V

    .line 191
    return-void
.end method
