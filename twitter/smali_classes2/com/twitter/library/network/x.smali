.class public Lcom/twitter/library/network/x;
.super Lcom/twitter/network/p;
.source "Twttr"


# instance fields
.field private final b:Lcom/twitter/util/network/g;

.field private final c:Lcom/twitter/library/network/r;

.field private final d:Lokhttp3/Dns;


# direct methods
.method public constructor <init>(Lcom/twitter/util/network/g;Lcom/twitter/network/f;)V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/network/x;-><init>(Lcom/twitter/util/network/g;Lcom/twitter/network/f;Lokhttp3/Dns;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Lcom/twitter/util/network/g;Lcom/twitter/network/f;Lokhttp3/Dns;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p2}, Lcom/twitter/network/p;-><init>(Lcom/twitter/network/f;)V

    .line 40
    iput-object p1, p0, Lcom/twitter/library/network/x;->b:Lcom/twitter/util/network/g;

    .line 41
    invoke-static {}, Lbms;->ah()Lbms;

    move-result-object v0

    invoke-virtual {v0}, Lbms;->O()Lcom/twitter/library/network/r;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/x;->c:Lcom/twitter/library/network/r;

    .line 42
    iput-object p3, p0, Lcom/twitter/library/network/x;->d:Lokhttp3/Dns;

    .line 45
    invoke-static {}, Lokhttp3/internal/platform/Platform;->get()Lokhttp3/internal/platform/Platform;

    move-result-object v0

    instance-of v0, v0, Lokhttp3/internal/platform/TwitterOkHttp3AndroidPlatform;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lokhttp3/internal/platform/TwitterOkHttp3AndroidPlatform;

    invoke-direct {v0, p1}, Lokhttp3/internal/platform/TwitterOkHttp3AndroidPlatform;-><init>(Lcom/twitter/util/network/g;)V

    invoke-static {v0}, Lokhttp3/internal/platform/TwitterOkHttp3AndroidPlatform;->install(Lokhttp3/internal/platform/Platform;)V

    .line 48
    :cond_0
    return-void
.end method

.method private static c()Lokhttp3/Interceptor;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 108
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lcof;->b()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcof;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 118
    :goto_0
    return-object v0

    .line 115
    :cond_0
    :try_start_0
    const-string/jumbo v0, "com.facebook.stetho.okhttp3.StethoInterceptor"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 116
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokhttp3/Interceptor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 117
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 118
    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/network/j;)Lcom/twitter/network/HttpOperation;
    .locals 4

    .prologue
    .line 92
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/network/p;->a(Lcom/twitter/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/network/j;)Lcom/twitter/network/HttpOperation;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lcom/twitter/library/network/x;->c:Lcom/twitter/library/network/r;

    invoke-virtual {v1, p2}, Lcom/twitter/library/network/r;->c(Ljava/net/URI;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 97
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/twitter/network/HttpOperation$Protocol;

    const/4 v2, 0x0

    sget-object v3, Lcom/twitter/network/HttpOperation$Protocol;->c:Lcom/twitter/network/HttpOperation$Protocol;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/network/HttpOperation;->a([Lcom/twitter/network/HttpOperation$Protocol;)V

    .line 99
    :cond_0
    return-object v0
.end method

.method protected a(Lcom/twitter/network/f;)Lokhttp3/OkHttpClient$Builder;
    .locals 5

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/twitter/network/p;->a(Lcom/twitter/network/f;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 59
    iget-object v1, p0, Lcom/twitter/library/network/x;->b:Lcom/twitter/util/network/g;

    invoke-interface {v1}, Lcom/twitter/util/network/g;->b()Ljavax/net/SocketFactory;

    move-result-object v1

    .line 60
    iget-object v2, p0, Lcom/twitter/library/network/x;->b:Lcom/twitter/util/network/g;

    invoke-interface {v2}, Lcom/twitter/util/network/g;->a()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v2

    .line 61
    iget-object v3, p0, Lcom/twitter/library/network/x;->b:Lcom/twitter/util/network/g;

    invoke-interface {v3}, Lcom/twitter/util/network/g;->c()Ljavax/net/ssl/X509TrustManager;

    move-result-object v3

    .line 62
    iget-object v4, p0, Lcom/twitter/library/network/x;->b:Lcom/twitter/util/network/g;

    invoke-interface {v4}, Lcom/twitter/util/network/g;->d()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v4

    .line 64
    if-eqz v4, :cond_0

    .line 65
    invoke-virtual {v0, v4}, Lokhttp3/OkHttpClient$Builder;->hostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)Lokhttp3/OkHttpClient$Builder;

    .line 68
    :cond_0
    if-eqz v1, :cond_1

    .line 69
    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->socketFactory(Ljavax/net/SocketFactory;)Lokhttp3/OkHttpClient$Builder;

    .line 72
    :cond_1
    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    .line 73
    invoke-virtual {v0, v2, v3}, Lokhttp3/OkHttpClient$Builder;->sslSocketFactory(Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/X509TrustManager;)Lokhttp3/OkHttpClient$Builder;

    .line 76
    :cond_2
    iget-object v1, p0, Lcom/twitter/library/network/x;->d:Lokhttp3/Dns;

    if-eqz v1, :cond_3

    .line 77
    iget-object v1, p0, Lcom/twitter/library/network/x;->d:Lokhttp3/Dns;

    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->dns(Lokhttp3/Dns;)Lokhttp3/OkHttpClient$Builder;

    .line 80
    :cond_3
    invoke-static {}, Lcom/twitter/library/network/x;->c()Lokhttp3/Interceptor;

    move-result-object v1

    .line 81
    if-eqz v1, :cond_4

    .line 82
    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->addNetworkInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    .line 85
    :cond_4
    return-object v0
.end method
