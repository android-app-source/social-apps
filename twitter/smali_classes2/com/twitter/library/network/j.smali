.class public Lcom/twitter/library/network/j;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/network/c;


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:J

.field private c:Z

.field private d:Z

.field private e:Lcom/twitter/analytics/feature/model/ClientEventLog;

.field private final f:Landroid/content/Context;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private final k:Lcom/twitter/library/network/c;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "okhttp"

    const-string/jumbo v2, "okhttp1"

    .line 38
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "null"

    const-string/jumbo v2, "unknown"

    .line 39
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 36
    invoke-static {v0}, Lcom/twitter/util/collection/ImmutableMap;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/network/j;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(JLcom/twitter/library/network/c;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-wide p1, p0, Lcom/twitter/library/network/j;->b:J

    .line 61
    iput-object p3, p0, Lcom/twitter/library/network/j;->k:Lcom/twitter/library/network/c;

    .line 62
    iput-object p4, p0, Lcom/twitter/library/network/j;->f:Landroid/content/Context;

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 53
    const-wide/16 v0, -0x1

    new-instance v2, Lcom/twitter/library/network/d;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/network/d;-><init>(ZLjava/lang/String;)V

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/twitter/library/network/j;-><init>(JLcom/twitter/library/network/c;Landroid/content/Context;)V

    .line 54
    return-void
.end method

.method static a(Lcom/twitter/network/l;)Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 176
    iget-object v0, p0, Lcom/twitter/network/l;->p:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 177
    sget-object v1, Lcom/twitter/library/network/j;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/twitter/library/network/j;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_0
    return-object v0
.end method


# virtual methods
.method a(Ljava/lang/String;Lcom/twitter/network/HttpOperation;Ljava/lang/String;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent;
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 156
    iget-object v0, p0, Lcom/twitter/library/network/j;->f:Landroid/content/Context;

    .line 157
    invoke-static {v0, p2}, Lbsh;->a(Landroid/content/Context;Lcom/twitter/network/HttpOperation;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequest;

    move-result-object v0

    .line 159
    new-instance v1, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent$a;

    invoke-direct {v1}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent$a;-><init>()V

    .line 162
    sget-object v2, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent;->b:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent$_Fields;

    .line 164
    invoke-static {}, Lcom/twitter/library/scribe/ScribeService;->a()Lcom/twitter/common_header/thriftandroid/VersionedCommonHeader;

    move-result-object v3

    .line 163
    invoke-virtual {v1, v2, v3}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent$a;

    move-result-object v2

    sget-object v3, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent;->c:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent$_Fields;

    .line 165
    invoke-virtual {v2, v3, p1}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent$a;

    move-result-object v2

    sget-object v3, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent;->d:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent$_Fields;

    .line 166
    invoke-virtual {v2, v3, v0}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent$a;

    move-result-object v0

    sget-object v2, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent;->e:Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent$_Fields;

    .line 167
    invoke-virtual {v0, v2, p3}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent$a;->a(Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent$_Fields;Ljava/lang/Object;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent$a;

    .line 169
    invoke-virtual {v1}, Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent$a;->a()Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/network/HttpOperation;)V
    .locals 5

    .prologue
    const-wide/16 v2, -0x1

    const/4 v4, 0x1

    .line 67
    invoke-static {}, Lcom/twitter/util/android/b;->a()Lcom/twitter/util/android/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/android/b;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/network/j;->c:Z

    .line 68
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v0

    invoke-virtual {v0}, Lcrr;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/network/j;->d:Z

    .line 70
    const-string/jumbo v0, "scribe_client_network_request_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/network/j;->g:Z

    .line 72
    const-string/jumbo v0, "scribe_legacy_client_network_request_enabled"

    invoke-static {v0, v4}, Lcoj;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/network/j;->h:Z

    .line 74
    invoke-static {p1}, Lcom/twitter/library/scribe/ScribeService;->a(Lcom/twitter/network/HttpOperation;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/network/j;->i:Z

    .line 76
    iget-boolean v0, p0, Lcom/twitter/library/network/j;->i:Z

    if-eqz v0, :cond_2

    .line 77
    const-string/jumbo v0, "scribe_cdn_sample_size"

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeService;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/network/j;->j:Z

    .line 82
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/twitter/library/network/j;->h:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/library/network/j;->j:Z

    if-eqz v0, :cond_1

    .line 84
    iget-wide v0, p0, Lcom/twitter/library/network/j;->b:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    iget-wide v0, p0, Lcom/twitter/library/network/j;->b:J

    .line 85
    :goto_1
    iget-boolean v2, p0, Lcom/twitter/library/network/j;->i:Z

    if-eqz v2, :cond_4

    const-string/jumbo v2, "cdn::::request"

    .line 86
    :goto_2
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v3, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v0, v4, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    invoke-virtual {v3, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iput-object v0, p0, Lcom/twitter/library/network/j;->e:Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 87
    iget-object v0, p0, Lcom/twitter/library/network/j;->e:Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-static {v0}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 89
    :cond_1
    return-void

    .line 78
    :cond_2
    iget-wide v0, p0, Lcom/twitter/library/network/j;->b:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 79
    const-string/jumbo v0, "scribe_api_sample_size"

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeService;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/network/j;->j:Z

    goto :goto_0

    .line 84
    :cond_3
    const-wide/16 v0, 0x0

    goto :goto_1

    .line 85
    :cond_4
    const-string/jumbo v2, "api::::request"

    goto :goto_2
.end method

.method public a(Lcom/twitter/network/HttpOperation;Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 150
    return-void
.end method

.method public b(Lcom/twitter/network/HttpOperation;)V
    .locals 14

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    const-wide/16 v12, -0x1

    .line 94
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->m()Lcom/twitter/network/l;

    move-result-object v8

    .line 97
    const-string/jumbo v1, "OkHttp-Response-Source"

    invoke-virtual {p1, v1}, Lcom/twitter/network/HttpOperation;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 98
    if-eqz v1, :cond_6

    const-string/jumbo v3, "CACHE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    move v1, v0

    .line 101
    :goto_0
    iget-boolean v3, p0, Lcom/twitter/library/network/j;->g:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/twitter/library/network/j;->j:Z

    if-eqz v3, :cond_0

    .line 102
    iget-boolean v3, p0, Lcom/twitter/library/network/j;->i:Z

    if-eqz v3, :cond_7

    const-string/jumbo v3, "cdn:all"

    .line 104
    :goto_1
    invoke-static {v8}, Lcom/twitter/library/network/j;->a(Lcom/twitter/network/l;)Ljava/lang/String;

    move-result-object v4

    .line 103
    invoke-virtual {p0, v3, p1, v4}, Lcom/twitter/library/network/j;->a(Ljava/lang/String;Lcom/twitter/network/HttpOperation;Ljava/lang/String;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent;

    move-result-object v3

    .line 105
    iget-wide v4, p0, Lcom/twitter/library/network/j;->b:J

    cmp-long v4, v4, v12

    if-eqz v4, :cond_8

    iget-wide v4, p0, Lcom/twitter/library/network/j;->b:J

    .line 106
    :goto_2
    iget-object v9, p0, Lcom/twitter/library/network/j;->f:Landroid/content/Context;

    sget-object v10, Lcom/twitter/library/scribe/LogCategory;->b:Lcom/twitter/library/scribe/LogCategory;

    invoke-static {v9, v10, v4, v5, v3}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/LogCategory;JLorg/apache/thrift/TBase;)V

    .line 111
    :cond_0
    iget-object v3, p0, Lcom/twitter/library/network/j;->e:Lcom/twitter/analytics/feature/model/ClientEventLog;

    if-eqz v3, :cond_1

    .line 112
    iget-object v3, p0, Lcom/twitter/library/network/j;->e:Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-boolean v4, p0, Lcom/twitter/library/network/j;->c:Z

    invoke-static {v3, p1, v4}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/model/ScribeLog;Lcom/twitter/network/HttpOperation;Z)V

    .line 113
    iget-object v3, p0, Lcom/twitter/library/network/j;->e:Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-static {v3}, Lcpm;->a(Lcpk;)V

    .line 117
    :cond_1
    iget-wide v4, p0, Lcom/twitter/library/network/j;->b:J

    cmp-long v3, v4, v12

    if-eqz v3, :cond_4

    invoke-virtual {v8}, Lcom/twitter/network/l;->a()Z

    move-result v3

    if-nez v3, :cond_4

    iget-boolean v3, p0, Lcom/twitter/library/network/j;->i:Z

    if-nez v3, :cond_4

    .line 118
    const-string/jumbo v3, "scribe_api_error_sample_size"

    invoke-static {v3}, Lcom/twitter/library/scribe/ScribeService;->a(Ljava/lang/String;)Z

    move-result v3

    .line 122
    iget-boolean v4, p0, Lcom/twitter/library/network/j;->g:Z

    if-eqz v4, :cond_3

    if-eqz v3, :cond_3

    .line 123
    const-string/jumbo v4, "api:error"

    .line 124
    invoke-static {v8}, Lcom/twitter/library/network/j;->a(Lcom/twitter/network/l;)Ljava/lang/String;

    move-result-object v5

    .line 123
    invoke-virtual {p0, v4, p1, v5}, Lcom/twitter/library/network/j;->a(Ljava/lang/String;Lcom/twitter/network/HttpOperation;Ljava/lang/String;)Lcom/twitter/client_network/thriftandroid/ClientNetworkRequestEvent;

    move-result-object v4

    .line 125
    iget-wide v10, p0, Lcom/twitter/library/network/j;->b:J

    cmp-long v5, v10, v12

    if-eqz v5, :cond_2

    iget-wide v6, p0, Lcom/twitter/library/network/j;->b:J

    .line 126
    :cond_2
    iget-object v5, p0, Lcom/twitter/library/network/j;->f:Landroid/content/Context;

    sget-object v9, Lcom/twitter/library/scribe/LogCategory;->b:Lcom/twitter/library/scribe/LogCategory;

    invoke-static {v5, v9, v6, v7, v4}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/LogCategory;JLorg/apache/thrift/TBase;)V

    .line 130
    :cond_3
    iget-boolean v4, p0, Lcom/twitter/library/network/j;->h:Z

    if-eqz v4, :cond_4

    if-eqz v3, :cond_4

    .line 131
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/library/network/j;->b:J

    invoke-direct {v3, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v4, "api::::error"

    aput-object v4, v0, v2

    .line 132
    invoke-virtual {v3, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 133
    invoke-static {v8}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/network/l;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(I)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget v2, v8, Lcom/twitter/network/l;->j:I

    .line 134
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 135
    iget-boolean v2, p0, Lcom/twitter/library/network/j;->c:Z

    invoke-static {v0, p1, v2}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/model/ScribeLog;Lcom/twitter/network/HttpOperation;Z)V

    .line 136
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 140
    :cond_4
    if-nez v1, :cond_5

    .line 141
    iget-object v0, p0, Lcom/twitter/library/network/j;->k:Lcom/twitter/library/network/c;

    iget-boolean v1, p0, Lcom/twitter/library/network/j;->d:Z

    invoke-interface {v0, v1, p1}, Lcom/twitter/library/network/c;->a(ZLcom/twitter/network/HttpOperation;)Lcom/twitter/library/network/DataUsageEvent;

    move-result-object v0

    .line 142
    if-eqz v0, :cond_5

    .line 143
    invoke-static {}, Lcom/twitter/library/network/b;->a()Lcom/twitter/library/network/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/b;->a(Ljava/lang/Object;)V

    .line 146
    :cond_5
    return-void

    :cond_6
    move v1, v2

    .line 98
    goto/16 :goto_0

    .line 102
    :cond_7
    const-string/jumbo v3, "api:all"

    goto/16 :goto_1

    :cond_8
    move-wide v4, v6

    .line 105
    goto/16 :goto_2
.end method
