.class public Lcom/twitter/library/network/y;
.super Lcom/twitter/network/r;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/network/r$b;


# instance fields
.field private final c:Lcom/twitter/library/network/r;


# direct methods
.method public constructor <init>(Lcom/twitter/util/network/g;Lcom/twitter/network/f;)V
    .locals 1

    .prologue
    .line 43
    invoke-static {}, Lbms;->ah()Lbms;

    move-result-object v0

    invoke-virtual {v0}, Lbms;->O()Lcom/twitter/library/network/r;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/network/y;-><init>(Lcom/twitter/util/network/g;Lcom/twitter/network/f;Lcom/twitter/library/network/r;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Lcom/twitter/util/network/g;Lcom/twitter/network/f;Lcom/twitter/library/network/r;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0, p2}, Lcom/twitter/network/r;-><init>(Lcom/twitter/network/f;)V

    .line 33
    iget-object v0, p0, Lcom/twitter/library/network/y;->b:Lcom/squareup/okhttp/v_1_5_1/h;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/h;->a(Ljava/net/CookieHandler;)Lcom/squareup/okhttp/v_1_5_1/h;

    .line 35
    iget-object v0, p0, Lcom/twitter/library/network/y;->b:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-interface {p1}, Lcom/twitter/util/network/g;->a()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/h;->a(Ljavax/net/ssl/SSLSocketFactory;)Lcom/squareup/okhttp/v_1_5_1/h;

    .line 36
    iget-object v0, p0, Lcom/twitter/library/network/y;->b:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-interface {p1}, Lcom/twitter/util/network/g;->d()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/h;->a(Ljavax/net/ssl/HostnameVerifier;)Lcom/squareup/okhttp/v_1_5_1/h;

    .line 37
    iput-object p3, p0, Lcom/twitter/library/network/y;->c:Lcom/twitter/library/network/r;

    .line 38
    invoke-virtual {p3, p0}, Lcom/twitter/library/network/r;->a(Lcom/twitter/library/network/r$b;)V

    .line 39
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/network/j;)Lcom/twitter/network/HttpOperation;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 50
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/network/r;->a(Lcom/twitter/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/network/j;)Lcom/twitter/network/HttpOperation;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lcom/twitter/library/network/y;->c:Lcom/twitter/library/network/r;

    invoke-virtual {v1, p2}, Lcom/twitter/library/network/r;->b(Ljava/net/URI;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52
    const/4 v1, 0x2

    new-array v1, v1, [Lcom/twitter/network/HttpOperation$Protocol;

    sget-object v2, Lcom/twitter/network/HttpOperation$Protocol;->g:Lcom/twitter/network/HttpOperation$Protocol;

    aput-object v2, v1, v3

    sget-object v2, Lcom/twitter/network/HttpOperation$Protocol;->c:Lcom/twitter/network/HttpOperation$Protocol;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/network/HttpOperation;->a([Lcom/twitter/network/HttpOperation$Protocol;)V

    .line 56
    :goto_0
    return-object v0

    .line 54
    :cond_0
    new-array v1, v4, [Lcom/twitter/network/HttpOperation$Protocol;

    sget-object v2, Lcom/twitter/network/HttpOperation$Protocol;->c:Lcom/twitter/network/HttpOperation$Protocol;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/twitter/network/HttpOperation;->a([Lcom/twitter/network/HttpOperation$Protocol;)V

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 86
    invoke-super {p0}, Lcom/twitter/network/r;->a()V

    .line 87
    iget-object v0, p0, Lcom/twitter/library/network/y;->c:Lcom/twitter/library/network/r;

    invoke-virtual {v0, p0}, Lcom/twitter/library/network/r;->b(Lcom/twitter/library/network/r$b;)V

    .line 88
    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/util/network/d;",
            "Lcom/twitter/util/network/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v2

    .line 65
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 66
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/util/network/d;

    .line 67
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/network/d;

    .line 69
    new-instance v4, Lcom/squareup/okhttp/v_1_5_1/internal/http/m;

    .line 70
    invoke-virtual {v1}, Lcom/twitter/util/network/d;->b()Ljava/lang/String;

    move-result-object v5

    .line 71
    invoke-virtual {v1}, Lcom/twitter/util/network/d;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Lcom/twitter/util/network/d;->c()I

    move-result v1

    invoke-direct {v4, v5, v6, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/m;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 72
    new-instance v1, Lcom/squareup/okhttp/v_1_5_1/internal/http/m;

    .line 73
    invoke-virtual {v0}, Lcom/twitter/util/network/d;->b()Ljava/lang/String;

    move-result-object v5

    .line 74
    invoke-virtual {v0}, Lcom/twitter/util/network/d;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/twitter/util/network/d;->c()I

    move-result v0

    invoke-direct {v1, v5, v6, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/m;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 75
    invoke-virtual {v2, v4, v1}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    goto :goto_0

    .line 77
    :cond_0
    iget-object v1, p0, Lcom/twitter/library/network/y;->b:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v2}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v1, v0}, Lcom/squareup/okhttp/v_1_5_1/h;->a(Ljava/util/Map;)Lcom/squareup/okhttp/v_1_5_1/h;

    .line 78
    return-void
.end method

.method public b(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 82
    return-void
.end method
