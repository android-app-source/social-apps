.class public Lcom/twitter/library/network/narc/e;
.super Lcom/twitter/app/common/util/b$a;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/library/network/narc/e;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/twitter/app/common/util/b$a;-><init>()V

    .line 15
    invoke-static {}, Lcom/twitter/app/common/util/b;->a()Lcom/twitter/app/common/util/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/app/common/util/b;->a(Lcom/twitter/app/common/util/b$a;)V

    .line 16
    return-void
.end method

.method public static declared-synchronized a()V
    .locals 2

    .prologue
    .line 19
    const-class v1, Lcom/twitter/library/network/narc/e;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/network/narc/e;->a:Lcom/twitter/library/network/narc/e;

    if-nez v0, :cond_0

    .line 20
    new-instance v0, Lcom/twitter/library/network/narc/e;

    invoke-direct {v0}, Lcom/twitter/library/network/narc/e;-><init>()V

    sput-object v0, Lcom/twitter/library/network/narc/e;->a:Lcom/twitter/library/network/narc/e;

    .line 21
    const-class v0, Lcom/twitter/library/network/narc/e;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    :cond_0
    monitor-exit v1

    return-void

    .line 19
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 48
    sget-object v0, Lcom/twitter/library/network/narc/AppStateNARCEntry$AppState;->c:Lcom/twitter/library/network/narc/AppStateNARCEntry$AppState;

    sget-object v1, Lcom/twitter/library/network/narc/AppStateNARCEntry$AppStateType;->b:Lcom/twitter/library/network/narc/AppStateNARCEntry$AppStateType;

    .line 51
    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v2

    .line 48
    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/narc/i;->a(Lcom/twitter/library/network/narc/AppStateNARCEntry$AppState;Lcom/twitter/library/network/narc/AppStateNARCEntry$AppStateType;Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 40
    sget-object v0, Lcom/twitter/library/network/narc/AppStateNARCEntry$AppState;->b:Lcom/twitter/library/network/narc/AppStateNARCEntry$AppState;

    sget-object v1, Lcom/twitter/library/network/narc/AppStateNARCEntry$AppStateType;->a:Lcom/twitter/library/network/narc/AppStateNARCEntry$AppStateType;

    .line 43
    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v2

    .line 40
    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/narc/i;->a(Lcom/twitter/library/network/narc/AppStateNARCEntry$AppState;Lcom/twitter/library/network/narc/AppStateNARCEntry$AppStateType;Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 32
    sget-object v0, Lcom/twitter/library/network/narc/AppStateNARCEntry$AppState;->a:Lcom/twitter/library/network/narc/AppStateNARCEntry$AppState;

    sget-object v1, Lcom/twitter/library/network/narc/AppStateNARCEntry$AppStateType;->a:Lcom/twitter/library/network/narc/AppStateNARCEntry$AppStateType;

    .line 35
    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v2

    .line 32
    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/narc/i;->a(Lcom/twitter/library/network/narc/AppStateNARCEntry$AppState;Lcom/twitter/library/network/narc/AppStateNARCEntry$AppStateType;Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 56
    sget-object v0, Lcom/twitter/library/network/narc/AppStateNARCEntry$AppState;->d:Lcom/twitter/library/network/narc/AppStateNARCEntry$AppState;

    sget-object v1, Lcom/twitter/library/network/narc/AppStateNARCEntry$AppStateType;->b:Lcom/twitter/library/network/narc/AppStateNARCEntry$AppStateType;

    .line 59
    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v2

    .line 56
    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/narc/i;->a(Lcom/twitter/library/network/narc/AppStateNARCEntry$AppState;Lcom/twitter/library/network/narc/AppStateNARCEntry$AppStateType;Ljava/lang/String;)V

    .line 60
    return-void
.end method
