.class public final Lcom/twitter/library/network/narc/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/network/c;


# instance fields
.field private a:Lcom/twitter/library/network/narc/p;

.field private final b:Lcom/twitter/async/service/b;


# direct methods
.method public constructor <init>(Lcom/twitter/async/service/b;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/twitter/library/network/narc/d;->b:Lcom/twitter/async/service/b;

    .line 21
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/network/HttpOperation;)V
    .locals 3

    .prologue
    .line 25
    const-string/jumbo v1, "HttpOperationNARCLogger"

    monitor-enter v1

    .line 26
    :try_start_0
    new-instance v0, Lcom/twitter/library/network/narc/p;

    iget-object v2, p0, Lcom/twitter/library/network/narc/d;->b:Lcom/twitter/async/service/b;

    invoke-direct {v0, p1, v2}, Lcom/twitter/library/network/narc/p;-><init>(Lcom/twitter/network/HttpOperation;Lcom/twitter/async/service/b;)V

    iput-object v0, p0, Lcom/twitter/library/network/narc/d;->a:Lcom/twitter/library/network/narc/p;

    .line 27
    iget-object v0, p0, Lcom/twitter/library/network/narc/d;->a:Lcom/twitter/library/network/narc/p;

    invoke-static {v0}, Lcom/twitter/library/network/narc/i;->a(Lcom/twitter/library/network/narc/AbstractNARCEntry;)V

    .line 28
    monitor-exit v1

    .line 29
    return-void

    .line 28
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/twitter/network/HttpOperation;Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public b(Lcom/twitter/network/HttpOperation;)V
    .locals 2

    .prologue
    .line 33
    const-string/jumbo v1, "HttpOperationNARCLogger"

    monitor-enter v1

    .line 34
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/narc/d;->a:Lcom/twitter/library/network/narc/p;

    invoke-virtual {v0}, Lcom/twitter/library/network/narc/p;->b()V

    .line 35
    monitor-exit v1

    .line 36
    return-void

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
