.class public final Lcom/twitter/library/network/narc/p;
.super Lcom/twitter/library/network/narc/AbstractNARCEntry;
.source "Twttr"


# instance fields
.field public c:Lcom/twitter/library/network/narc/l;

.field public d:I

.field public e:Lcom/twitter/library/network/narc/m;

.field public f:Ljava/lang/String;

.field public g:J

.field public h:Lcom/twitter/library/network/narc/o;

.field private i:Lcom/twitter/network/HttpOperation;

.field private j:Lcom/twitter/async/service/b;


# direct methods
.method public constructor <init>(Lcom/twitter/network/HttpOperation;Lcom/twitter/async/service/b;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const-wide/16 v0, -0x1

    .line 93
    sget-object v2, Lcom/twitter/library/network/narc/AbstractNARCEntry$EntryType;->c:Lcom/twitter/library/network/narc/AbstractNARCEntry$EntryType;

    invoke-direct {p0, v2}, Lcom/twitter/library/network/narc/AbstractNARCEntry;-><init>(Lcom/twitter/library/network/narc/AbstractNARCEntry$EntryType;)V

    .line 67
    iput-object v3, p0, Lcom/twitter/library/network/narc/p;->f:Ljava/lang/String;

    .line 73
    iput-wide v0, p0, Lcom/twitter/library/network/narc/p;->g:J

    .line 78
    iput-object v3, p0, Lcom/twitter/library/network/narc/p;->h:Lcom/twitter/library/network/narc/o;

    .line 95
    iput-object p1, p0, Lcom/twitter/library/network/narc/p;->i:Lcom/twitter/network/HttpOperation;

    .line 96
    iput-object p2, p0, Lcom/twitter/library/network/narc/p;->j:Lcom/twitter/async/service/b;

    .line 97
    new-instance v2, Lcom/twitter/library/network/narc/l;

    invoke-direct {v2, p1}, Lcom/twitter/library/network/narc/l;-><init>(Lcom/twitter/network/HttpOperation;)V

    iput-object v2, p0, Lcom/twitter/library/network/narc/p;->c:Lcom/twitter/library/network/narc/l;

    .line 100
    iget-object v2, p0, Lcom/twitter/library/network/narc/p;->j:Lcom/twitter/async/service/b;

    if-eqz v2, :cond_0

    .line 101
    iget-object v2, p0, Lcom/twitter/library/network/narc/p;->j:Lcom/twitter/async/service/b;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    iput v2, p0, Lcom/twitter/library/network/narc/p;->d:I

    .line 102
    iget-object v2, p0, Lcom/twitter/library/network/narc/p;->j:Lcom/twitter/async/service/b;

    const-string/jumbo v3, "blocking"

    invoke-virtual {v2, v3, v0, v1}, Lcom/twitter/async/service/b;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 108
    :goto_0
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    :goto_1
    iput-wide v0, p0, Lcom/twitter/library/network/narc/p;->b:J

    .line 109
    return-void

    .line 104
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    iput v2, p0, Lcom/twitter/library/network/narc/p;->d:I

    goto :goto_0

    .line 108
    :cond_1
    iget-wide v0, p0, Lcom/twitter/library/network/narc/p;->b:J

    goto :goto_1
.end method


# virtual methods
.method protected declared-synchronized a()Lorg/json/JSONObject;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 133
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/narc/p;->i:Lcom/twitter/network/HttpOperation;

    if-eqz v0, :cond_0

    .line 134
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/library/network/narc/p;->b:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/library/network/narc/p;->g:J

    .line 135
    new-instance v0, Lcom/twitter/library/network/narc/o;

    iget-object v1, p0, Lcom/twitter/library/network/narc/p;->i:Lcom/twitter/network/HttpOperation;

    iget-object v2, p0, Lcom/twitter/library/network/narc/p;->j:Lcom/twitter/async/service/b;

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/network/narc/o;-><init>(Lcom/twitter/network/HttpOperation;Lcom/twitter/async/service/b;)V

    iput-object v0, p0, Lcom/twitter/library/network/narc/p;->h:Lcom/twitter/library/network/narc/o;

    .line 136
    new-instance v0, Lcom/twitter/library/network/narc/m;

    iget-object v1, p0, Lcom/twitter/library/network/narc/p;->i:Lcom/twitter/network/HttpOperation;

    invoke-direct {v0, v1}, Lcom/twitter/library/network/narc/m;-><init>(Lcom/twitter/network/HttpOperation;)V

    iput-object v0, p0, Lcom/twitter/library/network/narc/p;->e:Lcom/twitter/library/network/narc/m;

    .line 139
    iget-object v0, p0, Lcom/twitter/library/network/narc/p;->e:Lcom/twitter/library/network/narc/m;

    const/4 v1, -0x1

    iput v1, v0, Lcom/twitter/library/network/narc/m;->g:I

    .line 142
    iget-object v0, p0, Lcom/twitter/library/network/narc/p;->h:Lcom/twitter/library/network/narc/o;

    iget-wide v2, p0, Lcom/twitter/library/network/narc/p;->g:J

    iget-object v1, p0, Lcom/twitter/library/network/narc/p;->h:Lcom/twitter/library/network/narc/o;

    iget-wide v4, v1, Lcom/twitter/library/network/narc/o;->f:J

    sub-long/2addr v2, v4

    iget-object v1, p0, Lcom/twitter/library/network/narc/p;->h:Lcom/twitter/library/network/narc/o;

    iget-wide v4, v1, Lcom/twitter/library/network/narc/o;->a:J

    sub-long/2addr v2, v4

    iget-object v1, p0, Lcom/twitter/library/network/narc/p;->h:Lcom/twitter/library/network/narc/o;

    iget-wide v4, v1, Lcom/twitter/library/network/narc/o;->c:J

    sub-long/2addr v2, v4

    iget-object v1, p0, Lcom/twitter/library/network/narc/p;->h:Lcom/twitter/library/network/narc/o;

    iget-wide v4, v1, Lcom/twitter/library/network/narc/o;->b:J

    sub-long/2addr v2, v4

    iget-object v1, p0, Lcom/twitter/library/network/narc/p;->h:Lcom/twitter/library/network/narc/o;

    iget-wide v4, v1, Lcom/twitter/library/network/narc/o;->d:J

    sub-long/2addr v2, v4

    iget-object v1, p0, Lcom/twitter/library/network/narc/p;->h:Lcom/twitter/library/network/narc/o;

    iget-wide v4, v1, Lcom/twitter/library/network/narc/o;->g:J

    sub-long/2addr v2, v4

    iget-object v1, p0, Lcom/twitter/library/network/narc/p;->h:Lcom/twitter/library/network/narc/o;

    iget-wide v4, v1, Lcom/twitter/library/network/narc/o;->e:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, Lcom/twitter/library/network/narc/o;->f:J

    .line 148
    :cond_0
    invoke-super {p0}, Lcom/twitter/library/network/narc/AbstractNARCEntry;->a()Lorg/json/JSONObject;

    move-result-object v0

    .line 150
    const-string/jumbo v1, "time"

    iget-wide v2, p0, Lcom/twitter/library/network/narc/p;->g:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 151
    const-string/jumbo v1, "requestId"

    iget v2, p0, Lcom/twitter/library/network/narc/p;->d:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 153
    iget-object v1, p0, Lcom/twitter/library/network/narc/p;->c:Lcom/twitter/library/network/narc/l;

    invoke-virtual {v1}, Lcom/twitter/library/network/narc/l;->a()Lorg/json/JSONObject;

    move-result-object v1

    .line 154
    const-string/jumbo v2, "request"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 156
    iget-object v1, p0, Lcom/twitter/library/network/narc/p;->e:Lcom/twitter/library/network/narc/m;

    invoke-virtual {v1}, Lcom/twitter/library/network/narc/m;->a()Lorg/json/JSONObject;

    move-result-object v1

    .line 157
    const-string/jumbo v2, "response"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 159
    const-string/jumbo v1, "cache"

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 161
    iget-object v1, p0, Lcom/twitter/library/network/narc/p;->h:Lcom/twitter/library/network/narc/o;

    invoke-virtual {v1}, Lcom/twitter/library/network/narc/o;->a()Lorg/json/JSONObject;

    move-result-object v1

    .line 162
    const-string/jumbo v2, "timings"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 164
    const-string/jumbo v1, "serverIPAddress"

    iget-object v2, p0, Lcom/twitter/library/network/narc/p;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    monitor-exit p0

    return-object v0

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 4

    .prologue
    .line 113
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/twitter/library/network/narc/m;

    iget-object v1, p0, Lcom/twitter/library/network/narc/p;->i:Lcom/twitter/network/HttpOperation;

    invoke-direct {v0, v1}, Lcom/twitter/library/network/narc/m;-><init>(Lcom/twitter/network/HttpOperation;)V

    iput-object v0, p0, Lcom/twitter/library/network/narc/p;->e:Lcom/twitter/library/network/narc/m;

    .line 115
    iget-object v0, p0, Lcom/twitter/library/network/narc/p;->i:Lcom/twitter/network/HttpOperation;

    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->m()Lcom/twitter/network/l;

    move-result-object v2

    .line 117
    iget-object v0, p0, Lcom/twitter/library/network/narc/p;->j:Lcom/twitter/async/service/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/network/narc/p;->j:Lcom/twitter/async/service/b;

    invoke-virtual {v0}, Lcom/twitter/async/service/b;->a()J

    move-result-wide v0

    .line 118
    :goto_0
    iget-wide v2, v2, Lcom/twitter/network/l;->e:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/library/network/narc/p;->g:J

    .line 119
    new-instance v0, Lcom/twitter/library/network/narc/o;

    iget-object v1, p0, Lcom/twitter/library/network/narc/p;->i:Lcom/twitter/network/HttpOperation;

    iget-object v2, p0, Lcom/twitter/library/network/narc/p;->j:Lcom/twitter/async/service/b;

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/network/narc/o;-><init>(Lcom/twitter/network/HttpOperation;Lcom/twitter/async/service/b;)V

    iput-object v0, p0, Lcom/twitter/library/network/narc/p;->h:Lcom/twitter/library/network/narc/o;

    .line 121
    iget-object v0, p0, Lcom/twitter/library/network/narc/p;->i:Lcom/twitter/network/HttpOperation;

    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/twitter/library/network/narc/p;->i:Lcom/twitter/network/HttpOperation;

    const-string/jumbo v1, "Server"

    invoke-virtual {v0, v1}, Lcom/twitter/network/HttpOperation;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/narc/p;->f:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/twitter/library/network/narc/p;->i:Lcom/twitter/network/HttpOperation;

    .line 127
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/network/narc/p;->j:Lcom/twitter/async/service/b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 129
    monitor-exit p0

    return-void

    .line 117
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 126
    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    :try_start_2
    iput-object v1, p0, Lcom/twitter/library/network/narc/p;->i:Lcom/twitter/network/HttpOperation;

    .line 127
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/library/network/narc/p;->j:Lcom/twitter/async/service/b;

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 113
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
