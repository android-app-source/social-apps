.class public Lcom/twitter/library/network/l;
.super Lcom/twitter/network/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/network/l$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/concurrent/atomic/AtomicInteger;

.field private d:Lcom/twitter/network/g;

.field private e:Lcom/twitter/network/g;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "com.twitter.library.network"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".debug.DebugHttpOperationClientFactory"

    .line 57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/network/l;->a:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/util/connectivity/a;)V
    .locals 2

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/twitter/network/i;-><init>()V

    .line 62
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/library/network/l;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 71
    iput-object p1, p0, Lcom/twitter/library/network/l;->b:Landroid/content/Context;

    .line 72
    invoke-static {}, Lcon;->a()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/l$1;

    invoke-direct {v1, p0}, Lcom/twitter/library/network/l$1;-><init>(Lcom/twitter/library/network/l;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 78
    if-eqz p2, :cond_0

    .line 79
    new-instance v0, Lcom/twitter/library/network/l$2;

    invoke-direct {v0, p0}, Lcom/twitter/library/network/l$2;-><init>(Lcom/twitter/library/network/l;)V

    invoke-virtual {p2, v0}, Lcom/twitter/util/connectivity/a;->a(Lcom/twitter/util/q;)Z

    .line 92
    :cond_0
    return-void
.end method

.method private static a(ZZ)I
    .locals 1

    .prologue
    .line 215
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    const/4 v0, 0x2

    .line 224
    :goto_0
    return v0

    .line 219
    :cond_0
    if-eqz p0, :cond_1

    .line 220
    const/4 v0, 0x6

    goto :goto_0

    .line 221
    :cond_1
    if-eqz p1, :cond_2

    .line 222
    const/4 v0, 0x3

    goto :goto_0

    .line 224
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;)Lcom/twitter/network/g;
    .locals 4

    .prologue
    .line 156
    :try_start_0
    sget-object v0, Lcom/twitter/library/network/l;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Landroid/content/Context;

    aput-object v3, v1, v2

    .line 157
    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    .line 158
    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/network/g;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    return-object v0

    .line 159
    :catch_0
    move-exception v0

    .line 160
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Could not initialize "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/network/l;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Landroid/content/Context;I)Lcom/twitter/network/g;
    .locals 1

    .prologue
    .line 130
    packed-switch p1, :pswitch_data_0

    .line 142
    :pswitch_0
    new-instance v0, Lcom/twitter/library/network/v$a;

    invoke-direct {v0, p0}, Lcom/twitter/library/network/v$a;-><init>(Landroid/content/Context;)V

    :goto_0
    return-object v0

    .line 132
    :pswitch_1
    invoke-static {p0}, Lcom/twitter/library/network/l;->a(Landroid/content/Context;)Lcom/twitter/network/g;

    move-result-object v0

    goto :goto_0

    .line 135
    :pswitch_2
    new-instance v0, Lcom/twitter/library/network/v$b;

    invoke-direct {v0, p0}, Lcom/twitter/library/network/v$b;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 138
    :pswitch_3
    new-instance v0, Lcom/twitter/library/network/v$c;

    invoke-direct {v0, p0}, Lcom/twitter/library/network/v$c;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 130
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lcom/twitter/library/network/l;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/twitter/library/network/l;->e()V

    return-void
.end method

.method private static a(Lcom/twitter/network/g;)V
    .locals 2

    .prologue
    .line 232
    new-instance v0, Lcom/twitter/library/network/l$a;

    invoke-direct {v0, p0}, Lcom/twitter/library/network/l$a;-><init>(Lcom/twitter/network/g;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/l$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 233
    return-void
.end method

.method private declared-synchronized d()V
    .locals 1

    .prologue
    .line 122
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/l;->d:Lcom/twitter/network/g;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/twitter/library/network/l;->d:Lcom/twitter/network/g;

    invoke-static {v0}, Lcom/twitter/library/network/l;->a(Lcom/twitter/network/g;)V

    .line 125
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/network/l;->d:Lcom/twitter/network/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    monitor-exit p0

    return-void

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private e()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/16 v3, 0xbb8

    .line 169
    const-string/jumbo v0, "android_network_stack_chooser_5415"

    invoke-static {v0}, Lcoi;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 173
    invoke-static {v0}, Lcoi;->g(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 175
    const-string/jumbo v1, "okhttp1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 176
    const-string/jumbo v2, "okhttp3"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 183
    :goto_0
    invoke-static {}, Lcom/twitter/network/f;->f()I

    move-result v6

    .line 184
    invoke-static {}, Lcom/twitter/network/f;->g()I

    move-result v7

    .line 188
    const-string/jumbo v2, "android_network_connect_timeout_ms"

    invoke-static {v2, v6}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v2

    .line 189
    if-ge v2, v3, :cond_0

    move v2, v3

    .line 192
    :cond_0
    const-string/jumbo v5, "android_network_read_timeout_ms"

    invoke-static {v5, v7}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v5

    .line 193
    if-ge v5, v3, :cond_1

    move v5, v3

    .line 197
    :cond_1
    const/4 v3, 0x0

    .line 198
    if-ne v2, v6, :cond_2

    if-eq v5, v7, :cond_6

    .line 199
    :cond_2
    invoke-static {v2, v5}, Lcom/twitter/network/f;->a(II)V

    move v2, v4

    .line 204
    :goto_1
    invoke-static {v0, v1}, Lcom/twitter/library/network/l;->a(ZZ)I

    move-result v0

    .line 205
    iget-object v1, p0, Lcom/twitter/library/network/l;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndSet(I)I

    move-result v1

    if-eq v1, v0, :cond_5

    .line 209
    :goto_2
    if-eqz v4, :cond_3

    .line 210
    invoke-direct {p0}, Lcom/twitter/library/network/l;->d()V

    .line 212
    :cond_3
    return-void

    .line 179
    :cond_4
    const-string/jumbo v0, "spdy_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    .line 180
    const-string/jumbo v0, "android_network_okhttp3_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :cond_5
    move v4, v2

    goto :goto_2

    :cond_6
    move v2, v3

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized a()Lcom/twitter/network/g;
    .locals 3

    .prologue
    .line 97
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/l;->e:Lcom/twitter/network/g;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/twitter/library/network/l;->e:Lcom/twitter/network/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    :goto_0
    monitor-exit p0

    return-object v0

    .line 99
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/twitter/library/network/l;->d:Lcom/twitter/network/g;

    if-nez v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/twitter/library/network/l;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/library/network/l;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/library/network/l;->a(Landroid/content/Context;I)Lcom/twitter/network/g;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/l;->d:Lcom/twitter/network/g;

    .line 101
    invoke-static {}, Lcqj;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 102
    const-string/jumbo v0, "TwitterNetwork"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Default HttpOperationClientFactory set to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/network/l;->d:Lcom/twitter/network/g;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/network/l;->d:Lcom/twitter/network/g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 1

    .prologue
    .line 117
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/twitter/library/network/l;->e()V

    .line 118
    invoke-direct {p0}, Lcom/twitter/library/network/l;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    monitor-exit p0

    return-void

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
