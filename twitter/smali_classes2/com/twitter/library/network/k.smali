.class public final Lcom/twitter/library/network/k;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Ljava/net/URI;

.field private final c:Lcom/twitter/library/network/i;

.field private d:Lcom/twitter/network/HttpOperation$RequestMethod;

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Lcom/twitter/network/apache/e;

.field private i:Lcom/twitter/network/j;

.field private j:Lcom/twitter/library/network/a;

.field private k:Lcom/twitter/network/c;

.field private l:Lcom/twitter/async/service/b;

.field private m:I

.field private n:Z

.field private o:Z

.field private p:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 75
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/ac;->a(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/network/k;-><init>(Landroid/content/Context;Ljava/net/URI;)V

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/net/URI;)V
    .locals 1

    .prologue
    .line 71
    invoke-static {}, Lbms;->ah()Lbms;

    move-result-object v0

    invoke-virtual {v0}, Lbms;->N()Lcom/twitter/library/network/i;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/network/k;-><init>(Landroid/content/Context;Ljava/net/URI;Lcom/twitter/library/network/i;)V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/net/URI;Lcom/twitter/library/network/i;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    sget-object v0, Lcom/twitter/network/HttpOperation$RequestMethod;->a:Lcom/twitter/network/HttpOperation$RequestMethod;

    iput-object v0, p0, Lcom/twitter/library/network/k;->d:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 50
    iput-boolean v1, p0, Lcom/twitter/library/network/k;->f:Z

    .line 52
    iput-boolean v1, p0, Lcom/twitter/library/network/k;->g:Z

    .line 53
    iput-object v2, p0, Lcom/twitter/library/network/k;->h:Lcom/twitter/network/apache/e;

    .line 54
    iput-object v2, p0, Lcom/twitter/library/network/k;->i:Lcom/twitter/network/j;

    .line 55
    iput-object v2, p0, Lcom/twitter/library/network/k;->j:Lcom/twitter/library/network/a;

    .line 59
    iput-boolean v1, p0, Lcom/twitter/library/network/k;->n:Z

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/network/k;->o:Z

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/k;->a:Landroid/content/Context;

    .line 66
    iput-object p2, p0, Lcom/twitter/library/network/k;->b:Ljava/net/URI;

    .line 67
    iput-object p3, p0, Lcom/twitter/library/network/k;->c:Lcom/twitter/library/network/i;

    .line 68
    return-void
.end method

.method static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 187
    invoke-static {p0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :cond_0
    return-void
.end method

.method private b()Lcom/twitter/network/HttpOperation;
    .locals 5

    .prologue
    .line 346
    invoke-static {}, Lcom/twitter/network/i;->c()Lcom/twitter/network/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/network/i;->a()Lcom/twitter/network/g;

    move-result-object v1

    .line 349
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/k;->c:Lcom/twitter/library/network/i;

    iget-object v2, p0, Lcom/twitter/library/network/k;->b:Ljava/net/URI;

    invoke-interface {v0, v2}, Lcom/twitter/library/network/i;->a(Ljava/net/URI;)Lcom/twitter/library/network/ad;

    move-result-object v2

    .line 350
    iget-object v0, v2, Lcom/twitter/library/network/ad;->a:Ljava/net/URI;

    iput-object v0, p0, Lcom/twitter/library/network/k;->b:Ljava/net/URI;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 356
    iget-object v0, p0, Lcom/twitter/library/network/k;->d:Lcom/twitter/network/HttpOperation$RequestMethod;

    iget-object v3, p0, Lcom/twitter/library/network/k;->b:Ljava/net/URI;

    .line 357
    invoke-virtual {v1, v0, v3}, Lcom/twitter/network/g;->a(Lcom/twitter/network/HttpOperation$RequestMethod;Ljava/net/URI;)Lcom/twitter/network/e;

    move-result-object v0

    .line 358
    iget-object v1, p0, Lcom/twitter/library/network/k;->d:Lcom/twitter/network/HttpOperation$RequestMethod;

    iget-object v3, p0, Lcom/twitter/library/network/k;->b:Ljava/net/URI;

    iget-object v4, p0, Lcom/twitter/library/network/k;->i:Lcom/twitter/network/j;

    invoke-virtual {v0, v1, v3, v4}, Lcom/twitter/network/e;->a(Lcom/twitter/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/network/j;)Lcom/twitter/network/HttpOperation;

    move-result-object v0

    .line 359
    iget v1, p0, Lcom/twitter/library/network/k;->m:I

    if-lez v1, :cond_0

    .line 360
    iget v1, p0, Lcom/twitter/library/network/k;->m:I

    invoke-virtual {v0, v1}, Lcom/twitter/network/HttpOperation;->a(I)V

    .line 362
    :cond_0
    iget-object v1, v2, Lcom/twitter/library/network/ad;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/network/HttpOperation;->a(Ljava/lang/String;)V

    .line 363
    :goto_0
    return-object v0

    .line 351
    :catch_0
    move-exception v0

    .line 352
    const-string/jumbo v2, "TwitterNetwork"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/library/network/k;->b:Ljava/net/URI;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "] Failed to rewrite host"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcqj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 353
    iget-object v2, p0, Lcom/twitter/library/network/k;->d:Lcom/twitter/network/HttpOperation$RequestMethod;

    iget-object v3, p0, Lcom/twitter/library/network/k;->b:Ljava/net/URI;

    iget-object v4, p0, Lcom/twitter/library/network/k;->i:Lcom/twitter/network/j;

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/network/g;->a(Lcom/twitter/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/network/j;)Lcom/twitter/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/network/HttpOperation;->a(Ljava/lang/Exception;)Lcom/twitter/network/HttpOperation;

    move-result-object v0

    goto :goto_0
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/twitter/library/network/k;->j:Lcom/twitter/library/network/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(I)Lcom/twitter/library/network/k;
    .locals 0

    .prologue
    .line 270
    iput p1, p0, Lcom/twitter/library/network/k;->m:I

    .line 271
    return-object p0
.end method

.method public a(J)Lcom/twitter/library/network/k;
    .locals 3

    .prologue
    .line 253
    new-instance v0, Lcom/twitter/library/network/d;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/network/d;-><init>(ZLjava/lang/String;)V

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/library/network/k;->a(JLcom/twitter/library/network/c;)Lcom/twitter/library/network/k;

    move-result-object v0

    return-object v0
.end method

.method public a(JLcom/twitter/library/network/c;)Lcom/twitter/library/network/k;
    .locals 3

    .prologue
    .line 264
    new-instance v0, Lcom/twitter/library/network/j;

    iget-object v1, p0, Lcom/twitter/library/network/k;->a:Landroid/content/Context;

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/twitter/library/network/j;-><init>(JLcom/twitter/library/network/c;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/library/network/k;->k:Lcom/twitter/network/c;

    .line 265
    return-object p0
.end method

.method public a(Lcom/twitter/async/service/b;)Lcom/twitter/library/network/k;
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lcom/twitter/library/network/k;->l:Lcom/twitter/async/service/b;

    .line 242
    return-object p0
.end method

.method public a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/k;
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/twitter/library/network/k;->j:Lcom/twitter/library/network/a;

    .line 99
    return-object p0
.end method

.method public a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/k;
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/twitter/library/network/k;->d:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 87
    return-object p0
.end method

.method public a(Lcom/twitter/network/apache/e;)Lcom/twitter/library/network/k;
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/twitter/library/network/k;->h:Lcom/twitter/network/apache/e;

    .line 201
    return-object p0
.end method

.method public a(Lcom/twitter/network/j;)Lcom/twitter/library/network/k;
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/twitter/library/network/k;->i:Lcom/twitter/network/j;

    .line 232
    return-object p0
.end method

.method public a(Lcom/twitter/util/q;)Lcom/twitter/library/network/k;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/q",
            "<",
            "Ljava/lang/Double;",
            ">;)",
            "Lcom/twitter/library/network/k;"
        }
    .end annotation

    .prologue
    .line 282
    iput-object p1, p0, Lcom/twitter/library/network/k;->p:Lcom/twitter/util/q;

    .line 283
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/network/k;
    .locals 1

    .prologue
    .line 143
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    const-string/jumbo v0, "Cannot force polling without a reason"

    invoke-static {p1, v0}, Lcom/twitter/library/network/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/network/k;->e:Z

    .line 147
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/library/network/k;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/twitter/network/apache/f;",
            ">;)",
            "Lcom/twitter/library/network/k;"
        }
    .end annotation

    .prologue
    .line 212
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    invoke-static {p1}, Lcom/twitter/library/util/af;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 214
    if-eqz v0, :cond_0

    .line 215
    new-instance v1, Lcom/twitter/network/apache/entity/c;

    sget-object v2, Lcom/twitter/network/apache/a;->a:Ljava/nio/charset/Charset;

    invoke-direct {v1, v0, v2}, Lcom/twitter/network/apache/entity/c;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V

    .line 216
    const-string/jumbo v0, "application/x-www-form-urlencoded"

    invoke-virtual {v1, v0}, Lcom/twitter/network/apache/entity/c;->a(Ljava/lang/String;)V

    .line 217
    iput-object v1, p0, Lcom/twitter/library/network/k;->h:Lcom/twitter/network/apache/e;

    .line 220
    :cond_0
    return-object p0
.end method

.method public a(Z)Lcom/twitter/library/network/k;
    .locals 0

    .prologue
    .line 276
    iput-boolean p1, p0, Lcom/twitter/library/network/k;->n:Z

    .line 277
    return-object p0
.end method

.method public a()Lcom/twitter/network/HttpOperation;
    .locals 3

    .prologue
    .line 294
    invoke-direct {p0}, Lcom/twitter/library/network/k;->b()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    .line 295
    iget-object v1, p0, Lcom/twitter/library/network/k;->p:Lcom/twitter/util/q;

    invoke-virtual {v0, v1}, Lcom/twitter/network/HttpOperation;->a(Lcom/twitter/util/q;)V

    .line 297
    iget-boolean v1, p0, Lcom/twitter/library/network/k;->f:Z

    if-eqz v1, :cond_0

    .line 298
    iget-object v1, p0, Lcom/twitter/library/network/k;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/network/ab;->a(Landroid/content/Context;)Lcom/twitter/library/network/ab;

    move-result-object v1

    .line 299
    iget-boolean v2, p0, Lcom/twitter/library/network/k;->e:Z

    if-eqz v2, :cond_2

    .line 300
    invoke-virtual {v1, v0}, Lcom/twitter/library/network/ab;->c(Lcom/twitter/network/HttpOperation;)V

    .line 306
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/twitter/library/network/k;->g:Z

    if-eqz v1, :cond_1

    .line 307
    const-string/jumbo v1, "Cache-Control"

    const-string/jumbo v2, "no-store"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/network/HttpOperation;

    .line 310
    :cond_1
    iget-object v1, p0, Lcom/twitter/library/network/k;->h:Lcom/twitter/network/apache/e;

    if-eqz v1, :cond_4

    .line 311
    iget-object v1, p0, Lcom/twitter/library/network/k;->d:Lcom/twitter/network/HttpOperation$RequestMethod;

    invoke-virtual {v1}, Lcom/twitter/network/HttpOperation$RequestMethod;->a()Z

    move-result v1

    if-nez v1, :cond_3

    .line 312
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "The RequestMethod "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/network/k;->d:Lcom/twitter/network/HttpOperation$RequestMethod;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " does not allow a request entity."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 302
    :cond_2
    invoke-virtual {v1, v0}, Lcom/twitter/library/network/ab;->b(Lcom/twitter/network/HttpOperation;)V

    goto :goto_0

    .line 315
    :cond_3
    iget-object v1, p0, Lcom/twitter/library/network/k;->h:Lcom/twitter/network/apache/e;

    invoke-virtual {v0, v1}, Lcom/twitter/network/HttpOperation;->a(Lcom/twitter/network/apache/e;)Lcom/twitter/network/HttpOperation;

    .line 318
    :cond_4
    invoke-static {}, Lcqj;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 319
    new-instance v1, Lcom/twitter/network/h;

    invoke-direct {v1}, Lcom/twitter/network/h;-><init>()V

    invoke-virtual {v0, v1}, Lcom/twitter/network/HttpOperation;->a(Lcom/twitter/network/c;)Lcom/twitter/network/HttpOperation;

    .line 321
    :cond_5
    invoke-direct {p0}, Lcom/twitter/library/network/k;->c()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 322
    new-instance v1, Lcom/twitter/library/network/u;

    iget-object v2, p0, Lcom/twitter/library/network/k;->j:Lcom/twitter/library/network/a;

    invoke-direct {v1, v2}, Lcom/twitter/library/network/u;-><init>(Lcom/twitter/library/network/a;)V

    invoke-virtual {v0, v1}, Lcom/twitter/network/HttpOperation;->a(Lcom/twitter/network/c;)Lcom/twitter/network/HttpOperation;

    .line 326
    :cond_6
    new-instance v1, Lcom/twitter/library/network/narc/d;

    iget-object v2, p0, Lcom/twitter/library/network/k;->l:Lcom/twitter/async/service/b;

    invoke-direct {v1, v2}, Lcom/twitter/library/network/narc/d;-><init>(Lcom/twitter/async/service/b;)V

    .line 327
    invoke-virtual {v0, v1}, Lcom/twitter/network/HttpOperation;->a(Lcom/twitter/network/c;)Lcom/twitter/network/HttpOperation;

    .line 329
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->p()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 330
    iget-boolean v1, p0, Lcom/twitter/library/network/k;->n:Z

    invoke-virtual {v0, v1}, Lcom/twitter/network/HttpOperation;->a(Z)V

    .line 333
    :cond_7
    iget-object v1, p0, Lcom/twitter/library/network/k;->k:Lcom/twitter/network/c;

    if-eqz v1, :cond_8

    .line 334
    iget-object v1, p0, Lcom/twitter/library/network/k;->k:Lcom/twitter/network/c;

    invoke-virtual {v0, v1}, Lcom/twitter/network/HttpOperation;->a(Lcom/twitter/network/c;)Lcom/twitter/network/HttpOperation;

    .line 339
    :goto_1
    iget-boolean v1, p0, Lcom/twitter/library/network/k;->o:Z

    invoke-virtual {v0, v1}, Lcom/twitter/network/HttpOperation;->b(Z)V

    .line 341
    return-object v0

    .line 336
    :cond_8
    new-instance v1, Lcom/twitter/library/network/j;

    iget-object v2, p0, Lcom/twitter/library/network/k;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/twitter/library/network/j;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/twitter/network/HttpOperation;->a(Lcom/twitter/network/c;)Lcom/twitter/network/HttpOperation;

    goto :goto_1
.end method

.method public b(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/k;
    .locals 1

    .prologue
    .line 111
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/network/a;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 112
    iput-object p1, p0, Lcom/twitter/library/network/k;->j:Lcom/twitter/library/network/a;

    .line 114
    :cond_0
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/library/network/k;
    .locals 1

    .prologue
    .line 176
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    const-string/jumbo v0, "Cannot force non-polling without a reason"

    invoke-static {p1, v0}, Lcom/twitter/library/network/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/network/k;->e:Z

    .line 180
    return-object p0
.end method

.method public b(Z)Lcom/twitter/library/network/k;
    .locals 0

    .prologue
    .line 288
    iput-boolean p1, p0, Lcom/twitter/library/network/k;->o:Z

    .line 289
    return-object p0
.end method

.method public c(Z)Lcom/twitter/library/network/k;
    .locals 0

    .prologue
    .line 376
    iput-boolean p1, p0, Lcom/twitter/library/network/k;->f:Z

    .line 377
    return-object p0
.end method

.method public d(Z)Lcom/twitter/library/network/k;
    .locals 0

    .prologue
    .line 385
    iput-boolean p1, p0, Lcom/twitter/library/network/k;->g:Z

    .line 386
    return-object p0
.end method
