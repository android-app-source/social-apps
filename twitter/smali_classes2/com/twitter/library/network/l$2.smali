.class Lcom/twitter/library/network/l$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/network/l;-><init>(Landroid/content/Context;Lcom/twitter/util/connectivity/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/q",
        "<",
        "Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/network/l;

.field private b:Z


# direct methods
.method constructor <init>(Lcom/twitter/library/network/l;)V
    .locals 1

    .prologue
    .line 79
    iput-object p1, p0, Lcom/twitter/library/network/l$2;->a:Lcom/twitter/library/network/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/network/l$2;->b:Z

    return-void
.end method


# virtual methods
.method public onEvent(Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;)V
    .locals 2

    .prologue
    .line 84
    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;

    invoke-interface {v0}, Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;->b()Z

    move-result v0

    .line 85
    iget-boolean v1, p0, Lcom/twitter/library/network/l$2;->b:Z

    if-eq v1, v0, :cond_0

    .line 86
    iput-boolean v0, p0, Lcom/twitter/library/network/l$2;->b:Z

    .line 87
    iget-object v0, p0, Lcom/twitter/library/network/l$2;->a:Lcom/twitter/library/network/l;

    invoke-virtual {v0}, Lcom/twitter/library/network/l;->a()Lcom/twitter/network/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/network/g;->b()V

    .line 89
    :cond_0
    return-void
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 79
    check-cast p1, Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;

    invoke-virtual {p0, p1}, Lcom/twitter/library/network/l$2;->onEvent(Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;)V

    return-void
.end method
