.class Lcom/twitter/library/network/livepipeline/LivePipeline$13$1;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a(Lcom/twitter/model/livepipeline/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Lcom/twitter/model/livepipeline/d;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/network/livepipeline/LivePipeline$13;


# direct methods
.method constructor <init>(Lcom/twitter/library/network/livepipeline/LivePipeline$13;)V
    .locals 0

    .prologue
    .line 330
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13$1;->a:Lcom/twitter/library/network/livepipeline/LivePipeline$13;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/livepipeline/d;)V
    .locals 4

    .prologue
    .line 333
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13$1;->a:Lcom/twitter/library/network/livepipeline/LivePipeline$13;

    iget-object v0, v0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-static {v0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->b(Lcom/twitter/library/network/livepipeline/LivePipeline;)Lrx/subjects/PublishSubject;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13$1;->a:Lcom/twitter/library/network/livepipeline/LivePipeline$13;

    iget-object v0, v0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-static {v0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->b(Lcom/twitter/library/network/livepipeline/LivePipeline;)Lrx/subjects/PublishSubject;

    move-result-object v0

    invoke-virtual {v0}, Lrx/subjects/PublishSubject;->s()Z

    move-result v0

    if-nez v0, :cond_0

    .line 334
    const-string/jumbo v0, "LivePipeline"

    const-string/jumbo v1, "Timeout reached without activity. Reset stream."

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13$1;->a:Lcom/twitter/library/network/livepipeline/LivePipeline$13;

    iget-object v0, v0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    sget-object v1, Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;->b:Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;

    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13$1;->a:Lcom/twitter/library/network/livepipeline/LivePipeline$13;

    iget-object v2, v2, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Lcom/twitter/async/service/AsyncOperation;)Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;)V

    .line 337
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 330
    check-cast p1, Lcom/twitter/model/livepipeline/d;

    invoke-virtual {p0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline$13$1;->a(Lcom/twitter/model/livepipeline/d;)V

    return-void
.end method
