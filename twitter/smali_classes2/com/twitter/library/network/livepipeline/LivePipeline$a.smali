.class abstract Lcom/twitter/library/network/livepipeline/LivePipeline$a;
.super Lcom/twitter/library/network/livepipeline/a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/network/livepipeline/LivePipeline;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "a"
.end annotation


# instance fields
.field protected final a:Ljava/util/concurrent/atomic/AtomicLong;

.field protected b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected c:J

.field protected d:J

.field protected e:J

.field protected f:J

.field protected g:J

.field final synthetic h:Lcom/twitter/library/network/livepipeline/LivePipeline;


# direct methods
.method protected constructor <init>(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/lang/String;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const-wide/16 v2, -0x1

    .line 763
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->h:Lcom/twitter/library/network/livepipeline/LivePipeline;

    .line 764
    invoke-static {p1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->d(Lcom/twitter/library/network/livepipeline/LivePipeline;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/twitter/library/network/livepipeline/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 757
    iput-wide v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->c:J

    .line 758
    iput-wide v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->d:J

    .line 759
    iput-wide v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->e:J

    .line 760
    iput-wide v4, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->f:J

    .line 761
    iput-wide v4, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->g:J

    .line 765
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->a:Ljava/util/concurrent/atomic/AtomicLong;

    .line 766
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->b:Ljava/util/Set;

    .line 767
    return-void
.end method


# virtual methods
.method public a()J
    .locals 4

    .prologue
    .line 784
    iget-wide v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->c:J

    iget-wide v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->j:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(J)V
    .locals 5

    .prologue
    .line 770
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->a:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 771
    iput-wide p1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->c:J

    .line 779
    :cond_0
    :goto_0
    iput-wide p1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->d:J

    .line 780
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->a:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 781
    return-void

    .line 772
    :cond_1
    iget-wide v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->d:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 773
    iget-wide v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->d:J

    invoke-static {p1, p2, v0, v1}, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->a(JJ)J

    move-result-wide v0

    .line 774
    iget-wide v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->e:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 775
    iput-wide v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->e:J

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 796
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()J
    .locals 4

    .prologue
    .line 788
    iget-wide v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->d:J

    iget-wide v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->j:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public b(J)V
    .locals 3

    .prologue
    .line 808
    iget-wide v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->f:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->f:J

    .line 809
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 800
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 801
    return-void
.end method

.method public c()J
    .locals 4

    .prologue
    .line 792
    iget-wide v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->k:J

    iget-wide v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->d:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public d()V
    .locals 4

    .prologue
    .line 804
    iget-wide v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->g:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->g:J

    .line 805
    return-void
.end method

.method protected f()Lcom/twitter/util/collection/i;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/collection/i",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 813
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->a:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    .line 814
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->k()J

    move-result-wide v0

    div-long/2addr v0, v2

    .line 816
    :goto_0
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v4

    const-string/jumbo v5, "time_to_first_event"

    .line 817
    invoke-virtual {p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v4

    const-string/jumbo v5, "time_to_last_event"

    .line 818
    invoke-virtual {p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->b()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v4

    const-string/jumbo v5, "final_idle_time"

    .line 819
    invoke-virtual {p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->c()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v4

    const-string/jumbo v5, "total_events"

    .line 820
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v2

    const-string/jumbo v3, "mean_time_between_events"

    .line 821
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "total_unique_topics"

    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->b:Ljava/util/Set;

    .line 822
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "total_subscriptions"

    iget-wide v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->g:J

    .line 823
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "total_bytes"

    iget-wide v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->f:J

    .line 824
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "max_time_between_events"

    iget-wide v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$a;->e:J

    .line 825
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 816
    return-object v0

    .line 814
    :cond_0
    const-wide/16 v0, -0x1

    goto/16 :goto_0
.end method
