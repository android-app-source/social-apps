.class public Lcom/twitter/library/network/livepipeline/d$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/network/livepipeline/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public a:Ljava/lang/Object;

.field public b:Lcom/twitter/model/livepipeline/PipelineEventType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/livepipeline/PipelineEventType;)Lcom/twitter/library/network/livepipeline/d$a;
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/d$a;->b:Lcom/twitter/model/livepipeline/PipelineEventType;

    .line 36
    return-object p0
.end method

.method public a(Ljava/lang/Object;)Lcom/twitter/library/network/livepipeline/d$a;
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/d$a;->a:Ljava/lang/Object;

    .line 28
    return-object p0
.end method

.method public a()Lcom/twitter/library/network/livepipeline/d;
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/d$a;->a:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/d$a;->b:Lcom/twitter/model/livepipeline/PipelineEventType;

    if-nez v0, :cond_1

    .line 42
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "A subscription must contain a non-null topic and type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_1
    new-instance v0, Lcom/twitter/library/network/livepipeline/d;

    invoke-direct {v0, p0}, Lcom/twitter/library/network/livepipeline/d;-><init>(Lcom/twitter/library/network/livepipeline/d$a;)V

    return-object v0
.end method
