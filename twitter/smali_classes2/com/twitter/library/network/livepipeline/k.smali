.class abstract Lcom/twitter/library/network/livepipeline/k;
.super Lcom/twitter/library/network/livepipeline/j;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/network/livepipeline/k$a;
    }
.end annotation


# instance fields
.field private final b:Lcom/twitter/library/network/livepipeline/f;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;Lcom/twitter/library/network/livepipeline/f;JLjava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/twitter/library/client/Session;",
            "Lcom/twitter/library/network/livepipeline/f;",
            "J",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-static {p5, p6}, Lcom/twitter/library/network/livepipeline/j;->a(J)Lcom/twitter/async/service/k;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/library/network/livepipeline/j;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;Lcom/twitter/async/service/k;)V

    .line 40
    iput-object p4, p0, Lcom/twitter/library/network/livepipeline/k;->b:Lcom/twitter/library/network/livepipeline/f;

    .line 41
    iput-object p7, p0, Lcom/twitter/library/network/livepipeline/k;->c:Ljava/util/Set;

    .line 42
    return-void
.end method

.method protected static c(Lcom/twitter/library/service/u;)Z
    .locals 2

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    const/16 v1, 0x190

    if-ne v0, v1, :cond_0

    const/16 v0, 0x188

    .line 102
    invoke-static {p0, v0}, Lcom/twitter/library/network/ab;->a(Lcom/twitter/library/service/u;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 101
    :goto_0
    return v0

    .line 102
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 5

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/twitter/library/network/livepipeline/k;->b()Ljava/lang/String;

    move-result-object v0

    .line 53
    invoke-virtual {p0}, Lcom/twitter/library/network/livepipeline/k;->u()Ljava/lang/String;

    move-result-object v1

    .line 54
    invoke-virtual {p2}, Lcom/twitter/library/service/u;->b()Z

    move-result v2

    if-nez v2, :cond_1

    .line 55
    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/k;->c:Ljava/util/Set;

    invoke-virtual {p0, v2}, Lcom/twitter/library/network/livepipeline/k;->b(Ljava/util/Collection;)V

    .line 56
    const-string/jumbo v2, "LivePipeline"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Operation "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " failed for topics: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string/jumbo v0, "LivePipeline"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Response Status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/twitter/library/service/u;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string/jumbo v0, "LivePipeline"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/twitter/library/service/u;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-static {p2}, Lcom/twitter/library/network/livepipeline/k;->c(Lcom/twitter/library/service/u;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    invoke-static {}, Lcom/twitter/library/network/livepipeline/b;->a()Lcom/twitter/library/network/livepipeline/b;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;->e:Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;

    sget-object v2, Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;->a:Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/network/livepipeline/b;->a(Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;)V

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/k;->c:Ljava/util/Set;

    invoke-virtual {p0, v2}, Lcom/twitter/library/network/livepipeline/k;->a(Ljava/util/Collection;)V

    .line 66
    const-string/jumbo v2, "LivePipeline"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Operation "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " succeeded for topics: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    return-void
.end method

.method protected b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 97
    const-string/jumbo v0, ","

    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/k;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected b(Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 80
    return-void
.end method

.method protected b(Lcom/twitter/library/service/u;)Z
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/k;->b:Lcom/twitter/library/network/livepipeline/f;

    invoke-interface {v0}, Lcom/twitter/library/network/livepipeline/f;->b()Z

    move-result v0

    return v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/twitter/library/network/livepipeline/k;->s()Lcom/twitter/library/network/livepipeline/k$a;

    move-result-object v0

    return-object v0
.end method

.method protected g()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "LivePipeline-Session"

    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/k;->b:Lcom/twitter/library/network/livepipeline/f;

    .line 86
    invoke-interface {v2}, Lcom/twitter/library/network/livepipeline/f;->a()Ljava/lang/String;

    move-result-object v2

    .line 85
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 85
    return-object v0
.end method

.method protected h()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "topic"

    invoke-virtual {p0}, Lcom/twitter/library/network/livepipeline/k;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method protected s()Lcom/twitter/library/network/livepipeline/k$a;
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lcom/twitter/library/network/livepipeline/k$a;

    invoke-direct {v0}, Lcom/twitter/library/network/livepipeline/k$a;-><init>()V

    return-object v0
.end method
