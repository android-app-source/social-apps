.class Lcom/twitter/library/network/livepipeline/LivePipeline;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/network/livepipeline/LivePipeline$c;,
        Lcom/twitter/library/network/livepipeline/LivePipeline$b;,
        Lcom/twitter/library/network/livepipeline/LivePipeline$a;,
        Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;,
        Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;
    }
.end annotation


# static fields
.field private static final a:J

.field private static final b:J

.field private static final c:J


# instance fields
.field private d:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Lcom/twitter/model/livepipeline/d;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/twitter/library/network/livepipeline/StreamManager;

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/livepipeline/d;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final h:Landroid/content/Context;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/Long;

.field private k:Ljava/lang/Long;

.field private l:Ljava/lang/Long;

.field private final m:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lrx/j;

.field private final p:Lcqt;

.field private final q:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcom/twitter/library/network/livepipeline/LivePipeline$b;

.field private s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 69
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/twitter/library/network/livepipeline/LivePipeline;->a:J

    .line 72
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/twitter/library/network/livepipeline/LivePipeline;->b:J

    .line 75
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/twitter/library/network/livepipeline/LivePipeline;->c:J

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 120
    invoke-direct {p0, p1, v0, v0}, Lcom/twitter/library/network/livepipeline/LivePipeline;-><init>(Landroid/content/Context;Lcom/twitter/library/network/livepipeline/StreamManager;Ljava/lang/Long;)V

    .line 121
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/twitter/library/network/livepipeline/StreamManager;Ljava/lang/Long;)V
    .locals 6

    .prologue
    const/16 v3, 0x10

    const/4 v2, 0x2

    const v1, 0x3f4ccccd    # 0.8f

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    .line 128
    :cond_0
    invoke-static {}, Lcom/twitter/util/f;->d()V

    .line 131
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Landroid/content/Context;Lcom/twitter/library/network/livepipeline/StreamManager;)V

    .line 132
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0, v3, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->f:Ljava/util/Set;

    .line 133
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0, v3, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->g:Ljava/util/concurrent/ConcurrentHashMap;

    .line 134
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->h:Landroid/content/Context;

    .line 135
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->m:Lrx/subjects/PublishSubject;

    .line 136
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->n:Lrx/subjects/PublishSubject;

    .line 138
    new-instance v2, Lcom/twitter/library/network/livepipeline/LivePipeline$1;

    invoke-direct {v2, p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$1;-><init>(Lcom/twitter/library/network/livepipeline/LivePipeline;)V

    .line 151
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 157
    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->m:Lrx/subjects/PublishSubject;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(J)Lrx/functions/d;

    move-result-object v0

    invoke-virtual {v1, v0}, Lrx/subjects/PublishSubject;->i(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/livepipeline/LivePipeline$9;

    invoke-direct {v1, p0, v2}, Lcom/twitter/library/network/livepipeline/LivePipeline$9;-><init>(Lcom/twitter/library/network/livepipeline/LivePipeline;Lcom/twitter/library/network/livepipeline/f;)V

    .line 158
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 174
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_1
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 175
    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->n:Lrx/subjects/PublishSubject;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(J)Lrx/functions/d;

    move-result-object v0

    invoke-virtual {v1, v0}, Lrx/subjects/PublishSubject;->i(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/livepipeline/LivePipeline$10;

    invoke-direct {v1, p0, v2}, Lcom/twitter/library/network/livepipeline/LivePipeline$10;-><init>(Lcom/twitter/library/network/livepipeline/LivePipeline;Lcom/twitter/library/network/livepipeline/f;)V

    .line 176
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 192
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->a()Lcqt;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->p:Lcqt;

    .line 194
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/livepipeline/LivePipeline$11;

    invoke-direct {v1, p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$11;-><init>(Lcom/twitter/library/network/livepipeline/LivePipeline;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/u;)V

    .line 201
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->q:Ljava/util/Set;

    .line 202
    return-void

    .line 151
    :cond_2
    const-wide/16 v0, 0x64

    goto :goto_0

    .line 174
    :cond_3
    const-wide/16 v0, 0x3e8

    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->j:Ljava/lang/Long;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/library/network/livepipeline/LivePipeline;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->i:Ljava/lang/String;

    return-object p1
.end method

.method protected static a(J)Lrx/functions/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/functions/d",
            "<",
            "Lrx/c",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 288
    new-instance v0, Lcom/twitter/library/network/livepipeline/LivePipeline$12;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline$12;-><init>(J)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/library/network/livepipeline/LivePipeline;Lrx/j;)Lrx/j;
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->o:Lrx/j;

    return-object p1
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 661
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    if-eqz v0, :cond_0

    .line 662
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    invoke-virtual {v0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->a(I)V

    .line 664
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/twitter/library/network/livepipeline/StreamManager;)V
    .locals 2

    .prologue
    .line 304
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->d:Lrx/subjects/PublishSubject;

    .line 305
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->d:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/twitter/library/network/livepipeline/LivePipeline$14;

    invoke-direct {v1, p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$14;-><init>(Lcom/twitter/library/network/livepipeline/LivePipeline;)V

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    const-class v1, Lcom/twitter/model/livepipeline/a;

    .line 310
    invoke-virtual {v0, v1}, Lrx/c;->a(Ljava/lang/Class;)Lrx/c;

    move-result-object v0

    invoke-virtual {v0}, Lrx/c;->n()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/livepipeline/LivePipeline$13;

    invoke-direct {v1, p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$13;-><init>(Lcom/twitter/library/network/livepipeline/LivePipeline;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 352
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->d:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/twitter/library/network/livepipeline/LivePipeline$16;

    invoke-direct {v1, p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$16;-><init>(Lcom/twitter/library/network/livepipeline/LivePipeline;)V

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    const-class v1, Lcom/twitter/model/livepipeline/f;

    .line 357
    invoke-virtual {v0, v1}, Lrx/c;->a(Ljava/lang/Class;)Lrx/c;

    move-result-object v0

    invoke-virtual {v0}, Lrx/c;->n()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/livepipeline/LivePipeline$15;

    invoke-direct {v1, p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$15;-><init>(Lcom/twitter/library/network/livepipeline/LivePipeline;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 364
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->d:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/twitter/library/network/livepipeline/LivePipeline$3;

    invoke-direct {v1, p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$3;-><init>(Lcom/twitter/library/network/livepipeline/LivePipeline;)V

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/livepipeline/LivePipeline$2;

    invoke-direct {v1, p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$2;-><init>(Lcom/twitter/library/network/livepipeline/LivePipeline;)V

    .line 370
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 377
    if-eqz p2, :cond_0

    .line 378
    :goto_0
    iput-object p2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->e:Lcom/twitter/library/network/livepipeline/StreamManager;

    .line 379
    return-void

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->d:Lrx/subjects/PublishSubject;

    .line 378
    invoke-static {v0, p1}, Lcom/twitter/library/network/livepipeline/StreamManager;->a(Lrx/d;Landroid/content/Context;)Lcom/twitter/library/network/livepipeline/StreamManager;

    move-result-object p2

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/library/network/livepipeline/LivePipeline;I)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/network/livepipeline/LivePipeline;J)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/network/livepipeline/LivePipeline;->b(J)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->c(Ljava/util/Set;)V

    return-void
.end method

.method private a(Ljava/util/Set;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 431
    monitor-enter p0

    .line 432
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->e:Lcom/twitter/library/network/livepipeline/StreamManager;

    invoke-virtual {v0}, Lcom/twitter/library/network/livepipeline/StreamManager;->a()Lcom/twitter/library/network/livepipeline/StreamManager$Status;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/network/livepipeline/StreamManager$Status;->a:Lcom/twitter/library/network/livepipeline/StreamManager$Status;

    if-ne v0, v1, :cond_0

    .line 433
    invoke-direct {p0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->b(Ljava/util/Set;)V

    .line 434
    const/4 v0, 0x0

    monitor-exit p0

    .line 439
    :goto_0
    return v0

    .line 436
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 438
    const-string/jumbo v0, "LivePipeline"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Don\'t need to connect: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->e:Lcom/twitter/library/network/livepipeline/StreamManager;

    invoke-virtual {v2}, Lcom/twitter/library/network/livepipeline/StreamManager;->a()Lcom/twitter/library/network/livepipeline/StreamManager$Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    const/4 v0, 0x1

    goto :goto_0

    .line 436
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic b()J
    .locals 2

    .prologue
    .line 51
    sget-wide v0, Lcom/twitter/library/network/livepipeline/LivePipeline;->b:J

    return-wide v0
.end method

.method static synthetic b(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->k:Ljava/lang/Long;

    return-object p1
.end method

.method private b(Ljava/lang/String;)Lrx/c;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/livepipeline/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 386
    const/4 v1, 0x0

    .line 387
    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->g:Ljava/util/concurrent/ConcurrentHashMap;

    monitor-enter v2

    .line 388
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->g:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 389
    if-eqz v0, :cond_1

    .line 390
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/c;

    .line 393
    :goto_0
    if-nez v0, :cond_0

    .line 394
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->d:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/twitter/library/network/livepipeline/LivePipeline$6;

    invoke-direct {v1, p0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline$6;-><init>(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/livepipeline/LivePipeline$5;

    invoke-direct {v1, p0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline$5;-><init>(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/lang/String;)V

    .line 399
    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/a;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/livepipeline/LivePipeline$4;

    invoke-direct {v1, p0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline$4;-><init>(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/lang/String;)V

    .line 406
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/functions/a;)Lrx/c;

    move-result-object v0

    .line 419
    invoke-virtual {v0}, Lrx/c;->m()Lcwb;

    move-result-object v0

    invoke-virtual {v0}, Lcwb;->r()Lrx/c;

    move-result-object v0

    .line 420
    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->g:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, p1, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 422
    :cond_0
    monitor-exit v2

    .line 424
    return-object v0

    .line 422
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/library/network/livepipeline/LivePipeline;)Lrx/subjects/PublishSubject;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->d:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method private b(J)V
    .locals 7

    .prologue
    .line 516
    const-string/jumbo v0, "LivePipeline"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Scheduling resubscribe after "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->k:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "ms at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    iget-object v3, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->p:Lcqt;

    .line 517
    invoke-interface {v3}, Lcqt;->a()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 516
    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->i:Ljava/lang/String;

    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 519
    invoke-virtual {v0, p1, p2, v1}, Lrx/c;->b(JLjava/util/concurrent/TimeUnit;)Lrx/c;

    move-result-object v0

    .line 520
    invoke-static {}, Lcws;->c()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/livepipeline/LivePipeline$8;

    invoke-direct {v1, p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$8;-><init>(Lcom/twitter/library/network/livepipeline/LivePipeline;)V

    .line 521
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 536
    return-void
.end method

.method static synthetic b(Lcom/twitter/library/network/livepipeline/LivePipeline;J)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/network/livepipeline/LivePipeline;->c(J)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->d(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 446
    invoke-virtual {p0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a()V

    .line 447
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->e:Lcom/twitter/library/network/livepipeline/StreamManager;

    new-instance v1, Lcom/twitter/library/network/livepipeline/LivePipeline$7;

    invoke-direct {v1, p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$7;-><init>(Lcom/twitter/library/network/livepipeline/LivePipeline;)V

    .line 464
    invoke-direct {p0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->g()J

    move-result-wide v2

    .line 447
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/twitter/library/network/livepipeline/StreamManager;->a(Ljava/util/Set;Lcom/twitter/async/service/AsyncOperation$b;J)V

    .line 466
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 467
    invoke-direct {p0, v0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 469
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/util/Set;)Z
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Ljava/util/Set;)Z

    move-result v0

    return v0
.end method

.method static synthetic c()J
    .locals 2

    .prologue
    .line 51
    sget-wide v0, Lcom/twitter/library/network/livepipeline/LivePipeline;->c:J

    return-wide v0
.end method

.method private c(Lcom/twitter/async/service/AsyncOperation;)Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/AsyncOperation",
            "<",
            "Landroid/os/Bundle;",
            "Lcom/twitter/library/service/u;",
            ">;)",
            "Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;"
        }
    .end annotation

    .prologue
    .line 671
    monitor-enter p0

    .line 673
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    if-eqz v0, :cond_2

    .line 674
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    invoke-virtual {v0}, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->i()Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;

    move-result-object v0

    if-nez v0, :cond_0

    .line 675
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    invoke-virtual {p0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Lcom/twitter/async/service/AsyncOperation;)Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->a(Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;)V

    .line 677
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    invoke-virtual {v0}, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->i()Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;

    move-result-object v0

    .line 678
    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    invoke-virtual {v1}, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->j()V

    .line 679
    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->r:Lcom/twitter/library/network/livepipeline/LivePipeline$b;

    if-eqz v1, :cond_1

    .line 680
    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->r:Lcom/twitter/library/network/livepipeline/LivePipeline$b;

    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    .line 681
    invoke-virtual {v2}, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->k()J

    move-result-wide v2

    iget-object v4, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    invoke-virtual {v4}, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->h()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 680
    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->c(J)V

    .line 683
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    .line 688
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->i:Ljava/lang/String;

    .line 690
    monitor-exit p0

    return-object v0

    .line 685
    :cond_2
    invoke-virtual {p0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Lcom/twitter/async/service/AsyncOperation;)Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;

    move-result-object v0

    goto :goto_0

    .line 691
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic c(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->l:Ljava/lang/Long;

    return-object p1
.end method

.method static synthetic c(Lcom/twitter/library/network/livepipeline/LivePipeline;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->f:Ljava/util/Set;

    return-object v0
.end method

.method private c(J)V
    .locals 1

    .prologue
    .line 651
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->r:Lcom/twitter/library/network/livepipeline/LivePipeline$b;

    if-eqz v0, :cond_0

    .line 652
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->r:Lcom/twitter/library/network/livepipeline/LivePipeline$b;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->b(J)V

    .line 655
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    if-eqz v0, :cond_1

    .line 656
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->b(J)V

    .line 658
    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->e(Ljava/lang/String;)V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 549
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 550
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->n:Lrx/subjects/PublishSubject;

    invoke-virtual {v0, p1}, Lrx/subjects/PublishSubject;->a(Ljava/lang/Object;)V

    .line 552
    :cond_0
    return-void
.end method

.method private c(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 539
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 540
    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->i:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 541
    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->m:Lrx/subjects/PublishSubject;

    invoke-virtual {v2, v0}, Lrx/subjects/PublishSubject;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 543
    :cond_0
    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->q:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 546
    :cond_1
    return-void
.end method

.method static synthetic d(Lcom/twitter/library/network/livepipeline/LivePipeline;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->h:Landroid/content/Context;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 503
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Lcom/twitter/async/service/AsyncOperation;)Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;

    move-result-object v0

    .line 504
    iget-boolean v1, v0, Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;->shouldBeConnected:Z

    if-nez v1, :cond_1

    .line 505
    const-string/jumbo v1, "LivePipeline"

    const-string/jumbo v2, "Terminate stream because it is not required anymore."

    invoke-static {v1, v2}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    sget-object v1, Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;->d:Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;)V

    .line 510
    :cond_0
    :goto_0
    return-void

    .line 507
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->k:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->k:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->b(J)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->c(Ljava/lang/String;)V

    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 601
    monitor-enter p0

    .line 602
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    invoke-virtual {v0}, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->g()V

    .line 604
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    invoke-virtual {v0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->c(Ljava/lang/String;)V

    .line 607
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->r:Lcom/twitter/library/network/livepipeline/LivePipeline$b;

    if-eqz v0, :cond_1

    .line 608
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->r:Lcom/twitter/library/network/livepipeline/LivePipeline$b;

    invoke-virtual {v0}, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->h()V

    .line 610
    :cond_1
    monitor-exit p0

    .line 611
    return-void

    .line 610
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic e(Lcom/twitter/library/network/livepipeline/LivePipeline;)J
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->g()J

    move-result-wide v0

    return-wide v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->f:Ljava/util/Set;

    invoke-direct {p0, v0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->c(Ljava/util/Set;)V

    .line 556
    invoke-direct {p0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->f()V

    .line 557
    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 632
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->r:Lcom/twitter/library/network/livepipeline/LivePipeline$b;

    if-eqz v0, :cond_0

    .line 633
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->r:Lcom/twitter/library/network/livepipeline/LivePipeline$b;

    invoke-virtual {v0}, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->d()V

    .line 634
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->r:Lcom/twitter/library/network/livepipeline/LivePipeline$b;

    invoke-virtual {v0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 635
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->r:Lcom/twitter/library/network/livepipeline/LivePipeline$b;

    invoke-virtual {v0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->b(Ljava/lang/String;)V

    .line 639
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    if-eqz v0, :cond_1

    .line 640
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    invoke-virtual {v0}, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->d()V

    .line 641
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    invoke-virtual {v0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 642
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    invoke-virtual {v0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->b(Ljava/lang/String;)V

    .line 645
    :cond_1
    return-void
.end method

.method static synthetic f(Lcom/twitter/library/network/livepipeline/LivePipeline;)Lrx/j;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->o:Lrx/j;

    return-object v0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 563
    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->g:Ljava/util/concurrent/ConcurrentHashMap;

    monitor-enter v1

    .line 565
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->g:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 566
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 567
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 568
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 569
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 572
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 573
    return-void
.end method

.method private g()J
    .locals 2

    .prologue
    .line 580
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->j:Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->j:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    sget-wide v0, Lcom/twitter/library/network/livepipeline/LivePipeline;->a:J

    goto :goto_0
.end method

.method static synthetic g(Lcom/twitter/library/network/livepipeline/LivePipeline;)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->l:Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/library/network/livepipeline/LivePipeline;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->q:Ljava/util/Set;

    return-object v0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 617
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->p:Lcqt;

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v0

    .line 619
    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->r:Lcom/twitter/library/network/livepipeline/LivePipeline$b;

    if-eqz v2, :cond_0

    .line 620
    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->r:Lcom/twitter/library/network/livepipeline/LivePipeline$b;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->a(J)V

    .line 623
    :cond_0
    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    if-eqz v2, :cond_1

    .line 624
    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->a(J)V

    .line 626
    :cond_1
    return-void
.end method

.method static synthetic i(Lcom/twitter/library/network/livepipeline/LivePipeline;)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->k:Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/library/network/livepipeline/LivePipeline;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->h()V

    return-void
.end method

.method static synthetic k(Lcom/twitter/library/network/livepipeline/LivePipeline;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->e()V

    return-void
.end method

.method static synthetic l(Lcom/twitter/library/network/livepipeline/LivePipeline;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->d()V

    return-void
.end method

.method static synthetic m(Lcom/twitter/library/network/livepipeline/LivePipeline;)Lcqt;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->p:Lcqt;

    return-object v0
.end method


# virtual methods
.method protected a(Lcom/twitter/async/service/AsyncOperation;)Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/AsyncOperation",
            "<",
            "Landroid/os/Bundle;",
            "Lcom/twitter/library/service/u;",
            ">;)",
            "Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;"
        }
    .end annotation

    .prologue
    .line 239
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    const-string/jumbo v0, "LivePipeline"

    const-string/jumbo v1, "Should not be connected because there are no active subscriptions"

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    sget-object v0, Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;->b:Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;

    .line 254
    :goto_0
    return-object v0

    .line 242
    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/twitter/async/service/AsyncOperation;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 243
    invoke-virtual {p1}, Lcom/twitter/async/service/AsyncOperation;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 247
    :cond_1
    const-string/jumbo v0, "LivePipeline"

    const-string/jumbo v1, "Should not be connected because the previous stream could not be connected to or failed in an unexpected way"

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    sget-object v0, Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;->c:Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;

    goto :goto_0

    .line 251
    :cond_2
    sget-object v0, Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;->a:Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/network/livepipeline/d;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/twitter/library/network/livepipeline/d;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 210
    iget-object v0, p1, Lcom/twitter/library/network/livepipeline/d;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->b(Ljava/lang/String;)Lrx/c;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected a()V
    .locals 2

    .prologue
    .line 588
    monitor-enter p0

    .line 589
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->r:Lcom/twitter/library/network/livepipeline/LivePipeline$b;

    if-nez v0, :cond_0

    .line 590
    new-instance v0, Lcom/twitter/library/network/livepipeline/LivePipeline$b;

    invoke-direct {v0, p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$b;-><init>(Lcom/twitter/library/network/livepipeline/LivePipeline;)V

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->r:Lcom/twitter/library/network/livepipeline/LivePipeline$b;

    .line 593
    :cond_0
    new-instance v0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->r:Lcom/twitter/library/network/livepipeline/LivePipeline$b;

    invoke-virtual {v1}, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/network/livepipeline/LivePipeline$c;-><init>(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    .line 594
    monitor-exit p0

    .line 595
    return-void

    .line 594
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected a(Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;)V
    .locals 3

    .prologue
    .line 220
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    invoke-virtual {v0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->a(Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;)V

    .line 222
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->s:Lcom/twitter/library/network/livepipeline/LivePipeline$c;

    invoke-virtual {v0, p2}, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->a(Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;)V

    .line 225
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->i:Ljava/lang/String;

    .line 226
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->e:Lcom/twitter/library/network/livepipeline/StreamManager;

    invoke-virtual {v0}, Lcom/twitter/library/network/livepipeline/StreamManager;->b()V

    .line 227
    const-string/jumbo v0, "LivePipeline"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Client-side termination of stream: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    return-void
.end method

.method protected a(Lcom/twitter/library/network/livepipeline/h$a;)V
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->h:Landroid/content/Context;

    invoke-virtual {p1, v0}, Lcom/twitter/library/network/livepipeline/h$a;->a(Landroid/content/Context;)Lcom/twitter/library/network/livepipeline/h$a;

    move-result-object v0

    .line 278
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/livepipeline/h$a;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/library/network/livepipeline/h$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/livepipeline/h$a;->q()Ljava/lang/Object;

    move-result-object v0

    .line 277
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/network/livepipeline/h;

    .line 279
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 281
    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->e:Lcom/twitter/library/network/livepipeline/StreamManager;

    invoke-virtual {v0}, Lcom/twitter/library/network/livepipeline/StreamManager;->a()Lcom/twitter/library/network/livepipeline/StreamManager$Status;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/network/livepipeline/StreamManager$Status;->c:Lcom/twitter/library/network/livepipeline/StreamManager$Status;

    if-eq v0, v1, :cond_0

    .line 262
    const-string/jumbo v0, "LivePipeline"

    const-string/jumbo v1, "Abandoning resubscribe because live pipeline is disconnected"

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 264
    const-string/jumbo v0, "LivePipeline"

    const-string/jumbo v1, "Abandoning resubscribe because session expired"

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 266
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method protected b(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/AsyncOperation",
            "<",
            "Landroid/os/Bundle;",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 478
    invoke-direct {p0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->c(Lcom/twitter/async/service/AsyncOperation;)Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;

    move-result-object v0

    .line 481
    iget-boolean v0, v0, Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;->shouldBeConnected:Z

    if-nez v0, :cond_1

    .line 482
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->r:Lcom/twitter/library/network/livepipeline/LivePipeline$b;

    if-eqz v0, :cond_0

    .line 483
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->r:Lcom/twitter/library/network/livepipeline/LivePipeline$b;

    invoke-virtual {v0}, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->j()V

    .line 484
    iput-object v1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->r:Lcom/twitter/library/network/livepipeline/LivePipeline$b;

    .line 486
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->d:Lrx/subjects/PublishSubject;

    invoke-virtual {v0}, Lrx/subjects/PublishSubject;->by_()V

    .line 487
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 488
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->g:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 489
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->h:Landroid/content/Context;

    invoke-direct {p0, v0, v1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Landroid/content/Context;Lcom/twitter/library/network/livepipeline/StreamManager;)V

    .line 496
    :goto_0
    return-void

    .line 491
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->f:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 492
    const-string/jumbo v1, "LivePipeline"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Reconnecting at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    iget-object v4, p0, Lcom/twitter/library/network/livepipeline/LivePipeline;->p:Lcqt;

    invoke-interface {v4}, Lcqt;->a()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " with the following topics: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    invoke-direct {p0, v0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Ljava/util/Set;)Z

    goto :goto_0
.end method
