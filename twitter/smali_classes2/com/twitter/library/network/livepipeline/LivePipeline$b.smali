.class Lcom/twitter/library/network/livepipeline/LivePipeline$b;
.super Lcom/twitter/library/network/livepipeline/LivePipeline$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/network/livepipeline/LivePipeline;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "b"
.end annotation


# instance fields
.field final synthetic i:Lcom/twitter/library/network/livepipeline/LivePipeline;

.field private l:I

.field private final m:Ljava/lang/String;

.field private n:J


# direct methods
.method protected constructor <init>(Lcom/twitter/library/network/livepipeline/LivePipeline;)V
    .locals 2

    .prologue
    .line 838
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->i:Lcom/twitter/library/network/livepipeline/LivePipeline;

    .line 839
    const-string/jumbo v0, "lp:events:::series"

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/network/livepipeline/LivePipeline$a;-><init>(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/lang/String;)V

    .line 834
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->l:I

    .line 836
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->n:J

    .line 840
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->m:Ljava/lang/String;

    .line 841
    return-void
.end method


# virtual methods
.method public c(J)V
    .locals 3

    .prologue
    .line 853
    iget-wide v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->n:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->n:J

    .line 854
    return-void
.end method

.method protected e()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 858
    invoke-virtual {p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->f()Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 859
    const-string/jumbo v1, "stream_count"

    iget v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->l:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    .line 860
    const-string/jumbo v1, "series_id"

    invoke-virtual {p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    .line 861
    const-string/jumbo v1, "gap_time"

    invoke-virtual {p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->k()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->n:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    .line 862
    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 845
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->m:Ljava/lang/String;

    return-object v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 849
    iget v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$b;->l:I

    .line 850
    return-void
.end method
