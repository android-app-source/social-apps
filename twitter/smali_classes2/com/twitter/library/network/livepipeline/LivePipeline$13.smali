.class Lcom/twitter/library/network/livepipeline/LivePipeline$13;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Landroid/content/Context;Lcom/twitter/library/network/livepipeline/StreamManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Lcom/twitter/model/livepipeline/a;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/network/livepipeline/LivePipeline;


# direct methods
.method constructor <init>(Lcom/twitter/library/network/livepipeline/LivePipeline;)V
    .locals 0

    .prologue
    .line 310
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/livepipeline/a;)V
    .locals 6

    .prologue
    .line 313
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    iget-object v1, p1, Lcom/twitter/model/livepipeline/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/lang/String;)Ljava/lang/String;

    .line 314
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-static {v1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Lcom/twitter/library/network/livepipeline/LivePipeline;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->b(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/lang/String;)V

    .line 315
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    iget-object v1, p1, Lcom/twitter/model/livepipeline/a;->b:Ljava/lang/Long;

    invoke-static {v0, v1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/lang/Long;)Ljava/lang/Long;

    .line 316
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    iget-object v1, p1, Lcom/twitter/model/livepipeline/a;->b:Ljava/lang/Long;

    .line 317
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-float v1, v2

    const/high16 v2, 0x3f400000    # 0.75f

    mul-float/2addr v1, v2

    float-to-long v2, v1

    .line 318
    invoke-static {}, Lcom/twitter/library/network/livepipeline/LivePipeline;->b()J

    move-result-wide v4

    .line 316
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->b(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/lang/Long;)Ljava/lang/Long;

    .line 319
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    iget-object v1, p1, Lcom/twitter/model/livepipeline/a;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x2

    mul-long/2addr v2, v4

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0xa

    invoke-virtual {v1, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 320
    invoke-static {}, Lcom/twitter/library/network/livepipeline/LivePipeline;->c()J

    move-result-wide v4

    .line 319
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->c(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/lang/Long;)Ljava/lang/Long;

    .line 323
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-static {v0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->f(Lcom/twitter/library/network/livepipeline/LivePipeline;)Lrx/j;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-static {v0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->f(Lcom/twitter/library/network/livepipeline/LivePipeline;)Lrx/j;

    move-result-object v0

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-static {v0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->g(Lcom/twitter/library/network/livepipeline/LivePipeline;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 329
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-static {v1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->b(Lcom/twitter/library/network/livepipeline/LivePipeline;)Lrx/subjects/PublishSubject;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-static {v2}, Lcom/twitter/library/network/livepipeline/LivePipeline;->g(Lcom/twitter/library/network/livepipeline/LivePipeline;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Lrx/subjects/PublishSubject;->a(JLjava/util/concurrent/TimeUnit;)Lrx/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/livepipeline/LivePipeline$13$1;

    invoke-direct {v2, p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$13$1;-><init>(Lcom/twitter/library/network/livepipeline/LivePipeline$13;)V

    .line 330
    invoke-virtual {v1, v2}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v1

    .line 329
    invoke-static {v0, v1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Lcom/twitter/library/network/livepipeline/LivePipeline;Lrx/j;)Lrx/j;

    .line 342
    :cond_1
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-static {v1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->h(Lcom/twitter/library/network/livepipeline/LivePipeline;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/util/Set;)V

    .line 344
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-static {v0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->h(Lcom/twitter/library/network/livepipeline/LivePipeline;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 346
    const-string/jumbo v0, "LivePipeline"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Processing config control frame: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    const-string/jumbo v0, "LivePipeline"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Resubscribe interval: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-static {v2}, Lcom/twitter/library/network/livepipeline/LivePipeline;->i(Lcom/twitter/library/network/livepipeline/LivePipeline;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-static {v1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->i(Lcom/twitter/library/network/livepipeline/LivePipeline;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Lcom/twitter/library/network/livepipeline/LivePipeline;J)V

    .line 349
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 310
    check-cast p1, Lcom/twitter/model/livepipeline/a;

    invoke-virtual {p0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline$13;->a(Lcom/twitter/model/livepipeline/a;)V

    return-void
.end method
