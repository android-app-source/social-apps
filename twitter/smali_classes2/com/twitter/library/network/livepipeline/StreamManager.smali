.class Lcom/twitter/library/network/livepipeline/StreamManager;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/network/livepipeline/StreamManager$Status;
    }
.end annotation


# instance fields
.field protected a:Lcom/twitter/library/network/livepipeline/c;

.field private final b:Lrx/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/d",
            "<",
            "Lcom/twitter/model/livepipeline/d;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/library/client/p;

.field private final d:Landroid/content/Context;

.field private e:Lcom/twitter/library/network/livepipeline/StreamManager$Status;

.field private final f:Lcqt;

.field private g:Ljava/lang/Long;


# direct methods
.method private constructor <init>(Lrx/d;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/d",
            "<",
            "Lcom/twitter/model/livepipeline/d;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    sget-object v0, Lcom/twitter/library/network/livepipeline/StreamManager$Status;->a:Lcom/twitter/library/network/livepipeline/StreamManager$Status;

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->e:Lcom/twitter/library/network/livepipeline/StreamManager$Status;

    .line 55
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->b:Lrx/d;

    .line 56
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->c:Lcom/twitter/library/client/p;

    .line 57
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->d:Landroid/content/Context;

    .line 58
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->a()Lcqt;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->f:Lcqt;

    .line 59
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/network/livepipeline/StreamManager;Lcom/twitter/library/network/livepipeline/StreamManager$Status;)Lcom/twitter/library/network/livepipeline/StreamManager$Status;
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->e:Lcom/twitter/library/network/livepipeline/StreamManager$Status;

    return-object p1
.end method

.method static a(Lrx/d;Landroid/content/Context;)Lcom/twitter/library/network/livepipeline/StreamManager;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/d",
            "<",
            "Lcom/twitter/model/livepipeline/d;",
            ">;",
            "Landroid/content/Context;",
            ")",
            "Lcom/twitter/library/network/livepipeline/StreamManager;"
        }
    .end annotation

    .prologue
    .line 51
    new-instance v0, Lcom/twitter/library/network/livepipeline/StreamManager;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/network/livepipeline/StreamManager;-><init>(Lrx/d;Landroid/content/Context;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/library/network/livepipeline/StreamManager;)Lcqt;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->f:Lcqt;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/library/network/livepipeline/StreamManager;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->g:Ljava/lang/Long;

    return-object p1
.end method

.method static synthetic b(Lcom/twitter/library/network/livepipeline/StreamManager;)Lcom/twitter/library/client/p;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->c:Lcom/twitter/library/client/p;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/library/network/livepipeline/StreamManager$Status;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->e:Lcom/twitter/library/network/livepipeline/StreamManager$Status;

    return-object v0
.end method

.method public a(Ljava/util/Set;Lcom/twitter/async/service/AsyncOperation$b;J)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/twitter/async/service/AsyncOperation$b",
            "<",
            "Landroid/os/Bundle;",
            "Lcom/twitter/async/service/AsyncOperation",
            "<",
            "Landroid/os/Bundle;",
            "Lcom/twitter/library/service/u;",
            ">;>;J)V"
        }
    .end annotation

    .prologue
    .line 67
    monitor-enter p0

    .line 68
    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/library/network/livepipeline/StreamManager;->a()Lcom/twitter/library/network/livepipeline/StreamManager$Status;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/network/livepipeline/StreamManager$Status;->a:Lcom/twitter/library/network/livepipeline/StreamManager$Status;

    if-eq v0, v1, :cond_0

    .line 69
    monitor-exit p0

    .line 112
    :goto_0
    return-void

    .line 72
    :cond_0
    sget-object v0, Lcom/twitter/library/network/livepipeline/StreamManager$Status;->b:Lcom/twitter/library/network/livepipeline/StreamManager$Status;

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->e:Lcom/twitter/library/network/livepipeline/StreamManager$Status;

    .line 73
    new-instance v0, Lcom/twitter/library/network/livepipeline/StreamManager$1;

    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->b:Lrx/d;

    iget-object v3, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->d:Landroid/content/Context;

    move-object v1, p0

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/network/livepipeline/StreamManager$1;-><init>(Lcom/twitter/library/network/livepipeline/StreamManager;Lrx/d;Landroid/content/Context;J)V

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->a:Lcom/twitter/library/network/livepipeline/c;

    .line 88
    if-eqz p2, :cond_1

    .line 89
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->a:Lcom/twitter/library/network/livepipeline/c;

    invoke-virtual {v0, p2}, Lcom/twitter/library/network/livepipeline/c;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    .line 92
    :cond_1
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 93
    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->a:Lcom/twitter/library/network/livepipeline/c;

    invoke-virtual {v2, v0}, Lcom/twitter/library/network/livepipeline/c;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 96
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/twitter/library/network/livepipeline/StreamManager;->c()J

    move-result-wide v0

    .line 100
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_3

    .line 101
    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->a:Lcom/twitter/library/network/livepipeline/c;

    invoke-static {v2}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v2

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1, v3}, Lrx/c;->b(JLjava/util/concurrent/TimeUnit;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/livepipeline/StreamManager$2;

    invoke-direct {v1, p0}, Lcom/twitter/library/network/livepipeline/StreamManager$2;-><init>(Lcom/twitter/library/network/livepipeline/StreamManager;)V

    .line 102
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 111
    :goto_2
    monitor-exit p0

    goto :goto_0

    .line 109
    :cond_3
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->c:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->a:Lcom/twitter/library/network/livepipeline/c;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method public b()V
    .locals 2

    .prologue
    .line 120
    monitor-enter p0

    .line 121
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->a:Lcom/twitter/library/network/livepipeline/c;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->a:Lcom/twitter/library/network/livepipeline/c;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/livepipeline/c;->cancel(Z)Z

    .line 124
    :cond_0
    sget-object v0, Lcom/twitter/library/network/livepipeline/StreamManager$Status;->a:Lcom/twitter/library/network/livepipeline/StreamManager$Status;

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->e:Lcom/twitter/library/network/livepipeline/StreamManager$Status;

    .line 125
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->a:Lcom/twitter/library/network/livepipeline/c;

    .line 126
    monitor-exit p0

    .line 127
    return-void

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected c()J
    .locals 6

    .prologue
    const-wide/16 v4, 0x7d0

    .line 135
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->f:Lcqt;

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v0

    .line 138
    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->g:Ljava/lang/Long;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->g:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long v2, v0, v2

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 139
    :cond_0
    const-wide/16 v0, 0x0

    .line 144
    :goto_0
    return-wide v0

    .line 141
    :cond_1
    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/StreamManager;->g:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long/2addr v2, v4

    sub-long v0, v2, v0

    goto :goto_0
.end method
