.class Lcom/twitter/library/network/livepipeline/LivePipeline$7;
.super Lcom/twitter/library/service/w;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/network/livepipeline/LivePipeline;->b(Ljava/util/Set;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/w",
        "<",
        "Landroid/os/Bundle;",
        "Lcom/twitter/async/service/AsyncOperation",
        "<",
        "Landroid/os/Bundle;",
        "Lcom/twitter/library/service/u;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/network/livepipeline/LivePipeline;


# direct methods
.method constructor <init>(Lcom/twitter/library/network/livepipeline/LivePipeline;)V
    .locals 0

    .prologue
    .line 448
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$7;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-direct {p0}, Lcom/twitter/library/service/w;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/AsyncOperation",
            "<",
            "Landroid/os/Bundle;",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 451
    invoke-virtual {p1}, Lcom/twitter/async/service/AsyncOperation;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 452
    if-eqz v0, :cond_0

    .line 453
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v0

    .line 454
    if-eqz v0, :cond_0

    .line 455
    iget-object v1, v0, Lcom/twitter/network/l;->t:[I

    .line 457
    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$7;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    const/4 v3, 0x2

    aget v3, v1, v3

    const/4 v4, 0x3

    aget v1, v1, v4

    add-int/2addr v1, v3

    invoke-static {v2, v1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Lcom/twitter/library/network/livepipeline/LivePipeline;I)V

    .line 459
    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$7;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    iget-wide v2, v0, Lcom/twitter/network/l;->i:J

    invoke-static {v1, v2, v3}, Lcom/twitter/library/network/livepipeline/LivePipeline;->b(Lcom/twitter/library/network/livepipeline/LivePipeline;J)V

    .line 462
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$7;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-virtual {v0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->b(Lcom/twitter/async/service/AsyncOperation;)V

    .line 463
    return-void
.end method
