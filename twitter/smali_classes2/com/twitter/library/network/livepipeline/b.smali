.class public Lcom/twitter/library/network/livepipeline/b;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/network/livepipeline/LivePipeline;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-direct {v0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/b;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    .line 21
    return-void
.end method

.method public static a()Lcom/twitter/library/network/livepipeline/b;
    .locals 1

    .prologue
    .line 25
    invoke-static {}, Lbms;->ah()Lbms;

    move-result-object v0

    invoke-virtual {v0}, Lbms;->ac()Lcom/twitter/library/network/livepipeline/b;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/twitter/model/livepipeline/PipelineEventType;J)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/livepipeline/PipelineEventType;",
            "J)",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/livepipeline/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    new-instance v0, Lcom/twitter/library/network/livepipeline/d$a;

    invoke-direct {v0}, Lcom/twitter/library/network/livepipeline/d$a;-><init>()V

    .line 71
    invoke-virtual {v0, p1}, Lcom/twitter/library/network/livepipeline/d$a;->a(Lcom/twitter/model/livepipeline/PipelineEventType;)Lcom/twitter/library/network/livepipeline/d$a;

    move-result-object v0

    .line 72
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/livepipeline/d$a;->a(Ljava/lang/Object;)Lcom/twitter/library/network/livepipeline/d$a;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lcom/twitter/library/network/livepipeline/d$a;->a()Lcom/twitter/library/network/livepipeline/d;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/b;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Lcom/twitter/library/network/livepipeline/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/c;

    return-object v0
.end method


# virtual methods
.method public a(J)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/livepipeline/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    sget-object v0, Lcom/twitter/model/livepipeline/PipelineEventType;->f:Lcom/twitter/model/livepipeline/PipelineEventType;

    invoke-direct {p0, v0, p1, p2}, Lcom/twitter/library/network/livepipeline/b;->a(Lcom/twitter/model/livepipeline/PipelineEventType;J)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/livepipeline/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    new-instance v0, Lcom/twitter/library/network/livepipeline/d$a;

    invoke-direct {v0}, Lcom/twitter/library/network/livepipeline/d$a;-><init>()V

    sget-object v1, Lcom/twitter/model/livepipeline/PipelineEventType;->d:Lcom/twitter/model/livepipeline/PipelineEventType;

    .line 31
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/livepipeline/d$a;->a(Lcom/twitter/model/livepipeline/PipelineEventType;)Lcom/twitter/library/network/livepipeline/d$a;

    move-result-object v0

    .line 32
    invoke-virtual {v0, p1}, Lcom/twitter/library/network/livepipeline/d$a;->a(Ljava/lang/Object;)Lcom/twitter/library/network/livepipeline/d$a;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lcom/twitter/library/network/livepipeline/d$a;->a()Lcom/twitter/library/network/livepipeline/d;

    move-result-object v0

    .line 34
    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/b;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Lcom/twitter/library/network/livepipeline/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/c;

    return-object v0
.end method

.method a(Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/b;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;)V

    .line 67
    return-void
.end method

.method public b(Ljava/lang/String;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/livepipeline/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    new-instance v0, Lcom/twitter/library/network/livepipeline/d$a;

    invoke-direct {v0}, Lcom/twitter/library/network/livepipeline/d$a;-><init>()V

    sget-object v1, Lcom/twitter/model/livepipeline/PipelineEventType;->e:Lcom/twitter/model/livepipeline/PipelineEventType;

    .line 40
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/livepipeline/d$a;->a(Lcom/twitter/model/livepipeline/PipelineEventType;)Lcom/twitter/library/network/livepipeline/d$a;

    move-result-object v0

    .line 41
    invoke-virtual {v0, p1}, Lcom/twitter/library/network/livepipeline/d$a;->a(Ljava/lang/Object;)Lcom/twitter/library/network/livepipeline/d$a;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Lcom/twitter/library/network/livepipeline/d$a;->a()Lcom/twitter/library/network/livepipeline/d;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/b;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Lcom/twitter/library/network/livepipeline/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/c;

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 59
    new-instance v0, Lcom/twitter/library/network/livepipeline/l$a;

    invoke-direct {v0}, Lcom/twitter/library/network/livepipeline/l$a;-><init>()V

    .line 60
    invoke-virtual {v0, p1}, Lcom/twitter/library/network/livepipeline/l$a;->a(Ljava/lang/String;)Lcom/twitter/library/network/livepipeline/l$a;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/b;->a:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->a(Lcom/twitter/library/network/livepipeline/h$a;)V

    .line 62
    return-void
.end method
