.class Lcom/twitter/library/network/livepipeline/LivePipeline$10;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/network/livepipeline/LivePipeline;-><init>(Landroid/content/Context;Lcom/twitter/library/network/livepipeline/StreamManager;Ljava/lang/Long;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/network/livepipeline/f;

.field final synthetic b:Lcom/twitter/library/network/livepipeline/LivePipeline;


# direct methods
.method constructor <init>(Lcom/twitter/library/network/livepipeline/LivePipeline;Lcom/twitter/library/network/livepipeline/f;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$10;->b:Lcom/twitter/library/network/livepipeline/LivePipeline;

    iput-object p2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$10;->a:Lcom/twitter/library/network/livepipeline/f;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 176
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/twitter/library/network/livepipeline/LivePipeline$10;->a(Ljava/util/List;)V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 180
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 181
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$10;->b:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-static {v0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->c(Lcom/twitter/library/network/livepipeline/LivePipeline;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 183
    invoke-interface {v6}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 184
    new-instance v0, Lcom/twitter/library/network/livepipeline/m;

    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$10;->b:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-static {v1}, Lcom/twitter/library/network/livepipeline/LivePipeline;->d(Lcom/twitter/library/network/livepipeline/LivePipeline;)Landroid/content/Context;

    move-result-object v1

    .line 185
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$10;->a:Lcom/twitter/library/network/livepipeline/f;

    iget-object v4, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$10;->b:Lcom/twitter/library/network/livepipeline/LivePipeline;

    .line 186
    invoke-static {v4}, Lcom/twitter/library/network/livepipeline/LivePipeline;->e(Lcom/twitter/library/network/livepipeline/LivePipeline;)J

    move-result-wide v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/network/livepipeline/m;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/network/livepipeline/f;JLjava/util/Set;)V

    .line 188
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 190
    :cond_0
    return-void
.end method
