.class public abstract Lcom/twitter/library/network/livepipeline/a;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:J

.field private final b:Lcqt;

.field private final c:Landroid/content/Context;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field protected final j:J

.field protected k:J


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/library/network/livepipeline/a;->k:J

    .line 44
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/a;->c:Landroid/content/Context;

    .line 45
    iput-object p2, p0, Lcom/twitter/library/network/livepipeline/a;->d:Ljava/lang/String;

    .line 46
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->a()Lcqt;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/a;->b:Lcqt;

    .line 47
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/a;->b:Lcqt;

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/network/livepipeline/a;->j:J

    .line 48
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/network/livepipeline/a;->a:J

    .line 49
    invoke-static {}, Lcom/twitter/library/network/forecaster/c;->a()Lcom/twitter/library/network/forecaster/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/forecaster/c;->b()Lcom/twitter/library/network/forecaster/NetworkQuality;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/forecaster/NetworkQuality;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/a;->e:Ljava/lang/String;

    .line 50
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v0

    invoke-virtual {v0}, Lcrr;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/a;->f:Ljava/lang/String;

    .line 51
    return-void
.end method

.method protected static a(JJ)J
    .locals 4

    .prologue
    const-wide/16 v0, -0x1

    .line 97
    cmp-long v2, p0, v0

    if-eqz v2, :cond_0

    cmp-long v2, p2, v0

    if-eqz v2, :cond_0

    cmp-long v2, p2, p0

    if-lez v2, :cond_1

    .line 100
    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    sub-long v0, p0, p2

    goto :goto_0
.end method

.method private a()Lcom/twitter/util/collection/i;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/collection/i",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "total_time"

    .line 107
    invoke-virtual {p0}, Lcom/twitter/library/network/livepipeline/a;->k()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "start_network_quality"

    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/a;->e:Ljava/lang/String;

    .line 108
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "end_network_quality"

    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/a;->g:Ljava/lang/String;

    .line 109
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "start_network_type"

    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/a;->f:Ljava/lang/String;

    .line 110
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "end_network_type"

    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/a;->h:Ljava/lang/String;

    .line 111
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "year_class"

    .line 112
    invoke-static {}, Lcob;->a()Lcob;

    move-result-object v2

    invoke-virtual {v2}, Lcob;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 106
    return-object v0
.end method

.method private a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/twitter/library/network/livepipeline/a;->a()Lcom/twitter/util/collection/i;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/util/collection/i;->b(Ljava/util/Map;)Lcom/twitter/util/collection/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 117
    invoke-virtual {p0}, Lcom/twitter/library/network/livepipeline/a;->l()Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    .line 119
    :try_start_0
    invoke-static {v0}, Lcom/twitter/library/network/livepipeline/a;->b(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    :goto_0
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 125
    return-void

    .line 120
    :catch_0
    move-exception v0

    .line 122
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static b(Ljava/util/Map;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 129
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 130
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 131
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 133
    :cond_0
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected abstract e()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public j()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/a;->b:Lcqt;

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/network/livepipeline/a;->k:J

    .line 63
    invoke-static {}, Lcom/twitter/library/network/forecaster/c;->a()Lcom/twitter/library/network/forecaster/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/forecaster/c;->b()Lcom/twitter/library/network/forecaster/NetworkQuality;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/forecaster/NetworkQuality;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/a;->g:Ljava/lang/String;

    .line 64
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v0

    invoke-virtual {v0}, Lcrr;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/a;->h:Ljava/lang/String;

    .line 65
    invoke-virtual {p0}, Lcom/twitter/library/network/livepipeline/a;->e()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/library/network/livepipeline/a;->a(Ljava/util/Map;)V

    .line 66
    return-void
.end method

.method protected k()J
    .locals 4

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/twitter/library/network/livepipeline/a;->k:J

    iget-wide v2, p0, Lcom/twitter/library/network/livepipeline/a;->j:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/network/livepipeline/a;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method protected l()Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/twitter/library/network/livepipeline/a;->m()Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    return-object v0
.end method

.method protected final m()Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 4

    .prologue
    .line 87
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/library/network/livepipeline/a;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/library/network/livepipeline/a;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 88
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/a;->c:Landroid/content/Context;

    .line 89
    invoke-static {v1}, Lcom/twitter/library/network/ab;->a(Landroid/content/Context;)Lcom/twitter/library/network/ab;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/library/network/ab;->c:Lcom/twitter/library/network/ae;

    invoke-virtual {v1}, Lcom/twitter/library/network/ae;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->e(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 87
    return-object v0
.end method
