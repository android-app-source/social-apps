.class Lcom/twitter/library/network/livepipeline/LivePipeline$c;
.super Lcom/twitter/library/network/livepipeline/LivePipeline$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/network/livepipeline/LivePipeline;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "c"
.end annotation


# instance fields
.field final synthetic i:Lcom/twitter/library/network/livepipeline/LivePipeline;

.field private l:J

.field private m:I

.field private n:Ljava/lang/String;

.field private final o:Ljava/lang/String;

.field private p:Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;

.field private q:Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;


# direct methods
.method protected constructor <init>(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 877
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->i:Lcom/twitter/library/network/livepipeline/LivePipeline;

    .line 878
    const-string/jumbo v0, "lp:events:::stream"

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/network/livepipeline/LivePipeline$a;-><init>(Lcom/twitter/library/network/livepipeline/LivePipeline;Ljava/lang/String;)V

    .line 870
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->l:J

    .line 871
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->m:I

    .line 874
    sget-object v0, Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;->a:Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;

    iput-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->p:Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;

    .line 879
    iput-object p2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->o:Ljava/lang/String;

    .line 880
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 891
    iput p1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->m:I

    .line 892
    return-void
.end method

.method public a(Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;)V
    .locals 0

    .prologue
    .line 899
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->p:Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;

    .line 900
    return-void
.end method

.method public a(Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;)V
    .locals 0

    .prologue
    .line 908
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->q:Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;

    .line 909
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 895
    iput-object p1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->n:Ljava/lang/String;

    .line 896
    return-void
.end method

.method protected e()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 913
    invoke-virtual {p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->f()Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "time_to_establish"

    .line 914
    invoke-virtual {p0}, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->h()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "time_to_response"

    iget v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->m:I

    .line 915
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "disconnection_reason"

    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->p:Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;

    .line 916
    invoke-virtual {v2}, Lcom/twitter/library/network/livepipeline/LivePipeline$DisconnectionReason;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "session_id"

    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->n:Ljava/lang/String;

    .line 917
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "series_id"

    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->o:Ljava/lang/String;

    .line 918
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 920
    iget-object v1, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->q:Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;

    if-eqz v1, :cond_0

    .line 921
    const-string/jumbo v1, "will_reconnect"

    iget-object v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->q:Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;

    iget-boolean v2, v2, Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;->shouldBeConnected:Z

    .line 922
    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v1

    const-string/jumbo v2, "reconnect_decision_reason"

    iget-object v3, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->q:Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;

    .line 923
    invoke-virtual {v3}, Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    .line 925
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 883
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->i:Lcom/twitter/library/network/livepipeline/LivePipeline;

    invoke-static {v0}, Lcom/twitter/library/network/livepipeline/LivePipeline;->m(Lcom/twitter/library/network/livepipeline/LivePipeline;)Lcqt;

    move-result-object v0

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->l:J

    .line 884
    return-void
.end method

.method public h()J
    .locals 4

    .prologue
    .line 887
    iget-wide v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->l:J

    iget-wide v2, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->j:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public i()Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;
    .locals 1

    .prologue
    .line 904
    iget-object v0, p0, Lcom/twitter/library/network/livepipeline/LivePipeline$c;->q:Lcom/twitter/library/network/livepipeline/LivePipeline$ReconnectDecision;

    return-object v0
.end method
