.class public Lcom/twitter/library/network/g;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/network/e;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/g;->a:Landroid/content/Context;

    .line 45
    return-void
.end method

.method private d()[Landroid/accounts/Account;
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/library/network/g;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 128
    if-eqz v0, :cond_0

    .line 130
    const-string/jumbo v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 132
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Landroid/accounts/Account;

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 6

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/twitter/library/network/g;->c()Ljava/lang/String;

    move-result-object v0

    .line 59
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 60
    new-instance v1, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$a;

    invoke-direct {v1}, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$a;-><init>()V

    .line 61
    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$a;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$a;

    move-result-object v0

    const-string/jumbo v1, "695118608688-pm7k6hvatrgesugmkcti68kndb9e3tu6.apps.googleusercontent.com"

    .line 62
    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$a;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$a;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions$a;->c()Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;

    move-result-object v0

    .line 65
    new-instance v1, Lcom/google/android/gms/common/api/c$a;

    iget-object v2, p0, Lcom/twitter/library/network/g;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/c$a;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhr;->f:Lcom/google/android/gms/common/api/a;

    .line 66
    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/common/api/c$a;->a(Lcom/google/android/gms/common/api/a;Lcom/google/android/gms/common/api/a$a$a;)Lcom/google/android/gms/common/api/c$a;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/c$a;->b()Lcom/google/android/gms/common/api/c;

    move-result-object v1

    .line 70
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/common/api/c;->f()Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v0

    .line 71
    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    sget-object v0, Lhr;->k:Lcom/google/android/gms/auth/api/signin/a;

    .line 74
    invoke-interface {v0, v1}, Lcom/google/android/gms/auth/api/signin/a;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/d;->a()Lcom/google/android/gms/common/api/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/api/signin/b;

    .line 75
    invoke-virtual {v0}, Lcom/google/android/gms/auth/api/signin/b;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 76
    invoke-virtual {v0}, Lcom/google/android/gms/auth/api/signin/b;->a()Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    move-result-object v0

    .line 77
    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {v0}, Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;->b()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 90
    invoke-virtual {v1}, Lcom/google/android/gms/common/api/c;->g()V

    .line 94
    :goto_0
    return-object v0

    .line 90
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/common/api/c;->g()V

    .line 94
    :cond_1
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 82
    :catch_0
    move-exception v0

    .line 83
    :try_start_1
    iget-object v2, p0, Lcom/twitter/library/network/g;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/library/network/ab;->a(Landroid/content/Context;)Lcom/twitter/library/network/ab;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/library/network/ab;->c:Lcom/twitter/library/network/ae;

    invoke-virtual {v2}, Lcom/twitter/library/network/ae;->toString()Ljava/lang/String;

    move-result-object v2

    .line 84
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-wide/16 v4, 0x0

    invoke-direct {v3, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 85
    invoke-static {v0}, Lcqj;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "google_auth_token::::error"

    aput-object v5, v3, v4

    .line 86
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 87
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->e(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 88
    invoke-static {v0}, Lcpm;->a(Lcpk;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90
    invoke-virtual {v1}, Lcom/google/android/gms/common/api/c;->g()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/c;->g()V

    throw v0
.end method

.method public b()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/twitter/library/network/g;->d()[Landroid/accounts/Account;

    move-result-object v1

    .line 101
    new-instance v2, Ljava/util/ArrayList;

    array-length v0, v1

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 102
    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v1, v0

    .line 103
    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    :cond_0
    return-object v2
.end method

.method public c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 111
    const/4 v0, 0x0

    .line 113
    invoke-direct {p0}, Lcom/twitter/library/network/g;->d()[Landroid/accounts/Account;

    move-result-object v1

    .line 114
    array-length v2, v1

    if-lez v2, :cond_0

    .line 115
    const/4 v0, 0x0

    aget-object v0, v1, v0

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 118
    :cond_0
    return-object v0
.end method
