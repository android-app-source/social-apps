.class Lcom/twitter/library/network/forecaster/d$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/network/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/library/network/forecaster/d;-><init>(Lcom/twitter/util/p;Lcom/twitter/util/p;Lcom/twitter/network/d;Lcom/twitter/util/connectivity/b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/network/forecaster/d;


# direct methods
.method constructor <init>(Lcom/twitter/library/network/forecaster/d;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/twitter/library/network/forecaster/d$3;->a:Lcom/twitter/library/network/forecaster/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/network/HttpOperation;)V
    .locals 0

    .prologue
    .line 106
    return-void
.end method

.method public a(Lcom/twitter/network/HttpOperation;Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 128
    return-void
.end method

.method public b(Lcom/twitter/network/HttpOperation;)V
    .locals 8

    .prologue
    .line 110
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->m()Lcom/twitter/network/l;

    move-result-object v0

    .line 112
    new-instance v1, Lcom/twitter/util/units/data/Bytes;

    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->f()J

    move-result-wide v2

    long-to-double v2, v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/util/units/data/Bytes;-><init>(D)V

    .line 113
    new-instance v2, Lcom/twitter/util/units/duration/Milliseconds;

    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->g()J

    move-result-wide v4

    long-to-double v4, v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/util/units/duration/Milliseconds;-><init>(D)V

    .line 115
    new-instance v3, Lcom/twitter/util/units/data/Bytes;

    iget-wide v4, v0, Lcom/twitter/network/l;->i:J

    long-to-double v4, v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/util/units/data/Bytes;-><init>(D)V

    .line 116
    new-instance v4, Lcom/twitter/util/units/duration/Milliseconds;

    iget-wide v6, v0, Lcom/twitter/network/l;->f:J

    long-to-double v6, v6

    invoke-direct {v4, v6, v7}, Lcom/twitter/util/units/duration/Milliseconds;-><init>(D)V

    .line 118
    new-instance v5, Lcom/twitter/util/units/duration/Milliseconds;

    iget-object v0, v0, Lcom/twitter/network/l;->t:[I

    const/4 v6, 0x3

    aget v0, v0, v6

    int-to-double v6, v0

    invoke-direct {v5, v6, v7}, Lcom/twitter/util/units/duration/Milliseconds;-><init>(D)V

    .line 121
    iget-object v0, p0, Lcom/twitter/library/network/forecaster/d$3;->a:Lcom/twitter/library/network/forecaster/d;

    iget-object v6, p0, Lcom/twitter/library/network/forecaster/d$3;->a:Lcom/twitter/library/network/forecaster/d;

    invoke-static {v6}, Lcom/twitter/library/network/forecaster/d;->a(Lcom/twitter/library/network/forecaster/d;)Lcrm;

    move-result-object v6

    invoke-static {v0, v1, v2, v6}, Lcom/twitter/library/network/forecaster/d;->a(Lcom/twitter/library/network/forecaster/d;Lcom/twitter/util/units/data/Data;Lcom/twitter/util/units/duration/Duration;Lcrm;)V

    .line 122
    iget-object v0, p0, Lcom/twitter/library/network/forecaster/d$3;->a:Lcom/twitter/library/network/forecaster/d;

    iget-object v1, p0, Lcom/twitter/library/network/forecaster/d$3;->a:Lcom/twitter/library/network/forecaster/d;

    invoke-static {v1}, Lcom/twitter/library/network/forecaster/d;->b(Lcom/twitter/library/network/forecaster/d;)Lcrm;

    move-result-object v1

    invoke-static {v0, v3, v4, v1}, Lcom/twitter/library/network/forecaster/d;->a(Lcom/twitter/library/network/forecaster/d;Lcom/twitter/util/units/data/Data;Lcom/twitter/util/units/duration/Duration;Lcrm;)V

    .line 123
    iget-object v0, p0, Lcom/twitter/library/network/forecaster/d$3;->a:Lcom/twitter/library/network/forecaster/d;

    invoke-static {v0, v5}, Lcom/twitter/library/network/forecaster/d;->a(Lcom/twitter/library/network/forecaster/d;Lcom/twitter/util/units/duration/Duration;)V

    .line 124
    return-void
.end method
