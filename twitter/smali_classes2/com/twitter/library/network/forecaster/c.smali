.class public Lcom/twitter/library/network/forecaster/c;
.super Lcom/twitter/util/p;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/p",
        "<",
        "Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/network/forecaster/d;

.field private final b:Lcom/twitter/library/network/forecaster/a;

.field private final c:Lcrq;


# direct methods
.method public constructor <init>(Lcom/twitter/library/network/forecaster/d;Lcom/twitter/library/network/forecaster/a;Lcrq;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/twitter/util/p;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/twitter/library/network/forecaster/c;->a:Lcom/twitter/library/network/forecaster/d;

    .line 27
    iput-object p2, p0, Lcom/twitter/library/network/forecaster/c;->b:Lcom/twitter/library/network/forecaster/a;

    .line 28
    iput-object p3, p0, Lcom/twitter/library/network/forecaster/c;->c:Lcrq;

    .line 29
    return-void
.end method

.method public static declared-synchronized a()Lcom/twitter/library/network/forecaster/c;
    .locals 2

    .prologue
    .line 21
    const-class v1, Lcom/twitter/library/network/forecaster/c;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lbms;->ah()Lbms;

    move-result-object v0

    invoke-virtual {v0}, Lbms;->R()Lcom/twitter/library/network/forecaster/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(Lcom/twitter/util/q;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lcom/twitter/library/network/forecaster/c;->a:Lcom/twitter/library/network/forecaster/d;

    invoke-virtual {v0, p1}, Lcom/twitter/library/network/forecaster/d;->a(Lcom/twitter/util/q;)Z

    move-result v0

    return v0
.end method

.method public b()Lcom/twitter/library/network/forecaster/NetworkQuality;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/library/network/forecaster/c;->a:Lcom/twitter/library/network/forecaster/d;

    invoke-virtual {v0}, Lcom/twitter/library/network/forecaster/d;->b()Lcom/twitter/library/network/forecaster/NetworkQuality;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/twitter/util/q;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/twitter/library/network/forecaster/c;->a:Lcom/twitter/library/network/forecaster/d;

    invoke-virtual {v0, p1}, Lcom/twitter/library/network/forecaster/d;->b(Lcom/twitter/util/q;)Z

    move-result v0

    return v0
.end method

.method public c()Lcom/twitter/util/units/bitrate/KilobitsPerSecond;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/twitter/library/network/forecaster/c;->a:Lcom/twitter/library/network/forecaster/d;

    invoke-virtual {v0}, Lcom/twitter/library/network/forecaster/d;->c()Lcom/twitter/util/units/bitrate/KilobitsPerSecond;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/twitter/util/units/bitrate/KilobitsPerSecond;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/library/network/forecaster/c;->a:Lcom/twitter/library/network/forecaster/d;

    invoke-virtual {v0}, Lcom/twitter/library/network/forecaster/d;->d()Lcom/twitter/util/units/bitrate/KilobitsPerSecond;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/twitter/util/units/duration/Milliseconds;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/library/network/forecaster/c;->a:Lcom/twitter/library/network/forecaster/d;

    invoke-virtual {v0}, Lcom/twitter/library/network/forecaster/d;->e()Lcom/twitter/util/units/duration/Milliseconds;

    move-result-object v0

    return-object v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/library/network/forecaster/c;->b:Lcom/twitter/library/network/forecaster/a;

    invoke-virtual {v0}, Lcom/twitter/library/network/forecaster/a;->a()Z

    move-result v0

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/library/network/forecaster/c;->c:Lcrq;

    invoke-virtual {v0}, Lcrq;->b()I

    move-result v0

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/library/network/forecaster/c;->c:Lcrq;

    invoke-virtual {v0}, Lcrq;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
