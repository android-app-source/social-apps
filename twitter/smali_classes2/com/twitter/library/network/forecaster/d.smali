.class public Lcom/twitter/library/network/forecaster/d;
.super Lcom/twitter/util/p;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/p",
        "<",
        "Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/twitter/util/units/data/Kibibytes;

.field private static final b:Lcom/twitter/util/units/data/Kibibytes;

.field private static final c:Lcom/twitter/util/units/duration/Milliseconds;

.field private static final d:Lcom/twitter/util/units/duration/Duration;


# instance fields
.field private e:Lcom/twitter/library/network/forecaster/NetworkQuality;

.field private f:Lcrm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcrm",
            "<",
            "Lcom/twitter/util/units/data/Kibibytes;",
            "Lcom/twitter/util/units/bitrate/KilobitsPerSecond;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcrm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcrm",
            "<",
            "Lcom/twitter/util/units/data/Kibibytes;",
            "Lcom/twitter/util/units/bitrate/KilobitsPerSecond;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcrn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcrn",
            "<",
            "Lcom/twitter/util/units/duration/Milliseconds;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 38
    new-instance v0, Lcom/twitter/util/units/data/Kibibytes;

    const-wide/high16 v2, 0x4020000000000000L    # 8.0

    invoke-direct {v0, v2, v3}, Lcom/twitter/util/units/data/Kibibytes;-><init>(D)V

    sput-object v0, Lcom/twitter/library/network/forecaster/d;->a:Lcom/twitter/util/units/data/Kibibytes;

    .line 41
    new-instance v0, Lcom/twitter/util/units/data/Kibibytes;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-direct {v0, v2, v3}, Lcom/twitter/util/units/data/Kibibytes;-><init>(D)V

    sput-object v0, Lcom/twitter/library/network/forecaster/d;->b:Lcom/twitter/util/units/data/Kibibytes;

    .line 44
    new-instance v0, Lcom/twitter/util/units/duration/Milliseconds;

    const-wide v2, 0x4072c00000000000L    # 300.0

    invoke-direct {v0, v2, v3}, Lcom/twitter/util/units/duration/Milliseconds;-><init>(D)V

    sput-object v0, Lcom/twitter/library/network/forecaster/d;->c:Lcom/twitter/util/units/duration/Milliseconds;

    .line 46
    new-instance v0, Lcom/twitter/util/units/duration/Minutes;

    const-wide/high16 v2, 0x4012000000000000L    # 4.5

    invoke-direct {v0, v2, v3}, Lcom/twitter/util/units/duration/Minutes;-><init>(D)V

    sput-object v0, Lcom/twitter/library/network/forecaster/d;->d:Lcom/twitter/util/units/duration/Duration;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 66
    invoke-static {}, Lcom/twitter/library/client/d;->a()Lcom/twitter/library/client/d;

    move-result-object v0

    .line 67
    invoke-static {}, Lcom/twitter/util/connectivity/a;->a()Lcom/twitter/util/connectivity/a;

    move-result-object v1

    .line 68
    invoke-static {}, Lcom/twitter/network/d;->a()Lcom/twitter/network/d;

    move-result-object v2

    .line 69
    invoke-static {}, Lcom/twitter/util/connectivity/b;->a()Lcom/twitter/util/connectivity/b;

    move-result-object v3

    .line 66
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/twitter/library/network/forecaster/d;-><init>(Lcom/twitter/util/p;Lcom/twitter/util/p;Lcom/twitter/network/d;Lcom/twitter/util/connectivity/b;)V

    .line 70
    return-void
.end method

.method protected constructor <init>(Lcom/twitter/util/p;Lcom/twitter/util/p;Lcom/twitter/network/d;Lcom/twitter/util/connectivity/b;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/library/client/AppResumeEvent;",
            ">;",
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;",
            ">;",
            "Lcom/twitter/network/d;",
            "Lcom/twitter/util/connectivity/b;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/16 v1, 0xa

    .line 84
    invoke-direct {p0}, Lcom/twitter/util/p;-><init>()V

    .line 48
    sget-object v0, Lcom/twitter/library/network/forecaster/NetworkQuality;->e:Lcom/twitter/library/network/forecaster/NetworkQuality;

    iput-object v0, p0, Lcom/twitter/library/network/forecaster/d;->e:Lcom/twitter/library/network/forecaster/NetworkQuality;

    .line 50
    new-instance v0, Lcrm;

    invoke-direct {v0, v1}, Lcrm;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/library/network/forecaster/d;->f:Lcrm;

    .line 52
    new-instance v0, Lcrm;

    invoke-direct {v0, v1}, Lcrm;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/library/network/forecaster/d;->g:Lcrm;

    .line 54
    new-instance v0, Lcrn;

    invoke-direct {v0, v1}, Lcrn;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/library/network/forecaster/d;->h:Lcrn;

    .line 57
    iput-boolean v2, p0, Lcom/twitter/library/network/forecaster/d;->i:Z

    .line 59
    iput-boolean v2, p0, Lcom/twitter/library/network/forecaster/d;->j:Z

    .line 85
    sget-object v0, Lcom/twitter/util/connectivity/TwRadioType;->b:Lcom/twitter/util/connectivity/TwRadioType;

    invoke-direct {p0, v0}, Lcom/twitter/library/network/forecaster/d;->a(Lcom/twitter/util/connectivity/TwRadioType;)V

    .line 86
    invoke-virtual {p4}, Lcom/twitter/util/connectivity/b;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/network/forecaster/d;->j:Z

    .line 88
    new-instance v0, Lcom/twitter/library/network/forecaster/d$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/network/forecaster/d$1;-><init>(Lcom/twitter/library/network/forecaster/d;Lcom/twitter/util/p;)V

    invoke-virtual {p1, v0}, Lcom/twitter/util/p;->a(Lcom/twitter/util/q;)Z

    .line 96
    new-instance v0, Lcom/twitter/library/network/forecaster/d$2;

    invoke-direct {v0, p0}, Lcom/twitter/library/network/forecaster/d$2;-><init>(Lcom/twitter/library/network/forecaster/d;)V

    invoke-virtual {p2, v0}, Lcom/twitter/util/p;->a(Lcom/twitter/util/q;)Z

    .line 103
    new-instance v0, Lcom/twitter/library/network/forecaster/d$3;

    invoke-direct {v0, p0}, Lcom/twitter/library/network/forecaster/d$3;-><init>(Lcom/twitter/library/network/forecaster/d;)V

    invoke-virtual {p3, v0}, Lcom/twitter/network/d;->a(Lcom/twitter/network/c;)V

    .line 131
    new-instance v0, Lcom/twitter/library/network/forecaster/d$4;

    invoke-direct {v0, p0}, Lcom/twitter/library/network/forecaster/d$4;-><init>(Lcom/twitter/library/network/forecaster/d;)V

    invoke-virtual {p4, v0}, Lcom/twitter/util/connectivity/b;->a(Lcom/twitter/util/q;)Z

    .line 137
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/network/forecaster/d;)Lcrm;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/library/network/forecaster/d;->f:Lcrm;

    return-object v0
.end method

.method private declared-synchronized a(Lcom/twitter/library/network/forecaster/NetworkQuality;)V
    .locals 2

    .prologue
    .line 221
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/forecaster/d;->e:Lcom/twitter/library/network/forecaster/NetworkQuality;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p1, v0, :cond_0

    .line 227
    :goto_0
    monitor-exit p0

    return-void

    .line 225
    :cond_0
    :try_start_1
    new-instance v0, Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;

    iget-object v1, p0, Lcom/twitter/library/network/forecaster/d;->e:Lcom/twitter/library/network/forecaster/NetworkQuality;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/network/forecaster/NetworkForecastChangedEvent;-><init>(Lcom/twitter/library/network/forecaster/NetworkQuality;Lcom/twitter/library/network/forecaster/NetworkQuality;)V

    invoke-virtual {p0, v0}, Lcom/twitter/library/network/forecaster/d;->a(Ljava/lang/Object;)V

    .line 226
    iput-object p1, p0, Lcom/twitter/library/network/forecaster/d;->e:Lcom/twitter/library/network/forecaster/NetworkQuality;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 221
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/twitter/library/network/forecaster/d;Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/twitter/library/network/forecaster/d;->a(Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/network/forecaster/d;Lcom/twitter/util/units/data/Data;Lcom/twitter/util/units/duration/Duration;Lcrm;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/network/forecaster/d;->a(Lcom/twitter/util/units/data/Data;Lcom/twitter/util/units/duration/Duration;Lcrm;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/network/forecaster/d;Lcom/twitter/util/units/duration/Duration;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/twitter/library/network/forecaster/d;->a(Lcom/twitter/util/units/duration/Duration;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/network/forecaster/d;Z)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/twitter/library/network/forecaster/d;->a(Z)V

    return-void
.end method

.method private declared-synchronized a(Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;)V
    .locals 2

    .prologue
    .line 146
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;->a()Lcom/twitter/util/connectivity/TwRadioType;

    move-result-object v0

    sget-object v1, Lcom/twitter/util/connectivity/TwRadioType;->a:Lcom/twitter/util/connectivity/TwRadioType;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/network/forecaster/d;->i:Z

    .line 147
    invoke-interface {p1}, Lcom/twitter/util/connectivity/TwConnectivityChangeEvent;->a()Lcom/twitter/util/connectivity/TwRadioType;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/library/network/forecaster/d;->a(Lcom/twitter/util/connectivity/TwRadioType;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    monitor-exit p0

    return-void

    .line 146
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lcom/twitter/util/connectivity/TwRadioType;)V
    .locals 5

    .prologue
    .line 164
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/twitter/library/network/forecaster/d;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    invoke-static {p1}, Lcom/twitter/library/network/forecaster/e;->b(Lcom/twitter/util/connectivity/TwRadioType;)Lcom/twitter/util/units/bitrate/KilobitsPerSecond;

    move-result-object v0

    move-object v1, v0

    .line 167
    :goto_0
    invoke-direct {p0}, Lcom/twitter/library/network/forecaster/d;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    invoke-static {p1}, Lcom/twitter/library/network/forecaster/e;->a(Lcom/twitter/util/connectivity/TwRadioType;)Lcom/twitter/util/units/bitrate/KilobitsPerSecond;

    move-result-object v0

    .line 170
    :goto_1
    new-instance v2, Lcrm;

    const/16 v3, 0xa

    sget-object v4, Lcom/twitter/library/network/forecaster/d;->b:Lcom/twitter/util/units/data/Kibibytes;

    invoke-direct {v2, v3, v4, v1}, Lcrm;-><init>(ILjava/lang/Number;Ljava/lang/Number;)V

    iput-object v2, p0, Lcom/twitter/library/network/forecaster/d;->f:Lcrm;

    .line 171
    new-instance v1, Lcrm;

    const/16 v2, 0xa

    sget-object v3, Lcom/twitter/library/network/forecaster/d;->b:Lcom/twitter/util/units/data/Kibibytes;

    invoke-direct {v1, v2, v3, v0}, Lcrm;-><init>(ILjava/lang/Number;Ljava/lang/Number;)V

    iput-object v1, p0, Lcom/twitter/library/network/forecaster/d;->g:Lcrm;

    .line 173
    new-instance v0, Lcrn;

    const/16 v1, 0xa

    sget-object v2, Lcom/twitter/library/network/forecaster/d;->c:Lcom/twitter/util/units/duration/Milliseconds;

    invoke-direct {v0, v1, v2}, Lcrn;-><init>(ILjava/lang/Number;)V

    iput-object v0, p0, Lcom/twitter/library/network/forecaster/d;->h:Lcrn;

    .line 175
    invoke-virtual {p0}, Lcom/twitter/library/network/forecaster/d;->a()Lcom/twitter/library/network/forecaster/NetworkQuality;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/library/network/forecaster/d;->a(Lcom/twitter/library/network/forecaster/NetworkQuality;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176
    monitor-exit p0

    return-void

    .line 165
    :cond_0
    :try_start_1
    sget-object v0, Lcom/twitter/util/units/bitrate/KilobitsPerSecond;->a:Lcom/twitter/util/units/bitrate/KilobitsPerSecond;

    move-object v1, v0

    goto :goto_0

    .line 168
    :cond_1
    sget-object v0, Lcom/twitter/util/units/bitrate/KilobitsPerSecond;->a:Lcom/twitter/util/units/bitrate/KilobitsPerSecond;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lcom/twitter/util/units/data/Data;Lcom/twitter/util/units/duration/Duration;Lcrm;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/units/data/Data;",
            "Lcom/twitter/util/units/duration/Duration;",
            "Lcrm",
            "<",
            "Lcom/twitter/util/units/data/Kibibytes;",
            "Lcom/twitter/util/units/bitrate/KilobitsPerSecond;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 190
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/twitter/library/network/forecaster/d;->a:Lcom/twitter/util/units/data/Kibibytes;

    invoke-virtual {p1, v2}, Lcom/twitter/util/units/data/Data;->a(Lcom/twitter/util/units/Unit;)I

    move-result v2

    if-gtz v2, :cond_1

    move v2, v0

    .line 191
    :goto_0
    sget-object v3, Lcom/twitter/util/units/duration/Milliseconds;->a:Lcom/twitter/util/units/duration/Milliseconds;

    invoke-virtual {p2, v3}, Lcom/twitter/util/units/duration/Duration;->a(Lcom/twitter/util/units/Unit;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-gtz v3, :cond_2

    .line 192
    :goto_1
    if-nez v2, :cond_0

    if-eqz v0, :cond_3

    .line 198
    :cond_0
    :goto_2
    monitor-exit p0

    return-void

    :cond_1
    move v2, v1

    .line 190
    goto :goto_0

    :cond_2
    move v0, v1

    .line 191
    goto :goto_1

    .line 196
    :cond_3
    :try_start_1
    new-instance v0, Lcom/twitter/util/units/data/Kibibytes;

    invoke-direct {v0, p1}, Lcom/twitter/util/units/data/Kibibytes;-><init>(Lcom/twitter/util/units/data/Data;)V

    new-instance v1, Lcom/twitter/util/units/bitrate/KilobitsPerSecond;

    invoke-direct {v1, p1, p2}, Lcom/twitter/util/units/bitrate/KilobitsPerSecond;-><init>(Lcom/twitter/util/units/data/Data;Lcom/twitter/util/units/duration/Duration;)V

    invoke-virtual {p3, v0, v1}, Lcrm;->a(Ljava/lang/Number;Ljava/lang/Number;)V

    .line 197
    invoke-virtual {p0}, Lcom/twitter/library/network/forecaster/d;->a()Lcom/twitter/library/network/forecaster/NetworkQuality;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/library/network/forecaster/d;->a(Lcom/twitter/library/network/forecaster/NetworkQuality;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 190
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lcom/twitter/util/units/duration/Duration;)V
    .locals 2

    .prologue
    .line 206
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/twitter/util/units/duration/Milliseconds;->a:Lcom/twitter/util/units/duration/Milliseconds;

    invoke-virtual {p1, v0}, Lcom/twitter/util/units/duration/Duration;->a(Lcom/twitter/util/units/Unit;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    .line 207
    :goto_0
    if-eqz v0, :cond_1

    .line 212
    :goto_1
    monitor-exit p0

    return-void

    .line 206
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 211
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/twitter/library/network/forecaster/d;->h:Lcrn;

    new-instance v1, Lcom/twitter/util/units/duration/Milliseconds;

    invoke-direct {v1, p1}, Lcom/twitter/util/units/duration/Milliseconds;-><init>(Lcom/twitter/util/units/duration/Duration;)V

    invoke-virtual {v0, v1}, Lcrn;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 206
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 151
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/twitter/library/network/forecaster/d;->j:Z

    .line 153
    invoke-virtual {p0}, Lcom/twitter/library/network/forecaster/d;->a()Lcom/twitter/library/network/forecaster/NetworkQuality;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/library/network/forecaster/d;->a(Lcom/twitter/library/network/forecaster/NetworkQuality;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    monitor-exit p0

    return-void

    .line 151
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lcom/twitter/library/network/forecaster/d;)Lcrm;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/library/network/forecaster/d;->g:Lcrm;

    return-object v0
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/twitter/library/network/forecaster/d;->i:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/network/forecaster/d;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized a()Lcom/twitter/library/network/forecaster/NetworkQuality;
    .locals 4

    .prologue
    .line 236
    monitor-enter p0

    .line 237
    :try_start_0
    invoke-direct {p0}, Lcom/twitter/library/network/forecaster/d;->f()Z

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/library/network/forecaster/d;->d()Lcom/twitter/util/units/bitrate/KilobitsPerSecond;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/network/forecaster/d;->e:Lcom/twitter/library/network/forecaster/NetworkQuality;

    .line 236
    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/forecaster/NetworkQuality;->a(ZLcom/twitter/util/units/bitrate/KilobitsPerSecond;Lcom/twitter/library/network/forecaster/NetworkQuality;)Lcom/twitter/library/network/forecaster/NetworkQuality;

    move-result-object v0

    .line 240
    invoke-direct {p0}, Lcom/twitter/library/network/forecaster/d;->f()Z

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/library/network/forecaster/d;->c()Lcom/twitter/util/units/bitrate/KilobitsPerSecond;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/library/network/forecaster/d;->e:Lcom/twitter/library/network/forecaster/NetworkQuality;

    .line 239
    invoke-static {v1, v2, v3}, Lcom/twitter/library/network/forecaster/NetworkQuality;->b(ZLcom/twitter/util/units/bitrate/KilobitsPerSecond;Lcom/twitter/library/network/forecaster/NetworkQuality;)Lcom/twitter/library/network/forecaster/NetworkQuality;

    move-result-object v1

    .line 242
    invoke-static {v0, v1}, Lcom/twitter/library/network/forecaster/NetworkQuality;->a(Lcom/twitter/library/network/forecaster/NetworkQuality;Lcom/twitter/library/network/forecaster/NetworkQuality;)Lcom/twitter/library/network/forecaster/NetworkQuality;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 236
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()Lcom/twitter/library/network/forecaster/NetworkQuality;
    .locals 1

    .prologue
    .line 252
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/forecaster/d;->e:Lcom/twitter/library/network/forecaster/NetworkQuality;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()Lcom/twitter/util/units/bitrate/KilobitsPerSecond;
    .locals 4

    .prologue
    .line 262
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/twitter/util/units/bitrate/KilobitsPerSecond;

    iget-object v1, p0, Lcom/twitter/library/network/forecaster/d;->g:Lcrm;

    invoke-virtual {v1}, Lcrm;->c()D

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/util/units/bitrate/KilobitsPerSecond;-><init>(D)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()Lcom/twitter/util/units/bitrate/KilobitsPerSecond;
    .locals 4

    .prologue
    .line 272
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/twitter/util/units/bitrate/KilobitsPerSecond;

    iget-object v1, p0, Lcom/twitter/library/network/forecaster/d;->f:Lcrm;

    invoke-virtual {v1}, Lcrm;->c()D

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/util/units/bitrate/KilobitsPerSecond;-><init>(D)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e()Lcom/twitter/util/units/duration/Milliseconds;
    .locals 4

    .prologue
    .line 282
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/twitter/util/units/duration/Milliseconds;

    iget-object v1, p0, Lcom/twitter/library/network/forecaster/d;->h:Lcrn;

    invoke-virtual {v1}, Lcrn;->c()D

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/util/units/duration/Milliseconds;-><init>(D)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
