.class public Lcom/twitter/library/network/n;
.super Lcom/twitter/network/apache/entity/a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/network/n$c;,
        Lcom/twitter/library/network/n$d;,
        Lcom/twitter/library/network/n$a;,
        Lcom/twitter/library/network/n$b;
    }
.end annotation


# instance fields
.field private final d:Lcom/twitter/network/s;

.field private final e:Landroid/content/Context;

.field private final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/library/network/n$b;",
            ">;"
        }
    .end annotation
.end field

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/network/s;)V
    .locals 3

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/twitter/network/apache/entity/a;-><init>()V

    .line 44
    iput-object p2, p0, Lcom/twitter/library/network/n;->d:Lcom/twitter/network/s;

    .line 45
    new-instance v0, Lcom/twitter/network/apache/message/BasicHeader;

    const-string/jumbo v1, "Content-Type"

    const-string/jumbo v2, "multipart/form-data; boundary=twitter"

    invoke-direct {v0, v1, v2}, Lcom/twitter/network/apache/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/library/network/n;->a:Lcom/twitter/network/apache/c;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/network/n;->f:Ljava/util/ArrayList;

    .line 48
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/n;->e:Landroid/content/Context;

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/network/apache/entity/a;->c:Z

    .line 50
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 59
    iget v0, p0, Lcom/twitter/library/network/n;->g:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public a(Ljava/io/OutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    new-instance v1, Lcom/twitter/network/a;

    iget v0, p0, Lcom/twitter/library/network/n;->g:I

    int-to-long v2, v0

    iget-object v0, p0, Lcom/twitter/library/network/n;->d:Lcom/twitter/network/s;

    invoke-direct {v1, p1, v2, v3, v0}, Lcom/twitter/network/a;-><init>(Ljava/io/OutputStream;JLcom/twitter/network/s;)V

    .line 77
    iget-object v0, p0, Lcom/twitter/library/network/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/network/n$b;

    .line 78
    invoke-interface {v0, v1}, Lcom/twitter/library/network/n$b;->a(Ljava/io/OutputStream;)V

    goto :goto_0

    .line 80
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "--twitter"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\r\n"

    .line 84
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "Content-Disposition: form-data; name=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 85
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\"; filename=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\""

    .line 86
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "Content-Type: image/jpeg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "Content-Transfer-Encoding: binary"

    .line 87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\r\n"

    .line 88
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 89
    const-string/jumbo v1, "\r\n"

    const-string/jumbo v2, "UTF-8"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    .line 91
    iget-object v2, p0, Lcom/twitter/library/network/n;->f:Ljava/util/ArrayList;

    .line 92
    new-instance v3, Lcom/twitter/library/network/n$a;

    invoke-direct {v3, v0}, Lcom/twitter/library/network/n$a;-><init>([B)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    new-instance v3, Lcom/twitter/library/network/n$d;

    iget-object v4, p0, Lcom/twitter/library/network/n;->e:Landroid/content/Context;

    invoke-direct {v3, v4, p3}, Lcom/twitter/library/network/n$d;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    new-instance v3, Lcom/twitter/library/network/n$a;

    invoke-direct {v3, v1}, Lcom/twitter/library/network/n$a;-><init>([B)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    :try_start_0
    iget v2, p0, Lcom/twitter/library/network/n;->g:I

    iget-object v3, p0, Lcom/twitter/library/network/n;->e:Landroid/content/Context;

    invoke-static {v3, p3}, Lcqc;->b(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v3

    array-length v0, v0

    add-int/2addr v0, v3

    array-length v1, v1

    add-int/2addr v0, v1

    add-int/2addr v0, v2

    iput v0, p0, Lcom/twitter/library/network/n;->g:I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    return-void

    .line 99
    :catch_0
    move-exception v0

    .line 100
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/io/IOException;

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/util/x;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "--twitter"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\r\n"

    .line 107
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "Content-Disposition: form-data; name=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 108
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\"; filename=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\""

    .line 109
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "Content-Transfer-Encoding: binary"

    .line 110
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\r\n"

    .line 111
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 112
    const-string/jumbo v1, "\r\n"

    const-string/jumbo v2, "UTF-8"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    .line 114
    iget-object v2, p0, Lcom/twitter/library/network/n;->f:Ljava/util/ArrayList;

    .line 115
    new-instance v3, Lcom/twitter/library/network/n$a;

    invoke-direct {v3, v0}, Lcom/twitter/library/network/n$a;-><init>([B)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    new-instance v3, Lcom/twitter/library/network/n$c;

    invoke-direct {v3, p3}, Lcom/twitter/library/network/n$c;-><init>(Lcom/twitter/library/util/x;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    new-instance v3, Lcom/twitter/library/network/n$a;

    invoke-direct {v3, v1}, Lcom/twitter/library/network/n$a;-><init>([B)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    iget v2, p0, Lcom/twitter/library/network/n;->g:I

    array-length v0, v0

    add-int/2addr v0, p4

    array-length v1, v1

    add-int/2addr v0, v1

    add-int/2addr v0, v2

    iput v0, p0, Lcom/twitter/library/network/n;->g:I

    .line 120
    return-void
.end method

.method public b()Ljava/io/InputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    new-instance v0, Lcom/twitter/library/network/o;

    iget-object v1, p0, Lcom/twitter/library/network/n;->f:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Lcom/twitter/library/network/o;-><init>(Ljava/util/ArrayList;)V

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return v0
.end method

.method public d()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    new-instance v0, Lcom/twitter/library/network/n$a;

    const-string/jumbo v1, "--twitter--\r\n"

    const-string/jumbo v2, "UTF-8"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/network/n$a;-><init>([B)V

    .line 143
    iget v1, p0, Lcom/twitter/library/network/n;->g:I

    invoke-static {v0}, Lcom/twitter/library/network/n$a;->a(Lcom/twitter/library/network/n$a;)[B

    move-result-object v2

    array-length v2, v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/twitter/library/network/n;->g:I

    .line 144
    iget-object v1, p0, Lcom/twitter/library/network/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    return-void
.end method
