.class public abstract Lcom/twitter/library/network/v;
.super Lcom/twitter/network/g;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/network/v$c;,
        Lcom/twitter/library/network/v$b;,
        Lcom/twitter/library/network/v$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/twitter/network/e;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/twitter/network/g;-><init>()V

    .line 31
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/v;->a:Landroid/content/Context;

    .line 32
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/network/r;)Lcom/twitter/util/network/g;
    .locals 3

    .prologue
    .line 69
    invoke-static {p0}, Lcom/twitter/library/network/z;->a(Landroid/content/Context;)V

    .line 71
    invoke-static {}, Lcom/twitter/util/network/a;->i()Ljava/security/Provider$Service;

    move-result-object v0

    .line 72
    invoke-static {p0}, Lcom/twitter/util/network/l;->a(Landroid/content/Context;)Ljava/security/KeyStore;

    move-result-object v1

    new-instance v2, Lciw;

    invoke-direct {v2}, Lciw;-><init>()V

    .line 71
    invoke-static {v0, v1, v2}, Lcom/twitter/util/network/a;->a(Ljava/security/Provider$Service;Ljava/security/KeyStore;Ljavax/net/ssl/HostnameVerifier;)Lcom/twitter/util/network/a;

    move-result-object v1

    .line 73
    invoke-virtual {v1}, Lcom/twitter/util/network/a;->a()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    .line 74
    instance-of v2, v0, Lcom/twitter/util/network/e;

    if-eqz v2, :cond_0

    .line 75
    check-cast v0, Lcom/twitter/util/network/e;

    .line 76
    new-instance v2, Lcom/twitter/library/network/v$1;

    invoke-direct {v2, v0}, Lcom/twitter/library/network/v$1;-><init>(Lcom/twitter/util/network/e;)V

    invoke-virtual {p1, v2}, Lcom/twitter/library/network/r;->a(Lcom/twitter/library/network/r$b;)V

    .line 100
    :cond_0
    return-object v1
.end method


# virtual methods
.method public declared-synchronized a(Lcom/twitter/network/HttpOperation$RequestMethod;Ljava/net/URI;)Lcom/twitter/network/e;
    .locals 2

    .prologue
    .line 37
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/v;->b:Lcom/twitter/network/e;

    if-nez v0, :cond_0

    .line 38
    invoke-static {}, Lbms;->ah()Lbms;

    move-result-object v0

    invoke-virtual {v0}, Lbms;->P()Lcom/twitter/util/network/g;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/network/v;->a:Landroid/content/Context;

    .line 39
    invoke-static {v1}, Lcom/twitter/library/provider/q;->c(Landroid/content/Context;)Lcom/twitter/network/f;

    move-result-object v1

    .line 38
    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/network/v;->a(Lcom/twitter/util/network/g;Lcom/twitter/network/f;)Lcom/twitter/network/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/v;->b:Lcom/twitter/network/e;

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/network/v;->b:Lcom/twitter/network/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract a(Lcom/twitter/util/network/g;Lcom/twitter/network/f;)Lcom/twitter/network/e;
.end method

.method public declared-synchronized a()V
    .locals 1

    .prologue
    .line 46
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/v;->b:Lcom/twitter/network/e;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/twitter/library/network/v;->b:Lcom/twitter/network/e;

    invoke-virtual {v0}, Lcom/twitter/network/e;->a()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/network/v;->b:Lcom/twitter/network/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    :cond_0
    monitor-exit p0

    return-void

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 1

    .prologue
    .line 54
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/v;->b:Lcom/twitter/network/e;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/twitter/library/network/v;->b:Lcom/twitter/network/e;

    invoke-virtual {v0}, Lcom/twitter/network/e;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    :cond_0
    monitor-exit p0

    return-void

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
