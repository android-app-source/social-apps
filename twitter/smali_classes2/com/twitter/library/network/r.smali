.class public Lcom/twitter/library/network/r;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/network/i;
.implements Lcqs$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/network/r$b;,
        Lcom/twitter/library/network/r$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/twitter/library/network/s;

.field private final c:Lbaa;

.field private final d:Lcqs;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/network/r$b;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lazy;

.field private volatile g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private volatile h:Ljava/lang/String;

.field private volatile i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/util/network/d;",
            ">;"
        }
    .end annotation
.end field

.field private volatile j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/util/network/d;",
            ">;"
        }
    .end annotation
.end field

.field private volatile k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/util/network/d;",
            "Lcom/twitter/util/network/d;",
            ">;"
        }
    .end annotation
.end field

.field private volatile l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-string/jumbo v0, "/1.1/help/settings.json"

    invoke-static {v0}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/network/r;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/network/s;Lbaa;Lcqs;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/network/r;->e:Ljava/util/List;

    .line 67
    new-instance v0, Lcom/twitter/library/network/r$a;

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/network/r$a;-><init>(Lcom/twitter/library/network/r;Lcom/twitter/library/network/r$1;)V

    iput-object v0, p0, Lcom/twitter/library/network/r;->f:Lazy;

    .line 68
    iput-object v1, p0, Lcom/twitter/library/network/r;->g:Ljava/util/Map;

    .line 74
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/network/r;->i:Ljava/util/Set;

    .line 79
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/network/r;->j:Ljava/util/Set;

    .line 88
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/network/r;->k:Ljava/util/Map;

    .line 89
    iput-object v1, p0, Lcom/twitter/library/network/r;->l:Ljava/util/Map;

    .line 94
    iput-object p1, p0, Lcom/twitter/library/network/r;->b:Lcom/twitter/library/network/s;

    .line 95
    iput-object p2, p0, Lcom/twitter/library/network/r;->c:Lbaa;

    .line 96
    iput-object p3, p0, Lcom/twitter/library/network/r;->d:Lcqs;

    .line 97
    invoke-direct {p0}, Lcom/twitter/library/network/r;->c()V

    .line 98
    return-void
.end method

.method private static a(Ljava/util/Map;Ljava/net/URI;Z)Lcom/twitter/library/network/ad;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/net/URI;",
            "Z)",
            "Lcom/twitter/library/network/ad;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/URISyntaxException;
        }
    .end annotation

    .prologue
    .line 314
    new-instance v1, Lcom/twitter/library/network/ad$a;

    invoke-direct {v1, p1}, Lcom/twitter/library/network/ad$a;-><init>(Ljava/net/URI;)V

    .line 315
    if-eqz p0, :cond_0

    .line 316
    invoke-virtual {p1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v2

    .line 317
    invoke-interface {p0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 318
    if-eqz v0, :cond_0

    .line 319
    invoke-static {p1, v0}, Lcom/twitter/util/ac;->a(Ljava/net/URI;Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/ad$a;->a(Ljava/net/URI;)Lcom/twitter/library/network/ad$a;

    .line 320
    if-eqz p2, :cond_0

    .line 321
    invoke-virtual {v1, v2}, Lcom/twitter/library/network/ad$a;->a(Ljava/lang/String;)Lcom/twitter/library/network/ad$a;

    .line 325
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/library/network/ad$a;->a()Lcom/twitter/library/network/ad;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/library/network/r;Ljava/util/Map;)Ljava/util/Map;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/twitter/library/network/r;->l:Ljava/util/Map;

    return-object p1
.end method

.method private static a(Ljava/util/List;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/util/network/d;",
            "Lcom/twitter/util/network/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 206
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v2

    .line 207
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 208
    const-string/jumbo v1, "\\|"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 209
    array-length v4, v1

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 211
    :try_start_0
    new-instance v4, Ljava/net/URI;

    const/4 v5, 0x0

    aget-object v5, v1, v5

    invoke-direct {v4, v5}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 212
    new-instance v5, Ljava/net/URI;

    const/4 v6, 0x1

    aget-object v1, v1, v6

    invoke-direct {v5, v1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 213
    new-instance v1, Lcom/twitter/util/network/d;

    invoke-direct {v1, v4}, Lcom/twitter/util/network/d;-><init>(Ljava/net/URI;)V

    new-instance v4, Lcom/twitter/util/network/d;

    invoke-direct {v4, v5}, Lcom/twitter/util/network/d;-><init>(Ljava/net/URI;)V

    invoke-virtual {v2, v1, v4}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 214
    :catch_0
    move-exception v1

    .line 215
    new-instance v4, Lcpb;

    invoke-direct {v4, v1}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    const-string/jumbo v1, "message"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Failure in parsing origin "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    goto :goto_0

    .line 219
    :cond_0
    new-instance v1, Lcpb;

    invoke-direct {v1}, Lcpb;-><init>()V

    const-string/jumbo v4, "message"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Failure in parsing origins "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    goto :goto_0

    .line 223
    :cond_1
    invoke-virtual {v2}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/library/network/r;Ljava/util/Set;)Ljava/util/Set;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/twitter/library/network/r;->i:Ljava/util/Set;

    return-object p1
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 55
    invoke-static {p0}, Lcom/twitter/library/network/r;->b(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 180
    const-string/jumbo v0, "spdy_aliases_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/network/r;->b:Lcom/twitter/library/network/s;

    .line 181
    invoke-virtual {v0}, Lcom/twitter/library/network/s;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 182
    :cond_0
    const-string/jumbo v0, "spdy_origin_aliases"

    .line 183
    invoke-static {v0}, Lcoj;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/network/r;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    .line 189
    :goto_0
    monitor-enter p0

    .line 190
    :try_start_0
    iget-object v1, p0, Lcom/twitter/library/network/r;->k:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 191
    iput-object v0, p0, Lcom/twitter/library/network/r;->k:Ljava/util/Map;

    .line 192
    invoke-direct {p0}, Lcom/twitter/library/network/r;->b()V

    .line 194
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 195
    return-void

    .line 185
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    .line 194
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/twitter/library/network/r;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/twitter/library/network/r;->a()V

    return-void
.end method

.method private a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 274
    iget-object v0, p0, Lcom/twitter/library/network/r;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/network/r$b;

    .line 275
    invoke-interface {v0, p1}, Lcom/twitter/library/network/r$b;->b(Ljava/util/Map;)V

    goto :goto_0

    .line 277
    :cond_0
    return-void
.end method

.method private static a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 256
    invoke-interface {p0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 257
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 262
    :goto_0
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 263
    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    :cond_0
    return-void

    .line 259
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 260
    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static b(Ljava/util/List;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 233
    new-instance v1, Ljava/util/HashMap;

    const/4 v0, 0x5

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 234
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 235
    const-string/jumbo v3, "\\|"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 236
    array-length v4, v3

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 237
    aget-object v0, v3, v7

    aget-object v4, v3, v8

    const/4 v5, 0x2

    aget-object v3, v3, v5

    invoke-static {v1, v0, v4, v3}, Lcom/twitter/library/network/r;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 238
    :cond_0
    array-length v4, v3

    if-ne v4, v8, :cond_1

    .line 240
    aget-object v0, v3, v7

    invoke-static {v1, v0, v9, v9}, Lcom/twitter/library/network/r;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 242
    :cond_1
    new-instance v3, Lcpb;

    invoke-direct {v3}, Lcpb;-><init>()V

    const-string/jumbo v4, "message"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Failure in parsing host map "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    goto :goto_0

    .line 246
    :cond_2
    return-object v1
.end method

.method static synthetic b(Lcom/twitter/library/network/r;Ljava/util/Set;)Ljava/util/Set;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/twitter/library/network/r;->j:Ljava/util/Set;

    return-object p1
.end method

.method private static b(Ljava/lang/String;)Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/util/network/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 282
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v2

    .line 284
    invoke-static {p0}, Lcoj;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 285
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 286
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 288
    :try_start_0
    new-instance v1, Lcom/twitter/util/network/d;

    new-instance v4, Ljava/net/URI;

    invoke-direct {v4, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v4}, Lcom/twitter/util/network/d;-><init>(Ljava/net/URI;)V

    invoke-virtual {v2, v1}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 289
    :catch_0
    move-exception v1

    .line 291
    new-instance v4, Lcpb;

    invoke-direct {v4, v1}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    const-string/jumbo v1, "message"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Failure in parsing origin set  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, " for feature switch "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    goto :goto_0

    .line 296
    :cond_0
    invoke-virtual {v2}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 268
    iget-object v0, p0, Lcom/twitter/library/network/r;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/network/r$b;

    .line 269
    iget-object v2, p0, Lcom/twitter/library/network/r;->k:Ljava/util/Map;

    invoke-interface {v0, v2}, Lcom/twitter/library/network/r$b;->a(Ljava/util/Map;)V

    goto :goto_0

    .line 271
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/twitter/library/network/r;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/twitter/library/network/r;->d()V

    return-void
.end method

.method static synthetic b(Lcom/twitter/library/network/r;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/twitter/library/network/r;->a(Ljava/util/Map;)V

    return-void
.end method

.method static synthetic c(Lcom/twitter/library/network/r;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/library/network/r;->l:Ljava/util/Map;

    return-object v0
.end method

.method private static c(Ljava/lang/String;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 330
    const-string/jumbo v0, "traffic_redirect_5347_hostmap"

    .line 331
    invoke-static {v0}, Lcoj;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 332
    invoke-static {v0}, Lcom/twitter/library/network/r;->b(Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 333
    if-eqz v0, :cond_0

    .line 334
    invoke-static {v0}, Lcom/twitter/util/collection/ImmutableMap;->a(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 336
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 341
    iget-object v0, p0, Lcom/twitter/library/network/r;->c:Lbaa;

    iget-object v1, p0, Lcom/twitter/library/network/r;->f:Lazy;

    invoke-virtual {v0, v1}, Lbaa;->a(Lazy;)V

    .line 342
    iget-object v0, p0, Lcom/twitter/library/network/r;->d:Lcqs;

    invoke-interface {v0, p0}, Lcqs;->a(Lcqs$a;)V

    .line 345
    const-string/jumbo v0, "spdy_origins"

    invoke-static {v0}, Lcon;->a(Ljava/lang/String;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/r$1;

    invoke-direct {v1, p0}, Lcom/twitter/library/network/r$1;-><init>(Lcom/twitter/library/network/r;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 354
    const-string/jumbo v0, "network_layer_no_proto_negotiation"

    invoke-static {v0}, Lcon;->a(Ljava/lang/String;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/r$2;

    invoke-direct {v1, p0}, Lcom/twitter/library/network/r$2;-><init>(Lcom/twitter/library/network/r;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 364
    const-string/jumbo v0, "spdy_aliases_enabled"

    invoke-static {v0}, Lcon;->a(Ljava/lang/String;)Lrx/c;

    move-result-object v0

    const-string/jumbo v1, "spdy_origin_aliases"

    .line 365
    invoke-static {v1}, Lcon;->a(Ljava/lang/String;)Lrx/c;

    move-result-object v1

    .line 364
    invoke-static {v0, v1}, Lrx/c;->c(Lrx/c;Lrx/c;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/r$3;

    invoke-direct {v1, p0}, Lcom/twitter/library/network/r$3;-><init>(Lcom/twitter/library/network/r;)V

    .line 366
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 376
    const-string/jumbo v0, "traffic_redirect_5347"

    invoke-static {v0}, Lcon;->a(Ljava/lang/String;)Lrx/c;

    move-result-object v0

    const-string/jumbo v1, "traffic_redirect_5347_hostmap"

    .line 377
    invoke-static {v1}, Lcon;->a(Ljava/lang/String;)Lrx/c;

    move-result-object v1

    .line 376
    invoke-static {v0, v1}, Lrx/c;->c(Lrx/c;Lrx/c;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/r$4;

    invoke-direct {v1, p0}, Lcom/twitter/library/network/r$4;-><init>(Lcom/twitter/library/network/r;)V

    .line 378
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 384
    return-void
.end method

.method static synthetic d(Lcom/twitter/library/network/r;)Lbaa;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/library/network/r;->c:Lbaa;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 387
    const-string/jumbo v0, "traffic_redirect_5347"

    .line 388
    invoke-static {v0}, Lcoi;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 389
    invoke-static {v0}, Lcoi;->g(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 390
    invoke-static {v0}, Lcom/twitter/library/network/r;->c(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/network/r;->g:Ljava/util/Map;

    .line 391
    iget-object v1, p0, Lcom/twitter/library/network/r;->g:Ljava/util/Map;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/network/r;->c:Lbaa;

    invoke-virtual {v1}, Lbaa;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 394
    const-string/jumbo v1, "traffic_redirect_5347"

    invoke-static {v1}, Lcoi;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 405
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/twitter/library/network/r;->g:Ljava/util/Map;

    invoke-direct {p0, v1}, Lcom/twitter/library/network/r;->a(Ljava/util/Map;)V

    .line 406
    iput-object v0, p0, Lcom/twitter/library/network/r;->h:Ljava/lang/String;

    .line 407
    return-void

    .line 397
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/library/network/r;->g:Ljava/util/Map;

    .line 398
    const-string/jumbo v1, "control"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/network/r;->c:Lbaa;

    .line 399
    invoke-virtual {v1}, Lbaa;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 402
    const-string/jumbo v1, "traffic_redirect_5347"

    invoke-static {v1}, Lcoi;->a(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method private d(Ljava/net/URI;)Z
    .locals 2

    .prologue
    .line 304
    sget-object v0, Lcom/twitter/library/network/r;->a:Ljava/util/Set;

    invoke-virtual {p1}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    const/4 v0, 0x0

    .line 307
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/network/r;->h:Ljava/lang/String;

    invoke-static {v0}, Lcoi;->h(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/net/URI;)Lcom/twitter/library/network/ad;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/URISyntaxException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 141
    iget-object v0, p0, Lcom/twitter/library/network/r;->c:Lbaa;

    invoke-virtual {v0}, Lbaa;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/twitter/library/network/r;->l:Ljava/util/Map;

    invoke-static {v0, p1, v1}, Lcom/twitter/library/network/r;->a(Ljava/util/Map;Ljava/net/URI;Z)Lcom/twitter/library/network/ad;

    move-result-object v0

    .line 149
    :goto_0
    return-object v0

    .line 143
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/library/network/r;->d(Ljava/net/URI;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/twitter/library/network/r;->g:Ljava/util/Map;

    invoke-static {v0, p1, v1}, Lcom/twitter/library/network/r;->a(Ljava/util/Map;Ljava/net/URI;Z)Lcom/twitter/library/network/ad;

    move-result-object v0

    goto :goto_0

    .line 149
    :cond_1
    new-instance v0, Lcom/twitter/library/network/ad$a;

    invoke-direct {v0, p1}, Lcom/twitter/library/network/ad$a;-><init>(Ljava/net/URI;)V

    invoke-virtual {v0}, Lcom/twitter/library/network/ad$a;->a()Lcom/twitter/library/network/ad;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/network/r$b;)V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/twitter/library/network/r;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    iget-object v0, p0, Lcom/twitter/library/network/r;->k:Ljava/util/Map;

    invoke-interface {p1, v0}, Lcom/twitter/library/network/r$b;->a(Ljava/util/Map;)V

    .line 163
    iget-object v0, p0, Lcom/twitter/library/network/r;->l:Ljava/util/Map;

    .line 164
    if-eqz v0, :cond_0

    .line 165
    invoke-interface {p1, v0}, Lcom/twitter/library/network/r$b;->b(Ljava/util/Map;)V

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/network/r;->g:Ljava/util/Map;

    .line 168
    if-eqz v0, :cond_1

    .line 169
    invoke-interface {p1, v0}, Lcom/twitter/library/network/r$b;->b(Ljava/util/Map;)V

    .line 171
    :cond_1
    return-void
.end method

.method public a(Lcqs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 106
    invoke-static {p2}, Lcom/twitter/library/network/s;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    invoke-direct {p0}, Lcom/twitter/library/network/r;->a()V

    .line 109
    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/library/network/r$b;)V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/twitter/library/network/r;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 175
    return-void
.end method

.method public b(Ljava/net/URI;)Z
    .locals 2

    .prologue
    .line 116
    new-instance v0, Lcom/twitter/util/network/d;

    invoke-direct {v0, p1}, Lcom/twitter/util/network/d;-><init>(Ljava/net/URI;)V

    .line 117
    iget-object v1, p0, Lcom/twitter/library/network/r;->i:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/network/r;->k:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/twitter/library/network/r;->a:Ljava/util/Set;

    .line 118
    invoke-virtual {p1}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 117
    :goto_0
    return v0

    .line 118
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Ljava/net/URI;)Z
    .locals 2

    .prologue
    .line 129
    sget-object v0, Lcom/twitter/library/network/r;->a:Ljava/util/Set;

    invoke-virtual {p1}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/network/r;->j:Ljava/util/Set;

    new-instance v1, Lcom/twitter/util/network/d;

    invoke-direct {v1, p1}, Lcom/twitter/util/network/d;-><init>(Ljava/net/URI;)V

    .line 130
    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 129
    :goto_0
    return v0

    .line 130
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
