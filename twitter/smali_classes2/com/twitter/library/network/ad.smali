.class public Lcom/twitter/library/network/ad;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/network/ad$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/net/URI;

.field public final b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/twitter/library/network/ad$a;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {p1}, Lcom/twitter/library/network/ad$a;->a(Lcom/twitter/library/network/ad$a;)Ljava/net/URI;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/ad;->a:Ljava/net/URI;

    .line 20
    invoke-static {p1}, Lcom/twitter/library/network/ad$a;->b(Lcom/twitter/library/network/ad$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/ad;->b:Ljava/lang/String;

    .line 21
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/library/network/ad$a;Lcom/twitter/library/network/ad$1;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/twitter/library/network/ad;-><init>(Lcom/twitter/library/network/ad$a;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 25
    if-ne p0, p1, :cond_1

    .line 32
    :cond_0
    :goto_0
    return v0

    .line 28
    :cond_1
    instance-of v2, p1, Lcom/twitter/library/network/ad;

    if-nez v2, :cond_2

    move v0, v1

    .line 29
    goto :goto_0

    .line 31
    :cond_2
    check-cast p1, Lcom/twitter/library/network/ad;

    .line 32
    iget-object v2, p0, Lcom/twitter/library/network/ad;->a:Ljava/net/URI;

    iget-object v3, p1, Lcom/twitter/library/network/ad;->a:Ljava/net/URI;

    invoke-virtual {v2, v3}, Ljava/net/URI;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/twitter/library/network/ad;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/network/ad;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/library/network/ad;->a:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->hashCode()I

    move-result v0

    .line 38
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/network/ad;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/twitter/library/network/ad;->a:Ljava/net/URI;

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 45
    iget-object v1, p0, Lcom/twitter/library/network/ad;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 46
    const-string/jumbo v1, "[host="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/network/ad;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
