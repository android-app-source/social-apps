.class public Lcom/twitter/library/network/s;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcqs;

.field private final c:Lcom/twitter/util/object/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/j",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/network/p;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-string/jumbo v0, "/1.1/help/settings.json"

    invoke-static {v0}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/network/s;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcqs;Lcom/twitter/util/object/j;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcqs;",
            "Lcom/twitter/util/object/j",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/network/p;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/twitter/library/network/s;->b:Lcqs;

    .line 61
    iput-object p2, p0, Lcom/twitter/library/network/s;->c:Lcom/twitter/util/object/j;

    .line 62
    return-void
.end method

.method public static a()Lcom/twitter/library/network/s;
    .locals 1

    .prologue
    .line 55
    invoke-static {}, Lbms;->ah()Lbms;

    move-result-object v0

    invoke-virtual {v0}, Lbms;->Q()Lcom/twitter/library/network/s;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 112
    const-string/jumbo v0, "network_simulation_profile"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "custom_network_simulation_bandwidth"

    .line 113
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "custom_network_simulation_latency"

    .line 114
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 112
    :goto_0
    return v0

    .line 114
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(ZII)Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x7530

    const/16 v2, 0x20

    const/4 v5, 0x1

    .line 180
    if-nez p0, :cond_1

    .line 198
    :cond_0
    return v5

    .line 183
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 184
    if-ge p1, v2, :cond_2

    .line 185
    const-string/jumbo v1, "Bandwidth limit must be greater than "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 186
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 188
    :cond_2
    if-lt p2, v5, :cond_3

    if-le p2, v6, :cond_5

    .line 189
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_4

    .line 190
    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    :cond_4
    const-string/jumbo v1, "Latency must be between %d and %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 193
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    .line 192
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    :cond_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 196
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public a(Lcom/twitter/library/network/p;)V
    .locals 4

    .prologue
    .line 151
    const/4 v0, 0x1

    .line 153
    invoke-virtual {p1}, Lcom/twitter/library/network/p;->b()I

    move-result v1

    .line 154
    invoke-virtual {p1}, Lcom/twitter/library/network/p;->c()I

    move-result v2

    .line 155
    invoke-virtual {p1}, Lcom/twitter/library/network/p;->a()Ljava/lang/String;

    move-result-object v3

    .line 151
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/library/network/s;->a(ZIILjava/lang/String;)V

    .line 156
    return-void
.end method

.method public a(ZIILjava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 136
    invoke-static {p1, p2, p3}, Lcom/twitter/library/network/s;->a(ZII)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/twitter/library/network/s;->b:Lcqs;

    invoke-interface {v0}, Lcqs;->a()Lcqs$b;

    move-result-object v0

    const-string/jumbo v1, "custom_network_simulation_bandwidth"

    .line 138
    invoke-interface {v0, v1, p2}, Lcqs$b;->a(Ljava/lang/String;I)Lcqs$b;

    move-result-object v0

    const-string/jumbo v1, "custom_network_simulation_latency"

    .line 140
    invoke-interface {v0, v1, p3}, Lcqs$b;->a(Ljava/lang/String;I)Lcqs$b;

    move-result-object v0

    const-string/jumbo v1, "network_simulation_profile"

    if-eqz p1, :cond_1

    .line 141
    :goto_0
    invoke-interface {v0, v1, p4}, Lcqs$b;->a(Ljava/lang/String;Ljava/lang/String;)Lcqs$b;

    move-result-object v0

    .line 143
    invoke-interface {v0}, Lcqs$b;->a()V

    .line 145
    :cond_0
    return-void

    .line 140
    :cond_1
    const-string/jumbo p4, "Disabled"

    goto :goto_0
.end method

.method public a(Ljava/net/URI;)Z
    .locals 2

    .prologue
    .line 121
    sget-object v0, Lcom/twitter/library/network/s;->a:Ljava/util/Set;

    invoke-virtual {p1}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/library/network/p;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/twitter/library/network/s;->c:Lcom/twitter/util/object/j;

    invoke-interface {v0}, Lcom/twitter/util/object/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/network/p;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 70
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/network/s;->b:Lcqs;

    const-string/jumbo v1, "network_simulation_profile"

    const-string/jumbo v2, "Disabled"

    .line 72
    invoke-interface {v0, v1, v2}, Lcqs;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 70
    :goto_0
    return-object v0

    .line 72
    :cond_0
    const-string/jumbo v0, "Disabled"

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/twitter/library/network/s;->d()Lcom/twitter/library/network/p;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Lcom/twitter/library/network/p;
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 90
    invoke-virtual {p0}, Lcom/twitter/library/network/s;->b()Ljava/lang/String;

    move-result-object v1

    .line 91
    const-string/jumbo v0, "Custom"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    iget-object v0, p0, Lcom/twitter/library/network/s;->b:Lcqs;

    const-string/jumbo v2, "custom_network_simulation_bandwidth"

    invoke-interface {v0, v2, v4}, Lcqs;->a(Ljava/lang/String;I)I

    move-result v2

    .line 94
    iget-object v0, p0, Lcom/twitter/library/network/s;->b:Lcqs;

    const-string/jumbo v3, "custom_network_simulation_latency"

    invoke-interface {v0, v3, v4}, Lcqs;->a(Ljava/lang/String;I)I

    move-result v3

    .line 96
    if-eq v2, v4, :cond_0

    if-eq v3, v4, :cond_0

    .line 97
    new-instance v0, Lcom/twitter/library/network/p;

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/network/p;-><init>(Ljava/lang/String;II)V

    .line 104
    :goto_0
    return-object v0

    .line 101
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 104
    :cond_1
    invoke-virtual {p0, v1}, Lcom/twitter/library/network/s;->b(Ljava/lang/String;)Lcom/twitter/library/network/p;

    move-result-object v0

    goto :goto_0
.end method

.method public e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 162
    const-string/jumbo v0, "Disabled"

    invoke-virtual {p0, v1, v1, v1, v0}, Lcom/twitter/library/network/s;->a(ZIILjava/lang/String;)V

    .line 163
    return-void
.end method

.method public f()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/twitter/library/network/p;",
            ">;"
        }
    .end annotation

    .prologue
    .line 170
    iget-object v0, p0, Lcom/twitter/library/network/s;->c:Lcom/twitter/util/object/j;

    invoke-interface {v0}, Lcom/twitter/util/object/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
