.class public Lcom/twitter/library/network/ab;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/network/ab$a;
    }
.end annotation


# static fields
.field private static h:Lcom/twitter/library/network/ab;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/twitter/library/network/ae;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            ">;>;"
        }
    .end annotation
.end field

.field private final j:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 555
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/ab;->j:Landroid/content/Context;

    .line 556
    invoke-static {p1}, Lcom/twitter/util/d;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/ab;->e:Ljava/lang/String;

    .line 557
    new-instance v0, Lcom/twitter/library/network/ae;

    invoke-static {p1}, Lcom/twitter/util/d;->b(Landroid/content/Context;)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/library/network/ab;->e:Ljava/lang/String;

    .line 558
    invoke-static {p1}, Lcom/twitter/library/network/ab;->b(Landroid/content/Context;)Z

    move-result v4

    invoke-direct {v0, p1, v2, v3, v4}, Lcom/twitter/library/network/ae;-><init>(Landroid/content/Context;Landroid/content/pm/PackageInfo;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/twitter/library/network/ab;->c:Lcom/twitter/library/network/ae;

    .line 559
    invoke-static {}, Lcoh;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/ab;->d:Ljava/lang/String;

    .line 560
    const-string/jumbo v0, "https://api.twitter.com"

    .line 561
    const-string/jumbo v2, "https://upload.twitter.com"

    .line 563
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v3

    invoke-virtual {v3}, Lcof;->p()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 564
    const-string/jumbo v3, "debug_prefs"

    .line 565
    invoke-virtual {p1, v3, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 566
    const-string/jumbo v4, "staging_enabled"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 567
    const-string/jumbo v4, "staging_url"

    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 568
    if-eqz v4, :cond_0

    .line 569
    const/16 v0, 0x2f

    invoke-virtual {v4, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    invoke-virtual {v4, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 572
    :cond_0
    const-string/jumbo v4, "upload_staging_enabled"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 573
    const-string/jumbo v4, "upload_staging_host"

    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 574
    if-eqz v1, :cond_1

    .line 579
    :goto_0
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    move-object v6, v2

    move-object v2, v0

    move-object v0, v6

    .line 581
    :goto_1
    iput-object v2, p0, Lcom/twitter/library/network/ab;->a:Ljava/lang/String;

    .line 582
    iput-object v1, p0, Lcom/twitter/library/network/ab;->b:Ljava/lang/String;

    .line 583
    invoke-virtual {p0}, Lcom/twitter/library/network/ab;->a()V

    .line 584
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "android_id"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/network/ab;->g:Ljava/lang/String;

    .line 586
    iput-object v0, p0, Lcom/twitter/library/network/ab;->i:Ljava/util/List;

    .line 587
    return-void

    :cond_1
    move-object v1, v2

    goto :goto_0

    :cond_2
    move-object v6, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v6

    goto :goto_1
.end method

.method public static a(II)I
    .locals 1

    .prologue
    .line 751
    rem-int v0, p0, p1

    if-lez v0, :cond_0

    .line 752
    div-int v0, p0, p1

    add-int/lit8 v0, v0, 0x1

    .line 754
    :goto_0
    return v0

    :cond_0
    div-int v0, p0, p1

    goto :goto_0
.end method

.method public static a(Lcom/twitter/network/HttpOperation;)Lcom/twitter/library/api/RateLimit;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 622
    :try_start_0
    const-string/jumbo v0, "x-rate-limit-limit"

    invoke-virtual {p0, v0}, Lcom/twitter/network/HttpOperation;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 623
    if-eqz v0, :cond_0

    .line 624
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 629
    const-string/jumbo v0, "x-rate-limit-remaining"

    invoke-virtual {p0, v0}, Lcom/twitter/network/HttpOperation;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 630
    if-eqz v0, :cond_1

    .line 631
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 636
    const-string/jumbo v0, "x-rate-limit-reset"

    invoke-virtual {p0, v0}, Lcom/twitter/network/HttpOperation;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 637
    if-eqz v0, :cond_2

    .line 638
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    .line 642
    new-instance v0, Lcom/twitter/library/api/RateLimit;

    invoke-direct {v0, v4, v3, v6, v7}, Lcom/twitter/library/api/RateLimit;-><init>(IIJ)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 652
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 626
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 633
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 640
    goto :goto_0

    .line 643
    :catch_0
    move-exception v0

    .line 644
    new-instance v3, Lcpb;

    invoke-direct {v3, v0}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    .line 645
    const-string/jumbo v0, "url"

    invoke-virtual {p0}, Lcom/twitter/network/HttpOperation;->i()Ljava/net/URI;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 646
    const-string/jumbo v0, "method"

    invoke-virtual {p0}, Lcom/twitter/network/HttpOperation;->h()Lcom/twitter/network/HttpOperation$RequestMethod;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/network/HttpOperation$RequestMethod;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 647
    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const-string/jumbo v0, "x-rate-limit-limit"

    aput-object v0, v4, v2

    const/4 v0, 0x1

    const-string/jumbo v5, "x-rate-limit-remaining"

    aput-object v5, v4, v0

    const/4 v0, 0x2

    const-string/jumbo v5, "x-rate-limit-reset"

    aput-object v5, v4, v0

    array-length v5, v4

    :goto_1
    if-ge v2, v5, :cond_4

    aget-object v6, v4, v2

    .line 648
    invoke-virtual {p0, v6}, Lcom/twitter/network/HttpOperation;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 649
    if-nez v0, :cond_3

    const-string/jumbo v0, "null"

    :cond_3
    invoke-virtual {v3, v6, v0}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 647
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 651
    :cond_4
    invoke-static {v3}, Lcpd;->c(Lcpb;)V

    move-object v0, v1

    .line 652
    goto :goto_0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/library/network/ab;
    .locals 3

    .prologue
    .line 591
    const-class v1, Lcom/twitter/library/network/ab;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/network/ab;->h:Lcom/twitter/library/network/ab;

    if-nez v0, :cond_0

    .line 592
    new-instance v0, Lcom/twitter/library/network/ab;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/library/network/ab;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/library/network/ab;->h:Lcom/twitter/library/network/ab;

    .line 593
    const-class v0, Lcom/twitter/library/network/ab;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 595
    :cond_0
    sget-object v0, Lcom/twitter/library/network/ab;->h:Lcom/twitter/library/network/ab;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 591
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;
    .locals 5

    .prologue
    .line 722
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 723
    if-eqz p1, :cond_0

    .line 724
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    .line 725
    const/16 v4, 0x2f

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 727
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-static {v3, v4}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 724
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 730
    :cond_0
    return-object v1
.end method

.method public static a(Lcom/twitter/network/HttpOperation;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 764
    invoke-virtual {p0, p1, p2}, Lcom/twitter/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/network/HttpOperation;

    .line 765
    return-void
.end method

.method public static a(Lcom/twitter/library/service/u;)Z
    .locals 1

    .prologue
    .line 670
    const/16 v0, 0x146

    invoke-static {p0, v0}, Lcom/twitter/library/network/ab;->a(Lcom/twitter/library/service/u;I)Z

    move-result v0

    return v0
.end method

.method public static a(Lcom/twitter/library/service/u;I)Z
    .locals 1

    .prologue
    .line 693
    invoke-static {p0, p1}, Lcom/twitter/library/network/ab;->b(Lcom/twitter/library/service/u;I)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/os/Bundle;)[I
    .locals 1

    .prologue
    .line 657
    const-string/jumbo v0, "custom_errors"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 658
    const-string/jumbo v0, "custom_errors"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    .line 660
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/model/core/z;->b:[I

    goto :goto_0
.end method

.method public static b(Lcom/twitter/library/service/u;I)Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/service/u;",
            "I)",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 700
    invoke-virtual {p0}, Lcom/twitter/library/service/u;->i()Lcom/twitter/library/service/r;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/library/service/a;

    if-eqz v0, :cond_0

    .line 701
    invoke-virtual {p0}, Lcom/twitter/library/service/u;->i()Lcom/twitter/library/service/r;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/a;

    invoke-virtual {v0}, Lcom/twitter/library/service/a;->a()Lcom/twitter/model/core/z;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/ab$1;

    invoke-direct {v1, p1}, Lcom/twitter/library/network/ab$1;-><init>(I)V

    invoke-static {v0, v1}, Lcpt;->a(Ljava/lang/Iterable;Lcpv;)Ljava/lang/Iterable;

    move-result-object v0

    .line 709
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/twitter/library/service/u;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 675
    invoke-static {p0}, Lcom/twitter/library/network/ab;->d(Lcom/twitter/library/service/u;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/y;

    .line 676
    iget-object v2, v0, Lcom/twitter/model/core/y;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 677
    iget-object v0, v0, Lcom/twitter/model/core/y;->g:Ljava/lang/String;

    .line 680
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 791
    invoke-static {}, Lcrt;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 793
    :cond_0
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 794
    const-string/jumbo v2, "debug_prod_ua"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 796
    :cond_1
    return v0
.end method

.method private b(Ljava/net/URI;)Z
    .locals 2

    .prologue
    .line 800
    invoke-virtual {p1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 801
    if-eqz v0, :cond_1

    const-string/jumbo v1, ".twitter.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "twitter.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/twitter/library/service/u;)Z
    .locals 2

    .prologue
    .line 684
    invoke-static {p0}, Lcom/twitter/library/network/ab;->d(Lcom/twitter/library/service/u;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/y;

    .line 685
    iget v0, v0, Lcom/twitter/model/core/y;->f:I

    if-lez v0, :cond_0

    .line 686
    const/4 v0, 0x1

    .line 689
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Lcom/twitter/library/service/u;)Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/service/u;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 666
    const/16 v0, 0x146

    invoke-static {p0, v0}, Lcom/twitter/library/network/ab;->b(Lcom/twitter/library/service/u;I)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public static d(Lcom/twitter/network/HttpOperation;)Z
    .locals 2

    .prologue
    .line 909
    const-string/jumbo v0, "True"

    const-string/jumbo v1, "X-Twitter-Polling"

    invoke-virtual {p0, v1}, Lcom/twitter/network/HttpOperation;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private h()Lcom/twitter/library/api/c;
    .locals 4

    .prologue
    .line 872
    iget-object v0, p0, Lcom/twitter/library/network/ab;->j:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 873
    const-string/jumbo v1, "adid_no_tracking_enabled"

    const/4 v2, 0x0

    .line 874
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 876
    const-string/jumbo v2, "adid_identifier"

    const-string/jumbo v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 878
    invoke-static {v2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 879
    new-instance v0, Lcom/twitter/library/api/c;

    invoke-direct {v0, v2, v1}, Lcom/twitter/library/api/c;-><init>(Ljava/lang/String;Z)V

    .line 881
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/net/URI;)Ljava/util/HashMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URI;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 811
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 812
    const-string/jumbo v0, "User-Agent"

    iget-object v2, p0, Lcom/twitter/library/network/ab;->c:Lcom/twitter/library/network/ae;

    invoke-virtual {v2}, Lcom/twitter/library/network/ae;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 813
    const-string/jumbo v0, "X-Client-UUID"

    iget-object v2, p0, Lcom/twitter/library/network/ab;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 814
    const-string/jumbo v0, "X-Twitter-Client"

    const-string/jumbo v2, "TwitterAndroid"

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 815
    const-string/jumbo v0, "X-Twitter-Client-Version"

    iget-object v2, p0, Lcom/twitter/library/network/ab;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 816
    const-string/jumbo v0, "X-Twitter-API-Version"

    const-string/jumbo v2, "5"

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 817
    const-string/jumbo v0, "Accept-Language"

    iget-object v2, p0, Lcom/twitter/library/network/ab;->f:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 818
    const-string/jumbo v0, "X-Twitter-Client-Language"

    iget-object v2, p0, Lcom/twitter/library/network/ab;->f:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 819
    const-string/jumbo v0, "X-Twitter-Client-DeviceID"

    iget-object v2, p0, Lcom/twitter/library/network/ab;->g:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 820
    const-string/jumbo v0, "X-Twitter-Active"

    invoke-static {}, Lcom/twitter/util/android/b;->a()Lcom/twitter/util/android/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/util/android/b;->b()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 821
    invoke-direct {p0, p1}, Lcom/twitter/library/network/ab;->b(Ljava/net/URI;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 822
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/twitter/util/y;->b(I)Ljava/lang/String;

    move-result-object v0

    .line 823
    const-string/jumbo v2, "X-B3-TraceId"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 824
    invoke-virtual {p0}, Lcom/twitter/library/network/ab;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 825
    const-string/jumbo v2, "X-B3-Flags"

    const-string/jumbo v3, "1"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 826
    const-string/jumbo v2, "X-B3-SpanId"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 827
    iget-object v2, p0, Lcom/twitter/library/network/ab;->i:Ljava/util/List;

    const/4 v3, 0x0

    new-instance v4, Landroid/util/Pair;

    invoke-direct {v4, v0, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v2, v3, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 828
    iget-object v2, p0, Lcom/twitter/library/network/ab;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/16 v3, 0xa

    if-le v2, v3, :cond_0

    .line 829
    iget-object v2, p0, Lcom/twitter/library/network/ab;->i:Ljava/util/List;

    iget-object v3, p0, Lcom/twitter/library/network/ab;->i:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 831
    :cond_0
    const-string/jumbo v2, "TwitterAPI"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "TraceID "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, " for ["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 834
    :cond_1
    invoke-static {}, Lcom/twitter/library/network/s;->a()Lcom/twitter/library/network/s;

    move-result-object v0

    .line 835
    invoke-virtual {v0}, Lcom/twitter/library/network/s;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, p1}, Lcom/twitter/library/network/s;->a(Ljava/net/URI;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 836
    invoke-virtual {v0}, Lcom/twitter/library/network/s;->d()Lcom/twitter/library/network/p;

    move-result-object v0

    .line 837
    const-string/jumbo v2, "x-tsa-max-connection-bandwidth-kbs"

    .line 838
    invoke-virtual {v0}, Lcom/twitter/library/network/p;->b()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    .line 837
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 839
    const-string/jumbo v2, "x-tsa-fixed-request-latency-ms"

    .line 840
    invoke-virtual {v0}, Lcom/twitter/library/network/p;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 839
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 843
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/library/network/ab;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 844
    const-string/jumbo v0, "Dtab-Local"

    invoke-virtual {p0}, Lcom/twitter/library/network/ab;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 846
    :cond_3
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v0

    invoke-virtual {v0}, Lbqg;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 847
    const-string/jumbo v0, "Geolocation"

    iget-object v2, p0, Lcom/twitter/library/network/ab;->j:Landroid/content/Context;

    .line 848
    invoke-static {v2}, Lbqn;->a(Landroid/content/Context;)Lbqn;

    move-result-object v2

    invoke-virtual {v2}, Lbqn;->b()Ljava/lang/String;

    move-result-object v2

    .line 847
    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 851
    :cond_4
    invoke-direct {p0}, Lcom/twitter/library/network/ab;->h()Lcom/twitter/library/api/c;

    move-result-object v0

    .line 852
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/twitter/library/api/c;->b()Z

    move-result v2

    if-nez v2, :cond_6

    .line 853
    :cond_5
    const-string/jumbo v2, "Timezone"

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 855
    :cond_6
    if-eqz v0, :cond_7

    .line 856
    const-string/jumbo v2, "X-Twitter-Client-AdID"

    invoke-virtual {v0}, Lcom/twitter/library/api/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 857
    const-string/jumbo v2, "X-Twitter-Client-Limit-Ad-Tracking"

    invoke-virtual {v0}, Lcom/twitter/library/api/c;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string/jumbo v0, "1"

    :goto_0
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 861
    :cond_7
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->p()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 862
    iget-object v0, p0, Lcom/twitter/library/network/ab;->j:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 863
    const-string/jumbo v2, "simulate_back_pressure"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 864
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 865
    const-string/jumbo v2, "Simulate-Back-Pressure"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 868
    :cond_8
    return-object v1

    .line 857
    :cond_9
    const-string/jumbo v0, "0"

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Lcom/twitter/library/network/ab;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Lcom/twitter/util/b;->c(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/ab;->f:Ljava/lang/String;

    .line 605
    return-void
.end method

.method public a(Lcom/twitter/library/service/d$a;)V
    .locals 4

    .prologue
    .line 734
    iget-object v0, p0, Lcom/twitter/library/network/ab;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 735
    if-eqz v0, :cond_2

    .line 736
    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    .line 737
    invoke-static {v0}, Lcom/twitter/util/b;->b(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 738
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 739
    :cond_0
    const-string/jumbo v2, "localize"

    const/4 v3, 0x1

    invoke-virtual {p1, v2, v3}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/d$a;

    .line 740
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 741
    const-string/jumbo v2, "lang"

    invoke-virtual {p1, v2, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 743
    :cond_1
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 744
    const-string/jumbo v0, "country"

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 748
    :cond_2
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 760
    iget-object v0, p0, Lcom/twitter/library/network/ab;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b(Lcom/twitter/network/HttpOperation;)V
    .locals 3

    .prologue
    .line 890
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->i()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/network/ab;->a(Ljava/net/URI;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 891
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Lcom/twitter/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/network/HttpOperation;

    goto :goto_0

    .line 893
    :cond_0
    return-void
.end method

.method public c(Lcom/twitter/network/HttpOperation;)V
    .locals 2

    .prologue
    .line 904
    const-string/jumbo v0, "X-Twitter-Polling"

    const-string/jumbo v1, "True"

    invoke-virtual {p1, v0, v1}, Lcom/twitter/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/network/HttpOperation;

    .line 905
    invoke-virtual {p0, p1}, Lcom/twitter/library/network/ab;->b(Lcom/twitter/network/HttpOperation;)V

    .line 906
    return-void
.end method

.method public c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 768
    invoke-static {}, Lcrt;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 769
    :cond_0
    iget-object v1, p0, Lcom/twitter/library/network/ab;->j:Landroid/content/Context;

    .line 770
    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 771
    const-string/jumbo v2, "extra_dtab_enabled"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 773
    :cond_1
    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 777
    iget-object v0, p0, Lcom/twitter/library/network/ab;->j:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 778
    const-string/jumbo v1, "extra_dtab"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 782
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->p()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 783
    iget-object v1, p0, Lcom/twitter/library/network/ab;->j:Landroid/content/Context;

    .line 784
    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 785
    const-string/jumbo v2, "debug_force_zipkin_tracing"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 787
    :cond_0
    return v0
.end method

.method public f()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 805
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->p()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/network/ab;->j:Landroid/content/Context;

    .line 806
    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "nullcast_tweets"

    .line 807
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 805
    :cond_0
    return v0
.end method

.method public g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 900
    iget-object v0, p0, Lcom/twitter/library/network/ab;->i:Ljava/util/List;

    return-object v0
.end method
