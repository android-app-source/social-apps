.class public final Lcom/twitter/library/card/ab;
.super Lcom/twitter/library/widget/renderablecontent/a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/widget/renderablecontent/a",
        "<",
        "Lcom/twitter/library/card/CardContext;",
        "Lcom/twitter/library/card/z;",
        ">;"
    }
.end annotation


# instance fields
.field protected final a:Lcom/twitter/library/card/CardContext;

.field final b:Ljava/lang/String;

.field final c:J

.field final d:Lcar;

.field final e:Lcom/twitter/library/card/m;

.field f:Landroid/graphics/Rect;

.field g:Lcom/twitter/library/card/m$b;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/twitter/library/card/CardContext;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Ljava/lang/String;JLcar;Lcom/twitter/library/card/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 7

    .prologue
    .line 127
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p9

    move-object/from16 v5, p10

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/widget/renderablecontent/a;-><init>(Landroid/app/Activity;Ljava/lang/Object;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 128
    iput-object p2, p0, Lcom/twitter/library/card/ab;->a:Lcom/twitter/library/card/CardContext;

    .line 129
    iput-object p4, p0, Lcom/twitter/library/card/ab;->b:Ljava/lang/String;

    .line 130
    iput-wide p5, p0, Lcom/twitter/library/card/ab;->c:J

    .line 131
    iput-object p7, p0, Lcom/twitter/library/card/ab;->d:Lcar;

    .line 132
    iput-object p8, p0, Lcom/twitter/library/card/ab;->e:Lcom/twitter/library/card/m;

    .line 133
    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/twitter/library/card/CardContext;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/library/card/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/card/ab;
    .locals 18

    .prologue
    .line 60
    move-object/from16 v0, p0

    instance-of v2, v0, Lcom/twitter/app/common/util/j;

    if-nez v2, :cond_1

    .line 61
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v2

    invoke-virtual {v2}, Lcof;->p()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 62
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Not assignable from LifecycleAwareActivity"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 64
    :cond_0
    const/4 v7, 0x0

    .line 108
    :goto_0
    return-object v7

    .line 67
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/card/CardContext;->a()Lcax;

    move-result-object v9

    .line 68
    if-nez v9, :cond_2

    .line 69
    const/4 v7, 0x0

    goto :goto_0

    .line 72
    :cond_2
    invoke-virtual {v9}, Lcax;->b()Ljava/lang/String;

    move-result-object v11

    .line 75
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/card/CardContext;->d()J

    move-result-wide v4

    .line 76
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/card/CardContext;->c()J

    move-result-wide v2

    .line 79
    invoke-virtual {v9}, Lcax;->K()Lcar;

    move-result-object v14

    .line 82
    invoke-static {}, Lcom/twitter/library/card/p;->a()Lcom/twitter/library/card/p;

    move-result-object v6

    .line 83
    move-object/from16 v0, p1

    invoke-virtual {v6, v2, v3, v0}, Lcom/twitter/library/card/p;->c(JLjava/lang/Object;)V

    .line 85
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v2

    .line 86
    invoke-virtual {v2, v4, v5, v14}, Lcom/twitter/library/card/q;->c(JLjava/lang/Object;)V

    .line 88
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/card/CardContext;->b()Lcaq;

    move-result-object v6

    .line 89
    if-eqz v6, :cond_3

    .line 90
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    .line 91
    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 92
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/twitter/library/card/s;->a(J)Lcom/twitter/library/card/s;

    move-result-object v3

    .line 95
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Lcom/twitter/library/card/s;->a(JLjava/lang/Object;Ljava/lang/Object;Z)V

    .line 98
    :cond_3
    invoke-static {}, Lcom/twitter/library/card/ak;->a()Lcom/twitter/library/card/ak;

    move-result-object v3

    .line 99
    invoke-virtual {v9}, Lcax;->d()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 100
    invoke-virtual {v9}, Lcax;->d()Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/core/TwitterUser;

    .line 101
    iget-wide v12, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v3, v12, v13, v2}, Lcom/twitter/library/card/ak;->c(JLjava/lang/Object;)V

    goto :goto_1

    .line 104
    :cond_4
    invoke-static {}, Lcom/twitter/library/card/ae;->b()Lcom/twitter/library/card/ae;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v11, v0, v14}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 106
    const/4 v7, 0x0

    goto :goto_0

    .line 108
    :cond_5
    new-instance v7, Lcom/twitter/library/card/ab;

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-wide v12, v4

    move-object/from16 v15, p3

    move-object/from16 v16, p4

    move-object/from16 v17, p5

    invoke-direct/range {v7 .. v17}, Lcom/twitter/library/card/ab;-><init>(Landroid/app/Activity;Lcom/twitter/library/card/CardContext;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Ljava/lang/String;JLcar;Lcom/twitter/library/card/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;IIII)Landroid/graphics/Rect;
    .locals 3

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/twitter/library/card/ab;->d()Landroid/view/View;

    move-result-object v0

    .line 204
    if-nez v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/twitter/library/card/ab;->a:Lcom/twitter/library/card/CardContext;

    invoke-virtual {v0}, Lcom/twitter/library/card/CardContext;->a()Lcax;

    move-result-object v0

    .line 207
    invoke-static {}, Lcom/twitter/util/z;->b()F

    move-result v1

    invoke-virtual {v0, v1}, Lcax;->a(F)I

    move-result v0

    .line 209
    add-int v1, p2, p4

    .line 210
    add-int/2addr v0, p3

    .line 218
    :goto_0
    iget-object v2, p0, Lcom/twitter/library/card/ab;->f:Landroid/graphics/Rect;

    if-nez v2, :cond_1

    .line 219
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, p2, p3, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lcom/twitter/library/card/ab;->f:Landroid/graphics/Rect;

    .line 224
    :goto_1
    iget-object v0, p0, Lcom/twitter/library/card/ab;->f:Landroid/graphics/Rect;

    return-object v0

    .line 212
    :cond_0
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {p4, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 213
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 214
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, p2

    .line 215
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, p3

    goto :goto_0

    .line 221
    :cond_1
    iget-object v2, p0, Lcom/twitter/library/card/ab;->f:Landroid/graphics/Rect;

    invoke-virtual {v2, p2, p3, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1
.end method

.method protected synthetic a(Landroid/app/Activity;)Lcom/twitter/library/widget/renderablecontent/c;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/twitter/library/card/ab;->b(Landroid/app/Activity;)Lcom/twitter/library/card/z;

    move-result-object v0

    return-object v0
.end method

.method protected a()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 143
    new-instance v1, Lcom/twitter/library/card/z$a;

    iget-object v0, p0, Lcom/twitter/library/card/ab;->a:Lcom/twitter/library/card/CardContext;

    .line 144
    invoke-virtual {v0}, Lcom/twitter/library/card/CardContext;->c()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/twitter/library/card/ab;->c:J

    iget-object v6, p0, Lcom/twitter/library/card/ab;->d:Lcar;

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/card/z$a;-><init>(JJLcar;)V

    const-string/jumbo v0, "params_extra_scribe_association"

    iget-object v2, p0, Lcom/twitter/library/card/ab;->i:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 145
    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/card/z$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/twitter/library/card/z$a;

    move-result-object v0

    const-string/jumbo v1, "params_extra_source_scribe_association"

    iget-object v2, p0, Lcom/twitter/library/card/ab;->j:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 146
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/card/z$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/twitter/library/card/z$a;

    move-result-object v0

    .line 143
    return-object v0
.end method

.method protected b(Landroid/app/Activity;)Lcom/twitter/library/card/z;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 152
    iget-object v1, p0, Lcom/twitter/library/card/ab;->a:Lcom/twitter/library/card/CardContext;

    invoke-virtual {v1}, Lcom/twitter/library/card/CardContext;->a()Lcax;

    move-result-object v1

    .line 153
    if-nez v1, :cond_1

    .line 173
    :cond_0
    :goto_0
    return-object v0

    .line 156
    :cond_1
    invoke-static {}, Lcom/twitter/library/card/ae;->b()Lcom/twitter/library/card/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/card/ab;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/library/card/ab;->n:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Lcom/twitter/library/card/aa;

    move-result-object v2

    .line 157
    if-eqz v2, :cond_0

    .line 162
    iget-object v1, p0, Lcom/twitter/library/card/ab;->e:Lcom/twitter/library/card/m;

    if-nez v1, :cond_2

    move-object v1, v0

    .line 164
    :goto_1
    if-nez v1, :cond_3

    .line 166
    :goto_2
    if-eqz v0, :cond_4

    .line 169
    :goto_3
    if-eqz v1, :cond_0

    .line 170
    iget-object v3, p0, Lcom/twitter/library/card/ab;->n:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    iget-object v4, p0, Lcom/twitter/library/card/ab;->d:Lcar;

    invoke-virtual {v2, p1, v3, v4}, Lcom/twitter/library/card/aa;->c(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Lcom/twitter/library/card/m$b;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/library/card/ab;->g:Lcom/twitter/library/card/m$b;

    .line 171
    invoke-virtual {v0, v1}, Lcom/twitter/library/card/z;->a(Lcom/twitter/library/card/n;)V

    goto :goto_0

    .line 162
    :cond_2
    iget-object v1, p0, Lcom/twitter/library/card/ab;->n:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    iget-object v3, p0, Lcom/twitter/library/card/ab;->d:Lcar;

    invoke-virtual {v2, p1, v1, v3}, Lcom/twitter/library/card/aa;->b(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Lcom/twitter/library/card/n;

    move-result-object v1

    goto :goto_1

    .line 164
    :cond_3
    iget-object v0, p0, Lcom/twitter/library/card/ab;->e:Lcom/twitter/library/card/m;

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/m;->a(Lcom/twitter/library/card/n;)Lcom/twitter/library/card/z;

    move-result-object v0

    goto :goto_2

    .line 166
    :cond_4
    iget-object v0, p0, Lcom/twitter/library/card/ab;->n:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    iget-object v3, p0, Lcom/twitter/library/card/ab;->d:Lcar;

    .line 167
    invoke-virtual {v2, p1, v0, v3}, Lcom/twitter/library/card/aa;->a(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Lcom/twitter/library/card/z;

    move-result-object v0

    goto :goto_3
.end method

.method public bh_()V
    .locals 4

    .prologue
    .line 179
    iget-boolean v1, p0, Lcom/twitter/library/card/ab;->m:Z

    .line 180
    iget-object v0, p0, Lcom/twitter/library/card/ab;->l:Lcom/twitter/library/widget/renderablecontent/c;

    check-cast v0, Lcom/twitter/library/card/z;

    .line 182
    invoke-super {p0}, Lcom/twitter/library/widget/renderablecontent/a;->bh_()V

    .line 185
    iget-object v2, p0, Lcom/twitter/library/card/ab;->e:Lcom/twitter/library/card/m;

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 186
    invoke-virtual {v0}, Lcom/twitter/library/card/z;->r()Lcom/twitter/library/card/n;

    move-result-object v1

    .line 187
    if-eqz v1, :cond_0

    .line 188
    iget-object v2, p0, Lcom/twitter/library/card/ab;->g:Lcom/twitter/library/card/m$b;

    if-nez v2, :cond_1

    .line 189
    iget-object v2, p0, Lcom/twitter/library/card/ab;->e:Lcom/twitter/library/card/m;

    invoke-virtual {v2, v1, v0}, Lcom/twitter/library/card/m;->a(Lcom/twitter/library/card/n;Lcom/twitter/library/card/z;)Z

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 191
    :cond_1
    iget-object v2, p0, Lcom/twitter/library/card/ab;->e:Lcom/twitter/library/card/m;

    iget-object v3, p0, Lcom/twitter/library/card/ab;->g:Lcom/twitter/library/card/m$b;

    invoke-virtual {v2, v1, v0, v3}, Lcom/twitter/library/card/m;->a(Lcom/twitter/library/card/n;Lcom/twitter/library/card/z;Lcom/twitter/library/card/m$b;)Z

    goto :goto_0
.end method
