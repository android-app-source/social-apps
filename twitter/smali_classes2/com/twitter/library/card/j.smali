.class public Lcom/twitter/library/card/j;
.super Lcom/twitter/library/card/t;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/card/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/card/j$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/card/t",
        "<",
        "Lcom/twitter/library/card/j$a;",
        "Lcom/twitter/library/card/d",
        "<",
        "Lcar;",
        ">;>;",
        "Lcom/twitter/library/card/b;"
    }
.end annotation


# static fields
.field private static c:Lcom/twitter/library/card/j;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/twitter/library/card/t;-><init>()V

    .line 20
    return-void
.end method

.method public static declared-synchronized a()Lcom/twitter/library/card/j;
    .locals 2

    .prologue
    .line 23
    const-class v1, Lcom/twitter/library/card/j;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/card/j;->c:Lcom/twitter/library/card/j;

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Lcom/twitter/library/card/j;

    invoke-direct {v0}, Lcom/twitter/library/card/j;-><init>()V

    sput-object v0, Lcom/twitter/library/card/j;->c:Lcom/twitter/library/card/j;

    .line 25
    const-class v0, Lcom/twitter/library/card/j;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 27
    :cond_0
    sget-object v0, Lcom/twitter/library/card/j;->c:Lcom/twitter/library/card/j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 23
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(Landroid/content/Context;)Lcom/twitter/library/card/a;
    .locals 1

    .prologue
    .line 58
    new-instance v0, Lcom/twitter/library/card/h;

    invoke-direct {v0, p1, p0}, Lcom/twitter/library/card/h;-><init>(Landroid/content/Context;Lcom/twitter/library/card/b;)V

    return-object v0
.end method

.method public a(JI)V
    .locals 3

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/library/card/j;->a:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/t$a;

    .line 70
    if-eqz v0, :cond_0

    .line 71
    iget-object v0, v0, Lcom/twitter/library/card/t$a;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/j$a;

    .line 72
    invoke-interface {v0, p1, p2, p3}, Lcom/twitter/library/card/j$a;->a(JI)V

    goto :goto_0

    .line 75
    :cond_0
    return-void
.end method

.method public a(JILcar;)V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/twitter/library/card/d;

    invoke-direct {v0, p3, p4}, Lcom/twitter/library/card/d;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/library/card/j;->c(JLjava/lang/Object;)V

    .line 64
    return-void
.end method

.method protected a(Lcom/twitter/library/card/j$a;JLcom/twitter/library/card/d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/card/j$a;",
            "J",
            "Lcom/twitter/library/card/d",
            "<",
            "Lcar;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 52
    iget v1, p4, Lcom/twitter/library/card/d;->a:I

    iget-object v0, p4, Lcom/twitter/library/card/d;->b:Ljava/lang/Object;

    check-cast v0, Lcar;

    invoke-interface {p1, p2, p3, v1, v0}, Lcom/twitter/library/card/j$a;->a(JILcar;)V

    .line 53
    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;JLjava/lang/Object;)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Lcom/twitter/library/card/j$a;

    check-cast p4, Lcom/twitter/library/card/d;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/library/card/j;->a(Lcom/twitter/library/card/j$a;JLcom/twitter/library/card/d;)V

    return-void
.end method
