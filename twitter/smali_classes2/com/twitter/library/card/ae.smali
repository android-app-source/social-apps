.class public Lcom/twitter/library/card/ae;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcoj$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/card/ae$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field private b:Z

.field private c:Z

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/widget/renderablecontent/DisplayMode;",
            ">;",
            "Lcom/twitter/library/card/ae$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-string/jumbo v0, "\\W"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/card/ae;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/ae;->d:Ljava/util/Map;

    .line 86
    const-string/jumbo v0, "card_registry_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/card/ae;->b:Z

    .line 87
    const-string/jumbo v0, "cards_forward_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/card/ae;->c:Z

    .line 88
    invoke-static {p0}, Lcoj;->a(Lcoj$a;)V

    .line 89
    return-void
.end method

.method public static b()Lcom/twitter/library/card/ae;
    .locals 1

    .prologue
    .line 93
    invoke-static {}, Lbms;->ah()Lbms;

    move-result-object v0

    invoke-virtual {v0}, Lbms;->Z()Lcom/twitter/library/card/ae;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 208
    const-string/jumbo v1, "card_registry_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    sget-object v1, Lcom/twitter/library/card/ae;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    const-string/jumbo v2, "_"

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    sget-object v1, Lcom/twitter/library/card/ae$1;->a:[I

    invoke-virtual {p1}, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 247
    :goto_0
    const-string/jumbo v1, "_enabled"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 212
    :pswitch_0
    const-string/jumbo v1, "_forward"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 216
    :pswitch_1
    const-string/jumbo v1, "_full"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 220
    :pswitch_2
    const-string/jumbo v1, "_compose"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 224
    :pswitch_3
    const-string/jumbo v1, "_direct_message"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 228
    :pswitch_4
    const-string/jumbo v1, "_direct_message_compose"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 232
    :pswitch_5
    const-string/jumbo v1, "_carousel"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 236
    :pswitch_6
    const-string/jumbo v1, "_moments"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 240
    :pswitch_7
    const-string/jumbo v1, "_profile"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 210
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private static c(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Z
    .locals 1

    .prologue
    .line 200
    invoke-static {}, Lcom/twitter/library/card/v;->a()Lcom/twitter/library/card/v;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/twitter/library/card/v;->a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Lcom/twitter/library/card/aa;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 146
    iget-boolean v0, p0, Lcom/twitter/library/card/ae;->b:Z

    if-nez v0, :cond_0

    .line 151
    :goto_0
    return-object v1

    .line 149
    :cond_0
    invoke-static {p1, p2}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    .line 150
    iget-object v2, p0, Lcom/twitter/library/card/ae;->d:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/ae$a;

    .line 151
    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/twitter/library/card/ae$a;->a:Lcom/twitter/library/card/aa;

    :goto_1
    move-object v1, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public a()V
    .locals 2

    .prologue
    .line 180
    const-string/jumbo v0, "card_registry_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/card/ae;->b:Z

    .line 181
    const-string/jumbo v0, "cards_forward_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/card/ae;->c:Z

    .line 182
    iget-object v0, p0, Lcom/twitter/library/card/ae;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/ae$a;

    .line 183
    invoke-virtual {v0}, Lcom/twitter/library/card/ae$a;->a()V

    goto :goto_0

    .line 185
    :cond_0
    return-void
.end method

.method public varargs a(Ljava/lang/String;Lcom/twitter/library/card/aa;[Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V
    .locals 5

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/twitter/library/card/ae;->b:Z

    if-nez v0, :cond_1

    .line 127
    :cond_0
    return-void

    .line 106
    :cond_1
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 107
    invoke-static {}, Lcom/twitter/util/f;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Missing card name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_2
    array-length v1, p3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p3, v0

    .line 114
    invoke-static {p1, v2}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v3

    .line 115
    iget-object v4, p0, Lcom/twitter/library/card/ae;->d:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 117
    invoke-static {}, Lcom/twitter/util/f;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Duplicate registration for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 124
    :cond_3
    new-instance v4, Lcom/twitter/library/card/ae$a;

    invoke-static {p1, v2}, Lcom/twitter/library/card/ae;->b(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, p2, v2}, Lcom/twitter/library/card/ae$a;-><init>(Lcom/twitter/library/card/aa;Ljava/lang/String;)V

    .line 125
    iget-object v2, p0, Lcom/twitter/library/card/ae;->d:Ljava/util/Map;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a(Lcax;)Z
    .locals 3

    .prologue
    .line 155
    invoke-virtual {p1}, Lcax;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-virtual {p1}, Lcax;->K()Lcar;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 133
    iget-boolean v0, p0, Lcom/twitter/library/card/ae;->b:Z

    if-nez v0, :cond_0

    .line 138
    :goto_0
    return v1

    .line 136
    :cond_0
    invoke-static {p1, p2}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    .line 137
    iget-object v2, p0, Lcom/twitter/library/card/ae;->d:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/ae$a;

    .line 138
    if-eqz v0, :cond_2

    iget-object v2, v0, Lcom/twitter/library/card/ae$a;->a:Lcom/twitter/library/card/aa;

    invoke-virtual {v2, p2, p3}, Lcom/twitter/library/card/aa;->a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v0, v0, Lcom/twitter/library/card/ae$a;->c:Z

    if-nez v0, :cond_1

    .line 139
    invoke-static {p1, p2}, Lcom/twitter/library/card/ae;->c(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 138
    goto :goto_0

    :cond_2
    move v0, v1

    .line 139
    goto :goto_1
.end method

.method public b(Lcax;)Z
    .locals 3

    .prologue
    .line 159
    invoke-virtual {p1}, Lcax;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-virtual {p1}, Lcax;->K()Lcar;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Z

    move-result v0

    return v0
.end method

.method public c(Lcax;)Z
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/twitter/library/card/ae;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/library/card/ae;->a(Lcax;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Lcax;)Z
    .locals 1

    .prologue
    .line 167
    invoke-virtual {p1}, Lcax;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "legacy_deciders_amplify_player_enabled"

    .line 168
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    const/4 v0, 0x0

    .line 171
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/library/card/ae;->b(Lcax;)Z

    move-result v0

    goto :goto_0
.end method

.method public e(Lcax;)Z
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0, p1}, Lcom/twitter/library/card/ae;->d(Lcax;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/library/card/ae;->c(Lcax;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
