.class public final Lcom/twitter/library/card/i;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method static a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    const-string/jumbo v0, "card_registry_capi_endpoint_url"

    invoke-static {v0}, Lcoj;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 24
    const-string/jumbo v0, "card_poll_create_url"

    const-string/jumbo v1, "https://caps.twitter.com/v2/cards/create"

    invoke-static {v0, v1}, Lcoj;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c()I
    .locals 2

    .prologue
    .line 32
    const-string/jumbo v0, "card_registry_capi_poll2choice_text_only_refresh_interval_seconds"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method
