.class Lcom/twitter/library/card/CardContextFactory$CardInstanceDataCardContextDataProvider;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/card/CardContextDataProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/card/CardContextFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CardInstanceDataCardContextDataProvider"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/library/card/CardContextFactory$CardInstanceDataCardContextDataProvider;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcax;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 220
    new-instance v0, Lcom/twitter/library/card/CardContextFactory$CardInstanceDataCardContextDataProvider$1;

    invoke-direct {v0}, Lcom/twitter/library/card/CardContextFactory$CardInstanceDataCardContextDataProvider$1;-><init>()V

    sput-object v0, Lcom/twitter/library/card/CardContextFactory$CardInstanceDataCardContextDataProvider;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237
    sget-object v0, Lcax;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcax;

    iput-object v0, p0, Lcom/twitter/library/card/CardContextFactory$CardInstanceDataCardContextDataProvider;->a:Lcax;

    .line 238
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/twitter/library/card/CardContextFactory$1;)V
    .locals 0

    .prologue
    .line 219
    invoke-direct {p0, p1}, Lcom/twitter/library/card/CardContextFactory$CardInstanceDataCardContextDataProvider;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Lcax;)V
    .locals 0

    .prologue
    .line 240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 241
    iput-object p1, p0, Lcom/twitter/library/card/CardContextFactory$CardInstanceDataCardContextDataProvider;->a:Lcax;

    .line 242
    return-void
.end method


# virtual methods
.method public a()Lcax;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/twitter/library/card/CardContextFactory$CardInstanceDataCardContextDataProvider;->a:Lcax;

    return-object v0
.end method

.method public b()Lcaq;
    .locals 1

    .prologue
    .line 253
    const/4 v0, 0x0

    return-object v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 258
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lcom/twitter/library/card/CardContextFactory$CardInstanceDataCardContextDataProvider;->a:Lcax;

    invoke-virtual {v0}, Lcax;->hashCode()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 294
    const/4 v0, 0x0

    return v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 272
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 277
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 283
    const/4 v0, 0x0

    return-object v0
.end method

.method public h()Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/twitter/library/card/CardContextFactory$CardInstanceDataCardContextDataProvider;->a:Lcax;

    sget-object v1, Lcax;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 300
    return-void
.end method
