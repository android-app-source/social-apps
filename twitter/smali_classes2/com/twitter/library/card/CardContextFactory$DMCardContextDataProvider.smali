.class Lcom/twitter/library/card/CardContextFactory$DMCardContextDataProvider;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/card/CardContextDataProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/card/CardContextFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DMCardContextDataProvider"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/library/card/CardContextFactory$DMCardContextDataProvider;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/twitter/model/dms/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lcom/twitter/library/card/CardContextFactory$DMCardContextDataProvider$1;

    invoke-direct {v0}, Lcom/twitter/library/card/CardContextFactory$DMCardContextDataProvider$1;-><init>()V

    sput-object v0, Lcom/twitter/library/card/CardContextFactory$DMCardContextDataProvider;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    sget-object v0, Lcom/twitter/model/dms/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/a;

    iput-object v0, p0, Lcom/twitter/library/card/CardContextFactory$DMCardContextDataProvider;->a:Lcom/twitter/model/dms/a;

    .line 81
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/twitter/library/card/CardContextFactory$1;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/twitter/library/card/CardContextFactory$DMCardContextDataProvider;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/dms/a;)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/twitter/library/card/CardContextFactory$DMCardContextDataProvider;->a:Lcom/twitter/model/dms/a;

    .line 77
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/dms/a;Lcom/twitter/library/card/CardContextFactory$1;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/twitter/library/card/CardContextFactory$DMCardContextDataProvider;-><init>(Lcom/twitter/model/dms/a;)V

    return-void
.end method


# virtual methods
.method public a()Lcax;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/twitter/library/card/CardContextFactory$DMCardContextDataProvider;->a:Lcom/twitter/model/dms/a;

    invoke-virtual {v0}, Lcom/twitter/model/dms/a;->f()Lcax;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcaq;
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    return-object v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/twitter/library/card/CardContextFactory$DMCardContextDataProvider;->a:Lcom/twitter/model/dms/a;

    invoke-virtual {v0}, Lcom/twitter/model/dms/a;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/twitter/library/card/CardContextFactory$DMCardContextDataProvider;->a:Lcom/twitter/model/dms/a;

    invoke-virtual {v0}, Lcom/twitter/model/dms/a;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    return v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 112
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 117
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/twitter/library/card/CardContextFactory$DMCardContextDataProvider;->a:Lcom/twitter/model/dms/a;

    iget-object v0, v0, Lcom/twitter/model/dms/a;->f:Ljava/lang/String;

    return-object v0
.end method

.method public h()Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/library/card/CardContextFactory$DMCardContextDataProvider;->a:Lcom/twitter/model/dms/a;

    sget-object v1, Lcom/twitter/model/dms/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 86
    return-void
.end method
