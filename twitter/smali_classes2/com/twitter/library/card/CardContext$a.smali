.class public final Lcom/twitter/library/card/CardContext$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/card/CardContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/library/card/CardContext;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/library/card/CardContextDataProvider;

.field private b:Lcom/twitter/library/scribe/ScribeItemsProvider;

.field private c:Lcom/twitter/model/core/v;

.field private d:Lcgi;

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 189
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/card/CardContext$a;)Lcom/twitter/library/card/CardContextDataProvider;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/twitter/library/card/CardContext$a;->a:Lcom/twitter/library/card/CardContextDataProvider;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/library/card/CardContext$a;)Lcom/twitter/library/scribe/ScribeItemsProvider;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/twitter/library/card/CardContext$a;->b:Lcom/twitter/library/scribe/ScribeItemsProvider;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/library/card/CardContext$a;)Lcom/twitter/model/core/v;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/twitter/library/card/CardContext$a;->c:Lcom/twitter/model/core/v;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/library/card/CardContext$a;)Lcgi;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/twitter/library/card/CardContext$a;->d:Lcgi;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/library/card/CardContext$a;)Z
    .locals 1

    .prologue
    .line 189
    iget-boolean v0, p0, Lcom/twitter/library/card/CardContext$a;->e:Z

    return v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 228
    invoke-super {p0}, Lcom/twitter/util/object/i;->R_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/CardContext$a;->a:Lcom/twitter/library/card/CardContextDataProvider;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/CardContext$a;->b:Lcom/twitter/library/scribe/ScribeItemsProvider;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcgi;)Lcom/twitter/library/card/CardContext$a;
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/twitter/library/card/CardContext$a;->d:Lcgi;

    .line 217
    return-object p0
.end method

.method public a(Lcom/twitter/library/card/CardContextDataProvider;)Lcom/twitter/library/card/CardContext$a;
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/twitter/library/card/CardContext$a;->a:Lcom/twitter/library/card/CardContextDataProvider;

    .line 199
    return-object p0
.end method

.method public a(Lcom/twitter/library/scribe/ScribeItemsProvider;)Lcom/twitter/library/card/CardContext$a;
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/twitter/library/card/CardContext$a;->b:Lcom/twitter/library/scribe/ScribeItemsProvider;

    .line 205
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/v;)Lcom/twitter/library/card/CardContext$a;
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lcom/twitter/library/card/CardContext$a;->c:Lcom/twitter/model/core/v;

    .line 211
    return-object p0
.end method

.method public a(Z)Lcom/twitter/library/card/CardContext$a;
    .locals 0

    .prologue
    .line 222
    iput-boolean p1, p0, Lcom/twitter/library/card/CardContext$a;->e:Z

    .line 223
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/twitter/library/card/CardContext$a;->e()Lcom/twitter/library/card/CardContext;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/library/card/CardContext;
    .locals 2

    .prologue
    .line 234
    new-instance v0, Lcom/twitter/library/card/CardContext;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/card/CardContext;-><init>(Lcom/twitter/library/card/CardContext$a;Lcom/twitter/library/card/CardContext$1;)V

    return-object v0
.end method
