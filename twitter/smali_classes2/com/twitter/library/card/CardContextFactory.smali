.class public Lcom/twitter/library/card/CardContextFactory;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/card/CardContextFactory$CardInstanceDataCardContextDataProvider;,
        Lcom/twitter/library/card/CardContextFactory$TweetCardContextDataProvider;,
        Lcom/twitter/library/card/CardContextFactory$DMCardContextDataProvider;
    }
.end annotation


# direct methods
.method public static a(Lcax;)Lcom/twitter/library/card/CardContext$a;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lcom/twitter/library/card/CardContext$a;

    invoke-direct {v0}, Lcom/twitter/library/card/CardContext$a;-><init>()V

    new-instance v1, Lcom/twitter/library/card/CardContextFactory$CardInstanceDataCardContextDataProvider;

    invoke-direct {v1, p0}, Lcom/twitter/library/card/CardContextFactory$CardInstanceDataCardContextDataProvider;-><init>(Lcax;)V

    .line 52
    invoke-virtual {v0, v1}, Lcom/twitter/library/card/CardContext$a;->a(Lcom/twitter/library/card/CardContextDataProvider;)Lcom/twitter/library/card/CardContext$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/scribe/EmptyScribeItemsProvider;->a:Lcom/twitter/library/scribe/EmptyScribeItemsProvider;

    .line 53
    invoke-virtual {v0, v1}, Lcom/twitter/library/card/CardContext$a;->a(Lcom/twitter/library/scribe/ScribeItemsProvider;)Lcom/twitter/library/card/CardContext$a;

    move-result-object v0

    .line 51
    return-object v0
.end method

.method public static a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/card/CardContext;
    .locals 3

    .prologue
    .line 26
    new-instance v0, Lcom/twitter/library/card/CardContext$a;

    invoke-direct {v0}, Lcom/twitter/library/card/CardContext$a;-><init>()V

    new-instance v1, Lcom/twitter/library/card/CardContextFactory$TweetCardContextDataProvider;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/twitter/library/card/CardContextFactory$TweetCardContextDataProvider;-><init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/card/CardContextFactory$1;)V

    .line 27
    invoke-virtual {v0, v1}, Lcom/twitter/library/card/CardContext$a;->a(Lcom/twitter/library/card/CardContextDataProvider;)Lcom/twitter/library/card/CardContext$a;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/TweetScribeItemsProvider;

    invoke-direct {v1, p0}, Lcom/twitter/library/scribe/TweetScribeItemsProvider;-><init>(Lcom/twitter/model/core/Tweet;)V

    .line 28
    invoke-virtual {v0, v1}, Lcom/twitter/library/card/CardContext$a;->a(Lcom/twitter/library/scribe/ScribeItemsProvider;)Lcom/twitter/library/card/CardContext$a;

    move-result-object v0

    .line 29
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/CardContext$a;->a(Lcom/twitter/model/core/v;)Lcom/twitter/library/card/CardContext$a;

    move-result-object v0

    .line 30
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/CardContext$a;->a(Lcgi;)Lcom/twitter/library/card/CardContext$a;

    move-result-object v0

    .line 31
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->Z()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/CardContext$a;->a(Z)Lcom/twitter/library/card/CardContext$a;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcom/twitter/library/card/CardContext$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/CardContext;

    .line 26
    return-object v0
.end method

.method public static a(Lcom/twitter/model/dms/a;)Lcom/twitter/library/card/CardContext;
    .locals 3

    .prologue
    .line 37
    new-instance v0, Lcom/twitter/library/card/CardContext$a;

    invoke-direct {v0}, Lcom/twitter/library/card/CardContext$a;-><init>()V

    new-instance v1, Lcom/twitter/library/card/CardContextFactory$DMCardContextDataProvider;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/twitter/library/card/CardContextFactory$DMCardContextDataProvider;-><init>(Lcom/twitter/model/dms/a;Lcom/twitter/library/card/CardContextFactory$1;)V

    .line 38
    invoke-virtual {v0, v1}, Lcom/twitter/library/card/CardContext$a;->a(Lcom/twitter/library/card/CardContextDataProvider;)Lcom/twitter/library/card/CardContext$a;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/DMScribeItemsProvider;

    invoke-direct {v1, p0}, Lcom/twitter/library/scribe/DMScribeItemsProvider;-><init>(Lcom/twitter/model/dms/a;)V

    .line 39
    invoke-virtual {v0, v1}, Lcom/twitter/library/card/CardContext$a;->a(Lcom/twitter/library/scribe/ScribeItemsProvider;)Lcom/twitter/library/card/CardContext$a;

    move-result-object v0

    .line 40
    invoke-virtual {p0}, Lcom/twitter/model/dms/a;->t()Lcom/twitter/model/core/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/CardContext$a;->a(Lcom/twitter/model/core/v;)Lcom/twitter/library/card/CardContext$a;

    move-result-object v0

    const/4 v1, 0x0

    .line 41
    invoke-virtual {v0, v1}, Lcom/twitter/library/card/CardContext$a;->a(Z)Lcom/twitter/library/card/CardContext$a;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Lcom/twitter/library/card/CardContext$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/CardContext;

    .line 37
    return-object v0
.end method
