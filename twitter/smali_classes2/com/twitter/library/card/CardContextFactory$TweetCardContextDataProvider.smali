.class Lcom/twitter/library/card/CardContextFactory$TweetCardContextDataProvider;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/card/CardContextDataProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/card/CardContextFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TweetCardContextDataProvider"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/library/card/CardContextFactory$TweetCardContextDataProvider;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/twitter/model/core/Tweet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 139
    new-instance v0, Lcom/twitter/library/card/CardContextFactory$TweetCardContextDataProvider$1;

    invoke-direct {v0}, Lcom/twitter/library/card/CardContextFactory$TweetCardContextDataProvider$1;-><init>()V

    sput-object v0, Lcom/twitter/library/card/CardContextFactory$TweetCardContextDataProvider;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    const-class v0, Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    iput-object v0, p0, Lcom/twitter/library/card/CardContextFactory$TweetCardContextDataProvider;->a:Lcom/twitter/model/core/Tweet;

    .line 162
    return-void
.end method

.method private constructor <init>(Lcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    iput-object p1, p0, Lcom/twitter/library/card/CardContextFactory$TweetCardContextDataProvider;->a:Lcom/twitter/model/core/Tweet;

    .line 158
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/card/CardContextFactory$1;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/twitter/library/card/CardContextFactory$TweetCardContextDataProvider;-><init>(Lcom/twitter/model/core/Tweet;)V

    return-void
.end method


# virtual methods
.method public a()Lcax;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/twitter/library/card/CardContextFactory$TweetCardContextDataProvider;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcaq;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/twitter/library/card/CardContextFactory$TweetCardContextDataProvider;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ae()Lcaq;

    move-result-object v0

    return-object v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/twitter/library/card/CardContextFactory$TweetCardContextDataProvider;->a:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->G:J

    return-wide v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/twitter/library/card/CardContextFactory$TweetCardContextDataProvider;->a:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->G:J

    return-wide v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x0

    return v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/twitter/library/card/CardContextFactory$TweetCardContextDataProvider;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->af()J

    move-result-wide v0

    return-wide v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lcom/twitter/library/card/CardContextFactory$TweetCardContextDataProvider;->a:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->G:J

    return-wide v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x0

    return-object v0
.end method

.method public h()Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/twitter/library/card/CardContextFactory$TweetCardContextDataProvider;->a:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/twitter/library/card/CardContextFactory$TweetCardContextDataProvider;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 216
    return-void
.end method
