.class Lcom/twitter/library/card/h$a;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/card/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/service/b",
        "<",
        "Lcom/twitter/library/api/y;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:Lcom/twitter/library/card/c;

.field private final c:Landroid/net/Uri;

.field private final g:Lcom/twitter/network/HttpOperation$RequestMethod;

.field private h:Lcar;


# direct methods
.method constructor <init>(Landroid/content/Context;JLcom/twitter/library/card/c;Landroid/net/Uri;Lcom/twitter/network/HttpOperation$RequestMethod;)V
    .locals 2

    .prologue
    .line 167
    const-class v0, Lcom/twitter/library/card/h$a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 168
    iput-wide p2, p0, Lcom/twitter/library/card/h$a;->a:J

    .line 169
    iput-object p4, p0, Lcom/twitter/library/card/h$a;->b:Lcom/twitter/library/card/c;

    .line 170
    iput-object p5, p0, Lcom/twitter/library/card/h$a;->c:Landroid/net/Uri;

    .line 171
    iput-object p6, p0, Lcom/twitter/library/card/h$a;->g:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 172
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 4

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/twitter/library/card/h$a;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/card/h$a;->g:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 191
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/card/h$a;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->c(Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/card/h$a;->c:Landroid/net/Uri;

    .line 192
    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->b(Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/library/card/h$a;->c:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->b([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v2

    .line 194
    iget-object v0, p0, Lcom/twitter/library/card/h$a;->b:Lcom/twitter/library/card/c;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/twitter/library/card/h$a;->b:Lcom/twitter/library/card/c;

    invoke-virtual {v0}, Lcom/twitter/library/card/c;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 196
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    goto :goto_0

    .line 199
    :cond_0
    invoke-virtual {v2}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V
    .locals 1

    .prologue
    .line 210
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/library/service/b;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V

    .line 212
    invoke-virtual {p1}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    invoke-virtual {p3}, Lcom/twitter/library/api/y;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcar;

    iput-object v0, p0, Lcom/twitter/library/card/h$a;->h:Lcar;

    .line 215
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 157
    check-cast p3, Lcom/twitter/library/api/y;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/card/h$a;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/y;)V

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;)Z
    .locals 1

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/twitter/library/card/h$a;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/library/service/v;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    return v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 175
    iget-wide v0, p0, Lcom/twitter/library/card/h$a;->a:J

    return-wide v0
.end method

.method public e()Lcar;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/twitter/library/card/h$a;->h:Lcar;

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/twitter/library/card/h$a;->g()Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method

.method protected g()Lcom/twitter/library/api/y;
    .locals 1

    .prologue
    .line 204
    const/16 v0, 0x62

    invoke-static {v0}, Lcom/twitter/library/api/y;->a(I)Lcom/twitter/library/api/y;

    move-result-object v0

    return-object v0
.end method
