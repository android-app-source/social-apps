.class public Lcom/twitter/library/card/s;
.super Lcom/twitter/library/card/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/card/s$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/card/t",
        "<",
        "Lcom/twitter/library/card/s$a;",
        "Lcaq;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/library/card/s;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final d:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/library/card/s;->c:Ljava/util/Map;

    return-void
.end method

.method constructor <init>(J)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/twitter/library/card/t;-><init>()V

    .line 25
    iput-wide p1, p0, Lcom/twitter/library/card/s;->d:J

    .line 26
    return-void
.end method

.method public static declared-synchronized a(J)Lcom/twitter/library/card/s;
    .locals 4

    .prologue
    .line 36
    const-class v1, Lcom/twitter/library/card/s;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/card/s;->c:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/s;

    .line 37
    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/twitter/library/card/s;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/card/s;-><init>(J)V

    .line 39
    sget-object v2, Lcom/twitter/library/card/s;->c:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41
    :cond_0
    monitor-exit v1

    return-object v0

    .line 36
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(JJLcaq;Lcom/twitter/library/card/s$a;)V
    .locals 9

    .prologue
    .line 57
    const/4 v6, 0x1

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p5

    move-object v5, p6

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/card/s;->a(JLjava/lang/Object;Ljava/lang/Object;Z)V

    .line 58
    iget-wide v0, p0, Lcom/twitter/library/card/s;->d:J

    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v3

    .line 59
    new-instance v0, Lcom/twitter/util/concurrent/c;

    invoke-direct {v0}, Lcom/twitter/util/concurrent/c;-><init>()V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    .line 60
    invoke-virtual {v0, v1}, Lcom/twitter/util/concurrent/c;->a(Ljava/util/concurrent/Executor;)Lcom/twitter/util/concurrent/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/card/s$1;

    move-object v2, p0

    move-wide v4, p1

    move-wide v6, p3

    move-object v8, p5

    invoke-direct/range {v1 .. v8}, Lcom/twitter/library/card/s$1;-><init>(Lcom/twitter/library/card/s;Lcom/twitter/library/provider/t;JJLcaq;)V

    .line 61
    invoke-virtual {v0, v1}, Lcom/twitter/util/concurrent/c;->a(Ljava/util/concurrent/Callable;)Lcom/twitter/util/concurrent/c;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lcom/twitter/util/concurrent/c;->a()Lcom/twitter/util/concurrent/g;

    .line 68
    return-void
.end method

.method protected a(Lcom/twitter/library/card/s$a;JLcaq;)V
    .locals 0

    .prologue
    .line 72
    invoke-interface {p1, p2, p3, p4}, Lcom/twitter/library/card/s$a;->a(JLcaq;)V

    .line 73
    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;JLjava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lcom/twitter/library/card/s$a;

    check-cast p4, Lcaq;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/library/card/s;->a(Lcom/twitter/library/card/s$a;JLcaq;)V

    return-void
.end method
