.class public Lcom/twitter/library/card/o;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field final b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

.field final c:Lcax;

.field final d:Ljava/lang/String;

.field final e:J

.field f:Z

.field g:Lcom/twitter/library/card/z;

.field private final h:Lcom/twitter/app/common/util/b$a;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;JLjava/lang/String;Lcax;)V
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Lcom/twitter/library/card/o$1;

    invoke-direct {v0, p0}, Lcom/twitter/library/card/o$1;-><init>(Lcom/twitter/library/card/o;)V

    iput-object v0, p0, Lcom/twitter/library/card/o;->h:Lcom/twitter/app/common/util/b$a;

    .line 116
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/card/o;->a:Ljava/lang/ref/WeakReference;

    .line 117
    iput-object p2, p0, Lcom/twitter/library/card/o;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 118
    iput-object p6, p0, Lcom/twitter/library/card/o;->c:Lcax;

    .line 119
    iput-object p5, p0, Lcom/twitter/library/card/o;->d:Ljava/lang/String;

    .line 120
    iput-wide p3, p0, Lcom/twitter/library/card/o;->e:J

    .line 121
    return-void
.end method

.method public static a(Landroid/app/Activity;Lcax;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Lcom/twitter/library/card/o;
    .locals 10

    .prologue
    const-wide/16 v4, -0x1

    const/4 v1, 0x0

    .line 81
    if-nez p0, :cond_1

    .line 111
    :cond_0
    :goto_0
    return-object v1

    .line 85
    :cond_1
    instance-of v0, p0, Lcom/twitter/app/common/util/j;

    if-nez v0, :cond_2

    .line 86
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Not assignable from LifecycleAwareActivity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_2
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 97
    invoke-virtual {p1}, Lcax;->b()Ljava/lang/String;

    move-result-object v6

    .line 98
    invoke-virtual {p1}, Lcax;->K()Lcar;

    move-result-object v2

    .line 99
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    .line 100
    invoke-virtual {v0, v4, v5, v2}, Lcom/twitter/library/card/q;->c(JLjava/lang/Object;)V

    .line 102
    invoke-static {}, Lcom/twitter/library/card/ak;->a()Lcom/twitter/library/card/ak;

    move-result-object v3

    .line 103
    invoke-virtual {p1}, Lcax;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 104
    invoke-virtual {p1}, Lcax;->d()Ljava/util/Map;

    move-result-object v8

    invoke-interface {v8, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 105
    iget-wide v8, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v3, v8, v9, v0}, Lcom/twitter/library/card/ak;->c(JLjava/lang/Object;)V

    goto :goto_1

    .line 108
    :cond_3
    invoke-static {}, Lcom/twitter/library/card/ae;->b()Lcom/twitter/library/card/ae;

    move-result-object v0

    invoke-virtual {v0, v6, p2, v2}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    new-instance v1, Lcom/twitter/library/card/o;

    move-object v2, p0

    move-object v3, p2

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, Lcom/twitter/library/card/o;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;JLjava/lang/String;Lcax;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 7

    .prologue
    .line 124
    iget-object v0, p0, Lcom/twitter/library/card/o;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 125
    if-nez v0, :cond_1

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    invoke-static {}, Lcom/twitter/library/card/ae;->b()Lcom/twitter/library/card/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/card/o;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/library/card/o;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Lcom/twitter/library/card/aa;

    move-result-object v1

    .line 130
    if-eqz v1, :cond_0

    .line 134
    iget-object v2, p0, Lcom/twitter/library/card/o;->c:Lcax;

    invoke-virtual {v2}, Lcax;->K()Lcar;

    move-result-object v6

    .line 135
    iget-object v2, p0, Lcom/twitter/library/card/o;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-virtual {v1, v0, v2, v6}, Lcom/twitter/library/card/aa;->a(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Lcom/twitter/library/card/z;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/card/o;->g:Lcom/twitter/library/card/z;

    .line 137
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/util/j;

    iget-object v1, p0, Lcom/twitter/library/card/o;->h:Lcom/twitter/app/common/util/b$a;

    .line 138
    invoke-interface {v0, v1}, Lcom/twitter/app/common/util/j;->a(Lcom/twitter/app/common/util/b$a;)V

    .line 140
    iget-object v0, p0, Lcom/twitter/library/card/o;->g:Lcom/twitter/library/card/z;

    new-instance v1, Lcom/twitter/library/card/z$a;

    iget-wide v2, p0, Lcom/twitter/library/card/o;->e:J

    iget-wide v4, p0, Lcom/twitter/library/card/o;->e:J

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/card/z$a;-><init>(JJLcar;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/z;->a(Ljava/lang/Object;)V

    .line 142
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/card/o;->f:Z

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/twitter/library/card/o;->g:Lcom/twitter/library/card/z;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/twitter/library/card/o;->g:Lcom/twitter/library/card/z;

    invoke-virtual {v0}, Lcom/twitter/library/card/z;->b()V

    .line 149
    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/twitter/library/card/o;->f:Z

    if-eqz v0, :cond_2

    .line 153
    iget-object v0, p0, Lcom/twitter/library/card/o;->g:Lcom/twitter/library/card/z;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/twitter/library/card/o;->g:Lcom/twitter/library/card/z;

    invoke-virtual {v0}, Lcom/twitter/library/card/z;->a()V

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/twitter/library/card/o;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 158
    if-eqz v0, :cond_1

    .line 159
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/util/j;

    iget-object v1, p0, Lcom/twitter/library/card/o;->h:Lcom/twitter/app/common/util/b$a;

    .line 160
    invoke-interface {v0, v1}, Lcom/twitter/app/common/util/j;->b(Lcom/twitter/app/common/util/b$a;)V

    .line 163
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/card/o;->f:Z

    .line 165
    :cond_2
    return-void
.end method

.method public d()Landroid/view/View;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/twitter/library/card/o;->g:Lcom/twitter/library/card/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/o;->g:Lcom/twitter/library/card/z;

    invoke-virtual {v0}, Lcom/twitter/library/card/z;->e()Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
