.class Lcom/twitter/library/card/o$1;
.super Lcom/twitter/app/common/util/b$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/card/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/card/o;


# direct methods
.method constructor <init>(Lcom/twitter/library/card/o;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/twitter/library/card/o$1;->a:Lcom/twitter/library/card/o;

    invoke-direct {p0}, Lcom/twitter/app/common/util/b$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/library/card/o$1;->a:Lcom/twitter/library/card/o;

    iget-object v0, v0, Lcom/twitter/library/card/o;->g:Lcom/twitter/library/card/z;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/twitter/library/card/o$1;->a:Lcom/twitter/library/card/o;

    iget-object v0, v0, Lcom/twitter/library/card/o;->g:Lcom/twitter/library/card/z;

    invoke-virtual {v0, p2}, Lcom/twitter/library/card/z;->a(Landroid/content/res/Configuration;)V

    .line 75
    :cond_0
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/library/card/o$1;->a:Lcom/twitter/library/card/o;

    invoke-virtual {v0}, Lcom/twitter/library/card/o;->c()V

    .line 65
    iget-object v0, p0, Lcom/twitter/library/card/o$1;->a:Lcom/twitter/library/card/o;

    iget-object v0, v0, Lcom/twitter/library/card/o;->g:Lcom/twitter/library/card/z;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/twitter/library/card/o$1;->a:Lcom/twitter/library/card/o;

    iget-object v1, v0, Lcom/twitter/library/card/o;->g:Lcom/twitter/library/card/z;

    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/util/j;

    invoke-interface {v0}, Lcom/twitter/app/common/util/j;->isChangingConfigurations()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/card/z;->b(Z)V

    .line 68
    :cond_0
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/library/card/o$1;->a:Lcom/twitter/library/card/o;

    iget-object v0, v0, Lcom/twitter/library/card/o;->g:Lcom/twitter/library/card/z;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/twitter/library/card/o$1;->a:Lcom/twitter/library/card/o;

    iget-object v0, v0, Lcom/twitter/library/card/o;->g:Lcom/twitter/library/card/z;

    invoke-virtual {v0}, Lcom/twitter/library/card/z;->aj_()V

    .line 53
    :cond_0
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/library/card/o$1;->a:Lcom/twitter/library/card/o;

    iget-object v0, v0, Lcom/twitter/library/card/o;->g:Lcom/twitter/library/card/z;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/twitter/library/card/o$1;->a:Lcom/twitter/library/card/o;

    iget-object v0, v0, Lcom/twitter/library/card/o;->g:Lcom/twitter/library/card/z;

    invoke-virtual {v0}, Lcom/twitter/library/card/z;->d()V

    .line 46
    :cond_0
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/library/card/o$1;->a:Lcom/twitter/library/card/o;

    iget-object v0, v0, Lcom/twitter/library/card/o;->g:Lcom/twitter/library/card/z;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/twitter/library/card/o$1;->a:Lcom/twitter/library/card/o;

    iget-object v0, v0, Lcom/twitter/library/card/o;->g:Lcom/twitter/library/card/z;

    invoke-virtual {v0}, Lcom/twitter/library/card/z;->ai_()V

    .line 39
    :cond_0
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/library/card/o$1;->a:Lcom/twitter/library/card/o;

    iget-object v0, v0, Lcom/twitter/library/card/o;->g:Lcom/twitter/library/card/z;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/twitter/library/card/o$1;->a:Lcom/twitter/library/card/o;

    iget-object v1, v0, Lcom/twitter/library/card/o;->g:Lcom/twitter/library/card/z;

    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/util/j;

    invoke-interface {v0}, Lcom/twitter/app/common/util/j;->isChangingConfigurations()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/card/z;->a(Z)V

    .line 60
    :cond_0
    return-void
.end method
