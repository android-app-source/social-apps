.class public Lcom/twitter/library/card/v;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/card/v$a;
    }
.end annotation


# static fields
.field private static a:Lcom/twitter/library/card/v;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/widget/renderablecontent/DisplayMode;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/card/v$a;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/v;->b:Ljava/util/Map;

    .line 34
    return-void
.end method

.method public static a()Lcom/twitter/library/card/v;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/twitter/library/card/v;->a:Lcom/twitter/library/card/v;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/twitter/library/card/v;

    invoke-direct {v0}, Lcom/twitter/library/card/v;-><init>()V

    sput-object v0, Lcom/twitter/library/card/v;->a:Lcom/twitter/library/card/v;

    .line 39
    const-class v0, Lcom/twitter/library/card/v;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 41
    :cond_0
    sget-object v0, Lcom/twitter/library/card/v;->a:Lcom/twitter/library/card/v;

    return-object v0
.end method


# virtual methods
.method public varargs a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 104
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    invoke-static {}, Lcom/twitter/util/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Missing card name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :cond_0
    new-instance v1, Lcom/twitter/library/card/v$a;

    invoke-direct {v1, p3, p4}, Lcom/twitter/library/card/v$a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    .line 113
    invoke-static {p1, p2}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    .line 114
    iget-object v2, p0, Lcom/twitter/library/card/v;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 115
    iget-object v2, p0, Lcom/twitter/library/card/v;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    :cond_1
    :goto_0
    return-void

    .line 117
    :cond_2
    iget-object v2, p0, Lcom/twitter/library/card/v;->b:Ljava/util/Map;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/twitter/library/card/v$a;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v3}, Lcom/twitter/util/collection/CollectionUtils;->d([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 56
    iget-object v0, p0, Lcom/twitter/library/card/v;->b:Ljava/util/Map;

    invoke-static {p1, p2}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 57
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 61
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/card/v$a;

    move-object v3, v1

    move v4, v2

    .line 63
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 64
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/card/v$a;

    .line 66
    iget-object v5, v1, Lcom/twitter/library/card/v$a;->a:Ljava/lang/String;

    .line 67
    invoke-static {v5}, Lcoi;->e(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 68
    if-nez v4, :cond_1

    .line 69
    const/4 v4, 0x1

    move-object v3, v1

    .line 63
    :cond_0
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 75
    :cond_1
    new-instance v5, Lcpb;

    invoke-direct {v5}, Lcpb;-><init>()V

    const-string/jumbo v6, "card name"

    .line 76
    invoke-virtual {v5, v6, p1}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v5

    const-string/jumbo v6, "display mode"

    .line 77
    invoke-virtual {v5, v6, p2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v5

    const-string/jumbo v6, "experiments"

    .line 78
    invoke-virtual {v5, v6, v0}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v5

    const-string/jumbo v6, "chosen experiment key"

    iget-object v7, v3, Lcom/twitter/library/card/v$a;->a:Ljava/lang/String;

    .line 79
    invoke-virtual {v5, v6, v7}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v5

    const-string/jumbo v6, "conflicting experiment key"

    iget-object v1, v1, Lcom/twitter/library/card/v$a;->a:Ljava/lang/String;

    .line 80
    invoke-virtual {v5, v6, v1}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v1

    new-instance v5, Ljava/lang/IllegalStateException;

    const-string/jumbo v6, "Multiple assigned experiments trying to register. Use same experiment group to ensure mutual exclusivity."

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 81
    invoke-virtual {v1, v5}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v1

    .line 75
    invoke-static {v1}, Lcpd;->c(Lcpb;)V

    goto :goto_1

    .line 89
    :cond_2
    iget-object v0, v3, Lcom/twitter/library/card/v$a;->a:Ljava/lang/String;

    .line 90
    iget-object v1, v3, Lcom/twitter/library/card/v$a;->b:[Ljava/lang/String;

    invoke-static {v0, v1}, Lcoi;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    .line 92
    :cond_3
    return v2
.end method
