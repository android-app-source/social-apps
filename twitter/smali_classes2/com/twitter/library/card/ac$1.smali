.class Lcom/twitter/library/card/ac$1;
.super Lcom/twitter/app/common/util/b$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/card/ac;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/card/ac;


# direct methods
.method constructor <init>(Lcom/twitter/library/card/ac;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/twitter/library/card/ac$1;->a:Lcom/twitter/library/card/ac;

    invoke-direct {p0}, Lcom/twitter/app/common/util/b$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/twitter/library/card/ac$1;->a:Lcom/twitter/library/card/ac;

    iget-object v0, v0, Lcom/twitter/library/card/ac;->h:Lcom/twitter/library/card/z;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/twitter/library/card/ac$1;->a:Lcom/twitter/library/card/ac;

    iget-object v0, v0, Lcom/twitter/library/card/ac;->h:Lcom/twitter/library/card/z;

    invoke-virtual {v0, p2}, Lcom/twitter/library/card/z;->a(Landroid/content/res/Configuration;)V

    .line 80
    :cond_0
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/library/card/ac$1;->a:Lcom/twitter/library/card/ac;

    invoke-virtual {v0}, Lcom/twitter/library/card/ac;->bh_()V

    .line 70
    iget-object v0, p0, Lcom/twitter/library/card/ac$1;->a:Lcom/twitter/library/card/ac;

    iget-object v0, v0, Lcom/twitter/library/card/ac;->h:Lcom/twitter/library/card/z;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/twitter/library/card/ac$1;->a:Lcom/twitter/library/card/ac;

    iget-object v1, v0, Lcom/twitter/library/card/ac;->h:Lcom/twitter/library/card/z;

    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/util/j;

    invoke-interface {v0}, Lcom/twitter/app/common/util/j;->isChangingConfigurations()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/card/z;->b(Z)V

    .line 73
    :cond_0
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/library/card/ac$1;->a:Lcom/twitter/library/card/ac;

    iget-object v0, v0, Lcom/twitter/library/card/ac;->h:Lcom/twitter/library/card/z;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/twitter/library/card/ac$1;->a:Lcom/twitter/library/card/ac;

    iget-object v0, v0, Lcom/twitter/library/card/ac;->h:Lcom/twitter/library/card/z;

    invoke-virtual {v0}, Lcom/twitter/library/card/z;->aj_()V

    .line 58
    :cond_0
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/library/card/ac$1;->a:Lcom/twitter/library/card/ac;

    iget-object v0, v0, Lcom/twitter/library/card/ac;->h:Lcom/twitter/library/card/z;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/twitter/library/card/ac$1;->a:Lcom/twitter/library/card/ac;

    iget-object v0, v0, Lcom/twitter/library/card/ac;->h:Lcom/twitter/library/card/z;

    invoke-virtual {v0}, Lcom/twitter/library/card/z;->d()V

    .line 51
    :cond_0
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/library/card/ac$1;->a:Lcom/twitter/library/card/ac;

    iget-object v0, v0, Lcom/twitter/library/card/ac;->h:Lcom/twitter/library/card/z;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/twitter/library/card/ac$1;->a:Lcom/twitter/library/card/ac;

    iget-object v0, v0, Lcom/twitter/library/card/ac;->h:Lcom/twitter/library/card/z;

    invoke-virtual {v0}, Lcom/twitter/library/card/z;->ai_()V

    .line 44
    :cond_0
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/library/card/ac$1;->a:Lcom/twitter/library/card/ac;

    iget-object v0, v0, Lcom/twitter/library/card/ac;->h:Lcom/twitter/library/card/z;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/twitter/library/card/ac$1;->a:Lcom/twitter/library/card/ac;

    iget-object v1, v0, Lcom/twitter/library/card/ac;->h:Lcom/twitter/library/card/z;

    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/util/j;

    invoke-interface {v0}, Lcom/twitter/app/common/util/j;->isChangingConfigurations()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/card/z;->a(Z)V

    .line 65
    :cond_0
    return-void
.end method
