.class public Lcom/twitter/library/card/ac;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/renderablecontent/d;


# instance fields
.field final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field final b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

.field final c:Lcar;

.field final d:Ljava/lang/String;

.field final e:J

.field final f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field g:Z

.field h:Lcom/twitter/library/card/z;

.field private final i:Lcom/twitter/app/common/util/b$a;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;JLjava/lang/String;Lcar;)V
    .locals 3

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Lcom/twitter/library/card/ac$1;

    invoke-direct {v0, p0}, Lcom/twitter/library/card/ac$1;-><init>(Lcom/twitter/library/card/ac;)V

    iput-object v0, p0, Lcom/twitter/library/card/ac;->i:Lcom/twitter/app/common/util/b$a;

    .line 105
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/card/ac;->a:Ljava/lang/ref/WeakReference;

    .line 106
    iput-object p2, p0, Lcom/twitter/library/card/ac;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 107
    iput-object p6, p0, Lcom/twitter/library/card/ac;->c:Lcar;

    .line 108
    iput-object p5, p0, Lcom/twitter/library/card/ac;->d:Ljava/lang/String;

    .line 109
    iput-wide p3, p0, Lcom/twitter/library/card/ac;->e:J

    .line 110
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const/4 v1, 0x5

    .line 111
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v1, "profile"

    .line 112
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v1, "spotlight"

    .line 113
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->d(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/library/card/ac;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 114
    return-void
.end method

.method public static a(Landroid/app/Activity;JLcax;)Lcom/twitter/library/card/ac;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 88
    if-nez p0, :cond_1

    .line 100
    :cond_0
    :goto_0
    return-object v1

    .line 92
    :cond_1
    if-eqz p3, :cond_0

    .line 96
    invoke-virtual {p3}, Lcax;->K()Lcar;

    move-result-object v7

    .line 97
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    .line 98
    invoke-virtual {v0, p1, p2, v7}, Lcom/twitter/library/card/q;->c(JLjava/lang/Object;)V

    .line 100
    new-instance v1, Lcom/twitter/library/card/ac;

    sget-object v3, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->c:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    const-string/jumbo v6, "app"

    move-object v2, p0

    move-wide v4, p1

    invoke-direct/range {v1 .. v7}, Lcom/twitter/library/card/ac;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;JLjava/lang/String;Lcar;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;IIII)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    return-object v0
.end method

.method public bg_()V
    .locals 7

    .prologue
    .line 118
    iget-object v0, p0, Lcom/twitter/library/card/ac;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 119
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/twitter/library/card/ae;->b()Lcom/twitter/library/card/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/card/ac;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/library/card/ac;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    iget-object v4, p0, Lcom/twitter/library/card/ac;->c:Lcar;

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 120
    invoke-static {}, Lcom/twitter/library/card/ae;->b()Lcom/twitter/library/card/ae;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/card/ac;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/library/card/ac;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Lcom/twitter/library/card/aa;

    move-result-object v1

    .line 121
    if-eqz v1, :cond_0

    .line 122
    iget-object v2, p0, Lcom/twitter/library/card/ac;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    iget-object v3, p0, Lcom/twitter/library/card/ac;->c:Lcar;

    invoke-virtual {v1, v0, v2, v3}, Lcom/twitter/library/card/aa;->a(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Lcom/twitter/library/card/z;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/card/ac;->h:Lcom/twitter/library/card/z;

    .line 124
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/util/j;

    iget-object v1, p0, Lcom/twitter/library/card/ac;->i:Lcom/twitter/app/common/util/b$a;

    .line 125
    invoke-interface {v0, v1}, Lcom/twitter/app/common/util/j;->a(Lcom/twitter/app/common/util/b$a;)V

    .line 127
    iget-object v0, p0, Lcom/twitter/library/card/ac;->h:Lcom/twitter/library/card/z;

    new-instance v1, Lcom/twitter/library/card/z$a;

    iget-wide v2, p0, Lcom/twitter/library/card/ac;->e:J

    iget-wide v4, p0, Lcom/twitter/library/card/ac;->e:J

    iget-object v6, p0, Lcom/twitter/library/card/ac;->c:Lcar;

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/card/z$a;-><init>(JJLcar;)V

    const-string/jumbo v2, "params_extra_scribe_association"

    iget-object v3, p0, Lcom/twitter/library/card/ac;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 128
    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/card/z$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/twitter/library/card/z$a;

    move-result-object v1

    .line 127
    invoke-virtual {v0, v1}, Lcom/twitter/library/card/z;->a(Ljava/lang/Object;)V

    .line 130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/card/ac;->g:Z

    .line 133
    :cond_0
    return-void
.end method

.method public bh_()V
    .locals 2

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/twitter/library/card/ac;->g:Z

    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p0, Lcom/twitter/library/card/ac;->h:Lcom/twitter/library/card/z;

    invoke-virtual {v0}, Lcom/twitter/library/card/z;->a()V

    .line 147
    iget-object v0, p0, Lcom/twitter/library/card/ac;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 148
    if-eqz v0, :cond_0

    .line 149
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/util/j;

    iget-object v1, p0, Lcom/twitter/library/card/ac;->i:Lcom/twitter/app/common/util/b$a;

    .line 150
    invoke-interface {v0, v1}, Lcom/twitter/app/common/util/j;->b(Lcom/twitter/app/common/util/b$a;)V

    .line 153
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/card/ac;->g:Z

    .line 155
    :cond_1
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/twitter/library/card/ac;->h:Lcom/twitter/library/card/z;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/twitter/library/card/ac;->h:Lcom/twitter/library/card/z;

    invoke-virtual {v0}, Lcom/twitter/library/card/z;->b()V

    .line 140
    :cond_0
    return-void
.end method

.method public d()Landroid/view/View;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/twitter/library/card/ac;->h:Lcom/twitter/library/card/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/ac;->h:Lcom/twitter/library/card/z;

    invoke-virtual {v0}, Lcom/twitter/library/card/z;->e()Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Lcom/twitter/library/widget/renderablecontent/c;
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x0

    return-object v0
.end method
