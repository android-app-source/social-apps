.class public Lcom/twitter/library/platform/PushService;
.super Lcom/google/android/gcm/GCMBaseIntentService;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/platform/PushService$InvalidNotificationPayloadException;,
        Lcom/twitter/library/platform/PushService$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".push.logged_in"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/platform/PushService;->a:Ljava/lang/String;

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".push.logged_out"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/platform/PushService;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 116
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "49625052041"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/gcm/GCMBaseIntentService;-><init>([Ljava/lang/String;)V

    .line 117
    return-void
.end method

.method public static a(JJ)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 506
    .line 507
    invoke-static {}, Lcom/twitter/library/provider/j;->c()Lcom/twitter/library/provider/j;

    move-result-object v0

    .line 508
    invoke-virtual {v0}, Lcom/twitter/library/provider/j;->d()Lcom/twitter/database/schema/GlobalSchema;

    move-result-object v0

    const-class v3, Lawg;

    invoke-interface {v0, v3}, Lcom/twitter/database/schema/GlobalSchema;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Lawg;

    invoke-interface {v0}, Lawg;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    .line 509
    const-string/jumbo v3, "account_settings_account_id"

    .line 510
    invoke-static {v3}, Laux;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-interface {v0, v3, v4}, Lcom/twitter/database/model/l;->a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/twitter/database/model/g;

    move-result-object v3

    .line 512
    :try_start_0
    invoke-virtual {v3}, Lcom/twitter/database/model/g;->b()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 513
    iget-object v0, v3, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lawg$a;

    invoke-interface {v0}, Lawg$a;->f()I

    move-result v4

    .line 515
    const-string/jumbo v0, "push_notifications_separate_photo_tags"

    .line 516
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v5

    .line 519
    const/16 v0, 0x200

    invoke-static {v4, v0}, Lcga;->b(II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v3, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lawg$a;

    invoke-interface {v0}, Lawg$a;->c()I

    move-result v0

    if-ne v2, v0, :cond_0

    move v1, v2

    .line 522
    :cond_0
    iget-object v0, v3, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lawg$a;

    invoke-interface {v0}, Lawg$a;->d()I

    move-result v2

    .line 523
    const/16 v0, 0x8

    const/4 v6, 0x4

    invoke-static {v4, v0, v6}, Lcga;->a(III)I

    move-result v0

    if-eqz v0, :cond_e

    .line 525
    and-int/lit8 v0, v2, 0x1

    if-eqz v0, :cond_d

    .line 526
    or-int/lit8 v0, v1, 0x2

    .line 528
    :goto_0
    and-int/lit16 v1, v2, 0x400

    if-eqz v1, :cond_1

    .line 529
    or-int/lit16 v0, v0, 0x800

    .line 531
    :cond_1
    if-nez v5, :cond_2

    and-int/lit16 v1, v2, 0x200

    if-eqz v1, :cond_2

    .line 532
    or-int/lit16 v0, v0, 0x200

    .line 535
    :cond_2
    :goto_1
    if-eqz v5, :cond_3

    const/high16 v1, 0x4000000

    invoke-static {v4, v1}, Lcga;->b(II)Z

    move-result v1

    if-eqz v1, :cond_3

    and-int/lit16 v1, v2, 0x200

    if-eqz v1, :cond_3

    .line 537
    or-int/lit16 v0, v0, 0x200

    .line 539
    :cond_3
    const/16 v1, 0x100

    const/16 v5, 0x80

    invoke-static {v4, v1, v5}, Lcga;->a(III)I

    move-result v1

    if-eqz v1, :cond_4

    and-int/lit8 v1, v2, 0x8

    if-eqz v1, :cond_4

    .line 541
    or-int/lit8 v0, v0, 0x8

    .line 543
    :cond_4
    const/16 v1, 0x40

    const/16 v5, 0x20

    invoke-static {v4, v1, v5}, Lcga;->a(III)I

    move-result v1

    if-eqz v1, :cond_5

    and-int/lit8 v1, v2, 0x2

    if-eqz v1, :cond_5

    .line 545
    or-int/lit8 v0, v0, 0x10

    .line 547
    :cond_5
    const/16 v1, 0x10

    invoke-static {v4, v1}, Lcga;->b(II)Z

    move-result v1

    if-eqz v1, :cond_7

    and-int/lit8 v1, v2, 0x4

    if-nez v1, :cond_6

    and-int/lit8 v1, v2, 0x20

    if-eqz v1, :cond_7

    .line 550
    :cond_6
    or-int/lit8 v0, v0, 0x20

    .line 552
    :cond_7
    and-int/lit8 v1, v2, 0x40

    if-eqz v1, :cond_8

    .line 553
    or-int/lit8 v0, v0, 0x40

    .line 555
    :cond_8
    and-int/lit16 v1, v2, 0x80

    if-eqz v1, :cond_9

    .line 556
    or-int/lit16 v0, v0, 0x80

    .line 558
    :cond_9
    and-int/lit16 v1, v2, 0x100

    if-eqz v1, :cond_a

    .line 559
    or-int/lit16 v0, v0, 0x100

    .line 561
    :cond_a
    const/4 v1, 0x1

    invoke-static {v4, v1}, Lcga;->b(II)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 562
    invoke-static {p2, p3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v1

    .line 563
    invoke-virtual {v1}, Lcom/twitter/library/provider/t;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string/jumbo v2, "SELECT COUNT(*) FROM conversations WHERE last_readable_event_id > last_read_event_id AND last_readable_event_id > (SELECT COALESCE((SELECT CAST(next as int) AS last_seen_event_id FROM cursors WHERE kind=14 AND type=0 ORDER BY last_seen_event_id DESC LIMIT 1), 0));"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 565
    if-eqz v1, :cond_c

    .line 566
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_b

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-lez v2, :cond_b

    .line 568
    or-int/lit8 v0, v0, 0x4

    .line 570
    :cond_b
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 575
    :cond_c
    :goto_2
    invoke-virtual {v3}, Lcom/twitter/database/model/g;->close()V

    .line 577
    return v0

    .line 575
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/twitter/database/model/g;->close()V

    throw v0

    :cond_d
    move v0, v1

    goto/16 :goto_0

    :cond_e
    move v0, v1

    goto/16 :goto_1

    :cond_f
    move v0, v1

    goto :goto_2
.end method

.method private static a(Landroid/os/Bundle;I)I
    .locals 3

    .prologue
    const/4 v0, 0x7

    .line 471
    if-ne p1, v0, :cond_0

    .line 472
    const-string/jumbo v1, "presentation_type"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 473
    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 474
    const/16 p1, 0x8

    .line 481
    :cond_0
    :goto_0
    return p1

    .line 475
    :cond_1
    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 476
    const/16 p1, 0x9

    goto :goto_0

    :cond_2
    move p1, v0

    .line 478
    goto :goto_0
.end method

.method private static a(Landroid/os/Bundle;Landroid/content/SharedPreferences;ILjava/lang/String;)Lcom/twitter/library/platform/PushService$a;
    .locals 10

    .prologue
    .line 223
    new-instance v2, Lcom/twitter/library/platform/notifications/e;

    invoke-direct {v2, p0}, Lcom/twitter/library/platform/notifications/e;-><init>(Landroid/os/Bundle;)V

    .line 224
    if-nez p2, :cond_0

    .line 225
    new-instance v0, Lcom/twitter/library/platform/PushService$a;

    invoke-direct {v0}, Lcom/twitter/library/platform/PushService$a;-><init>()V

    .line 226
    iput-object p0, v0, Lcom/twitter/library/platform/PushService$a;->d:Landroid/os/Bundle;

    .line 227
    iget-object v1, v0, Lcom/twitter/library/platform/PushService$a;->d:Landroid/os/Bundle;

    const-string/jumbo v3, "recipient_id"

    invoke-virtual {v2}, Lcom/twitter/library/platform/notifications/e;->f()J

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 341
    :goto_0
    return-object v0

    .line 230
    :cond_0
    invoke-virtual {v2}, Lcom/twitter/library/platform/notifications/e;->l()Lcom/twitter/library/platform/notifications/s;

    move-result-object v3

    .line 233
    invoke-virtual {v2}, Lcom/twitter/library/platform/notifications/e;->g()Lcfw;

    move-result-object v4

    .line 234
    if-nez v4, :cond_1

    .line 235
    const/4 v0, 0x0

    goto :goto_0

    .line 237
    :cond_1
    invoke-static {}, Lcpd;->a()Lcpd;

    move-result-object v0

    invoke-virtual {v0}, Lcpd;->b()Lcpa;

    move-result-object v0

    .line 238
    invoke-virtual {v0}, Lcpa;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 239
    const-string/jumbo v1, "user_id"

    iget-object v5, v4, Lcfw;->b:Lcfv;

    iget-wide v6, v5, Lcfv;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Lcpa;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    :cond_2
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 243
    const-string/jumbo v0, "priority"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 244
    const-string/jumbo v0, "priority"

    invoke-virtual {v2}, Lcom/twitter/library/platform/notifications/e;->j()I

    move-result v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 249
    :goto_1
    invoke-virtual {v2}, Lcom/twitter/library/platform/notifications/e;->h()Lcfu;

    move-result-object v6

    .line 253
    iget-object v0, v4, Lcfw;->b:Lcfv;

    iget-wide v0, v0, Lcfv;->b:J

    invoke-static {p0, v0, v1}, Lcom/twitter/library/platform/PushService;->a(Landroid/os/Bundle;J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 254
    const-string/jumbo v0, "actions"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 255
    invoke-static {}, Lcqj;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 257
    :try_start_0
    const-string/jumbo v1, "PushService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Notification Payload - actions: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x2

    .line 258
    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 257
    invoke-static {v1, v7}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 262
    :cond_3
    :goto_2
    const-class v1, Lcfr;

    invoke-static {v0, v1}, Lcom/twitter/model/json/common/e;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    .line 267
    :goto_3
    const/4 v0, 0x2

    if-ne p2, v0, :cond_8

    .line 268
    iget v0, v3, Lcom/twitter/library/platform/notifications/s;->b:I

    const/16 v7, 0x16

    if-eq v0, v7, :cond_4

    iget v0, v3, Lcom/twitter/library/platform/notifications/s;->b:I

    const/16 v7, 0x112

    if-ne v0, v7, :cond_8

    .line 270
    :cond_4
    const-string/jumbo v0, "notification_event_data"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 271
    invoke-static {p0}, Lcom/twitter/library/platform/PushService;->b(Landroid/os/Bundle;)Lcom/twitter/model/dms/s;

    move-result-object v0

    .line 272
    if-nez v0, :cond_7

    .line 273
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 246
    :cond_5
    const-string/jumbo v0, "priority"

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 247
    new-instance v0, Lcom/twitter/library/platform/PushService$InvalidNotificationPayloadException;

    const-string/jumbo v1, "Missing priority"

    invoke-direct {v0, v1}, Lcom/twitter/library/platform/PushService$InvalidNotificationPayloadException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 264
    :cond_6
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_3

    .line 278
    :cond_7
    const-string/jumbo v7, "notification_dm_data"

    sget-object v8, Lcom/twitter/model/dms/s;->i:Lcom/twitter/util/serialization/l;

    invoke-static {v5, v7, v0, v8}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    .line 285
    :cond_8
    iget v0, v3, Lcom/twitter/library/platform/notifications/s;->b:I

    const/16 v7, 0xfd

    if-ne v0, v7, :cond_9

    .line 286
    const-string/jumbo v0, "notification_event_data"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 287
    const-string/jumbo v0, "notification_event_data"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 288
    invoke-static {v0}, Lcom/twitter/library/platform/notifications/NotificationParser;->a(Ljava/lang/String;)Lcom/twitter/model/dms/p;

    move-result-object v0

    .line 289
    invoke-virtual {v0}, Lcom/twitter/model/dms/p;->c()Ljava/util/List;

    move-result-object v0

    const/4 v7, 0x0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/z;

    .line 290
    const-string/jumbo v7, "notification_dm_data"

    sget-object v8, Lcom/twitter/model/dms/z;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v5, v7, v0, v8}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    .line 295
    :cond_9
    const-string/jumbo v0, "text"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 296
    const-string/jumbo v0, "text"

    const-string/jumbo v7, "text"

    invoke-virtual {p0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v0, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    :goto_4
    const-string/jumbo v0, "title"

    const-string/jumbo v7, "title"

    invoke-virtual {p0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v0, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    const-string/jumbo v0, "scribe_target"

    const-string/jumbo v7, "scribe_target"

    invoke-virtual {p0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v0, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    const-string/jumbo v0, "sound"

    const-string/jumbo v7, "sound"

    invoke-virtual {p0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v0, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    const-string/jumbo v0, "category_type"

    iget v7, v3, Lcom/twitter/library/platform/notifications/s;->b:I

    invoke-virtual {v5, v0, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 308
    const-string/jumbo v0, "recipient_id"

    iget-object v7, v4, Lcfw;->b:Lcfv;

    iget-wide v8, v7, Lcfv;->b:J

    invoke-virtual {v5, v0, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 309
    const-string/jumbo v0, "impression_id"

    invoke-virtual {v5, v0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    iget v0, v3, Lcom/twitter/library/platform/notifications/s;->b:I

    const/16 v7, 0x9

    if-ne v0, v7, :cond_f

    .line 313
    const-string/jumbo v0, "notification_setting_key"

    iget-object v7, v3, Lcom/twitter/library/platform/notifications/s;->a:Ljava/lang/String;

    invoke-virtual {v5, v0, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    const-string/jumbo v0, "uri"

    const-string/jumbo v7, "uri"

    invoke-virtual {p0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v0, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    const-string/jumbo v0, "uri_new"

    invoke-virtual {v2}, Lcom/twitter/library/platform/notifications/e;->m()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v0, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v3, Lcom/twitter/library/platform/notifications/s;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "_opt_out_count"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 317
    const/4 v3, 0x0

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 318
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    add-int/lit8 v3, v3, 0x1

    invoke-interface {v7, v0, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 331
    :cond_a
    :goto_5
    const-string/jumbo v0, "ticker"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 332
    const-string/jumbo v0, "ticker"

    const-string/jumbo v3, "ticker"

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    :cond_b
    const-string/jumbo v0, "should_show_negative_feedback"

    invoke-virtual {v2}, Lcom/twitter/library/platform/notifications/e;->n()Z

    move-result v3

    invoke-virtual {v5, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 335
    const-string/jumbo v0, "badge_count"

    invoke-virtual {v2}, Lcom/twitter/library/platform/notifications/e;->o()I

    move-result v2

    invoke-virtual {v5, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 336
    new-instance v0, Lcom/twitter/library/platform/PushService$a;

    invoke-direct {v0}, Lcom/twitter/library/platform/PushService$a;-><init>()V

    .line 337
    iput-object v6, v0, Lcom/twitter/library/platform/PushService$a;->a:Lcfu;

    .line 338
    iput-object v4, v0, Lcom/twitter/library/platform/PushService$a;->b:Lcfw;

    .line 339
    iput-object v1, v0, Lcom/twitter/library/platform/PushService$a;->c:Ljava/util/List;

    .line 340
    iput-object v5, v0, Lcom/twitter/library/platform/PushService$a;->d:Landroid/os/Bundle;

    goto/16 :goto_0

    .line 280
    :cond_c
    new-instance v0, Lcom/twitter/library/platform/PushService$InvalidNotificationPayloadException;

    const-string/jumbo v1, "Missing notification_event_data"

    invoke-direct {v0, v1}, Lcom/twitter/library/platform/PushService$InvalidNotificationPayloadException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 281
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 297
    :cond_d
    if-eqz v6, :cond_e

    .line 298
    const-string/jumbo v0, "text"

    iget-object v7, v6, Lcfu;->e:Ljava/lang/String;

    invoke-virtual {v5, v0, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 300
    :cond_e
    new-instance v0, Lcom/twitter/library/platform/PushService$InvalidNotificationPayloadException;

    const-string/jumbo v1, "Missing text"

    invoke-direct {v0, v1}, Lcom/twitter/library/platform/PushService$InvalidNotificationPayloadException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 301
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 319
    :cond_f
    iget v0, v3, Lcom/twitter/library/platform/notifications/s;->b:I

    const/16 v7, 0x10e

    if-ne v0, v7, :cond_11

    .line 320
    const-string/jumbo v0, "uri"

    const-string/jumbo v3, "uri"

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    invoke-static {}, Lcom/twitter/library/platform/notifications/q;->e()Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v3, "presentation_type"

    .line 322
    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 323
    if-eqz v0, :cond_10

    .line 324
    const-string/jumbo v3, "presentation_type"

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v5, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_5

    .line 326
    :cond_10
    const-string/jumbo v0, "presentation_type"

    const/4 v3, 0x0

    invoke-virtual {v5, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_5

    .line 328
    :cond_11
    iget v0, v3, Lcom/twitter/library/platform/notifications/s;->b:I

    const/16 v3, 0x124

    if-ne v0, v3, :cond_a

    .line 329
    const-string/jumbo v0, "uri"

    const-string/jumbo v3, "uri"

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 259
    :catch_0
    move-exception v1

    goto/16 :goto_2
.end method

.method static a(Landroid/os/Bundle;)Lcom/twitter/library/platform/notifications/u;
    .locals 6
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 492
    new-instance v1, Lcom/twitter/library/platform/notifications/u;

    const-string/jumbo v0, ""

    const-wide/16 v2, -0x1

    const/4 v4, 0x0

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/twitter/library/platform/notifications/u;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    .line 493
    new-instance v0, Lcom/twitter/library/platform/notifications/r$a;

    invoke-direct {v0}, Lcom/twitter/library/platform/notifications/r$a;-><init>()V

    .line 494
    invoke-virtual {v0, v5}, Lcom/twitter/library/platform/notifications/r$a;->a(I)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    .line 495
    invoke-virtual {v0, v5}, Lcom/twitter/library/platform/notifications/r$a;->c(I)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    const-string/jumbo v2, "impression_id"

    .line 496
    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/platform/notifications/r$a;->a(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    const-string/jumbo v2, "uri"

    .line 497
    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/platform/notifications/r$a;->f(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    const-string/jumbo v2, "text"

    .line 498
    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/platform/notifications/r$a;->c(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    const-string/jumbo v2, "title"

    .line 499
    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/platform/notifications/r$a;->b(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    const-string/jumbo v2, "scribe_target"

    .line 500
    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/platform/notifications/r$a;->e(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    .line 501
    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/r$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/platform/notifications/r;

    iput-object v0, v1, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    .line 502
    return-object v1
.end method

.method private a(Lcom/twitter/library/platform/notifications/u;Z)V
    .locals 3

    .prologue
    .line 178
    if-eqz p2, :cond_0

    .line 179
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/twitter/library/platform/PushService;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 185
    :goto_0
    const-string/jumbo v1, "push_data"

    sget-object v2, Lcom/twitter/library/platform/notifications/u;->a:Lcom/twitter/util/serialization/l;

    .line 186
    invoke-static {p1, v2}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    .line 185
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 189
    sget-object v1, Lcom/twitter/database/schema/a;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/platform/PushService;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 190
    return-void

    .line 181
    :cond_0
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/twitter/library/platform/PushService;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static a(Landroid/os/Bundle;J)Z
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 361
    const-string/jumbo v1, "actions"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    .line 362
    const-string/jumbo v2, "scribe_target"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 364
    invoke-static {v2}, Lcom/twitter/library/platform/notifications/l;->a(Ljava/lang/String;)Z

    move-result v2

    .line 365
    if-eqz v1, :cond_1

    if-nez v2, :cond_0

    const-string/jumbo v1, "android_notification_actions_from_push_payload_enabled"

    invoke-static {p1, p2, v1, v0}, Lcoj;->a(JLjava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private static b(Landroid/os/Bundle;)Lcom/twitter/model/dms/s;
    .locals 2

    .prologue
    .line 346
    const-string/jumbo v0, "notification_event_data"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 347
    const-string/jumbo v1, "is_partial"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 348
    invoke-static {v0, v1}, Lcom/twitter/library/platform/notifications/NotificationParser;->a(Ljava/lang/String;Z)Lcom/twitter/model/dms/s;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 162
    new-instance v0, Lcom/twitter/library/platform/notifications/e;

    invoke-direct {v0, p1}, Lcom/twitter/library/platform/notifications/e;-><init>(Landroid/os/Bundle;)V

    .line 163
    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/e;->f()J

    move-result-wide v2

    .line 165
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const-string/jumbo v1, "oem_launcher_badge_push_update"

    const/4 v4, 0x0

    .line 166
    invoke-static {v2, v3, v1, v4}, Lcoj;->a(JLjava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 170
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/e;->o()I

    move-result v0

    .line 171
    if-ltz v0, :cond_0

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    cmp-long v1, v4, v2

    if-nez v1, :cond_0

    .line 172
    invoke-static {p0, v0}, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;->a(Landroid/content/Context;I)V

    goto :goto_0
.end method


# virtual methods
.method a(Landroid/os/Bundle;Ljava/lang/String;)Lcom/twitter/library/platform/PushService$a;
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 195
    const-string/jumbo v0, "schema"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 196
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 197
    :goto_0
    const-string/jumbo v1, "PushService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Payload schema="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-static {p1, v1, v0, p2}, Lcom/twitter/library/platform/PushService;->a(Landroid/os/Bundle;Landroid/content/SharedPreferences;ILjava/lang/String;)Lcom/twitter/library/platform/PushService$a;

    move-result-object v0

    return-object v0

    .line 196
    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method a(Lcom/twitter/library/platform/PushService$a;)Lcom/twitter/library/platform/notifications/u;
    .locals 24
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 378
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/twitter/library/platform/PushService$a;->d:Landroid/os/Bundle;

    .line 379
    const-string/jumbo v2, "recipient_id"

    const-wide/16 v6, 0x0

    invoke-virtual {v5, v2, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 380
    invoke-static {v6, v7}, Lcom/twitter/library/util/b;->a(J)Lakm;

    move-result-object v2

    .line 381
    if-nez v2, :cond_0

    .line 382
    new-instance v2, Lcpb;

    invoke-direct {v2, v6, v7}, Lcpb;-><init>(J)V

    new-instance v3, Lcom/twitter/library/platform/PushService$InvalidNotificationPayloadException;

    const-string/jumbo v4, "Account does not exist on this device."

    invoke-direct {v3, v4}, Lcom/twitter/library/platform/PushService$InvalidNotificationPayloadException;-><init>(Ljava/lang/String;)V

    .line 383
    invoke-virtual {v2, v3}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v2

    .line 382
    invoke-static {v2}, Lcpd;->c(Lcpb;)V

    .line 384
    const/4 v4, 0x0

    .line 466
    :goto_0
    return-object v4

    .line 388
    :cond_0
    invoke-virtual {v2}, Lakm;->d()Ljava/lang/String;

    move-result-object v8

    .line 391
    const-string/jumbo v2, "impression_id"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 392
    const-string/jumbo v2, "category_type"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    .line 393
    const-string/jumbo v2, "priority"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    .line 394
    const-string/jumbo v2, "title"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 395
    const-string/jumbo v2, "text"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 396
    const-string/jumbo v2, "scribe_target"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 397
    const-string/jumbo v2, "notification_setting_key"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 398
    const-string/jumbo v2, "sound"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 399
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/platform/PushService$a;->b:Lcfw;

    if-eqz v2, :cond_2

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/platform/PushService$a;->b:Lcfw;

    iget-object v2, v2, Lcfw;->c:Lcfv;

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    move v4, v2

    .line 400
    :goto_1
    if-eqz v4, :cond_3

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/platform/PushService$a;->b:Lcfw;

    iget-object v2, v2, Lcfw;->c:Lcfv;

    iget-object v9, v2, Lcfv;->c:Ljava/lang/String;

    .line 401
    :goto_2
    if-eqz v4, :cond_4

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/platform/PushService$a;->b:Lcfw;

    iget-object v2, v2, Lcfw;->c:Lcfv;

    iget-object v2, v2, Lcfv;->e:Ljava/lang/String;

    move-object v3, v2

    .line 402
    :goto_3
    if-eqz v4, :cond_5

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/platform/PushService$a;->b:Lcfw;

    iget-object v2, v2, Lcfw;->c:Lcfv;

    iget-object v2, v2, Lcfv;->d:Ljava/lang/String;

    .line 403
    :goto_4
    const-string/jumbo v4, "ticker"

    invoke-virtual {v5, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 404
    const-string/jumbo v4, "should_show_negative_feedback"

    invoke-virtual {v5, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v21

    .line 405
    const-string/jumbo v4, "badge_count"

    invoke-virtual {v5, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v22

    .line 407
    invoke-static {}, Lcqj;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 408
    const-string/jumbo v4, "PushService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "impression_id: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v4, v11}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    const-string/jumbo v4, "PushService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "category_type: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v4, v11}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const-string/jumbo v4, "PushService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "priority:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v4, v11}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    const-string/jumbo v4, "PushService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "sender_name: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v4, v11}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    const-string/jumbo v4, "PushService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "recipient_name: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v4, v11}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    const-string/jumbo v4, "PushService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "recipient_id: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v4, v11}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    const-string/jumbo v4, "PushService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "text: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v4, v11}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    const-string/jumbo v4, "PushService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "title: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v4, v11}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    const-string/jumbo v4, "PushService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "scribe_target: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v4, v11}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    const-string/jumbo v4, "PushService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "notification_setting_key: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v4, v11}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    const-string/jumbo v4, "PushService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "sound: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v4, v11}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    const-string/jumbo v4, "PushService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "ticker: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v4, v11}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    const-string/jumbo v4, "PushService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "should_show_negative_feedback: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v4, v11}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    const-string/jumbo v4, "PushService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "badge_count: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v4, v11}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    :cond_1
    invoke-static {}, Lcom/twitter/library/provider/j;->c()Lcom/twitter/library/provider/j;

    move-result-object v11

    .line 425
    invoke-static {v6, v7}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v12

    .line 427
    new-instance v4, Lcom/twitter/library/platform/notifications/u;

    move-object/from16 v0, v19

    invoke-direct {v4, v8, v6, v7, v0}, Lcom/twitter/library/platform/notifications/u;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    .line 428
    new-instance v19, Lcom/twitter/library/platform/notifications/r$a;

    invoke-direct/range {v19 .. v19}, Lcom/twitter/library/platform/notifications/r$a;-><init>()V

    const/16 v23, 0x1

    .line 429
    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/r$a;->c(I)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v19

    .line 430
    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lcom/twitter/library/platform/notifications/r$a;->a(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v10

    .line 431
    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/twitter/library/platform/notifications/r$a;->c(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v10

    .line 432
    invoke-virtual {v10, v15}, Lcom/twitter/library/platform/notifications/r$a;->b(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v10

    .line 433
    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lcom/twitter/library/platform/notifications/r$a;->e(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v10

    .line 434
    invoke-virtual {v10, v2}, Lcom/twitter/library/platform/notifications/r$a;->j(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v2

    .line 435
    invoke-virtual {v2, v3}, Lcom/twitter/library/platform/notifications/r$a;->k(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/platform/PushService$a;->a:Lcfu;

    if-eqz v2, :cond_6

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/platform/PushService$a;->a:Lcfu;

    iget-object v2, v2, Lcfu;->f:Ljava/lang/String;

    .line 436
    :goto_5
    invoke-virtual {v3, v2}, Lcom/twitter/library/platform/notifications/r$a;->g(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v2

    .line 437
    invoke-virtual {v2, v13}, Lcom/twitter/library/platform/notifications/r$a;->e(I)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/twitter/library/platform/PushService$a;->b:Lcfw;

    .line 438
    invoke-virtual {v2, v3}, Lcom/twitter/library/platform/notifications/r$a;->a(Lcfw;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/twitter/library/platform/PushService$a;->a:Lcfu;

    .line 439
    invoke-virtual {v2, v3}, Lcom/twitter/library/platform/notifications/r$a;->a(Lcfu;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v2

    .line 440
    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/twitter/library/platform/notifications/r$a;->h(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v2

    .line 441
    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/twitter/library/platform/notifications/r$a;->i(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v2

    .line 442
    move/from16 v0, v21

    invoke-virtual {v2, v0}, Lcom/twitter/library/platform/notifications/r$a;->a(Z)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v2

    .line 443
    invoke-virtual {v12, v9}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/library/platform/notifications/r$a;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v2

    .line 444
    invoke-static {v14}, Lcom/twitter/library/platform/notifications/q;->b(I)I

    move-result v3

    invoke-static {v5, v3}, Lcom/twitter/library/platform/PushService;->a(Landroid/os/Bundle;I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/twitter/library/platform/notifications/r$a;->a(I)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v2

    .line 445
    invoke-virtual {v2, v14}, Lcom/twitter/library/platform/notifications/r$a;->b(I)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v2

    .line 446
    move/from16 v0, v22

    invoke-virtual {v2, v0}, Lcom/twitter/library/platform/notifications/r$a;->f(I)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v2

    .line 447
    invoke-virtual {v2}, Lcom/twitter/library/platform/notifications/r$a;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/platform/notifications/r;

    iput-object v2, v4, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    .line 449
    iget-object v2, v4, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/twitter/library/platform/PushService$a;->c:Ljava/util/List;

    iput-object v3, v2, Lcom/twitter/library/platform/notifications/r;->v:Ljava/util/List;

    .line 450
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/platform/PushService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 451
    new-instance v13, Laut;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v13, v3}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 453
    invoke-static {}, Lcom/twitter/library/platform/notifications/q;->a()Ljava/util/Map;

    move-result-object v3

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v3, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/platform/notifications/v;

    .line 454
    if-nez v3, :cond_7

    .line 455
    new-instance v2, Lcpb;

    invoke-direct {v2, v6, v7}, Lcpb;-><init>(J)V

    new-instance v3, Lcom/twitter/library/platform/PushService$InvalidNotificationPayloadException;

    const-string/jumbo v4, "Unknown notification data converter."

    invoke-direct {v3, v4}, Lcom/twitter/library/platform/PushService$InvalidNotificationPayloadException;-><init>(Ljava/lang/String;)V

    .line 456
    invoke-virtual {v2, v3}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v2

    .line 455
    invoke-static {v2}, Lcpd;->c(Lcpb;)V

    .line 457
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 399
    :cond_2
    const/4 v2, 0x0

    move v4, v2

    goto/16 :goto_1

    .line 400
    :cond_3
    const-string/jumbo v9, ""

    goto/16 :goto_2

    .line 401
    :cond_4
    const-string/jumbo v2, ""

    move-object v3, v2

    goto/16 :goto_3

    .line 402
    :cond_5
    const-string/jumbo v2, ""

    goto/16 :goto_4

    .line 435
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_5

    :cond_7
    move-object/from16 v10, p0

    .line 459
    invoke-virtual/range {v3 .. v13}, Lcom/twitter/library/platform/notifications/v;->a(Lcom/twitter/library/platform/notifications/u;Landroid/os/Bundle;JLjava/lang/String;Ljava/lang/String;Landroid/content/Context;Lcom/twitter/library/provider/j;Lcom/twitter/library/provider/t;Laut;)V

    .line 461
    invoke-virtual {v13}, Laut;->a()V

    .line 462
    iget-object v3, v4, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    iget v3, v3, Lcom/twitter/library/platform/notifications/r;->h:I

    const/4 v5, 0x3

    if-ne v3, v5, :cond_8

    .line 463
    invoke-static {v2, v6, v7}, Lcom/twitter/library/platform/TwitterDataSyncService;->d(Landroid/content/Context;J)V

    .line 465
    :cond_8
    invoke-static {v6, v7, v6, v7}, Lcom/twitter/library/platform/PushService;->a(JJ)I

    move-result v2

    iput v2, v4, Lcom/twitter/library/platform/notifications/u;->e:I

    goto/16 :goto_0
.end method

.method protected a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 208
    new-instance v1, Lcom/twitter/library/platform/notifications/PushRegistration$a;

    invoke-direct {v1}, Lcom/twitter/library/platform/notifications/PushRegistration$a;-><init>()V

    const-string/jumbo v2, "onInvalidIntent"

    if-nez p2, :cond_0

    const-string/jumbo v0, "intent was null"

    .line 209
    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/platform/notifications/PushRegistration$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/platform/notifications/PushRegistration$DebugNotificationException;

    const-string/jumbo v2, "onInvalidIntent() invoked"

    invoke-direct {v1, v2}, Lcom/twitter/library/platform/notifications/PushRegistration$DebugNotificationException;-><init>(Ljava/lang/String;)V

    .line 211
    invoke-virtual {v0, v1}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v0

    .line 208
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    .line 212
    return-void

    .line 208
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "action was: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 210
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 121
    invoke-static {}, Lcpd;->a()Lcpd;

    move-result-object v0

    invoke-virtual {v0}, Lcpd;->b()Lcpa;

    move-result-object v1

    .line 122
    invoke-virtual {v1}, Lcpa;->a()I

    .line 124
    :try_start_0
    new-instance v0, Lcom/twitter/library/platform/notifications/e;

    invoke-direct {v0, p2}, Lcom/twitter/library/platform/notifications/e;-><init>(Landroid/os/Bundle;)V

    .line 125
    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/e;->b()Ljava/lang/String;

    move-result-object v2

    .line 126
    const-string/jumbo v3, "impression_id"

    invoke-virtual {v1, v3, v2}, Lcpa;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/e;->f()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/e;->i()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v5, v3, v2}, Lcom/google/android/gcm/GCMScribeReporter;->a(JLjava/lang/String;Ljava/lang/String;)V

    .line 129
    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/e;->f()J

    move-result-wide v4

    invoke-static {v4, v5, v2}, Lcom/google/android/gcm/GCMScribeReporter;->a(JLjava/lang/String;)V

    .line 131
    const-string/jumbo v3, "type"

    const-string/jumbo v4, "0"

    invoke-virtual {p2, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 132
    const/16 v4, 0xd2

    if-ne v3, v4, :cond_0

    .line 133
    invoke-static {p1, p2}, Lcom/twitter/library/platform/PushService;->b(Landroid/content/Context;Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    invoke-virtual {v1}, Lcpa;->b()I

    .line 157
    invoke-virtual {v1}, Lcpa;->e()V

    .line 159
    :goto_0
    return-void

    .line 138
    :cond_0
    :try_start_1
    invoke-static {p1}, Landroid/support/v4/app/NotificationManagerCompat;->from(Landroid/content/Context;)Landroid/support/v4/app/NotificationManagerCompat;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/NotificationManagerCompat;->areNotificationsEnabled()Z

    move-result v4

    if-nez v4, :cond_1

    .line 139
    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/e;->f()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/e;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lcom/google/android/gcm/GCMScribeReporter;->b(JLjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 156
    invoke-virtual {v1}, Lcpa;->b()I

    .line 157
    invoke-virtual {v1}, Lcpa;->e()V

    goto :goto_0

    .line 143
    :cond_1
    const/16 v0, 0x11d

    if-ne v3, v0, :cond_3

    .line 145
    :try_start_2
    invoke-static {p2}, Lcom/twitter/library/platform/PushService;->a(Landroid/os/Bundle;)Lcom/twitter/library/platform/notifications/u;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/platform/PushService;->a(Lcom/twitter/library/platform/notifications/u;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 156
    :cond_2
    :goto_1
    invoke-virtual {v1}, Lcpa;->b()I

    .line 157
    invoke-virtual {v1}, Lcpa;->e()V

    goto :goto_0

    .line 146
    :cond_3
    const/16 v0, 0x127

    if-eq v3, v0, :cond_2

    .line 147
    :try_start_3
    invoke-virtual {p0, p2, v2}, Lcom/twitter/library/platform/PushService;->a(Landroid/os/Bundle;Ljava/lang/String;)Lcom/twitter/library/platform/PushService$a;

    move-result-object v0

    .line 148
    if-eqz v0, :cond_2

    .line 149
    invoke-virtual {p0, v0}, Lcom/twitter/library/platform/PushService;->a(Lcom/twitter/library/platform/PushService$a;)Lcom/twitter/library/platform/notifications/u;

    move-result-object v0

    .line 150
    if-eqz v0, :cond_2

    .line 151
    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/platform/PushService;->a(Lcom/twitter/library/platform/notifications/u;Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 156
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcpa;->b()I

    .line 157
    invoke-virtual {v1}, Lcpa;->e()V

    throw v0
.end method

.method protected a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 203
    invoke-static {p1, p2}, Lcom/twitter/library/platform/notifications/PushRegistration;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 204
    return-void
.end method

.method protected b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 217
    invoke-static {p1, p2}, Lcom/twitter/library/platform/notifications/PushRegistration;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 218
    return-void
.end method
