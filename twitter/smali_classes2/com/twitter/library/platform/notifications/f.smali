.class public Lcom/twitter/library/platform/notifications/f;
.super Lcom/twitter/library/platform/notifications/v;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/twitter/library/platform/notifications/v;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/platform/notifications/u;Landroid/os/Bundle;JLjava/lang/String;Ljava/lang/String;Landroid/content/Context;Lcom/twitter/library/provider/j;Lcom/twitter/library/provider/t;Laut;)V
    .locals 17

    .prologue
    .line 22
    const/16 v4, 0x100

    move-object/from16 v0, p8

    move-wide/from16 v1, p3

    move-object/from16 v3, p10

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/twitter/library/provider/j;->a(JILaut;)I

    .line 23
    const-string/jumbo v4, "text"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 24
    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    const/16 v7, 0xa

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    .line 25
    invoke-virtual {v4}, Lcom/twitter/library/platform/notifications/r;->e()J

    move-result-wide v10

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v4, p9

    move-wide/from16 v5, p3

    .line 24
    invoke-virtual/range {v4 .. v14}, Lcom/twitter/library/provider/t;->a(JILjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;[B)I

    move-result v4

    iput v4, v15, Lcom/twitter/library/platform/notifications/r;->t:I

    .line 26
    const-string/jumbo v4, "uri_new"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 27
    invoke-static {v4}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 28
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    iput-object v4, v5, Lcom/twitter/library/platform/notifications/r;->r:Ljava/lang/String;

    .line 32
    :goto_0
    const/16 v4, 0x100

    move-object/from16 v0, p1

    iput v4, v0, Lcom/twitter/library/platform/notifications/u;->f:I

    .line 33
    return-void

    .line 30
    :cond_0
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    const-string/jumbo v5, "uri"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/twitter/library/platform/notifications/r;->r:Ljava/lang/String;

    goto :goto_0
.end method
