.class public final Lcom/twitter/library/platform/notifications/r$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/platform/notifications/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/library/platform/notifications/r;",
        "Lcom/twitter/library/platform/notifications/r$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 320
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/util/serialization/b;-><init>(I)V

    .line 321
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/platform/notifications/r$a;
    .locals 1

    .prologue
    .line 396
    new-instance v0, Lcom/twitter/library/platform/notifications/r$a;

    invoke-direct {v0}, Lcom/twitter/library/platform/notifications/r$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/platform/notifications/r$a;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 354
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/twitter/library/platform/notifications/r$a;->a(I)Lcom/twitter/library/platform/notifications/r$a;

    .line 355
    if-ge p3, v2, :cond_0

    .line 356
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    .line 358
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/twitter/library/platform/notifications/r$a;->c(I)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    .line 359
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/r$a;->a(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    .line 360
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/r$a;->b(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    .line 361
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/r$a;->c(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    .line 362
    if-ge p3, v2, :cond_1

    .line 363
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    .line 364
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    .line 365
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    .line 367
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/library/platform/notifications/r$a;->d(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    .line 368
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/r$a;->e(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    .line 369
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/r$a;->f(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    .line 370
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/r$a;->g(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    .line 371
    if-ge p3, v2, :cond_2

    .line 372
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    .line 373
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    .line 375
    :cond_2
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/twitter/library/platform/notifications/r$a;->d(I)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    .line 376
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/r$a;->j(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    .line 377
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/r$a;->h(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v1

    sget-object v0, Lcfu;->a:Lcom/twitter/util/serialization/l;

    .line 378
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfu;

    invoke-virtual {v1, v0}, Lcom/twitter/library/platform/notifications/r$a;->a(Lcfu;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v1

    sget-object v0, Lcfw;->a:Lcom/twitter/util/serialization/l;

    .line 379
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfw;

    invoke-virtual {v1, v0}, Lcom/twitter/library/platform/notifications/r$a;->a(Lcfw;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v1

    sget-object v0, Lcfr;->b:Lcom/twitter/util/serialization/l;

    .line 380
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/twitter/library/platform/notifications/r$a;->b(Ljava/util/List;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v1

    sget-object v0, Lcom/twitter/library/platform/notifications/h;->b:Lcom/twitter/util/serialization/l;

    .line 381
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/twitter/library/platform/notifications/r$a;->a(Ljava/util/List;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    .line 382
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/r$a;->e(I)Lcom/twitter/library/platform/notifications/r$a;

    .line 383
    if-ge p3, v2, :cond_3

    .line 384
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    .line 386
    :cond_3
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/library/platform/notifications/r$a;->i(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    .line 387
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/r$a;->a(Z)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    .line 388
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/r$a;->k(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    .line 389
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/r$a;->f(I)Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    .line 390
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/r$a;->b(I)Lcom/twitter/library/platform/notifications/r$a;

    .line 391
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 316
    check-cast p2, Lcom/twitter/library/platform/notifications/r$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/platform/notifications/r$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/library/platform/notifications/r$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/platform/notifications/r;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 326
    iget v0, p2, Lcom/twitter/library/platform/notifications/r;->h:I

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/library/platform/notifications/r;->o:I

    .line 327
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/r;->b:Ljava/lang/String;

    .line 328
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/r;->p:Ljava/lang/String;

    .line 329
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/r;->j:Ljava/lang/String;

    .line 330
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/r;->q:Ljava/lang/String;

    .line 331
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/r;->k:Ljava/lang/String;

    .line 332
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/r;->r:Ljava/lang/String;

    .line 333
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/r;->s:Ljava/lang/String;

    .line 334
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/library/platform/notifications/r;->t:I

    .line 335
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/r;->w:Ljava/lang/String;

    .line 336
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/r;->c:Ljava/lang/String;

    .line 337
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/r;->m:Lcfu;

    sget-object v2, Lcfu;->a:Lcom/twitter/util/serialization/l;

    .line 338
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    sget-object v2, Lcfw;->a:Lcom/twitter/util/serialization/l;

    .line 339
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/r;->v:Ljava/util/List;

    sget-object v2, Lcfr;->b:Lcom/twitter/util/serialization/l;

    .line 340
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/r;->u:Ljava/util/List;

    sget-object v2, Lcom/twitter/library/platform/notifications/h;->b:Lcom/twitter/util/serialization/l;

    .line 341
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/library/platform/notifications/r;->d:I

    .line 342
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/r;->e:Ljava/lang/String;

    .line 343
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/library/platform/notifications/r;->f:Z

    .line 344
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Z)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/r;->g:Ljava/lang/String;

    .line 345
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/library/platform/notifications/r;->l:I

    .line 346
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/library/platform/notifications/r;->i:I

    .line 347
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    .line 348
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 316
    check-cast p2, Lcom/twitter/library/platform/notifications/r;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/platform/notifications/r$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/platform/notifications/r;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 316
    invoke-virtual {p0}, Lcom/twitter/library/platform/notifications/r$b;->a()Lcom/twitter/library/platform/notifications/r$a;

    move-result-object v0

    return-object v0
.end method
