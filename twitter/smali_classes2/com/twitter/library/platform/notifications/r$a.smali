.class public final Lcom/twitter/library/platform/notifications/r$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/platform/notifications/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/library/platform/notifications/r;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:I

.field private m:I

.field private n:Ljava/lang/String;

.field private o:Z

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/platform/notifications/h;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcfu;

.field private r:Lcfw;

.field private s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcfr;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 166
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/library/platform/notifications/r$a;->v:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/platform/notifications/r$a;)I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/twitter/library/platform/notifications/r$a;->a:I

    return v0
.end method

.method static synthetic b(Lcom/twitter/library/platform/notifications/r$a;)I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/twitter/library/platform/notifications/r$a;->b:I

    return v0
.end method

.method static synthetic c(Lcom/twitter/library/platform/notifications/r$a;)I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/twitter/library/platform/notifications/r$a;->c:I

    return v0
.end method

.method static synthetic d(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r$a;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r$a;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r$a;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r$a;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r$a;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r$a;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r$a;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/library/platform/notifications/r$a;)I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/twitter/library/platform/notifications/r$a;->l:I

    return v0
.end method

.method static synthetic m(Lcom/twitter/library/platform/notifications/r$a;)I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/twitter/library/platform/notifications/r$a;->m:I

    return v0
.end method

.method static synthetic n(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r$a;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Lcom/twitter/library/platform/notifications/r$a;)Z
    .locals 1

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/twitter/library/platform/notifications/r$a;->o:Z

    return v0
.end method

.method static synthetic p(Lcom/twitter/library/platform/notifications/r$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r$a;->p:Ljava/util/List;

    return-object v0
.end method

.method static synthetic q(Lcom/twitter/library/platform/notifications/r$a;)Lcfu;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r$a;->q:Lcfu;

    return-object v0
.end method

.method static synthetic r(Lcom/twitter/library/platform/notifications/r$a;)Lcfw;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r$a;->r:Lcfw;

    return-object v0
.end method

.method static synthetic s(Lcom/twitter/library/platform/notifications/r$a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r$a;->s:Ljava/util/List;

    return-object v0
.end method

.method static synthetic t(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r$a;->t:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic u(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r$a;->u:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic v(Lcom/twitter/library/platform/notifications/r$a;)I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/twitter/library/platform/notifications/r$a;->v:I

    return v0
.end method


# virtual methods
.method public a(I)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0

    .prologue
    .line 170
    iput p1, p0, Lcom/twitter/library/platform/notifications/r$a;->a:I

    .line 171
    return-object p0
.end method

.method public a(Lcfu;)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/r$a;->q:Lcfu;

    .line 267
    return-object p0
.end method

.method public a(Lcfw;)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0

    .prologue
    .line 272
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/r$a;->r:Lcfw;

    .line 273
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/platform/notifications/r$a;
    .locals 1

    .prologue
    .line 296
    if-eqz p1, :cond_0

    .line 297
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/r$a;->t:Ljava/lang/String;

    .line 298
    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/r$a;->u:Ljava/lang/String;

    .line 300
    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/r$a;->d:Ljava/lang/String;

    .line 189
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/platform/notifications/h;",
            ">;)",
            "Lcom/twitter/library/platform/notifications/r$a;"
        }
    .end annotation

    .prologue
    .line 260
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/r$a;->p:Ljava/util/List;

    .line 261
    return-object p0
.end method

.method public a(Z)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0

    .prologue
    .line 254
    iput-boolean p1, p0, Lcom/twitter/library/platform/notifications/r$a;->o:Z

    .line 255
    return-object p0
.end method

.method public b(I)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0

    .prologue
    .line 176
    iput p1, p0, Lcom/twitter/library/platform/notifications/r$a;->b:I

    .line 177
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0

    .prologue
    .line 194
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/r$a;->e:Ljava/lang/String;

    .line 195
    return-object p0
.end method

.method public b(Ljava/util/List;)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcfr;",
            ">;)",
            "Lcom/twitter/library/platform/notifications/r$a;"
        }
    .end annotation

    .prologue
    .line 278
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/r$a;->s:Ljava/util/List;

    .line 279
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/twitter/library/platform/notifications/r$a;->e()Lcom/twitter/library/platform/notifications/r;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0

    .prologue
    .line 182
    iput p1, p0, Lcom/twitter/library/platform/notifications/r$a;->c:I

    .line 183
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/r$a;->f:Ljava/lang/String;

    .line 201
    return-object p0
.end method

.method public d(I)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0

    .prologue
    .line 236
    iput p1, p0, Lcom/twitter/library/platform/notifications/r$a;->l:I

    .line 237
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/r$a;->g:Ljava/lang/String;

    .line 207
    return-object p0
.end method

.method public e(I)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0

    .prologue
    .line 242
    iput p1, p0, Lcom/twitter/library/platform/notifications/r$a;->m:I

    .line 243
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0

    .prologue
    .line 212
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/r$a;->h:Ljava/lang/String;

    .line 213
    return-object p0
.end method

.method protected e()Lcom/twitter/library/platform/notifications/r;
    .locals 2

    .prologue
    .line 312
    new-instance v0, Lcom/twitter/library/platform/notifications/r;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/platform/notifications/r;-><init>(Lcom/twitter/library/platform/notifications/r$a;Lcom/twitter/library/platform/notifications/r$1;)V

    return-object v0
.end method

.method public f(I)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0

    .prologue
    .line 305
    iput p1, p0, Lcom/twitter/library/platform/notifications/r$a;->v:I

    .line 306
    return-object p0
.end method

.method public f(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/r$a;->i:Ljava/lang/String;

    .line 219
    return-object p0
.end method

.method public g(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/r$a;->j:Ljava/lang/String;

    .line 225
    return-object p0
.end method

.method public h(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/r$a;->k:Ljava/lang/String;

    .line 231
    return-object p0
.end method

.method public i(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0

    .prologue
    .line 248
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/r$a;->n:Ljava/lang/String;

    .line 249
    return-object p0
.end method

.method public j(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/r$a;->t:Ljava/lang/String;

    .line 285
    return-object p0
.end method

.method public k(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/r$a;
    .locals 0

    .prologue
    .line 290
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/r$a;->u:Ljava/lang/String;

    .line 291
    return-object p0
.end method
