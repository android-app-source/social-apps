.class public Lcom/twitter/library/platform/notifications/a;
.super Lcom/evernote/android/job/Job;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/evernote/android/job/Job;-><init>()V

    return-void
.end method

.method private static b(Landroid/content/Context;)V
    .locals 10

    .prologue
    .line 41
    invoke-static {p0}, Landroid/support/v4/app/NotificationManagerCompat;->from(Landroid/content/Context;)Landroid/support/v4/app/NotificationManagerCompat;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationManagerCompat;->areNotificationsEnabled()Z

    move-result v1

    .line 42
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v0

    invoke-virtual {v0}, Lakn;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    .line 43
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/twitter/library/util/b;->c(Lakm;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 44
    :goto_1
    if-eqz v0, :cond_0

    .line 45
    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 46
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-string/jumbo v6, "notification"

    const-string/jumbo v7, ""

    const-string/jumbo v8, ""

    const-string/jumbo v9, "system_push_setting"

    if-eqz v1, :cond_2

    const-string/jumbo v0, "enabled"

    .line 47
    :goto_2
    invoke-static {v6, v7, v8, v9, v0}, Lcom/twitter/analytics/model/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/a;

    move-result-object v0

    invoke-direct {v3, v4, v5, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(JLcom/twitter/analytics/model/a;)V

    .line 46
    invoke-static {v3}, Lcpm;->a(Lcpk;)V

    .line 49
    invoke-static {p0, v4, v5}, Lcom/twitter/library/platform/notifications/t;->a(Landroid/content/Context;J)Lcom/twitter/library/platform/notifications/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/t;->a()Z

    move-result v0

    .line 50
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-string/jumbo v6, "app"

    const-string/jumbo v7, "notifications"

    const-string/jumbo v8, "settings"

    const-string/jumbo v9, "master_switch"

    if-eqz v0, :cond_3

    const-string/jumbo v0, "enabled"

    .line 51
    :goto_3
    invoke-static {v6, v7, v8, v9, v0}, Lcom/twitter/analytics/model/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/a;

    move-result-object v0

    invoke-direct {v3, v4, v5, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(JLcom/twitter/analytics/model/a;)V

    .line 50
    invoke-static {v3}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 43
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 46
    :cond_2
    const-string/jumbo v0, "disabled"

    goto :goto_2

    .line 50
    :cond_3
    const-string/jumbo v0, "disabled"

    goto :goto_3

    .line 55
    :cond_4
    return-void
.end method

.method public static l()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 58
    invoke-static {}, Lcom/evernote/android/job/d;->a()Lcom/evernote/android/job/d;

    move-result-object v0

    const-string/jumbo v1, "CheckSystemPushEnabled"

    invoke-virtual {v0, v1}, Lcom/evernote/android/job/d;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    new-instance v0, Lcom/evernote/android/job/JobRequest$a;

    const-string/jumbo v1, "CheckSystemPushEnabled"

    invoke-direct {v0, v1}, Lcom/evernote/android/job/JobRequest$a;-><init>(Ljava/lang/String;)V

    const-wide/32 v2, 0x5265c00

    .line 60
    invoke-virtual {v0, v2, v3}, Lcom/evernote/android/job/JobRequest$a;->b(J)Lcom/evernote/android/job/JobRequest$a;

    move-result-object v0

    .line 61
    invoke-virtual {v0, v4}, Lcom/evernote/android/job/JobRequest$a;->b(Z)Lcom/evernote/android/job/JobRequest$a;

    move-result-object v0

    .line 62
    invoke-virtual {v0, v4}, Lcom/evernote/android/job/JobRequest$a;->c(Z)Lcom/evernote/android/job/JobRequest$a;

    move-result-object v0

    .line 63
    invoke-virtual {v0, v4}, Lcom/evernote/android/job/JobRequest$a;->e(Z)Lcom/evernote/android/job/JobRequest$a;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest$a;->a()Lcom/evernote/android/job/JobRequest;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->t()I

    .line 67
    :cond_0
    return-void
.end method


# virtual methods
.method protected a(Lcom/evernote/android/job/Job$a;)Lcom/evernote/android/job/Job$Result;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/twitter/library/platform/notifications/a;->f()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/platform/notifications/a;->b(Landroid/content/Context;)V

    .line 35
    sget-object v0, Lcom/evernote/android/job/Job$Result;->a:Lcom/evernote/android/job/Job$Result;

    return-object v0
.end method
