.class public Lcom/twitter/library/platform/notifications/e;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/e;->a:Landroid/os/Bundle;

    .line 48
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/e;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "collapse_key"

    const-string/jumbo v2, "unknown"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/e;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "impression_id"

    const-string/jumbo v2, "not_provided"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/e;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "message_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()I
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/e;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "total_deleted"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 69
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()I
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/e;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "schema"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 74
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 82
    invoke-virtual {p0}, Lcom/twitter/library/platform/notifications/e;->e()I

    move-result v2

    .line 83
    if-nez v2, :cond_1

    .line 84
    iget-object v2, p0, Lcom/twitter/library/platform/notifications/e;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "user_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 85
    invoke-static {v2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 92
    :cond_0
    :goto_0
    return-wide v0

    .line 87
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/platform/notifications/e;->g()Lcfw;

    move-result-object v2

    .line 88
    if-eqz v2, :cond_0

    .line 89
    iget-object v0, v2, Lcfw;->b:Lcfv;

    iget-wide v0, v0, Lcfv;->b:J

    goto :goto_0
.end method

.method public g()Lcfw;
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/e;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "users"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 100
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-class v1, Lcfw;

    invoke-static {v0, v1}, Lcom/twitter/model/json/common/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfw;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Lcfu;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/e;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "tweet"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 106
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-class v1, Lcfu;

    invoke-static {v0, v1}, Lcom/twitter/model/json/common/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfu;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/e;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "scribe_target"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 116
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 120
    :goto_0
    return-object v0

    .line 119
    :cond_0
    new-instance v0, Lcom/twitter/library/platform/PushService$InvalidNotificationPayloadException;

    const-string/jumbo v1, "Missing scribe_target."

    invoke-direct {v0, v1}, Lcom/twitter/library/platform/PushService$InvalidNotificationPayloadException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 120
    invoke-virtual {p0}, Lcom/twitter/library/platform/notifications/e;->l()Lcom/twitter/library/platform/notifications/s;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/s;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public j()I
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/e;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "priority"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 126
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()I
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/e;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 133
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x9

    goto :goto_0
.end method

.method public l()Lcom/twitter/library/platform/notifications/s;
    .locals 4

    .prologue
    .line 143
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/e;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 144
    invoke-virtual {p0}, Lcom/twitter/library/platform/notifications/e;->k()I

    move-result v0

    .line 145
    invoke-static {v0}, Lcom/twitter/library/platform/notifications/q;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 148
    if-nez v1, :cond_0

    .line 149
    const/16 v0, 0x9

    .line 150
    const-string/jumbo v1, "unknown"

    .line 157
    :cond_0
    :goto_0
    invoke-static {}, Lcpd;->a()Lcpd;

    move-result-object v2

    invoke-virtual {v2}, Lcpd;->b()Lcpa;

    move-result-object v2

    .line 158
    invoke-virtual {v2}, Lcpa;->d()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 159
    const-string/jumbo v3, "collapse_key"

    invoke-virtual {v2, v3, v1}, Lcpa;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    :cond_1
    new-instance v2, Lcom/twitter/library/platform/notifications/s;

    invoke-direct {v2, v1, v0}, Lcom/twitter/library/platform/notifications/s;-><init>(Ljava/lang/String;I)V

    return-object v2

    .line 154
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/library/platform/notifications/e;->a()Ljava/lang/String;

    move-result-object v1

    .line 155
    invoke-static {v1}, Lcom/twitter/library/platform/notifications/q;->a(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/e;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "uri_new"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public n()Z
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/e;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "should_show_negative_feedback"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public o()I
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/e;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "badge_count"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/twitter/util/y;->d(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method
