.class public Lcom/twitter/library/platform/notifications/h;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/platform/notifications/h$b;,
        Lcom/twitter/library/platform/notifications/h$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/library/platform/notifications/h;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/platform/notifications/h;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public final c:I

.field public final d:I

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:J

.field public final i:Ljava/lang/String;

.field public final j:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/twitter/library/platform/notifications/h$b;

    invoke-direct {v0}, Lcom/twitter/library/platform/notifications/h$b;-><init>()V

    sput-object v0, Lcom/twitter/library/platform/notifications/h;->a:Lcom/twitter/util/serialization/l;

    .line 21
    new-instance v0, Lcom/twitter/library/platform/notifications/h$b;

    invoke-direct {v0}, Lcom/twitter/library/platform/notifications/h$b;-><init>()V

    .line 22
    invoke-static {v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/platform/notifications/h;->b:Lcom/twitter/util/serialization/l;

    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/platform/notifications/h$a;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/h$a;->a(Lcom/twitter/library/platform/notifications/h$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/platform/notifications/h;->c:I

    .line 41
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/h$a;->b(Lcom/twitter/library/platform/notifications/h$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/platform/notifications/h;->d:I

    .line 42
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/h$a;->c(Lcom/twitter/library/platform/notifications/h$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/h;->e:Ljava/lang/String;

    .line 43
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/h$a;->d(Lcom/twitter/library/platform/notifications/h$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/h;->f:Ljava/lang/String;

    .line 44
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/h$a;->e(Lcom/twitter/library/platform/notifications/h$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/h;->g:Ljava/lang/String;

    .line 45
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/h$a;->f(Lcom/twitter/library/platform/notifications/h$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/platform/notifications/h;->h:J

    .line 46
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/h$a;->g(Lcom/twitter/library/platform/notifications/h$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/h;->i:Ljava/lang/String;

    .line 47
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/h$a;->h(Lcom/twitter/library/platform/notifications/h$a;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/h;->j:[B

    .line 48
    return-void
.end method
