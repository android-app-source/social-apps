.class public final Lcom/twitter/library/platform/notifications/r;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/platform/notifications/r$b;,
        Lcom/twitter/library/platform/notifications/r$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/library/platform/notifications/r;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:Ljava/lang/String;

.field public final f:Z

.field public final g:Ljava/lang/String;

.field public final h:I

.field public final i:I

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;

.field public final l:I

.field public final m:Lcfu;

.field public final n:Lcfw;

.field public o:I

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:I

.field public u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/platform/notifications/h;",
            ">;"
        }
    .end annotation
.end field

.field public v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcfr;",
            ">;"
        }
    .end annotation
.end field

.field final w:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/twitter/library/platform/notifications/r$b;

    invoke-direct {v0}, Lcom/twitter/library/platform/notifications/r$b;-><init>()V

    sput-object v0, Lcom/twitter/library/platform/notifications/r;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/library/platform/notifications/r$a;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/r;->u:Ljava/util/List;

    .line 66
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->a(Lcom/twitter/library/platform/notifications/r$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/platform/notifications/r;->h:I

    .line 67
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->b(Lcom/twitter/library/platform/notifications/r$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/platform/notifications/r;->i:I

    .line 68
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->c(Lcom/twitter/library/platform/notifications/r$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/platform/notifications/r;->o:I

    .line 69
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->d(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/r;->b:Ljava/lang/String;

    .line 70
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->e(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/r;->p:Ljava/lang/String;

    .line 71
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->f(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/r;->j:Ljava/lang/String;

    .line 72
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->g(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/r;->q:Ljava/lang/String;

    .line 73
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->h(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/r;->k:Ljava/lang/String;

    .line 74
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->i(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/r;->r:Ljava/lang/String;

    .line 75
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->j(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/r;->s:Ljava/lang/String;

    .line 76
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->k(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/r;->c:Ljava/lang/String;

    .line 77
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->l(Lcom/twitter/library/platform/notifications/r$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/platform/notifications/r;->t:I

    .line 78
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->m(Lcom/twitter/library/platform/notifications/r$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/platform/notifications/r;->d:I

    .line 79
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->n(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/r;->e:Ljava/lang/String;

    .line 80
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->o(Lcom/twitter/library/platform/notifications/r$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/platform/notifications/r;->f:Z

    .line 81
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->p(Lcom/twitter/library/platform/notifications/r$a;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/r;->u:Ljava/util/List;

    .line 82
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->q(Lcom/twitter/library/platform/notifications/r$a;)Lcfu;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/r;->m:Lcfu;

    .line 83
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->r(Lcom/twitter/library/platform/notifications/r$a;)Lcfw;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    .line 84
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->s(Lcom/twitter/library/platform/notifications/r$a;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/r;->v:Ljava/util/List;

    .line 85
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->t(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/r;->w:Ljava/lang/String;

    .line 86
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->u(Lcom/twitter/library/platform/notifications/r$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/r;->g:Ljava/lang/String;

    .line 87
    invoke-static {p1}, Lcom/twitter/library/platform/notifications/r$a;->v(Lcom/twitter/library/platform/notifications/r$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/platform/notifications/r;->l:I

    .line 88
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/library/platform/notifications/r$a;Lcom/twitter/library/platform/notifications/r$1;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/twitter/library/platform/notifications/r;-><init>(Lcom/twitter/library/platform/notifications/r$a;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r;->w:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    invoke-virtual {p0}, Lcom/twitter/library/platform/notifications/r;->b()Ljava/lang/String;

    move-result-object v0

    .line 95
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r;->w:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r;->m:Lcfu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r;->m:Lcfu;

    iget-object p1, v0, Lcfu;->e:Ljava/lang/String;

    :cond_0
    return-object p1
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/library/platform/notifications/r;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v0, v0, Lcfw;->c:Lcfv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v0, v0, Lcfw;->c:Lcfv;

    iget-object v0, v0, Lcfv;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v0, v0, Lcfw;->c:Lcfv;

    iget-object v0, v0, Lcfv;->d:Ljava/lang/String;

    .line 109
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/platform/notifications/r;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r;->m:Lcfu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r;->m:Lcfu;

    iget-wide v0, v0, Lcfu;->b:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v0, v0, Lcfw;->c:Lcfv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v0, v0, Lcfw;->c:Lcfv;

    iget-wide v0, v0, Lcfv;->b:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r;->m:Lcfu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r;->m:Lcfu;

    iget-boolean v0, v0, Lcfu;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v0, v0, Lcfw;->c:Lcfv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/r;->n:Lcfw;

    iget-object v0, v0, Lcfw;->c:Lcfv;

    iget-object v0, v0, Lcfv;->c:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method
