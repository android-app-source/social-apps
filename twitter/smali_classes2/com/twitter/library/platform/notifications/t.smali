.class public Lcom/twitter/library/platform/notifications/t;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/util/a;


# direct methods
.method private constructor <init>(Lcom/twitter/util/a;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/t;->a:Lcom/twitter/util/a;

    .line 31
    return-void
.end method

.method public static a(Landroid/content/Context;J)Lcom/twitter/library/platform/notifications/t;
    .locals 5

    .prologue
    .line 69
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 70
    invoke-static {v0}, Lcom/twitter/library/platform/notifications/t;->a(Landroid/content/Context;)V

    .line 71
    new-instance v1, Lcom/twitter/library/platform/notifications/t;

    new-instance v2, Lcom/twitter/util/a;

    const-string/jumbo v3, "c2dm"

    invoke-direct {v2, v0, p1, p2, v3}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/twitter/library/platform/notifications/t;-><init>(Lcom/twitter/util/a;)V

    return-object v1
.end method

.method private static a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 75
    const-string/jumbo v0, "c2dm"

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 76
    const-string/jumbo v1, "ver"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 77
    const-string/jumbo v1, "ver"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 78
    if-ge v1, v3, :cond_0

    .line 79
    invoke-static {v0, v1}, Lcom/twitter/library/platform/notifications/t;->a(Landroid/content/SharedPreferences;I)V

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    invoke-static {v0, v2}, Lcom/twitter/library/platform/notifications/t;->a(Landroid/content/SharedPreferences;I)V

    .line 89
    invoke-static {p0, v0}, Lcom/twitter/library/platform/notifications/t;->a(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 8

    .prologue
    .line 119
    invoke-interface {p1}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 138
    :cond_0
    return-void

    .line 122
    :cond_1
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v0

    invoke-virtual {v0}, Lakn;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    .line 123
    new-instance v2, Lcom/twitter/util/a;

    invoke-virtual {v0}, Lakm;->b()Lcnz;

    move-result-object v3

    invoke-virtual {v3}, Lcnz;->b()J

    move-result-wide v4

    const-string/jumbo v3, "c2dm"

    invoke-direct {v2, p0, v4, v5, v3}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    .line 124
    invoke-virtual {v2}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v2

    .line 128
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "last_refresh."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lakm;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 129
    invoke-interface {p1, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 130
    const-string/jumbo v4, "last_refresh."

    const-wide/16 v6, 0x0

    invoke-interface {p1, v3, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-virtual {v2, v4, v6, v7}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;J)Lcom/twitter/util/a$a;

    .line 132
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "reg_enabled_for."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lakm;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 133
    invoke-interface {p1, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 134
    const-string/jumbo v3, "reg_enabled_for."

    const/4 v4, 0x0

    invoke-interface {p1, v0, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v2, v3, v0}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;I)Lcom/twitter/util/a$a;

    .line 136
    :cond_3
    invoke-virtual {v2}, Lcom/twitter/util/a$a;->apply()V

    goto :goto_0
.end method

.method private static a(Landroid/content/SharedPreferences;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 97
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 99
    if-nez p1, :cond_1

    move v0, v1

    .line 102
    :goto_0
    if-ne v0, v1, :cond_0

    .line 106
    const-string/jumbo v0, "reg_id"

    invoke-interface {v2, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "backoff"

    .line 107
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "backoff_ceil"

    .line 108
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "last_refresh."

    .line 109
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 110
    const/4 v0, 0x2

    .line 112
    :cond_0
    const-string/jumbo v1, "ver"

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 113
    return-void

    :cond_1
    move v0, p1

    goto :goto_0
.end method


# virtual methods
.method public a(J)V
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/t;->a:Lcom/twitter/util/a;

    invoke-virtual {v0}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "last_refresh."

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;J)Lcom/twitter/util/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 47
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/t;->a:Lcom/twitter/util/a;

    invoke-virtual {v0}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "enabled"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Z)Lcom/twitter/util/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 39
    return-void
.end method

.method public a()Z
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/t;->a:Lcom/twitter/util/a;

    const-string/jumbo v1, "enabled"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/a;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public b()J
    .locals 4

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/t;->a:Lcom/twitter/util/a;

    const-string/jumbo v1, "last_refresh."

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/util/a;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method
