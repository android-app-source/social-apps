.class public final Lcom/twitter/library/platform/notifications/h$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/platform/notifications/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/library/platform/notifications/h;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:J

.field private g:Ljava/lang/String;

.field private h:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/platform/notifications/h$a;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/twitter/library/platform/notifications/h$a;->a:I

    return v0
.end method

.method static synthetic b(Lcom/twitter/library/platform/notifications/h$a;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/twitter/library/platform/notifications/h$a;->b:I

    return v0
.end method

.method static synthetic c(Lcom/twitter/library/platform/notifications/h$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/h$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/library/platform/notifications/h$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/h$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/library/platform/notifications/h$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/h$a;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/library/platform/notifications/h$a;)J
    .locals 2

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/twitter/library/platform/notifications/h$a;->f:J

    return-wide v0
.end method

.method static synthetic g(Lcom/twitter/library/platform/notifications/h$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/h$a;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/library/platform/notifications/h$a;)[B
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/library/platform/notifications/h$a;->h:[B

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/twitter/library/platform/notifications/h$a;
    .locals 0

    .prologue
    .line 63
    iput p1, p0, Lcom/twitter/library/platform/notifications/h$a;->a:I

    .line 64
    return-object p0
.end method

.method public a(J)Lcom/twitter/library/platform/notifications/h$a;
    .locals 1

    .prologue
    .line 93
    iput-wide p1, p0, Lcom/twitter/library/platform/notifications/h$a;->f:J

    .line 94
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/h$a;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/h$a;->c:Ljava/lang/String;

    .line 76
    return-object p0
.end method

.method public a([B)Lcom/twitter/library/platform/notifications/h$a;
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/h$a;->h:[B

    .line 106
    return-object p0
.end method

.method public b(I)Lcom/twitter/library/platform/notifications/h$a;
    .locals 0

    .prologue
    .line 69
    iput p1, p0, Lcom/twitter/library/platform/notifications/h$a;->b:I

    .line 70
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/h$a;
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/h$a;->d:Ljava/lang/String;

    .line 82
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/twitter/library/platform/notifications/h$a;->e()Lcom/twitter/library/platform/notifications/h;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/h$a;
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/h$a;->e:Ljava/lang/String;

    .line 88
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/h$a;
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/twitter/library/platform/notifications/h$a;->g:Ljava/lang/String;

    .line 100
    return-object p0
.end method

.method protected e()Lcom/twitter/library/platform/notifications/h;
    .locals 1

    .prologue
    .line 112
    new-instance v0, Lcom/twitter/library/platform/notifications/h;

    invoke-direct {v0, p0}, Lcom/twitter/library/platform/notifications/h;-><init>(Lcom/twitter/library/platform/notifications/h$a;)V

    return-object v0
.end method
