.class public Lcom/twitter/library/platform/notifications/o;
.super Lcom/twitter/library/platform/notifications/v;
.source "Twttr"


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/twitter/library/platform/notifications/v;-><init>()V

    .line 32
    iput p1, p0, Lcom/twitter/library/platform/notifications/o;->a:I

    .line 33
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/platform/notifications/u;Landroid/os/Bundle;JLjava/lang/String;Ljava/lang/String;Landroid/content/Context;Lcom/twitter/library/provider/j;Lcom/twitter/library/provider/t;Laut;)V
    .locals 15

    .prologue
    .line 40
    const-string/jumbo v2, "notification_dm_data"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 41
    const-string/jumbo v2, "notification_dm_data"

    sget-object v3, Lcom/twitter/model/dms/s;->i:Lcom/twitter/util/serialization/l;

    move-object/from16 v0, p2

    invoke-static {v0, v2, v3}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v2

    move-object v13, v2

    check-cast v13, Lcom/twitter/model/dms/e;

    .line 43
    iget-object v4, v13, Lcom/twitter/model/dms/e;->f:Ljava/lang/String;

    .line 46
    const-string/jumbo v2, "recipient_id"

    const-wide/16 v6, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 47
    const/16 v5, 0x16

    iget v6, p0, Lcom/twitter/library/platform/notifications/o;->a:I

    if-ne v5, v6, :cond_2

    .line 48
    new-instance v5, Lcom/twitter/model/dms/Participant$a;

    invoke-direct {v5}, Lcom/twitter/model/dms/Participant$a;-><init>()V

    .line 50
    invoke-virtual {v13}, Lcom/twitter/model/dms/e;->l()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/twitter/model/dms/Participant$a;->a(J)Lcom/twitter/model/dms/Participant$a;

    move-result-object v5

    .line 51
    invoke-virtual {v5, v4}, Lcom/twitter/model/dms/Participant$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/Participant$a;

    move-result-object v5

    .line 52
    invoke-virtual {v5}, Lcom/twitter/model/dms/Participant$a;->q()Ljava/lang/Object;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Lcom/twitter/model/dms/Participant;

    const/4 v7, 0x0

    new-instance v8, Lcom/twitter/model/dms/Participant$a;

    invoke-direct {v8}, Lcom/twitter/model/dms/Participant$a;-><init>()V

    .line 54
    invoke-virtual {v8, v2, v3}, Lcom/twitter/model/dms/Participant$a;->a(J)Lcom/twitter/model/dms/Participant$a;

    move-result-object v2

    .line 55
    invoke-virtual {v2, v4}, Lcom/twitter/model/dms/Participant$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/Participant$a;

    move-result-object v2

    .line 56
    invoke-virtual {v2}, Lcom/twitter/model/dms/Participant$a;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/dms/Participant;

    aput-object v2, v6, v7

    .line 48
    invoke-static {v5, v6}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v3

    .line 58
    const/4 v2, 0x0

    .line 63
    :goto_0
    new-instance v5, Lcom/twitter/model/dms/l$a;

    invoke-direct {v5}, Lcom/twitter/model/dms/l$a;-><init>()V

    .line 64
    invoke-virtual {v5, v2}, Lcom/twitter/model/dms/l$a;->a(I)Lcom/twitter/model/dms/l$a;

    move-result-object v2

    .line 65
    invoke-virtual {v2, v3}, Lcom/twitter/model/dms/l$a;->a(Ljava/util/Collection;)Lcom/twitter/model/dms/l$a;

    move-result-object v2

    .line 66
    invoke-virtual {v2, v4}, Lcom/twitter/model/dms/l$a;->c(Ljava/lang/String;)Lcom/twitter/model/dms/l$a;

    move-result-object v2

    iget-wide v4, v13, Lcom/twitter/model/dms/e;->e:J

    .line 67
    invoke-virtual {v2, v4, v5}, Lcom/twitter/model/dms/l$a;->b(J)Lcom/twitter/model/dms/l$a;

    move-result-object v2

    iget-wide v4, v13, Lcom/twitter/model/dms/e;->g:J

    .line 68
    invoke-virtual {v2, v4, v5}, Lcom/twitter/model/dms/l$a;->c(J)Lcom/twitter/model/dms/l$a;

    move-result-object v2

    .line 69
    invoke-virtual {v2}, Lcom/twitter/model/dms/l$a;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/dms/l;

    .line 70
    const/4 v3, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/model/dms/l;Z)V

    .line 71
    const/4 v2, 0x0

    move-object/from16 v0, p9

    move-object/from16 v1, p10

    invoke-virtual {v0, v13, v2, v1}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/model/dms/c;ZLaut;)V

    .line 73
    invoke-virtual {v13}, Lcom/twitter/model/dms/e;->z()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 74
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    invoke-virtual {v13}, Lcom/twitter/model/dms/e;->u()Lcbx;

    move-result-object v2

    check-cast v2, Lccf;

    invoke-virtual {v2}, Lccf;->j()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/platform/notifications/r;->s:Ljava/lang/String;

    .line 77
    :cond_0
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    const-string/jumbo v3, "title"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/twitter/library/platform/notifications/r;->p:Ljava/lang/String;

    .line 79
    const-string/jumbo v2, "text"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 80
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    const/16 v5, 0xd

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    .line 81
    invoke-virtual {v2}, Lcom/twitter/library/platform/notifications/r;->a()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    .line 82
    invoke-virtual {v2}, Lcom/twitter/library/platform/notifications/r;->e()J

    move-result-wide v8

    iget-object v11, v13, Lcom/twitter/model/dms/e;->f:Ljava/lang/String;

    const/4 v12, 0x0

    move-object/from16 v2, p9

    move-wide/from16 v3, p3

    move-object/from16 v6, p6

    .line 80
    invoke-virtual/range {v2 .. v12}, Lcom/twitter/library/provider/t;->a(JILjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;[B)I

    move-result v2

    iput v2, v14, Lcom/twitter/library/platform/notifications/r;->t:I

    .line 83
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    iget-object v3, v13, Lcom/twitter/model/dms/e;->f:Ljava/lang/String;

    move-object/from16 v0, p9

    invoke-virtual {v0, v3}, Lcom/twitter/library/provider/t;->f(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v2, Lcom/twitter/library/platform/notifications/r;->u:Ljava/util/List;

    .line 84
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    iget-object v3, v13, Lcom/twitter/model/dms/e;->f:Ljava/lang/String;

    iput-object v3, v2, Lcom/twitter/library/platform/notifications/r;->q:Ljava/lang/String;

    .line 85
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    iget-object v3, v3, Lcom/twitter/library/platform/notifications/r;->u:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    iput v3, v2, Lcom/twitter/library/platform/notifications/r;->o:I

    .line 87
    :cond_1
    return-void

    .line 60
    :cond_2
    const/4 v3, 0x0

    .line 61
    const/4 v2, 0x1

    goto/16 :goto_0
.end method
