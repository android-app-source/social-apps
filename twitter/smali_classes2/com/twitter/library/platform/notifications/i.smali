.class public Lcom/twitter/library/platform/notifications/i;
.super Lcom/twitter/library/platform/notifications/v;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/twitter/library/platform/notifications/v;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/platform/notifications/u;Landroid/os/Bundle;JLjava/lang/String;Ljava/lang/String;Landroid/content/Context;Lcom/twitter/library/provider/j;Lcom/twitter/library/provider/t;Laut;)V
    .locals 15

    .prologue
    .line 30
    const-string/jumbo v2, "notification_dm_data"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 31
    const-string/jumbo v2, "recipient_id"

    const-wide/16 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 32
    const-string/jumbo v2, "notification_dm_data"

    sget-object v3, Lcom/twitter/model/dms/z;->a:Lcom/twitter/util/serialization/l;

    .line 33
    move-object/from16 v0, p2

    invoke-static {v0, v2, v3}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v2

    .line 32
    invoke-static {v2}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v13, v2

    check-cast v13, Lcom/twitter/model/dms/z;

    .line 34
    iget-object v3, v13, Lcom/twitter/model/dms/z;->f:Ljava/lang/String;

    .line 35
    new-instance v2, Lcom/twitter/model/dms/Participant$a;

    invoke-direct {v2}, Lcom/twitter/model/dms/Participant$a;-><init>()V

    .line 37
    invoke-virtual {v13}, Lcom/twitter/model/dms/z;->l()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Lcom/twitter/model/dms/Participant$a;->a(J)Lcom/twitter/model/dms/Participant$a;

    move-result-object v2

    .line 38
    invoke-virtual {v2, v3}, Lcom/twitter/model/dms/Participant$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/Participant$a;

    move-result-object v2

    .line 39
    invoke-virtual {v2}, Lcom/twitter/model/dms/Participant$a;->q()Ljava/lang/Object;

    move-result-object v6

    const/4 v2, 0x1

    new-array v7, v2, [Lcom/twitter/model/dms/Participant;

    const/4 v8, 0x0

    new-instance v2, Lcom/twitter/model/dms/Participant$a;

    invoke-direct {v2}, Lcom/twitter/model/dms/Participant$a;-><init>()V

    .line 41
    invoke-virtual {v2, v4, v5}, Lcom/twitter/model/dms/Participant$a;->a(J)Lcom/twitter/model/dms/Participant$a;

    move-result-object v2

    .line 42
    invoke-virtual {v2, v3}, Lcom/twitter/model/dms/Participant$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/Participant$a;

    move-result-object v2

    .line 43
    invoke-virtual {v2}, Lcom/twitter/model/dms/Participant$a;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/dms/Participant;

    aput-object v2, v7, v8

    .line 35
    invoke-static {v6, v7}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    .line 45
    new-instance v4, Lcom/twitter/model/dms/l$a;

    invoke-direct {v4}, Lcom/twitter/model/dms/l$a;-><init>()V

    const/4 v5, 0x1

    .line 46
    invoke-virtual {v4, v5}, Lcom/twitter/model/dms/l$a;->a(I)Lcom/twitter/model/dms/l$a;

    move-result-object v4

    .line 47
    invoke-virtual {v4, v2}, Lcom/twitter/model/dms/l$a;->a(Ljava/util/Collection;)Lcom/twitter/model/dms/l$a;

    move-result-object v2

    .line 48
    invoke-virtual {v2, v3}, Lcom/twitter/model/dms/l$a;->c(Ljava/lang/String;)Lcom/twitter/model/dms/l$a;

    move-result-object v2

    iget-wide v4, v13, Lcom/twitter/model/dms/z;->e:J

    .line 49
    invoke-virtual {v2, v4, v5}, Lcom/twitter/model/dms/l$a;->b(J)Lcom/twitter/model/dms/l$a;

    move-result-object v2

    iget-wide v4, v13, Lcom/twitter/model/dms/z;->g:J

    .line 50
    invoke-virtual {v2, v4, v5}, Lcom/twitter/model/dms/l$a;->c(J)Lcom/twitter/model/dms/l$a;

    move-result-object v2

    .line 51
    invoke-virtual {v2}, Lcom/twitter/model/dms/l$a;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/dms/l;

    .line 52
    const/4 v3, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/model/dms/l;Z)V

    .line 53
    const/4 v2, 0x0

    move-object/from16 v0, p9

    move-object/from16 v1, p10

    invoke-virtual {v0, v13, v2, v1}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/model/dms/c;ZLaut;)V

    .line 54
    const-string/jumbo v2, "text"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 55
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    const/16 v5, 0xd

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    .line 56
    invoke-virtual {v2}, Lcom/twitter/library/platform/notifications/r;->a()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    .line 57
    invoke-virtual {v2}, Lcom/twitter/library/platform/notifications/r;->e()J

    move-result-wide v8

    iget-object v11, v13, Lcom/twitter/model/dms/z;->f:Ljava/lang/String;

    const/4 v12, 0x0

    move-object/from16 v2, p9

    move-wide/from16 v3, p3

    move-object/from16 v6, p6

    .line 55
    invoke-virtual/range {v2 .. v12}, Lcom/twitter/library/provider/t;->a(JILjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;[B)I

    move-result v2

    iput v2, v14, Lcom/twitter/library/platform/notifications/r;->t:I

    .line 58
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    iget-object v3, v13, Lcom/twitter/model/dms/z;->f:Ljava/lang/String;

    move-object/from16 v0, p9

    invoke-virtual {v0, v3}, Lcom/twitter/library/provider/t;->f(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v2, Lcom/twitter/library/platform/notifications/r;->u:Ljava/util/List;

    .line 59
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    iget-object v3, v13, Lcom/twitter/model/dms/z;->f:Ljava/lang/String;

    iput-object v3, v2, Lcom/twitter/library/platform/notifications/r;->q:Ljava/lang/String;

    .line 61
    :cond_0
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    iget-object v3, v3, Lcom/twitter/library/platform/notifications/r;->u:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    iput v3, v2, Lcom/twitter/library/platform/notifications/r;->o:I

    .line 62
    return-void
.end method
