.class public final Lcom/twitter/library/platform/notifications/h$b;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/platform/notifications/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/library/platform/notifications/h;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/library/platform/notifications/h;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 133
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v0

    .line 134
    new-instance v1, Lcom/twitter/library/platform/notifications/h$a;

    invoke-direct {v1}, Lcom/twitter/library/platform/notifications/h$a;-><init>()V

    .line 135
    invoke-virtual {v1, v0}, Lcom/twitter/library/platform/notifications/h$a;->a(I)Lcom/twitter/library/platform/notifications/h$a;

    move-result-object v0

    .line 136
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/h$a;->b(I)Lcom/twitter/library/platform/notifications/h$a;

    move-result-object v0

    .line 137
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/h$a;->a(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/h$a;

    move-result-object v0

    .line 138
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/h$a;->b(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/h$a;

    move-result-object v0

    .line 139
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/h$a;->c(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/h$a;

    move-result-object v0

    .line 140
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/platform/notifications/h$a;->a(J)Lcom/twitter/library/platform/notifications/h$a;

    move-result-object v0

    .line 141
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/h$a;->d(Ljava/lang/String;)Lcom/twitter/library/platform/notifications/h$a;

    move-result-object v0

    .line 142
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->j()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/notifications/h$a;->a([B)Lcom/twitter/library/platform/notifications/h$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/h$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/platform/notifications/h;

    .line 134
    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/platform/notifications/h;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    iget v0, p2, Lcom/twitter/library/platform/notifications/h;->c:I

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/library/platform/notifications/h;->d:I

    .line 120
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/h;->e:Ljava/lang/String;

    .line 121
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/h;->f:Ljava/lang/String;

    .line 122
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/h;->g:Ljava/lang/String;

    .line 123
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/library/platform/notifications/h;->h:J

    .line 124
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/h;->i:Ljava/lang/String;

    .line 125
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/h;->j:[B

    .line 126
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b([B)Lcom/twitter/util/serialization/o;

    .line 127
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 116
    check-cast p2, Lcom/twitter/library/platform/notifications/h;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/platform/notifications/h$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/platform/notifications/h;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 116
    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/platform/notifications/h$b;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/library/platform/notifications/h;

    move-result-object v0

    return-object v0
.end method
