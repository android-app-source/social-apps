.class public Lcom/twitter/library/platform/notifications/NotificationParser;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/platform/notifications/NotificationParser$NotificationParseException;
    }
.end annotation


# direct methods
.method public static a(Ljava/lang/String;)Lcom/twitter/model/dms/p;
    .locals 2

    .prologue
    .line 81
    :try_start_0
    sget-object v0, Lcom/twitter/model/json/common/e;->a:Lcom/fasterxml/jackson/core/JsonFactory;

    invoke-virtual {v0, p0}, Lcom/fasterxml/jackson/core/JsonFactory;->a(Ljava/lang/String;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    .line 83
    new-instance v1, Lcom/twitter/model/json/dms/d;

    invoke-direct {v1}, Lcom/twitter/model/json/dms/d;-><init>()V

    invoke-virtual {v1, v0}, Lcom/twitter/model/json/dms/d;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/dms/k;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 86
    :goto_0
    return-object v0

    .line 84
    :catch_0
    move-exception v0

    .line 85
    new-instance v0, Lcom/twitter/library/platform/notifications/NotificationParser$NotificationParseException;

    const-string/jumbo v1, "Invalid event data"

    invoke-direct {v0, v1}, Lcom/twitter/library/platform/notifications/NotificationParser$NotificationParseException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 86
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Z)Lcom/twitter/model/dms/s;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 21
    if-nez p0, :cond_0

    move-object v0, v1

    .line 74
    :goto_0
    return-object v0

    .line 26
    :cond_0
    :try_start_0
    sget-object v0, Lcom/twitter/model/json/common/e;->a:Lcom/fasterxml/jackson/core/JsonFactory;

    invoke-virtual {v0, p0}, Lcom/fasterxml/jackson/core/JsonFactory;->a(Ljava/lang/String;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v2

    .line 27
    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 29
    :goto_1
    if-eqz v0, :cond_4

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v3, :cond_4

    .line 30
    sget-object v3, Lcom/twitter/library/platform/notifications/NotificationParser$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    .line 68
    :cond_1
    :goto_2
    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_1

    .line 32
    :pswitch_0
    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    .line 33
    :goto_3
    if-eqz v0, :cond_1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v3, :cond_1

    .line 34
    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonParser;->e()Ljava/lang/String;

    move-result-object v3

    .line 35
    sget-object v4, Lcom/twitter/library/platform/notifications/NotificationParser$1;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_1

    .line 56
    :goto_4
    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_3

    .line 37
    :pswitch_1
    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_4

    .line 73
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 74
    goto :goto_0

    .line 41
    :pswitch_2
    const-string/jumbo v0, "message"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 42
    if-eqz p1, :cond_2

    .line 43
    new-instance v0, Lcom/twitter/model/json/dms/o;

    invoke-direct {v0}, Lcom/twitter/model/json/dms/o;-><init>()V

    invoke-virtual {v0, v2}, Lcom/twitter/model/json/dms/o;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/model/dms/a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/s;

    goto :goto_0

    .line 45
    :cond_2
    const-class v0, Lcom/twitter/model/dms/s;

    invoke-static {v2, v0}, Lcom/twitter/model/json/common/e;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/s;

    goto :goto_0

    .line 48
    :cond_3
    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_4

    .line 61
    :pswitch_3
    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    .line 71
    :cond_4
    new-instance v0, Lcom/twitter/library/platform/notifications/NotificationParser$NotificationParseException;

    const-string/jumbo v2, "Invalid event data"

    invoke-direct {v0, v2}, Lcom/twitter/library/platform/notifications/NotificationParser$NotificationParseException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 72
    goto :goto_0

    .line 30
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
    .end packed-switch

    .line 35
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
