.class public final Lcom/twitter/library/platform/notifications/u$a;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/library/platform/notifications/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/library/platform/notifications/u;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/util/serialization/i;-><init>(I)V

    .line 36
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/library/platform/notifications/u;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->p()Ljava/lang/String;

    move-result-object v1

    .line 54
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v2

    .line 55
    const/4 v0, 0x1

    if-ge p2, v0, :cond_0

    .line 56
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->d()Z

    .line 58
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v4

    .line 59
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->e()I

    move-result v5

    .line 60
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v6

    .line 61
    sget-object v0, Lcom/twitter/library/platform/notifications/r;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/platform/notifications/r;

    .line 63
    new-instance v7, Lcom/twitter/library/platform/notifications/u;

    invoke-direct {v7, v1, v2, v3, v6}, Lcom/twitter/library/platform/notifications/u;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    .line 64
    iput v4, v7, Lcom/twitter/library/platform/notifications/u;->e:I

    .line 65
    iput v5, v7, Lcom/twitter/library/platform/notifications/u;->f:I

    .line 66
    iput-object v0, v7, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    .line 67
    return-object v7
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/platform/notifications/u;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p2, Lcom/twitter/library/platform/notifications/u;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-wide v2, p2, Lcom/twitter/library/platform/notifications/u;->c:J

    .line 42
    invoke-virtual {v0, v2, v3}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/library/platform/notifications/u;->e:I

    .line 43
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget v1, p2, Lcom/twitter/library/platform/notifications/u;->f:I

    .line 44
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->e(I)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/u;->d:Ljava/lang/String;

    .line 45
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    sget-object v2, Lcom/twitter/library/platform/notifications/r;->a:Lcom/twitter/util/serialization/l;

    .line 46
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 47
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    check-cast p2, Lcom/twitter/library/platform/notifications/u;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/platform/notifications/u$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/library/platform/notifications/u;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 31
    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/platform/notifications/u$a;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/library/platform/notifications/u;

    move-result-object v0

    return-object v0
.end method
