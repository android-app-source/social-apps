.class public abstract Lcom/twitter/library/platform/notifications/PushRegistration;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/library/platform/notifications/PushRegistration$b;,
        Lcom/twitter/library/platform/notifications/PushRegistration$DebugNotificationException;,
        Lcom/twitter/library/platform/notifications/PushRegistration$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field private static final c:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static final d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static final e:Landroid/os/Handler;

.field private static f:Z

.field private static g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".c2dm.registered"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/platform/notifications/PushRegistration;->a:Ljava/lang/String;

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".c2dm.error"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/platform/notifications/PushRegistration;->b:Ljava/lang/String;

    .line 71
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/twitter/library/platform/notifications/PushRegistration;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 76
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/twitter/library/platform/notifications/PushRegistration;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 77
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/twitter/library/platform/notifications/PushRegistration;->e:Landroid/os/Handler;

    .line 82
    sput-boolean v2, Lcom/twitter/library/platform/notifications/PushRegistration;->f:Z

    .line 83
    const-string/jumbo v0, ""

    sput-object v0, Lcom/twitter/library/platform/notifications/PushRegistration;->g:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395
    return-void
.end method

.method public static a(J)I
    .locals 2

    .prologue
    .line 105
    invoke-static {}, Lcom/twitter/library/provider/j;->c()Lcom/twitter/library/provider/j;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/twitter/library/provider/j;->a(J)I

    move-result v0

    return v0
.end method

.method public static a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 90
    invoke-static {}, Lcom/twitter/util/z;->a()Ljava/lang/String;

    move-result-object v0

    .line 91
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 92
    invoke-static {}, Lcom/google/android/gcm/GCMScribeReporter;->c()V

    .line 94
    :cond_0
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 97
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 99
    :cond_1
    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 232
    sget-object v0, Lcom/twitter/library/platform/notifications/PushRegistration;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v3, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "49625052041"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gcm/b;->a(Landroid/content/Context;[Ljava/lang/String;)V

    .line 235
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;JI)V
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, p3, v0}, Lcom/twitter/library/platform/notifications/PushRegistration;->b(Landroid/content/Context;JIZ)V

    .line 200
    return-void
.end method

.method public static a(Landroid/content/Context;JIZ)V
    .locals 7

    .prologue
    .line 118
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 119
    invoke-static {v1, p1, p2}, Lcom/twitter/library/platform/notifications/t;->a(Landroid/content/Context;J)Lcom/twitter/library/platform/notifications/t;

    move-result-object v2

    .line 120
    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/twitter/library/platform/notifications/t;->a(Z)V

    .line 121
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 122
    new-instance v0, Lbei;

    .line 123
    invoke-static {}, Lcom/twitter/library/platform/notifications/PushRegistration;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, Lcom/google/android/gcm/b;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lbei;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V

    .line 124
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/platform/notifications/PushRegistration$1;

    invoke-direct {v3, p4, v1}, Lcom/twitter/library/platform/notifications/PushRegistration$1;-><init>(ZLandroid/content/Context;)V

    invoke-virtual {v2, v0, v3}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 134
    return-void

    .line 120
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 15

    .prologue
    .line 146
    sget-object v0, Lcom/twitter/library/platform/notifications/PushRegistration;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    :goto_0
    return-void

    .line 149
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 150
    const-string/jumbo v0, "android_push_settings_check_in_success_interval_hours"

    const-wide/16 v2, 0x18

    invoke-static {v0, v2, v3}, Lcoj;->a(Ljava/lang/String;J)J

    move-result-wide v2

    const-wide/32 v4, 0x36ee80

    mul-long v6, v2, v4

    .line 152
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v8

    .line 153
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 154
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v10

    .line 155
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v0

    invoke-virtual {v0}, Lakn;->c()Ljava/util/List;

    move-result-object v12

    .line 156
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    .line 159
    invoke-virtual {v0}, Lakm;->b()Lcnz;

    move-result-object v2

    invoke-virtual {v2}, Lcnz;->b()J

    move-result-wide v4

    .line 160
    invoke-virtual {v0}, Lakm;->d()Ljava/lang/String;

    move-result-object v14

    .line 161
    invoke-static {v1, v4, v5}, Lcom/twitter/library/platform/notifications/t;->a(Landroid/content/Context;J)Lcom/twitter/library/platform/notifications/t;

    move-result-object v0

    .line 162
    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/t;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/t;->b()J

    move-result-wide v2

    add-long/2addr v2, v6

    cmp-long v0, v2, v10

    if-ltz v0, :cond_2

    .line 163
    :cond_1
    invoke-interface {v9, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 165
    :cond_2
    invoke-virtual {v8, v4, v5}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 166
    invoke-static {v4, v5}, Lcom/twitter/library/platform/notifications/PushRegistration;->a(J)I

    move-result v5

    .line 167
    new-instance v0, Lbeb;

    invoke-static {}, Lcom/twitter/library/platform/notifications/PushRegistration;->a()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v4, p1

    invoke-direct/range {v0 .. v5}, Lbeb;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;I)V

    new-instance v2, Lcom/twitter/library/platform/notifications/PushRegistration$2;

    invoke-direct {v2, v9, v14}, Lcom/twitter/library/platform/notifications/PushRegistration$2;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    .line 168
    invoke-virtual {v0, v2}, Lbeb;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/s;

    .line 177
    invoke-virtual {v0}, Lcom/twitter/library/service/s;->O()Lcom/twitter/library/service/u;

    goto :goto_1

    .line 180
    :cond_3
    invoke-interface {v9}, Ljava/util/Set;->size()I

    move-result v0

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v2

    if-ne v0, v2, :cond_4

    .line 181
    invoke-static {v1, v6, v7}, Lcom/google/android/gcm/b;->a(Landroid/content/Context;J)V

    .line 182
    const/4 v0, 0x1

    invoke-static {v1, v0}, Lcom/google/android/gcm/b;->a(Landroid/content/Context;Z)V

    .line 184
    :cond_4
    sget-object v0, Lcom/twitter/library/platform/notifications/PushRegistration;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto/16 :goto_0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 339
    const/4 v0, 0x1

    sput-boolean v0, Lcom/twitter/library/platform/notifications/PushRegistration;->f:Z

    .line 340
    sput-object p0, Lcom/twitter/library/platform/notifications/PushRegistration;->g:Ljava/lang/String;

    .line 341
    const-class v0, Lcom/twitter/library/platform/notifications/PushRegistration;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 342
    return-void
.end method

.method public static a(Landroid/content/Context;J)Z
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 194
    invoke-static {p0}, Lcom/google/android/gcm/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    cmp-long v0, p1, v2

    if-lez v0, :cond_1

    .line 195
    invoke-static {p0, p1, p2}, Lcom/twitter/library/platform/notifications/t;->a(Landroid/content/Context;J)Lcom/twitter/library/platform/notifications/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/t;->b()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 194
    :goto_0
    return v0

    .line 195
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()V
    .locals 1

    .prologue
    .line 345
    const/4 v0, 0x0

    sput-boolean v0, Lcom/twitter/library/platform/notifications/PushRegistration;->f:Z

    .line 346
    const-string/jumbo v0, ""

    sput-object v0, Lcom/twitter/library/platform/notifications/PushRegistration;->g:Ljava/lang/String;

    .line 347
    const-class v0, Lcom/twitter/library/platform/notifications/PushRegistration;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 348
    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 263
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gcm/b;->a(Landroid/content/Context;)V

    .line 264
    return-void
.end method

.method public static b(Landroid/content/Context;JIZ)V
    .locals 3

    .prologue
    .line 209
    new-instance v0, Lbbf;

    .line 210
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbbf;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v1, 0x2

    .line 211
    invoke-virtual {v0, v1}, Lbbf;->d(I)Lcom/twitter/library/service/j;

    move-result-object v0

    check-cast v0, Lbbf;

    .line 212
    iput p3, v0, Lbbf;->b:I

    .line 213
    iput-boolean p4, v0, Lbbf;->c:Z

    .line 214
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 215
    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 279
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 280
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v6

    .line 281
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 282
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v0

    invoke-virtual {v0}, Lakn;->c()Ljava/util/List;

    move-result-object v8

    .line 283
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakm;

    .line 284
    invoke-virtual {v0}, Lakm;->b()Lcnz;

    move-result-object v2

    invoke-virtual {v2}, Lcnz;->b()J

    move-result-wide v4

    .line 285
    invoke-virtual {v0}, Lakm;->d()Ljava/lang/String;

    move-result-object v10

    .line 286
    invoke-static {v1, v4, v5}, Lcom/twitter/library/platform/notifications/t;->a(Landroid/content/Context;J)Lcom/twitter/library/platform/notifications/t;

    move-result-object v0

    .line 287
    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/t;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288
    invoke-virtual {v6, v4, v5}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 289
    invoke-static {v4, v5}, Lcom/twitter/library/platform/notifications/PushRegistration;->a(J)I

    move-result v5

    .line 290
    new-instance v0, Lbeb;

    invoke-static {}, Lcom/twitter/library/platform/notifications/PushRegistration;->a()Ljava/lang/String;

    move-result-object v3

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lbeb;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;I)V

    new-instance v2, Lcom/twitter/library/platform/notifications/PushRegistration$3;

    invoke-direct {v2, v7, v10}, Lcom/twitter/library/platform/notifications/PushRegistration$3;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    .line 291
    invoke-virtual {v0, v2}, Lbeb;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/s;

    .line 300
    invoke-virtual {v0}, Lcom/twitter/library/service/s;->O()Lcom/twitter/library/service/u;

    goto :goto_0

    .line 302
    :cond_0
    invoke-interface {v7, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 305
    :cond_1
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v2

    if-ne v0, v2, :cond_2

    .line 306
    const-string/jumbo v0, "android_push_settings_check_in_success_interval_hours"

    const-wide/16 v2, 0x18

    invoke-static {v0, v2, v3}, Lcoj;->a(Ljava/lang/String;J)J

    move-result-wide v2

    const-wide/32 v4, 0x36ee80

    mul-long/2addr v2, v4

    .line 309
    invoke-static {p0, v2, v3}, Lcom/google/android/gcm/b;->a(Landroid/content/Context;J)V

    .line 310
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/gcm/b;->a(Landroid/content/Context;Z)V

    .line 312
    :cond_2
    sget-object v0, Lcom/twitter/library/platform/notifications/PushRegistration;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 313
    new-instance v0, Landroid/content/Intent;

    sget-object v2, Lcom/twitter/library/platform/notifications/PushRegistration;->a:Ljava/lang/String;

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/twitter/database/schema/a;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 314
    sget-boolean v0, Lcom/twitter/library/platform/notifications/PushRegistration;->f:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/twitter/library/platform/notifications/PushRegistration;->g:Ljava/lang/String;

    invoke-interface {v7, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 315
    sget-object v0, Lcom/twitter/library/platform/notifications/PushRegistration;->e:Landroid/os/Handler;

    new-instance v2, Lcom/twitter/library/platform/notifications/PushRegistration$b;

    sget v3, Lazw$k;->preference_notification_success:I

    invoke-direct {v2, v1, v3}, Lcom/twitter/library/platform/notifications/PushRegistration$b;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 316
    invoke-static {}, Lcom/twitter/library/platform/notifications/PushRegistration;->b()V

    .line 318
    :cond_3
    return-void
.end method

.method public static b(Landroid/content/Context;J)Z
    .locals 1

    .prologue
    .line 222
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 223
    invoke-static {v0}, Lcom/google/android/gcm/b;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    invoke-static {p0, p1, p2}, Lcom/twitter/library/platform/notifications/t;->a(Landroid/content/Context;J)Lcom/twitter/library/platform/notifications/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/t;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 223
    :goto_0
    return v0

    .line 224
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/twitter/library/platform/notifications/PushRegistration;->e:Landroid/os/Handler;

    return-object v0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 321
    sget-object v0, Lcom/twitter/library/platform/notifications/PushRegistration;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 322
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/twitter/library/platform/notifications/PushRegistration;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/twitter/database/schema/a;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 323
    sget-boolean v0, Lcom/twitter/library/platform/notifications/PushRegistration;->f:Z

    if-eqz v0, :cond_0

    .line 324
    sget-object v0, Lcom/twitter/library/platform/notifications/PushRegistration;->e:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/library/platform/notifications/PushRegistration$b;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lazw$k;->preference_notification_error:I

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/platform/notifications/PushRegistration$b;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 326
    invoke-static {}, Lcom/twitter/library/platform/notifications/PushRegistration;->b()V

    .line 328
    :cond_0
    new-instance v0, Lcom/twitter/library/platform/notifications/PushRegistration$a;

    invoke-direct {v0}, Lcom/twitter/library/platform/notifications/PushRegistration$a;-><init>()V

    const-string/jumbo v1, "Error id"

    .line 329
    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/platform/notifications/PushRegistration$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/platform/notifications/PushRegistration$DebugNotificationException;

    const-string/jumbo v2, "onError in PushService"

    invoke-direct {v1, v2}, Lcom/twitter/library/platform/notifications/PushRegistration$DebugNotificationException;-><init>(Ljava/lang/String;)V

    .line 330
    invoke-virtual {v0, v1}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v0

    .line 328
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    .line 331
    return-void
.end method

.method public static c(Landroid/content/Context;J)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 245
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 246
    invoke-static {v1}, Lcom/google/android/gcm/b;->g(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 247
    invoke-static {p0, p1, p2}, Lcom/twitter/library/platform/notifications/t;->a(Landroid/content/Context;J)Lcom/twitter/library/platform/notifications/t;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/platform/notifications/t;->a(Z)V

    .line 251
    :goto_0
    return v0

    .line 250
    :cond_0
    invoke-static {v1}, Lcom/twitter/library/platform/notifications/PushRegistration;->a(Landroid/content/Context;)V

    .line 251
    const/4 v0, 0x0

    goto :goto_0
.end method
