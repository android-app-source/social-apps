.class public final Lcom/twitter/library/platform/a;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 122
    return-void
.end method

.method private static a(ILandroid/content/Context;Lakm;Lcom/twitter/model/account/OAuthToken;Lcom/twitter/model/core/TwitterUser;Landroid/content/SyncResult;ZZZZ)I
    .locals 10

    .prologue
    .line 484
    move/from16 v0, p7

    move/from16 v1, p8

    invoke-static {p0, p4, v0, v1}, Lcom/twitter/library/platform/a;->a(ILcom/twitter/model/core/TwitterUser;ZZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 485
    const-string/jumbo v2, "TwitterDataSync"

    const-string/jumbo v3, "=====> Sync activity"

    invoke-static {v2, v3}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    iget-wide v4, p4, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 487
    new-instance v3, Lcom/twitter/library/service/v;

    invoke-virtual {p2}, Lakm;->d()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x1

    move-object v7, p3

    invoke-direct/range {v3 .. v8}, Lcom/twitter/library/service/v;-><init>(JLjava/lang/String;Lcom/twitter/model/account/OAuthToken;Z)V

    .line 488
    invoke-static {v4, v5}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v6

    .line 490
    invoke-static {v4, v5, p0}, Lcom/twitter/library/api/activity/c;->a(JI)V

    .line 491
    new-instance v2, Lcom/twitter/library/api/activity/FetchActivityTimeline;

    iget-boolean v7, p4, Lcom/twitter/model/core/TwitterUser;->m:Z

    invoke-direct {v2, p1, v3, p0, v7}, Lcom/twitter/library/api/activity/FetchActivityTimeline;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;IZ)V

    .line 493
    move/from16 v0, p6

    invoke-virtual {v2, v0}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->a(Z)Lcom/twitter/library/api/activity/FetchActivityTimeline;

    move-result-object v2

    const/16 v3, 0x14

    .line 494
    invoke-virtual {v2, v3}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->c(I)Lbge;

    move-result-object v2

    new-instance v3, Lbsk;

    .line 495
    invoke-virtual {v6}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v7

    invoke-direct {v3, v7}, Lbsk;-><init>(Lcom/twitter/database/model/i;)V

    invoke-virtual {v3, p0}, Lbsk;->b(I)J

    move-result-wide v8

    invoke-virtual {v2, v8, v9}, Lbge;->c(J)Lbge;

    move-result-object v2

    const-string/jumbo v3, "Data sync happens in the background and is not triggered by a user action."

    .line 496
    invoke-virtual {v2, v3}, Lbge;->l(Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/activity/FetchActivityTimeline;

    .line 497
    invoke-virtual {v2}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->O()Lcom/twitter/library/service/u;

    move-result-object v3

    .line 498
    invoke-virtual {v3}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v3

    iget v3, v3, Lcom/twitter/network/l;->a:I

    .line 499
    const/16 v7, 0xc8

    if-ne v3, v7, :cond_0

    .line 500
    invoke-static {v4, v5, p0}, Lcom/twitter/library/api/activity/c;->b(JI)V

    .line 501
    invoke-virtual {p2}, Lakm;->b()Lcnz;

    move-result-object v3

    invoke-virtual {v3}, Lcnz;->b()J

    move-result-wide v4

    invoke-static {p1, v4, v5}, Lcom/twitter/library/platform/TwitterDataSyncService;->b(Landroid/content/Context;J)V

    .line 502
    if-eqz p6, :cond_1

    if-nez p9, :cond_1

    .line 503
    invoke-virtual {v2}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->e()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/util/Collection;)I

    move-result v2

    if-lez v2, :cond_1

    .line 504
    new-instance v2, Lbsk;

    invoke-virtual {v6}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v3

    invoke-direct {v2, v3}, Lbsk;-><init>(Lcom/twitter/database/model/i;)V

    invoke-virtual {v2, p0}, Lbsk;->a(I)I

    move-result v2

    .line 510
    :goto_0
    return v2

    .line 507
    :cond_0
    invoke-static {p0, p5, v4, v5, v3}, Lcom/twitter/library/platform/a;->a(ILandroid/content/SyncResult;JI)V

    .line 510
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static a(ILandroid/content/Context;Lakm;Lcom/twitter/model/core/TwitterUser;Landroid/content/SyncResult;ZZ)V
    .locals 12

    .prologue
    .line 516
    move/from16 v0, p5

    move/from16 v1, p6

    invoke-static {p0, p3, v0, v1}, Lcom/twitter/library/platform/a;->a(ILcom/twitter/model/core/TwitterUser;ZZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 517
    iget-wide v10, p3, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 518
    invoke-static {v10, v11, p0}, Lcom/twitter/library/api/activity/c;->a(JI)V

    .line 519
    new-instance v2, Lbsk;

    .line 520
    invoke-static {v10, v11}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v3

    invoke-direct {v2, v3}, Lbsk;-><init>(Lcom/twitter/database/model/i;)V

    .line 521
    new-instance v3, Lcom/twitter/library/api/activity/f;

    .line 522
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {p3}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v6

    move-object v4, p1

    move v8, p0

    invoke-direct/range {v3 .. v8}, Lcom/twitter/library/api/activity/f;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JI)V

    .line 523
    invoke-virtual {v2, p0}, Lbsk;->b(I)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/api/activity/f;->c(J)Lbge;

    move-result-object v2

    const/16 v3, 0x14

    .line 524
    invoke-virtual {v2, v3}, Lbge;->c(I)Lbge;

    move-result-object v2

    const-string/jumbo v3, "Data sync happens in the background and is not triggered by a user action."

    .line 525
    invoke-virtual {v2, v3}, Lbge;->l(Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/activity/f;

    .line 526
    invoke-virtual {v2}, Lcom/twitter/library/api/activity/f;->O()Lcom/twitter/library/service/u;

    move-result-object v2

    .line 527
    invoke-virtual {v2}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v2

    iget v2, v2, Lcom/twitter/network/l;->a:I

    .line 528
    const/16 v3, 0xc8

    if-ne v2, v3, :cond_1

    .line 529
    invoke-static {v10, v11, p0}, Lcom/twitter/library/api/activity/c;->b(JI)V

    .line 530
    invoke-virtual {p2}, Lakm;->b()Lcnz;

    move-result-object v2

    invoke-virtual {v2}, Lcnz;->b()J

    move-result-wide v2

    invoke-static {p1, v2, v3}, Lcom/twitter/library/platform/TwitterDataSyncService;->b(Landroid/content/Context;J)V

    .line 535
    :cond_0
    :goto_0
    return-void

    .line 532
    :cond_1
    move-object/from16 v0, p4

    invoke-static {p0, v0, v10, v11, v2}, Lcom/twitter/library/platform/a;->a(ILandroid/content/SyncResult;JI)V

    goto :goto_0
.end method

.method private static a(ILandroid/content/SyncResult;JI)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 539
    invoke-static {p2, p3, p0}, Lcom/twitter/library/api/activity/c;->c(JI)V

    .line 540
    if-nez p4, :cond_1

    .line 541
    iget-object v0, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numParseExceptions:J

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numParseExceptions:J

    .line 545
    :cond_0
    :goto_0
    return-void

    .line 542
    :cond_1
    const/16 v0, 0x191

    if-ne p4, v0, :cond_0

    .line 543
    iget-object v0, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    goto :goto_0
.end method

.method private a(Lakm;Lcom/twitter/model/account/OAuthToken;Lcom/twitter/model/core/TwitterUser;Landroid/content/SyncResult;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x1

    .line 299
    const-string/jumbo v0, "android_pending_followers_navigation_5203"

    invoke-static {v0}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p3, Lcom/twitter/model/core/TwitterUser;->l:Z

    if-eqz v0, :cond_0

    .line 301
    invoke-virtual {p0}, Lcom/twitter/library/platform/a;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 302
    iget-wide v2, p3, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 303
    new-instance v1, Lcom/twitter/library/service/v;

    invoke-virtual {p1}, Lakm;->d()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/service/v;-><init>(JLjava/lang/String;Lcom/twitter/model/account/OAuthToken;Z)V

    .line 304
    new-instance v2, Lbib;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lbib;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;I)V

    const-string/jumbo v0, "Data sync happens in the background and is not triggered by a user action."

    .line 305
    invoke-virtual {v2, v0}, Lbib;->l(Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v0

    .line 306
    invoke-virtual {v0}, Lcom/twitter/library/service/s;->O()Lcom/twitter/library/service/u;

    move-result-object v0

    .line 307
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 308
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    .line 309
    if-nez v0, :cond_1

    .line 310
    iget-object v0, p4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numParseExceptions:J

    add-long/2addr v2, v8

    iput-wide v2, v0, Landroid/content/SyncStats;->numParseExceptions:J

    .line 316
    :cond_0
    :goto_0
    return-void

    .line 311
    :cond_1
    const/16 v1, 0x194

    if-ne v0, v1, :cond_0

    .line 312
    iget-object v0, p4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v2, v8

    iput-wide v2, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    goto :goto_0
.end method

.method static a(Landroid/content/Context;)V
    .locals 14

    .prologue
    const v3, 0x7fffffff

    const/4 v1, 0x0

    .line 581
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/library/platform/TwitterDataSyncService;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "on_poll_alarm_ev"

    .line 582
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 583
    const/high16 v0, 0x20000000

    invoke-static {p0, v1, v6, v0}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    .line 584
    const-string/jumbo v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 586
    invoke-static {}, Lcom/twitter/library/provider/j;->c()Lcom/twitter/library/provider/j;

    move-result-object v4

    .line 587
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v9

    .line 588
    const-string/jumbo v2, "alarm_interval"

    const/4 v5, -0x1

    invoke-interface {v9, v2, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 590
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v2

    invoke-virtual {v2}, Lakn;->c()Ljava/util/List;

    move-result-object v2

    .line 592
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v7, v3

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lakm;

    .line 593
    invoke-virtual {v2}, Lakm;->b()Lcnz;

    move-result-object v2

    invoke-virtual {v2}, Lcnz;->b()J

    move-result-wide v12

    .line 594
    invoke-static {p0, v12, v13}, Lcom/twitter/library/platform/notifications/t;->a(Landroid/content/Context;J)Lcom/twitter/library/platform/notifications/t;

    move-result-object v2

    .line 595
    invoke-virtual {v2}, Lcom/twitter/library/platform/notifications/t;->a()Z

    move-result v2

    invoke-virtual {v4, v12, v13, v2}, Lcom/twitter/library/provider/j;->b(JZ)I

    move-result v2

    invoke-static {v7, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v7, v2

    .line 596
    goto :goto_0

    .line 598
    :cond_0
    if-ge v7, v3, :cond_4

    .line 599
    if-ne v7, v5, :cond_1

    if-nez v8, :cond_3

    .line 601
    :cond_1
    int-to-long v2, v7

    const-wide/32 v4, 0xea60

    mul-long/2addr v4, v2

    .line 602
    if-eqz v8, :cond_2

    .line 603
    invoke-virtual {v0, v8}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 606
    :cond_2
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide/32 v10, 0x36ee80

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    long-to-double v10, v10

    mul-double/2addr v2, v10

    double-to-long v2, v2

    .line 608
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v10

    add-long/2addr v10, v4

    add-long/2addr v2, v10

    .line 609
    invoke-static {p0, v1, v6, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 607
    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 610
    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "alarm_interval"

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 619
    :cond_3
    :goto_1
    return-void

    .line 614
    :cond_4
    if-eqz v8, :cond_3

    .line 615
    invoke-virtual {v0, v8}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 616
    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "alarm_interval"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Lakm;Lcom/twitter/model/account/OAuthToken;Lcom/twitter/model/core/TwitterUser;)V
    .locals 7

    .prologue
    .line 554
    iget-wide v2, p3, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 555
    new-instance v1, Lcom/twitter/library/service/v;

    invoke-virtual {p1}, Lakm;->d()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/service/v;-><init>(JLjava/lang/String;Lcom/twitter/model/account/OAuthToken;Z)V

    .line 556
    new-instance v0, Lbdz;

    invoke-direct {v0, p0, v1}, Lbdz;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V

    .line 557
    invoke-static {}, Lbsi;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbdz;->a(Ljava/lang/String;)Lbdz;

    move-result-object v0

    .line 558
    invoke-static {}, Lbsi;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbdz;->b(Ljava/lang/String;)Lbdz;

    move-result-object v0

    const/16 v1, 0x1e

    .line 559
    invoke-virtual {v0, v1}, Lbdz;->a(I)Lbdz;

    move-result-object v0

    const-string/jumbo v1, "Data sync happens in the background and is not triggered by a user action."

    .line 560
    invoke-virtual {v0, v1}, Lbdz;->l(Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v0

    .line 561
    invoke-virtual {v0}, Lcom/twitter/library/service/s;->O()Lcom/twitter/library/service/u;

    .line 562
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/content/SyncResult;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 452
    new-instance v0, Lcom/twitter/library/api/dm/j;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/api/dm/j;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const-string/jumbo v1, "Data sync happens in the background and is not triggered by a user action."

    .line 453
    invoke-virtual {v0, v1}, Lcom/twitter/library/api/dm/j;->l(Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v0

    .line 454
    invoke-virtual {v0}, Lcom/twitter/library/service/s;->O()Lcom/twitter/library/service/u;

    move-result-object v0

    .line 455
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 456
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    .line 457
    if-nez v0, :cond_1

    .line 458
    iget-object v0, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numParseExceptions:J

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numParseExceptions:J

    .line 463
    :cond_0
    :goto_0
    return-void

    .line 459
    :cond_1
    const/16 v1, 0x194

    if-ne v0, v1, :cond_0

    .line 460
    iget-object v0, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/library/network/a;JII)V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 391
    invoke-static {p2, p3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v1

    .line 393
    invoke-virtual {v1, p4, p5}, Lcom/twitter/library/provider/t;->a(II)[J

    move-result-object v7

    .line 395
    if-nez v7, :cond_1

    .line 437
    :cond_0
    return-void

    .line 399
    :cond_1
    invoke-static {p0}, Lcom/twitter/library/network/ab;->a(Landroid/content/Context;)Lcom/twitter/library/network/ab;

    move-result-object v0

    .line 401
    iget-object v0, v0, Lcom/twitter/library/network/ab;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const-string/jumbo v4, "1.1"

    aput-object v4, v3, v2

    const-string/jumbo v4, "users"

    aput-object v4, v3, v6

    const/4 v4, 0x2

    const-string/jumbo v5, "lookup"

    aput-object v5, v3, v4

    invoke-static {v0, v3}, Lcom/twitter/library/network/ab;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, ".json"

    .line 402
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 404
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 405
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 407
    new-instance v0, Lcom/twitter/library/network/ab$a;

    invoke-direct {v0}, Lcom/twitter/library/network/ab$a;-><init>()V

    .line 408
    const-string/jumbo v4, "user_id"

    array-length v5, v7

    invoke-virtual {v0, v4, v7, v2, v5}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;[JII)I

    .line 409
    const-string/jumbo v4, "include_user_entities"

    invoke-virtual {v0, v4, v6}, Lcom/twitter/library/network/ab$a;->a(Ljava/lang/String;Z)V

    .line 410
    invoke-virtual {v0}, Lcom/twitter/library/network/ab$a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    invoke-static {}, Lcpd;->a()Lcpd;

    move-result-object v0

    invoke-virtual {v0}, Lcpd;->b()Lcpa;

    move-result-object v0

    const-string/jumbo v4, "data_sync_adapter_url"

    invoke-virtual {v0, v4, v3}, Lcpa;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414
    const-class v0, Lcom/twitter/model/core/TwitterUser;

    .line 415
    invoke-static {v0}, Lcom/twitter/library/api/j;->a(Ljava/lang/Class;)Lcom/twitter/library/api/j;

    move-result-object v0

    .line 416
    new-instance v4, Lcom/twitter/library/network/k;

    invoke-direct {v4, p0, v3}, Lcom/twitter/library/network/k;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 417
    invoke-virtual {v4, p2, p3}, Lcom/twitter/library/network/k;->a(J)Lcom/twitter/library/network/k;

    move-result-object v3

    .line 418
    invoke-virtual {v3, p1}, Lcom/twitter/library/network/k;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/k;

    move-result-object v3

    const-string/jumbo v4, "Data sync happens in the background and is not triggered by a user action."

    .line 419
    invoke-virtual {v3, v4}, Lcom/twitter/library/network/k;->a(Ljava/lang/String;)Lcom/twitter/library/network/k;

    move-result-object v3

    .line 420
    invoke-virtual {v3, v0}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/j;)Lcom/twitter/library/network/k;

    move-result-object v3

    .line 421
    invoke-virtual {v3}, Lcom/twitter/library/network/k;->a()Lcom/twitter/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/network/HttpOperation;->c()Lcom/twitter/network/HttpOperation;

    move-result-object v3

    .line 423
    invoke-virtual {v3}, Lcom/twitter/network/HttpOperation;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 424
    invoke-virtual {v0}, Lcom/twitter/library/api/i;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 425
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 426
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 427
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 429
    :cond_2
    array-length v9, v7

    move v0, v2

    :goto_1
    if-ge v0, v9, :cond_0

    aget-wide v4, v7, v0

    .line 430
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v8, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 431
    new-instance v6, Laut;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v6, v2}, Laut;-><init>(Landroid/content/ContentResolver;)V

    move-wide v2, p2

    .line 432
    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/t;->c(JJLaut;)V

    .line 433
    invoke-virtual {v6}, Laut;->a()V

    .line 429
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private a(Landroid/content/SyncResult;)V
    .locals 5

    .prologue
    .line 320
    invoke-virtual {p0}, Lcom/twitter/library/platform/a;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 321
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 322
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 323
    invoke-static {v0, v1}, Lbbg;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lbbg;

    move-result-object v4

    invoke-virtual {v4}, Lbbg;->O()Lcom/twitter/library/service/u;

    .line 325
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v1

    .line 326
    if-eqz v1, :cond_1

    iget-boolean v4, v1, Lcom/twitter/model/account/UserSettings;->x:Z

    if-eqz v4, :cond_1

    .line 327
    invoke-static {v0, v2, v3}, Lbmp;->a(Landroid/content/Context;J)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string/jumbo v4, "people_discovery_live_sync_enabled"

    .line 328
    invoke-static {v4}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 329
    invoke-static {v0}, Lcom/twitter/library/util/h;->a(Landroid/content/Context;)Lcom/twitter/library/util/g;

    move-result-object v0

    .line 330
    invoke-interface {v0}, Lcom/twitter/library/util/g;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 331
    invoke-interface {v0}, Lcom/twitter/library/util/g;->a()Landroid/database/Cursor;

    move-result-object v1

    .line 332
    if-eqz v1, :cond_0

    .line 334
    invoke-interface {v0, v1}, Lcom/twitter/library/util/g;->a(Landroid/database/Cursor;)Ljava/util/Map;

    move-result-object v2

    .line 335
    invoke-interface {v0, v2}, Lcom/twitter/library/util/g;->a(Ljava/util/Map;)Lbmo;

    move-result-object v2

    .line 336
    invoke-virtual {v2}, Lbmo;->b()Ljava/util/Map;

    move-result-object v3

    .line 337
    invoke-virtual {v2}, Lbmo;->a()Ljava/util/Set;

    move-result-object v2

    .line 338
    new-instance v4, Lcom/twitter/library/util/c;

    invoke-direct {v4, p1}, Lcom/twitter/library/util/c;-><init>(Landroid/content/SyncResult;)V

    .line 339
    invoke-interface {v0, v3, v4}, Lcom/twitter/library/util/g;->a(Ljava/util/Map;Lcom/twitter/library/util/k;)V

    .line 340
    invoke-interface {v0, v2, v4}, Lcom/twitter/library/util/g;->a(Ljava/util/Set;Lcom/twitter/library/util/k;)V

    .line 341
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 348
    :cond_0
    :goto_0
    return-void

    .line 344
    :cond_1
    if-eqz v1, :cond_0

    iget-boolean v1, v1, Lcom/twitter/model/account/UserSettings;->x:Z

    if-nez v1, :cond_0

    .line 346
    const/4 v1, 0x0

    invoke-static {v0, v2, v3, v1}, Lbmp;->a(Landroid/content/Context;JI)V

    goto :goto_0
.end method

.method static a(ILcom/twitter/model/core/TwitterUser;ZZ)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 361
    packed-switch p0, :pswitch_data_0

    move v0, v1

    .line 384
    :cond_0
    :goto_0
    return v0

    .line 366
    :pswitch_0
    if-nez p3, :cond_1

    if-eqz p2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 372
    :pswitch_1
    if-eqz p3, :cond_2

    if-eqz p2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :pswitch_2
    move v0, p2

    .line 378
    goto :goto_0

    .line 381
    :pswitch_3
    iget-boolean v0, p1, Lcom/twitter/model/core/TwitterUser;->m:Z

    goto :goto_0

    .line 361
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private static b(Landroid/content/Context;Lakm;Lcom/twitter/model/account/OAuthToken;Lcom/twitter/model/core/TwitterUser;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 566
    iget-wide v2, p3, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 567
    new-instance v1, Lcom/twitter/library/service/v;

    invoke-virtual {p1}, Lakm;->d()Ljava/lang/String;

    move-result-object v4

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/service/v;-><init>(JLjava/lang/String;Lcom/twitter/model/account/OAuthToken;Z)V

    .line 568
    invoke-static {p0, v1, v6}, Lbfd;->a(Landroid/content/Context;Lcom/twitter/library/service/v;Z)Lber;

    move-result-object v0

    .line 569
    if-eqz v0, :cond_0

    .line 570
    invoke-virtual {v0}, Lber;->O()Lcom/twitter/library/service/u;

    .line 572
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/SyncResult;)V
    .locals 22

    .prologue
    .line 126
    if-nez p1, :cond_0

    .line 294
    :goto_0
    return-void

    .line 129
    :cond_0
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lakn;->a(Landroid/accounts/Account;)Lakm;

    move-result-object v4

    .line 130
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/platform/a;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 131
    invoke-static {v4}, Lcom/twitter/library/util/b;->c(Lakm;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v6

    .line 132
    if-nez v6, :cond_1

    .line 133
    move-object/from16 v0, p3

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 134
    const-string/jumbo v2, "TwitterDataSync"

    const-string/jumbo v3, "User Info content not found."

    invoke-static {v2, v3}, Lcqj;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 138
    :cond_1
    invoke-static {}, Lcpd;->a()Lcpd;

    move-result-object v2

    invoke-virtual {v2}, Lcpd;->b()Lcpa;

    move-result-object v19

    .line 139
    invoke-virtual/range {v19 .. v19}, Lcpa;->a()I

    .line 142
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v4, v2}, Lcom/twitter/library/util/b;->a(Lakm;Z)Lcom/twitter/model/account/OAuthToken;

    move-result-object v5

    .line 143
    if-nez v5, :cond_2

    .line 144
    move-object/from16 v0, p3

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 145
    const-string/jumbo v2, "TwitterDataSync"

    const-string/jumbo v3, "Token not found."

    invoke-static {v2, v3}, Lcqj;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 292
    invoke-virtual/range {v19 .. v19}, Lcpa;->b()I

    goto :goto_0

    .line 148
    :cond_2
    :try_start_1
    new-instance v20, Lcom/twitter/util/a;

    iget-wide v8, v6, Lcom/twitter/model/core/TwitterUser;->b:J

    move-object/from16 v0, v20

    invoke-direct {v0, v3, v8, v9}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 150
    const-string/jumbo v2, "data_sync_adapter_owner_id"

    iget-wide v8, v6, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v7}, Lcpa;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    const-string/jumbo v2, "messages"

    const/4 v7, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 154
    move-object/from16 v0, p3

    invoke-static {v3, v0}, Lcom/twitter/library/platform/a;->a(Landroid/content/Context;Landroid/content/SyncResult;)V

    .line 158
    :cond_3
    const-string/jumbo v2, "activity"

    const/4 v7, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 159
    invoke-static {v3}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;)Z

    move-result v11

    .line 160
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 161
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v2

    .line 163
    if-eqz v2, :cond_c

    iget-object v7, v2, Lcom/twitter/model/account/UserSettings;->A:Ljava/lang/String;

    const-string/jumbo v8, "following"

    .line 164
    invoke-static {v7, v8}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    const/4 v9, 0x1

    .line 165
    :goto_1
    if-eqz v2, :cond_d

    iget-object v2, v2, Lcom/twitter/model/account/UserSettings;->z:Ljava/lang/String;

    const-string/jumbo v7, "enabled"

    invoke-static {v2, v7}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    const/4 v10, 0x1

    .line 169
    :goto_2
    const/4 v2, 0x0

    const/4 v8, 0x1

    move-object/from16 v7, p3

    .line 170
    invoke-static/range {v2 .. v11}, Lcom/twitter/library/platform/a;->a(ILandroid/content/Context;Lakm;Lcom/twitter/model/account/OAuthToken;Lcom/twitter/model/core/TwitterUser;Landroid/content/SyncResult;ZZZZ)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    const/4 v2, 0x2

    new-array v13, v2, [Ljava/lang/Integer;

    const/4 v14, 0x0

    const/4 v2, 0x2

    const/4 v8, 0x1

    move-object/from16 v7, p3

    .line 172
    invoke-static/range {v2 .. v11}, Lcom/twitter/library/platform/a;->a(ILandroid/content/Context;Lakm;Lcom/twitter/model/account/OAuthToken;Lcom/twitter/model/core/TwitterUser;Landroid/content/SyncResult;ZZZZ)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v13, v14

    const/4 v14, 0x1

    const/4 v2, 0x3

    const/4 v8, 0x1

    move-object/from16 v7, p3

    .line 174
    invoke-static/range {v2 .. v11}, Lcom/twitter/library/platform/a;->a(ILandroid/content/Context;Lakm;Lcom/twitter/model/account/OAuthToken;Lcom/twitter/model/core/TwitterUser;Landroid/content/SyncResult;ZZZZ)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v13, v14

    .line 169
    invoke-static {v12, v13}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v21

    .line 179
    invoke-static {}, Lcom/twitter/library/api/activity/g;->a()Lcom/twitter/library/api/activity/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/api/activity/g;->b()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 180
    const/4 v12, 0x7

    move-object v13, v3

    move-object v14, v4

    move-object v15, v6

    move-object/from16 v16, p3

    move/from16 v17, v9

    move/from16 v18, v10

    invoke-static/range {v12 .. v18}, Lcom/twitter/library/platform/a;->a(ILandroid/content/Context;Lakm;Lcom/twitter/model/core/TwitterUser;Landroid/content/SyncResult;ZZ)V

    .line 182
    const/16 v12, 0x8

    move-object v13, v3

    move-object v14, v4

    move-object v15, v6

    move-object/from16 v16, p3

    move/from16 v17, v9

    move/from16 v18, v10

    invoke-static/range {v12 .. v18}, Lcom/twitter/library/platform/a;->a(ILandroid/content/Context;Lakm;Lcom/twitter/model/core/TwitterUser;Landroid/content/SyncResult;ZZ)V

    .line 184
    const/16 v12, 0x9

    move-object v13, v3

    move-object v14, v4

    move-object v15, v6

    move-object/from16 v16, p3

    move/from16 v17, v9

    move/from16 v18, v10

    invoke-static/range {v12 .. v18}, Lcom/twitter/library/platform/a;->a(ILandroid/content/Context;Lakm;Lcom/twitter/model/core/TwitterUser;Landroid/content/SyncResult;ZZ)V

    .line 194
    :goto_3
    const/4 v2, 0x4

    const/4 v8, 0x0

    move-object/from16 v7, p3

    invoke-static/range {v2 .. v11}, Lcom/twitter/library/platform/a;->a(ILandroid/content/Context;Lakm;Lcom/twitter/model/account/OAuthToken;Lcom/twitter/model/core/TwitterUser;Landroid/content/SyncResult;ZZZZ)I

    .line 197
    new-instance v2, Lcom/twitter/library/platform/a$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/twitter/library/platform/a$1;-><init>(Lcom/twitter/library/platform/a;)V

    move-object/from16 v0, v21

    invoke-static {v0, v2}, Lcpt;->c(Ljava/lang/Iterable;Lcpv;)I

    move-result v2

    .line 203
    const/4 v7, -0x1

    if-eq v2, v7, :cond_f

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move v7, v2

    .line 205
    :goto_4
    new-instance v8, Landroid/content/Intent;

    sget-object v2, Lcom/twitter/library/platform/TwitterDataSyncService;->a:Ljava/lang/String;

    invoke-direct {v8, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 207
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v8, v2}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    .line 208
    if-nez v11, :cond_10

    if-lez v7, :cond_10

    const-string/jumbo v2, "show_notif"

    const/4 v9, 0x0

    .line 209
    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_10

    const/4 v2, 0x1

    .line 210
    :goto_5
    if-eqz v2, :cond_4

    .line 211
    new-instance v9, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v10, v6, Lcom/twitter/model/core/TwitterUser;->b:J

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string/jumbo v14, "app:::sync:notify"

    aput-object v14, v12, v13

    invoke-direct {v9, v10, v11, v12}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    invoke-static {v9}, Lcpm;->a(Lcpk;)V

    .line 213
    :cond_4
    const-string/jumbo v9, "show_notif"

    invoke-virtual {v8, v9, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v9, "unread_notifications_count"

    .line 214
    invoke-virtual {v2, v9, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v7, "owner_id"

    iget-wide v10, v6, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 215
    invoke-virtual {v2, v7, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 216
    sget-object v2, Lcom/twitter/database/schema/a;->a:Ljava/lang/String;

    invoke-virtual {v3, v8, v2}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 220
    :cond_5
    const-string/jumbo v2, "live_addressbook_sync"

    const/4 v7, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 221
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/twitter/library/platform/a;->a(Landroid/content/SyncResult;)V

    .line 225
    :cond_6
    const-string/jumbo v2, "android_pending_followers_navigation_5203"

    invoke-static {v2}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string/jumbo v2, "user_settings_sync"

    const/4 v7, 0x1

    .line 226
    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 227
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v4, v5, v6, v1}, Lcom/twitter/library/platform/a;->a(Lakm;Lcom/twitter/model/account/OAuthToken;Lcom/twitter/model/core/TwitterUser;Landroid/content/SyncResult;)V

    .line 231
    :cond_7
    iget-wide v8, v6, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v8, v9}, Lbsi;->g(J)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string/jumbo v2, "news"

    const/4 v7, 0x1

    .line 232
    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-wide v8, v6, Lcom/twitter/model/core/TwitterUser;->b:J

    const-string/jumbo v2, "japan_news_android_periodic_sync_enabled"

    const/4 v7, 0x0

    .line 233
    invoke-static {v8, v9, v2, v7}, Lcoj;->a(JLjava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 235
    const-string/jumbo v2, "news_last_sync"

    const-wide/16 v8, 0x0

    .line 236
    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Lcom/twitter/util/a;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 237
    iget-wide v10, v6, Lcom/twitter/model/core/TwitterUser;->b:J

    const-string/jumbo v2, "japan_news_android_periodic_sync_interval_seconds"

    const/4 v7, -0x1

    invoke-static {v10, v11, v2, v7}, Lcoj;->a(JLjava/lang/String;I)I

    move-result v2

    .line 239
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v10

    .line 241
    if-lez v2, :cond_8

    int-to-long v12, v2

    const-wide/16 v14, 0x3e8

    mul-long/2addr v12, v14

    add-long/2addr v8, v12

    cmp-long v2, v10, v8

    if-lez v2, :cond_8

    .line 243
    invoke-static {v3, v4, v5, v6}, Lcom/twitter/library/platform/a;->a(Landroid/content/Context;Lakm;Lcom/twitter/model/account/OAuthToken;Lcom/twitter/model/core/TwitterUser;)V

    .line 244
    invoke-virtual/range {v20 .. v20}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v2

    const-string/jumbo v7, "news_last_sync"

    invoke-virtual {v2, v7, v10, v11}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;J)Lcom/twitter/util/a$a;

    .line 250
    :cond_8
    invoke-virtual/range {p3 .. p3}, Landroid/content/SyncResult;->hasError()Z

    move-result v2

    if-nez v2, :cond_9

    .line 251
    new-instance v8, Lben;

    iget-wide v10, v6, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v12, v6, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    move-object v9, v3

    move-object v13, v5

    invoke-direct/range {v8 .. v13}, Lben;-><init>(Landroid/content/Context;JLjava/lang/String;Lcom/twitter/model/account/OAuthToken;)V

    const-string/jumbo v2, "Data sync happens in the background and is not triggered by a user action."

    .line 252
    invoke-virtual {v8, v2}, Lben;->l(Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v2

    .line 253
    invoke-virtual {v2}, Lcom/twitter/library/service/s;->O()Lcom/twitter/library/service/u;

    .line 260
    :cond_9
    const-string/jumbo v2, "fs_config"

    const/4 v7, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 261
    iget-wide v8, v6, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v8, v9}, Lbpy;->a(J)V

    .line 265
    :cond_a
    const-string/jumbo v2, "legacy_deciders_antispam_query_frequency_sec"

    const/4 v7, 0x0

    invoke-static {v2, v7}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v2

    .line 267
    const-string/jumbo v7, "antispam_last_poll_timestamp"

    const-wide/16 v8, 0x0

    .line 268
    move-object/from16 v0, v20

    invoke-virtual {v0, v7, v8, v9}, Lcom/twitter/util/a;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 270
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v14

    .line 271
    int-to-long v10, v2

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    add-long/2addr v8, v10

    cmp-long v2, v14, v8

    if-lez v2, :cond_b

    .line 272
    const-string/jumbo v2, "legacy_deciders_antispam_connect_tweet_count"

    const/4 v7, 0x0

    invoke-static {v2, v7}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v12

    .line 274
    const-string/jumbo v2, "legacy_deciders_antispam_connect_user_count"

    const/4 v7, 0x0

    invoke-static {v2, v7}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v13

    .line 276
    new-instance v9, Lcom/twitter/library/network/t;

    invoke-direct {v9, v5}, Lcom/twitter/library/network/t;-><init>(Lcom/twitter/model/account/OAuthToken;)V

    iget-wide v10, v6, Lcom/twitter/model/core/TwitterUser;->b:J

    move-object v8, v3

    invoke-static/range {v8 .. v13}, Lcom/twitter/library/platform/a;->a(Landroid/content/Context;Lcom/twitter/library/network/a;JII)V

    .line 278
    invoke-virtual/range {v20 .. v20}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v2

    const-string/jumbo v7, "antispam_last_poll_timestamp"

    invoke-virtual {v2, v7, v14, v15}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;J)Lcom/twitter/util/a$a;

    .line 282
    :cond_b
    invoke-static {v3, v4, v5, v6}, Lcom/twitter/library/platform/a;->b(Landroid/content/Context;Lakm;Lcom/twitter/model/account/OAuthToken;Lcom/twitter/model/core/TwitterUser;)V

    .line 284
    invoke-virtual/range {v20 .. v20}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v2

    const-string/jumbo v3, "last_sync"

    invoke-virtual {v2, v3, v14, v15}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;J)Lcom/twitter/util/a$a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/util/a$a;->apply()V
    :try_end_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 292
    invoke-virtual/range {v19 .. v19}, Lcpa;->b()I

    goto/16 :goto_0

    .line 164
    :cond_c
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 165
    :cond_d
    const/4 v10, 0x0

    goto/16 :goto_2

    .line 187
    :cond_e
    const/4 v2, 0x1

    const/4 v8, 0x0

    move-object/from16 v7, p3

    :try_start_2
    invoke-static/range {v2 .. v11}, Lcom/twitter/library/platform/a;->a(ILandroid/content/Context;Lakm;Lcom/twitter/model/account/OAuthToken;Lcom/twitter/model/core/TwitterUser;Landroid/content/SyncResult;ZZZZ)I

    .line 189
    const/4 v2, 0x5

    const/4 v8, 0x0

    move-object/from16 v7, p3

    invoke-static/range {v2 .. v11}, Lcom/twitter/library/platform/a;->a(ILandroid/content/Context;Lakm;Lcom/twitter/model/account/OAuthToken;Lcom/twitter/model/core/TwitterUser;Landroid/content/SyncResult;ZZZZ)I

    .line 191
    const/4 v2, 0x6

    const/4 v8, 0x0

    move-object/from16 v7, p3

    invoke-static/range {v2 .. v11}, Lcom/twitter/library/platform/a;->a(ILandroid/content/Context;Lakm;Lcom/twitter/model/account/OAuthToken;Lcom/twitter/model/core/TwitterUser;Landroid/content/SyncResult;ZZZZ)I
    :try_end_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_3

    .line 285
    :catch_0
    move-exception v2

    .line 286
    :try_start_3
    move-object/from16 v0, p3

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v2, Landroid/content/SyncStats;->numAuthExceptions:J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 292
    invoke-virtual/range {v19 .. v19}, Lcpa;->b()I

    goto/16 :goto_0

    .line 203
    :cond_f
    const/4 v2, 0x0

    move v7, v2

    goto/16 :goto_4

    .line 209
    :cond_10
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 287
    :catch_1
    move-exception v2

    .line 288
    :try_start_4
    const-string/jumbo v2, "TwitterDataSync"

    const-string/jumbo v3, "Sync canceled."

    invoke-static {v2, v3}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 292
    invoke-virtual/range {v19 .. v19}, Lcpa;->b()I

    goto/16 :goto_0

    .line 289
    :catch_2
    move-exception v2

    .line 290
    :try_start_5
    move-object/from16 v0, p3

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 292
    invoke-virtual/range {v19 .. v19}, Lcpa;->b()I

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    invoke-virtual/range {v19 .. v19}, Lcpa;->b()I

    throw v2
.end method

.method a(Lakm;)Z
    .locals 12

    .prologue
    const-wide/32 v10, 0xea60

    const/4 v0, 0x0

    .line 622
    if-nez p1, :cond_1

    .line 637
    :cond_0
    :goto_0
    return v0

    .line 625
    :cond_1
    invoke-virtual {p1}, Lakm;->a()Landroid/accounts/Account;

    move-result-object v1

    sget-object v2, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 626
    invoke-virtual {p1}, Lakm;->a()Landroid/accounts/Account;

    move-result-object v1

    sget-object v2, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    .line 627
    invoke-virtual {p0}, Lcom/twitter/library/platform/a;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 628
    invoke-virtual {p1}, Lakm;->b()Lcnz;

    move-result-object v2

    invoke-virtual {v2}, Lcnz;->b()J

    move-result-wide v2

    .line 629
    new-instance v4, Lcom/twitter/util/a;

    invoke-direct {v4, v1, v2, v3}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 630
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v6

    const-string/jumbo v5, "last_sync"

    const-wide/16 v8, 0x0

    invoke-virtual {v4, v5, v8, v9}, Lcom/twitter/util/a;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    sub-long v4, v6, v4

    .line 631
    invoke-static {}, Lcom/twitter/library/provider/j;->c()Lcom/twitter/library/provider/j;

    move-result-object v6

    .line 632
    invoke-static {v1, v2, v3}, Lcom/twitter/library/platform/notifications/t;->a(Landroid/content/Context;J)Lcom/twitter/library/platform/notifications/t;

    move-result-object v1

    .line 634
    invoke-virtual {v1}, Lcom/twitter/library/platform/notifications/t;->a()Z

    move-result v1

    .line 633
    invoke-virtual {v6, v2, v3, v1}, Lcom/twitter/library/provider/j;->b(JZ)I

    move-result v1

    int-to-long v2, v1

    mul-long/2addr v2, v10

    .line 635
    sub-long/2addr v2, v10

    cmp-long v1, v4, v2

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 0

    .prologue
    .line 442
    invoke-virtual {p0, p1, p2, p5}, Lcom/twitter/library/platform/a;->a(Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/SyncResult;)V

    .line 443
    return-void
.end method
