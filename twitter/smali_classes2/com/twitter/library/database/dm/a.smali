.class public Lcom/twitter/library/database/dm/a;
.super Laur;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/database/dm/b;


# instance fields
.field private final a:J


# direct methods
.method public constructor <init>(Landroid/content/Context;J)V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "-dm.db"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Laur;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 39
    iput-wide p2, p0, Lcom/twitter/library/database/dm/a;->a:J

    .line 40
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/database/dm/a;)J
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/twitter/library/database/dm/a;->a:J

    return-wide v0
.end method

.method public static a(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "-dm.db"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/d;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 55
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/d;

    .line 56
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/model/dms/d;->o()I

    move-result v3

    if-nez v3, :cond_0

    .line 57
    check-cast v0, Lcom/twitter/model/dms/a;

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 60
    :cond_1
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static declared-synchronized b(J)Lcom/twitter/library/database/dm/a;
    .locals 2

    .prologue
    .line 49
    const-class v1, Lcom/twitter/library/database/dm/a;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcnz;

    invoke-direct {v0, p0, p1}, Lcnz;-><init>(J)V

    invoke-static {v0}, Lbnt;->b(Lcnz;)Lbnt;

    move-result-object v0

    invoke-virtual {v0}, Lbnt;->am()Lcom/twitter/library/database/dm/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(I)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/database/dm/c;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 74
    invoke-virtual {p0}, Lcom/twitter/library/database/dm/a;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 75
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    move-object v8, v3

    .line 76
    :goto_0
    const-string/jumbo v1, "share_history"

    sget-object v2, Lcom/twitter/library/database/dm/ShareHistoryTable$b;->a:[Ljava/lang/String;

    const-string/jumbo v5, "reference_id,type"

    const-string/jumbo v7, "type ASC, MAX(event_id) DESC"

    move-object v4, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 85
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 86
    new-instance v2, Lcom/twitter/library/database/dm/ShareHistoryTable$b$a;

    invoke-direct {v2}, Lcom/twitter/library/database/dm/ShareHistoryTable$b$a;-><init>()V

    .line 87
    if-eqz v1, :cond_2

    .line 89
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 90
    invoke-virtual {v2, v1}, Lcom/twitter/library/database/dm/ShareHistoryTable$b$a;->a(Landroid/database/Cursor;)Lcom/twitter/library/database/dm/c;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 93
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 75
    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .line 93
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 96
    :cond_2
    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public a(Lcom/twitter/model/dms/p;)V
    .locals 13

    .prologue
    const/4 v12, 0x1

    .line 132
    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/library/database/dm/a;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 138
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v1

    .line 139
    invoke-virtual {p1}, Lcom/twitter/model/dms/p;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/l;

    .line 140
    iget-object v3, v0, Lcom/twitter/model/dms/l;->a:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    goto :goto_0

    .line 133
    :catch_0
    move-exception v0

    .line 134
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 180
    :cond_0
    return-void

    .line 142
    :cond_1
    invoke-virtual {v1}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 144
    invoke-virtual {p1}, Lcom/twitter/model/dms/p;->c()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/database/dm/a;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/database/dm/a$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/library/database/dm/a$1;-><init>(Lcom/twitter/library/database/dm/a;Ljava/util/Map;)V

    invoke-static {v1, v2}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Lcpv;)Ljava/util/List;

    move-result-object v1

    .line 153
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/a;

    .line 154
    iget-object v2, v1, Lcom/twitter/model/dms/a;->f:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/dms/l;

    .line 155
    new-instance v6, Lcom/twitter/library/database/dm/ShareHistoryTable$a;

    invoke-direct {v6}, Lcom/twitter/library/database/dm/ShareHistoryTable$a;-><init>()V

    .line 156
    iget-wide v8, v1, Lcom/twitter/model/dms/a;->e:J

    invoke-virtual {v6, v8, v9}, Lcom/twitter/library/database/dm/ShareHistoryTable$a;->a(J)Lcom/twitter/library/database/dm/ShareHistoryTable$a;

    .line 158
    iget v3, v2, Lcom/twitter/model/dms/l;->b:I

    if-ne v3, v12, :cond_3

    .line 159
    invoke-virtual {v6, v12}, Lcom/twitter/library/database/dm/ShareHistoryTable$a;->a(Z)Lcom/twitter/library/database/dm/ShareHistoryTable$a;

    .line 160
    iget-object v2, v2, Lcom/twitter/model/dms/l;->a:Ljava/lang/String;

    invoke-virtual {v6, v2}, Lcom/twitter/library/database/dm/ShareHistoryTable$a;->a(Ljava/lang/String;)Lcom/twitter/library/database/dm/ShareHistoryTable$a;

    .line 171
    :cond_2
    :goto_2
    invoke-virtual {v1}, Lcom/twitter/model/dms/a;->A()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 172
    invoke-virtual {v1}, Lcom/twitter/model/dms/a;->u()Lcbx;

    move-result-object v1

    check-cast v1, Lcci;

    iget-wide v2, v1, Lcci;->c:J

    invoke-virtual {v6, v2, v3}, Lcom/twitter/library/database/dm/ShareHistoryTable$a;->b(J)Lcom/twitter/library/database/dm/ShareHistoryTable$a;

    .line 173
    sget-object v1, Lcom/twitter/library/database/dm/ShareHistoryTable$Type;->a:Lcom/twitter/library/database/dm/ShareHistoryTable$Type;

    invoke-virtual {v6, v1}, Lcom/twitter/library/database/dm/ShareHistoryTable$a;->a(Lcom/twitter/library/database/dm/ShareHistoryTable$Type;)Lcom/twitter/library/database/dm/ShareHistoryTable$a;

    .line 178
    :goto_3
    const-string/jumbo v1, "share_history"

    const/4 v2, 0x0

    invoke-virtual {v6}, Lcom/twitter/library/database/dm/ShareHistoryTable$a;->a()Landroid/content/ContentValues;

    move-result-object v3

    invoke-virtual {v4, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_1

    .line 162
    :cond_3
    const/4 v3, 0x0

    invoke-virtual {v6, v3}, Lcom/twitter/library/database/dm/ShareHistoryTable$a;->a(Z)Lcom/twitter/library/database/dm/ShareHistoryTable$a;

    .line 163
    iget-object v3, v2, Lcom/twitter/model/dms/l;->d:Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/model/dms/Participant;

    .line 164
    iget-object v8, v2, Lcom/twitter/model/dms/l;->d:Ljava/util/Collection;

    invoke-interface {v8}, Ljava/util/Collection;->size()I

    move-result v8

    if-eq v8, v12, :cond_5

    iget-wide v8, v3, Lcom/twitter/model/dms/Participant;->b:J

    iget-wide v10, p0, Lcom/twitter/library/database/dm/a;->a:J

    cmp-long v8, v8, v10

    if-eqz v8, :cond_4

    .line 165
    :cond_5
    iget-wide v2, v3, Lcom/twitter/model/dms/Participant;->b:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Lcom/twitter/library/database/dm/ShareHistoryTable$a;->a(Ljava/lang/String;)Lcom/twitter/library/database/dm/ShareHistoryTable$a;

    goto :goto_2

    .line 175
    :cond_6
    sget-object v1, Lcom/twitter/library/database/dm/ShareHistoryTable$Type;->c:Lcom/twitter/library/database/dm/ShareHistoryTable$Type;

    invoke-virtual {v6, v1}, Lcom/twitter/library/database/dm/ShareHistoryTable$a;->a(Lcom/twitter/library/database/dm/ShareHistoryTable$Type;)Lcom/twitter/library/database/dm/ShareHistoryTable$a;

    goto :goto_3
.end method

.method public c(J)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/database/dm/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/twitter/library/database/dm/a;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 104
    const-string/jumbo v1, "share_history"

    sget-object v2, Lcom/twitter/library/database/dm/ShareHistoryTable$b;->a:[Ljava/lang/String;

    const-string/jumbo v3, "tweet_id=?"

    .line 107
    invoke-static {p1, p2}, Lcom/twitter/library/database/dm/ShareHistoryTable$c;->a(J)[Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "reference_id,type"

    const/4 v6, 0x0

    const-string/jumbo v7, "type ASC, MAX(event_id) DESC"

    .line 104
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 112
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 114
    if-eqz v1, :cond_1

    .line 116
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 117
    invoke-static {v1}, Lcom/twitter/library/database/dm/ShareHistoryTable$b;->d(Landroid/database/Cursor;)Lcom/twitter/library/database/dm/c;

    move-result-object v2

    .line 118
    iget-object v3, v2, Lcom/twitter/library/database/dm/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 121
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 125
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 65
    invoke-static {}, Lcom/twitter/library/database/dm/ShareHistoryTable;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 66
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method
