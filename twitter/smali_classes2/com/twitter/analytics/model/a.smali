.class public Lcom/twitter/analytics/model/a;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/twitter/analytics/model/a;->a:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/twitter/analytics/model/a;->b:Ljava/lang/String;

    .line 33
    iput-object p3, p0, Lcom/twitter/analytics/model/a;->c:Ljava/lang/String;

    .line 34
    iput-object p4, p0, Lcom/twitter/analytics/model/a;->d:Ljava/lang/String;

    .line 35
    iput-object p5, p0, Lcom/twitter/analytics/model/a;->e:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public static a(Lcom/twitter/analytics/model/c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/a;
    .locals 6

    .prologue
    .line 56
    new-instance v0, Lcom/twitter/analytics/model/a;

    iget-object v1, p0, Lcom/twitter/analytics/model/c;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/analytics/model/c;->d:Ljava/lang/String;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/analytics/model/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/model/a;
    .locals 6

    .prologue
    .line 42
    new-instance v0, Lcom/twitter/analytics/model/a;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/analytics/model/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 76
    const-string/jumbo v0, ":"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/analytics/model/a;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/analytics/model/a;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/twitter/analytics/model/a;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/twitter/analytics/model/a;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/twitter/analytics/model/a;->e:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
