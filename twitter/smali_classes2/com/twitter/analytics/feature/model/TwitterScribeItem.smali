.class public Lcom/twitter/analytics/feature/model/TwitterScribeItem;
.super Lcom/twitter/analytics/model/ScribeItem;
.source "Twttr"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public A:Ljava/lang/String;

.field public B:Ljava/lang/String;

.field public C:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public D:J

.field public E:J

.field public F:Ljava/lang/String;

.field public G:J

.field public H:Ljava/lang/String;

.field public I:I

.field public J:Ljava/lang/String;

.field public K:Ljava/lang/String;

.field public L:Ljava/lang/String;

.field public M:Ljava/lang/String;

.field public N:J

.field public O:Ljava/lang/Boolean;

.field public P:Ljava/lang/String;

.field public Q:J

.field public R:Ljava/lang/String;

.field public S:Ljava/lang/String;

.field public T:Ljava/lang/String;

.field public U:Ljava/lang/String;

.field public V:Ljava/lang/String;

.field public W:Ljava/lang/String;

.field public X:Ljava/lang/String;

.field public Y:Ljava/lang/String;

.field public Z:Ljava/lang/String;

.field public a:J

.field public aA:Ljava/lang/Boolean;

.field public aB:J

.field public aC:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public aa:Ljava/lang/String;

.field public ab:J

.field public ac:I

.field public ad:J

.field public ae:J

.field public af:J

.field public ag:I

.field public ah:J

.field public ai:I

.field public aj:Ljava/lang/String;

.field public ak:J

.field public al:Ljava/lang/String;

.field public am:Lcom/twitter/analytics/feature/model/e;

.field public an:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

.field public ao:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

.field public ap:Lcom/twitter/analytics/feature/model/b;

.field public aq:Lcom/twitter/analytics/feature/model/a;

.field public ar:Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;

.field public as:Lcom/twitter/analytics/feature/model/f;

.field public at:Lcom/twitter/analytics/feature/model/d;

.field public au:Lcom/twitter/analytics/feature/model/c;

.field public av:Lcom/twitter/model/timeline/r;

.field public aw:Lcom/twitter/analytics/feature/model/NativeCardEvent;

.field public ax:I

.field public ay:Ljava/lang/String;

.field public az:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:J

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:I

.field public h:I

.field public i:I

.field public j:Z

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:I

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Z

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;

.field public w:Ljava/lang/String;

.field public x:Ljava/lang/String;

.field public y:I

.field public z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem$1;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem$1;-><init>()V

    sput-object v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, -0x1

    const-wide/16 v2, -0x1

    .line 187
    invoke-direct {p0}, Lcom/twitter/analytics/model/ScribeItem;-><init>()V

    .line 94
    iput-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 96
    iput v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 97
    iput-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->d:J

    .line 100
    iput v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 101
    iput v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->h:I

    .line 102
    iput v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->i:I

    .line 106
    iput v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->m:I

    .line 118
    iput v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->y:I

    .line 122
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->C:Ljava/util/Map;

    .line 123
    iput-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->D:J

    .line 124
    iput-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->E:J

    .line 130
    iput-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->G:J

    .line 132
    iput v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->I:I

    .line 137
    iput-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->N:J

    .line 140
    iput-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->Q:J

    .line 151
    iput-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ab:J

    .line 152
    iput v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ac:I

    .line 153
    iput-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ad:J

    .line 154
    iput-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ae:J

    .line 155
    iput-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->af:J

    .line 156
    iput v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ag:I

    .line 157
    iput-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ah:J

    .line 160
    iput v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ai:I

    .line 162
    iput-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ak:J

    .line 178
    iput v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ax:I

    .line 182
    iput-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aB:J

    .line 185
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aC:Ljava/util/List;

    .line 188
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, -0x1

    const-wide/16 v4, -0x1

    .line 191
    invoke-direct {p0, p1}, Lcom/twitter/analytics/model/ScribeItem;-><init>(Landroid/os/Parcel;)V

    .line 94
    iput-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 96
    iput v3, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 97
    iput-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->d:J

    .line 100
    iput v3, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 101
    iput v3, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->h:I

    .line 102
    iput v3, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->i:I

    .line 106
    iput v3, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->m:I

    .line 118
    iput v3, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->y:I

    .line 122
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->C:Ljava/util/Map;

    .line 123
    iput-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->D:J

    .line 124
    iput-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->E:J

    .line 130
    iput-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->G:J

    .line 132
    iput v3, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->I:I

    .line 137
    iput-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->N:J

    .line 140
    iput-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->Q:J

    .line 151
    iput-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ab:J

    .line 152
    iput v3, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ac:I

    .line 153
    iput-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ad:J

    .line 154
    iput-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ae:J

    .line 155
    iput-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->af:J

    .line 156
    iput v3, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ag:I

    .line 157
    iput-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ah:J

    .line 160
    iput v3, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ai:I

    .line 162
    iput-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ak:J

    .line 178
    iput v3, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ax:I

    .line 182
    iput-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aB:J

    .line 185
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aC:Ljava/util/List;

    .line 192
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 193
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 194
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 195
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->d:J

    .line 196
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->e:Ljava/lang/String;

    .line 197
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->f:Ljava/lang/String;

    .line 198
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 199
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->h:I

    .line 200
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->i:I

    .line 201
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->j:Z

    .line 202
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->k:Ljava/lang/String;

    .line 203
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->l:Ljava/lang/String;

    .line 204
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->m:I

    .line 205
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->n:Ljava/lang/String;

    .line 206
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->o:Ljava/lang/String;

    .line 207
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->p:Ljava/lang/String;

    .line 208
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->q:Ljava/lang/String;

    .line 209
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->r:Z

    .line 210
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->s:Ljava/lang/String;

    .line 211
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 212
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->w:Ljava/lang/String;

    .line 213
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->x:Ljava/lang/String;

    .line 214
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->y:I

    .line 215
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->A:Ljava/lang/String;

    .line 216
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->B:Ljava/lang/String;

    .line 217
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->z:Ljava/lang/String;

    .line 218
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 219
    :goto_2
    if-ge v2, v0, :cond_2

    .line 220
    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->C:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_0
    move v0, v2

    .line 201
    goto :goto_0

    :cond_1
    move v1, v2

    .line 209
    goto :goto_1

    .line 222
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->D:J

    .line 223
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->E:J

    .line 224
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->F:Ljava/lang/String;

    .line 225
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->H:Ljava/lang/String;

    .line 226
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->G:J

    .line 227
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->I:I

    .line 228
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->K:Ljava/lang/String;

    .line 229
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->J:Ljava/lang/String;

    .line 230
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->L:Ljava/lang/String;

    .line 231
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->M:Ljava/lang/String;

    .line 232
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->N:J

    .line 233
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_3

    .line 234
    new-instance v0, Lcom/twitter/analytics/feature/model/e;

    invoke-direct {v0, p1}, Lcom/twitter/analytics/feature/model/e;-><init>(Landroid/os/Parcel;)V

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->am:Lcom/twitter/analytics/feature/model/e;

    .line 236
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    invoke-static {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a(B)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->O:Ljava/lang/Boolean;

    .line 237
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->Q:J

    .line 238
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->P:Ljava/lang/String;

    .line 239
    const-class v0, Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->an:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    .line 240
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->R:Ljava/lang/String;

    .line 241
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->S:Ljava/lang/String;

    .line 242
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->T:Ljava/lang/String;

    .line 243
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->U:Ljava/lang/String;

    .line 244
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->V:Ljava/lang/String;

    .line 245
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->W:Ljava/lang/String;

    .line 246
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->X:Ljava/lang/String;

    .line 247
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->Y:Ljava/lang/String;

    .line 248
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->Z:Ljava/lang/String;

    .line 249
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aa:Ljava/lang/String;

    .line 250
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ab:J

    .line 251
    const-class v0, Lcom/twitter/analytics/feature/model/NativeCardEvent;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/NativeCardEvent;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aw:Lcom/twitter/analytics/feature/model/NativeCardEvent;

    .line 252
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ai:I

    .line 253
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aj:Ljava/lang/String;

    .line 254
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ak:J

    .line 255
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->al:Ljava/lang/String;

    .line 256
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ac:I

    .line 257
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ad:J

    .line 258
    const-class v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ao:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    .line 259
    const-class v0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ar:Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;

    .line 260
    sget-object v0, Lcom/twitter/model/timeline/r;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/r;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    .line 261
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->u:Ljava/lang/String;

    .line 262
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->t:Ljava/lang/String;

    .line 263
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ax:I

    .line 264
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ay:Ljava/lang/String;

    .line 265
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    invoke-static {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a(B)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aA:Ljava/lang/Boolean;

    .line 266
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->az:Ljava/lang/String;

    .line 267
    sget-object v0, Lcom/twitter/analytics/feature/model/b;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/b;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ap:Lcom/twitter/analytics/feature/model/b;

    .line 268
    sget-object v0, Lcom/twitter/analytics/feature/model/a;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/a;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aq:Lcom/twitter/analytics/feature/model/a;

    .line 269
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aC:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 270
    sget-object v0, Lcom/twitter/analytics/feature/model/f;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/f;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->as:Lcom/twitter/analytics/feature/model/f;

    .line 271
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ae:J

    .line 272
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->af:J

    .line 273
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ag:I

    .line 274
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ah:J

    .line 275
    sget-object v0, Lcom/twitter/analytics/feature/model/d;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/d;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->at:Lcom/twitter/analytics/feature/model/d;

    .line 276
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aB:J

    .line 277
    sget-object v0, Lcom/twitter/analytics/feature/model/c;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/c;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->au:Lcom/twitter/analytics/feature/model/c;

    .line 278
    return-void
.end method

.method private static a(Ljava/lang/Boolean;)B
    .locals 1

    .prologue
    .line 728
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private static a(B)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 711
    packed-switch p0, :pswitch_data_0

    .line 719
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 713
    :pswitch_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 716
    :pswitch_1
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 711
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 697
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->m:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 698
    iput p1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->m:I

    .line 699
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aw:Lcom/twitter/analytics/feature/model/NativeCardEvent;

    if-eqz v0, :cond_0

    .line 700
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aw:Lcom/twitter/analytics/feature/model/NativeCardEvent;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/NativeCardEvent;->a(I)V

    .line 703
    :cond_0
    return-void
.end method

.method public a(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v8, -0x1

    const-wide/16 v6, -0x1

    .line 420
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_0

    .line 421
    const-string/jumbo v0, "id"

    iget-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    invoke-virtual {p1, v0, v4, v5}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 423
    :cond_0
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 424
    const-string/jumbo v0, "name"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    :cond_1
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    if-eq v0, v8, :cond_2

    .line 427
    const-string/jumbo v0, "item_type"

    iget v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 429
    :cond_2
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 430
    const-string/jumbo v0, "promoted_id"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    :cond_3
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 433
    const-string/jumbo v0, "disclosure_type"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    :cond_4
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->d:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_5

    .line 436
    const-string/jumbo v0, "retweeting_tweet_id"

    iget-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->d:J

    invoke-virtual {p1, v0, v4, v5}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 438
    :cond_5
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    if-eq v0, v8, :cond_6

    .line 439
    const-string/jumbo v0, "position"

    iget v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 441
    :cond_6
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ax:I

    if-eq v0, v8, :cond_7

    .line 442
    const-string/jumbo v0, "user_index"

    iget v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ax:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 444
    :cond_7
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ay:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 445
    const-string/jumbo v0, "people_module_name"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ay:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    :cond_8
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->az:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 448
    const-string/jumbo v0, "people_module_id"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->az:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    :cond_9
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aA:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 451
    const-string/jumbo v0, "is_compact"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aA:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 453
    :cond_a
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->h:I

    if-eq v0, v8, :cond_b

    .line 454
    const-string/jumbo v0, "cursor"

    iget v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 456
    :cond_b
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->i:I

    if-eq v0, v8, :cond_c

    .line 457
    const-string/jumbo v0, "card_type"

    iget v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 458
    const-string/jumbo v0, "pre_expanded"

    iget-boolean v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->j:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 459
    const-string/jumbo v0, "forward_card_pre_expanded"

    iget-boolean v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->r:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 461
    :cond_c
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->k:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 462
    const-string/jumbo v0, "token"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    :cond_d
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->l:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 465
    const-string/jumbo v0, "card_name"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    :cond_e
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->m:I

    if-eq v0, v8, :cond_f

    .line 468
    const-string/jumbo v0, "publisher_app_install_status"

    iget v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->m:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 470
    :cond_f
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->n:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 471
    const-string/jumbo v0, "publisher_app_id"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    :cond_10
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->o:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 474
    const-string/jumbo v0, "card_platform_key"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    :cond_11
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->p:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 477
    const-string/jumbo v0, "audience_name"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->q:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 479
    const-string/jumbo v0, "audience_bucket"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    :cond_12
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->u:Ljava/lang/String;

    if-eqz v0, :cond_13

    .line 483
    const-string/jumbo v0, "preview_type"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->u:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    :cond_13
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->s:Ljava/lang/String;

    if-eqz v0, :cond_14

    .line 486
    const-string/jumbo v0, "card_url"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    :cond_14
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->t:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 489
    const-string/jumbo v0, "redirect_hop_details"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 490
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 491
    const-string/jumbo v0, "url"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 494
    :cond_15
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    if-eqz v0, :cond_16

    .line 495
    const-string/jumbo v0, "description"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    :cond_16
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->w:Ljava/lang/String;

    if-eqz v0, :cond_17

    .line 498
    const-string/jumbo v0, "item_query"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->w:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    :cond_17
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->x:Ljava/lang/String;

    if-eqz v0, :cond_18

    .line 501
    const-string/jumbo v0, "entity_type"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->x:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    :cond_18
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->y:I

    if-eq v0, v8, :cond_19

    .line 504
    const-string/jumbo v0, "tweet_count"

    iget v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->y:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 506
    :cond_19
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->B:Ljava/lang/String;

    if-eqz v0, :cond_1a

    .line 507
    const-string/jumbo v0, "story_source"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    :cond_1a
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->A:Ljava/lang/String;

    if-eqz v0, :cond_1b

    .line 510
    const-string/jumbo v0, "story_type"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->A:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    :cond_1b
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->z:Ljava/lang/String;

    if-eqz v0, :cond_1c

    .line 513
    const-string/jumbo v0, "impression_id"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->z:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    :cond_1c
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->C:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 516
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 518
    :cond_1d
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->D:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1e

    .line 519
    const-string/jumbo v0, "visibility_start"

    iget-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->D:J

    invoke-virtual {p1, v0, v4, v5}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 521
    :cond_1e
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->E:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1f

    .line 522
    const-string/jumbo v0, "visibility_end"

    iget-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->E:J

    invoke-virtual {p1, v0, v4, v5}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 524
    :cond_1f
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->F:Ljava/lang/String;

    if-eqz v0, :cond_20

    .line 525
    const-string/jumbo v0, "video_uuid"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->F:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    :cond_20
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->H:Ljava/lang/String;

    if-eqz v0, :cond_21

    .line 528
    const-string/jumbo v0, "video_type"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->H:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    :cond_21
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->G:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_22

    .line 531
    const-string/jumbo v0, "video_owner_id"

    iget-wide v4, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->G:J

    invoke-virtual {p1, v0, v4, v5}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 533
    :cond_22
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->I:I

    if-eq v0, v8, :cond_23

    .line 534
    const-string/jumbo v1, "video_is_muted"

    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->I:I

    if-ne v0, v2, :cond_4a

    move v0, v2

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 536
    :cond_23
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->K:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 537
    const-string/jumbo v0, "error_code"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->K:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    :cond_24
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->J:Ljava/lang/String;

    if-eqz v0, :cond_25

    .line 540
    const-string/jumbo v0, "error_message"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->J:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    :cond_25
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->L:Ljava/lang/String;

    if-eqz v0, :cond_26

    .line 543
    const-string/jumbo v0, "content_id"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->L:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    :cond_26
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->M:Ljava/lang/String;

    if-eqz v0, :cond_27

    .line 546
    const-string/jumbo v0, "playlist_url"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->M:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    :cond_27
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->N:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_28

    .line 549
    const-string/jumbo v0, "playback_lapse_ms"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->N:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 551
    :cond_28
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->O:Ljava/lang/Boolean;

    if-eqz v0, :cond_29

    .line 552
    const-string/jumbo v0, "is_replay"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->O:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 554
    :cond_29
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->P:Ljava/lang/String;

    if-eqz v0, :cond_2a

    .line 555
    const-string/jumbo v0, "connection_type"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->P:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    :cond_2a
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->Q:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_2b

    .line 558
    const-string/jumbo v0, "latency"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->Q:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 560
    :cond_2b
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->am:Lcom/twitter/analytics/feature/model/e;

    if-eqz v0, :cond_2c

    .line 561
    const-string/jumbo v0, "media_details"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 562
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->am:Lcom/twitter/analytics/feature/model/e;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/e;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 564
    :cond_2c
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->an:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    if-eqz v0, :cond_2d

    .line 565
    const-string/jumbo v0, "geo_details"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 566
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->an:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/ScribeGeoDetails;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 568
    :cond_2d
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ao:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    if-eqz v0, :cond_2e

    .line 569
    const-string/jumbo v0, "moments_details"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 570
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ao:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 572
    :cond_2e
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ar:Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;

    if-eqz v0, :cond_2f

    .line 573
    const-string/jumbo v0, "live_video_event_details"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 574
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ar:Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 576
    :cond_2f
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->R:Ljava/lang/String;

    if-eqz v0, :cond_30

    .line 577
    const-string/jumbo v0, "artist_name"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->R:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    :cond_30
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->S:Ljava/lang/String;

    if-eqz v0, :cond_31

    .line 580
    const-string/jumbo v0, "integration_partner"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->S:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    :cond_31
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->T:Ljava/lang/String;

    if-eqz v0, :cond_32

    .line 583
    const-string/jumbo v0, "card_title"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->T:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    :cond_32
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->U:Ljava/lang/String;

    if-eqz v0, :cond_33

    .line 586
    const-string/jumbo v0, "image_url"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->U:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    :cond_33
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->V:Ljava/lang/String;

    if-eqz v0, :cond_34

    .line 589
    const-string/jumbo v0, "artist_handle"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->V:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    :cond_34
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->W:Ljava/lang/String;

    if-eqz v0, :cond_35

    .line 592
    const-string/jumbo v0, "playlist_uuid"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->W:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    :cond_35
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->X:Ljava/lang/String;

    if-eqz v0, :cond_36

    .line 595
    const-string/jumbo v0, "track_uuid"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->X:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    :cond_36
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->Y:Ljava/lang/String;

    if-eqz v0, :cond_37

    .line 598
    const-string/jumbo v0, "cta_url"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->Y:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    :cond_37
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->Z:Ljava/lang/String;

    if-eqz v0, :cond_38

    .line 601
    const-string/jumbo v0, "play_store_id"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->Z:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    :cond_38
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aa:Ljava/lang/String;

    if-eqz v0, :cond_39

    .line 604
    const-string/jumbo v0, "play_store_name"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aa:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    :cond_39
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ab:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_3a

    .line 607
    const-string/jumbo v0, "publisher_id"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ab:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 609
    :cond_3a
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ai:I

    if-eq v0, v8, :cond_3b

    .line 610
    const-string/jumbo v0, "dynamic_preroll_type"

    iget v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ai:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 612
    :cond_3b
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aj:Ljava/lang/String;

    if-eqz v0, :cond_3c

    .line 613
    const-string/jumbo v0, "preroll_uuid"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aj:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    :cond_3c
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ak:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_3d

    .line 616
    const-string/jumbo v0, "preroll_owner_id"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ak:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 618
    :cond_3d
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->al:Ljava/lang/String;

    if-eqz v0, :cond_3e

    .line 619
    const-string/jumbo v0, "video_analytics_scribe_passthrough"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->al:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    :cond_3e
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ac:I

    if-eq v0, v8, :cond_3f

    .line 622
    const-string/jumbo v0, "player_mode"

    iget v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ac:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 624
    :cond_3f
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ad:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_40

    .line 625
    const-string/jumbo v0, "video_ad_skip_time_ms"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ad:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 627
    :cond_40
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    if-eqz v0, :cond_45

    .line 628
    const-string/jumbo v0, "suggestion_details"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 629
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 630
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    iget-object v0, v0, Lcom/twitter/model/timeline/r;->g:Ljava/lang/String;

    if-eqz v0, :cond_41

    .line 631
    const-string/jumbo v0, "type_id"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    iget-object v1, v1, Lcom/twitter/model/timeline/r;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    :cond_41
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    iget-object v0, v0, Lcom/twitter/model/timeline/r;->d:Ljava/lang/String;

    if-eqz v0, :cond_42

    .line 634
    const-string/jumbo v0, "source_data"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    iget-object v1, v1, Lcom/twitter/model/timeline/r;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    :cond_42
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    iget-object v0, v0, Lcom/twitter/model/timeline/r;->c:Ljava/lang/String;

    if-eqz v0, :cond_43

    .line 637
    const-string/jumbo v0, "controller_data"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    iget-object v1, v1, Lcom/twitter/model/timeline/r;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    :cond_43
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    iget-object v0, v0, Lcom/twitter/model/timeline/r;->b:Ljava/lang/String;

    if-eqz v0, :cond_44

    .line 640
    const-string/jumbo v0, "suggestion_type"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    iget-object v1, v1, Lcom/twitter/model/timeline/r;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    :cond_44
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 644
    :cond_45
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aB:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_46

    .line 645
    const-string/jumbo v0, "sort_index"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aB:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 647
    :cond_46
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ap:Lcom/twitter/analytics/feature/model/b;

    if-eqz v0, :cond_47

    .line 648
    const-string/jumbo v0, "slot_details"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 649
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 650
    const-string/jumbo v0, "id"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ap:Lcom/twitter/analytics/feature/model/b;

    iget-object v1, v1, Lcom/twitter/analytics/feature/model/b;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 653
    :cond_47
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aq:Lcom/twitter/analytics/feature/model/a;

    if-eqz v0, :cond_48

    .line 654
    const-string/jumbo v0, "ad_entity_details"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 655
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aq:Lcom/twitter/analytics/feature/model/a;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/a;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 657
    :cond_48
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->as:Lcom/twitter/analytics/feature/model/f;

    if-eqz v0, :cond_49

    .line 658
    const-string/jumbo v0, "sticker_group_details"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 659
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->as:Lcom/twitter/analytics/feature/model/f;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/f;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 661
    :cond_49
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aC:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_4c

    .line 662
    const-string/jumbo v0, "dedupe_ids"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 663
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a()V

    .line 664
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aC:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 665
    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->b(Ljava/lang/String;)V

    goto :goto_2

    .line 534
    :cond_4a
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 667
    :cond_4b
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->b()V

    .line 669
    :cond_4c
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aw:Lcom/twitter/analytics/feature/model/NativeCardEvent;

    if-eqz v0, :cond_4d

    .line 670
    const-string/jumbo v0, "card_event"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 671
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aw:Lcom/twitter/analytics/feature/model/NativeCardEvent;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/NativeCardEvent;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 673
    :cond_4d
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ae:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_4e

    .line 674
    const-string/jumbo v0, "start_program_date_time_millis"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ae:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 676
    :cond_4e
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->af:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_4f

    .line 677
    const-string/jumbo v0, "end_program_date_time_millis"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->af:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 679
    :cond_4f
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ag:I

    if-eq v0, v8, :cond_50

    .line 680
    const-string/jumbo v0, "sampled_bitrate"

    iget v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ag:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 682
    :cond_50
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ah:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_51

    .line 683
    const-string/jumbo v0, "buffering_duration_millis"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ah:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 685
    :cond_51
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->at:Lcom/twitter/analytics/feature/model/d;

    if-eqz v0, :cond_52

    .line 686
    const-string/jumbo v0, "player_layout_states"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 687
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->at:Lcom/twitter/analytics/feature/model/d;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/d;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 689
    :cond_52
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->au:Lcom/twitter/analytics/feature/model/c;

    if-eqz v0, :cond_53

    .line 690
    const-string/jumbo v0, "connect_details"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 691
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->au:Lcom/twitter/analytics/feature/model/c;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/c;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 693
    :cond_53
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;JZLjava/lang/Boolean;Ljava/lang/Long;)V
    .locals 3

    .prologue
    .line 290
    iput-object p1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->F:Ljava/lang/String;

    .line 291
    iput-object p2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->H:Ljava/lang/String;

    .line 292
    iput-wide p3, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->G:J

    .line 293
    if-eqz p5, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->I:I

    .line 294
    iput-object p6, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->O:Ljava/lang/Boolean;

    .line 295
    if-eqz p7, :cond_1

    invoke-virtual {p7}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_1
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->Q:J

    .line 296
    return-void

    .line 293
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 295
    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 305
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 306
    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 307
    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 309
    :cond_0
    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 311
    iget-object v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 313
    :cond_1
    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    iget v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    if-eq v1, v6, :cond_2

    .line 315
    iget v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 317
    :cond_2
    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    iget v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    if-eq v1, v6, :cond_3

    .line 319
    iget v1, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 321
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 326
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 327
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 328
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 329
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 330
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 331
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 332
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 333
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 334
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 335
    iget-boolean v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->j:Z

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 336
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 337
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 338
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->m:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 339
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 340
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 341
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 342
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 343
    iget-boolean v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->r:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 344
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 345
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 346
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->w:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 347
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->x:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 348
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->y:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 349
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->A:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 350
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->B:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 351
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->z:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 352
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->C:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 353
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->C:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 354
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 355
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2

    :cond_0
    move v0, v3

    .line 335
    goto/16 :goto_0

    :cond_1
    move v0, v3

    .line 343
    goto :goto_1

    .line 357
    :cond_2
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->D:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 358
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->E:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 359
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->F:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 360
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->H:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 361
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->G:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 362
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->I:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 363
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->K:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 364
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->J:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 365
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->L:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 366
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->M:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 367
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->N:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 368
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->am:Lcom/twitter/analytics/feature/model/e;

    if-eqz v0, :cond_3

    .line 369
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByte(B)V

    .line 370
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->am:Lcom/twitter/analytics/feature/model/e;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/e;->a(Landroid/os/Parcel;)V

    .line 374
    :goto_3
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->O:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a(Ljava/lang/Boolean;)B

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 375
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->Q:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 376
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->P:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 377
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->an:Lcom/twitter/analytics/feature/model/ScribeGeoDetails;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 378
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->R:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 379
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->S:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 380
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->T:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 381
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->U:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 382
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->V:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 383
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->W:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 384
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->X:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 385
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->Y:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 386
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->Z:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 387
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aa:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 388
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ab:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 389
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aw:Lcom/twitter/analytics/feature/model/NativeCardEvent;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 390
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ai:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 391
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aj:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 392
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ak:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 393
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->al:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 394
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ac:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 395
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ad:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 396
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ao:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 397
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ar:Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 398
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    sget-object v1, Lcom/twitter/model/timeline/r;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 399
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->u:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 400
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 401
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ax:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 402
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ay:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 403
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aA:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a(Ljava/lang/Boolean;)B

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 404
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->az:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 405
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ap:Lcom/twitter/analytics/feature/model/b;

    sget-object v1, Lcom/twitter/analytics/feature/model/b;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 406
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aq:Lcom/twitter/analytics/feature/model/a;

    sget-object v1, Lcom/twitter/analytics/feature/model/a;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 407
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aC:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 408
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->as:Lcom/twitter/analytics/feature/model/f;

    sget-object v1, Lcom/twitter/analytics/feature/model/f;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 409
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ae:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 410
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->af:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 411
    iget v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ag:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 412
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->ah:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 413
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->at:Lcom/twitter/analytics/feature/model/d;

    sget-object v1, Lcom/twitter/analytics/feature/model/d;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 414
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->aB:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 415
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->au:Lcom/twitter/analytics/feature/model/c;

    sget-object v1, Lcom/twitter/analytics/feature/model/c;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 416
    return-void

    .line 372
    :cond_3
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeByte(B)V

    goto/16 :goto_3
.end method
