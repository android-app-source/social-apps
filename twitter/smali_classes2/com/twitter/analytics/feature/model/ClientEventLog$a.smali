.class public Lcom/twitter/analytics/feature/model/ClientEventLog$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/analytics/feature/model/ClientEventLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/twitter/analytics/feature/model/ClientEventLog;


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 357
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 358
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog$a;->a:Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 359
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;ILjava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog$a;
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog$a;->a:Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;ILjava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 371
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog$a;
    .locals 3

    .prologue
    .line 364
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog$a;->a:Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x2

    aput-object p3, v1, v2

    const/4 v2, 0x3

    aput-object p4, v1, v2

    const/4 v2, 0x4

    aput-object p5, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 365
    return-object p0
.end method

.method public a()Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/ClientEventLog$a;->a:Lcom/twitter/analytics/feature/model/ClientEventLog;

    return-object v0
.end method
