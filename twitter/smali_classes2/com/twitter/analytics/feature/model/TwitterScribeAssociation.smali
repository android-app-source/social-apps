.class public Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
.super Lcom/twitter/analytics/model/ScribeAssociation;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/analytics/model/ScribeAssociation",
        "<",
        "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation$1;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation$1;-><init>()V

    sput-object v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/twitter/analytics/model/ScribeAssociation;-><init>()V

    .line 47
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/twitter/analytics/model/ScribeAssociation;-><init>(Landroid/os/Parcel;)V

    .line 44
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation$1;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/twitter/analytics/model/ScribeAssociation;-><init>()V

    .line 55
    invoke-virtual {p1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    .line 57
    invoke-virtual {p1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->d()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    .line 58
    invoke-virtual {p1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    .line 59
    invoke-virtual {p1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    .line 60
    invoke-virtual {p1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->d(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    .line 61
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return v0
.end method
