.class public Lcom/twitter/analytics/feature/model/NativeCardUserAction;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/analytics/feature/model/NativeCardUserAction;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/twitter/analytics/feature/model/NativeCardUserAction$1;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/NativeCardUserAction$1;-><init>()V

    sput-object v0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIIIILjava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->c:I

    .line 61
    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->d:I

    .line 62
    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->e:I

    .line 63
    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->f:I

    .line 64
    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->g:I

    .line 65
    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->h:I

    .line 69
    iput p1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->c:I

    .line 70
    iput p2, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->d:I

    .line 71
    iput p3, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->e:I

    .line 72
    iput p4, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->f:I

    .line 73
    iput p5, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->g:I

    .line 74
    iput-object p6, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->a:Ljava/lang/String;

    .line 75
    iput-object p7, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->b:Ljava/lang/String;

    .line 76
    iput p8, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->h:I

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->c:I

    .line 61
    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->d:I

    .line 62
    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->e:I

    .line 63
    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->f:I

    .line 64
    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->g:I

    .line 65
    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->h:I

    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->c:I

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->d:I

    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->e:I

    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->f:I

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->g:I

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->a:Ljava/lang/String;

    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->b:Ljava/lang/String;

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->h:I

    .line 88
    return-void
.end method


# virtual methods
.method public a(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 109
    const-string/jumbo v0, "x_coord"

    iget v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 110
    const-string/jumbo v0, "y_coord"

    iget v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 111
    const-string/jumbo v0, "width"

    iget v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 112
    const-string/jumbo v0, "height"

    iget v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 113
    const-string/jumbo v0, "card_elements"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 114
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a()V

    .line 115
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 116
    const-string/jumbo v0, "element_type"

    iget v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 117
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 118
    const-string/jumbo v0, "element_name"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 121
    const-string/jumbo v0, "element_value"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    :cond_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 124
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->b()V

    .line 125
    const-string/jumbo v0, "action_type"

    iget v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 126
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 127
    return-void
.end method

.method public a(Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Z
    .locals 2

    .prologue
    .line 151
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    iget v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->h:I

    iget v1, p1, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->h:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->f:I

    iget v1, p1, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->f:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->e:I

    iget v1, p1, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->e:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->g:I

    iget v1, p1, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->g:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->c:I

    iget v1, p1, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->c:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->d:I

    iget v1, p1, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->d:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->a:Ljava/lang/String;

    .line 158
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->b:Ljava/lang/String;

    .line 159
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 151
    :goto_0
    return v0

    .line 159
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 146
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    instance-of v0, p1, Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    .line 147
    invoke-virtual {p0, p1}, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->a(Lcom/twitter/analytics/feature/model/NativeCardUserAction;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 146
    :goto_0
    return v0

    .line 147
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 164
    iget v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->c:I

    .line 165
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->d:I

    add-int/2addr v0, v1

    .line 166
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->e:I

    add-int/2addr v0, v1

    .line 167
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->f:I

    add-int/2addr v0, v1

    .line 168
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->g:I

    add-int/2addr v0, v1

    .line 169
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->h:I

    add-int/2addr v0, v1

    .line 170
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x27

    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "NativeCardUserAction{mXCoord="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mYCoord="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mCardWidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mCardHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mElementType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mElementName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mElementValue=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mActionType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 98
    iget v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 99
    iget v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 100
    iget v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 101
    iget v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 102
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 104
    iget v0, p0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 105
    return-void
.end method
