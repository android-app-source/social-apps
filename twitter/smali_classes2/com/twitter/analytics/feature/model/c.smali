.class public Lcom/twitter/analytics/feature/model/c;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/analytics/feature/model/c$b;,
        Lcom/twitter/analytics/feature/model/c$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lcom/twitter/analytics/feature/model/c;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/twitter/analytics/feature/model/c$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/analytics/feature/model/c$b;-><init>(Lcom/twitter/analytics/feature/model/c$1;)V

    sput-object v0, Lcom/twitter/analytics/feature/model/c;->a:Lcom/twitter/util/serialization/l;

    return-void
.end method

.method constructor <init>(Lcom/twitter/analytics/feature/model/c$a;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iget-object v0, p1, Lcom/twitter/analytics/feature/model/c$a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/c;->b:Ljava/lang/String;

    .line 26
    iget-object v0, p1, Lcom/twitter/analytics/feature/model/c$a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/c;->c:Ljava/lang/String;

    .line 27
    iget-object v0, p1, Lcom/twitter/analytics/feature/model/c$a;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/c;->d:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method public a(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 32
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/c;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 33
    const-string/jumbo v0, "people_module_id"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/c;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    :cond_0
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/c;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 36
    const-string/jumbo v0, "people_module_name"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/c;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    :cond_1
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/c;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 39
    const-string/jumbo v0, "people_module_token"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/c;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    :cond_2
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 42
    return-void
.end method
