.class public Lcom/twitter/analytics/feature/model/b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/analytics/feature/model/b$a;,
        Lcom/twitter/analytics/feature/model/b$b;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Lcom/twitter/analytics/feature/model/b;",
            "Lcom/twitter/analytics/feature/model/b$b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lcom/twitter/analytics/feature/model/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/analytics/feature/model/b$a;-><init>(Lcom/twitter/analytics/feature/model/b$1;)V

    sput-object v0, Lcom/twitter/analytics/feature/model/b;->a:Lcom/twitter/util/serialization/b;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/analytics/feature/model/b$b;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lcom/twitter/analytics/feature/model/b$b;->a(Lcom/twitter/analytics/feature/model/b$b;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/b;->b:Ljava/lang/String;

    .line 33
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/analytics/feature/model/b$b;Lcom/twitter/analytics/feature/model/b$1;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/twitter/analytics/feature/model/b;-><init>(Lcom/twitter/analytics/feature/model/b$b;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/analytics/feature/model/b;)Z
    .locals 2

    .prologue
    .line 42
    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/analytics/feature/model/b;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/analytics/feature/model/b;->b:Ljava/lang/String;

    .line 43
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 42
    :goto_0
    return v0

    .line 43
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 37
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/analytics/feature/model/b;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/twitter/analytics/feature/model/b;

    invoke-virtual {p0, p1}, Lcom/twitter/analytics/feature/model/b;->a(Lcom/twitter/analytics/feature/model/b;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/b;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->b(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
