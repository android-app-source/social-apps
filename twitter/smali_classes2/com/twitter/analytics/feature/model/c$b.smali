.class Lcom/twitter/analytics/feature/model/c$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/analytics/feature/model/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/analytics/feature/model/c;",
        "Lcom/twitter/analytics/feature/model/c$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/analytics/feature/model/c$1;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/twitter/analytics/feature/model/c$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/analytics/feature/model/c$a;
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/twitter/analytics/feature/model/c$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/c$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/analytics/feature/model/c$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 84
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/analytics/feature/model/c$a;->a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/c$a;

    move-result-object v0

    .line 85
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/c$a;->b(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/c$a;

    move-result-object v0

    .line 86
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/c$a;->c(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/c$a;

    .line 87
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 74
    check-cast p2, Lcom/twitter/analytics/feature/model/c$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/analytics/feature/model/c$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/analytics/feature/model/c$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/analytics/feature/model/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p2, Lcom/twitter/analytics/feature/model/c;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/analytics/feature/model/c;->c:Ljava/lang/String;

    .line 93
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/analytics/feature/model/c;->d:Ljava/lang/String;

    .line 94
    invoke-virtual {v0, v1}, Lcom/twitter/util/serialization/o;->b(Ljava/lang/String;)Lcom/twitter/util/serialization/o;

    .line 95
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    check-cast p2, Lcom/twitter/analytics/feature/model/c;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/analytics/feature/model/c$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/analytics/feature/model/c;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/twitter/analytics/feature/model/c$b;->a()Lcom/twitter/analytics/feature/model/c$a;

    move-result-object v0

    return-object v0
.end method
