.class public Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/analytics/feature/model/MomentScribeDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Metadata"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:J

.field public final c:I

.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 366
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$1;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$1;-><init>()V

    sput-object v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(IJII)V
    .locals 0

    .prologue
    .line 386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387
    iput p1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;->a:I

    .line 388
    iput-wide p2, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;->b:J

    .line 389
    iput p4, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;->c:I

    .line 390
    iput p5, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;->d:I

    .line 391
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 393
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;->a:I

    .line 396
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;->b:J

    .line 397
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;->c:I

    .line 398
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;->d:I

    .line 399
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/twitter/analytics/feature/model/MomentScribeDetails$1;)V
    .locals 0

    .prologue
    .line 365
    invoke-direct {p0, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;)V
    .locals 6

    .prologue
    .line 402
    iget v1, p1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;->a:I

    iget-wide v2, p1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;->b:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    iget-wide v2, p1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;->b:J

    :goto_0
    iget v4, p1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;->c:I

    iget v5, p1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;->d:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;-><init>(IJII)V

    .line 404
    return-void

    .line 402
    :cond_0
    const-wide/high16 v2, -0x8000000000000000L

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 420
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 421
    const-string/jumbo v0, "content_type"

    iget v1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 422
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;->b:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 423
    const-string/jumbo v0, "media_id"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 425
    :cond_0
    iget v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;->d:I

    if-lez v0, :cond_1

    .line 426
    const-string/jumbo v0, "page_index"

    iget v1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 427
    const-string/jumbo v0, "total_pages"

    iget v1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 429
    :cond_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 430
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 408
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 413
    iget v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 414
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 415
    iget v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 416
    iget v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 417
    return-void
.end method
