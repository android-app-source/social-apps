.class Lcom/twitter/analytics/feature/model/g$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/analytics/feature/model/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/analytics/feature/model/g;",
        "Lcom/twitter/analytics/feature/model/g$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/analytics/feature/model/g$1;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/twitter/analytics/feature/model/g$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/analytics/feature/model/g$a;
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/twitter/analytics/feature/model/g$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/g$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/analytics/feature/model/g$a;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/twitter/analytics/feature/model/g$a;->a(J)Lcom/twitter/analytics/feature/model/g$a;

    .line 97
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 80
    check-cast p2, Lcom/twitter/analytics/feature/model/g$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/analytics/feature/model/g$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/analytics/feature/model/g$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/analytics/feature/model/g;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    iget-wide v0, p2, Lcom/twitter/analytics/feature/model/g;->b:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    .line 91
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    check-cast p2, Lcom/twitter/analytics/feature/model/g;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/analytics/feature/model/g$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/analytics/feature/model/g;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/twitter/analytics/feature/model/g$b;->a()Lcom/twitter/analytics/feature/model/g$a;

    move-result-object v0

    return-object v0
.end method
