.class public Lcom/twitter/analytics/feature/model/d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/analytics/feature/model/d$b;,
        Lcom/twitter/analytics/feature/model/d$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/util/serialization/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/b",
            "<",
            "Lcom/twitter/analytics/feature/model/d;",
            "Lcom/twitter/analytics/feature/model/d$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/av/PlayerLayoutStates$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/twitter/analytics/feature/model/d$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/analytics/feature/model/d$b;-><init>(Lcom/twitter/analytics/feature/model/d$1;)V

    sput-object v0, Lcom/twitter/analytics/feature/model/d;->a:Lcom/twitter/util/serialization/b;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/analytics/feature/model/d$a;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iget-object v0, p1, Lcom/twitter/analytics/feature/model/d$a;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/ImmutableList;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/d;->b:Ljava/util/List;

    .line 27
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/analytics/feature/model/d$a;Lcom/twitter/analytics/feature/model/d$1;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/twitter/analytics/feature/model/d;-><init>(Lcom/twitter/analytics/feature/model/d$a;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a()V

    .line 31
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/d;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/av/PlayerLayoutStates$a;

    .line 32
    iget-object v2, v0, Lcom/twitter/model/av/PlayerLayoutStates$a;->b:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    if-eqz v2, :cond_0

    .line 33
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 34
    const-string/jumbo v2, "layout"

    iget-object v3, v0, Lcom/twitter/model/av/PlayerLayoutStates$a;->b:Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;

    iget v3, v3, Lcom/twitter/model/av/PlayerLayoutStates$LiveVideoPlayerLayout;->index:I

    invoke-virtual {p1, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 35
    const-string/jumbo v2, "time_millis"

    iget-wide v4, v0, Lcom/twitter/model/av/PlayerLayoutStates$a;->c:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 36
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    goto :goto_0

    .line 39
    :cond_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->b()V

    .line 40
    return-void
.end method
