.class public final Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/analytics/feature/model/MomentScribeDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/analytics/feature/model/MomentScribeDetails;",
        ">;"
    }
.end annotation


# instance fields
.field a:J

.field b:J

.field c:Ljava/lang/String;

.field d:Ljava/lang/Boolean;

.field e:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;

.field f:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;

.field g:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;

.field h:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Dismiss;

.field i:J

.field j:Lcom/twitter/model/moments/u;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/high16 v0, -0x8000000000000000L

    .line 515
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 516
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a:J

    .line 517
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b:J

    .line 524
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->i:J

    return-void
.end method

.method static synthetic a(Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 515
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 515
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->l:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;
    .locals 1

    .prologue
    .line 531
    iput-wide p1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a:J

    .line 532
    return-object p0
.end method

.method public a(Lcom/twitter/analytics/feature/model/MomentScribeDetails$Dismiss;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;
    .locals 0

    .prologue
    .line 579
    iput-object p1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->h:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Dismiss;

    .line 580
    return-object p0
.end method

.method public a(Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;
    .locals 0

    .prologue
    .line 573
    iput-object p1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->g:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;

    .line 574
    return-object p0
.end method

.method public a(Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;
    .locals 0

    .prologue
    .line 567
    iput-object p1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->f:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;

    .line 568
    return-object p0
.end method

.method public a(Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;
    .locals 0

    .prologue
    .line 561
    iput-object p1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->e:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;

    .line 562
    return-object p0
.end method

.method public a(Lcom/twitter/model/moments/MomentVisibilityMode;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;
    .locals 1

    .prologue
    .line 597
    invoke-virtual {p1}, Lcom/twitter/model/moments/MomentVisibilityMode;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->l:Ljava/lang/String;

    .line 598
    return-object p0
.end method

.method public a(Lcom/twitter/model/moments/u;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;
    .locals 0

    .prologue
    .line 591
    iput-object p1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->j:Lcom/twitter/model/moments/u;

    .line 592
    return-object p0
.end method

.method public a(Ljava/lang/Boolean;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;
    .locals 0

    .prologue
    .line 549
    iput-object p1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->d:Ljava/lang/Boolean;

    .line 550
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;
    .locals 0

    .prologue
    .line 543
    iput-object p1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->c:Ljava/lang/String;

    .line 544
    return-object p0
.end method

.method protected a()Lcom/twitter/analytics/feature/model/MomentScribeDetails;
    .locals 2

    .prologue
    .line 604
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails;-><init>(Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;Lcom/twitter/analytics/feature/model/MomentScribeDetails$1;)V

    return-object v0
.end method

.method public b(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;
    .locals 1

    .prologue
    .line 537
    iput-wide p1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b:J

    .line 538
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;
    .locals 0

    .prologue
    .line 555
    iput-object p1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->k:Ljava/lang/String;

    .line 556
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 515
    invoke-virtual {p0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a()Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;
    .locals 1

    .prologue
    .line 585
    iput-wide p1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->i:J

    .line 586
    return-object p0
.end method
