.class final Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/analytics/feature/model/WebsiteAssetsLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "WebsiteAsset"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final a:Ljava/lang/String;

.field final b:J

.field final c:J

.field final d:Ljava/lang/String;

.field final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    new-instance v0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset$1;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset$1;-><init>()V

    sput-object v0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->a:Ljava/lang/String;

    .line 143
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->b:J

    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->c:J

    .line 145
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->d:Ljava/lang/String;

    .line 146
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->e:Z

    .line 147
    return-void

    .line 146
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Ljava/lang/String;JJLjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    iput-object p1, p0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->a:Ljava/lang/String;

    .line 135
    iput-wide p2, p0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->b:J

    .line 136
    iput-wide p4, p0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->c:J

    .line 137
    iput-object p6, p0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->d:Ljava/lang/String;

    .line 138
    iput-boolean p7, p0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->e:Z

    .line 139
    return-void
.end method


# virtual methods
.method public a(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 151
    const-string/jumbo v0, "asset_type"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string/jumbo v0, "time_to_load"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 153
    const-string/jumbo v0, "asset_size"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 154
    const-string/jumbo v0, "asset_url"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string/jumbo v0, "asset_prefetched"

    iget-boolean v1, p0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 156
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 157
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 167
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 168
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 169
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 170
    iget-boolean v0, p0, Lcom/twitter/analytics/feature/model/WebsiteAssetsLog$WebsiteAsset;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 171
    return-void

    .line 170
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
