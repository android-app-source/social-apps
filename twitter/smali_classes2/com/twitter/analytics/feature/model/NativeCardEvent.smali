.class public Lcom/twitter/analytics/feature/model/NativeCardEvent;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/analytics/feature/model/NativeCardEvent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lcom/twitter/analytics/feature/model/NativeCardUserAction;

.field private c:Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;

.field private d:Ljava/lang/String;

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/twitter/analytics/feature/model/NativeCardEvent$1;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/NativeCardEvent$1;-><init>()V

    sput-object v0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->e:I

    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->a:Ljava/lang/String;

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    .line 58
    new-instance v0, Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    invoke-direct {v0, p1}, Lcom/twitter/analytics/feature/model/NativeCardUserAction;-><init>(Landroid/os/Parcel;)V

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->b:Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    .line 60
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    .line 61
    new-instance v0, Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;

    invoke-direct {v0, p1}, Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;-><init>(Landroid/os/Parcel;)V

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->c:Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;

    .line 63
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->d:Ljava/lang/String;

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->e:I

    .line 65
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->e:I

    .line 52
    iput-object p1, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->a:Ljava/lang/String;

    .line 53
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 119
    if-nez p1, :cond_1

    .line 132
    :cond_0
    :goto_0
    return-object v0

    .line 124
    :cond_1
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 125
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    .line 126
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-le v2, v3, :cond_0

    .line 130
    const/4 v2, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 131
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 72
    iput p1, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->e:I

    .line 73
    return-void
.end method

.method public a(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 84
    const/4 v0, 0x0

    .line 85
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 87
    iget-object v2, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->b:Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    if-eqz v2, :cond_0

    .line 88
    const-string/jumbo v0, "card_user_action"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->b:Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    move v0, v1

    .line 92
    :cond_0
    iget-object v2, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->c:Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;

    if-eqz v2, :cond_1

    .line 93
    const-string/jumbo v0, "card_user_data"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->c:Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    move v0, v1

    .line 97
    :cond_1
    iget-object v2, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->d:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 98
    const-string/jumbo v0, "network_provider"

    iget-object v2, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 101
    :cond_2
    iget v2, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->e:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_5

    .line 102
    const-string/jumbo v0, "publisher_app_install_status"

    iget v2, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->e:I

    invoke-virtual {p1, v0, v2}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 106
    :goto_0
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/analytics/feature/model/NativeCardEvent;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 107
    if-eqz v0, :cond_4

    .line 108
    if-eqz v1, :cond_3

    .line 109
    const-string/jumbo v1, ","

    invoke-virtual {p1, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c(Ljava/lang/String;)V

    .line 111
    :cond_3
    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->c(Ljava/lang/String;)V

    .line 114
    :cond_4
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 115
    return-void

    :cond_5
    move v1, v0

    goto :goto_0
.end method

.method public a(Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->b:Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    .line 77
    return-void
.end method

.method public a(Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->c:Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;

    .line 81
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 180
    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    .line 189
    :cond_0
    :goto_0
    return v0

    .line 181
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 183
    check-cast p1, Lcom/twitter/analytics/feature/model/NativeCardEvent;

    .line 185
    iget v1, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->e:I

    iget v2, p1, Lcom/twitter/analytics/feature/model/NativeCardEvent;->e:I

    if-ne v1, v2, :cond_0

    .line 186
    iget-object v1, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/analytics/feature/model/NativeCardEvent;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 187
    iget-object v1, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->b:Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    iget-object v2, p1, Lcom/twitter/analytics/feature/model/NativeCardEvent;->b:Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    invoke-static {v1, v2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    iget-object v1, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->c:Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;

    iget-object v2, p1, Lcom/twitter/analytics/feature/model/NativeCardEvent;->c:Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;

    invoke-static {v1, v2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 189
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->d:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/analytics/feature/model/NativeCardEvent;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 194
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->b:Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    iget-object v2, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->c:Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;

    iget-object v3, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->d:Ljava/lang/String;

    iget v4, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->e:I

    .line 195
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 194
    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 163
    const/4 v0, 0x0

    .line 165
    :try_start_0
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 166
    new-instance v1, Lcom/fasterxml/jackson/core/JsonFactory;

    invoke-direct {v1}, Lcom/fasterxml/jackson/core/JsonFactory;-><init>()V

    .line 167
    invoke-virtual {v1, v2}, Lcom/fasterxml/jackson/core/JsonFactory;->a(Ljava/io/Writer;)Lcom/fasterxml/jackson/core/JsonGenerator;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 168
    :try_start_1
    invoke-virtual {p0, v1}, Lcom/twitter/analytics/feature/model/NativeCardEvent;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 169
    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->flush()V

    .line 170
    invoke-virtual {v2}, Ljava/io/StringWriter;->getBuffer()Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 174
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 172
    :goto_0
    return-object v0

    .line 171
    :catch_0
    move-exception v1

    move-object v1, v0

    .line 172
    :goto_1
    :try_start_2
    const-string/jumbo v0, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 174
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_2
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 171
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 143
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->b:Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByte(B)V

    .line 146
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->b:Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/analytics/feature/model/NativeCardUserAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 150
    :goto_0
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->c:Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;

    if-eqz v0, :cond_1

    .line 151
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByte(B)V

    .line 152
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->c:Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/analytics/feature/model/ScribeKeyValuesHolder;->writeToParcel(Landroid/os/Parcel;I)V

    .line 156
    :goto_1
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 157
    iget v0, p0, Lcom/twitter/analytics/feature/model/NativeCardEvent;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 158
    return-void

    .line 148
    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    goto :goto_0

    .line 154
    :cond_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    goto :goto_1
.end method
