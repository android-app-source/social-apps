.class public Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$1;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$1;-><init>()V

    sput-object v0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;->a:J

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;->c:J

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;->b:Ljava/lang/String;

    .line 67
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$1;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;)V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iget-wide v0, p1, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;->a:J

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;->a:J

    .line 59
    iget-wide v0, p1, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;->b:J

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;->c:J

    .line 60
    iget-object v0, p1, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;->b:Ljava/lang/String;

    .line 61
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$1;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;-><init>(Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails$a;)V

    return-void
.end method

.method public static a(J)Z
    .locals 2

    .prologue
    .line 96
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 83
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;->a:J

    invoke-static {v0, v1}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    const-string/jumbo v0, "id"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 86
    :cond_0
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;->c:J

    invoke-static {v0, v1}, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    const-string/jumbo v0, "card_event_id"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 90
    const-string/jumbo v0, "timeline_id"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    :cond_2
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 93
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 76
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 77
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 78
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/LiveVideoEventScribeDetails;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 79
    return-void
.end method
