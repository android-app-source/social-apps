.class public final Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;",
        ">;"
    }
.end annotation


# instance fields
.field a:J

.field b:J

.field c:J

.field d:Z

.field e:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/high16 v0, -0x8000000000000000L

    .line 274
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 275
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->a:J

    .line 276
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->b:J

    .line 277
    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->c:J

    return-void
.end method


# virtual methods
.method public a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;
    .locals 1

    .prologue
    .line 283
    iput-wide p1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->a:J

    .line 284
    return-object p0
.end method

.method public a(Z)Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;
    .locals 0

    .prologue
    .line 301
    iput-boolean p1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->d:Z

    .line 302
    return-object p0
.end method

.method protected a()Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;
    .locals 2

    .prologue
    .line 314
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;-><init>(Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;Lcom/twitter/analytics/feature/model/MomentScribeDetails$1;)V

    return-object v0
.end method

.method public b(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;
    .locals 1

    .prologue
    .line 289
    iput-wide p1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->b:J

    .line 290
    return-object p0
.end method

.method public b(Z)Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;
    .locals 0

    .prologue
    .line 307
    iput-boolean p1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->e:Z

    .line 308
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->a()Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;
    .locals 1

    .prologue
    .line 295
    iput-wide p1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->c:J

    .line 296
    return-object p0
.end method
