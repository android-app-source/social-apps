.class public Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/analytics/feature/model/MomentScribeDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Engagement"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 472
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement$1;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement$1;-><init>()V

    sput-object v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490
    iput p1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;->a:I

    .line 491
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 493
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 495
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;->a:I

    .line 496
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/twitter/analytics/feature/model/MomentScribeDetails$1;)V
    .locals 0

    .prologue
    .line 470
    invoke-direct {p0, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 509
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 510
    const-string/jumbo v0, "user_action"

    iget v1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    .line 511
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 512
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 500
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 505
    iget v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 506
    return-void
.end method
