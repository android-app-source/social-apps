.class Lcom/twitter/analytics/feature/model/f$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/analytics/feature/model/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/analytics/feature/model/f;",
        "Lcom/twitter/analytics/feature/model/f$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/analytics/feature/model/f$1;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/twitter/analytics/feature/model/f$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/analytics/feature/model/f$a;
    .locals 1

    .prologue
    .line 87
    new-instance v0, Lcom/twitter/analytics/feature/model/f$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/f$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/analytics/feature/model/f$a;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 99
    sget-object v0, Lcom/twitter/analytics/feature/model/g;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/analytics/feature/model/f$a;->a(Ljava/util/List;)Lcom/twitter/analytics/feature/model/f$a;

    .line 101
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 82
    check-cast p2, Lcom/twitter/analytics/feature/model/f$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/analytics/feature/model/f$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/analytics/feature/model/f$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/analytics/feature/model/f;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p2, Lcom/twitter/analytics/feature/model/f;->b:Ljava/util/List;

    sget-object v1, Lcom/twitter/analytics/feature/model/g;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 94
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    check-cast p2, Lcom/twitter/analytics/feature/model/f;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/analytics/feature/model/f$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/analytics/feature/model/f;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/twitter/analytics/feature/model/f$b;->a()Lcom/twitter/analytics/feature/model/f$a;

    move-result-object v0

    return-object v0
.end method
