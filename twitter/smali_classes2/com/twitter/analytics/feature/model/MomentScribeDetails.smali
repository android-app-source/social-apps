.class public Lcom/twitter/analytics/feature/model/MomentScribeDetails;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;,
        Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;,
        Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;,
        Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;,
        Lcom/twitter/analytics/feature/model/MomentScribeDetails$Dismiss;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/analytics/feature/model/MomentScribeDetails;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/Boolean;

.field public final e:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;

.field public final f:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;

.field public final g:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;

.field public final h:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Dismiss;

.field public final i:J

.field public final j:Lcom/twitter/model/moments/u;

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$1;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$1;-><init>()V

    sput-object v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->a:J

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->b:J

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->c:Ljava/lang/String;

    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->d:Ljava/lang/Boolean;

    .line 78
    const-class v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->e:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;

    .line 79
    const-class v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->f:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;

    .line 80
    const-class v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->g:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;

    .line 81
    const-class v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Dismiss;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Dismiss;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->h:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Dismiss;

    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->i:J

    .line 83
    sget-object v0, Lcom/twitter/model/moments/u;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/u;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->j:Lcom/twitter/model/moments/u;

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->k:Ljava/lang/String;

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->l:Ljava/lang/String;

    .line 86
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/twitter/analytics/feature/model/MomentScribeDetails$1;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;)V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iget-wide v0, p1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a:J

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->a:J

    .line 60
    iget-wide v0, p1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b:J

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->b:J

    .line 61
    iget-object v0, p1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->c:Ljava/lang/String;

    .line 62
    iget-object v0, p1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->d:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->d:Ljava/lang/Boolean;

    .line 63
    iget-object v0, p1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->e:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->e:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;

    .line 64
    iget-object v0, p1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->f:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->f:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;

    .line 65
    iget-object v0, p1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->g:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->g:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;

    .line 66
    iget-object v0, p1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->h:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Dismiss;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->h:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Dismiss;

    .line 67
    iget-wide v0, p1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->i:J

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->i:J

    .line 68
    iget-object v0, p1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->j:Lcom/twitter/model/moments/u;

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->j:Lcom/twitter/model/moments/u;

    .line 69
    invoke-static {p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->k:Ljava/lang/String;

    .line 70
    invoke-static {p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b(Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->l:Ljava/lang/String;

    .line 71
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;Lcom/twitter/analytics/feature/model/MomentScribeDetails$1;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails;-><init>(Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;)V

    return-void
.end method

.method public static a(J)Z
    .locals 2

    .prologue
    .line 609
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 111
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->g:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;

    if-eqz v0, :cond_0

    .line 112
    const-string/jumbo v0, "moment_engagement"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->g:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 116
    const-string/jumbo v0, "is_moment_followed"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 118
    :cond_1
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->f:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;

    if-eqz v0, :cond_2

    .line 119
    const-string/jumbo v0, "moment_metadata"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->f:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 122
    :cond_2
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->a:J

    invoke-static {v0, v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->a(J)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 123
    const-string/jumbo v0, "moment_id"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 125
    :cond_3
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->e:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;

    if-eqz v0, :cond_4

    .line 126
    const-string/jumbo v0, "moment_transition"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->e:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 129
    :cond_4
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->b:J

    invoke-static {v0, v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->a(J)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 130
    const-string/jumbo v0, "tweet_id"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 132
    :cond_5
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->c:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 133
    const-string/jumbo v0, "guide_category_id"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :cond_6
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->h:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Dismiss;

    if-eqz v0, :cond_7

    .line 136
    const-string/jumbo v0, "moment_dismiss"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->h:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Dismiss;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Dismiss;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 139
    :cond_7
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->i:J

    invoke-static {v0, v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->a(J)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 140
    const-string/jumbo v0, "impression_id"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->i:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 142
    :cond_8
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->j:Lcom/twitter/model/moments/u;

    if-eqz v0, :cond_9

    .line 143
    const-string/jumbo v0, "context_scribe_info"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->j:Lcom/twitter/model/moments/u;

    invoke-virtual {v0, p1}, Lcom/twitter/model/moments/u;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    .line 146
    :cond_9
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->k:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 147
    const-string/jumbo v0, "navigation_uri"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :cond_a
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->l:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 150
    const-string/jumbo v0, "visibility_mode"

    iget-object v1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    :cond_b
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 153
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 95
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 96
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 97
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->d:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 99
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->e:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 100
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->f:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 101
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->g:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 102
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->h:Lcom/twitter/analytics/feature/model/MomentScribeDetails$Dismiss;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 103
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->i:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 104
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->j:Lcom/twitter/model/moments/u;

    sget-object v1, Lcom/twitter/model/moments/u;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Parcel;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    .line 105
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 107
    return-void
.end method
