.class public Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/analytics/feature/model/MomentScribeDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Transition"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:J

.field public final c:J

.field public final d:Z

.field public final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 207
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$1;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$1;-><init>()V

    sput-object v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->a:J

    .line 238
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->b:J

    .line 239
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->c:J

    .line 240
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->d:Z

    .line 241
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->e:Z

    .line 242
    return-void

    :cond_0
    move v0, v2

    .line 240
    goto :goto_0

    :cond_1
    move v1, v2

    .line 241
    goto :goto_1
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/twitter/analytics/feature/model/MomentScribeDetails$1;)V
    .locals 0

    .prologue
    .line 206
    invoke-direct {p0, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;)V
    .locals 2

    .prologue
    .line 228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229
    iget-wide v0, p1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->a:J

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->a:J

    .line 230
    iget-wide v0, p1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->b:J

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->b:J

    .line 231
    iget-wide v0, p1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->c:J

    iput-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->c:J

    .line 232
    iget-boolean v0, p1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->d:Z

    iput-boolean v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->d:Z

    .line 233
    iget-boolean v0, p1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->e:Z

    iput-boolean v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->e:Z

    .line 234
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;Lcom/twitter/analytics/feature/model/MomentScribeDetails$1;)V
    .locals 0

    .prologue
    .line 206
    invoke-direct {p0, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;-><init>(Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 259
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    .line 260
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->b:J

    invoke-static {v0, v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    const-string/jumbo v0, "curr_tweet_id"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 263
    :cond_0
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->c:J

    invoke-static {v0, v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264
    const-string/jumbo v0, "previous_moment_id"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 266
    :cond_1
    iget-wide v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->a:J

    invoke-static {v0, v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails;->a(J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 267
    const-string/jumbo v0, "prev_tweet_id"

    iget-wide v2, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 269
    :cond_2
    const-string/jumbo v0, "reached_capsule_end"

    iget-boolean v1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 270
    const-string/jumbo v0, "reached_capsule_start"

    iget-boolean v1, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    .line 271
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    .line 272
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 246
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 251
    iget-wide v4, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->a:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 252
    iget-wide v4, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->b:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 253
    iget-wide v4, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->c:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 254
    iget-boolean v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->d:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 255
    iget-boolean v0, p0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;->e:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 256
    return-void

    :cond_0
    move v0, v2

    .line 254
    goto :goto_0

    :cond_1
    move v1, v2

    .line 255
    goto :goto_1
.end method
