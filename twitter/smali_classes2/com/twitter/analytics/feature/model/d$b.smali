.class Lcom/twitter/analytics/feature/model/d$b;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/analytics/feature/model/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/analytics/feature/model/d;",
        "Lcom/twitter/analytics/feature/model/d$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/analytics/feature/model/d$1;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/twitter/analytics/feature/model/d$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/analytics/feature/model/d$a;
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/twitter/analytics/feature/model/d$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/d$a;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/analytics/feature/model/d$a;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 69
    sget-object v0, Lcom/twitter/model/av/PlayerLayoutStates$a;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/serialization/l;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/analytics/feature/model/d$a;->a(Ljava/util/List;)Lcom/twitter/analytics/feature/model/d$a;

    .line 71
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 58
    check-cast p2, Lcom/twitter/analytics/feature/model/d$a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/analytics/feature/model/d$b;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/analytics/feature/model/d$a;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/analytics/feature/model/d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p2, Lcom/twitter/analytics/feature/model/d;->b:Ljava/util/List;

    sget-object v1, Lcom/twitter/model/av/PlayerLayoutStates$a;->a:Lcom/twitter/util/serialization/b;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/o;Ljava/util/List;Lcom/twitter/util/serialization/l;)V

    .line 78
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    check-cast p2, Lcom/twitter/analytics/feature/model/d;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/analytics/feature/model/d$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/analytics/feature/model/d;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/twitter/analytics/feature/model/d$b;->a()Lcom/twitter/analytics/feature/model/d$a;

    move-result-object v0

    return-object v0
.end method
