.class Lcom/twitter/analytics/feature/model/a$a;
.super Lcom/twitter/util/serialization/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/analytics/feature/model/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/b",
        "<",
        "Lcom/twitter/analytics/feature/model/a;",
        "Lcom/twitter/analytics/feature/model/a$b;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/twitter/util/serialization/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/analytics/feature/model/a$1;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/twitter/analytics/feature/model/a$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/analytics/feature/model/a$b;
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lcom/twitter/analytics/feature/model/a$b;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/a$b;-><init>()V

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/analytics/feature/model/a$b;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 111
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/twitter/analytics/feature/model/a$b;->a(J)Lcom/twitter/analytics/feature/model/a$b;

    .line 112
    invoke-virtual {p1}, Lcom/twitter/util/serialization/n;->f()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/twitter/analytics/feature/model/a$b;->b(J)Lcom/twitter/analytics/feature/model/a$b;

    .line 113
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Lcom/twitter/util/object/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 94
    check-cast p2, Lcom/twitter/analytics/feature/model/a$b;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/analytics/feature/model/a$a;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/analytics/feature/model/a$b;I)V

    return-void
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/analytics/feature/model/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    iget-wide v0, p2, Lcom/twitter/analytics/feature/model/a;->b:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    .line 105
    iget-wide v0, p2, Lcom/twitter/analytics/feature/model/a;->c:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->b(J)Lcom/twitter/util/serialization/o;

    .line 106
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    check-cast p2, Lcom/twitter/analytics/feature/model/a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/analytics/feature/model/a$a;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/analytics/feature/model/a;)V

    return-void
.end method

.method protected synthetic b()Lcom/twitter/util/object/i;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/twitter/analytics/feature/model/a$a;->a()Lcom/twitter/analytics/feature/model/a$b;

    move-result-object v0

    return-object v0
.end method
