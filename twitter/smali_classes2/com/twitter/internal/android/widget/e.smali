.class Lcom/twitter/internal/android/widget/e;
.super Landroid/view/View;
.source "Twttr"


# static fields
.field private static final a:Landroid/text/TextPaint;

.field private static final b:[I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "PrivateResource"
        }
    .end annotation
.end field


# instance fields
.field private A:Landroid/graphics/drawable/Drawable;

.field private B:Landroid/animation/ValueAnimator;

.field private C:Landroid/animation/ValueAnimator;

.field private D:Z

.field private E:Z

.field private F:Z

.field private final G:Z

.field private final H:I

.field private I:Z

.field private J:Z

.field private K:Z

.field private L:Z

.field private M:Z

.field private N:Z

.field private O:Z

.field private P:Z

.field private Q:I

.field private R:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private f:F

.field private g:F

.field private final h:Landroid/graphics/Point;

.field private final i:Landroid/graphics/Point;

.field private j:Landroid/content/res/ColorStateList;

.field private k:Landroid/content/res/ColorStateList;

.field private l:Landroid/graphics/Typeface;

.field private m:Landroid/graphics/Typeface;

.field private n:Landroid/text/StaticLayout;

.field private o:Ljava/lang/CharSequence;

.field private p:Landroid/text/StaticLayout;

.field private q:Ljava/lang/CharSequence;

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:Landroid/graphics/drawable/Drawable;

.field private w:Landroid/graphics/drawable/Drawable;

.field private x:Ljava/lang/CharSequence;

.field private y:Ljava/lang/CharSequence;

.field private z:Lcmh;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 41
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    sput-object v0, Lcom/twitter/internal/android/widget/e;->a:Landroid/text/TextPaint;

    .line 42
    new-array v0, v1, [I

    const/4 v1, 0x0

    sget v2, Lazw$c;->state_collapsed:I

    aput v2, v0, v1

    sput-object v0, Lcom/twitter/internal/android/widget/e;->b:[I

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 103
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/e;->h:Landroid/graphics/Point;

    .line 54
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/e;->i:Landroid/graphics/Point;

    .line 104
    sget-object v0, Lazw$l;->ToolBarHomeView:[I

    invoke-virtual {p1, p2, v0, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 106
    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/e;->a(Landroid/content/res/TypedArray;)V

    .line 107
    sget v1, Lazw$l;->ToolBarHomeView_numberBackground:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/e;->d:I

    .line 108
    sget v1, Lazw$l;->ToolBarHomeView_numberColor:I

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/e;->e:I

    .line 109
    sget v1, Lazw$l;->ToolBarHomeView_toolBarIconSpacing:I

    const/high16 v2, 0x40a00000    # 5.0f

    .line 110
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    int-to-float v2, v2

    .line 109
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/twitter/internal/android/widget/e;->c:I

    .line 111
    iput-boolean v4, p0, Lcom/twitter/internal/android/widget/e;->D:Z

    .line 112
    iput-boolean v4, p0, Lcom/twitter/internal/android/widget/e;->E:Z

    .line 113
    iput-boolean v4, p0, Lcom/twitter/internal/android/widget/e;->F:Z

    .line 114
    sget v1, Lazw$l;->ToolBarHomeView_iconVisibleWithUp:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/e;->G:Z

    .line 115
    sget v1, Lazw$l;->ToolBarHomeView_titleEndPadding:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/e;->H:I

    .line 117
    sget v1, Lazw$l;->ToolBarHomeView_upIndicatorDescription:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/e;->y:Ljava/lang/CharSequence;

    .line 118
    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->y:Ljava/lang/CharSequence;

    if-nez v1, :cond_0

    .line 119
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/twitter/internal/android/widget/e;->y:Ljava/lang/CharSequence;

    .line 122
    :cond_0
    invoke-static {p0, v4}, Lcom/twitter/util/ui/a;->a(Landroid/view/View;I)V

    .line 124
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 125
    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;I)I
    .locals 4

    .prologue
    .line 203
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 204
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->getMeasuredHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/util/ui/k;->a(II)I

    move-result v1

    .line 205
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    .line 206
    add-int v3, p2, v2

    add-int/2addr v0, v1

    invoke-virtual {p1, p2, v1, v3, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 207
    invoke-virtual {p0, p1}, Lcom/twitter/internal/android/widget/e;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 208
    return v2
.end method

.method static synthetic a(Lcom/twitter/internal/android/widget/e;I)I
    .locals 0

    .prologue
    .line 40
    iput p1, p0, Lcom/twitter/internal/android/widget/e;->t:I

    return p1
.end method

.method private a(ILjava/lang/CharSequence;FLandroid/graphics/Typeface;)Landroid/text/StaticLayout;
    .locals 13

    .prologue
    .line 261
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/e;->f()I

    move-result v1

    sub-int v12, v1, p1

    .line 262
    sget-object v5, Lcom/twitter/internal/android/widget/e;->a:Landroid/text/TextPaint;

    .line 263
    move/from16 v0, p3

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 264
    move-object/from16 v0, p4

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 265
    new-instance v1, Landroid/text/StaticLayout;

    const/4 v3, 0x0

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v4

    .line 266
    invoke-static {p2, v5}, Lcom/twitter/util/ui/k;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v6

    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const/4 v10, 0x0

    sget-object v11, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object v2, p2

    invoke-direct/range {v1 .. v12}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    .line 265
    return-object v1
.end method

.method static synthetic a(Lcom/twitter/internal/android/widget/e;Landroid/text/StaticLayout;)Landroid/text/StaticLayout;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/internal/android/widget/e;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/twitter/internal/android/widget/e;->o:Ljava/lang/CharSequence;

    return-object p1
.end method

.method private a(II)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 296
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/e;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v0, v0, Landroid/graphics/drawable/RippleDrawable;

    if-eqz v0, :cond_0

    .line 298
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/RippleDrawable;

    .line 299
    iget v1, p0, Lcom/twitter/internal/android/widget/e;->c:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, p1

    invoke-virtual {v0, v2, v2, v1, p2}, Landroid/graphics/drawable/RippleDrawable;->setHotspotBounds(IIII)V

    .line 301
    :cond_0
    return-void
.end method

.method private a(Landroid/content/res/TypedArray;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v3, -0x1000000

    .line 128
    sget v0, Lazw$l;->ToolBarHomeView_textSize:I

    const/high16 v1, 0x41800000    # 16.0f

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/e;->f:F

    .line 129
    sget v0, Lazw$l;->ToolBarHomeView_subtitleTextSize:I

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/e;->g:F

    .line 130
    sget v0, Lazw$l;->ToolBarHomeView_textColor:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/e;->j:Landroid/content/res/ColorStateList;

    .line 131
    sget v0, Lazw$l;->ToolBarHomeView_subtitleTextColor:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/e;->k:Landroid/content/res/ColorStateList;

    .line 133
    sget v0, Lazw$l;->ToolBarHomeView_titleTextStyle:I

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 135
    sget v1, Lazw$l;->ToolBarHomeView_subtitleTextStyle:I

    invoke-virtual {p1, v1, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 137
    sget v2, Lazw$l;->ToolBarHomeView_allCaps:I

    invoke-virtual {p1, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/internal/android/widget/e;->J:Z

    .line 138
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->j:Landroid/content/res/ColorStateList;

    if-eqz v2, :cond_1

    .line 139
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/e;->d()V

    .line 144
    :goto_0
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->k:Landroid/content/res/ColorStateList;

    if-eqz v2, :cond_2

    .line 145
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/e;->e()V

    .line 150
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 151
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->isInEditMode()Z

    move-result v3

    if-nez v3, :cond_0

    .line 152
    invoke-static {v2}, Lcom/twitter/ui/widget/i;->a(Landroid/content/Context;)Lcom/twitter/ui/widget/i;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/twitter/ui/widget/i;->b(I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/e;->l:Landroid/graphics/Typeface;

    .line 153
    invoke-static {v2}, Lcom/twitter/ui/widget/i;->a(Landroid/content/Context;)Lcom/twitter/ui/widget/i;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/i;->b(I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/e;->m:Landroid/graphics/Typeface;

    .line 155
    :cond_0
    return-void

    .line 141
    :cond_1
    iput v3, p0, Lcom/twitter/internal/android/widget/e;->r:I

    .line 142
    iget v2, p0, Lcom/twitter/internal/android/widget/e;->r:I

    iput v2, p0, Lcom/twitter/internal/android/widget/e;->t:I

    goto :goto_0

    .line 147
    :cond_2
    iput v3, p0, Lcom/twitter/internal/android/widget/e;->s:I

    .line 148
    iget v2, p0, Lcom/twitter/internal/android/widget/e;->s:I

    iput v2, p0, Lcom/twitter/internal/android/widget/e;->u:I

    goto :goto_1
.end method

.method private a(ZI)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 404
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->B:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_1

    .line 405
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/e;->B:Landroid/animation/ValueAnimator;

    .line 406
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->B:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x15e

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 407
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->B:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/twitter/internal/android/widget/e$1;

    invoke-direct {v1, p0}, Lcom/twitter/internal/android/widget/e$1;-><init>(Lcom/twitter/internal/android/widget/e;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 433
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->B:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/twitter/internal/android/widget/e$2;

    invoke-direct {v1, p0}, Lcom/twitter/internal/android/widget/e$2;-><init>(Lcom/twitter/internal/android/widget/e;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 446
    :cond_0
    :goto_0
    iput-boolean v5, p0, Lcom/twitter/internal/android/widget/e;->L:Z

    .line 447
    if-eqz p1, :cond_2

    .line 448
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->B:Landroid/animation/ValueAnimator;

    new-array v1, v6, [I

    aput p2, v1, v4

    aput v4, v1, v5

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 452
    :goto_1
    iget v0, p0, Lcom/twitter/internal/android/widget/e;->r:I

    invoke-static {v0, p2}, Lcom/twitter/util/ui/g;->a(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/e;->t:I

    .line 453
    iget v0, p0, Lcom/twitter/internal/android/widget/e;->s:I

    invoke-static {v0, p2}, Lcom/twitter/util/ui/g;->a(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/e;->u:I

    .line 454
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/e;->K:Z

    .line 455
    return-void

    .line 442
    :cond_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->B:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->B:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    goto :goto_0

    .line 450
    :cond_2
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->B:Landroid/animation/ValueAnimator;

    new-array v1, v6, [I

    aput p2, v1, v4

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/e;->i()I

    move-result v2

    aput v2, v1, v5

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/internal/android/widget/e;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->K:Z

    return v0
.end method

.method static synthetic a(Lcom/twitter/internal/android/widget/e;Z)Z
    .locals 0

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/e;->L:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/internal/android/widget/e;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/twitter/internal/android/widget/e;->r:I

    return v0
.end method

.method static synthetic b(Lcom/twitter/internal/android/widget/e;I)I
    .locals 0

    .prologue
    .line 40
    iput p1, p0, Lcom/twitter/internal/android/widget/e;->u:I

    return p1
.end method

.method static synthetic b(Lcom/twitter/internal/android/widget/e;Landroid/text/StaticLayout;)Landroid/text/StaticLayout;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    return-object p1
.end method

.method static synthetic b(Lcom/twitter/internal/android/widget/e;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/twitter/internal/android/widget/e;->q:Ljava/lang/CharSequence;

    return-object p1
.end method

.method private b(II)V
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->C:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->C:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 499
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/twitter/internal/android/widget/e;->c(II)V

    .line 500
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->C:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 502
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/twitter/internal/android/widget/e;Z)Z
    .locals 0

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/e;->K:Z

    return p1
.end method

.method static synthetic c(Lcom/twitter/internal/android/widget/e;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/twitter/internal/android/widget/e;->s:I

    return v0
.end method

.method static synthetic c(Lcom/twitter/internal/android/widget/e;I)I
    .locals 0

    .prologue
    .line 40
    iput p1, p0, Lcom/twitter/internal/android/widget/e;->R:I

    return p1
.end method

.method private c(I)V
    .locals 4

    .prologue
    .line 246
    iget v0, p0, Lcom/twitter/internal/android/widget/e;->H:I

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->o:Ljava/lang/CharSequence;

    iget v2, p0, Lcom/twitter/internal/android/widget/e;->f:F

    iget-object v3, p0, Lcom/twitter/internal/android/widget/e;->l:Landroid/graphics/Typeface;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/twitter/internal/android/widget/e;->a(ILjava/lang/CharSequence;FLandroid/graphics/Typeface;)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    .line 248
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->M:Z

    if-eqz v0, :cond_0

    .line 249
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/e;->j()V

    .line 250
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->M:Z

    .line 252
    :cond_0
    return-void
.end method

.method private c(II)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 535
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->C:Landroid/animation/ValueAnimator;

    if-nez v2, :cond_1

    .line 536
    new-instance v2, Landroid/animation/ValueAnimator;

    invoke-direct {v2}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v2, p0, Lcom/twitter/internal/android/widget/e;->C:Landroid/animation/ValueAnimator;

    .line 537
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->C:Landroid/animation/ValueAnimator;

    const-wide/16 v4, 0xfa

    invoke-virtual {v2, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 538
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->C:Landroid/animation/ValueAnimator;

    new-instance v3, Lcom/twitter/internal/android/widget/e$3;

    invoke-direct {v3, p0}, Lcom/twitter/internal/android/widget/e$3;-><init>(Lcom/twitter/internal/android/widget/e;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 563
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->C:Landroid/animation/ValueAnimator;

    new-instance v3, Lcom/twitter/internal/android/widget/e$4;

    invoke-direct {v3, p0}, Lcom/twitter/internal/android/widget/e$4;-><init>(Lcom/twitter/internal/android/widget/e;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 574
    :cond_0
    :goto_0
    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->N:Z

    .line 575
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->C:Landroid/animation/ValueAnimator;

    const/4 v3, 0x2

    new-array v3, v3, [I

    aput p1, v3, v1

    aput p2, v3, v0

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 576
    if-le p2, p1, :cond_2

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->O:Z

    .line 577
    return-void

    .line 570
    :cond_1
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->C:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 571
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->C:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->cancel()V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 576
    goto :goto_1
.end method

.method static synthetic c(Lcom/twitter/internal/android/widget/e;Z)Z
    .locals 0

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/e;->N:Z

    return p1
.end method

.method private d()V
    .locals 3

    .prologue
    .line 185
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->j:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->getDrawableState()[I

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    .line 186
    iget v1, p0, Lcom/twitter/internal/android/widget/e;->r:I

    if-eq v0, v1, :cond_0

    .line 187
    iput v0, p0, Lcom/twitter/internal/android/widget/e;->r:I

    .line 188
    iput v0, p0, Lcom/twitter/internal/android/widget/e;->t:I

    .line 189
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->invalidate()V

    .line 191
    :cond_0
    return-void
.end method

.method private d(I)V
    .locals 3

    .prologue
    .line 255
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->q:Ljava/lang/CharSequence;

    iget v1, p0, Lcom/twitter/internal/android/widget/e;->g:F

    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->m:Landroid/graphics/Typeface;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/twitter/internal/android/widget/e;->a(ILjava/lang/CharSequence;FLandroid/graphics/Typeface;)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    .line 256
    return-void
.end method

.method static synthetic d(Lcom/twitter/internal/android/widget/e;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->O:Z

    return v0
.end method

.method static synthetic d(Lcom/twitter/internal/android/widget/e;Z)Z
    .locals 0

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/e;->O:Z

    return p1
.end method

.method private e()V
    .locals 3

    .prologue
    .line 194
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->k:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->getDrawableState()[I

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    .line 195
    iget v1, p0, Lcom/twitter/internal/android/widget/e;->s:I

    if-eq v0, v1, :cond_0

    .line 196
    iput v0, p0, Lcom/twitter/internal/android/widget/e;->s:I

    .line 197
    iput v0, p0, Lcom/twitter/internal/android/widget/e;->u:I

    .line 198
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->invalidate()V

    .line 200
    :cond_0
    return-void
.end method

.method private f()I
    .locals 2

    .prologue
    .line 271
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 272
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/e;->g()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->v:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/twitter/internal/android/widget/e;->c:I

    sub-int/2addr v0, v1

    .line 271
    return v0
.end method

.method private g()I
    .locals 2

    .prologue
    .line 276
    const/4 v0, 0x0

    .line 277
    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/e;->D:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->A:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 279
    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->A:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    add-int/2addr v0, v1

    .line 287
    :cond_0
    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/e;->F:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/e;->G:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/e;->D:Z

    if-nez v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->v:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    .line 288
    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->v:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    add-int/2addr v0, v1

    .line 291
    :cond_2
    return v0
.end method

.method private h()Z
    .locals 2

    .prologue
    .line 304
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()I
    .locals 1

    .prologue
    .line 458
    iget v0, p0, Lcom/twitter/internal/android/widget/e;->r:I

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    return v0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->B:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->B:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 491
    :cond_0
    return-void
.end method

.method private k()V
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->B:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 495
    return-void
.end method

.method private l()V
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->C:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 506
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->q:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 660
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    if-eqz v0, :cond_0

    .line 661
    invoke-direct {p0, p1}, Lcom/twitter/internal/android/widget/e;->c(I)V

    .line 663
    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    if-eqz v0, :cond_1

    .line 664
    invoke-direct {p0, p1}, Lcom/twitter/internal/android/widget/e;->d(I)V

    .line 666
    :cond_1
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 651
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->v:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    .line 652
    iput-object p1, p0, Lcom/twitter/internal/android/widget/e;->w:Landroid/graphics/drawable/Drawable;

    .line 653
    iput-object p1, p0, Lcom/twitter/internal/android/widget/e;->v:Landroid/graphics/drawable/Drawable;

    .line 654
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/internal/android/widget/e;->Q:I

    .line 655
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->requestLayout()V

    .line 657
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 509
    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/e;->L:Z

    if-eqz v2, :cond_0

    .line 510
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/e;->k()V

    .line 514
    :cond_0
    if-nez p1, :cond_3

    .line 515
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    if-eqz v2, :cond_2

    .line 516
    :goto_0
    iput-object v3, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    .line 517
    iput-object v3, p0, Lcom/twitter/internal/android/widget/e;->o:Ljava/lang/CharSequence;

    .line 529
    :goto_1
    if-eqz v0, :cond_1

    .line 530
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->requestLayout()V

    .line 532
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 515
    goto :goto_0

    .line 518
    :cond_3
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 519
    :cond_4
    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/e;->J:Z

    if-eqz v1, :cond_5

    .line 520
    invoke-static {p1}, Lcom/twitter/util/b;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/e;->o:Ljava/lang/CharSequence;

    .line 524
    :goto_2
    iput-object v3, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    goto :goto_1

    .line 522
    :cond_5
    iput-object p1, p0, Lcom/twitter/internal/android/widget/e;->o:Ljava/lang/CharSequence;

    goto :goto_2

    :cond_6
    move v0, v1

    .line 527
    goto :goto_1
.end method

.method public a(Ljava/lang/CharSequence;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 462
    if-nez p2, :cond_1

    .line 463
    invoke-virtual {p0, p1}, Lcom/twitter/internal/android/widget/e;->a(Ljava/lang/CharSequence;)V

    .line 485
    :cond_0
    :goto_0
    return-void

    .line 467
    :cond_1
    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    if-eqz v0, :cond_2

    .line 468
    iget v0, p0, Lcom/twitter/internal/android/widget/e;->t:I

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    invoke-direct {p0, v3, v0}, Lcom/twitter/internal/android/widget/e;->a(ZI)V

    .line 469
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->B:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 470
    :cond_2
    if-nez p1, :cond_3

    .line 471
    iput-object v2, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    .line 472
    iput-object v2, p0, Lcom/twitter/internal/android/widget/e;->o:Ljava/lang/CharSequence;

    goto :goto_0

    .line 473
    :cond_3
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 474
    :cond_4
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->J:Z

    if-eqz v0, :cond_5

    .line 475
    invoke-static {p1}, Lcom/twitter/util/b;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/e;->o:Ljava/lang/CharSequence;

    .line 479
    :goto_1
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->L:Z

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/twitter/internal/android/widget/e;->t:I

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    .line 480
    :goto_2
    iput-object v2, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    .line 481
    invoke-direct {p0, v1, v0}, Lcom/twitter/internal/android/widget/e;->a(ZI)V

    .line 482
    iput-boolean v3, p0, Lcom/twitter/internal/android/widget/e;->M:Z

    .line 483
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->requestLayout()V

    goto :goto_0

    .line 477
    :cond_5
    iput-object p1, p0, Lcom/twitter/internal/android/widget/e;->o:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_6
    move v0, v1

    .line 479
    goto :goto_2
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 707
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->D:Z

    if-ne p1, v0, :cond_0

    .line 713
    :goto_0
    return-void

    .line 710
    :cond_0
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/e;->D:Z

    .line 711
    invoke-virtual {p0, p1}, Lcom/twitter/internal/android/widget/e;->setClickable(Z)V

    .line 712
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->requestLayout()V

    goto :goto_0
.end method

.method public b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 584
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->o:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public b(I)V
    .locals 5

    .prologue
    .line 673
    iget v0, p0, Lcom/twitter/internal/android/widget/e;->Q:I

    if-eq p1, v0, :cond_1

    .line 674
    if-lez p1, :cond_3

    .line 675
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 677
    const/16 v0, 0x64

    if-ge p1, v0, :cond_2

    .line 678
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 682
    :goto_0
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->z:Lcmh;

    if-nez v2, :cond_0

    .line 683
    new-instance v2, Lcmh;

    invoke-direct {v2, v1}, Lcmh;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/twitter/internal/android/widget/e;->z:Lcmh;

    .line 684
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->z:Lcmh;

    iget v3, p0, Lcom/twitter/internal/android/widget/e;->e:I

    invoke-virtual {v2, v3}, Lcmh;->a(I)V

    .line 685
    iget v2, p0, Lcom/twitter/internal/android/widget/e;->d:I

    if-eqz v2, :cond_0

    .line 686
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->z:Lcmh;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, Lcom/twitter/internal/android/widget/e;->d:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcmh;->a(Landroid/graphics/drawable/Drawable;)V

    .line 689
    :cond_0
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->z:Lcmh;

    invoke-virtual {v2, v1, v0}, Lcmh;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 690
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->z:Lcmh;

    iput-object v0, p0, Lcom/twitter/internal/android/widget/e;->v:Landroid/graphics/drawable/Drawable;

    .line 694
    :goto_1
    iput p1, p0, Lcom/twitter/internal/android/widget/e;->Q:I

    .line 695
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->requestLayout()V

    .line 697
    :cond_1
    return-void

    .line 680
    :cond_2
    const-string/jumbo v0, "99+"

    goto :goto_0

    .line 692
    :cond_3
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->w:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/twitter/internal/android/widget/e;->v:Landroid/graphics/drawable/Drawable;

    goto :goto_1
.end method

.method public b(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 700
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->A:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    .line 701
    iput-object p1, p0, Lcom/twitter/internal/android/widget/e;->A:Landroid/graphics/drawable/Drawable;

    .line 702
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->requestLayout()V

    .line 704
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 621
    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/e;->N:Z

    if-eqz v2, :cond_0

    .line 622
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/e;->l()V

    .line 626
    :cond_0
    if-nez p1, :cond_3

    .line 627
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    if-eqz v2, :cond_2

    .line 628
    :goto_0
    iput-object v3, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    .line 629
    iput-object v3, p0, Lcom/twitter/internal/android/widget/e;->q:Ljava/lang/CharSequence;

    .line 641
    :goto_1
    if-eqz v0, :cond_1

    .line 642
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->requestLayout()V

    .line 644
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 627
    goto :goto_0

    .line 630
    :cond_3
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 631
    :cond_4
    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/e;->J:Z

    if-eqz v1, :cond_5

    .line 632
    invoke-static {p1}, Lcom/twitter/util/b;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/e;->q:Ljava/lang/CharSequence;

    .line 636
    :goto_2
    iput-object v3, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    goto :goto_1

    .line 634
    :cond_5
    iput-object p1, p0, Lcom/twitter/internal/android/widget/e;->q:Ljava/lang/CharSequence;

    goto :goto_2

    :cond_6
    move v0, v1

    .line 639
    goto :goto_1
.end method

.method public b(Ljava/lang/CharSequence;Z)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 588
    if-nez p2, :cond_1

    .line 589
    invoke-virtual {p0, p1}, Lcom/twitter/internal/android/widget/e;->b(Ljava/lang/CharSequence;)V

    .line 618
    :cond_0
    :goto_0
    return-void

    .line 593
    :cond_1
    if-nez p1, :cond_2

    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    if-eqz v2, :cond_2

    .line 594
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    .line 595
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->getBottom()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->getTop()I

    move-result v3

    sub-int/2addr v2, v3

    .line 594
    invoke-static {v0, v2}, Lcom/twitter/util/ui/k;->a(II)I

    move-result v0

    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->h:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v2

    .line 596
    invoke-direct {p0, v1, v0}, Lcom/twitter/internal/android/widget/e;->c(II)V

    .line 597
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->C:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 598
    :cond_2
    if-nez p1, :cond_4

    .line 599
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    if-eqz v2, :cond_3

    .line 600
    :goto_1
    iput-object v3, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    .line 601
    iput-object v3, p0, Lcom/twitter/internal/android/widget/e;->q:Ljava/lang/CharSequence;

    .line 602
    if-eqz v0, :cond_0

    .line 603
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->requestLayout()V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 599
    goto :goto_1

    .line 605
    :cond_4
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 606
    :cond_5
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->q:Ljava/lang/CharSequence;

    if-nez v2, :cond_6

    .line 607
    :goto_2
    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/e;->J:Z

    if-eqz v2, :cond_7

    .line 608
    invoke-static {p1}, Lcom/twitter/util/b;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/internal/android/widget/e;->q:Ljava/lang/CharSequence;

    .line 613
    :goto_3
    iput-object v3, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    .line 614
    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->P:Z

    .line 615
    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/e;->O:Z

    .line 616
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->requestLayout()V

    goto :goto_0

    :cond_6
    move v0, v1

    .line 606
    goto :goto_2

    .line 610
    :cond_7
    iput-object p1, p0, Lcom/twitter/internal/android/widget/e;->q:Ljava/lang/CharSequence;

    goto :goto_3
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 716
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->E:Z

    if-ne p1, v0, :cond_0

    .line 721
    :goto_0
    return-void

    .line 719
    :cond_0
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/e;->E:Z

    .line 720
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->requestLayout()V

    goto :goto_0
.end method

.method public c(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 647
    iput-object p1, p0, Lcom/twitter/internal/android/widget/e;->x:Ljava/lang/CharSequence;

    .line 648
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 724
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->F:Z

    if-ne p1, v0, :cond_0

    .line 729
    :goto_0
    return-void

    .line 727
    :cond_0
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/e;->F:Z

    .line 728
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->requestLayout()V

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 741
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->I:Z

    return v0
.end method

.method public d(Z)V
    .locals 1

    .prologue
    .line 732
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->I:Z

    if-ne v0, p1, :cond_0

    .line 738
    :goto_0
    return-void

    .line 735
    :cond_0
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/e;->I:Z

    .line 736
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->refreshDrawableState()V

    .line 737
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->requestLayout()V

    goto :goto_0
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 159
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 160
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->j:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->j:Landroid/content/res/ColorStateList;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/e;->d()V

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->k:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->k:Landroid/content/res/ColorStateList;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 164
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/e;->e()V

    .line 166
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->getDrawableState()[I

    move-result-object v0

    .line 167
    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->v:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 168
    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->v:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 170
    :cond_2
    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->A:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 171
    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->A:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 173
    :cond_3
    return-void
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 376
    invoke-super {p0}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    .line 377
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 400
    :cond_0
    :goto_0
    return-object v0

    .line 381
    :cond_1
    const/4 v0, 0x0

    .line 382
    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->x:Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 383
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->x:Ljava/lang/CharSequence;

    .line 388
    :cond_2
    :goto_1
    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/e;->D:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->y:Ljava/lang/CharSequence;

    .line 390
    :goto_2
    const-string/jumbo v2, ""

    .line 391
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 392
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 394
    :goto_3
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 395
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 396
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ". "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 398
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 384
    :cond_4
    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->o:Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 385
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->o:Ljava/lang/CharSequence;

    goto :goto_1

    .line 388
    :cond_5
    const-string/jumbo v1, ""

    goto :goto_2

    :cond_6
    move-object v0, v2

    goto :goto_3
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 177
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/view/View;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 178
    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/e;->I:Z

    if-eqz v1, :cond_0

    .line 179
    sget-object v1, Lcom/twitter/internal/android/widget/e;->b:[I

    invoke-static {v0, v1}, Lcom/twitter/internal/android/widget/e;->mergeDrawableStates([I[I)[I

    .line 181
    :cond_0
    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 347
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 348
    sget-object v2, Lcom/twitter/internal/android/widget/e;->a:Landroid/text/TextPaint;

    .line 349
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->D:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->A:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->A:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 352
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->F:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->v:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 353
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->v:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 355
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->I:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->E:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    if-eqz v0, :cond_3

    .line 356
    iget v0, p0, Lcom/twitter/internal/android/widget/e;->f:F

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 357
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->L:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/twitter/internal/android/widget/e;->t:I

    :goto_0
    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 358
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->l:Landroid/graphics/Typeface;

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 359
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    move-result v3

    .line 360
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->N:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/twitter/internal/android/widget/e;->R:I

    .line 361
    :goto_1
    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->h:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v4, p0, Lcom/twitter/internal/android/widget/e;->h:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    add-int/2addr v4, v0

    int-to-float v4, v4

    invoke-virtual {p1, v1, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 362
    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 363
    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    if-eqz v1, :cond_2

    .line 364
    iget v1, p0, Lcom/twitter/internal/android/widget/e;->g:F

    invoke-virtual {v2, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 365
    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/e;->L:Z

    if-eqz v1, :cond_6

    iget v1, p0, Lcom/twitter/internal/android/widget/e;->u:I

    :goto_2
    invoke-virtual {v2, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 366
    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->m:Landroid/graphics/Typeface;

    invoke-virtual {v2, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 367
    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->i:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->i:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    add-int/2addr v0, v2

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 368
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 370
    :cond_2
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 372
    :cond_3
    return-void

    .line 357
    :cond_4
    iget v0, p0, Lcom/twitter/internal/android/widget/e;->r:I

    goto :goto_0

    .line 360
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 365
    :cond_6
    iget v1, p0, Lcom/twitter/internal/android/widget/e;->s:I

    goto :goto_2
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 309
    sub-int v1, p5, p3

    .line 310
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->getPaddingLeft()I

    move-result v0

    .line 311
    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/e;->D:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->A:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    .line 312
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->A:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v2, v0}, Lcom/twitter/internal/android/widget/e;->a(Landroid/graphics/drawable/Drawable;I)I

    move-result v2

    add-int/2addr v0, v2

    .line 314
    :cond_0
    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/e;->F:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/e;->G:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/e;->D:Z

    if-nez v2, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->v:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_2

    .line 315
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->v:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v2, v0}, Lcom/twitter/internal/android/widget/e;->a(Landroid/graphics/drawable/Drawable;I)I

    move-result v2

    add-int/2addr v0, v2

    .line 317
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->getPaddingLeft()I

    move-result v2

    if-le v0, v2, :cond_3

    .line 319
    invoke-direct {p0, v0, v1}, Lcom/twitter/internal/android/widget/e;->a(II)V

    .line 321
    :cond_3
    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/e;->I:Z

    if-nez v2, :cond_5

    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/e;->E:Z

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    if-eqz v2, :cond_5

    .line 322
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    if-eqz v2, :cond_6

    .line 324
    iget v2, p0, Lcom/twitter/internal/android/widget/e;->c:I

    add-int/2addr v0, v2

    .line 325
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->h:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    .line 326
    iget-object v3, p0, Lcom/twitter/internal/android/widget/e;->h:Landroid/graphics/Point;

    iget-object v4, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    iget-object v5, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    .line 327
    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 326
    invoke-static {v4, v1}, Lcom/twitter/util/ui/k;->a(II)I

    move-result v1

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 328
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->i:Landroid/graphics/Point;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    invoke-virtual {v0, v6, v1}, Landroid/graphics/Point;->set(II)V

    .line 330
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/e;->P:Z

    if-eqz v0, :cond_5

    .line 331
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->h:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    sub-int v0, v2, v0

    .line 332
    if-lez v0, :cond_4

    .line 333
    invoke-direct {p0, v0, v6}, Lcom/twitter/internal/android/widget/e;->b(II)V

    .line 335
    :cond_4
    iput-boolean v6, p0, Lcom/twitter/internal/android/widget/e;->P:Z

    .line 343
    :cond_5
    :goto_0
    return-void

    .line 339
    :cond_6
    iget v2, p0, Lcom/twitter/internal/android/widget/e;->c:I

    add-int/2addr v0, v2

    .line 340
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->h:Landroid/graphics/Point;

    iget-object v3, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    invoke-static {v3, v1}, Lcom/twitter/util/ui/k;->a(II)I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Point;->set(II)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 218
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->getSuggestedMinimumHeight()I

    move-result v0

    invoke-static {v0, p2}, Lcom/twitter/internal/android/widget/e;->getDefaultSize(II)I

    move-result v1

    .line 219
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/e;->g()I

    move-result v0

    .line 220
    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/e;->E:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    if-nez v2, :cond_2

    .line 221
    :cond_0
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->o:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1

    .line 222
    invoke-direct {p0, v3}, Lcom/twitter/internal/android/widget/e;->c(I)V

    .line 224
    :cond_1
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->q:Ljava/lang/CharSequence;

    if-eqz v2, :cond_2

    .line 225
    invoke-direct {p0, v3}, Lcom/twitter/internal/android/widget/e;->d(I)V

    .line 229
    :cond_2
    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/e;->I:Z

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/e;->E:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    if-eqz v2, :cond_3

    .line 230
    iget-object v2, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    if-eqz v2, :cond_5

    .line 231
    iget v2, p0, Lcom/twitter/internal/android/widget/e;->c:I

    iget-object v3, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    .line 232
    invoke-virtual {v3}, Landroid/text/StaticLayout;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/twitter/internal/android/widget/e;->p:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getWidth()I

    move-result v4

    .line 231
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 238
    :cond_3
    :goto_0
    if-lez v0, :cond_4

    .line 239
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/e;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 242
    :cond_4
    invoke-virtual {p0, v0, v1}, Lcom/twitter/internal/android/widget/e;->setMeasuredDimension(II)V

    .line 243
    return-void

    .line 234
    :cond_5
    iget v2, p0, Lcom/twitter/internal/android/widget/e;->c:I

    iget-object v3, p0, Lcom/twitter/internal/android/widget/e;->n:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->v:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->A:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
