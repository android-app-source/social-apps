.class Lcom/twitter/internal/android/widget/ToolBar$b;
.super Landroid/widget/BaseAdapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/internal/android/widget/ToolBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private final a:Lcom/twitter/internal/android/widget/ToolBar$c;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lazv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/twitter/internal/android/widget/ToolBar$c;)V
    .locals 1

    .prologue
    .line 1279
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1277
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar$b;->b:Ljava/util/List;

    .line 1280
    iput-object p1, p0, Lcom/twitter/internal/android/widget/ToolBar$b;->a:Lcom/twitter/internal/android/widget/ToolBar$c;

    .line 1281
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/internal/android/widget/ToolBar$c;Lcom/twitter/internal/android/widget/ToolBar$1;)V
    .locals 0

    .prologue
    .line 1275
    invoke-direct {p0, p1}, Lcom/twitter/internal/android/widget/ToolBar$b;-><init>(Lcom/twitter/internal/android/widget/ToolBar$c;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lazv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1334
    if-nez p1, :cond_0

    .line 1346
    :goto_0
    return-void

    .line 1337
    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar$b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1339
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazv;

    .line 1340
    invoke-virtual {v0}, Lazv;->k()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lazv;->i()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1341
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar$b;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1344
    :cond_2
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar$b;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar$b;->a:Lcom/twitter/internal/android/widget/ToolBar$c;

    invoke-static {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(Ljava/util/Collection;Lcom/twitter/internal/android/widget/ToolBar$c;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar$b;->b:Ljava/util/List;

    .line 1345
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar$b;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 1285
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar$b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1290
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar$b;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 1295
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar$b;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazv;

    invoke-virtual {v0}, Lazv;->a()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 1306
    if-nez p2, :cond_0

    .line 1307
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1308
    sget v1, Lazw$h;->overflow_drop_down_item:I

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 1309
    new-instance v0, Lcom/twitter/internal/android/widget/ToolBar$e;

    invoke-direct {v0, p2}, Lcom/twitter/internal/android/widget/ToolBar$e;-><init>(Landroid/view/View;)V

    .line 1310
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    .line 1314
    :goto_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar$b;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazv;

    .line 1315
    iget-object v2, v1, Lcom/twitter/internal/android/widget/ToolBar$e;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Lazv;->k()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1316
    invoke-virtual {v0}, Lazv;->p()Ljava/lang/CharSequence;

    move-result-object v2

    .line 1317
    invoke-static {v2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1318
    iget-object v3, v1, Lcom/twitter/internal/android/widget/ToolBar$e;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1319
    iget-object v2, v1, Lcom/twitter/internal/android/widget/ToolBar$e;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1323
    :goto_1
    invoke-virtual {v0}, Lazv;->q()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1324
    if-eqz v0, :cond_2

    .line 1325
    iget-object v2, v1, Lcom/twitter/internal/android/widget/ToolBar$e;->c:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1326
    iget-object v0, v1, Lcom/twitter/internal/android/widget/ToolBar$e;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1330
    :goto_2
    return-object p2

    .line 1312
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar$e;

    move-object v1, v0

    goto :goto_0

    .line 1321
    :cond_1
    iget-object v2, v1, Lcom/twitter/internal/android/widget/ToolBar$e;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 1328
    :cond_2
    iget-object v0, v1, Lcom/twitter/internal/android/widget/ToolBar$e;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 1300
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar$b;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazv;

    invoke-virtual {v0}, Lazv;->m()Z

    move-result v0

    return v0
.end method
