.class public Lcom/twitter/internal/android/widget/GroupedRowView;
.super Landroid/view/ViewGroup;
.source "Twttr"


# static fields
.field private static final a:Landroid/graphics/Paint;


# instance fields
.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private f:I

.field private final g:Landroid/graphics/RectF;

.field private final h:Landroid/graphics/RectF;

.field private final i:Landroid/graphics/RectF;

.field private final j:Landroid/graphics/RectF;

.field private k:Z

.field private l:Z

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/twitter/internal/android/widget/GroupedRowView;->a:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 71
    sget v0, Lazw$c;->groupedRowViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 75
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->g:Landroid/graphics/RectF;

    .line 59
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->h:Landroid/graphics/RectF;

    .line 60
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->i:Landroid/graphics/RectF;

    .line 61
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->j:Landroid/graphics/RectF;

    .line 77
    sget-object v0, Lazw$l;->GroupedRowView:[I

    invoke-virtual {p1, p2, v0, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 81
    sget v1, Lazw$l;->GroupedRowView_cardStyle:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->f:I

    .line 82
    sget v1, Lazw$l;->GroupedRowView_single:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->k:Z

    .line 83
    sget v1, Lazw$l;->GroupedRowView_fillColor:I

    sget v2, Lazw$d;->app_background:I

    .line 84
    invoke-static {p1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 83
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->d:I

    .line 85
    sget v1, Lazw$l;->GroupedRowView_borderColor:I

    sget v2, Lazw$d;->border_color:I

    .line 86
    invoke-static {p1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 85
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->e:I

    .line 87
    sget v1, Lazw$l;->GroupedRowView_borderHeight:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->c:I

    .line 88
    sget v1, Lazw$l;->GroupedRowView_gapSize:I

    .line 89
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/GroupedRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lazw$e;->grouped_row_view_gap_size:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 88
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->b:I

    .line 90
    sget v1, Lazw$l;->GroupedRowView_hideDivider:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->l:Z

    .line 91
    sget v1, Lazw$l;->GroupedRowView_hideBorder:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->m:Z

    .line 94
    invoke-super {p0, v4, v4, v4, v4}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 96
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 97
    return-void
.end method

.method private b(II)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 280
    if-le p2, v0, :cond_2

    .line 281
    if-nez p1, :cond_0

    .line 282
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    .line 291
    :goto_0
    return-void

    .line 283
    :cond_0
    add-int/lit8 v0, p2, -0x1

    if-ne p1, v0, :cond_1

    .line 284
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0

    .line 286
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0

    .line 289
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 306
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->l:Z

    .line 307
    return-void
.end method

.method public a(II)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 270
    if-gt p2, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->k:Z

    .line 271
    invoke-direct {p0, p1, p2}, Lcom/twitter/internal/android/widget/GroupedRowView;->b(II)V

    .line 272
    return-void

    .line 270
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/GroupedRowView;->getChildCount()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->addView(Landroid/view/View;I)V

    .line 102
    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 107
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/GroupedRowView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 117
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "GroupedRowView can only hold a single child view."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 120
    return-void
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/GroupedRowView;->getChildCount()I

    move-result v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/internal/android/widget/GroupedRowView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 112
    return-void
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 239
    iget-object v0, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->g:Landroid/graphics/RectF;

    .line 240
    iget v1, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->f:I

    .line 241
    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->k:Z

    .line 243
    if-nez v1, :cond_0

    if-eqz v2, :cond_1

    :cond_0
    if-nez v0, :cond_2

    .line 244
    :cond_1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 261
    :goto_0
    return-void

    .line 246
    :cond_2
    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->m:Z

    if-nez v1, :cond_3

    .line 248
    sget-object v1, Lcom/twitter/internal/android/widget/GroupedRowView;->a:Landroid/graphics/Paint;

    .line 249
    iget v2, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->d:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 250
    iget-object v2, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->h:Landroid/graphics/RectF;

    invoke-virtual {p1, v2, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 251
    iget v2, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->e:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 252
    iget-object v2, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->i:Landroid/graphics/RectF;

    invoke-virtual {p1, v2, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 253
    iget-object v2, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->j:Landroid/graphics/RectF;

    invoke-virtual {p1, v2, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 256
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 257
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 258
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 259
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    .prologue
    .line 223
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 213
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/GroupedRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 218
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getStyle()I
    .locals 1

    .prologue
    .line 315
    iget v0, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->f:I

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    .line 228
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 229
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 230
    iget-object v2, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->g:Landroid/graphics/RectF;

    .line 231
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 232
    iget v3, v2, Landroid/graphics/RectF;->left:F

    float-to-int v3, v3

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v3, v4

    iget v4, v2, Landroid/graphics/RectF;->top:F

    float-to-int v4, v4

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v4, v5

    iget v5, v2, Landroid/graphics/RectF;->right:F

    float-to-int v5, v5

    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v5, v6

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    float-to-int v2, v2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v0, v2, v0

    invoke-virtual {v1, v3, v4, v5, v0}, Landroid/view/View;->layout(IIII)V

    .line 235
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 13

    .prologue
    const/4 v11, 0x0

    const/4 v3, 0x0

    .line 129
    invoke-virtual {p0, v3}, Lcom/twitter/internal/android/widget/GroupedRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 130
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-ne v0, v2, :cond_1

    .line 131
    :cond_0
    invoke-virtual {p0, v3, v3}, Lcom/twitter/internal/android/widget/GroupedRowView;->setMeasuredDimension(II)V

    .line 209
    :goto_0
    return-void

    .line 135
    :cond_1
    iget v9, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->f:I

    .line 136
    iget-boolean v10, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->k:Z

    .line 138
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 139
    if-nez v9, :cond_2

    if-nez v10, :cond_2

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    .line 140
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/internal/android/widget/GroupedRowView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 141
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iget v2, v6, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v0, v2

    iget v2, v6, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v2

    .line 142
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v2, v6, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v1, v2

    iget v2, v6, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v1, v2

    .line 143
    iget-object v2, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->g:Landroid/graphics/RectF;

    int-to-float v3, v0

    int-to-float v4, v1

    invoke-virtual {v2, v11, v11, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 144
    invoke-virtual {p0, v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setMeasuredDimension(II)V

    goto :goto_0

    .line 148
    :cond_2
    iget v8, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->c:I

    .line 150
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 151
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 154
    const/high16 v2, 0x40000000    # 2.0f

    if-ne v4, v2, :cond_5

    move v7, v0

    .line 163
    :goto_1
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 165
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/GroupedRowView;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/GroupedRowView;->getPaddingRight()I

    move-result v4

    add-int/2addr v2, v4

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 164
    invoke-static {p1, v2, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->getChildMeasureSpec(III)I

    move-result v2

    move-object v0, p0

    move v4, p2

    move v5, v3

    .line 168
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/internal/android/widget/GroupedRowView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 170
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget v1, v6, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v0, v1

    iget v1, v6, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int v4, v0, v1

    .line 178
    if-eqz v10, :cond_7

    .line 179
    iget v2, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->b:I

    .line 180
    add-int v0, v3, v2

    .line 182
    add-int v1, v0, v8

    .line 184
    add-int v0, v3, v8

    move v3, v8

    .line 186
    :goto_2
    const/4 v5, 0x1

    if-ne v9, v5, :cond_6

    .line 187
    iget v2, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->b:I

    .line 188
    add-int/2addr v1, v2

    .line 190
    add-int/2addr v1, v8

    .line 191
    iget-boolean v5, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->l:Z

    if-nez v5, :cond_6

    .line 193
    add-int/2addr v0, v8

    move v3, v2

    move v2, v1

    move v1, v8

    .line 196
    :goto_3
    const/4 v5, 0x2

    if-eq v9, v5, :cond_3

    const/4 v5, 0x3

    if-ne v9, v5, :cond_4

    .line 198
    :cond_3
    add-int/2addr v0, v8

    move v1, v8

    .line 200
    :cond_4
    iget-object v5, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->g:Landroid/graphics/RectF;

    int-to-float v6, v2

    int-to-float v8, v7

    add-int v9, v4, v2

    int-to-float v9, v9

    invoke-virtual {v5, v11, v6, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 201
    add-int/2addr v0, v2

    add-int/2addr v0, v4

    .line 203
    iget-object v2, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->h:Landroid/graphics/RectF;

    int-to-float v4, v7

    int-to-float v5, v3

    invoke-virtual {v2, v11, v11, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 204
    iget-object v2, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->i:Landroid/graphics/RectF;

    int-to-float v3, v3

    int-to-float v4, v7

    iget-object v5, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->g:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    invoke-virtual {v2, v11, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 205
    iget-object v2, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->j:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->g:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    int-to-float v4, v7

    iget-object v5, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->g:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    int-to-float v1, v1

    add-float/2addr v1, v5

    invoke-virtual {v2, v11, v3, v4, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 208
    invoke-virtual {p0, v7, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setMeasuredDimension(II)V

    goto/16 :goto_0

    .line 157
    :cond_5
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/GroupedRowView;->getSuggestedMinimumWidth()I

    move-result v2

    .line 158
    const/high16 v5, -0x80000000

    if-ne v4, v5, :cond_8

    .line 159
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v7, v0

    goto/16 :goto_1

    :cond_6
    move v12, v1

    move v1, v3

    move v3, v2

    move v2, v12

    goto :goto_3

    :cond_7
    move v0, v3

    move v1, v3

    move v2, v3

    goto :goto_2

    :cond_8
    move v7, v2

    goto/16 :goto_1
.end method

.method public setPadding(IIII)V
    .locals 0

    .prologue
    .line 125
    return-void
.end method

.method public setSingle(Z)V
    .locals 1

    .prologue
    .line 294
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->k:Z

    if-eq p1, v0, :cond_1

    .line 295
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->k:Z

    .line 296
    if-eqz p1, :cond_0

    .line 297
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->f:I

    .line 299
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/GroupedRowView;->requestLayout()V

    .line 303
    :goto_0
    return-void

    .line 301
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/GroupedRowView;->invalidate()V

    goto :goto_0
.end method

.method public setStyle(I)V
    .locals 1

    .prologue
    .line 324
    iget v0, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->f:I

    if-eq p1, v0, :cond_0

    .line 325
    iput p1, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->f:I

    .line 326
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/GroupedRowView;->requestLayout()V

    .line 330
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/GroupedRowView;->l:Z

    .line 331
    return-void

    .line 328
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/GroupedRowView;->invalidate()V

    goto :goto_0
.end method
