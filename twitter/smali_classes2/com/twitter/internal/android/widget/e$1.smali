.class Lcom/twitter/internal/android/widget/e$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/internal/android/widget/e;->a(ZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/internal/android/widget/e;


# direct methods
.method constructor <init>(Lcom/twitter/internal/android/widget/e;)V
    .locals 0

    .prologue
    .line 407
    iput-object p1, p0, Lcom/twitter/internal/android/widget/e$1;->a:Lcom/twitter/internal/android/widget/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 425
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e$1;->a:Lcom/twitter/internal/android/widget/e;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/internal/android/widget/e;->b(Lcom/twitter/internal/android/widget/e;Z)Z

    .line 426
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 414
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e$1;->a:Lcom/twitter/internal/android/widget/e;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/internal/android/widget/e;->a(Lcom/twitter/internal/android/widget/e;Z)Z

    .line 415
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e$1;->a:Lcom/twitter/internal/android/widget/e;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/e;->a(Lcom/twitter/internal/android/widget/e;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e$1;->a:Lcom/twitter/internal/android/widget/e;

    invoke-static {v0, v2}, Lcom/twitter/internal/android/widget/e;->a(Lcom/twitter/internal/android/widget/e;Landroid/text/StaticLayout;)Landroid/text/StaticLayout;

    .line 417
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e$1;->a:Lcom/twitter/internal/android/widget/e;

    invoke-static {v0, v2}, Lcom/twitter/internal/android/widget/e;->a(Lcom/twitter/internal/android/widget/e;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 418
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e$1;->a:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/e;->requestLayout()V

    .line 419
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e$1;->a:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/e;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 421
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 430
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 410
    return-void
.end method
