.class public Lcom/twitter/internal/android/widget/ToolBar;
.super Landroid/view/ViewGroup;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;,
        Lcom/twitter/internal/android/widget/ToolBar$d;,
        Lcom/twitter/internal/android/widget/ToolBar$a;,
        Lcom/twitter/internal/android/widget/ToolBar$c;,
        Lcom/twitter/internal/android/widget/ToolBar$b;,
        Lcom/twitter/internal/android/widget/ToolBar$e;
    }
.end annotation


# instance fields
.field private A:Lcom/twitter/internal/android/widget/DrawerToolBarItemView;

.field private B:Lazv;

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:I

.field private G:Landroid/view/View;

.field private H:Z

.field private I:Landroid/widget/ImageView;

.field private J:Lazv;

.field private K:Landroid/widget/PopupWindow;

.field private L:I

.field private final M:Landroid/graphics/Rect;

.field private final N:I

.field private O:Landroid/view/ViewGroup;

.field private P:Landroid/view/View;

.field private Q:Z

.field private R:I

.field private S:Z

.field private T:I

.field private U:I

.field private V:I

.field private W:Z

.field private final a:I

.field private aa:Z

.field private ab:Z

.field private final b:Landroid/content/Context;

.field private c:I

.field private d:Landroid/content/Context;

.field private final e:I

.field private final f:Landroid/graphics/drawable/Drawable;

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:Lcom/twitter/internal/android/widget/ToolBar$b;

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lazv;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Lcom/twitter/internal/android/widget/ToolBar$c;

.field private final p:Lcom/twitter/internal/android/widget/ToolBar$a;

.field private final q:Lcom/twitter/internal/android/widget/ToolBar$d;

.field private final r:Ljava/lang/String;

.field private s:Lcmr$a;

.field private final t:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lazv;",
            ">;"
        }
    .end annotation
.end field

.field private final u:I

.field private v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lazv;",
            ">;"
        }
    .end annotation
.end field

.field private w:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lazv;",
            ">;"
        }
    .end annotation
.end field

.field private final x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lazv;",
            ">;"
        }
    .end annotation
.end field

.field private final y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lazv;",
            ">;"
        }
    .end annotation
.end field

.field private z:Lcom/twitter/internal/android/widget/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 144
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 147
    sget v0, Lazw$c;->toolBarStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/internal/android/widget/ToolBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 148
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 151
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 93
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->t:Ljava/util/Map;

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Ljava/util/List;

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->w:Ljava/util/List;

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->x:Ljava/util/List;

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->y:Ljava/util/List;

    .line 124
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->M:Landroid/graphics/Rect;

    .line 152
    iput-object p1, p0, Lcom/twitter/internal/android/widget/ToolBar;->b:Landroid/content/Context;

    .line 153
    sget-object v0, Lazw$l;->ToolBar:[I

    invoke-virtual {p1, p2, v0, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 155
    sget v0, Lazw$l;->ToolBar_toolBarTheme:I

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 156
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setThemeId(I)V

    .line 157
    sget v0, Lazw$l;->ToolBar_toolBarItemBackground:I

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->a:I

    .line 158
    sget v0, Lazw$l;->ToolBar_toolBarItemPadding:I

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->e:I

    .line 159
    invoke-static {p1}, Lcnh;->a(Landroid/content/Context;)I

    move-result v2

    .line 160
    sget v0, Lazw$l;->ToolBar_toolBarOverflowDrawable:I

    .line 161
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 160
    invoke-static {v0, v2}, Lcnd;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->f:Landroid/graphics/drawable/Drawable;

    .line 162
    sget v0, Lazw$l;->ToolBar_toolBarOverflowContentDescription:I

    .line 163
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->r:Ljava/lang/String;

    .line 164
    sget v0, Lazw$l;->ToolBar_toolBarDrawerItemStyle:I

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->g:I

    .line 165
    sget v0, Lazw$l;->ToolBar_toolBarPaddingTop:I

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->u:I

    .line 166
    sget v0, Lazw$l;->ToolBar_toolBarHomeStyle:I

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->k:I

    .line 167
    sget v0, Lazw$l;->ToolBar_toolBarItemStyle:I

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->l:I

    .line 168
    sget v0, Lazw$l;->ToolBar_toolBarTitle:I

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 169
    sget v0, Lazw$l;->ToolBar_toolBarIcon:I

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 170
    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    :cond_0
    invoke-static {v0, v2}, Lcnd;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 172
    sget v0, Lazw$l;->ToolBar_toolBarUpIndicator:I

    .line 173
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 172
    invoke-static {v0, v2}, Lcnd;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setUpIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 174
    invoke-super {p0, v4, v4, v4, v4}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 175
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 176
    sget v2, Lazw$e;->preferred_popup_width:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->i:I

    .line 177
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v0, v0, 0x2

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->i:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->h:I

    .line 178
    new-instance v0, Lcom/twitter/internal/android/widget/ToolBar$c;

    invoke-direct {v0}, Lcom/twitter/internal/android/widget/ToolBar$c;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->o:Lcom/twitter/internal/android/widget/ToolBar$c;

    .line 179
    new-instance v0, Lcom/twitter/internal/android/widget/ToolBar$d;

    invoke-direct {v0}, Lcom/twitter/internal/android/widget/ToolBar$d;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->q:Lcom/twitter/internal/android/widget/ToolBar$d;

    .line 180
    new-instance v0, Lcom/twitter/internal/android/widget/ToolBar$a;

    invoke-direct {v0}, Lcom/twitter/internal/android/widget/ToolBar$a;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->p:Lcom/twitter/internal/android/widget/ToolBar$a;

    .line 181
    new-instance v0, Lcom/twitter/internal/android/widget/ToolBar$b;

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->o:Lcom/twitter/internal/android/widget/ToolBar$c;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/twitter/internal/android/widget/ToolBar$b;-><init>(Lcom/twitter/internal/android/widget/ToolBar$c;Lcom/twitter/internal/android/widget/ToolBar$1;)V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->j:Lcom/twitter/internal/android/widget/ToolBar$b;

    .line 182
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->n:Ljava/util/List;

    .line 183
    sget v0, Lazw$l;->ToolBar_toolBarDisplayOptions:I

    const/16 v2, 0x2e

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->R:I

    .line 186
    sget v0, Lazw$l;->ToolBar_popupMenuXOffset:I

    .line 187
    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->m:I

    .line 188
    sget v0, Lazw$l;->ToolBar_toolBarCustomViewId:I

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->N:I

    .line 189
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->c()V

    .line 190
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 191
    return-void
.end method

.method private a(Landroid/graphics/Rect;Landroid/graphics/Rect;Ljava/util/List;)I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Rect;",
            "Landroid/graphics/Rect;",
            "Ljava/util/List",
            "<",
            "Lazv;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 694
    .line 695
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazv;

    .line 696
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->c(Lazv;)Landroid/view/View;

    move-result-object v5

    .line 697
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    .line 700
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v3

    if-ge v3, v6, :cond_0

    const/4 v3, 0x1

    .line 701
    :goto_1
    invoke-virtual {v0}, Lazv;->j()I

    move-result v7

    .line 702
    if-eqz v3, :cond_1

    and-int/lit8 v3, v7, 0x2

    if-nez v3, :cond_1

    .line 704
    invoke-virtual {p0, v0, v5}, Lcom/twitter/internal/android/widget/ToolBar;->a(Lazv;Landroid/view/View;)V

    move v0, v1

    :goto_2
    move v1, v0

    .line 722
    goto :goto_0

    :cond_0
    move v3, v2

    .line 700
    goto :goto_1

    .line 705
    :cond_1
    invoke-virtual {v0}, Lazv;->i()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 706
    invoke-virtual {v0}, Lazv;->t()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 707
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iput v0, p2, Landroid/graphics/Rect;->left:I

    .line 708
    iget v0, p1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v6

    iput v0, p2, Landroid/graphics/Rect;->right:I

    .line 709
    iget v0, p2, Landroid/graphics/Rect;->right:I

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 715
    :goto_3
    invoke-direct {p0, v5}, Lcom/twitter/internal/android/widget/ToolBar;->b(Landroid/view/View;)V

    .line 716
    iget v0, p2, Landroid/graphics/Rect;->left:I

    iget v3, p2, Landroid/graphics/Rect;->top:I

    iget v7, p2, Landroid/graphics/Rect;->right:I

    iget v8, p2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v5, v0, v3, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 718
    add-int/2addr v1, v6

    move v0, v1

    goto :goto_2

    .line 711
    :cond_2
    iget v0, p1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v6

    iput v0, p2, Landroid/graphics/Rect;->left:I

    .line 712
    iget v0, p1, Landroid/graphics/Rect;->right:I

    iput v0, p2, Landroid/graphics/Rect;->right:I

    .line 713
    iget v0, p2, Landroid/graphics/Rect;->left:I

    iput v0, p1, Landroid/graphics/Rect;->right:I

    goto :goto_3

    .line 720
    :cond_3
    invoke-direct {p0, v5}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;)V

    move v0, v1

    goto :goto_2

    .line 723
    :cond_4
    return v1
.end method

.method private a(Landroid/widget/ListAdapter;)I
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 1086
    invoke-interface {p1}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1112
    :goto_0
    return v0

    .line 1094
    :cond_0
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 1096
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 1097
    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v8

    .line 1098
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->O:Landroid/view/ViewGroup;

    if-nez v1, :cond_1

    .line 1099
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getThemedContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->O:Landroid/view/ViewGroup;

    :cond_1
    move v5, v0

    move v1, v0

    move-object v3, v2

    move v4, v0

    .line 1101
    :goto_1
    if-ge v5, v8, :cond_2

    .line 1102
    invoke-interface {p1, v5}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    .line 1103
    if-eq v0, v1, :cond_3

    move-object v1, v2

    .line 1107
    :goto_2
    iget-object v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->O:Landroid/view/ViewGroup;

    invoke-interface {p1, v5, v1, v3}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1108
    invoke-virtual {v3, v6, v7}, Landroid/view/View;->measure(II)V

    .line 1109
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 1110
    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v9

    add-int/2addr v1, v9

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v9

    add-int/2addr v1, v9

    .line 1109
    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 1101
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v1, v0

    goto :goto_1

    :cond_2
    move v0, v4

    .line 1112
    goto :goto_0

    :cond_3
    move v0, v1

    move-object v1, v3

    goto :goto_2
.end method

.method static synthetic a(Lcom/twitter/internal/android/widget/ToolBar;)Lcmr$a;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->s:Lcmr$a;

    return-object v0
.end method

.method private a()Lcom/twitter/internal/android/widget/e;
    .locals 4

    .prologue
    .line 422
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-nez v0, :cond_0

    .line 423
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 424
    new-instance v1, Lcom/twitter/internal/android/widget/e;

    new-instance v2, Landroid/view/ContextThemeWrapper;

    iget v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->k:I

    invoke-direct {v2, v0, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    const/4 v0, 0x0

    iget v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->k:I

    invoke-direct {v1, v2, v0, v3}, Lcom/twitter/internal/android/widget/e;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    .line 425
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/e;->setFocusable(Z)V

    .line 426
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/e;->getPaddingLeft()I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->T:I

    .line 427
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/e;->getPaddingRight()I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->U:I

    .line 428
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    sget v1, Lazw$g;->home:I

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/e;->setId(I)V

    .line 429
    new-instance v0, Lazv;

    invoke-direct {v0, p0}, Lazv;-><init>(Lcom/twitter/internal/android/widget/ToolBar;)V

    sget v1, Lazw$g;->home:I

    invoke-virtual {v0, v1}, Lazv;->a(I)Lazv;

    move-result-object v0

    .line 430
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    new-instance v2, Lcom/twitter/internal/android/widget/ToolBar$2;

    invoke-direct {v2, p0, v0}, Lcom/twitter/internal/android/widget/ToolBar$2;-><init>(Lcom/twitter/internal/android/widget/ToolBar;Lazv;)V

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/e;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 451
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->addView(Landroid/view/View;I)V

    .line 453
    :cond_0
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->b()V

    .line 454
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    return-object v0
.end method

.method static a(Ljava/util/Collection;Lcom/twitter/internal/android/widget/ToolBar$c;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lazv;",
            ">;",
            "Lcom/twitter/internal/android/widget/ToolBar$c;",
            ")",
            "Ljava/util/List",
            "<",
            "Lazv;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v9, -0x1

    .line 922
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 926
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 927
    invoke-static {v0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 928
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 930
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 931
    const/4 v0, 0x0

    move-object v1, v0

    .line 932
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 933
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazv;

    .line 934
    invoke-virtual {v0}, Lazv;->r()I

    move-result v7

    .line 938
    if-eqz v1, :cond_1

    .line 939
    invoke-virtual {v1}, Lazv;->r()I

    move-result v1

    .line 944
    :goto_1
    if-eq v7, v9, :cond_6

    .line 945
    sub-int v1, v7, v1

    add-int/lit8 v2, v1, -0x1

    .line 951
    :goto_2
    if-gtz v2, :cond_0

    if-ne v7, v9, :cond_2

    .line 952
    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 953
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lazv;

    .line 954
    invoke-virtual {v1}, Lazv;->r()I

    move-result v8

    if-ne v8, v9, :cond_5

    .line 955
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 956
    add-int/lit8 v1, v2, -0x1

    :goto_3
    move v2, v1

    .line 958
    goto :goto_2

    :cond_1
    move v1, v3

    .line 941
    goto :goto_1

    .line 960
    :cond_2
    if-eq v7, v9, :cond_3

    .line 961
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    move-object v1, v0

    .line 964
    goto :goto_0

    .line 965
    :cond_4
    return-object v4

    :cond_5
    move v1, v2

    goto :goto_3

    :cond_6
    move v2, v3

    goto :goto_2
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 653
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 654
    invoke-virtual {p0, p1}, Lcom/twitter/internal/android/widget/ToolBar;->detachViewFromParent(Landroid/view/View;)V

    .line 656
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/twitter/internal/android/widget/ToolBar;)Lazv;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->B:Lazv;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 458
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-eqz v0, :cond_3

    .line 459
    iget-object v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->C:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->E:Z

    if-eqz v0, :cond_4

    :cond_0
    move v0, v2

    :goto_0
    invoke-virtual {v3, v0}, Lcom/twitter/internal/android/widget/e;->setClickable(Z)V

    .line 460
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    iget-boolean v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->C:Z

    if-nez v3, :cond_1

    iget-boolean v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->E:Z

    if-eqz v3, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/e;->setLongClickable(Z)V

    .line 461
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->C:Z

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/e;->a(Z)V

    .line 463
    :cond_3
    return-void

    :cond_4
    move v0, v1

    .line 459
    goto :goto_0
.end method

.method private b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 659
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    .line 660
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 662
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/twitter/internal/android/widget/ToolBar;)Lcom/twitter/internal/android/widget/e;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1198
    iget v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->R:I

    .line 1199
    and-int/lit8 v0, v3, 0x4

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayShowHomeAsUpEnabled(Z)V

    .line 1200
    and-int/lit8 v0, v3, 0x2

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayHomeEnabled(Z)V

    .line 1201
    and-int/lit8 v0, v3, 0x8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayShowTitleEnabled(Z)V

    .line 1202
    and-int/lit8 v0, v3, 0x10

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayFullExpandEnabled(Z)V

    .line 1203
    and-int/lit8 v0, v3, 0x20

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayHomeClickable(Z)V

    .line 1204
    and-int/lit8 v0, v3, 0x40

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayHideOverflow(Z)V

    .line 1205
    and-int/lit16 v0, v3, 0x80

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayDrawerIcon(Z)V

    .line 1206
    and-int/lit16 v0, v3, 0x100

    if-eqz v0, :cond_7

    :goto_7
    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setIgnoreExpandedItems(Z)V

    .line 1207
    return-void

    :cond_0
    move v0, v2

    .line 1199
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1200
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1201
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1202
    goto :goto_3

    :cond_4
    move v0, v2

    .line 1203
    goto :goto_4

    :cond_5
    move v0, v2

    .line 1204
    goto :goto_5

    :cond_6
    move v0, v2

    .line 1205
    goto :goto_6

    :cond_7
    move v1, v2

    .line 1206
    goto :goto_7
.end method

.method static synthetic d(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->ab:Z

    return v0
.end method

.method static synthetic e(Lcom/twitter/internal/android/widget/ToolBar;)Ljava/util/List;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->x:Ljava/util/List;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/internal/android/widget/ToolBar;)Ljava/util/List;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->y:Ljava/util/List;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/internal/android/widget/ToolBar;)Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->K:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method private static g(Lazv;)Z
    .locals 2

    .prologue
    .line 646
    invoke-virtual {p0}, Lazv;->j()I

    move-result v0

    .line 647
    invoke-virtual {p0}, Lazv;->b()I

    move-result v1

    if-nez v1, :cond_0

    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_1

    :cond_0
    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getStartIndex()I
    .locals 1

    .prologue
    .line 759
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-nez v0, :cond_0

    .line 760
    const/4 v0, 0x0

    .line 764
    :goto_0
    return v0

    .line 762
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setDisplayHideOverflow(Z)V
    .locals 1

    .prologue
    .line 287
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->W:Z

    if-ne v0, p1, :cond_1

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 291
    :cond_1
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/ToolBar;->W:Z

    .line 292
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->W:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;)V

    .line 294
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Landroid/widget/ImageView;

    goto :goto_0
.end method

.method private setDisplayHomeClickable(Z)V
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-eqz v0, :cond_0

    .line 281
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/ToolBar;->E:Z

    .line 282
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->b()V

    .line 284
    :cond_0
    return-void
.end method

.method private setDisplayHomeEnabled(Z)V
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/e;->c(Z)V

    .line 277
    :cond_0
    return-void
.end method

.method private setExtraWidth(I)V
    .locals 1

    .prologue
    .line 334
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->S:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-nez v0, :cond_1

    .line 338
    :cond_0
    :goto_0
    return-void

    .line 337
    :cond_1
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->a()Lcom/twitter/internal/android/widget/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/e;->a(I)V

    goto :goto_0
.end method

.method private setUpIndicator(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 341
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-nez v0, :cond_0

    .line 345
    :goto_0
    return-void

    .line 344
    :cond_0
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->a()Lcom/twitter/internal/android/widget/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/e;->b(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public a(I)Lazv;
    .locals 2

    .prologue
    .line 969
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->t:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazv;

    return-object v0
.end method

.method protected a(IIII)V
    .locals 4

    .prologue
    .line 669
    const/4 v0, 0x0

    .line 672
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, p1, p2, p3, p4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 675
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, p1, p2, p3, p4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 676
    iget-object v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->x:Ljava/util/List;

    invoke-direct {p0, v1, v2, v3}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/graphics/Rect;Landroid/graphics/Rect;Ljava/util/List;)I

    move-result v3

    add-int/2addr v0, v3

    .line 677
    iget-object v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->y:Ljava/util/List;

    invoke-direct {p0, v1, v2, v3}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/graphics/Rect;Landroid/graphics/Rect;Ljava/util/List;)I

    move-result v2

    add-int/2addr v0, v2

    .line 678
    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setExtraWidth(I)V

    .line 681
    iget v0, v1, Landroid/graphics/Rect;->left:I

    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget v3, v1, Landroid/graphics/Rect;->right:I

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/twitter/internal/android/widget/ToolBar;->b(IIII)V

    .line 682
    return-void
.end method

.method public a(Landroid/view/View;Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x0

    .line 1140
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->P:Landroid/view/View;

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    .line 1141
    :goto_0
    if-eqz v0, :cond_0

    .line 1142
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->P:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_5

    .line 1143
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->P:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->removeView(Landroid/view/View;)V

    .line 1147
    :goto_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->P:Landroid/view/View;

    .line 1149
    :cond_0
    if-eqz p1, :cond_1

    .line 1150
    if-eqz p2, :cond_6

    .line 1151
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1159
    :cond_1
    :goto_2
    iput-object p1, p0, Lcom/twitter/internal/android/widget/ToolBar;->P:Landroid/view/View;

    .line 1160
    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->Q:Z

    .line 1161
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->P:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 1162
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->requestLayout()V

    .line 1163
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->invalidate()V

    .line 1165
    :cond_3
    return-void

    :cond_4
    move v0, v1

    .line 1140
    goto :goto_0

    .line 1145
    :cond_5
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->P:Landroid/view/View;

    invoke-virtual {p0, v2, v1}, Lcom/twitter/internal/android/widget/ToolBar;->removeDetachedView(Landroid/view/View;Z)V

    goto :goto_1

    .line 1153
    :cond_6
    new-instance v2, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    invoke-direct {v2, v3, v3}, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2
.end method

.method a(Lazv;)V
    .locals 1

    .prologue
    .line 635
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->x:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 636
    return-void
.end method

.method a(Lazv;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 730
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 731
    invoke-direct {p0, p2}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;)V

    .line 732
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Z)V
    .locals 1

    .prologue
    .line 361
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-nez v0, :cond_0

    .line 365
    :goto_0
    return-void

    .line 364
    :cond_0
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->a()Lcom/twitter/internal/android/widget/e;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/internal/android/widget/e;->a(Ljava/lang/CharSequence;Z)V

    goto :goto_0
.end method

.method public a(Ljava/util/Collection;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lazv;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 848
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 905
    :goto_0
    return-void

    .line 851
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getChildCount()I

    move-result v0

    .line 852
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazv;

    .line 853
    invoke-virtual {v0}, Lazv;->a()I

    move-result v6

    .line 854
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->t:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 855
    invoke-virtual {v0}, Lazv;->j()I

    move-result v7

    .line 856
    and-int/lit8 v2, v7, 0x2

    if-eqz v2, :cond_4

    move v2, v3

    .line 858
    :goto_2
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getThemedContext()Landroid/content/Context;

    move-result-object v8

    .line 859
    new-instance v9, Lcom/twitter/internal/android/widget/ToolBarItemView;

    invoke-direct {v9, v8}, Lcom/twitter/internal/android/widget/ToolBarItemView;-><init>(Landroid/content/Context;)V

    .line 860
    iget v10, p0, Lcom/twitter/internal/android/widget/ToolBar;->l:I

    if-eqz v10, :cond_1

    .line 861
    iget v10, p0, Lcom/twitter/internal/android/widget/ToolBar;->l:I

    invoke-virtual {v9, v8, v10}, Lcom/twitter/internal/android/widget/ToolBarItemView;->a(Landroid/content/Context;I)V

    .line 863
    :cond_1
    invoke-virtual {v9, v6}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setId(I)V

    .line 864
    invoke-virtual {v0}, Lazv;->b()I

    move-result v8

    invoke-virtual {v9, v8}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setImageResource(I)V

    .line 865
    invoke-virtual {v0}, Lazv;->k()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v9, v8}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setLabel(Ljava/lang/CharSequence;)V

    .line 866
    and-int/lit8 v7, v7, 0x4

    if-nez v7, :cond_2

    .line 867
    invoke-virtual {v0}, Lazv;->b()I

    move-result v7

    if-nez v7, :cond_5

    if-eqz v2, :cond_5

    :cond_2
    move v2, v3

    .line 866
    :goto_3
    invoke-virtual {v9, v2}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setWithText(Z)V

    .line 868
    invoke-virtual {v0}, Lazv;->m()Z

    move-result v2

    invoke-virtual {v9, v2}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setEnabled(Z)V

    .line 869
    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->a:I

    if-eqz v2, :cond_3

    .line 870
    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->a:I

    invoke-virtual {v9, v2}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setBackgroundResource(I)V

    .line 872
    :cond_3
    new-instance v2, Lcom/twitter/internal/android/widget/ToolBar$6;

    invoke-direct {v2, p0, v0}, Lcom/twitter/internal/android/widget/ToolBar$6;-><init>(Lcom/twitter/internal/android/widget/ToolBar;Lazv;)V

    invoke-virtual {v9, v2}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 880
    new-instance v2, Lcom/twitter/internal/android/widget/ToolBar$7;

    invoke-direct {v2, p0, v0}, Lcom/twitter/internal/android/widget/ToolBar$7;-><init>(Lcom/twitter/internal/android/widget/ToolBar;Lazv;)V

    invoke-virtual {v9, v2}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 890
    invoke-virtual {v0, v9}, Lazv;->a(Lcom/twitter/internal/android/widget/ToolBarItemView;)Lazv;

    .line 891
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->c(Lazv;)Landroid/view/View;

    move-result-object v7

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    invoke-virtual {p0, v7, v1, v8, v3}, Lcom/twitter/internal/android/widget/ToolBar;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 892
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->t:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v2

    :goto_4
    move v1, v0

    .line 894
    goto/16 :goto_1

    :cond_4
    move v2, v4

    .line 856
    goto :goto_2

    :cond_5
    move v2, v4

    .line 867
    goto :goto_3

    .line 897
    :cond_6
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->t:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    .line 898
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->o:Lcom/twitter/internal/android/widget/ToolBar$c;

    invoke-static {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(Ljava/util/Collection;Lcom/twitter/internal/android/widget/ToolBar$c;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Ljava/util/List;

    .line 901
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->o:Lcom/twitter/internal/android/widget/ToolBar$c;

    invoke-static {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(Ljava/util/Collection;Lcom/twitter/internal/android/widget/ToolBar$c;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->w:Ljava/util/List;

    .line 902
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->w:Ljava/util/List;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->q:Lcom/twitter/internal/android/widget/ToolBar$d;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 903
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->w:Ljava/util/List;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->p:Lcom/twitter/internal/android/widget/ToolBar$a;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 904
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->requestLayout()V

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto :goto_4
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 1188
    iget v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->R:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->R:I

    .line 1189
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->c()V

    .line 1190
    return-void
.end method

.method b(IIII)V
    .locals 2

    .prologue
    .line 738
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->Q:Z

    if-eqz v0, :cond_0

    .line 739
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->P:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 740
    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    iget v0, v0, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;->a:I

    .line 741
    packed-switch v0, :pswitch_data_0

    .line 747
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->P:Landroid/view/View;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/view/View;->layout(IIII)V

    .line 752
    :cond_0
    :goto_0
    return-void

    .line 743
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->P:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->P:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int v1, p3, v1

    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    .line 741
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method b(Lazv;)V
    .locals 1

    .prologue
    .line 642
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->y:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 643
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;Z)V
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-nez v0, :cond_0

    .line 388
    :goto_0
    return-void

    .line 387
    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/internal/android/widget/e;->b(Ljava/lang/CharSequence;Z)V

    goto :goto_0
.end method

.method c(Lazv;)Landroid/view/View;
    .locals 2

    .prologue
    .line 908
    invoke-virtual {p1}, Lazv;->j()I

    move-result v0

    .line 909
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lazv;->e()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 910
    invoke-virtual {p1}, Lazv;->e()Landroid/view/View;

    move-result-object v0

    .line 912
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lazv;->d()Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 1193
    iget v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->R:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->R:I

    .line 1194
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->c()V

    .line 1195
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 1117
    instance-of v0, p1, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Lazv;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 985
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->G:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1023
    :cond_0
    :goto_0
    return v1

    .line 988
    :cond_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->G:Landroid/view/View;

    invoke-virtual {p1}, Lazv;->e()Landroid/view/View;

    move-result-object v2

    if-ne v0, v2, :cond_0

    .line 991
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->G:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->removeView(Landroid/view/View;)V

    .line 992
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getChildCount()I

    move-result v3

    .line 993
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getStartIndex()I

    move-result v0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_4

    .line 994
    invoke-virtual {p0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 995
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->t:Ljava/util/Map;

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazv;

    .line 996
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lazv;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 997
    :cond_2
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 993
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1000
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->G:Landroid/view/View;

    .line 1001
    invoke-virtual {p1, v1}, Lazv;->a(Z)V

    .line 1002
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-eqz v0, :cond_5

    .line 1003
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->D:Z

    if-eqz v0, :cond_6

    .line 1004
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->F:I

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/e;->setVisibility(I)V

    .line 1014
    :goto_2
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Lcom/twitter/internal/android/widget/DrawerToolBarItemView;

    if-eqz v0, :cond_5

    .line 1015
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Lcom/twitter/internal/android/widget/DrawerToolBarItemView;

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->aa:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_3
    invoke-virtual {v2, v0}, Lcom/twitter/internal/android/widget/DrawerToolBarItemView;->setVisibility(I)V

    .line 1016
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->C:Z

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/e;->a(Z)V

    .line 1017
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->F:I

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/e;->setVisibility(I)V

    .line 1018
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->T:I

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v2}, Lcom/twitter/internal/android/widget/e;->getPaddingTop()I

    move-result v2

    iget v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->U:I

    iget-object v4, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    .line 1019
    invoke-virtual {v4}, Lcom/twitter/internal/android/widget/e;->getPaddingBottom()I

    move-result v4

    .line 1018
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/internal/android/widget/e;->setPadding(IIII)V

    .line 1022
    :cond_5
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->requestLayout()V

    .line 1023
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 1006
    :cond_6
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/e;->d(Z)V

    .line 1007
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->C:Z

    if-nez v0, :cond_7

    .line 1008
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->T:I

    iget-object v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v3}, Lcom/twitter/internal/android/widget/e;->getPaddingTop()I

    move-result v3

    iget v4, p0, Lcom/twitter/internal/android/widget/ToolBar;->U:I

    iget-object v5, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    .line 1009
    invoke-virtual {v5}, Lcom/twitter/internal/android/widget/e;->getPaddingBottom()I

    move-result v5

    .line 1008
    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/twitter/internal/android/widget/e;->setPadding(IIII)V

    .line 1011
    :cond_7
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->C:Z

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/e;->a(Z)V

    goto :goto_2

    .line 1015
    :cond_8
    const/16 v0, 0x8

    goto :goto_3
.end method

.method e()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 768
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->K:Landroid/widget/PopupWindow;

    if-nez v0, :cond_0

    .line 769
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getThemedContext()Landroid/content/Context;

    move-result-object v0

    .line 771
    new-instance v1, Lcom/twitter/internal/android/widget/DropDownListView;

    sget v2, Lazw$c;->dropDownListViewStyle:I

    invoke-direct {v1, v0, v6, v2}, Lcom/twitter/internal/android/widget/DropDownListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 772
    invoke-virtual {v1, v4}, Lcom/twitter/internal/android/widget/DropDownListView;->setFocusable(Z)V

    .line 773
    invoke-virtual {v1, v4}, Lcom/twitter/internal/android/widget/DropDownListView;->setFocusableInTouchMode(Z)V

    .line 774
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->j:Lcom/twitter/internal/android/widget/ToolBar$b;

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/DropDownListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 775
    new-instance v2, Lcom/twitter/internal/android/widget/ToolBar$4;

    invoke-direct {v2, p0}, Lcom/twitter/internal/android/widget/ToolBar$4;-><init>(Lcom/twitter/internal/android/widget/ToolBar;)V

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/DropDownListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 785
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/DropDownListView;->setSelection(I)V

    .line 786
    new-instance v2, Lcom/twitter/internal/android/widget/ToolBar$5;

    invoke-direct {v2, p0}, Lcom/twitter/internal/android/widget/ToolBar$5;-><init>(Lcom/twitter/internal/android/widget/ToolBar;)V

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/DropDownListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 802
    new-instance v2, Landroid/widget/PopupWindow;

    sget v3, Lazw$c;->toolBarPopupWindowStyle:I

    invoke-direct {v2, v0, v6, v3}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 805
    const/4 v0, 0x2

    invoke-virtual {v2, v0}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 806
    invoke-virtual {v2, v4}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 807
    invoke-virtual {v2, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 809
    invoke-virtual {v2, v4}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 810
    iput-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->K:Landroid/widget/PopupWindow;

    .line 812
    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->j:Lcom/twitter/internal/android/widget/ToolBar$b;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->n:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar$b;->a(Ljava/util/List;)V

    .line 813
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->K:Landroid/widget/PopupWindow;

    .line 814
    const/4 v1, -0x2

    invoke-virtual {v0, v5, v1}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    .line 815
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->j:Lcom/twitter/internal/android/widget/ToolBar$b;

    invoke-direct {p0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/widget/ListAdapter;)I

    move-result v1

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->h:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 816
    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->i:I

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setContentWidth(I)V

    .line 818
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    neg-int v3, v1

    .line 819
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 820
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Landroid/widget/ImageView;

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->m:I

    iget v4, p0, Lcom/twitter/internal/android/widget/ToolBar;->L:I

    invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(Landroid/view/View;IIII)V

    .line 825
    :goto_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->s:Lcmr$a;

    if-eqz v0, :cond_1

    .line 826
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->s:Lcmr$a;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->J:Lazv;

    invoke-interface {v0, v1}, Lcmr$a;->a(Lcmm;)Z

    .line 828
    :cond_1
    return-void

    .line 822
    :cond_2
    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->L:I

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 823
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Landroid/widget/ImageView;

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->m:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    goto :goto_0
.end method

.method public e(Lazv;)Z
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1030
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->G:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 1061
    :goto_0
    return v0

    .line 1033
    :cond_0
    invoke-virtual {p1}, Lazv;->e()Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->G:Landroid/view/View;

    .line 1034
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getChildCount()I

    move-result v3

    .line 1035
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getStartIndex()I

    move-result v2

    :goto_1
    if-ge v2, v3, :cond_1

    .line 1036
    invoke-virtual {p0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1035
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1038
    :cond_1
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->G:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->addView(Landroid/view/View;)V

    .line 1039
    invoke-virtual {p1, v1}, Lazv;->a(Z)V

    .line 1040
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-eqz v2, :cond_2

    .line 1041
    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->D:Z

    if-eqz v2, :cond_3

    .line 1042
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v2}, Lcom/twitter/internal/android/widget/e;->getVisibility()I

    move-result v2

    iput v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->F:I

    .line 1043
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v2, v5}, Lcom/twitter/internal/android/widget/e;->setVisibility(I)V

    .line 1052
    :goto_2
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Lcom/twitter/internal/android/widget/DrawerToolBarItemView;

    if-eqz v2, :cond_2

    .line 1053
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Lcom/twitter/internal/android/widget/DrawerToolBarItemView;

    invoke-virtual {v2, v5}, Lcom/twitter/internal/android/widget/DrawerToolBarItemView;->setVisibility(I)V

    .line 1054
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v2, v0}, Lcom/twitter/internal/android/widget/e;->setVisibility(I)V

    .line 1055
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/e;->a(Z)V

    .line 1056
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->e:I

    mul-int/lit8 v2, v2, 0x2

    iget v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->e:I

    iget v4, p0, Lcom/twitter/internal/android/widget/ToolBar;->e:I

    iget v5, p0, Lcom/twitter/internal/android/widget/ToolBar;->e:I

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/twitter/internal/android/widget/e;->setPadding(IIII)V

    .line 1060
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->requestLayout()V

    move v0, v1

    .line 1061
    goto :goto_0

    .line 1045
    :cond_3
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v2, v1}, Lcom/twitter/internal/android/widget/e;->d(Z)V

    .line 1046
    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->C:Z

    if-nez v2, :cond_4

    .line 1047
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v2, v0, v0, v0, v0}, Lcom/twitter/internal/android/widget/e;->setPadding(IIII)V

    .line 1049
    :cond_4
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v2, v1}, Lcom/twitter/internal/android/widget/e;->a(Z)V

    goto :goto_2
.end method

.method public f()V
    .locals 1

    .prologue
    .line 841
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->t:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 842
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 843
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 844
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->requestLayout()V

    .line 845
    return-void
.end method

.method public f(Lazv;)V
    .locals 2

    .prologue
    .line 1241
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->G:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1242
    invoke-virtual {p0, p1}, Lcom/twitter/internal/android/widget/ToolBar;->c(Lazv;)Landroid/view/View;

    move-result-object v0

    .line 1243
    if-eqz v0, :cond_0

    .line 1244
    invoke-virtual {p1}, Lazv;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1245
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1251
    :cond_0
    :goto_0
    return-void

    .line 1247
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 973
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->t:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazv;

    .line 974
    invoke-virtual {v0}, Lazv;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 975
    const/4 v0, 0x1

    .line 978
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 1132
    new-instance v0, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    invoke-direct {v0, v1, v1}, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 1122
    new-instance v0, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getThemedContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1127
    new-instance v0, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    invoke-direct {v0, p1}, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getDisplayOptions()I
    .locals 1

    .prologue
    .line 1184
    iget v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->R:I

    return v0
.end method

.method public final getSubtitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-nez v0, :cond_0

    .line 392
    const/4 v0, 0x0

    .line 394
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/e;->a()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public getThemeId()I
    .locals 1

    .prologue
    .line 218
    iget v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->c:I

    return v0
.end method

.method public getThemedContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->d:Landroid/content/Context;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->b:Landroid/content/Context;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->d:Landroid/content/Context;

    goto :goto_0
.end method

.method public final getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-nez v0, :cond_0

    .line 399
    const/4 v0, 0x0

    .line 401
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/e;->b()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 1169
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setVisibility(I)V

    .line 1170
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 1174
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setVisibility(I)V

    .line 1175
    return-void
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 1213
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->K:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->K:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 1220
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Z

    if-eqz v0, :cond_0

    .line 1221
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->e()V

    .line 1222
    const/4 v0, 0x1

    .line 1224
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 1232
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->K:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 1233
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->K:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1234
    const/4 v0, 0x1

    .line 1236
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()V
    .locals 1

    .prologue
    .line 1258
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->K:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->K:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1259
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->j:Lcom/twitter/internal/android/widget/ToolBar$b;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar$b;->notifyDataSetChanged()V

    .line 1261
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 832
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 833
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->l()Z

    .line 834
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 467
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 468
    iget v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->N:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->N:I

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 469
    :goto_0
    if-eqz v1, :cond_0

    .line 470
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    invoke-virtual {p0, v1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;)V

    .line 472
    :cond_0
    return-void

    .line 468
    :cond_1
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    const/16 v6, 0x8

    const/4 v1, 0x0

    .line 572
    .line 573
    sub-int v3, p5, p3

    .line 574
    sub-int v2, p4, p2

    .line 576
    iget v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->V:I

    iget v4, p0, Lcom/twitter/internal/android/widget/ToolBar;->u:I

    add-int/2addr v4, v0

    .line 577
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Lcom/twitter/internal/android/widget/DrawerToolBarItemView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Lcom/twitter/internal/android/widget/DrawerToolBarItemView;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/DrawerToolBarItemView;->getVisibility()I

    move-result v0

    if-eq v0, v6, :cond_9

    .line 578
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Lcom/twitter/internal/android/widget/DrawerToolBarItemView;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/DrawerToolBarItemView;->getMeasuredWidth()I

    move-result v0

    .line 579
    iget-object v5, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Lcom/twitter/internal/android/widget/DrawerToolBarItemView;

    invoke-virtual {v5, v1, v4, v0, v3}, Lcom/twitter/internal/android/widget/DrawerToolBarItemView;->layout(IIII)V

    .line 582
    :goto_0
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v1}, Lcom/twitter/internal/android/widget/e;->getVisibility()I

    move-result v1

    if-eq v1, v6, :cond_8

    .line 583
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v1}, Lcom/twitter/internal/android/widget/e;->getMeasuredWidth()I

    move-result v1

    .line 584
    iget-object v5, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    add-int v6, v0, v1

    invoke-virtual {v5, v0, v4, v6, v3}, Lcom/twitter/internal/android/widget/e;->layout(IIII)V

    .line 585
    add-int/2addr v0, v1

    move v1, v0

    .line 587
    :goto_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->G:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 588
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->G:Landroid/view/View;

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 629
    :goto_2
    return-void

    .line 591
    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 593
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 594
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Z

    if-eqz v0, :cond_3

    .line 595
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    .line 596
    iget-object v5, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-nez v5, :cond_1

    .line 597
    iget-object v5, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getChildCount()I

    move-result v6

    iget-object v7, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Landroid/widget/ImageView;

    .line 598
    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    const/4 v8, 0x1

    .line 597
    invoke-virtual {p0, v5, v6, v7, v8}, Lcom/twitter/internal/android/widget/ToolBar;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 601
    :cond_1
    iget-object v5, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Landroid/widget/ImageView;

    sub-int v6, v2, v0

    invoke-virtual {v5, v6, v4, v2, v3}, Landroid/widget/ImageView;->layout(IIII)V

    .line 602
    sub-int v0, v2, v0

    move v2, v0

    .line 607
    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 608
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 610
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->S:Z

    if-eqz v0, :cond_4

    .line 611
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->w:Ljava/util/List;

    .line 615
    :goto_4
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazv;

    .line 616
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->c(Lazv;)Landroid/view/View;

    move-result-object v6

    .line 617
    invoke-static {v0}, Lcom/twitter/internal/android/widget/ToolBar;->g(Lazv;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 618
    invoke-virtual {p0, v0, v6}, Lcom/twitter/internal/android/widget/ToolBar;->a(Lazv;Landroid/view/View;)V

    goto :goto_5

    .line 604
    :cond_3
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;)V

    goto :goto_3

    .line 613
    :cond_4
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Ljava/util/List;

    goto :goto_4

    .line 620
    :cond_5
    invoke-virtual {v0}, Lazv;->t()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 621
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->b(Lazv;)V

    goto :goto_5

    .line 623
    :cond_6
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(Lazv;)V

    goto :goto_5

    .line 627
    :cond_7
    invoke-virtual {p0, v1, v4, v2, v3}, Lcom/twitter/internal/android/widget/ToolBar;->a(IIII)V

    goto :goto_2

    :cond_8
    move v1, v0

    goto/16 :goto_1

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 12

    .prologue
    .line 476
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 477
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getMeasuredHeight()I

    move-result v0

    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->u:I

    sub-int/2addr v0, v1

    .line 478
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 479
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getMeasuredWidth()I

    move-result v1

    .line 480
    const/4 v0, 0x0

    .line 482
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Lcom/twitter/internal/android/widget/DrawerToolBarItemView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Lcom/twitter/internal/android/widget/DrawerToolBarItemView;

    invoke-virtual {v2}, Lcom/twitter/internal/android/widget/DrawerToolBarItemView;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    .line 483
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Lcom/twitter/internal/android/widget/DrawerToolBarItemView;

    const/4 v3, 0x0

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2, v3, v6}, Lcom/twitter/internal/android/widget/DrawerToolBarItemView;->measure(II)V

    .line 485
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Lcom/twitter/internal/android/widget/DrawerToolBarItemView;

    invoke-virtual {v2}, Lcom/twitter/internal/android/widget/DrawerToolBarItemView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v0, v2

    .line 488
    :cond_0
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v2}, Lcom/twitter/internal/android/widget/e;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_1

    .line 489
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    const/4 v3, 0x0

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2, v3, v6}, Lcom/twitter/internal/android/widget/e;->measure(II)V

    .line 491
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v2}, Lcom/twitter/internal/android/widget/e;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v0, v2

    .line 494
    :cond_1
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->G:Landroid/view/View;

    if-eqz v2, :cond_3

    .line 495
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->G:Landroid/view/View;

    sub-int v0, v1, v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v2, v0, v6}, Landroid/view/View;->measure(II)V

    .line 568
    :cond_2
    :goto_0
    return-void

    .line 498
    :cond_3
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 500
    sub-int v2, v1, v0

    .line 501
    const/4 v1, 0x0

    .line 502
    const/4 v0, 0x0

    .line 503
    iget-object v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->t:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v3, v2

    move v2, v1

    move v1, v0

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazv;

    .line 504
    invoke-virtual {v0}, Lazv;->j()I

    move-result v9

    .line 505
    and-int/lit8 v4, v9, 0x2

    if-eqz v4, :cond_5

    const/4 v4, 0x1

    .line 506
    :goto_2
    invoke-virtual {v0}, Lazv;->i()Z

    move-result v10

    .line 507
    if-eqz v10, :cond_6

    if-eqz v9, :cond_4

    .line 508
    invoke-virtual {v0}, Lazv;->b()I

    move-result v5

    if-nez v5, :cond_6

    if-nez v4, :cond_6

    :cond_4
    const/4 v5, 0x1

    :goto_3
    or-int/2addr v2, v5

    .line 509
    if-nez v4, :cond_7

    if-eqz v10, :cond_7

    const/4 v4, 0x1

    :goto_4
    or-int/2addr v1, v4

    .line 510
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->c(Lazv;)Landroid/view/View;

    move-result-object v4

    .line 511
    if-eqz v9, :cond_14

    if-eqz v10, :cond_14

    .line 512
    iget v5, p0, Lcom/twitter/internal/android/widget/ToolBar;->e:I

    const/4 v9, 0x0

    iget v10, p0, Lcom/twitter/internal/android/widget/ToolBar;->e:I

    const/4 v11, 0x0

    invoke-virtual {v4, v5, v9, v10, v11}, Landroid/view/View;->setPadding(IIII)V

    .line 513
    invoke-virtual {v4, v7, v6}, Landroid/view/View;->measure(II)V

    .line 514
    invoke-static {v0}, Lcom/twitter/internal/android/widget/ToolBar;->g(Lazv;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 515
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr v3, v0

    move v0, v3

    :goto_5
    move v3, v0

    .line 518
    goto :goto_1

    .line 505
    :cond_5
    const/4 v4, 0x0

    goto :goto_2

    .line 508
    :cond_6
    const/4 v5, 0x0

    goto :goto_3

    .line 509
    :cond_7
    const/4 v4, 0x0

    goto :goto_4

    .line 520
    :cond_8
    if-gez v3, :cond_f

    const/4 v0, 0x1

    :goto_6
    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->S:Z

    .line 521
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->W:Z

    if-nez v0, :cond_10

    if-nez v2, :cond_9

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->S:Z

    if-eqz v0, :cond_10

    if-eqz v1, :cond_10

    :cond_9
    const/4 v0, 0x1

    :goto_7
    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Z

    .line 522
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Z

    if-eqz v0, :cond_11

    .line 523
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Landroid/widget/ImageView;

    if-nez v0, :cond_c

    .line 524
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getThemedContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 525
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 526
    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->a:I

    if-eqz v1, :cond_a

    .line 527
    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 529
    :cond_a
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 530
    new-instance v1, Lcom/twitter/internal/android/widget/ToolBar$3;

    invoke-direct {v1, p0}, Lcom/twitter/internal/android/widget/ToolBar$3;-><init>(Lcom/twitter/internal/android/widget/ToolBar;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 536
    sget v1, Lazw$g;->overflow:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setId(I)V

    .line 537
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 538
    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->e:I

    const/4 v2, 0x0

    iget v4, p0, Lcom/twitter/internal/android/widget/ToolBar;->e:I

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 539
    invoke-virtual {v0, v7, v6}, Landroid/widget/ImageView;->measure(II)V

    .line 540
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->r:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 541
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 543
    :cond_b
    new-instance v1, Lazv;

    invoke-direct {v1, p0}, Lazv;-><init>(Lcom/twitter/internal/android/widget/ToolBar;)V

    sget v2, Lazw$g;->overflow:I

    invoke-virtual {v1, v2}, Lazv;->a(I)Lazv;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->J:Lazv;

    .line 544
    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Landroid/widget/ImageView;

    .line 546
    :cond_c
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 547
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Landroid/widget/ImageView;

    invoke-virtual {v0, v7, v6}, Landroid/widget/ImageView;->measure(II)V

    .line 548
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr v3, v0

    .line 553
    :cond_d
    :goto_8
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->P:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 554
    if-lez v3, :cond_12

    const/4 v0, 0x1

    :goto_9
    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->Q:Z

    .line 555
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->Q:Z

    if-eqz v0, :cond_13

    .line 556
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->P:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_e

    .line 557
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->P:Landroid/view/View;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->P:Landroid/view/View;

    .line 558
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 557
    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/internal/android/widget/ToolBar;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 561
    :cond_e
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->P:Landroid/view/View;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v3, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0, v1, v6}, Landroid/view/View;->measure(II)V

    goto/16 :goto_0

    .line 520
    :cond_f
    const/4 v0, 0x0

    goto/16 :goto_6

    .line 521
    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_7

    .line 549
    :cond_11
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Landroid/widget/ImageView;

    if-eqz v0, :cond_d

    .line 550
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_8

    .line 554
    :cond_12
    const/4 v0, 0x0

    goto :goto_9

    .line 564
    :cond_13
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->P:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_14
    move v0, v3

    goto/16 :goto_5
.end method

.method public setContentWidth(I)V
    .locals 2

    .prologue
    .line 1073
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->K:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1074
    if-eqz v0, :cond_0

    .line 1075
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->M:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 1076
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->M:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->M:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->L:I

    .line 1080
    :goto_0
    return-void

    .line 1078
    :cond_0
    iput p1, p0, Lcom/twitter/internal/android/widget/ToolBar;->L:I

    goto :goto_0
.end method

.method public setCustomView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1136
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;)V

    .line 1137
    return-void
.end method

.method public setDisplayDrawerIcon(Z)V
    .locals 6

    .prologue
    const/16 v1, 0x8

    const/4 v0, 0x0

    .line 299
    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->aa:Z

    if-ne v2, p1, :cond_0

    .line 327
    :goto_0
    return-void

    .line 303
    :cond_0
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Lcom/twitter/internal/android/widget/DrawerToolBarItemView;

    if-nez v2, :cond_2

    .line 304
    new-instance v2, Lcom/twitter/internal/android/widget/DrawerToolBarItemView;

    new-instance v3, Landroid/view/ContextThemeWrapper;

    .line 305
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getThemedContext()Landroid/content/Context;

    move-result-object v4

    iget v5, p0, Lcom/twitter/internal/android/widget/ToolBar;->g:I

    invoke-direct {v3, v4, v5}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    const/4 v4, 0x0

    iget v5, p0, Lcom/twitter/internal/android/widget/ToolBar;->g:I

    invoke-direct {v2, v3, v4, v5}, Lcom/twitter/internal/android/widget/DrawerToolBarItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 306
    sget v3, Lazw$g;->drawer_icon:I

    invoke-virtual {v2, v3}, Lcom/twitter/internal/android/widget/DrawerToolBarItemView;->setId(I)V

    .line 307
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lazw$k;->modern_nav_drawer_icon:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/internal/android/widget/DrawerToolBarItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 308
    iget v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->a:I

    if-eqz v3, :cond_1

    .line 309
    iget v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->a:I

    invoke-virtual {v2, v3}, Lcom/twitter/internal/android/widget/DrawerToolBarItemView;->setBackgroundResource(I)V

    .line 311
    :cond_1
    new-instance v3, Lazv;

    invoke-direct {v3, p0}, Lazv;-><init>(Lcom/twitter/internal/android/widget/ToolBar;)V

    sget v4, Lazw$g;->drawer_icon:I

    invoke-virtual {v3, v4}, Lazv;->a(I)Lazv;

    move-result-object v3

    invoke-virtual {v3, v2}, Lazv;->a(Lcom/twitter/internal/android/widget/ToolBarItemView;)Lazv;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->B:Lazv;

    .line 312
    new-instance v3, Lcom/twitter/internal/android/widget/ToolBar$1;

    invoke-direct {v3, p0}, Lcom/twitter/internal/android/widget/ToolBar$1;-><init>(Lcom/twitter/internal/android/widget/ToolBar;)V

    invoke-virtual {v2, v3}, Lcom/twitter/internal/android/widget/DrawerToolBarItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 320
    iput-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Lcom/twitter/internal/android/widget/DrawerToolBarItemView;

    .line 321
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Lcom/twitter/internal/android/widget/DrawerToolBarItemView;

    invoke-virtual {v2, v1}, Lcom/twitter/internal/android/widget/DrawerToolBarItemView;->setVisibility(I)V

    .line 322
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Lcom/twitter/internal/android/widget/DrawerToolBarItemView;

    invoke-virtual {p0, v2, v0}, Lcom/twitter/internal/android/widget/ToolBar;->addView(Landroid/view/View;I)V

    .line 325
    :cond_2
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Lcom/twitter/internal/android/widget/DrawerToolBarItemView;

    if-eqz p1, :cond_3

    :goto_1
    invoke-virtual {v2, v0}, Lcom/twitter/internal/android/widget/DrawerToolBarItemView;->setVisibility(I)V

    .line 326
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/ToolBar;->aa:Z

    goto :goto_0

    :cond_3
    move v0, v1

    .line 325
    goto :goto_1
.end method

.method public setDisplayFullExpandEnabled(Z)V
    .locals 2

    .prologue
    .line 253
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->D:Z

    if-ne v0, p1, :cond_0

    .line 268
    :goto_0
    return-void

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    .line 257
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->G:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 258
    if-eqz p1, :cond_2

    .line 259
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v1}, Lcom/twitter/internal/android/widget/e;->getVisibility()I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->F:I

    .line 260
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/e;->d(Z)V

    .line 261
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/e;->setVisibility(I)V

    .line 267
    :cond_1
    :goto_1
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/ToolBar;->D:Z

    goto :goto_0

    .line 263
    :cond_2
    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->F:I

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/e;->setVisibility(I)V

    .line 264
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/e;->d(Z)V

    goto :goto_1
.end method

.method public setDisplayOptions(I)V
    .locals 0

    .prologue
    .line 1178
    iput p1, p0, Lcom/twitter/internal/android/widget/ToolBar;->R:I

    .line 1179
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->c()V

    .line 1180
    return-void
.end method

.method public setDisplayShowHomeAsUpEnabled(Z)V
    .locals 1

    .prologue
    .line 236
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->C:Z

    if-ne v0, p1, :cond_0

    .line 244
    :goto_0
    return-void

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-eqz v0, :cond_1

    .line 240
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/e;->a(Z)V

    .line 242
    :cond_1
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/ToolBar;->C:Z

    .line 243
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->b()V

    goto :goto_0
.end method

.method public setDisplayShowTitleEnabled(Z)V
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/e;->b(Z)V

    .line 250
    :cond_0
    return-void
.end method

.method public final setDrawerIcon(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->B:Lazv;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->B:Lazv;

    invoke-virtual {v0, p1}, Lazv;->a(Landroid/graphics/Bitmap;)Lazv;

    .line 358
    :cond_0
    return-void
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 348
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-nez v0, :cond_0

    .line 352
    :goto_0
    return-void

    .line 351
    :cond_0
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->a()Lcom/twitter/internal/android/widget/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/e;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setIgnoreExpandedItems(Z)V
    .locals 0

    .prologue
    .line 330
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/ToolBar;->ab:Z

    .line 331
    return-void
.end method

.method public setNumber(I)V
    .locals 1

    .prologue
    .line 412
    if-gtz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-nez v0, :cond_0

    .line 416
    :goto_0
    return-void

    .line 415
    :cond_0
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->a()Lcom/twitter/internal/android/widget/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/e;->b(I)V

    goto :goto_0
.end method

.method protected setOffsetLayoutTopPx(I)V
    .locals 0

    .prologue
    .line 207
    iput p1, p0, Lcom/twitter/internal/android/widget/ToolBar;->V:I

    .line 208
    return-void
.end method

.method public setOnToolBarItemSelectedListener(Lcmr$a;)V
    .locals 0

    .prologue
    .line 837
    iput-object p1, p0, Lcom/twitter/internal/android/widget/ToolBar;->s:Lcmr$a;

    .line 838
    return-void
.end method

.method public final setPadding(IIII)V
    .locals 0

    .prologue
    .line 215
    return-void
.end method

.method public final setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-nez v0, :cond_0

    .line 380
    :goto_0
    return-void

    .line 379
    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/e;->b(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setThemeId(I)V
    .locals 3

    .prologue
    .line 222
    iget v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->c:I

    if-eq v0, p1, :cond_0

    .line 223
    iput p1, p0, Lcom/twitter/internal/android/widget/ToolBar;->c:I

    .line 224
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->d:Landroid/content/Context;

    .line 225
    iget v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->c:I

    if-lez v0, :cond_0

    .line 226
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->b:Landroid/content/Context;

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->c:I

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->d:Landroid/content/Context;

    .line 229
    :cond_0
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 368
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-nez v0, :cond_0

    .line 372
    :goto_0
    return-void

    .line 371
    :cond_0
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->a()Lcom/twitter/internal/android/widget/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/e;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final setTitleDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    if-nez v0, :cond_0

    .line 409
    :goto_0
    return-void

    .line 408
    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/e;->c(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
