.class public Lcom/twitter/internal/android/widget/NavItemView;
.super Landroid/view/View;
.source "Twttr"

# interfaces
.implements Lcom/twitter/ui/widget/b;


# static fields
.field private static final a:Landroid/text/TextPaint;


# instance fields
.field private final b:F

.field private final c:Landroid/graphics/Point;

.field private final d:Landroid/graphics/Rect;

.field private final e:Lcom/twitter/ui/widget/i;

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:Lcom/twitter/ui/widget/a;

.field private final j:F

.field private k:Landroid/text/StaticLayout;

.field private l:Ljava/lang/String;

.field private m:Landroid/content/res/ColorStateList;

.field private n:I

.field private o:I

.field private p:I

.field private final q:I

.field private final r:I

.field private s:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    sput-object v0, Lcom/twitter/internal/android/widget/NavItemView;->a:Landroid/text/TextPaint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 71
    const/4 v0, 0x0

    sget v1, Lazw$c;->navItemStyle:I

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/internal/android/widget/NavItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 75
    sget v0, Lazw$c;->navItemStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/internal/android/widget/NavItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 79
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->c:Landroid/graphics/Point;

    .line 49
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->d:Landroid/graphics/Rect;

    .line 80
    sget-object v0, Lazw$l;->NavItemView:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 82
    sget v1, Lazw$l;->NavItemView_textColor:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->m:Landroid/content/res/ColorStateList;

    .line 83
    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->m:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_0

    .line 84
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/NavItemView;->a()V

    .line 86
    :cond_0
    sget v1, Lazw$l;->NavItemView_textSize:I

    const/high16 v2, 0x41900000    # 18.0f

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->b:F

    .line 87
    invoke-static {p1}, Lcom/twitter/ui/widget/i;->a(Landroid/content/Context;)Lcom/twitter/ui/widget/i;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->e:Lcom/twitter/ui/widget/i;

    .line 88
    sget v1, Lazw$l;->NavItemView_displayMode:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->h:I

    .line 89
    sget v1, Lazw$l;->NavItemView_textStyle:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->f:I

    .line 90
    sget v1, Lazw$l;->NavItemView_selectedTextStyle:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->g:I

    .line 91
    new-instance v1, Lcom/twitter/ui/widget/a;

    sget v2, Lazw$l;->NavItemView_badgeIndicatorStyle:I

    .line 92
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-direct {v1, p0, p1, v2}, Lcom/twitter/ui/widget/a;-><init>(Landroid/view/View;Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->i:Lcom/twitter/ui/widget/a;

    .line 94
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 95
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lazw$e;->standard_header_letter_spacing:I

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 96
    invoke-virtual {v1}, Landroid/util/TypedValue;->getFloat()F

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->j:F

    .line 97
    invoke-static {p1}, Lcnh;->a(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->q:I

    .line 98
    sget v1, Lazw$l;->NavItemView_unselectedIconTint:I

    iget v2, p0, Lcom/twitter/internal/android/widget/NavItemView;->q:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->r:I

    .line 99
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 100
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 103
    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->m:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getDrawableState()[I

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    .line 104
    iget v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->n:I

    if-eq v0, v1, :cond_0

    .line 105
    iput v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->n:I

    .line 107
    :cond_0
    return-void
.end method

.method private b()Landroid/text/TextPaint;
    .locals 3

    .prologue
    .line 149
    sget-object v0, Lcom/twitter/internal/android/widget/NavItemView;->a:Landroid/text/TextPaint;

    .line 150
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 151
    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->e:Lcom/twitter/ui/widget/i;

    iget v2, p0, Lcom/twitter/internal/android/widget/NavItemView;->g:I

    invoke-virtual {v1, v2}, Lcom/twitter/ui/widget/i;->a(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 152
    iget v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->n:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 157
    :goto_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 158
    iget v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->j:F

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setLetterSpacing(F)V

    .line 160
    :cond_0
    iget v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->b:F

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 161
    return-object v0

    .line 154
    :cond_1
    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->e:Lcom/twitter/ui/widget/i;

    iget v2, p0, Lcom/twitter/internal/android/widget/NavItemView;->f:I

    invoke-virtual {v1, v2}, Lcom/twitter/ui/widget/i;->a(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 155
    iget v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->r:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_0
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 1

    .prologue
    .line 176
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 177
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/NavItemView;->a()V

    .line 178
    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->i:Lcom/twitter/ui/widget/a;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/a;->b()V

    .line 179
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 240
    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->s:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->h:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 241
    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->q:I

    :goto_0
    invoke-static {v1, v0}, Lcnd;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 243
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 252
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->i:Lcom/twitter/ui/widget/a;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/a;->a(Landroid/graphics/Canvas;)V

    .line 253
    return-void

    .line 241
    :cond_1
    iget v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->r:I

    goto :goto_0

    .line 244
    :cond_2
    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->k:Landroid/text/StaticLayout;

    if-eqz v0, :cond_0

    .line 245
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/NavItemView;->b()Landroid/text/TextPaint;

    .line 246
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    move-result v0

    .line 247
    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->c:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/twitter/internal/android/widget/NavItemView;->c:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 248
    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->k:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 249
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    .line 203
    const/4 v6, 0x0

    .line 204
    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->s:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->h:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 205
    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 206
    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 207
    sub-int v2, p4, p2

    invoke-static {v2, v1}, Lcom/twitter/util/ui/k;->a(II)I

    move-result v2

    .line 208
    sub-int v3, p5, p3

    invoke-static {v3, v0}, Lcom/twitter/util/ui/k;->a(II)I

    move-result v3

    .line 209
    add-int v7, v3, v0

    .line 210
    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->s:Landroid/graphics/drawable/Drawable;

    add-int/2addr v1, v2

    invoke-virtual {v0, v2, v3, v1, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 211
    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    .line 235
    :goto_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->i:Lcom/twitter/ui/widget/a;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/ui/widget/a;->a(ZIIIILandroid/graphics/Rect;I)V

    .line 236
    return-void

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->k:Landroid/text/StaticLayout;

    .line 215
    if-eqz v0, :cond_1

    .line 216
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/NavItemView;->b()Landroid/text/TextPaint;

    move-result-object v1

    .line 217
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingLeft()I

    move-result v2

    .line 218
    sub-int v3, p4, p2

    sub-int/2addr v3, v2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    .line 219
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getWidth()I

    move-result v4

    .line 218
    invoke-static {v3, v4}, Lcom/twitter/util/ui/k;->a(II)I

    move-result v3

    add-int/2addr v3, v2

    .line 220
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingTop()I

    move-result v4

    .line 221
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    .line 222
    sub-int v6, p5, p3

    sub-int/2addr v6, v4

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-static {v6, v5}, Lcom/twitter/util/ui/k;->a(II)I

    move-result v6

    add-int/2addr v6, v4

    .line 224
    iget-object v7, p0, Lcom/twitter/internal/android/widget/NavItemView;->c:Landroid/graphics/Point;

    invoke-virtual {v7, v3, v6}, Landroid/graphics/Point;->set(II)V

    .line 226
    invoke-virtual {v1}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v1

    .line 227
    iget v7, v1, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget v8, v1, Landroid/graphics/Paint$FontMetricsInt;->top:I

    sub-int/2addr v7, v8

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    sub-int v1, v7, v1

    add-int v7, v6, v1

    .line 228
    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->d:Landroid/graphics/Rect;

    sub-int v4, v6, v4

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getWidth()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int v2, v6, v5

    invoke-virtual {v1, v3, v4, v0, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 229
    iget-object v6, p0, Lcom/twitter/internal/android/widget/NavItemView;->d:Landroid/graphics/Rect;

    goto :goto_0

    .line 231
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingTop()I

    move-result v7

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 4

    .prologue
    .line 183
    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->k:Landroid/text/StaticLayout;

    .line 184
    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->s:Landroid/graphics/drawable/Drawable;

    .line 185
    if-eqz v1, :cond_0

    iget v2, p0, Lcom/twitter/internal/android/widget/NavItemView;->h:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 187
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-static {v0, p1}, Lcom/twitter/internal/android/widget/NavItemView;->getDefaultSize(II)I

    move-result v0

    .line 188
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-static {v1, p2}, Lcom/twitter/internal/android/widget/NavItemView;->getDefaultSize(II)I

    move-result v1

    .line 186
    invoke-virtual {p0, v0, v1}, Lcom/twitter/internal/android/widget/NavItemView;->setMeasuredDimension(II)V

    .line 197
    :goto_0
    iput p1, p0, Lcom/twitter/internal/android/widget/NavItemView;->o:I

    .line 198
    return-void

    .line 189
    :cond_0
    if-eqz v0, :cond_1

    .line 190
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    .line 191
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v0, v2

    .line 192
    invoke-static {v1, p1}, Lcom/twitter/internal/android/widget/NavItemView;->getDefaultSize(II)I

    move-result v1

    .line 193
    invoke-static {v0, p2}, Lcom/twitter/internal/android/widget/NavItemView;->getDefaultSize(II)I

    move-result v0

    .line 192
    invoke-virtual {p0, v1, v0}, Lcom/twitter/internal/android/widget/NavItemView;->setMeasuredDimension(II)V

    goto :goto_0

    .line 195
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    goto :goto_0
.end method

.method public setBadgeMode(I)V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->i:Lcom/twitter/ui/widget/a;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/a;->setBadgeMode(I)V

    .line 112
    return-void
.end method

.method public setBadgeNumber(I)V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->i:Lcom/twitter/ui/widget/a;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/a;->setBadgeNumber(I)V

    .line 167
    return-void
.end method

.method public setIconResource(I)V
    .locals 2

    .prologue
    .line 261
    iget v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->h:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->p:I

    if-ne p1, v0, :cond_1

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 265
    iput p1, p0, Lcom/twitter/internal/android/widget/NavItemView;->p:I

    .line 266
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->s:Landroid/graphics/drawable/Drawable;

    .line 267
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->requestLayout()V

    .line 268
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->invalidate()V

    goto :goto_0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 116
    if-nez p1, :cond_2

    .line 117
    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->l:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 118
    :goto_0
    iput-object v3, p0, Lcom/twitter/internal/android/widget/NavItemView;->l:Ljava/lang/String;

    .line 125
    :goto_1
    if-eqz v0, :cond_0

    .line 126
    if-eqz p1, :cond_5

    .line 127
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/NavItemView;->b()Landroid/text/TextPaint;

    move-result-object v4

    .line 128
    invoke-static {p1, v4}, Lcom/twitter/util/ui/k;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v5

    .line 129
    iget v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->o:I

    invoke-static {v0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 131
    if-nez v0, :cond_4

    .line 132
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    add-int v11, v0, v5

    .line 137
    :goto_2
    new-instance v0, Landroid/text/StaticLayout;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    sget-object v10, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object v1, p1

    move v9, v2

    invoke-direct/range {v0 .. v11}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->k:Landroid/text/StaticLayout;

    .line 143
    :goto_3
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->requestLayout()V

    .line 144
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->invalidate()V

    .line 146
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 117
    goto :goto_0

    .line 119
    :cond_2
    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->l:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 121
    iput-object p1, p0, Lcom/twitter/internal/android/widget/NavItemView;->l:Ljava/lang/String;

    goto :goto_1

    :cond_3
    move v0, v2

    .line 123
    goto :goto_1

    .line 134
    :cond_4
    iget v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->o:I

    invoke-static {v0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 135
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingRight()I

    move-result v1

    sub-int v11, v0, v1

    goto :goto_2

    .line 141
    :cond_5
    iput-object v3, p0, Lcom/twitter/internal/android/widget/NavItemView;->k:Landroid/text/StaticLayout;

    goto :goto_3
.end method

.method public setTextColor(I)V
    .locals 1

    .prologue
    .line 272
    iget v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->n:I

    if-eq v0, p1, :cond_0

    if-eqz p1, :cond_0

    .line 273
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->m:Landroid/content/res/ColorStateList;

    .line 274
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/NavItemView;->a()V

    .line 276
    :cond_0
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->i:Lcom/twitter/ui/widget/a;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/a;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
