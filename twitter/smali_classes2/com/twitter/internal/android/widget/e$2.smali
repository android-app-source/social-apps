.class Lcom/twitter/internal/android/widget/e$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/internal/android/widget/e;->a(ZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/internal/android/widget/e;


# direct methods
.method constructor <init>(Lcom/twitter/internal/android/widget/e;)V
    .locals 0

    .prologue
    .line 433
    iput-object p1, p0, Lcom/twitter/internal/android/widget/e$2;->a:Lcom/twitter/internal/android/widget/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .prologue
    .line 436
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 437
    iget-object v1, p0, Lcom/twitter/internal/android/widget/e$2;->a:Lcom/twitter/internal/android/widget/e;

    iget-object v2, p0, Lcom/twitter/internal/android/widget/e$2;->a:Lcom/twitter/internal/android/widget/e;

    invoke-static {v2}, Lcom/twitter/internal/android/widget/e;->b(Lcom/twitter/internal/android/widget/e;)I

    move-result v2

    invoke-static {v2, v0}, Lcom/twitter/util/ui/g;->a(II)I

    move-result v2

    invoke-static {v1, v2}, Lcom/twitter/internal/android/widget/e;->a(Lcom/twitter/internal/android/widget/e;I)I

    .line 438
    iget-object v1, p0, Lcom/twitter/internal/android/widget/e$2;->a:Lcom/twitter/internal/android/widget/e;

    iget-object v2, p0, Lcom/twitter/internal/android/widget/e$2;->a:Lcom/twitter/internal/android/widget/e;

    invoke-static {v2}, Lcom/twitter/internal/android/widget/e;->c(Lcom/twitter/internal/android/widget/e;)I

    move-result v2

    invoke-static {v2, v0}, Lcom/twitter/util/ui/g;->a(II)I

    move-result v0

    invoke-static {v1, v0}, Lcom/twitter/internal/android/widget/e;->b(Lcom/twitter/internal/android/widget/e;I)I

    .line 439
    iget-object v0, p0, Lcom/twitter/internal/android/widget/e$2;->a:Lcom/twitter/internal/android/widget/e;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/e;->invalidate()V

    .line 440
    return-void
.end method
