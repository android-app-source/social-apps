.class Lcom/twitter/internal/android/widget/FlowLayoutManager$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/internal/android/widget/FlowLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/twitter/internal/android/widget/FlowLayoutManager$a;",
            ">;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 482
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/FlowLayoutManager$b;->a:Ljava/util/Map;

    .line 483
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/internal/android/widget/FlowLayoutManager$b;->b:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/internal/android/widget/FlowLayoutManager$1;)V
    .locals 0

    .prologue
    .line 481
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/FlowLayoutManager$b;-><init>()V

    return-void
.end method


# virtual methods
.method a(I)Lcom/twitter/internal/android/widget/FlowLayoutManager$a;
    .locals 2

    .prologue
    .line 486
    iget-object v0, p0, Lcom/twitter/internal/android/widget/FlowLayoutManager$b;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/FlowLayoutManager$a;

    return-object v0
.end method

.method a()V
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Lcom/twitter/internal/android/widget/FlowLayoutManager$b;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 499
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/FlowLayoutManager$b;->d(I)V

    .line 500
    return-void
.end method

.method a(ILcom/twitter/internal/android/widget/FlowLayoutManager$a;)V
    .locals 2

    .prologue
    .line 490
    iget-object v0, p0, Lcom/twitter/internal/android/widget/FlowLayoutManager$b;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 491
    return-void
.end method

.method b(I)Z
    .locals 2

    .prologue
    .line 494
    iget-object v0, p0, Lcom/twitter/internal/android/widget/FlowLayoutManager$b;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method c(I)V
    .locals 2

    .prologue
    .line 503
    iget-object v0, p0, Lcom/twitter/internal/android/widget/FlowLayoutManager$b;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 504
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 505
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 506
    if-lt v0, p1, :cond_0

    .line 507
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 510
    :cond_1
    return-void
.end method

.method d(I)V
    .locals 1

    .prologue
    .line 513
    iget v0, p0, Lcom/twitter/internal/android/widget/FlowLayoutManager$b;->b:I

    if-eq v0, p1, :cond_0

    .line 514
    iput p1, p0, Lcom/twitter/internal/android/widget/FlowLayoutManager$b;->b:I

    .line 515
    iget-object v0, p0, Lcom/twitter/internal/android/widget/FlowLayoutManager$b;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 517
    :cond_0
    return-void
.end method
