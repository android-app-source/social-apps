.class Lcom/twitter/internal/android/widget/FlowLayoutManager$c;
.super Landroid/support/v7/widget/RecyclerView$LayoutParams;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/internal/android/widget/FlowLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# instance fields
.field public final a:Z


# direct methods
.method constructor <init>(II)V
    .locals 1

    .prologue
    .line 569
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(II)V

    .line 570
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/FlowLayoutManager$c;->a:Z

    .line 571
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 562
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 563
    sget-object v0, Lazw$l;->FlowLayoutManagerLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 564
    sget v1, Lazw$l;->FlowLayoutManagerLayout_ignoreParentPadding:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/FlowLayoutManager$c;->a:Z

    .line 565
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 566
    return-void
.end method

.method constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 574
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 575
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/FlowLayoutManager$c;->a:Z

    .line 576
    return-void
.end method
