.class public Lcom/twitter/internal/android/widget/ToolBarItemView;
.super Landroid/view/View;
.source "Twttr"

# interfaces
.implements Lcom/twitter/ui/widget/b;


# static fields
.field private static final c:[I

.field private static final d:Landroid/text/TextPaint;


# instance fields
.field protected a:Landroid/graphics/drawable/Drawable;

.field protected b:I

.field private final e:I

.field private final f:Landroid/graphics/Point;

.field private final g:I

.field private h:F

.field private i:Landroid/graphics/Typeface;

.field private j:Landroid/content/res/ColorStateList;

.field private k:Landroid/text/StaticLayout;

.field private l:I

.field private m:Z

.field private n:I

.field private o:Ljava/lang/String;

.field private final p:Lcom/twitter/ui/widget/a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 32
    new-array v0, v3, [I

    const/4 v1, 0x0

    sget v2, Lazw$c;->state_numbered:I

    aput v2, v0, v1

    sput-object v0, Lcom/twitter/internal/android/widget/ToolBarItemView;->c:[I

    .line 36
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0, v3}, Landroid/text/TextPaint;-><init>(I)V

    sput-object v0, Lcom/twitter/internal/android/widget/ToolBarItemView;->d:Landroid/text/TextPaint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 57
    const/4 v0, 0x0

    sget v1, Lazw$c;->toolBarItemStyle:I

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/internal/android/widget/ToolBarItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 61
    sget v0, Lazw$c;->toolBarItemStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/internal/android/widget/ToolBarItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 65
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->f:Landroid/graphics/Point;

    .line 66
    sget-object v0, Lazw$l;->ToolBarItemView:[I

    invoke-virtual {p1, p2, v0, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 68
    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    iput v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->e:I

    .line 69
    new-instance v1, Lcom/twitter/ui/widget/a;

    sget v2, Lazw$l;->ToolBarItemView_badgeIndicatorStyle:I

    .line 70
    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-direct {v1, p0, p1, v2}, Lcom/twitter/ui/widget/a;-><init>(Landroid/view/View;Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->p:Lcom/twitter/ui/widget/a;

    .line 71
    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setTextAppearance(Landroid/content/res/TypedArray;)V

    .line 72
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 74
    invoke-static {p1}, Lcnh;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->g:I

    .line 75
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->j:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getDrawableState()[I

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    .line 79
    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->n:I

    if-eq v0, v1, :cond_0

    .line 80
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setTextColor(I)V

    .line 82
    :cond_0
    return-void
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->k:Landroid/text/StaticLayout;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->m:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setTextAppearance(Landroid/content/res/TypedArray;)V
    .locals 2

    .prologue
    .line 260
    sget v0, Lazw$l;->ToolBarItemView_textSize:I

    const/high16 v1, 0x41600000    # 14.0f

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:F

    .line 261
    sget v0, Lazw$l;->ToolBarItemView_textColor:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->j:Landroid/content/res/ColorStateList;

    .line 262
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->i:Landroid/graphics/Typeface;

    .line 263
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->j:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_0

    .line 264
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->a()V

    .line 268
    :goto_0
    return-void

    .line 266
    :cond_0
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->n:I

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 254
    sget-object v0, Lazw$l;->ToolBarItemView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 255
    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setTextAppearance(Landroid/content/res/TypedArray;)V

    .line 256
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 257
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;Z)V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    if-ne v0, p1, :cond_0

    .line 223
    :goto_0
    return-void

    .line 220
    :cond_0
    if-eqz p2, :cond_1

    iget v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->g:I

    invoke-static {p1, v0}, Lcnd;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    :cond_1
    iput-object p1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    .line 221
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->requestLayout()V

    .line 222
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 165
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 166
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 169
    :cond_0
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->a()V

    .line 170
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 155
    sget-object v0, Lcom/twitter/internal/android/widget/ToolBarItemView;->c:[I

    array-length v0, v0

    add-int/2addr v0, p1

    invoke-super {p0, v0}, Landroid/view/View;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 157
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->p:Lcom/twitter/ui/widget/a;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/a;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 158
    sget-object v1, Lcom/twitter/internal/android/widget/ToolBarItemView;->c:[I

    invoke-static {v0, v1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->mergeDrawableStates([I[I)[I

    .line 160
    :cond_0
    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 272
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 273
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->p:Lcom/twitter/ui/widget/a;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/a;->c()V

    .line 274
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 135
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 136
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 139
    :cond_0
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 140
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    move-result v0

    .line 141
    sget-object v1, Lcom/twitter/internal/android/widget/ToolBarItemView;->d:Landroid/text/TextPaint;

    .line 142
    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:F

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 143
    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->n:I

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 144
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->i:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 145
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->f:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->f:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 146
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->k:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 147
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->p:Lcom/twitter/ui/widget/a;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/a;->a(Landroid/graphics/Canvas;)V

    .line 151
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    .prologue
    .line 104
    sub-int v0, p4, p2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 105
    sub-int v3, p5, p3

    .line 106
    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->l:I

    invoke-static {v0, v1}, Lcom/twitter/util/ui/k;->a(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    const/4 v7, 0x0

    .line 108
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    sget-object v1, Lcom/twitter/internal/android/widget/ToolBarItemView;->d:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v1

    .line 111
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->f:Landroid/graphics/Point;

    iget v4, v1, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget v5, v1, Landroid/graphics/Paint$FontMetricsInt;->top:I

    sub-int/2addr v4, v5

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    sub-int v1, v4, v1

    invoke-static {v1, v3}, Lcom/twitter/util/ui/k;->a(II)I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 112
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->k:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getWidth()I

    move-result v1

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->e:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 114
    :cond_0
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 115
    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->b:I

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    .line 116
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->b:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 117
    :goto_0
    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->b:I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    .line 118
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget v4, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->b:I

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 119
    :goto_1
    invoke-static {v3, v1}, Lcom/twitter/util/ui/k;->a(II)I

    move-result v3

    .line 120
    iget-object v4, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    add-int/2addr v2, v0

    add-int v5, v3, v1

    invoke-virtual {v4, v0, v3, v2, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 121
    add-int v7, v3, v1

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->p:Lcom/twitter/ui/widget/a;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    .line 125
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    :goto_2
    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    .line 124
    invoke-virtual/range {v0 .. v7}, Lcom/twitter/ui/widget/a;->a(ZIIIILandroid/graphics/Rect;I)V

    .line 126
    return-void

    .line 116
    :cond_2
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    goto :goto_0

    .line 118
    :cond_3
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    goto :goto_1

    .line 125
    :cond_4
    const/4 v6, 0x0

    goto :goto_2
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 86
    const/4 v0, 0x0

    .line 87
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 88
    iget v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->b:I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->b:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 90
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 91
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 92
    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->e:I

    add-int/2addr v0, v1

    .line 94
    :cond_1
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->k:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    :cond_2
    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->l:I

    .line 97
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getSuggestedMinimumWidth()I

    move-result v1

    .line 97
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getDefaultSize(II)I

    move-result v0

    .line 99
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getSuggestedMinimumHeight()I

    move-result v1

    invoke-static {v1, p2}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getDefaultSize(II)I

    move-result v1

    .line 97
    invoke-virtual {p0, v0, v1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setMeasuredDimension(II)V

    .line 100
    return-void

    .line 88
    :cond_3
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    goto :goto_0
.end method

.method public setBadgeMode(I)V
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->p:Lcom/twitter/ui/widget/a;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/a;->setBadgeMode(I)V

    .line 284
    return-void
.end method

.method public setBadgeNumber(I)V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->p:Lcom/twitter/ui/widget/a;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/a;->setBadgeNumber(I)V

    .line 279
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 226
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->a(Landroid/graphics/drawable/Drawable;Z)V

    .line 227
    return-void
.end method

.method public setImageResource(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 210
    if-nez p1, :cond_0

    .line 214
    :goto_0
    return-void

    .line 213
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->a(Landroid/graphics/drawable/Drawable;Z)V

    goto :goto_0
.end method

.method public setLabel(Ljava/lang/CharSequence;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 181
    if-nez p1, :cond_3

    .line 182
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->k:Landroid/text/StaticLayout;

    if-eqz v0, :cond_0

    move v7, v8

    .line 183
    :cond_0
    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->k:Landroid/text/StaticLayout;

    .line 184
    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->o:Ljava/lang/String;

    .line 203
    :cond_1
    :goto_0
    if-eqz v7, :cond_2

    .line 204
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->requestLayout()V

    .line 205
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->invalidate()V

    .line 207
    :cond_2
    return-void

    .line 186
    :cond_3
    invoke-static {p1}, Lcom/twitter/util/b;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 187
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->k:Landroid/text/StaticLayout;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->o:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 189
    :cond_4
    sget-object v0, Lcom/twitter/internal/android/widget/ToolBarItemView;->d:Landroid/text/TextPaint;

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:F

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 190
    sget-object v0, Lcom/twitter/internal/android/widget/ToolBarItemView;->d:Landroid/text/TextPaint;

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->i:Landroid/graphics/Typeface;

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 195
    sget-object v0, Lcom/twitter/internal/android/widget/ToolBarItemView;->d:Landroid/text/TextPaint;

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->g:I

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 196
    new-instance v0, Landroid/text/StaticLayout;

    sget-object v2, Lcom/twitter/internal/android/widget/ToolBarItemView;->d:Landroid/text/TextPaint;

    sget-object v3, Lcom/twitter/internal/android/widget/ToolBarItemView;->d:Landroid/text/TextPaint;

    invoke-static {v1, v3}, Lcom/twitter/util/ui/k;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->k:Landroid/text/StaticLayout;

    .line 198
    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->o:Ljava/lang/String;

    move v7, v8

    goto :goto_0
.end method

.method public setLabelResource(I)V
    .locals 1

    .prologue
    .line 173
    if-nez p1, :cond_0

    .line 177
    :goto_0
    return-void

    .line 176
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setLabel(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setMaxIconSize(I)V
    .locals 0

    .prologue
    .line 287
    iput p1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->b:I

    .line 288
    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->n:I

    if-eq p1, v0, :cond_1

    const/4 v0, 0x1

    .line 240
    :goto_0
    iput p1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->n:I

    .line 241
    if-eqz v0, :cond_0

    .line 242
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->invalidate()V

    .line 244
    :cond_0
    return-void

    .line 239
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setWithText(Z)V
    .locals 1

    .prologue
    .line 230
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->m:Z

    if-eq p1, v0, :cond_1

    const/4 v0, 0x1

    .line 231
    :goto_0
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->m:Z

    .line 232
    if-eqz v0, :cond_0

    .line 233
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->requestLayout()V

    .line 234
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->invalidate()V

    .line 236
    :cond_0
    return-void

    .line 230
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->p:Lcom/twitter/ui/widget/a;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/a;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
