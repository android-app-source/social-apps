.class Lcom/twitter/internal/android/widget/ToolBar$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/internal/android/widget/ToolBar;->a()Lcom/twitter/internal/android/widget/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lazv;

.field final synthetic b:Lcom/twitter/internal/android/widget/ToolBar;


# direct methods
.method constructor <init>(Lcom/twitter/internal/android/widget/ToolBar;Lazv;)V
    .locals 0

    .prologue
    .line 430
    iput-object p1, p0, Lcom/twitter/internal/android/widget/ToolBar$2;->b:Lcom/twitter/internal/android/widget/ToolBar;

    iput-object p2, p0, Lcom/twitter/internal/android/widget/ToolBar$2;->a:Lazv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 433
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar$2;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/ToolBar;->c(Lcom/twitter/internal/android/widget/ToolBar;)Lcom/twitter/internal/android/widget/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/e;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar$2;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/ToolBar;->d(Lcom/twitter/internal/android/widget/ToolBar;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 434
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar$2;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/ToolBar;->e(Lcom/twitter/internal/android/widget/ToolBar;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazv;

    .line 435
    invoke-virtual {v0}, Lazv;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 436
    invoke-virtual {v0}, Lazv;->g()Z

    .line 449
    :cond_1
    :goto_0
    return-void

    .line 440
    :cond_2
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar$2;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/ToolBar;->f(Lcom/twitter/internal/android/widget/ToolBar;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazv;

    .line 441
    invoke-virtual {v0}, Lazv;->h()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 442
    invoke-virtual {v0}, Lazv;->g()Z

    goto :goto_0

    .line 446
    :cond_4
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar$2;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(Lcom/twitter/internal/android/widget/ToolBar;)Lcmr$a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 447
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar$2;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(Lcom/twitter/internal/android/widget/ToolBar;)Lcmr$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar$2;->a:Lazv;

    invoke-interface {v0, v1}, Lcmr$a;->a(Lcmm;)Z

    goto :goto_0
.end method
