.class public Lcom/twitter/internal/android/widget/d;
.super Lcom/twitter/internal/android/widget/c;
.source "Twttr"


# direct methods
.method public constructor <init>(II)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    neg-int v0, p1

    .line 24
    :goto_0
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v1

    :goto_1
    neg-int v3, p2

    .line 23
    invoke-direct {p0, v0, v2, v1, v3}, Lcom/twitter/internal/android/widget/c;-><init>(IIII)V

    .line 26
    return-void

    :cond_0
    move v0, v1

    .line 23
    goto :goto_0

    .line 24
    :cond_1
    neg-int v2, p1

    goto :goto_1
.end method


# virtual methods
.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$State;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 31
    invoke-virtual {p3, p2}, Landroid/support/v7/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_0

    .line 32
    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/internal/android/widget/c;->getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$State;)V

    .line 36
    :goto_0
    return-void

    .line 34
    :cond_0
    invoke-virtual {p1, v2, v2, v2, v2}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method
