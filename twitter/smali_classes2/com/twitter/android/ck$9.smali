.class Lcom/twitter/android/ck$9;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/client/Session;

.field final synthetic b:Lcom/twitter/model/core/Tweet;

.field final synthetic c:Lcom/twitter/android/ck;


# direct methods
.method constructor <init>(Lcom/twitter/android/ck;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 524
    iput-object p1, p0, Lcom/twitter/android/ck$9;->c:Lcom/twitter/android/ck;

    iput-object p2, p0, Lcom/twitter/android/ck$9;->a:Lcom/twitter/library/client/Session;

    iput-object p3, p0, Lcom/twitter/android/ck$9;->b:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 4

    .prologue
    .line 528
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    .line 529
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ck$9;->c:Lcom/twitter/android/ck;

    iget-object v1, v1, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/ck$9;->a:Lcom/twitter/library/client/Session;

    iget-object v3, p0, Lcom/twitter/android/ck$9;->b:Lcom/twitter/model/core/Tweet;

    .line 530
    invoke-static {v1, v2, v3}, Lbhb;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/Tweet;)Lbhb;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/ck$9$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/ck$9$1;-><init>(Lcom/twitter/android/ck$9;)V

    .line 529
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 542
    iget-object v0, p0, Lcom/twitter/android/ck$9;->c:Lcom/twitter/android/ck;

    const-string/jumbo v1, "delete"

    iget-object v2, p0, Lcom/twitter/android/ck$9;->b:Lcom/twitter/model/core/Tweet;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/ck;->a(Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 544
    :cond_0
    return-void
.end method
