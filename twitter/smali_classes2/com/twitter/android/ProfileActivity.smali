.class public Lcom/twitter/android/ProfileActivity;
.super Lcom/twitter/android/ScrollingHeaderUserQueryActivity;
.source "Twttr"

# interfaces
.implements Landroid/accounts/OnAccountsUpdateListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/ads/a;
.implements Lcom/twitter/android/businessprofiles/c;
.implements Lcom/twitter/android/profiles/HeaderImageView$a;
.implements Lcom/twitter/android/profiles/c$a;
.implements Lcom/twitter/android/profiles/j$a;
.implements Lcom/twitter/android/profiles/o$a;
.implements Lcom/twitter/android/profiles/q$a;
.implements Lcom/twitter/android/profiles/t$a;
.implements Lcom/twitter/android/profiles/t$b;
.implements Lcom/twitter/app/common/dialog/b$d;
.implements Lcom/twitter/app/profile/ProfileTimelinesFragment$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/ProfileActivity$c;,
        Lcom/twitter/android/ProfileActivity$f;,
        Lcom/twitter/android/ProfileActivity$g;,
        Lcom/twitter/android/ProfileActivity$b;,
        Lcom/twitter/android/ProfileActivity$e;,
        Lcom/twitter/android/ProfileActivity$d;,
        Lcom/twitter/android/ProfileActivity$a;,
        Lcom/twitter/android/ProfileActivity$DisplayState;
    }
.end annotation


# static fields
.field public static final a:Landroid/net/Uri;

.field public static final b:Landroid/net/Uri;

.field public static final c:Landroid/net/Uri;

.field public static final d:Landroid/net/Uri;

.field public static final e:Landroid/net/Uri;

.field public static final f:Landroid/net/Uri;

.field public static final g:Landroid/net/Uri;

.field public static final h:Landroid/net/Uri;

.field public static final i:Landroid/net/Uri;

.field public static final j:Landroid/net/Uri;

.field public static final k:Landroid/net/Uri;

.field public static final l:Landroid/net/Uri;


# instance fields
.field private K:Lcom/twitter/android/ProfileActivity$f;

.field private L:Lcom/twitter/android/profiles/l;

.field private M:Lcom/twitter/android/profiles/ProfileDetailsViewManager;

.field private N:Lcom/twitter/android/profiles/HeaderImageView;

.field private O:Lcom/twitter/media/ui/image/UserImageView;

.field private P:Lcom/twitter/android/profiles/a;

.field private Q:Lcom/twitter/android/widget/TweetStatView;

.field private R:Lcom/twitter/android/widget/TweetStatView;

.field private S:Ljava/lang/String;

.field private T:Landroid/view/ViewGroup;

.field private U:F

.field private V:Ljava/lang/String;

.field private W:Landroid/widget/LinearLayout;

.field private X:I

.field private Y:Z

.field private Z:I

.field private aA:Landroid/view/View;

.field private aB:Lcom/twitter/android/metrics/d;

.field private aC:Z

.field private aD:Z

.field private aE:Ljava/lang/String;

.field private aF:Z

.field private aG:Lcom/twitter/android/profiles/d;

.field private aH:Lcom/twitter/android/profiles/t;

.field private aI:Lcom/twitter/android/profiles/j;

.field private aJ:Lafm;

.field private final aK:Z

.field private aL:Z

.field private aM:Landroid/widget/TextView;

.field private aN:Lazv;

.field private aO:Lazv;

.field private aP:Lcom/twitter/android/profiles/animation/BalloonSetAnimationView;

.field private aQ:Z

.field private aR:Lcom/twitter/model/ads/b;

.field private aS:Z

.field private aT:Landroid/view/ViewGroup;

.field private aU:Lcom/twitter/ui/widget/TypefacesTextView;

.field private aV:Lcom/twitter/model/businessprofiles/d;

.field private aW:Landroid/widget/FrameLayout;

.field private aX:Lcmw;

.field private aY:Lcom/twitter/media/model/MediaFile;

.field private aZ:Lcom/twitter/android/ProfileActivity$c;

.field private aa:Ljava/lang/String;

.field private ab:Z

.field private ac:Z

.field private ad:Lcom/twitter/model/util/FriendshipCache;

.field private ae:Lcgi;

.field private af:Z

.field private ag:Landroid/content/SharedPreferences;

.field private ah:Lcom/twitter/model/core/TwitterUser;

.field private ai:Landroid/net/Uri;

.field private aj:Z

.field private ak:Lcom/twitter/android/ScrollingHeaderActivity$a;

.field private al:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private am:Lcom/twitter/model/timeline/r;

.field private an:Lcom/twitter/android/bz;

.field private ao:Landroid/widget/ListView;

.field private ap:Landroid/widget/LinearLayout;

.field private aq:I

.field private ar:Landroid/view/View;

.field private as:Landroid/view/ViewGroup;

.field private at:Lcom/twitter/library/service/t;

.field private au:Lcom/twitter/android/ProfileActivity$e;

.field private av:Lcom/twitter/android/profiles/o;

.field private aw:Z

.field private ax:Z

.field private ay:Lcom/twitter/android/ProfileActivity$DisplayState;

.field private az:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 217
    const-string/jumbo v0, "twitter://profile/profile_without_replies"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/ProfileActivity;->a:Landroid/net/Uri;

    .line 218
    const-string/jumbo v0, "twitter://profile/tweets"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/ProfileActivity;->b:Landroid/net/Uri;

    .line 219
    const-string/jumbo v0, "twitter://profile/media"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/ProfileActivity;->c:Landroid/net/Uri;

    .line 220
    const-string/jumbo v0, "twitter://profile/favorites"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/ProfileActivity;->d:Landroid/net/Uri;

    .line 221
    const-string/jumbo v0, "twitter://profile/protected_account"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/ProfileActivity;->e:Landroid/net/Uri;

    .line 222
    const-string/jumbo v0, "twitter://profile/blocked_account"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/ProfileActivity;->f:Landroid/net/Uri;

    .line 223
    const-string/jumbo v0, "twitter://profile/blocker_interstitial"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/ProfileActivity;->g:Landroid/net/Uri;

    .line 224
    const-string/jumbo v0, "twitter://profile/followers"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/ProfileActivity;->h:Landroid/net/Uri;

    .line 225
    const-string/jumbo v0, "twitter://profile/following"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/ProfileActivity;->i:Landroid/net/Uri;

    .line 226
    const-string/jumbo v0, "twitter://profile/follow"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/ProfileActivity;->j:Landroid/net/Uri;

    .line 229
    const-string/jumbo v0, "twitter://profile/device_follow"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/ProfileActivity;->k:Landroid/net/Uri;

    .line 232
    const-string/jumbo v0, "twitter://profile/vine/enable_display"

    .line 233
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/ProfileActivity;->l:Landroid/net/Uri;

    .line 232
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 202
    invoke-direct {p0}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;-><init>()V

    .line 371
    sget-object v0, Lcom/twitter/android/ProfileActivity$DisplayState;->a:Lcom/twitter/android/ProfileActivity$DisplayState;

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->ay:Lcom/twitter/android/ProfileActivity$DisplayState;

    .line 375
    iput-boolean v2, p0, Lcom/twitter/android/ProfileActivity;->aC:Z

    .line 376
    iput-boolean v1, p0, Lcom/twitter/android/ProfileActivity;->aD:Z

    .line 384
    iput-boolean v2, p0, Lcom/twitter/android/ProfileActivity;->aK:Z

    .line 385
    iput-boolean v1, p0, Lcom/twitter/android/ProfileActivity;->aL:Z

    return-void
.end method

.method public static a(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ILcom/twitter/library/api/PromotedEvent;Lcom/twitter/model/timeline/r;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 434
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "user_id"

    .line 435
    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    .line 436
    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "scribe_content"

    sget-object v2, Lcom/twitter/model/timeline/r;->a:Lcom/twitter/util/serialization/b;

    .line 438
    invoke-static {p8, v2}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    .line 437
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "screen_name"

    .line 439
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 441
    const/4 v1, -0x1

    if-eq p6, v1, :cond_0

    .line 442
    const-string/jumbo v1, "friendship"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 445
    :cond_0
    if-eqz p4, :cond_2

    .line 446
    if-eqz p7, :cond_1

    .line 447
    invoke-static {p7, p4}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v1

    invoke-virtual {v1}, Lbsq$a;->a()Lbsq;

    move-result-object v1

    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 449
    :cond_1
    const-string/jumbo v1, "pc"

    invoke-static {p4}, Lcgi;->a(Lcgi;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 452
    :cond_2
    return-object v0
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/media/ui/image/UserImageView;)Landroid/widget/FrameLayout;
    .locals 5

    .prologue
    .line 864
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 865
    const v1, 0x7f0402c4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 866
    new-instance v2, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 867
    invoke-virtual {p1}, Lcom/twitter/media/ui/image/UserImageView;->getImageView()Landroid/widget/ImageView;

    move-result-object v1

    check-cast v1, Lcom/twitter/media/ui/image/RichImageView;

    .line 868
    invoke-virtual {v1}, Lcom/twitter/media/ui/image/RichImageView;->getCornerRadii()[F

    move-result-object v1

    .line 870
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f110190

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 871
    if-eqz v1, :cond_0

    array-length v3, v1

    if-lez v3, :cond_0

    const/4 v3, 0x0

    aget v1, v1, v3

    :goto_0
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 872
    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 873
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 874
    invoke-virtual {p1, v0}, Lcom/twitter/media/ui/image/UserImageView;->addView(Landroid/view/View;)V

    .line 876
    return-object v0

    .line 871
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/ProfileActivity;)Lcom/twitter/android/composer/ComposerDockLayout;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->I:Lcom/twitter/android/composer/ComposerDockLayout;

    return-object v0
.end method

.method private a(Lcom/twitter/model/core/TwitterUser;Z)Lcom/twitter/model/core/TwitterUser;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1866
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/x;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/library/client/n;

    move-result-object v1

    .line 1867
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    iget-boolean v0, v1, Lcom/twitter/library/client/n;->i:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1868
    :goto_0
    if-nez v0, :cond_2

    if-eqz p2, :cond_0

    iget-object v2, p1, Lcom/twitter/model/core/TwitterUser;->F:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 1893
    :cond_0
    :goto_1
    return-object p1

    .line 1867
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1871
    :cond_2
    new-instance v2, Lcom/twitter/model/core/TwitterUser$a;

    invoke-direct {v2, p1}, Lcom/twitter/model/core/TwitterUser$a;-><init>(Lcom/twitter/model/core/TwitterUser;)V

    .line 1872
    if-eqz v0, :cond_5

    .line 1873
    iget-object v0, v1, Lcom/twitter/library/client/n;->g:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/twitter/model/core/TwitterUser$a;->h(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v3, v1, Lcom/twitter/library/client/n;->h:Lcom/twitter/util/collection/k;

    .line 1874
    invoke-virtual {v0, v3}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/util/collection/k;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    iget-object v3, v1, Lcom/twitter/library/client/n;->d:Ljava/lang/String;

    .line 1875
    invoke-virtual {v0, v3}, Lcom/twitter/model/core/TwitterUser$a;->b(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    .line 1878
    iget-boolean v0, v1, Lcom/twitter/library/client/n;->j:Z

    if-eqz v0, :cond_3

    .line 1879
    iget-object v0, v1, Lcom/twitter/library/client/n;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/model/util/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/model/core/TwitterUser$a;->e(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 1880
    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/core/v;)Lcom/twitter/model/core/TwitterUser$a;

    .line 1882
    :cond_3
    iget-boolean v0, v1, Lcom/twitter/library/client/n;->k:Z

    if-eqz v0, :cond_4

    .line 1883
    iget-object v0, v1, Lcom/twitter/library/client/n;->f:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/twitter/model/core/TwitterUser$a;->f(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    move-result-object v0

    .line 1884
    invoke-virtual {v0, v4}, Lcom/twitter/model/core/TwitterUser$a;->b(Lcom/twitter/model/core/v;)Lcom/twitter/model/core/TwitterUser$a;

    .line 1886
    :cond_4
    iget-object v0, v1, Lcom/twitter/library/client/n;->l:Lcom/twitter/model/profile/ExtendedProfile;

    if-eqz v0, :cond_5

    .line 1887
    iget-object v0, v1, Lcom/twitter/library/client/n;->l:Lcom/twitter/model/profile/ExtendedProfile;

    invoke-virtual {v2, v0}, Lcom/twitter/model/core/TwitterUser$a;->a(Lcom/twitter/model/profile/ExtendedProfile;)Lcom/twitter/model/core/TwitterUser$a;

    .line 1890
    :cond_5
    if-eqz p2, :cond_6

    .line 1891
    invoke-virtual {v2, v4}, Lcom/twitter/model/core/TwitterUser$a;->i(Ljava/lang/String;)Lcom/twitter/model/core/TwitterUser$a;

    .line 1893
    :cond_6
    invoke-virtual {v2}, Lcom/twitter/model/core/TwitterUser$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    move-object p1, v0

    goto :goto_1
.end method

.method private a(JLcgi;)V
    .locals 7

    .prologue
    .line 2179
    new-instance v1, Lbhq;

    .line 2180
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    move-object v2, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lbhq;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    const/4 v0, 0x1

    .line 2181
    invoke-virtual {v1, v0}, Lbhq;->b(Z)Lbhq;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    .line 2182
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbhq;->a(Ljava/lang/Integer;)Lbhq;

    move-result-object v0

    .line 2183
    const/16 v1, 0x9

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 2184
    return-void
.end method

.method private a(JLjava/lang/String;)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 2472
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->af:Z

    .line 2473
    iget-object v5, p0, Lcom/twitter/android/ProfileActivity;->ae:Lcgi;

    const/4 v7, -0x1

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v8, v6

    move-object v9, v6

    invoke-static/range {v1 .. v9}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ILcom/twitter/library/api/PromotedEvent;Lcom/twitter/model/timeline/r;)Landroid/content/Intent;

    move-result-object v0

    .line 2475
    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->startActivity(Landroid/content/Intent;)V

    .line 2476
    const v0, 0x7f050055

    const v1, 0x7f050056

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileActivity;->overridePendingTransition(II)V

    .line 2477
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->finish()V

    .line 2478
    return-void
.end method

.method public static a(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/model/timeline/r;)V
    .locals 1

    .prologue
    .line 405
    invoke-static/range {p0 .. p6}, Lcom/twitter/android/ProfileActivity;->b(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/model/timeline/r;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 406
    return-void
.end method

.method private a(Lcom/twitter/android/ProfileActivity$DisplayState;)V
    .locals 2

    .prologue
    .line 728
    iput-object p1, p0, Lcom/twitter/android/ProfileActivity;->ay:Lcom/twitter/android/ProfileActivity$DisplayState;

    .line 730
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ay:Lcom/twitter/android/ProfileActivity$DisplayState;

    sget-object v1, Lcom/twitter/android/ProfileActivity$DisplayState;->a:Lcom/twitter/android/ProfileActivity$DisplayState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ay:Lcom/twitter/android/ProfileActivity$DisplayState;

    sget-object v1, Lcom/twitter/android/ProfileActivity$DisplayState;->b:Lcom/twitter/android/ProfileActivity$DisplayState;

    if-eq v0, v1, :cond_0

    .line 732
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aB:Lcom/twitter/android/metrics/d;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/d;->k()V

    .line 735
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 736
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 737
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 738
    if-eqz v1, :cond_1

    .line 739
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 741
    :cond_1
    new-instance v1, Lcom/twitter/android/profiles/p;

    invoke-direct {v1}, Lcom/twitter/android/profiles/p;-><init>()V

    .line 742
    invoke-virtual {v1, p0}, Lcom/twitter/android/profiles/p;->a(Landroid/content/Context;)Lcom/twitter/android/profiles/p;

    move-result-object v1

    .line 743
    invoke-virtual {v1, v0}, Lcom/twitter/android/profiles/p;->a(Landroid/os/Bundle;)Lcom/twitter/android/profiles/p;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    .line 744
    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/p;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/android/profiles/p;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    .line 745
    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/p;->a(Z)Lcom/twitter/android/profiles/p;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    .line 746
    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/p;->a(I)Lcom/twitter/android/profiles/p;

    move-result-object v0

    .line 747
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->R()Lbqn;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/p;->a(Lbqn;)Lcom/twitter/android/profiles/p;

    move-result-object v0

    .line 748
    invoke-virtual {v0, p1}, Lcom/twitter/android/profiles/p;->a(Lcom/twitter/android/ProfileActivity$DisplayState;)Lcom/twitter/android/profiles/p;

    move-result-object v0

    .line 749
    invoke-virtual {v0}, Lcom/twitter/android/profiles/p;->a()Lcom/twitter/android/profiles/l;

    move-result-object v0

    .line 750
    if-eqz v0, :cond_2

    .line 751
    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->L:Lcom/twitter/android/profiles/l;

    .line 754
    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ab()V

    .line 755
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ai()V

    .line 756
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ac()V

    .line 757
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ProfileActivity;I)V
    .locals 0

    .prologue
    .line 202
    invoke-direct {p0, p1}, Lcom/twitter/android/ProfileActivity;->o(I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ProfileActivity;Z)V
    .locals 0

    .prologue
    .line 202
    invoke-direct {p0, p1}, Lcom/twitter/android/ProfileActivity;->j(Z)V

    return-void
.end method

.method private a(Lcom/twitter/android/widget/LoggedOutBar;)V
    .locals 2

    .prologue
    .line 668
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->I:Lcom/twitter/android/composer/ComposerDockLayout;

    new-instance v1, Lcom/twitter/android/ProfileActivity$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/ProfileActivity$1;-><init>(Lcom/twitter/android/ProfileActivity;Lcom/twitter/android/widget/LoggedOutBar;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/ComposerDockLayout;->a(Lcom/twitter/internal/android/widget/DockLayout$a;)V

    .line 696
    return-void
.end method

.method private a(Lcom/twitter/android/widget/TweetStatView;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 1942
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1943
    int-to-long v2, p3

    invoke-static {v0, v2, v3}, Lcom/twitter/util/r;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/TweetStatView;->setValue(Ljava/lang/CharSequence;)V

    .line 1944
    invoke-virtual {p2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/TweetStatView;->setName(Ljava/lang/CharSequence;)V

    .line 1945
    return-void
.end method

.method private a(Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 7

    .prologue
    .line 1923
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1925
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1926
    new-instance v0, Lcom/twitter/android/profiles/i;

    invoke-direct {v0}, Lcom/twitter/android/profiles/i;-><init>()V

    .line 1928
    invoke-static {p0}, Lcom/twitter/android/al;->a(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1929
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayShowHomeAsUpEnabled(Z)V

    .line 1936
    :cond_0
    :goto_0
    invoke-virtual {v0, p1}, Lcom/twitter/android/profiles/u;->a(Lcom/twitter/internal/android/widget/ToolBar;)V

    .line 1937
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aI:Lcom/twitter/android/profiles/j;

    invoke-interface {v0, p0}, Lcom/twitter/android/profiles/j;->a(Lcom/twitter/android/profiles/j$a;)V

    .line 1939
    :cond_1
    return-void

    .line 1932
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    .line 1933
    new-instance v0, Lcom/twitter/android/profiles/h;

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget v3, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    iget-boolean v4, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    iget-object v5, p0, Lcom/twitter/android/ProfileActivity;->aR:Lcom/twitter/model/ads/b;

    iget-boolean v6, p0, Lcom/twitter/android/ProfileActivity;->aS:Z

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/profiles/h;-><init>(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/core/TwitterUser;IZLcom/twitter/model/ads/b;Z)V

    goto :goto_0
.end method

.method private a(ZZ)V
    .locals 1

    .prologue
    .line 1755
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1756
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aN:Lazv;

    invoke-virtual {v0, p1}, Lazv;->b(Z)Lazv;

    .line 1757
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aO:Lazv;

    invoke-virtual {v0, p2}, Lazv;->b(Z)Lazv;

    .line 1759
    :cond_0
    return-void
.end method

.method private varargs a([Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2031
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    invoke-static {v0, v1, v2, p1}, Lcom/twitter/android/profiles/v;->a(JLcom/twitter/android/profiles/t;[Ljava/lang/String;)V

    .line 2032
    return-void
.end method

.method private a(ILandroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 1049
    invoke-direct {p0, p1}, Lcom/twitter/android/ProfileActivity;->l(I)Lcom/twitter/library/client/m;

    move-result-object v0

    .line 1050
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/library/client/m;->a:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/ProfileActivity;ILandroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 202
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/ProfileActivity;->a(ILandroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method private aA()Z
    .locals 2

    .prologue
    .line 2671
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->s:Lcom/twitter/model/profile/ExtendedProfile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    .line 2672
    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->s:Lcom/twitter/model/profile/ExtendedProfile;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-static {v0, v1}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/model/profile/ExtendedProfile;Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2671
    :goto_0
    return v0

    .line 2672
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aB()V
    .locals 4

    .prologue
    .line 2685
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 2686
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    .line 2687
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/m;->a(Landroid/support/v4/app/FragmentManager;)Lcom/twitter/app/common/base/BaseFragment;

    move-result-object v0

    .line 2688
    instance-of v3, v0, Lcom/twitter/app/common/timeline/TimelineFragment;

    if-eqz v3, :cond_0

    .line 2689
    check-cast v0, Lcom/twitter/app/common/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/twitter/app/common/timeline/TimelineFragment;->M()V

    goto :goto_0

    .line 2692
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aG:Lcom/twitter/android/profiles/d;

    if-eqz v0, :cond_2

    .line 2693
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aG:Lcom/twitter/android/profiles/d;

    invoke-interface {v0}, Lcom/twitter/android/profiles/d;->o()V

    .line 2695
    :cond_2
    return-void
.end method

.method private aC()V
    .locals 5

    .prologue
    .line 2969
    const v0, 0x7f13069c

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->aM:Landroid/widget/TextView;

    .line 2971
    new-instance v0, Landroid/text/SpannableStringBuilder;

    const v1, 0x7f0a06dd

    .line 2972
    invoke-virtual {p0, v1}, Lcom/twitter/android/ProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2973
    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2975
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 2976
    const v2, 0x7f0a09b0

    invoke-virtual {p0, v2}, Lcom/twitter/android/ProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2977
    new-instance v2, Lcom/twitter/android/ProfileActivity$8;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f1100c9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, p0, v3}, Lcom/twitter/android/ProfileActivity$8;-><init>(Lcom/twitter/android/ProfileActivity;I)V

    .line 2982
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v4, 0x21

    .line 2977
    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2984
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->aM:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/twitter/ui/view/g;->a(Landroid/widget/TextView;)V

    .line 2985
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->aM:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2986
    return-void
.end method

.method private aD()V
    .locals 4

    .prologue
    .line 2989
    invoke-static {}, Lafq;->a()Lafq;

    move-result-object v0

    .line 2990
    const-string/jumbo v1, "fatigue_account_notif_profile_tooltip"

    iget-wide v2, p0, Lcom/twitter/android/ProfileActivity;->F:J

    invoke-static {p0, v1, v2, v3}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v1

    .line 2992
    new-instance v2, Lafl;

    invoke-direct {v2, v1}, Lafl;-><init>(Lcom/twitter/android/util/h;)V

    .line 2993
    new-instance v1, Lafn;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-direct {v1, p0, v3, v2, v0}, Lafn;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Lafk;Lafp$a;)V

    iput-object v1, p0, Lcom/twitter/android/ProfileActivity;->aJ:Lafm;

    .line 2995
    return-void
.end method

.method private ab()V
    .locals 2

    .prologue
    .line 760
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->t()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->m:Ljava/util/List;

    .line 761
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->K:Lcom/twitter/android/ProfileActivity$f;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->m:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/twitter/android/ProfileActivity$f;->a(Ljava/util/List;)V

    .line 762
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/android/ProfileActivity$g;

    if-eqz v0, :cond_0

    .line 763
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ProfileActivity$g;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->m:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/twitter/android/ProfileActivity$g;->a(Ljava/util/List;)V

    .line 765
    :cond_0
    return-void
.end method

.method private ac()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 770
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ai:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 825
    :goto_0
    return-void

    .line 774
    :cond_0
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    .line 775
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 776
    sget-object v2, Lcom/twitter/android/ProfileActivity;->h:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/android/ProfileActivity;->ai:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 777
    if-eqz v0, :cond_2

    .line 779
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->S:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/twitter/android/al;->b(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V

    .line 824
    :cond_1
    :goto_1
    iput-object v5, p0, Lcom/twitter/android/ProfileActivity;->ai:Landroid/net/Uri;

    goto :goto_0

    .line 783
    :cond_2
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    .line 782
    invoke-static {p0, v0, v1, v2, v5}, Lcom/twitter/android/util/i;->a(Landroid/content/Context;JLcom/twitter/model/core/TwitterUser;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 785
    :cond_3
    sget-object v2, Lcom/twitter/android/ProfileActivity;->i:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/android/ProfileActivity;->ai:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 786
    if-eqz v0, :cond_4

    .line 788
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->S:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/twitter/android/al;->b(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V

    goto :goto_1

    .line 791
    :cond_4
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->aq()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 793
    :cond_5
    sget-object v2, Lcom/twitter/android/ProfileActivity;->j:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/android/ProfileActivity;->ai:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 794
    if-eqz v0, :cond_6

    .line 795
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->S:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/twitter/android/al;->b(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V

    goto :goto_1

    .line 797
    :cond_6
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v0}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    .line 798
    invoke-static {v0}, Lcom/twitter/model/core/g;->f(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v0}, Lcom/twitter/model/core/g;->e(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 799
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->q(I)V

    goto :goto_1

    .line 801
    :cond_7
    sget-object v2, Lcom/twitter/android/ProfileActivity;->k:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/android/ProfileActivity;->ai:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 804
    if-nez v0, :cond_1

    .line 805
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    .line 806
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/twitter/android/profiles/v;->a(Landroid/content/Context;J)Z

    move-result v1

    .line 805
    invoke-static {v0, v4, v1}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/android/profiles/t;ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 807
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->q(I)V

    goto/16 :goto_1

    .line 810
    :cond_8
    sget-object v2, Lcom/twitter/android/ProfileActivity;->l:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/android/ProfileActivity;->ai:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 811
    if-nez v0, :cond_1

    .line 812
    const-string/jumbo v0, "me:::vine:add"

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->c(Ljava/lang/String;)V

    .line 813
    new-instance v0, Lcom/twitter/library/client/n;

    invoke-direct {v0, v4}, Lcom/twitter/library/client/n;-><init>(Z)V

    .line 814
    invoke-static {p0, v1, v0}, Lcom/twitter/android/client/x;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/n;)Ljava/lang/String;

    .line 815
    const v0, 0x7f0a037f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 819
    :cond_9
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ai:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->b(Landroid/net/Uri;)I

    move-result v0

    .line 820
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 821
    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->a(I)V

    goto/16 :goto_1
.end method

.method private ad()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->L:Lcom/twitter/android/profiles/l;

    if-eqz v0, :cond_0

    .line 1065
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->L:Lcom/twitter/android/profiles/l;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->K:Lcom/twitter/android/ProfileActivity$f;

    invoke-virtual {v1}, Lcom/twitter/android/ProfileActivity$f;->a()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->l(I)Lcom/twitter/library/client/m;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/twitter/android/profiles/l;->a(Lcom/twitter/library/client/m;Lcom/twitter/model/core/TwitterUser;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    .line 1067
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method private ae()V
    .locals 8

    .prologue
    .line 1477
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    .line 1478
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->E:Lcom/twitter/android/profiles/x;

    const-string/jumbo v1, "ads_account_permissions"

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/x;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1479
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/ads/c;->b(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1480
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->E:Lcom/twitter/android/profiles/x;

    new-instance v1, Lcom/twitter/android/ads/b;

    const/4 v2, 0x7

    invoke-direct {v1, p0, p0, v3, v2}, Lcom/twitter/android/ads/b;-><init>(Landroid/content/Context;Lcom/twitter/android/ads/a;Landroid/support/v4/app/LoaderManager;I)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/x;->a(Lcom/twitter/android/profiles/w;)V

    .line 1486
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->E:Lcom/twitter/android/profiles/x;

    const-string/jumbo v1, "business_profile"

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/x;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->aV:Lcom/twitter/model/businessprofiles/d;

    .line 1487
    invoke-static {v0, v1}, Lbld;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/businessprofiles/d;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1488
    iget-object v7, p0, Lcom/twitter/android/ProfileActivity;->E:Lcom/twitter/android/profiles/x;

    new-instance v0, Lcom/twitter/android/businessprofiles/d;

    const/16 v4, 0x8

    iget-object v5, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    .line 1494
    invoke-static {p0}, Lbld;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/businessprofiles/d;-><init>(Landroid/content/Context;Lcom/twitter/android/businessprofiles/c;Landroid/support/v4/app/LoaderManager;ILcom/twitter/model/core/TwitterUser;Ljava/lang/String;)V

    .line 1488
    invoke-virtual {v7, v0}, Lcom/twitter/android/profiles/x;->a(Lcom/twitter/android/profiles/w;)V

    .line 1496
    :cond_1
    return-void
.end method

.method private af()V
    .locals 1

    .prologue
    .line 1500
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->aC:Z

    if-eqz v0, :cond_0

    .line 1501
    const-string/jumbo v0, ":::impression"

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->c(Ljava/lang/String;)V

    .line 1504
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->aC:Z

    .line 1506
    :cond_0
    return-void
.end method

.method private ag()V
    .locals 4

    .prologue
    .line 1510
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, ":user:muted_button:click"

    invoke-direct {p0, v2}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    .line 1511
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->B:Ljava/lang/String;

    const/16 v1, 0xb

    .line 1512
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const/4 v3, 0x0

    .line 1511
    invoke-static {p0, v0, v1, v2, v3}, Lcom/twitter/android/util/y;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/support/v4/app/FragmentManager;Landroid/support/v4/app/Fragment;)Z

    .line 1513
    return-void
.end method

.method private ah()V
    .locals 3

    .prologue
    .line 1561
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->N:Lcom/twitter/android/profiles/HeaderImageView;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/HeaderImageView;->setProfileUser(Lcom/twitter/android/profiles/t;)V

    .line 1562
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->Q:Lcom/twitter/android/widget/TweetStatView;

    const v1, 0x7f0a06c8

    invoke-virtual {p0, v1}, Lcom/twitter/android/ProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget v2, v2, Lcom/twitter/model/core/TwitterUser;->R:I

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/ProfileActivity;->a(Lcom/twitter/android/widget/TweetStatView;Ljava/lang/String;I)V

    .line 1563
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->R:Lcom/twitter/android/widget/TweetStatView;

    const v1, 0x7f0a06c9

    invoke-virtual {p0, v1}, Lcom/twitter/android/ProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget v2, v2, Lcom/twitter/model/core/TwitterUser;->u:I

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/ProfileActivity;->a(Lcom/twitter/android/widget/TweetStatView;Ljava/lang/String;I)V

    .line 1564
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1565
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->c(Lcom/twitter/model/core/TwitterUser;)V

    .line 1570
    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ak()V

    .line 1571
    return-void

    .line 1567
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aW:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1568
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->O:Lcom/twitter/media/ui/image/UserImageView;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;Z)Z

    goto :goto_0
.end method

.method private ai()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/16 v10, 0x8

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1575
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v8, :cond_7

    move v0, v8

    .line 1576
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->p:Lcom/twitter/internal/android/widget/HorizontalListView;

    if-eqz v0, :cond_8

    move v1, v9

    :goto_1
    invoke-virtual {v2, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setVisibility(I)V

    .line 1577
    if-eqz v0, :cond_9

    .line 1578
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0367

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    :goto_2
    iput v1, p0, Lcom/twitter/android/ProfileActivity;->x:I

    .line 1579
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->ar:Landroid/view/View;

    if-eqz v0, :cond_a

    move v0, v9

    :goto_3
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1580
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->av:Lcom/twitter/android/profiles/o;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/o;->f()V

    .line 1581
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ay:Lcom/twitter/android/ProfileActivity$DisplayState;

    sget-object v1, Lcom/twitter/android/ProfileActivity$DisplayState;->c:Lcom/twitter/android/ProfileActivity$DisplayState;

    if-ne v0, v1, :cond_c

    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aV:Lcom/twitter/model/businessprofiles/d;

    if-eqz v0, :cond_c

    .line 1583
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->M:Lcom/twitter/android/profiles/ProfileDetailsViewManager;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/twitter/android/profiles/ProfileDetailsViewManager$IconItemType;

    sget-object v2, Lcom/twitter/android/profiles/ProfileDetailsViewManager$IconItemType;->a:Lcom/twitter/android/profiles/ProfileDetailsViewManager$IconItemType;

    aput-object v2, v1, v9

    sget-object v2, Lcom/twitter/android/profiles/ProfileDetailsViewManager$IconItemType;->b:Lcom/twitter/android/profiles/ProfileDetailsViewManager$IconItemType;

    aput-object v2, v1, v8

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/ProfileDetailsViewManager;->a(Ljava/util/List;)V

    .line 1586
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ap()V

    .line 1593
    new-instance v0, Lcom/twitter/android/businessprofiles/a;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->as:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->ar:Landroid/view/View;

    iget-object v3, p0, Lcom/twitter/android/ProfileActivity;->aV:Lcom/twitter/model/businessprofiles/d;

    iget-object v4, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/businessprofiles/a;-><init>(Landroid/view/ViewGroup;Landroid/view/View;Lcom/twitter/model/businessprofiles/d;Lcom/twitter/model/core/TwitterUser;Landroid/app/Activity;)V

    .line 1599
    invoke-virtual {v0}, Lcom/twitter/android/businessprofiles/a;->a()Z

    move-result v12

    .line 1600
    if-nez v12, :cond_10

    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aV:Lcom/twitter/model/businessprofiles/d;

    invoke-static {v0}, Lbld;->a(Lcom/twitter/model/businessprofiles/d;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1601
    new-instance v0, Lcom/twitter/android/businessprofiles/f;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->aT:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/twitter/android/ProfileActivity;->aV:Lcom/twitter/model/businessprofiles/d;

    iget-object v4, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->aV:Lcom/twitter/model/businessprofiles/d;

    iget-object v2, v2, Lcom/twitter/model/businessprofiles/d;->h:Lcom/twitter/model/businessprofiles/e;

    iget-object v5, v2, Lcom/twitter/model/businessprofiles/e;->d:Lcom/twitter/model/core/TwitterUser;

    .line 1607
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    move-object v2, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/businessprofiles/f;-><init>(Landroid/view/ViewGroup;Landroid/app/Activity;Lcom/twitter/model/businessprofiles/d;Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/core/TwitterUser;J)V

    .line 1608
    invoke-virtual {v0}, Lcom/twitter/android/businessprofiles/f;->c()Z

    move-result v0

    .line 1609
    if-eqz v0, :cond_0

    .line 1610
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->av:Lcom/twitter/android/profiles/o;

    invoke-virtual {v1}, Lcom/twitter/android/profiles/o;->e()V

    :cond_0
    move v11, v0

    .line 1614
    :goto_4
    if-nez v11, :cond_1

    if-nez v12, :cond_1

    invoke-static {}, Lbld;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v1, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    .line 1615
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_b

    move v0, v8

    :goto_5
    invoke-static {v1, v0}, Lcom/twitter/android/profiles/v;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1616
    new-instance v1, Lcom/twitter/android/businessprofiles/e;

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->aT:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/twitter/android/ProfileActivity;->aV:Lcom/twitter/model/businessprofiles/d;

    iget-object v5, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    .line 1621
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    move-object v3, p0

    invoke-direct/range {v1 .. v7}, Lcom/twitter/android/businessprofiles/e;-><init>(Landroid/view/ViewGroup;Landroid/app/Activity;Lcom/twitter/model/businessprofiles/d;Lcom/twitter/model/core/TwitterUser;J)V

    .line 1622
    invoke-virtual {v1}, Lcom/twitter/android/businessprofiles/e;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1623
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->av:Lcom/twitter/android/profiles/o;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/o;->e()V

    .line 1627
    :cond_1
    if-nez v11, :cond_2

    .line 1628
    new-instance v0, Lcom/twitter/android/businessprofiles/g;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->aV:Lcom/twitter/model/businessprofiles/d;

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->aU:Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-direct {v0, v1, p0, v2}, Lcom/twitter/android/businessprofiles/g;-><init>(Lcom/twitter/model/businessprofiles/d;Landroid/content/Context;Lcom/twitter/ui/widget/TypefacesTextView;)V

    .line 1630
    invoke-virtual {v0}, Lcom/twitter/android/businessprofiles/g;->a()V

    .line 1637
    :cond_2
    :goto_6
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ay:Lcom/twitter/android/ProfileActivity$DisplayState;

    sget-object v1, Lcom/twitter/android/ProfileActivity$DisplayState;->b:Lcom/twitter/android/ProfileActivity$DisplayState;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ay:Lcom/twitter/android/ProfileActivity$DisplayState;

    sget-object v1, Lcom/twitter/android/ProfileActivity$DisplayState;->c:Lcom/twitter/android/ProfileActivity$DisplayState;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ay:Lcom/twitter/android/ProfileActivity$DisplayState;

    sget-object v1, Lcom/twitter/android/ProfileActivity$DisplayState;->f:Lcom/twitter/android/ProfileActivity$DisplayState;

    if-ne v0, v1, :cond_d

    .line 1640
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->az:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1641
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aA:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1642
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->av:Lcom/twitter/android/profiles/o;

    iget v1, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/o;->b(I)V

    .line 1647
    :goto_7
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ay:Lcom/twitter/android/ProfileActivity$DisplayState;

    sget-object v1, Lcom/twitter/android/ProfileActivity$DisplayState;->b:Lcom/twitter/android/ProfileActivity$DisplayState;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ay:Lcom/twitter/android/ProfileActivity$DisplayState;

    sget-object v1, Lcom/twitter/android/ProfileActivity$DisplayState;->c:Lcom/twitter/android/ProfileActivity$DisplayState;

    if-ne v0, v1, :cond_e

    .line 1649
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->R:Lcom/twitter/android/widget/TweetStatView;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/TweetStatView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1650
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->Q:Lcom/twitter/android/widget/TweetStatView;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/TweetStatView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1655
    :goto_8
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ay:Lcom/twitter/android/ProfileActivity$DisplayState;

    sget-object v1, Lcom/twitter/android/ProfileActivity$DisplayState;->e:Lcom/twitter/android/ProfileActivity$DisplayState;

    if-ne v0, v1, :cond_f

    .line 1656
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aG:Lcom/twitter/android/profiles/d;

    if-eqz v0, :cond_5

    .line 1657
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aG:Lcom/twitter/android/profiles/d;

    invoke-interface {v0}, Lcom/twitter/android/profiles/d;->a()V

    .line 1659
    :cond_5
    invoke-virtual {p0, v8}, Lcom/twitter/android/ProfileActivity;->c(Z)V

    .line 1660
    new-array v0, v8, [Ljava/lang/String;

    const-string/jumbo v1, "blocker_interstitial:::impression"

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v9

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    .line 1665
    :goto_9
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->aj()V

    .line 1666
    iput-boolean v8, p0, Lcom/twitter/android/ProfileActivity;->aL:Z

    .line 1668
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ay:Lcom/twitter/android/ProfileActivity$DisplayState;

    sget-object v1, Lcom/twitter/android/ProfileActivity$DisplayState;->d:Lcom/twitter/android/ProfileActivity$DisplayState;

    if-ne v0, v1, :cond_6

    .line 1669
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->av:Lcom/twitter/android/profiles/o;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/o;->c()V

    .line 1670
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->P()Lcom/twitter/android/search/SearchSuggestionController;

    move-result-object v0

    const-string/jumbo v1, "blocked_profile"

    invoke-virtual {v0, v1}, Lcom/twitter/android/search/SearchSuggestionController;->a(Ljava/lang/String;)Lcom/twitter/android/search/SearchSuggestionController;

    .line 1671
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->aw:Z

    if-eqz v0, :cond_6

    .line 1672
    new-array v0, v8, [Ljava/lang/String;

    const-string/jumbo v1, "blocked_profile:profile:::impression"

    aput-object v1, v0, v9

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    .line 1673
    iput-boolean v9, p0, Lcom/twitter/android/ProfileActivity;->aw:Z

    .line 1676
    :cond_6
    return-void

    :cond_7
    move v0, v9

    .line 1575
    goto/16 :goto_0

    :cond_8
    move v1, v10

    .line 1576
    goto/16 :goto_1

    :cond_9
    move v1, v9

    .line 1578
    goto/16 :goto_2

    :cond_a
    move v0, v10

    .line 1579
    goto/16 :goto_3

    :cond_b
    move v0, v9

    .line 1615
    goto/16 :goto_5

    .line 1633
    :cond_c
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aT:Landroid/view/ViewGroup;

    invoke-virtual {v0, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1634
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aU:Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-virtual {v0, v10}, Lcom/twitter/ui/widget/TypefacesTextView;->setVisibility(I)V

    .line 1635
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->as:Landroid/view/ViewGroup;

    invoke-virtual {v0, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_6

    .line 1644
    :cond_d
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->az:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 1645
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aA:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_7

    .line 1652
    :cond_e
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->R:Lcom/twitter/android/widget/TweetStatView;

    invoke-virtual {v0, v13}, Lcom/twitter/android/widget/TweetStatView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1653
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->Q:Lcom/twitter/android/widget/TweetStatView;

    invoke-virtual {v0, v13}, Lcom/twitter/android/widget/TweetStatView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_8

    .line 1662
    :cond_f
    invoke-virtual {p0, v9}, Lcom/twitter/android/ProfileActivity;->c(Z)V

    goto :goto_9

    :cond_10
    move v11, v9

    goto/16 :goto_4
.end method

.method private aj()V
    .locals 2

    .prologue
    .line 1679
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ay:Lcom/twitter/android/ProfileActivity$DisplayState;

    sget-object v1, Lcom/twitter/android/ProfileActivity$DisplayState;->e:Lcom/twitter/android/ProfileActivity$DisplayState;

    if-ne v0, v1, :cond_0

    .line 1680
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->h(Z)V

    .line 1684
    :goto_0
    return-void

    .line 1682
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->h(Z)V

    goto :goto_0
.end method

.method private ak()V
    .locals 4

    .prologue
    .line 1762
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    invoke-static {v0, v1}, Lcom/twitter/android/profiles/v;->a(ZLcom/twitter/model/core/TwitterUser;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1763
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ao:Landroid/widget/ListView;

    if-nez v0, :cond_0

    .line 1764
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->W:Landroid/widget/LinearLayout;

    const v1, 0x7f13069d

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->ao:Landroid/widget/ListView;

    .line 1766
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ao:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1769
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->an:Lcom/twitter/android/bz;

    if-nez v0, :cond_1

    .line 1770
    new-instance v0, Lcom/twitter/app/users/f;

    invoke-direct {v0}, Lcom/twitter/app/users/f;-><init>()V

    const/16 v1, 0x12

    .line 1771
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/f;->a(I)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 1772
    invoke-virtual {v0, p0}, Lcom/twitter/app/users/f;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 1773
    new-instance v1, Lcom/twitter/android/ca;

    const v2, 0x7f0a03a7

    .line 1774
    invoke-virtual {p0, v2}, Lcom/twitter/android/ProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/twitter/android/ca;-><init>(Ljava/lang/String;Landroid/content/Intent;)V

    .line 1773
    invoke-static {v1}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 1775
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f040374

    move v1, v0

    .line 1777
    :goto_0
    new-instance v3, Lcom/twitter/android/bz;

    .line 1778
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/twitter/android/ca;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/ca;

    invoke-direct {v3, v0, v1}, Lcom/twitter/android/bz;-><init>([Lcom/twitter/android/ca;I)V

    iput-object v3, p0, Lcom/twitter/android/ProfileActivity;->an:Lcom/twitter/android/bz;

    .line 1780
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ao:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->an:Lcom/twitter/android/bz;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1784
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aX:Lcmw;

    if-nez v0, :cond_2

    .line 1785
    new-instance v0, Lcmw;

    new-instance v1, Ladp;

    new-instance v2, Lauh;

    .line 1786
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v2, v3}, Lauh;-><init>(Landroid/content/ContentResolver;)V

    invoke-direct {v1, v2}, Ladp;-><init>(Lauj;)V

    invoke-direct {v0, v1}, Lcmw;-><init>(Lcmv;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->aX:Lcmw;

    .line 1788
    :cond_2
    new-instance v0, Lcom/twitter/android/ProfileActivity$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/ProfileActivity$c;-><init>(Lcom/twitter/android/ProfileActivity;Lcom/twitter/android/ProfileActivity$1;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->aZ:Lcom/twitter/android/ProfileActivity$c;

    .line 1789
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aX:Lcmw;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v0, v2, v3}, Lcmw;->a(J)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->aZ:Lcom/twitter/android/ProfileActivity$c;

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 1791
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->al()V

    .line 1794
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ap:Landroid/widget/LinearLayout;

    if-nez v0, :cond_4

    .line 1795
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->W:Landroid/widget/LinearLayout;

    const v1, 0x7f130694

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->ap:Landroid/widget/LinearLayout;

    .line 1798
    :cond_4
    return-void

    .line 1775
    :cond_5
    const v0, 0x7f0402de

    move v1, v0

    goto :goto_0
.end method

.method private al()V
    .locals 3

    .prologue
    .line 1801
    iget v0, p0, Lcom/twitter/android/ProfileActivity;->aq:I

    and-int/lit8 v0, v0, 0x20

    if-nez v0, :cond_0

    .line 1802
    new-instance v0, Lbib;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lbib;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 1803
    iget v0, p0, Lcom/twitter/android/ProfileActivity;->aq:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/twitter/android/ProfileActivity;->aq:I

    .line 1805
    :cond_0
    return-void
.end method

.method private am()V
    .locals 6

    .prologue
    .line 1808
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 1809
    iget v1, p0, Lcom/twitter/android/ProfileActivity;->aq:I

    and-int/lit16 v1, v1, 0x80

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    const/4 v2, 0x0

    .line 1811
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {p0, v4, v5}, Lcom/twitter/android/profiles/v;->a(Landroid/content/Context;J)Z

    move-result v3

    .line 1810
    invoke-static {v1, v2, v3}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/android/profiles/t;ZZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1812
    new-instance v1, Lbhw;

    invoke-direct {v1, p0, v0}, Lbhw;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    .line 1814
    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->e()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lbhw;->a(J)Lbhw;

    move-result-object v0

    .line 1815
    iget v1, p0, Lcom/twitter/android/ProfileActivity;->aq:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/twitter/android/ProfileActivity;->aq:I

    .line 1816
    const/16 v1, 0xf

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 1818
    :cond_0
    return-void
.end method

.method private an()V
    .locals 7

    .prologue
    .line 1830
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->c()Lcom/twitter/android/profiles/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/ProfileActivity;->ae:Lcgi;

    .line 1831
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    const/4 v6, 0x7

    move-object v1, p0

    .line 1830
    invoke-static/range {v1 .. v6}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->a(Landroid/app/Activity;Lcom/twitter/model/core/TwitterUser;Lcgi;JI)V

    .line 1832
    return-void
.end method

.method private ao()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x4

    .line 1837
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    invoke-static {v0}, Lcom/twitter/android/profilecompletionmodule/t;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1838
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/ProfileActivity;->A:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "profile"

    aput-object v2, v1, v5

    const-string/jumbo v2, "edit_profile_flow"

    aput-object v2, v1, v6

    const/4 v2, 0x0

    aput-object v2, v1, v7

    const-string/jumbo v2, "header"

    aput-object v2, v1, v8

    const-string/jumbo v2, "launch"

    aput-object v2, v1, v4

    .line 1839
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1838
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1840
    const-string/jumbo v0, "profile"

    invoke-static {p0, v0}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1846
    :goto_0
    invoke-virtual {p0, v0, v4}, Lcom/twitter/android/ProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1847
    return-void

    .line 1842
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/ProfileActivity;->A:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "profile"

    aput-object v2, v1, v5

    const-string/jumbo v2, "edit_profile"

    aput-object v2, v1, v6

    const/4 v2, 0x0

    aput-object v2, v1, v7

    const-string/jumbo v2, "header"

    aput-object v2, v1, v8

    const-string/jumbo v2, "launch"

    aput-object v2, v1, v4

    .line 1843
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1842
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1844
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/EditProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method private ap()V
    .locals 3

    .prologue
    .line 1850
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const/4 v1, 0x5

    .line 1851
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-boolean v1, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    .line 1852
    invoke-static {v1}, Lcom/twitter/android/profiles/v;->a(Z)Ljava/lang/String;

    move-result-object v1

    .line 1851
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1854
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->M:Lcom/twitter/android/profiles/ProfileDetailsViewManager;

    new-instance v2, Lcom/twitter/android/widget/ai;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/widget/ai;-><init>(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/profiles/ProfileDetailsViewManager;->a(Lcne;)V

    .line 1855
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->M:Lcom/twitter/android/profiles/ProfileDetailsViewManager;

    new-instance v1, Lcom/twitter/android/ProfileActivity$4;

    invoke-direct {v1, p0}, Lcom/twitter/android/ProfileActivity$4;-><init>(Lcom/twitter/android/ProfileActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/ProfileDetailsViewManager;->a(Landroid/view/View$OnClickListener;)Lcom/twitter/android/profiles/ProfileDetailsViewManager;

    .line 1861
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->M:Lcom/twitter/android/profiles/ProfileDetailsViewManager;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->aa:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p0}, Lcom/twitter/android/profiles/ProfileDetailsViewManager;->a(Lcom/twitter/android/profiles/t;Ljava/lang/String;Landroid/content/Context;)V

    .line 1862
    return-void
.end method

.method private aq()Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1948
    new-instance v0, Lcom/twitter/app/users/f;

    invoke-direct {v0}, Lcom/twitter/app/users/f;-><init>()V

    iget-wide v2, p0, Lcom/twitter/android/ProfileActivity;->A:J

    .line 1949
    invoke-virtual {v0, v2, v3}, Lcom/twitter/app/users/f;->a(J)Lcom/twitter/app/users/f;

    move-result-object v0

    const/4 v1, 0x0

    .line 1950
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/f;->a(I)Lcom/twitter/app/users/f;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget-object v1, v1, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    .line 1951
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/f;->c(Ljava/lang/String;)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 1952
    invoke-virtual {v0, v4}, Lcom/twitter/app/users/f;->c(Z)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 1953
    invoke-virtual {v0, v4}, Lcom/twitter/app/users/f;->a(Z)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 1954
    invoke-virtual {v0, p0}, Lcom/twitter/app/users/f;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 1948
    return-object v0
.end method

.method private ar()V
    .locals 3

    .prologue
    .line 2009
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, ":user:mute_dialog:mute_user"

    invoke-direct {p0, v2}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    .line 2010
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aI:Lcom/twitter/android/profiles/j;

    invoke-interface {v0}, Lcom/twitter/android/profiles/j;->a()V

    .line 2011
    return-void
.end method

.method private as()V
    .locals 4

    .prologue
    .line 2187
    new-instance v0, Lbhp;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbhp;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    iget-wide v2, p0, Lcom/twitter/android/ProfileActivity;->A:J

    .line 2188
    invoke-virtual {v0, v2, v3}, Lbhp;->a(J)Lbhp;

    move-result-object v0

    const/4 v1, 0x3

    .line 2187
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 2189
    const/16 v0, 0x4000

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->n(I)V

    .line 2190
    return-void
.end method

.method private at()V
    .locals 7

    .prologue
    .line 2194
    iget v0, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v0}, Lcom/twitter/model/core/g;->c(I)Z

    move-result v0

    .line 2195
    iget v1, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v1}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v1

    .line 2196
    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget-boolean v2, v2, Lcom/twitter/model/core/TwitterUser;->l:Z

    if-eqz v2, :cond_2

    .line 2197
    const/16 v2, 0x4000

    invoke-direct {p0, v2}, Lcom/twitter/android/ProfileActivity;->m(I)V

    .line 2201
    :goto_0
    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/twitter/android/ProfileActivity;->ac:Z

    if-eqz v1, :cond_0

    .line 2202
    const/16 v1, 0xd

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->q(I)V

    .line 2204
    :cond_0
    new-instance v1, Lbhq;

    .line 2205
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/android/ProfileActivity;->A:J

    iget-object v6, p0, Lcom/twitter/android/ProfileActivity;->ae:Lcgi;

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lbhq;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    const/4 v2, 0x0

    .line 2206
    invoke-virtual {v1, v2}, Lbhq;->a(Z)Lbhq;

    move-result-object v1

    const/4 v2, -0x1

    .line 2207
    invoke-virtual {v1, v2}, Lbhq;->a(I)Lbhq;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget-boolean v2, v2, Lcom/twitter/model/core/TwitterUser;->l:Z

    .line 2208
    invoke-virtual {v1, v2}, Lbhq;->e(Z)Lbhq;

    move-result-object v1

    .line 2209
    const/16 v2, 0x8

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/ProfileActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 2210
    const-string/jumbo v1, "profile::user:follow"

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->c(Ljava/lang/String;)V

    .line 2211
    if-eqz v0, :cond_1

    .line 2212
    const-string/jumbo v0, "profile::user:follow_back"

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->c(Ljava/lang/String;)V

    .line 2214
    :cond_1
    return-void

    .line 2199
    :cond_2
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/twitter/android/ProfileActivity;->m(I)V

    goto :goto_0
.end method

.method private au()V
    .locals 1

    .prologue
    .line 2218
    const-string/jumbo v0, "profile::user:unfollow"

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->c(Ljava/lang/String;)V

    .line 2219
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->av()V

    .line 2220
    return-void
.end method

.method private av()V
    .locals 7

    .prologue
    .line 2223
    iget-object v6, p0, Lcom/twitter/android/ProfileActivity;->ae:Lcgi;

    .line 2224
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->n(I)V

    .line 2225
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ad:Lcom/twitter/model/util/FriendshipCache;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->c(J)V

    .line 2226
    new-instance v1, Lbhs;

    .line 2227
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/android/ProfileActivity;->A:J

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lbhs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    const/4 v0, -0x1

    .line 2228
    invoke-virtual {v1, v0}, Lbhs;->a(I)Lbhs;

    move-result-object v0

    .line 2229
    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 2230
    return-void
.end method

.method private aw()V
    .locals 1

    .prologue
    .line 2234
    const-string/jumbo v0, ":user:block_dialog:block"

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->c(Ljava/lang/String;)V

    .line 2235
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ax()V

    .line 2236
    return-void
.end method

.method private ax()V
    .locals 8

    .prologue
    .line 2239
    new-instance v1, Lbes;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/android/ProfileActivity;->A:J

    iget-object v6, p0, Lcom/twitter/android/ProfileActivity;->ae:Lcgi;

    const/4 v7, 0x1

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lbes;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;I)V

    .line 2241
    const/4 v0, 0x5

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/ProfileActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 2242
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->m(I)V

    .line 2243
    return-void
.end method

.method private ay()V
    .locals 1

    .prologue
    .line 2254
    const/16 v0, 0x810

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->n(I)V

    .line 2255
    const-string/jumbo v0, "profile::user:device_unfollow"

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->c(Ljava/lang/String;)V

    .line 2256
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->g(Z)V

    .line 2257
    return-void
.end method

.method private az()V
    .locals 1

    .prologue
    .line 2261
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->m(I)V

    .line 2262
    const-string/jumbo v0, "profile::user:device_follow"

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->c(Ljava/lang/String;)V

    .line 2263
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->g(Z)V

    .line 2264
    return-void
.end method

.method private b(Landroid/net/Uri;)I
    .locals 2

    .prologue
    .line 1054
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1055
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    iget-object v0, v0, Lcom/twitter/library/client/m;->a:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1059
    :goto_1
    return v1

    .line 1054
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1059
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/model/timeline/r;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 413
    const/4 v7, -0x1

    sget-object v8, Lcom/twitter/library/api/PromotedEvent;->c:Lcom/twitter/library/api/PromotedEvent;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v9, p6

    invoke-static/range {v1 .. v9}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ILcom/twitter/library/api/PromotedEvent;Lcom/twitter/model/timeline/r;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/ProfileActivity;)Lcom/twitter/android/composer/ComposerDockLayout;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->I:Lcom/twitter/android/composer/ComposerDockLayout;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2014
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    iget v1, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v0, v1}, Lcom/twitter/android/profiles/v;->a(ZI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2015
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v1, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    invoke-static {v1}, Lcom/twitter/android/profiles/v;->a(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2020
    :goto_0
    return-object v0

    .line 2017
    :cond_0
    const-string/jumbo v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2018
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "blocked_profile:profile"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2020
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "blocked_profile:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 1700
    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->N:Lcom/twitter/android/profiles/HeaderImageView;

    const v1, 0x7f0a041a

    invoke-virtual {p0, v1}, Lcom/twitter/android/ProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/HeaderImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1701
    invoke-virtual {p0, p1}, Lcom/twitter/android/ProfileActivity;->a(Landroid/graphics/Bitmap;)V

    .line 1702
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->aj:Z

    if-nez v0, :cond_1

    .line 1703
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ak:Lcom/twitter/android/ScrollingHeaderActivity$a;

    if-eqz v0, :cond_0

    .line 1704
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ak:Lcom/twitter/android/ScrollingHeaderActivity$a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/ScrollingHeaderActivity$a;->cancel(Z)Z

    .line 1706
    :cond_0
    new-instance v0, Lcom/twitter/android/ScrollingHeaderActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/ScrollingHeaderActivity$a;-><init>(Lcom/twitter/android/ScrollingHeaderActivity;Z)V

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->ak:Lcom/twitter/android/ScrollingHeaderActivity$a;

    .line 1707
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ak:Lcom/twitter/android/ScrollingHeaderActivity$a;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/android/ScrollingHeaderActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1712
    :cond_1
    :goto_0
    return-void

    .line 1709
    :catch_0
    move-exception v0

    .line 1710
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->z:Lcom/twitter/android/bq;

    invoke-interface {v0}, Lcom/twitter/android/bq;->b()V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Z)V
    .locals 5

    .prologue
    .line 1958
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1960
    const-string/jumbo v0, "_normal."

    const-string/jumbo v1, "."

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1961
    new-instance v2, Landroid/content/Intent;

    if-eqz p2, :cond_1

    .line 1962
    invoke-static {}, Lbpr;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-class v0, Lcom/twitter/android/ProfilePhotoImageActivity;

    :goto_0
    invoke-direct {v2, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1966
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "android.intent.extra.TEXT"

    iget-object v4, p0, Lcom/twitter/android/ProfileActivity;->B:Ljava/lang/String;

    .line 1967
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "image_url"

    .line 1968
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1970
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    if-eqz v0, :cond_2

    .line 1971
    const-string/jumbo v0, "action_label_resid"

    const v1, 0x7f0a0335

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1972
    const/4 v0, 0x6

    invoke-virtual {p0, v2, v0}, Lcom/twitter/android/ProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1977
    :cond_0
    :goto_1
    return-void

    .line 1962
    :cond_1
    const-class v0, Lcom/twitter/android/ImageActivity;

    goto :goto_0

    .line 1974
    :cond_2
    invoke-virtual {p0, v2}, Lcom/twitter/android/ProfileActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method static synthetic c(Lcom/twitter/android/ProfileActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private c(Landroid/content/res/Resources;)Lcom/twitter/media/ui/image/UserImageView;
    .locals 2

    .prologue
    .line 855
    new-instance v0, Lcom/twitter/media/ui/image/UserImageView;

    invoke-direct {v0, p0}, Lcom/twitter/media/ui/image/UserImageView;-><init>(Landroid/content/Context;)V

    .line 856
    const v1, 0x7f13031a

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setId(I)V

    .line 857
    invoke-static {p1, v0}, Lcom/twitter/android/profiles/v;->a(Landroid/content/res/Resources;Lcom/twitter/media/ui/image/UserImageView;)V

    .line 858
    return-object v0
.end method

.method private c(Lcom/twitter/model/core/TwitterUser;)V
    .locals 2

    .prologue
    .line 880
    iget v0, p1, Lcom/twitter/model/core/TwitterUser;->i:I

    if-eqz v0, :cond_0

    .line 881
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aW:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 882
    iget v1, p1, Lcom/twitter/model/core/TwitterUser;->i:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 883
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->aW:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 885
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aW:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 886
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 2026
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/ProfileActivity;->A:J

    iget-object v4, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    iget-object v5, p0, Lcom/twitter/android/ProfileActivity;->ae:Lcgi;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    .line 2027
    invoke-static {v1}, Lcom/twitter/android/profiles/v;->b(Lcom/twitter/model/core/TwitterUser;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/twitter/android/ProfileActivity;->al:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/twitter/android/ProfileActivity;->am:Lcom/twitter/model/timeline/r;

    move-object v1, p1

    .line 2026
    invoke-static/range {v0 .. v9}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;JLcom/twitter/android/profiles/t;Lcgi;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/model/timeline/r;)V

    .line 2028
    return-void
.end method

.method static synthetic d(Lcom/twitter/android/ProfileActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private d(Z)V
    .locals 3

    .prologue
    .line 1518
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->Y:Z

    if-eqz v0, :cond_1

    .line 1527
    :cond_0
    :goto_0
    return-void

    .line 1520
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/t;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1521
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->N:Lcom/twitter/android/profiles/HeaderImageView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/twitter/android/ProfileActivity;->b(Landroid/content/res/Resources;)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/HeaderImageView;->setDefaultDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1526
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->Y:Z

    goto :goto_0

    .line 1523
    :cond_2
    invoke-direct {p0, p1}, Lcom/twitter/android/ProfileActivity;->e(Z)V

    goto :goto_1
.end method

.method static synthetic e(Lcom/twitter/android/ProfileActivity;)V
    .locals 0

    .prologue
    .line 202
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->aB()V

    return-void
.end method

.method private e(Z)V
    .locals 5

    .prologue
    .line 1530
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1531
    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->b(Landroid/content/res/Resources;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileActivity;->s:I

    .line 1532
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/twitter/android/ProfileActivity;->s:I

    iget v1, p0, Lcom/twitter/android/ProfileActivity;->X:I

    if-eq v0, v1, :cond_0

    .line 1534
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->z:Lcom/twitter/android/bq;

    invoke-interface {v0}, Lcom/twitter/android/bq;->b()V

    .line 1536
    new-instance v0, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    iget v4, p0, Lcom/twitter/android/ProfileActivity;->X:I

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    iget v4, p0, Lcom/twitter/android/ProfileActivity;->s:I

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 1541
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->N:Lcom/twitter/android/profiles/HeaderImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/android/profiles/HeaderImageView;->setDefaultDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1542
    const/16 v1, 0x1f4

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 1548
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->au:Lcom/twitter/android/ProfileActivity$e;

    new-instance v1, Lcom/twitter/android/ProfileActivity$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/ProfileActivity$3;-><init>(Lcom/twitter/android/ProfileActivity;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/ProfileActivity$e;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1558
    :goto_0
    return-void

    .line 1555
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->z:Lcom/twitter/android/bq;

    invoke-interface {v0}, Lcom/twitter/android/bq;->a()V

    .line 1556
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->N:Lcom/twitter/android/profiles/HeaderImageView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget v2, p0, Lcom/twitter/android/ProfileActivity;->s:I

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/HeaderImageView;->setDefaultDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/twitter/android/ProfileActivity;)V
    .locals 0

    .prologue
    .line 202
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ag()V

    return-void
.end method

.method private f(Z)V
    .locals 2

    .prologue
    .line 2002
    iget v0, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v0}, Lcom/twitter/android/profiles/v;->a(I)I

    move-result v0

    .line 2004
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->aM:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2005
    return-void
.end method

.method static synthetic g(Lcom/twitter/android/ProfileActivity;)Lcom/twitter/android/metrics/d;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aB:Lcom/twitter/android/metrics/d;

    return-object v0
.end method

.method private g(Z)V
    .locals 5

    .prologue
    .line 2246
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 2248
    :goto_0
    new-instance v1, Lbij;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget-object v4, p0, Lcom/twitter/android/ProfileActivity;->ae:Lcgi;

    invoke-direct {v1, p0, v2, v3, v4}, Lbij;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/TwitterUser;Lcgi;)V

    .line 2249
    invoke-virtual {v1, v0, p1}, Lbij;->a(IZ)Lbij;

    move-result-object v0

    const/16 v1, 0xc

    .line 2248
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 2250
    return-void

    .line 2246
    :cond_0
    const/16 v0, 0x10

    goto :goto_0
.end method

.method static synthetic h(Lcom/twitter/android/ProfileActivity;)Lcom/twitter/android/bz;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->an:Lcom/twitter/android/bz;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/ProfileActivity;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ao:Landroid/widget/ListView;

    return-object v0
.end method

.method private i(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2457
    if-eqz p1, :cond_0

    .line 2458
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ag:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "profile_device_follow_dialog_shown"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2459
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->p(I)V

    .line 2460
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ag:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "profile_device_follow_dialog_shown"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2461
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 2469
    :cond_0
    :goto_0
    return-void

    .line 2463
    :cond_1
    const v0, 0x7f0a09e7

    new-array v1, v3, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    .line 2464
    invoke-virtual {v2}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2463
    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 2466
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method static synthetic j(Lcom/twitter/android/ProfileActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ar:Landroid/view/View;

    return-object v0
.end method

.method private j(Z)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2641
    new-instance v0, Lcom/twitter/android/profiles/b;

    invoke-direct {v0}, Lcom/twitter/android/profiles/b;-><init>()V

    iget-object v4, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    .line 2642
    invoke-virtual {v0, v4}, Lcom/twitter/android/profiles/b;->a(Lcom/twitter/android/profiles/t;)Lcom/twitter/android/profiles/b$a;

    move-result-object v5

    .line 2643
    if-eqz v5, :cond_7

    move v0, v1

    .line 2644
    :goto_0
    iget v4, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v4}, Lcom/twitter/model/core/g;->e(I)Z

    move-result v4

    .line 2645
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->aA()Z

    move-result v6

    if-nez v6, :cond_0

    if-eqz v0, :cond_6

    .line 2646
    :cond_0
    invoke-static {}, Lbhu;->a()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2647
    invoke-static {}, Lbhu;->b()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2648
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->hasWindowFocus()Z

    move-result v6

    if-eqz v6, :cond_6

    iget-boolean v6, p0, Lcom/twitter/android/ProfileActivity;->aQ:Z

    if-eqz v6, :cond_1

    if-eqz v4, :cond_2

    :cond_1
    if-eqz p1, :cond_6

    .line 2650
    :cond_2
    if-eqz p1, :cond_3

    .line 2651
    new-array v4, v1, [Ljava/lang/String;

    const-string/jumbo v6, "::birthday:click"

    invoke-direct {p0, v6}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v2

    invoke-direct {p0, v4}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    .line 2653
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v4

    const v6, 0x7f130043

    invoke-virtual {v4, v6}, Lcom/twitter/internal/android/widget/ToolBar;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v6

    .line 2654
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v4

    const v7, 0x7f13005e

    invoke-virtual {v4, v7}, Lcom/twitter/internal/android/widget/ToolBar;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    .line 2655
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v7

    const v8, 0x7f13088d

    invoke-virtual {v7, v8}, Lcom/twitter/internal/android/widget/ToolBar;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    move-result v7

    add-int/2addr v7, v4

    .line 2657
    iget-object v4, p0, Lcom/twitter/android/ProfileActivity;->aP:Lcom/twitter/android/profiles/animation/BalloonSetAnimationView;

    iget-object v8, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    invoke-virtual {v4, v8}, Lcom/twitter/android/profiles/animation/BalloonSetAnimationView;->setProfileUser(Lcom/twitter/android/profiles/t;)V

    .line 2658
    iget-object v4, p0, Lcom/twitter/android/ProfileActivity;->aP:Lcom/twitter/android/profiles/animation/BalloonSetAnimationView;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v8

    invoke-virtual {v8}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Lcom/twitter/android/profiles/animation/BalloonSetAnimationView;->setUserId(J)V

    .line 2660
    iget-object v8, p0, Lcom/twitter/android/ProfileActivity;->aP:Lcom/twitter/android/profiles/animation/BalloonSetAnimationView;

    if-eqz v5, :cond_8

    iget-object v4, v5, Lcom/twitter/android/profiles/b$a;->b:[Ljava/lang/String;

    :goto_1
    if-eqz v5, :cond_4

    iget-object v3, v5, Lcom/twitter/android/profiles/b$a;->c:[I

    :cond_4
    invoke-virtual {v8, v6, v7, v4, v3}, Lcom/twitter/android/profiles/animation/BalloonSetAnimationView;->a(II[Ljava/lang/String;[I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2663
    new-array v1, v1, [Ljava/lang/String;

    if-eqz v0, :cond_9

    const-string/jumbo v0, "::balloon_override:play"

    :goto_2
    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    .line 2666
    :cond_5
    iput-boolean v2, p0, Lcom/twitter/android/ProfileActivity;->aQ:Z

    .line 2668
    :cond_6
    return-void

    :cond_7
    move v0, v2

    .line 2643
    goto/16 :goto_0

    :cond_8
    move-object v4, v3

    .line 2660
    goto :goto_1

    .line 2663
    :cond_9
    const-string/jumbo v0, "::birthday:play"

    goto :goto_2
.end method

.method private l(I)Lcom/twitter/library/client/m;
    .locals 1

    .prologue
    .line 1042
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1043
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    .line 1045
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private m(I)V
    .locals 1

    .prologue
    .line 1898
    iget v0, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v0, p1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->o(I)V

    .line 1899
    return-void
.end method

.method private n(I)V
    .locals 1

    .prologue
    .line 1902
    iget v0, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v0, p1}, Lcom/twitter/model/core/g;->b(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->o(I)V

    .line 1903
    return-void
.end method

.method private o(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1906
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->av:Lcom/twitter/android/profiles/o;

    invoke-virtual {v0, p1}, Lcom/twitter/android/profiles/o;->a(I)V

    .line 1907
    iput p1, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    .line 1908
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)V

    .line 1909
    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->f(Z)V

    .line 1910
    invoke-virtual {p0, v1}, Lcom/twitter/android/ProfileActivity;->a(Z)V

    .line 1911
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    iget v1, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/t;->a(I)V

    .line 1913
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->av:Lcom/twitter/android/profiles/o;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/o;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1914
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aJ:Lafm;

    invoke-interface {v0}, Lafm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1915
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aJ:Lafm;

    invoke-interface {v0}, Lafm;->b()V

    .line 1920
    :cond_0
    :goto_0
    return-void

    .line 1918
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aJ:Lafm;

    invoke-interface {v0}, Lafm;->c()V

    goto :goto_0
.end method

.method private p(I)V
    .locals 2

    .prologue
    .line 2035
    invoke-direct {p0, p1}, Lcom/twitter/android/ProfileActivity;->r(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    .line 2036
    if-eqz v0, :cond_0

    .line 2037
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 2039
    :cond_0
    return-void
.end method

.method private q(I)V
    .locals 3

    .prologue
    .line 2042
    invoke-direct {p0, p1}, Lcom/twitter/android/ProfileActivity;->r(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    .line 2043
    if-eqz v0, :cond_0

    .line 2046
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/twitter/android/ProfileActivity$5;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/ProfileActivity$5;-><init>(Lcom/twitter/android/ProfileActivity;Lcom/twitter/android/widget/PromptDialogFragment;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2053
    :cond_0
    return-void
.end method

.method private r(I)Lcom/twitter/android/widget/PromptDialogFragment;
    .locals 11

    .prologue
    const v3, 0x7f0a03a5

    const v1, 0x7f0a00f6

    const v5, 0x7f0a0616

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2058
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    if-nez v0, :cond_0

    .line 2059
    const/4 v0, 0x0

    .line 2175
    :goto_0
    return-object v0

    .line 2062
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 2065
    const v4, 0x7f0a0a40

    .line 2066
    const v2, 0x7f0a05e0

    .line 2067
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v9

    .line 2069
    packed-switch p1, :pswitch_data_0

    .line 2164
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2071
    :pswitch_1
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/16 v2, 0xe

    invoke-direct {v0, v2}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    .line 2072
    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 2073
    const v2, 0x7f0a09ee

    move v4, v6

    move v10, v1

    move-object v1, v0

    move v0, v2

    move v2, v10

    .line 2168
    :goto_1
    new-array v5, v6, [Ljava/lang/Object;

    aput-object v9, v5, v7

    .line 2169
    invoke-virtual {v8, v0, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/aj$b;->a(Ljava/lang/CharSequence;)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 2170
    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    .line 2171
    if-eqz v4, :cond_1

    .line 2172
    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    .line 2175
    :cond_1
    invoke-virtual {v1}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PromptDialogFragment;

    goto :goto_0

    .line 2079
    :pswitch_2
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/16 v2, 0xc

    invoke-direct {v0, v2}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    .line 2080
    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(Ljava/lang/String;)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 2081
    const v3, 0x7f0a09db

    .line 2082
    const v1, 0x7f0a05e0

    .line 2083
    const v2, 0x7f0a0a40

    move v4, v6

    move v10, v1

    move-object v1, v0

    move v0, v3

    move v3, v2

    move v2, v10

    .line 2084
    goto :goto_1

    .line 2087
    :pswitch_3
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/4 v2, 0x6

    invoke-direct {v0, v2}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v2, 0x7f0a09dd

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v9, v3, v7

    .line 2088
    invoke-virtual {v8, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/aj$b;->a(Ljava/lang/String;)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v2, 0x7f0a09eb

    .line 2089
    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/aj$b;->e(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 2090
    const v3, 0x7f0a09dc

    .line 2091
    const v2, 0x7f0a09e2

    move v4, v6

    move v10, v1

    move-object v1, v0

    move v0, v3

    move v3, v10

    .line 2093
    goto :goto_1

    .line 2097
    :pswitch_4
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, v6}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a09e2

    .line 2098
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 2099
    const v1, 0x7f0a09e4

    .line 2100
    iget v3, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v3}, Lcom/twitter/model/core/g;->i(I)Z

    move-result v3

    if-nez v3, :cond_2

    iget v3, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v3}, Lcom/twitter/model/core/g;->b(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2101
    :cond_2
    const v3, 0x7f0a09eb

    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/aj$b;->e(I)Lcom/twitter/android/widget/aj$a;

    move v3, v4

    move v4, v6

    move-object v10, v0

    move v0, v1

    move-object v1, v10

    goto/16 :goto_1

    .line 2107
    :pswitch_5
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a09f9

    .line 2108
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 2109
    const v1, 0x7f0a09fa

    move v3, v4

    move v4, v6

    move-object v10, v0

    move v0, v1

    move-object v1, v10

    .line 2110
    goto/16 :goto_1

    .line 2115
    :pswitch_6
    iput-boolean v7, p0, Lcom/twitter/android/ProfileActivity;->ac:Z

    .line 2116
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/16 v1, 0xd

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a09e6

    .line 2117
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 2118
    const v2, 0x7f0a09e5

    .line 2120
    const v1, 0x7f0a05e9

    .line 2124
    const-string/jumbo v3, "::device_follow_prompt:impression"

    invoke-direct {p0, v3}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/twitter/android/ProfileActivity;->c(Ljava/lang/String;)V

    move v3, v5

    move v4, v6

    move-object v10, v0

    move v0, v2

    move v2, v1

    move-object v1, v10

    .line 2125
    goto/16 :goto_1

    .line 2128
    :pswitch_7
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a0a00

    .line 2129
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 2131
    const v1, 0x7f0a09ff

    move v3, v5

    move v4, v7

    move-object v10, v0

    move v0, v1

    move-object v1, v10

    .line 2134
    goto/16 :goto_1

    .line 2137
    :pswitch_8
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/16 v2, 0x9

    invoke-direct {v0, v2}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    .line 2138
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a09e8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2139
    const v2, 0x7f0a09e9

    .line 2142
    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/aj$b;->a(Ljava/lang/String;)Lcom/twitter/android/widget/aj$a;

    move v3, v5

    move v4, v6

    move-object v10, v0

    move v0, v2

    move v2, v1

    move-object v1, v10

    .line 2143
    goto/16 :goto_1

    .line 2147
    :pswitch_9
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a09f0

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v9, v3, v7

    .line 2148
    invoke-virtual {v8, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(Ljava/lang/String;)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 2149
    const v1, 0x7f0a09ef

    move v3, v5

    move v4, v7

    move-object v10, v0

    move v0, v1

    move-object v1, v10

    .line 2152
    goto/16 :goto_1

    .line 2156
    :pswitch_a
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a09f2

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v9, v3, v7

    .line 2157
    invoke-virtual {v8, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(Ljava/lang/String;)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 2158
    const v1, 0x7f0a09f1

    move v3, v5

    move v4, v7

    move-object v10, v0

    move v0, v1

    move-object v1, v10

    .line 2161
    goto/16 :goto_1

    :cond_3
    move v3, v4

    move v4, v6

    move-object v10, v0

    move v0, v1

    move-object v1, v10

    goto/16 :goto_1

    .line 2069
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_7
        :pswitch_3
        :pswitch_9
        :pswitch_a
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_6
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected A_()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2482
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->af:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->K()Landroid/content/Intent;

    move-result-object v0

    .line 2483
    :goto_0
    if-eqz v0, :cond_0

    .line 2484
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2486
    :cond_0
    return-object v0

    .line 2482
    :cond_1
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->A_()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Landroid/content/res/Resources;)I
    .locals 2

    .prologue
    .line 2930
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 2931
    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    const/high16 v1, 0x40400000    # 3.0f

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method protected a(Ljava/util/List;Landroid/support/v4/view/ViewPager;)Landroid/support/v4/view/PagerAdapter;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;",
            "Landroid/support/v4/view/ViewPager;",
            ")",
            "Landroid/support/v4/view/PagerAdapter;"
        }
    .end annotation

    .prologue
    .line 2895
    new-instance v0, Lcom/twitter/android/ProfileActivity$g;

    iget-object v5, p0, Lcom/twitter/android/ProfileActivity;->p:Lcom/twitter/internal/android/widget/HorizontalListView;

    iget-object v6, p0, Lcom/twitter/android/ProfileActivity;->K:Lcom/twitter/android/ProfileActivity$f;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileActivity$g;-><init>(Lcom/twitter/android/ProfileActivity;Landroid/support/v4/app/FragmentActivity;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;)V

    return-object v0
.end method

.method protected a(Ljava/util/List;)Landroid/widget/BaseAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;)",
            "Landroid/widget/BaseAdapter;"
        }
    .end annotation

    .prologue
    .line 2888
    new-instance v0, Lcom/twitter/android/ProfileActivity$f;

    invoke-direct {v0, p1}, Lcom/twitter/android/ProfileActivity$f;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->K:Lcom/twitter/android/ProfileActivity$f;

    .line 2889
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->K:Lcom/twitter/android/ProfileActivity$f;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 2876
    const v0, 0x7f0402c2

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 2877
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->b(Z)V

    .line 2878
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2879
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 2880
    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(I)V

    .line 2882
    :cond_0
    return-object p2
.end method

.method public a()V
    .locals 2

    .prologue
    .line 839
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->a()V

    .line 846
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->au:Lcom/twitter/android/ProfileActivity$e;

    new-instance v1, Lcom/twitter/android/ProfileActivity$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/ProfileActivity$2;-><init>(Lcom/twitter/android/ProfileActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/ProfileActivity$e;->post(Ljava/lang/Runnable;)Z

    .line 852
    return-void
.end method

.method protected a(I)V
    .locals 1

    .prologue
    .line 2908
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->a(I)V

    .line 2909
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2910
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->K:Lcom/twitter/android/ProfileActivity$f;

    invoke-virtual {v0, p1}, Lcom/twitter/android/ProfileActivity$f;->a(I)V

    .line 2911
    return-void
.end method

.method public a(II)V
    .locals 2

    .prologue
    .line 1014
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->O:Lcom/twitter/media/ui/image/UserImageView;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setTranslationY(F)V

    .line 1015
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->N:Lcom/twitter/android/profiles/HeaderImageView;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/HeaderImageView;->getHeight()I

    move-result v0

    if-nez v0, :cond_0

    .line 1022
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->au:Lcom/twitter/android/ProfileActivity$e;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/android/ProfileActivity$e;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 1023
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->au:Lcom/twitter/android/ProfileActivity$e;

    invoke-virtual {v1, v0}, Lcom/twitter/android/ProfileActivity$e;->sendMessage(Landroid/os/Message;)Z

    .line 1027
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aP:Lcom/twitter/android/profiles/animation/BalloonSetAnimationView;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/animation/BalloonSetAnimationView;->a()V

    .line 1028
    return-void

    .line 1025
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->P:Lcom/twitter/android/profiles/a;

    neg-int v1, p1

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/a;->a(I)V

    goto :goto_0
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v9, 0x4

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v0, -0x1

    .line 1092
    iget-wide v4, p0, Lcom/twitter/android/ProfileActivity;->A:J

    .line 1093
    iget-object v6, p0, Lcom/twitter/android/ProfileActivity;->ae:Lcgi;

    .line 1094
    packed-switch p2, :pswitch_data_0

    .line 1211
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1096
    :pswitch_1
    if-ne p3, v0, :cond_0

    .line 1097
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->as()V

    goto :goto_0

    .line 1102
    :pswitch_2
    if-ne p3, v0, :cond_1

    .line 1103
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->aw()V

    goto :goto_0

    .line 1104
    :cond_1
    const/4 v0, -0x2

    if-ne p3, v0, :cond_0

    .line 1105
    const-string/jumbo v0, ":user:block_dialog:cancel"

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 1110
    :pswitch_3
    if-ne p3, v0, :cond_2

    .line 1111
    new-instance v1, Lbes;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    const/4 v7, 0x3

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lbes;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;I)V

    .line 1113
    invoke-virtual {p0, v1, v9}, Lcom/twitter/android/ProfileActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 1114
    const-string/jumbo v0, ":user:unblock_dialog:unblock"

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->c(Ljava/lang/String;)V

    .line 1115
    invoke-direct {p0, v9}, Lcom/twitter/android/ProfileActivity;->n(I)V

    goto :goto_0

    .line 1116
    :cond_2
    const/4 v0, -0x2

    if-ne p3, v0, :cond_0

    .line 1117
    const-string/jumbo v0, ":user:unblock_dialog:cancel"

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 1122
    :pswitch_4
    if-ne p3, v0, :cond_0

    .line 1123
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->at()V

    goto :goto_0

    .line 1128
    :pswitch_5
    const/4 v0, -0x2

    if-ne p3, v0, :cond_3

    .line 1129
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->au()V

    goto :goto_0

    .line 1130
    :cond_3
    const/4 v0, -0x3

    if-ne p3, v0, :cond_0

    .line 1131
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ay()V

    goto :goto_0

    .line 1136
    :pswitch_6
    if-ne p3, v0, :cond_4

    .line 1137
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->au()V

    goto :goto_0

    .line 1138
    :cond_4
    const/4 v0, -0x3

    if-ne p3, v0, :cond_0

    .line 1139
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ay()V

    goto :goto_0

    .line 1144
    :pswitch_7
    if-ne p3, v0, :cond_0

    .line 1145
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ay()V

    goto :goto_0

    .line 1150
    :pswitch_8
    if-ne p3, v0, :cond_0

    .line 1151
    new-instance v1, Lbes;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/android/ProfileActivity;->A:J

    iget-object v6, p0, Lcom/twitter/android/ProfileActivity;->ae:Lcgi;

    const/4 v7, 0x2

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lbes;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;I)V

    .line 1153
    const-string/jumbo v0, "spam"

    iput-object v0, v1, Lbes;->j:Ljava/lang/String;

    .line 1154
    iput-boolean v8, v1, Lbes;->g:Z

    .line 1155
    const/4 v0, 0x6

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/ProfileActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 1157
    invoke-direct {p0, v9}, Lcom/twitter/android/ProfileActivity;->m(I)V

    .line 1158
    const-string/jumbo v0, ":::report_as_spam"

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1163
    :pswitch_9
    if-ne p3, v0, :cond_5

    .line 1164
    const-string/jumbo v0, "::device_follow_prompt:accept"

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->c(Ljava/lang/String;)V

    .line 1165
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->az()V

    goto/16 :goto_0

    .line 1166
    :cond_5
    const/4 v0, -0x2

    if-ne p3, v0, :cond_0

    .line 1167
    const-string/jumbo v0, "::device_follow_prompt:dismiss"

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1172
    :pswitch_a
    if-ne p3, v0, :cond_6

    .line 1173
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ar()V

    goto/16 :goto_0

    .line 1175
    :cond_6
    new-array v0, v8, [Ljava/lang/String;

    const-string/jumbo v1, ":user:mute_dialog:cancel"

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1180
    :pswitch_b
    if-ne p3, v0, :cond_7

    .line 1181
    new-array v0, v8, [Ljava/lang/String;

    const-string/jumbo v1, ":user:muted_button:unmute_user"

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    .line 1182
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aI:Lcom/twitter/android/profiles/j;

    invoke-interface {v0}, Lcom/twitter/android/profiles/j;->b()V

    goto/16 :goto_0

    .line 1184
    :cond_7
    new-array v0, v8, [Ljava/lang/String;

    const-string/jumbo v1, ":user:muted_button:cancel"

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1189
    :pswitch_c
    if-ne p3, v0, :cond_8

    .line 1190
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "profile_system_prompt"

    aput-object v2, v1, v3

    aput-object v7, v1, v8

    const/4 v2, 0x2

    aput-object v7, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "start_tweet"

    aput-object v3, v1, v2

    const-string/jumbo v2, "click"

    aput-object v2, v1, v9

    .line 1191
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1190
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1192
    const v0, 0x7f0a05c7

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1193
    new-instance v1, Lcom/twitter/model/drafts/DraftAttachment;

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->aY:Lcom/twitter/media/model/MediaFile;

    sget-object v3, Lcom/twitter/model/media/MediaSource;->b:Lcom/twitter/model/media/MediaSource;

    .line 1194
    invoke-static {v2, v3}, Lcom/twitter/model/media/EditableMedia;->a(Lcom/twitter/media/model/MediaFile;Lcom/twitter/model/media/MediaSource;)Lcom/twitter/model/media/EditableMedia;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/model/drafts/DraftAttachment;-><init>(Lcom/twitter/model/media/EditableMedia;)V

    .line 1195
    invoke-static {v1}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 1196
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v2

    .line 1197
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/twitter/android/composer/a;->a(Ljava/lang/String;I)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 1198
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->a(Ljava/util/List;)Lcom/twitter/android/composer/a;

    move-result-object v0

    const-string/jumbo v1, "profile_system_prompt"

    .line 1199
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->a(Ljava/lang/String;)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 1200
    invoke-virtual {v0, p0}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 1196
    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1202
    :cond_8
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "profile_system_prompt"

    aput-object v2, v1, v3

    aput-object v7, v1, v8

    const/4 v2, 0x2

    aput-object v7, v1, v2

    const/4 v2, 0x3

    aput-object v7, v1, v2

    const-string/jumbo v2, "cancel"

    aput-object v2, v1, v9

    .line 1203
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1202
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 1094
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_2
        :pswitch_3
        :pswitch_8
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_a
        :pswitch_b
        :pswitch_1
        :pswitch_9
        :pswitch_4
        :pswitch_c
    .end packed-switch
.end method

.method protected a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 2925
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->N:Lcom/twitter/android/profiles/HeaderImageView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/profiles/HeaderImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2926
    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 457
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    .line 458
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->aj()V

    .line 459
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    .line 461
    :goto_0
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 462
    const v0, 0x7f13067b

    .line 463
    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/LoggedOutBar;

    .line 464
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/LoggedOutBar;->setVisibility(I)V

    .line 465
    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/LoggedOutBar;->setDefaultOnClickListener(Landroid/app/Activity;)V

    .line 466
    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->a(Lcom/twitter/android/widget/LoggedOutBar;)V

    .line 468
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 459
    goto :goto_0
.end method

.method public a(Lcom/twitter/android/profiles/t;)V
    .locals 1

    .prologue
    .line 1004
    invoke-virtual {p1}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    .line 1005
    invoke-virtual {p1}, Lcom/twitter/android/profiles/t;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    .line 1006
    invoke-virtual {p1}, Lcom/twitter/android/profiles/t;->d()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    .line 1007
    invoke-virtual {p1}, Lcom/twitter/android/profiles/t;->c()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->ad:Lcom/twitter/model/util/FriendshipCache;

    .line 1009
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->am()V

    .line 1010
    return-void
.end method

.method public a(Lcom/twitter/library/service/s;I)V
    .locals 10

    .prologue
    const v8, 0x7f0a0260

    const/16 v4, 0x200

    const/16 v7, 0x10

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2301
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 2302
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 2304
    packed-switch p2, :pswitch_data_0

    .line 2450
    :pswitch_0
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->a(Lcom/twitter/library/service/s;I)V

    .line 2454
    :cond_0
    :goto_0
    return-void

    .line 2306
    :pswitch_1
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2307
    check-cast p1, Lbhy;

    .line 2308
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    iget-wide v0, p1, Lbhy;->a:J

    iget-wide v2, p0, Lcom/twitter/android/ProfileActivity;->A:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2309
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget v1, p1, Lbhy;->b:I

    iput v1, v0, Lcom/twitter/model/core/TwitterUser;->U:I

    .line 2310
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget v0, v0, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->o(I)V

    goto :goto_0

    .line 2316
    :pswitch_2
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2317
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->m(I)V

    goto :goto_0

    .line 2319
    :cond_1
    iput-boolean v2, p0, Lcom/twitter/android/ProfileActivity;->ax:Z

    .line 2320
    invoke-virtual {p0, v2}, Lcom/twitter/android/ProfileActivity;->a(Z)V

    .line 2321
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ad:Lcom/twitter/model/util/FriendshipCache;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    invoke-virtual {v1}, Lcom/twitter/android/profiles/t;->e()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->i(J)V

    goto :goto_0

    .line 2326
    :pswitch_3
    if-eqz v3, :cond_0

    .line 2327
    check-cast p1, Lbhq;

    .line 2329
    invoke-virtual {p1}, Lbhq;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    .line 2330
    if-eqz v0, :cond_2

    .line 2331
    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    move v2, v1

    .line 2332
    :cond_2
    if-nez v2, :cond_3

    .line 2333
    iget v0, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->b(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    goto :goto_0

    .line 2335
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget-boolean v0, v0, Lcom/twitter/model/core/TwitterUser;->l:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aG:Lcom/twitter/android/profiles/d;

    if-eqz v0, :cond_0

    .line 2336
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aG:Lcom/twitter/android/profiles/d;

    invoke-interface {v0}, Lcom/twitter/android/profiles/d;->n()V

    goto :goto_0

    .line 2343
    :pswitch_4
    if-eqz v3, :cond_0

    .line 2344
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2345
    iget v0, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v0, v1}, Lcom/twitter/model/core/g;->a(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    .line 2346
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2347
    const v0, 0x7f0a09e3

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2348
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 2350
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aG:Lcom/twitter/android/profiles/d;

    if-eqz v0, :cond_0

    .line 2351
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aG:Lcom/twitter/android/profiles/d;

    invoke-interface {v0}, Lcom/twitter/android/profiles/d;->a()V

    goto/16 :goto_0

    .line 2357
    :pswitch_5
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v0

    .line 2358
    if-eqz v0, :cond_5

    iget v0, v0, Lcom/twitter/network/l;->a:I

    const/16 v2, 0xc8

    if-ne v0, v2, :cond_5

    .line 2359
    const v0, 0x7f0a049b

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2360
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 2362
    :cond_5
    const v0, 0x7f0a049a

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2363
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 2368
    :pswitch_6
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_6

    check-cast p1, Lbes;

    iget-wide v0, p1, Lbes;->b:J

    iget-wide v2, p0, Lcom/twitter/android/ProfileActivity;->A:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_6

    .line 2370
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->l()V

    goto/16 :goto_0

    .line 2372
    :cond_6
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ad:Lcom/twitter/model/util/FriendshipCache;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    invoke-virtual {v1}, Lcom/twitter/android/profiles/t;->e()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->h(J)V

    goto/16 :goto_0

    .line 2377
    :pswitch_7
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_7

    check-cast p1, Lbes;

    iget-wide v2, p1, Lbes;->b:J

    iget-wide v4, p0, Lcom/twitter/android/ProfileActivity;->A:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_7

    .line 2378
    iget v0, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v0}, Lcom/twitter/model/core/g;->e(I)Z

    move-result v0

    .line 2379
    if-eqz v0, :cond_0

    .line 2381
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->l()V

    goto/16 :goto_0

    .line 2384
    :cond_7
    const v0, 0x7f0a09f8

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2385
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 2390
    :pswitch_8
    iget v3, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v3}, Lcom/twitter/model/core/g;->g(I)Z

    move-result v3

    .line 2391
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2392
    if-eqz v3, :cond_8

    const v0, 0x7f0a09fe

    .line 2395
    :goto_1
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v3}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 2396
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 2392
    :cond_8
    const v0, 0x7f0a09fc

    goto :goto_1

    .line 2398
    :cond_9
    invoke-static {p0, v8, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2399
    if-eqz v3, :cond_a

    .line 2400
    invoke-direct {p0, v4}, Lcom/twitter/android/ProfileActivity;->n(I)V

    goto/16 :goto_0

    .line 2402
    :cond_a
    invoke-direct {p0, v4}, Lcom/twitter/android/ProfileActivity;->m(I)V

    goto/16 :goto_0

    .line 2408
    :pswitch_9
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v3

    .line 2409
    if-eqz v3, :cond_0

    .line 2412
    iget-wide v4, v3, Lcom/twitter/library/service/v;->c:J

    invoke-static {p0, v4, v5}, Lcom/twitter/library/platform/notifications/PushRegistration;->b(Landroid/content/Context;J)Z

    move-result v4

    .line 2413
    iget v5, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v5}, Lcom/twitter/model/core/g;->i(I)Z

    move-result v5

    .line 2415
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v6

    if-eqz v6, :cond_c

    if-nez v4, :cond_b

    if-nez v5, :cond_c

    .line 2416
    :cond_b
    invoke-direct {p0, v5}, Lcom/twitter/android/ProfileActivity;->i(Z)V

    goto/16 :goto_0

    .line 2419
    :cond_c
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    const/16 v6, 0x3e9

    if-eq v0, v6, :cond_d

    if-nez v4, :cond_f

    .line 2420
    :cond_d
    invoke-static {p0}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;)Lcom/twitter/android/client/l;

    move-result-object v6

    iget-wide v8, v3, Lcom/twitter/library/service/v;->c:J

    if-nez v4, :cond_e

    move v0, v1

    :goto_2
    invoke-virtual {v6, v8, v9, v0, v1}, Lcom/twitter/android/client/l;->a(JZZ)V

    .line 2422
    invoke-direct {p0, v5}, Lcom/twitter/android/ProfileActivity;->i(Z)V

    goto/16 :goto_0

    :cond_e
    move v0, v2

    .line 2420
    goto :goto_2

    .line 2424
    :cond_f
    invoke-static {p0, v8, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2425
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2426
    if-eqz v5, :cond_10

    .line 2427
    invoke-direct {p0, v7}, Lcom/twitter/android/ProfileActivity;->n(I)V

    goto/16 :goto_0

    .line 2429
    :cond_10
    invoke-direct {p0, v7}, Lcom/twitter/android/ProfileActivity;->m(I)V

    goto/16 :goto_0

    .line 2435
    :pswitch_a
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 2436
    check-cast p1, Lbhw;

    .line 2437
    invoke-virtual {p1}, Lbhw;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->ac:Z

    goto/16 :goto_0

    .line 2439
    :cond_11
    iput-boolean v2, p0, Lcom/twitter/android/ProfileActivity;->ac:Z

    .line 2440
    iget v0, p0, Lcom/twitter/android/ProfileActivity;->aq:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/twitter/android/ProfileActivity;->aq:I

    goto/16 :goto_0

    .line 2445
    :pswitch_b
    iget v0, p0, Lcom/twitter/android/ProfileActivity;->aq:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/twitter/android/ProfileActivity;->aq:I

    .line 2446
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->a(Lcom/twitter/library/service/s;I)V

    goto/16 :goto_0

    .line 2304
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_6
        :pswitch_7
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_9
        :pswitch_0
        :pswitch_8
        :pswitch_a
    .end packed-switch
.end method

.method public a(Lcom/twitter/model/ads/b;)V
    .locals 3

    .prologue
    .line 1457
    iput-object p1, p0, Lcom/twitter/android/ProfileActivity;->aR:Lcom/twitter/model/ads/b;

    .line 1458
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e03f8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 1460
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->av:Lcom/twitter/android/profiles/o;

    invoke-virtual {v1}, Lcom/twitter/android/profiles/o;->d()Lcom/twitter/ui/widget/TwitterButton;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 1461
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->av:Lcom/twitter/android/profiles/o;

    invoke-virtual {v1}, Lcom/twitter/android/profiles/o;->d()Lcom/twitter/ui/widget/TwitterButton;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/ui/widget/TwitterButton;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->O:Lcom/twitter/media/ui/image/UserImageView;

    .line 1462
    invoke-virtual {v2}, Lcom/twitter/media/ui/image/UserImageView;->getRight()I

    move-result v2

    add-int/2addr v0, v2

    if-lt v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->aS:Z

    .line 1464
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->av:Lcom/twitter/android/profiles/o;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->aR:Lcom/twitter/model/ads/b;

    iget-boolean v2, p0, Lcom/twitter/android/ProfileActivity;->aS:Z

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/profiles/o;->a(Lcom/twitter/model/ads/b;Z)V

    .line 1465
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)V

    .line 1466
    return-void

    .line 1462
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/businessprofiles/d;)V
    .locals 1

    .prologue
    .line 1470
    iput-object p1, p0, Lcom/twitter/android/ProfileActivity;->aV:Lcom/twitter/model/businessprofiles/d;

    .line 1471
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aV:Lcom/twitter/model/businessprofiles/d;

    if-eqz v0, :cond_0

    .line 1472
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->a(Z)V

    .line 1474
    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 1419
    invoke-direct {p0, p1, v7}, Lcom/twitter/android/ProfileActivity;->a(Lcom/twitter/model/core/TwitterUser;Z)Lcom/twitter/model/core/TwitterUser;

    move-result-object v4

    .line 1420
    invoke-super {p0, v4}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 1421
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->S:Ljava/lang/String;

    .line 1422
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->av:Lcom/twitter/android/profiles/o;

    invoke-virtual {v0, v4}, Lcom/twitter/android/profiles/o;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 1423
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    iget-boolean v1, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    invoke-virtual {v0, v4, v1}, Lcom/twitter/android/profiles/t;->a(Lcom/twitter/model/core/TwitterUser;Z)V

    .line 1424
    iget v0, v4, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->o(I)V

    .line 1426
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ap()V

    .line 1427
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->T:Landroid/view/ViewGroup;

    iget-boolean v2, v4, Lcom/twitter/model/core/TwitterUser;->m:Z

    iget-object v3, v4, Lcom/twitter/model/core/TwitterUser;->P:Lcom/twitter/model/profile/TranslatorType;

    iget-boolean v4, v4, Lcom/twitter/model/core/TwitterUser;->l:Z

    const v5, 0x7f110012

    const v6, 0x7f110018

    move-object v0, p0

    move v8, v7

    invoke-static/range {v0 .. v8}, Lcom/twitter/library/util/w;->a(Landroid/content/Context;Landroid/view/ViewGroup;ZLcom/twitter/model/profile/TranslatorType;ZIIII)V

    .line 1430
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->N:Lcom/twitter/android/profiles/HeaderImageView;

    invoke-virtual {v0, p0}, Lcom/twitter/android/profiles/HeaderImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1434
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ah:Lcom/twitter/model/core/TwitterUser;

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->d(Z)V

    .line 1436
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->O:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0, p0}, Lcom/twitter/media/ui/image/UserImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1440
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/ToolBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1442
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->af()V

    .line 1444
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ae()V

    .line 1446
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ah()V

    .line 1448
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->ab:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aG:Lcom/twitter/android/profiles/d;

    if-eqz v0, :cond_0

    .line 1449
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aG:Lcom/twitter/android/profiles/d;

    invoke-interface {v0}, Lcom/twitter/android/profiles/d;->n()V

    .line 1450
    iput-boolean v7, p0, Lcom/twitter/android/ProfileActivity;->ab:Z

    .line 1452
    :cond_0
    invoke-direct {p0, v7}, Lcom/twitter/android/ProfileActivity;->j(Z)V

    .line 1453
    return-void

    .line 1421
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v7

    .line 1434
    goto :goto_1
.end method

.method public a(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 1688
    if-eqz p2, :cond_0

    .line 1689
    iput-object p1, p0, Lcom/twitter/android/ProfileActivity;->V:Ljava/lang/String;

    .line 1690
    invoke-direct {p0, p2}, Lcom/twitter/android/ProfileActivity;->b(Landroid/graphics/Bitmap;)V

    .line 1696
    :goto_0
    return-void

    .line 1692
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->V:Ljava/lang/String;

    .line 1693
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->z:Lcom/twitter/android/bq;

    invoke-interface {v0}, Lcom/twitter/android/bq;->b()V

    .line 1694
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->N:Lcom/twitter/android/profiles/HeaderImageView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->v()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/HeaderImageView;->setDefaultDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 1821
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1822
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "/sticky/default_profile_images/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1823
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ao()V

    .line 1827
    :cond_1
    :goto_0
    return-void

    .line 1824
    :cond_2
    if-eqz p1, :cond_1

    .line 1825
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 707
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    if-nez v0, :cond_2

    .line 708
    sget-object v0, Lcom/twitter/android/ProfileActivity$DisplayState;->a:Lcom/twitter/android/ProfileActivity$DisplayState;

    .line 722
    :goto_0
    if-nez p1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->ay:Lcom/twitter/android/ProfileActivity$DisplayState;

    if-eq v1, v0, :cond_1

    .line 723
    :cond_0
    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->a(Lcom/twitter/android/ProfileActivity$DisplayState;)V

    .line 725
    :cond_1
    return-void

    .line 709
    :cond_2
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    iget v1, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v0, v1}, Lcom/twitter/android/profiles/v;->a(ZI)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 710
    sget-object v0, Lcom/twitter/android/ProfileActivity$DisplayState;->d:Lcom/twitter/android/ProfileActivity$DisplayState;

    goto :goto_0

    .line 711
    :cond_3
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget v2, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v0, v1, v2}, Lcom/twitter/android/profiles/v;->a(ZLcom/twitter/model/core/TwitterUser;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 712
    sget-object v0, Lcom/twitter/android/ProfileActivity$DisplayState;->f:Lcom/twitter/android/ProfileActivity$DisplayState;

    goto :goto_0

    .line 713
    :cond_4
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget v2, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v0, v1, v2}, Lcom/twitter/android/profiles/v;->b(ZLcom/twitter/model/core/TwitterUser;I)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->ax:Z

    if-nez v0, :cond_5

    .line 715
    sget-object v0, Lcom/twitter/android/ProfileActivity$DisplayState;->e:Lcom/twitter/android/ProfileActivity$DisplayState;

    goto :goto_0

    .line 716
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->aV:Lcom/twitter/model/businessprofiles/d;

    invoke-static {v0, v1}, Lbld;->b(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/businessprofiles/d;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 717
    sget-object v0, Lcom/twitter/android/ProfileActivity$DisplayState;->c:Lcom/twitter/android/ProfileActivity$DisplayState;

    goto :goto_0

    .line 719
    :cond_6
    sget-object v0, Lcom/twitter/android/ProfileActivity$DisplayState;->b:Lcom/twitter/android/ProfileActivity$DisplayState;

    goto :goto_0
.end method

.method public a(Lcmm;)Z
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v0, 0x0

    const/4 v6, 0x1

    .line 1215
    invoke-interface {p1}, Lcmm;->a()I

    move-result v1

    .line 1216
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 1217
    sparse-switch v1, :sswitch_data_0

    .line 1305
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->a(Lcmm;)Z

    move-result v6

    .line 1309
    :cond_0
    :goto_0
    return v6

    .line 1219
    :sswitch_0
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    if-eqz v1, :cond_3

    .line 1220
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v1}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget-object v2, v2, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget-object v3, v3, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1222
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    iget-boolean v2, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    invoke-static {v2}, Lcom/twitter/android/profiles/v;->a(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    aput-object v5, v1, v6

    const-string/jumbo v0, "user"

    aput-object v0, v1, v4

    aput-object v5, v1, v7

    const/4 v0, 0x4

    const-string/jumbo v2, "share"

    aput-object v2, v1, v0

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    goto :goto_0

    .line 1229
    :sswitch_1
    iget v1, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v1}, Lcom/twitter/model/core/g;->g(I)Z

    move-result v1

    .line 1230
    if-nez v1, :cond_2

    .line 1231
    const/16 v2, 0x200

    invoke-direct {p0, v2}, Lcom/twitter/android/ProfileActivity;->m(I)V

    .line 1235
    :goto_1
    new-instance v2, Lbij;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    invoke-direct {v2, p0, v3, v4, v5}, Lbij;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/TwitterUser;Lcgi;)V

    const/4 v3, 0x4

    if-nez v1, :cond_1

    move v0, v6

    .line 1236
    :cond_1
    invoke-virtual {v2, v3, v0}, Lbij;->a(IZ)Lbij;

    move-result-object v0

    const/16 v1, 0xe

    .line 1235
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileActivity;->b(Lcom/twitter/library/service/s;I)Z

    goto :goto_0

    .line 1233
    :cond_2
    const/16 v2, 0x200

    invoke-direct {p0, v2}, Lcom/twitter/android/ProfileActivity;->n(I)V

    goto :goto_1

    .line 1241
    :sswitch_2
    iget-wide v0, p0, Lcom/twitter/android/ProfileActivity;->A:J

    invoke-static {p0, v0, v1, v2, v3}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/app/common/abs/AbsFragmentActivity;JJ)V

    .line 1242
    const-string/jumbo v0, ":::add_to_list"

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 1246
    :sswitch_3
    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, ":user:unmute_dialog:open"

    invoke-direct {p0, v2}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    .line 1247
    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, ":user:unmute_dialog:unmute_user"

    invoke-direct {p0, v2}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    .line 1248
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aI:Lcom/twitter/android/profiles/j;

    invoke-interface {v0}, Lcom/twitter/android/profiles/j;->b()V

    goto/16 :goto_0

    .line 1252
    :sswitch_4
    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, ":user:mute_dialog:open"

    invoke-direct {p0, v2}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    .line 1253
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->B:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    const/16 v3, 0xa

    .line 1255
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    move-object v0, p0

    .line 1253
    invoke-static/range {v0 .. v5}, Lcom/twitter/android/util/y;->a(Landroid/content/Context;Ljava/lang/String;IILandroid/support/v4/app/FragmentManager;Landroid/support/v4/app/Fragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1256
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ar()V

    goto/16 :goto_0

    .line 1261
    :sswitch_5
    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, ":user:block_dialog:impression"

    invoke-direct {p0, v2}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    .line 1262
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->B:Ljava/lang/String;

    .line 1263
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 1262
    invoke-static {p0, v0, v4, v1}, Lcom/twitter/android/util/y;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/support/v4/app/FragmentManager;)V

    goto/16 :goto_0

    .line 1267
    :sswitch_6
    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, ":user:unblock_dialog:impression"

    invoke-direct {p0, v2}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    .line 1268
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->B:Ljava/lang/String;

    .line 1269
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 1268
    invoke-static {p0, v0, v7, v1}, Lcom/twitter/android/util/y;->b(Landroid/content/Context;Ljava/lang/String;ILandroid/support/v4/app/FragmentManager;)V

    goto/16 :goto_0

    .line 1273
    :sswitch_7
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget v1, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->al:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-static {p0, v0, v1, v2}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/app/common/abs/AbsFragmentActivity;Lcom/twitter/model/core/TwitterUser;ILcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    goto/16 :goto_0

    .line 1277
    :sswitch_8
    iget-wide v0, p0, Lcom/twitter/android/ProfileActivity;->A:J

    invoke-static {p0, v0, v1}, Lcom/twitter/android/profiles/v;->b(Landroid/content/Context;J)V

    goto/16 :goto_0

    .line 1281
    :sswitch_9
    invoke-static {p0}, Lcom/twitter/android/profiles/v;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1285
    :sswitch_a
    const/4 v0, -0x1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v2, "user_id"

    iget-wide v4, p0, Lcom/twitter/android/ProfileActivity;->A:J

    .line 1286
    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "friendship"

    iget v3, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    .line 1287
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    .line 1285
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileActivity;->setResult(ILandroid/content/Intent;)V

    .line 1288
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->a(Lcmm;)Z

    move-result v6

    goto/16 :goto_0

    .line 1291
    :sswitch_b
    invoke-static {p0}, Lcom/twitter/android/ads/AdsCompanionWebViewActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->startActivity(Landroid/content/Intent;)V

    .line 1309
    :cond_3
    :goto_2
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->a(Lcmm;)Z

    move-result v6

    goto/16 :goto_0

    .line 1295
    :sswitch_c
    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "::qr_code:click"

    invoke-direct {p0, v2}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    .line 1296
    invoke-static {p0}, Lcom/twitter/android/qrcodes/a;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1300
    :sswitch_d
    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "user_moments:::show"

    invoke-direct {p0, v2}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    .line 1301
    iget-wide v0, p0, Lcom/twitter/android/ProfileActivity;->A:J

    invoke-static {p0, v0, v1}, Lcom/twitter/android/moments/ui/guide/ac;->a(Landroid/app/Activity;J)V

    goto :goto_2

    .line 1217
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f130043 -> :sswitch_a
        0x7f130883 -> :sswitch_5
        0x7f1308b3 -> :sswitch_4
        0x7f1308bc -> :sswitch_0
        0x7f1308bd -> :sswitch_1
        0x7f1308be -> :sswitch_1
        0x7f1308bf -> :sswitch_9
        0x7f1308c0 -> :sswitch_2
        0x7f1308c1 -> :sswitch_8
        0x7f1308c2 -> :sswitch_d
        0x7f1308c3 -> :sswitch_3
        0x7f1308c4 -> :sswitch_6
        0x7f1308c5 -> :sswitch_7
        0x7f1308c6 -> :sswitch_b
        0x7f1308c7 -> :sswitch_c
    .end sparse-switch
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 993
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->a(Lcmr;)Z

    .line 994
    const v0, 0x7f140024

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 995
    const/4 v0, 0x1

    return v0
.end method

.method protected ab_()I
    .locals 1

    .prologue
    .line 1037
    iget v0, p0, Lcom/twitter/android/ProfileActivity;->U:F

    float-to-int v0, v0

    return v0
.end method

.method protected b(Landroid/content/res/Resources;)I
    .locals 2

    .prologue
    .line 2936
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget v1, p0, Lcom/twitter/android/ProfileActivity;->X:I

    invoke-static {v0, v1}, Lcom/twitter/android/profiles/v;->a(Lcom/twitter/model/core/TwitterUser;I)I

    move-result v0

    return v0
.end method

.method public b(Lcmr;)I
    .locals 4

    .prologue
    .line 1991
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->b(Lcmr;)I

    move-result v1

    .line 1992
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 1993
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v2

    const v3, 0x7f1308b3

    invoke-virtual {v2, v3}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/ProfileActivity;->aN:Lazv;

    .line 1994
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v2

    const v3, 0x7f1308c3

    invoke-virtual {v2, v3}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/ProfileActivity;->aO:Lazv;

    .line 1995
    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1996
    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)V

    .line 1998
    :cond_0
    return v1
.end method

.method public b()Lcom/twitter/model/util/FriendshipCache;
    .locals 1

    .prologue
    .line 889
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ad:Lcom/twitter/model/util/FriendshipCache;

    return-object v0
.end method

.method protected b(I)V
    .locals 1

    .prologue
    .line 2953
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->b(I)V

    .line 2954
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->aj:Z

    .line 2955
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->ak:Lcom/twitter/android/ScrollingHeaderActivity$a;

    .line 2956
    return-void
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 12

    .prologue
    .line 472
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v7

    .line 473
    invoke-virtual {v7}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->aE:Ljava/lang/String;

    .line 475
    invoke-virtual {v7}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->O()Lcom/twitter/metrics/j;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/metrics/d;->a(JLcom/twitter/metrics/j;Z)Lcom/twitter/android/metrics/d;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->aB:Lcom/twitter/android/metrics/d;

    .line 476
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aB:Lcom/twitter/android/metrics/d;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/d;->i()V

    .line 477
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 479
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 480
    sget-object v0, Lcom/twitter/android/ProfileActivity$DisplayState;->a:Lcom/twitter/android/ProfileActivity$DisplayState;

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->ay:Lcom/twitter/android/ProfileActivity$DisplayState;

    .line 481
    const/high16 v0, 0x40400000    # 3.0f

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->c(F)V

    .line 482
    new-instance v0, Lcom/twitter/android/ProfileActivity$d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/ProfileActivity$d;-><init>(Lcom/twitter/android/ProfileActivity;Lcom/twitter/android/ProfileActivity$1;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->at:Lcom/twitter/library/service/t;

    .line 483
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->G:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->at:Lcom/twitter/library/service/t;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/t;)V

    .line 484
    new-instance v0, Lcom/twitter/android/ProfileActivity$e;

    invoke-direct {v0, p0}, Lcom/twitter/android/ProfileActivity$e;-><init>(Lcom/twitter/android/ProfileActivity;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->au:Lcom/twitter/android/ProfileActivity$e;

    .line 485
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/twitter/android/profilecompletionmodule/t;->a(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 486
    invoke-static {p0}, Lcom/twitter/android/widget/ProfileEmptyAvatarOverlay;->b(Landroid/support/v4/app/FragmentActivity;)V

    .line 489
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 491
    const-string/jumbo v0, "association"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->al:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 492
    const-string/jumbo v0, "scribe_content"

    sget-object v1, Lcom/twitter/model/timeline/r;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v2, v0, v1}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/r;

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->am:Lcom/twitter/model/timeline/r;

    .line 494
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->ag:Landroid/content/SharedPreferences;

    .line 496
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->B:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lcom/twitter/android/ProfileActivity;->A:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_6

    :cond_1
    const/4 v0, 0x1

    .line 497
    :goto_0
    if-nez v0, :cond_11

    .line 498
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 499
    if-eqz v1, :cond_11

    .line 500
    const-string/jumbo v3, "com.twitter.android.action.USER_SHOW"

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string/jumbo v3, "com.twitter.android.action.USER_SHOW_TYPEAHEAD"

    .line 501
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 502
    :cond_2
    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->B:Ljava/lang/String;

    .line 503
    const/4 v0, 0x1

    move v1, v0

    .line 512
    :goto_1
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 513
    if-eqz v0, :cond_3

    .line 514
    const-string/jumbo v3, "pc"

    .line 515
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 514
    invoke-static {v0}, Lcgi;->a([B)Lcgi;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->ae:Lcgi;

    .line 518
    :cond_3
    if-eqz p1, :cond_9

    .line 519
    const-string/jumbo v0, "state_friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 520
    const-string/jumbo v0, "state_friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/util/FriendshipCache;

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->ad:Lcom/twitter/model/util/FriendshipCache;

    .line 526
    :goto_2
    const-string/jumbo v0, "state_friendship"

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    .line 527
    const-string/jumbo v0, "state_fr"

    const/4 v3, 0x0

    .line 528
    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->ab:Z

    .line 529
    const-string/jumbo v0, "state_user"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->ah:Lcom/twitter/model/core/TwitterUser;

    .line 530
    const-string/jumbo v0, "state_fetch_flags"

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileActivity;->aq:I

    .line 531
    const-string/jumbo v0, "should_hide_blocker_interstitial"

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->ax:Z

    .line 533
    const-string/jumbo v0, "show_dev_follow_rec_prompt"

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->ac:Z

    .line 535
    const-string/jumbo v0, "show_balloon_animation"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->aQ:Z

    .line 536
    const-string/jumbo v0, "updated_avatar"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaFile;

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->aY:Lcom/twitter/media/model/MediaFile;

    .line 548
    :goto_3
    new-instance v0, Lcom/twitter/android/profiles/t;

    iget-object v3, p0, Lcom/twitter/android/ProfileActivity;->ah:Lcom/twitter/model/core/TwitterUser;

    iget-boolean v4, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    iget-object v5, p0, Lcom/twitter/android/ProfileActivity;->ad:Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v0, p0, v3, v4, v5}, Lcom/twitter/android/profiles/t;-><init>(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;ZLcom/twitter/model/util/FriendshipCache;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    .line 549
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    invoke-virtual {v0, p0}, Lcom/twitter/android/profiles/t;->a(Lcom/twitter/android/profiles/t$a;)V

    .line 551
    if-nez v1, :cond_b

    .line 552
    const v0, 0x7f0a09ec

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 553
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    .line 554
    invoke-virtual {v0}, Lcof;->a()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v0}, Lcof;->b()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v0}, Lcof;->o()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 555
    :cond_4
    new-instance v0, Lcpb;

    invoke-direct {v0}, Lcpb;-><init>()V

    const-string/jumbo v1, "intent_extras"

    .line 556
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    const-string/jumbo v1, "intent_data"

    .line 557
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Insufficient arguments to launch ProfileActivity."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 558
    invoke-virtual {v0, v1}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v0

    .line 555
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    .line 561
    :cond_5
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->finish()V

    .line 653
    :goto_4
    return-void

    .line 496
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 504
    :cond_7
    const-string/jumbo v3, "twitter"

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    .line 506
    new-instance v0, Lcom/twitter/android/ProfileActivity$b;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/ProfileActivity$b;-><init>(Lcom/twitter/android/ProfileActivity;Landroid/net/Uri;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/ProfileActivity$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 507
    const/4 v0, 0x1

    move v1, v0

    goto/16 :goto_1

    .line 523
    :cond_8
    new-instance v0, Lcom/twitter/model/util/FriendshipCache;

    const/4 v3, 0x6

    invoke-direct {v0, v3}, Lcom/twitter/model/util/FriendshipCache;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->ad:Lcom/twitter/model/util/FriendshipCache;

    goto/16 :goto_2

    .line 538
    :cond_9
    const-string/jumbo v0, "profile"

    invoke-static {p0, v0}, Lcom/twitter/android/al;->b(Landroid/app/Activity;Ljava/lang/String;)V

    .line 539
    new-instance v0, Lcom/twitter/model/util/FriendshipCache;

    const/4 v3, 0x6

    invoke-direct {v0, v3}, Lcom/twitter/model/util/FriendshipCache;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->ad:Lcom/twitter/model/util/FriendshipCache;

    .line 540
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    .line 541
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->ab:Z

    .line 542
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->aw:Z

    .line 543
    const-string/jumbo v0, "start_page"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 544
    const-string/jumbo v0, "start_page"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->ai:Landroid/net/Uri;

    .line 546
    :cond_a
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->aQ:Z

    goto/16 :goto_3

    .line 565
    :cond_b
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->P()Lcom/twitter/android/search/SearchSuggestionController;

    move-result-object v0

    .line 566
    iget-wide v4, p0, Lcom/twitter/android/ProfileActivity;->A:J

    invoke-virtual {v7}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v10

    cmp-long v1, v4, v10

    if-eqz v1, :cond_10

    .line 567
    const-string/jumbo v1, "profile"

    invoke-virtual {v0, v1}, Lcom/twitter/android/search/SearchSuggestionController;->a(Ljava/lang/String;)Lcom/twitter/android/search/SearchSuggestionController;

    .line 572
    :goto_5
    const-string/jumbo v0, "reason"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->aa:Ljava/lang/String;

    .line 574
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 576
    const v1, 0x7f0402cc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/LinearLayout;

    .line 581
    invoke-virtual {v6, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 583
    iput-object v6, p0, Lcom/twitter/android/ProfileActivity;->W:Landroid/widget/LinearLayout;

    .line 584
    new-instance v0, Lcom/twitter/android/profiles/ProfileDetailsViewManager;

    invoke-direct {v0, v6}, Lcom/twitter/android/profiles/ProfileDetailsViewManager;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->M:Lcom/twitter/android/profiles/ProfileDetailsViewManager;

    .line 585
    const v0, 0x7f130010

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->T:Landroid/view/ViewGroup;

    .line 586
    const v0, 0x7f1306a1

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/profiles/HeaderImageView;

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->N:Lcom/twitter/android/profiles/HeaderImageView;

    .line 587
    const-string/jumbo v0, "bitmaps"

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->b_(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 588
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->N:Lcom/twitter/android/profiles/HeaderImageView;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->v()I

    move-result v2

    invoke-virtual {v1, p0, v0, v2}, Lcom/twitter/android/profiles/HeaderImageView;->a(Lcom/twitter/android/profiles/HeaderImageView$a;Ljava/util/Set;I)V

    .line 589
    invoke-direct {p0, v8}, Lcom/twitter/android/ProfileActivity;->c(Landroid/content/res/Resources;)Lcom/twitter/media/ui/image/UserImageView;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->O:Lcom/twitter/media/ui/image/UserImageView;

    .line 590
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->O:Lcom/twitter/media/ui/image/UserImageView;

    invoke-static {p0, v0}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;Lcom/twitter/media/ui/image/UserImageView;)Landroid/widget/FrameLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->aW:Landroid/widget/FrameLayout;

    .line 591
    new-instance v0, Lcom/twitter/android/profiles/a;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->O:Lcom/twitter/media/ui/image/UserImageView;

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->N:Lcom/twitter/android/profiles/HeaderImageView;

    iget v3, p0, Lcom/twitter/android/ProfileActivity;->r:I

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/profiles/a;-><init>(Lcom/twitter/media/ui/image/MediaImageView;Lcom/twitter/media/ui/image/BackgroundImageView;I)V

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->P:Lcom/twitter/android/profiles/a;

    .line 593
    const v0, 0x7f13069f

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->ar:Landroid/view/View;

    .line 594
    const v0, 0x7f13069e

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->as:Landroid/view/ViewGroup;

    .line 595
    const v0, 0x7f13069b

    .line 596
    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->aT:Landroid/view/ViewGroup;

    .line 597
    const v0, 0x7f13069a

    .line 598
    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->aU:Lcom/twitter/ui/widget/TypefacesTextView;

    .line 599
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aU:Lcom/twitter/ui/widget/TypefacesTextView;

    .line 600
    invoke-virtual {v0}, Lcom/twitter/ui/widget/TypefacesTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 601
    if-eqz v0, :cond_c

    .line 602
    const v1, 0x7f110177

    .line 603
    invoke-static {p0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    .line 602
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 606
    :cond_c
    new-instance v0, Lcom/twitter/android/profiles/o;

    const v1, 0x7f130682

    .line 607
    invoke-virtual {v6, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v4

    iget-boolean v5, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/profiles/o;-><init>(Landroid/content/Context;Lcom/twitter/android/profiles/o$a;Landroid/widget/LinearLayout;Lcom/twitter/model/core/TwitterUser;Z)V

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->av:Lcom/twitter/android/profiles/o;

    .line 609
    const v0, 0x7f130543

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TweetStatView;

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->R:Lcom/twitter/android/widget/TweetStatView;

    .line 610
    const v0, 0x7f130544

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TweetStatView;

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->Q:Lcom/twitter/android/widget/TweetStatView;

    .line 612
    const v0, 0x7f130698

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->az:Landroid/view/View;

    .line 613
    const v0, 0x7f130488

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->aA:Landroid/view/View;

    .line 614
    const v0, 0x7f13067c

    .line 615
    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/profiles/animation/BalloonSetAnimationView;

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->aP:Lcom/twitter/android/profiles/animation/BalloonSetAnimationView;

    .line 616
    invoke-virtual {p0, v6}, Lcom/twitter/android/ProfileActivity;->setHeaderView(Landroid/view/View;)V

    .line 618
    const v0, 0x7f130058

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 619
    const v1, 0x7f0e03f6

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iget v2, p0, Lcom/twitter/android/ProfileActivity;->r:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    .line 621
    invoke-virtual {v0}, Landroid/widget/TextView;->getTextSize()F

    move-result v0

    add-float/2addr v0, v1

    iput v0, p0, Lcom/twitter/android/ProfileActivity;->U:F

    .line 622
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->o:Lcom/twitter/android/widget/UnboundedFrameLayout;

    const v1, 0x7f130696

    .line 623
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/UnboundedFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 624
    new-instance v0, Lcom/twitter/android/profiles/e;

    .line 625
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    iget-object v4, p0, Lcom/twitter/android/ProfileActivity;->al:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/profiles/e;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/v;Lcom/twitter/android/profiles/t;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Landroid/view/View;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->aG:Lcom/twitter/android/profiles/d;

    .line 627
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aG:Lcom/twitter/android/profiles/d;

    invoke-interface {v0, p0}, Lcom/twitter/android/profiles/d;->a(Lcom/twitter/android/profiles/q$a;)V

    .line 628
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aG:Lcom/twitter/android/profiles/d;

    invoke-interface {v0, p1}, Lcom/twitter/android/profiles/d;->a(Landroid/os/Bundle;)V

    .line 633
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->aD()V

    .line 634
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->aC()V

    .line 636
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ah:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_d

    .line 637
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ah:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 640
    :cond_d
    iget-wide v0, p0, Lcom/twitter/android/ProfileActivity;->A:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->B:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 641
    :cond_e
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->C()V

    .line 643
    :cond_f
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->ah:Lcom/twitter/model/core/TwitterUser;

    .line 645
    const v0, 0x7f110190

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileActivity;->X:I

    .line 647
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, p0, v1, v2}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    .line 648
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->aD:Z

    .line 650
    new-instance v0, Lcom/twitter/android/profiles/k;

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    new-instance v5, Lcom/twitter/android/bl;

    invoke-direct {v5, p0}, Lcom/twitter/android/bl;-><init>(Landroid/content/Context;)V

    move-object v1, p0

    move-object v3, v7

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/profiles/k;-><init>(Lcom/twitter/android/profiles/j$a;Lcom/twitter/android/profiles/t;Lcom/twitter/library/client/Session;Landroid/content/Context;Lcom/twitter/android/bl;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->aI:Lcom/twitter/android/profiles/j;

    .line 652
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aI:Lcom/twitter/android/profiles/j;

    invoke-interface {v0, p0}, Lcom/twitter/android/profiles/j;->a(Lcom/twitter/android/profiles/j$a;)V

    goto/16 :goto_4

    .line 569
    :cond_10
    const-string/jumbo v1, "me"

    invoke-virtual {v0, v1}, Lcom/twitter/android/search/SearchSuggestionController;->a(Ljava/lang/String;)Lcom/twitter/android/search/SearchSuggestionController;

    goto/16 :goto_5

    :cond_11
    move v1, v0

    goto/16 :goto_1
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 1073
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->b(Z)V

    .line 1074
    if-eqz p1, :cond_0

    .line 1075
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->D()V

    .line 1077
    :cond_0
    return-void
.end method

.method public c()Lcom/twitter/android/profiles/t;
    .locals 1

    .prologue
    .line 894
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    return-object v0
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 2999
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->av:Lcom/twitter/android/profiles/o;

    invoke-virtual {v0, p1}, Lcom/twitter/android/profiles/o;->c(I)V

    .line 3000
    return-void
.end method

.method protected d()V
    .locals 2

    .prologue
    .line 924
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ak:Lcom/twitter/android/ScrollingHeaderActivity$a;

    if-eqz v0, :cond_0

    .line 925
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ak:Lcom/twitter/android/ScrollingHeaderActivity$a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/ScrollingHeaderActivity$a;->cancel(Z)Z

    .line 926
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->ak:Lcom/twitter/android/ScrollingHeaderActivity$a;

    .line 928
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->G:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->at:Lcom/twitter/library/service/t;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->b(Lcom/twitter/library/service/t;)V

    .line 929
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->aD:Z

    if-eqz v0, :cond_1

    .line 930
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/accounts/AccountManager;->removeOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;)V

    .line 932
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aG:Lcom/twitter/android/profiles/d;

    if-eqz v0, :cond_2

    .line 933
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aG:Lcom/twitter/android/profiles/d;

    invoke-interface {v0}, Lcom/twitter/android/profiles/d;->p()V

    .line 935
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aX:Lcmw;

    if-eqz v0, :cond_3

    .line 936
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aX:Lcmw;

    invoke-virtual {v0}, Lcmw;->close()V

    .line 938
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aZ:Lcom/twitter/android/ProfileActivity$c;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 939
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->d()V

    .line 940
    return-void
.end method

.method protected e()I
    .locals 1

    .prologue
    .line 1032
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->ab_()I

    move-result v0

    return v0
.end method

.method public h()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1737
    invoke-direct {p0, v0, v0}, Lcom/twitter/android/ProfileActivity;->a(ZZ)V

    .line 1738
    return-void
.end method

.method public i()V
    .locals 4

    .prologue
    .line 1742
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aM:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1743
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/ProfileActivity;->a(ZZ)V

    .line 1744
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ad:Lcom/twitter/model/util/FriendshipCache;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    invoke-virtual {v1}, Lcom/twitter/android/profiles/t;->e()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->g(J)V

    .line 1745
    return-void
.end method

.method public j()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1749
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aM:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1750
    const/4 v0, 0x1

    invoke-direct {p0, v1, v0}, Lcom/twitter/android/ProfileActivity;->a(ZZ)V

    .line 1751
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ad:Lcom/twitter/model/util/FriendshipCache;

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->aH:Lcom/twitter/android/profiles/t;

    invoke-virtual {v1}, Lcom/twitter/android/profiles/t;->e()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->f(J)V

    .line 1752
    return-void
.end method

.method protected k()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1981
    iget-wide v0, p0, Lcom/twitter/android/ProfileActivity;->A:J

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1982
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1984
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method l()V
    .locals 4

    .prologue
    .line 2267
    .line 2268
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x6

    const/4 v2, 0x0

    new-instance v3, Lcom/twitter/android/ProfileActivity$6;

    invoke-direct {v3, p0, p0}, Lcom/twitter/android/ProfileActivity$6;-><init>(Lcom/twitter/android/ProfileActivity;Landroid/app/Activity;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 2297
    return-void
.end method

.method protected l_()V
    .locals 2

    .prologue
    .line 700
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->l_()V

    .line 701
    const-string/jumbo v0, "bitmaps"

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->N:Lcom/twitter/android/profiles/HeaderImageView;

    invoke-virtual {v1}, Lcom/twitter/android/profiles/HeaderImageView;->getSavedBitmaps()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileActivity;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 702
    return-void
.end method

.method protected n()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2491
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/ProfileActivity;->A:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "profile_system_prompt"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v4, v1, v2

    const/4 v2, 0x2

    aput-object v4, v1, v2

    const/4 v2, 0x3

    aput-object v4, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "impression"

    aput-object v3, v1, v2

    .line 2492
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 2491
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 2493
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/16 v1, 0xf

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a06e6

    .line 2494
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a06e5

    .line 2495
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a06e4

    .line 2496
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a05eb

    .line 2497
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 2498
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/ProfileActivity$7;

    invoke-direct {v1, p0}, Lcom/twitter/android/ProfileActivity$7;-><init>(Lcom/twitter/android/ProfileActivity;)V

    .line 2499
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$a;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 2507
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 2508
    return-void
.end method

.method public o()V
    .locals 2

    .prologue
    .line 2699
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->M:Lcom/twitter/android/profiles/ProfileDetailsViewManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/profiles/ProfileDetailsViewManager;->a(Z)V

    .line 2700
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ap:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2701
    return-void
.end method

.method public onAccountsUpdated([Landroid/accounts/Account;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1716
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aE:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/twitter/util/collection/CollectionUtils;->a([Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1733
    :cond_0
    :goto_0
    return-void

    .line 1721
    :cond_1
    array-length v4, p1

    move v2, v1

    move v0, v3

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, p1, v2

    .line 1724
    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/ProfileActivity;->aE:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v0, v1

    .line 1721
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1729
    :cond_3
    if-eqz v0, :cond_0

    .line 1730
    iput-boolean v3, p0, Lcom/twitter/android/ProfileActivity;->aF:Z

    .line 1731
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->aE:Ljava/lang/String;

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 11

    .prologue
    const-wide/16 v4, 0x0

    const/4 v10, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, -0x1

    .line 2512
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2513
    if-nez p2, :cond_1

    .line 2636
    :cond_0
    :goto_0
    return-void

    .line 2517
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 2519
    :pswitch_0
    invoke-static {p3}, Lcom/twitter/app/lists/b;->a(Landroid/content/Intent;)Lcom/twitter/app/lists/b;

    move-result-object v0

    .line 2520
    new-instance v1, Lbct;

    .line 2521
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 2522
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 2523
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    iget-wide v6, v0, Lcom/twitter/app/lists/b;->b:J

    iget-wide v8, v0, Lcom/twitter/app/lists/b;->a:J

    invoke-direct/range {v1 .. v10}, Lbct;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJJI)V

    .line 2527
    const/4 v0, 0x7

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/ProfileActivity;->b(Lcom/twitter/library/service/s;I)Z

    goto :goto_0

    .line 2531
    :pswitch_1
    if-ne v1, p2, :cond_0

    if-eqz p3, :cond_0

    .line 2532
    const-string/jumbo v0, "remove_header"

    invoke-virtual {p3, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 2533
    const-string/jumbo v0, "updated_profile_picture"

    .line 2534
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaFile;

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->aY:Lcom/twitter/media/model/MediaFile;

    .line 2536
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/ProfileActivity;->a(Lcom/twitter/model/core/TwitterUser;Z)Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    .line 2537
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ap()V

    .line 2538
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ah()V

    .line 2539
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->aB()V

    .line 2540
    iget v0, p0, Lcom/twitter/android/ProfileActivity;->aq:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/twitter/android/ProfileActivity;->aq:I

    .line 2542
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aY:Lcom/twitter/media/model/MediaFile;

    if-eqz v0, :cond_0

    .line 2543
    invoke-static {}, Lbpr;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2544
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->n()V

    goto :goto_0

    .line 2545
    :cond_2
    invoke-static {}, Lbpr;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2546
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aY:Lcom/twitter/media/model/MediaFile;

    invoke-static {p0, v0}, Lcom/twitter/android/ProfilePhotoAutoTweetActivity;->a(Landroid/content/Context;Lcom/twitter/media/model/MediaFile;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 2547
    :cond_3
    invoke-static {}, Lbpr;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2548
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/twitter/android/ProfilePhotoAutoTweetActivity;->a(Landroid/content/Context;Lcom/twitter/media/model/MediaFile;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 2555
    :pswitch_2
    if-ne p2, v2, :cond_0

    .line 2556
    const-string/jumbo v0, "account"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/UserAccount;

    .line 2558
    iget-object v0, v0, Lcom/twitter/model/account/UserAccount;->a:Landroid/accounts/Account;

    .line 2559
    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 2560
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    .line 2559
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2561
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v1

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/v;->d(Ljava/lang/String;)V

    .line 2564
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 2565
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v2, v3, v0}, Lcom/twitter/android/ProfileActivity;->a(JLjava/lang/String;)V

    goto/16 :goto_0

    .line 2571
    :pswitch_3
    if-ne v1, p2, :cond_0

    if-eqz p3, :cond_0

    .line 2572
    const-string/jumbo v0, "user_id"

    invoke-virtual {p3, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2573
    cmp-long v2, v0, v4

    if-lez v2, :cond_0

    const-string/jumbo v2, "friendship"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2574
    const-string/jumbo v2, "friendship"

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 2576
    iget-object v3, p0, Lcom/twitter/android/ProfileActivity;->ad:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v3, v0, v1, v2}, Lcom/twitter/model/util/FriendshipCache;->a(JI)Z

    move-result v3

    if-nez v3, :cond_4

    .line 2577
    iget-object v3, p0, Lcom/twitter/android/ProfileActivity;->ad:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v3, v0, v1, v2}, Lcom/twitter/model/util/FriendshipCache;->b(JI)V

    .line 2578
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->aB()V

    .line 2580
    :cond_4
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->aB()V

    goto/16 :goto_0

    .line 2588
    :pswitch_4
    packed-switch p2, :pswitch_data_1

    goto/16 :goto_0

    .line 2590
    :pswitch_5
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->av()V

    goto/16 :goto_0

    .line 2594
    :pswitch_6
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aI:Lcom/twitter/android/profiles/j;

    invoke-interface {v0}, Lcom/twitter/android/profiles/j;->a()V

    goto/16 :goto_0

    .line 2598
    :pswitch_7
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ax()V

    goto/16 :goto_0

    .line 2608
    :pswitch_8
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    if-eqz v0, :cond_5

    if-ne p2, v2, :cond_5

    .line 2609
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/EditProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2610
    iget-wide v2, p0, Lcom/twitter/android/ProfileActivity;->A:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 2611
    sget-object v2, Lcom/twitter/database/schema/a$z;->b:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 2612
    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string/jumbo v3, "ownerId"

    .line 2613
    invoke-virtual {v2, v3, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 2614
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 2611
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2615
    invoke-virtual {p0, v0, v10}, Lcom/twitter/android/ProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 2616
    :cond_5
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->C:Z

    if-eqz v0, :cond_0

    if-ne p2, v1, :cond_0

    if-eqz p3, :cond_0

    .line 2617
    const-string/jumbo v0, "extra_editable_image"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    .line 2618
    if-eqz v0, :cond_0

    .line 2619
    iget-object v0, v0, Lcom/twitter/model/media/EditableImage;->k:Lcom/twitter/media/model/MediaFile;

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->aY:Lcom/twitter/media/model/MediaFile;

    .line 2620
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->n()V

    goto/16 :goto_0

    .line 2626
    :pswitch_9
    if-eqz p3, :cond_0

    .line 2627
    const-string/jumbo v0, "AccountNotificationActivity_friendship"

    iget v1, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2628
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->av:Lcom/twitter/android/profiles/o;

    invoke-virtual {v1, v0}, Lcom/twitter/android/profiles/o;->a(I)V

    goto/16 :goto_0

    .line 2517
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 2588
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 6

    .prologue
    .line 2678
    const/4 v0, -0x1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v2, "user_id"

    iget-wide v4, p0, Lcom/twitter/android/ProfileActivity;->A:J

    .line 2679
    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "friendship"

    iget v3, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    .line 2680
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    .line 2678
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileActivity;->setResult(ILandroid/content/Intent;)V

    .line 2681
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->onBackPressed()V

    .line 2682
    return-void
.end method

.method public onButtonBarItemClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/16 v6, 0x100

    const/4 v3, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1318
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1415
    :goto_0
    :pswitch_0
    return-void

    .line 1320
    :pswitch_1
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1321
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->S:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/twitter/android/al;->a(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V

    goto :goto_0

    .line 1324
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->at()V

    goto :goto_0

    .line 1329
    :pswitch_2
    iget v0, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v0}, Lcom/twitter/model/core/g;->i(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-static {v0}, Lcom/twitter/model/core/g;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1330
    :cond_1
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->p(I)V

    goto :goto_0

    .line 1332
    :cond_2
    invoke-direct {p0, v4}, Lcom/twitter/android/ProfileActivity;->p(I)V

    goto :goto_0

    .line 1337
    :pswitch_3
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->p(I)V

    goto :goto_0

    .line 1341
    :pswitch_4
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, ":user:blocked_button:click"

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    .line 1342
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, ":user:unblock_dialog:impression"

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    .line 1343
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->B:Ljava/lang/String;

    .line 1344
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 1343
    invoke-static {p0, v0, v3, v1}, Lcom/twitter/android/util/y;->b(Landroid/content/Context;Ljava/lang/String;ILandroid/support/v4/app/FragmentManager;)V

    goto :goto_0

    .line 1348
    :pswitch_5
    new-instance v0, Lbij;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lbij;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/TwitterUser;Lcgi;)V

    const/4 v1, 0x2

    .line 1349
    invoke-virtual {v0, v1, v5}, Lbij;->a(IZ)Lbij;

    move-result-object v0

    const/16 v1, 0xd

    .line 1348
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 1351
    invoke-direct {p0, v6}, Lcom/twitter/android/ProfileActivity;->n(I)V

    .line 1352
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ag:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "lifeline_unfollow_dialog_shown"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1353
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->p(I)V

    .line 1354
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ag:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "lifeline_unfollow_dialog_shown"

    .line 1355
    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1356
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1362
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->av:Lcom/twitter/android/profiles/o;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/o;->a()V

    goto/16 :goto_0

    .line 1358
    :cond_3
    const v0, 0x7f0a09f3

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    .line 1360
    invoke-virtual {v2}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    .line 1359
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1358
    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 1360
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 1366
    :pswitch_6
    iget-wide v0, p0, Lcom/twitter/android/ProfileActivity;->A:J

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->ae:Lcgi;

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/ProfileActivity;->a(JLcgi;)V

    .line 1367
    invoke-direct {p0, v6}, Lcom/twitter/android/ProfileActivity;->m(I)V

    .line 1369
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ag:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "lifeline_follow_dialog_shown"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1370
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->p(I)V

    .line 1371
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ag:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "lifeline_follow_dialog_shown"

    .line 1372
    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1373
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1375
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->av:Lcom/twitter/android/profiles/o;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/o;->b()V

    goto/16 :goto_0

    .line 1379
    :pswitch_7
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ao()V

    goto/16 :goto_0

    .line 1383
    :pswitch_8
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/AccountsDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "AccountsDialogActivity_account_name"

    .line 1385
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    .line 1384
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1383
    invoke-virtual {p0, v0, v3}, Lcom/twitter/android/ProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1390
    :pswitch_9
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->az()V

    goto/16 :goto_0

    .line 1394
    :pswitch_a
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->p(I)V

    goto/16 :goto_0

    .line 1399
    :pswitch_b
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->an()V

    goto/16 :goto_0

    .line 1403
    :pswitch_c
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "::message:click"

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    .line 1404
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    invoke-static {p0, v0}, Lcom/twitter/android/profiles/v;->a(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;)V

    goto/16 :goto_0

    .line 1408
    :pswitch_d
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, ":user:open_ads_companion:click"

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    .line 1409
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {p0, v0, v1}, Lcom/twitter/android/ads/AdsCompanionWebViewActivity;->a(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1318
    nop

    :pswitch_data_0
    .packed-switch 0x7f130683
        :pswitch_d
        :pswitch_9
        :pswitch_a
        :pswitch_c
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_b
        :pswitch_b
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2706
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 2744
    :goto_0
    return-void

    .line 2708
    :sswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->N()Z

    goto :goto_0

    .line 2712
    :sswitch_1
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2714
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->S:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/twitter/android/al;->a(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V

    goto :goto_0

    .line 2717
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->aq()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 2722
    :sswitch_2
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2724
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->S:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/twitter/android/al;->a(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V

    goto :goto_0

    .line 2728
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    const/4 v3, 0x0

    .line 2727
    invoke-static {p0, v0, v1, v2, v3}, Lcom/twitter/android/util/i;->a(Landroid/content/Context;JLcom/twitter/model/core/TwitterUser;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 2733
    :sswitch_3
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileActivity;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 2737
    :sswitch_4
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->V:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileActivity;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 2706
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f13031a -> :sswitch_3
        0x7f130543 -> :sswitch_1
        0x7f130544 -> :sswitch_2
        0x7f13065d -> :sswitch_0
        0x7f1306a1 -> :sswitch_4
    .end sparse-switch
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 899
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getId()I

    move-result v0

    const v1, 0x7f13069d

    if-ne v0, v1, :cond_0

    .line 900
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->startActivity(Landroid/content/Intent;)V

    .line 904
    :goto_0
    return-void

    .line 902
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 984
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aG:Lcom/twitter/android/profiles/d;

    if-eqz v0, :cond_0

    .line 985
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aG:Lcom/twitter/android/profiles/d;

    invoke-interface {v0}, Lcom/twitter/android/profiles/d;->q()V

    .line 987
    :cond_0
    invoke-static {}, Lcpd;->a()Lcpd;

    move-result-object v0

    invoke-virtual {v0}, Lcpd;->b()Lcpa;

    move-result-object v0

    const-string/jumbo v1, "profile_user"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcpa;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 988
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->onPause()V

    .line 989
    return-void
.end method

.method protected onRestart()V
    .locals 1

    .prologue
    .line 908
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->onRestart()V

    .line 909
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->N:Lcom/twitter/android/profiles/HeaderImageView;

    if-eqz v0, :cond_0

    .line 910
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->N:Lcom/twitter/android/profiles/HeaderImageView;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/HeaderImageView;->e()V

    .line 912
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 8

    .prologue
    .line 971
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->onResume()V

    .line 974
    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->aF:Z

    if-eqz v0, :cond_0

    .line 975
    iget-wide v0, p0, Lcom/twitter/android/ProfileActivity;->A:J

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->B:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/ProfileActivity;->a(JLjava/lang/String;)V

    .line 978
    :cond_0
    invoke-static {}, Lcpd;->a()Lcpd;

    move-result-object v0

    invoke-virtual {v0}, Lcpd;->b()Lcpa;

    move-result-object v0

    const-string/jumbo v1, "profile_user"

    const-string/jumbo v2, "id: %d, name: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v6, p0, Lcom/twitter/android/ProfileActivity;->A:J

    .line 979
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/twitter/android/ProfileActivity;->B:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 978
    invoke-virtual {v0, v1, v2}, Lcpa;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 980
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 944
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 945
    const-string/jumbo v0, "state_fetch_flags"

    iget v1, p0, Lcom/twitter/android/ProfileActivity;->aq:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 946
    const-string/jumbo v0, "state_friendship"

    iget v1, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 947
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aG:Lcom/twitter/android/profiles/d;

    if-eqz v0, :cond_0

    .line 948
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->aG:Lcom/twitter/android/profiles/d;

    invoke-interface {v0, p1}, Lcom/twitter/android/profiles/d;->b(Landroid/os/Bundle;)V

    .line 949
    const-string/jumbo v0, "state_fr"

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->aG:Lcom/twitter/android/profiles/d;

    .line 950
    invoke-interface {v1}, Lcom/twitter/android/profiles/d;->k()Z

    move-result v1

    .line 949
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 953
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ad:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/model/util/FriendshipCache;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 954
    const-string/jumbo v0, "state_friendship_cache"

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->ad:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 957
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_3

    .line 958
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    iget v1, p0, Lcom/twitter/android/ProfileActivity;->Z:I

    iput v1, v0, Lcom/twitter/model/core/TwitterUser;->U:I

    .line 959
    const-string/jumbo v0, "state_user"

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 963
    :cond_2
    :goto_0
    const-string/jumbo v0, "should_hide_blocker_interstitial"

    iget-boolean v1, p0, Lcom/twitter/android/ProfileActivity;->ax:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 964
    const-string/jumbo v0, "show_dev_follow_rec_prompt"

    iget-boolean v1, p0, Lcom/twitter/android/ProfileActivity;->ac:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 965
    const-string/jumbo v0, "show_balloon_animation"

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->aP:Lcom/twitter/android/profiles/animation/BalloonSetAnimationView;

    invoke-virtual {v1}, Lcom/twitter/android/profiles/animation/BalloonSetAnimationView;->c()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 966
    const-string/jumbo v0, "updated_avatar"

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->aY:Lcom/twitter/media/model/MediaFile;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 967
    return-void

    .line 960
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->ah:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_2

    .line 961
    const-string/jumbo v0, "state_user"

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->ah:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 916
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->N:Lcom/twitter/android/profiles/HeaderImageView;

    if-eqz v0, :cond_0

    .line 917
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->N:Lcom/twitter/android/profiles/HeaderImageView;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/HeaderImageView;->f()V

    .line 919
    :cond_0
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->onStop()V

    .line 920
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 830
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->onWindowFocusChanged(Z)V

    .line 832
    if-eqz p1, :cond_0

    .line 833
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->j(Z)V

    .line 835
    :cond_0
    return-void
.end method

.method public q()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 2748
    iput-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->ax:Z

    .line 2749
    invoke-virtual {p0, v2}, Lcom/twitter/android/ProfileActivity;->a(Z)V

    .line 2750
    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "blocker_interstitial:::click"

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileActivity;->a([Ljava/lang/String;)V

    .line 2751
    return-void
.end method

.method protected r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2915
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->B:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->D:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2920
    invoke-direct {p0}, Lcom/twitter/android/ProfileActivity;->ad()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setHeaderView(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2941
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    .line 2942
    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 2943
    instance-of v1, v0, Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 2944
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 2945
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->t:Lcom/twitter/android/widget/SwipeProgressBarView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->indexOfChild(Landroid/view/View;)I

    move-result v1

    .line 2946
    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->O:Lcom/twitter/media/ui/image/UserImageView;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    .line 2948
    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderUserQueryActivity;->setHeaderView(Landroid/view/View;)V

    .line 2949
    return-void
.end method

.method protected t()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2961
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->L:Lcom/twitter/android/profiles/l;

    if-eqz v0, :cond_0

    .line 2962
    iget-object v0, p0, Lcom/twitter/android/ProfileActivity;->L:Lcom/twitter/android/profiles/l;

    invoke-interface {v0}, Lcom/twitter/android/profiles/l;->a()Ljava/util/List;

    move-result-object v0

    .line 2964
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
