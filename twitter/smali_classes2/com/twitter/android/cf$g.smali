.class public final Lcom/twitter/android/cf$g;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/cf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "g"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/cf$g$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/twitter/android/cf$b;

.field public final b:Lcom/twitter/android/cf$a;

.field public final c:Lcom/twitter/android/cf$l;

.field public final d:Lcom/twitter/android/cf$k;

.field public final e:Lcom/twitter/android/cf$j;

.field public final f:Lcom/twitter/android/cf$i;

.field public final g:Lcom/twitter/android/widget/EventView;

.field public final h:Lcom/twitter/android/cb;

.field public final i:Lcom/twitter/android/z;

.field public final j:Lcom/twitter/android/widget/WhoToFollowUsersView;

.field public final k:Lcom/twitter/android/cf$e;

.field public final l:Lcom/twitter/android/cu;

.field public final m:Lcom/twitter/android/cf$f;

.field public final n:Lcom/twitter/android/cf$d;


# direct methods
.method private constructor <init>(Lcom/twitter/android/cf$g$a;)V
    .locals 1

    .prologue
    .line 2014
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2015
    invoke-static {p1}, Lcom/twitter/android/cf$g$a;->a(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cf$b;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cf$g;->a:Lcom/twitter/android/cf$b;

    .line 2016
    invoke-static {p1}, Lcom/twitter/android/cf$g$a;->b(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cf$a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cf$g;->b:Lcom/twitter/android/cf$a;

    .line 2017
    invoke-static {p1}, Lcom/twitter/android/cf$g$a;->c(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cf$l;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cf$g;->c:Lcom/twitter/android/cf$l;

    .line 2018
    invoke-static {p1}, Lcom/twitter/android/cf$g$a;->d(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cf$k;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cf$g;->d:Lcom/twitter/android/cf$k;

    .line 2019
    invoke-static {p1}, Lcom/twitter/android/cf$g$a;->e(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cf$j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cf$g;->e:Lcom/twitter/android/cf$j;

    .line 2020
    invoke-static {p1}, Lcom/twitter/android/cf$g$a;->f(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cf$i;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cf$g;->f:Lcom/twitter/android/cf$i;

    .line 2021
    invoke-static {p1}, Lcom/twitter/android/cf$g$a;->g(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/widget/EventView;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cf$g;->g:Lcom/twitter/android/widget/EventView;

    .line 2022
    invoke-static {p1}, Lcom/twitter/android/cf$g$a;->h(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cb;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cf$g;->h:Lcom/twitter/android/cb;

    .line 2023
    invoke-static {p1}, Lcom/twitter/android/cf$g$a;->i(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/z;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cf$g;->i:Lcom/twitter/android/z;

    .line 2024
    invoke-static {p1}, Lcom/twitter/android/cf$g$a;->j(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/widget/WhoToFollowUsersView;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cf$g;->j:Lcom/twitter/android/widget/WhoToFollowUsersView;

    .line 2025
    invoke-static {p1}, Lcom/twitter/android/cf$g$a;->k(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cf$e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cf$g;->k:Lcom/twitter/android/cf$e;

    .line 2026
    invoke-static {p1}, Lcom/twitter/android/cf$g$a;->l(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cu;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cf$g;->l:Lcom/twitter/android/cu;

    .line 2027
    invoke-static {p1}, Lcom/twitter/android/cf$g$a;->m(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cf$f;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cf$g;->m:Lcom/twitter/android/cf$f;

    .line 2028
    invoke-static {p1}, Lcom/twitter/android/cf$g$a;->n(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cf$d;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cf$g;->n:Lcom/twitter/android/cf$d;

    .line 2029
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/cf$g$a;Lcom/twitter/android/cf$1;)V
    .locals 0

    .prologue
    .line 1997
    invoke-direct {p0, p1}, Lcom/twitter/android/cf$g;-><init>(Lcom/twitter/android/cf$g$a;)V

    return-void
.end method
