.class public Lcom/twitter/android/aa;
.super Lcom/twitter/android/UsersAdapter;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/ui/user/BaseUserView$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserApprovalView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/ui/user/BaseUserView$a;Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserApprovalView;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 42
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/UsersAdapter;-><init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/UsersAdapter$CheckboxConfig;)V

    .line 43
    iput-object p3, p0, Lcom/twitter/android/aa;->a:Ljava/util/Map;

    .line 44
    iput-object p2, p0, Lcom/twitter/android/aa;->b:Lcom/twitter/ui/user/BaseUserView$a;

    .line 45
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 50
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040420

    .line 51
    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/user/UserApprovalView;

    .line 52
    iget-object v1, p0, Lcom/twitter/android/aa;->b:Lcom/twitter/ui/user/BaseUserView$a;

    .line 54
    const v2, 0x7f02020b

    invoke-virtual {v0, v3, v2, v1}, Lcom/twitter/ui/user/UserApprovalView;->a(IILcom/twitter/ui/user/BaseUserView$a;)V

    .line 56
    const/4 v2, 0x1

    const v3, 0x7f02034b

    invoke-virtual {v0, v2, v3, v1}, Lcom/twitter/ui/user/UserApprovalView;->a(IILcom/twitter/ui/user/BaseUserView$a;)V

    .line 57
    const v2, 0x7f0200b0

    invoke-virtual {v0, v4, v2, v1}, Lcom/twitter/ui/user/UserApprovalView;->a(IILcom/twitter/ui/user/BaseUserView$a;)V

    .line 59
    const v1, 0x7f0200b1

    invoke-virtual {v0, v4, v1}, Lcom/twitter/ui/user/UserApprovalView;->a(II)V

    .line 60
    new-instance v1, Lcom/twitter/android/db;

    invoke-direct {v1, v0}, Lcom/twitter/android/db;-><init>(Lcom/twitter/ui/user/BaseUserView;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserApprovalView;->setTag(Ljava/lang/Object;)V

    .line 61
    return-object v0
.end method

.method public bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 17
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/aa;->a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;I)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    .line 66
    invoke-interface {p3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-object v2, p1

    .line 67
    check-cast v2, Lcom/twitter/ui/user/UserApprovalView;

    move-object v1, p0

    move-object v3, p3

    move v6, p4

    .line 68
    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/aa;->a(Lcom/twitter/ui/user/BaseUserView;Landroid/database/Cursor;JI)V

    .line 69
    invoke-virtual {p0}, Lcom/twitter/android/aa;->d()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 70
    invoke-virtual {v2, v7}, Lcom/twitter/ui/user/UserApprovalView;->setState(I)V

    .line 71
    invoke-virtual {v2}, Lcom/twitter/ui/user/UserApprovalView;->g()V

    .line 102
    :goto_0
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/aa;->a:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 74
    if-eqz v0, :cond_1

    .line 75
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 92
    invoke-virtual {v2, v7}, Lcom/twitter/ui/user/UserApprovalView;->setState(I)V

    .line 93
    invoke-virtual {v2}, Lcom/twitter/ui/user/UserApprovalView;->c()V

    goto :goto_0

    .line 77
    :pswitch_0
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lcom/twitter/ui/user/UserApprovalView;->setState(I)V

    .line 78
    invoke-virtual {v2}, Lcom/twitter/ui/user/UserApprovalView;->d()V

    goto :goto_0

    .line 82
    :pswitch_1
    invoke-virtual {v2, v8}, Lcom/twitter/ui/user/UserApprovalView;->setState(I)V

    .line 83
    invoke-virtual {v2}, Lcom/twitter/ui/user/UserApprovalView;->g()V

    goto :goto_0

    .line 87
    :pswitch_2
    const/4 v0, 0x3

    invoke-virtual {v2, v0}, Lcom/twitter/ui/user/UserApprovalView;->setState(I)V

    .line 88
    invoke-virtual {v2}, Lcom/twitter/ui/user/UserApprovalView;->d()V

    goto :goto_0

    .line 98
    :cond_1
    invoke-virtual {v2, v7}, Lcom/twitter/ui/user/UserApprovalView;->setState(I)V

    .line 99
    invoke-virtual {v2}, Lcom/twitter/ui/user/UserApprovalView;->c()V

    goto :goto_0

    .line 75
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 17
    check-cast p3, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/android/aa;->a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;I)V

    return-void
.end method
