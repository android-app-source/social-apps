.class public abstract Lcom/twitter/android/people/o;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method static a(Lcom/twitter/android/people/n;)Laem$a;
    .locals 2

    .prologue
    .line 45
    new-instance v0, Laem$a$a;

    invoke-direct {v0}, Laem$a$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/people/n;->a:Ljava/util/Map;

    .line 46
    invoke-virtual {v0, v1}, Laem$a$a;->a(Ljava/util/Map;)Laem$a$a;

    move-result-object v0

    const-string/jumbo v1, "connect"

    .line 47
    invoke-virtual {v0, v1}, Laem$a$a;->a(Ljava/lang/String;)Laem$a$a;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Laem$a$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laem$a;

    .line 45
    return-object v0
.end method

.method static a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lbsv;
    .locals 4

    .prologue
    .line 63
    new-instance v0, Lbsv;

    new-instance v1, Lcom/twitter/util/a;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, p0, v2, v3}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    invoke-direct {v0, v1}, Lbsv;-><init>(Lcom/twitter/util/a;)V

    return-object v0
.end method

.method static a(Lcom/twitter/library/client/Session;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/people/ai;
    .locals 4

    .prologue
    .line 96
    new-instance v0, Lcom/twitter/android/people/ai;

    invoke-virtual {p0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3, p1}, Lcom/twitter/android/people/ai;-><init>(JLcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    return-object v0
.end method

.method static a(Lanr;)Lcom/twitter/android/people/n;
    .locals 1

    .prologue
    .line 38
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/n;

    return-object v0
.end method

.method static a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;)Lcom/twitter/app/users/c;
    .locals 1

    .prologue
    .line 105
    new-instance v0, Lcom/twitter/app/users/c;

    invoke-direct {v0, p3, p2, p1, p0}, Lcom/twitter/app/users/c;-><init>(Lcom/twitter/library/client/p;Lcom/twitter/library/client/Session;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    return-object v0
.end method

.method static a()Lcom/twitter/model/util/FriendshipCache;
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/model/util/FriendshipCache;-><init>()V

    return-object v0
.end method

.method static b(Landroid/content/Context;Lcom/twitter/library/client/Session;)Laet;
    .locals 3

    .prologue
    .line 71
    new-instance v0, Laet;

    new-instance v1, Lauh;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, v2}, Lauh;-><init>(Landroid/content/ContentResolver;)V

    invoke-direct {v0, v1, p1}, Laet;-><init>(Lauj;Lcom/twitter/library/client/Session;)V

    return-object v0
.end method

.method static b(Lcom/twitter/android/people/n;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 2

    .prologue
    .line 86
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const-string/jumbo v1, "people"

    .line 87
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v1, p0, Lcom/twitter/android/people/n;->a:Ljava/util/Map;

    .line 88
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "pivot"

    :goto_0
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 86
    return-object v0

    .line 88
    :cond_0
    const-string/jumbo v1, "browse"

    goto :goto_0
.end method

.method static c(Landroid/content/Context;Lcom/twitter/library/client/Session;)Laes;
    .locals 3

    .prologue
    .line 79
    new-instance v0, Laes;

    new-instance v1, Lauh;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, v2}, Lauh;-><init>(Landroid/content/ContentResolver;)V

    invoke-direct {v0, v1, p1}, Laes;-><init>(Lauj;Lcom/twitter/library/client/Session;)V

    return-object v0
.end method
