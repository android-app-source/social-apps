.class public Lcom/twitter/android/people/ai;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Z

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final e:J


# direct methods
.method public constructor <init>(JLcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/ai;->b:Ljava/util/Set;

    .line 53
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/ai;->c:Ljava/util/Set;

    .line 58
    iput-wide p1, p0, Lcom/twitter/android/people/ai;->e:J

    .line 59
    iput-object p3, p0, Lcom/twitter/android/people/ai;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 60
    return-void
.end method

.method private a(Ljava/util/Set;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Set",
            "<TT;>;TT;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/twitter/analytics/feature/model/ClientEventLog;"
        }
    .end annotation

    .prologue
    .line 232
    invoke-interface {p1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 233
    invoke-interface {p1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 234
    const-string/jumbo v0, "impression"

    invoke-virtual {p0, p3, p4, v0}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 236
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;J)Lcom/twitter/android/people/ai;
    .locals 3

    .prologue
    .line 65
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    .line 66
    invoke-virtual {v0, p0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v1, "address_book"

    .line 67
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 68
    new-instance v1, Lcom/twitter/android/people/ai;

    invoke-direct {v1, p1, p2, v0}, Lcom/twitter/android/people/ai;-><init>(JLcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    return-object v1
.end method

.method private static a(Lcom/twitter/model/people/b;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 242
    invoke-interface {p0}, Lcom/twitter/model/people/b;->c()Lcom/twitter/model/people/f;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/people/f;->b:Ljava/lang/String;

    const-string/jumbo v1, "-"

    const-string/jumbo v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Lcom/twitter/android/people/adapters/b$d;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lcom/twitter/android/people/adapters/b$d",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 246
    iget-object v0, p1, Lcom/twitter/android/people/adapters/b$d;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static b(Lcom/twitter/model/people/b;Ljava/lang/Iterable;Ljava/lang/Object;Lcom/twitter/model/people/l;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/twitter/model/people/b;",
            "Ljava/lang/Iterable",
            "<TT;>;TT;",
            "Lcom/twitter/model/people/l;",
            ")",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;"
        }
    .end annotation

    .prologue
    .line 188
    new-instance v0, Lcom/twitter/android/people/ai$1;

    invoke-direct {v0, p2}, Lcom/twitter/android/people/ai$1;-><init>(Ljava/lang/Object;)V

    invoke-static {p1, v0}, Lcpt;->c(Ljava/lang/Iterable;Lcpv;)I

    move-result v0

    .line 194
    invoke-static {p3, p0, v0}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/model/people/l;Lcom/twitter/model/people/b;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 4

    .prologue
    .line 224
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/people/ai;->e:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/people/ai;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 225
    invoke-virtual {v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/people/ai;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object p1, v1, v2

    const/4 v2, 0x3

    aput-object p2, v1, v2

    const/4 v2, 0x4

    aput-object p3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/people/ai;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 226
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 224
    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 73
    iget-boolean v0, p0, Lcom/twitter/android/people/ai;->a:Z

    if-nez v0, :cond_0

    .line 74
    const-string/jumbo v0, "impression"

    invoke-virtual {p0, v1, v1, v0}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/people/ai;->a:Z

    .line 77
    :cond_0
    return-void
.end method

.method public a(II)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 211
    add-int/lit8 v0, p1, 0x1

    int-to-float v0, v0

    int-to-float v1, p2

    div-float/2addr v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    .line 212
    const-string/jumbo v1, "scroll"

    invoke-virtual {p0, v2, v2, v1}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    float-to-long v2, v0

    .line 213
    invoke-virtual {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 212
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 214
    return-void
.end method

.method public a(Lcom/twitter/android/people/adapters/b$a;)V
    .locals 3

    .prologue
    .line 163
    invoke-virtual {p1}, Lcom/twitter/android/people/adapters/b$a;->c()Lcom/twitter/model/people/b;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/people/ai;->a(Lcom/twitter/model/people/b;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const-string/jumbo v2, "click"

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 164
    invoke-virtual {p1}, Lcom/twitter/android/people/adapters/b$a;->c()Lcom/twitter/model/people/b;

    move-result-object v1

    iget-boolean v2, p1, Lcom/twitter/android/people/adapters/b$a;->a:Z

    invoke-static {v1, v2}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/model/people/b;Z)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 163
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 165
    return-void
.end method

.method public a(Lcom/twitter/android/people/adapters/b;)V
    .locals 5

    .prologue
    .line 81
    instance-of v0, p1, Lcom/twitter/android/people/adapters/viewbinders/p;

    if-eqz v0, :cond_0

    .line 82
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/adapters/viewbinders/p;

    invoke-interface {v0}, Lcom/twitter/android/people/adapters/viewbinders/p;->c()Lcom/twitter/model/people/b;

    move-result-object v0

    .line 83
    iget-object v1, p0, Lcom/twitter/android/people/ai;->b:Ljava/util/Set;

    invoke-interface {v0}, Lcom/twitter/model/people/b;->b()Ljava/lang/String;

    move-result-object v2

    .line 84
    invoke-static {v0}, Lcom/twitter/android/people/ai;->a(Lcom/twitter/model/people/b;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 83
    invoke-direct {p0, v1, v2, v3, v4}, Lcom/twitter/android/people/ai;->a(Ljava/util/Set;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    .line 86
    if-eqz v1, :cond_0

    .line 88
    invoke-static {v0}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/model/people/b;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 87
    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 91
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/android/people/adapters/viewbinders/p;)V
    .locals 3

    .prologue
    .line 170
    .line 171
    invoke-interface {p1}, Lcom/twitter/android/people/adapters/viewbinders/p;->c()Lcom/twitter/model/people/b;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/people/ai;->a(Lcom/twitter/model/people/b;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "social_proof_avatar"

    const-string/jumbo v2, "click"

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 172
    invoke-interface {p1}, Lcom/twitter/android/people/adapters/viewbinders/p;->c()Lcom/twitter/model/people/b;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/model/people/b;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 171
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 173
    return-void
.end method

.method public a(Lcom/twitter/model/people/b;Lcom/twitter/android/people/adapters/b$d;Ljava/lang/Object;Lcom/twitter/model/people/l;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/twitter/model/people/b;",
            "Lcom/twitter/android/people/adapters/b$d",
            "<TT;>;TT;",
            "Lcom/twitter/model/people/l;",
            ")V"
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p2, Lcom/twitter/android/people/adapters/b$d;->a:Ljava/util/List;

    invoke-virtual {p0, p1, v0, p3, p4}, Lcom/twitter/android/people/ai;->a(Lcom/twitter/model/people/b;Ljava/lang/Iterable;Ljava/lang/Object;Lcom/twitter/model/people/l;)V

    .line 105
    return-void
.end method

.method public a(Lcom/twitter/model/people/b;Lcom/twitter/android/people/adapters/b$d;Ljava/lang/Object;Lcom/twitter/model/people/l;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/twitter/model/people/b;",
            "Lcom/twitter/android/people/adapters/b$d",
            "<TT;>;TT;",
            "Lcom/twitter/model/people/l;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 145
    if-eqz p5, :cond_1

    const-string/jumbo v0, "swipe_next"

    .line 146
    :goto_0
    invoke-static {p1}, Lcom/twitter/android/people/ai;->a(Lcom/twitter/model/people/b;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v3, v0}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    .line 148
    if-eqz p4, :cond_2

    .line 149
    iget-object v0, p2, Lcom/twitter/android/people/adapters/b$d;->a:Ljava/util/List;

    invoke-static {p1, v0, p3, p4}, Lcom/twitter/android/people/ai;->b(Lcom/twitter/model/people/b;Ljava/lang/Iterable;Ljava/lang/Object;Lcom/twitter/model/people/l;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 153
    :goto_1
    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 155
    if-eqz p5, :cond_0

    invoke-static {p3, p2}, Lcom/twitter/android/people/ai;->a(Ljava/lang/Object;Lcom/twitter/android/people/adapters/b$d;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 156
    invoke-static {p1}, Lcom/twitter/android/people/ai;->a(Lcom/twitter/model/people/b;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "swipe_end"

    invoke-virtual {p0, v1, v3, v2}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    .line 157
    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 156
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 159
    :cond_0
    return-void

    .line 145
    :cond_1
    const-string/jumbo v0, "swipe_previous"

    goto :goto_0

    .line 151
    :cond_2
    invoke-static {p1}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/model/people/b;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    goto :goto_1
.end method

.method public a(Lcom/twitter/model/people/b;Ljava/lang/Iterable;Ljava/lang/Object;Lcom/twitter/model/people/l;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/twitter/model/people/b;",
            "Ljava/lang/Iterable",
            "<TT;>;TT;",
            "Lcom/twitter/model/people/l;",
            ")V"
        }
    .end annotation

    .prologue
    .line 119
    iget-object v0, p4, Lcom/twitter/model/people/l;->a:Lcom/twitter/model/core/TwitterUser;

    .line 120
    iget-object v1, p0, Lcom/twitter/android/people/ai;->c:Ljava/util/Set;

    .line 121
    invoke-interface {p1}, Lcom/twitter/model/people/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    invoke-static {p1}, Lcom/twitter/android/people/ai;->a(Lcom/twitter/model/people/b;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "user"

    .line 120
    invoke-direct {p0, v1, v0, v2, v3}, Lcom/twitter/android/people/ai;->a(Ljava/util/Set;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 123
    if-eqz v0, :cond_0

    .line 124
    invoke-static {p1, p2, p3, p4}, Lcom/twitter/android/people/ai;->b(Lcom/twitter/model/people/b;Ljava/lang/Iterable;Ljava/lang/Object;Lcom/twitter/model/people/l;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 126
    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 206
    const-string/jumbo v0, "scroll"

    invoke-virtual {p0, v1, v1, v0}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 207
    return-void
.end method

.method public b(Lcom/twitter/android/people/adapters/viewbinders/p;)V
    .locals 4

    .prologue
    .line 199
    invoke-interface {p1}, Lcom/twitter/android/people/adapters/viewbinders/p;->c()Lcom/twitter/model/people/b;

    move-result-object v0

    .line 200
    invoke-static {v0}, Lcom/twitter/android/people/ai;->a(Lcom/twitter/model/people/b;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "more"

    const-string/jumbo v3, "click"

    invoke-virtual {p0, v1, v2, v3}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    .line 201
    invoke-static {v0}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/model/people/b;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 200
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 202
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 218
    const-string/jumbo v0, "bottom"

    invoke-virtual {p0, v1, v1, v0}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 219
    return-void
.end method
