.class public final Lcom/twitter/android/people/x;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lcom/twitter/app/users/c;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/model/util/FriendshipCache;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/twitter/android/people/x;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/people/x;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcta;Lcta;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/model/util/FriendshipCache;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    sget-boolean v0, Lcom/twitter/android/people/x;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 33
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/people/x;->b:Lcta;

    .line 34
    sget-boolean v0, Lcom/twitter/android/people/x;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 35
    :cond_1
    iput-object p2, p0, Lcom/twitter/android/people/x;->c:Lcta;

    .line 36
    sget-boolean v0, Lcom/twitter/android/people/x;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 37
    :cond_2
    iput-object p3, p0, Lcom/twitter/android/people/x;->d:Lcta;

    .line 38
    sget-boolean v0, Lcom/twitter/android/people/x;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 39
    :cond_3
    iput-object p4, p0, Lcom/twitter/android/people/x;->e:Lcta;

    .line 40
    return-void
.end method

.method public static a(Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/model/util/FriendshipCache;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Lcom/twitter/app/users/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    new-instance v0, Lcom/twitter/android/people/x;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/twitter/android/people/x;-><init>(Lcta;Lcta;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/app/users/c;
    .locals 4

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/android/people/x;->b:Lcta;

    .line 46
    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v1, p0, Lcom/twitter/android/people/x;->c:Lcta;

    .line 47
    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/util/FriendshipCache;

    iget-object v2, p0, Lcom/twitter/android/people/x;->d:Lcta;

    .line 48
    invoke-interface {v2}, Lcta;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/Session;

    iget-object v3, p0, Lcom/twitter/android/people/x;->e:Lcta;

    .line 49
    invoke-interface {v3}, Lcta;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/client/p;

    .line 45
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/people/o;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;)Lcom/twitter/app/users/c;

    move-result-object v0

    const-string/jumbo v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 44
    invoke-static {v0, v1}, Ldagger/internal/e;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/c;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/android/people/x;->a()Lcom/twitter/app/users/c;

    move-result-object v0

    return-object v0
.end method
