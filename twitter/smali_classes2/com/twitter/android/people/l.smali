.class public Lcom/twitter/android/people/l;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/people/k;


# instance fields
.field protected final a:Lcom/twitter/android/people/i;

.field private final b:Lbsv;

.field private final c:Lcom/twitter/model/util/FriendshipCache;

.field private d:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/android/people/adapters/b;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/twitter/android/people/k$a;

.field private f:Lrx/j;

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Lbsv;Lcom/twitter/android/people/i;Lcom/twitter/model/util/FriendshipCache;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/l;->d:Ljava/lang/Iterable;

    .line 37
    iput-object p1, p0, Lcom/twitter/android/people/l;->b:Lbsv;

    .line 38
    iput-object p2, p0, Lcom/twitter/android/people/l;->a:Lcom/twitter/android/people/i;

    .line 39
    iput-object p3, p0, Lcom/twitter/android/people/l;->c:Lcom/twitter/model/util/FriendshipCache;

    .line 40
    invoke-direct {p0}, Lcom/twitter/android/people/l;->e()V

    .line 41
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/people/l;)Lcom/twitter/android/people/k$a;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/twitter/android/people/l;->e:Lcom/twitter/android/people/k$a;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/people/l;Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Lcom/twitter/android/people/l;->d:Ljava/lang/Iterable;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/people/l;Z)Z
    .locals 0

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/twitter/android/people/l;->g:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/people/l;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/twitter/android/people/l;->f()V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/people/l;Z)Z
    .locals 0

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/twitter/android/people/l;->h:Z

    return p1
.end method

.method private e()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/android/people/l;->f:Lrx/j;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/twitter/android/people/l;->f:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 47
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/people/l;->g:Z

    .line 49
    iget-object v0, p0, Lcom/twitter/android/people/l;->a:Lcom/twitter/android/people/i;

    .line 50
    invoke-virtual {v0}, Lcom/twitter/android/people/i;->a()Lrx/c;

    move-result-object v0

    .line 51
    invoke-virtual {p0}, Lcom/twitter/android/people/l;->c()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/people/l$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/people/l$1;-><init>(Lcom/twitter/android/people/l;)V

    .line 52
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/l;->f:Lrx/j;

    .line 69
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, Lcom/twitter/android/people/l;->e:Lcom/twitter/android/people/k$a;

    if-eqz v0, :cond_0

    .line 89
    iget-object v1, p0, Lcom/twitter/android/people/l;->e:Lcom/twitter/android/people/k$a;

    iget-boolean v0, p0, Lcom/twitter/android/people/l;->g:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v0}, Lcom/twitter/android/people/k$a;->a(Lcbi;)V

    .line 91
    :cond_0
    return-void

    .line 89
    :cond_1
    new-instance v0, Lcbl;

    iget-object v2, p0, Lcom/twitter/android/people/l;->d:Ljava/lang/Iterable;

    invoke-direct {v0, v2}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/twitter/android/people/l;->e()V

    .line 74
    return-void
.end method

.method public a(Lcom/twitter/android/people/k$a;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/twitter/android/people/l;->e:Lcom/twitter/android/people/k$a;

    .line 84
    invoke-direct {p0}, Lcom/twitter/android/people/l;->f()V

    .line 85
    return-void
.end method

.method public a(Lcom/twitter/app/common/di/scope/InjectionScope;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 95
    sget-object v0, Lcom/twitter/app/common/di/scope/InjectionScope;->c:Lcom/twitter/app/common/di/scope/InjectionScope;

    if-ne p1, v0, :cond_2

    .line 96
    iget-object v0, p0, Lcom/twitter/android/people/l;->f:Lrx/j;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/twitter/android/people/l;->f:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 98
    iput-object v1, p0, Lcom/twitter/android/people/l;->f:Lrx/j;

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/people/l;->a:Lcom/twitter/android/people/i;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 101
    iget-object v0, p0, Lcom/twitter/android/people/l;->b:Lbsv;

    invoke-virtual {v0}, Lbsv;->a()V

    .line 105
    :cond_1
    :goto_0
    return-void

    .line 102
    :cond_2
    sget-object v0, Lcom/twitter/app/common/di/scope/InjectionScope;->e:Lcom/twitter/app/common/di/scope/InjectionScope;

    if-ne p1, v0, :cond_1

    .line 103
    iput-object v1, p0, Lcom/twitter/android/people/l;->e:Lcom/twitter/android/people/k$a;

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/twitter/android/people/l;->h:Z

    return v0
.end method

.method c()Lrx/f;
    .locals 1

    .prologue
    .line 78
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/twitter/android/people/l;->b:Lbsv;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbsv;->a(Z)V

    .line 116
    iget-object v0, p0, Lcom/twitter/android/people/l;->a:Lcom/twitter/android/people/i;

    invoke-virtual {v0}, Lcom/twitter/android/people/i;->b()V

    .line 117
    return-void
.end method
