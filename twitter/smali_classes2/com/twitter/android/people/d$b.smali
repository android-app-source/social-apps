.class final Lcom/twitter/android/people/d$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/people/ab;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/people/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/people/d;

.field private final b:Lant;

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laod;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/list/l$d;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/m;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/e;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/r;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lank;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/d",
            "<",
            "Lcom/twitter/model/people/l;",
            "Lcom/twitter/android/people/adapters/viewbinders/v$a;",
            "Lcom/twitter/android/people/adapters/b$m;",
            "Lcom/twitter/android/people/adapters/viewbinders/v$b;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/v;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/j;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/d",
            "<",
            "Lcom/twitter/model/people/h;",
            "Lcom/twitter/android/people/adapters/a;",
            "Lcom/twitter/android/people/adapters/b$f;",
            "Lcom/twitter/android/people/adapters/viewbinders/g$b;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private o:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/g;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/a;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/k;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/d",
            "<",
            "Lcom/twitter/model/core/r;",
            "Lcom/twitter/android/people/adapters/f;",
            "Lcom/twitter/android/people/adapters/b$l;",
            "Ljava/lang/Object;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private s:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/t;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/i;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/z;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/twitter/android/people/d;Lant;)V
    .locals 1

    .prologue
    .line 423
    iput-object p1, p0, Lcom/twitter/android/people/d$b;->a:Lcom/twitter/android/people/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 424
    invoke-static {p2}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lant;

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->b:Lant;

    .line 425
    invoke-direct {p0}, Lcom/twitter/android/people/d$b;->c()V

    .line 426
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/people/d;Lant;Lcom/twitter/android/people/d$1;)V
    .locals 0

    .prologue
    .line 365
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/people/d$b;-><init>(Lcom/twitter/android/people/d;Lant;)V

    return-void
.end method

.method private c()V
    .locals 15

    .prologue
    .line 431
    iget-object v0, p0, Lcom/twitter/android/people/d$b;->b:Lant;

    .line 433
    invoke-static {v0}, Lanu;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 432
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->c:Lcta;

    .line 435
    iget-object v0, p0, Lcom/twitter/android/people/d$b;->c:Lcta;

    .line 437
    invoke-static {v0}, Lanx;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 436
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->d:Lcta;

    .line 439
    iget-object v0, p0, Lcom/twitter/android/people/d$b;->b:Lant;

    .line 441
    invoke-static {v0}, Laoc;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 440
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->e:Lcta;

    .line 443
    iget-object v0, p0, Lcom/twitter/android/people/d$b;->e:Lcta;

    .line 445
    invoke-static {v0}, Lcom/twitter/android/people/ae;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 444
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->f:Lcta;

    .line 450
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/people/d$b;->c:Lcta;

    iget-object v2, p0, Lcom/twitter/android/people/d$b;->a:Lcom/twitter/android/people/d;

    .line 452
    invoke-static {v2}, Lcom/twitter/android/people/d;->a(Lcom/twitter/android/people/d;)Lcta;

    move-result-object v2

    .line 449
    invoke-static {v0, v1, v2}, Lcom/twitter/android/people/adapters/viewbinders/n;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 448
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->g:Lcta;

    .line 456
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/people/adapters/viewbinders/f;->a(Lcsd;)Ldagger/internal/c;

    move-result-object v0

    .line 455
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->h:Lcta;

    .line 461
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/people/d$b;->a:Lcom/twitter/android/people/d;

    .line 462
    invoke-static {v1}, Lcom/twitter/android/people/d;->b(Lcom/twitter/android/people/d;)Lcta;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/people/d$b;->a:Lcom/twitter/android/people/d;

    .line 463
    invoke-static {v2}, Lcom/twitter/android/people/d;->c(Lcom/twitter/android/people/d;)Lcta;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/people/d$b;->a:Lcom/twitter/android/people/d;

    .line 464
    invoke-static {v3}, Lcom/twitter/android/people/d;->a(Lcom/twitter/android/people/d;)Lcta;

    move-result-object v3

    .line 460
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/people/adapters/viewbinders/s;->a(Lcsd;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 459
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->i:Lcta;

    .line 466
    iget-object v0, p0, Lcom/twitter/android/people/d$b;->b:Lant;

    .line 468
    invoke-static {v0}, Laoa;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 467
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->j:Lcta;

    .line 470
    iget-object v0, p0, Lcom/twitter/android/people/d$b;->j:Lcta;

    .line 472
    invoke-static {v0}, Lcom/twitter/android/people/ag;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 471
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->k:Lcta;

    .line 478
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/people/d$b;->a:Lcom/twitter/android/people/d;

    .line 479
    invoke-static {v1}, Lcom/twitter/android/people/d;->b(Lcom/twitter/android/people/d;)Lcta;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/people/d$b;->a:Lcom/twitter/android/people/d;

    .line 480
    invoke-static {v2}, Lcom/twitter/android/people/d;->c(Lcom/twitter/android/people/d;)Lcta;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/people/d$b;->a:Lcom/twitter/android/people/d;

    .line 481
    invoke-static {v3}, Lcom/twitter/android/people/d;->a(Lcom/twitter/android/people/d;)Lcta;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/people/d$b;->k:Lcta;

    .line 477
    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/android/people/adapters/viewbinders/w;->a(Lcsd;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 476
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->l:Lcta;

    .line 484
    iget-object v0, p0, Lcom/twitter/android/people/d$b;->c:Lcta;

    .line 486
    invoke-static {v0}, Lany;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 485
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->m:Lcta;

    .line 489
    iget-object v0, p0, Lcom/twitter/android/people/d$b;->j:Lcta;

    .line 491
    invoke-static {v0}, Lcom/twitter/android/people/ad;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 490
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->n:Lcta;

    .line 497
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/people/d$b;->a:Lcom/twitter/android/people/d;

    .line 498
    invoke-static {v1}, Lcom/twitter/android/people/d;->d(Lcom/twitter/android/people/d;)Lcta;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/people/d$b;->a:Lcom/twitter/android/people/d;

    .line 499
    invoke-static {v2}, Lcom/twitter/android/people/d;->c(Lcom/twitter/android/people/d;)Lcta;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/people/d$b;->m:Lcta;

    iget-object v4, p0, Lcom/twitter/android/people/d$b;->a:Lcom/twitter/android/people/d;

    .line 501
    invoke-static {v4}, Lcom/twitter/android/people/d;->a(Lcom/twitter/android/people/d;)Lcta;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/people/d$b;->n:Lcta;

    .line 496
    invoke-static/range {v0 .. v5}, Lcom/twitter/android/people/adapters/viewbinders/h;->a(Lcsd;Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 495
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->o:Lcta;

    .line 507
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/people/d$b;->c:Lcta;

    iget-object v2, p0, Lcom/twitter/android/people/d$b;->a:Lcom/twitter/android/people/d;

    .line 511
    invoke-static {v2}, Lcom/twitter/android/people/d;->e(Lcom/twitter/android/people/d;)Lcta;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/people/d$b;->a:Lcom/twitter/android/people/d;

    .line 512
    invoke-static {v3}, Lcom/twitter/android/people/d;->a(Lcom/twitter/android/people/d;)Lcta;

    move-result-object v3

    .line 506
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/people/adapters/viewbinders/b;->a(Lcsd;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 505
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->p:Lcta;

    .line 517
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    .line 516
    invoke-static {v0}, Lcom/twitter/android/people/adapters/viewbinders/l;->a(Lcsd;)Ldagger/internal/c;

    move-result-object v0

    .line 515
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->q:Lcta;

    .line 519
    iget-object v0, p0, Lcom/twitter/android/people/d$b;->j:Lcta;

    .line 521
    invoke-static {v0}, Lcom/twitter/android/people/af;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 520
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->r:Lcta;

    .line 527
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/people/d$b;->a:Lcom/twitter/android/people/d;

    .line 528
    invoke-static {v1}, Lcom/twitter/android/people/d;->a(Lcom/twitter/android/people/d;)Lcta;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/people/d$b;->r:Lcta;

    .line 526
    invoke-static {v0, v1, v2}, Lcom/twitter/android/people/adapters/viewbinders/u;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 525
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->s:Lcta;

    .line 533
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/people/adapters/viewbinders/j;->a(Lcsd;)Ldagger/internal/c;

    move-result-object v0

    .line 532
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->t:Lcta;

    .line 538
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/people/d$b;->a:Lcom/twitter/android/people/d;

    .line 539
    invoke-static {v1}, Lcom/twitter/android/people/d;->d(Lcom/twitter/android/people/d;)Lcta;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/people/d$b;->d:Lcta;

    iget-object v3, p0, Lcom/twitter/android/people/d$b;->f:Lcta;

    iget-object v4, p0, Lcom/twitter/android/people/d$b;->a:Lcom/twitter/android/people/d;

    .line 542
    invoke-static {v4}, Lcom/twitter/android/people/d;->f(Lcom/twitter/android/people/d;)Lcta;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/people/d$b;->a:Lcom/twitter/android/people/d;

    .line 543
    invoke-static {v5}, Lcom/twitter/android/people/d;->a(Lcom/twitter/android/people/d;)Lcta;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/people/d$b;->g:Lcta;

    iget-object v7, p0, Lcom/twitter/android/people/d$b;->h:Lcta;

    iget-object v8, p0, Lcom/twitter/android/people/d$b;->i:Lcta;

    iget-object v9, p0, Lcom/twitter/android/people/d$b;->l:Lcta;

    iget-object v10, p0, Lcom/twitter/android/people/d$b;->o:Lcta;

    iget-object v11, p0, Lcom/twitter/android/people/d$b;->p:Lcta;

    iget-object v12, p0, Lcom/twitter/android/people/d$b;->q:Lcta;

    iget-object v13, p0, Lcom/twitter/android/people/d$b;->s:Lcta;

    iget-object v14, p0, Lcom/twitter/android/people/d$b;->t:Lcta;

    .line 537
    invoke-static/range {v0 .. v14}, Lcom/twitter/android/people/aa;->a(Lcsd;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 536
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->u:Lcta;

    .line 554
    iget-object v0, p0, Lcom/twitter/android/people/d$b;->u:Lcta;

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d$b;->v:Lcta;

    .line 555
    return-void
.end method


# virtual methods
.method public a()Laog;
    .locals 1

    .prologue
    .line 564
    iget-object v0, p0, Lcom/twitter/android/people/d$b;->v:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laog;

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 559
    iget-object v0, p0, Lcom/twitter/android/people/d$b;->a:Lcom/twitter/android/people/d;

    invoke-static {v0}, Lcom/twitter/android/people/d;->g(Lcom/twitter/android/people/d;)Lcta;

    move-result-object v0

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method
