.class public Lcom/twitter/android/people/PeopleDiscoveryListFragment;
.super Lcom/twitter/app/common/list/TwitterListFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/list/TwitterListFragment",
        "<",
        "Lcom/twitter/android/people/adapters/b;",
        "Lcjr",
        "<",
        "Lcom/twitter/android/people/adapters/b;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;-><init>()V

    return-void
.end method

.method private f()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/twitter/android/people/PeopleDiscoveryListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/ac;->b(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private k()Lcom/twitter/android/people/k;
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/twitter/android/people/PeopleDiscoveryListFragment;->ah()Lann;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/y;

    invoke-interface {v0}, Lcom/twitter/android/people/y;->d()Lcom/twitter/android/people/k;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a(Lank;)Lcom/twitter/android/people/y;
    .locals 4

    .prologue
    .line 24
    invoke-static {}, Lcom/twitter/android/people/d;->a()Lcom/twitter/android/people/d$a;

    move-result-object v0

    .line 25
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/people/d$a;->a(Lamu;)Lcom/twitter/android/people/d$a;

    move-result-object v0

    new-instance v1, Lano;

    new-instance v2, Lcom/twitter/android/people/n;

    .line 26
    invoke-direct {p0}, Lcom/twitter/android/people/PeopleDiscoveryListFragment;->f()Ljava/util/Map;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/twitter/android/people/n;-><init>(Ljava/util/Map;)V

    invoke-direct {v1, v2}, Lano;-><init>(Lanr;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/people/d$a;->a(Lano;)Lcom/twitter/android/people/d$a;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Lcom/twitter/android/people/d$a;->a()Lcom/twitter/android/people/y;

    move-result-object v0

    .line 23
    return-object v0
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 1

    .prologue
    .line 32
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 33
    const v0, 0x7f0a0636

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->a(I)Lcom/twitter/app/common/list/l$d;

    .line 34
    const v0, 0x7f0a0635

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->b(I)Lcom/twitter/app/common/list/l$d;

    .line 35
    const v0, 0x7f04005f

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->f(I)Lcom/twitter/app/common/list/l$d;

    .line 36
    return-void
.end method

.method protected synthetic b(Lank;)Lcom/twitter/app/common/list/j;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/twitter/android/people/PeopleDiscoveryListFragment;->a(Lank;)Lcom/twitter/android/people/y;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c(Lank;)Lcom/twitter/app/common/abs/c;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/twitter/android/people/PeopleDiscoveryListFragment;->a(Lank;)Lcom/twitter/android/people/y;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d(Lank;)Lann;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/twitter/android/people/PeopleDiscoveryListFragment;->a(Lank;)Lcom/twitter/android/people/y;

    move-result-object v0

    return-object v0
.end method

.method protected e(Lank;)Lcom/twitter/android/people/ab;
    .locals 4

    .prologue
    .line 41
    new-instance v2, Lcom/twitter/app/common/list/l$d;

    invoke-direct {v2}, Lcom/twitter/app/common/list/l$d;-><init>()V

    .line 42
    invoke-virtual {p0, v2}, Lcom/twitter/android/people/PeopleDiscoveryListFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 44
    invoke-virtual {p0}, Lcom/twitter/android/people/PeopleDiscoveryListFragment;->ah()Lann;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/y;

    .line 45
    new-instance v3, Lant;

    .line 46
    invoke-virtual {p0}, Lcom/twitter/android/people/PeopleDiscoveryListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-direct {v3, v1, p1, v2}, Lant;-><init>(Landroid/app/Activity;Lank;Laod;)V

    .line 45
    invoke-interface {v0, v3}, Lcom/twitter/android/people/y;->a(Lant;)Lcom/twitter/android/people/ab;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f(Lank;)Lcom/twitter/app/common/list/k;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/twitter/android/people/PeopleDiscoveryListFragment;->e(Lank;)Lcom/twitter/android/people/ab;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic g(Lank;)Lcom/twitter/app/common/abs/d;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/twitter/android/people/PeopleDiscoveryListFragment;->e(Lank;)Lcom/twitter/android/people/ab;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic h(Lank;)Lans;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/twitter/android/people/PeopleDiscoveryListFragment;->e(Lank;)Lcom/twitter/android/people/ab;

    move-result-object v0

    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->onStart()V

    .line 52
    invoke-direct {p0}, Lcom/twitter/android/people/PeopleDiscoveryListFragment;->k()Lcom/twitter/android/people/k;

    move-result-object v0

    .line 53
    invoke-interface {v0}, Lcom/twitter/android/people/k;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 54
    invoke-interface {v0}, Lcom/twitter/android/people/k;->a()V

    .line 56
    :cond_0
    return-void
.end method
