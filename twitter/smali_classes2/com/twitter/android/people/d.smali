.class public final Lcom/twitter/android/people/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/people/y;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/people/d$b;,
        Lcom/twitter/android/people/d$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private A:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/users/c;",
            ">;"
        }
    .end annotation
.end field

.field private B:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lbsv;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/model/util/FriendshipCache;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lanr;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/n;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laem$a;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laet;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laes;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lawb;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/people/j;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/a;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/ah;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laeq;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laem;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/d;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/i;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/l;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/k;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;>;"
        }
    .end annotation
.end field

.field private w:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private x:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field

.field private y:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            ">;"
        }
    .end annotation
.end field

.field private z:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/ai;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const-class v0, Lcom/twitter/android/people/d;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/people/d;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/twitter/android/people/d$a;)V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    sget-boolean v0, Lcom/twitter/android/people/d;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 139
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/android/people/d;->a(Lcom/twitter/android/people/d$a;)V

    .line 140
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/people/d$a;Lcom/twitter/android/people/d$1;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/twitter/android/people/d;-><init>(Lcom/twitter/android/people/d$a;)V

    return-void
.end method

.method public static a()Lcom/twitter/android/people/d$a;
    .locals 2

    .prologue
    .line 143
    new-instance v0, Lcom/twitter/android/people/d$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/people/d$a;-><init>(Lcom/twitter/android/people/d$1;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/people/d;)Lcta;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/people/d;->z:Lcta;

    return-object v0
.end method

.method private a(Lcom/twitter/android/people/d$a;)V
    .locals 6

    .prologue
    .line 149
    new-instance v0, Lcom/twitter/android/people/d$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/people/d$1;-><init>(Lcom/twitter/android/people/d;Lcom/twitter/android/people/d$a;)V

    iput-object v0, p0, Lcom/twitter/android/people/d;->b:Lcta;

    .line 162
    new-instance v0, Lcom/twitter/android/people/d$2;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/people/d$2;-><init>(Lcom/twitter/android/people/d;Lcom/twitter/android/people/d$a;)V

    iput-object v0, p0, Lcom/twitter/android/people/d;->c:Lcta;

    .line 175
    iget-object v0, p0, Lcom/twitter/android/people/d;->b:Lcta;

    iget-object v1, p0, Lcom/twitter/android/people/d;->c:Lcta;

    .line 177
    invoke-static {v0, v1}, Lcom/twitter/android/people/s;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 176
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->d:Lcta;

    .line 181
    invoke-static {}, Lcom/twitter/android/people/p;->c()Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->e:Lcta;

    .line 186
    invoke-static {p1}, Lcom/twitter/android/people/d$a;->b(Lcom/twitter/android/people/d$a;)Lano;

    move-result-object v0

    .line 185
    invoke-static {v0}, Lanp;->a(Lano;)Ldagger/internal/c;

    move-result-object v0

    .line 184
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->f:Lcta;

    .line 188
    iget-object v0, p0, Lcom/twitter/android/people/d;->f:Lcta;

    .line 189
    invoke-static {v0}, Lcom/twitter/android/people/r;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->g:Lcta;

    .line 192
    iget-object v0, p0, Lcom/twitter/android/people/d;->g:Lcta;

    .line 194
    invoke-static {v0}, Lcom/twitter/android/people/q;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 193
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->h:Lcta;

    .line 197
    iget-object v0, p0, Lcom/twitter/android/people/d;->b:Lcta;

    iget-object v1, p0, Lcom/twitter/android/people/d;->c:Lcta;

    .line 199
    invoke-static {v0, v1}, Lcom/twitter/android/people/w;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 198
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->i:Lcta;

    .line 202
    iget-object v0, p0, Lcom/twitter/android/people/d;->b:Lcta;

    iget-object v1, p0, Lcom/twitter/android/people/d;->c:Lcta;

    .line 204
    invoke-static {v0, v1}, Lcom/twitter/android/people/u;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 203
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->j:Lcta;

    .line 207
    new-instance v0, Lcom/twitter/android/people/d$3;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/people/d$3;-><init>(Lcom/twitter/android/people/d;Lcom/twitter/android/people/d$a;)V

    iput-object v0, p0, Lcom/twitter/android/people/d;->k:Lcta;

    .line 220
    iget-object v0, p0, Lcom/twitter/android/people/d;->k:Lcta;

    .line 222
    invoke-static {v0}, Lcom/twitter/android/people/h;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 221
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->l:Lcta;

    .line 225
    iget-object v0, p0, Lcom/twitter/android/people/d;->b:Lcta;

    .line 226
    invoke-static {v0}, Lcom/twitter/android/people/b;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->m:Lcta;

    .line 228
    iget-object v0, p0, Lcom/twitter/android/people/d;->m:Lcta;

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->n:Lcta;

    .line 233
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/people/d;->b:Lcta;

    iget-object v2, p0, Lcom/twitter/android/people/d;->c:Lcta;

    iget-object v3, p0, Lcom/twitter/android/people/d;->l:Lcta;

    iget-object v4, p0, Lcom/twitter/android/people/d;->n:Lcta;

    .line 232
    invoke-static {v0, v1, v2, v3, v4}, Laer;->a(Lcsd;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 231
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->o:Lcta;

    .line 239
    iget-object v0, p0, Lcom/twitter/android/people/d;->e:Lcta;

    iget-object v1, p0, Lcom/twitter/android/people/d;->h:Lcta;

    iget-object v2, p0, Lcom/twitter/android/people/d;->i:Lcta;

    iget-object v3, p0, Lcom/twitter/android/people/d;->j:Lcta;

    iget-object v4, p0, Lcom/twitter/android/people/d;->o:Lcta;

    iget-object v5, p0, Lcom/twitter/android/people/d;->l:Lcta;

    .line 241
    invoke-static/range {v0 .. v5}, Laen;->a(Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 240
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->p:Lcta;

    .line 249
    iget-object v0, p0, Lcom/twitter/android/people/d;->n:Lcta;

    .line 250
    invoke-static {v0}, Lcom/twitter/android/people/adapters/e;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->q:Lcta;

    .line 252
    iget-object v0, p0, Lcom/twitter/android/people/d;->d:Lcta;

    iget-object v1, p0, Lcom/twitter/android/people/d;->p:Lcta;

    iget-object v2, p0, Lcom/twitter/android/people/d;->q:Lcta;

    .line 254
    invoke-static {v0, v1, v2}, Lcom/twitter/android/people/j;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 253
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->r:Lcta;

    .line 259
    iget-object v0, p0, Lcom/twitter/android/people/d;->d:Lcta;

    iget-object v1, p0, Lcom/twitter/android/people/d;->r:Lcta;

    iget-object v2, p0, Lcom/twitter/android/people/d;->e:Lcta;

    .line 261
    invoke-static {v0, v1, v2}, Lcom/twitter/android/people/m;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 260
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->s:Lcta;

    .line 266
    iget-object v0, p0, Lcom/twitter/android/people/d;->s:Lcta;

    .line 267
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->t:Lcta;

    .line 269
    iget-object v0, p0, Lcom/twitter/android/people/d;->t:Lcta;

    .line 270
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->u:Lcta;

    .line 272
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 273
    invoke-static {v0, v1}, Ldagger/internal/f;->a(II)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/people/d;->u:Lcta;

    .line 274
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    .line 275
    invoke-virtual {v0}, Ldagger/internal/f$a;->a()Ldagger/internal/f;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->v:Lcta;

    .line 277
    new-instance v0, Lcom/twitter/android/people/d$4;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/people/d$4;-><init>(Lcom/twitter/android/people/d;Lcom/twitter/android/people/d$a;)V

    iput-object v0, p0, Lcom/twitter/android/people/d;->w:Lcta;

    .line 290
    iget-object v0, p0, Lcom/twitter/android/people/d;->w:Lcta;

    .line 292
    invoke-static {v0}, Lcom/twitter/app/common/abs/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 291
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->x:Lcta;

    .line 294
    iget-object v0, p0, Lcom/twitter/android/people/d;->g:Lcta;

    .line 296
    invoke-static {v0}, Lcom/twitter/android/people/v;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 295
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->y:Lcta;

    .line 299
    iget-object v0, p0, Lcom/twitter/android/people/d;->c:Lcta;

    iget-object v1, p0, Lcom/twitter/android/people/d;->y:Lcta;

    .line 301
    invoke-static {v0, v1}, Lcom/twitter/android/people/t;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 300
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->z:Lcta;

    .line 304
    iget-object v0, p0, Lcom/twitter/android/people/d;->y:Lcta;

    iget-object v1, p0, Lcom/twitter/android/people/d;->e:Lcta;

    iget-object v2, p0, Lcom/twitter/android/people/d;->c:Lcta;

    iget-object v3, p0, Lcom/twitter/android/people/d;->w:Lcta;

    .line 306
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/people/x;->a(Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 305
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->A:Lcta;

    .line 312
    iget-object v0, p0, Lcom/twitter/android/people/d;->s:Lcta;

    .line 313
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/d;->B:Lcta;

    .line 314
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/people/d;)Lcta;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/people/d;->e:Lcta;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/people/d;)Lcta;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/people/d;->A:Lcta;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/people/d;)Lcta;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/people/d;->b:Lcta;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/people/d;)Lcta;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/people/d;->B:Lcta;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/people/d;)Lcta;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/people/d;->t:Lcta;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/people/d;)Lcta;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/people/d;->v:Lcta;

    return-object v0
.end method


# virtual methods
.method public a(Lant;)Lcom/twitter/android/people/ab;
    .locals 2

    .prologue
    .line 333
    new-instance v0, Lcom/twitter/android/people/d$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/twitter/android/people/d$b;-><init>(Lcom/twitter/android/people/d;Lant;Lcom/twitter/android/people/d$1;)V

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 318
    iget-object v0, p0, Lcom/twitter/android/people/d;->v:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c()Lcom/twitter/app/common/abs/j;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/twitter/android/people/d;->x:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    return-object v0
.end method

.method public d()Lcom/twitter/android/people/k;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/twitter/android/people/d;->t:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/k;

    return-object v0
.end method
