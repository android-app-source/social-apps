.class public abstract Lcom/twitter/android/people/ac;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method static a(Laod;)Lcom/twitter/app/common/list/l$d;
    .locals 1

    .prologue
    .line 40
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/l$d;

    return-object v0
.end method

.method static a(Lank;)Lcom/twitter/app/common/util/StateSaver;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lank;",
            ")",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/d",
            "<",
            "Lcom/twitter/model/people/h;",
            "Lcom/twitter/android/people/adapters/a;",
            "Lcom/twitter/android/people/adapters/b$f;",
            "Lcom/twitter/android/people/adapters/viewbinders/g$b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 50
    const-string/jumbo v0, "STATE_FEATURED_CAROUSEL_VIEW_BINDER"

    invoke-static {p0, v0}, Lcom/twitter/android/people/ac;->a(Lank;Ljava/lang/String;)Lcom/twitter/app/common/util/StateSaver;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lank;Ljava/lang/String;)Lcom/twitter/app/common/util/StateSaver;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lank;",
            "Ljava/lang/String;",
            ")",
            "Lcom/twitter/app/common/util/StateSaver",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 75
    invoke-static {p0}, Lcom/twitter/android/people/ac;->d(Lank;)Lank;

    move-result-object v0

    invoke-virtual {v0, p1}, Lank;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 76
    invoke-static {}, Lcom/twitter/app/common/util/StateSaver;->a()Lcom/twitter/app/common/util/StateSaver;

    move-result-object v1

    .line 75
    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/util/StateSaver;

    return-object v0
.end method

.method static b(Lank;)Lcom/twitter/app/common/util/StateSaver;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lank;",
            ")",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/d",
            "<",
            "Lcom/twitter/model/people/l;",
            "Lcom/twitter/android/people/adapters/viewbinders/v$a;",
            "Lcom/twitter/android/people/adapters/b$m;",
            "Lcom/twitter/android/people/adapters/viewbinders/v$b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 60
    const-string/jumbo v0, "STATE_USER_CAROUSEL_VIEW_BINDER"

    invoke-static {p0, v0}, Lcom/twitter/android/people/ac;->a(Lank;Ljava/lang/String;)Lcom/twitter/app/common/util/StateSaver;

    move-result-object v0

    return-object v0
.end method

.method static c(Lank;)Lcom/twitter/app/common/util/StateSaver;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lank;",
            ")",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/d",
            "<",
            "Lcom/twitter/model/core/r;",
            "Lcom/twitter/android/people/adapters/f;",
            "Lcom/twitter/android/people/adapters/b$l;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 70
    const-string/jumbo v0, "STATE_TWEET_CAROUSEL_VIEW_BINDER"

    invoke-static {p0, v0}, Lcom/twitter/android/people/ac;->a(Lank;Ljava/lang/String;)Lcom/twitter/app/common/util/StateSaver;

    move-result-object v0

    return-object v0
.end method

.method private static d(Lank;)Lank;
    .locals 1

    .prologue
    .line 81
    const-string/jumbo v0, "ViewHost"

    invoke-virtual {p0, v0}, Lank;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    invoke-static {v0}, Lank;->a(Landroid/os/Bundle;)Lank;

    move-result-object v0

    return-object v0
.end method
