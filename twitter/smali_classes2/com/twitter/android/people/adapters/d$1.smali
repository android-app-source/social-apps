.class Lcom/twitter/android/people/adapters/d$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcpp;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/people/adapters/d;->a(Ljava/lang/Iterable;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcpp",
        "<",
        "Lcom/twitter/model/people/b;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/twitter/android/people/adapters/b;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/people/adapters/d;


# direct methods
.method constructor <init>(Lcom/twitter/android/people/adapters/d;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/twitter/android/people/adapters/d$1;->a:Lcom/twitter/android/people/adapters/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/people/b;)Ljava/lang/Iterable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/people/b;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/android/people/adapters/b;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/b;

    invoke-interface {v0}, Lcom/twitter/model/people/b;->c()Lcom/twitter/model/people/f;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/people/f;->b:Ljava/lang/String;

    .line 49
    iget-object v2, p0, Lcom/twitter/android/people/adapters/d$1;->a:Lcom/twitter/android/people/adapters/d;

    invoke-static {v2}, Lcom/twitter/android/people/adapters/d;->a(Lcom/twitter/android/people/adapters/d;)Lcom/twitter/android/people/ah;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/twitter/android/people/ah;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 50
    invoke-static {}, Lcom/twitter/android/people/adapters/d;->a()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/adapters/d$a;

    .line 51
    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/twitter/android/people/adapters/d$a;->a(Lcom/twitter/model/people/b;)Ljava/lang/Iterable;

    move-result-object v0

    .line 53
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 51
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 53
    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    check-cast p1, Lcom/twitter/model/people/b;

    invoke-virtual {p0, p1}, Lcom/twitter/android/people/adapters/d$1;->a(Lcom/twitter/model/people/b;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method
