.class public Lcom/twitter/android/people/adapters/c;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Lcom/twitter/android/people/adapters/b;

.field private static final b:Lcom/twitter/android/people/adapters/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/twitter/android/people/adapters/b$e;

    invoke-direct {v0}, Lcom/twitter/android/people/adapters/b$e;-><init>()V

    sput-object v0, Lcom/twitter/android/people/adapters/c;->a:Lcom/twitter/android/people/adapters/b;

    .line 24
    new-instance v0, Lcom/twitter/android/people/adapters/b$g;

    invoke-direct {v0}, Lcom/twitter/android/people/adapters/b$g;-><init>()V

    sput-object v0, Lcom/twitter/android/people/adapters/c;->b:Lcom/twitter/android/people/adapters/b;

    return-void
.end method

.method public static a()Lcom/twitter/android/people/adapters/b;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/twitter/android/people/adapters/c;->a:Lcom/twitter/android/people/adapters/b;

    return-object v0
.end method

.method public static a(Lcom/twitter/model/people/b;)Lcom/twitter/android/people/adapters/b;
    .locals 3

    .prologue
    .line 38
    invoke-interface {p0}, Lcom/twitter/model/people/b;->c()Lcom/twitter/model/people/f;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/people/f;->c:Lcom/twitter/model/people/g;

    .line 39
    new-instance v1, Lcom/twitter/android/people/adapters/b$i;

    iget-object v2, v0, Lcom/twitter/model/people/g;->a:Lcom/twitter/model/people/d;

    iget-object v0, v0, Lcom/twitter/model/people/g;->c:Lcom/twitter/model/people/k;

    invoke-direct {v1, p0, v2, v0}, Lcom/twitter/android/people/adapters/b$i;-><init>(Lcom/twitter/model/people/b;Lcom/twitter/model/people/d;Lcom/twitter/model/people/k;)V

    return-object v1
.end method

.method private static a(Lcom/twitter/model/people/b;Z)Lcpp;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/people/b;",
            "Z)",
            "Lcpp",
            "<",
            "Lcom/twitter/model/people/l;",
            "Lcom/twitter/android/people/adapters/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    new-instance v0, Lcom/twitter/android/people/adapters/c$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/people/adapters/c$1;-><init>(Lcom/twitter/model/people/b;Z)V

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .locals 1

    .prologue
    .line 22
    invoke-static {p0}, Lcom/twitter/android/people/adapters/c;->b(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public static b()Lcom/twitter/android/people/adapters/b;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/twitter/android/people/adapters/c;->b:Lcom/twitter/android/people/adapters/b;

    return-object v0
.end method

.method public static b(Lcom/twitter/model/people/b;)Lcom/twitter/android/people/adapters/b;
    .locals 2

    .prologue
    .line 44
    invoke-interface {p0}, Lcom/twitter/model/people/b;->c()Lcom/twitter/model/people/f;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/people/f;->c:Lcom/twitter/model/people/g;

    .line 45
    new-instance v1, Lcom/twitter/android/people/adapters/b$j;

    iget-object v0, v0, Lcom/twitter/model/people/g;->f:Lcom/twitter/model/people/i;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/people/adapters/b$j;-><init>(Lcom/twitter/model/people/b;Lcom/twitter/model/people/i;)V

    return-object v1
.end method

.method private static b(Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;)",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/core/r;",
            ">;"
        }
    .end annotation

    .prologue
    .line 134
    new-instance v0, Lcom/twitter/android/people/adapters/c$4;

    invoke-direct {v0}, Lcom/twitter/android/people/adapters/c$4;-><init>()V

    invoke-static {p0, v0}, Lcpt;->b(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lcom/twitter/model/people/b;)Lcom/twitter/android/people/adapters/b;
    .locals 2

    .prologue
    .line 50
    invoke-interface {p0}, Lcom/twitter/model/people/b;->c()Lcom/twitter/model/people/f;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/people/f;->c:Lcom/twitter/model/people/g;

    .line 51
    new-instance v1, Lcom/twitter/android/people/adapters/b$m;

    iget-object v0, v0, Lcom/twitter/model/people/g;->d:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/people/adapters/b$m;-><init>(Lcom/twitter/model/people/b;Ljava/lang/Iterable;)V

    return-object v1
.end method

.method public static d(Lcom/twitter/model/people/b;)Lcom/twitter/android/people/adapters/b;
    .locals 4

    .prologue
    .line 56
    invoke-interface {p0}, Lcom/twitter/model/people/b;->c()Lcom/twitter/model/people/f;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/people/f;->c:Lcom/twitter/model/people/g;

    .line 57
    new-instance v1, Lcom/twitter/android/people/adapters/b$f;

    iget-object v2, v0, Lcom/twitter/model/people/g;->h:Ljava/lang/Iterable;

    iget-object v0, v0, Lcom/twitter/model/people/g;->a:Lcom/twitter/model/people/d;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v0, v3}, Lcom/twitter/android/people/adapters/b$f;-><init>(Lcom/twitter/model/people/b;Ljava/lang/Iterable;Lcom/twitter/model/people/d;Ljava/lang/String;)V

    return-object v1
.end method

.method public static e(Lcom/twitter/model/people/b;)Lcom/twitter/android/people/adapters/b;
    .locals 3

    .prologue
    .line 62
    invoke-interface {p0}, Lcom/twitter/model/people/b;->c()Lcom/twitter/model/people/f;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/people/f;->c:Lcom/twitter/model/people/g;

    .line 63
    new-instance v1, Lcom/twitter/android/people/adapters/b$h;

    iget-object v2, v0, Lcom/twitter/model/people/g;->a:Lcom/twitter/model/people/d;

    iget-object v0, v0, Lcom/twitter/model/people/g;->c:Lcom/twitter/model/people/k;

    invoke-direct {v1, p0, v2, v0}, Lcom/twitter/android/people/adapters/b$h;-><init>(Lcom/twitter/model/people/b;Lcom/twitter/model/people/d;Lcom/twitter/model/people/k;)V

    return-object v1
.end method

.method public static f(Lcom/twitter/model/people/b;)Lcom/twitter/android/people/adapters/b;
    .locals 4

    .prologue
    .line 68
    invoke-interface {p0}, Lcom/twitter/model/people/b;->c()Lcom/twitter/model/people/f;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/people/f;->c:Lcom/twitter/model/people/g;

    .line 69
    new-instance v1, Lcom/twitter/android/people/adapters/b$a;

    iget-object v2, v0, Lcom/twitter/model/people/g;->a:Lcom/twitter/model/people/d;

    iget-object v0, v0, Lcom/twitter/model/people/g;->c:Lcom/twitter/model/people/k;

    .line 70
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/k;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v0, v3}, Lcom/twitter/android/people/adapters/b$a;-><init>(Lcom/twitter/model/people/b;Lcom/twitter/model/people/d;Lcom/twitter/model/people/k;Z)V

    .line 69
    return-object v1
.end method

.method public static g(Lcom/twitter/model/people/b;)Ljava/lang/Iterable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/people/b;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/android/people/adapters/b;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 75
    invoke-interface {p0}, Lcom/twitter/model/people/b;->c()Lcom/twitter/model/people/f;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/people/f;->c:Lcom/twitter/model/people/g;

    iget-object v0, v0, Lcom/twitter/model/people/g;->d:Ljava/util/List;

    .line 77
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Lcpt;->a(Ljava/lang/Iterable;I)Ljava/lang/Iterable;

    move-result-object v1

    .line 79
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 81
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Iterable;

    .line 82
    invoke-static {p0, v5}, Lcom/twitter/android/people/adapters/c;->a(Lcom/twitter/model/people/b;Z)Lcpp;

    move-result-object v3

    invoke-static {v1, v3}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v1

    aput-object v1, v2, v4

    .line 83
    invoke-static {p0, v4}, Lcom/twitter/android/people/adapters/c;->a(Lcom/twitter/model/people/b;Z)Lcpp;

    move-result-object v1

    invoke-static {v0, v1}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    aput-object v0, v2, v5

    .line 81
    invoke-static {v2}, Lcpt;->a([Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public static h(Lcom/twitter/model/people/b;)Ljava/lang/Iterable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/people/b;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/android/people/adapters/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    invoke-interface {p0}, Lcom/twitter/model/people/b;->c()Lcom/twitter/model/people/f;

    move-result-object v0

    iget-object v1, v0, Lcom/twitter/model/people/f;->c:Lcom/twitter/model/people/g;

    .line 104
    iget-object v0, v1, Lcom/twitter/model/people/g;->d:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    iget-object v0, v1, Lcom/twitter/model/people/g;->e:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->c()Ljava/util/Map;

    move-result-object v2

    .line 109
    iget-object v0, v1, Lcom/twitter/model/people/g;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/l;

    .line 110
    iget-object v4, v1, Lcom/twitter/model/people/g;->e:Ljava/util/List;

    new-instance v5, Lcom/twitter/android/people/adapters/c$2;

    invoke-direct {v5, v0}, Lcom/twitter/android/people/adapters/c$2;-><init>(Lcom/twitter/model/people/l;)V

    invoke-static {v4, v5}, Lcpt;->a(Ljava/lang/Iterable;Lcpv;)Ljava/lang/Iterable;

    move-result-object v4

    invoke-interface {v2, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 118
    :cond_0
    iget-object v0, v1, Lcom/twitter/model/people/g;->d:Ljava/util/List;

    new-instance v1, Lcom/twitter/android/people/adapters/c$3;

    invoke-direct {v1, v2, p0}, Lcom/twitter/android/people/adapters/c$3;-><init>(Ljava/util/Map;Lcom/twitter/model/people/b;)V

    invoke-static {v0, v1}, Lcpt;->b(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcpt;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method
