.class public abstract Lcom/twitter/android/people/adapters/b$d;
.super Lcom/twitter/android/people/adapters/b$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/people/adapters/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/twitter/android/people/adapters/b$b;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Lcom/twitter/model/people/b;Ljava/lang/Iterable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/people/b;",
            "Ljava/lang/Iterable",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 205
    invoke-direct {p0, p1}, Lcom/twitter/android/people/adapters/b$b;-><init>(Lcom/twitter/model/people/b;)V

    .line 206
    invoke-static {p2}, Lcpt;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/adapters/b$d;->a:Ljava/util/List;

    .line 207
    const-string/jumbo v0, "__"

    invoke-virtual {p0}, Lcom/twitter/android/people/adapters/b$d;->a()Lcpp;

    move-result-object v1

    invoke-static {p2, v1}, Lcpt;->b(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/adapters/b$d;->b:Ljava/lang/String;

    .line 208
    return-void
.end method


# virtual methods
.method protected a()Lcpp;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcpp",
            "<TT;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 212
    new-instance v0, Lcom/twitter/android/people/adapters/b$d$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/people/adapters/b$d$1;-><init>(Lcom/twitter/android/people/adapters/b$d;)V

    return-object v0
.end method
