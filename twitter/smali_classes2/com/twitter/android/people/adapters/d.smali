.class public Lcom/twitter/android/people/adapters/d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/people/adapters/d$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/people/adapters/d$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/twitter/android/people/ah;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 25
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "user-bio-list"

    sget-object v2, Lcom/twitter/android/people/adapters/d$a;->a:Lcom/twitter/android/people/adapters/d$a;

    .line 26
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "user-profile-carousel"

    sget-object v2, Lcom/twitter/android/people/adapters/d$a;->b:Lcom/twitter/android/people/adapters/d$a;

    .line 27
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "address-book-contacts"

    sget-object v2, Lcom/twitter/android/people/adapters/d$a;->b:Lcom/twitter/android/people/adapters/d$a;

    .line 28
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "featured-users-carousel"

    sget-object v2, Lcom/twitter/android/people/adapters/d$a;->c:Lcom/twitter/android/people/adapters/d$a;

    .line 29
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "prompt"

    sget-object v2, Lcom/twitter/android/people/adapters/d$a;->d:Lcom/twitter/android/people/adapters/d$a;

    .line 30
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "address-book-prompt"

    sget-object v2, Lcom/twitter/android/people/adapters/d$a;->e:Lcom/twitter/android/people/adapters/d$a;

    .line 31
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "user-tweet-carousel"

    sget-object v2, Lcom/twitter/android/people/adapters/d$a;->f:Lcom/twitter/android/people/adapters/d$a;

    .line 32
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    sput-object v0, Lcom/twitter/android/people/adapters/d;->a:Ljava/util/Map;

    .line 25
    return-void
.end method

.method public constructor <init>(Lcom/twitter/android/people/ah;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/twitter/android/people/adapters/d;->b:Lcom/twitter/android/people/ah;

    .line 40
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/people/adapters/d;)Lcom/twitter/android/people/ah;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/twitter/android/people/adapters/d;->b:Lcom/twitter/android/people/ah;

    return-object v0
.end method

.method static synthetic a()Ljava/util/Map;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/twitter/android/people/adapters/d;->a:Ljava/util/Map;

    return-object v0
.end method

.method private static b(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/android/people/adapters/b;",
            ">;>;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/people/adapters/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 62
    const/4 v0, 0x1

    .line 63
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 64
    if-eqz v1, :cond_0

    .line 65
    const/4 v1, 0x0

    .line 69
    :goto_1
    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 67
    :cond_0
    invoke-static {}, Lcom/twitter/android/people/adapters/c;->a()Lcom/twitter/android/people/adapters/b;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1

    .line 71
    :cond_1
    invoke-static {}, Lcom/twitter/android/people/adapters/c;->b()Lcom/twitter/android/people/adapters/b;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 72
    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/people/b;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/people/adapters/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    new-instance v0, Lcom/twitter/android/people/adapters/d$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/people/adapters/d$1;-><init>(Lcom/twitter/android/people/adapters/d;)V

    invoke-static {p1, v0}, Lcpt;->b(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/people/adapters/d;->b(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
