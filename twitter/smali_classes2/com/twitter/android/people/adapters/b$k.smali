.class public Lcom/twitter/android/people/adapters/b$k;
.super Lcom/twitter/android/people/adapters/b$b;
.source "Twttr"

# interfaces
.implements Lcom/twitter/model/core/j;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/people/adapters/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "k"
.end annotation


# instance fields
.field public final a:Lcom/twitter/model/people/l;

.field public final b:I

.field public final c:Z


# direct methods
.method public constructor <init>(Lcom/twitter/model/people/b;Lcom/twitter/model/people/l;IZ)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/twitter/android/people/adapters/b$b;-><init>(Lcom/twitter/model/people/b;)V

    .line 69
    iput-object p2, p0, Lcom/twitter/android/people/adapters/b$k;->a:Lcom/twitter/model/people/l;

    .line 70
    iput p3, p0, Lcom/twitter/android/people/adapters/b$k;->b:I

    .line 71
    iput-boolean p4, p0, Lcom/twitter/android/people/adapters/b$k;->c:Z

    .line 72
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/android/people/adapters/b$k;->a:Lcom/twitter/model/people/l;

    iget-object v0, v0, Lcom/twitter/model/people/l;->a:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/twitter/android/people/adapters/b$k;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
