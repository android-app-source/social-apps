.class final Lcom/twitter/android/people/adapters/c$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcpp;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/people/adapters/c;->h(Lcom/twitter/model/people/b;)Ljava/lang/Iterable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcpp",
        "<",
        "Lcom/twitter/model/people/l;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/twitter/android/people/adapters/b;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/util/Map;

.field final synthetic b:Lcom/twitter/model/people/b;


# direct methods
.method constructor <init>(Ljava/util/Map;Lcom/twitter/model/people/b;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/twitter/android/people/adapters/c$3;->a:Ljava/util/Map;

    iput-object p2, p0, Lcom/twitter/android/people/adapters/c$3;->b:Lcom/twitter/model/people/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/people/l;)Ljava/lang/Iterable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/people/l;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/android/people/adapters/b;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 123
    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    iget-object v0, p0, Lcom/twitter/android/people/adapters/c$3;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 125
    new-instance v1, Lcom/twitter/android/people/adapters/b$k;

    iget-object v2, p0, Lcom/twitter/android/people/adapters/c$3;->b:Lcom/twitter/model/people/b;

    invoke-direct {v1, v2, p1, v3, v5}, Lcom/twitter/android/people/adapters/b$k;-><init>(Lcom/twitter/model/people/b;Lcom/twitter/model/people/l;IZ)V

    new-array v2, v3, [Lcom/twitter/android/people/adapters/b;

    new-instance v3, Lcom/twitter/android/people/adapters/b$l;

    iget-object v4, p0, Lcom/twitter/android/people/adapters/c$3;->b:Lcom/twitter/model/people/b;

    .line 127
    invoke-static {v0}, Lcom/twitter/android/people/adapters/c;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lcom/twitter/android/people/adapters/b$l;-><init>(Lcom/twitter/model/people/b;Ljava/lang/Iterable;)V

    aput-object v3, v2, v5

    .line 125
    invoke-static {v1, v2}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 119
    check-cast p1, Lcom/twitter/model/people/l;

    invoke-virtual {p0, p1}, Lcom/twitter/android/people/adapters/c$3;->a(Lcom/twitter/model/people/l;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method
