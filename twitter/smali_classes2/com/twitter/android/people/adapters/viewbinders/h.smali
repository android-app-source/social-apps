.class public final Lcom/twitter/android/people/adapters/viewbinders/h;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lcom/twitter/android/people/adapters/viewbinders/g;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcsd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcsd",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/g;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/users/c;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/j;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/ai;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/d",
            "<",
            "Lcom/twitter/model/people/h;",
            "Lcom/twitter/android/people/adapters/a;",
            "Lcom/twitter/android/people/adapters/b$f;",
            "Lcom/twitter/android/people/adapters/viewbinders/g$b;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/twitter/android/people/adapters/viewbinders/h;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/people/adapters/viewbinders/h;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcsd;Lcta;Lcta;Lcta;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/g;",
            ">;",
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/users/c;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/j;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/ai;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/d",
            "<",
            "Lcom/twitter/model/people/h;",
            "Lcom/twitter/android/people/adapters/a;",
            "Lcom/twitter/android/people/adapters/b$f;",
            "Lcom/twitter/android/people/adapters/viewbinders/g$b;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    sget-boolean v0, Lcom/twitter/android/people/adapters/viewbinders/h;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 54
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/people/adapters/viewbinders/h;->b:Lcsd;

    .line 55
    sget-boolean v0, Lcom/twitter/android/people/adapters/viewbinders/h;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 56
    :cond_1
    iput-object p2, p0, Lcom/twitter/android/people/adapters/viewbinders/h;->c:Lcta;

    .line 57
    sget-boolean v0, Lcom/twitter/android/people/adapters/viewbinders/h;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 58
    :cond_2
    iput-object p3, p0, Lcom/twitter/android/people/adapters/viewbinders/h;->d:Lcta;

    .line 59
    sget-boolean v0, Lcom/twitter/android/people/adapters/viewbinders/h;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 60
    :cond_3
    iput-object p4, p0, Lcom/twitter/android/people/adapters/viewbinders/h;->e:Lcta;

    .line 61
    sget-boolean v0, Lcom/twitter/android/people/adapters/viewbinders/h;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 62
    :cond_4
    iput-object p5, p0, Lcom/twitter/android/people/adapters/viewbinders/h;->f:Lcta;

    .line 63
    sget-boolean v0, Lcom/twitter/android/people/adapters/viewbinders/h;->a:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 64
    :cond_5
    iput-object p6, p0, Lcom/twitter/android/people/adapters/viewbinders/h;->g:Lcta;

    .line 65
    return-void
.end method

.method public static a(Lcsd;Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/g;",
            ">;",
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/users/c;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/j;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/ai;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/d",
            "<",
            "Lcom/twitter/model/people/h;",
            "Lcom/twitter/android/people/adapters/a;",
            "Lcom/twitter/android/people/adapters/b$f;",
            "Lcom/twitter/android/people/adapters/viewbinders/g$b;",
            ">;>;>;)",
            "Ldagger/internal/c",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    new-instance v0, Lcom/twitter/android/people/adapters/viewbinders/h;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/people/adapters/viewbinders/h;-><init>(Lcsd;Lcta;Lcta;Lcta;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/android/people/adapters/viewbinders/g;
    .locals 7

    .prologue
    .line 69
    iget-object v6, p0, Lcom/twitter/android/people/adapters/viewbinders/h;->b:Lcsd;

    new-instance v0, Lcom/twitter/android/people/adapters/viewbinders/g;

    iget-object v1, p0, Lcom/twitter/android/people/adapters/viewbinders/h;->c:Lcta;

    .line 72
    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/people/adapters/viewbinders/h;->d:Lcta;

    .line 73
    invoke-interface {v2}, Lcta;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/app/users/c;

    iget-object v3, p0, Lcom/twitter/android/people/adapters/viewbinders/h;->e:Lcta;

    .line 74
    invoke-interface {v3}, Lcta;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/app/common/util/j;

    iget-object v4, p0, Lcom/twitter/android/people/adapters/viewbinders/h;->f:Lcta;

    .line 75
    invoke-interface {v4}, Lcta;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/people/ai;

    iget-object v5, p0, Lcom/twitter/android/people/adapters/viewbinders/h;->g:Lcta;

    .line 76
    invoke-interface {v5}, Lcta;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/app/common/util/StateSaver;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/people/adapters/viewbinders/g;-><init>(Landroid/content/Context;Lcom/twitter/app/users/c;Lcom/twitter/app/common/util/j;Lcom/twitter/android/people/ai;Lcom/twitter/app/common/util/StateSaver;)V

    .line 69
    invoke-static {v6, v0}, Ldagger/internal/MembersInjectors;->a(Lcsd;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/adapters/viewbinders/g;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/twitter/android/people/adapters/viewbinders/h;->a()Lcom/twitter/android/people/adapters/viewbinders/g;

    move-result-object v0

    return-object v0
.end method
