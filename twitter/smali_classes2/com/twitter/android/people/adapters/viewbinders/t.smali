.class public Lcom/twitter/android/people/adapters/viewbinders/t;
.super Lcom/twitter/android/people/adapters/viewbinders/d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/people/adapters/viewbinders/d",
        "<",
        "Lcom/twitter/model/core/r;",
        "Lcom/twitter/android/people/adapters/f;",
        "Lcom/twitter/android/people/adapters/b$l;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/twitter/android/people/ai;Lcom/twitter/app/common/util/StateSaver;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/people/ai;",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/d",
            "<",
            "Lcom/twitter/model/core/r;",
            "Lcom/twitter/android/people/adapters/f;",
            "Lcom/twitter/android/people/adapters/b$l;",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 29
    new-instance v0, Lcom/twitter/android/people/adapters/viewbinders/t$1;

    invoke-direct {v0}, Lcom/twitter/android/people/adapters/viewbinders/t$1;-><init>()V

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/android/people/adapters/viewbinders/d;-><init>(Lcom/twitter/android/people/ai;Lcom/twitter/util/object/j;Lcom/twitter/app/common/util/StateSaver;)V

    .line 36
    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/support/v4/view/ViewPager;Lcom/twitter/android/people/adapters/b$d;)V
    .locals 0

    .prologue
    .line 20
    check-cast p2, Lcom/twitter/android/people/adapters/b$l;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/t;->a(Landroid/support/v4/view/ViewPager;Lcom/twitter/android/people/adapters/b$l;)V

    return-void
.end method

.method protected a(Landroid/support/v4/view/ViewPager;Lcom/twitter/android/people/adapters/b$l;)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p2, Lcom/twitter/android/people/adapters/b$l;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 41
    return-void
.end method

.method public al_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    const-string/jumbo v0, "STATE_TWEET_CAROUSEL_VIEW_BINDER"

    return-object v0
.end method
