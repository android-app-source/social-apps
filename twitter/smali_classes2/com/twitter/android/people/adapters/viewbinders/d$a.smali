.class public Lcom/twitter/android/people/adapters/viewbinders/d$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/f$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/people/adapters/viewbinders/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/android/widget/f$a",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public a:Lcom/twitter/android/people/adapters/b$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TC;"
        }
    .end annotation
.end field

.field final synthetic b:Lcom/twitter/android/people/adapters/viewbinders/d;

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/people/adapters/viewbinders/d;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/twitter/android/people/adapters/viewbinders/d$a;->b:Lcom/twitter/android/people/adapters/viewbinders/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/people/adapters/viewbinders/d$a;->d:Z

    .line 214
    return-void
.end method

.method public a(Ljava/lang/Object;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)V"
        }
    .end annotation

    .prologue
    .line 194
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/d$a;->a:Lcom/twitter/android/people/adapters/b$d;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/d$a;->b:Lcom/twitter/android/people/adapters/viewbinders/d;

    iget-object v1, p0, Lcom/twitter/android/people/adapters/viewbinders/d$a;->a:Lcom/twitter/android/people/adapters/b$d;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/people/adapters/viewbinders/d;->a(Ljava/lang/Object;Lcom/twitter/android/people/adapters/b$d;)V

    .line 197
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Object;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation

    .prologue
    .line 201
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/d$a;->a:Lcom/twitter/android/people/adapters/b$d;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/people/adapters/viewbinders/d$a;->d:Z

    if-nez v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/d$a;->b:Lcom/twitter/android/people/adapters/viewbinders/d;

    iget-object v1, p0, Lcom/twitter/android/people/adapters/viewbinders/d$a;->a:Lcom/twitter/android/people/adapters/b$d;

    invoke-virtual {v0, p1, v1, p2}, Lcom/twitter/android/people/adapters/viewbinders/d;->a(Ljava/lang/Object;Lcom/twitter/android/people/adapters/b$d;Z)V

    .line 204
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/people/adapters/viewbinders/d$a;->d:Z

    .line 205
    return-void
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 209
    const/4 v0, 0x1

    return v0
.end method
