.class public Lcom/twitter/android/people/adapters/viewbinders/m;
.super Lckb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/people/adapters/viewbinders/m$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lckb",
        "<",
        "Lcom/twitter/android/people/adapters/b$i;",
        "Lcom/twitter/android/people/adapters/viewbinders/m$a;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/model/people/ModuleTitle$Icon;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/model/people/ModuleTitle$Icon;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/android/people/ai;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 46
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/people/ModuleTitle$Icon;->b:Lcom/twitter/model/people/ModuleTitle$Icon;

    const v2, 0x7f020836

    .line 47
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    sput-object v0, Lcom/twitter/android/people/adapters/viewbinders/m;->a:Ljava/util/Map;

    .line 49
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/people/ModuleTitle$Icon;->c:Lcom/twitter/model/people/ModuleTitle$Icon;

    const v2, 0x7f020684

    .line 50
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/people/ModuleTitle$Icon;->d:Lcom/twitter/model/people/ModuleTitle$Icon;

    const v2, 0x7f02068c

    .line 51
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    sput-object v0, Lcom/twitter/android/people/adapters/viewbinders/m;->b:Ljava/util/Map;

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/android/people/ai;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Lckb;-><init>()V

    .line 60
    iput-object p2, p0, Lcom/twitter/android/people/adapters/viewbinders/m;->d:Lcom/twitter/android/people/ai;

    .line 61
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/m;->c:Ljava/lang/ref/WeakReference;

    .line 62
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/people/adapters/viewbinders/m;)Lcom/twitter/android/people/ai;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/m;->d:Lcom/twitter/android/people/ai;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/people/adapters/viewbinders/m;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/m;->c:Ljava/lang/ref/WeakReference;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)Lcom/twitter/android/people/adapters/viewbinders/m$a;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 67
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 68
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 69
    const v2, 0x7f040141

    invoke-virtual {v1, v2, p1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 70
    new-instance v2, Lcom/twitter/android/people/adapters/viewbinders/m$a;

    invoke-direct {v2, v1}, Lcom/twitter/android/people/adapters/viewbinders/m$a;-><init>(Landroid/view/View;)V

    .line 71
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 73
    iget-object v1, v2, Lcom/twitter/android/people/adapters/viewbinders/m$a;->e:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, Landroid/support/v7/widget/LinearLayoutManager;

    .line 74
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v4

    invoke-direct {v3, v0, v5, v4}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 73
    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 75
    iget-object v1, v2, Lcom/twitter/android/people/adapters/viewbinders/m$a;->e:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, Lcom/twitter/android/people/adapters/viewbinders/q;

    iget-object v4, p0, Lcom/twitter/android/people/adapters/viewbinders/m;->d:Lcom/twitter/android/people/ai;

    invoke-direct {v3, v4}, Lcom/twitter/android/people/adapters/viewbinders/q;-><init>(Lcom/twitter/android/people/ai;)V

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 76
    iget-object v1, v2, Lcom/twitter/android/people/adapters/viewbinders/m$a;->e:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, Lcom/twitter/internal/android/widget/c;

    .line 77
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0e0306

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-direct {v3, v5, v0, v5, v5}, Lcom/twitter/internal/android/widget/c;-><init>(IIII)V

    .line 76
    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 78
    iget-object v0, v2, Lcom/twitter/android/people/adapters/viewbinders/m$a;->e:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 80
    return-object v2
.end method

.method public bridge synthetic a(Lckb$a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 41
    check-cast p1, Lcom/twitter/android/people/adapters/viewbinders/m$a;

    check-cast p2, Lcom/twitter/android/people/adapters/b$i;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/m;->a(Lcom/twitter/android/people/adapters/viewbinders/m$a;Lcom/twitter/android/people/adapters/b$i;)V

    return-void
.end method

.method public a(Lcom/twitter/android/people/adapters/viewbinders/m$a;Lcom/twitter/android/people/adapters/b$i;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x6

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 85
    iget-object v0, p2, Lcom/twitter/android/people/adapters/b$i;->a:Lcom/twitter/model/people/d;

    iget-object v4, v0, Lcom/twitter/model/people/d;->e:Lcom/twitter/model/people/k;

    .line 86
    iget-object v0, v4, Lcom/twitter/model/people/k;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v4, Lcom/twitter/model/people/k;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    .line 88
    :goto_0
    iget-object v0, p2, Lcom/twitter/android/people/adapters/b$i;->a:Lcom/twitter/model/people/d;

    iget-object v0, v0, Lcom/twitter/model/people/d;->c:Lcom/twitter/model/people/ModuleTitle;

    iget-object v3, p1, Lcom/twitter/android/people/adapters/viewbinders/m$a;->a:Landroid/widget/TextView;

    sget-object v5, Lcom/twitter/android/people/adapters/viewbinders/m;->a:Ljava/util/Map;

    invoke-virtual {p0, v0, v3, v5}, Lcom/twitter/android/people/adapters/viewbinders/m;->a(Lcom/twitter/model/people/ModuleTitle;Landroid/widget/TextView;Ljava/util/Map;)V

    .line 89
    iget-object v0, p2, Lcom/twitter/android/people/adapters/b$i;->a:Lcom/twitter/model/people/d;

    iget-object v0, v0, Lcom/twitter/model/people/d;->d:Lcom/twitter/model/people/ModuleTitle;

    iget-object v3, p1, Lcom/twitter/android/people/adapters/viewbinders/m$a;->b:Landroid/widget/TextView;

    sget-object v5, Lcom/twitter/android/people/adapters/viewbinders/m;->b:Ljava/util/Map;

    invoke-virtual {p0, v0, v3, v5}, Lcom/twitter/android/people/adapters/viewbinders/m;->a(Lcom/twitter/model/people/ModuleTitle;Landroid/widget/TextView;Ljava/util/Map;)V

    .line 91
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/m$a;->e:Landroid/support/v7/widget/RecyclerView;

    .line 92
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/adapters/viewbinders/q;

    .line 93
    iget-object v3, p2, Lcom/twitter/android/people/adapters/b$i;->a:Lcom/twitter/model/people/d;

    iget-object v3, v3, Lcom/twitter/model/people/d;->f:Lcom/twitter/model/people/c;

    iget-object v3, v3, Lcom/twitter/model/people/c;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 95
    iget-object v3, p2, Lcom/twitter/android/people/adapters/b$i;->a:Lcom/twitter/model/people/d;

    iget-object v3, v3, Lcom/twitter/model/people/d;->f:Lcom/twitter/model/people/c;

    iget-object v3, v3, Lcom/twitter/model/people/c;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v7, :cond_1

    .line 96
    iget-object v3, p2, Lcom/twitter/android/people/adapters/b$i;->a:Lcom/twitter/model/people/d;

    iget-object v3, v3, Lcom/twitter/model/people/d;->f:Lcom/twitter/model/people/c;

    iget-object v3, v3, Lcom/twitter/model/people/c;->c:Ljava/util/List;

    invoke-interface {v3, v2, v7}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    .line 100
    :goto_1
    invoke-virtual {v0, p2}, Lcom/twitter/android/people/adapters/viewbinders/q;->a(Lcom/twitter/android/people/adapters/viewbinders/p;)V

    .line 101
    invoke-virtual {v0, v3}, Lcom/twitter/android/people/adapters/viewbinders/q;->a(Ljava/util/List;)V

    .line 102
    invoke-virtual {v0}, Lcom/twitter/android/people/adapters/viewbinders/q;->notifyDataSetChanged()V

    .line 103
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/m$a;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 108
    :goto_2
    if-eqz v1, :cond_3

    .line 109
    invoke-virtual {p1}, Lcom/twitter/android/people/adapters/viewbinders/m$a;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 110
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    .line 111
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/m$a;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 112
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/m$a;->c:Landroid/widget/TextView;

    iget-object v1, v4, Lcom/twitter/model/people/k;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/m$a;->f:Landroid/view/View;

    new-instance v1, Lcom/twitter/android/people/adapters/viewbinders/m$1;

    move-object v2, p0

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/twitter/android/people/adapters/viewbinders/m$1;-><init>(Lcom/twitter/android/people/adapters/viewbinders/m;Lcom/twitter/android/people/adapters/b$i;Lcom/twitter/model/people/k;Landroid/content/Context;J)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/m$a;->f:Landroid/view/View;

    const v1, 0x7f0206cf

    .line 129
    invoke-static {v5, v1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 128
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 136
    :goto_3
    return-void

    :cond_0
    move v1, v2

    .line 86
    goto/16 :goto_0

    .line 98
    :cond_1
    iget-object v3, p2, Lcom/twitter/android/people/adapters/b$i;->a:Lcom/twitter/model/people/d;

    iget-object v3, v3, Lcom/twitter/model/people/d;->f:Lcom/twitter/model/people/c;

    iget-object v3, v3, Lcom/twitter/model/people/c;->c:Ljava/util/List;

    goto :goto_1

    .line 105
    :cond_2
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/m$a;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v8}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto :goto_2

    .line 131
    :cond_3
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/m$a;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 132
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/m$a;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/m$a;->f:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/m$a;->f:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3
.end method

.method a(Lcom/twitter/model/people/ModuleTitle;Landroid/widget/TextView;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/people/ModuleTitle;",
            "Landroid/widget/TextView;",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/model/people/ModuleTitle$Icon;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 140
    iget-object v0, p1, Lcom/twitter/model/people/ModuleTitle;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 141
    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 142
    iget-object v0, p1, Lcom/twitter/model/people/ModuleTitle;->c:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    :goto_0
    iget-object v0, p1, Lcom/twitter/model/people/ModuleTitle;->d:Lcom/twitter/model/people/ModuleTitle$Icon;

    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 150
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v1

    .line 157
    :goto_1
    invoke-virtual {p2, v2, v1, v0, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 158
    return-void

    .line 144
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v2, v0

    move v0, v1

    .line 155
    goto :goto_1
.end method

.method public a(Lcom/twitter/android/people/adapters/b$i;)Z
    .locals 1

    .prologue
    .line 162
    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 41
    check-cast p1, Lcom/twitter/android/people/adapters/b$i;

    invoke-virtual {p0, p1}, Lcom/twitter/android/people/adapters/viewbinders/m;->a(Lcom/twitter/android/people/adapters/b$i;)Z

    move-result v0

    return v0
.end method

.method public synthetic b(Landroid/view/ViewGroup;)Lckb$a;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/twitter/android/people/adapters/viewbinders/m;->a(Landroid/view/ViewGroup;)Lcom/twitter/android/people/adapters/viewbinders/m$a;

    move-result-object v0

    return-object v0
.end method
