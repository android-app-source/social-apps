.class public Lcom/twitter/android/people/adapters/viewbinders/v$a;
.super Lcom/twitter/android/timeline/e;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/people/adapters/viewbinders/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/timeline/e",
        "<",
        "Lcom/twitter/model/people/l;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Lcom/twitter/util/collection/ReferenceList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/ReferenceList",
            "<",
            "Lcom/twitter/ui/user/UserSocialView;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/twitter/android/people/adapters/b$m;


# direct methods
.method public constructor <init>(ILcom/twitter/model/util/FriendshipCache;Lcom/twitter/app/users/c;Lcom/twitter/util/collection/ReferenceList;)V
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/twitter/model/util/FriendshipCache;",
            "Lcom/twitter/app/users/c;",
            "Lcom/twitter/util/collection/ReferenceList",
            "<",
            "Lcom/twitter/ui/user/UserSocialView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 128
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/timeline/e;-><init>(ILcom/twitter/model/util/FriendshipCache;Lcom/twitter/app/users/c;)V

    .line 129
    iput-object p4, p0, Lcom/twitter/android/people/adapters/viewbinders/v$a;->b:Lcom/twitter/util/collection/ReferenceList;

    .line 130
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/twitter/model/people/l;I)Landroid/view/View;
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/v$a;->b:Lcom/twitter/util/collection/ReferenceList;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/user/UserSocialView;

    .line 135
    if-eqz v0, :cond_0

    .line 136
    iget-object v1, p0, Lcom/twitter/android/people/adapters/viewbinders/v$a;->b:Lcom/twitter/util/collection/ReferenceList;

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/ReferenceList;->c(Ljava/lang/Object;)Z

    .line 137
    invoke-virtual {p0, v0, p2, p3}, Lcom/twitter/android/people/adapters/viewbinders/v$a;->a(Landroid/view/View;Lcom/twitter/model/people/l;I)V

    .line 140
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/timeline/e;->a(Landroid/content/Context;Ljava/lang/Object;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 121
    check-cast p2, Lcom/twitter/model/people/l;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/people/adapters/viewbinders/v$a;->a(Landroid/content/Context;Lcom/twitter/model/people/l;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;Lcom/twitter/model/people/l;I)V
    .locals 6

    .prologue
    .line 145
    check-cast p1, Lcom/twitter/ui/user/UserSocialView;

    .line 146
    iget-object v0, p2, Lcom/twitter/model/people/l;->a:Lcom/twitter/model/core/TwitterUser;

    .line 147
    iget-object v1, p2, Lcom/twitter/model/people/l;->b:Ljava/lang/String;

    .line 149
    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserSocialView;->setUser(Lcom/twitter/model/core/TwitterUser;)V

    .line 150
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserSocialView;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 151
    iget-object v2, p0, Lcom/twitter/android/people/adapters/viewbinders/v$a;->a:Lcom/twitter/model/util/FriendshipCache;

    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v2, v4, v5}, Lcom/twitter/model/util/FriendshipCache;->a(J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 152
    iget-object v2, p0, Lcom/twitter/android/people/adapters/viewbinders/v$a;->a:Lcom/twitter/model/util/FriendshipCache;

    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v2, v4, v5}, Lcom/twitter/model/util/FriendshipCache;->k(J)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserSocialView;->setIsFollowing(Z)V

    .line 158
    :cond_0
    :goto_0
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/twitter/ui/user/UserSocialView;->a(Ljava/lang/String;Z)V

    .line 159
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/v$a;->c:Lcom/twitter/android/people/adapters/b$m;

    .line 160
    invoke-virtual {v0}, Lcom/twitter/android/people/adapters/b$m;->c()Lcom/twitter/model/people/b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/people/adapters/viewbinders/v$a;->c:Lcom/twitter/android/people/adapters/b$m;

    iget-object v1, v1, Lcom/twitter/android/people/adapters/b$m;->a:Ljava/util/List;

    .line 159
    invoke-static {v0, v1, p2, p2}, Lcom/twitter/android/people/ai;->b(Lcom/twitter/model/people/b;Ljava/lang/Iterable;Ljava/lang/Object;Lcom/twitter/model/people/l;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserSocialView;->setScribeItem(Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 161
    return-void

    .line 154
    :cond_1
    iget v2, v0, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-static {v2}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v2

    invoke-virtual {p1, v2}, Lcom/twitter/ui/user/UserSocialView;->setIsFollowing(Z)V

    .line 155
    iget-object v2, p0, Lcom/twitter/android/people/adapters/viewbinders/v$a;->a:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v2, v0}, Lcom/twitter/model/util/FriendshipCache;->a(Lcom/twitter/model/core/TwitterUser;)V

    goto :goto_0
.end method

.method public synthetic a(Landroid/view/View;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 121
    check-cast p2, Lcom/twitter/model/people/l;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/people/adapters/viewbinders/v$a;->b(Landroid/view/View;Lcom/twitter/model/people/l;I)V

    return-void
.end method

.method public a(Lcom/twitter/android/people/adapters/b$m;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/twitter/android/people/adapters/viewbinders/v$a;->c:Lcom/twitter/android/people/adapters/b$m;

    .line 170
    return-void
.end method

.method public b(Landroid/view/View;Lcom/twitter/model/people/l;I)V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/v$a;->b:Lcom/twitter/util/collection/ReferenceList;

    check-cast p1, Lcom/twitter/ui/user/UserSocialView;

    invoke-virtual {v0, p1}, Lcom/twitter/util/collection/ReferenceList;->b(Ljava/lang/Object;)V

    .line 166
    return-void
.end method

.method public synthetic b(Landroid/view/View;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 121
    check-cast p2, Lcom/twitter/model/people/l;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/people/adapters/viewbinders/v$a;->a(Landroid/view/View;Lcom/twitter/model/people/l;I)V

    return-void
.end method
