.class public Lcom/twitter/android/people/adapters/viewbinders/r;
.super Lckb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lckb",
        "<",
        "Lcom/twitter/android/people/adapters/b$k;",
        "Lckb$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/model/util/FriendshipCache;

.field private final b:Lcom/twitter/app/users/c;

.field private final c:Lcom/twitter/android/people/ai;


# direct methods
.method public constructor <init>(Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/app/users/c;Lcom/twitter/android/people/ai;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lckb;-><init>()V

    .line 44
    iput-object p2, p0, Lcom/twitter/android/people/adapters/viewbinders/r;->b:Lcom/twitter/app/users/c;

    .line 45
    iput-object p1, p0, Lcom/twitter/android/people/adapters/viewbinders/r;->a:Lcom/twitter/model/util/FriendshipCache;

    .line 46
    iput-object p3, p0, Lcom/twitter/android/people/adapters/viewbinders/r;->c:Lcom/twitter/android/people/ai;

    .line 47
    return-void
.end method

.method private static a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V
    .locals 9

    .prologue
    .line 116
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    .line 117
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    .line 118
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 119
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    .line 120
    new-instance v4, Landroid/graphics/drawable/LayerDrawable;

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/graphics/drawable/Drawable;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0206cf

    invoke-static {v7, v8}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-direct {v4, v5}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 122
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 123
    return-void
.end method

.method private a(Lcom/twitter/android/people/adapters/b$k;Lcom/twitter/ui/user/UserView;Lcom/twitter/model/people/l;)V
    .locals 3

    .prologue
    .line 109
    invoke-virtual {p1}, Lcom/twitter/android/people/adapters/b$k;->c()Lcom/twitter/model/people/b;

    move-result-object v0

    .line 110
    invoke-interface {v0}, Lcom/twitter/model/people/b;->c()Lcom/twitter/model/people/f;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/model/people/f;->c:Lcom/twitter/model/people/g;

    iget-object v1, v1, Lcom/twitter/model/people/g;->d:Ljava/util/List;

    .line 111
    iget-object v2, p0, Lcom/twitter/android/people/adapters/viewbinders/r;->c:Lcom/twitter/android/people/ai;

    invoke-virtual {v2, v0, v1, p3, p3}, Lcom/twitter/android/people/ai;->a(Lcom/twitter/model/people/b;Ljava/lang/Iterable;Ljava/lang/Object;Lcom/twitter/model/people/l;)V

    .line 112
    invoke-static {v0, v1, p3, p3}, Lcom/twitter/android/people/ai;->b(Lcom/twitter/model/people/b;Ljava/lang/Iterable;Ljava/lang/Object;Lcom/twitter/model/people/l;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/ui/user/UserView;->setScribeItem(Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 113
    return-void
.end method


# virtual methods
.method public a(Lckb$a;Lcom/twitter/android/people/adapters/b$k;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 59
    invoke-virtual {p1}, Lckb$a;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/user/UserSocialView;

    .line 60
    iget-object v1, p2, Lcom/twitter/android/people/adapters/b$k;->a:Lcom/twitter/model/people/l;

    .line 61
    iget-object v2, v1, Lcom/twitter/model/people/l;->a:Lcom/twitter/model/core/TwitterUser;

    .line 62
    invoke-virtual {v0}, Lcom/twitter/ui/user/UserSocialView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 63
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 64
    invoke-direct {p0, p2, v0, v1}, Lcom/twitter/android/people/adapters/viewbinders/r;->a(Lcom/twitter/android/people/adapters/b$k;Lcom/twitter/ui/user/UserView;Lcom/twitter/model/people/l;)V

    .line 65
    invoke-virtual {v0, v2}, Lcom/twitter/ui/user/UserSocialView;->setUser(Lcom/twitter/model/core/TwitterUser;)V

    .line 66
    iget-object v1, v1, Lcom/twitter/model/people/l;->b:Ljava/lang/String;

    const v5, 0x7f020835

    .line 67
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v6

    .line 66
    invoke-virtual {v0, v1, v5, v6}, Lcom/twitter/ui/user/UserSocialView;->a(Ljava/lang/String;IZ)V

    .line 68
    iget-object v1, v2, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    iget-object v5, v2, Lcom/twitter/model/core/TwitterUser;->C:Lcom/twitter/model/core/v;

    invoke-virtual {v0, v1, v5}, Lcom/twitter/ui/user/UserSocialView;->a(Ljava/lang/String;Lcom/twitter/model/core/v;)V

    .line 69
    iget-object v1, p0, Lcom/twitter/android/people/adapters/viewbinders/r;->b:Lcom/twitter/app/users/c;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    invoke-virtual {v0}, Lcom/twitter/ui/user/UserSocialView;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    const v1, 0x7f0200b1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setFollowBackgroundResource(I)V

    .line 72
    iget-object v1, p0, Lcom/twitter/android/people/adapters/viewbinders/r;->a:Lcom/twitter/model/util/FriendshipCache;

    iget-wide v6, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/util/FriendshipCache;->a(J)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 73
    iget-object v1, p0, Lcom/twitter/android/people/adapters/viewbinders/r;->a:Lcom/twitter/model/util/FriendshipCache;

    iget-wide v6, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/util/FriendshipCache;->k(J)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setIsFollowing(Z)V

    .line 78
    :goto_0
    const v1, 0x7f0200b0

    iget-object v5, p0, Lcom/twitter/android/people/adapters/viewbinders/r;->b:Lcom/twitter/app/users/c;

    invoke-virtual {v0, v1, v5}, Lcom/twitter/ui/user/UserSocialView;->a(ILcom/twitter/ui/user/BaseUserView$a;)V

    .line 81
    :cond_0
    iget v1, p2, Lcom/twitter/android/people/adapters/b$k;->b:I

    if-nez v1, :cond_4

    .line 82
    const v1, 0x7f0e0055

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setUserImageSize(I)V

    .line 84
    iget-boolean v1, p2, Lcom/twitter/android/people/adapters/b$k;->c:Z

    if-eqz v1, :cond_3

    .line 85
    const v1, 0x7f02009c

    invoke-static {v3, v1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 89
    :goto_1
    invoke-static {v0, v1}, Lcom/twitter/android/people/adapters/viewbinders/r;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 90
    invoke-virtual {v0, v8}, Lcom/twitter/ui/user/UserSocialView;->a(Z)V

    .line 91
    iget-object v1, v2, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    iget-object v2, v2, Lcom/twitter/model/core/TwitterUser;->C:Lcom/twitter/model/core/v;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/ui/user/UserSocialView;->a(Ljava/lang/String;Lcom/twitter/model/core/v;)V

    .line 101
    :cond_1
    :goto_2
    return-void

    .line 75
    :cond_2
    iget v1, v2, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-static {v1}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setIsFollowing(Z)V

    .line 76
    iget-object v1, p0, Lcom/twitter/android/people/adapters/viewbinders/r;->a:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v1, v2}, Lcom/twitter/model/util/FriendshipCache;->a(Lcom/twitter/model/core/TwitterUser;)V

    goto :goto_0

    .line 87
    :cond_3
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v4, 0x7f11004e

    invoke-static {v3, v4}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    invoke-direct {v1, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_1

    .line 92
    :cond_4
    iget v1, p2, Lcom/twitter/android/people/adapters/b$k;->b:I

    if-ne v8, v1, :cond_1

    .line 93
    const v1, 0x7f0e03a1

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 94
    const v2, 0x7f0e03a2

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    .line 95
    invoke-virtual {v0}, Lcom/twitter/ui/user/UserSocialView;->g()V

    .line 96
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v5, 0x7f110000

    .line 97
    invoke-static {v3, v5}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    invoke-direct {v4, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 96
    invoke-static {v0, v4}, Lcom/twitter/android/people/adapters/viewbinders/r;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 98
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/twitter/ui/user/UserSocialView;->a(Z)V

    .line 99
    invoke-virtual {v0}, Lcom/twitter/ui/user/UserSocialView;->getPaddingLeft()I

    move-result v3

    invoke-virtual {v0}, Lcom/twitter/ui/user/UserSocialView;->getPaddingRight()I

    move-result v4

    invoke-virtual {v0, v3, v1, v4, v2}, Lcom/twitter/ui/user/UserSocialView;->setPadding(IIII)V

    goto :goto_2
.end method

.method public bridge synthetic a(Lckb$a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 35
    check-cast p2, Lcom/twitter/android/people/adapters/b$k;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/r;->a(Lckb$a;Lcom/twitter/android/people/adapters/b$k;)V

    return-void
.end method

.method public a(Lcom/twitter/android/people/adapters/b$k;)Z
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 35
    check-cast p1, Lcom/twitter/android/people/adapters/b$k;

    invoke-virtual {p0, p1}, Lcom/twitter/android/people/adapters/viewbinders/r;->a(Lcom/twitter/android/people/adapters/b$k;)Z

    move-result v0

    return v0
.end method

.method public b(Landroid/view/ViewGroup;)Lckb$a;
    .locals 4

    .prologue
    .line 52
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 53
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 54
    new-instance v1, Lckb$a;

    const v2, 0x7f04042f

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {v1, v0}, Lckb$a;-><init>(Landroid/view/View;)V

    return-object v1
.end method
