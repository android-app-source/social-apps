.class public Lcom/twitter/android/people/adapters/viewbinders/a;
.super Lcom/twitter/android/people/adapters/viewbinders/c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/people/adapters/viewbinders/a$b;,
        Lcom/twitter/android/people/adapters/viewbinders/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/people/adapters/viewbinders/c",
        "<",
        "Lcom/twitter/android/people/adapters/b$a;",
        "Lcom/twitter/android/people/adapters/viewbinders/a$b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/android/people/adapters/viewbinders/a$a;

.field private final c:Lcom/twitter/android/people/ai;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/android/people/adapters/viewbinders/a$a;Lcom/twitter/android/people/ai;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/twitter/android/people/adapters/viewbinders/c;-><init>()V

    .line 36
    iput-object p2, p0, Lcom/twitter/android/people/adapters/viewbinders/a;->b:Lcom/twitter/android/people/adapters/viewbinders/a$a;

    .line 37
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/a;->a:Ljava/lang/ref/WeakReference;

    .line 38
    iput-object p3, p0, Lcom/twitter/android/people/adapters/viewbinders/a;->c:Lcom/twitter/android/people/ai;

    .line 39
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/people/adapters/viewbinders/a;)Lcom/twitter/android/people/adapters/viewbinders/a$a;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/a;->b:Lcom/twitter/android/people/adapters/viewbinders/a$a;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/people/adapters/viewbinders/a;Lcom/twitter/android/people/adapters/b$a;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/twitter/android/people/adapters/viewbinders/a;->b(Lcom/twitter/android/people/adapters/b$a;)V

    return-void
.end method

.method private b(Lcom/twitter/android/people/adapters/b$a;)V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 90
    if-eqz v0, :cond_0

    .line 91
    iget-object v1, p0, Lcom/twitter/android/people/adapters/viewbinders/a;->c:Lcom/twitter/android/people/ai;

    invoke-virtual {v1, p1}, Lcom/twitter/android/people/ai;->a(Lcom/twitter/android/people/adapters/b$a;)V

    .line 92
    new-instance v1, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;

    invoke-direct {v1}, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;-><init>()V

    .line 93
    invoke-virtual {v1, v0}, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 92
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 95
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)Lcom/twitter/android/people/adapters/viewbinders/a$b;
    .locals 3

    .prologue
    .line 44
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 45
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04002c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 46
    new-instance v1, Lcom/twitter/android/people/adapters/viewbinders/a$b;

    invoke-direct {v1, v0}, Lcom/twitter/android/people/adapters/viewbinders/a$b;-><init>(Landroid/view/View;)V

    .line 47
    iget-object v0, v1, Lcom/twitter/android/people/adapters/viewbinders/a$b;->b:Lcom/twitter/android/people/ui/AddressBookPromptView;

    iget-object v2, v1, Lcom/twitter/android/people/adapters/viewbinders/a$b;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/people/ui/AddressBookPromptView;->setEndView(Landroid/view/View;)V

    .line 48
    return-object v1
.end method

.method public bridge synthetic a(Lckb$a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lcom/twitter/android/people/adapters/viewbinders/a$b;

    check-cast p2, Lcom/twitter/android/people/adapters/b$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/a;->a(Lcom/twitter/android/people/adapters/viewbinders/a$b;Lcom/twitter/android/people/adapters/b$a;)V

    return-void
.end method

.method public a(Lcom/twitter/android/people/adapters/viewbinders/a$b;Lcom/twitter/android/people/adapters/b$a;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 53
    iget-boolean v0, p2, Lcom/twitter/android/people/adapters/b$a;->a:Z

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/a$b;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 55
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/a$b;->a:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/twitter/android/people/adapters/b$a;->b:Lcom/twitter/model/people/d;

    iget-object v1, v1, Lcom/twitter/model/people/d;->c:Lcom/twitter/model/people/ModuleTitle;

    iget-object v1, v1, Lcom/twitter/model/people/ModuleTitle;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/a$b;->b:Lcom/twitter/android/people/ui/AddressBookPromptView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/people/ui/AddressBookPromptView;->setVisibility(I)V

    .line 64
    :goto_0
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/a$b;->a:Landroid/widget/TextView;

    new-instance v1, Lcom/twitter/android/people/adapters/viewbinders/a$1;

    invoke-direct {v1, p0, p2}, Lcom/twitter/android/people/adapters/viewbinders/a$1;-><init>(Lcom/twitter/android/people/adapters/viewbinders/a;Lcom/twitter/android/people/adapters/b$a;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/a$b;->b:Lcom/twitter/android/people/ui/AddressBookPromptView;

    new-instance v1, Lcom/twitter/android/people/adapters/viewbinders/a$2;

    invoke-direct {v1, p0, p2}, Lcom/twitter/android/people/adapters/viewbinders/a$2;-><init>(Lcom/twitter/android/people/adapters/viewbinders/a;Lcom/twitter/android/people/adapters/b$a;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/people/ui/AddressBookPromptView;->setOnPromptClickListener(Lcom/twitter/ui/widget/PromptView$a;)V

    .line 81
    return-void

    .line 58
    :cond_0
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/a$b;->a:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 59
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/a$b;->b:Lcom/twitter/android/people/ui/AddressBookPromptView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/people/ui/AddressBookPromptView;->setVisibility(I)V

    .line 60
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/a$b;->b:Lcom/twitter/android/people/ui/AddressBookPromptView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/people/ui/AddressBookPromptView;->setDismissVisibility(I)V

    .line 61
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/a$b;->b:Lcom/twitter/android/people/ui/AddressBookPromptView;

    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/people/adapters/viewbinders/a;->a(Lcom/twitter/ui/widget/PromptView;Lcom/twitter/android/people/adapters/b$c;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/people/adapters/b$a;)Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p1, Lcom/twitter/android/people/adapters/b$a;->a:Z

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 26
    check-cast p1, Lcom/twitter/android/people/adapters/b$a;

    invoke-virtual {p0, p1}, Lcom/twitter/android/people/adapters/viewbinders/a;->a(Lcom/twitter/android/people/adapters/b$a;)Z

    move-result v0

    return v0
.end method

.method public synthetic b(Landroid/view/ViewGroup;)Lckb$a;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/twitter/android/people/adapters/viewbinders/a;->a(Landroid/view/ViewGroup;)Lcom/twitter/android/people/adapters/viewbinders/a$b;

    move-result-object v0

    return-object v0
.end method
