.class public Lcom/twitter/android/people/adapters/viewbinders/o;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/people/adapters/viewbinders/o$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/twitter/android/people/adapters/viewbinders/o$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/media/ui/image/config/g;

.field private final b:Z

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field private final d:I

.field private final e:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private final f:F


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 38
    sget-object v1, Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;->a:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    const/4 v3, -0x3

    const/4 v5, 0x0

    move-object v0, p0

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/people/adapters/viewbinders/o;-><init>(Lcom/twitter/media/ui/image/config/g;ZIIF)V

    .line 39
    return-void
.end method

.method public constructor <init>(Lcom/twitter/media/ui/image/config/g;ZIIF)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/twitter/android/people/adapters/viewbinders/o;->a:Lcom/twitter/media/ui/image/config/g;

    .line 51
    iput-boolean p2, p0, Lcom/twitter/android/people/adapters/viewbinders/o;->b:Z

    .line 52
    iput p3, p0, Lcom/twitter/android/people/adapters/viewbinders/o;->d:I

    .line 53
    iput p4, p0, Lcom/twitter/android/people/adapters/viewbinders/o;->e:I

    .line 54
    iput p5, p0, Lcom/twitter/android/people/adapters/viewbinders/o;->f:F

    .line 55
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Lcom/twitter/android/people/adapters/viewbinders/o$a;
    .locals 4

    .prologue
    .line 63
    new-instance v1, Lcom/twitter/android/people/adapters/viewbinders/o$a;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040140

    const/4 v3, 0x0

    .line 64
    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    invoke-direct {v1, v0}, Lcom/twitter/android/people/adapters/viewbinders/o$a;-><init>(Lcom/twitter/media/ui/image/UserImageView;)V

    .line 65
    iget-object v0, v1, Lcom/twitter/android/people/adapters/viewbinders/o$a;->a:Lcom/twitter/media/ui/image/UserImageView;

    iget-object v2, p0, Lcom/twitter/android/people/adapters/viewbinders/o;->a:Lcom/twitter/media/ui/image/config/g;

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/image/UserImageView;->setRoundingStrategy(Lcom/twitter/media/ui/image/config/g;)V

    .line 66
    return-object v1
.end method

.method protected a(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;)V
    .locals 4

    .prologue
    .line 91
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "user_id"

    iget-wide v2, p2, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 92
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 91
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 93
    return-void
.end method

.method public a(Lcom/twitter/android/people/adapters/viewbinders/o$a;I)V
    .locals 7

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/o;->c:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 72
    iget-object v1, p1, Lcom/twitter/android/people/adapters/viewbinders/o$a;->a:Lcom/twitter/media/ui/image/UserImageView;

    .line 73
    invoke-virtual {v1}, Lcom/twitter/media/ui/image/UserImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 75
    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;)Z

    .line 76
    const v3, 0x7f0a041c

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/twitter/media/ui/image/UserImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 77
    new-instance v3, Lcom/twitter/android/people/adapters/viewbinders/o$1;

    invoke-direct {v3, p0, v2, v0}, Lcom/twitter/android/people/adapters/viewbinders/o$1;-><init>(Lcom/twitter/android/people/adapters/viewbinders/o;Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;)V

    invoke-virtual {v1, v3}, Lcom/twitter/media/ui/image/UserImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget v0, p0, Lcom/twitter/android/people/adapters/viewbinders/o;->d:I

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->setSize(I)V

    .line 84
    iget-boolean v0, p0, Lcom/twitter/android/people/adapters/viewbinders/o;->b:Z

    if-eqz v0, :cond_0

    .line 85
    invoke-virtual {p1}, Lcom/twitter/android/people/adapters/viewbinders/o$a;->getAdapterPosition()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    invoke-static {v1, v0}, Landroid/support/v4/view/ViewCompat;->setZ(Landroid/view/View;F)V

    .line 87
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/media/ui/image/UserImageView;->getImageConfigurator()Lcom/twitter/media/ui/image/config/c;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/people/adapters/viewbinders/o;->e:I

    iget v2, p0, Lcom/twitter/android/people/adapters/viewbinders/o;->f:F

    invoke-interface {v0, v1, v2}, Lcom/twitter/media/ui/image/config/c;->a(IF)Lcom/twitter/media/ui/image/config/c;

    .line 88
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    iput-object p1, p0, Lcom/twitter/android/people/adapters/viewbinders/o;->c:Ljava/util/List;

    .line 59
    return-void
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/o;->c:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/util/Collection;)I

    move-result v0

    return v0
.end method

.method public synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Lcom/twitter/android/people/adapters/viewbinders/o$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/o;->a(Lcom/twitter/android/people/adapters/viewbinders/o$a;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/o;->a(Landroid/view/ViewGroup;I)Lcom/twitter/android/people/adapters/viewbinders/o$a;

    move-result-object v0

    return-object v0
.end method
