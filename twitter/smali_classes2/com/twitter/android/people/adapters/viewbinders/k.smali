.class public Lcom/twitter/android/people/adapters/viewbinders/k;
.super Lcom/twitter/android/people/adapters/viewbinders/c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/people/adapters/viewbinders/c",
        "<",
        "Lcom/twitter/android/people/adapters/b$h;",
        "Lckb$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/twitter/android/people/adapters/viewbinders/c;-><init>()V

    .line 22
    return-void
.end method


# virtual methods
.method public a(Lckb$a;Lcom/twitter/android/people/adapters/b$h;)V
    .locals 2

    .prologue
    .line 32
    invoke-virtual {p1}, Lckb$a;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/PromptView;

    .line 33
    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/people/adapters/viewbinders/k;->a(Lcom/twitter/ui/widget/PromptView;Lcom/twitter/android/people/adapters/b$c;)V

    .line 34
    new-instance v1, Lcom/twitter/android/people/adapters/viewbinders/k$1;

    invoke-direct {v1, p0, p2}, Lcom/twitter/android/people/adapters/viewbinders/k$1;-><init>(Lcom/twitter/android/people/adapters/viewbinders/k;Lcom/twitter/android/people/adapters/b$h;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/PromptView;->setOnPromptClickListener(Lcom/twitter/ui/widget/PromptView$a;)V

    .line 45
    return-void
.end method

.method public bridge synthetic a(Lckb$a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p2, Lcom/twitter/android/people/adapters/b$h;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/k;->a(Lckb$a;Lcom/twitter/android/people/adapters/b$h;)V

    return-void
.end method

.method public a(Lcom/twitter/android/people/adapters/b$h;)Z
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 17
    check-cast p1, Lcom/twitter/android/people/adapters/b$h;

    invoke-virtual {p0, p1}, Lcom/twitter/android/people/adapters/viewbinders/k;->a(Lcom/twitter/android/people/adapters/b$h;)Z

    move-result v0

    return v0
.end method

.method public b(Landroid/view/ViewGroup;)Lckb$a;
    .locals 3

    .prologue
    .line 27
    new-instance v0, Lckb$a;

    new-instance v1, Lcom/twitter/ui/widget/PromptView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/ui/widget/PromptView;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, Lckb$a;-><init>(Landroid/view/View;)V

    return-object v0
.end method
