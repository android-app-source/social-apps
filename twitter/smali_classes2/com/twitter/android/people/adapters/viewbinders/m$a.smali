.class Lcom/twitter/android/people/adapters/viewbinders/m$a;
.super Lckb$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/people/adapters/viewbinders/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field public final a:Landroid/widget/TextView;

.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/widget/ImageView;

.field public final e:Landroid/support/v7/widget/RecyclerView;

.field public final f:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 174
    invoke-direct {p0, p1}, Lckb$a;-><init>(Landroid/view/View;)V

    .line 175
    const v0, 0x7f1303d3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/m$a;->a:Landroid/widget/TextView;

    .line 176
    const v0, 0x7f1303d1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/m$a;->b:Landroid/widget/TextView;

    .line 177
    const v0, 0x7f1303d4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/m$a;->c:Landroid/widget/TextView;

    .line 178
    const v0, 0x7f1303d2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/m$a;->f:Landroid/view/View;

    .line 179
    const v0, 0x7f1300fa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/m$a;->d:Landroid/widget/ImageView;

    .line 180
    const v0, 0x7f130153

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/m$a;->e:Landroid/support/v7/widget/RecyclerView;

    .line 181
    return-void
.end method
