.class public Lcom/twitter/android/people/adapters/viewbinders/g;
.super Lcom/twitter/android/people/adapters/viewbinders/d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/people/adapters/viewbinders/g$b;,
        Lcom/twitter/android/people/adapters/viewbinders/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/people/adapters/viewbinders/d",
        "<",
        "Lcom/twitter/model/people/h;",
        "Lcom/twitter/android/people/adapters/a;",
        "Lcom/twitter/android/people/adapters/b$f;",
        "Lcom/twitter/android/people/adapters/viewbinders/g$b;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Landroid/os/Handler;

.field private final d:Lcom/twitter/app/common/util/j;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/app/users/c;Lcom/twitter/app/common/util/j;Lcom/twitter/android/people/ai;Lcom/twitter/app/common/util/StateSaver;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/app/users/c;",
            "Lcom/twitter/app/common/util/j;",
            "Lcom/twitter/android/people/ai;",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/d",
            "<",
            "Lcom/twitter/model/people/h;",
            "Lcom/twitter/android/people/adapters/a;",
            "Lcom/twitter/android/people/adapters/b$f;",
            "Lcom/twitter/android/people/adapters/viewbinders/g$b;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 60
    new-instance v0, Lcom/twitter/android/people/adapters/viewbinders/g$1;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/g$1;-><init>(Landroid/content/Context;Lcom/twitter/app/users/c;)V

    invoke-direct {p0, p4, v0, p5}, Lcom/twitter/android/people/adapters/viewbinders/d;-><init>(Lcom/twitter/android/people/ai;Lcom/twitter/util/object/j;Lcom/twitter/app/common/util/StateSaver;)V

    .line 50
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/g;->c:Landroid/os/Handler;

    .line 69
    iput-object p3, p0, Lcom/twitter/android/people/adapters/viewbinders/g;->d:Lcom/twitter/app/common/util/j;

    .line 70
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/people/adapters/viewbinders/g;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/twitter/android/people/adapters/viewbinders/g;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    .line 109
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/g;->c:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 110
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/g;->c:Landroid/os/Handler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, p1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 111
    return-void
.end method


# virtual methods
.method public synthetic a(Landroid/view/ViewGroup;)Lcom/twitter/android/people/adapters/viewbinders/d$b;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0, p1}, Lcom/twitter/android/people/adapters/viewbinders/g;->c(Landroid/view/ViewGroup;)Lcom/twitter/android/people/adapters/viewbinders/g$b;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/android/widget/CarouselRowView;Lcom/twitter/android/widget/e;)Lcom/twitter/android/people/adapters/viewbinders/d$b;
    .locals 1

    .prologue
    .line 42
    check-cast p2, Lcom/twitter/android/people/adapters/a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/g;->a(Lcom/twitter/android/widget/CarouselRowView;Lcom/twitter/android/people/adapters/a;)Lcom/twitter/android/people/adapters/viewbinders/g$b;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/android/widget/CarouselRowView;Lcom/twitter/android/people/adapters/a;)Lcom/twitter/android/people/adapters/viewbinders/g$b;
    .locals 1

    .prologue
    .line 92
    new-instance v0, Lcom/twitter/android/people/adapters/viewbinders/g$b;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/g$b;-><init>(Lcom/twitter/android/people/adapters/viewbinders/g;Lcom/twitter/android/widget/CarouselRowView;Lcom/twitter/android/people/adapters/a;)V

    return-object v0
.end method

.method protected a(Landroid/content/Context;Lcom/twitter/android/people/adapters/a;)Lcom/twitter/android/widget/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/android/people/adapters/a;",
            ")",
            "Lcom/twitter/android/widget/d",
            "<",
            "Lcom/twitter/model/people/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    new-instance v0, Lcom/twitter/android/widget/s;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/widget/s;-><init>(Landroid/content/Context;Lcom/twitter/android/widget/e;)V

    return-object v0
.end method

.method protected bridge synthetic a(Landroid/content/Context;Lcom/twitter/android/widget/e;)Lcom/twitter/android/widget/d;
    .locals 1

    .prologue
    .line 42
    check-cast p2, Lcom/twitter/android/people/adapters/a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/g;->a(Landroid/content/Context;Lcom/twitter/android/people/adapters/a;)Lcom/twitter/android/widget/d;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Landroid/support/v4/view/ViewPager;Lcom/twitter/android/people/adapters/b$d;)V
    .locals 0

    .prologue
    .line 42
    check-cast p2, Lcom/twitter/android/people/adapters/b$f;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/g;->a(Landroid/support/v4/view/ViewPager;Lcom/twitter/android/people/adapters/b$f;)V

    return-void
.end method

.method protected a(Landroid/support/v4/view/ViewPager;Lcom/twitter/android/people/adapters/b$f;)V
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 136
    return-void
.end method

.method public bridge synthetic a(Lckb$a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 42
    check-cast p1, Lcom/twitter/android/people/adapters/viewbinders/g$b;

    check-cast p2, Lcom/twitter/android/people/adapters/b$f;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/g;->a(Lcom/twitter/android/people/adapters/viewbinders/g$b;Lcom/twitter/android/people/adapters/b$f;)V

    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/android/people/adapters/viewbinders/d$b;Lcom/twitter/android/people/adapters/b$d;)V
    .locals 0

    .prologue
    .line 42
    check-cast p1, Lcom/twitter/android/people/adapters/viewbinders/g$b;

    check-cast p2, Lcom/twitter/android/people/adapters/b$f;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/g;->a(Lcom/twitter/android/people/adapters/viewbinders/g$b;Lcom/twitter/android/people/adapters/b$f;)V

    return-void
.end method

.method public a(Lcom/twitter/android/people/adapters/viewbinders/g$b;Lcom/twitter/android/people/adapters/b$f;)V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/d;->a(Lcom/twitter/android/people/adapters/viewbinders/d$b;Lcom/twitter/android/people/adapters/b$d;)V

    .line 105
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/g$b;->f:Ljava/lang/Runnable;

    invoke-direct {p0, v0}, Lcom/twitter/android/people/adapters/viewbinders/g;->a(Ljava/lang/Runnable;)V

    .line 106
    return-void
.end method

.method protected a(Lcom/twitter/model/people/h;Lcom/twitter/android/people/adapters/b$f;)V
    .locals 3

    .prologue
    .line 125
    iget-object v0, p1, Lcom/twitter/model/people/h;->d:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/l;

    .line 127
    if-eqz v0, :cond_0

    .line 128
    iget-object v1, p0, Lcom/twitter/android/people/adapters/viewbinders/g;->a:Lcom/twitter/android/people/ai;

    .line 129
    invoke-virtual {p2}, Lcom/twitter/android/people/adapters/b$f;->c()Lcom/twitter/model/people/b;

    move-result-object v2

    .line 128
    invoke-virtual {v1, v2, p2, p1, v0}, Lcom/twitter/android/people/ai;->a(Lcom/twitter/model/people/b;Lcom/twitter/android/people/adapters/b$d;Ljava/lang/Object;Lcom/twitter/model/people/l;)V

    .line 131
    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/model/people/h;Lcom/twitter/android/people/adapters/b$f;Z)V
    .locals 6

    .prologue
    .line 116
    iget-object v0, p1, Lcom/twitter/model/people/h;->d:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/people/l;

    .line 118
    if-eqz v4, :cond_0

    .line 119
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/g;->a:Lcom/twitter/android/people/ai;

    invoke-virtual {p2}, Lcom/twitter/android/people/adapters/b$f;->c()Lcom/twitter/model/people/b;

    move-result-object v1

    move-object v2, p2

    move-object v3, p1

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/people/ai;->a(Lcom/twitter/model/people/b;Lcom/twitter/android/people/adapters/b$d;Ljava/lang/Object;Lcom/twitter/model/people/l;Z)V

    .line 121
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Lcom/twitter/android/people/adapters/b$d;)V
    .locals 0

    .prologue
    .line 42
    check-cast p1, Lcom/twitter/model/people/h;

    check-cast p2, Lcom/twitter/android/people/adapters/b$f;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/g;->a(Lcom/twitter/model/people/h;Lcom/twitter/android/people/adapters/b$f;)V

    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Lcom/twitter/android/people/adapters/b$d;Z)V
    .locals 0

    .prologue
    .line 42
    check-cast p1, Lcom/twitter/model/people/h;

    check-cast p2, Lcom/twitter/android/people/adapters/b$f;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/people/adapters/viewbinders/g;->a(Lcom/twitter/model/people/h;Lcom/twitter/android/people/adapters/b$f;Z)V

    return-void
.end method

.method public al_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    const-string/jumbo v0, "STATE_FEATURED_CAROUSEL_VIEW_BINDER"

    return-object v0
.end method

.method protected b()I
    .locals 1

    .prologue
    .line 140
    const v0, 0x7f0400f3

    return v0
.end method

.method public synthetic b(Landroid/view/ViewGroup;)Lckb$a;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0, p1}, Lcom/twitter/android/people/adapters/viewbinders/g;->c(Landroid/view/ViewGroup;)Lcom/twitter/android/people/adapters/viewbinders/g$b;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/view/ViewGroup;)Lcom/twitter/android/people/adapters/viewbinders/g$b;
    .locals 4

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/twitter/android/people/adapters/viewbinders/d;->a(Landroid/view/ViewGroup;)Lcom/twitter/android/people/adapters/viewbinders/d$b;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/adapters/viewbinders/g$b;

    .line 77
    new-instance v1, Lcom/twitter/android/people/adapters/viewbinders/g$a;

    iget-object v2, p0, Lcom/twitter/android/people/adapters/viewbinders/g;->d:Lcom/twitter/app/common/util/j;

    invoke-direct {v1, v0, v2}, Lcom/twitter/android/people/adapters/viewbinders/g$a;-><init>(Lcom/twitter/android/people/adapters/viewbinders/g$b;Lcom/twitter/app/common/util/j;)V

    .line 78
    iget-object v2, v0, Lcom/twitter/android/people/adapters/viewbinders/g$b;->d:Landroid/support/v4/view/ViewPager;

    new-instance v3, Lcom/twitter/android/people/adapters/viewbinders/g$2;

    invoke-direct {v3, p0, v1}, Lcom/twitter/android/people/adapters/viewbinders/g$2;-><init>(Lcom/twitter/android/people/adapters/viewbinders/g;Lcom/twitter/android/people/adapters/viewbinders/g$a;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 85
    return-object v0
.end method

.method protected d()I
    .locals 1
    .annotation build Landroid/support/annotation/FractionRes;
    .end annotation

    .prologue
    .line 146
    const v0, 0x7f100005

    return v0
.end method
