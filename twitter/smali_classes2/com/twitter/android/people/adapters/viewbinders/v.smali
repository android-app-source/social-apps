.class public Lcom/twitter/android/people/adapters/viewbinders/v;
.super Lcom/twitter/android/people/adapters/viewbinders/d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/people/adapters/viewbinders/v$b;,
        Lcom/twitter/android/people/adapters/viewbinders/v$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/people/adapters/viewbinders/d",
        "<",
        "Lcom/twitter/model/people/l;",
        "Lcom/twitter/android/people/adapters/viewbinders/v$a;",
        "Lcom/twitter/android/people/adapters/b$m;",
        "Lcom/twitter/android/people/adapters/viewbinders/v$b;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:Lcom/twitter/util/collection/ReferenceList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/ReferenceList",
            "<",
            "Lcom/twitter/ui/user/UserSocialView;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Lcom/twitter/android/people/ai;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Lcom/twitter/util/collection/ReferenceList;->a()Lcom/twitter/util/collection/ReferenceList;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/people/adapters/viewbinders/v;->c:Lcom/twitter/util/collection/ReferenceList;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/app/users/c;Lcom/twitter/android/people/ai;Lcom/twitter/app/common/util/StateSaver;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/util/FriendshipCache;",
            "Lcom/twitter/app/users/c;",
            "Lcom/twitter/android/people/ai;",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/d",
            "<",
            "Lcom/twitter/model/people/l;",
            "Lcom/twitter/android/people/adapters/viewbinders/v$a;",
            "Lcom/twitter/android/people/adapters/b$m;",
            "Lcom/twitter/android/people/adapters/viewbinders/v$b;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 50
    new-instance v0, Lcom/twitter/android/people/adapters/viewbinders/v$1;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/v$1;-><init>(Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/app/users/c;)V

    invoke-direct {p0, p3, v0, p4}, Lcom/twitter/android/people/adapters/viewbinders/d;-><init>(Lcom/twitter/android/people/ai;Lcom/twitter/util/object/j;Lcom/twitter/app/common/util/StateSaver;)V

    .line 58
    sget-object v0, Lcom/twitter/android/people/adapters/viewbinders/v;->c:Lcom/twitter/util/collection/ReferenceList;

    invoke-virtual {v0}, Lcom/twitter/util/collection/ReferenceList;->b()V

    .line 59
    iput-object p3, p0, Lcom/twitter/android/people/adapters/viewbinders/v;->d:Lcom/twitter/android/people/ai;

    .line 60
    return-void
.end method

.method static synthetic f()Lcom/twitter/util/collection/ReferenceList;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/twitter/android/people/adapters/viewbinders/v;->c:Lcom/twitter/util/collection/ReferenceList;

    return-object v0
.end method


# virtual methods
.method protected bridge synthetic a(Lcom/twitter/android/widget/CarouselRowView;Lcom/twitter/android/widget/e;)Lcom/twitter/android/people/adapters/viewbinders/d$b;
    .locals 1

    .prologue
    .line 34
    check-cast p2, Lcom/twitter/android/people/adapters/viewbinders/v$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/v;->a(Lcom/twitter/android/widget/CarouselRowView;Lcom/twitter/android/people/adapters/viewbinders/v$a;)Lcom/twitter/android/people/adapters/viewbinders/v$b;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/android/widget/CarouselRowView;Lcom/twitter/android/people/adapters/viewbinders/v$a;)Lcom/twitter/android/people/adapters/viewbinders/v$b;
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/twitter/android/people/adapters/viewbinders/v$b;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/v$b;-><init>(Lcom/twitter/android/people/adapters/viewbinders/v;Lcom/twitter/android/widget/CarouselRowView;Lcom/twitter/android/people/adapters/viewbinders/v$a;)V

    return-object v0
.end method

.method protected bridge synthetic a(Landroid/support/v4/view/ViewPager;Lcom/twitter/android/people/adapters/b$d;)V
    .locals 0

    .prologue
    .line 34
    check-cast p2, Lcom/twitter/android/people/adapters/b$m;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/v;->a(Landroid/support/v4/view/ViewPager;Lcom/twitter/android/people/adapters/b$m;)V

    return-void
.end method

.method protected a(Landroid/support/v4/view/ViewPager;Lcom/twitter/android/people/adapters/b$m;)V
    .locals 3

    .prologue
    .line 89
    const/4 v0, 0x0

    .line 90
    iget-object v1, p2, Lcom/twitter/android/people/adapters/b$m;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/people/l;

    .line 91
    iget-object v0, v0, Lcom/twitter/model/people/l;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 92
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 94
    goto :goto_0

    .line 96
    :cond_0
    iget-object v0, p2, Lcom/twitter/android/people/adapters/b$m;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v1, v0, :cond_1

    if-nez v1, :cond_2

    .line 97
    :cond_1
    const/4 v0, 0x1

    .line 101
    :goto_2
    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 102
    return-void

    .line 99
    :cond_2
    iget-object v0, p2, Lcom/twitter/android/people/adapters/b$m;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public bridge synthetic a(Lckb$a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 34
    check-cast p1, Lcom/twitter/android/people/adapters/viewbinders/v$b;

    check-cast p2, Lcom/twitter/android/people/adapters/b$m;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/v;->a(Lcom/twitter/android/people/adapters/viewbinders/v$b;Lcom/twitter/android/people/adapters/b$m;)V

    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/android/people/adapters/viewbinders/d$b;Lcom/twitter/android/people/adapters/b$d;)V
    .locals 0

    .prologue
    .line 34
    check-cast p1, Lcom/twitter/android/people/adapters/viewbinders/v$b;

    check-cast p2, Lcom/twitter/android/people/adapters/b$m;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/v;->a(Lcom/twitter/android/people/adapters/viewbinders/v$b;Lcom/twitter/android/people/adapters/b$m;)V

    return-void
.end method

.method public a(Lcom/twitter/android/people/adapters/viewbinders/v$b;Lcom/twitter/android/people/adapters/b$m;)V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/d;->a(Lcom/twitter/android/people/adapters/viewbinders/d$b;Lcom/twitter/android/people/adapters/b$d;)V

    .line 66
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/v$b;->a:Lcom/twitter/android/widget/e;

    check-cast v0, Lcom/twitter/android/people/adapters/viewbinders/v$a;

    invoke-virtual {v0, p2}, Lcom/twitter/android/people/adapters/viewbinders/v$a;->a(Lcom/twitter/android/people/adapters/b$m;)V

    .line 67
    return-void
.end method

.method protected a(Lcom/twitter/model/people/l;Lcom/twitter/android/people/adapters/b$m;)V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/v;->d:Lcom/twitter/android/people/ai;

    invoke-virtual {p2}, Lcom/twitter/android/people/adapters/b$m;->c()Lcom/twitter/model/people/b;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p1, p1}, Lcom/twitter/android/people/ai;->a(Lcom/twitter/model/people/b;Lcom/twitter/android/people/adapters/b$d;Ljava/lang/Object;Lcom/twitter/model/people/l;)V

    .line 79
    return-void
.end method

.method protected a(Lcom/twitter/model/people/l;Lcom/twitter/android/people/adapters/b$m;Z)V
    .locals 6

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/v;->d:Lcom/twitter/android/people/ai;

    invoke-virtual {p2}, Lcom/twitter/android/people/adapters/b$m;->c()Lcom/twitter/model/people/b;

    move-result-object v1

    move-object v2, p2

    move-object v3, p1

    move-object v4, p1

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/people/ai;->a(Lcom/twitter/model/people/b;Lcom/twitter/android/people/adapters/b$d;Ljava/lang/Object;Lcom/twitter/model/people/l;Z)V

    .line 85
    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Lcom/twitter/android/people/adapters/b$d;)V
    .locals 0

    .prologue
    .line 34
    check-cast p1, Lcom/twitter/model/people/l;

    check-cast p2, Lcom/twitter/android/people/adapters/b$m;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/v;->a(Lcom/twitter/model/people/l;Lcom/twitter/android/people/adapters/b$m;)V

    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Lcom/twitter/android/people/adapters/b$d;Z)V
    .locals 0

    .prologue
    .line 34
    check-cast p1, Lcom/twitter/model/people/l;

    check-cast p2, Lcom/twitter/android/people/adapters/b$m;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/people/adapters/viewbinders/v;->a(Lcom/twitter/model/people/l;Lcom/twitter/android/people/adapters/b$m;Z)V

    return-void
.end method

.method public al_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    const-string/jumbo v0, "STATE_USER_CAROUSEL_VIEW_BINDER"

    return-object v0
.end method

.method protected b()I
    .locals 1

    .prologue
    .line 106
    const v0, 0x7f040289

    return v0
.end method

.method protected d()I
    .locals 1
    .annotation build Landroid/support/annotation/FractionRes;
    .end annotation

    .prologue
    .line 112
    const v0, 0x7f100001

    return v0
.end method
