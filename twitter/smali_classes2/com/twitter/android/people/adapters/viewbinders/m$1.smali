.class Lcom/twitter/android/people/adapters/viewbinders/m$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/people/adapters/viewbinders/m;->a(Lcom/twitter/android/people/adapters/viewbinders/m$a;Lcom/twitter/android/people/adapters/b$i;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/people/adapters/b$i;

.field final synthetic b:Lcom/twitter/model/people/k;

.field final synthetic c:Landroid/content/Context;

.field final synthetic d:J

.field final synthetic e:Lcom/twitter/android/people/adapters/viewbinders/m;


# direct methods
.method constructor <init>(Lcom/twitter/android/people/adapters/viewbinders/m;Lcom/twitter/android/people/adapters/b$i;Lcom/twitter/model/people/k;Landroid/content/Context;J)V
    .locals 1

    .prologue
    .line 113
    iput-object p1, p0, Lcom/twitter/android/people/adapters/viewbinders/m$1;->e:Lcom/twitter/android/people/adapters/viewbinders/m;

    iput-object p2, p0, Lcom/twitter/android/people/adapters/viewbinders/m$1;->a:Lcom/twitter/android/people/adapters/b$i;

    iput-object p3, p0, Lcom/twitter/android/people/adapters/viewbinders/m$1;->b:Lcom/twitter/model/people/k;

    iput-object p4, p0, Lcom/twitter/android/people/adapters/viewbinders/m$1;->c:Landroid/content/Context;

    iput-wide p5, p0, Lcom/twitter/android/people/adapters/viewbinders/m$1;->d:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 116
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/m$1;->e:Lcom/twitter/android/people/adapters/viewbinders/m;

    invoke-static {v0}, Lcom/twitter/android/people/adapters/viewbinders/m;->a(Lcom/twitter/android/people/adapters/viewbinders/m;)Lcom/twitter/android/people/ai;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/people/adapters/viewbinders/m$1;->a:Lcom/twitter/android/people/adapters/b$i;

    invoke-virtual {v0, v1}, Lcom/twitter/android/people/ai;->b(Lcom/twitter/android/people/adapters/viewbinders/p;)V

    .line 117
    const-string/jumbo v0, "twitter://people_address_book"

    iget-object v1, p0, Lcom/twitter/android/people/adapters/viewbinders/m$1;->b:Lcom/twitter/model/people/k;

    iget-object v1, v1, Lcom/twitter/model/people/k;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/m$1;->e:Lcom/twitter/android/people/adapters/viewbinders/m;

    invoke-static {v0}, Lcom/twitter/android/people/adapters/viewbinders/m;->b(Lcom/twitter/android/people/adapters/viewbinders/m;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 119
    if-eqz v0, :cond_0

    .line 120
    new-instance v1, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;

    invoke-direct {v1}, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;-><init>()V

    .line 121
    invoke-virtual {v1, v0}, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 120
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/people/adapters/viewbinders/m$1;->c:Landroid/content/Context;

    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/m$1;->b:Lcom/twitter/model/people/k;

    iget-object v3, v0, Lcom/twitter/model/people/k;->d:Ljava/lang/String;

    iget-wide v4, p0, Lcom/twitter/android/people/adapters/viewbinders/m$1;->d:J

    const/4 v9, 0x1

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    invoke-static/range {v1 .. v9}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Z)V

    goto :goto_0
.end method
