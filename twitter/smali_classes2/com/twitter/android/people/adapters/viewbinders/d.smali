.class public abstract Lcom/twitter/android/people/adapters/viewbinders/d;
.super Lckb;
.source "Twttr"

# interfaces
.implements Lang;


# annotations
.annotation build Lcom/twitter/app/AutoSaveState;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/people/adapters/viewbinders/d$a;,
        Lcom/twitter/android/people/adapters/viewbinders/d$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "A::",
        "Lcom/twitter/android/widget/e",
        "<TT;>;C:",
        "Lcom/twitter/android/people/adapters/b$d",
        "<TT;>;VH:",
        "Lcom/twitter/android/people/adapters/viewbinders/d$b;",
        ">",
        "Lckb",
        "<TC;TVH;>;",
        "Lang",
        "<",
        "Lcom/twitter/app/common/util/StateSaver",
        "<",
        "Lcom/twitter/android/people/adapters/viewbinders/d",
        "<TT;TA;TC;TVH;>;>;>;"
    }
.end annotation


# instance fields
.field protected final a:Lcom/twitter/android/people/ai;

.field b:Ljava/util/Map;
    .annotation build Lcom/twitter/app/SaveState;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/util/object/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/j",
            "<TA;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/twitter/android/people/ai;Lcom/twitter/util/object/j;Lcom/twitter/app/common/util/StateSaver;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/people/ai;",
            "Lcom/twitter/util/object/j",
            "<TA;>;",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/d",
            "<TT;TA;TC;TVH;>;>;)V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0}, Lckb;-><init>()V

    .line 47
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/d;->b:Ljava/util/Map;

    .line 52
    iput-object p1, p0, Lcom/twitter/android/people/adapters/viewbinders/d;->a:Lcom/twitter/android/people/ai;

    .line 53
    iput-object p2, p0, Lcom/twitter/android/people/adapters/viewbinders/d;->c:Lcom/twitter/util/object/j;

    .line 55
    invoke-virtual {p3, p0}, Lcom/twitter/app/common/util/StateSaver;->a(Ljava/lang/Object;)V

    .line 56
    return-void
.end method

.method private a(Lcom/twitter/android/people/adapters/viewbinders/d$b;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/people/adapters/viewbinders/d",
            "<TT;TA;TC;TVH;>.b;I)V"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/d$b;->c:Lcom/twitter/android/people/adapters/b$d;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/d$b;->c:Lcom/twitter/android/people/adapters/b$d;

    iget-object v0, v0, Lcom/twitter/android/people/adapters/b$d;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/d;->b:Ljava/util/Map;

    iget-object v1, p1, Lcom/twitter/android/people/adapters/viewbinders/d$b;->c:Lcom/twitter/android/people/adapters/b$d;

    iget-object v1, v1, Lcom/twitter/android/people/adapters/b$d;->b:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/people/adapters/viewbinders/d;Lcom/twitter/android/people/adapters/viewbinders/d$b;I)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/d;->a(Lcom/twitter/android/people/adapters/viewbinders/d$b;I)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)Lcom/twitter/android/people/adapters/viewbinders/d$b;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")TVH;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 61
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 62
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 63
    invoke-virtual {p0}, Lcom/twitter/android/people/adapters/viewbinders/d;->b()I

    move-result v1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/CarouselRowView;

    .line 64
    iget-object v1, p0, Lcom/twitter/android/people/adapters/viewbinders/d;->c:Lcom/twitter/util/object/j;

    invoke-interface {v1}, Lcom/twitter/util/object/j;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/e;

    .line 65
    invoke-virtual {p0, v2, v1}, Lcom/twitter/android/people/adapters/viewbinders/d;->a(Landroid/content/Context;Lcom/twitter/android/widget/e;)Lcom/twitter/android/widget/d;

    move-result-object v3

    .line 66
    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v4

    .line 68
    invoke-virtual {p0}, Lcom/twitter/android/people/adapters/viewbinders/d;->d()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v3, v5}, Lcom/twitter/android/widget/d;->a(F)V

    .line 69
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/people/adapters/viewbinders/d;->a(Lcom/twitter/android/widget/CarouselRowView;Lcom/twitter/android/widget/e;)Lcom/twitter/android/people/adapters/viewbinders/d$b;

    move-result-object v1

    .line 70
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/CarouselRowView;->setTag(Ljava/lang/Object;)V

    .line 71
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {p0}, Lcom/twitter/android/people/adapters/viewbinders/d;->d()I

    move-result v6

    invoke-virtual {v5, v6, v7, v7}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v5

    invoke-virtual {v3, v5}, Lcom/twitter/android/widget/d;->a(F)V

    .line 72
    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/CarouselRowView;->setCarouselAdapter(Lcom/twitter/android/widget/d;)V

    .line 73
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v5, 0x7f110000

    invoke-static {v2, v5}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {v3, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/CarouselRowView;->setCarouselBackground(Landroid/graphics/drawable/Drawable;)V

    .line 76
    new-instance v2, Lcom/twitter/android/widget/f;

    iget-object v3, v1, Lcom/twitter/android/people/adapters/viewbinders/d$b;->b:Lcom/twitter/android/people/adapters/viewbinders/d$a;

    invoke-direct {v2, v0, p1, v4, v3}, Lcom/twitter/android/widget/f;-><init>(Lcom/twitter/android/widget/CarouselRowView;Landroid/view/ViewParent;ILcom/twitter/android/widget/f$a;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/CarouselRowView;->a(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 78
    new-instance v2, Lcom/twitter/android/people/adapters/viewbinders/d$1;

    invoke-direct {v2, p0, v1}, Lcom/twitter/android/people/adapters/viewbinders/d$1;-><init>(Lcom/twitter/android/people/adapters/viewbinders/d;Lcom/twitter/android/people/adapters/viewbinders/d$b;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/CarouselRowView;->a(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 84
    return-object v1
.end method

.method protected a(Lcom/twitter/android/widget/CarouselRowView;Lcom/twitter/android/widget/e;)Lcom/twitter/android/people/adapters/viewbinders/d$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/widget/CarouselRowView;",
            "TA;)TVH;"
        }
    .end annotation

    .prologue
    .line 89
    new-instance v0, Lcom/twitter/android/people/adapters/viewbinders/d$b;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/d$b;-><init>(Lcom/twitter/android/people/adapters/viewbinders/d;Lcom/twitter/android/widget/CarouselRowView;Lcom/twitter/android/widget/e;)V

    return-object v0
.end method

.method protected a(Landroid/content/Context;Lcom/twitter/android/widget/e;)Lcom/twitter/android/widget/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TA;)",
            "Lcom/twitter/android/widget/d",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 100
    new-instance v0, Lcom/twitter/android/widget/d;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/widget/d;-><init>(Landroid/content/Context;Lcom/twitter/android/widget/e;)V

    return-object v0
.end method

.method protected a(Landroid/support/v4/view/ViewPager;Lcom/twitter/android/people/adapters/b$d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/view/ViewPager;",
            "TC;)V"
        }
    .end annotation

    .prologue
    .line 151
    return-void
.end method

.method public bridge synthetic a(Lckb$a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 37
    check-cast p1, Lcom/twitter/android/people/adapters/viewbinders/d$b;

    check-cast p2, Lcom/twitter/android/people/adapters/b$d;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/d;->a(Lcom/twitter/android/people/adapters/viewbinders/d$b;Lcom/twitter/android/people/adapters/b$d;)V

    return-void
.end method

.method public a(Lcom/twitter/android/people/adapters/viewbinders/d$b;Lcom/twitter/android/people/adapters/b$d;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;TC;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 118
    invoke-virtual {p1}, Lcom/twitter/android/people/adapters/viewbinders/d$b;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/CarouselRowView;

    .line 120
    invoke-virtual {v0}, Lcom/twitter/android/widget/CarouselRowView;->getCarouselAdapter()Lcom/twitter/android/widget/d;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/d;

    .line 122
    iput-object p2, p1, Lcom/twitter/android/people/adapters/viewbinders/d$b;->c:Lcom/twitter/android/people/adapters/b$d;

    .line 123
    iget-object v2, p1, Lcom/twitter/android/people/adapters/viewbinders/d$b;->b:Lcom/twitter/android/people/adapters/viewbinders/d$a;

    iput-object p2, v2, Lcom/twitter/android/people/adapters/viewbinders/d$a;->a:Lcom/twitter/android/people/adapters/b$d;

    .line 128
    invoke-virtual {v1}, Lcom/twitter/android/widget/d;->a()I

    move-result v2

    iget-object v3, p2, Lcom/twitter/android/people/adapters/b$d;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 129
    new-instance v2, Lcbl;

    iget-object v3, p2, Lcom/twitter/android/people/adapters/b$d;->a:Ljava/util/List;

    invoke-direct {v2, v3}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/d;->b(Lcbi;)V

    .line 133
    :goto_0
    invoke-virtual {v1}, Lcom/twitter/android/widget/d;->a()I

    move-result v2

    if-lez v2, :cond_2

    .line 134
    iget-object v2, p0, Lcom/twitter/android/people/adapters/viewbinders/d;->b:Ljava/util/Map;

    iget-object v3, p2, Lcom/twitter/android/people/adapters/b$d;->b:Ljava/lang/String;

    .line 135
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1}, Lcom/twitter/android/widget/d;->a()I

    move-result v3

    rem-int/2addr v2, v3

    .line 136
    iget-object v3, p2, Lcom/twitter/android/people/adapters/b$d;->a:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3, p2}, Lcom/twitter/android/people/adapters/viewbinders/d;->a(Ljava/lang/Object;Lcom/twitter/android/people/adapters/b$d;)V

    .line 137
    invoke-virtual {v0}, Lcom/twitter/android/widget/CarouselRowView;->getCurrentItemIndex()I

    move-result v3

    if-eq v3, v2, :cond_0

    .line 138
    iget-object v3, p1, Lcom/twitter/android/people/adapters/viewbinders/d$b;->b:Lcom/twitter/android/people/adapters/viewbinders/d$a;

    invoke-virtual {v3}, Lcom/twitter/android/people/adapters/viewbinders/d$a;->a()V

    .line 139
    invoke-virtual {v0, v2, v4}, Lcom/twitter/android/widget/CarouselRowView;->a(IZ)V

    .line 145
    :cond_0
    :goto_1
    iget-object v0, p1, Lcom/twitter/android/people/adapters/viewbinders/d$b;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/people/adapters/viewbinders/d;->a(Landroid/support/v4/view/ViewPager;Lcom/twitter/android/people/adapters/b$d;)V

    .line 146
    invoke-virtual {v1}, Lcom/twitter/android/widget/d;->notifyDataSetChanged()V

    .line 147
    return-void

    .line 131
    :cond_1
    new-instance v2, Lcbl;

    iget-object v3, p2, Lcom/twitter/android/people/adapters/b$d;->a:Ljava/util/List;

    invoke-direct {v2, v3}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/d;->a(Lcbi;)V

    goto :goto_0

    .line 142
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "A carousel should not be empty"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method protected a(Ljava/lang/Object;Lcom/twitter/android/people/adapters/b$d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TC;)V"
        }
    .end annotation

    .prologue
    .line 109
    return-void
.end method

.method protected a(Ljava/lang/Object;Lcom/twitter/android/people/adapters/b$d;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TC;Z)V"
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/d;->a:Lcom/twitter/android/people/ai;

    invoke-virtual {p2}, Lcom/twitter/android/people/adapters/b$d;->c()Lcom/twitter/model/people/b;

    move-result-object v1

    const/4 v4, 0x0

    move-object v2, p2

    move-object v3, p1

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/people/ai;->a(Lcom/twitter/model/people/b;Lcom/twitter/android/people/adapters/b$d;Ljava/lang/Object;Lcom/twitter/model/people/l;Z)V

    .line 105
    return-void
.end method

.method public a(Lcom/twitter/android/people/adapters/b$d;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;)Z"
        }
    .end annotation

    .prologue
    .line 155
    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 37
    check-cast p1, Lcom/twitter/android/people/adapters/b$d;

    invoke-virtual {p0, p1}, Lcom/twitter/android/people/adapters/viewbinders/d;->a(Lcom/twitter/android/people/adapters/b$d;)Z

    move-result v0

    return v0
.end method

.method protected b()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 113
    const v0, 0x7f04012f

    return v0
.end method

.method public synthetic b(Landroid/view/ViewGroup;)Lckb$a;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/twitter/android/people/adapters/viewbinders/d;->a(Landroid/view/ViewGroup;)Lcom/twitter/android/people/adapters/viewbinders/d$b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/twitter/android/people/adapters/viewbinders/d;->e()Lcom/twitter/app/common/util/StateSaver;

    move-result-object v0

    return-object v0
.end method

.method protected d()I
    .locals 1
    .annotation build Landroid/support/annotation/FractionRes;
    .end annotation

    .prologue
    .line 164
    const v0, 0x7f100004

    return v0
.end method

.method public e()Lcom/twitter/app/common/util/StateSaver;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/d",
            "<TT;TA;TC;TVH;>;>;"
        }
    .end annotation

    .prologue
    .line 170
    new-instance v0, Lcom/twitter/android/people/adapters/viewbinders/CarouselViewBinderSavedState;

    invoke-direct {v0, p0}, Lcom/twitter/android/people/adapters/viewbinders/CarouselViewBinderSavedState;-><init>(Lcom/twitter/android/people/adapters/viewbinders/d;)V

    return-object v0
.end method
