.class public Lcom/twitter/android/people/adapters/viewbinders/q;
.super Lcom/twitter/android/people/adapters/viewbinders/o;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/people/ai;

.field private b:Lcom/twitter/android/people/adapters/viewbinders/p;


# direct methods
.method public constructor <init>(Lcom/twitter/android/people/ai;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 21
    sget-object v1, Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;->b:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    const/4 v3, -0x1

    const/4 v5, 0x0

    move-object v0, p0

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/people/adapters/viewbinders/o;-><init>(Lcom/twitter/media/ui/image/config/g;ZIIF)V

    .line 22
    iput-object p1, p0, Lcom/twitter/android/people/adapters/viewbinders/q;->a:Lcom/twitter/android/people/ai;

    .line 23
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;)V
    .locals 2

    .prologue
    .line 31
    invoke-super {p0, p1, p2}, Lcom/twitter/android/people/adapters/viewbinders/o;->a(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;)V

    .line 32
    iget-object v1, p0, Lcom/twitter/android/people/adapters/viewbinders/q;->a:Lcom/twitter/android/people/ai;

    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/q;->b:Lcom/twitter/android/people/adapters/viewbinders/p;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/adapters/viewbinders/p;

    invoke-virtual {v1, v0}, Lcom/twitter/android/people/ai;->a(Lcom/twitter/android/people/adapters/viewbinders/p;)V

    .line 33
    return-void
.end method

.method public a(Lcom/twitter/android/people/adapters/viewbinders/p;)V
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcom/twitter/android/people/adapters/viewbinders/q;->b:Lcom/twitter/android/people/adapters/viewbinders/p;

    .line 27
    return-void
.end method
