.class public abstract Lcom/twitter/android/people/adapters/viewbinders/c;
.super Lckb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/android/people/adapters/b$c;",
        "VH:",
        "Lckb$a;",
        ">",
        "Lckb",
        "<TT;TVH;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lckb;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/ui/widget/PromptView;Lcom/twitter/android/people/adapters/b$c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/ui/widget/PromptView;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 14
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/ui/widget/PromptView;->setVisibility(I)V

    .line 15
    iget-object v0, p2, Lcom/twitter/android/people/adapters/b$c;->b:Lcom/twitter/model/people/d;

    iget-object v0, v0, Lcom/twitter/model/people/d;->c:Lcom/twitter/model/people/ModuleTitle;

    iget-object v0, v0, Lcom/twitter/model/people/ModuleTitle;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/ui/widget/PromptView;->setTitle(Ljava/lang/CharSequence;)V

    .line 16
    iget-object v0, p2, Lcom/twitter/android/people/adapters/b$c;->b:Lcom/twitter/model/people/d;

    iget-object v0, v0, Lcom/twitter/model/people/d;->d:Lcom/twitter/model/people/ModuleTitle;

    iget-object v0, v0, Lcom/twitter/model/people/ModuleTitle;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/ui/widget/PromptView;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 17
    iget-object v0, p2, Lcom/twitter/android/people/adapters/b$c;->c:Lcom/twitter/model/people/k;

    iget-object v0, v0, Lcom/twitter/model/people/k;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/ui/widget/PromptView;->setButtonText(Ljava/lang/CharSequence;)V

    .line 18
    return-void
.end method
