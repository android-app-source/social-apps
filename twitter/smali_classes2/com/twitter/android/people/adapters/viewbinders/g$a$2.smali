.class Lcom/twitter/android/people/adapters/viewbinders/g$a$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/people/adapters/viewbinders/g$a;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/support/v4/view/ViewPager;

.field final synthetic b:Lcom/twitter/android/people/adapters/viewbinders/g$a;

.field private c:I


# direct methods
.method constructor <init>(Lcom/twitter/android/people/adapters/viewbinders/g$a;Landroid/support/v4/view/ViewPager;)V
    .locals 1

    .prologue
    .line 199
    iput-object p1, p0, Lcom/twitter/android/people/adapters/viewbinders/g$a$2;->b:Lcom/twitter/android/people/adapters/viewbinders/g$a;

    iput-object p2, p0, Lcom/twitter/android/people/adapters/viewbinders/g$a$2;->a:Landroid/support/v4/view/ViewPager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 200
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/people/adapters/viewbinders/g$a$2;->c:I

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .prologue
    .line 204
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/g$a$2;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->isFakeDragging()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    :try_start_0
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 207
    iget-object v1, p0, Lcom/twitter/android/people/adapters/viewbinders/g$a$2;->a:Landroid/support/v4/view/ViewPager;

    iget v2, p0, Lcom/twitter/android/people/adapters/viewbinders/g$a$2;->c:I

    sub-int/2addr v2, v0

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->fakeDragBy(F)V

    .line 208
    iput v0, p0, Lcom/twitter/android/people/adapters/viewbinders/g$a$2;->c:I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 209
    :catch_0
    move-exception v0

    goto :goto_0
.end method
