.class Lcom/twitter/android/people/adapters/viewbinders/g$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/people/adapters/viewbinders/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/d",
            "<",
            "Lcom/twitter/model/people/h;",
            "Lcom/twitter/android/people/adapters/a;",
            "Lcom/twitter/android/people/adapters/b$f;",
            "Lcom/twitter/android/people/adapters/viewbinders/g$b;",
            ">.a;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/support/v4/view/ViewPager;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/app/common/util/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/android/people/adapters/viewbinders/g$b;Lcom/twitter/app/common/util/j;)V
    .locals 2

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p1, Lcom/twitter/android/people/adapters/viewbinders/g$b;->b:Lcom/twitter/android/people/adapters/viewbinders/d$a;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/g$a;->a:Ljava/lang/ref/WeakReference;

    .line 163
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p1, Lcom/twitter/android/people/adapters/viewbinders/g$b;->d:Landroid/support/v4/view/ViewPager;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/g$a;->b:Ljava/lang/ref/WeakReference;

    .line 164
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/g$a;->c:Ljava/lang/ref/WeakReference;

    .line 165
    iput-object p0, p1, Lcom/twitter/android/people/adapters/viewbinders/g$b;->f:Ljava/lang/Runnable;

    .line 166
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/android/people/adapters/viewbinders/d$a;Landroid/support/v4/view/ViewPager;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/people/adapters/viewbinders/d",
            "<",
            "Lcom/twitter/model/people/h;",
            "Lcom/twitter/android/people/adapters/a;",
            "Lcom/twitter/android/people/adapters/b$f;",
            "Lcom/twitter/android/people/adapters/viewbinders/g$b;",
            ">.a;",
            "Landroid/support/v4/view/ViewPager;",
            ")V"
        }
    .end annotation

    .prologue
    .line 222
    invoke-virtual {p1}, Lcom/twitter/android/people/adapters/viewbinders/d$a;->a()V

    .line 223
    invoke-virtual {p2}, Landroid/support/v4/view/ViewPager;->isFakeDragging()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    :try_start_0
    invoke-virtual {p2}, Landroid/support/v4/view/ViewPager;->endFakeDrag()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 226
    :catch_0
    move-exception v0

    .line 232
    :try_start_1
    const-class v0, Landroid/support/v4/view/ViewPager;

    const-string/jumbo v1, "mVelocityTracker"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 233
    if-eqz v0, :cond_0

    .line 234
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 235
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 237
    invoke-virtual {p2}, Landroid/support/v4/view/ViewPager;->endFakeDrag()V
    :try_end_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 239
    :catch_1
    move-exception v0

    .line 240
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 241
    :catch_2
    move-exception v0

    .line 242
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 170
    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/g$a;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    .line 171
    iget-object v1, p0, Lcom/twitter/android/people/adapters/viewbinders/g$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/people/adapters/viewbinders/d$a;

    .line 172
    iget-object v2, p0, Lcom/twitter/android/people/adapters/viewbinders/g$a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/app/common/util/j;

    .line 173
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 175
    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-eq v3, v4, :cond_0

    .line 177
    invoke-static {v0}, Lcom/twitter/util/ui/k;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 179
    invoke-interface {v2}, Lcom/twitter/app/common/util/j;->g_()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 181
    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->beginFakeDrag()Z

    move-result v2

    if-nez v2, :cond_1

    .line 219
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    const/4 v2, 0x2

    new-array v2, v2, [I

    aput v5, v2, v5

    const/4 v3, 0x1

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v4

    aput v4, v2, v3

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 186
    new-instance v3, Lcom/twitter/android/people/adapters/viewbinders/g$a$1;

    invoke-direct {v3, p0, v1, v0}, Lcom/twitter/android/people/adapters/viewbinders/g$a$1;-><init>(Lcom/twitter/android/people/adapters/viewbinders/g$a;Lcom/twitter/android/people/adapters/viewbinders/d$a;Landroid/support/v4/view/ViewPager;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 198
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 199
    new-instance v1, Lcom/twitter/android/people/adapters/viewbinders/g$a$2;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/people/adapters/viewbinders/g$a$2;-><init>(Lcom/twitter/android/people/adapters/viewbinders/g$a;Landroid/support/v4/view/ViewPager;)V

    invoke-virtual {v2, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 217
    const-wide/16 v0, 0x3e8

    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 218
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method
