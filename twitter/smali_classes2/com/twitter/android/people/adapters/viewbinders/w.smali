.class public final Lcom/twitter/android/people/adapters/viewbinders/w;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lcom/twitter/android/people/adapters/viewbinders/v;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcsd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcsd",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/v;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/model/util/FriendshipCache;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/users/c;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/ai;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/d",
            "<",
            "Lcom/twitter/model/people/l;",
            "Lcom/twitter/android/people/adapters/viewbinders/v$a;",
            "Lcom/twitter/android/people/adapters/b$m;",
            "Lcom/twitter/android/people/adapters/viewbinders/v$b;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/twitter/android/people/adapters/viewbinders/w;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/people/adapters/viewbinders/w;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcsd;Lcta;Lcta;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/v;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/model/util/FriendshipCache;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/users/c;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/ai;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/d",
            "<",
            "Lcom/twitter/model/people/l;",
            "Lcom/twitter/android/people/adapters/viewbinders/v$a;",
            "Lcom/twitter/android/people/adapters/b$m;",
            "Lcom/twitter/android/people/adapters/viewbinders/v$b;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    sget-boolean v0, Lcom/twitter/android/people/adapters/viewbinders/w;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 47
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/people/adapters/viewbinders/w;->b:Lcsd;

    .line 48
    sget-boolean v0, Lcom/twitter/android/people/adapters/viewbinders/w;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 49
    :cond_1
    iput-object p2, p0, Lcom/twitter/android/people/adapters/viewbinders/w;->c:Lcta;

    .line 50
    sget-boolean v0, Lcom/twitter/android/people/adapters/viewbinders/w;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 51
    :cond_2
    iput-object p3, p0, Lcom/twitter/android/people/adapters/viewbinders/w;->d:Lcta;

    .line 52
    sget-boolean v0, Lcom/twitter/android/people/adapters/viewbinders/w;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 53
    :cond_3
    iput-object p4, p0, Lcom/twitter/android/people/adapters/viewbinders/w;->e:Lcta;

    .line 54
    sget-boolean v0, Lcom/twitter/android/people/adapters/viewbinders/w;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 55
    :cond_4
    iput-object p5, p0, Lcom/twitter/android/people/adapters/viewbinders/w;->f:Lcta;

    .line 56
    return-void
.end method

.method public static a(Lcsd;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/v;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/model/util/FriendshipCache;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/users/c;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/ai;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/d",
            "<",
            "Lcom/twitter/model/people/l;",
            "Lcom/twitter/android/people/adapters/viewbinders/v$a;",
            "Lcom/twitter/android/people/adapters/b$m;",
            "Lcom/twitter/android/people/adapters/viewbinders/v$b;",
            ">;>;>;)",
            "Ldagger/internal/c",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/v;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    new-instance v0, Lcom/twitter/android/people/adapters/viewbinders/w;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/people/adapters/viewbinders/w;-><init>(Lcsd;Lcta;Lcta;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/android/people/adapters/viewbinders/v;
    .locals 6

    .prologue
    .line 60
    iget-object v4, p0, Lcom/twitter/android/people/adapters/viewbinders/w;->b:Lcsd;

    new-instance v5, Lcom/twitter/android/people/adapters/viewbinders/v;

    iget-object v0, p0, Lcom/twitter/android/people/adapters/viewbinders/w;->c:Lcta;

    .line 63
    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/util/FriendshipCache;

    iget-object v1, p0, Lcom/twitter/android/people/adapters/viewbinders/w;->d:Lcta;

    .line 64
    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/app/users/c;

    iget-object v2, p0, Lcom/twitter/android/people/adapters/viewbinders/w;->e:Lcta;

    .line 65
    invoke-interface {v2}, Lcta;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/people/ai;

    iget-object v3, p0, Lcom/twitter/android/people/adapters/viewbinders/w;->f:Lcta;

    .line 66
    invoke-interface {v3}, Lcta;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/app/common/util/StateSaver;

    invoke-direct {v5, v0, v1, v2, v3}, Lcom/twitter/android/people/adapters/viewbinders/v;-><init>(Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/app/users/c;Lcom/twitter/android/people/ai;Lcom/twitter/app/common/util/StateSaver;)V

    .line 60
    invoke-static {v4, v5}, Ldagger/internal/MembersInjectors;->a(Lcsd;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/adapters/viewbinders/v;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/twitter/android/people/adapters/viewbinders/w;->a()Lcom/twitter/android/people/adapters/viewbinders/v;

    move-result-object v0

    return-object v0
.end method
