.class public Lcom/twitter/android/people/z;
.super Lcom/twitter/app/common/list/l;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/list/l",
        "<",
        "Lcom/twitter/android/people/adapters/b;",
        "Lcjr",
        "<",
        "Lcom/twitter/android/people/adapters/b;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final e:Lcom/twitter/android/people/ai;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/twitter/app/common/list/l$d;Lcom/twitter/android/people/k;Lcom/twitter/android/people/ai;Lcom/twitter/android/people/adapters/viewbinders/m;Lcom/twitter/android/people/adapters/viewbinders/e;Lcom/twitter/android/people/adapters/viewbinders/r;Lcom/twitter/android/people/adapters/viewbinders/v;Lcom/twitter/android/people/adapters/viewbinders/g;Lcom/twitter/android/people/adapters/viewbinders/a;Lcom/twitter/android/people/adapters/viewbinders/k;Lcom/twitter/android/people/adapters/viewbinders/t;Lcom/twitter/android/people/adapters/viewbinders/i;)V
    .locals 6

    .prologue
    .line 56
    invoke-direct {p0, p2, p3}, Lcom/twitter/app/common/list/l;-><init>(Landroid/view/LayoutInflater;Lcom/twitter/app/common/list/l$d;)V

    .line 58
    iput-object p5, p0, Lcom/twitter/android/people/z;->e:Lcom/twitter/android/people/ai;

    .line 60
    iget-object v1, p0, Lcom/twitter/android/people/z;->a:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 62
    new-instance v2, Lckg;

    new-instance v3, Lcjp;

    invoke-direct {v3}, Lcjp;-><init>()V

    new-instance v4, Lcjz;

    .line 65
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v1

    const-class v5, Lcom/twitter/android/people/adapters/b$i;

    .line 66
    invoke-virtual {v1, v5, p6}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v1

    const-class v5, Lcom/twitter/android/people/adapters/b$e;

    .line 67
    invoke-virtual {v1, v5, p7}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v1

    const-class v5, Lcom/twitter/android/people/adapters/b$k;

    .line 68
    invoke-virtual {v1, v5, p8}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v1

    const-class v5, Lcom/twitter/android/people/adapters/b$m;

    .line 69
    invoke-virtual {v1, v5, p9}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v1

    const-class v5, Lcom/twitter/android/people/adapters/b$f;

    .line 70
    move-object/from16 v0, p10

    invoke-virtual {v1, v5, v0}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v1

    const-class v5, Lcom/twitter/android/people/adapters/b$a;

    .line 71
    move-object/from16 v0, p11

    invoke-virtual {v1, v5, v0}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v1

    const-class v5, Lcom/twitter/android/people/adapters/b$h;

    .line 72
    move-object/from16 v0, p12

    invoke-virtual {v1, v5, v0}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v1

    const-class v5, Lcom/twitter/android/people/adapters/b$l;

    .line 73
    move-object/from16 v0, p13

    invoke-virtual {v1, v5, v0}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v1

    const-class v5, Lcom/twitter/android/people/adapters/b$g;

    .line 74
    move-object/from16 v0, p14

    invoke-virtual {v1, v5, v0}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v1

    .line 75
    invoke-virtual {v1}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-direct {v4, v1}, Lcjz;-><init>(Ljava/util/Map;)V

    invoke-direct {v2, v3, v4}, Lckg;-><init>(Lcju;Lckd;)V

    .line 77
    new-instance v1, Lcom/twitter/android/people/z$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/people/z$1;-><init>(Lcom/twitter/android/people/z;)V

    invoke-virtual {v2, v1}, Lckg;->a(Lcke;)V

    .line 85
    invoke-virtual {p0, v2}, Lcom/twitter/android/people/z;->a(Landroid/widget/ListAdapter;)V

    .line 87
    new-instance v1, Lcom/twitter/android/people/z$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/people/z$2;-><init>(Lcom/twitter/android/people/z;)V

    invoke-interface {p4, v1}, Lcom/twitter/android/people/k;->a(Lcom/twitter/android/people/k$a;)V

    .line 99
    iget-object v1, p0, Lcom/twitter/android/people/z;->b:Landroid/view/View;

    new-instance v2, Lcom/twitter/android/people/z$3;

    invoke-direct {v2, p0, p4}, Lcom/twitter/android/people/z$3;-><init>(Lcom/twitter/android/people/z;Lcom/twitter/android/people/k;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    invoke-virtual {p0}, Lcom/twitter/android/people/z;->o()Lanh;

    move-result-object v1

    .line 109
    invoke-virtual {v1, p9}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 110
    move-object/from16 v0, p10

    invoke-virtual {v1, v0}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 111
    move-object/from16 v0, p13

    invoke-virtual {v1, v0}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 113
    new-instance v1, Lcom/twitter/android/people/z$4;

    invoke-direct {v1, p0}, Lcom/twitter/android/people/z$4;-><init>(Lcom/twitter/android/people/z;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/people/z;->a(Lcno$c;)V

    .line 119
    return-void
.end method

.method private A()Z
    .locals 2

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/twitter/android/people/z;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/people/z;->B()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/people/z;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeight()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private B()I
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/twitter/android/people/z;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/people/z;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    return v0
.end method

.method private C()Z
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/twitter/android/people/z;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/people/z;->k()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/people/z;)Lcom/twitter/android/people/ai;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/people/z;->e:Lcom/twitter/android/people/ai;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/people/z;I)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/twitter/android/people/z;->c(I)V

    return-void
.end method

.method private c(I)V
    .locals 3

    .prologue
    .line 128
    if-nez p1, :cond_0

    .line 129
    iget-object v0, p0, Lcom/twitter/android/people/z;->e:Lcom/twitter/android/people/ai;

    iget-object v1, p0, Lcom/twitter/android/people/z;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/android/people/z;->k()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/people/ai;->a(II)V

    .line 130
    invoke-direct {p0}, Lcom/twitter/android/people/z;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/twitter/android/people/z;->e:Lcom/twitter/android/people/ai;

    invoke-virtual {v0}, Lcom/twitter/android/people/ai;->c()V

    .line 134
    :cond_0
    return-void
.end method


# virtual methods
.method public aB_()V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0}, Lcom/twitter/app/common/list/l;->aB_()V

    .line 124
    iget-object v0, p0, Lcom/twitter/android/people/z;->e:Lcom/twitter/android/people/ai;

    invoke-virtual {v0}, Lcom/twitter/android/people/ai;->a()V

    .line 125
    return-void
.end method
