.class public final Lcom/twitter/android/people/aa;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lcom/twitter/android/people/z;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcsd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcsd",
            "<",
            "Lcom/twitter/android/people/z;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/list/l$d;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/k;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/ai;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/m;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/e;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/r;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/v;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/g;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/a;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/k;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/t;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/twitter/android/people/aa;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/people/aa;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcsd;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lcom/twitter/android/people/z;",
            ">;",
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/common/list/l$d;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/k;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/ai;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/m;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/e;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/r;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/v;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/g;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/a;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/k;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/t;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/i;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    sget-boolean v1, Lcom/twitter/android/people/aa;->a:Z

    if-nez v1, :cond_0

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 73
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/people/aa;->b:Lcsd;

    .line 74
    sget-boolean v1, Lcom/twitter/android/people/aa;->a:Z

    if-nez v1, :cond_1

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 75
    :cond_1
    iput-object p2, p0, Lcom/twitter/android/people/aa;->c:Lcta;

    .line 76
    sget-boolean v1, Lcom/twitter/android/people/aa;->a:Z

    if-nez v1, :cond_2

    if-nez p3, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 77
    :cond_2
    iput-object p3, p0, Lcom/twitter/android/people/aa;->d:Lcta;

    .line 78
    sget-boolean v1, Lcom/twitter/android/people/aa;->a:Z

    if-nez v1, :cond_3

    if-nez p4, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 79
    :cond_3
    iput-object p4, p0, Lcom/twitter/android/people/aa;->e:Lcta;

    .line 80
    sget-boolean v1, Lcom/twitter/android/people/aa;->a:Z

    if-nez v1, :cond_4

    if-nez p5, :cond_4

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 81
    :cond_4
    iput-object p5, p0, Lcom/twitter/android/people/aa;->f:Lcta;

    .line 82
    sget-boolean v1, Lcom/twitter/android/people/aa;->a:Z

    if-nez v1, :cond_5

    if-nez p6, :cond_5

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 83
    :cond_5
    iput-object p6, p0, Lcom/twitter/android/people/aa;->g:Lcta;

    .line 84
    sget-boolean v1, Lcom/twitter/android/people/aa;->a:Z

    if-nez v1, :cond_6

    if-nez p7, :cond_6

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 85
    :cond_6
    iput-object p7, p0, Lcom/twitter/android/people/aa;->h:Lcta;

    .line 86
    sget-boolean v1, Lcom/twitter/android/people/aa;->a:Z

    if-nez v1, :cond_7

    if-nez p8, :cond_7

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 87
    :cond_7
    iput-object p8, p0, Lcom/twitter/android/people/aa;->i:Lcta;

    .line 88
    sget-boolean v1, Lcom/twitter/android/people/aa;->a:Z

    if-nez v1, :cond_8

    if-nez p9, :cond_8

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 89
    :cond_8
    iput-object p9, p0, Lcom/twitter/android/people/aa;->j:Lcta;

    .line 90
    sget-boolean v1, Lcom/twitter/android/people/aa;->a:Z

    if-nez v1, :cond_9

    if-nez p10, :cond_9

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 91
    :cond_9
    iput-object p10, p0, Lcom/twitter/android/people/aa;->k:Lcta;

    .line 92
    sget-boolean v1, Lcom/twitter/android/people/aa;->a:Z

    if-nez v1, :cond_a

    if-nez p11, :cond_a

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 93
    :cond_a
    iput-object p11, p0, Lcom/twitter/android/people/aa;->l:Lcta;

    .line 94
    sget-boolean v1, Lcom/twitter/android/people/aa;->a:Z

    if-nez v1, :cond_b

    if-nez p12, :cond_b

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 95
    :cond_b
    iput-object p12, p0, Lcom/twitter/android/people/aa;->m:Lcta;

    .line 96
    sget-boolean v1, Lcom/twitter/android/people/aa;->a:Z

    if-nez v1, :cond_c

    if-nez p13, :cond_c

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 97
    :cond_c
    iput-object p13, p0, Lcom/twitter/android/people/aa;->n:Lcta;

    .line 98
    sget-boolean v1, Lcom/twitter/android/people/aa;->a:Z

    if-nez v1, :cond_d

    if-nez p14, :cond_d

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 99
    :cond_d
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/twitter/android/people/aa;->o:Lcta;

    .line 100
    sget-boolean v1, Lcom/twitter/android/people/aa;->a:Z

    if-nez v1, :cond_e

    if-nez p15, :cond_e

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 101
    :cond_e
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/twitter/android/people/aa;->p:Lcta;

    .line 102
    return-void
.end method

.method public static a(Lcsd;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lcom/twitter/android/people/z;",
            ">;",
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/common/list/l$d;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/k;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/ai;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/m;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/e;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/r;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/v;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/g;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/a;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/k;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/t;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/adapters/viewbinders/i;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Lcom/twitter/android/people/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    new-instance v0, Lcom/twitter/android/people/aa;

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lcom/twitter/android/people/aa;-><init>(Lcsd;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/android/people/z;
    .locals 17

    .prologue
    .line 106
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/people/aa;->b:Lcsd;

    move-object/from16 v16, v0

    new-instance v1, Lcom/twitter/android/people/z;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/people/aa;->c:Lcta;

    .line 109
    invoke-interface {v2}, Lcta;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/people/aa;->d:Lcta;

    .line 110
    invoke-interface {v3}, Lcta;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/people/aa;->e:Lcta;

    .line 111
    invoke-interface {v4}, Lcta;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/app/common/list/l$d;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/people/aa;->f:Lcta;

    .line 112
    invoke-interface {v5}, Lcta;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/android/people/k;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/people/aa;->g:Lcta;

    .line 113
    invoke-interface {v6}, Lcta;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/people/ai;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/people/aa;->h:Lcta;

    .line 114
    invoke-interface {v7}, Lcta;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/twitter/android/people/adapters/viewbinders/m;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/people/aa;->i:Lcta;

    .line 115
    invoke-interface {v8}, Lcta;->b()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/android/people/adapters/viewbinders/e;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/people/aa;->j:Lcta;

    .line 116
    invoke-interface {v9}, Lcta;->b()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/twitter/android/people/adapters/viewbinders/r;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/people/aa;->k:Lcta;

    .line 117
    invoke-interface {v10}, Lcta;->b()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/twitter/android/people/adapters/viewbinders/v;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/people/aa;->l:Lcta;

    .line 118
    invoke-interface {v11}, Lcta;->b()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/twitter/android/people/adapters/viewbinders/g;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/twitter/android/people/aa;->m:Lcta;

    .line 119
    invoke-interface {v12}, Lcta;->b()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/twitter/android/people/adapters/viewbinders/a;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/twitter/android/people/aa;->n:Lcta;

    .line 120
    invoke-interface {v13}, Lcta;->b()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/twitter/android/people/adapters/viewbinders/k;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/twitter/android/people/aa;->o:Lcta;

    .line 121
    invoke-interface {v14}, Lcta;->b()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/twitter/android/people/adapters/viewbinders/t;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/twitter/android/people/aa;->p:Lcta;

    .line 122
    invoke-interface {v15}, Lcta;->b()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/twitter/android/people/adapters/viewbinders/i;

    invoke-direct/range {v1 .. v15}, Lcom/twitter/android/people/z;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/twitter/app/common/list/l$d;Lcom/twitter/android/people/k;Lcom/twitter/android/people/ai;Lcom/twitter/android/people/adapters/viewbinders/m;Lcom/twitter/android/people/adapters/viewbinders/e;Lcom/twitter/android/people/adapters/viewbinders/r;Lcom/twitter/android/people/adapters/viewbinders/v;Lcom/twitter/android/people/adapters/viewbinders/g;Lcom/twitter/android/people/adapters/viewbinders/a;Lcom/twitter/android/people/adapters/viewbinders/k;Lcom/twitter/android/people/adapters/viewbinders/t;Lcom/twitter/android/people/adapters/viewbinders/i;)V

    .line 106
    move-object/from16 v0, v16

    invoke-static {v0, v1}, Ldagger/internal/MembersInjectors;->a(Lcsd;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/people/z;

    return-object v1
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/twitter/android/people/aa;->a()Lcom/twitter/android/people/z;

    move-result-object v0

    return-object v0
.end method
