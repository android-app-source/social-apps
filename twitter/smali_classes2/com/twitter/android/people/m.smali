.class public final Lcom/twitter/android/people/m;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lcom/twitter/android/people/l;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lbsv;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/i;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/model/util/FriendshipCache;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/twitter/android/people/m;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/people/m;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcta;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lbsv;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/i;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/model/util/FriendshipCache;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    sget-boolean v0, Lcom/twitter/android/people/m;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 25
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/people/m;->b:Lcta;

    .line 26
    sget-boolean v0, Lcom/twitter/android/people/m;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 27
    :cond_1
    iput-object p2, p0, Lcom/twitter/android/people/m;->c:Lcta;

    .line 28
    sget-boolean v0, Lcom/twitter/android/people/m;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 29
    :cond_2
    iput-object p3, p0, Lcom/twitter/android/people/m;->d:Lcta;

    .line 30
    return-void
.end method

.method public static a(Lcta;Lcta;Lcta;)Ldagger/internal/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lbsv;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/people/i;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/model/util/FriendshipCache;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Lcom/twitter/android/people/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    new-instance v0, Lcom/twitter/android/people/m;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/android/people/m;-><init>(Lcta;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/android/people/l;
    .locals 4

    .prologue
    .line 34
    new-instance v3, Lcom/twitter/android/people/l;

    iget-object v0, p0, Lcom/twitter/android/people/m;->b:Lcta;

    .line 35
    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsv;

    iget-object v1, p0, Lcom/twitter/android/people/m;->c:Lcta;

    .line 36
    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/people/i;

    iget-object v2, p0, Lcom/twitter/android/people/m;->d:Lcta;

    .line 37
    invoke-interface {v2}, Lcta;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v3, v0, v1, v2}, Lcom/twitter/android/people/l;-><init>(Lbsv;Lcom/twitter/android/people/i;Lcom/twitter/model/util/FriendshipCache;)V

    .line 34
    return-object v3
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/twitter/android/people/m;->a()Lcom/twitter/android/people/l;

    move-result-object v0

    return-object v0
.end method
