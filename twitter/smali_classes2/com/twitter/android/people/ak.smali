.class public Lcom/twitter/android/people/ak;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/people/aj;


# instance fields
.field private a:Lcom/twitter/android/search/SearchSuggestionController;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/people/ak;->a:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-virtual {v0}, Lcom/twitter/android/search/SearchSuggestionController;->f()Z

    .line 32
    return-void
.end method

.method public a(Lcom/twitter/android/search/SearchSuggestionController;)V
    .locals 2

    .prologue
    .line 23
    iput-object p1, p0, Lcom/twitter/android/people/ak;->a:Lcom/twitter/android/search/SearchSuggestionController;

    .line 24
    iget-object v0, p0, Lcom/twitter/android/people/ak;->a:Lcom/twitter/android/search/SearchSuggestionController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/search/SearchSuggestionController;->a(I)V

    .line 25
    iget-object v0, p0, Lcom/twitter/android/people/ak;->a:Lcom/twitter/android/search/SearchSuggestionController;

    const-string/jumbo v1, "people"

    invoke-virtual {v0, v1}, Lcom/twitter/android/search/SearchSuggestionController;->a(Ljava/lang/String;)Lcom/twitter/android/search/SearchSuggestionController;

    .line 26
    iget-object v0, p0, Lcom/twitter/android/people/ak;->a:Lcom/twitter/android/search/SearchSuggestionController;

    const v1, 0x7f0a07d8

    invoke-virtual {v0, v1}, Lcom/twitter/android/search/SearchSuggestionController;->c(I)V

    .line 27
    return-void
.end method

.method public a(Lcom/twitter/app/common/di/scope/InjectionScope;)V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/twitter/app/common/di/scope/InjectionScope;->e:Lcom/twitter/app/common/di/scope/InjectionScope;

    if-ne p1, v0, :cond_0

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/people/ak;->a:Lcom/twitter/android/search/SearchSuggestionController;

    .line 39
    :cond_0
    return-void
.end method
