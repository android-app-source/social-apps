.class public abstract Lcom/twitter/android/people/g;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method static a(Lawb;)Lcom/twitter/database/lru/l;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lawb;",
            ")",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/people/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    invoke-static {}, Lcom/twitter/database/lru/e$a;->a()Lcom/twitter/database/lru/e$a;

    move-result-object v0

    const-string/jumbo v1, "ModuleCache"

    .line 29
    invoke-virtual {v0, v1}, Lcom/twitter/database/lru/e$a;->a(Ljava/lang/String;)Lcom/twitter/database/lru/e$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/people/j;->a:Lcom/twitter/util/serialization/l;

    .line 30
    invoke-virtual {v0, v1}, Lcom/twitter/database/lru/e$a;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/database/lru/e$a;

    move-result-object v0

    new-instance v1, Lcom/twitter/database/lru/k;

    new-instance v2, Lcom/twitter/database/lru/LruPolicy;

    sget-object v3, Lcom/twitter/database/lru/LruPolicy$Type;->a:Lcom/twitter/database/lru/LruPolicy$Type;

    const/16 v4, 0xa

    invoke-direct {v2, v3, v4}, Lcom/twitter/database/lru/LruPolicy;-><init>(Lcom/twitter/database/lru/LruPolicy$Type;I)V

    const-wide/16 v4, 0x0

    invoke-direct {v1, v2, v4, v5}, Lcom/twitter/database/lru/k;-><init>(Lcom/twitter/database/lru/LruPolicy;J)V

    .line 31
    invoke-virtual {v0, v1}, Lcom/twitter/database/lru/e$a;->a(Lcom/twitter/database/lru/k;)Lcom/twitter/database/lru/e$a;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lcom/twitter/database/lru/e$a;->c()Lcom/twitter/database/lru/e;

    move-result-object v0

    .line 28
    invoke-virtual {p0, v0}, Lawb;->a(Lcom/twitter/database/lru/e;)Lcom/twitter/database/lru/l;

    move-result-object v0

    return-object v0
.end method
