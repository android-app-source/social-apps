.class public final Lcom/twitter/android/people/e;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/people/am;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/people/e$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/ak;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/people/aj;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/twitter/android/people/e;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/people/e;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/twitter/android/people/e$a;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    sget-boolean v0, Lcom/twitter/android/people/e;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/android/people/e;->a(Lcom/twitter/android/people/e$a;)V

    .line 37
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/people/e$a;Lcom/twitter/android/people/e$1;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/twitter/android/people/e;-><init>(Lcom/twitter/android/people/e$a;)V

    return-void
.end method

.method public static a()Lcom/twitter/android/people/e$a;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lcom/twitter/android/people/e$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/people/e$a;-><init>(Lcom/twitter/android/people/e$1;)V

    return-object v0
.end method

.method private a(Lcom/twitter/android/people/e$a;)V
    .locals 2

    .prologue
    .line 46
    invoke-static {}, Lcom/twitter/android/people/al;->c()Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/e;->b:Lcta;

    .line 48
    iget-object v0, p0, Lcom/twitter/android/people/e;->b:Lcta;

    .line 49
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/e;->c:Lcta;

    .line 51
    iget-object v0, p0, Lcom/twitter/android/people/e;->c:Lcta;

    .line 52
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/e;->d:Lcta;

    .line 54
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 55
    invoke-static {v0, v1}, Ldagger/internal/f;->a(II)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/people/e;->d:Lcta;

    .line 56
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Ldagger/internal/f$a;->a()Ldagger/internal/f;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/e;->e:Lcta;

    .line 59
    new-instance v0, Lcom/twitter/android/people/e$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/people/e$1;-><init>(Lcom/twitter/android/people/e;Lcom/twitter/android/people/e$a;)V

    iput-object v0, p0, Lcom/twitter/android/people/e;->f:Lcta;

    .line 72
    iget-object v0, p0, Lcom/twitter/android/people/e;->f:Lcta;

    .line 74
    invoke-static {v0}, Lcom/twitter/app/common/abs/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 73
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/e;->g:Lcta;

    .line 75
    return-void
.end method


# virtual methods
.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/people/e;->e:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c()Lcom/twitter/app/common/abs/j;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/android/people/e;->g:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    return-object v0
.end method

.method public d()Lcom/twitter/android/people/aj;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/twitter/android/people/e;->c:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/aj;

    return-object v0
.end method
