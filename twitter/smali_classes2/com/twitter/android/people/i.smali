.class public Lcom/twitter/android/people/i;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final a:Laem;

.field private final b:Lrx/subjects/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/b",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lbsv;

.field private final d:Lcom/twitter/android/people/adapters/d;


# direct methods
.method public constructor <init>(Lbsv;Laem;Lcom/twitter/android/people/adapters/d;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p2, p0, Lcom/twitter/android/people/i;->a:Laem;

    .line 40
    iput-object p1, p0, Lcom/twitter/android/people/i;->c:Lbsv;

    .line 41
    iput-object p3, p0, Lcom/twitter/android/people/i;->d:Lcom/twitter/android/people/adapters/d;

    .line 42
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/Void;

    invoke-static {v0}, Lrx/subjects/b;->e(Ljava/lang/Object;)Lrx/subjects/b;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/i;->b:Lrx/subjects/b;

    .line 43
    return-void
.end method

.method private a(Lcom/twitter/android/people/adapters/b;)Lcom/twitter/android/people/adapters/b;
    .locals 1

    .prologue
    .line 83
    instance-of v0, p1, Lcom/twitter/android/people/adapters/b$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/people/i;->c:Lbsv;

    .line 84
    invoke-virtual {v0}, Lbsv;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    check-cast p1, Lcom/twitter/android/people/adapters/b$a;

    invoke-virtual {p1}, Lcom/twitter/android/people/adapters/b$a;->a()Lcom/twitter/android/people/adapters/b$a;

    move-result-object p1

    .line 87
    :cond_0
    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/people/i;Lcom/twitter/android/people/adapters/b;)Lcom/twitter/android/people/adapters/b;
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/twitter/android/people/i;->a(Lcom/twitter/android/people/adapters/b;)Lcom/twitter/android/people/adapters/b;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/people/i;)Lcom/twitter/android/people/adapters/d;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/people/i;->d:Lcom/twitter/android/people/adapters/d;

    return-object v0
.end method


# virtual methods
.method public a()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/android/people/adapters/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/people/i;->a(Ljava/util/Map;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Map;)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lrx/c",
            "<",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/android/people/adapters/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/people/i;->a:Laem;

    invoke-virtual {v0, p1}, Laem;->a(Ljava/util/Map;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/people/i$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/people/i$1;-><init>(Lcom/twitter/android/people/i;)V

    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 55
    iget-object v1, p0, Lcom/twitter/android/people/i;->b:Lrx/subjects/b;

    .line 56
    invoke-static {}, Lcre;->a()Lrx/functions/e;

    move-result-object v2

    .line 55
    invoke-static {v0, v1, v2}, Lrx/c;->a(Lrx/c;Lrx/c;Lrx/functions/e;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/people/i$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/people/i$2;-><init>(Lcom/twitter/android/people/i;)V

    .line 57
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 55
    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/android/people/i;->b:Lrx/subjects/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lrx/subjects/b;->a(Ljava/lang/Object;)V

    .line 79
    return-void
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/twitter/android/people/i;->a:Laem;

    invoke-virtual {v0}, Laem;->close()V

    .line 94
    return-void
.end method
