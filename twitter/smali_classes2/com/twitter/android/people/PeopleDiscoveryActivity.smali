.class public Lcom/twitter/android/people/PeopleDiscoveryActivity;
.super Lcom/twitter/android/ListFragmentActivity;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/android/people/aj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/twitter/android/ListFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 96
    if-eqz v0, :cond_0

    .line 97
    invoke-virtual {v0, p1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 99
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()Z
    .locals 3

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/twitter/android/people/PeopleDiscoveryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "is_internal"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected a(Landroid/content/Intent;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/android/ListFragmentActivity$a;
    .locals 3

    .prologue
    .line 84
    new-instance v0, Lcom/twitter/android/people/PeopleDiscoveryListFragment;

    invoke-direct {v0}, Lcom/twitter/android/people/PeopleDiscoveryListFragment;-><init>()V

    .line 85
    invoke-virtual {v0}, Lcom/twitter/android/people/PeopleDiscoveryListFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/app/common/list/i;->h()Lcom/twitter/app/common/list/i$a;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/app/common/list/i$a;->e(Z)Lcom/twitter/app/common/list/i$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/app/common/list/i$a;->b()Lcom/twitter/app/common/list/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/people/PeopleDiscoveryListFragment;->a(Lcom/twitter/app/common/base/b;)V

    .line 86
    new-instance v1, Lcom/twitter/android/ListFragmentActivity$a;

    invoke-direct {v1, v0}, Lcom/twitter/android/ListFragmentActivity$a;-><init>(Lcom/twitter/app/common/list/TwitterListFragment;)V

    return-object v1
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    const v0, 0x7f040287

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 31
    const/16 v0, 0xe

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(I)V

    .line 33
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(I)V

    .line 34
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(Z)V

    .line 35
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->b(Z)V

    .line 36
    return-object p2
.end method

.method protected a(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 68
    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lcom/twitter/android/people/PeopleDiscoveryActivity;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 69
    invoke-direct {p0}, Lcom/twitter/android/people/PeopleDiscoveryActivity;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 70
    :cond_0
    const v0, 0x7f0a0637

    invoke-virtual {p0, v0}, Lcom/twitter/android/people/PeopleDiscoveryActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 72
    :cond_1
    return-object v0
.end method

.method public a(Lcmm;)Z
    .locals 2

    .prologue
    .line 57
    const v0, 0x7f13088d

    invoke-interface {p1}, Lcmm;->a()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 58
    iget-object v0, p0, Lcom/twitter/android/people/PeopleDiscoveryActivity;->a:Lcom/twitter/android/people/aj;

    invoke-interface {v0}, Lcom/twitter/android/people/aj;->a()V

    .line 59
    const/4 v0, 0x1

    .line 61
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/ListFragmentActivity;->a(Lcmm;)Z

    move-result v0

    goto :goto_0
.end method

.method protected b(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/twitter/android/people/PeopleDiscoveryActivity;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "subtitle"

    invoke-static {p1, v0}, Lcom/twitter/android/people/PeopleDiscoveryActivity;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ListFragmentActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 42
    invoke-virtual {p0}, Lcom/twitter/android/people/PeopleDiscoveryActivity;->X()Lann;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/people/am;

    .line 43
    invoke-interface {v0}, Lcom/twitter/android/people/am;->d()Lcom/twitter/android/people/aj;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/people/PeopleDiscoveryActivity;->a:Lcom/twitter/android/people/aj;

    .line 44
    iget-object v0, p0, Lcom/twitter/android/people/PeopleDiscoveryActivity;->a:Lcom/twitter/android/people/aj;

    invoke-virtual {p0}, Lcom/twitter/android/people/PeopleDiscoveryActivity;->P()Lcom/twitter/android/search/SearchSuggestionController;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/people/aj;->a(Lcom/twitter/android/search/SearchSuggestionController;)V

    .line 45
    return-void
.end method

.method protected d(Lank;)Lcom/twitter/android/people/am;
    .locals 2

    .prologue
    .line 50
    invoke-static {}, Lcom/twitter/android/people/e;->a()Lcom/twitter/android/people/e$a;

    move-result-object v0

    .line 51
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/people/e$a;->a(Lamu;)Lcom/twitter/android/people/e$a;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/twitter/android/people/e$a;->a()Lcom/twitter/android/people/am;

    move-result-object v0

    .line 50
    return-object v0
.end method

.method protected synthetic e(Lank;)Lcom/twitter/app/common/base/i;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/twitter/android/people/PeopleDiscoveryActivity;->d(Lank;)Lcom/twitter/android/people/am;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f(Lank;)Lcom/twitter/app/common/abs/a;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/twitter/android/people/PeopleDiscoveryActivity;->d(Lank;)Lcom/twitter/android/people/am;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic g(Lank;)Lann;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/twitter/android/people/PeopleDiscoveryActivity;->d(Lank;)Lcom/twitter/android/people/am;

    move-result-object v0

    return-object v0
.end method
