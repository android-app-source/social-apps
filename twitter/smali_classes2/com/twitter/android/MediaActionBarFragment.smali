.class public Lcom/twitter/android/MediaActionBarFragment;
.super Lcom/twitter/app/common/base/BaseFragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/bm$b;


# instance fields
.field private a:Lcom/twitter/android/widget/EngagementActionBar;

.field private b:Landroid/widget/TextView;

.field private c:Lcom/twitter/android/widget/ToggleImageButton;

.field private d:Landroid/widget/TextView;

.field private e:Lcom/twitter/android/widget/ToggleImageButton;

.field private f:Landroid/widget/TextView;

.field private g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private h:Lcom/twitter/library/client/v;

.field private i:Lcom/twitter/model/core/Tweet;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

.field private n:Landroid/content/Context;

.field private o:Z

.field private p:Lcom/twitter/library/client/p;

.field private q:Lbpl;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/twitter/app/common/base/BaseFragment;-><init>()V

    return-void
.end method

.method protected static a(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/model/core/Tweet;I)Landroid/app/Dialog;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 414
    packed-switch p2, :pswitch_data_0

    .line 457
    :goto_0
    return-object v0

    .line 416
    :pswitch_0
    new-instance v1, Lcom/twitter/android/MediaActionBarFragment$3;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/MediaActionBarFragment$3;-><init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/model/core/Tweet;)V

    .line 449
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0a0993

    .line 450
    invoke-virtual {p0, v3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a0992

    .line 451
    invoke-virtual {p0, v3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a0a40

    .line 452
    invoke-virtual {p0, v3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a05e0

    .line 453
    invoke-virtual {p0, v2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 454
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 414
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lcom/twitter/app/common/base/TwitterFragmentActivity;ILcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/android/MediaActionBarFragment;
    .locals 3

    .prologue
    .line 132
    .line 133
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 134
    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/MediaActionBarFragment;

    .line 135
    if-nez v0, :cond_0

    .line 136
    new-instance v0, Lcom/twitter/android/MediaActionBarFragment;

    invoke-direct {v0}, Lcom/twitter/android/MediaActionBarFragment;-><init>()V

    .line 137
    new-instance v1, Lcom/twitter/app/common/base/b$a;

    invoke-direct {v1}, Lcom/twitter/app/common/base/b$a;-><init>()V

    const-string/jumbo v2, "association"

    .line 138
    invoke-virtual {v1, v2, p2}, Lcom/twitter/app/common/base/b$a;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/app/common/base/b$a;

    move-result-object v1

    const-string/jumbo v2, "page"

    .line 139
    invoke-virtual {v1, v2, p3}, Lcom/twitter/app/common/base/b$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/common/base/b$a;

    move-result-object v1

    const-string/jumbo v2, "section"

    .line 140
    invoke-virtual {v1, v2, p4}, Lcom/twitter/app/common/base/b$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/common/base/b$a;

    move-result-object v1

    const-string/jumbo v2, "component"

    .line 141
    invoke-virtual {v1, v2, p5}, Lcom/twitter/app/common/base/b$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/common/base/b$a;

    move-result-object v1

    .line 142
    invoke-virtual {v1}, Lcom/twitter/app/common/base/b$a;->c()Lcom/twitter/app/common/base/b;

    move-result-object v1

    .line 137
    invoke-virtual {v0, v1}, Lcom/twitter/android/MediaActionBarFragment;->a(Lcom/twitter/app/common/base/b;)V

    .line 143
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 144
    invoke-virtual {v1, p1, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 145
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 147
    :cond_0
    return-object v0
.end method

.method private a(ILandroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;)V
    .locals 2

    .prologue
    .line 493
    sparse-switch p1, :sswitch_data_0

    .line 518
    :goto_0
    return-void

    .line 495
    :sswitch_0
    const/4 v0, 0x3

    .line 517
    :goto_1
    invoke-virtual {p3}, Lcom/twitter/model/core/Tweet;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v0, v1}, Lcom/twitter/android/al;->a(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V

    goto :goto_0

    .line 499
    :sswitch_1
    const/4 v0, 0x1

    .line 500
    iget-object v1, p0, Lcom/twitter/android/MediaActionBarFragment;->c:Lcom/twitter/android/widget/ToggleImageButton;

    invoke-virtual {v1}, Lcom/twitter/android/widget/ToggleImageButton;->a()V

    goto :goto_1

    .line 504
    :sswitch_2
    const/4 v0, 0x2

    .line 505
    goto :goto_1

    .line 508
    :sswitch_3
    const/4 v0, 0x0

    invoke-static {p2, p3, v0}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Z)V

    .line 509
    const-string/jumbo v0, "share"

    invoke-direct {p0, v0, p3}, Lcom/twitter/android/MediaActionBarFragment;->a(Ljava/lang/String;Lcom/twitter/model/core/Tweet;)V

    goto :goto_0

    .line 493
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f130032 -> :sswitch_1
        0x7f13006e -> :sswitch_0
        0x7f130071 -> :sswitch_2
        0x7f13014a -> :sswitch_3
    .end sparse-switch
.end method

.method static synthetic a(Lcom/twitter/android/MediaActionBarFragment;Lcom/twitter/model/core/Tweet;ZI)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/MediaActionBarFragment;->a(Lcom/twitter/model/core/Tweet;ZI)V

    return-void
.end method

.method private a(Lcom/twitter/model/core/Tweet;Landroid/view/View;)V
    .locals 8

    .prologue
    .line 346
    iget-boolean v0, p1, Lcom/twitter/model/core/Tweet;->a:Z

    if-eqz v0, :cond_1

    .line 347
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/MediaActionBarFragment;->c(Lcom/twitter/model/core/Tweet;Z)V

    .line 348
    new-instance v1, Lbfp;

    iget-object v2, p0, Lcom/twitter/android/MediaActionBarFragment;->n:Landroid/content/Context;

    iget-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->h:Lcom/twitter/library/client/v;

    .line 349
    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, p1, Lcom/twitter/model/core/Tweet;->t:J

    iget-wide v6, p1, Lcom/twitter/model/core/Tweet;->u:J

    invoke-direct/range {v1 .. v7}, Lbfp;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V

    .line 351
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    .line 350
    invoke-virtual {v1, v0}, Lbfp;->a(Lcgi;)Lbfp;

    move-result-object v0

    .line 352
    iget-object v1, p0, Lcom/twitter/android/MediaActionBarFragment;->p:Lcom/twitter/library/client/p;

    new-instance v2, Lcom/twitter/android/MediaActionBarFragment$1;

    iget-object v3, p0, Lcom/twitter/android/MediaActionBarFragment;->n:Landroid/content/Context;

    invoke-direct {v2, p0, v3, p1}, Lcom/twitter/android/MediaActionBarFragment$1;-><init>(Lcom/twitter/android/MediaActionBarFragment;Landroid/content/Context;Lcom/twitter/model/core/Tweet;)V

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 362
    const-string/jumbo v0, "unfavorite"

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/MediaActionBarFragment;->a(Ljava/lang/String;Lcom/twitter/model/core/Tweet;)V

    .line 363
    iget-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->q:Lbpl;

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->q:Lbpl;

    invoke-virtual {v0, p2}, Lbpl;->c(Landroid/view/View;)V

    .line 388
    :cond_0
    :goto_0
    return-void

    .line 367
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/MediaActionBarFragment;->c(Lcom/twitter/model/core/Tweet;Z)V

    .line 368
    new-instance v1, Lbfm;

    iget-object v2, p0, Lcom/twitter/android/MediaActionBarFragment;->n:Landroid/content/Context;

    iget-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->h:Lcom/twitter/library/client/v;

    .line 369
    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, p1, Lcom/twitter/model/core/Tweet;->t:J

    iget-wide v6, p1, Lcom/twitter/model/core/Tweet;->u:J

    invoke-direct/range {v1 .. v7}, Lbfm;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V

    .line 371
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbfm;->a(Lcgi;)Lbfm;

    move-result-object v0

    .line 372
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->m()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbfm;->a(Ljava/lang/Boolean;)Lbfm;

    move-result-object v0

    .line 373
    iget-object v1, p0, Lcom/twitter/android/MediaActionBarFragment;->p:Lcom/twitter/library/client/p;

    new-instance v2, Lcom/twitter/android/MediaActionBarFragment$2;

    invoke-direct {v2, p0, p1}, Lcom/twitter/android/MediaActionBarFragment$2;-><init>(Lcom/twitter/android/MediaActionBarFragment;Lcom/twitter/model/core/Tweet;)V

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 383
    const-string/jumbo v0, "favorite"

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/MediaActionBarFragment;->a(Ljava/lang/String;Lcom/twitter/model/core/Tweet;)V

    .line 384
    iget-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->q:Lbpl;

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->q:Lbpl;

    invoke-virtual {v0, p2}, Lbpl;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/model/core/Tweet;ZI)V
    .locals 0

    .prologue
    .line 399
    iput-boolean p2, p1, Lcom/twitter/model/core/Tweet;->a:Z

    .line 400
    iput p3, p1, Lcom/twitter/model/core/Tweet;->n:I

    .line 401
    invoke-virtual {p0, p1}, Lcom/twitter/android/MediaActionBarFragment;->a(Lcom/twitter/model/core/Tweet;)V

    .line 402
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/twitter/model/core/Tweet;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 480
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/MediaActionBarFragment;->j:Ljava/lang/String;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/twitter/android/MediaActionBarFragment;->k:Ljava/lang/String;

    aput-object v1, v0, v5

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/twitter/android/MediaActionBarFragment;->l:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "tweet"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    aput-object p1, v0, v1

    .line 481
    invoke-static {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 483
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    .line 484
    iget-object v2, p0, Lcom/twitter/android/MediaActionBarFragment;->n:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v1, v2, p2, v3}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 485
    new-array v2, v5, [Ljava/lang/String;

    aput-object v0, v2, v4

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/MediaActionBarFragment;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 486
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/MediaActionBarFragment;->m:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 487
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 485
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 488
    return-void
.end method

.method static synthetic a(ZZLandroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 46
    invoke-static {p0, p1, p2, p3}, Lcom/twitter/android/MediaActionBarFragment;->b(ZZLandroid/content/Context;Z)V

    return-void
.end method

.method private b(Lcom/twitter/model/core/Tweet;)V
    .locals 8

    .prologue
    const v1, 0x7f110195

    .line 323
    invoke-virtual {p0}, Lcom/twitter/android/MediaActionBarFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 324
    iget v0, p1, Lcom/twitter/model/core/Tweet;->n:I

    .line 325
    iget v3, p1, Lcom/twitter/model/core/Tweet;->k:I

    .line 327
    iget-object v4, p0, Lcom/twitter/android/MediaActionBarFragment;->b:Landroid/widget/TextView;

    invoke-static {v4, p1}, Lcom/twitter/library/view/e;->a(Landroid/widget/TextView;Lcom/twitter/model/core/Tweet;)V

    .line 329
    iget-object v4, p0, Lcom/twitter/android/MediaActionBarFragment;->d:Landroid/widget/TextView;

    if-lez v0, :cond_1

    int-to-long v6, v0

    .line 330
    invoke-static {v2, v6, v7}, Lcom/twitter/util/r;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v0

    .line 329
    :goto_0
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 331
    iget-object v4, p0, Lcom/twitter/android/MediaActionBarFragment;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->i:Lcom/twitter/model/core/Tweet;

    iget-boolean v0, v0, Lcom/twitter/model/core/Tweet;->a:Z

    if-eqz v0, :cond_2

    const v0, 0x7f1100e4

    :goto_1
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 333
    iget-object v4, p0, Lcom/twitter/android/MediaActionBarFragment;->f:Landroid/widget/TextView;

    if-lez v3, :cond_3

    int-to-long v6, v3

    .line 334
    invoke-static {v2, v6, v7}, Lcom/twitter/util/r;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v0

    .line 333
    :goto_2
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 335
    iget-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->f:Landroid/widget/TextView;

    iget-boolean v3, p1, Lcom/twitter/model/core/Tweet;->c:Z

    if-eqz v3, :cond_0

    const v1, 0x7f1100de

    :cond_0
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 337
    return-void

    .line 330
    :cond_1
    const-string/jumbo v0, ""

    goto :goto_0

    :cond_2
    move v0, v1

    .line 331
    goto :goto_1

    .line 334
    :cond_3
    const-string/jumbo v0, ""

    goto :goto_2
.end method

.method private static b(ZZLandroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 464
    if-nez p0, :cond_0

    if-nez p3, :cond_0

    .line 465
    if-eqz p1, :cond_1

    const v0, 0x7f0a0994

    .line 467
    :goto_0
    const/4 v1, 0x1

    invoke-static {p2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 469
    :cond_0
    return-void

    .line 465
    :cond_1
    const v0, 0x7f0a099d

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 167
    const v0, 0x7f0401a0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(JLcom/twitter/model/core/Tweet;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 236
    invoke-virtual {p0}, Lcom/twitter/android/MediaActionBarFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    if-eqz p4, :cond_1

    .line 238
    const-string/jumbo v0, "unretweet"

    iget-object v1, p0, Lcom/twitter/android/MediaActionBarFragment;->i:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/MediaActionBarFragment;->a(Ljava/lang/String;Lcom/twitter/model/core/Tweet;)V

    .line 239
    iget-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->e:Lcom/twitter/android/widget/ToggleImageButton;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/ToggleImageButton;->setToggledOn(Z)V

    .line 240
    iget-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->i:Lcom/twitter/model/core/Tweet;

    iput-boolean v2, v0, Lcom/twitter/model/core/Tweet;->c:Z

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 242
    :cond_1
    const-string/jumbo v0, "retweet"

    iget-object v1, p0, Lcom/twitter/android/MediaActionBarFragment;->i:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/MediaActionBarFragment;->a(Ljava/lang/String;Lcom/twitter/model/core/Tweet;)V

    .line 243
    iget-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->e:Lcom/twitter/android/widget/ToggleImageButton;

    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/ToggleImageButton;->setToggledOn(Z)V

    .line 244
    iget-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->i:Lcom/twitter/model/core/Tweet;

    iput-boolean v3, v0, Lcom/twitter/model/core/Tweet;->c:Z

    goto :goto_0
.end method

.method public a(JZZZ)V
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->n:Landroid/content/Context;

    invoke-static {p3, p4, v0, p5}, Lcom/twitter/android/MediaActionBarFragment;->b(ZZLandroid/content/Context;Z)V

    .line 253
    return-void
.end method

.method public a(Lbpl;)V
    .locals 0

    .prologue
    .line 521
    iput-object p1, p0, Lcom/twitter/android/MediaActionBarFragment;->q:Lbpl;

    .line 522
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;)V
    .locals 1

    .prologue
    .line 308
    iput-object p1, p0, Lcom/twitter/android/MediaActionBarFragment;->i:Lcom/twitter/model/core/Tweet;

    .line 309
    iget-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->a:Lcom/twitter/android/widget/EngagementActionBar;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/EngagementActionBar;->setTweet(Lcom/twitter/model/core/Tweet;)V

    .line 310
    iget-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->a:Lcom/twitter/android/widget/EngagementActionBar;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/EngagementActionBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 311
    invoke-direct {p0, p1}, Lcom/twitter/android/MediaActionBarFragment;->b(Lcom/twitter/model/core/Tweet;)V

    .line 312
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Z)V
    .locals 4

    .prologue
    .line 264
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/MediaActionBarFragment;->j:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/MediaActionBarFragment;->k:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "retweet_dialog::dismiss"

    aput-object v3, v1, v2

    .line 265
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 264
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 266
    return-void
.end method

.method public b(JLcom/twitter/model/core/Tweet;Z)V
    .locals 2

    .prologue
    .line 258
    const-string/jumbo v0, "quote"

    iget-object v1, p0, Lcom/twitter/android/MediaActionBarFragment;->i:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/MediaActionBarFragment;->a(Ljava/lang/String;Lcom/twitter/model/core/Tweet;)V

    .line 259
    return-void
.end method

.method public b(Lcom/twitter/model/core/Tweet;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 271
    iget-boolean v0, p0, Lcom/twitter/android/MediaActionBarFragment;->o:Z

    if-nez v0, :cond_0

    .line 272
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/MediaActionBarFragment;->j:Ljava/lang/String;

    aput-object v3, v1, v2

    iget-object v2, p0, Lcom/twitter/android/MediaActionBarFragment;->k:Ljava/lang/String;

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string/jumbo v3, "retweet_dialog::impression"

    aput-object v3, v1, v2

    .line 273
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 272
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 274
    iput-boolean v4, p0, Lcom/twitter/android/MediaActionBarFragment;->o:Z

    .line 276
    :cond_0
    return-void
.end method

.method c(Lcom/twitter/model/core/Tweet;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 391
    if-eqz p2, :cond_0

    .line 392
    const/4 v0, 0x1

    iget v1, p1, Lcom/twitter/model/core/Tweet;->n:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/MediaActionBarFragment;->a(Lcom/twitter/model/core/Tweet;ZI)V

    .line 396
    :goto_0
    return-void

    .line 394
    :cond_0
    iget v0, p1, Lcom/twitter/model/core/Tweet;->n:I

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, p1, v1, v0}, Lcom/twitter/android/MediaActionBarFragment;->a(Lcom/twitter/model/core/Tweet;ZI)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 204
    iget-object v1, p0, Lcom/twitter/android/MediaActionBarFragment;->i:Lcom/twitter/model/core/Tweet;

    .line 205
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 206
    invoke-virtual {p0}, Lcom/twitter/android/MediaActionBarFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/base/TwitterFragmentActivity;

    .line 207
    iget-object v3, p0, Lcom/twitter/android/MediaActionBarFragment;->h:Lcom/twitter/library/client/v;

    .line 208
    invoke-virtual {v3}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 209
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 210
    invoke-direct {p0, v2, v0, v1}, Lcom/twitter/android/MediaActionBarFragment;->a(ILandroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;)V

    .line 231
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    const v4, 0x7f13006e

    if-ne v2, v4, :cond_2

    .line 213
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v2

    .line 214
    invoke-virtual {v2, v1}, Lcom/twitter/android/composer/a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/composer/a;

    move-result-object v2

    .line 215
    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/composer/a;->b(Ljava/lang/String;)Lcom/twitter/android/composer/a;

    move-result-object v2

    .line 216
    invoke-virtual {v2, v0}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 213
    invoke-virtual {v0, v2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 217
    const-string/jumbo v0, "reply"

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/MediaActionBarFragment;->a(Ljava/lang/String;Lcom/twitter/model/core/Tweet;)V

    goto :goto_0

    .line 218
    :cond_2
    const v3, 0x7f130032

    if-ne v2, v3, :cond_3

    .line 219
    invoke-direct {p0, v1, p1}, Lcom/twitter/android/MediaActionBarFragment;->a(Lcom/twitter/model/core/Tweet;Landroid/view/View;)V

    goto :goto_0

    .line 220
    :cond_3
    const v3, 0x7f130071

    if-ne v2, v3, :cond_4

    .line 221
    new-instance v2, Lcom/twitter/android/bm$a;

    invoke-direct {v2, v0, v1}, Lcom/twitter/android/bm$a;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;)V

    .line 222
    invoke-virtual {v2, p0}, Lcom/twitter/android/bm$a;->a(Lcom/twitter/android/bm$b;)Lcom/twitter/android/bm$a;

    move-result-object v0

    .line 223
    invoke-virtual {v0, p0}, Lcom/twitter/android/bm$a;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/android/bm$a;

    move-result-object v0

    .line 224
    invoke-virtual {v0}, Lcom/twitter/android/bm$a;->a()Lcom/twitter/android/bm;

    move-result-object v0

    .line 225
    invoke-virtual {v0}, Lcom/twitter/android/bm;->a()V

    goto :goto_0

    .line 226
    :cond_4
    const v3, 0x7f13014a

    if-ne v2, v3, :cond_0

    .line 227
    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Z)V

    .line 228
    const-string/jumbo v0, "share"

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/MediaActionBarFragment;->a(Ljava/lang/String;Lcom/twitter/model/core/Tweet;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 107
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 109
    invoke-virtual {p0}, Lcom/twitter/android/MediaActionBarFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->n:Landroid/content/Context;

    .line 111
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->h:Lcom/twitter/library/client/v;

    .line 112
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->p:Lcom/twitter/library/client/p;

    .line 114
    invoke-virtual {p0}, Lcom/twitter/android/MediaActionBarFragment;->I()Lcom/twitter/app/common/base/b;

    move-result-object v1

    .line 115
    const-string/jumbo v0, "page"

    invoke-virtual {v1, v0}, Lcom/twitter/app/common/base/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->j:Ljava/lang/String;

    .line 116
    const-string/jumbo v0, "section"

    invoke-virtual {v1, v0}, Lcom/twitter/app/common/base/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->k:Ljava/lang/String;

    .line 117
    const-string/jumbo v0, "component"

    .line 118
    invoke-virtual {v1, v0}, Lcom/twitter/app/common/base/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->l:Ljava/lang/String;

    .line 119
    const-string/jumbo v0, "association"

    invoke-virtual {v1, v0}, Lcom/twitter/app/common/base/b;->h(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 120
    const-string/jumbo v0, "item"

    invoke-virtual {v1, v0}, Lcom/twitter/app/common/base/b;->h(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    iput-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->m:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 122
    if-eqz p1, :cond_0

    .line 123
    const-string/jumbo v0, "dialog_impression_scribed"

    .line 124
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/MediaActionBarFragment;->o:Z

    .line 126
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 291
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 292
    const-string/jumbo v0, "dialog_impression_scribed"

    iget-boolean v1, p0, Lcom/twitter/android/MediaActionBarFragment;->o:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 293
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 180
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 182
    const v0, 0x7f130145

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/EngagementActionBar;

    iput-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->a:Lcom/twitter/android/widget/EngagementActionBar;

    .line 184
    const v0, 0x7f130032

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ToggleImageButton;

    iput-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->c:Lcom/twitter/android/widget/ToggleImageButton;

    .line 185
    const v0, 0x7f130033

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->d:Landroid/widget/TextView;

    .line 187
    const v0, 0x7f130071

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ToggleImageButton;

    iput-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->e:Lcom/twitter/android/widget/ToggleImageButton;

    .line 188
    const v0, 0x7f130072

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->f:Landroid/widget/TextView;

    .line 190
    const v0, 0x7f13006f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->b:Landroid/widget/TextView;

    .line 191
    iget-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->i:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/twitter/android/MediaActionBarFragment;->i:Lcom/twitter/model/core/Tweet;

    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaActionBarFragment;->a(Lcom/twitter/model/core/Tweet;)V

    .line 194
    :cond_0
    return-void
.end method
