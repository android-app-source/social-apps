.class public Lcom/twitter/android/livevideo/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lala;
.implements Lang;


# annotations
.annotation build Lcom/twitter/app/AutoSaveState;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/livevideo/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lala;",
        "Lang",
        "<",
        "Lcom/twitter/app/common/util/StateSaver",
        "<",
        "Lcom/twitter/android/livevideo/a;",
        ">;>;"
    }
.end annotation


# instance fields
.field a:Lcom/twitter/model/livevideo/b;
    .annotation build Lcom/twitter/app/SaveState;
    .end annotation
.end field

.field b:Lcom/twitter/model/livevideo/d;
    .annotation build Lcom/twitter/app/SaveState;
    .end annotation
.end field

.field private c:Lcom/twitter/android/livevideo/a$a;

.field private final d:Lcwv;

.field private final e:Lcom/twitter/library/client/p;

.field private final f:Lcom/twitter/library/client/Session;

.field private final g:Ltz;

.field private final h:Lrx/f;

.field private final i:Lrx/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/d",
            "<",
            "Lcom/twitter/model/livevideo/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;Ltz;Lcom/twitter/app/common/util/StateSaver;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/client/Session;",
            "Lcom/twitter/library/client/p;",
            "Ltz;",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/livevideo/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 93
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/livevideo/a;-><init>(Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;Ltz;Lcom/twitter/app/common/util/StateSaver;Lrx/f;)V

    .line 94
    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;Ltz;Lcom/twitter/app/common/util/StateSaver;Lrx/f;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/client/Session;",
            "Lcom/twitter/library/client/p;",
            "Ltz;",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/livevideo/a;",
            ">;",
            "Lrx/f;",
            ")V"
        }
    .end annotation

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Lcwv;

    invoke-direct {v0}, Lcwv;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/livevideo/a;->d:Lcwv;

    .line 69
    new-instance v0, Lcom/twitter/android/livevideo/a$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/livevideo/a$1;-><init>(Lcom/twitter/android/livevideo/a;)V

    iput-object v0, p0, Lcom/twitter/android/livevideo/a;->i:Lrx/d;

    .line 102
    iput-object p1, p0, Lcom/twitter/android/livevideo/a;->f:Lcom/twitter/library/client/Session;

    .line 103
    iput-object p2, p0, Lcom/twitter/android/livevideo/a;->e:Lcom/twitter/library/client/p;

    .line 104
    iput-object p3, p0, Lcom/twitter/android/livevideo/a;->g:Ltz;

    .line 105
    iput-object p5, p0, Lcom/twitter/android/livevideo/a;->h:Lrx/f;

    .line 106
    invoke-virtual {p4, p0}, Lcom/twitter/app/common/util/StateSaver;->a(Ljava/lang/Object;)V

    .line 107
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/livevideo/a;Lcom/twitter/model/livevideo/c;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/twitter/android/livevideo/a;->a(Lcom/twitter/model/livevideo/c;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/livevideo/a;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/twitter/android/livevideo/a;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method private a(Lcom/twitter/model/livevideo/c;)V
    .locals 3

    .prologue
    .line 168
    iget-object v0, p0, Lcom/twitter/android/livevideo/a;->a:Lcom/twitter/model/livevideo/b;

    if-eqz v0, :cond_0

    .line 169
    new-instance v0, Lcom/twitter/model/livevideo/d$a;

    invoke-direct {v0}, Lcom/twitter/model/livevideo/d$a;-><init>()V

    invoke-virtual {v0, p1}, Lcom/twitter/model/livevideo/d$a;->a(Lcom/twitter/model/livevideo/c;)Lcom/twitter/model/livevideo/d$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/livevideo/d$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/livevideo/d;

    iput-object v0, p0, Lcom/twitter/android/livevideo/a;->b:Lcom/twitter/model/livevideo/d;

    .line 170
    iget-object v0, p0, Lcom/twitter/android/livevideo/a;->g:Ltz;

    iget-object v1, p0, Lcom/twitter/android/livevideo/a;->a:Lcom/twitter/model/livevideo/b;

    iget-object v2, p0, Lcom/twitter/android/livevideo/a;->b:Lcom/twitter/model/livevideo/d;

    invoke-interface {v0, v1, v2}, Ltz;->a(Lcom/twitter/model/livevideo/b;Lcom/twitter/model/livevideo/d;)Lcom/twitter/model/livevideo/d;

    .line 172
    :cond_0
    iget-boolean v0, p1, Lcom/twitter/model/livevideo/c;->c:Z

    invoke-direct {p0, v0}, Lcom/twitter/android/livevideo/a;->a(Z)V

    .line 173
    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 182
    const-string/jumbo v0, "LVSubscriptPresenter"

    const-string/jumbo v1, "Error when trying to update the reminder subscription"

    invoke-static {v0, v1, p1}, Lcqj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 183
    iget-object v0, p0, Lcom/twitter/android/livevideo/a;->c:Lcom/twitter/android/livevideo/a$a;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/twitter/android/livevideo/a;->c:Lcom/twitter/android/livevideo/a$a;

    invoke-interface {v0, p1}, Lcom/twitter/android/livevideo/a$a;->a(Ljava/lang/Throwable;)V

    .line 186
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/twitter/android/livevideo/a;->c:Lcom/twitter/android/livevideo/a$a;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/twitter/android/livevideo/a;->c:Lcom/twitter/android/livevideo/a$a;

    invoke-interface {v0, p1}, Lcom/twitter/android/livevideo/a$a;->a(Z)V

    .line 179
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 138
    iget-object v0, p0, Lcom/twitter/android/livevideo/a;->a:Lcom/twitter/model/livevideo/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/livevideo/a;->g:Ltz;

    iget-object v1, p0, Lcom/twitter/android/livevideo/a;->a:Lcom/twitter/model/livevideo/b;

    iget-wide v2, v1, Lcom/twitter/model/livevideo/b;->b:J

    invoke-interface {v0, v2, v3}, Ltz;->a(J)Lcom/twitter/model/livevideo/d;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/livevideo/a;->g:Ltz;

    iget-object v1, p0, Lcom/twitter/android/livevideo/a;->a:Lcom/twitter/model/livevideo/b;

    iget-wide v2, v1, Lcom/twitter/model/livevideo/b;->b:J

    invoke-interface {v0, v2, v3}, Ltz;->a(J)Lcom/twitter/model/livevideo/d;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/livevideo/d;->b:Lcom/twitter/model/livevideo/c;

    if-nez v0, :cond_1

    .line 139
    :cond_0
    new-instance v0, Lcom/twitter/library/util/InvalidDataException;

    const-string/jumbo v1, "Invalid event or subscriptions when remind me button was tapped"

    invoke-direct {v0, v1}, Lcom/twitter/library/util/InvalidDataException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 161
    :goto_0
    return-void

    .line 143
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/livevideo/a;->g:Ltz;

    iget-object v1, p0, Lcom/twitter/android/livevideo/a;->a:Lcom/twitter/model/livevideo/b;

    iget-wide v2, v1, Lcom/twitter/model/livevideo/b;->b:J

    invoke-interface {v0, v2, v3}, Ltz;->a(J)Lcom/twitter/model/livevideo/d;

    move-result-object v0

    iget-object v1, v0, Lcom/twitter/model/livevideo/d;->b:Lcom/twitter/model/livevideo/c;

    .line 146
    iget-boolean v0, v1, Lcom/twitter/model/livevideo/c;->c:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 149
    :goto_1
    iget-object v2, p0, Lcom/twitter/android/livevideo/a;->f:Lcom/twitter/library/client/Session;

    iget-object v3, p0, Lcom/twitter/android/livevideo/a;->a:Lcom/twitter/model/livevideo/b;

    iget-wide v4, v3, Lcom/twitter/model/livevideo/b;->b:J

    .line 152
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    iget-object v1, v1, Lcom/twitter/model/livevideo/c;->d:Ljava/lang/String;

    .line 149
    invoke-static {p1, v2, v3, v1, v0}, Lbde;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Z)Lbde;

    move-result-object v0

    .line 155
    invoke-virtual {v0}, Lbde;->e()Lrx/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/livevideo/a;->h:Lrx/f;

    .line 156
    invoke-virtual {v1, v2}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/livevideo/a;->i:Lrx/d;

    .line 157
    invoke-virtual {v1, v2}, Lrx/c;->a(Lrx/d;)Lrx/j;

    move-result-object v1

    .line 158
    iget-object v2, p0, Lcom/twitter/android/livevideo/a;->d:Lcwv;

    invoke-virtual {v2, v1}, Lcwv;->a(Lrx/j;)V

    .line 160
    iget-object v1, p0, Lcom/twitter/android/livevideo/a;->e:Lcom/twitter/library/client/p;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    goto :goto_0

    .line 146
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/a;->b()Lcom/twitter/app/common/util/StateSaver;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/util/StateSaver;->a(Landroid/os/Bundle;)V

    .line 165
    return-void
.end method

.method public a(Lcom/twitter/android/livevideo/a$a;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/twitter/android/livevideo/a;->c:Lcom/twitter/android/livevideo/a$a;

    .line 123
    return-void
.end method

.method public a(Lcom/twitter/app/common/di/scope/InjectionScope;)V
    .locals 1

    .prologue
    .line 190
    sget-object v0, Lcom/twitter/app/common/di/scope/InjectionScope;->c:Lcom/twitter/app/common/di/scope/InjectionScope;

    if-ne p1, v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/twitter/android/livevideo/a;->d:Lcwv;

    invoke-virtual {v0}, Lcwv;->B_()V

    .line 192
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/livevideo/a;->c:Lcom/twitter/android/livevideo/a$a;

    .line 194
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/livevideo/b;)V
    .locals 2

    .prologue
    .line 126
    iput-object p1, p0, Lcom/twitter/android/livevideo/a;->a:Lcom/twitter/model/livevideo/b;

    .line 127
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->m:Lcom/twitter/model/livevideo/d;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->m:Lcom/twitter/model/livevideo/d;

    iput-object v0, p0, Lcom/twitter/android/livevideo/a;->b:Lcom/twitter/model/livevideo/d;

    .line 130
    iget-object v0, p0, Lcom/twitter/android/livevideo/a;->g:Ltz;

    iget-object v1, p1, Lcom/twitter/model/livevideo/b;->m:Lcom/twitter/model/livevideo/d;

    invoke-interface {v0, p1, v1}, Ltz;->a(Lcom/twitter/model/livevideo/b;Lcom/twitter/model/livevideo/d;)Lcom/twitter/model/livevideo/d;

    .line 132
    :cond_0
    return-void
.end method

.method public al_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    const-string/jumbo v0, "presenter_live_video_event_subscription"

    return-object v0
.end method

.method public b()Lcom/twitter/app/common/util/StateSaver;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/android/livevideo/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    new-instance v0, Lcom/twitter/android/livevideo/LiveVideoEventSubscriptionPresenterSavedState;

    invoke-direct {v0, p0}, Lcom/twitter/android/livevideo/LiveVideoEventSubscriptionPresenterSavedState;-><init>(Lcom/twitter/android/livevideo/a;)V

    return-object v0
.end method

.method public synthetic c()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/a;->b()Lcom/twitter/app/common/util/StateSaver;

    move-result-object v0

    return-object v0
.end method
