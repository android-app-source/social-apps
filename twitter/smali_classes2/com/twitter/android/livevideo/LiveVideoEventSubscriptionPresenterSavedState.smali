.class public Lcom/twitter/android/livevideo/LiveVideoEventSubscriptionPresenterSavedState;
.super Lcom/twitter/app/common/util/BaseStateSaver;
.source "Twttr"


# annotations
.annotation build Lcod;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<OBJ:",
        "Lcom/twitter/android/livevideo/a;",
        ">",
        "Lcom/twitter/app/common/util/BaseStateSaver",
        "<TOBJ;>;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/twitter/android/livevideo/LiveVideoEventSubscriptionPresenterSavedState$1;

    invoke-direct {v0}, Lcom/twitter/android/livevideo/LiveVideoEventSubscriptionPresenterSavedState$1;-><init>()V

    sput-object v0, Lcom/twitter/android/livevideo/LiveVideoEventSubscriptionPresenterSavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/twitter/app/common/util/BaseStateSaver;-><init>(Landroid/os/Parcel;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Lcom/twitter/android/livevideo/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TOBJ;)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/twitter/app/common/util/BaseStateSaver;-><init>(Ljava/lang/Object;)V

    .line 34
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/android/livevideo/a;)Lcom/twitter/android/livevideo/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/n;",
            "TOBJ;)TOBJ;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/util/BaseStateSaver;->a(Lcom/twitter/util/serialization/n;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/livevideo/a;

    .line 51
    sget-object v1, Lcom/twitter/model/livevideo/b;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v1}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/livevideo/b;

    iput-object v1, v0, Lcom/twitter/android/livevideo/a;->a:Lcom/twitter/model/livevideo/b;

    .line 52
    sget-object v1, Lcom/twitter/model/livevideo/d;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v1}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/livevideo/d;

    iput-object v1, v0, Lcom/twitter/android/livevideo/a;->b:Lcom/twitter/model/livevideo/d;

    .line 53
    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 16
    check-cast p2, Lcom/twitter/android/livevideo/a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/livevideo/LiveVideoEventSubscriptionPresenterSavedState;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/android/livevideo/a;)Lcom/twitter/android/livevideo/a;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/android/livevideo/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/o;",
            "TOBJ;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/util/BaseStateSaver;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    .line 43
    iget-object v0, p2, Lcom/twitter/android/livevideo/a;->a:Lcom/twitter/model/livevideo/b;

    sget-object v1, Lcom/twitter/model/livevideo/b;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 44
    iget-object v0, p2, Lcom/twitter/android/livevideo/a;->b:Lcom/twitter/model/livevideo/d;

    sget-object v1, Lcom/twitter/model/livevideo/d;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 45
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16
    check-cast p2, Lcom/twitter/android/livevideo/a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/livevideo/LiveVideoEventSubscriptionPresenterSavedState;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/android/livevideo/a;)V

    return-void
.end method
