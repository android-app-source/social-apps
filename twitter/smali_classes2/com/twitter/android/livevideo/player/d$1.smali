.class Lcom/twitter/android/livevideo/player/d$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/livevideo/player/d;->a()Lrx/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/c$a",
        "<",
        "Lcom/twitter/android/livevideo/player/a$a;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/livevideo/player/d;


# direct methods
.method constructor <init>(Lcom/twitter/android/livevideo/player/d;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/twitter/android/livevideo/player/d$1;->a:Lcom/twitter/android/livevideo/player/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lrx/i;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-",
            "Lcom/twitter/android/livevideo/player/a$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/d$1;->a:Lcom/twitter/android/livevideo/player/d;

    invoke-static {v0}, Lcom/twitter/android/livevideo/player/d;->a(Lcom/twitter/android/livevideo/player/d;)Landroid/view/accessibility/CaptioningManager;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/livevideo/player/d$1$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/livevideo/player/d$1$1;-><init>(Lcom/twitter/android/livevideo/player/d$1;Lrx/i;)V

    .line 38
    invoke-virtual {v0, v1}, Landroid/view/accessibility/CaptioningManager;->addCaptioningChangeListener(Landroid/view/accessibility/CaptioningManager$CaptioningChangeListener;)V

    .line 53
    new-instance v0, Lcom/twitter/android/livevideo/player/a$a;

    iget-object v1, p0, Lcom/twitter/android/livevideo/player/d$1;->a:Lcom/twitter/android/livevideo/player/d;

    .line 54
    invoke-static {v1}, Lcom/twitter/android/livevideo/player/d;->a(Lcom/twitter/android/livevideo/player/d;)Landroid/view/accessibility/CaptioningManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/accessibility/CaptioningManager;->getUserStyle()Landroid/view/accessibility/CaptioningManager$CaptionStyle;

    move-result-object v1

    .line 53
    invoke-static {v1}, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->createFromCaptionStyle(Landroid/view/accessibility/CaptioningManager$CaptionStyle;)Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/livevideo/player/d$1;->a:Lcom/twitter/android/livevideo/player/d;

    .line 54
    invoke-static {v2}, Lcom/twitter/android/livevideo/player/d;->a(Lcom/twitter/android/livevideo/player/d;)Landroid/view/accessibility/CaptioningManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/accessibility/CaptioningManager;->getFontScale()F

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/livevideo/player/a$a;-><init>(Lcom/google/android/exoplayer/text/CaptionStyleCompat;F)V

    .line 53
    invoke-virtual {p1, v0}, Lrx/i;->a(Ljava/lang/Object;)V

    .line 55
    return-void
.end method

.method public synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 34
    check-cast p1, Lrx/i;

    invoke-virtual {p0, p1}, Lcom/twitter/android/livevideo/player/d$1;->a(Lrx/i;)V

    return-void
.end method
