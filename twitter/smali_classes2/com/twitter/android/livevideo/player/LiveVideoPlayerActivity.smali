.class public Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;
.super Lcom/twitter/android/AVMediaPlayerActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/n$a;


# instance fields
.field f:Lcom/twitter/android/livevideo/d;

.field private g:Lcom/twitter/util/n;

.field private h:Z

.field private i:Z

.field private j:Lcom/twitter/library/widget/StatusToolBar;

.field private final k:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$b;

.field private l:Lcom/twitter/model/livevideo/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/twitter/android/AVMediaPlayerActivity;-><init>()V

    .line 59
    new-instance v0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity$1;-><init>(Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;)V

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->k:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$b;

    return-void
.end method

.method private a(Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 136
    new-instance v0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity$2;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity$2;-><init>(Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;)Lcom/twitter/library/widget/StatusToolBar;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->j:Lcom/twitter/library/widget/StatusToolBar;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    .line 109
    invoke-super {p0, p1, p2}, Lcom/twitter/android/AVMediaPlayerActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    .line 110
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(I)V

    .line 111
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 112
    return-object v0
.end method

.method protected a(Lank;)Lcom/twitter/app/common/base/j;
    .locals 4

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/twitter/android/AVMediaPlayerActivity;->a(Lank;)Lcom/twitter/app/common/base/j;

    .line 79
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 80
    const-string/jumbo v1, "should_finish_on_portrait"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->h:Z

    .line 81
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "event"

    sget-object v2, Lcom/twitter/model/livevideo/b;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1, v2}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/livevideo/b;

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->l:Lcom/twitter/model/livevideo/b;

    .line 83
    new-instance v0, Lvy;

    iget-object v1, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->l:Lcom/twitter/model/livevideo/b;

    iget-wide v2, v1, Lcom/twitter/model/livevideo/b;->b:J

    invoke-direct {v0, p0, p1, v2, v3}, Lvy;-><init>(Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;Lank;J)V

    .line 86
    invoke-static {}, Lvw;->c()Lvw$a;

    move-result-object v1

    .line 87
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v2

    invoke-virtual {v1, v2}, Lvw$a;->a(Lamu;)Lvw$a;

    move-result-object v1

    .line 88
    invoke-virtual {v1, v0}, Lvw$a;->a(Lvy;)Lvw$a;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lvw$a;->a()Lvx;

    move-result-object v0

    .line 91
    invoke-interface {v0, p0}, Lvx;->a(Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;)V

    .line 93
    return-object v0
.end method

.method protected a(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 70
    invoke-super {p0, p1}, Lcom/twitter/android/AVMediaPlayerActivity;->a(Landroid/view/ViewGroup;)V

    .line 71
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0403f4

    invoke-virtual {v0, v1, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 72
    const v1, 0x7f13007f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/StatusToolBar;

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->j:Lcom/twitter/library/widget/StatusToolBar;

    .line 73
    return-void
.end method

.method public a(Lcmm;)Z
    .locals 4

    .prologue
    .line 220
    invoke-interface {p1}, Lcmm;->a()I

    move-result v0

    .line 221
    const v1, 0x7f1308bc

    if-ne v1, v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->f:Lcom/twitter/android/livevideo/d;

    iget-object v1, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->l:Lcom/twitter/model/livevideo/b;

    iget-wide v2, v1, Lcom/twitter/model/livevideo/b;->b:J

    invoke-virtual {v0, p0, v2, v3}, Lcom/twitter/android/livevideo/d;->a(Landroid/content/Context;J)V

    .line 223
    const/4 v0, 0x1

    .line 225
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/AVMediaPlayerActivity;->a(Lcmm;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lcmr;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 164
    invoke-super {p0, p1}, Lcom/twitter/android/AVMediaPlayerActivity;->a(Lcmr;)Z

    .line 165
    const v0, 0x7f14002f

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 166
    const v0, 0x7f1308bc

    invoke-interface {p1, v0}, Lcmr;->b(I)Lcmm;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->l:Lcom/twitter/model/livevideo/b;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-interface {v2, v0}, Lcmm;->f(Z)Lcmm;

    .line 167
    return v1

    .line 166
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcmr;)I
    .locals 2

    .prologue
    .line 155
    const v0, 0x7f13088d

    invoke-interface {p1, v0}, Lcmr;->b(I)Lcmm;

    move-result-object v0

    .line 156
    if-eqz v0, :cond_0

    .line 157
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcmm;->f(Z)Lcmm;

    .line 159
    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/AVMediaPlayerActivity;->b(Lcmr;)I

    move-result v0

    return v0
.end method

.method protected synthetic b(Lank;)Lcom/twitter/app/common/abs/b;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->a(Lank;)Lcom/twitter/app/common/base/j;

    move-result-object v0

    return-object v0
.end method

.method protected b()Lcom/twitter/library/av/VideoPlayerView;
    .locals 3

    .prologue
    .line 118
    new-instance v0, Lcom/twitter/library/av/LiveVideoPlayerView;

    iget-object v1, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->b:Lcom/twitter/library/av/playback/AVPlayerAttachment;

    sget-object v2, Lcom/twitter/library/av/VideoPlayerView$Mode;->k:Lcom/twitter/library/av/VideoPlayerView$Mode;

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/library/av/LiveVideoPlayerView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    return-object v0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 2

    .prologue
    .line 98
    invoke-super {p0, p1, p2}, Lcom/twitter/android/AVMediaPlayerActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 100
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 102
    new-instance v0, Lcom/twitter/util/n;

    invoke-virtual {p0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/util/n;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->g:Lcom/twitter/util/n;

    .line 103
    return-void
.end method

.method protected synthetic c(Lank;)Lans;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->a(Lank;)Lcom/twitter/app/common/base/j;

    move-result-object v0

    return-object v0
.end method

.method protected c()Lcom/twitter/library/av/control/e;
    .locals 2

    .prologue
    .line 124
    new-instance v0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;

    invoke-direct {v0, p0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;-><init>(Landroid/content/Context;)V

    .line 125
    iget-object v1, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->k:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$b;

    invoke-virtual {v0, v1}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->setOnFullscreenClickListener(Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$b;)V

    .line 126
    invoke-direct {p0, v0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->a(Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    iget-object v1, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->l:Lcom/twitter/model/livevideo/b;

    if-eqz v1, :cond_0

    .line 128
    iget-object v1, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->l:Lcom/twitter/model/livevideo/b;

    invoke-virtual {v0, v1}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->setEvent(Lcom/twitter/model/livevideo/b;)V

    .line 130
    :cond_0
    return-object v0
.end method

.method protected d_()Z
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->z()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e_(I)V
    .locals 1

    .prologue
    .line 206
    if-nez p1, :cond_0

    .line 210
    iget-boolean v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->h:Z

    if-nez v0, :cond_1

    .line 211
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->h:Z

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->finish()V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 193
    invoke-super {p0, p1}, Lcom/twitter/android/AVMediaPlayerActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 194
    const-string/jumbo v0, "state_start_video_on_restart"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->i:Z

    .line 195
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->a:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->i:Z

    .line 200
    const-string/jumbo v0, "state_start_video_on_restart"

    iget-boolean v1, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->i:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 201
    invoke-super {p0, p1}, Lcom/twitter/android/AVMediaPlayerActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 202
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 172
    invoke-super {p0}, Lcom/twitter/android/AVMediaPlayerActivity;->onStart()V

    .line 173
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->g:Lcom/twitter/util/n;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->g:Lcom/twitter/util/n;

    invoke-virtual {v0, p0}, Lcom/twitter/util/n;->a(Lcom/twitter/util/n$a;)V

    .line 175
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->g:Lcom/twitter/util/n;

    invoke-virtual {v0}, Lcom/twitter/util/n;->a()V

    .line 177
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->i:Z

    if-eqz v0, :cond_1

    .line 178
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->a:Lcom/twitter/library/av/playback/AVPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->b(Z)V

    .line 180
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 184
    invoke-super {p0}, Lcom/twitter/android/AVMediaPlayerActivity;->onStop()V

    .line 185
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->g:Lcom/twitter/util/n;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->g:Lcom/twitter/util/n;

    invoke-virtual {v0}, Lcom/twitter/util/n;->b()V

    .line 187
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;->g:Lcom/twitter/util/n;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/util/n;->a(Lcom/twitter/util/n$a;)V

    .line 189
    :cond_0
    return-void
.end method
