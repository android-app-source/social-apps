.class public Lcom/twitter/android/livevideo/player/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/livevideo/player/a;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field private final a:Landroid/view/accessibility/CaptioningManager;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lrx/c;",
            "Landroid/view/accessibility/CaptioningManager$CaptioningChangeListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-string/jumbo v0, "captioning"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/CaptioningManager;

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/d;->a:Landroid/view/accessibility/CaptioningManager;

    .line 27
    new-instance v0, Ljava/util/IdentityHashMap;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/IdentityHashMap;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/d;->b:Ljava/util/Map;

    .line 28
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/livevideo/player/d;)Landroid/view/accessibility/CaptioningManager;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/d;->a:Landroid/view/accessibility/CaptioningManager;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/livevideo/player/d;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/d;->b:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public a()Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcom/twitter/android/livevideo/player/a$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    new-instance v0, Lcom/twitter/android/livevideo/player/d$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/livevideo/player/d$1;-><init>(Lcom/twitter/android/livevideo/player/d;)V

    .line 34
    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    .line 58
    new-instance v1, Lcom/twitter/android/livevideo/player/d$2;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/livevideo/player/d$2;-><init>(Lcom/twitter/android/livevideo/player/d;Lrx/c;)V

    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public b()Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    new-instance v0, Lcom/twitter/android/livevideo/player/d$3;

    invoke-direct {v0, p0}, Lcom/twitter/android/livevideo/player/d$3;-><init>(Lcom/twitter/android/livevideo/player/d;)V

    .line 69
    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    .line 84
    new-instance v1, Lcom/twitter/android/livevideo/player/d$4;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/livevideo/player/d$4;-><init>(Lcom/twitter/android/livevideo/player/d;Lrx/c;)V

    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method
