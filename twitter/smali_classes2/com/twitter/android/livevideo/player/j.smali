.class Lcom/twitter/android/livevideo/player/j;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/livevideo/player/i;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/livevideo/player/j$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/view/View;

.field private final c:Ljava/lang/String;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/ImageButton;

.field private final f:Landroid/view/View;

.field private final g:Lcom/twitter/library/av/control/b;

.field private final h:Landroid/view/View;

.field private final i:Landroid/widget/ImageButton;

.field private final j:Lcom/google/android/exoplayer/text/SubtitleLayout;

.field private final k:Landroid/content/res/Resources;

.field private final l:Landroid/widget/ImageButton;

.field private m:Lcom/twitter/android/livevideo/player/i$a;

.field private final n:Lcwv;

.field private o:Lcom/twitter/model/av/AVMedia;

.field private final p:Lrx/subjects/ReplaySubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/ReplaySubject",
            "<",
            "Lcom/twitter/library/av/playback/AVPlayerAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcom/twitter/model/livevideo/b;

.field private final r:Landroid/view/View$OnClickListener;


# direct methods
.method constructor <init>(Landroid/content/res/Resources;Landroid/view/LayoutInflater;Lrx/subjects/ReplaySubject;Lcom/twitter/android/livevideo/player/a;Lcom/twitter/android/livevideo/player/i$a;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Landroid/view/LayoutInflater;",
            "Lrx/subjects/ReplaySubject",
            "<",
            "Lcom/twitter/library/av/playback/AVPlayerAttachment;",
            ">;",
            "Lcom/twitter/android/livevideo/player/a;",
            "Lcom/twitter/android/livevideo/player/i$a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 90
    .line 91
    invoke-static {}, Lcom/twitter/library/av/control/c;->b()Lcom/twitter/library/av/control/b;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 90
    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/livevideo/player/j;-><init>(Landroid/content/res/Resources;Landroid/view/LayoutInflater;Lrx/subjects/ReplaySubject;Lcom/twitter/android/livevideo/player/a;Lcom/twitter/android/livevideo/player/i$a;Lcom/twitter/library/av/control/b;)V

    .line 92
    return-void
.end method

.method constructor <init>(Landroid/content/res/Resources;Landroid/view/LayoutInflater;Lrx/subjects/ReplaySubject;Lcom/twitter/android/livevideo/player/a;Lcom/twitter/android/livevideo/player/i$a;Lcom/twitter/library/av/control/b;)V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Landroid/view/LayoutInflater;",
            "Lrx/subjects/ReplaySubject",
            "<",
            "Lcom/twitter/library/av/playback/AVPlayerAttachment;",
            ">;",
            "Lcom/twitter/android/livevideo/player/a;",
            "Lcom/twitter/android/livevideo/player/i$a;",
            "Lcom/twitter/library/av/control/b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Lcwv;

    invoke-direct {v0}, Lcwv;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/j;->n:Lcwv;

    .line 56
    new-instance v0, Lcom/twitter/android/livevideo/player/j$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/livevideo/player/j$1;-><init>(Lcom/twitter/android/livevideo/player/j;)V

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/j;->r:Landroid/view/View$OnClickListener;

    .line 102
    iput-object p1, p0, Lcom/twitter/android/livevideo/player/j;->k:Landroid/content/res/Resources;

    .line 103
    iput-object p5, p0, Lcom/twitter/android/livevideo/player/j;->m:Lcom/twitter/android/livevideo/player/i$a;

    .line 104
    iput-object p3, p0, Lcom/twitter/android/livevideo/player/j;->p:Lrx/subjects/ReplaySubject;

    .line 106
    const v0, 0x7f04017b

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/j;->b:Landroid/view/View;

    .line 107
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->b:Landroid/view/View;

    const v1, 0x7f1301a5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/j;->a:Landroid/view/View;

    .line 109
    const v0, 0x7f0a009c

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/j;->c:Ljava/lang/String;

    .line 110
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->b:Landroid/view/View;

    const v1, 0x7f130441

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/j;->d:Landroid/widget/TextView;

    .line 112
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->b:Landroid/view/View;

    const v1, 0x7f130440

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/j;->e:Landroid/widget/ImageButton;

    .line 113
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->e:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 114
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->e:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/twitter/android/livevideo/player/j;->r:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->b:Landroid/view/View;

    const v1, 0x7f130444

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/j;->f:Landroid/view/View;

    .line 117
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/livevideo/player/j;->r:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    iput-object p6, p0, Lcom/twitter/android/livevideo/player/j;->g:Lcom/twitter/library/av/control/b;

    .line 120
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->b:Landroid/view/View;

    const v1, 0x7f130443

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/j;->l:Landroid/widget/ImageButton;

    .line 121
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->l:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/twitter/android/livevideo/player/j;->r:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->b:Landroid/view/View;

    const v1, 0x7f130442

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/j;->h:Landroid/view/View;

    .line 125
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->b:Landroid/view/View;

    const v1, 0x7f130445

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/j;->i:Landroid/widget/ImageButton;

    .line 126
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->i:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/twitter/android/livevideo/player/j;->r:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->b:Landroid/view/View;

    const v1, 0x7f130446

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/text/SubtitleLayout;

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/j;->j:Lcom/google/android/exoplayer/text/SubtitleLayout;

    .line 129
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->j:Lcom/google/android/exoplayer/text/SubtitleLayout;

    invoke-direct {p0, v0, p4}, Lcom/twitter/android/livevideo/player/j;->a(Lcom/google/android/exoplayer/text/SubtitleLayout;Lcom/twitter/android/livevideo/player/a;)V

    .line 130
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/livevideo/player/j;)Lcom/twitter/android/livevideo/player/i$a;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->m:Lcom/twitter/android/livevideo/player/i$a;

    return-object v0
.end method

.method private a(Lcom/google/android/exoplayer/text/SubtitleLayout;Lcom/twitter/android/livevideo/player/a;)V
    .locals 3

    .prologue
    .line 134
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->n:Lcwv;

    invoke-interface {p2}, Lcom/twitter/android/livevideo/player/a;->b()Lrx/c;

    move-result-object v1

    invoke-virtual {v1}, Lrx/c;->j()Lrx/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/livevideo/player/j$2;

    invoke-direct {v2, p0, p1}, Lcom/twitter/android/livevideo/player/j$2;-><init>(Lcom/twitter/android/livevideo/player/j;Lcom/google/android/exoplayer/text/SubtitleLayout;)V

    invoke-virtual {v1, v2}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcwv;->a(Lrx/j;)V

    .line 142
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->n:Lcwv;

    invoke-interface {p2}, Lcom/twitter/android/livevideo/player/a;->a()Lrx/c;

    move-result-object v1

    invoke-virtual {v1}, Lrx/c;->j()Lrx/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/livevideo/player/j$3;

    invoke-direct {v2, p0, p1}, Lcom/twitter/android/livevideo/player/j$3;-><init>(Lcom/twitter/android/livevideo/player/j;Lcom/google/android/exoplayer/text/SubtitleLayout;)V

    invoke-virtual {v1, v2}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcwv;->a(Lrx/j;)V

    .line 151
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/livevideo/player/j;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->e:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/livevideo/player/j;)Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->k:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/livevideo/player/j;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->i:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 252
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->o:Lcom/twitter/model/av/AVMedia;

    if-nez v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->e:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 254
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 255
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 256
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->l:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 257
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 271
    :goto_0
    return-void

    .line 258
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->o:Lcom/twitter/model/av/AVMedia;

    invoke-static {v0}, Lcom/twitter/model/av/b;->a(Lcom/twitter/model/av/AVMedia;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 259
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->e:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 260
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 261
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 262
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->l:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 263
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 265
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->e:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 266
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 267
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 268
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 269
    invoke-direct {p0}, Lcom/twitter/android/livevideo/player/j;->f()V

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 274
    const-string/jumbo v0, "docking_enabled"

    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->q:Lcom/twitter/model/livevideo/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->q:Lcom/twitter/model/livevideo/b;

    iget-boolean v0, v0, Lcom/twitter/model/livevideo/b;->n:Z

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->l:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 281
    :goto_0
    return-void

    .line 279
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->l:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->b:Landroid/view/View;

    return-object v0
.end method

.method public a(Lcom/twitter/library/av/playback/aa;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 223
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->o:Lcom/twitter/model/av/AVMedia;

    invoke-static {v0}, Lcom/twitter/model/av/b;->a(Lcom/twitter/model/av/AVMedia;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    iget-wide v0, p1, Lcom/twitter/library/av/playback/aa;->c:J

    iget-wide v2, p1, Lcom/twitter/library/av/playback/aa;->b:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 225
    invoke-static {v0, v1}, Lcom/twitter/util/aa;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 226
    iget-object v1, p0, Lcom/twitter/android/livevideo/player/j;->d:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/twitter/android/livevideo/player/j;->c:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->g:Lcom/twitter/library/av/control/b;

    invoke-virtual {v0, p1}, Lcom/twitter/library/av/control/b;->a(Lcom/twitter/library/av/playback/aa;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->f:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 232
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/av/AVMedia;)V
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lcom/twitter/android/livevideo/player/j;->o:Lcom/twitter/model/av/AVMedia;

    .line 243
    invoke-direct {p0}, Lcom/twitter/android/livevideo/player/j;->e()V

    .line 244
    return-void
.end method

.method public a(Lcom/twitter/model/livevideo/b;)V
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lcom/twitter/android/livevideo/player/j;->q:Lcom/twitter/model/livevideo/b;

    .line 237
    invoke-direct {p0}, Lcom/twitter/android/livevideo/player/j;->e()V

    .line 238
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer/text/Cue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 218
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->j:Lcom/google/android/exoplayer/text/SubtitleLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer/text/SubtitleLayout;->setCues(Ljava/util/List;)V

    .line 219
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->a:Landroid/view/View;

    invoke-static {v0}, Lcom/twitter/util/e;->b(Landroid/view/View;)V

    .line 184
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->p:Lrx/subjects/ReplaySubject;

    new-instance v1, Lcom/twitter/android/livevideo/player/j$4;

    invoke-direct {v1, p0}, Lcom/twitter/android/livevideo/player/j$4;-><init>(Lcom/twitter/android/livevideo/player/j;)V

    invoke-virtual {v0, v1}, Lrx/subjects/ReplaySubject;->c(Lrx/functions/b;)V

    .line 213
    invoke-direct {p0}, Lcom/twitter/android/livevideo/player/j;->e()V

    .line 214
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j;->n:Lcwv;

    invoke-virtual {v0}, Lcwv;->B_()V

    .line 249
    return-void
.end method
