.class public Lcom/twitter/android/livevideo/player/f;
.super Lcom/twitter/android/av/ad;
.source "Twttr"


# instance fields
.field private k:Z

.field private l:Lcom/twitter/model/livevideo/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/twitter/android/av/ad;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/livevideo/player/f;->k:Z

    .line 24
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 45
    new-instance v0, Lcom/twitter/app/common/base/h;

    invoke-direct {v0}, Lcom/twitter/app/common/base/h;-><init>()V

    iget-boolean v1, p0, Lcom/twitter/android/livevideo/player/f;->f:Z

    .line 46
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/h;->d(Z)Lcom/twitter/app/common/base/h;

    move-result-object v0

    const-class v1, Lcom/twitter/android/livevideo/player/LiveVideoPlayerActivity;

    .line 47
    invoke-virtual {v0, p1, v1}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "should_finish_on_portrait"

    iget-boolean v2, p0, Lcom/twitter/android/livevideo/player/f;->k:Z

    .line 48
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 49
    const-string/jumbo v1, "event"

    iget-object v2, p0, Lcom/twitter/android/livevideo/player/f;->l:Lcom/twitter/model/livevideo/b;

    sget-object v3, Lcom/twitter/model/livevideo/b;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 50
    invoke-virtual {p0, v0}, Lcom/twitter/android/livevideo/player/f;->b(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/model/livevideo/b;)Lcom/twitter/android/livevideo/player/f;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/twitter/android/livevideo/player/f;->l:Lcom/twitter/model/livevideo/b;

    .line 39
    return-object p0
.end method

.method public a(Z)Lcom/twitter/android/livevideo/player/f;
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/twitter/android/livevideo/player/f;->k:Z

    .line 33
    return-object p0
.end method
