.class Lcom/twitter/android/livevideo/player/j$4;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/livevideo/player/j;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/b",
        "<",
        "Lcom/twitter/library/av/playback/AVPlayerAttachment;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/livevideo/player/j;


# direct methods
.method constructor <init>(Lcom/twitter/android/livevideo/player/j;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/twitter/android/livevideo/player/j$4;->a:Lcom/twitter/android/livevideo/player/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V
    .locals 3

    .prologue
    .line 194
    invoke-virtual {p1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    .line 195
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j$4;->a:Lcom/twitter/android/livevideo/player/j;

    invoke-static {v0}, Lcom/twitter/android/livevideo/player/j;->b(Lcom/twitter/android/livevideo/player/j;)Landroid/widget/ImageButton;

    move-result-object v0

    const v1, 0x7f0206b0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 197
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j$4;->a:Lcom/twitter/android/livevideo/player/j;

    invoke-static {v0}, Lcom/twitter/android/livevideo/player/j;->b(Lcom/twitter/android/livevideo/player/j;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/livevideo/player/j$4;->a:Lcom/twitter/android/livevideo/player/j;

    invoke-static {v1}, Lcom/twitter/android/livevideo/player/j;->c(Lcom/twitter/android/livevideo/player/j;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0923

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 203
    :goto_0
    invoke-virtual {p1}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->b()Lbyf;

    move-result-object v0

    invoke-interface {v0}, Lbyf;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j$4;->a:Lcom/twitter/android/livevideo/player/j;

    invoke-static {v0}, Lcom/twitter/android/livevideo/player/j;->d(Lcom/twitter/android/livevideo/player/j;)Landroid/widget/ImageButton;

    move-result-object v0

    const v1, 0x7f02069c

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 205
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j$4;->a:Lcom/twitter/android/livevideo/player/j;

    invoke-static {v0}, Lcom/twitter/android/livevideo/player/j;->d(Lcom/twitter/android/livevideo/player/j;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/livevideo/player/j$4;->a:Lcom/twitter/android/livevideo/player/j;

    invoke-static {v1}, Lcom/twitter/android/livevideo/player/j;->c(Lcom/twitter/android/livevideo/player/j;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0097

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 210
    :goto_1
    return-void

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j$4;->a:Lcom/twitter/android/livevideo/player/j;

    invoke-static {v0}, Lcom/twitter/android/livevideo/player/j;->b(Lcom/twitter/android/livevideo/player/j;)Landroid/widget/ImageButton;

    move-result-object v0

    const v1, 0x7f0206a7

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 200
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j$4;->a:Lcom/twitter/android/livevideo/player/j;

    invoke-static {v0}, Lcom/twitter/android/livevideo/player/j;->b(Lcom/twitter/android/livevideo/player/j;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/livevideo/player/j$4;->a:Lcom/twitter/android/livevideo/player/j;

    invoke-static {v1}, Lcom/twitter/android/livevideo/player/j;->c(Lcom/twitter/android/livevideo/player/j;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a06a1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j$4;->a:Lcom/twitter/android/livevideo/player/j;

    invoke-static {v0}, Lcom/twitter/android/livevideo/player/j;->d(Lcom/twitter/android/livevideo/player/j;)Landroid/widget/ImageButton;

    move-result-object v0

    const v1, 0x7f0206a1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 208
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/j$4;->a:Lcom/twitter/android/livevideo/player/j;

    invoke-static {v0}, Lcom/twitter/android/livevideo/player/j;->d(Lcom/twitter/android/livevideo/player/j;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/livevideo/player/j$4;->a:Lcom/twitter/android/livevideo/player/j;

    invoke-static {v1}, Lcom/twitter/android/livevideo/player/j;->c(Lcom/twitter/android/livevideo/player/j;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0099

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 191
    check-cast p1, Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {p0, p1}, Lcom/twitter/android/livevideo/player/j$4;->a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V

    return-void
.end method
