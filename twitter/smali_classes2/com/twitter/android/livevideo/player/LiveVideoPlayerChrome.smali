.class public Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;
.super Landroid/widget/FrameLayout;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/livevideo/player/i$a;
.implements Lcom/twitter/library/av/control/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$a;,
        Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$b;
    }
.end annotation


# instance fields
.field private final a:Lrx/subjects/ReplaySubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/ReplaySubject",
            "<",
            "Lcom/twitter/library/av/playback/AVPlayerAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/android/livevideo/player/i;

.field private final c:Lcom/twitter/android/livevideo/player/LiveVideoPlayerErrorView;

.field private final d:Lcom/twitter/android/livevideo/player/LiveVideoPlayerRetryView;

.field private e:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$b;

.field private f:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 61
    invoke-static {}, Lrx/subjects/ReplaySubject;->r()Lrx/subjects/ReplaySubject;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILrx/subjects/ReplaySubject;)V

    .line 62
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILrx/subjects/ReplaySubject;)V
    .locals 6
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/util/AttributeSet;",
            "I",
            "Lrx/subjects/ReplaySubject",
            "<",
            "Lcom/twitter/library/av/playback/AVPlayerAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 67
    new-instance v5, Lcom/twitter/android/livevideo/player/j$a;

    invoke-direct {v5}, Lcom/twitter/android/livevideo/player/j$a;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILrx/subjects/ReplaySubject;Lcom/twitter/android/livevideo/player/j$a;)V

    .line 69
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILrx/subjects/ReplaySubject;Lcom/twitter/android/livevideo/player/j$a;)V
    .locals 7
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/util/AttributeSet;",
            "I",
            "Lrx/subjects/ReplaySubject",
            "<",
            "Lcom/twitter/library/av/playback/AVPlayerAttachment;",
            ">;",
            "Lcom/twitter/android/livevideo/player/j$a;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/16 v6, 0x8

    .line 75
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    iput-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->e:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$b;

    .line 49
    iput-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->f:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$a;

    .line 77
    iput-object p4, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->a:Lrx/subjects/ReplaySubject;

    .line 79
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v0, "layout_inflater"

    .line 80
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    new-instance v0, Lcom/twitter/android/livevideo/player/c;

    invoke-direct {v0, p1}, Lcom/twitter/android/livevideo/player/c;-><init>(Landroid/content/Context;)V

    .line 81
    invoke-virtual {v0}, Lcom/twitter/android/livevideo/player/c;->a()Lcom/twitter/android/livevideo/player/a;

    move-result-object v4

    move-object v0, p5

    move-object v3, p4

    move-object v5, p0

    .line 79
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/livevideo/player/j$a;->a(Landroid/content/res/Resources;Landroid/view/LayoutInflater;Lrx/subjects/ReplaySubject;Lcom/twitter/android/livevideo/player/a;Lcom/twitter/android/livevideo/player/i$a;)Lcom/twitter/android/livevideo/player/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->b:Lcom/twitter/android/livevideo/player/i;

    .line 82
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->b:Lcom/twitter/android/livevideo/player/i;

    invoke-interface {v0}, Lcom/twitter/android/livevideo/player/i;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->addView(Landroid/view/View;)V

    .line 84
    new-instance v0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerErrorView;

    invoke-direct {v0, p1}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerErrorView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->c:Lcom/twitter/android/livevideo/player/LiveVideoPlayerErrorView;

    .line 85
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->c:Lcom/twitter/android/livevideo/player/LiveVideoPlayerErrorView;

    invoke-virtual {v0, v6}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerErrorView;->setVisibility(I)V

    .line 86
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->c:Lcom/twitter/android/livevideo/player/LiveVideoPlayerErrorView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->addView(Landroid/view/View;)V

    .line 88
    new-instance v0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerRetryView;

    invoke-direct {v0, p1}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerRetryView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->d:Lcom/twitter/android/livevideo/player/LiveVideoPlayerRetryView;

    .line 89
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->d:Lcom/twitter/android/livevideo/player/LiveVideoPlayerRetryView;

    invoke-virtual {v0, v6}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerRetryView;->setVisibility(I)V

    .line 90
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->d:Lcom/twitter/android/livevideo/player/LiveVideoPlayerRetryView;

    new-instance v1, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$1;-><init>(Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerRetryView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->d:Lcom/twitter/android/livevideo/player/LiveVideoPlayerRetryView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->addView(Landroid/view/View;)V

    .line 103
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;)Lcom/twitter/android/livevideo/player/LiveVideoPlayerRetryView;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->d:Lcom/twitter/android/livevideo/player/LiveVideoPlayerRetryView;

    return-object v0
.end method

.method private static a(Lcom/twitter/library/av/ad;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 315
    const-string/jumbo v1, "live_video_playback_retry_button_enabled"

    invoke-static {v1, v0}, Lcoj;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 316
    invoke-virtual {p0}, Lcom/twitter/library/av/ad;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 315
    :cond_0
    return v0
.end method

.method static synthetic b(Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;)Lrx/subjects/ReplaySubject;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->a:Lrx/subjects/ReplaySubject;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 166
    return-void
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/av/ad;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 231
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->f:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$a;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->f:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$a;

    invoke-interface {v0, p2}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$a;->a(Lcom/twitter/library/av/ad;)V

    .line 234
    :cond_0
    invoke-static {p2}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->a(Lcom/twitter/library/av/ad;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->d:Lcom/twitter/android/livevideo/player/LiveVideoPlayerRetryView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerRetryView;->setVisibility(I)V

    .line 236
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->c:Lcom/twitter/android/livevideo/player/LiveVideoPlayerErrorView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerErrorView;->setVisibility(I)V

    .line 237
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->d:Lcom/twitter/android/livevideo/player/LiveVideoPlayerRetryView;

    iget-object v1, p2, Lcom/twitter/library/av/ad;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerRetryView;->setError(Ljava/lang/String;)V

    .line 243
    :goto_0
    return-void

    .line 239
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->c:Lcom/twitter/android/livevideo/player/LiveVideoPlayerErrorView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerErrorView;->setVisibility(I)V

    .line 240
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->d:Lcom/twitter/android/livevideo/player/LiveVideoPlayerRetryView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerRetryView;->setVisibility(I)V

    .line 241
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->c:Lcom/twitter/android/livevideo/player/LiveVideoPlayerErrorView;

    iget-object v1, p2, Lcom/twitter/library/av/ad;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerErrorView;->setError(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->b:Lcom/twitter/android/livevideo/player/i;

    invoke-interface {v0}, Lcom/twitter/android/livevideo/player/i;->c()V

    .line 162
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V
    .locals 1

    .prologue
    .line 126
    if-eqz p1, :cond_0

    .line 127
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->a:Lrx/subjects/ReplaySubject;

    invoke-virtual {v0, p1}, Lrx/subjects/ReplaySubject;->a(Ljava/lang/Object;)V

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->b:Lcom/twitter/android/livevideo/player/i;

    invoke-interface {v0}, Lcom/twitter/android/livevideo/player/i;->c()V

    .line 130
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/aa;)V
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->b:Lcom/twitter/android/livevideo/player/i;

    invoke-interface {v0, p1}, Lcom/twitter/android/livevideo/player/i;->a(Lcom/twitter/library/av/playback/aa;)V

    .line 248
    return-void
.end method

.method public a(Lcom/twitter/model/av/AVMedia;)V
    .locals 2

    .prologue
    .line 139
    if-nez p1, :cond_0

    .line 157
    :goto_0
    return-void

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->a:Lrx/subjects/ReplaySubject;

    new-instance v1, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$2;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$2;-><init>(Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;Lcom/twitter/model/av/AVMedia;)V

    invoke-virtual {v0, v1}, Lrx/subjects/ReplaySubject;->c(Lrx/functions/b;)V

    .line 156
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->b:Lcom/twitter/android/livevideo/player/i;

    invoke-interface {v0, p1}, Lcom/twitter/android/livevideo/player/i;->a(Lcom/twitter/model/av/AVMedia;)V

    goto :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer/text/Cue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 252
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->b:Lcom/twitter/android/livevideo/player/i;

    invoke-interface {v0, p1}, Lcom/twitter/android/livevideo/player/i;->a(Ljava/util/List;)V

    .line 253
    return-void
.end method

.method public a_(Z)V
    .locals 0

    .prologue
    .line 186
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 170
    return-void
.end method

.method public b_(Z)V
    .locals 0

    .prologue
    .line 223
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 174
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 178
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 182
    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->m()V

    .line 191
    const/4 v0, 0x0

    return v0
.end method

.method public g()V
    .locals 0

    .prologue
    .line 204
    return-void
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 121
    return-object p0
.end method

.method public h()V
    .locals 0

    .prologue
    .line 200
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->b:Lcom/twitter/android/livevideo/player/i;

    invoke-interface {v0}, Lcom/twitter/android/livevideo/player/i;->d()V

    .line 209
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->b:Lcom/twitter/android/livevideo/player/i;

    invoke-interface {v0}, Lcom/twitter/android/livevideo/player/i;->c()V

    .line 214
    return-void
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->a:Lrx/subjects/ReplaySubject;

    invoke-virtual {v0}, Lrx/subjects/ReplaySubject;->s()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->a:Lrx/subjects/ReplaySubject;

    invoke-virtual {v0}, Lrx/subjects/ReplaySubject;->s()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/av/playback/AVPlayerAttachment;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->a()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()V
    .locals 0

    .prologue
    .line 218
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->requestLayout()V

    .line 219
    return-void
.end method

.method public m()V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->b:Lcom/twitter/android/livevideo/player/i;

    invoke-interface {v0}, Lcom/twitter/android/livevideo/player/i;->b()V

    .line 196
    return-void
.end method

.method public n()V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->e:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$b;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->e:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$b;

    invoke-interface {v0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$b;->a()V

    .line 260
    :cond_0
    return-void
.end method

.method public o()V
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->a:Lrx/subjects/ReplaySubject;

    new-instance v1, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$3;-><init>(Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;)V

    invoke-virtual {v0, v1}, Lrx/subjects/ReplaySubject;->c(Lrx/functions/b;)V

    .line 270
    return-void
.end method

.method public p()V
    .locals 0

    .prologue
    .line 275
    return-void
.end method

.method public q()V
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->a:Lrx/subjects/ReplaySubject;

    new-instance v1, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$4;

    invoke-direct {v1, p0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$4;-><init>(Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;)V

    invoke-virtual {v0, v1}, Lrx/subjects/ReplaySubject;->c(Lrx/functions/b;)V

    .line 285
    return-void
.end method

.method public setEvent(Lcom/twitter/model/livevideo/b;)V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->c:Lcom/twitter/android/livevideo/player/LiveVideoPlayerErrorView;

    iget-object v1, p1, Lcom/twitter/model/livevideo/b;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerErrorView;->setTitle(Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->c:Lcom/twitter/android/livevideo/player/LiveVideoPlayerErrorView;

    iget-object v1, p1, Lcom/twitter/model/livevideo/b;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerErrorView;->setSubtitle(Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->b:Lcom/twitter/android/livevideo/player/i;

    invoke-interface {v0, p1}, Lcom/twitter/android/livevideo/player/i;->a(Lcom/twitter/model/livevideo/b;)V

    .line 109
    return-void
.end method

.method public setOnErrorListener(Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$a;)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->f:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$a;

    .line 296
    return-void
.end method

.method public setOnFullscreenClickListener(Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$b;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->e:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$b;

    .line 289
    return-void
.end method
