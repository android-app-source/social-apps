.class public Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;
.super Lcom/twitter/app/common/timeline/TimelineFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/timeline/br$a;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Lua;

.field c:Lcom/twitter/library/scribe/n;

.field d:Lcom/twitter/android/timeline/br;

.field e:Lub;

.field f:Lcom/twitter/analytics/model/ScribeItem;

.field private final t:Lcwv;

.field private final u:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Lcom/twitter/model/livevideo/b;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcom/twitter/android/livevideo/landing/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;-><init>()V

    .line 76
    new-instance v0, Lcwv;

    invoke-direct {v0}, Lcwv;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->t:Lcwv;

    .line 79
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->u:Lrx/subjects/PublishSubject;

    .line 78
    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f130011

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/widget/NewItemBannerView;

    .line 124
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->t()Lcom/twitter/android/ai;

    move-result-object v3

    .line 125
    invoke-static {v3}, Lcom/twitter/android/livevideo/landing/b;->a(Lcom/twitter/app/common/base/b;)Lcom/twitter/android/livevideo/landing/b;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->v:Lcom/twitter/android/livevideo/landing/b;

    .line 126
    new-instance v0, Lvc;

    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->v:Lcom/twitter/android/livevideo/landing/b;

    .line 127
    invoke-virtual {v3}, Lcom/twitter/android/ai;->e()Ljava/lang/String;

    move-result-object v6

    move-object v3, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lvc;-><init>(Landroid/content/Context;Lcom/twitter/android/livevideo/landing/b;Landroid/os/Bundle;Lcom/twitter/android/widget/NewItemBannerView;Lcom/twitter/android/timeline/br$a;Ljava/lang/String;)V

    .line 130
    invoke-static {}, Lue;->a()Lue$a;

    move-result-object v1

    .line 131
    invoke-virtual {v1, v0}, Lue$a;->a(Lvc;)Lue$a;

    move-result-object v0

    .line 132
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lue$a;->a(Lamu;)Lue$a;

    move-result-object v0

    .line 133
    invoke-virtual {v0}, Lue$a;->a()Lvb;

    move-result-object v0

    .line 135
    invoke-interface {v0, p0}, Lvb;->a(Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;)V

    .line 136
    return-void
.end method

.method private a(Lbdg;I)V
    .locals 4

    .prologue
    .line 250
    invoke-virtual {p1}, Lbdg;->G()I

    move-result v1

    .line 251
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    .line 252
    :goto_0
    invoke-virtual {p1}, Lbdg;->H()Lcom/twitter/model/timeline/u;

    move-result-object v2

    .line 253
    iget-object v3, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->d:Lcom/twitter/android/timeline/br;

    invoke-interface {v3, v1, v0, v2}, Lcom/twitter/android/timeline/br;->a(IZLcom/twitter/model/timeline/u;)V

    .line 254
    return-void

    .line 251
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;I)Z
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0, p1}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->c(I)Z

    move-result v0

    return v0
.end method

.method private aU()V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->e:Lub;

    .line 163
    invoke-virtual {v0}, Lub;->d()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment$3;-><init>(Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;)V

    .line 164
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/functions/b;)Lrx/c;

    move-result-object v0

    .line 172
    invoke-static {}, Lcqw;->d()Lcqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    .line 173
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->t:Lcwv;

    invoke-virtual {v1, v0}, Lcwv;->a(Lrx/j;)V

    .line 174
    return-void
.end method

.method private aV()V
    .locals 2

    .prologue
    .line 177
    new-instance v0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment$4;

    invoke-direct {v0, p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment$4;-><init>(Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->a(Lcno$c;)Lcom/twitter/app/common/list/TwitterListFragment;

    .line 193
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->d:Lcom/twitter/android/timeline/br;

    invoke-interface {v0}, Lcom/twitter/android/timeline/br;->e()V

    .line 194
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->ag()Lanh;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->d:Lcom/twitter/android/timeline/br;

    invoke-virtual {v0, v1}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 195
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 258
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 259
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 260
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->i()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x1

    aput-object v4, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v3, "new_tweet_prompt"

    aput-object v3, v0, v1

    const/4 v1, 0x3

    aput-object v4, v0, v1

    const/4 v1, 0x4

    aput-object p1, v0, v1

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->f:Lcom/twitter/analytics/model/ScribeItem;

    .line 261
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 262
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 263
    return-void
.end method

.method private x()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->b:Lua;

    .line 140
    invoke-virtual {v0}, Lua;->a()Lrx/c;

    move-result-object v0

    .line 141
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 142
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 143
    invoke-static {}, Lcre;->d()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment$2;-><init>(Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;)V

    .line 144
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/functions/b;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment$1;-><init>(Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;)V

    .line 150
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->u:Lrx/subjects/PublishSubject;

    .line 157
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/d;)Lrx/j;

    move-result-object v0

    .line 158
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->t:Lcwv;

    invoke-virtual {v1, v0}, Lcwv;->a(Lrx/j;)V

    .line 159
    return-void
.end method


# virtual methods
.method public synthetic H()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->t()Lcom/twitter/android/ai;

    move-result-object v0

    return-object v0
.end method

.method public synthetic I()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->t()Lcom/twitter/android/ai;

    move-result-object v0

    return-object v0
.end method

.method public L()V
    .locals 1

    .prologue
    .line 199
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->L()V

    .line 200
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->d:Lcom/twitter/android/timeline/br;

    invoke-interface {v0}, Lcom/twitter/android/timeline/br;->j()V

    .line 201
    return-void
.end method

.method protected O()Lcom/twitter/android/ck;
    .locals 3

    .prologue
    .line 237
    new-instance v0, Lcom/twitter/android/livevideo/landing/h;

    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->c:Lcom/twitter/library/scribe/n;

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/android/livevideo/landing/h;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/scribe/n;)V

    return-object v0
.end method

.method public V_()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 98
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->V_()V

    .line 99
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 100
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 101
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->i()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v4, v1, v2

    const/4 v2, 0x2

    aput-object v4, v1, v2

    const/4 v2, 0x3

    aput-object v4, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "pull_to_refresh"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->f:Lcom/twitter/analytics/model/ScribeItem;

    .line 102
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 103
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 104
    return-void
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 1

    .prologue
    .line 84
    invoke-super {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 85
    const v0, 0x7f040180

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->c(I)Lcom/twitter/app/common/list/l$d;

    .line 86
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 1

    .prologue
    .line 242
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 243
    instance-of v0, p1, Lbdg;

    if-eqz v0, :cond_0

    .line 244
    check-cast p1, Lbdg;

    invoke-direct {p0, p1, p3}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->a(Lbdg;I)V

    .line 246
    :cond_0
    return-void
.end method

.method public aC_()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/livevideo/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 231
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->u:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method public aD_()V
    .locals 1

    .prologue
    .line 279
    const-string/jumbo v0, "show"

    invoke-direct {p0, v0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->b(Ljava/lang/String;)V

    .line 280
    return-void
.end method

.method public aE_()V
    .locals 1

    .prologue
    .line 284
    const-string/jumbo v0, "dismiss"

    invoke-direct {p0, v0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->b(Ljava/lang/String;)V

    .line 285
    return-void
.end method

.method protected aP_()V
    .locals 6

    .prologue
    .line 301
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aP_()V

    .line 302
    iget v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->J:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 303
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 304
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "::::impression"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 305
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-direct {v3, v0, v1, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    .line 306
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->f:Lcom/twitter/analytics/model/ScribeItem;

    .line 307
    invoke-virtual {v3, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 308
    invoke-static {v3}, Lcpm;->a(Lcpk;)V

    .line 310
    :cond_0
    return-void
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 267
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->b()V

    .line 268
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->d:Lcom/twitter/android/timeline/br;

    invoke-interface {v0}, Lcom/twitter/android/timeline/br;->g()V

    .line 269
    return-void
.end method

.method protected e(I)Lcom/twitter/library/service/s;
    .locals 3

    .prologue
    .line 206
    invoke-virtual {p0, p1}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->i(I)Lajc$a;

    move-result-object v0

    invoke-virtual {v0}, Lajc$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajc;

    .line 207
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->b:Lua;

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->f:Lcom/twitter/analytics/model/ScribeItem;

    invoke-virtual {v1, v0, v2}, Lua;->a(Lajc;Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/library/service/s;

    move-result-object v0

    return-object v0
.end method

.method protected f()Lcom/twitter/android/cp;
    .locals 6

    .prologue
    .line 91
    new-instance v0, Lcom/twitter/android/cq;

    invoke-direct {v0}, Lcom/twitter/android/cq;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v2

    iget v3, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->J:I

    .line 92
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->c:Lcom/twitter/library/scribe/n;

    .line 91
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/cq;->a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ILcom/twitter/library/client/v;Lcom/twitter/library/scribe/n;)Lcom/twitter/android/cp;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lcom/twitter/app/common/timeline/c;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->t()Lcom/twitter/android/ai;

    move-result-object v0

    return-object v0
.end method

.method protected n()Lti;
    .locals 3

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->c:Lcom/twitter/library/scribe/n;

    invoke-static {v0, v1, v2}, Ltj;->a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/scribe/n;)Lti;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 212
    invoke-super {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 213
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->b:Lua;

    invoke-virtual {v0}, Lua;->c()V

    .line 214
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->d:Lcom/twitter/android/timeline/br;

    invoke-interface {v0}, Lcom/twitter/android/timeline/br;->f()V

    .line 219
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->ag()Lanh;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->d:Lcom/twitter/android/timeline/br;

    invoke-virtual {v0, v1}, Lanh;->b(Ljava/lang/Object;)Lanh;

    .line 220
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->e:Lub;

    invoke-virtual {v0}, Lub;->e()V

    .line 221
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->t:Lcwv;

    invoke-virtual {v0}, Lcwv;->B_()V

    .line 222
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->b:Lua;

    invoke-virtual {v0}, Lua;->b()V

    .line 223
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->onDestroyView()V

    .line 224
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0, p2}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->a(Landroid/os/Bundle;)V

    .line 115
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/timeline/TimelineFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 116
    invoke-direct {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->x()V

    .line 117
    invoke-direct {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->aU()V

    .line 118
    invoke-direct {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->aV()V

    .line 119
    return-void
.end method

.method protected q_()V
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->d:Lcom/twitter/android/timeline/br;

    invoke-interface {v0}, Lcom/twitter/android/timeline/br;->h()V

    .line 274
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->q_()V

    .line 275
    return-void
.end method

.method public s()V
    .locals 1

    .prologue
    .line 289
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->x()V

    .line 290
    const-string/jumbo v0, "click"

    invoke-direct {p0, v0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->b(Ljava/lang/String;)V

    .line 291
    return-void
.end method

.method public t()Lcom/twitter/android/ai;
    .locals 1

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/ai;->a(Landroid/os/Bundle;)Lcom/twitter/android/ai;

    move-result-object v0

    return-object v0
.end method
