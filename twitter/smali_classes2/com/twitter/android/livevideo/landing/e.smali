.class public final Lcom/twitter/android/livevideo/landing/e;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lcom/twitter/android/livevideo/landing/d;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcsd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcsd",
            "<",
            "Lcom/twitter/android/livevideo/landing/d;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/base/BaseFragmentActivity;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lank;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/base/d;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/media/selection/c;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lvo;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/player/f;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/landing/b;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/landing/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/ClientEventLog;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lvu;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/twitter/android/livevideo/landing/e;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/livevideo/landing/e;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcsd;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lcom/twitter/android/livevideo/landing/d;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/common/base/BaseFragmentActivity;",
            ">;",
            "Lcta",
            "<",
            "Lank;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/common/base/d;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/media/selection/c;",
            ">;",
            "Lcta",
            "<",
            "Lvo;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/player/f;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/landing/b;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/landing/a$a;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/ClientEventLog;",
            ">;",
            "Lcta",
            "<",
            "Lvu;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/e;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 65
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/livevideo/landing/e;->b:Lcsd;

    .line 66
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/e;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 67
    :cond_1
    iput-object p2, p0, Lcom/twitter/android/livevideo/landing/e;->c:Lcta;

    .line 68
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/e;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 69
    :cond_2
    iput-object p3, p0, Lcom/twitter/android/livevideo/landing/e;->d:Lcta;

    .line 70
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/e;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 71
    :cond_3
    iput-object p4, p0, Lcom/twitter/android/livevideo/landing/e;->e:Lcta;

    .line 72
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/e;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 73
    :cond_4
    iput-object p5, p0, Lcom/twitter/android/livevideo/landing/e;->f:Lcta;

    .line 74
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/e;->a:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 75
    :cond_5
    iput-object p6, p0, Lcom/twitter/android/livevideo/landing/e;->g:Lcta;

    .line 76
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/e;->a:Z

    if-nez v0, :cond_6

    if-nez p7, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 77
    :cond_6
    iput-object p7, p0, Lcom/twitter/android/livevideo/landing/e;->h:Lcta;

    .line 78
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/e;->a:Z

    if-nez v0, :cond_7

    if-nez p8, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 79
    :cond_7
    iput-object p8, p0, Lcom/twitter/android/livevideo/landing/e;->i:Lcta;

    .line 80
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/e;->a:Z

    if-nez v0, :cond_8

    if-nez p9, :cond_8

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 81
    :cond_8
    iput-object p9, p0, Lcom/twitter/android/livevideo/landing/e;->j:Lcta;

    .line 82
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/e;->a:Z

    if-nez v0, :cond_9

    if-nez p10, :cond_9

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 83
    :cond_9
    iput-object p10, p0, Lcom/twitter/android/livevideo/landing/e;->k:Lcta;

    .line 84
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/e;->a:Z

    if-nez v0, :cond_a

    if-nez p11, :cond_a

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 85
    :cond_a
    iput-object p11, p0, Lcom/twitter/android/livevideo/landing/e;->l:Lcta;

    .line 86
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/e;->a:Z

    if-nez v0, :cond_b

    if-nez p12, :cond_b

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 87
    :cond_b
    iput-object p12, p0, Lcom/twitter/android/livevideo/landing/e;->m:Lcta;

    .line 88
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/e;->a:Z

    if-nez v0, :cond_c

    if-nez p13, :cond_c

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 89
    :cond_c
    iput-object p13, p0, Lcom/twitter/android/livevideo/landing/e;->n:Lcta;

    .line 90
    return-void
.end method

.method public static a(Lcsd;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lcom/twitter/android/livevideo/landing/d;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/common/base/BaseFragmentActivity;",
            ">;",
            "Lcta",
            "<",
            "Lank;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/common/base/d;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/media/selection/c;",
            ">;",
            "Lcta",
            "<",
            "Lvo;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/player/f;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/landing/b;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/landing/a$a;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/ClientEventLog;",
            ">;",
            "Lcta",
            "<",
            "Lvu;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/a;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Lcom/twitter/android/livevideo/landing/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    new-instance v0, Lcom/twitter/android/livevideo/landing/e;

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/twitter/android/livevideo/landing/e;-><init>(Lcsd;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/android/livevideo/landing/d;
    .locals 14

    .prologue
    .line 94
    iget-object v13, p0, Lcom/twitter/android/livevideo/landing/e;->b:Lcsd;

    new-instance v0, Lcom/twitter/android/livevideo/landing/d;

    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/e;->c:Lcta;

    .line 97
    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/app/common/base/BaseFragmentActivity;

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/e;->d:Lcta;

    .line 98
    invoke-interface {v2}, Lcta;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lank;

    iget-object v3, p0, Lcom/twitter/android/livevideo/landing/e;->e:Lcta;

    .line 99
    invoke-interface {v3}, Lcta;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/app/common/base/d;

    iget-object v4, p0, Lcom/twitter/android/livevideo/landing/e;->f:Lcta;

    .line 100
    invoke-interface {v4}, Lcta;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/media/selection/c;

    iget-object v5, p0, Lcom/twitter/android/livevideo/landing/e;->g:Lcta;

    .line 101
    invoke-interface {v5}, Lcta;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lvo;

    iget-object v6, p0, Lcom/twitter/android/livevideo/landing/e;->h:Lcta;

    .line 102
    invoke-interface {v6}, Lcta;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/livevideo/player/f;

    iget-object v7, p0, Lcom/twitter/android/livevideo/landing/e;->i:Lcta;

    .line 103
    invoke-interface {v7}, Lcta;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v8, p0, Lcom/twitter/android/livevideo/landing/e;->j:Lcta;

    .line 104
    invoke-interface {v8}, Lcta;->b()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/android/livevideo/landing/b;

    iget-object v9, p0, Lcom/twitter/android/livevideo/landing/e;->k:Lcta;

    .line 105
    invoke-interface {v9}, Lcta;->b()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/twitter/android/livevideo/landing/a$a;

    iget-object v10, p0, Lcom/twitter/android/livevideo/landing/e;->l:Lcta;

    .line 106
    invoke-interface {v10}, Lcta;->b()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v11, p0, Lcom/twitter/android/livevideo/landing/e;->m:Lcta;

    .line 107
    invoke-interface {v11}, Lcta;->b()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lvu;

    iget-object v12, p0, Lcom/twitter/android/livevideo/landing/e;->n:Lcta;

    .line 108
    invoke-interface {v12}, Lcta;->b()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/twitter/android/livevideo/a;

    invoke-direct/range {v0 .. v12}, Lcom/twitter/android/livevideo/landing/d;-><init>(Lcom/twitter/app/common/base/BaseFragmentActivity;Lank;Lcom/twitter/app/common/base/d;Lcom/twitter/android/media/selection/c;Lvo;Lcom/twitter/android/livevideo/player/f;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/livevideo/landing/b;Lcom/twitter/android/livevideo/landing/a$a;Lcom/twitter/analytics/feature/model/ClientEventLog;Lvu;Lcom/twitter/android/livevideo/a;)V

    .line 94
    invoke-static {v13, v0}, Ldagger/internal/MembersInjectors;->a(Lcsd;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/livevideo/landing/d;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/e;->a()Lcom/twitter/android/livevideo/landing/d;

    move-result-object v0

    return-object v0
.end method
