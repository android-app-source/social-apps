.class public Lcom/twitter/android/livevideo/landing/d;
.super Laoh;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/livevideo/landing/d$a;
    }
.end annotation


# static fields
.field private static final a:Lrx/functions/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/d",
            "<-",
            "Lcom/twitter/model/livevideo/b;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/twitter/android/livevideo/a;

.field private final c:Lvn;

.field private final d:Lcom/twitter/app/common/base/BaseFragmentActivity;

.field private final e:Lcom/twitter/android/livevideo/player/f;

.field private final f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final g:Lcom/twitter/android/livevideo/landing/d$a;

.field private final h:Lcom/twitter/android/livevideo/landing/b;

.field private final i:Lvu;

.field private j:Z

.field private final k:Lcom/twitter/android/livevideo/landing/a;

.field private final l:Lcwv;

.field private m:Lcom/twitter/model/livevideo/a;

.field private final n:Landroid/view/View$OnClickListener;

.field private final o:Lrx/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/d",
            "<",
            "Lcom/twitter/model/livevideo/b;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final q:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$b;

.field private final r:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$a;

.field private final s:Lcom/twitter/android/livevideo/a$a;

.field private t:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/twitter/android/livevideo/landing/d$1;

    invoke-direct {v0}, Lcom/twitter/android/livevideo/landing/d$1;-><init>()V

    sput-object v0, Lcom/twitter/android/livevideo/landing/d;->a:Lrx/functions/d;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/app/common/base/BaseFragmentActivity;Landroid/os/Bundle;Lcom/twitter/app/common/base/d;Lcom/twitter/android/media/selection/c;Lvo;Lcom/twitter/android/livevideo/player/f;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/livevideo/landing/b;Lcom/twitter/android/livevideo/landing/a$a;Lcom/twitter/analytics/feature/model/ClientEventLog;Lvu;Lcom/twitter/android/livevideo/a;)V
    .locals 3

    .prologue
    .line 216
    invoke-direct {p0, p2}, Laoh;-><init>(Landroid/os/Bundle;)V

    .line 102
    new-instance v0, Lcwv;

    invoke-direct {v0}, Lcwv;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->l:Lcwv;

    .line 106
    new-instance v0, Lcom/twitter/android/livevideo/landing/d$3;

    invoke-direct {v0, p0}, Lcom/twitter/android/livevideo/landing/d$3;-><init>(Lcom/twitter/android/livevideo/landing/d;)V

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->n:Landroid/view/View$OnClickListener;

    .line 114
    new-instance v0, Lcom/twitter/android/livevideo/landing/d$4;

    invoke-direct {v0, p0}, Lcom/twitter/android/livevideo/landing/d$4;-><init>(Lcom/twitter/android/livevideo/landing/d;)V

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->o:Lrx/d;

    .line 146
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->p:Lrx/subjects/PublishSubject;

    .line 148
    new-instance v0, Lcom/twitter/android/livevideo/landing/d$5;

    invoke-direct {v0, p0}, Lcom/twitter/android/livevideo/landing/d$5;-><init>(Lcom/twitter/android/livevideo/landing/d;)V

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->q:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$b;

    .line 160
    new-instance v0, Lcom/twitter/android/livevideo/landing/d$6;

    invoke-direct {v0, p0}, Lcom/twitter/android/livevideo/landing/d$6;-><init>(Lcom/twitter/android/livevideo/landing/d;)V

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->r:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$a;

    .line 174
    new-instance v0, Lcom/twitter/android/livevideo/landing/d$7;

    invoke-direct {v0, p0}, Lcom/twitter/android/livevideo/landing/d$7;-><init>(Lcom/twitter/android/livevideo/landing/d;)V

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->s:Lcom/twitter/android/livevideo/a$a;

    .line 217
    iput-object p1, p0, Lcom/twitter/android/livevideo/landing/d;->d:Lcom/twitter/app/common/base/BaseFragmentActivity;

    .line 218
    iput-object p7, p0, Lcom/twitter/android/livevideo/landing/d;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 220
    invoke-direct {p0, p1}, Lcom/twitter/android/livevideo/landing/d;->a(Landroid/support/v4/app/FragmentActivity;)Landroid/view/View;

    move-result-object v1

    .line 221
    invoke-virtual {p0, v1}, Lcom/twitter/android/livevideo/landing/d;->a(Landroid/view/View;)V

    .line 222
    new-instance v0, Lcom/twitter/android/livevideo/landing/d$a;

    invoke-direct {v0, v1}, Lcom/twitter/android/livevideo/landing/d$a;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    .line 224
    invoke-direct/range {p0 .. p5}, Lcom/twitter/android/livevideo/landing/d;->a(Landroid/support/v4/app/FragmentActivity;Landroid/os/Bundle;Lcom/twitter/app/common/base/d;Lcom/twitter/android/media/selection/c;Lvo;)Lvn;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->c:Lvn;

    .line 227
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->a:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;

    invoke-virtual {v0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->a:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;

    invoke-virtual {v0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->getView()Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->a:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/d;->q:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$b;

    invoke-virtual {v0, v2}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->setOnFullscreenClickListener(Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome$b;)V

    .line 232
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->e:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/d;->r:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$a;

    invoke-virtual {v0, v2}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->setListener(Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$a;)V

    .line 233
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->e:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/d;->d:Lcom/twitter/app/common/base/BaseFragmentActivity;

    .line 234
    invoke-virtual {v2}, Lcom/twitter/app/common/base/BaseFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/android/livevideo/landing/d;->b(Landroid/content/res/Configuration;)F

    move-result v2

    .line 233
    invoke-virtual {v0, v2}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->setAspectRatio(F)V

    .line 236
    iput-object p6, p0, Lcom/twitter/android/livevideo/landing/d;->e:Lcom/twitter/android/livevideo/player/f;

    .line 237
    if-eqz p2, :cond_1

    const-string/jumbo v0, "fullscreen_on_landscape"

    const/4 v2, 0x1

    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/livevideo/landing/d;->j:Z

    .line 238
    if-eqz p2, :cond_3

    const-string/jumbo v0, "state_start_video_on_view_resume"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/android/livevideo/landing/d;->t:Z

    .line 240
    iput-object p8, p0, Lcom/twitter/android/livevideo/landing/d;->h:Lcom/twitter/android/livevideo/landing/b;

    .line 242
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->d:Lcom/twitter/app/common/base/BaseFragmentActivity;

    const-string/jumbo v2, ""

    invoke-virtual {v0, v2}, Lcom/twitter/app/common/base/BaseFragmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 244
    const v0, 0x7f13044f

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/d;->c:Lvn;

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/livevideo/landing/d;->a(ILaog;)V

    .line 246
    iput-object p12, p0, Lcom/twitter/android/livevideo/landing/d;->b:Lcom/twitter/android/livevideo/a;

    .line 247
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->b:Lcom/twitter/android/livevideo/a;

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/d;->s:Lcom/twitter/android/livevideo/a$a;

    invoke-virtual {v0, v2}, Lcom/twitter/android/livevideo/a;->a(Lcom/twitter/android/livevideo/a$a;)V

    .line 248
    new-instance v0, Lcom/twitter/android/livevideo/landing/a;

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    invoke-direct {v0, p9, v2, p10, p2}, Lcom/twitter/android/livevideo/landing/a;-><init>(Lcom/twitter/android/livevideo/landing/a$a;Lcom/twitter/android/livevideo/landing/d$a;Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->k:Lcom/twitter/android/livevideo/landing/a;

    .line 249
    iput-object p11, p0, Lcom/twitter/android/livevideo/landing/d;->i:Lvu;

    .line 250
    new-instance v0, Lvt;

    invoke-direct {v0, p1, v1}, Lvt;-><init>(Landroid/support/v4/app/FragmentActivity;Landroid/view/View;)V

    .line 251
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/d;->i:Lvu;

    invoke-virtual {v1, v0}, Lvu;->a(Lvu$a;)V

    .line 252
    new-instance v1, Lcom/twitter/android/livevideo/landing/d$8;

    invoke-direct {v1, p0}, Lcom/twitter/android/livevideo/landing/d$8;-><init>(Lcom/twitter/android/livevideo/landing/d;)V

    invoke-virtual {v0, v1}, Lvt;->a(Lvt$a;)V

    .line 258
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->i:Lvu;

    invoke-virtual {v0}, Lvu;->b()Lrx/c;

    move-result-object v0

    .line 260
    invoke-virtual {v0}, Lrx/c;->j()Lrx/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/d;->o:Lrx/d;

    .line 261
    invoke-virtual {v1, v2}, Lrx/c;->a(Lrx/d;)Lrx/j;

    move-result-object v1

    .line 262
    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/d;->l:Lcwv;

    invoke-virtual {v2, v1}, Lcwv;->a(Lrx/j;)V

    .line 263
    invoke-direct {p0, p2, v0}, Lcom/twitter/android/livevideo/landing/d;->a(Landroid/os/Bundle;Lrx/c;)Lrx/j;

    move-result-object v0

    .line 264
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/d;->l:Lcwv;

    invoke-virtual {v1, v0}, Lcwv;->a(Lrx/j;)V

    .line 265
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->i:Lvu;

    invoke-virtual {v0}, Lvu;->c()V

    .line 266
    return-void

    .line 237
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 238
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public constructor <init>(Lcom/twitter/app/common/base/BaseFragmentActivity;Lank;Lcom/twitter/app/common/base/d;Lcom/twitter/android/media/selection/c;Lvo;Lcom/twitter/android/livevideo/player/f;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/livevideo/landing/b;Lcom/twitter/android/livevideo/landing/a$a;Lcom/twitter/analytics/feature/model/ClientEventLog;Lvu;Lcom/twitter/android/livevideo/a;)V
    .locals 13

    .prologue
    .line 201
    const-string/jumbo v0, "ViewHost"

    invoke-virtual {p2, v0}, Lank;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    invoke-direct/range {v0 .. v12}, Lcom/twitter/android/livevideo/landing/d;-><init>(Lcom/twitter/app/common/base/BaseFragmentActivity;Landroid/os/Bundle;Lcom/twitter/app/common/base/d;Lcom/twitter/android/media/selection/c;Lvo;Lcom/twitter/android/livevideo/player/f;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/livevideo/landing/b;Lcom/twitter/android/livevideo/landing/a$a;Lcom/twitter/analytics/feature/model/ClientEventLog;Lvu;Lcom/twitter/android/livevideo/a;)V

    .line 204
    return-void
.end method

.method private a(Landroid/support/v4/app/FragmentActivity;)Landroid/view/View;
    .locals 3

    .prologue
    .line 271
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 272
    const v1, 0x7f04017c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/livevideo/landing/d;)Lcom/twitter/android/livevideo/landing/d$a;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    return-object v0
.end method

.method private a(Landroid/os/Bundle;Lrx/c;)Lrx/j;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/livevideo/b;",
            ">;)",
            "Lrx/j;"
        }
    .end annotation

    .prologue
    .line 323
    .line 326
    invoke-static {}, Lcre;->d()Lrx/functions/d;

    move-result-object v0

    invoke-virtual {p2, v0}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/livevideo/landing/d;->a:Lrx/functions/d;

    .line 327
    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 328
    invoke-virtual {v0}, Lrx/c;->k()Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/d;->p:Lrx/subjects/PublishSubject;

    new-instance v2, Lcom/twitter/android/livevideo/landing/d$10;

    invoke-direct {v2, p0}, Lcom/twitter/android/livevideo/landing/d$10;-><init>(Lcom/twitter/android/livevideo/landing/d;)V

    .line 324
    invoke-static {v0, v1, v2}, Lrx/c;->a(Lrx/c;Lrx/c;Lrx/functions/e;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/livevideo/landing/d$9;

    invoke-direct {v1, p0}, Lcom/twitter/android/livevideo/landing/d$9;-><init>(Lcom/twitter/android/livevideo/landing/d;)V

    .line 337
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    .line 346
    invoke-direct {p0, p1}, Lcom/twitter/android/livevideo/landing/d;->b(Landroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 349
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/d;->p:Lrx/subjects/PublishSubject;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/subjects/PublishSubject;->a(Ljava/lang/Object;)V

    .line 352
    :cond_0
    return-object v0
.end method

.method private a(Landroid/support/v4/app/FragmentActivity;Landroid/os/Bundle;Lcom/twitter/app/common/base/d;Lcom/twitter/android/media/selection/c;Lvo;)Lvn;
    .locals 3

    .prologue
    .line 280
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0514

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 281
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/d;->aN_()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f130447

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 282
    new-instance v2, Late;

    invoke-direct {v2, v1, v0}, Late;-><init>(Landroid/view/View;I)V

    .line 283
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/d;->aN_()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f13044f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 284
    new-instance v0, Lvn$a;

    invoke-direct {v0}, Lvn$a;-><init>()V

    .line 285
    invoke-virtual {v0, p2}, Lvn$a;->a(Landroid/os/Bundle;)Lath$a;

    move-result-object v0

    check-cast v0, Lvn$a;

    .line 286
    invoke-virtual {v0, v1}, Lvn$a;->a(Landroid/view/View;)Lath$a;

    move-result-object v0

    check-cast v0, Lvn$a;

    .line 287
    invoke-virtual {v0, p1}, Lvn$a;->a(Landroid/support/v4/app/FragmentActivity;)Lath$a;

    move-result-object v0

    check-cast v0, Lvn$a;

    .line 288
    invoke-virtual {v0, p3}, Lvn$a;->a(Lcom/twitter/app/common/base/d;)Lath$a;

    move-result-object v0

    check-cast v0, Lvn$a;

    .line 289
    invoke-virtual {v0, p4}, Lvn$a;->a(Lcom/twitter/android/media/selection/c;)Lath$a;

    move-result-object v0

    check-cast v0, Lvn$a;

    .line 290
    invoke-virtual {v0, v2}, Lvn$a;->a(Late;)Lath$a;

    move-result-object v0

    check-cast v0, Lvn$a;

    const v1, 0x7f0a06c0

    .line 291
    invoke-virtual {p1, v1}, Landroid/support/v4/app/FragmentActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lvn$a;->a(Ljava/lang/CharSequence;)Lath$a;

    move-result-object v0

    check-cast v0, Lvn$a;

    .line 292
    invoke-virtual {v0, p5}, Lvn$a;->a(Lvo;)Lvn$a;

    move-result-object v0

    .line 293
    invoke-virtual {v0}, Lvn$a;->a()Lvn;

    move-result-object v0

    .line 284
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/livevideo/landing/d;Lcom/twitter/model/livevideo/b;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/twitter/android/livevideo/landing/d;->d(Lcom/twitter/model/livevideo/b;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/livevideo/landing/d;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/twitter/android/livevideo/landing/d;->b(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/twitter/model/livevideo/b;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 382
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->e:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->setEvent(Lcom/twitter/model/livevideo/b;)V

    .line 384
    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    iget-object v0, v0, Lcom/twitter/model/livevideo/a;->d:Lcom/twitter/model/livevideo/BroadcastState;

    sget-object v1, Lcom/twitter/model/livevideo/BroadcastState;->b:Lcom/twitter/model/livevideo/BroadcastState;

    if-ne v0, v1, :cond_0

    .line 385
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->b:Lcom/twitter/internal/android/widget/ToolBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayShowTitleEnabled(Z)V

    .line 386
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->e:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;

    invoke-virtual {v0, v3}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->setVisibility(I)V

    .line 387
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 393
    :goto_0
    return-void

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayShowTitleEnabled(Z)V

    .line 390
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->e:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->setVisibility(I)V

    .line 391
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 464
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 472
    :goto_0
    return-void

    .line 468
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->c:Lvn;

    invoke-virtual {v0, p1}, Lvn;->b(Ljava/lang/String;)V

    .line 470
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->d:Lcom/twitter/app/common/base/BaseFragmentActivity;

    const v1, 0x7f0a01fb

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/base/BaseFragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 471
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/d;->c:Lvn;

    invoke-virtual {v1, v0}, Lvn;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/livevideo/landing/d;Z)Z
    .locals 0

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/twitter/android/livevideo/landing/d;->j:Z

    return p1
.end method

.method private static b(Landroid/content/res/Configuration;)F
    .locals 2

    .prologue
    .line 443
    iget v0, p0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/high16 v0, 0x40800000    # 4.0f

    :goto_0
    return v0

    :cond_0
    const v0, 0x3fe38e39

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/livevideo/landing/d;)Lcom/twitter/app/common/base/BaseFragmentActivity;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->d:Lcom/twitter/app/common/base/BaseFragmentActivity;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/livevideo/landing/d;Lcom/twitter/model/livevideo/b;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/twitter/android/livevideo/landing/d;->a(Lcom/twitter/model/livevideo/b;)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/livevideo/landing/d;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/twitter/android/livevideo/landing/d;->a(Ljava/lang/String;)V

    return-void
.end method

.method private b(Lcom/twitter/model/livevideo/b;)V
    .locals 3

    .prologue
    .line 396
    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    if-nez v0, :cond_1

    .line 424
    :cond_0
    :goto_0
    return-void

    .line 400
    :cond_1
    invoke-direct {p0, p1}, Lcom/twitter/android/livevideo/landing/d;->c(Lcom/twitter/model/livevideo/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->m:Lcom/twitter/model/livevideo/a;

    .line 405
    new-instance v0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;

    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/d;->h:Lcom/twitter/android/livevideo/landing/b;

    iget-object v1, v1, Lcom/twitter/android/livevideo/landing/b;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/d;->h:Lcom/twitter/android/livevideo/landing/b;

    iget-object v2, v2, Lcom/twitter/android/livevideo/landing/b;->c:Lcom/twitter/model/core/Tweet;

    invoke-direct {v0, v1, p1, v2}, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;-><init>(Ljava/lang/String;Lcom/twitter/model/livevideo/b;Lcom/twitter/model/core/Tweet;)V

    .line 407
    new-instance v1, Lcom/twitter/android/av/video/f$a;

    invoke-direct {v1}, Lcom/twitter/android/av/video/f$a;-><init>()V

    .line 408
    invoke-virtual {v1, v0}, Lcom/twitter/android/av/video/f$a;->a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/android/av/video/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/d;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 409
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/video/f$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/av/video/f$a;

    move-result-object v0

    sget-object v1, Lbyo;->g:Lbyf;

    .line 410
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/video/f$a;->a(Lbyf;)Lcom/twitter/android/av/video/f$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/av/VideoPlayerView$Mode;->l:Lcom/twitter/library/av/VideoPlayerView$Mode;

    .line 411
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/video/f$a;->a(Lcom/twitter/library/av/VideoPlayerView$Mode;)Lcom/twitter/android/av/video/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v1, v1, Lcom/twitter/android/livevideo/landing/d$a;->a:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;

    .line 412
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/video/f$a;->a(Lcom/twitter/library/av/control/e;)Lcom/twitter/android/av/video/f$a;

    move-result-object v0

    const v1, 0x3fe38e39

    .line 413
    invoke-static {v1}, Lcom/twitter/library/av/model/b;->a(F)Lcom/twitter/library/av/model/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/video/f$a;->a(Lcom/twitter/library/av/model/b;)Lcom/twitter/android/av/video/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/d;->n:Landroid/view/View$OnClickListener;

    .line 414
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/video/f$a;->a(Landroid/view/View$OnClickListener;)Lcom/twitter/android/av/video/f$a;

    move-result-object v0

    .line 415
    invoke-virtual {v0}, Lcom/twitter/android/av/video/f$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/video/f;

    .line 416
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v1, v1, Lcom/twitter/android/livevideo/landing/d$a;->d:Lcom/twitter/android/av/video/VideoContainerHost;

    invoke-virtual {v1, v0}, Lcom/twitter/android/av/video/VideoContainerHost;->setVideoContainerConfig(Lcom/twitter/android/av/video/f;)V

    .line 417
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->d:Lcom/twitter/android/av/video/VideoContainerHost;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/VideoContainerHost;->getAutoPlayableItem()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->at_()V

    .line 418
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->d:Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-virtual {v0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/livevideo/landing/d;->b(Landroid/content/res/Configuration;)F

    move-result v0

    .line 419
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v1, v1, Lcom/twitter/android/livevideo/landing/d$a;->d:Lcom/twitter/android/av/video/VideoContainerHost;

    invoke-virtual {v1, v0}, Lcom/twitter/android/av/video/VideoContainerHost;->setAspectRatio(F)V

    .line 420
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->a:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;

    invoke-virtual {v0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 421
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->a:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;

    invoke-virtual {v0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->getView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 423
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->k:Lcom/twitter/android/livevideo/landing/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/livevideo/landing/a;->a(Z)V

    goto/16 :goto_0
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 475
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 479
    :goto_0
    return-void

    .line 478
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->c:Lvn;

    invoke-static {p1}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lvn;->a_(Ljava/util/List;)V

    goto :goto_0
.end method

.method private b(Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 377
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->d:Lcom/twitter/app/common/base/BaseFragmentActivity;

    .line 378
    invoke-virtual {v0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 377
    :goto_0
    return v0

    .line 378
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/livevideo/landing/d;)Lcom/twitter/android/livevideo/a;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->b:Lcom/twitter/android/livevideo/a;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/livevideo/landing/d;Lcom/twitter/model/livevideo/b;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/twitter/android/livevideo/landing/d;->b(Lcom/twitter/model/livevideo/b;)V

    return-void
.end method

.method private c(Lcom/twitter/model/livevideo/b;)Z
    .locals 2

    .prologue
    .line 432
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->m:Lcom/twitter/model/livevideo/a;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    iget-object v0, v0, Lcom/twitter/model/livevideo/a;->d:Lcom/twitter/model/livevideo/BroadcastState;

    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/d;->m:Lcom/twitter/model/livevideo/a;

    iget-object v1, v1, Lcom/twitter/model/livevideo/a;->d:Lcom/twitter/model/livevideo/BroadcastState;

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/android/livevideo/landing/d;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/twitter/android/livevideo/landing/d;->h()V

    return-void
.end method

.method private d(Lcom/twitter/model/livevideo/b;)V
    .locals 3

    .prologue
    .line 449
    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    if-nez v0, :cond_0

    .line 461
    :goto_0
    return-void

    .line 454
    :cond_0
    new-instance v0, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;

    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/d;->h:Lcom/twitter/android/livevideo/landing/b;

    iget-object v1, v1, Lcom/twitter/android/livevideo/landing/b;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/d;->h:Lcom/twitter/android/livevideo/landing/b;

    iget-object v2, v2, Lcom/twitter/android/livevideo/landing/b;->c:Lcom/twitter/model/core/Tweet;

    invoke-direct {v0, v1, p1, v2}, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;-><init>(Ljava/lang/String;Lcom/twitter/model/livevideo/b;Lcom/twitter/model/core/Tweet;)V

    .line 456
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/d;->e:Lcom/twitter/android/livevideo/player/f;

    .line 457
    invoke-virtual {v1, p1}, Lcom/twitter/android/livevideo/player/f;->a(Lcom/twitter/model/livevideo/b;)Lcom/twitter/android/livevideo/player/f;

    move-result-object v1

    .line 458
    invoke-virtual {v1, v0}, Lcom/twitter/android/livevideo/player/f;->a(Lcom/twitter/library/av/playback/AVDataSource;)Lcom/twitter/library/av/ab;

    move-result-object v0

    const/4 v1, 0x1

    .line 459
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/ab;->c(Z)Lcom/twitter/library/av/ab;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/d;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 460
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/ab;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/av/ab;

    goto :goto_0
.end method

.method static synthetic e(Lcom/twitter/android/livevideo/landing/d;)Lrx/subjects/PublishSubject;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->p:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/livevideo/landing/d;)Lvu;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->i:Lvu;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/livevideo/landing/d;)Lcom/twitter/android/livevideo/player/f;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->e:Lcom/twitter/android/livevideo/player/f;

    return-object v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 362
    iget-boolean v0, p0, Lcom/twitter/android/livevideo/landing/d;->t:Z

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->d:Lcom/twitter/android/av/video/VideoContainerHost;

    new-instance v1, Lcom/twitter/android/livevideo/landing/d$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/livevideo/landing/d$2;-><init>(Lcom/twitter/android/livevideo/landing/d;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/video/VideoContainerHost;->post(Ljava/lang/Runnable;)Z

    .line 370
    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 436
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->d:Lcom/twitter/android/av/video/VideoContainerHost;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/VideoContainerHost;->getAutoPlayableItem()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->au_()V

    .line 437
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->d:Lcom/twitter/android/av/video/VideoContainerHost;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/VideoContainerHost;->a()V

    .line 438
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->m:Lcom/twitter/model/livevideo/a;

    .line 439
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->k:Lcom/twitter/android/livevideo/landing/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/livevideo/landing/a;->a(Z)V

    .line 440
    return-void
.end method


# virtual methods
.method public a(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 483
    invoke-super {p0, p1}, Laoh;->a(Landroid/content/res/Configuration;)V

    .line 484
    iget-boolean v0, p0, Lcom/twitter/android/livevideo/landing/d;->j:Z

    if-eqz v0, :cond_1

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->k:Lcom/twitter/android/livevideo/landing/a;

    .line 485
    invoke-virtual {v0}, Lcom/twitter/android/livevideo/landing/a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 486
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->p:Lrx/subjects/PublishSubject;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject;->a(Ljava/lang/Object;)V

    .line 492
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->d:Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-virtual {v0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 493
    const v1, 0x7f0e0014

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 494
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v1, v1, Lcom/twitter/android/livevideo/landing/d$a;->c:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setMinimumHeight(I)V

    .line 496
    invoke-static {p1}, Lcom/twitter/android/livevideo/landing/d;->b(Landroid/content/res/Configuration;)F

    move-result v0

    .line 497
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v1, v1, Lcom/twitter/android/livevideo/landing/d$a;->d:Lcom/twitter/android/av/video/VideoContainerHost;

    invoke-virtual {v1, v0}, Lcom/twitter/android/av/video/VideoContainerHost;->setAspectRatio(F)V

    .line 498
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v1, v1, Lcom/twitter/android/livevideo/landing/d$a;->e:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;

    invoke-virtual {v1, v0}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->setAspectRatio(F)V

    .line 499
    return-void

    .line 487
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/livevideo/landing/d;->j:Z

    if-nez v0, :cond_0

    .line 488
    iput-boolean v2, p0, Lcom/twitter/android/livevideo/landing/d;->j:Z

    goto :goto_0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 511
    const-string/jumbo v0, "fullscreen_on_landscape"

    iget-boolean v1, p0, Lcom/twitter/android/livevideo/landing/d;->j:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 512
    const-string/jumbo v0, "state_start_video_on_view_resume"

    iget-boolean v1, p0, Lcom/twitter/android/livevideo/landing/d;->t:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 513
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->k:Lcom/twitter/android/livevideo/landing/a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/livevideo/landing/a;->a(Landroid/os/Bundle;)V

    .line 514
    invoke-super {p0, p1}, Laoh;->a(Landroid/os/Bundle;)V

    .line 515
    return-void
.end method

.method public aO_()V
    .locals 0

    .prologue
    .line 310
    invoke-super {p0}, Laoh;->aO_()V

    .line 311
    invoke-direct {p0}, Lcom/twitter/android/livevideo/landing/d;->g()V

    .line 312
    return-void
.end method

.method public ak_()V
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->g:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->a:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;

    invoke-virtual {v0}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->k()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/livevideo/landing/d;->t:Z

    .line 317
    invoke-super {p0}, Laoh;->ak_()V

    .line 318
    return-void
.end method

.method public am_()V
    .locals 1

    .prologue
    .line 303
    invoke-super {p0}, Laoh;->am_()V

    .line 304
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->l:Lcwv;

    invoke-virtual {v0}, Lcwv;->B_()V

    .line 305
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->i:Lvu;

    invoke-virtual {v0}, Lvu;->a()V

    .line 306
    return-void
.end method

.method public e()Lvn;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->c:Lvn;

    return-object v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 506
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d;->k:Lcom/twitter/android/livevideo/landing/a;

    invoke-virtual {v0}, Lcom/twitter/android/livevideo/landing/a;->b()V

    .line 507
    return-void
.end method
