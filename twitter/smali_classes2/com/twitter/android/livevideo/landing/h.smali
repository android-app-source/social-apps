.class Lcom/twitter/android/livevideo/landing/h;
.super Lcom/twitter/android/ck;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/scribe/n;


# direct methods
.method constructor <init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/scribe/n;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/ck;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 24
    iput-object p3, p0, Lcom/twitter/android/livevideo/landing/h;->a:Lcom/twitter/library/scribe/n;

    .line 25
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 7

    .prologue
    .line 30
    invoke-static {p3}, Lcom/twitter/model/core/Tweet;->b(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v6

    .line 31
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    .line 32
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/h;->f:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/livevideo/landing/h;->a:Lcom/twitter/library/scribe/n;

    iget-object v4, p0, Lcom/twitter/android/livevideo/landing/h;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 33
    invoke-virtual {p0, p3}, Lcom/twitter/android/livevideo/landing/h;->a(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v5

    move-object v2, p3

    .line 32
    invoke-static/range {v0 .. v5}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/scribe/n;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)V

    .line 34
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/livevideo/landing/h;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-static {v3, v6, p1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/h;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 36
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 37
    invoke-virtual {v0, p4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 34
    return-object v0
.end method
