.class Lcom/twitter/android/livevideo/landing/d$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/livevideo/landing/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field public final a:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;

.field public final b:Lcom/twitter/internal/android/widget/ToolBar;

.field public final c:Landroid/view/ViewGroup;

.field public final d:Lcom/twitter/android/av/video/VideoContainerHost;

.field public final e:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;

.field public final f:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 531
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 532
    const v0, 0x7f13007f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/d$a;->b:Lcom/twitter/internal/android/widget/ToolBar;

    .line 533
    const v0, 0x7f13044a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/video/VideoContainerHost;

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/d$a;->d:Lcom/twitter/android/av/video/VideoContainerHost;

    .line 534
    const v0, 0x7f130449

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/d$a;->c:Landroid/view/ViewGroup;

    .line 535
    const v0, 0x7f13044b

    .line 536
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/d$a;->a:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;

    .line 537
    const v0, 0x7f13044c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/d$a;->e:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;

    .line 538
    const v0, 0x7f130448

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/d$a;->f:Landroid/view/View;

    .line 539
    return-void
.end method
