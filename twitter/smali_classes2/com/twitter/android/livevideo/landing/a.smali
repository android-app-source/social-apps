.class public Lcom/twitter/android/livevideo/landing/a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/livevideo/landing/a$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/livevideo/landing/a$a;

.field private final b:Lcom/twitter/android/livevideo/landing/d$a;

.field private final c:Lcom/twitter/analytics/feature/model/ClientEventLog;

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/livevideo/landing/a$a;Lcom/twitter/android/livevideo/landing/d$a;Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-boolean v1, p0, Lcom/twitter/android/livevideo/landing/a;->d:Z

    .line 33
    iput-object p1, p0, Lcom/twitter/android/livevideo/landing/a;->a:Lcom/twitter/android/livevideo/landing/a$a;

    .line 34
    iput-object p2, p0, Lcom/twitter/android/livevideo/landing/a;->b:Lcom/twitter/android/livevideo/landing/d$a;

    .line 35
    iput-object p3, p0, Lcom/twitter/android/livevideo/landing/a;->c:Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 36
    if-eqz p4, :cond_0

    .line 37
    const-string/jumbo v0, "state_hide_video_header"

    invoke-virtual {p4, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/livevideo/landing/a;->d:Z

    .line 39
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 5

    .prologue
    .line 80
    if-eqz p1, :cond_0

    const-string/jumbo v0, "show_video_button"

    .line 81
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/a;->c:Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "live_video_timeline"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, ""

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, ""

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object v0, v2, v3

    const/4 v0, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v2, v0

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 82
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/a;->c:Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 83
    return-void

    .line 80
    :cond_0
    const-string/jumbo v0, "hide_video_button"

    goto :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 86
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/a;->b:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 87
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/a;->b:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->c:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 88
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/a;->b:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->d:Lcom/twitter/android/av/video/VideoContainerHost;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/VideoContainerHost;->getAutoPlayableItem()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->at_()V

    .line 89
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/a;->a:Lcom/twitter/android/livevideo/landing/a$a;

    const v1, 0x7f0a04a8

    invoke-interface {v0, v1}, Lcom/twitter/android/livevideo/landing/a$a;->a(I)V

    .line 90
    iput-boolean v2, p0, Lcom/twitter/android/livevideo/landing/a;->d:Z

    .line 91
    invoke-direct {p0, v2}, Lcom/twitter/android/livevideo/landing/a;->b(Z)V

    .line 92
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 95
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/a;->b:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->d:Lcom/twitter/android/av/video/VideoContainerHost;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/VideoContainerHost;->getAutoPlayableItem()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->au_()V

    .line 96
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/a;->b:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/a;->b:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->c:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/a;->a:Lcom/twitter/android/livevideo/landing/a$a;

    const v1, 0x7f0a04b1

    invoke-interface {v0, v1}, Lcom/twitter/android/livevideo/landing/a$a;->a(I)V

    .line 99
    iput-boolean v2, p0, Lcom/twitter/android/livevideo/landing/a;->d:Z

    .line 100
    invoke-direct {p0, v2}, Lcom/twitter/android/livevideo/landing/a;->b(Z)V

    .line 101
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 74
    const-string/jumbo v0, "state_hide_video_header"

    iget-boolean v1, p0, Lcom/twitter/android/livevideo/landing/a;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 75
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/twitter/android/livevideo/landing/a;->d:Z

    if-nez v0, :cond_0

    .line 56
    invoke-direct {p0}, Lcom/twitter/android/livevideo/landing/a;->d()V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/a;->a:Lcom/twitter/android/livevideo/landing/a$a;

    invoke-interface {v0, p1}, Lcom/twitter/android/livevideo/landing/a$a;->a(Z)V

    .line 59
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/twitter/android/livevideo/landing/a;->d:Z

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/a;->b:Lcom/twitter/android/livevideo/landing/d$a;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 66
    :goto_0
    if-eqz v0, :cond_1

    .line 67
    invoke-direct {p0}, Lcom/twitter/android/livevideo/landing/a;->d()V

    .line 71
    :goto_1
    return-void

    .line 65
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 69
    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/livevideo/landing/a;->c()V

    goto :goto_1
.end method
