.class Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation


# instance fields
.field public final a:Lcom/twitter/media/ui/image/MediaImageView;

.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/widget/TextView;

.field public final e:Lcom/twitter/ui/widget/ToggleTwitterButton;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208
    const v0, 0x7f130451

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;->a:Lcom/twitter/media/ui/image/MediaImageView;

    .line 209
    const v0, 0x7f130452

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;->b:Landroid/widget/TextView;

    .line 210
    const v0, 0x7f130453

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;->c:Landroid/widget/TextView;

    .line 211
    const v0, 0x7f130454

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;->d:Landroid/widget/TextView;

    .line 212
    const v0, 0x7f130455

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/ToggleTwitterButton;

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;->e:Lcom/twitter/ui/widget/ToggleTwitterButton;

    .line 213
    return-void
.end method
