.class public final Lcom/twitter/android/livevideo/landing/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcsd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcsd",
        "<",
        "Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/d;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/landing/d;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lvj;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/media/selection/c;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/landing/b;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/ClientEventLog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/twitter/android/livevideo/landing/c;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/livevideo/landing/c;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/d;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/landing/d;",
            ">;",
            "Lcta",
            "<",
            "Lvj;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/media/selection/c;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/landing/b;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/ClientEventLog;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/c;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 37
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/livevideo/landing/c;->b:Lcta;

    .line 38
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/c;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 39
    :cond_1
    iput-object p2, p0, Lcom/twitter/android/livevideo/landing/c;->c:Lcta;

    .line 40
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/c;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 41
    :cond_2
    iput-object p3, p0, Lcom/twitter/android/livevideo/landing/c;->d:Lcta;

    .line 42
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/c;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 43
    :cond_3
    iput-object p4, p0, Lcom/twitter/android/livevideo/landing/c;->e:Lcta;

    .line 44
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/c;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 45
    :cond_4
    iput-object p5, p0, Lcom/twitter/android/livevideo/landing/c;->f:Lcta;

    .line 46
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/c;->a:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 47
    :cond_5
    iput-object p6, p0, Lcom/twitter/android/livevideo/landing/c;->g:Lcta;

    .line 48
    return-void
.end method

.method public static a(Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)Lcsd;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/d;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/landing/d;",
            ">;",
            "Lcta",
            "<",
            "Lvj;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/media/selection/c;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/livevideo/landing/b;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/ClientEventLog;",
            ">;)",
            "Lcsd",
            "<",
            "Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    new-instance v0, Lcom/twitter/android/livevideo/landing/c;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/livevideo/landing/c;-><init>(Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;)V
    .locals 2

    .prologue
    .line 68
    if-nez p1, :cond_0

    .line 69
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/c;->b:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/livevideo/d;

    iput-object v0, p1, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->b:Lcom/twitter/android/livevideo/d;

    .line 72
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/c;->c:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/livevideo/landing/d;

    iput-object v0, p1, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->c:Lcom/twitter/android/livevideo/landing/d;

    .line 73
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/c;->d:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvj;

    iput-object v0, p1, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->d:Lvj;

    .line 74
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/c;->e:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/selection/c;

    iput-object v0, p1, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->e:Lcom/twitter/android/media/selection/c;

    .line 75
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/c;->f:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/livevideo/landing/b;

    iput-object v0, p1, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->f:Lcom/twitter/android/livevideo/landing/b;

    .line 76
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/c;->g:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iput-object v0, p1, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->g:Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 77
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 11
    check-cast p1, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;

    invoke-virtual {p0, p1}, Lcom/twitter/android/livevideo/landing/c;->a(Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;)V

    return-void
.end method
