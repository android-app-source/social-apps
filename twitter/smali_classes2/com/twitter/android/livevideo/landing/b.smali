.class public Lcom/twitter/android/livevideo/landing/b;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final d:Ljava/lang/String;

.field private static final e:Ljava/lang/String;

.field private static final f:Ljava/lang/String;

.field private static final g:Ljava/lang/String;


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:Lcom/twitter/model/core/Tweet;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    const-class v0, Lcom/twitter/android/livevideo/landing/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/livevideo/landing/b;->d:Ljava/lang/String;

    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/twitter/android/livevideo/landing/b;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":event_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/livevideo/landing/b;->e:Ljava/lang/String;

    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/twitter/android/livevideo/landing/b;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":data_source_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/livevideo/landing/b;->f:Ljava/lang/String;

    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/twitter/android/livevideo/landing/b;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":tweet"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/livevideo/landing/b;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 42
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/livevideo/landing/b;-><init>(JLjava/lang/String;)V

    .line 43
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/android/livevideo/landing/b;-><init>(JLjava/lang/String;Lcom/twitter/model/core/Tweet;)V

    .line 50
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Lcom/twitter/model/core/Tweet;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-wide p1, p0, Lcom/twitter/android/livevideo/landing/b;->a:J

    .line 60
    iput-object p3, p0, Lcom/twitter/android/livevideo/landing/b;->b:Ljava/lang/String;

    .line 61
    iput-object p4, p0, Lcom/twitter/android/livevideo/landing/b;->c:Lcom/twitter/model/core/Tweet;

    .line 62
    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/twitter/android/livevideo/landing/b;
    .locals 5

    .prologue
    .line 74
    sget-object v0, Lcom/twitter/android/livevideo/landing/b;->e:Ljava/lang/String;

    const-wide/16 v2, -0x1

    invoke-virtual {p0, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 75
    sget-object v0, Lcom/twitter/android/livevideo/landing/b;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 76
    sget-object v0, Lcom/twitter/android/livevideo/landing/b;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 77
    new-instance v4, Lcom/twitter/android/livevideo/landing/b;

    invoke-direct {v4, v2, v3, v1, v0}, Lcom/twitter/android/livevideo/landing/b;-><init>(JLjava/lang/String;Lcom/twitter/model/core/Tweet;)V

    return-object v4
.end method

.method public static a(Lbrc;)Lcom/twitter/android/livevideo/landing/b;
    .locals 4

    .prologue
    .line 67
    invoke-virtual {p0}, Lbrc;->c()J

    move-result-wide v0

    .line 68
    invoke-virtual {p0}, Lbrc;->o()Ljava/lang/String;

    move-result-object v2

    .line 69
    new-instance v3, Lcom/twitter/android/livevideo/landing/b;

    invoke-direct {v3, v0, v1, v2}, Lcom/twitter/android/livevideo/landing/b;-><init>(JLjava/lang/String;)V

    return-object v3
.end method

.method public static a(Lcom/twitter/app/common/base/b;)Lcom/twitter/android/livevideo/landing/b;
    .locals 5

    .prologue
    .line 82
    sget-object v0, Lcom/twitter/android/livevideo/landing/b;->e:Ljava/lang/String;

    const-wide/16 v2, -0x1

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 83
    sget-object v0, Lcom/twitter/android/livevideo/landing/b;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/base/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 84
    sget-object v0, Lcom/twitter/android/livevideo/landing/b;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/base/b;->h(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 85
    new-instance v4, Lcom/twitter/android/livevideo/landing/b;

    invoke-direct {v4, v2, v3, v1, v0}, Lcom/twitter/android/livevideo/landing/b;-><init>(JLjava/lang/String;Lcom/twitter/model/core/Tweet;)V

    return-object v4
.end method


# virtual methods
.method public a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 90
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 91
    sget-object v1, Lcom/twitter/android/livevideo/landing/b;->e:Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/android/livevideo/landing/b;->a:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 92
    sget-object v1, Lcom/twitter/android/livevideo/landing/b;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    sget-object v1, Lcom/twitter/android/livevideo/landing/b;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/b;->c:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 94
    return-object v0
.end method

.method public a(Lcom/twitter/android/ai$a;)Lcom/twitter/android/ai;
    .locals 4

    .prologue
    .line 99
    sget-object v0, Lcom/twitter/android/livevideo/landing/b;->e:Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/android/livevideo/landing/b;->a:J

    .line 100
    invoke-virtual {p1, v0, v2, v3}, Lcom/twitter/android/ai$a;->a(Ljava/lang/String;J)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ai$a;

    sget-object v1, Lcom/twitter/android/livevideo/landing/b;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/b;->b:Ljava/lang/String;

    .line 101
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/ai$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ai$a;

    sget-object v1, Lcom/twitter/android/livevideo/landing/b;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/b;->c:Lcom/twitter/model/core/Tweet;

    .line 102
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/ai$a;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ai$a;

    .line 103
    invoke-virtual {v0}, Lcom/twitter/android/ai$a;->a()Lcom/twitter/android/ai;

    move-result-object v0

    .line 99
    return-object v0
.end method
