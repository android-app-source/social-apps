.class public Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;
.super Lcom/twitter/android/ActivityWithProgress;
.source "Twttr"

# interfaces
.implements Lbrp;
.implements Lcom/twitter/android/livevideo/landing/a$a;
.implements Lvo;


# instance fields
.field b:Lcom/twitter/android/livevideo/d;

.field c:Lcom/twitter/android/livevideo/landing/d;

.field d:Lvj;

.field e:Lcom/twitter/android/media/selection/c;

.field f:Lcom/twitter/android/livevideo/landing/b;

.field g:Lcom/twitter/analytics/feature/model/ClientEventLog;

.field private h:Lvn;

.field private i:Lcom/twitter/android/livevideo/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/twitter/android/ActivityWithProgress;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/android/livevideo/landing/b;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p1, p0}, Lcom/twitter/android/livevideo/landing/b;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 146
    if-eqz p1, :cond_0

    .line 147
    invoke-static {p1}, Lcom/twitter/android/util/j;->a(Landroid/content/Intent;)Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v0

    .line 148
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 149
    new-instance v1, Lcom/twitter/android/media/selection/MediaAttachment;

    invoke-direct {v1, v0}, Lcom/twitter/android/media/selection/MediaAttachment;-><init>(Lcom/twitter/model/drafts/DraftAttachment;)V

    .line 150
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->d:Lvj;

    invoke-interface {v0, v1}, Lvj;->a(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 152
    :cond_0
    sget-object v0, Lcom/twitter/android/composer/ComposerType;->b:Lcom/twitter/android/composer/ComposerType;

    invoke-static {v0, p1}, Lcom/twitter/android/util/j;->a(Lcom/twitter/android/composer/ComposerType;Landroid/content/Intent;)V

    .line 153
    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    const/16 v1, 0x80

    .line 213
    if-eqz p1, :cond_0

    .line 214
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 218
    :goto_0
    return-void

    .line 216
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method

.method private c(I)Lcmm;
    .locals 1

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->c()Lcmr;

    move-result-object v0

    .line 206
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0, p1}, Lcmr;->b(I)Lcmm;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    .line 85
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->b(Z)V

    .line 86
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ActivityWithProgress;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    .line 87
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(I)V

    .line 88
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 89
    return-object v0
.end method

.method protected synthetic a(Lank;)Lcom/twitter/app/common/base/j;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->d(Lank;)Luf;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 248
    const v0, 0x7f13089d

    invoke-direct {p0, v0}, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->c(I)Lcmm;

    move-result-object v0

    .line 249
    if-eqz v0, :cond_0

    .line 250
    invoke-interface {v0, p1}, Lcmm;->g(I)Lcmm;

    .line 252
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 133
    invoke-static {p0, p1, p2, p3}, Landroid/support/v4/app/ActivityCompat;->startActivityForResult(Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 134
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 237
    const v0, 0x7f13089d

    invoke-direct {p0, v0}, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->c(I)Lcmm;

    move-result-object v0

    .line 238
    if-eqz v0, :cond_0

    .line 239
    invoke-interface {v0, p1}, Lcmm;->f(Z)Lcmm;

    .line 243
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->b(Z)V

    .line 244
    return-void
.end method

.method public a(Lcmm;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 185
    invoke-interface {p1}, Lcmm;->a()I

    move-result v1

    .line 186
    const v2, 0x7f130043

    if-ne v1, v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->h:Lvn;

    invoke-virtual {v2}, Lvn;->aK_()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 187
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->h:Lvn;

    invoke-virtual {v1}, Lvn;->aG_()V

    .line 200
    :goto_0
    return v0

    .line 189
    :cond_0
    const v2, 0x7f1308bc

    if-ne v2, v1, :cond_1

    .line 190
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->b:Lcom/twitter/android/livevideo/d;

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->f:Lcom/twitter/android/livevideo/landing/b;

    iget-wide v2, v2, Lcom/twitter/android/livevideo/landing/b;->a:J

    invoke-virtual {v1, p0, v2, v3}, Lcom/twitter/android/livevideo/d;->a(Landroid/content/Context;J)V

    goto :goto_0

    .line 192
    :cond_1
    const v2, 0x7f13005e

    if-ne v2, v1, :cond_2

    .line 194
    const-string/jumbo v1, "live_video_hide_video_android_5401"

    invoke-static {v1}, Lcoi;->b(Ljava/lang/String;)Z

    goto :goto_0

    .line 196
    :cond_2
    const v2, 0x7f13089d

    if-ne v2, v1, :cond_3

    .line 197
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->c:Lcom/twitter/android/livevideo/landing/d;

    invoke-virtual {v1}, Lcom/twitter/android/livevideo/landing/d;->f()V

    goto :goto_0

    .line 200
    :cond_3
    invoke-super {p0, p1}, Lcom/twitter/android/ActivityWithProgress;->a(Lcmm;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 166
    invoke-super {p0, p1}, Lcom/twitter/android/ActivityWithProgress;->a(Lcmr;)Z

    .line 167
    const v0, 0x7f14002f

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 168
    const-string/jumbo v0, "live_video_hide_video_android_5401"

    invoke-static {v0}, Lcoi;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    const v0, 0x7f140013

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 171
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public b(Lcmr;)I
    .locals 2

    .prologue
    .line 157
    const v0, 0x7f13088d

    invoke-interface {p1, v0}, Lcmr;->b(I)Lcmm;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_0

    .line 159
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcmm;->f(Z)Lcmm;

    .line 161
    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/ActivityWithProgress;->b(Lcmr;)I

    move-result v0

    return v0
.end method

.method protected synthetic b(Lank;)Lcom/twitter/app/common/abs/b;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->d(Lank;)Luf;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 222
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->m_()V

    .line 223
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->d:Lvj;

    invoke-interface {v0, p1}, Lvj;->a(I)V

    .line 228
    return-void
.end method

.method protected synthetic c(Lank;)Lans;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->d(Lank;)Luf;

    move-result-object v0

    return-object v0
.end method

.method protected d(Lank;)Luf;
    .locals 3

    .prologue
    .line 108
    new-instance v0, Lug;

    .line 109
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->V()Lcom/twitter/app/common/base/d;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->i:Lcom/twitter/android/livevideo/a;

    invoke-direct {v0, p0, p1, v1, v2}, Lug;-><init>(Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;Lank;Lcom/twitter/app/common/base/d;Lcom/twitter/android/livevideo/a;)V

    .line 112
    invoke-static {}, Luc;->c()Luc$a;

    move-result-object v1

    .line 113
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v2

    invoke-virtual {v1, v2}, Luc$a;->a(Lamu;)Luc$a;

    move-result-object v1

    .line 114
    invoke-virtual {v1, v0}, Luc$a;->a(Lug;)Luc$a;

    move-result-object v0

    .line 115
    invoke-virtual {v0}, Luc$a;->a()Luf;

    move-result-object v0

    .line 117
    invoke-interface {v0, p0}, Luf;->a(Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;)V

    .line 119
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->c:Lcom/twitter/android/livevideo/landing/d;

    invoke-virtual {v1}, Lcom/twitter/android/livevideo/landing/d;->e()Lvn;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->h:Lvn;

    .line 120
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->h:Lvn;

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->d:Lvj;

    invoke-virtual {v1, v2}, Lvn;->a(Latg$a;)V

    .line 122
    return-object v0
.end method

.method protected e(Lank;)Lcom/twitter/app/common/base/i;
    .locals 3

    .prologue
    .line 95
    new-instance v0, Luy;

    invoke-direct {v0, p1}, Luy;-><init>(Lank;)V

    .line 97
    invoke-static {}, Lud;->a()Lud$a;

    move-result-object v1

    .line 98
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v2

    invoke-virtual {v1, v2}, Lud$a;->a(Lamu;)Lud$a;

    move-result-object v1

    .line 99
    invoke-virtual {v1, v0}, Lud$a;->a(Luy;)Lud$a;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Lud$a;->a()Lux;

    move-result-object v0

    .line 101
    invoke-interface {v0}, Lux;->d()Lcom/twitter/android/livevideo/a;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->i:Lcom/twitter/android/livevideo/a;

    .line 102
    return-object v0
.end method

.method protected synthetic f(Lank;)Lcom/twitter/app/common/abs/a;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->e(Lank;)Lcom/twitter/app/common/base/i;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic g(Lank;)Lann;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->e(Lank;)Lcom/twitter/app/common/base/i;

    move-result-object v0

    return-object v0
.end method

.method public i()V
    .locals 2

    .prologue
    .line 232
    const/4 v0, 0x0

    sget-object v1, Lcom/twitter/android/composer/ComposerType;->b:Lcom/twitter/android/composer/ComposerType;

    invoke-static {p0, v0, v1}, Lcom/twitter/android/util/j;->a(Landroid/app/Activity;ILcom/twitter/android/composer/ComposerType;)V

    .line 233
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 138
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/ActivityWithProgress;->onActivityResult(IILandroid/content/Intent;)V

    .line 139
    if-nez p1, :cond_0

    const/4 v0, -0x1

    if-ne v0, p2, :cond_0

    .line 140
    invoke-direct {p0, p3}, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->a(Landroid/content/Intent;)V

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->e:Lcom/twitter/android/media/selection/c;

    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->h:Lvn;

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/twitter/android/media/selection/c;->a(IILandroid/content/Intent;Lcom/twitter/android/media/selection/a;)V

    .line 143
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->d:Lvj;

    invoke-interface {v0}, Lvj;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    :goto_0
    return-void

    .line 180
    :cond_0
    invoke-super {p0}, Lcom/twitter/android/ActivityWithProgress;->onBackPressed()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 127
    invoke-super {p0, p1}, Lcom/twitter/android/ActivityWithProgress;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 128
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->i:Lcom/twitter/android/livevideo/a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/livevideo/a;->a(Landroid/os/Bundle;)V

    .line 129
    return-void
.end method
