.class public Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;
.super Lcom/twitter/media/ui/image/AspectRatioFrameLayout;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/livevideo/landing/i$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;,
        Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$a;
    }
.end annotation


# instance fields
.field final a:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;

.field private final b:Lcom/twitter/android/livevideo/landing/i;

.field private d:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$a;

.field private final e:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    new-instance v0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$1;-><init>(Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;)V

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->e:Landroid/view/View$OnClickListener;

    .line 59
    const v0, 0x7f04017d

    invoke-static {p1, v0, p0}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 60
    new-instance v1, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;

    invoke-direct {v1, v0}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->a:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;

    .line 61
    new-instance v0, Lcom/twitter/android/livevideo/landing/i;

    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/livevideo/landing/i;-><init>(Landroid/content/res/Resources;Lrx/f;)V

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->b:Lcom/twitter/android/livevideo/landing/i;

    .line 62
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->b:Lcom/twitter/android/livevideo/landing/i;

    invoke-virtual {v0, p0}, Lcom/twitter/android/livevideo/landing/i;->a(Lcom/twitter/android/livevideo/landing/i$a;)V

    .line 63
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->a:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;->e:Lcom/twitter/ui/widget/ToggleTwitterButton;

    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;)Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$a;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->d:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$a;

    return-object v0
.end method

.method private a(Lcom/twitter/model/livevideo/b;)V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->m:Lcom/twitter/model/livevideo/d;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->m:Lcom/twitter/model/livevideo/d;

    iget-object v0, v0, Lcom/twitter/model/livevideo/d;->b:Lcom/twitter/model/livevideo/c;

    if-nez v0, :cond_1

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    const-string/jumbo v0, "live_video_remind_me_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 100
    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->m:Lcom/twitter/model/livevideo/d;

    iget-object v0, v0, Lcom/twitter/model/livevideo/d;->b:Lcom/twitter/model/livevideo/c;

    iget-boolean v0, v0, Lcom/twitter/model/livevideo/c;->c:Z

    invoke-virtual {p0, v0}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->setReminderValue(Z)V

    .line 101
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->a:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;->e:Lcom/twitter/ui/widget/ToggleTwitterButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setVisibility(I)V

    goto :goto_0

    .line 103
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->a:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;->e:Lcom/twitter/ui/widget/ToggleTwitterButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/model/livevideo/b;I)V
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->a(Lcom/twitter/model/livevideo/b;Ljava/lang/String;)V

    .line 131
    return-void
.end method

.method private a(Lcom/twitter/model/livevideo/b;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 134
    invoke-static {p1}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->b(Lcom/twitter/model/livevideo/b;)Lcom/twitter/util/collection/k;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Lcom/twitter/util/collection/k;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    new-instance v1, Lcom/twitter/media/request/a$a;

    invoke-virtual {v0}, Lcom/twitter/util/collection/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/card/property/ImageSpec;

    iget-object v0, v0, Lcom/twitter/model/card/property/ImageSpec;->b:Ljava/lang/String;

    invoke-direct {v1, v0}, Lcom/twitter/media/request/a$a;-><init>(Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->a:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;->a:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->a:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;->c:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/twitter/model/livevideo/b;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->a:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;->d:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/twitter/model/livevideo/b;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->a:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    return-void
.end method

.method private static b(Lcom/twitter/model/livevideo/b;)Lcom/twitter/util/collection/k;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/livevideo/b;",
            ")",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/card/property/ImageSpec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lcom/twitter/model/livevideo/b;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    invoke-static {}, Lcom/twitter/util/collection/k;->a()Lcom/twitter/util/collection/k;

    move-result-object v0

    .line 175
    :goto_0
    return-object v0

    .line 164
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/model/livevideo/b;->k:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 165
    new-instance v1, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$2;

    invoke-direct {v1}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$2;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 175
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/k;->a(Ljava/lang/Object;)Lcom/twitter/util/collection/k;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->a:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->d:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$a;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->d:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$a;

    invoke-interface {v0}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$a;->a()V

    .line 156
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->b:Lcom/twitter/android/livevideo/landing/i;

    invoke-virtual {v0}, Lcom/twitter/android/livevideo/landing/i;->a()V

    .line 126
    invoke-super {p0}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;->onDetachedFromWindow()V

    .line 127
    return-void
.end method

.method public setEvent(Lcom/twitter/model/livevideo/b;)V
    .locals 4

    .prologue
    .line 70
    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    if-nez v0, :cond_0

    .line 93
    :goto_0
    :pswitch_0
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->b:Lcom/twitter/android/livevideo/landing/i;

    invoke-virtual {v0}, Lcom/twitter/android/livevideo/landing/i;->a()V

    .line 74
    sget-object v0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$3;->a:[I

    iget-object v1, p1, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    iget-object v1, v1, Lcom/twitter/model/livevideo/a;->d:Lcom/twitter/model/livevideo/BroadcastState;

    invoke-virtual {v1}, Lcom/twitter/model/livevideo/BroadcastState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 90
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Unknown event state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :pswitch_1
    const v0, 0x7f0a04aa

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->a(Lcom/twitter/model/livevideo/b;I)V

    .line 77
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->b:Lcom/twitter/android/livevideo/landing/i;

    iget-wide v2, p1, Lcom/twitter/model/livevideo/b;->d:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/livevideo/landing/i;->d(J)V

    .line 78
    invoke-direct {p0, p1}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->a(Lcom/twitter/model/livevideo/b;)V

    goto :goto_0

    .line 82
    :pswitch_2
    const v0, 0x7f0a04a9

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->a(Lcom/twitter/model/livevideo/b;I)V

    .line 83
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->a:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;->e:Lcom/twitter/ui/widget/ToggleTwitterButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setVisibility(I)V

    goto :goto_0

    .line 74
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public setListener(Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$a;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->d:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$a;

    .line 121
    return-void
.end method

.method public setReminderValue(Z)V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView;->a:Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/LiveVideoPlaceholderView$b;->e:Lcom/twitter/ui/widget/ToggleTwitterButton;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/ToggleTwitterButton;->setToggledOn(Z)V

    .line 112
    return-void
.end method
