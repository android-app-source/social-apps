.class Lcom/twitter/android/livevideo/landing/d$4;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/livevideo/landing/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/d",
        "<",
        "Lcom/twitter/model/livevideo/b;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/livevideo/landing/d;


# direct methods
.method constructor <init>(Lcom/twitter/android/livevideo/landing/d;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/twitter/android/livevideo/landing/d$4;->a:Lcom/twitter/android/livevideo/landing/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/livevideo/b;)V
    .locals 2

    .prologue
    .line 129
    if-eqz p1, :cond_0

    .line 130
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d$4;->a:Lcom/twitter/android/livevideo/landing/d;

    invoke-static {v0}, Lcom/twitter/android/livevideo/landing/d;->b(Lcom/twitter/android/livevideo/landing/d;)Lcom/twitter/app/common/base/BaseFragmentActivity;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/livevideo/b;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/BaseFragmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d$4;->a:Lcom/twitter/android/livevideo/landing/d;

    iget-object v1, p1, Lcom/twitter/model/livevideo/b;->j:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/android/livevideo/landing/d;->a(Lcom/twitter/android/livevideo/landing/d;Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d$4;->a:Lcom/twitter/android/livevideo/landing/d;

    invoke-static {v0, p1}, Lcom/twitter/android/livevideo/landing/d;->a(Lcom/twitter/android/livevideo/landing/d;Lcom/twitter/model/livevideo/b;)V

    .line 133
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d$4;->a:Lcom/twitter/android/livevideo/landing/d;

    invoke-static {v0, p1}, Lcom/twitter/android/livevideo/landing/d;->b(Lcom/twitter/android/livevideo/landing/d;Lcom/twitter/model/livevideo/b;)V

    .line 134
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d$4;->a:Lcom/twitter/android/livevideo/landing/d;

    invoke-static {v0}, Lcom/twitter/android/livevideo/landing/d;->c(Lcom/twitter/android/livevideo/landing/d;)Lcom/twitter/android/livevideo/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/livevideo/a;->a(Lcom/twitter/model/livevideo/b;)V

    .line 135
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d$4;->a:Lcom/twitter/android/livevideo/landing/d;

    invoke-static {v0}, Lcom/twitter/android/livevideo/landing/d;->a(Lcom/twitter/android/livevideo/landing/d;)Lcom/twitter/android/livevideo/landing/d$a;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/android/livevideo/landing/d$a;->a:Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;

    invoke-virtual {v0, p1}, Lcom/twitter/android/livevideo/player/LiveVideoPlayerChrome;->setEvent(Lcom/twitter/model/livevideo/b;)V

    .line 136
    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    iget-object v0, v0, Lcom/twitter/model/livevideo/a;->d:Lcom/twitter/model/livevideo/BroadcastState;

    sget-object v1, Lcom/twitter/model/livevideo/BroadcastState;->b:Lcom/twitter/model/livevideo/BroadcastState;

    if-ne v0, v1, :cond_1

    .line 137
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d$4;->a:Lcom/twitter/android/livevideo/landing/d;

    invoke-static {v0, p1}, Lcom/twitter/android/livevideo/landing/d;->c(Lcom/twitter/android/livevideo/landing/d;Lcom/twitter/model/livevideo/b;)V

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/d$4;->a:Lcom/twitter/android/livevideo/landing/d;

    invoke-static {v0}, Lcom/twitter/android/livevideo/landing/d;->d(Lcom/twitter/android/livevideo/landing/d;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 115
    check-cast p1, Lcom/twitter/model/livevideo/b;

    invoke-virtual {p0, p1}, Lcom/twitter/android/livevideo/landing/d$4;->a(Lcom/twitter/model/livevideo/b;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 122
    if-eqz p1, :cond_0

    .line 123
    invoke-static {p1}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 125
    :cond_0
    return-void
.end method

.method public by_()V
    .locals 0

    .prologue
    .line 118
    return-void
.end method
