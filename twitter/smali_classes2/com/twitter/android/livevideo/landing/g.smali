.class public final Lcom/twitter/android/livevideo/landing/g;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcsd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcsd",
        "<",
        "Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lua;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/scribe/n;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/timeline/br;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lub;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/analytics/model/ScribeItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/twitter/android/livevideo/landing/g;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/livevideo/landing/g;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcta;Lcta;Lcta;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lua;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/scribe/n;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/timeline/br;",
            ">;",
            "Lcta",
            "<",
            "Lub;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/analytics/model/ScribeItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/g;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 35
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/livevideo/landing/g;->b:Lcta;

    .line 36
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/g;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 37
    :cond_1
    iput-object p2, p0, Lcom/twitter/android/livevideo/landing/g;->c:Lcta;

    .line 38
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/g;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 39
    :cond_2
    iput-object p3, p0, Lcom/twitter/android/livevideo/landing/g;->d:Lcta;

    .line 40
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/g;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 41
    :cond_3
    iput-object p4, p0, Lcom/twitter/android/livevideo/landing/g;->e:Lcta;

    .line 42
    sget-boolean v0, Lcom/twitter/android/livevideo/landing/g;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 43
    :cond_4
    iput-object p5, p0, Lcom/twitter/android/livevideo/landing/g;->f:Lcta;

    .line 44
    return-void
.end method

.method public static a(Lcta;Lcta;Lcta;Lcta;Lcta;)Lcsd;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lua;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/scribe/n;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/android/timeline/br;",
            ">;",
            "Lcta",
            "<",
            "Lub;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/analytics/model/ScribeItem;",
            ">;)",
            "Lcsd",
            "<",
            "Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v0, Lcom/twitter/android/livevideo/landing/g;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/livevideo/landing/g;-><init>(Lcta;Lcta;Lcta;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;)V
    .locals 2

    .prologue
    .line 62
    if-nez p1, :cond_0

    .line 63
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/g;->b:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lua;

    iput-object v0, p1, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->b:Lua;

    .line 66
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/g;->c:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/n;

    iput-object v0, p1, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->c:Lcom/twitter/library/scribe/n;

    .line 67
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/g;->d:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/br;

    iput-object v0, p1, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->d:Lcom/twitter/android/timeline/br;

    .line 68
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/g;->e:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lub;

    iput-object v0, p1, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->e:Lub;

    .line 69
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/g;->f:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeItem;

    iput-object v0, p1, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->f:Lcom/twitter/analytics/model/ScribeItem;

    .line 70
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;

    invoke-virtual {p0, p1}, Lcom/twitter/android/livevideo/landing/g;->a(Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;)V

    return-void
.end method
