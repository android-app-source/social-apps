.class public Lcom/twitter/android/livevideo/landing/i;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/livevideo/landing/i$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Lrx/f;

.field private c:Lcom/twitter/android/livevideo/landing/i$a;

.field private d:Lrx/j;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lrx/f;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Lcom/twitter/android/livevideo/landing/i$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/livevideo/landing/i$1;-><init>(Lcom/twitter/android/livevideo/landing/i;)V

    iput-object v0, p0, Lcom/twitter/android/livevideo/landing/i;->d:Lrx/j;

    .line 52
    iput-object p1, p0, Lcom/twitter/android/livevideo/landing/i;->a:Landroid/content/res/Resources;

    .line 53
    iput-object p2, p0, Lcom/twitter/android/livevideo/landing/i;->b:Lrx/f;

    .line 54
    return-void
.end method

.method static a(J)Z
    .locals 2

    .prologue
    .line 84
    const-wide/32 v0, 0x5265c00

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    const-wide/32 v0, 0xea60

    cmp-long v0, p0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b(J)Z
    .locals 2

    .prologue
    .line 88
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    const-wide/32 v0, 0xea60

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static c(J)I
    .locals 4

    .prologue
    .line 101
    long-to-double v0, p0

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method


# virtual methods
.method a(JJ)Ljava/lang/String;
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 67
    cmp-long v0, p3, v2

    if-gtz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/i;->a:Landroid/content/res/Resources;

    const v1, 0x7f0a04aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 79
    :goto_0
    return-object v0

    .line 69
    :cond_0
    invoke-static {p3, p4}, Lcom/twitter/android/livevideo/landing/i;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    invoke-static {v2, v3, p3, p4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 71
    invoke-static {v0, v1}, Lcom/twitter/android/livevideo/landing/i;->c(J)I

    move-result v0

    int-to-long v0, v0

    .line 72
    invoke-static {v0, v1}, Lcom/twitter/util/aa;->b(J)Ljava/lang/String;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/i;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a04ad

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 74
    :cond_1
    invoke-static {p3, p4}, Lcom/twitter/android/livevideo/landing/i;->b(J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 75
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/i;->a:Landroid/content/res/Resources;

    const v1, 0x7f0a04ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 77
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/i;->a:Landroid/content/res/Resources;

    invoke-static {v0, p1, p2}, Lcom/twitter/util/aa;->d(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v0

    .line 78
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/i;->a:Landroid/content/res/Resources;

    invoke-static {v1, p1, p2}, Lcom/twitter/util/aa;->e(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v1

    .line 79
    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/i;->a:Landroid/content/res/Resources;

    const v3, 0x7f0a04ac

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    aput-object v1, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/i;->d:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 106
    return-void
.end method

.method public a(Lcom/twitter/android/livevideo/landing/i$a;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/twitter/android/livevideo/landing/i;->c:Lcom/twitter/android/livevideo/landing/i$a;

    .line 58
    return-void
.end method

.method b(JJ)V
    .locals 3

    .prologue
    .line 130
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/i;->c:Lcom/twitter/android/livevideo/landing/i$a;

    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/android/livevideo/landing/i;->a(JJ)Ljava/lang/String;

    move-result-object v0

    .line 132
    iget-object v1, p0, Lcom/twitter/android/livevideo/landing/i;->c:Lcom/twitter/android/livevideo/landing/i$a;

    invoke-interface {v1, v0}, Lcom/twitter/android/livevideo/landing/i$a;->a(Ljava/lang/String;)V

    .line 135
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/i;->d:Lrx/j;

    invoke-interface {v0}, Lrx/j;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 136
    invoke-virtual {p0}, Lcom/twitter/android/livevideo/landing/i;->a()V

    .line 137
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/i;->c:Lcom/twitter/android/livevideo/landing/i$a;

    if-eqz v0, :cond_1

    .line 138
    iget-object v0, p0, Lcom/twitter/android/livevideo/landing/i;->c:Lcom/twitter/android/livevideo/landing/i$a;

    invoke-interface {v0}, Lcom/twitter/android/livevideo/landing/i$a;->f()V

    .line 141
    :cond_1
    return-void
.end method

.method public d(J)V
    .locals 7

    .prologue
    .line 109
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    sub-long v0, p1, v0

    .line 110
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-ltz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/i;->d:Lrx/j;

    invoke-interface {v2}, Lrx/j;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 111
    const-wide/16 v2, 0x1

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lcom/twitter/android/livevideo/landing/i;->b:Lrx/f;

    .line 112
    invoke-static {v2, v3, v4, v5}, Lrx/c;->a(JLjava/util/concurrent/TimeUnit;Lrx/f;)Lrx/c;

    move-result-object v2

    new-instance v3, Lcom/twitter/android/livevideo/landing/i$2;

    invoke-direct {v3, p0, p1, p2}, Lcom/twitter/android/livevideo/landing/i$2;-><init>(Lcom/twitter/android/livevideo/landing/i;J)V

    .line 113
    invoke-virtual {v2, v3}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/livevideo/landing/i;->d:Lrx/j;

    .line 121
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/twitter/android/livevideo/landing/i;->b(JJ)V

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/i;->c:Lcom/twitter/android/livevideo/landing/i$a;

    if-eqz v2, :cond_0

    .line 124
    iget-object v2, p0, Lcom/twitter/android/livevideo/landing/i;->c:Lcom/twitter/android/livevideo/landing/i$a;

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/twitter/android/livevideo/landing/i;->a(JJ)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/twitter/android/livevideo/landing/i$a;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
