.class public Lcom/twitter/android/livevideo/d;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field a:Lcom/twitter/analytics/feature/model/ClientEventLog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/livevideo/d;->a:Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "live_video_timeline"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "highlights"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "toolbar"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "share"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 37
    iget-object v0, p0, Lcom/twitter/android/livevideo/d;->a:Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 38
    return-void
.end method

.method private b(Landroid/content/Context;J)V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "https://twitter.com/i/live/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 29
    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 30
    invoke-direct {p0}, Lcom/twitter/android/livevideo/d;->a()V

    .line 31
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;J)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/livevideo/d;->b(Landroid/content/Context;J)V

    .line 25
    return-void
.end method
