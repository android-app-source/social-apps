.class public final Lcom/twitter/android/livevideo/f;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcsd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcsd",
        "<",
        "Lcom/twitter/android/livevideo/d;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/ClientEventLog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/twitter/android/livevideo/f;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/livevideo/f;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/ClientEventLog;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    sget-boolean v0, Lcom/twitter/android/livevideo/f;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 18
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/livevideo/f;->b:Lcta;

    .line 19
    return-void
.end method

.method public static a(Lcta;)Lcsd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcom/twitter/analytics/feature/model/ClientEventLog;",
            ">;)",
            "Lcsd",
            "<",
            "Lcom/twitter/android/livevideo/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23
    new-instance v0, Lcom/twitter/android/livevideo/f;

    invoke-direct {v0, p0}, Lcom/twitter/android/livevideo/f;-><init>(Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/livevideo/d;)V
    .locals 2

    .prologue
    .line 28
    if-nez p1, :cond_0

    .line 29
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/livevideo/f;->b:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iput-object v0, p1, Lcom/twitter/android/livevideo/d;->a:Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 32
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/twitter/android/livevideo/d;

    invoke-virtual {p0, p1}, Lcom/twitter/android/livevideo/f;->a(Lcom/twitter/android/livevideo/d;)V

    return-void
.end method
