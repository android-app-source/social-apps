.class Lcom/twitter/android/TweetFragment$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/TweetFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetFragment;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lcom/twitter/android/TweetFragment$1;->a:Lcom/twitter/android/TweetFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 289
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$1;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 290
    if-eqz v2, :cond_0

    .line 291
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$1;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    .line 292
    new-instance v3, Lcom/twitter/android/TweetFragment$d;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment$1;->a:Lcom/twitter/android/TweetFragment;

    iget-object v4, p0, Lcom/twitter/android/TweetFragment$1;->a:Lcom/twitter/android/TweetFragment;

    invoke-direct {v3, v1, v4, v0}, Lcom/twitter/android/TweetFragment$d;-><init>(Lcom/twitter/android/TweetFragment;Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 294
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    if-nez v0, :cond_1

    const-string/jumbo v0, "unknown"

    .line 295
    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment$1;->a:Lcom/twitter/android/TweetFragment;

    .line 296
    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->V()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":caret:click"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 297
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v4, p0, Lcom/twitter/android/TweetFragment$1;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v4}, Lcom/twitter/android/TweetFragment;->c(Lcom/twitter/android/TweetFragment;)J

    move-result-wide v6

    invoke-direct {v1, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v4, v8, [Ljava/lang/String;

    aput-object v0, v4, v5

    invoke-virtual {v1, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 298
    new-instance v1, Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v1}, Lcom/twitter/model/util/FriendshipCache;-><init>()V

    .line 299
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$1;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/model/util/FriendshipCache;->a(Lcom/twitter/model/core/Tweet;)V

    .line 300
    invoke-static {}, Lbph;->a()Z

    move-result v6

    .line 301
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$1;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->Q()Lwc;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/TweetFragment$1;->a:Lcom/twitter/android/TweetFragment;

    .line 302
    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->Q()Lwc;

    move-result-object v0

    invoke-virtual {v0}, Lwc;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 303
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$1;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    iget-object v4, p0, Lcom/twitter/android/TweetFragment$1;->a:Lcom/twitter/android/TweetFragment;

    iget-object v4, v4, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    const v7, 0x7f13007e

    .line 304
    invoke-virtual {v4, v7}, Lcom/twitter/android/widget/TweetDetailView;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/timeline/bk;

    const/4 v7, 0x0

    .line 303
    invoke-static/range {v0 .. v8}, Lcom/twitter/android/cl;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Landroid/support/v4/app/FragmentActivity;Lbxb;Lcom/twitter/android/timeline/bk;ZZLjava/lang/String;Z)V

    .line 307
    :cond_0
    return-void

    .line 295
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v8, v5

    .line 302
    goto :goto_1
.end method
