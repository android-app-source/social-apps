.class Lcom/twitter/android/TweetFragment$14;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lakv;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/TweetFragment;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lakv",
        "<",
        "Lcom/twitter/model/av/h;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetFragment;)V
    .locals 0

    .prologue
    .line 424
    iput-object p1, p0, Lcom/twitter/android/TweetFragment$14;->a:Lcom/twitter/android/TweetFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILcom/twitter/model/av/h;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 428
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$14;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    if-nez v0, :cond_1

    .line 447
    :cond_0
    :goto_0
    return-void

    .line 433
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$14;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->O()Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    .line 435
    if-eqz v0, :cond_0

    .line 439
    new-instance v1, Lbbt;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment$14;->a:Lcom/twitter/android/TweetFragment;

    .line 440
    invoke-virtual {v2}, Lcom/twitter/android/TweetFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetFragment$14;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v3}, Lcom/twitter/android/TweetFragment;->d(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, v0, Lcom/twitter/model/core/MediaEntity;->c:J

    .line 441
    invoke-static {p2}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/model/av/h;

    invoke-direct/range {v1 .. v6}, Lbbt;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcom/twitter/model/av/h;)V

    .line 442
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$14;->a:Lcom/twitter/android/TweetFragment;

    const/4 v2, 0x7

    invoke-static {v0, v1, v2, v7}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;Lcom/twitter/library/service/s;II)Z

    .line 444
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$14;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->e(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/cn;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/android/cn;->a(Lcom/twitter/model/av/h;)V

    .line 445
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$14;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0, p2, v7}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/model/av/h;Z)V

    goto :goto_0
.end method

.method public bridge synthetic a(ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 424
    check-cast p2, Lcom/twitter/model/av/h;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/TweetFragment$14;->a(ILcom/twitter/model/av/h;)V

    return-void
.end method
