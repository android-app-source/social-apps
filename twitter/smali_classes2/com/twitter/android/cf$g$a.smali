.class public Lcom/twitter/android/cf$g$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/cf$g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Lcom/twitter/android/cf$b;

.field private b:Lcom/twitter/android/cf$a;

.field private c:Lcom/twitter/android/cf$l;

.field private d:Lcom/twitter/android/cf$k;

.field private e:Lcom/twitter/android/cf$j;

.field private f:Lcom/twitter/android/cf$i;

.field private g:Lcom/twitter/android/widget/EventView;

.field private h:Lcom/twitter/android/cb;

.field private i:Lcom/twitter/android/z;

.field private j:Lcom/twitter/android/widget/WhoToFollowUsersView;

.field private k:Lcom/twitter/android/cf$e;

.field private l:Lcom/twitter/android/cu;

.field private m:Lcom/twitter/android/cf$f;

.field private n:Lcom/twitter/android/cf$d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cf$b;
    .locals 1

    .prologue
    .line 2031
    iget-object v0, p0, Lcom/twitter/android/cf$g$a;->a:Lcom/twitter/android/cf$b;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cf$a;
    .locals 1

    .prologue
    .line 2031
    iget-object v0, p0, Lcom/twitter/android/cf$g$a;->b:Lcom/twitter/android/cf$a;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cf$l;
    .locals 1

    .prologue
    .line 2031
    iget-object v0, p0, Lcom/twitter/android/cf$g$a;->c:Lcom/twitter/android/cf$l;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cf$k;
    .locals 1

    .prologue
    .line 2031
    iget-object v0, p0, Lcom/twitter/android/cf$g$a;->d:Lcom/twitter/android/cf$k;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cf$j;
    .locals 1

    .prologue
    .line 2031
    iget-object v0, p0, Lcom/twitter/android/cf$g$a;->e:Lcom/twitter/android/cf$j;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cf$i;
    .locals 1

    .prologue
    .line 2031
    iget-object v0, p0, Lcom/twitter/android/cf$g$a;->f:Lcom/twitter/android/cf$i;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/widget/EventView;
    .locals 1

    .prologue
    .line 2031
    iget-object v0, p0, Lcom/twitter/android/cf$g$a;->g:Lcom/twitter/android/widget/EventView;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cb;
    .locals 1

    .prologue
    .line 2031
    iget-object v0, p0, Lcom/twitter/android/cf$g$a;->h:Lcom/twitter/android/cb;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/z;
    .locals 1

    .prologue
    .line 2031
    iget-object v0, p0, Lcom/twitter/android/cf$g$a;->i:Lcom/twitter/android/z;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/widget/WhoToFollowUsersView;
    .locals 1

    .prologue
    .line 2031
    iget-object v0, p0, Lcom/twitter/android/cf$g$a;->j:Lcom/twitter/android/widget/WhoToFollowUsersView;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cf$e;
    .locals 1

    .prologue
    .line 2031
    iget-object v0, p0, Lcom/twitter/android/cf$g$a;->k:Lcom/twitter/android/cf$e;

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cu;
    .locals 1

    .prologue
    .line 2031
    iget-object v0, p0, Lcom/twitter/android/cf$g$a;->l:Lcom/twitter/android/cu;

    return-object v0
.end method

.method static synthetic m(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cf$f;
    .locals 1

    .prologue
    .line 2031
    iget-object v0, p0, Lcom/twitter/android/cf$g$a;->m:Lcom/twitter/android/cf$f;

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/android/cf$g$a;)Lcom/twitter/android/cf$d;
    .locals 1

    .prologue
    .line 2031
    iget-object v0, p0, Lcom/twitter/android/cf$g$a;->n:Lcom/twitter/android/cf$d;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/cb;)Lcom/twitter/android/cf$g$a;
    .locals 0

    .prologue
    .line 2100
    iput-object p1, p0, Lcom/twitter/android/cf$g$a;->h:Lcom/twitter/android/cb;

    .line 2101
    return-object p0
.end method

.method public a(Lcom/twitter/android/cf$a;)Lcom/twitter/android/cf$g$a;
    .locals 0

    .prologue
    .line 2061
    iput-object p1, p0, Lcom/twitter/android/cf$g$a;->b:Lcom/twitter/android/cf$a;

    .line 2062
    return-object p0
.end method

.method public a(Lcom/twitter/android/cf$b;)Lcom/twitter/android/cf$g$a;
    .locals 0

    .prologue
    .line 2055
    iput-object p1, p0, Lcom/twitter/android/cf$g$a;->a:Lcom/twitter/android/cf$b;

    .line 2056
    return-object p0
.end method

.method public a(Lcom/twitter/android/cf$d;)Lcom/twitter/android/cf$g$a;
    .locals 0

    .prologue
    .line 2130
    iput-object p1, p0, Lcom/twitter/android/cf$g$a;->n:Lcom/twitter/android/cf$d;

    .line 2131
    return-object p0
.end method

.method public a(Lcom/twitter/android/cf$f;)Lcom/twitter/android/cf$g$a;
    .locals 0

    .prologue
    .line 2049
    iput-object p1, p0, Lcom/twitter/android/cf$g$a;->m:Lcom/twitter/android/cf$f;

    .line 2050
    return-object p0
.end method

.method public a(Lcom/twitter/android/cf$i;)Lcom/twitter/android/cf$g$a;
    .locals 0

    .prologue
    .line 2088
    iput-object p1, p0, Lcom/twitter/android/cf$g$a;->f:Lcom/twitter/android/cf$i;

    .line 2089
    return-object p0
.end method

.method public a(Lcom/twitter/android/cf$j;)Lcom/twitter/android/cf$g$a;
    .locals 0

    .prologue
    .line 2081
    iput-object p1, p0, Lcom/twitter/android/cf$g$a;->e:Lcom/twitter/android/cf$j;

    .line 2082
    return-object p0
.end method

.method public a(Lcom/twitter/android/cf$k;)Lcom/twitter/android/cf$g$a;
    .locals 0

    .prologue
    .line 2074
    iput-object p1, p0, Lcom/twitter/android/cf$g$a;->d:Lcom/twitter/android/cf$k;

    .line 2075
    return-object p0
.end method

.method public a(Lcom/twitter/android/cf$l;)Lcom/twitter/android/cf$g$a;
    .locals 0

    .prologue
    .line 2067
    iput-object p1, p0, Lcom/twitter/android/cf$g$a;->c:Lcom/twitter/android/cf$l;

    .line 2068
    return-object p0
.end method

.method public a(Lcom/twitter/android/widget/WhoToFollowUsersView;)Lcom/twitter/android/cf$g$a;
    .locals 0

    .prologue
    .line 2112
    iput-object p1, p0, Lcom/twitter/android/cf$g$a;->j:Lcom/twitter/android/widget/WhoToFollowUsersView;

    .line 2113
    return-object p0
.end method

.method public a(Lcom/twitter/android/z;)Lcom/twitter/android/cf$g$a;
    .locals 0

    .prologue
    .line 2106
    iput-object p1, p0, Lcom/twitter/android/cf$g$a;->i:Lcom/twitter/android/z;

    .line 2107
    return-object p0
.end method

.method public a()Lcom/twitter/android/cf$g;
    .locals 2

    .prologue
    .line 2136
    new-instance v0, Lcom/twitter/android/cf$g;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/cf$g;-><init>(Lcom/twitter/android/cf$g$a;Lcom/twitter/android/cf$1;)V

    return-object v0
.end method
