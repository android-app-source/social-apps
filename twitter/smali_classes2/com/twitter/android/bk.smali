.class Lcom/twitter/android/bk;
.super Lcom/twitter/library/service/j;
.source "Twttr"


# instance fields
.field private final a:Landroid/net/Uri;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/twitter/android/bk;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/j;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 46
    iput-object p3, p0, Lcom/twitter/android/bk;->a:Landroid/net/Uri;

    .line 47
    return-void
.end method

.method static a(JLjava/lang/String;Landroid/net/Uri;)V
    .locals 4

    .prologue
    const/4 v1, 0x2

    .line 63
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 64
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->d(I)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "app:url_interpreter:redirect_service:"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    .line 65
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 66
    if-eqz p3, :cond_0

    .line 67
    invoke-virtual {p3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->d(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 69
    :cond_0
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 70
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 5

    .prologue
    .line 53
    new-instance v0, Lcom/twitter/library/network/k;

    iget-object v1, p0, Lcom/twitter/android/bk;->h:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/bk;->a:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/network/k;-><init>(Landroid/content/Context;Ljava/net/URI;)V

    const/4 v1, 0x0

    .line 54
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->c(Z)Lcom/twitter/library/network/k;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->e:Lcom/twitter/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/k;->a()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    .line 55
    invoke-virtual {p0}, Lcom/twitter/android/bk;->h()Lcom/twitter/library/service/v;

    move-result-object v1

    iget-wide v2, v1, Lcom/twitter/library/service/v;->c:J

    const-string/jumbo v1, "request"

    iget-object v4, p0, Lcom/twitter/android/bk;->a:Landroid/net/Uri;

    invoke-static {v2, v3, v1, v4}, Lcom/twitter/android/bk;->a(JLjava/lang/String;Landroid/net/Uri;)V

    .line 56
    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->c()Lcom/twitter/network/HttpOperation;

    .line 57
    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    invoke-virtual {p0}, Lcom/twitter/android/bk;->h()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/library/service/v;->c:J

    const-string/jumbo v2, "error"

    iget-object v3, p0, Lcom/twitter/android/bk;->a:Landroid/net/Uri;

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/bk;->a(JLjava/lang/String;Landroid/net/Uri;)V

    .line 60
    :cond_0
    return-void
.end method
