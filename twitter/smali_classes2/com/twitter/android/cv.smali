.class public Lcom/twitter/android/cv;
.super Lcjr;
.source "Twttr"

# interfaces
.implements Lcbq;
.implements Lcom/twitter/android/client/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcjr",
        "<",
        "Lcom/twitter/android/timeline/bk;",
        ">;",
        "Lcbq",
        "<",
        "Lcom/twitter/android/timeline/bk;",
        ">;",
        "Lcom/twitter/android/client/j;"
    }
.end annotation


# instance fields
.field protected final a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

.field protected final b:Lcom/twitter/library/client/p;

.field protected final c:Lcom/twitter/library/client/v;

.field protected final d:Lcom/twitter/library/view/d;

.field protected final e:Lcom/twitter/util/collection/ReferenceList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/ReferenceList",
            "<",
            "Lcom/twitter/android/cu;",
            ">;"
        }
    .end annotation
.end field

.field protected final f:Lcbp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcbp",
            "<",
            "Lcom/twitter/android/timeline/bk;",
            ">;"
        }
    .end annotation
.end field

.field protected g:Lcom/twitter/android/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation
.end field

.field protected final h:Lcom/twitter/ui/view/h;

.field private final i:Lcom/twitter/ui/view/h;

.field private final j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/twitter/library/card/m;

.field private final l:I

.field private m:Z

.field private n:Z

.field private final o:I

.field private final p:Z

.field private final q:Lcom/twitter/model/util/FriendshipCache;

.field private r:Z

.field private final s:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method public constructor <init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;ZLcom/twitter/library/view/d;Lcom/twitter/model/util/FriendshipCache;IIILcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 10

    .prologue
    .line 150
    sget-object v9, Lcom/twitter/library/widget/TweetView;->c:Lcom/twitter/ui/view/h;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/cv;-><init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;ZLcom/twitter/library/view/d;Lcom/twitter/model/util/FriendshipCache;IIILcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/ui/view/h;)V

    .line 152
    return-void
.end method

.method public constructor <init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;ZLcom/twitter/library/view/d;Lcom/twitter/model/util/FriendshipCache;IIILcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/ui/view/h;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 171
    invoke-direct {p0, p1}, Lcjr;-><init>(Landroid/content/Context;)V

    .line 97
    invoke-static {}, Lcom/twitter/util/collection/ReferenceList;->a()Lcom/twitter/util/collection/ReferenceList;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/cv;->e:Lcom/twitter/util/collection/ReferenceList;

    .line 106
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/twitter/android/cv;->j:Ljava/util/ArrayList;

    .line 114
    iput-boolean v0, p0, Lcom/twitter/android/cv;->r:Z

    .line 172
    iput-object p8, p0, Lcom/twitter/android/cv;->s:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 173
    iput-object p1, p0, Lcom/twitter/android/cv;->a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    .line 174
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/cv;->c:Lcom/twitter/library/client/v;

    .line 175
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/cv;->b:Lcom/twitter/library/client/p;

    .line 176
    iput-object p3, p0, Lcom/twitter/android/cv;->d:Lcom/twitter/library/view/d;

    .line 177
    iput-boolean p2, p0, Lcom/twitter/android/cv;->m:Z

    .line 178
    iget-object v2, p0, Lcom/twitter/android/cv;->c:Lcom/twitter/library/client/v;

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v2

    .line 179
    if-eqz v2, :cond_1

    iget-boolean v2, v2, Lcom/twitter/model/account/UserSettings;->k:Z

    if-eqz v2, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/cv;->p:Z

    .line 180
    iput p5, p0, Lcom/twitter/android/cv;->l:I

    .line 181
    iput p6, p0, Lcom/twitter/android/cv;->o:I

    .line 182
    iput-object p4, p0, Lcom/twitter/android/cv;->q:Lcom/twitter/model/util/FriendshipCache;

    .line 183
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    iput-boolean v1, p0, Lcom/twitter/android/cv;->r:Z

    .line 187
    :cond_0
    iput-object p9, p0, Lcom/twitter/android/cv;->h:Lcom/twitter/ui/view/h;

    .line 188
    iget-object v0, p0, Lcom/twitter/android/cv;->h:Lcom/twitter/ui/view/h;

    .line 189
    invoke-static {v0}, Lcom/twitter/android/revenue/k;->a(Lcom/twitter/ui/view/h;)Lcom/twitter/ui/view/h;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cv;->i:Lcom/twitter/ui/view/h;

    .line 191
    invoke-virtual {p0}, Lcom/twitter/android/cv;->U_()Lcbp;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cv;->f:Lcbp;

    .line 192
    invoke-static {}, Lcom/twitter/library/card/m;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/twitter/library/card/m;

    invoke-direct {v0}, Lcom/twitter/library/card/m;-><init>()V

    :goto_1
    iput-object v0, p0, Lcom/twitter/android/cv;->k:Lcom/twitter/library/card/m;

    .line 193
    return-void

    :cond_1
    move v0, v1

    .line 179
    goto :goto_0

    .line 192
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public constructor <init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;ZLcom/twitter/library/view/d;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 9

    .prologue
    .line 124
    const v5, 0x7f0403f0

    const v6, 0x7f040414

    const/4 v7, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/cv;-><init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;ZLcom/twitter/library/view/d;Lcom/twitter/model/util/FriendshipCache;IIILcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 127
    return-void
.end method

.method public constructor <init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;ZLcom/twitter/library/view/d;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/ui/view/h;)V
    .locals 10

    .prologue
    .line 137
    const v5, 0x7f0403f0

    const v6, 0x7f040414

    const/4 v7, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v8, p5

    move-object/from16 v9, p6

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/cv;-><init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;ZLcom/twitter/library/view/d;Lcom/twitter/model/util/FriendshipCache;IIILcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/ui/view/h;)V

    .line 140
    return-void
.end method

.method protected static a(Lcom/twitter/android/timeline/cd;I)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 361
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 362
    instance-of v0, p0, Lcom/twitter/android/timeline/i;

    if-eqz v0, :cond_0

    .line 363
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/i;

    .line 364
    invoke-interface {v0}, Lcom/twitter/android/timeline/i;->a()Ljava/lang/String;

    move-result-object v0

    .line 365
    const-string/jumbo v2, "ad_slot_id"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    :cond_0
    const-string/jumbo v0, "position"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 368
    return-object v1
.end method

.method private static a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 262
    instance-of v0, p0, Lcom/twitter/android/widget/GapView;

    if-eqz v0, :cond_0

    .line 263
    invoke-virtual {p0, p0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 267
    :goto_0
    return-void

    .line 265
    :cond_0
    const v0, 0x7f13014b

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/widget/TweetView;)V
    .locals 1

    .prologue
    .line 286
    iget-boolean v0, p0, Lcom/twitter/android/cv;->p:Z

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/TweetView;->setDisplaySensitiveMedia(Z)V

    .line 287
    iget-object v0, p0, Lcom/twitter/android/cv;->d:Lcom/twitter/library/view/d;

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/TweetView;->setOnTweetViewClickListener(Lcom/twitter/library/view/d;)V

    .line 288
    iget-boolean v0, p0, Lcom/twitter/android/cv;->r:Z

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/TweetView;->setShouldSimulateInlineActions(Z)V

    .line 289
    return-void
.end method

.method public static a(Landroid/database/Cursor;J)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 494
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    cmp-long v1, v2, p1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/widget/ListView;JLcom/twitter/model/core/Tweet;Ljava/lang/Runnable;)Z
    .locals 7

    .prologue
    .line 390
    new-instance v0, Lcom/twitter/android/cv$1;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/cv$1;-><init>(Landroid/widget/ListView;JLcom/twitter/model/core/Tweet;Ljava/lang/Runnable;)V

    invoke-virtual {p0, v0}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 430
    const/4 v0, 0x1

    return v0
.end method

.method private c(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 270
    new-instance v0, Lcom/twitter/android/bg;

    invoke-direct {v0, p1}, Lcom/twitter/android/bg;-><init>(Landroid/view/View;)V

    .line 271
    iget-object v1, v0, Lcom/twitter/android/bg;->d:Lcom/twitter/library/widget/TweetView;

    invoke-direct {p0, v1}, Lcom/twitter/android/cv;->a(Lcom/twitter/library/widget/TweetView;)V

    .line 272
    iget-object v1, v0, Lcom/twitter/android/bg;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setAlwaysExpandMedia(Z)V

    .line 273
    iget-object v1, v0, Lcom/twitter/android/bg;->a:Lcom/twitter/library/widget/TweetView;

    invoke-direct {p0, v1}, Lcom/twitter/android/cv;->a(Lcom/twitter/library/widget/TweetView;)V

    .line 274
    iget-object v1, v0, Lcom/twitter/android/bg;->a:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setAlwaysExpandMedia(Z)V

    .line 275
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 276
    return-void
.end method

.method public static d(Landroid/database/Cursor;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 483
    if-nez p0, :cond_0

    .line 490
    :goto_0
    return v1

    .line 487
    :cond_0
    const/16 v2, 0x1d

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 488
    and-int/lit8 v3, v2, 0x3

    if-eqz v3, :cond_1

    move v3, v0

    .line 489
    :goto_1
    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    move v2, v0

    .line 490
    :goto_2
    if-nez v2, :cond_3

    if-eqz v3, :cond_3

    :goto_3
    move v1, v0

    goto :goto_0

    :cond_1
    move v3, v1

    .line 488
    goto :goto_1

    :cond_2
    move v2, v1

    .line 489
    goto :goto_2

    :cond_3
    move v0, v1

    .line 490
    goto :goto_3
.end method


# virtual methods
.method protected U_()Lcbp;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcbp",
            "<",
            "Lcom/twitter/android/timeline/bk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    new-instance v0, Lcbn;

    new-instance v1, Lcom/twitter/android/timeline/cc;

    invoke-direct {v1}, Lcom/twitter/android/timeline/cc;-><init>()V

    invoke-direct {v0, v1}, Lcbn;-><init>(Lcbp;)V

    return-object v0
.end method

.method protected a(Lcom/twitter/android/timeline/bk;)I
    .locals 1

    .prologue
    .line 221
    instance-of v0, p1, Lcom/twitter/android/timeline/aq;

    if-eqz v0, :cond_0

    .line 222
    const/4 v0, 0x1

    .line 224
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 80
    check-cast p1, Lcom/twitter/android/timeline/bk;

    invoke-virtual {p0, p1}, Lcom/twitter/android/cv;->a(Lcom/twitter/android/timeline/bk;)I

    move-result v0

    return v0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/android/timeline/bk;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 248
    instance-of v0, p2, Lcom/twitter/android/timeline/aq;

    if-eqz v0, :cond_0

    .line 249
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/cv;->l:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 250
    invoke-static {v0}, Lcom/twitter/android/cv;->a(Landroid/view/View;)V

    .line 258
    :goto_0
    return-object v0

    .line 251
    :cond_0
    instance-of v0, p2, Lcom/twitter/android/timeline/ak;

    if-eqz v0, :cond_1

    .line 252
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0402c1

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 253
    invoke-direct {p0, v0}, Lcom/twitter/android/cv;->c(Landroid/view/View;)V

    goto :goto_0

    .line 255
    :cond_1
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/cv;->o:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 256
    invoke-virtual {p0, v0}, Lcom/twitter/android/cv;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 80
    check-cast p2, Lcom/twitter/android/timeline/bk;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/cv;->a(Landroid/content/Context;Lcom/twitter/android/timeline/bk;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Landroid/database/Cursor;)Lcbi;
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0, p1}, Lcom/twitter/android/cv;->b(Landroid/database/Cursor;)Lcom/twitter/android/timeline/bl;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/view/View;Lcom/twitter/android/timeline/cd;I)Lcom/twitter/model/core/Tweet;
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 308
    iget-object v4, p2, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    .line 309
    invoke-virtual {p0}, Lcom/twitter/android/cv;->j()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v5

    .line 310
    invoke-virtual {v5, v4}, Lbaa;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v3

    .line 311
    if-nez v3, :cond_0

    .line 312
    iget v0, v4, Lcom/twitter/model/core/Tweet;->d:I

    and-int/lit8 v0, v0, -0x9

    iput v0, v4, Lcom/twitter/model/core/Tweet;->d:I

    .line 314
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cu;

    .line 315
    iget-object v8, v0, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    .line 316
    iget-object v0, p0, Lcom/twitter/android/cv;->q:Lcom/twitter/model/util/FriendshipCache;

    if-eqz v0, :cond_1

    .line 317
    iget-object v0, p0, Lcom/twitter/android/cv;->q:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, v4}, Lcom/twitter/model/util/FriendshipCache;->a(Lcom/twitter/model/core/Tweet;)V

    .line 320
    :cond_1
    const v0, 0x7f13007e

    invoke-virtual {v8, v0, p2}, Lcom/twitter/library/widget/TweetView;->setTag(ILjava/lang/Object;)V

    .line 321
    iget-object v0, p0, Lcom/twitter/android/cv;->q:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v8, v0}, Lcom/twitter/library/widget/TweetView;->setFriendshipCache(Lcom/twitter/model/util/FriendshipCache;)V

    .line 322
    iget-boolean v0, p0, Lcom/twitter/android/cv;->r:Z

    invoke-virtual {v8, v0}, Lcom/twitter/library/widget/TweetView;->setShouldSimulateInlineActions(Z)V

    .line 323
    iget-boolean v0, p0, Lcom/twitter/android/cv;->m:Z

    if-eqz v0, :cond_5

    if-eqz v3, :cond_5

    move v0, v1

    :goto_0
    invoke-virtual {v8, v0}, Lcom/twitter/library/widget/TweetView;->setAlwaysExpandMedia(Z)V

    .line 324
    invoke-virtual {v8, v2}, Lcom/twitter/library/widget/TweetView;->setHideInlineActions(Z)V

    .line 325
    iget-object v0, p2, Lcom/twitter/android/timeline/cd;->c:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p2, Lcom/twitter/android/timeline/cd;->c:Ljava/lang/String;

    const-string/jumbo v3, "TweetFollowOnly"

    .line 326
    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    .line 325
    :goto_1
    invoke-virtual {v8, v0}, Lcom/twitter/library/widget/TweetView;->setForceFollowButtonOnly(Z)V

    .line 327
    sget v0, Lcni;->a:F

    invoke-virtual {v8, v0}, Lcom/twitter/library/widget/TweetView;->setContentSize(F)V

    .line 329
    invoke-virtual {v8}, Lcom/twitter/library/widget/TweetView;->getPreviewEnabled()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/twitter/android/cv;->m:Z

    if-nez v0, :cond_2

    .line 330
    invoke-virtual {v4}, Lcom/twitter/model/core/Tweet;->r()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_2
    move v3, v1

    .line 331
    :goto_2
    iget-boolean v0, p0, Lcom/twitter/android/cv;->p:Z

    .line 332
    invoke-virtual {v5}, Lbaa;->b()Z

    move-result v5

    .line 331
    invoke-static {v4, v0, v5}, Lbwr;->a(Lcom/twitter/model/core/Tweet;ZZ)Z

    move-result v5

    .line 334
    new-instance v0, Lbxy;

    if-eqz v3, :cond_8

    if-eqz v5, :cond_8

    :goto_3
    iget-object v2, p0, Lcom/twitter/android/cv;->k:Lcom/twitter/library/card/m;

    iget-object v3, p0, Lcom/twitter/android/cv;->a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    sget-object v5, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    iget-object v6, p0, Lcom/twitter/android/cv;->s:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lbxy;-><init>(ZLcom/twitter/library/card/m;Landroid/app/Activity;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 338
    instance-of v1, p2, Lcom/twitter/android/timeline/ae;

    if-eqz v1, :cond_3

    move-object v1, p2

    .line 339
    check-cast v1, Lcom/twitter/android/timeline/ae;

    iget-object v1, v1, Lcom/twitter/android/timeline/ae;->a:Lcom/twitter/model/moments/x;

    .line 340
    iget-object v1, v1, Lcom/twitter/model/moments/x;->c:Lcom/twitter/model/moments/Moment;

    iget-object v1, v1, Lcom/twitter/model/moments/Moment;->c:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lcom/twitter/library/widget/TweetView;->setSocialContextName(Ljava/lang/String;)V

    .line 345
    :cond_3
    invoke-virtual {v4}, Lcom/twitter/model/core/Tweet;->q()Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v1, 0x2

    .line 346
    :goto_4
    invoke-virtual {v8, v1}, Lcom/twitter/library/widget/TweetView;->setMinLines(I)V

    .line 349
    invoke-virtual {v4}, Lcom/twitter/model/core/Tweet;->S()Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/twitter/android/cv;->i:Lcom/twitter/ui/view/h;

    .line 350
    :goto_5
    iget-boolean v2, p0, Lcom/twitter/android/cv;->n:Z

    invoke-virtual {v8, v4, v1, v2, v0}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;ZLbxy;)V

    .line 351
    invoke-virtual {v8}, Lcom/twitter/library/widget/TweetView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 352
    iget-object v0, p0, Lcom/twitter/android/cv;->g:Lcom/twitter/android/av;

    if-eqz v0, :cond_4

    .line 353
    invoke-static {p2, p3}, Lcom/twitter/android/cv;->a(Lcom/twitter/android/timeline/cd;I)Landroid/os/Bundle;

    move-result-object v0

    .line 354
    iget-object v1, p0, Lcom/twitter/android/cv;->g:Lcom/twitter/android/av;

    invoke-interface {v1, p1, v4, v0}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 356
    :cond_4
    return-object v4

    :cond_5
    move v0, v2

    .line 323
    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 326
    goto :goto_1

    :cond_7
    move v3, v2

    .line 330
    goto :goto_2

    :cond_8
    move v1, v2

    .line 334
    goto :goto_3

    .line 345
    :cond_9
    const/4 v1, -0x1

    goto :goto_4

    .line 349
    :cond_a
    iget-object v1, p0, Lcom/twitter/android/cv;->h:Lcom/twitter/ui/view/h;

    goto :goto_5
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 443
    iget-object v0, p0, Lcom/twitter/android/cv;->j:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 444
    return-void
.end method

.method protected final a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/timeline/bk;)V
    .locals 0

    .prologue
    .line 505
    return-void
.end method

.method protected a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/timeline/bk;I)V
    .locals 1

    .prologue
    .line 294
    instance-of v0, p3, Lcom/twitter/android/timeline/aq;

    if-eqz v0, :cond_0

    .line 295
    check-cast p3, Lcom/twitter/android/timeline/aq;

    invoke-virtual {p0, p1, p3}, Lcom/twitter/android/cv;->a(Landroid/view/View;Lcom/twitter/android/timeline/aq;)V

    .line 299
    :goto_0
    return-void

    .line 297
    :cond_0
    check-cast p3, Lcom/twitter/android/timeline/cd;

    invoke-virtual {p0, p1, p3, p4}, Lcom/twitter/android/cv;->a(Landroid/view/View;Lcom/twitter/android/timeline/cd;I)Lcom/twitter/model/core/Tweet;

    goto :goto_0
.end method

.method protected bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 80
    check-cast p3, Lcom/twitter/android/timeline/bk;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/cv;->a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/timeline/bk;)V

    return-void
.end method

.method protected bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 80
    check-cast p3, Lcom/twitter/android/timeline/bk;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/android/cv;->a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/timeline/bk;I)V

    return-void
.end method

.method protected a(Landroid/view/View;Lcom/twitter/android/timeline/aq;)V
    .locals 4

    .prologue
    .line 302
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/GapView;

    .line 303
    iget-object v1, p0, Lcom/twitter/android/cv;->j:Ljava/util/ArrayList;

    iget-wide v2, p2, Lcom/twitter/android/timeline/aq;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GapView;->setSpinnerActive(Z)V

    .line 304
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 373
    iget-boolean v0, p0, Lcom/twitter/android/cv;->n:Z

    if-eq v0, p1, :cond_1

    .line 374
    iput-boolean p1, p0, Lcom/twitter/android/cv;->n:Z

    .line 375
    iget-boolean v0, p0, Lcom/twitter/android/cv;->n:Z

    if-nez v0, :cond_1

    .line 376
    iget-object v0, p0, Lcom/twitter/android/cv;->e:Lcom/twitter/util/collection/ReferenceList;

    invoke-virtual {v0}, Lcom/twitter/util/collection/ReferenceList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cu;

    .line 377
    if-eqz v0, :cond_0

    .line 378
    iget-object v0, v0, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->k()V

    goto :goto_0

    .line 383
    :cond_1
    return-void
.end method

.method public b(Landroid/database/Cursor;)Lcom/twitter/android/timeline/bl;
    .locals 2

    .prologue
    .line 458
    new-instance v0, Lcom/twitter/android/timeline/bl;

    iget-object v1, p0, Lcom/twitter/android/cv;->f:Lcbp;

    invoke-direct {v0, p1, v1}, Lcom/twitter/android/timeline/bl;-><init>(Landroid/database/Cursor;Lcbp;)V

    return-object v0
.end method

.method protected b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 279
    new-instance v0, Lcom/twitter/android/cu;

    invoke-direct {v0, p1}, Lcom/twitter/android/cu;-><init>(Landroid/view/View;)V

    .line 280
    iget-object v1, v0, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    invoke-direct {p0, v1}, Lcom/twitter/android/cv;->a(Lcom/twitter/library/widget/TweetView;)V

    .line 281
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 282
    iget-object v1, p0, Lcom/twitter/android/cv;->e:Lcom/twitter/util/collection/ReferenceList;

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/ReferenceList;->b(Ljava/lang/Object;)V

    .line 283
    return-void
.end method

.method public b(Lcom/twitter/android/av;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Lcom/twitter/model/core/Tweet;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 472
    iput-object p1, p0, Lcom/twitter/android/cv;->g:Lcom/twitter/android/av;

    .line 473
    return-void
.end method

.method protected c()Lcom/twitter/model/util/FriendshipCache;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/twitter/android/cv;->q:Lcom/twitter/model/util/FriendshipCache;

    return-object v0
.end method

.method public c(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 462
    invoke-virtual {p0}, Lcom/twitter/android/cv;->k()Lcjt;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/twitter/android/cv;->b(Landroid/database/Cursor;)Lcom/twitter/android/timeline/bl;

    move-result-object v1

    invoke-interface {v0, v1}, Lcjt;->a(Lcbi;)Lcbi;

    .line 463
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 476
    iget-boolean v0, p0, Lcom/twitter/android/cv;->m:Z

    if-eq v0, p1, :cond_0

    .line 477
    iput-boolean p1, p0, Lcom/twitter/android/cv;->m:Z

    .line 478
    invoke-virtual {p0}, Lcom/twitter/android/cv;->notifyDataSetChanged()V

    .line 480
    :cond_0
    return-void
.end method

.method public d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 448
    iget-object v0, p0, Lcom/twitter/android/cv;->j:Ljava/util/ArrayList;

    return-object v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/twitter/android/cv;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 453
    return-void
.end method

.method public f()Lcom/twitter/android/timeline/bl;
    .locals 1

    .prologue
    .line 468
    invoke-super {p0}, Lcjr;->g()Lcbi;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bl;

    return-object v0
.end method

.method public synthetic g()Lcbi;
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/twitter/android/cv;->f()Lcom/twitter/android/timeline/bl;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 435
    invoke-virtual {p0}, Lcom/twitter/android/cv;->f()Lcom/twitter/android/timeline/bl;

    move-result-object v0

    .line 436
    if-nez v0, :cond_0

    .line 437
    const-wide/16 v0, -0x1

    .line 439
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {v0, p1}, Lcom/twitter/android/timeline/bl;->d(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0, p1}, Lcom/twitter/android/cv;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk;

    .line 213
    if-eqz v0, :cond_0

    .line 214
    invoke-virtual {p0, v0}, Lcom/twitter/android/cv;->a(Lcom/twitter/android/timeline/bk;)I

    move-result v0

    .line 216
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 231
    invoke-virtual {p0, p1}, Lcom/twitter/android/cv;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk;

    .line 232
    instance-of v0, v0, Lcom/twitter/android/timeline/ak;

    if-nez v0, :cond_0

    if-eqz p2, :cond_1

    const v0, 0x7f130061

    .line 233
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 238
    :cond_0
    const/4 v0, 0x0

    invoke-super {p0, p1, v0, p3}, Lcjr;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 240
    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcjr;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x2

    return v0
.end method
