.class public Lcom/twitter/android/initialization/NetworkInfoScribeInitializer;
.super Lanb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanb",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lanb;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    return v0
.end method

.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/initialization/NetworkInfoScribeInitializer;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 4

    .prologue
    .line 26
    invoke-static {}, Lcom/twitter/util/android/b;->a()Lcom/twitter/util/android/b;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/metrics/b;

    invoke-direct {v1}, Lcom/twitter/library/metrics/b;-><init>()V

    invoke-virtual {v0, v1}, Lcom/twitter/util/android/b;->a(Lcom/twitter/util/android/b$a;)V

    .line 27
    invoke-static {}, Lcom/twitter/util/android/b;->a()Lcom/twitter/util/android/b;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/aa;

    .line 28
    invoke-static {}, Lalc;->d()Lalc;

    move-result-object v2

    invoke-virtual {v2}, Lalc;->s()Lcqq;

    move-result-object v2

    invoke-virtual {v2}, Lcqq;->b()Lcqs;

    move-result-object v2

    .line 29
    invoke-static {}, Lalc;->d()Lalc;

    move-result-object v3

    invoke-virtual {v3}, Lalc;->s()Lcqq;

    move-result-object v3

    invoke-virtual {v3}, Lcqq;->a()Lcqt;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/network/aa;-><init>(Lcqs;Lcqt;)V

    .line 27
    invoke-virtual {v0, v1}, Lcom/twitter/util/android/b;->a(Lcom/twitter/util/android/b$a;)V

    .line 30
    return-void
.end method
