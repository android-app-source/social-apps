.class public Lcom/twitter/android/initialization/AppSessionInitializer;
.super Lanb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanb",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lanb;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;JJ)V
    .locals 7

    .prologue
    .line 149
    new-instance v6, Lcnz;

    invoke-direct {v6, p1, p2}, Lcnz;-><init>(J)V

    .line 151
    invoke-static {v6}, Lbnt;->b(Lcnz;)Lbnt;

    move-result-object v5

    .line 152
    new-instance v1, Lcom/twitter/android/initialization/AppSessionInitializer$2;

    move-wide v2, p1

    move-object v4, p0

    invoke-direct/range {v1 .. v6}, Lcom/twitter/android/initialization/AppSessionInitializer$2;-><init>(JLandroid/content/Context;Lbnt;Lcnz;)V

    invoke-static {v1}, Lrx/c;->a(Ljava/util/concurrent/Callable;)Lrx/c;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 166
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v2

    invoke-virtual {v0, p3, p4, v1, v2}, Lrx/c;->d(JLjava/util/concurrent/TimeUnit;Lrx/f;)Lrx/c;

    move-result-object v0

    .line 167
    invoke-static {}, Lcqw;->d()Lcqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 168
    return-void
.end method

.method static synthetic a(Landroid/content/Context;Lbnt;)V
    .locals 0

    .prologue
    .line 50
    invoke-static {p0, p1}, Lcom/twitter/android/initialization/AppSessionInitializer;->b(Landroid/content/Context;Lbnt;)V

    return-void
.end method

.method static a(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 82
    invoke-static {p0}, Lcom/twitter/library/platform/TwitterDataSyncService;->a(Landroid/content/Context;)V

    .line 83
    return-void
.end method

.method static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Z)V
    .locals 4

    .prologue
    .line 110
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 111
    if-eqz p2, :cond_0

    .line 113
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/p;->a()V

    .line 116
    invoke-static {p0}, Lcom/twitter/library/media/manager/g;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/media/manager/g;->g()V

    .line 119
    :cond_0
    new-instance v2, Lcom/twitter/util/a;

    invoke-direct {v2, p0, v0, v1}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 120
    invoke-virtual {v2}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/util/a$a;->a()Lcom/twitter/util/a$a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/util/a$a;->apply()V

    .line 121
    const-wide/32 v2, 0x493e0

    invoke-static {p0, v0, v1, v2, v3}, Lcom/twitter/android/initialization/AppSessionInitializer;->a(Landroid/content/Context;JJ)V

    .line 123
    invoke-static {p0}, Lcom/twitter/library/platform/TwitterDataSyncService;->a(Landroid/content/Context;)V

    .line 124
    invoke-static {p0}, Lcom/twitter/library/util/v;->a(Landroid/content/Context;)Lcom/twitter/library/util/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/util/v;->a()V

    .line 125
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 208
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 209
    invoke-virtual {p0, p1}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    new-instance v0, Landroid/database/sqlite/SQLiteException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Cannot delete database: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/sqlite/SQLiteException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 212
    :cond_0
    return-void
.end method

.method private static b(Landroid/content/Context;Lbnt;)V
    .locals 9

    .prologue
    const/4 v3, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 172
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 173
    const/4 v1, 0x5

    new-array v2, v1, [Landroid/database/sqlite/SQLiteOpenHelper;

    .line 174
    invoke-virtual {p1}, Lbnt;->ag()Lcom/twitter/library/provider/t;

    move-result-object v1

    aput-object v1, v2, v0

    .line 175
    invoke-virtual {p1}, Lbnt;->ai()Lcom/twitter/library/provider/h;

    move-result-object v1

    aput-object v1, v2, v6

    .line 176
    invoke-virtual {p1}, Lbnt;->ak()Lcom/twitter/library/scribe/ScribeDatabaseHelper;

    move-result-object v1

    aput-object v1, v2, v7

    .line 177
    invoke-virtual {p1}, Lbnt;->al()Lavz;

    move-result-object v1

    aput-object v1, v2, v8

    .line 178
    invoke-virtual {p1}, Lbnt;->am()Lcom/twitter/library/database/dm/a;

    move-result-object v1

    aput-object v1, v2, v3

    .line 180
    invoke-virtual {p1}, Lbnt;->ar()Lcnz;

    move-result-object v1

    invoke-virtual {v1}, Lcnz;->b()J

    move-result-wide v4

    .line 181
    new-array v3, v3, [Ljava/lang/String;

    .line 182
    invoke-static {v4, v5}, Lcom/twitter/library/provider/t;->l(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    .line 184
    invoke-static {v4, v5}, Lcom/twitter/library/scribe/ScribeDatabaseHelper;->b(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v6

    .line 185
    invoke-static {v4, v5}, Lavz;->a(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v7

    .line 186
    invoke-static {v4, v5}, Lcom/twitter/library/database/dm/a;->a(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v8

    .line 190
    array-length v6, v2

    move v1, v0

    :goto_0
    if-ge v1, v6, :cond_0

    aget-object v7, v2, v1

    .line 191
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteOpenHelper;->close()V

    .line 190
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 194
    :cond_0
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 195
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->b()Lcom/twitter/library/client/Session$LoginStatus;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    if-ne v1, v2, :cond_2

    .line 197
    array-length v1, v3

    :goto_1
    if-ge v0, v1, :cond_1

    aget-object v2, v3, v0

    .line 198
    invoke-static {p0, v2}, Lcom/twitter/android/initialization/AppSessionInitializer;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 197
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 200
    :cond_1
    invoke-static {}, Lcom/twitter/library/provider/j;->c()Lcom/twitter/library/provider/j;

    move-result-object v0

    .line 201
    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/provider/j;->d(J)I

    .line 202
    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/provider/j;->c(J)I

    .line 204
    :cond_2
    return-void
.end method

.method static b(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 90
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 91
    new-instance v2, Lcnz;

    invoke-direct {v2, v0, v1}, Lcnz;-><init>(J)V

    invoke-static {v2}, Lamu;->e(Lcnz;)V

    .line 92
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v2

    invoke-virtual {v2}, Lcqq;->c()Lcqr;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcqr;->a(J)V

    .line 94
    invoke-static {p0}, Lnq;->a(Landroid/content/Context;)Lnq;

    move-result-object v2

    invoke-virtual {v2}, Lnq;->a()V

    .line 96
    invoke-static {}, Lcom/twitter/util/android/b;->a()Lcom/twitter/util/android/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/util/android/b;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 97
    const-wide/32 v2, 0xea60

    invoke-static {v0, v1, v2, v3}, Lbpy;->a(JJ)V

    .line 100
    :cond_0
    invoke-static {v4}, Lcom/twitter/android/ContactsUploadService;->a(Z)V

    .line 101
    invoke-static {v4}, Lcom/twitter/android/ContactsUploadService;->b(Z)V

    .line 103
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "app::switch_account::success"

    aput-object v2, v1, v4

    .line 104
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 105
    invoke-static {p0}, Lcom/twitter/android/az;->a(Landroid/content/Context;)Lcom/twitter/android/az;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/az;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 106
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 107
    return-void
.end method

.method static c(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 4

    .prologue
    .line 132
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "notification:status_bar::unauthorised:impression"

    aput-object v3, v1, v2

    .line 133
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 132
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 134
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/b;->a(Ljava/lang/String;)Landroid/accounts/AccountManagerFuture;

    .line 135
    invoke-static {p0}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;)Lcom/twitter/android/client/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/l;->a(Lcom/twitter/library/client/Session;)V

    .line 136
    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 50
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/initialization/AppSessionInitializer;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 2

    .prologue
    .line 56
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 57
    new-instance v1, Lcom/twitter/android/initialization/AppSessionInitializer$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/initialization/AppSessionInitializer$1;-><init>(Lcom/twitter/android/initialization/AppSessionInitializer;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/u;)V

    .line 78
    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/twitter/android/initialization/AppSessionInitializer;->b(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 79
    return-void
.end method
