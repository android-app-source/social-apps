.class Lcom/twitter/android/initialization/JobCreatorInitializer$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/evernote/android/job/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/initialization/JobCreatorInitializer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/initialization/JobCreatorInitializer$1;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/twitter/android/initialization/JobCreatorInitializer$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/evernote/android/job/Job;
    .locals 3

    .prologue
    .line 41
    const-string/jumbo v0, "DatabaseCleanUpJob"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    new-instance v0, Lcom/twitter/library/provider/e;

    invoke-direct {v0}, Lcom/twitter/library/provider/e;-><init>()V

    .line 55
    :goto_0
    return-object v0

    .line 43
    :cond_0
    const-string/jumbo v0, "TpmIdSyncJob_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 44
    new-instance v0, Lrs;

    invoke-direct {v0}, Lrs;-><init>()V

    goto :goto_0

    .line 45
    :cond_1
    invoke-static {p1}, Lxb;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 46
    new-instance v0, Lxb;

    invoke-direct {v0}, Lxb;-><init>()V

    goto :goto_0

    .line 47
    :cond_2
    const-string/jumbo v0, "CheckSystemPushEnabled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 48
    new-instance v0, Lcom/twitter/library/platform/notifications/a;

    invoke-direct {v0}, Lcom/twitter/library/platform/notifications/a;-><init>()V

    goto :goto_0

    .line 49
    :cond_3
    const-string/jumbo v0, "ScribeFlushJob"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 50
    new-instance v0, Lcom/twitter/library/scribe/i;

    invoke-direct {v0}, Lcom/twitter/library/scribe/i;-><init>()V

    goto :goto_0

    .line 51
    :cond_4
    const-string/jumbo v0, "GcmRetryJob"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 52
    new-instance v0, Lbuq;

    invoke-direct {v0}, Lbuq;-><init>()V

    goto :goto_0

    .line 54
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " has not been added to the JobCreator"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 55
    const/4 v0, 0x0

    goto :goto_0
.end method
