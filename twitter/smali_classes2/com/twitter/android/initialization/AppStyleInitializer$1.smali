.class Lcom/twitter/android/initialization/AppStyleInitializer$1;
.super Lcom/twitter/app/common/util/b$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/initialization/AppStyleInitializer;->a(Landroid/content/Context;Ljava/lang/Void;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/initialization/AppStyleInitializer;


# direct methods
.method constructor <init>(Lcom/twitter/android/initialization/AppStyleInitializer;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/twitter/android/initialization/AppStyleInitializer$1;->a:Lcom/twitter/android/initialization/AppStyleInitializer;

    invoke-direct {p0}, Lcom/twitter/app/common/util/b$a;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 34
    invoke-static {p1}, Lcom/twitter/library/util/d;->a(Landroid/content/Context;)V

    .line 35
    iget-object v0, p0, Lcom/twitter/android/initialization/AppStyleInitializer$1;->a:Lcom/twitter/android/initialization/AppStyleInitializer;

    invoke-static {v0, p1}, Lcom/twitter/android/initialization/AppStyleInitializer;->a(Lcom/twitter/android/initialization/AppStyleInitializer;Landroid/app/Activity;)V

    .line 36
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 40
    invoke-static {}, Lcmj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    invoke-virtual {p1}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v1, 0x7f0d01b9

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 43
    :cond_0
    return-void
.end method
