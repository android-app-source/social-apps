.class public Lcom/twitter/android/initialization/GeoInitializer;
.super Lanb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanb",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lanb;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/initialization/GeoInitializer;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 5

    .prologue
    .line 22
    const-string/jumbo v0, "geo_wifi_logging_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    new-instance v1, Lcom/twitter/library/geo/wifilog/WifiLogReceiver;

    new-instance v2, Lcom/twitter/library/geo/wifilog/a;

    invoke-direct {v2}, Lcom/twitter/library/geo/wifilog/a;-><init>()V

    new-instance v3, Lcom/twitter/library/geo/wifilog/b;

    invoke-direct {v3}, Lcom/twitter/library/geo/wifilog/b;-><init>()V

    .line 26
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v4

    const-string/jumbo v0, "wifi"

    .line 27
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/twitter/library/geo/wifilog/WifiLogReceiver;-><init>(Lcom/twitter/library/geo/wifilog/a;Lcom/twitter/library/geo/wifilog/b;Lcom/twitter/library/client/p;Landroid/net/wifi/WifiManager;)V

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v2, "com.twitter.library.geo.LOCATION_CHANGED"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 23
    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 30
    :cond_0
    return-void
.end method
