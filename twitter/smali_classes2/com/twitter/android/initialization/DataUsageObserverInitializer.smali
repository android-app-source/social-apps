.class public Lcom/twitter/android/initialization/DataUsageObserverInitializer;
.super Lanb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanb",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lanb;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    return v0
.end method

.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 16
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/initialization/DataUsageObserverInitializer;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 2

    .prologue
    .line 24
    new-instance v0, Lcom/twitter/library/metrics/DataUsageEventListener;

    invoke-direct {v0, p1}, Lcom/twitter/library/metrics/DataUsageEventListener;-><init>(Landroid/content/Context;)V

    .line 25
    invoke-static {}, Lcom/twitter/util/android/b;->a()Lcom/twitter/util/android/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/util/android/b;->a(Lcom/twitter/util/android/b$a;)V

    .line 26
    invoke-static {}, Lcom/twitter/library/network/b;->a()Lcom/twitter/library/network/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/b;->a(Lcom/twitter/util/q;)Z

    .line 27
    return-void
.end method
