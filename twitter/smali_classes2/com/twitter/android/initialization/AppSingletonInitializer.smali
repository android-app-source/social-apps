.class public Lcom/twitter/android/initialization/AppSingletonInitializer;
.super Lanb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanb",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lanb;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 32
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/initialization/AppSingletonInitializer;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 2

    .prologue
    .line 35
    invoke-static {p1}, Lcom/twitter/android/client/k;->a(Landroid/content/Context;)Lcom/twitter/android/client/k;

    .line 36
    invoke-static {p1}, Lcom/twitter/library/media/manager/g;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    .line 37
    invoke-static {p1}, Lcom/twitter/android/util/t;->a(Landroid/content/Context;)Lcom/twitter/android/util/s;

    .line 38
    new-instance v0, Laep;

    invoke-direct {v0}, Laep;-><init>()V

    invoke-static {v0}, Lbsx;->a(Lbsx;)V

    .line 40
    invoke-static {}, Lcom/twitter/android/card/h;->a()V

    .line 41
    invoke-static {}, Lcom/twitter/android/client/h;->a()V

    .line 42
    new-instance v0, Lcom/twitter/android/av/af;

    invoke-direct {v0}, Lcom/twitter/android/av/af;-><init>()V

    invoke-static {v0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/j;)V

    .line 43
    sget-object v0, Lcom/twitter/android/av/ac;->a:Lcom/twitter/library/av/playback/g;

    invoke-static {v0}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/playback/g;)V

    .line 44
    new-instance v0, Lakc;

    invoke-direct {v0}, Lakc;-><init>()V

    invoke-static {v0}, Lbxy;->a(Lbxy$a;)V

    .line 47
    invoke-static {p1}, Lcom/twitter/library/platform/TwitterDataSyncService;->a(Landroid/content/Context;)V

    .line 49
    new-instance v0, Lcom/twitter/library/media/manager/TwitterImageRequester$Factory;

    invoke-direct {v0}, Lcom/twitter/library/media/manager/TwitterImageRequester$Factory;-><init>()V

    invoke-static {v0}, Lcom/twitter/media/ui/image/BaseMediaImageView;->setImageRequesterFactory(Lcom/twitter/media/request/ImageRequester$Factory;)V

    .line 52
    invoke-static {p1}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v0

    .line 53
    new-instance v1, Lmh;

    invoke-direct {v1, p1}, Lmh;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lbaa;->a(Lazz;)V

    .line 54
    new-instance v1, Lmj;

    invoke-direct {v1, p1}, Lmj;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lbaa;->a(Lazy;)V

    .line 55
    new-instance v1, Lmi;

    invoke-direct {v1, p1}, Lmi;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lbaa;->a(Lazy;)V

    .line 57
    invoke-static {p1}, Lpt;->a(Landroid/content/Context;)V

    .line 59
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/client/c;

    invoke-direct {v1, p1}, Lcom/twitter/android/client/c;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/t;)V

    .line 60
    return-void
.end method
