.class Lcom/twitter/android/initialization/ErrorReporterInitializer$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/initialization/ErrorReporterInitializer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Z

.field private final c:Ljava/lang/Thread$UncaughtExceptionHandler;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/Thread$UncaughtExceptionHandler;Z)V
    .locals 1

    .prologue
    .line 171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/initialization/ErrorReporterInitializer$a;->a:Landroid/content/Context;

    .line 173
    iput-boolean p3, p0, Lcom/twitter/android/initialization/ErrorReporterInitializer$a;->b:Z

    .line 174
    iput-object p2, p0, Lcom/twitter/android/initialization/ErrorReporterInitializer$a;->c:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 175
    return-void
.end method


# virtual methods
.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/twitter/android/initialization/ErrorReporterInitializer$a;->b:Z

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/twitter/android/initialization/ErrorReporterInitializer$a;->a:Landroid/content/Context;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3, p2}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;JLjava/lang/Throwable;)V

    .line 183
    :cond_0
    instance-of v0, p2, Ljava/lang/OutOfMemoryError;

    if-eqz v0, :cond_2

    .line 185
    invoke-static {p2}, Lcpd;->d(Ljava/lang/Throwable;)V

    .line 211
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/initialization/ErrorReporterInitializer$a;->c:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v0, :cond_1

    .line 212
    iget-object v0, p0, Lcom/twitter/android/initialization/ErrorReporterInitializer$a;->c:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 214
    :cond_1
    return-void

    .line 187
    :cond_2
    invoke-static {}, Lcpd;->a()Lcpd;

    move-result-object v0

    invoke-virtual {v0}, Lcpd;->b()Lcpa;

    move-result-object v1

    .line 188
    invoke-virtual {v1}, Lcpa;->a()I

    .line 191
    :try_start_0
    new-instance v0, Lcom/twitter/android/initialization/ErrorReporterInitializer$a$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/initialization/ErrorReporterInitializer$a$1;-><init>(Lcom/twitter/android/initialization/ErrorReporterInitializer$a;)V

    invoke-static {v0}, Lcom/twitter/util/f;->a(Lcom/twitter/util/concurrent/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 198
    const-string/jumbo v2, "Logcat"

    const-string/jumbo v3, "\n"

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v3, v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcpa;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    invoke-static {}, Lcom/twitter/app/common/util/b;->a()Lcom/twitter/app/common/util/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/util/b;->b()Landroid/app/Activity;

    move-result-object v0

    .line 201
    if-eqz v0, :cond_3

    .line 202
    const-string/jumbo v2, "Last Resumed Activity"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcpa;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    :cond_3
    invoke-static {p2}, Lcpd;->d(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    invoke-virtual {v1}, Lcpa;->b()I

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcpa;->b()I

    throw v0
.end method
