.class public Lcom/twitter/android/initialization/AdIdInitializer;
.super Lanb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanb",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lanb;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    return v0
.end method

.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 14
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/initialization/AdIdInitializer;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Void;)V
    .locals 4

    .prologue
    .line 26
    sget-object v0, Lcom/twitter/library/client/b;->a:Lcom/twitter/library/client/b;

    invoke-static {p1}, Lcom/twitter/library/api/c;->a(Landroid/content/Context;)Lcom/twitter/library/api/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/b;->a(Lcom/twitter/library/api/c;)V

    .line 27
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 28
    const-string/jumbo v2, "app::::launch"

    const/4 v3, 0x1

    invoke-static {p1, v0, v1, v2, v3}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;JLjava/lang/String;Z)V

    .line 29
    return-void
.end method
