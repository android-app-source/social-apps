.class Lcom/twitter/android/initialization/ErrorReporterInitializer$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcph$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/initialization/ErrorReporterInitializer;->a(Landroid/content/Context;Ljava/lang/Void;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/initialization/ErrorReporterInitializer;

.field private final b:Ljava/lang/StringBuilder;


# direct methods
.method constructor <init>(Lcom/twitter/android/initialization/ErrorReporterInitializer;)V
    .locals 2

    .prologue
    .line 106
    iput-object p1, p0, Lcom/twitter/android/initialization/ErrorReporterInitializer$3;->a:Lcom/twitter/android/initialization/ErrorReporterInitializer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/initialization/ErrorReporterInitializer$3;->b:Ljava/lang/StringBuilder;

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Lcpb;Z)V
    .locals 6

    .prologue
    .line 111
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/initialization/ErrorReporterInitializer$3;->b:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 112
    iget-object v0, p0, Lcom/twitter/android/initialization/ErrorReporterInitializer$3;->b:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "JVM Uptime: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 114
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    invoke-virtual {v1}, Lcof;->g()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 113
    invoke-static {v2, v3}, Lcom/twitter/util/aa;->f(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    invoke-static {}, Lcom/twitter/util/d;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/twitter/android/initialization/ErrorReporterInitializer$3;->b:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "\nFile Descriptor Count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 117
    invoke-static {}, Lcom/twitter/android/initialization/ErrorReporterInitializer;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    :cond_0
    invoke-static {}, Lcom/twitter/util/android/b;->a()Lcom/twitter/util/android/b;

    move-result-object v0

    .line 120
    iget-object v1, p0, Lcom/twitter/android/initialization/ErrorReporterInitializer$3;->b:Ljava/lang/StringBuilder;

    const-string/jumbo v2, "\nIn Foreground: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 121
    invoke-virtual {v0}, Lcom/twitter/util/android/b;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\nForeground Time: "

    .line 122
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 123
    invoke-virtual {v0}, Lcom/twitter/util/android/b;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/twitter/util/aa;->f(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    invoke-static {}, Lcom/twitter/app/common/util/b;->a()Lcom/twitter/app/common/util/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/util/b;->b()Landroid/app/Activity;

    move-result-object v0

    .line 125
    if-eqz v0, :cond_1

    .line 126
    iget-object v1, p0, Lcom/twitter/android/initialization/ErrorReporterInitializer$3;->b:Ljava/lang/StringBuilder;

    const-string/jumbo v2, "\nLast Resumed Activity:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 127
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    :cond_1
    const-string/jumbo v0, "application_state"

    iget-object v1, p0, Lcom/twitter/android/initialization/ErrorReporterInitializer$3;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    monitor-exit p0

    return-void

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
