.class final Lcom/twitter/android/initialization/AppSessionInitializer$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/initialization/AppSessionInitializer;->a(Landroid/content/Context;JJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:Lbnt;

.field final synthetic d:Lcnz;


# direct methods
.method constructor <init>(JLandroid/content/Context;Lbnt;Lcnz;)V
    .locals 1

    .prologue
    .line 152
    iput-wide p1, p0, Lcom/twitter/android/initialization/AppSessionInitializer$2;->a:J

    iput-object p3, p0, Lcom/twitter/android/initialization/AppSessionInitializer$2;->b:Landroid/content/Context;

    iput-object p4, p0, Lcom/twitter/android/initialization/AppSessionInitializer$2;->c:Lbnt;

    iput-object p5, p0, Lcom/twitter/android/initialization/AppSessionInitializer$2;->d:Lcnz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Void;
    .locals 4

    .prologue
    .line 156
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/initialization/AppSessionInitializer$2;->a:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/client/v;->c(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 157
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 158
    :goto_0
    if-nez v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/twitter/android/initialization/AppSessionInitializer$2;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/initialization/AppSessionInitializer$2;->c:Lbnt;

    invoke-static {v0, v1}, Lcom/twitter/android/initialization/AppSessionInitializer;->a(Landroid/content/Context;Lbnt;)V

    .line 161
    iget-object v0, p0, Lcom/twitter/android/initialization/AppSessionInitializer$2;->d:Lcnz;

    invoke-static {v0}, Lbnt;->d(Lcnz;)V

    .line 163
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 157
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/twitter/android/initialization/AppSessionInitializer$2;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
